// Include gulp
var gulp = require('gulp');

// Include Our Plugins
var _ = require('lodash');
var async = require('async');
var jshint = require('gulp-jshint');
var sass = require('gulp-sass');
var less = require('gulp-less');
var concat = require('gulp-concat');
var uglify = require('gulp-uglify');
var rename = require('gulp-rename');
var cleanCSS = require('gulp-clean-css');
var iconfont = require('gulp-iconfont');
var consolidate = require('gulp-consolidate');
var template = require('gulp-template-compile');
var runTimestamp = Math.round(Date.now()/1000);

var config = {
    bowerDir: './bower_components',
    bootstrapDir: './bower_components/bootstrap',
    adminLTE: './bower_components/admin-lte',
    fa: './bower_components/font-awesome',
    dir: './resources/assets',
    publicDir: './web/assets',
};


// Lint Task
gulp.task('lint', function() {
    return  gulp.src(config.dir+'/js/*.js')
        .pipe(jshint())
        .pipe(jshint.reporter('default'));
});


// Compile Our Sass
gulp.task('bootstrap-less', function() {
    return gulp.src([


        config.dir+'/less/bootstrap-less/bootstrap.less'])
        .pipe(less({
            paths: [ config.dir+'/less/bootstrap-less',config.bootstrapDir+'/less']
        }))
        .pipe(cleanCSS({compatibility: 'ie8'}))
        .pipe(concat('bootstrap.min.css'))
        .pipe(gulp.dest(config.publicDir+'/css'));
});



gulp.task('sass', function() {
    return gulp.src([

        ])
        .pipe(sass({
            paths: [ config.bowerDir + '/datatable-select/css/']
        }))
        //  .pipe(cleanCSS({compatibility: 'ie8'}))
        .pipe(concat('styles_sass.css'))
        .pipe(gulp.dest(config.dir+'/css'));
});





// Compile Less
gulp.task('less', function() {
    return gulp.src([
            config.bowerDir+'/jquery-ui/themes/base/jquery-ui.css',
            config.dir+'/less/tree.less',
            config.dir+'/less/skins/skin-green.less',
            config.dir+'/font-svg/simplassofont.css',
            config.adminLTE+'/plugins/pace/pace.css',
            config.bowerDir+'/datatables.net-bs/css/dataTables.bootstrap.css',
            config.bowerDir+'/datatables.net-keytable-bs/css/keyTable.dataTables.css',
            config.bowerDir+'/datatables.net-responsive-bs/css/responsive.dataTables.css',
            config.bowerDir+'/datatables.net-scroller-bs/css/scroller.bootstrap.css',
            config.bowerDir+'/datatables.net-select-bs/css/select.bootstrap.css',
            config.adminLTE+'/plugins/datepicker/datepicker3.css',
            config.adminLTE+'/plugins/select2/select2.css',
            config.bowerDir+'/select2-bootstrap-theme/dist/select2-bootstrap.css',
            config.bowerDir+'/bootstrap-switch/dist/css/bootstrap3/bootstrap-switch.css',

            config.bowerDir+'/jquery-upload-file/css/uploadfile.css',
            config.bowerDir+'/x-editable/dist/bootstrap3-editable/css/bootstrap-editable.css',
            config.dir+'/less/app.less'
        ]
    )
        .pipe(less({
            paths: [ config.dir+'/less',config.adminLTE+'/build/less',config.adminLTE+'/build/bootstrap-less',config.fa+'/less']
        }))
        .pipe(cleanCSS({compatibility: 'ie8'}))
        .pipe(concat('styles.css'))
        .pipe(gulp.dest(config.publicDir+'/css'));
});



// Compile css carto
gulp.task('carto', function() {
    return gulp.src([
            config.bowerDir+'/leaflet/dist/leaflet.css',
            config.bowerDir+'/leaflet-draw/dist/leaflet.draw.css',
            config.bowerDir+'/leaflet.markercluster/dist/MarkerCluster.css',
            config.bowerDir+'/leaflet.markercluster/dist/MarkerCluster.Default.css',
            config.bowerDir+'/leaflet-fullscreen/dist/Leaflet.fullscreen.css'
        ]
    )
        .pipe(cleanCSS({compatibility: 'ie8'}))
        .pipe(concat('carto.css'))
        .pipe(gulp.dest(config.publicDir+'/css'));
});

gulp.task('wysi', function() {
    return gulp.src([config.bowerDir+'/bootstrap3-wysihtml5-bower/dist/bootstrap3-wysihtml5.css',
        ]
    )
        .pipe(cleanCSS({compatibility: 'ie8'}))
        .pipe(concat('wysi.css'))
        .pipe(gulp.dest(config.publicDir+'/css'));
});

// Concatenate & Minify JS
gulp.task('scripts', function() {
    return  gulp.src([

        config.bowerDir+'/jquery/dist/jquery.js',
        config.bowerDir+'/jquery-form/jquery.form.js',
        config.bowerDir+'/mustache.js/mustache.min.js',
        config.bowerDir+'/datatables.net/js/jquery.dataTables.js',
        config.bowerDir+'/datatables.net-bs/js/dataTables.bootstrap.js',
        config.bowerDir+'/datatables.net-plugins/sorting/date-eu.js',
        config.bowerDir+'/datatables.net-keytable/js/keyTable.dataTables.js',
        config.bowerDir+'/datatables.net-responsive/js/responsive.dataTables.js',
        config.bowerDir+'/datatables.net-responsive-bs/js/responsive.dataTables.js',
        config.bowerDir+'/datatables.net-scroller/js/dataTables.scroller.js',
        config.bowerDir+'/datatables.net-select/js/dataTables.select.js',
        config.bowerDir+'/Sortable/Sortable.js',
        config.bowerDir+'/moment/moment.js',
        config.bowerDir+'/moment/locale/fr.js',
        config.bowerDir+'/js-cookie/src/js.cookie.js',
        config.dir+'/js/**/*.js'])
        .pipe(concat('scripts.js'))
        .pipe(uglify())
        .pipe(gulp.dest(config.publicDir+'/js'));
});




gulp.task('scripts_bas_de_page', function() {
   return  gulp.src([

	config.adminLTE+'/plugins/fastclick/fastclick.js',
	config.adminLTE+'/plugins/pace/pace.js',
//	config.adminLTE+'/plugins/jQueryUI/jquery-ui.js',
	config.bootstrapDir+'/dist/js/bootstrap.js',
	config.bowerDir+'/URI-js/src/URI.js',
	config.bowerDir+'/bootstrap-switch/dist/js/bootstrap-switch.js',

	config.adminLTE+'/plugins/input-mask/jquery.inputmask.js',
	config.adminLTE+'/plugins/input-mask/jquery.inputmask.date.extensions.js',
	config.adminLTE+'/plugins/input-mask/jquery.inputmask.extensions.js',
	config.adminLTE+'/plugins/datepicker/bootstrap-datepicker.js',
	config.adminLTE+'/plugins/datepicker/locales/bootstrap-datepicker.fr.js',
	config.adminLTE+'/plugins/select2/select2.full.js',
	config.bowerDir+'/typeahead.js/dist/bloodhound.js',
	config.bowerDir+'/typeahead.js/dist/typeahead.jquery.js',
	config.bowerDir+'/jquery-upload-file/js/jquery.uploadfile.min.js',
	config.bowerDir+'/x-editable/dist/bootstrap3-editable/js/bootstrap-editable.js',
	config.bowerDir+'/kolorwheel.js/KolorWheel.js'
	])

	.pipe(concat('scripts_footer.js'))
	.pipe(uglify())
	.pipe(gulp.dest(config.publicDir+'/js'));
});

gulp.task('wysi-js', function() {


    return gulp.src([
        config.bowerDir+'/wysihtml5x/parser_rules/advanced.js',
        config.bowerDir+'/bootstrap3-wysihtml5-bower/dist/bootstrap3-wysihtml5.all.js',
        config.bowerDir+'/bootstrap3-wysihtml5-bower/dist/locales/bootstrap-wysihtml5.fr-FR.js',
        config.dir+'/js-wysi/**/*.js'
        ])
        .pipe(concat('wysi.js'))
        .pipe(uglify())
        .pipe(gulp.dest(config.publicDir+'/js'));
});


// Compile js carto
gulp.task('carto-js', function() {
    return gulp.src([
            config.bowerDir+'/leaflet/dist/leaflet-src.js',
            config.bowerDir+'/leaflet.markercluster/dist/leaflet.markercluster-src.js',
            config.bowerDir+'/leaflet-draw/dist/leaflet.draw-src.js',
            config.bowerDir+'/leaflet-fullscreen/dist/Leaflet.fullscreen.js'
        ]
    )
        .pipe(concat('carto.js'))
        .pipe(uglify())
        .pipe(gulp.dest(config.publicDir+'/js'));
});


// Compile js stat
gulp.task('stat-js', function() {
    return gulp.src([
            config.bowerDir+'/chart.js/dist/Chart.js'
        ]
    )
        .pipe(concat('stat.js'))
        .pipe(uglify())
        .pipe(gulp.dest(config.publicDir+'/js'));
});




gulp.task('icons', function() {
    return  gulp.src([config.bowerDir+'/datatables/media/images/*.png',
        config.bowerDir+'/leaflet-fullscreen/dist/*.png'

    ])
        .pipe(gulp.dest(config.publicDir+'/images'));

});

gulp.task('fonts', function() {
    return  gulp.src([config.bowerDir+'/font-awesome/fonts/**/*.{ttf,woff,woff2,eof,svg}',
            config.adminLTE+'/bootstrap/fonts/**/*.{ttf,woff,woff2,eof,svg}'
        ]
    )
        .pipe(gulp.dest(config.publicDir+'/fonts'));

});



gulp.task('copie_fichier', function() {
    /*return  gulp.src([
        config.bowerDir+'/datatable-plugins/i18n/French.lang'
    ])
        .pipe(concat('french.lang'))
        .pipe(gulp.dest(config.publicDir+'/js'));*/
    return true;
});


gulp.task('carto-fichier', function() {
    return  gulp.src([
        config.bowerDir+'/leaflet/dist/images/*.png'
    ])
        .pipe(gulp.dest(config.publicDir+'/css/images'));
});




gulp.task('iconfont', function(done){
    var iconStream =  gulp.src([config.dir+'/font-svg/*.svg'])
        .pipe(iconfont({
            fontName: 'simplassofont',
            prependUnicode: true,
            timestamp: runTimestamp,
            normalize:true,
            formats: ['ttf', 'eot', 'woff','woff2','svg'] }));

    async.parallel([
        function handleGlyphs (cb) {
            iconStream.on('glyphs', function(glyphs, options) {
                gulp.src(config.dir+'/font-svg/template-font.css')
                    .pipe(consolidate('lodash', {
                        glyphs: glyphs,
                        fontName: 'simplassofont',
                        fontPath: '../fonts/',
                        className: 'ico',

                    }))
                    .pipe(rename("simplassofont.css"))
                    .pipe(gulp.dest(config.dir+'/font-svg'))
                    .on('finish', cb);
            });
        },
        function handleFonts (cb) {
            iconStream
                .pipe(gulp.dest(config.publicDir+'/fonts'))
                .on('finish', cb);
        }
    ], done);

});


gulp.task('templates', function() {
    gulp.src(config.dir+'/templates/**/*.html')
        .pipe(template({namespace:'templates'}))
        .pipe(concat('templates.js'))
        .pipe(gulp.dest(config.dir+'/js/'));
});





// Default Task
gulp.task('default',['wysi','wysi-js','carto','carto-js','carto-fichier','stat-js','templates','copie_fichier','bootstrap-less','sass','less', 'scripts', 'scripts_bas_de_page','icons','fonts'], function(){

    // Watch For Changes To Our JS
    gulp.watch(config.dir+'/js/**/*.js', ['scripts'] );
    // Watch For Changes To Our JS
    gulp.watch(config.dir+'/js-wysi/**/*.js', ['wysi-js'] );

    // Watch For Changes To Our SCSS
    gulp.watch(config.dir+'/less/**/*.less', ['less','bootstrap-less']);


});





