<?php
/*
if (isset($_SERVER['HTTP_CLIENT_IP'])
    || isset($_SERVER['HTTP_X_FORWARDED_FOR'])
    || !in_array(@$_SERVER['REMOTE_ADDR'], array('127.0.0.1', 'fe80::1', '::1'))
) {
    header('HTTP/1.0 403 Forbidden');
    exit('You are not allowed to access this file. Check '.basename(__FILE__).' for more information.');
}
*/
require_once __DIR__.'/../vendor/autoload.php';

Symfony\Component\Debug\Debug::enable();

use Silex\Application;
class SimplassoApplication extends Application
{
    use Application\TwigTrait;
    use Application\SecurityTrait;
    use Application\FormTrait;
    use Application\UrlGeneratorTrait;
    use Application\SwiftmailerTrait;
    use Application\MonologTrait;
    use Application\TranslationTrait;
}

$app = new SimplassoApplication();


require __DIR__.'/../config/dev.php';
require __DIR__.'/../src/app.php';
require __DIR__.'/../src/controllers.php';
ob_start();
$app->run();
