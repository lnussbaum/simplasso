<?php


require_once __DIR__.'/../vendor/autoload.php';

use Silex\Application;
use Silex\Provider\FormServiceProvider;
use Silex\Provider\MonologServiceProvider;
use Silex\Provider\SecurityServiceProvider;
use Silex\Provider\SessionServiceProvider;
use Silex\Provider\TranslationServiceProvider;
use Silex\Provider\TwigServiceProvider;
use Silex\Provider\ValidatorServiceProvider;
use Symfony\Component\Security\Core\Encoder\PasswordEncoder;
use Symfony\Component\Translation\Loader\YamlFileLoader;
use Symfony\Component\Validator\Constraints as Assert;
use WhoopsSilex\WhoopsServiceProvider;


class SimplassoApplication extends Application
{
    use Application\TwigTrait;
    use Application\SecurityTrait;
    use Application\FormTrait;
    use Application\UrlGeneratorTrait;
    use Application\SwiftmailerTrait;
    use Application\MonologTrait;
    use Application\TranslationTrait;
}

$app = new SimplassoApplication();
$app['basepath'] = realpath(__DIR__ . '/../');
initialiserVariablesApplication();

$app['debug'] = true;

if ($app['debug']) {
    $app->register(new WhoopsServiceProvider);

}

$app->register(new SessionServiceProvider(), [
    'session.storage.options' => [
        'name' => $app['session.variable.name'],
        'cookie_lifetime' => $app['session.variable.lifetime'] = 3600
    ]
]);
$app->register(new ValidatorServiceProvider());
$app->register(new FormServiceProvider());


$app['security.jwt'] = [
    'secret_key' => $app['securite.cookie.secret_key'],
    'life_time' => $app['securite.cookie.duree_de_vie'],
    'options' => [
        'username_claim' => 'name', // default name, option specifying claim containing username
        'header_name' => 'X-Access-Token', // default null, option for usage normal oauth2 header
        'token_prefix' => 'Bearer',
    ]
];

$app->register(new SecurityServiceProvider());
$app['users'] = function () use ($app) {
    return new UserProvider($app['db']);
};


$app['security.firewalls'] = array(

    'default' => array(
        'pattern' => '/',
        'anonymous' => true,

    ),
);
$app['security.default_encoder'] = function () use ($app) {
    return new PasswordEncoder();
};

ini_set('date.timezone', $app['date.timezone']);
date_default_timezone_set($app['date.timezone']);
$app->register(new Silex\Provider\LocaleServiceProvider());
$app->register(new TranslationServiceProvider(), array(
    'locale_fallbacks' => array('fr'),
));
$app->extend('translator', function ($translator, $app) {
    $translator->addLoader('yaml', new YamlFileLoader());
    foreach ($app['translator.messages'] as $locale => $files) {
        foreach ($files as $file) {
            $translator->addResource('yaml', $file, $locale);
        }
    }
    return $translator;
});


$app->register(new MonologServiceProvider(), array(
    'monolog.logfile' => $app['log.path'] . '/app.log',
    'monolog.name' => 'app',
    'monolog.level' => 300 // = Logger::WARNING
));

$app->register(new TwigServiceProvider(), array(
    'twig.options' => array(
        'cache' => isset($app['twig.options.cache']) ? $app['twig.options.cache'] : false,
        'strict_variables' => true
    ),
    'twig.path' => array($app['resources.path'] . '/views/install')
));


$app->register(new Silex\Provider\AssetServiceProvider(), array(
    'assets.version' => 'v1',
    'assets.version_format' => '%s?version=%s',
));

$app['request'] = function () use ($app) {
    return $app['request_stack']->getCurrentRequest();
};

require_once($app['basepath'] . '/src/inc/fonctions_twig_basic.php');



$app->error(function (\Exception $e, $code) use ($app) {
    if ($app['debug']) { $message = $code; return; }
    switch ($code) {
        case "404":
            $message = 'La page demandée n\'a pu être trouvée.';
            break;
        default:
            $message = 'Oups...c\'est embarrassant, une erreur s\'est produite';
    }
    return new Response($message);
});

$app->match('/erreur_403', function () use ($app) {
    include('erreur_403.php');
    return erreur_403();
})->bind('erreur_403');

$app->match('/erreur_404', function () use ($app) {
    include('erreur_404.php');
    return erreur_404();
})->bind('erreur_404');



$app->match('/', function () use ($app) {


    $args_twig=[];
    return $app['twig']->render('install.html.twig', $args_twig);
})->bind('install');

//$app->register(new Silex\Provider\DoctrineServiceProvider(), array('dbs.options' => $app['db.options']));

$app->run();
