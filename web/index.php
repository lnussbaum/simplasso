<?php


require_once __DIR__.'/../vendor/autoload.php';

use Silex\Application;
class SimplassoApplication extends Application
{
    use Application\TwigTrait;
    use Application\SecurityTrait;
    use Application\FormTrait;
    use Application\UrlGeneratorTrait;
    use Application\SwiftmailerTrait;
    use Application\MonologTrait;
    use Application\TranslationTrait;
}
$app = new SimplassoApplication();

require __DIR__.'/../config/prod.php';
require __DIR__.'/../src/app.php';
require __DIR__.'/../src/controllers.php';
$app->run();

