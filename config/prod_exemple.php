<?php
$app['basepath'] = realpath(__DIR__ . '/../');
initialiserVariablesApplication();

$app['basepath_spip'] = realpath(__DIR__.'/../../spip');
$app['locales'] = 'fr';


// Doctrine (db)
$app['db.options'] = array(
    'new' =>array(
        'driver' => 'pdo_mysql',
        'host' => 'localhost',
        'dbname' => 'nom_de_la_base',
        'user' => 'user',
        'password' => 'secret',
        'charset'   => 'utf8mb4'
    )
);





