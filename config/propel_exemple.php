<?php
$path = realpath(__DIR__ . '/../');

$project = 'simplasso';
return [
    'propel' => [
        'database' => [
            'connections' => [
                'simplasso' => [
                    'adapter'    => 'mysql',
                    'classname'  => 'Propel\Runtime\Connection\ConnectionWrapper',
                    'dsn'        => 'mysql:host=localhost;dbname=NOM_DE_LA_BASE;charset=utf8',
                    'user'       => 'UTILISATEUR_BASE',
                    'password'   => 'MOT_DE_PASSE',
                    'attributes' => ['ATTR_EMULATE_PREPARES'=> true],
                    'settings' => [
                        'charset'   =>'utf8mb4',
                        'queries'   =>
                            ['utf8'=> "SET NAMES utf8mb4 COLLATE utf8mb4_unicode_ci, COLLATION_CONNECTION = utf8mb4_unicode_ci, COLLATION_DATABASE = utf8mb4_unicode_ci, COLLATION_SERVER = utf8mb4_unicode_ci"]
                    ]
                ]
            ]
        ],
        'runtime' => [
            'defaultConnection' => 'simplasso',
            'connections' => ['simplasso']
        ],
        'generator' => [
            'defaultConnection' => 'simplasso',
            'connections' => ['simplasso'],
            'namespaceAutoPackage' => true,
            'objectModel' => ['defaultKeyType'=>'fieldname']
        ],


        'general' => [
            'project' => $project
        ],

        'paths' => [
            'outputDir' => $path.'/src/modeles',
            'schemaDir' =>  $path.'/config/propel',
            'phpDir' =>  $path.'/src/modeles',
            'phpConfDir' => $path.'/config/propel',
            'sqlDir' => $path.'/tmp/sql'
        ]


        /*          : current-path

            # The directory where Propel should output classes, sql, config, etc.
            # Default value is current path #
            outputDir: current-path

            # The directory where Propel should output generated object model classes.
            phpDir: current-path/generated-classes

            # The directory where Propel should output the compiled runtime configuration.
            phpConfDir: current-path/generated-conf

            # The directory where Propel should output the generated DDL (or data insert statements, etc.)
            sqlDir: current-path/generated-sql



      propel.phpconf.dir  = ${propel.project.dir}/../src/simplasso/propel/
      propel.php.dir      = ${propel.project.dir}/../src/
      propel.sql.dir      = ${propel.project.dir}/../tmp/sql/
      */

    ]
];

