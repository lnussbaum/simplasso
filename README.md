# Le logiciel Simplasso : au service des associations

## Présentation rapide

Simplasso est vraiment un super logiciel qui vous offre un éventail d'outils pour gérer votre 
association, et même plus.
De la tenue de votre fichier adhérents à l'édition des convocations en assemblée
en passant par l'extraction de données comptables, Simplasso vous assiste dans 
toutes les tâches administratives de votre association.


## Objectifs du logiciel et moyens

Les deux objectifs principaux du logiciel simplasso sont :

  * **Simplifier la gestion** et la mise à jour du fichier adhérent. Ce but vise donc de libérer du temps du responsable du fichier adhérent. Il pourra, de cette manière, engagé davantage d'énergie à la réalisation des objectifs premiers de l'association ou bien faire la sieste.
  * **Dynamiser la vie associative** par la mise en place d'outils simple pour diffuser l'information.

Pour répondre à ces objectif, le logiciel Simplasso appuie sur la force des réseaux informatiques qui permettent :

  * Un **accès partagé et simultané** aux fichiers par les différents responsables du fichier. Le travail ingrat de mise à jour du fichier, bien souvent dévolu à une seule personne peut être partagé par différents responsables.
  * Un accès pour chaque adhérent à sa fiche pour consulter ces informations et lui permettre si nécessaires  de les mettre à jour

La **répartition du travail** nécessaire s'accompagne aussi d'une attention soutenue à améliorer **l'efficience des fonctions** du logiciel. L'outil visent donc à minimiser au maximum le nombre de clics et le temps pour effectuer une action (exemple : 5 secondes, une frappe au clavier et 3 clics de souris suffissent pour enregistrer une cotisation  ), soulageons nos petits doigts et nos amis les souris.

## Fonctionnalités du logiciel

à Décrire


