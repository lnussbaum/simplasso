<?php
function ##OBJET##_liste()
{
    global $app;
    $args_twig=[
        'tab_col'=>##OBJET##_colonnes()
        ];
    return $app['twig']->render(fichier_twig(), $args_twig);

}


function ##OBJET##_colonnes(){

    $tab_colonne= array();
##COLONNES##
    $tab_colonne['action']=["orderable"=> false];
    $tab_colonne = datatable_complete_liste_colonne($tab_colonne);

    return $tab_colonne;

}





function action_##OBJET##_liste_dataliste()
{

    global $app;
    $request = $app['request'];
    $args = array();
    if (sac('objet') != '##OBJET##') {
        $args = array('prestation_type' => descr( sac('objet') . '.alias_valeur'));
    }
    list($sous_requete, $nb_total) = getSelectionObjetNb('##OBJET##',$args);

    $tab_data = array();
    $start = request_ou_options('start');
    $length = request_ou_options('length');

    $tab_tri = tri_dataliste();
    $tab_tri_sql = tri_dataliste_sql( '##OBJET##', $tab_tri);
    $tab_id = $app['db']->fetchAll($sous_requete . $tab_tri_sql . ' LIMIT ' . (intval($start)) . ',' . $length);

    foreach ($tab_id as &$result) {
        $result = $result['id_##OBJET##'];
    }

    $tab_colonnes = ##OBJET##_colonnes();
    $tab_data=array();

    if (!empty($tab_id)) {

        $tab = ##CLASS##Query::getAll($tab_id,tri_dataliste($tab_tri));
        $tab_data= datatable_prepare_data($tab,$tab_colonnes);

    }



    return $app->json([
        'draw' => $request->get('draw'),
        'recordsTotal' => $nb_total,
        'recordsFiltered' => $nb_total,
        'data' => $tab_data,
        'error' => ''
    ]);

}


