<?php

use Symfony\Component\Form\FormError as FormError;
use Symfony\Component\Form\Extension\Core\Type\FormType;
use Symfony\Component\Validator\Constraints as Assert;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Propel\Runtime\Map\TableMap;

function ##OBJET##_form()
{
    global $app;
    $ok = false;
    $args_rep=[];
    $request = $app['request'];
    if (sac('id')) {
        $modification = true;
        $objet_data = charger_objet();
        $data = $objet_data->toArray(TableMap::TYPE_FIELDNAME);
    } else {
        $modification = false;
        $objet_data = array();
        $data = array();
    }
    $builder = $app['form.factory']->createNamedBuilder('##OBJET##',##FIELDS##::class,$data);
    formSetAction($builder,$modification);

    $form = $builder->add('submit',SubmitType::class,
                array('label' => $app->trans('Enregistrer'), 'attr' => array('class' => 'btn-primary')))
            ->getForm();


    $form->handleRequest($request);
    if ($form->isSubmitted()) {
        $data = $form->getData();
        if ($form->isValid()) {
            if (!$modification)
                $objet_data = new ##PROPEL_OBJET##();
            $objet_data->fromArray($data);
            $objet_data->save();
            $args_rep['id'] = $objet_data->getPrimaryKey();
        }
    }

    return reponse_formulaire($form,$args_rep);
}
