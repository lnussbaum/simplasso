$.fn.extend({
    treed: function (o) {

        var openedClass = 'glyphicon-minus-sign';
        var closedClass = 'glyphicon-plus-sign';

        if (typeof o != 'undefined'){
            if (typeof o.openedClass != 'undefined'){
                openedClass = o.openedClass;
            }
            if (typeof o.closedClass != 'undefined'){
                closedClass = o.closedClass;
            }
        };

        //initialize each of the top levels
        var tree = $(this);
        tree.addClass("tree");
        tree.find('li').has("ul").each(function (i) {
            var branch = $(this); //li with children ul
            $(this).attr('id','branche'+i );
            branch.prepend("<i class='indicator glyphicon " + closedClass + "'></i>");
            branch.addClass('branch');
            branch.on('click', function (e) {
                if (this == e.target) {
                    var icon = $(this).children('i:first');
                    icon.toggleClass(openedClass + " " + closedClass);
                    $(this).children().children().toggle();
                    tmp = Cookies.get('tree_open');
                    if (tmp!='' && tmp != undefined)
                        tree_open = JSON.parse(tmp);
                    else
                        tree_open= new Array();
                    valeur=$(this).attr('id');
                    indice = $.inArray(valeur)
                    if (indice==-1)
                        tree_open.push($(this).attr('id'));
                    else
                        delete tree_open[indice];
                    Cookies.set('tree_open', tree_open);

                }
            })
            branch.children().children().toggle();
        });
        //fire event from the dynamically added icon
        tree.find('.branch .indicator').each(function(){
            $(this).on('click', function () {
                $(this).closest('li').click();
            });
        });
        //fire event to open branch if the li contains an anchor instead of text
        tree.find('.branch>a').each(function () {
            $(this).on('click', function (e) {
                $(this).closest('li').click();
                e.preventDefault();
            });
        });
        //fire event to open branch if the li contains a button instead of text
        tree.find('.branch>button').each(function () {
            $(this).on('click', function (e) {
                $(this).closest('li').click();
                e.preventDefault();
            });
        });

        tree_open =Cookies.get('tree_open');

        if( tree_open == undefined)
            tree_open= new Array();
        else
            tree_open = JSON.parse(tree_open);
        for (i=0; i<tree_open.length ;i++ ) {
            var icon = $("#" + tree_open[i]).children('i:first');
            icon.toggleClass(openedClass + " " + closedClass);
            $("#" + tree_open[i]).children().children().toggle();

        }

    }
});


