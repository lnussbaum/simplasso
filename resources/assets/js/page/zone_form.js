function initialisation_zone_form(conteneur){

    if (conteneur === undefined)
        conteneur='body';

    conteneur=$(conteneur);

    $('#zone_descriptif',conteneur).after('<div id="map_form" class="map_form carte"></div>')

    var objet=new Array();
    var map_zone = L.map('map_form').setView([50.5, 3], 7);
    var drawnItems = new L.FeatureGroup();
    map_zone.addLayer(drawnItems);

    // Initialise the draw control and pass it the FeatureGroup of editable layers
    var drawControl = new L.Control.Draw({
        edit: {
            featureGroup: drawnItems
        },
        'draw':{marker:false}

    });
    map_zone.addControl(drawControl);
    geojson=$('#zone_zone_geo',conteneur).val()
    if (geojson){
        var objet = JSON.parse(geojson);
        var len = objet.length;
        for (i = 0; i < len; i++) {
            type = objet[i]['type'];

            geojson=objet[i]['geojson']
            if (type=='circle'){
                latlng0=L.latLng(
                    geojson['geometry']['coordinates'][1],
                    geojson['geometry']['coordinates'][0]
                );
                layer=L.circle(latlng0, objet[i]['plus']['radius']);
            }
            else if (type=='rectangle') {
                latlng0=L.latLng(
                    geojson['geometry']['coordinates'][0][0][1],
                    geojson['geometry']['coordinates'][0][0][0]
                );
                latlng1=L.latLng(
                    geojson['geometry']['coordinates'][0][2][1],
                    geojson['geometry']['coordinates'][0][2][0]
                );

                layer=L.rectangle(L.latLngBounds(latlng0,latlng1));
            }
            else if (type=='polyline') {
                var lenj = geojson['geometry']['coordinates'][0].length;
                var tab_coords = new Array();
                var j= 0;
                for (j = 0; j < lenj; j++) {
                    tab_coords.push(L.GeoJSON.coordsToLatLng(geojson['geometry']['coordinates'][0][j]))
                }
                layer=L.polyline(tab_coords);
            }
            else if (type=='polygon') {
                var lenj = geojson['geometry']['coordinates'][0].length;
                var tab_coords = new Array();
                var j= 0;
                for (j = 0; j < lenj; j++) {
                    tab_coords.push(L.GeoJSON.coordsToLatLng(geojson['geometry']['coordinates'][0][j]))
                }
                layer=L.polygon(tab_coords);
            }
            drawnItems.addLayer(layer);
        }
    }

    // add an OpenStreetMap tile layer
    L.tileLayer('https://a.tiles.mapbox.com/v4/guillaumew.lno8fg97/{z}/{x}/{y}.png?access_token=pk.eyJ1IjoiZ3VpbGxhdW1ldyIsImEiOiJiREFkR3IwIn0.-CUhlefzCSV-FTSQTcre9g', {
        attribution: '&copy; <a href="http://osm.org/copyright">OpenStreetMap</a> contributors'
    }).addTo(map_zone);



    map_zone.on('draw:created', function (e) {
        var type = e.layerType,
            layer = e.layer;
        drawnItems.addLayer(layer);
        simplasso_maj_zone_geo()

    });

    if(drawnItems)
         map_zone.fitBounds(drawnItems.getBounds());

    function simplasso_maj_zone_geo(){

        var tab_layers = drawnItems.getLayers()
        var len = tab_layers.length;
        objet=new Array();
        for (i = 0; i < len; i++) {
            layer=tab_layers[i]
            plus=new Array();
            type="polyline"
            if (layer instanceof L.Circle) {
                plus={'radius':layer.getRadius()};
                type="circle"
            }
            else if (layer instanceof L.Rectangle) {
                type="rectangle"

            }
            else if (layer instanceof L.Polygon) {
                type="polygon"
            }
            objet.push({'type':type,'geojson':layer.toGeoJSON(),'plus':plus})
        }
        if(objet.length>0)
            $('#zone_zone_geo').val(JSON.stringify(objet));
        else
            $('#zone_zone_geo').val('');
    }

    map_zone.on('draw:edited', simplasso_maj_zone_geo);
    map_zone.on('draw:deleted',simplasso_maj_zone_geo);


}

