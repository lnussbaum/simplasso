
var delay = (function(){
    var timer = 0;
    return function(callback, ms){
        clearTimeout (timer);
        timer = setTimeout(callback, ms);
    };
})();


function membre_form(conteneur_f,url_generer_nom_court,url_generer_login,options) {
    conteneur_form = $(conteneur_f);
    initialisation_composant_form(conteneur_form, 'form_membre_');

    initialisation_composant_form_membre(conteneur_form, 'form_membre_');
    mode_modif = ($('form_membre_id_membre').val() > 0)
    mode_modif = ($('form_membre_id_individu').val() > 0)
    modif = new Array();
    modif['form_membre_nom'] = mode_modif;
    modif['form_membre_nomcourt'] = mode_modif;
    modif['form_membre_identifiant_interne'] = mode_modif;
    modif['form_membre_login'] = mode_modif;
    modif['form_observation'] = mode_modif;


    $('#form_membre_recherche_membre').keypress(function () {
        modif['form_membre_recherche_membre'] = true
    });

    $('#form_membre_recherche_individu').keypress(function () {
        modif['form_membre_recherche_individu'] = true
    });

    $('#form_membre_nom').keypress(function () {
        modif['form_membre_nom'] = true
    });
    $('#form_membre_nomcourt').keypress(function () {
        modif['form_membre_nomcourt'] = true
    });
    $('#form_membre_individu_login').keypress(function () {
        modif['form_membre_login'] = true
    });

    $('#form_membre_individu_nom_famille').keyup(function () {
        delay(function(){
        membre_form_interrationAutreChamps(url_generer_login,url_generer_nom_court,options);
        }, 500 );
    });
    $('#form_membre_individu_prenom').keyup(function () {

        delay(function(){
            membre_form_interrationAutreChamps(url_generer_login,url_generer_nom_court,options);
        }, 1000 );

    });



}




function membre_form_interrationAutreChamps(url_generer_login,url_generer_nom_court,options) {

    nom = $('#form_membre_individu_nom_famille').val();
    prenom = $('#form_membre_individu_prenom').val();
    if (!modif['form_membre_nom']) {
        //$('#champs_nomcourt').val(nom + ' ' + prenom);
        $('#form_membre_nom').val(nom + ' ' + prenom);
    }
    if (options['champs_nomcourt']==true){
        if (!modif['form_membre_nomcourt']) {
            $.get(url_generer_nom_court + nom + '&prenom=' + prenom,
                function (data) {
                    $('#form_membre_nomcourt').val(data);
                });
        }
    }
    if (!modif['form_membre_individu_login']) {
        $.get(url_generer_login + nom + '&prenom=' + prenom,
            function (data) {
                $('#form_membre_individu_login').val(data);
            });
    }
}
function initialisation_composant_form_membre(conteneur_form,identif) {

    if (identif == undefined)
        identif='';





    $('#tab_mode_individu li',conteneur_form).click(function(){
        $('#tab_mode_individu li').removeClass('active');
        $(this).addClass('active');
        valeur = $(this).attr('data-value')
        $('#'+identif+'mode_individu',conteneur_form).val(valeur);
        $('#tab_content_mode_individu div.tab-pane').removeClass('active');
        $('#tab_content_mode_individu #'+valeur).addClass('active').addClass('in');

    });
    valeur = $('#'+identif+'mode_individu',conteneur_form).val();
    $('#tab_mode_individu li').removeClass('active');
    $('#tab_mode_individu li[data-value='+valeur+']').addClass('active');
    $('#tab_content_mode_individu div.tab-pane').removeClass('active');
    $('#tab_content_mode_individu #'+valeur).addClass('active').addClass('in');

    initialisation_champs_autocomplete_objet(conteneur_form,identif + 'id_individu','individu');
    initialisation_champs_autocomplete_ville_cp(conteneur_form,identif+'individu_');
}
