function donne_id_entite(nom_formulaire,jq_form) {
    var id_entite = $('input[name=' + nom_formulaire + '\\\[id_entite\\\]]:checked',jq_form).val();
    if (id_entite === undefined)
        id_entite = $('#' + nom_formulaire + '_id_entite').val();
    return id_entite
}

function donne_objet_beneficiaire(nom_formulaire,jq_form) {

    var selecteur_objet_benef = 'input[name=' + nom_formulaire + '\\\[objet_beneficiaire\\\]]';
    var objet_benef = $(selecteur_objet_benef+':checked', jq_form).val();
    if (objet_benef === undefined)
        objet_benef = $('#' + nom_formulaire + '_objet_beneficiaire').val();
    return objet_benef
}


function init_servicerendu_form(vars) {


    container_form = 'form[name=' + vars['nom_formulaire'] + ']'
    initialisation_composant_form();
    var jq_form = $(container_form).eq(0);


    $('#' + vars['nom_formulaire'] + '_reglement_question', jq_form).bootstrapSwitch({
        size: 'mini',
        onText: 'Oui',
        offText: 'Non',
        onColor: 'success',
        offColor: 'info',

        onInit: function () {
        },
        onSwitchChange: function () {
            if ($(this).bootstrapSwitch('state')) {
                $(".fieldset_reglement").slideUp('fast');
            }
            else {
                $(".fieldset_reglement").slideDown();
            }
        }
    });

    var selecteur_objet_benef = 'input[name=' + vars['nom_formulaire'] + '\\\[objet_beneficiaire\\\]]';



if (vars['objet_beneficiaire']==''){


    prestation_individu = false;
    prestation_membre = false;


    id_entite = donne_id_entite(vars['nom_formulaire'],jq_form);
    nb_prestation_possible=0;
    objet_benef = 'individu';
    for (id_prestation in vars['combinaison'][id_entite]) {
        if ( vars['combinaison'][id_entite][id_prestation]['objet_beneficiaire'][objet_benef]==objet_benef ){
            if ( vars['prestation_type'] ){
                if (vars['combinaison'][id_entite][id_prestation]['prestation_type']== vars['prestation_type']) {
                    prestation_individu = true;
                    break;
                }
            } else
            {
                prestation_individu = true;
                break;
            }
        }
    }

    objet_benef = 'membre';
    for (id_prestation in vars['combinaison'][id_entite]) {
        if ( vars['combinaison'][id_entite][id_prestation]['objet_beneficiaire'][objet_benef]==objet_benef ){
            if ( vars['prestation_type'] ){
                if (vars['combinaison'][id_entite][id_prestation]['prestation_type']== vars['prestation_type']) {
                    prestation_membre = true;
                    break;
                }
            } else
            {
                prestation_membre = true;
                break;
            }
        }
    }


    if (prestation_membre && prestation_individu ) {


        $(selecteur_objet_benef, jq_form).change(function () {
            $objet_benef = $(selecteur_objet_benef + ':checked', jq_form).val();
            if ($objet_benef == 'individu') {
                $('#' + vars['nom_formulaire'] + '_id_membre', jq_form).parent().parent().parent().hide();
                $('#' + vars['nom_formulaire'] + '_id_individu', jq_form).parent().parent().parent().slideDown();
            }
            else {
                $('#' + vars['nom_formulaire'] + '_id_membre', jq_form).parent().parent().parent().slideDown();
                $('#' + vars['nom_formulaire'] + '_id_individu', jq_form).parent().parent().parent().hide();
            }
            $('input[name=' + vars['nom_formulaire'] + '\\\[id_entite\\\]]', jq_form).trigger("change");
        });
        initialisation_champs_autocomplete_objet(jq_form, vars['nom_formulaire'] + '_id_individu', 'individu');
        initialisation_champs_autocomplete_objet(jq_form, vars['nom_formulaire'] + '_id_membre', 'membre');
        $('#' + vars['nom_formulaire'] + '_id_individu', jq_form).parent().parent().parent().hide();
        $('#' + vars['nom_formulaire'] + '_id_membre', jq_form).parent().parent().parent().hide();
        if ($(selecteur_objet_benef + ':checked', jq_form).length) {
            $(selecteur_objet_benef, jq_form).trigger('change');
        }
    }else{


        if(prestation_membre){
            $('#'+vars['nom_formulaire']+'_id_membre', jq_form).prop('checked', true);
            $('#'+vars['nom_formulaire']+'_id_individu', jq_form).parent().parent().hide();
            initialisation_champs_autocomplete_objet(jq_form, vars['nom_formulaire'] + '_id_membre', 'membre');


        }else{

            $('#'+vars['nom_formulaire']+'_id_individu', jq_form).prop('checked', true);
            $('#'+vars['nom_formulaire']+'_id_membre', jq_form).parent().parent().hide();
            initialisation_champs_autocomplete_objet(jq_form, vars['nom_formulaire'] + '_id_individu', 'individu');

        }
       $(selecteur_objet_benef, jq_form).parent().parent().parent().hide();

    }
}
else{

    $('#'+vars['nom_formulaire']+'_id_individu', jq_form).parent().parent().hide();
    $('#'+vars['nom_formulaire']+'_id_membre', jq_form).parent().parent().hide();
    $(selecteur_objet_benef, jq_form).parent().parent().parent().hide();


}



//////////////////////////////////////////////////////
// Lorsque l'on change d'entite
/////////////////////////////////////////////////////
    $('input[name=' + vars['nom_formulaire'] + '\\\[id_entite\\\]]', jq_form).change(function () {
        var id_entite = $(this).val();


        if (vars['lot']) {
            var selecteur_prestationslot = 'input[name=' + vars['nom_formulaire'] + '\\\[id_prestationslot\\\]]';
            $(selecteur_prestationslot + ':visible:first', jq_form).prop('checked', true);
            $(selecteur_prestationslot + ':checked', jq_form).trigger("change");

        }
        else {

            var selecteur_prestation = 'input[name=' + vars['nom_formulaire'] + '\\\[pr0\\\]\\\[id_prestation\\\]]'
            $(selecteur_prestation, jq_form).parent().hide();

            nb_prestation_possible=0;
            objet_benef = donne_objet_beneficiaire(vars['nom_formulaire'],jq_form);
            for (id_prestation in vars['combinaison'][id_entite]) {
                if (vars['combinaison'][id_entite][id_prestation]['objet_beneficiaire'][objet_benef]==objet_benef){
                    if ( vars['prestation_type'] ){
                        if (vars['combinaison'][id_entite][id_prestation]['prestation_type']== vars['prestation_type']) {
                            $(selecteur_prestation + '[value=' + id_prestation + ']').parent().show();
                            nb_prestation_possible++;
                        }
                    } else
                    {
                        $(selecteur_prestation + '[value=' + id_prestation + ']').parent().show();
                        nb_prestation_possible++;
                    }
                }
                // $('#'+nom_formulaire+'_id_prestation input[value='+id_prestation+']')
            }
            if($(selecteur_prestation + ':checked:visible', jq_form).length == 0)
                $(selecteur_prestation + ':visible:first', jq_form).prop('checked', true);
            if (nb_prestation_possible == 1){
                $(selecteur_prestation, jq_form).parent().parent().parent().hide();
            }
            $(selecteur_prestation + ':checked', jq_form).trigger("change");

        }


        var selecteur_tresor = 'input[name=' + vars['nom_formulaire'] + '\\\[reglement\\\]\\\[id_tresor\\\]]'

        $(selecteur_tresor, jq_form).parent().hide();
        for (id_tresor in vars['modepaiement'][id_entite]) {
            $(selecteur_tresor + '[value=' + id_tresor + ']').parent().show();
        }

        if($(selecteur_tresor + ':checked:visible', jq_form).length == 0)
            $(selecteur_tresor + ':visible:first', jq_form).prop('checked', true);
        $(selecteur_tresor + ':checked', jq_form).trigger("change");

        var selecteur_solde = 'input[name=' + vars['nom_formulaire'] + '\\\[reglement\\\]\\\[solde\\\]\\\[\\\]]'
        $(selecteur_solde, jq_form).parent().hide();
        $(selecteur_solde, jq_form).removeAttr('checked');
        for (temp in vars['soldes'][id_entite]) {
            $(selecteur_solde + '[value=' + temp + ']').parent().show();
        }
    });


//////////////////////////////////////////////////////
// Lorsque l'on change de lot
/////////////////////////////////////////////////////

    $('input[name=' + vars['nom_formulaire'] + '\\\[id_prestationslot\\\]]', jq_form).change(function () {

        var id_prestation_lot = $(this).val();
        var id_entite = donne_id_entite(vars['nom_formulaire'],jq_form);
        tab_prestations = vars['combinaison_lot'][id_entite][id_prestation_lot];

        for (i = 0; i < vars['nb_prestation']; i++) {
            $('#prestation' + i).hide();
        }

        i = 0;

        for (id in tab_prestations) {
            id_prestation = tab_prestations[id]['id_prestation'];

            pr = vars['combinaison'][id_entite][id_prestation];

            var nom_form_pr0 = vars['nom_formulaire'] + '\\[pr' + i + '\\]';
            div_prestation = $('#prestation' + i);
            $(".panel-title a", div_prestation).text(pr['nom']).append(' <span class="sm">' + getPrestationType(pr['prestation_type'])['nom'] + '</span>');
            $(div_prestation).slideDown();

            var selecteur_prestation = 'input[name=' + nom_form_pr0 + '\\\[id_prestation\\\]]'
            $(selecteur_prestation, div_prestation).parent().parent().parent().hide();
            $(selecteur_prestation, div_prestation).removeAttr('checked');
            $(selecteur_prestation + '[value=' + id_prestation + ']:first', div_prestation).prop('checked', true);
            $('input[name=' + nom_form_pr0 + '\\\[quantite\\\]]').val(tab_prestations[id_prestation]['quantite']);
            $(selecteur_prestation + ':checked', div_prestation).trigger("change");
            i++;
        }


    });



    for (i = 0; i < vars['nb_prestation']; i++) {


        nom_form_pr = vars['nom_formulaire'] + '\\[pr' + i + '\\]';
        id_form_pr = vars['nom_formulaire'] + '_pr' + i;


        $('input[name=' + nom_form_pr + '\\\[id_prestation\\\]]', jq_form).attr('data-id', id_form_pr).attr('data-nom', nom_form_pr);





//////////////////////////////////////////////////////
// Lorsque l'on change de prestation
/////////////////////////////////////////////////////
        $('input[name=' + nom_form_pr + '\\\[id_prestation\\\]]', jq_form).change(function () {


            var id_entite = donne_id_entite(vars['nom_formulaire'],jq_form);

            var id_form_pr0 = $(this).attr('data-id');
            var nom_form_pr0 = $(this).attr('data-nom');

            var id_prestation = $(this).val();
            var pres = vars['combinaison'][id_entite][id_prestation];
            prestation_type = pres['prestation_type'];

            if (prestation_type > 2 && prestation_type !=6 ) {
                $('.champs_dates').hide();
            }
            else if(prestation_type == 6){
                $('.champs_date_fin').hide();
            }
            if (prestation_type != 2)
                $('.champs_abo').hide();

            if (!vars['modification']) {


                var selecteur_taux = 'input[name=' + nom_form_pr0 + '\\\[taux\\\]]';

                var selecteur_tva = 'label.label_tva';

                if (pres['id_tva'] == 0) {
                    $(selecteur_taux, jq_form).parent().hide();
                    $(selecteur_tva, jq_form).hide();


                } else {
                    $(selecteur_taux, jq_form).parent().show();
                    $(selecteur_tva, jq_form).show();
                }


                var selecteur_montant = 'input[name=' + nom_form_pr0 + '\\\[montant\\\]]';
                var selecteur_quantite = 'input[name=' + nom_form_pr0 + '\\\[quantite\\\]]';
                var selecteur_total = 'input[name=' + nom_form_pr0 + '\\\[total\\\]]';


                if (pres['quantite'] == 0) {

                    $(selecteur_quantite, jq_form).parent().parent().hide();
                    $(selecteur_total, jq_form).parent().parent().parent().hide();

                } else {
                    $(selecteur_quantite, jq_form).parent().parent().show();
                    $(selecteur_total, jq_form).parent().parent().parent().show();
                }

                $('#' + id_form_pr0 + '_date_debut').val(pres['date_debut']);
                $('#' + id_form_pr0 + '_date_fin').val(pres['date_fin']);
                var montant = parseFloat(donnePrix(pres['prix'], pres['date_debut']));
                $('#' + id_form_pr0 + '_montant').val(montant);
                qte = parseFloat($('#' + id_form_pr0 + '_quantite').val());
                $('#' + id_form_pr0 + '_total').val(montant * qte);

                mise_a_jour_montant_reglement(vars['nom_formulaire'], vars['lot']);

            }
        });




//$('#' + nom_formulaire + '_dateenregistrement').change(function () {
//    var id_entite = donne_id_entite();
//    var id_prestation = $('input[name=' + nom_formulaire + '\\\[id_prestation\\\]]:checked').val();
//    var pres = combinaison[id_entite][id_prestation]
//    var pu = $('input[name=' + nom_formulaire + '_montant').val();
//    var quantite = $('input[name=' + nom_formulaire + '_quantite').val();
//    var tvataux = donneTva(pres['tva'], $(this).val())
//
//    $('#' + nom_formulaire + '_taux').val(0.15)
//    var total1 = montanttotal(pu,quantite,tvataux)
//    $('#' + nom_formulaire + '_total').val(200)

//});



//////////////////////////////////////////////////////
// Lorsque l'on change la quantite
/////////////////////////////////////////////////////

        $('#' + id_form_pr + '_quantite').change(function () {

            var id_form_pr0 = $(this).attr('id')
            id_form_pr0 = id_form_pr0.substring(0, id_form_pr0.length - 9);
            var montant = $('#' + id_form_pr0 + '_montant').val();
            $('#' + id_form_pr0 + '_total').val(montant * $(this).val());
            mise_a_jour_montant_reglement(vars['nom_formulaire'], vars['lot']);
        });


//////////////////////////////////////////////////////
// Lorsque l'on change le montant
/////////////////////////////////////////////////////

        $('#' + id_form_pr + '_montant').change(function () {
            var id_form_pr0 = $(this).attr('id')
            id_form_pr0 = id_form_pr0.substring(0, id_form_pr0.length - 8);
            var quantite = $('#' + id_form_pr0 + '_quantite').val();
            $('#' + id_form_pr0 + '_total').val(quantite * $(this).val());
            mise_a_jour_montant_reglement(vars['nom_formulaire'], vars['lot']);
        });


//////////////////////////////////////////////////////
// Lorsque l'on change la date de début
/////////////////////////////////////////////////////
        $('#' + id_form_pr + '_date_debut').change(function () {
            var id_entite = donne_id_entite(vars['nom_formulaire'],jq_form);
            var id_prestation = $('input[name=' + nom_form_pr + '\\\[id_prestation\\\]]:checked').val();

            var pres = vars['combinaison'][id_entite][id_prestation];
            var montant = donnePrix(pres['prix'], $(this).val());

            prestation_type = pres['prestation_type'];
            $('#' + id_form_pr + '_montant').val(montant)


            mise_a_jour_montant_reglement(vars['nom_formulaire'], vars['lot']);
        });





        if (!vars['modification']) {

            if ($('input[name=' + nom_form_pr + '\\\[id_entite\\\]]:checked').length > 0)
                $('input[name=' + nom_form_pr + '\\\[id_entite\\\]]:checked').trigger("change");
            else {
                if ($('input[name=' + nom_form_pr + '\\\[id_prestation\\\]]:checked').length == 0)
                    $('input[name=' + nom_form_pr + '\\\[id_prestation\\\]]:first').prop('checked', true);
                $('input[name=' + nom_form_pr + '\\\[id_prestation\\\]]:checked').trigger("change");
                if ($('input[name=' + nom_form_pr + '\\\[reglement\\\]\\\[id_tresorn\\\]]:checked').length == 0)
                    $('input[name=' + nom_form_pr + '\\\[reglement\\\]\\\[id_tresor\\\]]:first').prop('checked', true);
                $('input[name=' + nom_form_pr + '\\\[reglement\\\]\\\[id_tresor\\\]]:checked').trigger("change");
            }

            var selecteur_tresor = 'input[name=' + nom_form_pr + '\\\[reglement\\\]\\\[id_tresor\\\]]:first'
            $(selecteur_tresor, jq_form).prop('checked', true);

        }


        $('#' + id_form_pr + '_montant').trigger("change");
        $('#' + id_form_pr + '_date_debut input').trigger("change");
    }

    $('input[name=' + vars['nom_formulaire'] + '\\\[id_entite\\\]]', jq_form).trigger("change");


}




function donnePrix(grille_prix, st_date) {

    var pattern = '`(\d{2})/(\d{2})/(\d{4})`';
    var date = new Date(st_date.replace(pattern, '$3-$2-$1'));
    var tdate = Math.round(date.getTime() / 1000);
    m = grille_prix[0].montant
    for (var i = 1; i < grille_prix.length; i++) {
        if (tdate < grille_prix[i].date_fin + 0)
            break;
        m = grille_prix[i].montant;
    }

    return m
}



function mise_a_jour_montant_reglement(nom_formulaire, lot) {

    var montant = 0;
    if (lot) {
        $('#' + nom_formulaire + '_reglement_montant').val(0)
        $('.panneau_prestation:visible').each(function (i) {
            montant = parseFloat($('#' + nom_formulaire + '_reglement_montant').val());
            $('#' + nom_formulaire + '_reglement_montant').val(montant + parseFloat($('#' + nom_formulaire + '_pr' + i + '_total').val()));

        })
    } else {

        $('#' + nom_formulaire + '_reglement_montant').val($('#' + nom_formulaire + '_pr0_montant').val());

    }


}
