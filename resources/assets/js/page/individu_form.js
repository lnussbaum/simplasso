function initialisation_composant_form_individu_liaison(conteneur_form,identif) {

    $('#individu_submit',conteneur_form).closest('div.form-group').insertAfter($('.fieldset_individu_creer',conteneur_form));

    if (identif == undefined)
        identif='';

    $('#tab_mode_individu li',conteneur_form).click(function(){
        $('#tab_mode_individu li').removeClass('active');
        $(this).addClass('active');
        valeur = $(this).attr('data-value')
        $('#'+identif+'_mode_individu',conteneur_form).val(valeur);
        $('#tab_content_mode_individu div.tab-pane').removeClass('active');
        $('#tab_content_mode_individu #'+valeur).addClass('active').addClass('in');

    });
    valeur = $('#'+identif+'_mode_individu',conteneur_form).val();
    $('#tab_mode_individu li').removeClass('active');
    $('#tab_mode_individu li[data-value='+valeur+']').addClass('active');
    $('#tab_content_mode_individu div.tab-pane').removeClass('active');
    $('#tab_content_mode_individu #'+valeur).addClass('active').addClass('in');

    initialisation_champs_autocomplete_objet(conteneur_form,identif + '_id_individu','individu');
}


