function initialiseXeditable(conteneur) {

    if (conteneur === undefined)
        conteneur = 'body';

    $('.xeditable', conteneur).each(function () {
        $(this).editable({
            ajaxOptions: {
                dataType: 'json'
            },
            send: 'always',
            success: function (response, newValue) {
                if (!response) {
                    return "Unknown error!";
                }

                if (response.success === false) {
                    return response.msg;
                }
            }
        })
    });
}
