function initialisation_position_table(conteneur_form) {

    champs_position = $('.position_table',conteneur_form);
    $(champs_position).hide();
    nb_rows = parseInt($(champs_position).attr('data-rows'));
    nb_cols= parseInt($(champs_position).attr('data-cols'));
    proportion= 1.33;
    $(champs_position).before('<table class="table_pos" style="width:175px;height:'+(proportion*175)+'px" ><tbody></tbody></table>');
    champs_table = $('.table_pos',conteneur_form);
    $(champs_table).css('border-collapse','separate');

    ajouter_cellule(champs_table,nb_rows,nb_cols);
    $('td',champs_table).css('border','2px #EEE solid');
    $('td',champs_table).hover(function(){
        $('td',champs_table).css( "background-color", "" );
        $(this).css( "background-color", "#2E9AFE" ).nextAll('td').css( "background-color", "#81BEF7" );
        $(this).parent('tr').nextAll('tr').children('td').css( "background-color", "#81BEF7" );
    });
    $('td',champs_table).click(function(){
        $('td',champs_table).css('border','2px #EEE solid').removeClass('on');
        $(this).css('border','#25F 2px solid').addClass('on');
        $(champs_position).val($(this).attr('data-value'));
    });
    $(champs_table).mouseleave(function(){
        $('td',champs_table).css( "background-color", "" );
        $('td.on',champs_table).css( "background-color", "#2E9AFE" ).nextAll('td').css( "background-color", "#81BEF7" );
        $('td.on',champs_table).parent('tr').nextAll('tr').children('td').css( "background-color", "#81BEF7" );
    });
    position = parseInt($(champs_position).val())+0;
    $('td[data-value='+position+']',champs_table).css('border','#25F 2px solid').addClass('on');
    console.log(position);
    $(champs_table).trigger('mouseleave');
}


function ajouter_cellule(table,nb_rows,nb_cols) {

    for (var i = 0; i < nb_rows; i++) {
        var tr = document.createElement('tr');
        for (var j = 0; j < nb_cols; j++) {
                var td = document.createElement('td');
                $(td).attr('data-value',(i*nb_cols)+j+1);
                tr.appendChild(td)
            }
        $('tbody',table).append(tr);
        }

}
