var table= new Array();
var timer_recherche = 0;


datatable_lang_fr = {
    "sProcessing":     "Traitement en cours...",
    "sSearch":         "Rechercher&nbsp;:",
    "sLengthMenu":     "Afficher _MENU_ &eacute;l&eacute;ments",
    "sInfo":           "Affichage de l'&eacute;l&eacute;ment _START_ &agrave; _END_ sur _TOTAL_ &eacute;l&eacute;ments",
    "sInfoEmpty":      "Affichage de l'&eacute;l&eacute;ment 0 &agrave; 0 sur 0 &eacute;l&eacute;ment",
    "sInfoFiltered":   "(filtr&eacute; de _MAX_ &eacute;l&eacute;ments au total)",
    "sInfoPostFix":    "",
    "sLoadingRecords": "Chargement en cours...",
    "sZeroRecords":    "Aucun &eacute;l&eacute;ment &agrave; afficher",
    "sEmptyTable":     "Aucune donn&eacute;e disponible dans le tableau",
    "oPaginate": {
        "sFirst":      "Premier",
        "sPrevious":   "Pr&eacute;c&eacute;dent",
        "sNext":       "Suivant",
        "sLast":       "Dernier"
    },
    "oAria": {
        "sSortAscending":  ": activer pour trier la colonne par ordre croissant",
        "sSortDescending": ": activer pour trier la colonne par ordre d&eacute;croissant"
    }
};


$(document).ready(function(){

    $('.super_tableau').DataTable( {
        "paging": false,
        "info": true,
        "searching": false,
        "language": datatable_lang_fr }  );

    $('.super_tableau_simple').DataTable(  {
        "searching": false,
        "paging": false,
        "info": false,
        "columns": [ null, null,{ "type": "date-eu"},null,{ "orderable": false } ],
        "language": datatable_lang_fr } );

    $('.super_tableau_select').DataTable(

        {
            'columnDefs': [
                {
                    'targets': 0,
                    'searchable':false,
                    'orderable':false,
                    'className': 'dt-body-center',
                    'render': function (data, type, full, meta){
                        return '<input type="checkbox" />';
                    }

                }
            ],
            bAutoWidth: false,
            'order': [[ 1, 'asc' ]],
            'paging': false,
            'info': true,
            'searching': false,
            'language': datatable_lang_fr
        } );

    $('.super_tableau_select tbody').on( 'click', 'tr', function () {
        if ($(this).hasClass('selected')) {
            $(this).removeClass('selected');
            $("input[type=checkbox][checked]",this).removeAttr('checked');
        }
        else
        {
            $(this).addClass('selected');
            $("input[type=checkbox]",this).attr('checked','checked');

        }

    } );



});




function ajouteBoutonDatatable(options,id,num_col_action) {


    if (typeof options['indice_col_action'] =='undefined' )
        options['indice_col_action'] = num_col_action;

    var args = {};
    redirect='';
    if (options['redirection'])
        redirect = '&redirect='+options['redirection'];
    if (options['url']!=undefined)
        args['url']=options['url']+id;
    if (options['url_form']!=undefined)
        args['url_form']= options['url_form']+id+redirect;
    if (options['url_delete']!=undefined)
        args['url_delete']= options['url_delete']+id+redirect;

    if (typeof options['indice_col_id'] =='undefined' )
        options['indice_col_id']=0;
    return Mustache.render(window['templates']['bouton_action.html'](), args);
}



function super_datatable(data,id,colonnes,nb_ligne,options){


    selecteur = $('#'+id+' .super_tableau_data');
    colonnes[colonnes.length-1]['width']='85px';
    opt ={
        "paging": true,
        "info": true,
        "lengthChange": false,
        "pageLength": nb_ligne,
        "searching": false,
        "language": datatable_lang_fr,
        'columns':colonnes,
        'data': data,
        "autoWidth": false,
        'createdRow': function (row, data, index) {
            enjoliveCreatedRow(colonnes,options,row, data);
        },
        'drawCallback': function () {

            initialisation_modal_confirm_supprimer(selecteur);
            initialisation_modal_form(selecteur);

        }
    };
    if (options['tri']){
        tab_tri = datatable_decode_tri(colonnes,options['tri'])
        if (tab_tri.length>0)
            opt['order']=tab_tri;
     }
    table[id] = $(selecteur).DataTable(opt);
}

function datatable_decode_tri(colonnes,tri){

    tab_tri= [];
    tab_col = [];

    for(i=0;i<colonnes.length;i++){
        tab_col[colonnes[i].name+'']=i;
    }
    for(i=0;i<tri.length;i++){
        if (tab_col[tri[i][0]])
            tab_tri.push([tab_col[tri[i][0]],tri[i][1]]);
    }
    return tab_tri;
    }




function ajaxdatatable(id,url,colonnes,nb_ligne,options){
    selecteur = $('#'+id);
    selecteur_table = $('#'+id+' table.ajaxdatatable');

    $('#filtre_recherche',selecteur).keyup(function () {
        if (timer_recherche)
            clearTimeout(timer_recherche);
        timer_recherche = setTimeout(function () {
            table[id].ajax.reload();
        }, 300);

    });

    $('#filtres select',selecteur).change(function () {
            table[id].ajax.reload();
            return false;
        }
    );

    $('#filtres input[type=checkbox]',selecteur).change(function () {
            table[id].ajax.reload();
            return false;
        }
    );

    $('#filtres input[type=text]',selecteur).change(function () {
            table[id].ajax.reload();
            return false;
        }
    );

    $('#filtrer_plus',selecteur).click(function () {
        $('#filtres_secondaires',selecteur).removeClass('hide');
    });

    colonnes[colonnes.length-1]['width']='85px';
    opt =  {
            "language": datatable_lang_fr,
            "searching": false,
            "lengthChange": false,
            "pageLength": nb_ligne,
            "processing": true,
            "serverSide": true,
            "columns":colonnes,
            "autoWidth": false,
            "ajax": {
                "url": url,
                "data": function (d) {
                    d.action = "dataliste";


                    if (options['filtre_fonction']){
                        tab = eval(options['filtre_fonction']+'(options[\'filtres\'])');
                        for( key in tab ){
                            eval('d.'+key+'= tab[key]');
                        }
                    }else
                    {
                        filtres = options['filtres'];
                        for(i=0;i<filtres.length;i++){
                            selecteur_filtre = $('.filtres select[name='+filtres[i]+']');
                            if (selecteur_filtre.length==0)
                                selecteur_filtre = $('.filtres select[name='+filtres[i]+'\\[\\]]');
                            if (selecteur_filtre.length>0){
                                eval('d.'+filtres[i]+'=$(selecteur_filtre).val();');
                            } else {
                                selecteur_filtre = $('.filtres input[type=checkbox][name='+filtres[i]+']');
                                if (selecteur_filtre.length>0) {

                                    selecteur_filtre = $('.filtres input[name='+filtres[i]+']:checked');
                                    if (selecteur_filtre.length==0)
                                        selecteur_filtre = $('.filtres input[name='+filtres[i]+'\\[\\]]:checked');
                                    if (selecteur_filtre.length>0){
                                        eval('d.'+filtres[i]+'=$(selecteur_filtre).val();');
                                    }
                                }
                                else
                                {
                                    selecteur_filtre = $('.filtres input[type=text][name='+filtres[i]+']');
                                    if (filtres[i]=='search'){
                                        recherche = $('input#filtre_recherche',selecteur).val();
                                        d.search = {regex: false, value: recherche};

                                    }
                                    else{
                                    eval('d.'+filtres[i]+'=$(selecteur_filtre).val();');
                                }
                                }

                            }
                        }
                    }

                    if (options['filtres_statiques']) {
                        for( key in options['filtres_statiques'] ){
                            eval('d.'+key+' = '+options['filtres_statiques'][key]);

                        }
                    }

                },
                'complete': function () {
                    initialisation_modal_confirm_supprimer(selecteur_table);
                    initialisation_modal_form(selecteur_table);

                }
            },
            'createdRow': function (row, data, index) {
                enjoliveCreatedRow(colonnes,options,row, data);
                table[id].columns.adjust();
            }
        };

    if (options['tri']){
        tab_tri = datatable_decode_tri(colonnes,options['tri'])
        if (tab_tri.length>0)
            opt['order']=tab_tri;
    }
    table[id] = $('.ajaxdatatable',selecteur).DataTable(opt);


}
function enjoliveCreatedRow(colonnes,options,row, data){
    for (i = 0; i < colonnes.length; i++) {

        if (typeof(colonnes[i]['traitement'])!= "undefined"){

            td=$('td', row).eq(i);

            traitements = new Array();
            if( typeof colonnes[i]['traitement'] === 'string' ) {
                traitements.push(colonnes[i]['traitement']);
            }
            else{
                traitements=colonnes[i]['traitement'];
            }
            valeur0=$(td).text();
            for (j = 0; j < traitements.length; j++) {
                n=traitements[j].indexOf(":");
                if (n>-1){
                    trait = traitements[j].substring(0,n);
                    args_trait = traitements[j].substring(n+1);
                    valeur0 = eval(trait+ '(\'' + valeur0 + '\',\'' + args_trait + '\')');
                }
                else{
                    valeur0 = eval(traitements[j]+ '(\'' + valeur0.replace('\'','\\\'') + '\')');
                }
            }
            $(td).html(valeur0);
        }
    }

    if (options['action']){
        if (typeof options['indice_col_id'] =='undefined' )
            options['indice_col_id']=0;
        if (typeof options['indice_col_action'] =='undefined' )
            options['indice_col_action']=colonnes.length-1;
        btn = ajouteBoutonDatatable(options,data[options['indice_col_id']],options['indice_col_action']);
        $('td', row).eq(options['indice_col_action']).wrapInner(btn);
    }

}





function transformeIdPrestation(id){
    return getPrestation(id)['nom'];
}
function transformeIdEntite(id){
    return getEntite(id)['nom'];
}

function genererLien(id,objet){


    return '<a href="'+chemin_web+'/'+objet+'?id_'+objet+'='+id+'">'+id+'</a>'
}

function genererLienBeneficiaire(valeur){


    n0=valeur.indexOf(":");
    n1=valeur.lastIndexOf(":");
    if (n0>-1 && n1>-1){
        objet = valeur.substring(0,n0);
        id = valeur.substring(n0+1,n1);
        label = valeur.substring(n1+1);
        return '<a href="'+chemin_web+'/'+objet+'?id_'+objet+'='+id+'">'+label+'</a>'
    }

}



function transformePrestationType(id){
    return getPrestationType(id)['nom'];
}

function transformeTresor(id){
    return getTresor(id)['nom'];
}


function transformeOuiNon(valeur){
    if (valeur=='true'){
        return 'oui';
    }
    return 'non';

}


function transformeIdZonegroupe(id){
    return getZonegroupe(id)['nom'];
}