


function initialisation_composant_form(conteneur_f,identif) {
    if (identif == undefined)
        identif='';
    if (!$(conteneur_f).hasClass('form_inited')){

        $('.secondaire',conteneur_f).closest('form').prepend('<div class="pull-right option_form tortue_lievre"><input id="switch_tortue_lievre" class="tortue_lievre" type="checkbox" ></div>')

        $("#switch_tortue_lievre",conteneur_f).bootstrapSwitch(
            {
                onText : '<i class="ico ico-tortue"></i>',
                offText : '<i class="ico ico-lievre"></i>',
                onColor : 'success',
                offColor : 'warning',
                onSwitchChange: function(event, state){
                    if(state){
                        $('.secondaire',conteneur_f).closest('.form-group').slideDown();
                    } else {
                        $('.secondaire',conteneur_f).closest('.form-group').slideUp();
                    }
                }
            }

        );
        if($(conteneur_f).hasClass('lievre')){
            $('.secondaire',conteneur_f).closest('.form-group').slideUp();
        }
        else{
            $("#switch_tortue_lievre",conteneur_f).trigger('click')
        }



        $('.datepickerb',conteneur_f).wrap('<div class="input-group"></div>');
        $('.datepickerb',conteneur_f).before(' <div class="input-group-addon"><i class="fa fa-calendar"></i></div>');
        $('.datepickerb',conteneur_f).attr('data-inputmask',"'alias': 'dd/mm/yyyy'");
        $(".datepickerb",conteneur_f).inputmask("99/99/9999", {"placeholder": "jj/mm/aaaa"});
        $('.datepickerb',conteneur_f).datepicker({autoclose:true,language: 'fr' });

        $(".jq-ufs",conteneur_f).not( "[multiple]" ).each(function(i){
            $(this).after('<div id="fileuploader'+i+'" class="fileuploader" data-name-field="'+$(this).attr('id')+'">Upload </div>');
        });

        $(".jq-ufs[multiple]",conteneur_f).each(function(i){
            $(this).after('<div id="fileuploadermultiple'+i+'" class="fileuploader-multiple" data-name-field="'+$(this).attr('id')+'">Upload </div>');
        });

        $(".jq-ufs",conteneur_f).hide();

        $('.fileuploader',conteneur_f).each(function(){

            nom_champs=$(this).attr('data-name-field');
            $(this).uploadFile({
                url:chemin_root+'upload?nom_form='+nom_champs,
                multiple:false,
                dragDrop:true,
                maxFileCount:1,
                //formData: { 'nom_form': nom_champs},
                fileName:'fichiers',
                dragDropStr: "Faites glisser et déposez votre fichier ici",
                abortStr:"Abandonner",
                cancelStr:"Annuler",
                doneStr:"fait",
                multiDragErrorStr: "Plusieurs Drag &amp; Drop de fichiers ne sont pas autorisés.",
                extErrorStr:"n'est pas autorisé. Extensions autorisées:",
                sizeErrorStr:"Fichier trop volumineux",
                uploadErrorStr:"Upload n'est pas autorisé",
                uploadStr:"Téléverser",
                statusBarWidth: "75%",
                dragdropWidth: '75%',
                showPreview: true,
                previewHeight: "auto",
                previewWidth: "100%"
            });
        });

        $(".multiple",conteneur_f).uploadFile({
            url:"upload.php",
            multiple:true,
            dragDrop:true,
            fileName:"myfile",
            dragDropStr: "<span><b>Glisser et déposez vos fichiers ici</b></span>",
            abortStr:"Abandonner",
            cancelStr:"Annuler",
            doneStr:"fait",
            multiDragErrorStr: "Plusieurs Drag &amp; Drop de fichiers ne sont pas autorisés.",
            extErrorStr:"n'est pas autorisé. Extensions autorisées:",
            sizeErrorStr:"Fichier trop volumineux",
            uploadErrorStr:"Upload n'est pas autorisé",
            uploadStr:"Téléverser"

        });

        $('select.select2',conteneur_f).select2({  theme: "bootstrap",allowClear: true,minimumResultsForSearch: 20,dropdownAutoWidth : true  });
        $('select.select2_simple',conteneur_f).select2({theme: "bootstrap",allowClear: true,minimumResultsForSearch: Infinity,dropdownAutoWidth : true });

        $('input.bs_switch',conteneur_f).each(function(){
            $(this).bootstrapSwitch({
                onText : 'oui',
                offText : 'non',
                size :'small'
            });
        });

        $('button[type=submit]',conteneur_f).click(function(){
            if($(this).not('.en_attente')){
                $(this).prepend(' <i class="fa fa-spinner fa-pulse fa-fw"></i>');
                $(this).css('en_attente');
               // $(this).attr("disabled","disabled");
                return true;
            }

        });

        $('.password-field',conteneur_f).focus(function(){
            $(this).removeAttr('readonly');
        });

        $(conteneur_f).addClass('form_inited')
    }
}

function initialisation_composant_forms(conteneur){
    if (conteneur === undefined)
        conteneur='body';
    conteneur=$(conteneur);
    $('form',conteneur).each(function(){
        initialisation_composant_form(this);
    });
    $('.desactiveSoumission',conteneur).keypress(refuserToucheEntree);
}


