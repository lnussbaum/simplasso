function initialisation_composant(conteneur) {

    if (conteneur == undefined)
        conteneur = 'body';
    conteneur = $(conteneur);


    $('.force-tooltip',conteneur).tooltip({container: 'body','html':true});
    $('[data-toggle="tooltip"]',conteneur).tooltip({container: 'body','html':true});
    $('.btn_ajax_switch',conteneur).each(function(){
        $(this).attr('data-href',$(this).attr('href'));
        $(this).removeAttr('href');
        $(this).click(function(){
            el=$(this);
            $.ajax({'url': $(el).attr('data-href'), dataType:  'json','success':function(r){
                if (r['ok']){
                     if ($(el).hasClass('btn-primary')){
                        $(el).removeClass('btn-primary');
                        $(el).addClass('btn-default');
                    }else{
                        $(el).removeClass('btn-default');
                        $(el).addClass('btn-primary');
                    }
                    $(el).attr('data-href',r['url']);
                }
            }})

        })

    });

    $( ".sortable" ).each(function(){
        Sortable.create(this,{
            'handle':'.drag-handle',
            'onUpdate':function(evt){
                href = $(evt.target).attr('data-href')+'&bloc_id='+$(evt.item).attr('data-id')+'&index='+$(evt.item).index();
                $.getJSON( href, function( data ) {
                    if(data.ok) {

                    }

                })

            }});
    });




}

function initialisation_npai(conteneur){


    if (conteneur == undefined)
        conteneur='body';
    $('.npai a.btn-warning').parent().parent().addClass('disable');
    $('a.npai',conteneur).click(function(){
        href = $(this).attr('data-href');
        lien = $(this);
        $.getJSON( href, function( data ) {
            if(data.ok){
                if(data.ajout) {
                    $(lien).addClass('btn-warning');
                    $(lien).parent().parent().addClass('disable');
                    $(lien).removeClass('btn-default');
                }
                else {
                    $(lien).removeClass('btn-warning');
                    $(lien).parent().parent().removeClass('disable');
                    $(lien).addClass('btn-default');
                }
            }
        });
        return false;
    });
}



function initialisation_carte_adherent(conteneur){
    if (conteneur == undefined)
        conteneur='body';
    $('a.carte_adherent',conteneur).click(function(){
        href = $(this).attr('data-href');
     //   valeur = parseInt($(this).attr('data-value'));
        lien = $(this);
        $.getJSON( href, function( data ) {
            box =  $(lien).parents('.info-box-content');
            if(data.ok){
                    $('.carte_adherent span',box).removeClass('label-carte'+data.valeur_depart);
                    $('.carte_adherent span',box).addClass('label-carte'+data.valeur);
                    $('.carte_adherent_max',box).attr('title',getCarteAdherentAction(data.valeur));
                    $('.carte_adherent_mini',box).attr('title',getCarteAdherentEtat(data.valeur)+'<br/> Clic :'+getCarteAdherentAction(data.valeur_futur))
                        .tooltip('fixTitle');
                    if($(lien).hasClass('carte_adherent_mini'))
                        $('.carte_adherent_mini',box).tooltip('show')
                    $('.carte_adherent_maxi span',box).text(getCarteAdherentEtat(data.valeur));

            }
        });
        return false;
    });
    initialisation_depliable()
}


function redirection(url){
    window.location.href = url;
}


function refuserToucheEntree(event)
{

    // Compatibilité IE / Firefox
    if(!event && window.event) {
        event = window.event;
    }
    // IE
    if(event.keyCode == 13) {
        event.returnValue = false;
        event.cancelBubble = true;
    }
    // DOM
    if(event.which == 13) {
        event.preventDefault();
        event.stopPropagation();
    }
}


function ul_select_redirect(){
    $('ul.select_redirect').after('<select><option></option></select>');
    select=$('ul.select_redirect').next();
    $('ul.select_redirect li a').each(function(){
        $(select).append('<option value="'+$(this).attr("href")+'">'+ $(this).text()+'</option>');
    });
    $('ul.select_redirect').remove();
    $(select).change( function() {
        redirection(this.value);
    });

}

function initialisation_depliable(){
    $( ".depliable .gachette").prepend('<i class="fa fa-arrow-circle-right" /> ')

    $( ".depliable .gachette" ).click(function(){
        bloc_depliable = $(this).parent();
        if ($(bloc_depliable).hasClass('deplie')){
            $(this).children("i").removeClass('fa-arrow-circle-down').addClass('fa-arrow-circle-right');
            $(bloc_depliable).removeClass('deplie');
            $(bloc_depliable).children('.depliant').slideUp();
        }
        else
        {
            $(this).children("i").removeClass('fa-arrow-circle-right').addClass('fa-arrow-circle-down');
            $(bloc_depliable).addClass('deplie');
            $(bloc_depliable).children('.depliant').slideDown();
        }

    });

}


function initialisation_ajax_remplace(conteneur){

    if (conteneur == undefined)
        conteneur='body';

    $( ".ajax_remplace[data-cible]",conteneur).click(function(){
        url = $(this).attr('href');;
        cible = $(this).attr('data-cible');
        console.log(cible);
        $.get( url, function( data ) {
            $( cible ).html( data );

        });
        return false;
    });

}



$(document).ready(function(){


    if ( ($(window).height() + 50) < $(document).height() ) {
        $('#top-link-block').removeClass('hidden').affix({
            offset: {top:50}
        });
    }




    $('.filtre_select').each(function(){
        var id=$(this).attr('id');
        $(this).after('<select id="select_'+id+'" class="filtre" ></select>');

        $("a",this).each(function() {
            var el = $(this);
            sel='';
            if(el.attr('selected')=='selected')
                sel=' selected="selected"';
            $('#select_'+id).append('<option value="'+el.attr('href')+'"'+sel+'>'+el.text()+'</option>');
         });

        $(this).remove();

        $('#select_'+id).change(function() {
            window.location = $(this).find("option:selected").val();
        });


    });

    $('.filtre_checkbox').each(function(){
        var id=$(this).attr('id');
        $(this).after('<fieldset id="fieldset_'+id+'" class="filtre" ></fieldset>');

        $("a",this).each(function(i) {
            var el = $(this);
            sel='';
            if(el.hasClass('selected'))
                sel=' checked="checked"';
                id_check='filtre_'+id+'_'+i;
            $('#fieldset_'+id).append('<label><input type="checkbox" id="'+id_check+'" value="'+el.attr('href')+'"'+sel+' />'+el.text()+'</label>');
        });

        $(this).remove();

        $('#fieldset_'+id+ ' :checkbox').change(function() {
            window.location = $(this).val();
        });


    });




    initialisation_champs_autocomplete_recherche($('.main-header'))

    initialisation_composant();
    initialisation_composant_forms();
    initialisation_modal();
    initialisation_modal_form();
    initialisation_modal_confirm();
    initialisation_modal_confirm_supprimer();
    initialiseXeditable();

    });




