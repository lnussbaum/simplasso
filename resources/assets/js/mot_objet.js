

function initialisation_mot_objet(id_conteneur){

    conteneur = $(id_conteneur);

    if ($('select optgroup',conteneur).length > 0){

        $('select optgroup',conteneur).each(function(i){
            $('select',conteneur).parent().append('<div class="groupe_mot group'+(i+1)+'"><strong>'+$(this).attr('label')+'</strong><ul class="list-inline"></ul></div>')
            $('option',this).each(function(j){
                selected='';
                if ($(this).attr('selected'))
                    selected = ' class="selected"';
                $('div.group'+(i+1)+' ul',conteneur).append("<li id=\"mot"+$(this).val()+"\""+selected+" >"+$(this).text()+"</li>")
            })

        })
    }
    else
    {


        $('select',conteneur).parent().append('<ul class="list-inline"></ul>')
        $('option',this).each(function(j){
            selected=''
            if ($(this).attr('selected'))
                selected = ' class="selected"'
            $(' ul',conteneur).append("<li id=\"mot"+$(this).val()+"\""+selected+" >"+$(this).text()+"</li>")
        })
    }

    $('div.groupe_mot ul li',conteneur).click(function(){
        valeur = $(this).attr('id')
        valeur = valeur.substring(3)
        if(!$(this).hasClass('selected')){
            $('select option[value='+valeur+']',conteneur).attr('selected','selected')
            $(this).addClass('selected')
        }
        else{
            $('select option[value='+valeur+']',conteneur).removeAttr('selected')
            $(this).removeClass('selected')
        }
    })


    $('select',conteneur).hide()
    $('label',conteneur).hide()


}

