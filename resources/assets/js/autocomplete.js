function autocomplete_identify (obj) {
    return obj.id;
}

function autocomplete_reponse1(data){
    return '<p><strong>' + data.nom + '</strong> - ' + data.id  +' '+ data.ville + '</p>';

}

function autocomplete_reponse2(data){
    return '<p><strong>' + data.nom + '</strong> - ' + data.id + '</p>';

}

function autocomplete_reponse_commune(data){
    return '<p><strong>' + data.nom + '</strong> ' + data.nom_suite + '</p>';

}

function autocomplete_reponse_cp(data){
    nom_suite='';
    if (data.nom_suite)
        nom_suite=data.nom_suite;
    return '<p>'+data.code+' <strong>' + data.nom + '</strong> ' + nom_suite + '</p>';

}

function autocomplete_reponse_individu(data){
    return '<p><strong>' + data.nom + '</strong> - ' + data.id + '<br>' +
        '<small>' +data.email+' - '+ data.adresse + ' '+ data.code_postal +' '+ data.ville +'</small>'+
        '</p>';

}

function autocomplete_reponse_membre(data){
    return '<p><strong>' + data.nom + '</strong> - ' + data.id + '<br>' +
        '</p>';
}

function autocomplete_reponse_recherche(data){
    return '<a href="'+data.url+'"><p>'+data.objet+' - '+data.id+' <strong>' + data.nom + '</strong> ' + data.description + '</p></a>';
}

function autocomplete_reponse_region(data){
    return '<p><strong>' + data.nom + '</strong></p>';
}

var charMap = {
    'a': /[àáâã]/gi,
    'c': /[ç]/gi,
    'e': /[èéêë]/gi,
    'i': /[ïí]/gi,
    'o': /[ôó]/gi,
    'oe': /[œ]/gi,
    'u': /[üú]/gi
};

var normalize = function (str) {
    $.each(charMap, function (normalized, regex) {
        str = str.replace(regex, normalized);
    });

    return str;
};

var queryTokenizer = function (q) {
    var normalized = normalize(q);
    return Bloodhound.tokenizers.whitespace(normalized);
};

function initialisation_champs_autocomplete(cible,conteneur_form,nom_variable,champs_id,url,f_affichage,msg_aucun_resultat,fident){

    if (!fident)
        fident=autocomplete_identify;
    $(cible,conteneur_form).each(function()
    {
        Bloodhound.tokenizers.whitespace;
        $(this).attr('data-provide', 'typeahead');
        $(this).typeahead(null, {
            display: nom_variable,
            hint: true,
            highlight: true,
            minLength: 0,
            limit: 15,
            ttl: 0,
            ajax: {
                cache: false
            },
            source: new Bloodhound({
                'datumTokenizer': Bloodhound.tokenizers.obj.whitespace(nom_variable),
                'queryTokenizer': Bloodhound.tokenizers.whitespace,
                'identify': fident,
                'cache': false,
                remote: {
                    url: url,
                    wildcard: '%QUERY'
                },
            }),
            templates: {
                empty: '<div class="empty-message">'+ msg_aucun_resultat+'</div>',
                suggestion: f_affichage
            }
        });
        if(champs_id){
            $(this).bind('typeahead:select', function (ev, suggestion) {
                $('#'+champs_id ).val(suggestion.id);
            });
        }
    });
}

function initialisation_champs_autocomplete_geo(conteneur_form,champs_id) {

    initialisation_champs_autocomplete_select2('.select2_autocomplete_commune', conteneur_form, 'nom', champs_id, 'commune_liste?action=rechercher', autocomplete_reponse_commune, 'aucune commune trouvée');
    initialisation_champs_autocomplete_select2('.select2_autocomplete_arrondissement', conteneur_form, 'nom', champs_id, 'arrondissement_liste?action=rechercher', autocomplete_reponse_commune, 'aucun arrondissement trouvée');
    initialisation_champs_autocomplete_select2('.select2_autocomplete_departement', conteneur_form, 'nom', champs_id, 'departement_liste?action=rechercher', autocomplete_reponse_region, 'aucun département trouvée');
    initialisation_champs_autocomplete_select2('.select2_autocomplete_region', conteneur_form, 'nom', champs_id, 'region_liste?action=rechercher', autocomplete_reponse_region, 'aucune région trouvée');

}

function initialisation_champs_autocomplete_objet(conteneur_form,champs_id,objet) {

    url = objet+'_liste?action=rechercher&search[value]=%QUERY';
    cible = '.autocomplete_'+objet;
    reponse = autocomplete_reponse1;
    if(objet=='individu')
        reponse = autocomplete_reponse_individu;
    if(objet=='membre')
        reponse = autocomplete_reponse_membre;
    console.log(objet);
    initialisation_champs_autocomplete(cible,conteneur_form,'nom',champs_id,url, reponse,'Aucun '+objet+' n\'a été trouvé');
}

function initialisation_champs_autocomplete_recherche(conteneur_f) {


    $('#navbar-search-input',conteneur_f).each(function () {

        initialisation_champs_autocomplete(this, conteneur_f, 'nom', '', 'recherche?action=json&search[value]=%QUERY', autocomplete_reponse_recherche, 'Aucun élément ne correspond à la recherche');
        $(this).bind('typeahead:select', function (ev, suggestion) {
            window.location.href = suggestion.url;
        });

    });
}

function initialisation_champs_autocomplete_select2(cible,conteneur_form,nom_variable,champs_id,url,f_affichage,msg_aucun_resultat){
    $(cible,conteneur_form).each(function() {


        $(this).select2({
            data:[],
            ajax: {
                url: url,
                dataType: 'json',
                delay: 250,
                data: function (params) {
                    return {
                        'search[value]': params.term, // search term
                        page: params.page
                    };
                },
                processResults: function (data, params) {
                    params.page = params.page || 1;
                    return {
                        results: data,
                        pagination: {
                            more: (params.page * 30) < data.length
                        }
                    };
                },
                cache: true
            },
            escapeMarkup: function (markup) {
                return markup;
            }, // let our custom formatter work
            minimumInputLength: 1,
            minimumResultsForSearch: 15,
            templateResult: function (data) {
                return data.text ;
            },
            templateSelection: function (data, container) {
                return data.text;
            }
        });
    })

}

function selection_commune(composant,conteneur_f,identif,champs){

    $(composant,conteneur_f).attr('data-provide', 'typeahead');

    initialisation_champs_autocomplete(
        composant,
        conteneur_f,
        champs,
        '',
        'codespostaux_liste?action=rechercher&search[value]=%QUERY',
        autocomplete_reponse_cp,
        'Ce code postal n\'existe pas dans la base',
        function(obj) { return obj.id; }
    );



}


function initialisation_champs_autocomplete_ville_cp(conteneur_f,identif) {

    $('.autocomplete_cp', conteneur_f).each(function () {
        selection_commune(this,conteneur_f,identif,'code');

        $(this).bind('typeahead:select', function (ev, suggestion) {
            $('#' + identif + 'codepostal',conteneur_f).typeahead('val',suggestion.code);
            $('#' + identif + 'ville',conteneur_f).val(suggestion.nom);
            champ_suivant = $('#' + identif + 'ville').closest('div.form-group').nextAll(':visible').eq(0);
            $('input,select',champ_suivant).focus();
        });

    });

    $('.autocomplete_ville', conteneur_f).each(function () {
        selection_commune(this,conteneur_f,identif,'nom');

        $(this).bind('typeahead:select', function (ev, suggestion) {
            console.log(suggestion.nom);
            $('#' + identif + 'codepostal',conteneur_f).val(suggestion.code);
            $('#' + identif + 'ville',conteneur_f).typeahead('val',suggestion.nom);
            champ_suivant = $('#' + identif + 'ville').closest('div.form-group').nextAll(':visible').eq(0);
            $('input,select',champ_suivant).focus();
        });
    });



}



