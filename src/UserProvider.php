<?php
use Symfony\Component\Security\Core\User\UserProviderInterface;
use Symfony\Component\Security\Core\User\UserInterface;
use Symfony\Component\Security\Core\User\UserSimplasso;
use Symfony\Component\Security\Core\Exception\UnsupportedUserException;
use Symfony\Component\Security\Core\Exception\UsernameNotFoundException;
use Symfony\Component\Security\Core\Exception\AuthenticationException;
use Doctrine\DBAL\Connection;



class UserProvider implements UserProviderInterface
{
    private $conn;

    public function __construct(Connection $conn)
    {
        $this->conn = $conn;
    }

    public function loadUserByUsername($login)
    {
        global $app;

        $roles=array();

        $user = IndividuQuery::create()->filterByLogin(strtolower($login))->findOne();

        if (!$user) {
            throw new UsernameNotFoundException(sprintf('Username "%s" does not exist.', $login));
        }
        $id_user=$user->getIdIndividu();



        $tabEntites=array();
        $restriction=array();
        $profils=getProfil();
        $niveaux=getNiveau();
        $profil='';
        $profiltemp='';
        $tab_autorisations= AutorisationQuery::create()->filterByIdIndividu($id_user)->find();
        $niveaumax=0;

        foreach($tab_autorisations as $autorisation) {
            $roles[] = substr($autorisation->getProfil(), 0, 1).'-'.$autorisation->getIdEntite();
            $niveau=$autorisation->getNiveau();
            $niveaumax=max($niveau,$niveaumax);

            for ($i = 1; ($i <= $niveau && $i <= 6); $i++) {
                $pf=substr($autorisation->getProfil(), 0, 1);
                $roles[] = $pf. $i.'-'.$autorisation->getIdEntite();
                $profiltemp=' -'.$profils[$pf].' '.$niveaux[$i].'-';//todo entite non précisé
            }
            if ($autorisation->getIdRestriction())
                $restriction[$autorisation->getIdEntite()]=$autorisation->getIdRestriction();
            $e=$autorisation->getIdEntite();
            $tabEntites[]=$e;
            //todo bien vérifier la suppression des entités dans autorisation en meme temps qu'entite
            $profil.=$profiltemp;

        }
        $tabEntites=array_unique($tabEntites);
        $tva_oui=false;
        $tab_entites = EntiteQuery::create()->findPks($tabEntites);
        foreach($tab_entites as $entite) {
            if ($entite->getAssujettitva()) {
                $tva_oui=true;
                break;
            }
        }


        sort($tabEntites);
        // todo enleve l'entité en cours dans les entites autorisées comprendre autres entites autorisées mis en commentaire le 17 mai 2016 la variable tabidentite sert aux filtres sur tous les objets liés aux entités utilisé par l'opérateur
        init_sac();
        if (!conf('module.restriction') and
            (conf('restriction_mode.nom')=='aucun' or conf('restriction_mode.nom')=='')) {
                $restriction['concerne'] = false;
        }elseif(conf('restriction_mode.nom')=='mot_operateur') {
             if (is_array(conf('restriction_mode.non_concerne')) && in_array($id_user, conf('restriction_mode.non_concerne'))) {
                 $restriction['concerne'] = false;
             }else {$restiction['id_mot']=$id_user;
                 $restriction['concerne'] = true;}//todo a revoir pour ne pas etre idem a un mot cle autre
        }elseif(conf('restriction_mode.nom')=='individu'){
           $restriction['concerne'] = true;
            $restriction['mode']='id_entite et id_restriction';
        }
        $variable = array(
            'entite'=> $tabEntites,
//            'tresor'=>$tresor_aut,
            'entite_multi'=>(count($tabEntites)>1),
            'tresor_multi'=> (count($tabEntites)>1),
            'tva_oui'=>$tva_oui,
            'restriction'=>$restriction,
            'operateur'=>array('nom'=>$user->getNom(),
                'pseudo'=>$user->getLogin(),
                'niveau_max'=>$niveaumax,
                'id'=>$id_user,
                'profil'=>$profil)
        );

        $app['session']->set('preference',\PreferenceQuery::getPreferenceUtilisateur($id_user));
        $pref_paiement=getUserPreference($app['session']->get('preference'), 'paiement');
        //todo non testé en multisociété
        foreach ($tabEntites as $id_entite) {//prend en compte les entites autorisées
            //prendre en compte l'encaissement par l'operateur sur son mode de reglement
            if (isset($pref_paiement['tresor_tous_un']) && $pref_paiement['tresor_tous_un']){
                if ($id_entite==$pref_paiement['entite'] ){
                    foreach (table_simplifier(getTresorByIdEntite($id_entite)) as $id_tresor=>$nom) {
                        //le moyen de paiement sélectionné pour cet opérateur
                        if ($id_tresor==$pref_paiement['tresor'])
                            $variable['tresor'][$id_entite] = array($id_tresor=>$nom);
                    }
                }
            } else
                $variable['tresor'][$id_entite] = table_simplifier(getTresorByIdEntite($id_entite));
        };
        $app['session']->set('variable',$variable);
        $est_membre = $user->estMembre();
        $app['session']->set('est_membre',$est_membre);


        if (empty($roles)) {

            if($est_membre)
                $roles= ['V'];
            else{
                throw new AuthenticationException(sprintf('Vous n\'avez pas les droits suffisants pour accèder au logiciel.', $login));

            }
        }

        return new UserSimplasso($user->getLogin(), $user->getPass(),$roles,
            true, true, true, true,$user->getAleaActuel());
    }

    public function refreshUser(UserInterface $user)
    {
     // global $app;
        if (!$user instanceof UserSimplasso) {
            throw new UnsupportedUserException(sprintf('Instances of "%s" are not supported.', get_class($user)));
        }
/*
        if ($app['session']->get('user')){

            $user = $app['session']->get('user');
            $roles = $app['session']->get('autorisation');
            return new UserSimplasso($user->getLogin(), $user->getPass(),$roles,
                 true, true, true, true,$user->getAleaActuel());

        }
*/

        return $this->loadUserByUsername($user->getUsername());
    }



    public function supportsClass($class)
    {
        return $class === 'Symfony\Component\Security\Core\User\UserSimplasso';
    }
}
