<?php


function documents(){
    global $app;
    return $app['twig']->render(fichier_twig(),array());
}

function action_documents_assemblee_generale(){

    global $app;
    include_once($app['basepath'] . '/src/inc/fonctions_document.php');


    generer_document_ag(['sorti'=>'non','ardent'=>'ardent']);
}


function action_documents_bas_de_page(){

    global $app;
     $loader = new Twig_Loader_Filesystem( $app['resources.path'] . '/documents');
    $twig_page = new Twig_Environment($loader,['autoescape'=>false]);
   return $twig_page->render( 'bas_de_page.html.twig' );;
}