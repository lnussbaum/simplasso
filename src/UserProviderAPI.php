<?php
use Symfony\Component\Security\Core\User\UserProviderInterface;
use Symfony\Component\Security\Core\User\UserInterface;
use Symfony\Component\Security\Core\User\UserSimplasso;
use Symfony\Component\Security\Core\Exception\UnsupportedUserException;
use Symfony\Component\Security\Core\Exception\UsernameNotFoundException;
use Symfony\Component\Security\Core\Exception\AuthenticationException;
use Doctrine\DBAL\Connection;



class UserProviderAPI implements UserProviderInterface
{
    private $conn;

    public function __construct(Connection $conn)
    {
        $this->conn = $conn;
    }

    public function getUsernameForApiKey($apiKey)
    {
      global $app;
        $username = $app['security.jwt.encoder']->decode($apiKey);
        return $username;
    }


    public function loadUserByUsername($login)
    {
        global $app;

        $roles=array();

        $user = IndividuQuery::create()->filterByLogin(strtolower($login))->findOne();

        if (!$user) {
            throw new UsernameNotFoundException(sprintf('Username "%s" does not exist.', $login));
        }




        $est_membre = $user->estMembre();
        if($est_membre)
            $roles= ['ROLE_API'];
        else{
            throw new AuthenticationException(sprintf('Vous n\'avez pas les droits suffisants pour accèder au logiciel.', $login));

        }

        return new UserSimplasso($user->getLogin(), $user->getPass(),$roles,
            true, true, true, true,$user->getAleaActuel());
    }

    public function refreshUser(UserInterface $user)
    {
        // global $app;
        if (!$user instanceof UserSimplasso) {
            throw new UnsupportedUserException(sprintf('Instances of "%s" are not supported.', get_class($user)));
        }
        /*
                if ($app['session']->get('user')){

                    $user = $app['session']->get('user');
                    $roles = $app['session']->get('autorisation');
                    return new UserSimplasso($user->getLogin(), $user->getPass(),$roles,
                         true, true, true, true,$user->getAleaActuel());

                }
        */

        return $this->loadUserByUsername($user->getUsername());
    }



    public function supportsClass($class)
    {
        return $class === 'Symfony\Component\Security\Core\User\UserSimplasso';
    }
}
