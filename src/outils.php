<?php

function outils()
{
    global $app;
    $id_motgroupe_npai = table_filtrer_valeur_premiere(tab('motgroupe'),'nomcourt','npai')['id_motgroupe'];
    $args_twig['tab_mot_npai'] = array_keys(table_filtrer_valeur(tab('mot'),'id_motgroupe',$id_motgroupe_npai));
    return $app['twig']->render('outils.html.twig', $args_twig);
}
