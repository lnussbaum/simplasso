<?php

function rss(){
    global $app;

    $fichier_tmp=$app['tmp.path'].'/rss.xml';
    if(!(file_exists($fichier_tmp) && filemtime($fichier_tmp)-(time()+2*60*60)>0)){

        $content = file_get_contents($app['simplasso.rss']);
        file_put_contents($fichier_tmp,$content);
    }
    return $app->sendFile($fichier_tmp);
}