<?php

function tache_zone()
{
    global $app;
    include($app['basepath'] . '/src/geo/zone.php');
    $tab_zone = ZoneQuery::create()->find()->toKeyValue('PrimaryKey','PrimaryKey');
    foreach( $tab_zone as $id_zone){
        zone_lier($id_zone,'individu');
    }

    return true;
}
