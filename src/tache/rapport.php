<?php

function tache_rapport()
{
    global $app;


   // $content = array2htmltable(simplasso_stat_nb_adherent("2000"));
    $html2pdf = new HTML2PDF('P','A4','fr');
    //$html2pdf->WriteHTML($content);
    $html2pdf->WriteHTML("<p>Ceci est un <strong>test</strong></p>");
    $nom_fichier=$app['output.path'].'/rapport'.uniqid().'.pdf';
    $html2pdf->Output($nom_fichier, 'F');
    $args_twig=[
        'sujet'=>'Rapport',
        'texte'=>'Voir pièce jointe'
    ];
 
    $options=['pieces_jointes'=>['rapport.pdf'=>$nom_fichier]];
    courriel_twig($app['email_from'],'basic',$args_twig,$options);
    unlink($nom_fichier);
    return true;
}
