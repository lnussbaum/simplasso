<?php
use Propel\Runtime\ActiveQuery\Criteria;

function tache_import($args)
{
    global $app;
    $traitement_fini = false;
    $operation = $args['operation'];
    $import = ImportQuery::create()->filterByAvancement([3, 6],
        Criteria::NOT_IN)->filterByOperation($operation)->orderByCreatedAt()->findOne();
    $message_attente = '';

    if ($import) {
        switch ($import->getAvancement()) {
            case 0: // Lire l'entete du tableau à importer
                $message = ' debut a : ' . date('Y-m-d H:i:s');
                $pourcentage = 10;
                $ged = GedLienQuery::create()->filterByIdObjet($import->getPrimaryKey())->filterByObjet('import')->findOne()->getGed();
                $tmp_rep = $app['tmp.path'] . '/import/';
                $tmp_file = $tmp_rep . $ged->saveFile($tmp_rep);
                $debut = 0;
                $fin = 1;
                $tab_ligne_entete = fichier_csv_lecture($tmp_file, $debut, $fin, ';', true);
                importlignes_creation($import->getPrimaryKey(), 1, 'E', $tab_ligne_entete[0]);
                $nb_ligne = fichier_nb_ligne($tmp_file);
                unlink($tmp_file);
                $import->setLigneEnCours(1)->setNbLigne($nb_ligne)->setAvancement(1)->save();
                break;
            case 1 : // Créer toute les ligne dans importlignes

                $pas = 100;
                $debut = ($import->getLigneEnCours());
                $fin = min($debut + $pas, $import->getNbLigne());
                $ged = GedLienQuery::create()->filterByIdObjet($import->getPrimaryKey())->filterByObjet('import')->findOne()->getGed();
                $tmp_rep = $app['tmp.path'] . '/import/';
                $tmp_file = $tmp_rep . $ged->saveFile($tmp_rep);
                $tab_ligne = fichier_csv_lecture($tmp_file, $debut, $fin, ';', true);
                foreach ($tab_ligne as $i => $ligne) {
                    importlignes_creation($import->getPrimaryKey(), $debut + $i+1, 'D', $ligne);
                }
                $import->setLigneEnCours($fin)->save();
                if ($fin + 1 == $import->getNbLigne()) {
                    $import->setLigneEnCours(0)->setAvancement(2)->save();
                }
                break;
            case 2 : // Traitement sur chaque ligne
                $pas = 100;
                $debut = intval($import->getLigneEnCours());
                $fin = min($debut + $pas, $import->getNbLigne());
                $ligne_entete = ImportligneQuery::create()
                    ->filterByStatut('E')
                    ->filterByIdImport($import->getPrimaryKey())
                    ->findOne();
                $tab_col_entete = $ligne_entete->getVariables();
                $tab_lignes = ImportligneQuery::create()
                    ->filterByStatut('D')
                    ->filterByIdImport($import->getPrimaryKey())
                    ->filterByLigne($debut, Criteria::GREATER_THAN)
                    ->filterByLigne($fin, Criteria::LESS_EQUAL)
                    ->find();
                $traitement_ligne = 'import_traitement_ligne_' . $operation;
                foreach ($tab_lignes as $ligne) {
                    $ligne->setPropositions($traitement_ligne($ligne->getVariables(), $tab_col_entete))->save();
                }

                $import->setLigneEnCours($fin)->save();
                if ($fin  == $import->getNbLigne()) {
                    $import->setAvancement(3)->save();
                    $traitement_fini=true;
                }

                break;
            case 3 :
                $traitement_fini=true;
            break;
            case 4 :

            break;
        }
    }
    return $traitement_fini;
}


function importlignes_creation($id_import, $num_ligne, $statut, $tab, $observation = '')
{
    $objet_data = ImportligneQuery::create()->filterByIdImport($id_import)->filterByLigne($num_ligne)->findOne();
    if (!$objet_data) {
        $objet_data = new Importligne();
    }
    $objet_data->setIdImport($id_import);
    $objet_data->setligne($num_ligne);
    $objet_data->setStatut($statut);
    $objet_data->setVariables($tab);
    $objet_data->setObservation($observation);
    $objet_data->save();
}


function import_traitement_ligne_helloasso($ligne, $entete)
{
    $prop=[];
    $ligne = array_combine($entete, $ligne);
    $tab_correspondance_tsr = [
        'Adhésion'=>'cotisation',
        'Don unique'=>'don',
        'Option'=>'cotisation'
    ];

    $info_membre =[
        'nom_famille' => $ligne['Nom'],
        'prenom' => $ligne['Prénom'],
        'email' => $ligne['Email']
    ];
    foreach($info_membre as $k=>$val){
        if (empty($val)){
            unset($info_membre[$k]);
        }
    }

    list($tab_id_membre, $tab_id_individu) = membre_recherche($info_membre);
    $type_sr = $tab_correspondance_tsr[$ligne['Type']];

    $date_paiement =DateTime::createFromFormat('d/m/Y H:i',$ligne['Date']);
    $montant = intval($ligne['Montant']);

    if ( $type_sr == 'cotisation' )
    {
        $tab_cotisation = getPrestationDeType('cotisation');
        foreach($tab_cotisation as $cotisation){
            if ($montant==donne_prix_date($cotisation['prix'],$date_paiement )){
                $prop['id_prestation']=$cotisation['id_prestation'];
            }
        }
        $prop['id_membre'] = $tab_id_membre;
    }
    else
    {
        $tab_don = getPrestationDeType('don');
        $prestation_don = array_shift($tab_don);
        $prop['id_prestation']=$prestation_don['id_prestation'];
        $prop['id_individu'] = $tab_id_individu;
    }

    return $prop;

}


function membre_recherche($infos)
{

    $tab_id_individu = array();
    $tab_id_membre = array();

    $nom= '';
    if (isset($info['nom_famille'])) {
        $nom = $info['nom'];
    }
    if (isset($info['prenom'])) {
        $nom = trim($nom . ' ' . $info['prenom']);
    }


    if (isset($info['idenfiant_interne'])) {
        $tab_id_individu=[$info['idenfiant_interne']];
        $tab_id_membre = MembreQuery::create()->filterByIdentifiantInterne($info['idenfiant_interne'])->find()->toKeyValue('PrimaryKey','PrimaryKey');
    }
    if (empty($tab_id_membre) && strlen($nom) >= 6) {
        $tab_id_membre = MembreQuery::create()->filterByNom($nom)->find();
        foreach($tab_id_membre as &$membre){
            $tab_id_individu[] = $membre->getIdentifiantInterne();
            $tab_id_membre []= $membre->getPrimaryKey();
        }
    }

    if (empty($tab_id_membre)) {

        $tab_id_individu = individu_recherche($infos);
        $tab_id_membre = MembreQuery::create()->filterByIdIndividuTitulaire($tab_id_individu)->find()->toKeyValue('PrimaryKey','PrimaryKey');
    }


    return array(array_values($tab_id_membre), $tab_id_individu);
}


function individu_recherche($info)
{

    $tab_id_individu = [];
    if (isset($info['id_individu'])) {
        $tab_id_individu = [$info['id_individu']];
    }
    if (empty($tab_id_individu) && isset($info['id_individu_titulaire'])) {
        $tab_id_individu = [$info['id_individu_titulaire']];
    }
    if (empty($tab_id_individu) && isset($info['email'])) {
        print_r($info['email']);
        $tab_id_individu = IndividuQuery::create()->filterByEmail($info['email'])->find()->toKeyValue('PrimaryKey','PrimaryKey');

    }

    if (empty($tab_id_individu) && isset($info['nom_famille']) && isset($info['prenom'])) {
        $tab_id_individu = IndividuQuery::create()->filterByNomFamille($info['nom_famille'])->filterByPrenom($info['prenom'])->find()->toKeyValue('PrimaryKey','PrimaryKey');
    }
    return array_values($tab_id_individu);
}