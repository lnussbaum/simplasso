<?php

use Symfony\Component\Form\Extension\Core\Type\FormType;
use Symfony\Component\Form\Extension\Core\Type\DateType;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Propel\Runtime\Map\TableMap;
use Symfony\Component\Validator\Constraints as Assert;
use Symfony\Component\Form\FormError as FormError;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\Extension\Core\Type\MoneyType;
use Symfony\Component\Form\Extension\Core\Type\TextareaType;


function compte_form()
{
    global $app;
    $request = $app['request'];
    $args_rep=[];
    $modification = false;
    if (sac('id')) {
        $objet_data = charger_objet();
        $modification = true;
        $data = $objet_data->toArray(TableMap::TYPE_FIELDNAME);
        $data['date_anterieure'] = $objet_data->getDateAnterieure();
    } else {

        $data = sac('idinit');
    }

    $builder = $app['form.factory']->createNamedBuilder('compte',FormType::class,$data);
    formSetAction($builder,$modification);
    $builder->setRequired(false);



    $form = $builder
        ->add('ncompte',TextType::class, array('attr' => array('placeholder' => 'exemple 401AAAA')))
        ->add('nomcourt',TextType::class, array('label' => $app->trans('nomcourt'), 'attr' => array('placeholder' => '')))
        ->add('nom',TextType::class, array('label' => 'nom', 'attr' => array('placeholder' => '')))
        ->add('id_poste',ChoiceType::class, array('label' => $app->trans('poste'), 'choices' => array_flip(table_simplifier(tab('poste'))), 'attr' => array()))
        ->add('type_op',ChoiceType::class, array('label' => 'type opération', 'choices' => array_flip(getListeTypeOp()), 'attr' => array('placeholder' => '')))
        ->add('solde_anterieur',MoneyType::class, array('label' => 'Solde antérieur afficher la date si la somme # de zéro', 'attr' => array('placeholder' => '')))
        ->add('date_anterieure',DateType::class, array('label' => $app->trans('date antérieure'), 'widget' => 'single_text', 'format' => 'dd/MM/yyyy', 'attr' => array('class' => 'datepickerb')))
        ->add('id_entite',ChoiceType::class, array('label' => $app->trans('entite'), 'choices' => getEntitesDeLUtilisateur()))
        ->add('observation',TextareaType::class, array('label' => $app->trans('observation'), 'attr' => array('placeholder' => '')))
        ->add('save_compte',SubmitType::class, array('label' => $app->trans('Enregistrer->compte'), 'attr' => array('class' => 'btn-primary')))
        ->add('save_poste',SubmitType::class, array('label' => $app->trans('Enregistrer->Poste'), 'attr' => array('class' => 'btn-primary')))
        ->getForm();

    $form->handleRequest($request);

    if ($form->isSubmitted()) {
        if ($form->isValid()) {
            $data = $form->getData();
            if (!$modification) {
                $objet_data = new Compte();
            }
            $objet_data->fromArray($data);
            $dd = $objet_data->getSoldeAnterieur();
            if ($dd == 0) {
                $objet_data->setDateAnterieure(null);
            }
            $nextAction = $form->get('save_poste')->isClicked()
                ? 'poste'
                : 'compte';
            $objet_data->save();
            $args_rep['id'] = $objet_data->getIdCompte();
        }
    }
    if ($modification)
        $args_rep['objet_data'] = $objet_data;

    return reponse_formulaire($form,$args_rep);
}

function action_tresor_form_supprimer(){
    return action_supprimer_une_instance();
}