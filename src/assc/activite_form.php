<?php

use Propel\Runtime\Map\TableMap;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\Extension\Core\Type\FormType;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\Form\Extension\Core\Type\TextareaType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\FormError as FormError;
use Symfony\Component\Validator\Constraints as Assert;

function activite_form()
{
    global $app;
    $request = $app['request'];
    $args_rep=[];
    $modification = false;
    if (sac('id')) {
        $objet_data = charger_objet();
        $modification = true;
        $data = $objet_data->toArray(TableMap::TYPE_FIELDNAME);
    } else {
        $objet_data = array();
        $data = sac('idinit');
    }

    $builder = $app['form.factory']->createNamedBuilder('activite', FormType::class, $data);
    formSetAction($builder, $modification);
    $form = $builder
        ->add('nomcourt', TextType::class,
            array('label' => $app->trans('nomcourt'), 'attr' => array('placeholder' => '')))
        ->add('nom', TextType::class, array('label' => 'Nom', 'attr' => array('placeholder' => '')))
        ->add('id_entite', ChoiceType::class,
            array('label' => $app->trans('entite'), 'choices' => getEntitesDeLUtilisateur()))
        ->add('observation', TextareaType::class, array('label' => 'Observation', 'attr' => array('placeholder' => '')))
        ->add('submit', SubmitType::class,
            array('label' => $app->trans('Enregistrer'), 'attr' => array('class' => 'btn-primary')))
        ->getForm();

    $form->handleRequest($request);
    if ($form->isSubmitted()) {
        $data = $form->getData();

        if ($form->isValid()) {
            if (!$modification) {
                $objet_data = new Activite();
            }
            $objet_data->fromArray($data);
            $objet_data->save();
            $args_rep = [
                'ok' => true,
                'id' => $objet_data->getPrimaryKey()
            ];

        }
    }


    return reponse_formulaire($form, $args_rep);

}
