<?php

use Symfony\Component\Validator\Constraints as Assert;
use Symfony\Component\Form\FormError as FormError;
use Symfony\Component\Form\Extension\Core\Type\FormType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\Extension\Core\Type\MoneyType;

function piece_ac_form()
{
    global $app;
    $request = $app['request'];
    $id_piece = $request->get('id_piece');
    $id_ecriture = $request->get('id_ecriture');
    $id_compte = $request->get('id_compte');
    $id_poste = $request->get('id_poste');
    $id_entite = $request->get('id_entite');
    $fil = $request->get("fil");
    $objet_data=null;
    $tab_ecriturespiece=null;
    $modification=false;
    $data=array();
    $fil_url=$app->path('compte',array('id_compte'=>$id_compte));
// echo $id_ecriture.'<BR>'.$piece.$id_compte.'<BR>'.$id_poste.'<BR>';

    if($id_piece) {
        $modification=true;
        $objet_data = EcritureQuery::create()->findPk($id_ecriture);
        $data = $objet_data->toArray();
        $id_piece=$objet_data->getIdPiece();
        $id_compte = $objet_data->getIdCompte();
        $id_entite = $objet_data->getIdEntite();//voir si erreur entre entite en cours et entite de la piéce
        $compte = CompteQuery::create()->findPk($id_compte);
        $objets_ecriturespiece = EcritureQuery::create()->findByidPiece($id_piece);

        $choices_comptes=array();
        $tab_comptes= CompteQuery::create()->filterByIdEntite($id_entite)->find();
        foreach($tab_comptes as $compte) {
            $e=$compte->getIdCompte();
              $choices_comptes[$e] = array(
                  'id_compte' => $e,
                  'id_poste'=> $compte->getIdPoste(),
                  'numero' => $compte->getNcompte(),
                  'nom' => $compte->getNom());
        }

        $tab_ecriturespiece=array();
        foreach($objets_ecriturespiece as $e) {
            $id_c=$e->getIdCompte();
            $tab_ecriturespiece[]=array(
                'IdEcriture'=>$e->getIdEcriture(),
                'Credit'=>$e->getCredit(),
                'Debit'=>$e->getDebit(),
                'Libelle'=>$e->getLibelle(),
                'IdCompte'=>$id_c,
                'ncompte'=>$choices_comptes[$id_c]['Intitule'],
                'Idactivite'=>$e->getIdactivite(),
                'DateEcriture'=>$e->getDateEcriture()->format('d'),// ecran spécifique de qaisie mensuelle
                'Oservation'=>$e->getobservation()
            );

        }
        $tab_ecriturespiece=json_encode($tab_ecriturespiece);
        $data['lignes']=$tab_ecriturespiece;
        $data['montant'] = $data['credit'];
        $data['sens'] = 0;
        if ($data['montant']==0) {
            $data['montant']=$data['debit'];
            if ($data['montant']>0) {$data['sens']=1;
                $data['montant'] =-$data['montant'];}
        }
        elseif ($data['montant']<0) {$data['sens']=1;
            $data['montant'] =-$data['montant'];}

    }else{
        $data['lignes']=json_encode(array());
        $data['id_entite']=$entite_defaut=2;
        if ($id_poste==0) $data['id_compte']=$compte_defaut=2;
        else $data['id_compte']=$id_compte;
    }
    $builder = $app['form.factory']->createNamedBuilder('form',FormType::class,$data);
    $builder->setRequired(false);
//    $choices_entites= EntiteQuery::create()->find()->toKeyValue('idEntite','nom');
    $choices_activites_sql= ActiviteQuery::create()->findByidEntite($id_entite);
    $choices_activites=array();
    foreach($choices_activites_sql as $activite)
        $choices_activites[$activite->getIdActivite()]=$activite->getActivitenom();

    if ($id_poste >=1) $choices_comptes_sql= CompteQuery::create()->findByidPoste($id_poste);
        else $choices_comptes_sql= CompteQuery::create()->find();
    $choices_comptes=table_simplifier(tab('compte'));
    $choices_journals_sql= JournalsQuery::create()->find();
    $choices_journals=array();
    foreach($choices_journals_sql as $journal)
        $choices_journals[$journal->getIdJournal()]=$journal->getjournalnom();
    $choices_etat = getListeEtat();
//***********non testé
    $choices_sens = array('0'=>'Normal montant positif = Crédit positif Montant négatif =Débit positif','1'=>'Inverse Montant positif = Crédit négatif, Montant négatif = Débit négatif');
    $choices_sens = array('0'=>'Normal','1'=>'Inverse');
    $form = $builder

        ->add('date_ecriture',DateType::class, array('attr' => array('class' => 'span2')))
        ->add('date_ecriture',DateType::class, array('label'=>$app->trans('Date de l\'écriture'), 'widget'=>'single_text','format' => 'dd/MM/yyyy','attr' => array('class'=>'datepickerb')))
        ->add('classement',TextType::class, array('label'=>'Classement','attr' => array('placeholder' =>'')))
        ->add('montant',MoneyType::class, array('label'=>'Montant','attr' => array('placeholder' =>'')))
        ->add('sens',ChoiceType::class, array('label'=>'Sens','choices'=>$choices_sens,'expanded'=>true,'attr' => array('inline'=>true,'placeholder' =>'nom')))
        ->add('nom',TextType::class, array('label'=>'Libellé','attr' => array('placeholder' =>'')))
//        ->add('id_compte',ChoiceType::class, array('label'=>'Compte ajouter le nom du poste dans getlibelle','choices'=>$choices_comptes,'attr' => array()))
        ->add('id_journal',ChoiceType::class, array('label'=>'Journal','choices'=>$choices_journals,'attr' => array('placeholder' =>'')))
//        ->add('id_activite',ChoiceType::class, array('label'=>'Activité','choices'=>$choices_activites,'attr' => array('placeholder' =>'')))
//        ->add('id_entite',ChoiceType::class, array('label'=>'Entité Comment compter le nombre d\'entité pour éviter l\'affichage','choices'=>$choices_entites))
//        ->add('lettrage',TextType::class, array('label'=>'indice Lettrage','attr' => array('placeholder' =>'')))
//        ->add('date_lettrage',DateType::class, array('attr' => array('class' => 'span2')))
//        ->add('date_lettrage',DateType::class, array('label'=>$app->trans('Date de lettrage'), 'widget'=>'single_text','format' => 'dd/MM/yyyy','attr' => array('class'=>'datepickerb')))
//        ->add('etat',ChoiceType::class, array('label'=>'etat','choices'=>$choices_etat,'expanded'=>true,'attr' => array('inline'=>true,'placeholder' =>'nom')))
        ->add('observation',TextareaType::class, array('label'=>'Observation','attr' => array('placeholder' =>'')))
        ->add('id_piece',IntegerType::class, array('label'=>'Pièce','attr' => array('placeholder' =>'')))
//        ->add('contre_partie',CheckboxType::class, array('label'=>$app->trans('Masquer cette ligne en affichage simple partie') ,'attr' => array()))
        ->add("lignes",TextType::class,array('label'=>'tableau des lignes','attr' => array()))
        ->add("lignes_modif",'hidden',array('attr' => array()))

        ->add('save',SubmitType::class, array('label' => $app->trans('Enregistrer->liste des pieces'), 'attr' => array('class' => 'btn-primary')))
        ->add('save_compte',SubmitType::class, array('label' => $app->trans('Enregistrer->le compte(1° ligne'), 'attr' => array('class' => 'btn-primary')))
        ->getForm();

    $form->handleRequest($request);
    if ($form->isSubmitted()) {
        $data = $form->getData();
        //var_dump(json_decode($data['lignes']));
        //var_dump(json_decode($data['lignes_modif']));
        //exit();
        if ($form->isValid()) {
            if (!$modification) {
                $objet_data = new Ecritures();
            }
            $objet_data->fromArray($data);
            if ($data['montant']>0){
                $objet_data->setDebit(0);
                if ($data['sens']) $objet_data->setCredit(-$data['montant']);
                    else $objet_data->setCredit($data['montant']);
            } else {
                $objet_data->setCredit(0);
                    if ($data['sens']) $objet_data->setDebit(-$data['montant']);
                    else $objet_data->setDebit($data['montant']);
            }
            $objet_data->save();
            $premier['id']=$objet_data->getIdEcriture();
            $nextAction = $form->get('save')->isClicked()
                ? 'ecriturespiece_liste'
                : 'compte';
            $compte->save();
                    $app['session']->getFlashBag()->add('success', $app->trans($premier['objet_action'].'_ok'));
            return $app->redirect($premier['objet'].'?id_'.$premier['objet'].'=' . $premier['id']);
        }
    }
    $tab_param = array('form' => $form->createView(),
        'ecriture' => $objet_data,
        'modification'=>$modification,
        'id_piece'=>$id_piece,
        'tab_ecriturespiece'=>$tab_ecriturespiece,
        'id_compte'=>$id_compte,
        'id_poste'=>$id_poste,
        'id_entite'=>$id_entite,
        'fil'=>'Compte comptable a completer',//.$compte->getNcompte(),
        'fil_url'=>$fil_url);
    $EcranSaisie='ecriturespiece_ac_form.html.twig';
//    $EcranSaisie='ecriturespiece_'.$objet_data->getEcranSaisie().'_form.html.twig';
    return $app['twig']->render($EcranSaisie, $tab_param);


}
