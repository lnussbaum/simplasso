<?php

use Symfony\Component\Validator\Constraints as Assert;
use Symfony\Component\Form\FormError as FormError;
use Symfony\Component\Form\Extension\Core\Type\FormType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\Extension\Core\Type\TextareaType;
use Symfony\Component\Form\Extension\Core\Type\IntegerType;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\Extension\Core\Type\HiddenType;

use Propel\Runtime\Map\TableMap;


function journal_form()
{

    global $app;
    $ok = false;
    $args_twig=array();
    $url_redirect = '';
    $request = $app['request'];
    $objet_data = array();
    $modification = false;
    if (sac('id')) {
        $objet_data = charger_objet();
        $modification = true;
        $data = $objet_data->toArray(TableMap::TYPE_FIELDNAME);
    }
    else {
        $data = sac('idinit');
        $data['actif'] = 1;
        $data['mouvement'] = 0;
    }

    $builder = $app['form.factory']->createNamedBuilder('form',FormType::class,$data);
    $builder->setRequired(false);


    $form = $builder
        ->add('nomcourt',TextType::class, array('label' => $app->trans('nomcourt'), 'constraints' => new Assert\NotBlank(),'attr' => array('placeholder' => '')))
        ->add('nom',TextType::class, array('constraints' => new Assert\NotBlank(), 'attr' => array('class' => 'span2', 'placeholder' => 'nom'), 'extra_fields_message' => 'foobar'))
        ->add('id_compte',ChoiceType::class, array('label' => 'Compte contre partie', 'choices' => array_flip(table_simplifier(tab('compte'))), 'attr' => array()))
        ->add('actif',ChoiceType::class, array('label' => 'Actif', 'choices' => array('Non' => '0', 'Oui' => '1'), 'expanded' => true, 'attr' => array('inline'=>true)))
        ->add('mouvement', ChoiceType::class, array('label' => 'mouvement', 'choices' => array('Trésorerie' => '1', 'Autres' => '0'), 'expanded' => true, 'attr' => array()))
        ->add('id_entite',ChoiceType::class, array('label' => $app->trans('entite'), 'choices' => getEntitesDeLUtilisateur()))
        ->add('observation',TextareaType::class, array('label' => 'Observation', 'attr' => array('placeholder' => '')))
        ->add('submit',SubmitType::class, array('label' => $app->trans('Enregistrer'), 'attr' => array('class' => 'btn-primary')))
        ->getForm();

    $form->handleRequest($request);

    if ($form->isSubmitted()) {
        $data = $form->getData();
        if ($form->isValid()) {
            if ($modification) {
                $objet_data->fromArray($data);
                $objet_data->save();
                $id = $objet_data->getIdJournal();
                Log::EnrOp($modification, 'journal', $objet_data, $id);
            }else {
                require_once($app['basepath'].'/src/inc/fichier_creation.php');
                $id = journal_creation($data);
            }
            $args_rep['id'] = $id;
       }
    }

    return reponse_formulaire($form,$args_rep);

}