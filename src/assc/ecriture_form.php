<?php

use Symfony\Component\Validator\Constraints as Assert;
use Symfony\Component\Form\FormError as FormError;
use Symfony\Component\Form\Extension\Core\Type\FormType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\Extension\Core\Type\TextareaType;
use Symfony\Component\Form\Extension\Core\Type\IntegerType;
use Symfony\Component\Form\Extension\Core\Type\CheckboxType;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\Form\Extension\Core\Type\DateType;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\Extension\Core\Type\MoneyType;


function ecriture_form()
{
    global $app;
    $request = $app['request'];
    $id_ecriture = $request->get('id_ecriture');
    $id_compte = $request->get('id_compte');
    $id_poste = $request->get('id_poste');
    $id_entite = $request->get('id_entite');
    $objet_data=null;
    $modification=false;
    $data=array();
    if($id_ecriture) {
        $modification=true;
        $objet_data = EcritureQuery::create()->findPk($id_ecriture);
        $data = $objet_data->toArray();
        $data['montant'] = $data['credit'];
        $data['sens'] = 0;
        if ($data['montant']==0) {
            $data['montant']=$data['debit'];
            if ($data['montant']>0) {$data['sens']=1;
                $data['montant'] =-$data['montant'];}
        }
        elseif ($data['montant']<0) {$data['sens']=1;
            $data['montant'] =-$data['montant'];}

    }else{
        if ($id_compte==0) $data['id_compte']=$compte_defaut=269;
        else $data['id_compte']=$id_compte;
        if ($id_poste==0) $data['id_poste']=$poste_defaut=1;
        else $data['id_poste']=$id_poste;
        if ($id_entite==0) $data['id_entite']=$entite_defaut=1;
        else $data['id_entite']=$id_entite;
    }
    $builder = $app['form.factory']->createNamedBuilder('form',FormType::class,$data);
    $builder->setRequired(false);
    $choices_entites= EntiteQuery::create()->find()->toKeyValue('idEntite','nom');
    $choices_activites_sql= ActiviteQuery::create()->find();
    $choices_activites=array();
    foreach($choices_activites_sql as $activite)
        $choices_activites[$activite->getIdActivite()]=$activite->getActivitenom();

    if ($id_poste >=1) $choices_comptes_sql= CompteQuery::create()->findByidPrestation($id_poste);
        else $choices_comptes_sql= CompteQuery::create()->find();
    $choices_comptes=table_simplifier(tab('compte'));
    $choices_journals=table_simplifier(tab('journal'));
    $choices_etat = getListeEtat();
//***********non testé
    $choices_sens = array('0'=>'Normal montant positif = Crédit positif Montant négatif =Débit positif','1'=>'Inverse Montant positif = Crédit négatif, Montant négatif = Débit négatif');
    $choices_sens = array('0'=>'Normal','1'=>'Inverse');
    $form = $builder

        ->add('date_ecriture',DateType::class, array('attr' => array('class' => 'span2')))
        ->add('date_ecriture',DateType::class, array('label'=>$app->trans('Date de l\'écriture'), 'widget'=>'single_text','format' => 'dd/MM/yyyy','attr' => array('class'=>'datepickerb')))
        ->add('classement',TextType::class, array('label'=>'Classement','attr' => array('placeholder' =>'')))
        ->add('montant',MoneyType::class, array('label'=>'Montant','attr' => array('placeholder' =>'')))
        ->add('sens',ChoiceType::class, array('label'=>'Sens','choices'=>$choices_sens,'expanded'=>true,'attr' => array('inline'=>true,'placeholder' =>'nom')))
        ->add('nom',TextType::class, array('label'=>'Libellé','attr' => array('placeholder' =>'')))
        ->add('id_compte',ChoiceType::class, array('label'=>'Compte ajouter le nom du poste dans getlibelle','choices'=>$choices_comptes,'attr' => array()))
        ->add('id_journal',ChoiceType::class, array('label'=>'Journal','choices'=>$choices_journals,'attr' => array('placeholder' =>'')))
        ->add('id_activite',ChoiceType::class, array('label'=>'Activité','choices'=>$choices_activites,'attr' => array('placeholder' =>'')))
        ->add('id_entite',ChoiceType::class, array('label'=>'entite','choices'=>$choices_entites))
        ->add('lettrage',TextType::class, array('label'=>'indice Lettrage','attr' => array('placeholder' =>'')))
        ->add('date_lettrage',DateType::class, array('attr' => array('class' => 'span2')))
        ->add('date_lettrage',DateType::class, array('label'=>$app->trans('Date de lettrage'), 'widget'=>'single_text','format' => 'dd/MM/yyyy','attr' => array('class'=>'datepickerb')))
        ->add('etat',ChoiceType::class, array('label'=>'etat','choices'=>$choices_etat,'expanded'=>true,'attr' => array('inline'=>true,'placeholder' =>'nom')))
        ->add('observation',TextareaType::class, array('label'=>'Observation','attr' => array('placeholder' =>'')))
        ->add('id_piece',IntegerType::class, array('label'=>'Pièce','attr' => array('placeholder' =>'')))
        ->add('contre_partie',CheckboxType::class, array('label'=>$app->trans('Masquer cette ligne en affichage simple partie') ,'attr' => array()))
        ->add('save',SubmitType::class, array('label' => $app->trans('Enregistrer->ecriture'), 'attr' => array('class' => 'btn-primary')))
        ->add('save_compte',SubmitType::class, array('label' => $app->trans('Enregistrer->compte'), 'attr' => array('class' => 'btn-primary')))
        ->getForm();

    $form->handleRequest($request);
    if ($form->isSubmitted()) {
        $data = $form->getData();
       // var_dump($data);
        if ($form->isValid()) {
            if (!$modification) {
                $objet_data = new Ecritures();
            }
            $objet_data->fromArray($data);
            if ($data['montant']>0){
                $objet_data->setDebit(0);
                if ($data['sens']) $objet_data->setCredit(-$data['montant']);
                    else $objet_data->setCredit($data['montant']);
            } else {
                $objet_data->setCredit(0);
                    if ($data['sens']) $objet_data->setDebit(-$data['montant']);
                    else $objet_data->setDebit($data['montant']);
            }
            $objet_data->save();
            $premier['id']=$objet_data->getIdEcriture();
            $nextAction = $form->get('save')->isClicked()
                ? 'ecriture'
                : 'compte';
            $compte->save();
                $app['session']->getFlashBag()->add('success', $app->trans($premier['objet_action'].'_ok'));
             return $app->redirect($app->path($nextAction,array('id_compte'=>$objet_data->getIdCompte(),'id_ecriture'=>$objet_data->getIdEcriture())));
            return $app->redirect($premier['objet'].'?id_'.$premier['objet'].'=' . $premier['id']);
        }
    }

    $tab_param = array('form' => $form->createView(), 'ecriture' => $objet_data,'modification'=>$modification);
    return $app['twig']->render('ecriture_form.html.twig', $tab_param);


}
