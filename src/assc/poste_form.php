<?php

use Symfony\Component\Validator\Constraints as Assert;
use Symfony\Component\Form\FormError as FormError;
use Symfony\Component\Form\Extension\Core\Type\FormType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\Extension\Core\Type\TextareaType;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Propel\Runtime\Map\TableMap;


function poste_form()
{

    global $app;
    $request = $app['request'];
    $args_rep = [];
    $data = array();
    $objet_data = array();
    $modification = false;
    if (sac('id')) {
        $objet_data = charger_objet();

        $modification = true;
        $data = $objet_data->toArray(TableMap::TYPE_FIELDNAME);
    }


    $builder = $app['form.factory']->createNamedBuilder('form',FormType::class,$data);
    $builder->setRequired(false);

    $form = $builder
        ->add('nomcourt',TextType::class, array('label' => $app->trans('nomcourt'),'constraints' => new Assert\NotBlank(), 'attr' => array('class' => 'span2')))
        ->add('nom',TextType::class, array('constraints' => new Assert\NotBlank(), 'attr' => array('class' => 'span2', 'placeholder' => 'nom'), 'extra_fields_message' => 'foobar'))
        ->add('observation',TextareaType::class, array('label' => 'Observation', 'attr' => array('placeholder' => '')))
        ->add('submit',SubmitType::class, array('label' => $app->trans('Enregistrer'), 'attr' => array('class' => 'btn-primary')))
        ->getForm();

    $form->handleRequest($request);
    if ($form->isSubmitted()) {
        $data = $form->getData();

        if ($form->isValid()) {
            echo $modification.sac('id').'ligne 48 create<BR>';
            if (!$modification) {
                $objet_data = new Poste();
            }
            $objet_data->fromArray($data);
            $objet_data->save();
            $args_rep['id'] = $objet_data->getPrimaryKey();
        }
    }

    return reponse_formulaire($form, $args_rep);

}
