<?php

use Symfony\Component\Form\FormError as FormError; use Symfony\Component\Form\Extension\Core\Type\FormType; use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Validator\Constraints as Assert;

require_once('inc/fichier_creation.php');

function simplasso_trim(&$value, $key, $char = null)
{
    $value = trim($value, $char);
}

function piece_import()
{
    global $app;
    $local = $app['contexte'];
    $request = $app['request'];
    $erreurs = 0;
    $data = lire_config('import|dernier');
    $data['date_fin'] = new Datetime('2016-12-31');
    $data['date_debut'] = new Datetime('2015-01-01');
//    list($form_valide_eti, $form_etape_eti) = popupModifieConfig('import|defaut');
//    $tab_btn_eti['imprim_eti'] = array('label' => 'Imprimer les étiquettes', 'contenu' => $form_etape_eti);

    $builder = $app['form.factory']->createNamedBuilder('form',FormType::class,$data);
    $builder->setRequired(false);
    $form = $builder
        ->add('date_debut',DateType::class, array(
            'label' => 'Date début',
            'constraints' => new Assert\NotBlank(),
            'widget' => 'single_text',
            'format' => 'dd/MM/yyyy',
            'attr' => array('class' => 'datepickerb')
        ))
        ->add('date_fin',DateType::class, array(
            'label' => 'Date fin',
            'widget' => 'single_text',
            'format' => 'dd/MM/yyyy',
            'attr' => array('class' => 'datepickerb')
        ))
        ->add('id_entite',ChoiceType::class,
            array(
                'label' => $app->trans('entite'),
                'choices' => array_keys(tab_simplifier(sac('entite'))),
                'attr' => array('placeholder' => '')
            ))
        ->add('id_journal',ChoiceType::class,
            array(
                'label' => $app->trans('journal'),
                'choices' => array_keys(tab_simplifier(sac('journal'))),
                'attr' => array('placeholder' => '')
            ))
        ->add('ficimport',FileType::class,
            array(
                'label' => $app->trans('source'),
                'constraints' => new Assert\NotBlank()
            ))
        ->add('etat',ChoiceType::class,
            array(
                'label' => $app->trans('etat'),
                'choices' => lire_config('piece|etat'),
                'attr' => array('placeholder' => '')
            ))
        ->add('type',ChoiceType::class,
            array(
                'label' => $app->trans('Données'),
                'choices' => lire_config('piece|type'),
                'attr' => array('placeholder' => '')
            ))
        ->add('observation',TextType::class,
            array(
                'label' => $app->trans('observation'),
            ))
        ->add('save',SubmitType::class,
            array('label' => $app->trans('Enregistrer'), 'attr' => array('class' => 'btn-primary')))
        ->getForm();

    $form->handleRequest($request);


    if ($form->isSubmitted()) {
        $data = $form->getData();

        list($message, $erreurs) = action_piece_import($data);

        if ($form->isValid() and (!$erreurs))// Enregistrement
        {
            list($message, $erreur) = action_piece_import($data, true);
            $data['ficimport'] = "";
            ecrire_config('import|defaut', $data);
            $app['session']->getFlashBag()->add('success', $message);

            return $app->redirect(sac('objet') . '_liste');
        } else {
            $form->addError(new FormError($message));
        }
    }
    $local['fen_origine'] = 'piece';
    $local['form'] = $form->createView();
    return $app['twig']->render('piece_import.html.twig', $local);
}

/**
 * @param $data
 * @param bool|false $enregistre
 * @return array
 */
function action_piece_import($data, $enregistre = false)
{
    global $app;
    data_defaut($data);
    $tab_champs_autorises = lire_config('import|piece');
    $erreurs = 0;
    $fichier = $data['ficimport'];
    $tab_fichier = array();
    // les champs nommés ici sont transmis de tab dans tab_value
    $champ_table = array('nom', 'classement', 'observation');
//    echo $fichier;
//    arbre($tab_champs_autorises);
    $pointeur_fichier = fopen($fichier, "r");//lecture seule
    $message = 'debut a : ' . date('Y-m-d H:i:s') . PHP_EOL;
    $nb_lignes_ecrites = 0;
    $nb_lignes_lues = 0;
    $centimes = 0;
    $nbpiece = 0;
    $tab_piece = array(
        'piece' => -1,
        'id_piece' => 0,
        'nb_ligne' => 0,
        'created_at' => date('Y-m-d H:i:s'),
        'updated_at' => date('Y-m-d H:i:s'),
        'etat' => $data['etat']
    );
    $tab_ecriture = array(
        'id_piece' => 0,
        'id_journal' => $data['id_journal'],
        'id_entite' => $data['id_entite'],
        'id_compte_cp' => $data['id_compte_cp'],
        'id_poste_bilan' => $data['id_poste_bilan'],
        'id_poste_gestion' => $data['id_poste_gestion'],
        'created_at' => date('Y-m-d H:i:s'),
        'updated_at' => date('Y-m-d H:i:s'),
        'message' => "",
        'type' => $data['type'],
        'relevedebit' => 0,
        'relevecredit' => 0,
    );
    if ($tab_ecriture['type'] == 'releve') $tab_ecriture['id_piece'] ='releve';

        $tab_correspondance = array();
    if ($pointeur_fichier <> 0) {
        if ($data['type'] != 'ciel') {
            $ligne = fgets($pointeur_fichier, 4096);
            // Analyse de la première ligne
            $tab_nom_cols = explode("\t", preg_replace("/[\n\r\f]/", '', $ligne));
            if ($ligne[0] == '"') {//si le 1° caractere est "
                array_walk($tab_nom_cols, 'simplasso_trim', "\"");
            }
            foreach ($tab_nom_cols as $key => $value) {
                foreach ($tab_champs_autorises as $expreg) {
                    if (preg_match('/^' . $expreg . '$/i', $value) === 1) {
                        $tab_correspondance[$key] = $value;
                        break;
                    }
                }
            }
        }
//        echo '<br>ligne 154 ' . fgets($pointeur_fichier, 4096).'Commencement de la boucle<BR>';
//        arbre($data);
        $moisreleve = '';
        while (!feof($pointeur_fichier)) {
            $nb_lignes_lues++;
            $ligne = fgets($pointeur_fichier, 4096);
            $tab = interprete_ligne($ligne, $data, $tab_correspondance);
//            echo '<br>ligne 161 ' . $ligne;
//            arbre($tab);
//            echo '<br> piece ' . $tab_piece['piece'] . '-' . $tab['piece'];
//            arbre($tab);
//            exit;
//            if ($nb_lignes_lues==10)exit;
            if (($tab_piece['piece'] >= 1) and ($tab['piece'] != $tab_piece['piece'])) {// en début de nouvelle ligne on cherche si c'est une nouvelle piece
//                arbre($tab_piece);
//                arbre($tab_ecriture);
//                echo '<br>Fin de piece ligne lue: '. $nb_lignes_lues.'<br>';
                enregistre_piece($message, $erreurs, $tab_piece, $tab_ecriture, $nbpiece, $centimes, $enregistre);
//                arbre($tab_ecriture);
                $tab_piece['piece'] = -1;
            }
            if (strlen($ligne) >= 8) {//pour le derniere ligne
                if ($tab_piece['piece'] <= 0) {
                    //prep de la Création de la piéce , journal, compte_cp si absent
                    $tab_piece['piece'] = $tab['piece'];
                    $tab_piece['id_piece'] = $tab_ecritureecrite['id_piece'] = nouvelle_piece();
                    list($tab_piece['id_entite'], $tab_piece['id_journal'], $tab_piece['id_compte_cp'])//pour eventuel chgt entite
                        = entite($tab, $tab_ecriture, $data);
                    $tab_piece['nb_ligne'] = 0;
                }

                //$tab['date'] JJ/MM/AAAA -> AAAA-MM-JJ
                $date_ligne = substr($tab['date'], 6, 4) . '-' . substr($tab['date'], 3, 2) . '-' . substr($tab['date'],
                        0, 2);
                // Si la date est inférieure ou supérieure, passer la ligne
//                echo '<br>ligne 222---empty journal :'.(!empty($tab['journal'])).'  '.$date_ligne . '  '.$data['date_debut']->format('Y-m-d');
                if ($date_ligne >= $data['date_debut']->format('Y-m-d') and $date_ligne <= $data['date_fin']->format('Y-m-d')) {
                    $nb_lignes_ecrites++;
                    $tab_ecriture['date_ecriture'] = $date_ligne;
                    $tab_piece['date_piece'] = $date_ligne;
                    foreach ($champ_table as $champ) {
                        if (isset($tab[$champ])) {
                            $tab_ecriture[$champ] = $tab[$champ];
                            $tab_piece[$champ] = $tab[$champ];
                        }
                    }
                    $tab_ecriture['id_compte'] = compte($tab['compte'], $tab_ecriture, $data, $tab['libcompte']);
                    $tab_ecriture['id_activite'] = activite($tab['activite'], $tab_ecriture, $data);
                    $tab_ecriture['observation'] .= $data['observation'];
                    $tab_piece['observation'] .= $data['observation'];
                    totaux($tab['credit'], $tab['debit'], $tab_ecriture, $centimes);
                    //                echo '-debit-'.$tab_ecriture['debit'].'-credit-'.$tab_ecriture['credit'].'---';
                    //le numéro de pièce en reprise pour des OD AN CLO ou la reprise d'autres  écritures que relevé
                    if ($enregistre) {
                        $tab_piece['id_ecriture'] = enregistre_ecriture($tab_ecriture);
                    }
                    $tab_piece['nb_ligne']++;
// l'image associée voir aussi piece form
                    //~ if (!empty($tab['image'])) {
                    //~ if (!empty($tab['chemin'])) $fic=$tab['chemin'].'/';
                    //~ $fichier_ged($fic.$tab['image'],$id_ecriture);
                    //~ }
//comptre partie de la ligne
                    if (!empty($tab['compte_cp']) and $data['type'] != 'releve') {
                        totaux($tab['debit'], $tab['credit'], $tab_ecriture, $centimes);
                        $tab_ecriture['id_compte'] = compte($tab['compte_cp'], $tab_ecriture, $data, $tab['libcompte']);
                        $tab_ecriture['id_activite'] = activite($tab['activite_cp'], $tab_ecriture, $data);
                        if ($enregistre) {
                            enregistre_ecriture($tab_ecriture);
                        }
                        $tab_piece['nb_ligne']++;
                    }//fin contre partie
                }
            }
        }
//        if (($nb_lignes_lues % 50) == 0) {
//            $message .= $nb_lignes_lues . 'ligne à : ' . date('Y-m-d H:i:s') . PHP_EOL;
//        }
//        echo '  fin de la boucle ' . $centimes;
    }   // Test fichier chargé
    //enregistre_piece($message, $erreurs, $tab_piece, $tab_ecriture, $nbpiece, $centimes, $enregistre);
    $message .= $nb_lignes_lues . ' lignes lues ' . $nb_lignes_ecrites . ' lignes écrites ' . $nbpiece . ' piéces enregistrées ';
    $message .= '  fin à : ' . date('Y-m-d H:i:s');
    return array($message, $erreurs);
}

function totaux($credit, $debit, &$tab_ecriture, &$centimes)
{
    $tab_ecriture['credit'] = str_replace(",", ".", $credit);
    $tab_ecriture['debit'] = str_replace(",", ".", $debit);
    if ($tab_ecriture['type'] == 'releve') {
        $tab_ecriture['relevedebit'] = $tab_ecriture['relevedebit'] - $tab_ecriture['credit'];
        $tab_ecriture['relevecredit'] = $tab_ecriture['relevecredit'] - $tab_ecriture['debit'];
    }
    $centimes = $centimes + ($tab_ecriture['credit'] * 100) + ($tab_ecriture['debit'] * 100);
    echo '<br> ligne 265 :' . $debit . '*********' . $credit . '*********' . $centimes;
}

function nouvelle_piece()
{
    $res = PieceQuery::create()->orderByidPiece('desc')->findone();
    return intval($res->getIdPiece()) + 1;

}

function enregistre_piece(&$message, &$erreurs, &$tab_piece, &$tab_ecriture, &$nbpiece, &$centimes, $enregistre)
{

    if ($tab_ecriture['relevedebit'] != 0 or $tab_ecriture['relevecredit'] != 0) {
        $tab_ecriture['libelle'] = "Releve " . $tab_piece['piece'];
        $tab_piece['libelle'] = "Releve " . $tab_piece['piece'];
        $tab_piece['classement'] = "Releve";
        $tab_ecriture['id_compte'] = $tab_ecriture['id_compte_cp'];
        $tab_ecriture['id_activite'] = $tab_ecriture['id_activite_cp'];
//        $tab_ecriture[''];

        $centimes = $centimes + ($tab_ecriture['relevecredit'] * 100) + ($tab_ecriture['relevedebit'] * 100);

        if ($tab_ecriture['relevedebit'] != 0) {
            $tab_ecriture['credit'] = 0;
            $tab_ecriture['debit'] = $tab_ecriture['relevedebit'];
            if ($enregistre) {
                $tab_piece['id_ecriture']=enregistre_ecriture($tab_ecriture);
                $tab_piece['nb_ligne']++;
            }
            $tab_ecriture['relevedebit'] = 0;
        }
        if ($tab_ecriture['relevecredit'] != 0) {
            $tab_ecriture['debit'] = 0;
            $tab_ecriture['credit'] = $tab_ecriture['relevecredit'];
            if ($enregistre) {
                $tab_piece['id_ecriture']=enregistre_ecriture($tab_ecriture);
                $tab_piece['nb_ligne']++;
            }
            $tab_ecriture['relevecredit'] = 0;
        }
//        $tab_piece['id_ecriture']=
        echo '<br> ligne 301 :' .  '*********' . $centimes;
        echo "Ligne 305 en_cours";
        exit;
        echo '<br> ligne 301 :' .  '*********' . $centimes;
    }
    if (abs($centimes * 10) >= 1) {
        $message .= 'Ligne ' . $tab_piece['nb_ligne'] . ' non equilibre : . ' . $centimes . ' centimes  ' . PHP_EOL;
        if ($enregistre) {
            $tab_ecriture['libelle'] = "Equilibre en reprise ";
            $tab_ecriture['classement'] = "Equilibre";
            if ($centimes <= 0) {
                $tab_ecriture['credit'] = $centimes * (-0.01);
                $tab_ecriture['debit'] = 0;
            } else {
                $tab_ecriture['debit'] = $centimes * (-0.01);
                $tab_ecriture['credit'] = 0;
            }
            enregistre_ecriture($tab_ecriture);
            $tab_piece['nb_ligne']++;
        }
        $erreurs++;
    }
//    echo '<br>ligne 359 ' . $centimes.'---' ;
    if ($enregistre) {
        $piece = new Pieces();
        unset($tab_piece['id_piece']);
        $piece->fromArray($tab_piece);
        $piece->save();
//        echo 'ligne 383';
    }
    $nbpiece++;
    $centimes = 0;
    $tab_piece['piece='] = -1;
}

function enregistre_ecriture($tab_ecriture)
{
    $ecriture = new Ecritures();
    $ecriture->fromArray($tab_ecriture);
    $ecriture->save();
    return $ecriture->getIdEcriture();
}

function activite($activite, &$tab_ecriture, $data)
{
    $id_activite = null;
    if (!$activite) {
        $id_activite = $data['id_activite'];
    } else {
//        echo '<br> activite  :' . $activite;
        $objet_data = ActiviteQuery::create()
            ->filterByIdEntite($tab_ecriture['id_entite'])
            ->filterByNomcourt($activite)->findone();
        if ($objet_data) {
            $id_activite = $objet_data->getIdActivite();
        } else {
            $id_activite = null;
        }
    }
//    echo '<br> ligne 375 -' . $activite . '-' . $id_activite;
    if (!$id_activite and $data['creer']) {
        $id_activite = activite_creation($activite, $tab_ecriture['id_entite']);
    }
    if ($id_activite) {
        $tab_ecriture['id_activite'] = $id_activite;
        return $id_activite;
    }
    $tab_ecriture['message'] .= " Pas d'activite ";
    return false;
}

function compte($tab, &$tab_ecriture, $creer)
{
    $id_compte = null;
    if (!isset($tab['compte'])) {
        $id_compte = $tab_ecriture['id_compte_cp'];
        $compte = $tab_ecriture['compte_cp'];
    } else {
        $objet_data = CompteQuery::create()
            ->filterByIdEntite($tab_ecriture['id_entite'])
            ->filterByNcompte($tab['compte'])->findone();
        if ($objet_data) {
            $id_compte = $objet_data->getIdCompte();
        }
    }
    if (!$id_compte and $creer) {
        $id_compte = compte_creation($compte,
            0,
            $tab_ecriture['id_entite'],
            $tab['libcompte']);
        if ($objet_data) {
            $id_compte = $objet_data->getIdCompte();
        }
    }
    if ($id_compte) {
        if (!isset($tab_ecriture['compte_cp'])) {
            $tab_ecriture['compte_cp'] = CompteQuery::create()
                ->findPk($id_compte)->getNcompte();
            $tab_ecriture['id_compte_cp']=$id_compte;
        }
        return $id_compte;
    }
    $tab_ecriture['message'] .= " Pas de compte ";
    return false;
}

//function compte_cp($compte, &$tab_ecriture, $data, $libcompte)
//{
//    if (!$compte) {
//        $id_compte_cp = $tab_ecriture['id_compte_cp'];
//    } else {
////        echo '<br> compte compte_cp :' . $compte;
//        $objet_data = EntiteComptesQuerysQuery::create()
//            ->filterByIdEntite($tab_ecriture['id_entite'])
//            ->filterByNcompte($compte)->findone();
//        if ($objet_data) {
//            $id_compte_cp = $objet_data->getIdCompte();
//        } else {
//            $id_compte_cp = null;
//        }
//    }
//    if (!$id_compte_cp and $data['creer']) {
//        if (!$tab_ecriture['id_poste']) {
//            $tab_ecriture['id_poste'] = $data['poste'];
//        }
//        $id_compte_cp = compte_creation($data['compte_cp'],
//            $tab_ecriture['id_poste'],
//            $tab_ecriture['id_entite'],
//            $libcompte);
//    }
//    if ($id_compte_cp) {
//        $tab_ecriture['id_compte_cp'] = $id_compte_cp;
//        return $id_compte_cp;
//    }
//    $tab_ecriture['message'] .= " Pas de contre partie";
//    return false;
//}

function journal($tab, &$tab_ecriture, $creer)
{
    if (!$tab['journal']) {
        $tab_ecriture['id_journal'] = $tab_ecriture['id_journal'];
        $id_compte_cp = $tab_ecriture['id_compte_cp'];
    } else {
//        echo '<br> id_entite :' . $tab_ecriture['id_entite'] . '--journal :' . $journal;
        $objet_data = JournalsQuery::create()
            ->filterByIdEntite($tab_ecriture['id_entite'])
            ->filterByNomcourt($tab['journal'])->findone();
        if ($objet_data) {
            $tab_ecriture['id_journal'] = $objet_data->getIdJournal();
            $tab_ecriture['id_compte_cp'] = $objet_data->getIdCompte();
        } else {
            $id_journal = null;
        }
    }
    if (!$tab_ecriture['id_journal'] and $creer) {
        if (!$id_compte_cp) {
            $data1 = lire_config('creation|compte_cp');
            $tab_ecriture['compte_cp'] = $data1['compte'];
            if ($data1['compte'] > '6') {
                $id_poste = $tab_ecriture['id_poste_gestion'];
            } else {
                $id_poste = $tab_ecriture['id_poste_bilan'];
            }
            $tab_ecriture['id_compte_cp'] = compte_creation($data1['compte'],
                $id_poste,
                $tab_ecriture['id_entite'],
                $tab['libcompte']);
        }
        $data1 = lire_config('creation|journal');
        $tab_ecriture['id_journal'] = journal_creation($data1['nomcourt'], $tab_ecriture['id_compte_cp'],
            $tab_ecriture['id_entite']);
    }

//    echo $journal,$id_journal,$tab_ecriture['id_entite'];
    if ($tab_ecriture['id_journal']) {
        return $tab_ecriture['id_journal'];
    }
    $tab_ecriture['message'] .= " Pas de création de journal :" . $tab['journal'];
    return false;
}


function entite($tab, &$tab_ecriture, $creer = true)
{
//    echo 'ligne 544 '.$entite;
//    arbre($tab_ecriture);
    if (!$tab['entite']) {
        $id_entite = $tab_ecriture['entite'];
    } else {
//        echo '<br> id_entite :' . $entite;
        $objet_data = EntiteQuery::create()
            ->filterByNomcourt($tab['entite'])
            ->findone();
        if ($objet_data) {
            $id_entite = $objet_data->getIdEntite();
        } else {
            $id_entite = null;
        }
    }
    if (!$id_entite and $creer) {
        $id_entite = entite_creation($tab['entite']);
        $tab_ecriture['journal'] = '';
        $tab_ecriture['id_journal'] = 0;
        $tab_ecriture['compte_cp'] = '';
        $tab_ecriture['id_compte_cp'] = 0;

    }
    if ($id_entite) {
        $tab_ecriture['id_entite'] = $id_entite;
        $id_journal = journal($tab, $tab_ecriture, $creer);
        $id_compte_cp = compte($tab, $tab_ecriture, $creer);
        $tab_ecriture['id_activite_cp'] = activite_creation($tab['activite'], $id_entite);
       return array($id_entite, $id_journal, $id_compte_cp);
    }
    $tab_ecriture['message'] .= " Pas d'entité";
    return false;
}


function data_defaut(&$data)
{
    $data = array_merge(lire_config('import|defaut'), $data);
    $data['id_poste_bilan'] = poste_creation('BI', 'Bilan');
    $data['id_poste_gestion'] = poste_creation('GE', 'Gestion');
    $data['id_activite'] = activite_creation($data['activite'], $data['id_entite']);
    $data['id_compte'] = JournalsQuery::create()
        ->findpk($data['id_journal'])->getIdCompte();

    $data['entite'] = EntiteQuery::create()
        ->findpk($data['id_entite'])->getNomcourt();

    //$data['id_compte_cp'] sera utilisé si le journal n'existe pas dans la société défaut
    $objet_data1 = JournalsQuery::create()
        ->filterByIdEntite($data['id_entite'])
        ->filterByIdJournal($data['id_journal'])->findone();

    if ($objet_data1) {
        $data['id_compte_cp'] = $objet_data1->getIdCompte();
        $data['journal'] = $objet_data1->getNomcourt();
    } else {
        $data['id_compte_cp'] = JournalsQuery::create()
            ->findpk($data['compte_cp'])->getIdJournal();
    }
}

function interprete_ligne($ligne, $data, $tab_correspondance)
{
    $tab = array(
        'libcompte' => '',
        'compte_cp' => '',
        'piece' => '',
        'compte' => '',
        'activite' => '',
        'compte_cp' => '',
        'activite_cp' => '',
        'observation' => '',
        'entite' => '',
        'journal' => ''
    );
    switch ($data['type']) {
        case 'ciel':
            $tab['entite'] = trim(substr($ligne, 3, 3));
            if (!$tab['entite']) {
                $tab['entite'] = $data['entite'];
            }

            $tab['piece'] = trim(substr($ligne, 5, 5));
            $tab['journal'] = trim(substr($ligne, 10, 2));
            $tab['date'] = substr($ligne, 22, 2) . '-' . substr($ligne, 20, 2) . '-' . substr($ligne, 16, 4);
            $tab['echeance'] = substr($ligne, 24, 8);
            $tab['classement'] = substr($ligne, 32, 15);
            $tab['compte'] = substr($ligne, 47, 13);
            $tab['libelle'] = trim(substr($ligne, 60, 50));
            $sens = substr($ligne, 123, 1);
            if ($sens == 'C') {
                $tab['credit'] = trim(substr($ligne, 110, 13));
                $tab['debit'] = 0;
            } else {
                $tab['credit'] = 0;
                $tab['debit'] = trim(substr($ligne, 110, 13)) * -1;
            }
            $tab['observation'] = substr($ligne, 124, 15) . ' ' . substr($ligne, 198, 4);
            $tab['libcompte'] = trim(substr($ligne, 152, 35));
            $tab['activite'] = trim(substr($ligne, 139, 13));
            break;
        default:
            if (strlen($ligne) == 0) {
                break;
            }
            $tab_n = explode("\t", $ligne);
            if ($ligne[0] == '"') {
                array_walk($tab_n, 'simplasso_trim', "\"");
            }
            foreach ($tab_correspondance as $key => $value) {
                $tab[$value] = trim(trim($tab_n[$key], '"'));// Nommer les colonnes
            }
            if (!$tab['piece']) {
                $tab['piece'] = substr($tab['date'], 3, 7);
            }
            if (!$tab['entite']) {
                $tab['entite'] = $data['entite'];
            }
            if (!$tab['journal']) {
                $tab['journal'] = $data['journal'];
            }
    }
    return $tab;

}




