<?php

use Symfony\Component\Validator\Constraints as Assert;
use Symfony\Component\Form\FormError as FormError;
use Symfony\Component\Form\Extension\Core\Type\FormType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\Extension\Core\Type\HiddenType;
use Symfony\Component\Form\Extension\Core\Type\DateType;
use Symfony\Component\Form\Extension\Core\Type\TextareaType;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Propel\Runtime\Map\TableMap;

function piece_form()
{
    global $app;
    $local = $app['contexte'];
    $request = $app['request'];
    $objet_data = array();
    $modification = false;
    $id_piece = sac('id');
//******************************                                                      utile
    $liste_id_compte = '12,13 ';
    $liste_intitule = '"yy","zz"  ';
    $liste_id_activite = ' 1,2 ';
    $liste_activitenom = ' "a1","a2" ';
    //pour vérifier le fonctionnement sans les valeurs mettre en commentaire
    $choixCompte=tab('compte');
    $choixActivite=tab('activite');
    if (isset($choix)) {
        $liste_id_compte = implode(",", array_keys($choix));
        $liste_intitule = "'" . implode("', '", $choix) . "'";
    }
    if (isset($choixActivite)) {
        $liste_id_activite = implode(",", array_keys($choixActivite));
        $liste_activitenom = "'" . implode("', '", $choixActivite) . "'";
    }
//******************************                                                      utile


    if (sac('id')) {
        $objet_data = $local['data'][sac('objet')];
        $modification = true;
        $data = $objet_data->toArray(TableMap::TYPE_FIELDNAME);
        if (isset($local['id_ecriture'])) {
            $id_ecriture = $local['id_ecriture'];
        } else {
            $id_ecriture = 0;
        }
        if ($id_ecriture <= 0) {
            $id_ecriture = $local['tab']['ecriture'][0]->getIdEcriture();
        }
        // si le compte est necessaure dans la partie entete dans certain ecran
        //       if ($id_ecriture >= 1) {
//            $ecriture = EcritureQuery::create()->findPk($id_ecriture);
//            $id_compte = $local['tab']['ecriture'][0]->getIdCompte()
//         }
        $tabe_ecriturespiece = array();
        foreach ($local['tab']['ecriture'] as $tab) {
             $e = $tab->toArray();
            $id_c = $e['id_compte'];
            if (isset($choixActivite[$id_c])) {
                $lib_c = $choixActivite[$id_c];
            } else {
                $lib_c = 'Non trouvé';
                echo 'pas de compte ' . $id_c;
            }
            $id_a = $e['id_activite'];
            if (isset($choixActivite[$id_a])) {
                $lib_a = $choix[$id_a];
            } else {
                $lib_a = 'Non trouvé';
                echo 'pas d_activite ' . $id_a;
            }
            $tabe_ecriturespiece[] = array(
                'tIdEcriture' => $e['id_ecriture'],
//                'tJourEcriture' =>$e[date_piece],//->format('d'),// ecran spécifique de saisie mensuelle
                'tJourEcriture' =>'15',
                'tLibelle' => $e['nom'],
                'tIdCompte' => $id_c,
                'tIntitule' => $lib_c,
                'tIdActivite' => $id_a,
                'tActivitenom' => $lib_a,
                'tCredit' => $e['credit'],
                'tDebit' => -$e['debit']
            );

        }
    } else {
        $data = sac('idinit');
        $objet_data = new Piece;
        $tabe_ecriturespiece[] = array(
            'tIdEcriture' => 0,
            'tJourEcriture' => 1,// ecran spécifique de qaisie mensuelle
             'tLibelle' => "",
            'tIdCompte' => 0,
            'tIntitule' => "",
            'tIdActivite' => 0,
            'tActivitenom' => "",
                       'tCredit' => 0,
            'tDebit' => 0

        );
        $data['id_entite'] = pref('en_cours.id_entite');//  pd_entite_defaut=2;
        $data['date_piece'] = new Datetime();
        $data['etat'] = 2;
//        if ($id_poste == 0) {
//            $data['id_compte'] = $compte_defaut = 2;
//        } else {
//            $data['id_compte'] = $id_compte;
//        }
    }
    $data['modification'] = $modification;
    $data['image2']=GedQuery::create()->findPk(85)->getImage();
    $data['lignes'] = json_encode($tabe_ecriturespiece);

    $builder = $app['form.factory']->createNamedBuilder('form',FormType::class,$data);
    $builder->setRequired(false);
    $form = $builder
        ->add('modification',HiddenType::class)
        ->add('id_piece',HiddenType::class)
        ->add('date_piece',DateType::class, array('attr' => array('class' => 'span2')))
        ->add('date_piece',DateType::class, array(
            'label' => $app->trans('Date de l\'écriture'),
            'widget' => 'single_text',
            'format' => 'dd/MM/yyyy',
            'attr' => array('class' => 'datepickerb')
        ))
        ->add('classement',TextType::class, array('label' => 'Classement', 'attr' => array('placeholder' => '')))
        ->add('id_journal',ChoiceType::class,
            array('label' => 'Journal', 'choices' => tab('journal'), 'attr' => array('placeholder' => '')))
//        ->add('lettrage',TextType::class, array('label'=>'indice Lettrage','attr' => array('placeholder' =>'')))
//        ->add('date_lettrage',DateType::class, array('attr' => array('class' => 'span2')))
//        ->add('date_lettrage',DateType::class, array('label'=>$app->trans('Date de lettrage'), 'widget'=>'single_text','format' => 'dd/MM/yyyy','attr' => array('class'=>'datepickerb')))
        ->add('etat',ChoiceType::class, array(
            'label' => 'etat',
            'choices' => getListeEtat(),
            'expanded' => true,
            'attr' => array('inline' => true)
        ))//,'placeholder' =>'nom'
        ->add('image', 'file')
        ->add('image2', 'text')
        ->add('observation',TextareaType::class, array('label' => 'Observation', 'attr' => array('placeholder' => '')))
        ->add("lignes",TextType::class, array('label' => 'tableau des lignes', 'attr' => array()))
        ->add("lignes_modif",TextType::class, array('attr' => array()))
//        ->add("lignes",HiddenType::class, array('label' => 'tableau des lignes', 'attr' => array()))
//        ->add("lignes_modif",HiddenType::class, array('attr' => array()))
        ->add('save',SubmitType::class,
            array('label' => $app->trans('Enregistrer->liste des pieces'), 'attr' => array('class' => 'btn-primary')))
        ->add('save_compte',SubmitType::class,
            array('label' => $app->trans('Enregistrer->le compte(1° ligne'), 'attr' => array('class' => 'btn-primary')))
        ->getForm();

    $form->handleRequest($request);
    if ($form->isSubmitted()) {
        $data = $form->getData();
        $erreur = "";
        $tab_lignes = json_decode($data['lignes'], true);
//        Echo '<BR><BR>$tab_lignes<BR>';
//        var_dump($tab_lignes);
//        Echo '<BR><BR>$tab_lignes0<BR>';
        $totaldebit = 0;
        $totalcredit = 0;
        $i = 0;
        foreach ($tab_lignes as $ligne) {
            if ($ligne['tCredit'] - $ligne['tDebit']) {
                echo '<BR>' . $tab_lignes[$i]['tIdEcriture'] . ' ' .
                    $tab_lignes[$i]['tCredit'] . ' ' .
                    $tab_lignes[$i]['tDebit'] . ' ' .
                    $tab_lignes[$i]['tLibelle'] . ' ' .
                    $tab_lignes[$i]['tIdCompte'] . ' ' .
                    $tab_lignes[$i]['tIntitule'] . ' ' .
//                    $tab_lignes[$i]['tJourEcriture'].' ' .
                    $tab_lignes[$i]['tIdActivite'] . ' ' .
                    $tab_lignes[$i]['tActivitenom'] . ' ';
                $tab_lignes[$i]['tIdCompte'] = array_search($ligne['tIntitule'], $choixCompte, true);
                $tab_lignes[$i]['tIdActivite'] = array_search($ligne['tActivitenom'], $choixActivite,
                    true);
//                echo "<BR>  retour appres maj du numero id:";
//                echo '<BR>' . $tab_lignes[$i]['tIdEcriture'] . ' ' .
                $tab_lignes[$i]['tCredit'] . ' ' .
                $tab_lignes[$i]['tDebit'] . ' ' .
                $tab_lignes[$i]['tLibelle'] . ' ' .
                $tab_lignes[$i]['tIdCompte'] . ' ' .
                $tab_lignes[$i]['tIntitule'] . ' ' .
//                    $tab_lignes[$i]['tJourEcriture'].' ' .
                $tab_lignes[$i]['tIdActivite'] . ' ' .
                $tab_lignes[$i]['tActivitenom'] . ' ';
                if ($tab_lignes[$i]['tIdCompte'] === false) {
                    $erreur .= "\n Pas de compte valable ligne " . $i;
                }
                if ($tab_lignes[$i]['tIdActivite'] === false) {
                    $erreur .= "\n Pas d'activité valable ligne " . $i;
                }
                $totaldebit = $totaldebit + $ligne['tDebit'];
                $totalcredit = $totalcredit + $ligne['tCredit'];
                $i++;
            }
        }
        if ((round($totalcredit - $totaldebit, 2)) != 0) {
            $erreur .= "\n Ecriture non équilibrée Débit " . $totaldebit . " <> Crédit " . $totalcredit . " : " . ($totalcredit - $totaldebit);
        }
        if ($erreur) {
            $controle = false;
        } else {
            $controle = true;
        }
        if ($form->isValid() and $controle)// Enregistrement
        {
            if ($id_piece <= 0 or is_null($id_piece)) {
                echo '<br><br>ligne 181 <br><br>';
                $objet_data = new Piece();
            }
            $objet_data->fromArray($data);
            $objet_data->save();
            $i = 0;
            setContexte('id', $objet_data->getIdPiece());

            if ($id_piece <= 1) {
                $id_piece = $objet_data->getIdPiece();
            }
            $oklignevalide = true;
            //*********************************************** Image
 if (!empty($data['image'])) {
      $objet_data_image = new Ged;
     echo '<br>ligne 217';
     var_dump($data['image']);
       echo '<br>ligne 219';
     echo '<br>'.$data['image']->getsize();
     echo '<br>'.$data['image']->geterror();
//     echo '<br>'.$data['image']['originalName''];
     echo '<br>ligne 223'.'<br>';
     echo filetype($data['image']);
     echo '<br>';
     echo file_get_contents($data['image']);
     echo '<br>ligne 223'.'<br>';
//    echo  getimagesize($data['image']);


//     if (!$finfo) {
//         echo "Échec de l'ouverture de la base de données fileinfo";
//         exit();
//     }





      $objet_data_image->setImage(file_get_contents($data['image']));
     $objet_data_image->setExtension($data['image']->getmimeType());
     $objet_data_image->setNom('nom');
    $objet_data_image->save();
     exit;

}
//     $img_blob = file_get_contents ($_FILES['fic']['tmp_name']);
//$img_nom,$img_taille,$img_type,$img_blob
//
//     $fichier_ged($fic.$tab['image'],$id_ecriture);
//            //~ }

//            // Testons si le fichier a bien été envoyé et s'il n'y a pas d'erreur
//            if (isset($_FILES['image']) AND $_FILES['logo']['error'] == 0) $erreur = "Erreur lors du transfert";
//            {
//                if ($_FILES['image']['size'] <= 8000000) $erreur = "Le fichier est trop gros";
//                {
//                    $infosfichier = pathinfo($_FILES['image']['name']);
//                    $extensions_autorisees = array('jpg', 'jpeg', 'gif', 'png');
//                    $extension_upload = strtolower(  substr(  strrchr($_FILES['image']['name'], '.')  ,1)  );
//                    if (in_array($extension_upload, $extensions_autorisees)) echo "Extention correcte";
//                    {
//                        $image_sizes = getimagesize($_FILES['image']['tmp_name']);
//                        if ($image_sizes[0] > $maxwidth OR $image_sizes[1] > $maxheight) $erreur = "Image trop grande";
//                        {
//                            $nom_image = "avatars/{$id}.{$extension_upload}";
//                            move_uploaded_file($_FILES['logo']['tmp_name'], '../uploads/' . basename($_FILES['image']['name']));
//                            echo "L'envoi a bien été effectué !";
            //*********************************************** Fin Image

            foreach ($tab_lignes as $ligne) {
                if ($ligne['tIdEcriture'] <= 0) {
                    $ecriture = new Ecritures();
                } else {
                    $ecriture = EcritureQuery::create()->findPk($ligne['tIdEcriture']);
                }


                if ($ligne['tCredit'] - $ligne['tDebit']) {
                    $ecriture->setIdPiece(sac('id'));
                    $ecriture->setIdEntite($data['id_entite']);
                    $ecriture->setDebit(-$ligne['tDebit']);
                    $ecriture->setCredit($ligne['tCredit']);
                    if ($ligne['tDebit'] == null) {
                        $ecriture->setDebit(0);
                    }
                    if ($ligne['tCredit'] == null) {
                        $ecriture->setCredit(0);
                    }
                    $ecriture->setIdActivite($ligne['tIdActivite']);
                    $ecriture->setIdCompte($ligne['tIdCompte']);
                    $ecriture->setDateEcriture($data['date_piece']->format('Y-m-d'));
                    $ecriture->setIdJournal($objet_data->getIdJournal());
                    $ecriture->setEtat($data['etat']);
                    $ecriture->setObservation($data['observation']);
                    $ecriture->setClassement($data['classement']);
                    $ecriture->setIdJournal($data['id_journal']);
                    $ecriture->setLibelle($ligne['tLibelle']);
                    $ecriture->save();
                    if ($oklignevalide) {
                        $objet_data->setIdEcriture($ecriture->getIdEcriture());
                        $objet_data->setLibelle($ecriture->getLibelle());
                        $oklignevalide = false;
                    }
                    $i++;
                } elseif ($ligne['tIdEcriture'] >= 1)//effacer les lignes avec montant zéro si elle existait
                {
                    $ecriture->delete();
                }
            }
            $objet_data->setNbLigne($i);
            setContexte('id', $objet_data->getIdPiece());
            $objet_data->save();//pour enregistrer le numéro d'écriture le Nb ligne et le 1premier libellé dans piece
            $nextAction = $form->get('save')->isClicked()
                ? 'piece_liste'
                : 'compte';
            $app['session']->getFlashBag()->add('success', $app->trans(sac('objet_action') . '_ok'));
            return $app->redirect(sac('objet') . '?id_' . sac('objet') . '=' . sac('id'));
        } else {
            $form->addError(new FormError($erreur));
            //     $app['session']->getFlashBag()->add('info', $app->trans('probleme_contole_traitement_formulaire'));
        }
    }

    $tab_param = $local;
    $tab_param['liste_intitule'] = $liste_intitule;
    $tab_param['liste_activitenom'] = $liste_activitenom;
    $tab_param['liste_id_compte'] = $liste_id_compte;
    $tab_param['liste_id_activite'] = $liste_id_activite;
    $tab_param['js_suppl']="handontable";
    $tab_param['form'] = $form->createView();

    return $app['twig']->render(fichier_twig(), $tab_param);

}
