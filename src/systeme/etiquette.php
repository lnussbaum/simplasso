<?php

use Symfony\Component\Form\Extension\Core\Type\SubmitType;

function etiquette()
{
    global $app;


    $args_rep = [];
    $request = $app['request'];
    $pref = pref('imprimante.etiquette');
    $objet = $request->get('objet');
    $data =$pref[$objet];

    // list($where,$nb_carte)=carte_adherent_get_where();
    $builder = $app['form.factory']->createNamedBuilder('bloc', EtiquetteForm::class, $data);
    $builder->setRequired(false);
    formSetAction($builder, false, ['objet' => $objet]);


    $form = $builder->add('submit', SubmitType::class,
        array('label' => $app->trans('generer le PDF'), 'attr' => array('class' => 'btn-primary')))
        ->getForm();


    $form->handleRequest($request);
    if ($form->isSubmitted()) {
        $data = $form->getData();

        if ($form->isValid()) {

            $tab_pref=[
                'position_depart'=> $data['position_depart'],
                'avec_individu'=> $data['avec_individu'],
                'selection'=> $data['selection'],
                'champs_civilite'=> $data['champs_civilite'],
                'champs_titulaire'=> $data['champs_titulaire'],
                'champs_nom_membre'=> $data['champs_nom_membre'],
                'champs_numero'=> $data['champs_numero'],
                'ajout_nom_membre'=> $data['ajout_nom_membre'],
                'classement'=> $data['classement']

            ];

            ecrire_preference('imprimante.etiquette.'.$objet,$tab_pref);
            $args_rep['url_redirect'] = $app->path('etiquette',
                [
                    'action' => 'imprimer',
                    'objet' => $objet
                ]);

        }
    }


    $tab_log = LogQuery::create()->filterByCode('IMPETI')->orderByDateOperation(\Propel\Runtime\ActiveQuery\Criteria::DESC)->find();
    $tab_impression = [];
    foreach ($tab_log as $log) {
        $ged = GedLienQuery::create()->filterByObjet('log')->filterByIdObjet($log->getPrimaryKey())->findOne();
        if ($ged) {
            $tab_impression[] = ['id_ged' => $ged->getIdGed(), 'date' => $log->getDateOperation()];
        }
    }
    $args_rep['tab_impression'] = $tab_impression;
    $args_rep['js_init'] = 'position_table';
    include_once($app['basepath'] . '/src/systeme/preferences.php');
    $args_rep['form_pref'] = construire_form_preference('imprimante.etiquette',1,['objet'=>$objet])->createView();
    return reponse_formulaire($form, $args_rep);

}


function action_etiquette_imprimer()
{
// Chargement des paramètres de configuration
    global $app;
    $request = $app['request'];
    $objet = 'membre';
    if ($objet) {
        $objet = $request->get('objet');
    }
    $config = pref('imprimante.etiquette');
    $config_etiquette = $config[$objet];
    $avec_individu = (isset($config_etiquette['avec_individu'])) ? $config_etiquette['avec_individu'] : false;
    $where = "1=1";
    $valeur_selection = false;
    $pr_m = descr('membre.nom_sql');
    $pr_ind = descr('individu.nom_sql');

    $selection = $config_etiquette['selection'];
    if ($selection === 'courante') {
        $valeur_selection = $app['session']->get('selection_courante_' . $objet);

    } elseif ($selection !== 'tous') {
        $indice_selection = $selection;
        $valeur_selection = $prefs = pref('selection.' . $objet . '.' . $indice_selection . '.valeurs');

    }

    if ($valeur_selection) {
        switch ($objet) {
            case 'membre' :
                $sous_requete = getSelectionObjet('membre', $valeur_selection);
                $where .= ' AND ' . $pr_m . '.id_membre IN(' . $sous_requete . ')';
                break;
            case 'individu' :
                $sous_requete = getSelectionObjet('individu', $valeur_selection);
                $where .= ' AND ' . $pr_ind . '.id_individu IN(' . $sous_requete . ')';
                break;
        }
    }



    switch ($objet) {
        case 'membre' :
            $order_by = descr('membre.nom_sql') . '.nom,nom_famille,prenom';
            break;
        case 'individu' :
            $order_by = 'nom_famille,prenom';
            break;
    }
    $order = (isset($config_etiquette['classement'])) ? $config_etiquette['classement'] : false;
    if ($order) {
        switch ($order) {
            case 1 :
                $order_by = 'codepostal , ville, ' . $order_by; // cp avant pour regrouper les villes du même sercteur
                break;
            case 2 :
                $order_by = ' ville, ' . $order_by;
                break;
        }
    }


    $tab_champs_membre = array(
        'id' => 'id_membre',
        'id_titulaire' => 'id_individu_titulaire',
        'nom_membre' => 'nom',

    );
    foreach ($tab_champs_membre as $k => &$champs_membre) {
        $champs_membre = $pr_m . '.' . $champs_membre . ' as ' . $k;
    }

    $tab_champs_individus = array(
        'id_individu',
        'nom_famille',
        'prenom',
        'adresse',
        'codepostal',
        'ville',
        'pays'
    );

    foreach ($tab_champs_individus as &$champs_individu) {
        $champs_individu = $pr_ind . '.' . $champs_individu;
    }

    $nb_total = 0;
    $tab_id = [];
    switch ($objet) {
        case 'membre' :

            $select = 'SELECT distinct ' . implode(',', $tab_champs_membre) . ',' . implode(',', $tab_champs_individus);
            $from = ' FROM asso_individus ' . $pr_ind . ', asso_membres_individus ami, asso_membres ' . $pr_m . ' ';
            $where = ' WHERE ami.id_membre = ' . $pr_m . '.id_membre and  ami.id_individu=' . $pr_ind . '.id_individu and ' . $where;
            $res = $app['db']->fetchAll($select . $from . $where . ' ORDER BY ' . $order_by);


            $tab = [];
            foreach ($res as $r) {
                if ($r['id_individu'] == $r['id_titulaire']) {
                    $tab[$r['id']] = $r;
                    $tab[$r['id']]['individu'] = array();
                }
                $nb_total++;
            }
            if ($avec_individu){
                foreach ($res as $r) {
                    if ($r['id_individu'] != $r['id_titulaire']) {
                        $tab[$r['id']]['individu'][] = $r;
                    }
                }
            }
            $res = $tab;
            break;
        case 'individu' :
            $select = 'SELECT distinct ' . $pr_ind . '.id_individu as id, " " as id_titulaire, " " as nom_membre,' . implode(',',
                    $tab_champs_individus);
            $from = ' FROM asso_individus ' . $pr_ind;
            $where = ' WHERE ' . $where;
            $res = $app['db']->fetchAll($select . $from . $where . ' ORDER BY ' . $order_by);

            foreach ($res as $r) {
                $tab_id[] = $r['id'];
                $nb_total++;
            }
            break;
    }

    $erreurs = array();
    if ($nb_total == 0) {
        return ('Aucune étiquette à imprimer');
    } else {


        $pays_par_defaut = lire_config('pays');
        $position_depart = $request->get('position_initiale');

        $avec_titulaire = (isset($config_etiquette['champs_titulaire'])) ? $config_etiquette['champs_titulaire'] : false;
        $avec_nom_membre = (isset($config_etiquette['champs_nom_membre'])) ? $config_etiquette['champs_nom_membre'] : false;
        $ajout_nom_membre = (isset($config_etiquette['ajout_nom_membre'])) ? $config_etiquette['ajout_nom_membre'] : false;
        $avec_civilite = $config_etiquette['champs_civilite'];
        $avec_numero = $config_etiquette['champs_numero'];

        $nb_colonne = max($config['nb_colonne'], 1);
        $nb_ligne = $config['nb_ligne'];
        $largeur_page = $config['largeur_page'];
        $hauteur_page = $config['hauteur_page'];
        $marge_haut_etiquette = $config['marge_haut_etiquette'];
        $marge_gauche_etiquette = $config['marge_gauche_etiquette'];
        $marge_droite_etiquette = $config['marge_droite_etiquette'];
        $marge_haut_page = $config['marge_haut_page'];
        $marge_bas_page = $config['marge_bas_page'];
        $marge_gauche_page = $config['marge_gauche_page'];
        $marge_droite_page = $config['marge_droite_page'];
        $espace_etiquettesh = $config['espace_etiquettesh'];
        $espace_etiquettesl = $config['espace_etiquettesl'];
        $indice = 0;
        if (intval($position_depart) > 0) {
            $indice = intval($position_depart) - 1;
        }

        $tab_pays = tab('pays');

        // Calcul des dimensions des étiquettes
        $largeur_etiquette = ($largeur_page - $marge_gauche_page - $marge_droite_page - (($nb_colonne - 1) * $espace_etiquettesl)) / $nb_colonne;
        $pas_horizontal = $largeur_etiquette + $espace_etiquettesl;
        $largeur_cellule = $pas_horizontal - $marge_gauche_etiquette - $marge_droite_etiquette;
        $hauteur_etiquette = ($hauteur_page - $marge_haut_page - $marge_bas_page - (($nb_ligne - 1) * $espace_etiquettesh)) / $nb_ligne;
        $pas_vertical = $hauteur_etiquette + $espace_etiquettesh;
        $tab_etiquette = array();
        $indice_colonne = 0;
        $indice_ligne = 0;
        $num_page = 1;


        $pdf = new FPDF('P', 'mm', 'A4', false);
        $pdf->titre = '';
        //$pdf->Open();
        $pdf->AddPage();
        $pdf->SetAutoPageBreak(0, 0);
        $pdf->AliasNbPages();
        $pdf->SetFont('Arial', '', 8);

        foreach ($res as $r) {


            $tab_temp = array();
            if (!empty($r['individu'])) {
                $tab_temp = $r['individu'];

            }
            $tab_temp[] = $r;
            $tab_temp2 = [];
            foreach ($tab_temp as $key => $row) {
                $tab_temp2[$key] = $row['nom_famille'];
            }
            array_multisort($tab_temp2, SORT_ASC, $tab_temp);
            foreach ($tab_temp as $val) {

                $civ = "";
                if ($avec_civilite && isset($val['civilite'])) {
                    $civ = $val['civilite'] . " ";
                }

                if ($avec_nom_membre){
                    $vnom = trim($civ . $val['nom_membre']);

                }else{

                    if (strlen(trim($civ . $val['nom_famille'] . ' ' . $val['prenom'])) > 40) {
                        $vnom = trim($civ . $val['nom_famille']);
                    } else {
                        $vnom = trim($civ . $val['nom_famille'] . ' ' . $val['prenom']);
                    }

                }



                $val['adresse'] = str_replace('<br>',"\n", $val['adresse']);
                $tab_adresse = preg_split('/\n/', $val['adresse']);


                if ($val['pays'] == $pays_par_defaut) {
                    $val['pays'] = "";
                }

                if ($val['pays'] && isset($tab_pays[$val['pays']])) {
                    $val['pays'] = $tab_pays[$val['pays']];
                }
                if (strlen($tab_adresse[0]) > 40) {
                    if (!isset($tab_adresse[1])) {
                        $tab_adresse[1] = substr($tab_adresse[0], 40);
                        $tab_adresse[0] = substr($tab_adresse[0], 0, 40);
//                        echo '<br>' . $val['id'] . '-' . $tab_adresse[0] . '****' . $tab_adresse[1];
                    } else {
//                        echo '<br>' . $val['id'] . '++++++++++++++' . $tab_adresse[0] . '****' . $tab_adresse[1];

                    }
                }
                $etiquette = array(
                    'ligne1' => /*utf8_decode("Numéro d'adhérent : ") .*/
                        $val['id'],
                    'ligne2' => trim($vnom),
                    'ligne3' => substr($tab_adresse[0], 0, 40),
                    'ligne4' => '',
                    'ligne5' => trim($val['codepostal']) . ' ' . $val['ville'],
                    'ligne6' => $val['pays'],

                );

                if (isset($val['boite_postale'])) {
                    $etiquette['ligne5'] = $val['boite_postale'] . ' ' . $etiquette['ligne5'];
                }

                if (isset($tab_adresse[1])) {
                    $etiquette['ligne4'] = $tab_adresse[1];
                }

                if ($avec_titulaire) {
                    if ($val['id_individu'] == $val['id_titulaire']) {
                        $etiquette['ligne1'] .= ' * ';
                    }
                }

                if ((!$avec_nom_membre) && $ajout_nom_membre) {
                    if (isset($val['individu']) && count($val['individu']) > 0 || $val['id_individu'] != $val['id_titulaire']) {
                        $etiquette['ligne1'] .= ' - ' . utf8_decode(substr($val['nom_membre'], 0, 40));
                    }
                }

                if ((fmod($indice, $nb_colonne * $nb_ligne) == 0) and ($indice > 0)) {
                    $pdf->AddPage();
                    $num_page++;
                }
                $indice_colonne = $indice % $nb_colonne;
                $indice_ligne = floor($indice / $nb_colonne) % $nb_ligne;


                $posx = $indice_colonne * $pas_horizontal + $marge_gauche_page;
                $posy = $indice_ligne * $pas_vertical + $marge_haut_page;
                $imp_droite = ($posx + $pas_horizontal - $marge_droite_etiquette - $espace_etiquettesl - $marge_gauche_etiquette);
                //$pdf->SetRightMargin($imp_droite);
                $pdf->SetLeftMargin($posx + $marge_gauche_etiquette );
                //$pdf->setX($posx+$marge_gauche_etiquette);
                $pdf->setY($posy + $marge_haut_etiquette );
                //$pdf->SetRightMargin($imp_droite);
                $largeur_cellule_adjuste = $largeur_cellule ;

                if ($avec_numero) {
                    //$pdf->Rect($posx, $posy, $largeur_etiquette, $hauteur_etiquette );

                    $pdf->SetFont('Arial', '', 7);
                    $pdf->Cell(0, 5, $etiquette['ligne1'], 0, 2);
                    $pdf->SetFont('Arial', 'B', 9);
                }
                $pdf->SetFont('Arial', 'B', 9);
                if (strlen($etiquette['ligne2']) > 40) {
                    $pdf->SetFont('Arial', 'B', 8);
                    if (strlen($etiquette['ligne2']) > 40) {
                        $pdf->SetFont('Arial', 'B', 7);
                    }
                }
                $pdf->Cell($largeur_cellule_adjuste, 5, utf8_decode($etiquette['ligne2']), 0, 2);
                $pdf->SetFont('Arial', '', 8);
                if ($etiquette['ligne3'] > '') {
                    $pdf->Cell($largeur_cellule_adjuste, 5, utf8_decode($etiquette['ligne3']), 0, 2);
                }
                if ($etiquette['ligne4'] > '') {
                    $pdf->Cell($largeur_cellule_adjuste, 5, utf8_decode($etiquette['ligne4']), 0, 2);
                }
                $pdf->SetFont('Arial', '', 9);
                if ($etiquette['ligne5'] > '') {
                    $pdf->Cell($largeur_cellule_adjuste, 5, utf8_decode($etiquette['ligne5']), 0, 2);
                }
                if ($etiquette['ligne6'] > '') {
                    $pdf->Cell($largeur_cellule_adjuste, 5, utf8_decode($etiquette['ligne6']), 0, 2);
                }
                //$pdf->Rect($posx, $posy, $largeur_etiquette, $hauteur_etiquette );

                $indice++;
            }

        }

        return $pdf->Output('etiquettes.pdf', 'D');

    }

    return $erreurs;

}
