<?php

use \Propel\Runtime\ActiveQuery\Criteria;

function inspecteur()
{
    global $app;
    $args_twig = array();
    $args_twig['action'] = '';
    $args_twig['tab_action']=['servicerendu_sans_paiement'];


    return $app['twig']->render(fichier_twig(), $args_twig);
}


function action_inspecteur_servicerendu_sans_paiement(){

    global $app;
    $args_twig['action'] = $app['request']->get('action');
    $tab_servicerendu= ServicerenduQuery::create()
        ->filterBy('Montant',0,Criteria::GREATER_THAN)
        ->leftJoinServicepaiement()
        ->find();
    $tab=[];
    foreach($tab_servicerendu as $sr) {
        if ( ($sr->getMontant()>$sr->getSommeServicepaiement())){

            $tab[]=$sr;
        }

    }
    $args_twig['tab'] = $tab;

    return $app['twig']->render(fichier_twig(), $args_twig);
}



function action_inspecteur_individu_sans_coords(){

    global $app;
    $args_twig['action'] = $app['request']->get('action');
    $est_adherent = $app['request']->get('est_adherent');
    list($tab_id_individu,$nb) = objet_liste_dataliste_preselection('individu',['est_georeference'=>true,'est_adherent'=>$est_adherent],true,false);

    $args_twig['tab']= IndividuQuery::create()
        ->filterByAdresse(null,Criteria::ISNOTNULL)
        ->filterByIdIndividu($tab_id_individu,Criteria::NOT_IN)
        ->find();




    return $app['twig']->render(fichier_twig(), $args_twig);
}


function action_inspecteur_individu_ville_inconnue(){

    global $app;
    $args_twig['action'] = $app['request']->get('action');

    $tab_id_individu = CommuneLienQuery::create()->findByObjet('individu')->toKeyValue('IdObjet','IdObjet');

    $args_twig['tab']= IndividuQuery::create()
        ->filterByIdIndividu($tab_id_individu,Criteria::NOT_IN)
        ->filterByVille(null,Criteria::ISNOTNULL)
        ->find();



    return $app['twig']->render(fichier_twig(), $args_twig);
}




function action_inspecteur_individu_sans_ville(){

    global $app;
    $args_twig['action'] = $app['request']->get('action');

    $args_twig['tab']= IndividuQuery::create()
        ->filterByVille(null,Criteria::ISNULL)
        ->find();



    return $app['twig']->render(fichier_twig(), $args_twig);
}



function action_inspecteur_individu_sans_cp(){

    global $app;
    $args_twig['action'] = $app['request']->get('action');

    $args_twig['tab']= IndividuQuery::create()
        ->filterByVille(null,Criteria::ISNOTNULL)
        ->filterByCodepostal(null,Criteria::ISNULL)
        ->find();



    return $app['twig']->render(fichier_twig(), $args_twig);
}

function action_inspecteur_individu_email_invalide(){

    global $app;
    $args_twig['action'] = $app['request']->get('action');

    $args_twig['tab']= IndividuQuery::create()
        ->filterByEmail(null,Criteria::ISNOTNULL)
        ->filterByEmail('email NOT REGEXP \'^[^@]+@[^@]+\.[^@]{2,}$\'',Criteria::CUSTOM)
        ->filterByEmail('',Criteria::NOT_EQUAL)
        ->find();


    return $app['twig']->render(fichier_twig(), $args_twig);
}

