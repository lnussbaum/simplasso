<?php
function tache_liste()
{
    global $app;
    $args_twig=[
        'tab_col'=>tache_colonnes()
        ];
    return $app['twig']->render(fichier_twig(), $args_twig);

}


function tache_colonnes(){

    $tab_colonne= array();
	$tab_colonne['id_tache'] = ['title'=> 'id',];
	$tab_colonne['descriptif'] = [];
	$tab_colonne['fonction'] = [];
	$tab_colonne['args'] = [];
	$tab_colonne['statut'] = [];
	$tab_colonne['priorite'] = [];
	$tab_colonne['date_execution'] = ['type'=> 'date-eu',];
	$tab_colonne['created_at'] = [];

    $tab_colonne['action']=["orderable"=> false];
    $tab_colonne = datatable_complete_liste_colonne($tab_colonne);

    return $tab_colonne;

}





function action_tache_liste_dataliste()
{

    global $app;
    $request = $app['request'];
    $args = array();
    if (sac('objet') != 'tache') {
        $args = array('prestation_type' => descr( sac('objet') . '.alias_valeur'));
    }
    list($sous_requete, $nb_total) = getSelectionObjetNb();

    $tab_data = array();
    $start = request_ou_options('start');
    $length = request_ou_options('length');

    $tab_tri = tri_dataliste();
    $tab_tri_sql = tri_dataliste_sql(descr( sac('objet') . '.alias'), $tab_tri);
    $tab_id = $app['db']->fetchAll($sous_requete . $tab_tri_sql . ' LIMIT ' . (intval($start)) . ',' . $length);

    foreach ($tab_id as &$result) {
        $result = $result['id_tache'];
    }

    $tab_colonnes = tache_colonnes();
    $tab_data=array();

    if (!empty($tab_id)) {

        $tab = TacheQuery::getAll($tab_id,tri_dataliste($tab_tri));
        $tab_data= datatable_prepare_data($tab,$tab_colonnes);

    }



    return $app->json([
        'draw' => $request->get('draw'),
        'recordsTotal' => $nb_total,
        'recordsFiltered' => $nb_total,
        'data' => $tab_data,
        'error' => ''
    ]);

}


