<?php

use Propel\Runtime\ActiveQuery\Criteria;


function action_tache_execute()
{
    global $app;

    $file_lock = $app['tmp.path'] . '/tache_lock';
    $num = 0;
    if (file_exists($file_lock)) {
        echo('Traitement du cours détecté' . PHP_EOL);
        $num = intval(file_get_contents($file_lock));
        if ($num > 1) {
            $tache = TacheQuery::create()->filterByStatut(1)->findOne();
            if ($tache) {
                $tache->setStatut(4);
                $tache->save();
            }
            $num = 0;
        } else {
            $num++;
            file_put_contents($file_lock, $num);
        }

    }

    if ($num == 0) {
        // Reprendre les tâches suspendu en cours d'execution
        $tache = TacheQuery::create()
            ->filterByStatut(1)
            ->filterByDateExecution(time(),Criteria::LESS_EQUAL)
            ->orderByDateExecution()
            ->orderByPriorite()
            ->findOne();

        if ($tache) {
            file_put_contents($file_lock, "0");
            $tache->execute();

        } else {

            // Excecute les tâches à faire
            $tache = TacheQuery::create()
                ->filterByStatut(0)
                ->filterByDateExecution(time(), Criteria::LESS_EQUAL)
                ->orderByDateExecution()
                ->orderByPriorite()
                ->findOne();

            if ($tache) {

                file_put_contents($file_lock, "0");
                $tache->execute();


            }
        }
        // Supprime les tâches faites
        TacheQuery::create()->filterByStatut(2)->delete();


        // Ajoute les taches périodiques manquantes
        if(!TacheQuery::create()->filterByFonction('rapport')->exists()){
            $conf = lire_config('rapport');
            $date_exec = new DateTime();
            $date_exec->add(new DateInterval('P1M'));
            tache_ajouter('rapport', 'Rapport périodique d\'activité', '', $date_exec);
        }

        if(!TacheQuery::create()->filterByFonction('zone')->exists()){
            $conf = lire_config('zone');
            $date_exec = new DateTime();
            $date_exec->add(new DateInterval('PT10S'));
            tache_ajouter('zone', 'zone', '', $date_exec);
        }


        if (file_exists($file_lock)) {
            unlink($file_lock);
        }
    }
    return "OK";
}



