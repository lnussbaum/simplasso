<?php

use Symfony\Component\Form\Extension\Core\Type\FormType;
use Symfony\Component\Form\Extension\Core\Type\HiddenType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\Extension\Core\Type\TextareaType;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\Form\FormError as FormError;
use Symfony\Component\Validator\Constraints as Assert;


include_once($app['basepath'] . '/src/inc/variables_preference.php');
include_once($app['basepath'] . '/src/inc/fonctions_formbuilder.php');

function preferences()
{
    global $app;
    $request = $app['request'];
    $pref = str_replace('|','.',$request->get('pref'));

    if ($pref) {
        $args_rep=[];
        $niveau = $request->get('niveau',1);
        $form = construire_form_preference($pref,$niveau);
        return reponse_formulaire($form, $args_rep, 'inclure/form.html.twig');

    } else {

        $tab_entree = variables_preference();
        $choix = array();
        foreach ($tab_entree as $k => $entree) {
            $data = pref($k);
            $form = transforme_en_formulaire($k, $entree['variables'], $data,'ecrire_preference',100);
            $choix[$k] = $form->createView();
        }
        $args_twig['choix'] = $choix;
        return $app['twig']->render(fichier_twig(), $args_twig);
    }


}



function construire_form_preference($pref,$niveau=1,$args=array()){

    $data = pref($pref);
    $tab_entree = variables_preference();
    $cle=$pref;
    if (!strpos($pref,'.')===false){
        $pref0 = explode('.',$pref);
        $cle = $pref0[0];
        if (is_array($pref0) && !empty($pref0)){
            array_shift($pref0);
            $tab_entree = tableauChemin($tab_entree[$cle]['variables'],implode('.',$pref0));
        }
    }else
    {
        $tab_entree = $tab_entree[$cle]['variables'];
    }
    $args['pref'] = str_replace('.','|',$pref);
    $form = transforme_en_formulaire($pref, $tab_entree, $data, 'ecrire_preference',$niveau,$args);
    return $form;
}



function action_preferences_ajout_selection()
{

    global $app;
    $data = array();
    $args_rep = [];
    $request = $app['request'];
    $objet = $request->get('objet');

    $builder = $app['form.factory']->createNamedBuilder('ajout_selection', FormType::class, $data,
        array('action' => $app->path('preferences', array('action' => 'ajout_selection', 'objet' => $objet))));
    $builder->setRequired(false);

    $builder = $builder
        ->add('nom', textType::class, array('label' => 'Nom de la selection'))
        ->add('description', TextareaType::class, array('label' => 'Description de la selection'));

    $form = $builder->add('submit', SubmitType::class,
        array('label' => $app->trans('Enregistrer'), 'attr' => array('class' => 'btn-primary')))
        ->getForm();

    $form->handleRequest($request);
    if ($form->isSubmitted()) {
        $data = $form->getData();
        if ($form->isValid()) {
            $selection = pref('selection.' . $objet);

            if (empty($selection)) {
                $selection = array();
            }

            $vselection = $app['session']->get('selection_courante_' . $objet);
            if(isset($vselection['commune'])){
                foreach($vselection['commune'] as &$v){
                    $com=CommuneQuery::create()->findPk($v);
                    $v = ['id'=>$v,'text'=>$com->getNom().' ('.$com->getDepartement().')'];
                }
            }
            if(isset($vselection['departement'])){
                foreach($vselection['departement'] as &$v){
                    $com=DepartementQuery::create()->findPk($v);
                    $v = ['id'=>$v,'text'=>$com->getNom()];
                }
            }
            if(isset($vselection['region'])){
                foreach($vselection['region'] as &$v){
                    $com=RegionQuery::create()->findPk($v);
                    $v = ['id'=>$v,'text'=>$com->getNom()];
                }
            }
            if(isset($vselection['arrondissement'])){
                foreach($vselection['arrondissement'] as &$v){
                    $com=ArrondissementQuery::create()->findPk($v);
                    $v = ['id'=>$v,'text'=>$com->getNom().' ('.$com->getDepartement().')'];
                }
            }


            $selection[] = array(
                'nom' => $data['nom'],
                'description' => $data['description'],
                'valeurs' => $vselection
            );
            ecrire_preference('selection.' . $objet, $selection);
            $args_rep['message'] = 'selection_ajouter_ok';
            $args_rep['url_redirect'] = '';
            $args_rep['vars']['modif_html']['choix_selection'] = $app['twig']->render('inclure/selection.html.twig', ['objet'=>$objet,'tab_selections'=> pref('selection.'.$objet)]);
            $args_rep['vars']['declencheur_js'] = 'rafraichirSelection';


        }

    }

    return reponse_formulaire($form, $args_rep, 'inclure/form.html.twig');

}


function action_preferences_supprimer_selection()
{
    global $app;
    $request = $app['request'];
    $objet = $request->get('objet');
    $id = $request->get('id');
    $selection = pref('selection.' . $objet);
    unset($selection[$id]);
    ecrire_preference('selection.' . $objet, $selection);
    initUserPreference(suc('operateur.id'));
    $tab_result = array('ok' => true);
    return $app->json($tab_result);
}
