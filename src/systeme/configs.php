<?php

use Symfony\Component\Validator\Constraints as Assert;

include($app['basepath'] . '/src/inc/variables_config.php');
include($app['basepath'] . '/src/inc/fonctions_formbuilder.php');

function configs()
{

    global $app;
    $tab_entree = variables_config();
    $choix = array();
    foreach ($tab_entree as $k => $entree) {
        if (!isset($entree['systeme'])) {
            $data = conf($k);
            $form =  transforme_en_formulaire($k, $entree['variables'],$data,'config_enregistrement_result_form',100);
            $choix[$k] = $form->createView();
        }
    }
    $args_twig['choix'] = $choix;
    return $app['twig']->render(fichier_twig(), $args_twig);
}

function config_enregistrement_result_form($nom,$data){

    $objet_data = ConfigQuery::create()->filterByNom($nom)->findOne();
    if (!$objet_data){
        $objet_data=new Config();
        $objet_data->setnom($nom);
    }
    $objet_data->setValeur($data);
    $objet_data->save();

}