<?php

use Symfony\Component\Filesystem\Filesystem;
use Symfony\Component\Finder\Finder;


function memoire(){
    global $app;

    ob_start()
    ?>
    <div id="memoire">
        <div class="col-md-4">

            <h1>Table</h1>
            <div class="box">
                <?php   arbre(sac('table'));  ?>
            </div>

            <h1>Description</h1>
            <div class="box">
                <?php   arbre(descr()); ?>
            </div>

            <h1>Pages</h1>
            <div class="box">
                <?php    arbre(sac('pages'));  ?>
            </div>
        </div>
        <div class="col-md-4">

            <h1>Variables Utilisateur</h1>
            <div class="box">
            <?php arbre(suc());; ?>
            </div>

            <h1>Preference Utilisateur</h1>
            <div class="box">
                <?php arbre($app['session']->get('preference')); ?>
            </div>
            <h1>Preference Utilisateur par défaut</h1>
            <div class="box">
                <?php arbre(sac('preference')); ?>
            </div>
        </div>
        <div class="col-md-4">

                <h1>Config</h1>
                <div class="box">
                   <?php  arbre(sac('config')); ?>
                </div>

        </div>
    </div>

    <script>

        $('#memoire ul').treed();
        Cookies.set('name', 'value');
    </script>
<?php
    $content = ob_get_contents();
    ob_end_clean();
    return $app['twig']->render(fichier_twig(),array('content'=>$content ));
}


function action_memoire_vider_cache(){
    global $app;
    $cacheDir = $app['cache.path'];
    $finder = Finder::create()->in($cacheDir)->notName('.gitkeep');

    $filesystem = new Filesystem();
    $filesystem->remove($finder);
    $app['session']->getFlashBag()->add('success', 'Le cache est vide');
    return $app->redirect($app->path('memoire'));

}


function action_memoire_rafraichir(){
    global $app;
    $app['cache']->clear();
    require_once($app['basepath'].'/src/inc/config_init.php');
    config_maj();
    verifier_config();
    initUserPreference();
    $app['session']->getFlashBag()->add('success', 'Le rafraichissement des variables de cache a été effectué');
    return $app->redirect($app->path('memoire'));

}



