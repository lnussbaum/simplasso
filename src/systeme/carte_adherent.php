<?php

use mikehaertl\wkhtmlto\Pdf;
use Propel\Runtime\ActiveQuery\Criteria;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;


function carte_adherent()
{
    global $app;


    $args_rep = [];
    $request = $app['request'];
    $pref = pref('imprimante.carte_adherent');

    $id_log = $app['request']->get('id_log');
    $data = [
        'position_depart' => ($pref['position_depart'] + 0),
        'id_bloc' => $pref['id_bloc'],
    ];

    if ($id_log) {
        $objet = conf('carte_adherent.objet');
        list($where, $nb_carte) = getSelectionObjetNb($objet,['log_impcar' => $id_log]);
        $log = charger_objet('log',$id_log);
        $args_rep['date_log']=$log->getDateOperation();
    } else {
        list($where, $nb_carte) = carte_adherent_get_where();
    }

    $builder = $app['form.factory']->createNamedBuilder('bloc', CarteAdherentForm::class, $data);
    $builder->setRequired(false);
    formSetAction($builder, false,['id_log'=>$id_log]);


    $form = $builder->add('submit', SubmitType::class,
        array('label' => $app->trans('generer le PDF'), 'attr' => array('class' => 'btn-primary')))
        ->getForm();


    $form->handleRequest($request);
    if ($form->isSubmitted()) {
        $data = $form->getData();
        if ($form->isValid()) {
            $pref['position_depart'] = $data['position_depart'];
            $pref['id_bloc'] = $data['id_bloc'];
            ecrire_preference('imprimante.carte_adherent', $pref);
            $args_rep['url_redirect'] = $app->path('carte_adherent', [
                    'action' => 'imprimer',
                    'id_log' => $id_log
                ]
            );
        }
    }

    $args_rep['nb_carte'] = $nb_carte;
    $tab_log = LogQuery::create()->filterByCode('IMPCAR')->orderByDateOperation(Criteria::DESC)->find();
    $tab_impression = [];
    foreach ($tab_log as $log) {
        $ged = GedLienQuery::create()->filterByObjet('log')->filterByIdObjet($log->getPrimaryKey())->findOne();
        if ($ged) {
            $tab_impression[] = [
                'id_log' => $log->getIdLog(),
                'id_ged' => $ged->getIdGed(),
                'date' => $log->getDateOperation()
            ];
        }
    }
    $args_rep['tab_impression'] = $tab_impression;
    $args_rep['js_init'] = 'position_table';
    include_once($app['basepath'] . '/src/systeme/preferences.php');
    $args_rep['form_pref'] = construire_form_preference('imprimante.carte_adherent')->createView();
    return reponse_formulaire($form, $args_rep);

}


function action_carte_adherent_imprimer()
{
    global $app;
    $request = $app['request'];
    $id_log = $request->get('id_log');
    print_r(imprimer_carte_adherent($id_log));
    return '';
}

function carte_adherent_get_where()
{

    $where = "1=1";
    $valeur_supp = '';
    $pr_m = descr('membre.nom_sql');
    $pr_ind = descr('individu.nom_sql');
    $nb = 0;
    $objet = conf('carte_adherent.objet');
    $valeur_selection = ['mots' => [mot('carte_adh1')]];
    $colonnes = null;


    switch ($objet) {
        case 'membre' :
            list($sous_requete, $nb) = objet_selection_restriction_nb('membre', $valeur_selection, $colonnes);
            $where .= ' AND ' . $pr_m . '.id_membre IN(' . $sous_requete . ')';

            break;
        case 'individu' :
            list($sous_requete, $nb) = objet_selection_restriction_nb('individu', $valeur_selection, $colonnes);
            $where .= ' AND ' . $pr_ind . '.id_individu IN(' . $sous_requete . ')';
            break;
    }

    if ($valeur_supp) {
        list($from_ind, $where_supp) = IndividuQuery::getWhere($valeur_supp);
        $where .= ' AND ' . $where_supp;

    }

    return [$where, $nb];
}


function mm2px($val, $dpi = 123)
{

    return ($val * $dpi / 25.4);
}


function px2mm($val, $dpi = 123)
{

    return ($val * 25.4) / $dpi;
}


function imprimer_carte_adherent($id_log)//membre ou  individu
{
    global $app;
    include_once($app['basepath'] . '/src/inc/fonctions_ged.php');

// Chargement des paramètres de configuration
    global $app;
    $pref = pref('imprimante.carte_adherent');

    $pays_par_defaut = lire_config('pays');
    $id_bloc = $pref['id_bloc'];
    $position_depart = $pref['position_depart'];


    $pr_m = descr('membre.nom_sql');
    $pr_ind = descr('individu.nom_sql');


    $tab_champs_membre = [
        'id_membre',
        'id_titulaire' => 'id_individu_titulaire',
        'nom_membre' => 'nom',
        'civilite'

    ];
    foreach ($tab_champs_membre as &$champs_membre) {
        $champs_membre = $pr_m . '.' . $champs_membre . ' as membre_' . $champs_membre;
    }

    $tab_champs_individus = [
        'id_individu',
        'nom_famille',
        'prenom',
        'adresse',
        'codepostal',
        'ville',
        'pays'
    ];

    foreach ($tab_champs_individus as &$champs_individu) {
        $champs_individu = $pr_ind . '.' . $champs_individu . '  as membre_' . $champs_individu;
    }

    $objet = conf('carte_adherent.objet');
    $regeneration = false;


    if ($id_log) {
        $objetQuery = nom_query(conf('carte_adherent.objet'));
        list($from,$where) = $objetQuery::getWhere( ['log_impcar' => $id_log]);
        $regeneration = true;
    } else {
        list($where, $nb_carte) = carte_adherent_get_where();
    }


    switch ($objet) {
        case 'membre' :
            $order_by = descr('membre.nom_sql') . '.nom,nom_famille,prenom';
            break;
        case 'individu' :
            $order_by = 'nom_famille,prenom';
            break;
    }
    $order = (isset($pref_etiquette['classement'])) ? $pref_etiquette['classement'] : false;
    if ($order) {
        switch ($order) {
            case 1 :
                $order_by = 'codepostal , ville, ' . $order_by; // cp avant pour regrouper les villes du même sercteur
                break;
            case 2 :
                $order_by = ' ville, ' . $order_by;
                break;
        }
    }

    $select = 'SELECT distinct ' . $pr_ind . '.id_individu as id, ' . implode(',',
            $tab_champs_membre) . ',' . implode(',', $tab_champs_individus);
    $from = ' FROM asso_individus ' . $pr_ind . ', asso_membres ' . $pr_m;
    $where = ' WHERE ' . $pr_ind . '.id_individu=' . $pr_m . '.id_individu_titulaire AND ' . $where;


    $tab_carte = $app['db']->fetchAll($select . $from . $where . ' ORDER BY ' . $order_by);


    $nb_total = count($tab_carte);

    $loader = new Twig_Loader_Filesystem( $app['resources.path'] . '/documents');
    $twig_page = new Twig_Environment($loader,['autoescape'=>false]);

    $erreurs = array();
    if ($nb_total == 0) {
        return ('Aucune étiquette à imprimer');
    } else {


        $nb_colonne = max($pref['nb_colonne'], 1);
        $nb_ligne = $pref['nb_ligne'];
        $largeur_page = $pref['largeur_page'];
        $hauteur_page = $pref['hauteur_page'];
        $marge_haut_etiquette = $pref['marge_haut_etiquette'];
        $marge_gauche_etiquette = $pref['marge_gauche_etiquette'];
        $marge_droite_etiquette = $pref['marge_droite_etiquette'];
        $marge_haut_page = $pref['marge_haut_page'];
        $marge_bas_page = $pref['marge_bas_page'];
        $marge_gauche_page = $pref['marge_gauche_page'];
        $marge_droite_page = $pref['marge_droite_page'];
        $espace_etiquettesh = $pref['espace_etiquettesh'];
        $espace_etiquettesl = $pref['espace_etiquettesl'];
        $indice = 0;
        if (intval($position_depart) > 0) {
            $indice = intval($position_depart) - 1;
        }
        $tab_pays = tab('pays');

        // Calcul des dimensions des étiquettes
        $largeur_etiquette = ($largeur_page - $marge_gauche_page - $marge_droite_page - (($nb_colonne - 1) * $espace_etiquettesl)) / $nb_colonne;
        $hauteur_etiquette = ($hauteur_page - $marge_haut_page - $marge_bas_page - (($nb_ligne - 1) * $espace_etiquettesh)) / $nb_ligne;



        $bloc = BlocQuery::create()->findPk($id_bloc);
        $tab_load = ['modele' => $bloc->getTexte()];

        $loader = new Twig_Loader_Array($tab_load);
        $twig = new Twig_Environment($loader, ['autoescape' => false]);
        $html = '';
        $indice_page=0;

        foreach ($tab_carte as $k=>$carte) {
            $indice_colonne = $indice % $nb_colonne;
            $indice_ligne = floor($indice / $nb_colonne);
            $tab_id_prestation = array_keys(getPrestationDeType('cotisation'));
            $derniere_cotisation = ServicerenduQuery::create()->filterByIdPrestation($tab_id_prestation)->filterByIdMembre($carte['membre_id_membre'])->orderByDateFin(\Propel\Runtime\ActiveQuery\Criteria::DESC)->findOne();

            if ($derniere_cotisation) {
                $carte['cotisation_annee'] = $derniere_cotisation->getDateDebut()->format('Y');
                $carte['cotisation_date_fin'] = $derniere_cotisation->getDateFin()->format('d/m/Y');
            }
            $html_cellule = $twig->render('modele', $carte);
            $positionx = (($largeur_etiquette + $espace_etiquettesl) * $indice_colonne) ;
            $positiony = (($hauteur_etiquette + $espace_etiquettesh) * $indice_ligne);

            $html .= '<div class="cadre" style="position:absolute;
            height:' . ($hauteur_etiquette - $marge_haut_etiquette - $marge_haut_etiquette) . 'mm;
            width:' . ($largeur_etiquette - $marge_gauche_etiquette - $marge_droite_etiquette) . 'mm;
            left:' . $positionx . 'mm;
            top:' . $positiony . 'mm;
            padding-top:' . $marge_haut_etiquette . 'mm;
            padding-left:' . $marge_gauche_etiquette . 'mm;
            padding-right:' . $marge_droite_etiquette . 'mm;
            padding-bottom:' . $marge_haut_etiquette . 'mm">
            ' . $html_cellule . '
            </div>';
            $indice++;

            if ($indice >= ($nb_colonne * $nb_ligne) || (($k+1) == count($tab_carte) )) {
                $page_css=($indice_page > 0) ? 'nouvelle_page' : '';
                $pages[] = $twig_page->render('document.html.twig', ['content' => $html, 'page_css'=>$page_css ]);
                $indice = 0;
                $indice_page++;
                $html = '';
            }
        }


        $carte_adherent_objet = conf('carte_adherent.objet');

        if (!$regeneration) {
            if (conf('carte_adherent.etat_a_remettre')) {
                $etat = 2;
            } elseif (conf('carte_adherent.etat_a_envoyer')) {
                $etat = 3;
            } else {
                $etat = 4;
            }
            $mot1 = mot('carte_adh1');
            $mot = mot('carte_adh' . $etat);
            $tab_id = table_simplifier($tab_carte, $carte_adherent_objet . '_id_' . $carte_adherent_objet);
            MotLienQuery::create()->filterByIdMot($mot)->filterByIdObjet($tab_id)->filterByObjet($carte_adherent_objet)->delete();
            $req_mot = MotLienQuery::create();
            if ($mot1) {
                $req_mot->filterByIdMot($mot1);
            }
            $req_mot->filterByIdObjet($tab_id)->filterByObjet($carte_adherent_objet)->update(['IdMot' => $mot]);
            $id_log = Log::EnrOps('IMPCAR', '', [$carte_adherent_objet => $tab_id]);
        }



        $options = [
            'binary' => '/usr/bin/xvfb-run /usr/bin/wkhtmltopdf',
            'tmpDir' => $app['tmp.path'] . '/print',
            'margin-bottom' => $marge_bas_page,
            'margin-left' => $marge_gauche_page,
            'margin-right' => $marge_droite_page,
            'margin-top' => $marge_haut_page,
        ];


        $pdf = new Pdf($options);


        $css='body{font-size:10px;}';
        $css = $twig_page->render('document_style.css.twig',['css' => $css]);

          $options_page = [];
        foreach($pages as $page){
            $html = '<!DOCTYPE html><html><head><meta http-equiv="Content-Type" content="text/html; charset=UTF-8">'.$css.'</head><body>' . $page . '</body></html>';
            $pdf->addPage($html,$options_page);
        }



        $nom_fichier_pdf = $app['tmp.path'] . '/carte_adherent_' . date('Y-d-m-h-i') . '.pdf';
        if ($pdf->saveAs($nom_fichier_pdf)){
            ged_enregister($nom_fichier_pdf, ['log' => $id_log]);
            unlink($nom_fichier_pdf);
            $pdf->send();
            exit();
        }
        else {
            echo($html);
            echo($pdf->getError());

        }



    }

    return $erreurs;

}
