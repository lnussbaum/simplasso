<?php

use Symfony\Component\Form\Extension\Core\Type\FormType;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\FormError as FormError;
use Symfony\Component\Validator\Constraints as Assert;


function carte()
{
    global $app;
    $request = $app['request'];
    $objet = $request->get('objet');
    $selection = $request->get('selection');
    $tab_gis = PositionQuery::getAll($objet, $selection);
    return $app['twig']->render('carte.html.twig', array('objetxy' => $objet, 'tab_gis' => $tab_gis));
}


function action_carte_positionner()
{

    global $app;
    $request = $app['request'];
    $args_rep = [];
    $modification = false;
    $data = [];
    $objet = $request->get('objet');
    $id = $request->get('id');
    $gis = PositionQuery::create()
        ->usePositionLienQuery()
        ->filterByIdObjet($id)
        ->filterByObjet($objet)
        ->endUse()
        ->findOne();
    if ($gis) {
        $data['lon'] = $gis->getLon();
        $data['lat'] = $gis->getLat();
        $modification = true;
    }


    $builder = $app['form.factory']->createNamedBuilder('carte', FormType::class, $data);
    formSetAction($builder, $modification, ['objet' => $objet, 'id' => $id, 'action' => 'positionner']);

    $form = $builder
        ->add('lat', TextType::class,
            array('constraints' => new Assert\NotBlank(), 'attr' => array('placeholder' => '')))
        ->add('lon', TextType::class,
            array('constraints' => new Assert\NotBlank(), 'attr' => array('placeholder' => '')))
        ->add('submit', SubmitType::class,
            array('label' => $app->trans('Enregistrer'), 'attr' => array('class' => 'btn-primary')))
        ->getForm();

    $form->handleRequest($request);
    if ($form->isSubmitted()) {
        if ($form->isValid()) {
            $coord = $form->getData();
            creer_modifier_position_lien($coord['lon'], $coord['lat'], $objet, $id);
        }
    }

    return reponse_formulaire($form, $args_rep, 'carte_positionner.html.twig');


}
