<?php
function commune_liste(){
	global $app;

    return $app['twig']->render(fichier_twig());
}



function commune_colonnes(){

    $tab_colonne= array();
    $tab_colonne['id_commune'] = ['title'=> 'id',];
    $tab_colonne['pays'] = [];
    $tab_colonne['region'] = [];
    $tab_colonne['departement'] = [];
    $tab_colonne['code'] = [];
    $tab_colonne['arrondissement'] = [];
    $tab_colonne['canton'] = [];
    $tab_colonne['type_charniere'] = [];
    $tab_colonne['article'] = [];
    $tab_colonne['nom'] = [];
    $tab_colonne['lon'] = [];
    $tab_colonne['lat'] = [];
    $tab_colonne['zoom'] = [];
    $tab_colonne['elevation'] = [];
    $tab_colonne['elevation_moyenne'] = [];
    $tab_colonne['population'] = [];
    $tab_colonne['autre_nom'] = [];
    $tab_colonne['url'] = [];
    $tab_colonne['zonage'] = [];
    $tab_colonne['created_at'] = [];

    $tab_colonne['action']=["orderable"=> false];
    $tab_colonne = datatable_complete_liste_colonne($tab_colonne);

    return $tab_colonne;

}





function action_commune_liste_dataliste()
{

    return objet_liste_dataliste();
}

function action_commune_liste_rechercher()
{
    return objet_liste_rechercher();
}
