<?php
function zone_liste()
{
    global $app;
    $args_twig=[
        'tab_col' =>zone_colonnes(),
        ];
    return $app['twig']->render(fichier_twig(), $args_twig);

}


function zone_colonnes(){

    $tab_colonne= array();
	$tab_colonne['id_zone'] = ['title'=> 'id',];
	$tab_colonne['id_zonegroupe'] = ['title'=> 'groupe',"traitement" => 'transformeIdZonegroupe'];
	$tab_colonne['nom'] = [];
	$tab_colonne['descriptif'] = [];
    $tab_colonne['action']=["orderable"=> false];
    $tab_colonne = datatable_complete_liste_colonne($tab_colonne);
    return $tab_colonne;

}





function action_zone_liste_dataliste()
{

    return objet_liste_dataliste();

}


