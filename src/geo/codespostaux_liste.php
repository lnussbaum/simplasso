<?php
function codespostaux_liste(){
	global $app;
    return $app['twig']->render(fichier_twig());
}





function action_codespostaux_liste_dataliste()
{
    return objet_liste_dataliste();
}


function action_codespostaux_liste_rechercher()
{


    global $app;

    $sous_requete = getSelectionObjet('codespostaux');


    $tab_data = array();

    $tab_id_codespostaux = $app['db']->fetchAll($sous_requete. ' LIMIT 0,10' );
    foreach ($tab_id_codespostaux as &$result) {
        $result = $result['id_codepostal'];
    }
    $tab_codespostaux = Codespostaux::getAll($tab_id_codespostaux);
    foreach ($tab_codespostaux as $codespostaux) {

        $tab_data[] = array(

            'code'=>$codespostaux->getCodepostal(),
            'nom'=>$codespostaux->getNom(),
            'nom_suite'=>$codespostaux->getNomSuite(),

        );

    }

    return $app->json($tab_data);

}
