<?php


use Symfony\Component\Form\FormError as FormError;
use Symfony\Component\Form\Extension\Core\Type\FormType;
use Symfony\Component\Validator\Constraints as Assert;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Propel\Runtime\Map\TableMap;

function zone_form()
{
    global $app;
    $args_rep = [];
    $request = $app['request'];
    $id = sac('id');
    if ($id) {
        $modification = true;
        $objet_data = charger_objet();
        $data = $objet_data->toArray(TableMap::TYPE_FIELDNAME);
    } else {
        $modification = false;
        $objet_data = array();
        $data = array();
    }
    $builder = $app['form.factory']->createNamedBuilder('zone',ZonesForm::class,$data);
    $builder->setRequired(false);
    formSetAction($builder,$modification);



    $form = $builder->add('submit',SubmitType::class,
                array('label' => $app->trans('Enregistrer'), 'attr' => array('class' => 'btn-primary')))
            ->getForm();


    $form->handleRequest($request);
    if ($form->isSubmitted()) {
        $data = $form->getData();
        if ($form->isValid()) {
            if (!$modification)
                $objet_data = new Zone();
            $objet_data->fromArray($data);
            $objet_data->save();
            $id = $objet_data->getPrimaryKey();
            include($app['basepath'] . '/src/geo/zone.php');
            zone_lier($id,'individu');
            $args_rep['id']=$id;
            chargement_table();
        }
    }
    $args_rep['js_init'] = 'zone_form';
    return reponse_formulaire($form,$args_rep);

}


function action_zone_form_supprimer(){
    return action_supprimer_une_instance();
}