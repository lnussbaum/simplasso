<?php

function zone(){
    global $app;
    $tab_id = ZoneLienQuery::create()->filterByObjet('individu')->filterByIdZone(sac('id'))->find()->toKeyValue('IdObjet','IdObjet');
    $args['tab_individu']= IndividuQuery::create()->findPks($tab_id);
    return $app['twig']->render(fichier_twig(),$args);
}


function action_zone_lier(){
    global $app;
    $request = $app['request'];
    $id_zone = $request->get('id_zone');
    $objet = $request->get('objet');
    zone_lier($id_zone,$objet);
    return $app->redirect($app->path('zone',['id_zone'=>$id_zone]));
}

function zone_lier($id_zone,$objet){
    global $app;
    $tab_id_objet = rechercherObjetsParZone($id_zone,$objet);
    ZoneLienQuery::create()->filterByIdZone($id_zone)->filterByObjet($objet)->delete();
    foreach($tab_id_objet as $id_objet){
        $zl = new ZoneLien();
        $zl->setIdObjet($id_objet);
        $zl->setIdZone($id_zone);
        $zl->setObjet($objet);
        $zl->save();
    }
    return true;
}