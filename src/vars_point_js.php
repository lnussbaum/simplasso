<?php

function vars_point_js(){
    global $app;
    $fichier_js=$app['tmp.path'].'/js_vars.js';
    if (file_exists($app['tmp.path'])){
        file_put_contents($fichier_js,$app['twig']->render('js/vars.js.twig'));
    }
    return $app->sendFile($fichier_js);

}
