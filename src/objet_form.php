<?php
function objet_form()
{
    global $app;
    $args_rep=[];
    $objet = 'prestation';
    $request = $app['request'];
    if (sac('id')) {
        $modification = true;
        $objet_data = charger_objet();
        $data = $objet_data->toArray(TableMap::TYPE_FIELDNAME);
    } else {
        $modification = false;
        $objet_data = array();
        $data = array();
    }
    $builder = $app['form.factory']->createNamedBuilder($objet, ObjetForm::class, $data);
    $builder->setRequired(false);
    formSetAction($builder, $modification);
//todo en test claude 15/07/2017
//    include($app['basepath'] . '/src/inc/fonctions_formbuilder.php');
//    builder_ajoute_champs($builder, $data, $data, 'restriction');


    $form = $builder->add('submit', SubmitType::class,
        array('label' => $app->trans('Enregistrer'), 'attr' => array('class' => 'btn-primary')))
        ->getForm();

    $form->handleRequest($request);
    if ($form->isSubmitted()) {
        $data = $form->getData();
        if ($form->isValid()) {
            if (!$modification) {
                $objet_data = new desc($objet . '.phpname');
            }
            $objet_data->fromArray($data);
            $objet_data->save();
            $args_rep['id'] = $objet_data->getPrimaryKey();
        }
    }
    return reponse_formulaire($form, $args_rep);

}



