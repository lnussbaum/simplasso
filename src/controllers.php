<?php


use JsonRPC\Server;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Validator\Constraints as Assert;


$app->before(function (Request $req) {
    global $app;
    $route = $req->get('_route');
    if ($app['user'] )
    {
        if (verification_config() ){
            $app['cache']->clear();
            require_once($app['basepath'].'/src/inc/config_init.php');
            config_maj();
            verifier_config();
        }
    }


});




$app->post('/apilogin', function(Request $request) use ($app){
    include('api.php');

    $server = new Server();
    $app->log(sprintf("Un utilisatteur avec l'IP '%s' se trouve dans apilogin.", $_SERVER['REMOTE_ADDR']));
    $server->allowHosts($app['api_allow_hosts']);
    $procedureHandler = $server->getProcedureHandler();
    $app->log("requete. : ". $request->getUri());
    $procedureHandler->withClassAndMethod('login', 'SimplassoApi');
    $procedureHandler->withClassAndMethod('activation_adherent', 'SimplassoApi');
    $procedureHandler->withClassAndMethod('activation_adherent_test', 'SimplassoApi');
    $procedureHandler->withClassAndMethod('activation_adherent_mot_de_passe', 'SimplassoApi');
    return $server->execute();

});


$app->post('/apiservice', function(Request $request) use ($app){
    include('apiservice.php');
    $server = new Server();
    $server->allowHosts($app['api_allow_hosts']);
    $procedureHandler = $server->getProcedureHandler();
    $procedureHandler->withObject('SimplassoApiService');
    return $server->execute();

});


$app->post('/api/', function() use ($app){

    $app->log(sprintf("Un utilisatteur avec l'IP '%s' se trouve dans api.", $_SERVER['REMOTE_ADDR']));
    include('api.php');
    $server = new Server();
    $server->allowHosts($app['api_allow_hosts']);
    $procedureHandler = $server->getProcedureHandler();
    $procedureHandler->withObject('SimplassoApi');
    return $server->execute();

});






$app->error(function (\Exception $e, $code) use ($app) {
    if ($app['debug']) { $message = $code; return; }
    switch ($code) {
        case "404":
            $message = 'La page demandée n\'a pu être trouvée.';
            break;
        default:
            $message = 'Oups...c\'est embarrassant, une erreur s\'est produite';
    }
    return new Response($message);
});

$app->match('/erreur_403', function () use ($app) {
    include('erreur_403.php');
    return erreur_403();
})->bind('erreur_403');

$app->match('/erreur_404', function () use ($app) {
    include('erreur_404.php');
    return erreur_404();
})->bind('erreur_404');



$app->match('/', function () use ($app) {

    init_sac();
    require_once($app['basepath'].'/src/inc/fonctions_twig.php');
    twig_extension();
    include('accueil.php');
    menu();
    return accueil();
})->bind('homepage');



$app->match('/login', function (Request $request) use ($app) {

    include('login.php');
    $action = $request->get('action');
    $code_php = 'login';
    if (!empty($action)) {
        $code_php = 'action_login_' . $action;
    }
    return $code_php($request);
})->bind('login');


$app->get('/logout', function () use ($app) {$app['session']->clear();})->bind('user.logout');




$pages = getListePages();

foreach ($pages as $view ) {
    $app->match('/' . str_replace('_point_','.',$view), function () use ($app, $view) {

        init_sac();
        require_once($app['basepath'].'/src/inc/fonctions_twig.php');
        twig_extension();
        if (!pageAutorisee()) {
            return $app->redirect('erreur_403');
        }

        menu();

        $code_php = sac('fonction_php');
        include(fichier_php());

        if ($action = $app['request']->get('action')) {
            $code_php = 'action_' . $code_php . '_' . $action;
        }

        return $code_php();
    })->bind($view);
}
return $app;
