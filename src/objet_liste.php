<?php
function objet_liste()
{
    global $app;
    $args_twig=[
        'tab_col'=>objet_colonnes()
    ];
    return $app['twig']->render(fichier_twig(), $args_twig);

}


function objet_colonnes()
{
    global $app;
    require_once($app['basepath'] . '/src/inc/fonctions_bdd.php');//pour description des objets
    $objet = sac('objet');
    $tab_champs = donne_liste_colonne(descr($objet . '.table_sql'));
    $tab_colonne = array();
    foreach ($tab_champs as $k =>$v) {
        $tab_colonne[$k] = ['title' => $k];
    }
    $tab_colonne['action']=["orderable"=> false];
    $tab_colonne = datatable_complete_liste_colonne($tab_colonne);

    return $tab_colonne;

}
