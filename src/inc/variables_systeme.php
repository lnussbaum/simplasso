<?php



function initialiserVariablesApplication()
{

    global $app;

    $app['name'] = 'Simplasso';
    $app['organisation'] = '';
    $app['version'] = '0.9.1';
    $app['bdd_version'] = '0.9.13';
    $app['config_version'] = '0.9.65';


    //Chemin
    $app['resources.path'] = $app['basepath'] . '/resources';
    $app['document.path'] = $app['basepath'] . '/resources/documents';
    $app['config.propel'] = $app['basepath'] . '/config/propel/config.php';
    $app['vendor.path'] = $app['basepath'] . '/vendor';
    $app['tmp.path'] = $app['basepath'] . '/tmp';
    $app['upload.path'] = $app['tmp.path'].'/upload';
    $app['output.path'] = $app['tmp.path'].'/output';
    $app['image.path'] =  $app['tmp.path'].'/image';
    $app['log.path'] =  $app['tmp.path'].'/log';
    // Cache
    $app['cache.path'] =  $app['tmp.path'].'/cache';
    $app['cache.driver'] = array('driver'=>'file','cache_dir'=>$app['cache.path'].'/cache' );

    // Http cache
    $app['http_cache.cache_dir'] = $app['cache.path'] . '/http';
    // Twig cache
    $app['twig.options.cache'] = $app['cache.path'] . '/twig';
    $app['twig.options.cache_tache'] = $app['cache.path'] . '/twig_tache';

    //Session
    $app['session.variable.name'] = 'SIMPLASSO';

    //Securite
    $app['securite.cookie.lifetime'] = 31536000;
    $app['securite.cookie.lifetime_jwt'] = 600;
    $app['securite.cookie.secret_key'] = 'A MODIFIER dans votre fichier prod.php CleTrésCompliqueavecMAJUSCULES et des ? _ " - entre autres';

    //Local
    $app['locale'] = 'fr';
    $app['locale_sys'] = 'fr';
    $app['date.timezone'] = 'Europe/Paris';
    $app['session.default_locale'] = $app['locale'];
    $app['translator.messages'] = array(
        $app['locale'] => [$app['basepath'] . '/resources/locales/' . $app['locale_sys'] . '.yml'],
    );


    // Swiftmailer
    $app['swiftmailer.use_spool'] = false;
    $app['swiftmailer.options'] = array(
        'host' => 'smtp.numericable.fr',
        'port' => 25
    );


    $app['email'] = true;
    $app['email_redirect'] = '';
    $app['email_copie'] = '';
    $app['email_from'] = '';
    $app['email_prefixe_sujet'] = '[Simplasso] ';


    // Doctrine (db)
    $app['db.options'] = array(
        'new' => array(
            'driver' => 'pdo_mysql',
            'host' => 'localhost',
            'dbname' => 'simplasso',
            'user' => 'mr_simplasso',
            'password' => 'secret',
            'charset' => 'utf8'
        )
    );


    // Documentation
    $app['documentation.url']='http://simplasso.org/';
    $app['documentation.accueil']='-Documentation-';
    $app['simplasso.website']='http://simplasso.org/';
    $app['simplasso.rss']='http://simplasso.org/spip.php?page=backend&id_rubrique=9';

    // Monolog
    // cf https://github.com/Seldaek/monolog/blob/master/doc/01-usage.md#log-levels
    $app['log.level']=300;


    // API
    $app['api_name'] = 'simplasso';
    $app['api_mot_de_passe'] = 'MotdePasseTresTresCompliqué';
    $app['api_allow_hosts'] = ['127.0.0.1'] ;



    // API Spip
    $app['apispip'] = false;
    $app['apispip_mailsubscriber'] = false;
    $app['apispip_url'] = 'http://url_du_site_spip.org/?action=simplasso_serveur';
    $app['apispip_login'] = 'simplasso';
    $app['apispip_mot_de_passe'] = 'MotdePasseTresTresCompliqué';


    //API Ovh
    $app['apiovh'] = false;
    $app['apiovh_applicationkey'] = 'Clé application fourni par OVH';
    $app['apiovh_applicationsecret'] = 'Clé secrete application fourni par OVH';
    $app['apiovh_endpoint'] = 'ovh-eu';
    $app['apiovh_consumer_key'] = 'identifiant-ovh';
    $app['apiovh_infolettre_domaine'] = 'domaine.org';




}






/*lire au lieu de get car getPages existe dans vendor*/

function liste_des_pages() {

    $tab_pages=array(
        'accueil' => array(
            'droits'=>array('all')
        ),
        'image' => array(
            'droits'=>array('all')
        ),
        'carte' => array(
            'droits'=>array('all')
        ),
        'recherche' => array(
            'droits'=>array('all')
        ),
        'download' => array(
            'droits'=>array('all')
        ),
        'upload' => array(
            'droits'=>array('all')
        ),
        'profil' => array(
            'droits'=>array('all')
        ),
        'statistique' => array(
            'droits'=>array('G1','A5')
        ),
        'preferences' => array(
            'source' => 'systeme/',
            'droits'=>array('G1','C1','A1')
        ),
        'configs' => array(
            'source' => 'systeme/',
            'droits'=>array('G4','C4','A4')
        ),

        'outils' => array(
            'droits'=>array('G4','C4','A4')
        ),

        'inspecteur' => array(
            'source' => 'systeme/',
            'droits'=>array('G4','C4','A4')
        ),
        'credits' => array(
            'droits'=>array('G','C','A')
        ),
        'etiquette' => array(
            'source' => 'systeme/',
            'droits'=>array('G','A')
        ),
        'carte_adherent' => array(
            'source' => 'systeme/',
            'droits'=>array('G','A')
        ),
        'rss' => array(
            'droits'=>array('G','C','A')
        ),
        'timeline' => array(
            'droits'=>array('G1','C1','A1')
        ),
        'vars_point_js' => array(
            'droits'=>array('all')
        ),
        'helloasso' => array(
            'droits'=>array('G4')
        ),
        'helloasso_form' => array(
            'droits'=>array('G4')
        ),
        'documents' => array(
            'droits'=>array('G','C','A')
        ),
        'motgroupes' => array(
            'source' => 'asso/',
            'droits'=>array('G','A')),
        'memoire' => array(
            'source' => 'systeme/',
            'droits'=>array('A5'))


    );
    return $tab_pages;

}


