<?php

use Propel\Runtime\ActiveQuery\Criteria;

include_once('variables_config.php');
include_once('variables_preference.php');

function config_epurer($tab){
$tab_result=array();
    if(is_array($tab)){
        if (isset($tab['type_champs'])){
            return $tab['valeur'];
        }
        else {
            foreach ($tab as $k => $t) {
                if (is_array($t)) {
                    $tab_result[$k] = config_epurer($t);
                } else {
                    $tab_result[$k] = $t;
                }
            }
        }
    }
    else
        return $tab;
 return $tab_result;

}



function config_maj()
{

    $config_initial = variables_config();
    $num_version_en_cours = lire_config('config_version');

    foreach ($config_initial as $nom => $v) {

        // Mise à jour des configurations générales
        $valeur = config_epurer($v['variables']);

        $config = ConfigQuery::create()->filterByNom($nom)
            ->filterByIdEntite(null)
            ->findOne();

        if (!isset($config)) { // création
            $config = new Config();
            $config->setValeur($valeur);
        } else { // modification
            $config_valeur = $config->getValeur();
            if (is_array($config_valeur) && is_array($valeur)) {
                $config->setValeur(table_merge($valeur, $config_valeur));
            } else {
                $config->setValeur($valeur);
            }
        }
        $config->setNom($nom);
        $config->setObservation('Configuration par défaut');
        $config->setIdEntite(null);
        $config->save();
        if (isset($v['parametrage_par_entite']) && $v['parametrage_par_entite']) {
            $tab_config_ent = ConfigQuery::create()->filterByNom($nom)
                ->filterByIdEntite(null, Criteria::ISNOTNULL)
                ->find();
            foreach ($tab_config_ent as $config_ent) {
                $config_valeur = $config_ent->getValeur();
                if (is_array($config_valeur) && is_array($valeur)) {
                    $config_ent->setValeur(table_merge($valeur, $config_valeur));
                } else {
                    $config_ent->setValeur($valeur);
                }
                $config->save();
            }
        }
    }


    $preference_initial = config_epurer(variables_preference());

    foreach ($preference_initial as $nom => $v) {


        // Mise à jour des preference utilisateur

        $valeur = $v['variables'];

        $pref = PreferenceQuery::create()->filterByNom($nom)
            ->filterByIdEntite(null)
            ->filterByIdIndividu(null)
            ->findOne();

        if (!isset($pref)) { // création
            $pref = new Preference();
            $pref->setValeur($valeur);
        } else { // modification
            $pref_valeur = $pref->getValeur();
            if (is_array($pref_valeur) && is_array($valeur)) {
                $pref->setValeur(table_merge($valeur, $pref_valeur));
            } else {
                $pref->setValeur($valeur);
            }
        }
        $pref->setNom($nom);
        $pref->setObservation('preference par défaut');
        $pref->setIdEntite(null);
        $pref->setIdIndividu(null);
        $pref->save();


        $tab_pref_ent = PreferenceQuery::create()->filterByNom($nom)
            ->filterByIdEntite(null, Criteria::ISNOTNULL)
            ->filterByIdIndividu(null, Criteria::ISNOTNULL)
            ->find();
        foreach ($tab_pref_ent as $pref_ent) {
            $pref_valeur = $pref_ent->getValeur();
            if (is_array($pref_valeur) && is_array($valeur)) {
                $pref_ent->setValeur(table_merge($valeur, $pref_valeur));
            } else {
                $pref_ent->setValeur($valeur);
            }
            $pref->save();
        }
    }


    // Procedure complémentaire à un changement de version
    $tab_config_maj = ['0.9.7'];
    foreach ($tab_config_maj as $n) {
        if (version_compare($n, $num_version_en_cours) >= 0) {
            $nom_fonction = 'config_maj_' . str_replace('.', '_', $n);
            if (function_exists($nom_fonction)) {
                $nom_fonction();
            }
        }
        $num_version_en_cours = $n;
    }

    // Modification du num de version de la config dans la base
    $config = ConfigQuery::create()->filterByNom('config_version')->findOne();
    $config->setvariables(json_encode(config_version()));
    $config->save();

    // rechargement des config en cache
    chargement_config();
    // rechargement des preferences par défaut  en cache
    chargement_preference();
    return true;

}


function config_maj_0_9_7()
{

    $tab_nom_a_changer = [
        'adhesion' => 'sr_adhesion',
        'cotisation' => 'sr_cotisation',
        'vente' => 'sr_vente',
        'abonnement' => 'sr_abonnement',
        'don' => 'sr_don',
        'perte' => 'sr_perte'
    ];
    foreach ($tab_nom_a_changer as $anc => $nouv) {
        ConfigQuery::create()->findByNom($nouv)->delete();
        $tab_config = ConfigQuery::create()->findByNom($anc);
        foreach ($tab_config as $config) {
            $config->setNom($nouv)->save();
        }
    }

}


function verifier_config()
{


}

