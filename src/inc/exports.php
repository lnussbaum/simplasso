<?php


use Symfony\Component\Validator\Constraints as Assert;





function export_tableur($nom_fichier, $tab_data, $format = 'csv') {

    switch ($format) {
        case 'csv':
            header('Content-type: application/vnd.ms-excel');
            header('Content-disposition: attachment; filename="' . $nom_fichier . '.csv"');
            foreach($tab_data as $ligne){
                echo( str_replace(array("\n", "\r", "\""), '', implode("\t", $ligne))).PHP_EOL;
            }
            break;
        case 'excel':

            include_once('../vendor/phpoffice/phpexcel/Classes/PHPExcel.php');
            include_once('../vendor/phpoffice/phpexcel/Classes/PHPExcel/IOFactory.php');
            include_once('../vendor/phpoffice/phpexcel/Classes/PHPExcel/Writer/Excel2007.php');
            $objPHPExcel = new PHPExcel();
            $objWriter = new PHPExcel_Writer_Excel2007($objPHPExcel);
            $objWriter->setOffice2003Compatibility(true);
            $objPHPExcel->setActiveSheetIndex(0)
                ->setCellValue('A1', 'Hello')
                ->setCellValue('B2', 'world!')
                ->setCellValue('C1', 'Hello')
                ->setCellValue('D2', 'world!');


            // Save Excel 2007 file
            header('Content-type: application/vnd.openxmlformats-officedocument.spreadsheetml.sheet');
            header('Content-Disposition:inline;filename='.$nom_fichier.'.xls');


            $objWriter->save('php://output');

            break;


    }
    exit();

}





function action_export($objet)
{
    global $app;

    $message = 0;
    $prefs = pref($objet . '.export');

    $args=args_export($objet,$prefs);

    //todo reporter les colonnes dans les options  a faire
    $tab_champs_individu = [
        'id_membre',
        'id_individu',
        'nom_famille',
        'prenom',
        'email',
        'naissance',
        'adresse',
        'codepostal',
        'ville',
        'pays',
        'telephone',
        'fax',
        'mobile',
        'profession',
        'observation',
        'sexe'
    ];
    $tab_champs = [descr($objet.'.cle_sql')];
    $tab_liaison=[];
    switch ($objet){
        case 'membre':{
            $tab_liaison[]='individu';
            $pr_ind=descr('individu.nom_sql');
            foreach ($tab_champs_individu as &$ci) {
                $ci = $pr_ind.'.' . $ci;
            }
            $tab_champs += $tab_champs_individu;
            break;
        }
        case 'individu': {
            $tab_champs += $tab_champs_individu;
        }
    }

    $sql = objet_selection_restriction($objet, $args,$tab_champs,'individu');

    $tab_prestation_annee = array();
    $nomcourt_prestation = table_simplifier(tab('prestation'),'nomcourt');
    //tab('prestation')

    $tab_prestation_type = getPrestationTypeActive();

    $tab_me_pr_an = null;
    switch ($objet) {
        case 'membre':

            foreach ($tab_prestation_type as $id_prestation_type => $prestation_type) {



                if (in_array($prestation_type,$prefs['type'])) {

                    $tab_id_prestation = array_keys(getPrestationDeType($prestation_type));
                    if (!empty($tab_id_prestation)) {
                        $sql_id_objet = objet_selection_restriction($objet, $args);
                        $args_sr['sql'] = 'id_membre IN ('.$sql_id_objet.')';
                        $args_sr['id_prestation'] = $tab_id_prestation;
                        if ($prefs['annee']){
                            $args_sr['date_debut']=[$prefs['amj_debut']->format('Y-m-d')];
                        }
                        $tab_cols_sr=['YEAR(date_debut) as annee','id_prestation','id_membre'];
                        $sql_sr = objet_selection_restriction('servicerendu', $args_sr,$tab_cols_sr);
                        $nom_type_prestation = getPrestationTypeSimple()[$id_prestation_type];
                        $tab_cplt_servicerendu = $app['db']->fetchAll($sql_sr);

                        //ordonne les service rendu
                        foreach ($tab_cplt_servicerendu as $sr) {
                            if (!isset($tab_prestation_annee[$nom_type_prestation][$sr['annee']])) {
                                $tab_prestation_annee[$nom_type_prestation][$sr['annee']] = $sr['annee'];
                            }
                            $tab_me_pr_an[$sr['id_membre']][$nom_type_prestation][$sr['annee']][$sr['id_prestation']] = $sr['id_prestation'];
                        }
                    }
                    $tab_cplt_servicerendu = $tab_me_pr_an;
                    foreach ($tab_prestation_annee as $k => $tab_annee) {
                        $tab_annee = array_values($tab_annee);
                        sort($tab_annee);
                        $premiere_annee = $tab_annee[0];
                        if ($premiere_annee == 0) {
                            $premiere_annee = $tab_annee[1];
                        }
                        $derniere_annee = $tab_annee[count($tab_annee) - 1];

                        $tab_temp = array();
                        for ($i = $premiere_annee; $i <= $derniere_annee; $i++) {
                            $tab_temp[] = $i;
                        }
                        $tab_prestation_annee[$k] = $tab_temp;

                    }

                }
                // Trier le tableau des années et completer avec les années vides
            }

            break;


    }

    $indice = 0;
    $pas_de_servicerendu = array();
    $tab_entete = ['id','nom'] + $tab_champs;
    foreach ($tab_prestation_annee as $prestation => $tab_annee) {
        foreach ($tab_annee as $annee) {
            $tab_entete[]  = $prestation . $annee;
            $pas_de_servicerendu[] .= '';
        }
    }
    if (isset($prefs['motgroupe1id']) && !empty($id_motgroupe)) {
        $tab_entete[]  = tab('motgroupe.' . $id_motgroupe);
    }


    $statement = $app['db']->executeQuery($sql);

    if ($statement) {
        $cle = descr($objet . '.cle_sql');
        while ($val = $statement->fetch()) {
            $tab_sr = array();
            if (isset($tab_cplt_servicerendu[$val[$cle]])) { // ajout les colonnes de service rendu

                foreach ($tab_prestation_annee as $ktype => $tab_annee) {

                    foreach ($tab_annee as $annee) {
                        if (isset($tab_cplt_servicerendu[$val[$cle]][$ktype][$annee])) {
                            $tab_id_prestation = $tab_cplt_servicerendu[$val[$cle]][$ktype][$annee];
                            foreach ($tab_id_prestation as &$id_prestation) {
                                $id_prestation = $nomcourt_prestation[$id_prestation];
                            }
                            $tab_sr[$ktype.' '.$annee] = implode(',', $tab_id_prestation);
                        } else {
                            $tab_sr[$ktype.' '.$annee] = '';
                        }
                    }
                }
            } else {
                $tab_sr = $pas_de_servicerendu;
            }
            if (isset($tab_mots[$val[$cle]])) { // ajout les colonnes groupe mots
                $tab_sr[] = $tab_mots[$cle];
            } else {
                $tab_sr[] = '';
            }


            $val += $tab_sr;
            // format de chaque ligne
            $tab_export[] = $val;

            $indice++;
        }
    }
    if ($indice == 0) {
        $message .= 'Aucun ' . $app->trans('adherent') . '<BR />';
        echo($message);
    } else {

        $tab_entete = array_keys($tab_export[0]);
        $tab_export = ['entete'=>$tab_entete] + $tab_export;
        export_tableur("export_adherent_" . time(), $tab_export);

       // echo($export);
    }
    exit();
}

function action_export_vcard($objet)
{
    global $app;
// todo uniquement nom prenom et email sans controle de présence inclus dans liste adresse de roundcube le 25/1/2017
    $message = 0;
    $prefs = pref($objet . '.export');
    $args=args_export($objet, $prefs );
    if (isset($args)) {
        switch ($objet) {
            case 'membre':
                $where = getSelectionObjet('membre',$args);
                break;
            case 'individu' :
                if (is_array($args)) {
                    $where = getSelectionObjet('individu',$args);
                } else {
                    $where = ' WHERE id_' . $objet . ' IN(' . $args . ')';
                }
                break;
        }
    } else {
        $where = '';
    }
    $tab_champs_auteurs = array(
        'id_individu',
        'nom_famille',
        'prenom',
        'email',
    );
    $tab_champs_individus_sql = array();
    foreach ($tab_champs_auteurs as $champs_auteur) {
        $tab_champs_individus_sql[] = 'a.' . $champs_auteur;
    }

    switch ($objet) {
        case 'membre':
            //Récupération de la liste de membre et des informations du titulaire
            $order_by = 'nom_famille,prenom';
            $select = 'SELECT distinct id_membre as id,am.nom as nom,' . implode(',', $tab_champs_individus_sql);
            if ($where) {
                $where = ' WHERE id_individu_titulaire=id_individu and id_membre IN(' . $where . ')';
            } else {
                $where = ' WHERE id_individu_titulaire=id_individu ';
            }
            $from = ' FROM asso_individus a, asso_membres am ';
            $order_by = 'nom_famille,prenom';

            break;
        case 'individu' :
            $select = 'SELECT distinct a.id_individu as id, " " as  nom,' . implode(',', $tab_champs_individus_sql);
            $from = ' FROM asso_individus  a ';
            $order_by = 'nom_famille,prenom';
            break;
    }
    $statement = $app['db']->executeQuery($select . $from . $where . ' ORDER BY ' . $order_by);


    $indice = 0;
    $export = '';
    if ($statement) {
        while ($val = $statement->fetch()) {
            $tab_sr = array();
            $export .= "BEGIN:VCARD" . PHP_EOL . "VERSION:3.0" . PHP_EOL .
                "N:" . $val['nom_famille'] . ";" . $val['prenom'] . ";;;" . PHP_EOL .
                "FN:" . $val['prenom'] . " " . $val['nom_famille'] . PHP_EOL .
                "EMAIL;TYPE=INTERNET;TYPE=HOME:" . $val['email'] . PHP_EOL .
                "CATEGORIES:2017-01-24-AD-Present" . PHP_EOL .
                "END:VCARD" . PHP_EOL;
            $indice++;
        }
    }
    if ($indice == 0) {
        $message .= 'Aucun ' . $app->trans('adherent') . '<BR />';
        echo($message);
    } else {
        header("Content-type: application/vnd.ms-excel");
        header("Content-disposition: attachment; filename=\"export_adherent_" . time() . ".csv\"");
        echo($export);
    }
    exit();
}




function fwrite_stream($fp, $string)
{
    for ($written = 0; $written < strlen($string); $written += $fwrite) {
        $fwrite = fwrite($fp, substr($string, $written));
        if ($fwrite === false) {
            return $fwrite;
        }
    }
    return $written;
}

function args_export($objet,$prefs){
    global $app;
    $args=[];
    if ($prefs['selection'] === 'courante') {
        $args = $app['session']->get('selection_courante_' . $objet);
    } elseif ($prefs['selection'] !== 'tous') {
        $pref_selection = pref('selection.' . $objet);
        foreach ($pref_selection as $k => $ps) {
            if ($ps['nom'] == $prefs['selection']) {
                $args = $ps['valeurs'];
            }
        }
    }
return $args;
}
