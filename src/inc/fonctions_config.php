<?php



/**
 * conf
 * @param $var
 * @return array|null|string
 */
function conf($var)
{
    return lire_config($var);
}


function lire_config($nom, $defaut = '')
{
    $val = null;
    if ($nom) {
        $val = sac('config.' . $nom);
    } else {
        $val = sac('config');
    }
    if ($val===null)
        return $defaut;

    return $val;
}



function chargement_config(){


    $tab_config= ConfigQuery::create()->find();
    $tab=array();
    $tab_entite=array();
    foreach($tab_config as $c){
        if ($c->getIdEntite())
            $tab_entite[$c->getIdEntite()][$c->getNom()]=$c->getValeur();
        else
            $tab[$c->getNom()]=$c->getValeur();
    }

    setContexte('config',$tab );
    setContexte('config_entite',$tab_entite );

}



/**Mise a jour ou création(absence) de l'enregistrement opérateur et/ou entite la création se refere à l'enregistrement maitre en cas d'absense
 * @param $nom sous la forme niveau0[|niveau1[|niveau2]]
 * @param $saisie valeur du dernier niveauX ou tableau complet du dernier niveauX
 *
 * @return bool vrai réussie faux échec todo reste à documenter les erreurs (methode et texte)
 * Exemple : impimante|etisuette et array('nb_colonne' => 3,'nb_ligne' => 9) change 2 valeurs
 * ou impimante|etisuette|nb_colonne et  3  Change 1 valeur
 * ou impimante|etisuette et array('nb_colonne' => 3,'depart' => array('numero'=>4,'autre'=>'test')) change 3 valeurs dont une sous valeur composée de uniquement 2 valeurs si il y en avait plus elle serait perdu
 * todo voir les valeurs ajoutées dans le maitre aprés la création de l'enregistrement spécifique
 */
function ecrire_config($nom, $value, $entite = false)
{
    if ($nom) {
        $tab_clef = explode('.', $nom);
        $config_query= ConfigQuery::create()->filterByNom($tab_clef[0]);
        if ($entite)
            $config_query->filterByIdEntite(pref('en_cours.id_entite'));
        else
            $config_query->filterByIdEntite(null);

        $config = $config_query->findOne();

        if (!$config) {//
            $config=new Config;
            if ($entite)
                $config_query->filterByIdEntite(pref('en_cours.id_entite'));
            else
                $config_query->filterByIdEntite(null);
        }
        $config_valeur=$config->getValeur();
        array_shift($tab_clef);
        $noeud = implode('.', $tab_clef);
        $config->setValeur( dessinerUneBranche($config_valeur,$noeud,$value));
        $config->save();
        return true;
    } else {
        return false;
    }
}



/**
 * est utilisé dans les fenetres modales saisie par l'utilisateur n'affiche que les valeurs terminales
 * Todo reste a ajouter les valeurs par défauts combo case a cocher actuellement zone de texte
 * @param $nom sous la forme niveau0[|niveau1[|niveau2]]
 * @param null $id_entite
 * @param int $id_operateur
 * @param bool|true $data_id pour récuperer l'id et nom les data data par défaut False = ID
 * @param int $id si on envoie l'id  les autres parametres sont ignorés
 * @return array c'est un tableau avec les valeurs utilisables par modal
 *
 * pour utilisation voir exemple dans Membre_liste.php et membre_liste.twing
 */
function formulaire_modif_config($clef, $id_operateur = null, $id_entite = null)
{
    include_once('fonctions_formbuilder.php');


}



/** est utiliser en enregistrement des modales utilisateur
 * et par ecrire_config : replissage dans la programmation des préférences par exemple
 *
 * @param $valeur tableau du champs Valeur d'une l'intance de l'objet confiq à modifier
 * @param $clef  la clef indique le point de départ des modifications.
 * @param $saisie si saisie est valeur la branche est modifié par la valeur qui peut avoir des sous valeurs
 *                          ou un tableau
 *                                   chaque valeur remplace la branche de la ligne
 *                                   les branches non reseignées ne sont pas affectées.
 */

function modifieConfigValeur($valeur, $clef, $saisie)
{

    $clef_z = array_shift($clef);
    if (count($clef)>0){

       $valeur[$clef_z]=modifieConfigValeur($valeur[$clef_z], $clef, $saisie);
    } else {
        if (is_array($valeur[$clef_z])) {
            foreach ($valeur[$clef_z] as $k => $v) {
                if (isset($saisie[$k]))
                    $valeur[$clef_z][$k] = $saisie[$k];

            }
        }
        else
        $valeur[$clef_z] = $saisie;

    }

    return $valeur;
}




// enregistre ou crée l'enregistrement modifié suivant la base et le niveau
function enregistre_config($objet_data,  $id_entite = null, $preference = false)
{
    $entite = null;
    $operateur = null;
    $nom = $objet_data->getNom();
    $data_m['niveau_prefs'] = $objet_data->getNiveauPrefs();
    if ($preference) {// ce n''est pas un générique

// faut il un nouvel enregistrement
        $ok = false;
        if ( $id_entite!= $objet_data->getIdEntite()) {
            $ok = true;
        }
        if ($ok) {
            $objet_data2 = ConfigQuery::create()->filterByNom($nom)
                ->filterByIdEntite($entite)
                ->findOne();
            if (isset($objet_data2)) {
                $objet_data2->setValeur($objet_data->getValeur());
            } else {
                $objet_data2 = $objet_data->copy();
                $objet_data2->setIdEntite($entite);
                $objet_data2->setIdIndividu($operateur);
            }
            $objet_data2->save();
        } else {
            $objet_data->save();
        }
    }
}

