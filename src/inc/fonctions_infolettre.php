<?php



function changer_inscription($email, $tab_id_liste, $nom = "",$unique=false)
{

    global $app;

    $tab_id_existant = InfolettreInscritQuery::create()->filterByEmail($email)->find()->toKeyValue('IdInfolettre',
        'IdInfolettre');
    $tab_id_suppression = array_diff($tab_id_existant, $tab_id_liste);
    $tab_id_ajout = array_diff($tab_id_liste, $tab_id_existant);
    if ($unique){
        $tab_id_suppression = array_intersect($tab_id_liste,$tab_id_suppression);
        $tab_id_ajout = array_intersect($tab_id_liste,$tab_id_ajout);
    }


    foreach ($tab_id_suppression as $id) {
        if ($id > 0) {
            if ($app['apispip']) {
                $app['API_spip']->newsletter_desinscription($id, $email);
            }
            if ($app['apiovh']) {
                $nom = tab('infolettre.' . $id)['nom'];
                try {
                    if ($app['API_ovh']->get('/email/domain/' . $app['apiovh_infolettre_domaine'] . '/mailingList/' . $nom . '/subscriber/' . $email)) {
                        $app['API_ovh']->delete('/email/domain/' . $app['apiovh_infolettre_domaine'] . '/mailingList/' . $nom . '/subscriber/' . $email);
                    }
                } catch (Exception $e) {
                    $app->log("Erreur dans l'inscription d'un email sur l'API ovh" . $e->getMessage());
                }
            }
            InfolettreInscritQuery::create()->filterByEmail($email)->findByIdInfolettre($id)->delete();
        }
    }


    foreach ($tab_id_ajout as $id) {
        if ($id > 0) {
            if ($app['apispip']) {
                $app['API_spip']->newsletter_inscription($id, $email, $nom);
            }
            if ($app['apiovh']) {
                $nom = tab('infolettre.' . $id)['nom'];
                if (!empty($nom)) {
                    try {
                        $app['API_ovh']->post('/email/domain/' . $app['apiovh_infolettre_domaine'] . '/mailingList/' . $nom . '/subscriber',
                            array('email' => $email));
                    } catch (Exception $e) {
                        $app->log("Erreur dans l'inscription d'un email sur l'API ovh" . $e->getMessage());
                    }
                } else {
                    $app->log("Impossible de trouver la liste sur l'API ovh" . $e->getMessage());
                }
            }
            $inscrit = new InfolettreInscrit();
            $inscrit->setEmail($email);
            $inscrit->setIdInfolettre($id);
            $inscrit->save();
        }
    }

    return true;
}
