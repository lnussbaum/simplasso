<?php

/**
 * date_periode
 * @param $date_debut
 * @param $date_fin
 * @return string
 */
function date_periode($date_debut, $date_fin)
{

    global $app;
    if (is_a($date_debut,'DateTime') && is_a($date_fin,'DateTime')){

    if ($date_debut->format('d/m') == '01/01' && $date_fin->format('d/m') == '31/12' && $date_debut->format('Y') == $date_fin->format('Y')) {
        return $date_debut->format('Y');
    } elseif ($date_debut->format('d/m') == '01/07' && $date_fin->format('d/m') == '30/06' && $date_debut->format('Y') == ($date_fin->format('Y') - 1)) {
        return $date_debut->format('Y') . '-' . $date_fin->format('Y');
    } elseif ($date_debut->format('d') == '01' && $date_fin->format('d') > 27 && $date_debut->format('Y') == $date_fin->format('Y')) {
        return $app->trans(strtolower($date_debut->format('F'))) . $app->trans(' à ') . $app->trans(strtolower($date_fin->format('F'))) . ' ' . $date_fin->format('Y');
    } elseif ($date_debut->format('d') == '01' && $date_fin->format('d') > 27) {
        return $app->trans('De ') . $date_debut->format('F Y') . $app->trans(' à ') . $date_fin->format('F Y');
    }

    return $app->trans('Du ') . $date_debut->format('d/m/Y') . $app->trans(' au ') . $date_fin->format('d/m/Y');
    }

}

function reconstituePeriode($tab_date_debut){


    $date_debut0 = array_shift($tab_date_debut);
    $date_debut0 = DateTime::createFromFormat('Y-m-d',$date_debut0);
    $inter = new DateInterval('P1D');
    $tab_periode=[];
    foreach($tab_date_debut as $k=>$date){
        $date_fin = DateTime::createFromFormat('Y-m-d',$date);
        $date_fin->sub($inter);
        $tab_periode['periode'.$k] = [
            'date' => $date_debut0,
            'nom' => date_periode($date_debut0,$date_fin)
            ];
        $date_debut0 = DateTime::createFromFormat('Y-m-d',$date);
    }
    return $tab_periode;
}

