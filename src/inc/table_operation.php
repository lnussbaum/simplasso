<?php

///////////////////////////////////////////////////////////////////////////////////
//// Fichier qui regroupe les fonctions pour travailler sur la super variable "table"


function chargement_table()
{

    global $app;

    $type_nom_id = descr('prestation.tab_alias');
    $tab_prestation_type = array_flip($type_nom_id);
    $tab_prestation = charger_table('prestation');


    foreach ($tab_prestation as &$prestation) {
        $data = array();
        if (substr_count($prestation['periodique'], ',') >= 5) {
            list($data['periodiquea'],
                $data['periodiqueb'],
                $data['duree'],
                $data['mois_debut'],
                $data['jour_debut'],
                $data['fin']
                ) = explode(',', $prestation['periodique']);
            $libelle = getListePeriodiqueA($data['periodiquea']);
            if ($data['periodiquea'] > 0) {
                $libelle .= ' durée de ' . $data['duree'] . ' ' . getListePeriodiqueB($data['periodiqueb']) .
                    ' debut le ' . $data['jour_debut'] . ' ' . strtolower(date("F",
                        mktime(0, 0, 0, $data['mois_debut'], $data['jour_debut'], 2000)));
            }
        }
        //les données sont sous la forme "membre" ou  "individu" ou "membre;individu"
        $tab_objet = array();
        $temp = explode(';', $prestation['objet_beneficiaire']);
        foreach ($temp as $ok) {
            $tab_objet[$ok] = $ok;
        }
        $prestation = array_merge($prestation, [
            'libelle' => $libelle,
            'objet_beneficiaire' => $tab_objet,
            'beneficiaire_nom' => $prestation['objet_beneficiaire'],
            'prix' => f_getTabPrix($prestation['id_prestation']),
            'unite' => f_getUnite($prestation['id_unite']),
            'prestation_type_nom' => $tab_prestation_type[$prestation['prestation_type']]
        ]);

        if ($prestation['id_tva'] > 0) {
            $tab_temp['tva'] = f_getTva($prestation['id_tva']);
        }


        //les 2 dernière pour permetre de ne plus lire l'objetr prestation et mettre calculdatedebut et fin dans fonction au liey de l'objet(this)
        //$prestation_type_choix[$temp->getPrestationType()][$temp->getIdPrestation()] = $temp->getNom();
    }


    foreach ($tab_prestation_type as $k => $prestation_type) {
        $tab = table_filtrer_valeur($tab_prestation, 'prestation_type', $k);
        $tva = false;
        foreach ($tab as $t) {
            if ($t['id_tva']) {
                $tva = true;
                break;
            }
        }
        $quantite = false;
        foreach ($tab as $t) {
            if ($t['quantite']) {
                $quantite = true;
                break;
            }
        }
        $unique = count($tab) == 1;
        $periode = true;
        $tab_objet = [];
        foreach ($tab as $t) {
            $tab_periodique = explode(',', $t['periodique']);
            if ($tab_periodique[0] != 1 || ($tab_periodique[1] == 'M' && $tab_periodique[2] < 6)) {
                $periode = false;
            }

            $tab_objet = array_unique($tab_objet + $t['objet_beneficiaire']);
        }
        $tab_prestation_type[$k] = [
            'nom' => $prestation_type,
            'tva' => $tva,
            'quantite' => $quantite,
            'unique' => $unique,
            'periode' => $periode,
            'objet_beneficiaire' => $tab_objet
        ];


    }



    $pays = (include($app['basepath'] . '/vendor/umpirsky/country-list/data/' . $app['locale_sys'] . '/country.php'));

    $tab_prestation_lot = charger_table('prestationslot');
    $tab_plp = charge_table_specif('asso_prestationslots_prestations');
    foreach ($tab_plp as &$plp) {
        $id_pl = $plp['id_prestationslot'];
        unset($plp['id_prestationslot']);
        unset($plp['created_at']);
        unset($plp['updated_at']);
        $tab_prestation_lot[$id_pl]['prestations'][$plp['id_prestation']] = $plp;
    }

    $tab_mot = charger_table('mot');
    $tab_motgroupe = charger_table('motgroupe');
    $tab_mot_arbre = [];
    $tab_mot_arbre_tmp = [];
    $tab_mot_filtre = [];

    foreach ($tab_mot as $k=>&$mot) {
        if (isset($tab_motgroupe[$mot['id_motgroupe']])){
            $mot['objet_en_lien'] = explode(';', $tab_motgroupe[$mot['id_motgroupe']]['objets_en_lien']);
            $mot['systeme'] = $tab_motgroupe[$mot['id_motgroupe']]['systeme'];
        }
        else{
            $mot['id_motgroupe']=0;
        }
        $tab_mot_arbre_tmp[$mot['id_motgroupe']][]=$mot['id_mot'];
    }

    foreach ($tab_motgroupe as $id_motgroupe => &$motgroupe) {
        $tab_objet = explode(';', $motgroupe['objets_en_lien']);
        $tab_tmp = (isset($tab_mot_arbre_tmp[$id_motgroupe]))?$tab_mot_arbre_tmp[$id_motgroupe]:[];
        $motgroupe['mots'] = $tab_tmp;
        $motgroupe['enfant'] = table_simplifier(table_filtrer_valeur($tab_motgroupe,'id_parent',$motgroupe['id_motgroupe']));
        $motgroupe['options'] = json_decode($motgroupe['options'], true);
        $motgroupe['objets_en_lien'] = explode(';', $motgroupe['objets_en_lien']);
        if (isset($motgroupe['options']) && is_array($motgroupe['options'])) {
            foreach ($motgroupe['options'] as $ol => $tab_filtre) {
                $tab_mot_filtre[$ol][$tab_filtre['nom']] = $tab_filtre;
            }
        }
        foreach ($tab_objet as $objet) {
            $tab_mot_arbre[$objet][$motgroupe['nom']] = array_flip(table_simplifier(table_filtrer_valeur($tab_mot,
                'id_motgroupe', $id_motgroupe)));
        }
    }

    $tab_restrictions = charger_table('restriction');
    foreach ($tab_restrictions as &$restrictions) {
        $restrictions['valeur'] = json_decode($restrictions['variables'], true);
        unset($restrictions['variables']);
    }

    $tab_composition = charger_table('composition');
    $tab_cb = charger_table('composition_bloc', ['table' => 'com_compositions_blocs', 'cle' => 'id_bloc']);
    foreach ($tab_composition as $k => &$composition) {
        $composition['blocs'] = table_simplifier(table_trier_par(table_filtrer_valeur($tab_cb, 'id_composition', $k),
            'ordre'), 'ordre');
    }
    $tab_autorisation = charger_table('autorisation');
    $temp=array();
    foreach ($tab_autorisation as $v) {
        $temp[$v['id_individu']] = $v['id_individu'];
    }
    foreach ($temp as $v) {
        $individu = IndividuQuery::create()->filterByPrimaryKey($v)->findOne();
        if ($individu) $tab_operateur[$v]=$individu->getNom();
    }
    setContexte('table', array(
            'entite' => charger_table('entite'),
            'mot' => $tab_mot,
            'mot_arbre' => $tab_mot_arbre,
            'mot_filtre' => $tab_mot_filtre,
            'motgroupe' => $tab_motgroupe,
            'prestation' => $tab_prestation,
            'prestation_type' => $tab_prestation_type,
            'prestation_type_calendrier' => getDateCalendrierPrestationType($tab_prestation,$tab_prestation_type),
            'composition' => $tab_composition,
            'bloc' => charger_table('bloc'),
            'infolettre' => charger_table('infolettre'),
            'tresor' => charger_table('tresor'),
            'activite' => charger_table('activite'),
            'compte' => charger_table('compte'),
            'journal' => charger_table('journal'),
            'poste' => charger_table('poste'),
            'tva' => charger_table('tva'),
            'unite' => charger_table('unite'),
            'restriction' => $tab_restrictions,
            'prestationslot' => $tab_prestation_lot,
            'zonegroupe' => charger_table('zonegroupe'),
            'zone' => charger_table('zone'),
            'operateur' => $tab_operateur,
            //'tresor_entite_choix'=>charge_table_specif('asso_tresors','id_entite ,id_tresor, nom','id_entite,nom'),
            //'choixtous'=> $prestation_type_gere,*/
            'pays' => $pays

        )

    );

}

function f_getTabPrix($id_prestation)
{
    $tab_prix = array();
    $prixs = PrixQuery::create()->filterByIdPrestation($id_prestation)->orderByDateFin()->find();
    foreach ($prixs as $prix) {
        $tab_prix[] = array(
            'date_fin' => $prix->getDateFin()->getTimestamp(),
            'montant' => $prix->getMontant()
        );
    }
    return $tab_prix;
}

function f_getTva($id)
{
    $d3000 = new DateTime('3000-12-31');
    $datefin = $d3000->getTimestamp();
    $d3000 = new DateTime('2013-12-31');
    $date2012 = $d3000->getTimestamp();
    if ($id == 0) {
        return array(0 => array('date_fin' => $datefin, 'taux' => 0));
    }
    $tab = array(
        0 => array('date_fin' => $date2012, 'taux' => 0.196),
        1 => array('date_fin' => $datefin, 'taux' => 0.2)
    );
//    $objet_datas = TvatauxQuery::create()->filterByIdTva($id)->orderByDateFin()->find();
//    foreach ($objet_datas as $k=>$objet_data) {
//        $tab[] = array(
//            'date_fin' => $objet_data->getDateFin()->getTimestamp(),
//            'taux' => $objet_data->getTaux()
//        );
//    }
    return $tab;
}

function f_getUnite($id = 0)
{
    $tab = array();
    $objet_data = UniteQuery::create()->findpk($id);
    $nbdecimale=($objet_data) ? $objet_data->getNombre() : 0;
    //ajout' en reprise ccfd du 2 octobre 2017 mysql version 7
    //la lecture id=0 ne renvoi plus l'enregistrement existant'
    return $tab['unitenbdecimale'] = $nbdecimale;
}

function charge_table_specif($table)
{
    global $app;
    $select = 'SELECT *  FROM ' . $table;
    return $app['db']->fetchAll($select);

}

//Renvoi une table id => lib pour choix d'un élément'
function charger_table($objet, $options = array())
{
    global $app;
    $tab_champs = (isset($options['choix_colonnes'])) ? $options['choix_colonnes'] : array('*');
    $champs = implode(',', $tab_champs);
    $table = (isset($options['table'])) ? $options['table'] : nom_table_sql($objet);
    $where = (isset($options['filtre'])) ? $options['filtre'] : '1=1 ';
    $orderby = ' ORDER BY ' . descr($objet . '.choix_ordre');
    if ($orderby == ' ORDER BY ') {
        $orderby = '';
    }

    $sql = 'SELECT DISTINCT ' . $champs . ' FROM ' . $table . ' WHERE ' . $where . $orderby;
    $res = $app['db']->fetchAll($sql);
    $choix = array();
    $cle = (isset($options['cle'])) ? $options['cle'] : descr($objet . '.cle_sql');
    if (count($tab_champs) <= 2 && $tab_champs[0] != '*') {
//    if (count($tab_champs)==1 && $tab_champs[0]!='*') {
        foreach ($res as $r) {
            $choix[$r[$cle] . ''] = $r['nom'];
        }
    } else {
        foreach ($res as $r) {
            $id = $r[$cle];
            $choix[$id . ''] = $r;
        }
    }

    return $choix;
}

function getSrPrestation($objet_beneficiaire, $id = null)
{
    $tab_result = array();
    $tab_defaut = array();
    $tab_choix_prestation = array();
    $ents = suc('entite');
    $tab_dernier_sr_par_entite_type = array();
    $tab_entites_correspondantes = array();
    // recherche des entités concernées par la ou les types de prestations et filtrage des prestations concernées
    foreach (getPrestationTypeActive() as $prestationType => $type_nom) {
        if ($prestationType == sac('alias_valeur') or sac('objet') == 'servicerendu') {//todo arevoir si toutes les entites
            foreach (getPrestationDeType($prestationType) as $id_prestation => $nom_p) {
                $id_entite = tab('prestation.' . $id_prestation . '.id_entite');
                if (in_array($id_entite, $ents)) {
                    if ($objet_beneficiaire == '' or in_array($objet_beneficiaire,
                            tab('prestation.' . $id_prestation . '.objet_beneficiaire'))
                    ) {
                        if (tab('prestation.' . $id_prestation . '.active')) {
                            $tab_entites_correspondantes[$id_entite] = tab('entite.' . $id_entite . '.nom');
                            $prestations[$id_entite][$prestationType][$id_prestation] = $id_prestation;
                        }
                    }
                }
            }
        }
    }
    // recherche de la date de fin pour le membre pour chaque entite afin de servir de base pour tous les sr du meme type de l'entite
    if (isset($prestations)) {
        foreach ($prestations as $e => $tab_type) {
            foreach ($tab_type as $t => $prs) {

                $dernier_service_rendu = ServicerenduQuery::create();
                if ($objet_beneficiaire == 'membre') {
                    $dernier_service_rendu->filterByIdMembre($id);
                } else {
                    $dernier_service_rendu->useServicerenduindividuQuery()
                        ->filterByIdIndividu($id)
                        ->endUse();
                }
                if ($t == 6) {
                    $dernier_service_rendu->filterBy('DateFin','3000-01-01',\Propel\Runtime\ActiveQuery\Criteria::NOT_EQUAL);
                }
                $dernier_service_rendu = $dernier_service_rendu->filterByIdPrestation($prs)
                    ->orderByDateFin("DESC")
                    ->findOne();

                if ($dernier_service_rendu) {
                    $datefin = $dernier_service_rendu->getDateFin();
                    if ($datefin) {
                        $date_debut = $datefin->add(new DateInterval('P1D'));
                    } else {
                        $date_debut = new DateTime();
                    }
                    $tab_defaut[$date_debut->format('Y-m-d')][$e][$t] = $dernier_service_rendu->getIdPrestation();

                } else {
                    $date_debut = null;
                }
                $tab_dernier_sr_par_entite_type[$e][$t]['date_debut'] = $date_debut;
                $tab_dernier_sr_par_entite_type[$e][$t]['id_prestation'] = $prs;

                foreach ($prs as $id_prs) {
                    $prestation = tab('prestation.' . $id_prs);
                    $e = $prestation['id_entite'];

                    $tab_choix_prestation[$t][$id_prs] = $prestation['nom'];
                    if ($t < 3 or $t == 6) {//adhesions cotisations abonnements
                        if ($id and isset($tab_dernier_sr_par_entite_type[$e][$t])) {
                            $date_debut = calculeDateDebut($tab_dernier_sr_par_entite_type[$e][$t]['date_debut'],
                                $prestation['retard_jours'], $prestation['periodique']);
                        } else {
                            $date_debut = calculeDateDebut();

                        }
                        $date_fin = calculeDateFin($date_debut, $prestation['periodique']);
                    } else {
                        $date_debut = new DateTime();
                        $date_fin = $date_debut;
                    }

                    $tab_result[$prestation['id_entite'] . ''][$id_prs] = array(
                        'nom' => $prestation['nom'],
                        'prestation_type' => $prestation['prestation_type'],
                        'objet_beneficiaire' => $prestation['objet_beneficiaire'],
                        'prix' => $prestation['prix'],
                        'id_tva' => $prestation['id_tva'],
                        'quantite' => $prestation['quantite'],
                        'id_unite' => $prestation['id_unite'],
                        'unite' => $prestation['unite'],
                        'date_debut' => $date_debut,
                        'date_fin' => $date_fin
                    );

                }

            }
        }
    }

    return array($tab_result, $tab_choix_prestation, $tab_defaut);
}






function getDateCalendrierPrestationType($tab_prestation_init,$tab_prestation_type_init){

    global $app;

    $tab_prestation_type=[];
    $tab_prestation_type_init = table_simplifier($tab_prestation_type_init);
    foreach ($tab_prestation_type_init as $k => $nom) {
        $tab_prestation = table_filtrer_valeur($tab_prestation_init,'prestation_type',$k);



        foreach($tab_prestation as $p=>$prestation){
            if(substr($prestation['periodique'],0,1)!=1)
                unset($tab_prestation[$p]);
        }



        if (!empty($tab_prestation)) {


            $from = ' FROM asso_servicerendus s,asso_prestations p';
            $where = ' WHERE p.id_prestation IN(' . implode(',',
                    array_keys($tab_prestation)) . ') and s.id_prestation=p.id_prestation';



            $select = 'SELECT distinct s.date_debut as datex';
            $select2 = 'SELECT distinct DATE_ADD(s.date_fin, INTERVAL 1 DAY) as datex';

            $sql_date = $select . $from . $where . ' UNION ' . $select2 . $from . $where;
            $from .= ',(' . $sql_date . ') as tab_date ';
            $where_cplt = '';
            if ($nom == 'adhesion') {

                $where_cplt .= ' AND s.origine is NULL';

            }
            $where .= ' AND s.date_debut <= tab_date.datex AND tab_date.datex <=  s.date_fin';
            // $where .= ' AND origine is NULL';

            $select = 'SELECT tab_date.datex as datex';
            $groupby = ' GROUP BY s.id_entite,datex';
            $sql = $select . $from . $where . $where_cplt . $groupby;

            $data = $app['db']->fetchAll($sql);

            if ($nom == 'adhesion') {

                $select = 'SELECT distinct s.date_debut as datex';
                $select2 = 'SELECT distinct DATE_ADD(s.date_fin, INTERVAL 1 DAY) as datex';
                $from = ' FROM asso_servicerendus s,asso_prestations p';
                $where = ' WHERE p.id_prestation IN(' . implode(',',
                        array_keys($tab_prestation)) . ') and s.id_prestation=p.id_prestation ';

                $sql_date = $select . $from . $where . ' UNION ' . $select2 . $from . $where;
                $from .= ',(' . $sql_date . ') as tab_date ';
                $where .= ' AND s.date_debut <= tab_date.datex';
                // $where .= ' AND origine is NULL';

                $select = 'SELECT tab_date.datex as datex';
                $groupby = ' GROUP BY s.id_entite,datex';
                $where_cplt = ' AND montant = -46';
                $sql = $select . $from . $where . $where_cplt . $groupby;
                $data_m = $app['db']->fetchAll($sql);

                $data_m = table_colonne_cle($data_m, 'datex');
                foreach ($data as $k => &$d) {
                    if (isset($data_m[$d['datex']])) {
                        $d['nb'] -= $data_m[$d['datex']]['nb'];
                        $d['montant'] += $data_m[$d['datex']]['montant'];
                    }
                }
            }
            $tab_prestation_type[$nom]= reconstituePeriode(table_simplifier($data, 'datex'));
        }

    }
    return $tab_prestation_type;
}



function calculeDateDebut($date_ori = null, $retard_jours = 0, $temp = '0,M,12,1,1,fin')
{
//    echo '<br>-origine periodique :'.$temp.'-- Retard jour :'.$retard_jours.' date origine :';
//    echo ($date_ori) ? $date_ori->format('d-m-Y') : 'null' ;
    // la date origine est J+1 de la derniere cotisation de l'entite ou la date du jour en cas d'absence.
    $date_jour = new DateTime();
    if ($date_ori == null) {
        $date_ori = $date_jour;
        $premiere = true;
    } else {
        $premiere = false;
    }
    $format1 = 'Y-m-d';
    $format2 = "";
    if ($date_ori <= $date_jour or $premiere) {
        //           echo ' date du jour inferieure à la date de la prochaine cotisation on ne modife rien '.date_format($date_jour, 'Y-m-d').'**<BR>';
        if (substr_count($temp, ',') < 3) {
            $temp = '0,M,12,1,1,fin';
        }
        list($periodique,
            $unite,
            $duree,
            $mois_debut,
            $jour_debut
            ) = explode(',', $temp);
        $jour_debut = str_pad($jour_debut, 2, '0', STR_PAD_LEFT);
        $mois_debut = str_pad($mois_debut, 2, '0', STR_PAD_LEFT);
//            echo '  jour_debut :' . $jour_debut . ' mois debut :' . $mois_debut . '  retard autorise :' . $retard_jours . '<BR>periodique :' . $periodique . ' :';
        switch ($periodique) {
            case 0: //pas de duree
                $duree_jour = 0;
                $format1 = 'Y';
                $format2 = '-01-01';
                break;
            case 1: //fixe
                $format1 = 'Y';
                $format2 = '-' . $mois_debut . '-' . $jour_debut;
                $duree_jour = 365;
                if ($unite == 'M') {
                    if (intval($mois_debut) == 0) {
                        $format1 = 'Y-m';
                        $format2 = '-' . $jour_debut;
                    } else {
                        $format1 = 'Y';
                        $format2 = '-' . $mois_debut . '-' . $jour_debut;
                    }
                    $duree_jour = 30.5 * $duree;
                } elseif ($unite == 'D') {
                    $format1 = 'Y-m';
                    $format2 = '-' . $jour_debut;
                    $duree_jour = $duree;
                }
                break;
            case 2: //glissant
                $duree_jour = 365;
                if ($unite == 'M') {
                    $duree_jour = 30.5 * $duree;
                } elseif ($unite == 'D') {
                    $duree_jour = $duree;
                }
                $format1 = 'Y-m-d';
                $format2 = '';
                break;
        }
        $format1 = $format1 . $format2;
        $format = $format1 . ' 00:00:00';
//            echo " format :" . $format . '  ajoute  :' . $ajout . ' Duree en jours :' . $duree_jour;
        if ($periodique > 0) {
            $ajout = 'P' . $duree . $unite;
//            echo " format :" . $format . '  ajoute  :' . $ajout . ' Duree en jours :' . $duree_jour;
            if ($date_ori == $date_jour) {
                $date_ori = DateTime::createFromFormat('Y-m-d H:i:s', $date_ori->format($format));
            }
            $retard_reel = $date_ori->diff($date_jour)->format('%a');
            If ($date_jour <= $date_ori) { // la différence est toujours positive mettre en négatif les date suppérieure quand il n'y a pas de service rendu antérieur pour cette prestation
                $retard_reel = -$retard_reel;
            }
//                echo '  difference entre origine et date debut  :' . $retard_reel . ' jours';
            $retard_modulo = ($retard_reel % $duree_jour);
            $retard_nbperiode = (int)($retard_reel / $duree_jour);
//                echo '<BR> retard autorise :' . $retard_jours . ' Nombre de periode de retard ' . $retard_nbperiode . '  modulo retard modulo  : ' . $retard_modulo;
            if ($retard_modulo >= $retard_jours) {
                $retard_nbperiode++;
            }
            //               echo '<BR> retard autorise :' . $retard_jours . ' Nombre de periode de retard ' . $retard_nbperiode . '  modulo retard modulo  : ' . $retard_modulo;
            if ($retard_nbperiode <= 0) {
                $date_debut = clone $date_ori;
            } else {
                $modif = 'P' . ($retard_nbperiode * $duree) . $unite;
                $date_debut = $date_ori->add(new DateInterval($modif));
//                    echo '  modification :' . $modif ;
            }
        } else {
            $date_debut = clone $date_jour;
        }
    } else {
        $date_debut = clone $date_ori;
    }
//    echo ' date calculée :'.$date_debut->format('d-m-Y');
    return $date_debut;
}

function calculeDateFin($date, $temp = '0,M,12,1,1,fin')
{
    $date_fin = clone $date;
    if (is_a($date_fin, 'DateTime')) {
        list($periodique,
            $unite,
            $duree,
            $mois_debut,
            $jour_debut,
            $truc
            ) = explode(',', $temp);
        if ($periodique == 1 || $periodique == 2) {
            $ajout = 'P' . $duree . $unite;
            $date_fin = $date_fin->add(new DateInterval($ajout))->sub(new DateInterval('P1D'));
        } else {
            $date_fin = new DateTime('3000-01-01');
        }
    }
    return $date_fin;
}

function calcule_DateDebut_origine_DateFin($date_ori, $temp = '0,M,12,1,1,fin')
{
    $date_jour = new DateTime();
    if ($date_ori == null) {
        return false;
    }
    $format1 = 'Y-m-d';
    $format2 = "";
    if (substr_count($temp, ',') < 3) {
        $temp = '0,M,12,1,1,fin';
    }
    list($periodique,
        $unite,
        $duree,
        $mois_debut,
        $jour_debut
        ) = explode(',', $temp);
    $jour_debut = str_pad($jour_debut, 2, '0', STR_PAD_LEFT);
    $mois_debut = str_pad($mois_debut, 2, '0', STR_PAD_LEFT);
    switch ($periodique) {
        case 0: //pas de duree
            $format1 = 'Y';
            $format2 = '-01-01';
            break;
        case 1: //fixe
            $format1 = 'Y';
            $format2 = '-' . $mois_debut . '-' . $jour_debut;
            if ($unite == 'M') {
                if (intval($mois_debut) == 0) {
                    $format1 = 'Y-m';
                    $format2 = '-' . $jour_debut;
                } else {
                    $format1 = 'Y';
                    $format2 = '-' . $mois_debut . '-' . $jour_debut;
                }
            } elseif ($unite == 'D') {
                $format1 = 'Y-m';
                $format2 = '-' . $jour_debut;
            }
            break;
        case 2: //glissant
            if ($unite == 'M') {
                $duree_jour = 30.5 * $duree;
            } elseif ($unite == 'D') {
            }
            $format1 = 'Y-m-d';
            $format2 = '';
            break;
    }
    $format1 = $format1 . $format2;
    $format = $format1 . ' 00:00:00';
    if ($periodique > 0) {
        $ajout = 'P' . $duree . $unite;
        $date_debut = $date_ori->sub(new DateInterval($ajout))->add(new DateInterval('P1D'));
    } else {
        $date_debut = clone $date_jour;
    }
//    echo ' date calculée :'.$date_debut->format('d-m-Y');
    return $date_debut;
}

function tab_prestation($type_nom)
{
    $ke = suc('entite');
    $tab_choix2 = array();
    foreach (tab('prestation') as $p => $prest) {
        if ($type_nom='' or $prest['prestation_type_nom'] == $type_nom) {
            if (in_array($prest['id_entite'], $ke)) {
                $tab_choix2[$p] = $prest['nom'];
            }
        }
    }
    return $tab_choix2;
}


function tresor_suivant_entite($entites)
{
    global $app;
    $tresors = array();
    foreach ($entites as $id_entite) {//prend en compte les entites autorisées
        //prendre en compte l'encaissement par l'operateur sur son mode de reglement
        if (getUserPreference($app['session']->get('preference'), 'paiement.tresor_tous_un')){
            if ($id_entite==pref('paiement.entite') ){
                foreach (table_simplifier(getTresorByIdEntite($id_entite)) as $id_tresor=>$nom) {
                    //le moyen de paiement sélectionné pour cet opérateur
                    if ($id_tresor==pref('paiement.tresor'))
                        $tresors[$id_entite] = array($id_tresor=>$nom);
                }
            }
        } else
            $tresors[$id_entite] = table_simplifier(getTresorByIdEntite($id_entite));
    };
    return $tresors;
}

function getTresorByIdEntite($id_entite)
{

    return table_filtrer_valeur(tab('tresor'), 'id_entite', $id_entite);
}


function getPrestationTypeSimple()
{

    return table_simplifier(tab('prestation_type'));
}


function getPrestationDeType($value)
{

    if (intval($value) > 0) {
        return table_filtrer_valeur(tab('prestation'), 'prestation_type', $value);
    } else {
        return table_filtrer_valeur(tab('prestation'), 'prestation_type_nom', $value);
    }
}


function getPrestationEntite($prestation_type = '', $id_entite = null)
{

    if ($id_entite === null) {
        $id_entite = suc('entite');
    }
    if ($prestation_type != '') {
        $tab_prestation = getPrestationDeType($prestation_type);
    } else {
        $tab_prestation = tab('prestation');
    }
    $tab_prestation = table_filtrer_valeur($tab_prestation, 'id_entite', $id_entite);
    return $tab_prestation;

}


function getPrestationTypeActive()
{
    $prestation_type_active = array();
    $prestation_type_gere = conf('module');
    $tab = getPrestationTypeSimple();
    foreach ($tab as $id => $nom) {
        $type[$id] = $nom;
        if ($prestation_type_gere[$nom]) {
            $prestation_type_active[$id] = $nom;
        } else {
            $type_inactive[$id] = $nom;
        }
    }
    return $prestation_type_active;
}

function getPrestationTypeInactive()
{
    $prestation_type_inactive = array();
    $prestation_type_gere = conf_generique('module');
    $tab = getPrestationTypeSimple();
    foreach ($tab as $id => $nom) {
        $type[$id] = $nom;
        if ($prestation_type_gere[$nom] != 'oui') {
            $prestation_type_inactive[$id] = $nom;
        }
    }
    return $prestation_type_inactive;
}


function getEntitesNomDeLUtilisateur()
{
    return array_intersect_key(tab('entite'), array_flip(suc('entite')));
}

function getEntitesDeLUtilisateur()
{
    $tab = array_intersect_key(tab('entite'), array_flip(suc('entite')));
    foreach ($tab as $id => $v) {
        $tab_nom[$v['nom']] = $id;
    }
    return $tab_nom;
}


function donne_prix_date($prix, $date)
{
    $timestamp = $date->getTimestamp();
    $montant = 0;
    foreach ($prix as $p) {
        $montant = $p['montant'];
        if (intval($p['date_fin']) > $timestamp) {
            break;
        }
    }
    return $montant;
}


function tvaNom()
{
    foreach (tab('tva') as $id => $v) {
        $tab_nom[$v['nom']] = $id;
    }
    return $tab_nom;
}


function uniteNom()
{
    foreach (tab('unite') as $id => $v) {
        $tab_nom[$v['nom']] = $id;
    }
    return $tab_nom;
}


function charger_objet($objet=null,$id=null)
{
    $objet = ($objet) ? $objet : sac('objet');
    $id = ($id) ? $id : sac('id');
    $nomQuery = nom_query($objet);

    $result = $nomQuery::create()->findPk($id);
    return $result;
}


function charger_tab_objet($options = [])
{
    $objet = (isset($options['objet'])) ? $options['objet'] : sac('objet');
    $nomQuery = nom_query($objet);
    switch ($objet) {
        case 'prix':
        case 'poste':
        case 'entite':
        case 'membre':
        case 'motgroupe':
        case 'individu':
            if (isset($options['groupe'])) {

                switch ($options['groupe']) {
                    case 'autorisation':
                        $tab = $nomQuery::create()
                            ->useAutorisationQuery()
                                ->filterbyProfil($options['roles_profils'])
                                ->filterbyNiveau($options['roles_niveaux'])
                                ->filterByIdEntite(suc('entite'))
                                ->groupByIdIndividu()
                            ->endUse()
                            ->find();
                         break;
                    case 'entite':
                        $tab = $nomQuery::create()->filterByIdEntite($options['groupe'])->groupByidEntite()->find();
                        break;
                    default :
                        $tab = $nomQuery::create()->find();
                }
            } else {
                $tab = $nomQuery::create()->find();
            }

            // pas de filtre sur l'entite en cours car les enregistrements sont valables pour toutes les entites
            break;
        case 'servicepaiement':
            if (isset($options['id_filtre'])) {
                $tab = $nomQuery::create()->filterByIdPaiement($options['id_filtre'])->find();
            } else {
                $tab = $nomQuery::create()->find();
            }
            break;
        case 'config':
//        case 'autorisation':
            switch ($options['groupe']) {
                case 'individu':
                    $tab = $nomQuery::create()
                        ->filterbyprofil($options['roles_profils'])
                        ->filterbyniveau($options['roles_niveaux'])
                        ->filterbyidEntite(suc('entite'))
                        ->groupByIdIndividu()
                        ->find();
                    break;
                case 'entite':
                    $tab = $nomQuery::create()->filterByIdEntite($options['groupe'])->groupByidEntite()->find();
                    break;
                default:
                    $tab = $nomQuery::create()->find();
            }
            break;
        case 'import':
            //todo a parametrer
            $tab = $nomQuery::create()
                ->filterByAvancement(array(
                    '6valide',
                    '7compte_rendu',
                    '1copie',
                    '2charge',
                    '3controle',
                    '4modification',
                    '5ajout',
                    '0debut'
                ))
                ->orderBy('IdImport', 'desc')
                ->find();
            break;
        case 'sr_abonnement':
        case 'sr_vente':
        case 'sr_don':
        case 'sr_perte':
        case 'sr_cotisation':
        case 'sr_adhesion':
        case 'servicerendu':

            $tab = $nomQuery::getAll();


            break;
        case 'mot':
            if (isset($options['id_filtre'])) {
                $tab = $nomQuery::create()->filterIdMotgroupe($options['id_filtre'])->find();
            } else {
                $tab = $nomQuery::create()->find();
            }

            break;

        case 'tresor':
            if (isset($options['id_filtre'])) {
                $tab = $nomQuery::create()->filterByIdEntite($options['id_filtre'])->find();
            } else {
                $tab = $nomQuery::create()->find();
            }

            break;
        case 'prestation':

            $tab = PrestationQuery::create()->filterByPrestationType(array_keys(getPrestationTypeActive()))->find();
            break;
        default:
            if (isset($options['id_filtre'])) {
                $tab = $nomQuery::create()->filterByIdPaiement($options['id_filtre'])->find();
            } else {
                $tab = $nomQuery::create()->find();
            }

            break;
    }
    return $tab;

}

function charger_enfant($objet, $data_objet, $objet_enfant)
{
    $id = $data_objet->getPrimaryKey();
//    arbre($objet);arbre($data_objet);arbre($objet_enfant);arbre($id);
    $nomQuery = nom_query($objet_enfant);
    $tab = array();
    $ok = true;
    switch ($objet) {
        case 'config':
            switch ($objet_enfant) {
                case 'individu':
                    $tab = $nomQuery::create()->findByIdIndividu($id);
                    break;
                case 'entite':
                    $tab = $nomQuery::create()->findByidEntite($id);
                    break;
                default:
                    $tab = $nomQuery::create()->find();
                    $ok = false;
            }
            break;
        case 'poste':
            switch ($objet_enfant) {
                case 'compte':
                    $tab = $nomQuery::create()->findByidPoste($id);
                    break;
                default:
                    $ok = false;
            }
            break;
        case 'entite':

            switch ($objet_enfant) {
                case 'activite':
                case 'autorisation':
                case 'config':
                case 'compte':
                case 'ecriture':
                case 'journal':
                case 'piece':
                case 'prestation':
                case 'paiement':
                case 'relance':
                case 'RelancesMembres':
                case 'tresor':
                    $tab = $nomQuery::create()->filterByIdEntite(suc('entite'))->findByidEntite($id);
                    break;
                default:
                    $ok = false;
            }
            break;
        case 'prestation':
            switch ($objet_enfant) {
                case 'prix':
                    $tab = $nomQuery::create()->findByidPrestation($id);
                    break;
                case 'servicesrendu':
                    $tab = $nomQuery::create()->filterByIdEntite(array_keys(suc('entite')))->findByidPrestation($id);
                    break;
                default:
                    $ok = false;
            }
            break;
        case 'journal':
            switch ($objet_enfant) {
                case 'piece':
                    $tab = PieceQuery::create()->filterByIdEntite(array_keys(suc('entite')))->findByidJournal($id);
                    break;
                case 'ecriture':
                    $tab = EcritureQuery::create()->filterByIdEntite(array_keys(suc('entite')))->findByidJournal($id);
                    break;
                default:
                    $ok = false;
                    break;
            }
            break;
        case 'piece':
            switch ($objet_enfant) {
                case 'ecriture':
                    $tab = EcritureQuery::create()->filterByIdEntite(array_keys(suc('entite')))->findByidPiece($id);
                    break;
                default:
                    $ok = false;
                    break;
            }
            break;
        case 'compte':
            switch ($objet_enfant) {
                case 'ecriture':
                    $tab = EcritureQuery::create()->filterByIdEntite(array_keys(suc('entite')))->findByidCompte($id);
                    break;
                case 'piece':
                    //SELECT * FROM assc_piece pi INNER JOIN assc_ecriture ec ON pi.id_piece =ec.id_piece  WHERE ec.id_compte=175
                    //select * from assc_pieces where id_piece in (select distinct id_piece from assc_ecritures where id_compte=175)
                    $tab1 = EcritureQuery::create()->filterByIdEntite(array_keys(suc('entite')))
                        ->filterByIdCompte($id)
                        ->select('id_piece')
                        ->find();
                    $tab = PieceQuery::create()
                        ->filterByIdPiece($tab1->toArray())
                        ->orderBy('Pieces.date_piece', 'desc')
                        ->find();
                    break;
                default:
                    $ok = false;
            }
            break;
        case 'activite':
            switch ($objet_enfant) {
                case 'ecriture':
                    $tab = EcritureQuery::create()->filterByIdEntite(array_keys(suc('entite')))->findByidActivite($id);
                    break;
                case 'piece':
                    echo 'à faire ligne 630 les piéces des ecritures  controleurs_init';
                    break;
                default:
                    $ok = false;
            }
            break;
        case 'motgroupe':
            switch ($objet_enfant) {
                case 'mot':
                    $tab = MotQuery::create()->findByIdMotgroupe($id);
                    break;
                default:
                    $ok = false;
            }
            break;

        case 'servicerendu':
            switch ($objet_enfant) {
                case 'servicepaiement':
                    $tab = $nomQuery::create()->findByidServicerendu($id);
                    break;
                case 'paiement':
                    $tabx = ServicepaiementQuery::create()->filterByIdServicerendu($id)->find()->getPaiements();
                    $tab = $nomQuery::create()->findByidPaiement($tabx);
                    break;
                default:
                    $ok = false;
            }
            break;
        case 'relance':
        case 'convocation':
        case 'courrier':
            $tab = CourrierLienQuery::create()->findByIdCourrier($id);
            break;
        case 'paiement':
        case 'servicepaiement':
            $tab = $nomQuery::create()
                ->filterByIdEntite(suc('entite'))
                ->filterByObjet($objet)
                ->findByIdObjet($id);
            break;

        case 'relance':
        case 'relancesmembres':

            $tab = $nomQuery::create()
                ->filterByIdEntite(suc('entite'))
                ->findByIdMembre($id);
            break;
        case 'autorisation':
            //todo modofier (1==2)avec preference et niveau
            if (1 == 2) {
                $tab = $nomQuery::create()->findByIdIndividu($id);
            }
            break;
        case 'config':
            $tab = $nomQuery::create()->findByidConfig($id);
            break;
        case 'membre':
            $tab = $data_objet->getMembre();
            break;
        default:
            $ok = false;
    }

    if (!$ok) {
        echo 'ligne 389 pas de tableau enfant verifier le paramettrage <BR>';
        echo 'objet parent  :' . $objet . '--objet recherche :' . $objet_enfant . '  charge enfant <br>';
        exit;
    }
    return $tab;
}

// utiliser par service rendu structure conserver  toutes les case  enlevés a reprendre et supprimer de charger enfant
function charger_enfant_direct($objet_enfant)
{
    $id = sac('id');
    $objet = sac('objet');
    $nomQuery = nom_query($objet_enfant);
    $tab = array();
    $ok = true;


    switch ($objet) {


        case 'membre':
        case 'individu':
            switch ($objet_enfant) {
                case 'sr_abonnement':
                case 'sr_vente':
                case 'sr_don':
                case 'sr_perte':
                case 'sr_cotisation':
                case 'sr_adhesion':
                case 'servicerendu' :


                    $tab_id_prestation = array();
                    $tab_prestation = tab('prestation');
                    if ($objet_enfant != 'servicerendu') {
                        $prestation_type = descr($objet_enfant . '.alias_valeur');
                        $tab_prestation = table_filtrer_valeur($tab_prestation, 'prestation_type', $prestation_type);
                    }
                    $tab_prestation = table_filtrer_valeur($tab_prestation, 'prestation_type', $prestation_type);
                    foreach ($tab_prestation as $id_prest => $prest) {
                        if (in_array($objet, $prest['objet_beneficiaire']) != false) {
                            $tab_id_prestation[] = $id_prest;
                        }
                    }

                    $args = [
                        'id_prestation' => $tab_id_prestation,
                        'id_entite' => suc('entite')
                    ];
                    if ($objet == 'membre') {
                        $args ['id_membre'] = $id;
                    } else {
                        $args ['id_individu'] = $id;
                    }

                    list($tab_id, $nb) = objet_liste_dataliste_preselection('servicerendu', $args, false, false);

                    $tab = objet_liste_dataliste_selection('servicerendu', $tab_id);
                    break;

                case 'paiement':
                    $tab = $nomQuery::create()
                        ->filterByIdEntite(suc('entite'))
                        ->filterByObjet($objet)
                        ->findByIdObjet($id);
                    break;
                case 'autorisation':

                    $tab = $nomQuery::create()->findByIdIndividu($id);

                    break;

            }

            break;
        case 'sr_abonnement':
        case 'sr_vente':
        case 'sr_don':
        case 'sr_perte':
        case 'sr_cotisation':
        case 'sr_adhesion':
        case 'servicerendu' :
            switch ($objet_enfant) {

                case 'paiement':

                    $tabx = ServicepaiementQuery::create()->filterByIdServicerendu($id)->find()->toKeyValue('IdPaiement',
                        'IdPaiement');
                    $tab = PaiementQuery::create()->filterByIdPaiement($tabx)->find();

                    break;

            }

            break;

        case 'paiement':

            switch ($objet_enfant) {

                case 'sr_abonnement':
                case 'sr_vente':
                case 'sr_don':
                case 'sr_perte':
                case 'sr_cotisation':
                case 'sr_adhesion':
                case 'servicerendu' :

                    $tabx = ServicepaiementQuery::create()->filterByIdPaiement($id)->find()->toKeyValue('idServicerendu',
                        'idServicerendu');
                    $tab = $nomQuery::create()->findByidServicerendu($tabx);
                    break;
            }

            break;


    }
    if (!$ok) {
        echo 'ligne 500 pas de tableau enfant verifier le paramettrage <BR>';
        echo 'objet parent  :' . $objet . '--objet recherche :' . $objet_enfant . '  charge enfant <br>';
        exit;
    }
//    arbre($tab);
    return $tab;
}


function nom_propel($objet)
{
    $class = descr($objet . '.phpname');
    if (!class_exists($class)) {
        $err = ' pour l\'objet ' . $objet . '  fonction propel';
        $err .= ' Classe trouve :' . $class;
        echo $err;
        exit;
    }
    return $class;
}


function nom_query($objet)
{
    return descr($objet . '.phpname') . 'Query';

}