<?php

function activite_creation($data)
{
    $data['nomcourt'] = (isset($data['nomcourt'])) ? $data['nomcourt'] : lire_config('creation.activite.nomcourt');
    $data['nomcourt'] = ($data['nomcourt']>'!') ? $data['nomcourt'] : 'FG';
    $data['nom'] = ($data['nom']>'!') ? $data['nom'] : 'Frais généraux' ;

    $objet_data = ActiviteQuery::create()
        ->filterByIdEntite($data['id_entite'])
        ->findOneByNomcourt($data['nomcourt']);
    if (!$objet_data) {
        $objet_data = new Activite();
        $objet_data->fromArray($data);
        $objet_data->save();
        Log::EnrOp(false, 'activite',$objet_data->getPrimaryKey());
    }
    return $objet_data->getPrimaryKey();
}

function compte_creation($data,&$ncompte=null)//, $id_poste, $id_entite, $nom = '')
{

    if (!$ncompte) {
        $data['ncompte'] = (isset($data['ncompte'])) ? $data['ncompte'] : lire_config('creation.compte.bilan.ncompte');
        $data['ncompte'] = ($data['ncompte']>'1') ? $data['ncompte'] : '5301';
        $data['nomcourt'] = (isset($data['nomcourt'])) ? $data['nomcourt'] : lire_config('creation.compte.bilan.'.$data['ncompte'].'nomcourt');
        $data['nomcourt'] = ($data['nomcourt']>'!') ? $data['nomcourt'] : 'Caisse 1';
        $data['nom'] = (isset($data['nom'])) ? $data['nom'] : lire_config('creation.compte.bilan.'.$data['ncompte'].'nom');
        $data['nom'] = ($data['nom']<'!') ? $data['nomcourt'] : $data['nom'] ;
    }else $data['ncompte'] = $ncompte;
    $data['ncompte'] = ($data['ncompte']>'1') ? $data['ncompte'] : '5301';
        $objet_data = CompteQuery::create()
        ->filterByIdEntite($data['id_entite'])
        ->filterByNcompte($data['ncompte'])
        ->findone();
    if (!$objet_data) {
        $objet_data = new Compte();
        if (!isset($data['id_poste'])) {
            if ($data['ncompte'] >= '6') {
                $nomcourt_defaut_poste='BI';
                $nomcourt_defaut_poste = 'creation.poste.bilan';
            } else {
                $nomcourt_defaut_poste='GE';
                $nomcourt_defaut_poste = 'creation.poste.gestion';
            }
            $data['id_poste'] = poste_creation(array(), $nomcourt_defaut_poste);
         }
        $objet_data->fromArray($data);
        $ok = $objet_data->save();
        $ncompte=$objet_data->getPrimaryKey();
        Log::EnrOp(false, 'compte', $objet_data->getPrimaryKey());
    }
    return $objet_data->getPrimaryKey();
}

function entite_creation($data,$nom=null)
{
    global $app;
    if (!$nom) {
        $nom = (isset($data['nom'])) ?$data['nom'] : lire_config('creation.entite.nom');
        $nomcourt = (isset($data['nomcourt'])) ?$data['nomcourt'] : lire_config('creation.entite.nomcourt');
        if (!$nom) {
            $nom = (isset($data['nom'])) ? $data['nom'] :"SA Test" ;
            $nomcourt = (isset($data['nomcourt'])) ? $data['nomcourt'] : 'test';
        }
    }else
        $nomcourt = (isset($data['nomcourt'])) ?$data['nomcourt'] :substr($nom,0,6) ;

    $objet_data = EntiteQuery::create()
        ->filterByNom($nom)
        ->findone();
    if (!$objet_data) {
        $objet_data = EntiteQuery::create()
        ->filterByNomcourt($nomcourt)
        ->findone();
    };
    if (!$objet_data) {
         $objet_data = new Entite();
        $data = array(
            'nomcourt' => $nomcourt,
            'nom' => $nom,
            'observation' => 'Créé en reprise',
            'created_at' => date('Y-m-d H:i:s')
        );
        $objet_data->fromArray($data);
        $objet_data->save();
        $data['id_entite']= $objet_data->getPrimaryKey();
        $autorisation = new Autorisations();
         $data['id_individu'] =  suc('operateur.id');
         $data['profil'] = 'A';
         $data['niveau'] = '3';
        $autorisation->fromArray($data);
        $autorisation->save();
         $temp = "";
        $data['id_poste']=poste_creation(array('nomcourt'=>'BI','nom'=> 'Bilan'));
        $data['id_compte'] = compte_creation($data);
        activite_creation(array('nomcourt'=>'FG','id_entite'=> $data['id_entite']));
        journal_creation($data);
        tresor_creation($data);
    }
    return $objet_data->getIdEntite();
}

function journal_creation($data,$nomcourt=null)//, $id_compte, $id_entite)
{
     if (!$nomcourt) {
         $data['nomcourt'] = (isset($data['nomcourt'])) ?$data['nomcourt'] : lire_config('creation.journal.nomcourt');
        $data['nom'] = (isset($data['nom'])) ?$data['nom'] : lire_config('creation.journal.nom');
    }else{
         $data['nom'] = (isset($data['nom'])) ?$data['nom'] : lire_config('creation.journal.nom'); ;
        $data['nom'] = $data['nomcourt'];
    }
    $data['nomcourt'] =trim(substr($data['nomcourt'] ,0,6));
    $data['nom'] =trim(substr($data['nom'] ,0,25));
    $objet_data = JournalQuery::create()
        ->filterByIdEntite(intval($data['id_entite']))
        ->filterByNomcourt($data['nomcourt'])
        ->findone();
    if (!$objet_data) {
        if (!isset($data['id_compte'])) {
            $compte_contrepartie = lire_config('creation.compte.contre_partie');
            $data['ncompte']='512NEF';
            $data['id_compte'] = compte_creation($data,$data['ncompte']);
        }
        $objet_data = new Journal();
        $objet_data->fromArray($data);
        $objet_data->save();
        $id=$objet_data->getIdJournal();
         Log::EnrOp(false, 'journal',  $id);
    }else
        $id=$objet_data->getIdJournal();
    return $id;
}

function poste_creation($data, $nomcourt = null)
{
    if (!$nomcourt) {
        $data['nomcourt'] = (isset($data['nomcourt'])) ? $data['nomcourt'] : lire_config('creation.poste.nomcourt');
        $data['nomcourt'] = ($data['nomcourt']>'!') ? $data['nomcourt'] : 'BI';
        $data['nom'] = (isset($data['nom'])) ? $data['nom'] : lire_config('creation.poste.nom');
        $data['nom'] = ($data['nom']>'!') ? $data['nom'] : 'Bilan' ;
    }else
        $data['nomcourt'] = ($data['nomcourt']>'!') ? $data['nomcourt'] : 'BI';
    $objet_data = PosteQuery::create()->findOneByNomcourt($data['nomcourt']);
    if (!$objet_data) {
        $objet_data = new Poste();
        $objet_data->fromArray($data);
        $objet_data->save();
        Log::EnrOp(false, 'poste',  $objet_data->getPrimaryKey());
    }
    return $objet_data->getPrimaryKey();
}


function prestation_creation(
    $nom_groupe,
    $nom,
    $nomcourt,
    $prestation_type,
    $descriptif,
    $id_entite,
    $id_compte = 0,
    $periodique = '0,M,12,1,1,fin',
    $retard_jours = 0,
    $nombre_numero = 0,
    $prix_libre = 0
) {

    if (!$id_compte) {
        $id_compte = compte_creation(array('id_entite'=>$id_entite,'GE'));
    }
    $objet_data = new Prestation();
    $data = array(
        '$nom_groupe' => $nom_groupe,
        'nom' => $nomcourt,
        'nomcourt' => $nomcourt,
        'prestation_type' => $prestation_type,
        'descriptif' => $descriptif,
        'id_entite' => $id_entite,
        'periodique' => $periodique,
        'retard_jours' => $retard_jours,
        'nombre_numero' => $nombre_numero,
        'prix_libre' => $prix_libre,
        'sid_compte' => $id_compte,
        'created_at' => date('Y-m-d H:i:s')
    );
    $objet_data->fromArray($data);
    $objet_data->save();
    $id=$objet_data->getIdPrestation();
    Log::EnrOp(false, 'poste', $id);
    return $id;

}
function tresor_creation($data,$nom="")
{
    if (!$nom) {
        $nom = (isset($data['nom'])) ?$data['nom'] :'MP pour ' . $data['id_entite'] ;
        $nomcourt = (isset($data['nomcourt'])) ?$data['nomcourt'] :'MP ' . $data['id_entite'] ;
    }else
        $nomcourt = (isset($data['nomcourt'])) ?$data['nomcourt'] :substr($nom,0,2) ." ". $data['id_entite'] ;

    $data['nomcourt'] =substr($data['nomcourt'] ,0,8);
    $data['nom'] =substr($data['nom'] ,0,25);

    $data['iban'] = (isset($data['iban'])) ?$data['iban'] :'iban' ;
    $data['bic'] = (isset($data['bic'])) ?$data['bic'] :'bic' ;
    $data['observation'] = (isset($data['observation'])) ?$data['observation'] :'Créé ligne 230 fichier_création' ;
    $data['remise'] = (isset($data['remise'])) ?$data['remise'] :0 ;
    $data['iban'] =substr($data['iban'] ,0,27);
    $data['bic'] =substr($data['bic'] ,0,12);

    $objet_data = TresorQuery::create()->findOneByNom($nom);
    if (!$objet_data) {
        $objet_data = TresorQuery::create()
            ->findOneByNomcourt($nomcourt);
    }
    if (!$objet_data) {
        $objet_data = new Tresor();
        $objet_data->fromArray($data);
        $objet_data->save();
        $id=$objet_data->getPrimaryKey();
        Log::EnrOp(false, 'tresor', $id);
    }else{
        $id=$objet_data->getPrimaryKey();
    }
    return $id;
}
