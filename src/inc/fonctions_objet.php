<?php

function getSelectionObjet($objet=null,  $params = array(), $tab_cols = null,$liaison=null)
{
    if (!$objet) {
        $objet = sac('objet');
    }
    $nom_class = descr($objet . '.phpname') . 'Query';
    if (method_exists($nom_class,'getWhere')){
        list($from, $where) = $nom_class::getWhere($params);
    }else{
        list($from, $where) = getWhereObjet($objet, $params);
    }
    return getRequeteObjet($objet, $from, $where, $tab_cols,$liaison);
}


function getSelectionObjetNb($objet=null,  $params = array(), $tab_cols = null,$liaison=null)
{
    if (!$objet) {
        $objet = sac('objet');
    }
    $nom_class = descr($objet . '.phpname') . 'Query';
    if (method_exists($nom_class,'getWhere')){
        list($from, $where) = $nom_class::getWhere($params);
    }else{
        list($from, $where) = getWhereObjet($objet, $params);
    }
    if (property_exists($nom_class,'filtrage_entite')){
        if( $nom_class::$filtrage_entite){
            $where .= ' AND id_entite='.pref('en_cours.id_entite');
        }
    }
    return getRequeteObjetNb($objet, $from, $where, $tab_cols,$liaison);
}





function getRequeteObjet($objet, $from, $where, $tab_cols = null,$liaison=null)
{

    if (!empty($liaison)){
        $nom_class = descr($objet . '.phpname') . 'Query';
        if (!is_array($liaison)) $liaison=[$liaison];
        foreach($liaison as $l){
            $o = $nom_class::getTabLiaison($l);
            $pr = descr($objet . '.nom_sql');
            $prl = descr($o['objet'] . '.nom_sql');
            $from[$prl]= descr($o['objet'] . '.table_sql').'  '.$prl;
            $where .= ' AND '.$pr.'.'.$o['local'].'='.$prl.'.'.$o['foreign'];

        }

    }

    if (empty($tab_cols)) {
        $pr = descr($objet . '.nom_sql');
        $cle = descr($objet . '.cle_sql');
        $tab_cols = $pr . '.' . $cle;
    }elseif(is_array($tab_cols)){
        $tab_cols = implode(',',$tab_cols);
    }

    return 'SELECT ' . $tab_cols . ' FROM ' . implode(',', array_values($from)). ' WHERE ' . $where;
}



function getRequeteObjetNb($objet, $from, $where, $tab_cols = null,$liaison=null)
{
    global $app;
    $select = getRequeteObjet($objet, $from, $where, $tab_cols,$liaison);
    $nom_class = descr($objet . '.phpname') . 'Query';
    if (property_exists($nom_class,'filtrage_entite')){
        if( $nom_class::$filtrage_entite){
            $where .= ' AND id_entite='.pref('en_cours.id_entite');
        }
    }
    $select_count = 'SELECT COUNT(*) as nb_total FROM ' . implode(',',$from) . ' WHERE ' . $where;
    return [$select, $app['db']->fetchColumn($select_count)];
}






function getWhereObjet($objet,  $params = array())
{

    $where = "1=1";
    $pr = descr($objet . '.nom_sql');
    $cle = descr($objet . '.cle_sql');
    $tables = array($pr => descr($objet . '.table_sql'));
    $nom_class_query = descr($objet . '.phpname') . 'Query';
    $champs_recherche = (property_exists($nom_class_query,'champs_recherche'))?$nom_class_query::$champs_recherche:['nom'];

    if (!empty($champs_recherche)) {
        // La recherche
        if (($search = request_ou_options('search', $params))) {

            $recherche = trim($search['value']);
            if (!empty($recherche)) {
                $colle = 'AND (';
                if (intval($recherche) > 0) {
                    $where = '(' . $cle . ' = \'' . $recherche . '\'';
                    $colle = 'OR';
                }
                $where .= ' ' . $colle . ' ( ' . $pr . '.' . implode(' like \'%' . $recherche . '%\' OR ' . $pr . '.',
                        $champs_recherche) . ' like \'%' . $recherche . '%\'))';
            }
        }
    }

    if ($mots = request_ou_options('mots', $params)) {
        if (!is_array($mots)) {
            $mots = array($mots);
        }
        $where .= ' and (select count(ml.id_mot) from asso_mots_liens ml where ml.id_mot IN (' . implode(',',
                $mots) . ') and ml.id_objet=' . $pr . '.' . $cle . ' and objet=\'' . $objet . '\')=' . count($mots);
    }

    $restreindre_id = request_ou_options('limitation_id', $params);

    if (!empty($restreindre_id)) {
        $restreindre_id = explode(',', $restreindre_id);
        $where .= ' and ( ';
        foreach ($restreindre_id as $key => $id) {
            if ($key != 0) {
                $where .= ' or ';
            }
            $ids = explode('-', $id);
            if (count($ids) > 1) {
                $where .= '(' . $pr . '.' . $cle . ' BETWEEN ' . $ids[0] . ' AND ' . $ids[1] . ')';
            } else {
                $where .= $pr . '.' . $cle . ' =' . $ids[0];
            }

        }
        $where .= ')';
    }

    foreach ($tables as $k => $t) {
        $from[$k] = $t . ' ' . $k;
    }
    return [$from, $where];
}




/**
 * getAllObjet
 * @param $objet
 * @param $tab_id
 * @param array $order
 * @param int $offset
 * @param int $limit
 * @return mixed
 */
function getAllObjet($objet, $tab_id, $order = array(), $offset = 0, $limit = 0)
{

    $objet_class_query = descr($objet . '.phpname') . 'Query';
    $q = $objet_class_query::create()->filterByPrimaryKeys($tab_id);

    if (!empty($order) && is_array($order)) {

        foreach ($order as $k => $o) {

            $q->orderBy($k, strtoupper($o));
        }

    }
    if ($offset > 0) {
        $q = $q->offset($offset);
    }
    if ($limit > 0) {
        $q = $q->limit($limit);
    }

    return $q->find();


}






function objet_liste_dataliste($objet = null)
{

    if (!$objet) {
        $objet = sac('objet');
    }
    list($tab_id, $nb_total) = objet_liste_dataliste_preselection($objet);
    $tab = objet_liste_dataliste_selection($objet, $tab_id,tri_dataliste());
    if (objet_liste_dataliste_detection_complement($objet)){
        $tab_data = objet_liste_dataliste_preparation($objet, $tab,false);
        $tab_data = objet_liste_dataliste_complement($objet, $tab,$tab_data);
    }
    else
    {
        $tab_data = objet_liste_dataliste_preparation($objet, $tab);
    }
    return objet_liste_dataliste_envoi($tab_data, $nb_total);

}

function  objet_selection_restriction_mot($objet) {
    $sql='';
    if (!$objet) return '';
    if (!conf('module.restriction')) return '';
    if (suc('restriction.concerne')) {
        $sql_ml=' ml.objet = \'membre\' AND ml.id_mot = '.suc('operateur.id').')';
        switch ($objet) {
            case 'membre' :
                $sql =' AND id_'.$objet.' IN  (select ml.id_objet  from asso_mots_liens ml  where '.$sql_ml ;
                break;
            case 'individu' :
                $sql =' AND id_'.$objet.' IN  (select mi.id_individu
                from asso_mots_liens ml , asso_membres_individus mi
                where ml.id_objet = mi.id_membre AND'.$sql_ml ;
                break;
            case 'paiement' :
                $sql=' AND CASE WHEN paiement.objet=\'membre\' THEN
                '.$objet.'.id_objet IN ( select ml.id_objet from asso_mots_liens ml
                where '.$sql_ml.' ELSE
               '.$objet.'.id_objet IN ( select mi.id_individu from asso_mots_liens ml , asso_membres_individus mi
                where ml.id_objet = mi.id_membre AND'.$sql_ml.' END';
               break;
            case 'servicerendu' :
                $sql=' AND CASE WHEN servicerendu.id_membre >0 THEN
                '.$objet.'.id_membre IN ( select ml.id_objet from asso_mots_liens ml
                where '.$sql_ml.' ELSE
                '.$objet.'.id_servicerendu IN (select sri.id_servicerendu
                 from asso_mots_liens ml , asso_membres_individus mi , asso_servicerendus_individus sri
                 where sri.id_individu=mi.id_individu and ml.id_objet = mi.id_membre AND'.$sql_ml.' END';
                 break;
            }
    }elseif(suc('restriction.concerne')!=1){
        //todo non traité le mode individu (par entite 1 restriction
    }
    return $sql;
}

function  objet_selection_restriction($objet = null, $args = array(),$tab_cols=null,$liaison=null){


    if (!$objet) {
        $objet = sac('objet');
    }
    $sql = getSelectionObjet($objet,$args,$tab_cols,$liaison);
    // Restriction
    $id_restriction = suc('restriction.'.pref('en_cours.id_entite'));
    if ( $id_restriction ){
        $args_restrictions = tab('restriction.'.$id_restriction.'.valeur.'.$objet);
        if(!empty($args_restrictions)){
            $sous_requete_restriction = getSelectionObjet($objet,$args_restrictions);
            $sql .= ' AND '.descr($objet . '.cle_sql'). ' IN ('.$sous_requete_restriction.')';
        }
    }
    return $sql;
}



function  objet_selection_restriction_nb($objet = null, $args = array(),$tab_cols=null){

    global $app;
    if (!$objet) {
        $objet = sac('objet');
    }
    list($sql, $nb_total) = getSelectionObjetNb($objet,$args,$tab_cols);

    // Restriction
    $id_restriction = suc('restriction.'.pref('en_cours.id_entite'));
    if ( $id_restriction && $nb_total > 0 ){
        $args_restrictions = tab('restriction.'.$id_restriction.'.valeur.'.$objet);
        if(!empty($args_restrictions)){
            $sous_requete_restriction = getSelectionObjet($objet,$args_restrictions);
            $sql = $sql .' AND '.descr($objet . '.cle_sql'). ' IN ('.$sous_requete_restriction.')';
            $select_count = 'SELECT COUNT(*) as nb_total '.substr($sql,strpos($sql,' FROM '));
            $nb_total =  $app['db']->fetchColumn($select_count);
        }
    }
    return [$sql,$nb_total];
}


function objet_liste_dataliste_preselection($objet = null, $args = array(),$tri=true,$limit=true)
{
    global $app;
    if (!$objet) {
        $objet = sac('objet');
    }
    list($sql, $nb_total) = objet_selection_restriction_nb($objet,$args);
    if ($tri){
        $tab_tri = tri_dataliste();
        $tab_tri_sql = tri_dataliste_sql(descr($objet . '.nom_sql'), $tab_tri);
        $sql .= $tab_tri_sql;
    }
    if ($limit){
        $start = request_ou_options('start');
        $length = request_ou_options('length');
        $sql .= ' LIMIT ' . ($start + 0) . ',' . $length;
    }
    $tab_id = $app['db']->fetchAll($sql);
    $cle = descr($objet . '.cle_sql');
    $tab_id = table_simplifier($tab_id,$cle);
    return [$tab_id, $nb_total];
}

function objet_liste_dataliste_selection($objet, $tab_id,$tri='')
{
    $tab = array();
    if (!empty($tab_id)) {
        $nom_class = descr($objet . '.phpname') . 'Query';
        $tab = $nom_class::getAll($tab_id, $tri);
    }
    return $tab;
}

function objet_liste_dataliste_preparation($objet, $tab, $only_value = true)
{
    $tab_data = array();
    if (!empty($tab)) {
        $tab_colonnes = donneTabCol($objet);
        $tab_data = datatable_prepare_data($tab, $tab_colonnes, $only_value);
    }
    return $tab_data;
}

function objet_liste_dataliste_detection_complement($objet)
{

    global $app;
    $php = fichier_php($objet, 'liste');
    if ($php!='objet_liste.php') {
        $nom_fonction_liste = $objet . '_liste';
        if (function_exists($nom_fonction_liste)) {
            include_once($app['basepath'] . '/src/' . $php);
        }
        $nom_fonction = $objet . '_dataliste_complement';
        return function_exists($nom_fonction);
    }
    return false;
}

function objet_liste_dataliste_complement($objet, $tab, $tab_data, $only_value = true)
{
    $nom_fonction = $objet . '_dataliste_complement';
    $tab_data = $nom_fonction($tab, $tab_data);
    if($only_value)
        $tab_data = array_map('array_values',$tab_data);
    return $tab_data;
}

function objet_liste_dataliste_envoi($tab_data, $nb_total)
{
    global $app;
    return $app->json([
        'draw' => $app['request']->get('draw'),
        'recordsTotal' => $nb_total,
        'recordsFiltered' => $nb_total,
        'data' => $tab_data,
        'error' => ''
    ]);
}




function objet_liste_rechercher($objet=null)
{

    global $app;

    if (!$objet) {
        $objet = sac('objet');
    }
    $cle = descr($objet . '.cle_sql');
    $sous_requete = getSelectionObjet();
    $nom_class = descr($objet . '.phpname') ;
    $nom_class_query = $nom_class . 'Query';
    $tab_data = array();
    $tab_id = $app['db']->fetchAll($sous_requete. ' LIMIT 0,10' );
    $tab_id =table_simplifier($tab_id,$cle);

    $tab = $nom_class_query::getAll($tab_id);

    $f_libelle = (method_exists($nom_class,'getLibelle'))?'getLibelle':'getNom';

    foreach ($tab as $t) {
        $tab_data[] = [
            'id'=>$t->getPrimaryKey(),
            'text'=>$t->$f_libelle()];
    }
    return $app->json($tab_data);

}