<?php

use \Propel\Runtime\ActiveQuery\Criteria;
/**
 * pref
 * @param string $var
 * @param string $defaut
 * @return array|null|string
 */
function pref($var = '', $defaut = '')
{
    return lire_preference($var, $defaut);
}


function lire_preference($nom, $defaut = '')
{
    global $app;

    if ($nom && $app['user']) {
        $val_defaut = sac('preference.' . $nom);
        $pref_user = getUserPreference($app['session']->get('preference'),$nom);
        if ($pref_user === null) {
            if ($val_defaut)
                return $val_defaut;
            else
                return $defaut;
        }  else {
            $val = table_merge($val_defaut,$pref_user);
        }
    } else {
        return array();
    }
    if ($val === null) {
        return $defaut;
    }
    return $val;
}



function initUserPreference($id_utilisateur=null)
{
    global $app;
    if(intval($id_utilisateur)==0)
        $id_utilisateur = suc('operateur.id');
    $app['session']->set('preference', \PreferenceQuery::getPreferenceUtilisateur($id_utilisateur));
}

function getUserPreference($prefs,$nom = '')
{

    if (!empty($nom)) {
        if (isset($prefs['generique']))
            return tableauChemin($prefs['generique'], $nom);
        else
            return null;
    }
    return $prefs;
}


function chargement_preference()
{


    $tab_pref = PreferenceQuery::create()->filterByIdIndividu(null,Criteria::ISNULL)->find();
    $tab = array();
    foreach ($tab_pref as $c) {
        $tab[$c->getNom()] = $c->getValeur();
    }

    setContexte('preference', $tab);

}

/**Mise a jour ou création(absence) de l'enregistrement opérateur et/ou entite la création se refere à l'enregistrement maitre en cas d'absense
 * @param $nom sous la forme niveau0[|niveau1[|niveau2]]
 * @param $saisie valeur du dernier niveauX ou tableau complet du dernier niveauX
 *
 * @return bool vrai réussie faux échec todo reste à documenter les erreurs (methode et texte)
 * Exemple : impimante|etisuette et array('nb_colonne' => 3,'nb_ligne' => 9) change 2 valeurs
 * ou impimante|etisuette|nb_colonne et  3  Change 1 valeur
 * ou impimante|etisuette et array('nb_colonne' => 3,'depart' => array('numero'=>4,'autre'=>'test')) change 3 valeurs dont une sous valeur composée de uniquement 2 valeurs si il y en avait plus elle serait perdu
 * todo voir les valeurs ajoutées dans le maitre aprés la création de l'enregistrement spécifique
 */
function ecrire_preference($nom, $value, $entite = false, $operateur = true)
{
    global $app;
    if ($nom) {
        $tab_clef = explode('.', $nom);
        $preference_query = PreferenceQuery::create()->filterByNom($tab_clef[0]);
        if ($operateur) {
            $preference_query->filterByIdIndividu(suc('operateur.id'));
        } else {
            $preference_query->filterByIdIndividu(null);
        }
        if ($entite) {
            $preference_query->filterByIdEntite(pref('en_cours.id_entite'));
        } else {
            $preference_query->filterByIdEntite(null);
        }

        $preference = $preference_query->findOne();

        if (!$preference) {//
            $preference = new Preference();
            $preference->setNom($tab_clef[0]);
            if ($operateur) {
                $preference->setIdIndividu(suc('operateur.id'));
            }
            if ($entite){
                $preference->setIdEntite(pref('en_cours.id_entite'));
            }
        }
        $preference_valeur = $preference->getValeur();
        array_shift($tab_clef);
        if(empty($tab_clef)){

            $preference->setValeur($value);

        }
        else
        {
            $noeud = implode('.', $tab_clef);
            $preference->setValeur(dessinerUneBranche($preference_valeur, $noeud, $value));
        }

        $preference->save();
        initUserPreference(suc('operateur.id'));
        return true;
    } else {
        return false;
    }
}


/**
 * est utilisé dans les fenetres modales saisie par l'utilisateur n'affiche que les valeurs terminales
 * Todo reste a ajouter les valeurs par défauts combo case a cocher actuellement zone de texte
 * @param $nom sous la forme niveau0[|niveau1[|niveau2]]
 * @param null $id_entite
 * @param int $id_operateur
 * @param bool|true $data_id pour récuperer l'id et nom les data data par défaut False = ID
 * @param int $id si on envoie l'id  les autres parametres sont ignorés
 * @return array c'est un tableau avec les valeurs utilisables par modal
 *
 * pour utilisation voir exemple dans Membre_liste.php et membre_liste.twing
 */
function formulaire_modif_preference($clef, $id_operateur = null, $id_entite = null)
{
    include_once('fonctions_formbuilder.php');


}


/**
 * est utiliser en enregistrement des modales utilisateur
 * et par ecrire_preference : replissage dans la programmation des préférences par exemple
 *
 * @param $valeur tableau du champs Valeur d'une l'intance de l'objet confiq à modifier
 * @param array $clef la clef indique le point de départ des modifications.
 * @param $saisie si saisie est valeur la branche est modifié par la valeur qui peut avoir des sous valeurs
 *                          ou un tableau
 *                                   chaque valeur remplace la branche de la ligne
 *                                   les branches non reseignées ne sont pas affectées.
 */

function modifiePreferenceValeur($valeur, $clef, $saisie)
{

    $clef_z = array_shift($clef);
    if (count($clef) > 0) {
        $valeur[$clef_z] = modifiePreferenceValeur($valeur[$clef_z], $clef, $saisie);
    } else {
        if (empty($clef_z)) {
            return $saisie;
        } else {
            if (is_array($valeur[$clef_z])) {
                foreach ($valeur[$clef_z] as $k => $v) {
                    if (isset($saisie[$k])) {
                        $valeur[$clef_z][$k] = $saisie[$k];
                    }
                }
            } else {
                $valeur[$clef_z] = $saisie;
            }
        }
    }

    return $valeur;
}


function enregistre_preference($nom,$value, $id_operateur = null, $id_entite = null, $preference_utilisateur = false)
{
// todo a voir si doublon  avec ecrire_preference()
    // dans l'utilisation par autorisation form cree les preference pour un autre utilisateur
    $objet_data = PreferenceQuery::create()->filterByNom($nom)
        ->filterByIdEntite($id_entite)
        ->filterByIdIndividu($id_operateur)
        ->findOne();
    if (!isset($objet_data)) {
        $objet_data = new Preference();
    }
    if (!$preference_utilisateur) {// ce n''est pas un générique
         $id_operateur = null;
         $id_entite = null;
     }
    $objet_data->setIdEntite($id_entite);
    $objet_data->setIdIndividu($id_operateur);
    $objet_data->setnom($nom);
    $objet_data->setValeur($value);
    $objet_data->save();
}