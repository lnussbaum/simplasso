<?php


function bdd_version()
{
    global $app;
    return $app['bdd_version'];
}

function config_version()
{
    global $app;
    return $app['config_version'];
}

function verification_config()
{
    return !(config_version() == lire_config('config_version'));
}



function special_json_decode($vars)
{
    $vars = json_decode($vars);
    return objectToArray($vars);
}


function objectToArray($d)
{
    if (is_object($d)) {
        if (isset($d->date) && isset($d->timezone)) {
            $d = new DateTime($d->date, new DateTimeZone($d->timezone));
        } else {
            $d = get_object_vars($d);
        }
    }

    if (is_array($d)) {
         return array_map(__FUNCTION__, $d);
    } else {
        return $d;
    }
}

function init_sac()
{
    global $app;

    if ($app['request']) {
        $page = substr($app['request']->getPathInfo(), 1);
        if (is_bool($page)) {
            $page = '';
        }
        $v = array(
            'page' => str_replace('.','_point_',$page),
            'ajax' => $app['request']->isXmlHttpRequest(),
            'retour_form' => $app['request']->getUri()
        );
        // Analyse de l'url
        if (strrpos($page, "_")) {
            $objet = substr($page, 0, strrpos($page, "_"));
            $type_page = substr($page, strrpos($page, "_") + 1);
            if (!in_array($type_page, array('form', 'liste'))) {
                $objet = $page;
                $type_page = 'voir';
            }
        } else {
            $objet = $page;
            $type_page = 'voir';
        }

        if ($objet) {
            $description_objet = descr($objet);
            // on verifie s'il s'agit d'un objet
            if (!$description_objet === false) {
                $v['fonction_php'] = $description_objet['php'][$type_page];
                if (isset($description_objet['alias_valeur'])) {
                    $v['alias_valeur'] = $description_objet['alias_valeur'];
                }
                if (isset($description_objet['alias'])) {
                    $v['alias'] = $description_objet['alias'];
                }

                if (in_array($type_page, array('voir', 'form'))) {

                    $id = $app['request']->get($description_objet['cle']);
                    if (!$id) {
                        $id = $app['request']->get('id');
                    }

                    $faire = $type_page;
                    if ($type_page == 'form') {
                        $faire = ($id) ? 'modifier' : 'ajouter';
                    }
                    $v['objet_action'] = $objet . '_' . $faire;
                    $v['id'] = $id;
                    $v['action'] = $faire;

                }
                $v['objet'] = $objet;
                $v['type_page'] = $type_page;

            } else {
                $v['fonction_php'] = str_replace('.','_point_',$page);
                $tab_pages = liste_des_pages();
                if (isset($tab_pages[$page]['source'])) {
                    $v['source'] = $tab_pages[$page]['source'];
                }
                $v['objet'] = '';
                $v['type_page'] = 'voir';
            }

            $app['contexte'] = $v;
        }
    }
    if (sac('description') === null or $app['debug'] or $app['request']->get('rafraichir')) {
        // chargement des descriptions completes des objets
        setContexte('description', description_complete());
        // chargement des pages
        setContexte('pages', liste_des_pages_complete());
        // chargement des tables
        chargement_table();
//         chargement de la config
        chargement_config();
//         chargement des preferences par défaut
        chargement_preference();

    }
    return true;
}


function trans($chaine)
{
    global $app;
    return $app->trans($chaine);
}


function tableauChemin($tab, $chemin)
{

    if (!is_array($chemin)) {
        $chemin = explode('.', $chemin);
    }
    foreach ($chemin as $key) {
        if (isset($tab[$key])) {
            $tab = $tab[$key];
        } else {
            return null;
        }
    }
    return $tab;
}





function migration_config()
{
    global $app;


    $num_version = lire_config('config_version');
    if (empty($num_version)) {
        $num_version_actuel = '0.0.0';
    }

    echo ('Version en cours : ' . $num_version_actuel) . PHP_EOL;

    if (estUneAncienneVersion($num_version_actuel)) {

        // Série de mise à jour
        $serie_maj = array('0.9.0', '0.9.1', '0.9.2');

        foreach ($serie_maj as $v) {

            if (estUneAncienneVersion($num_version_actuel, $v)) {
                echo ('Passage de la version ' . $num_version_actuel . ' à la version ' . $v) . PHP_EOL;
                $fonction_maj = 'maj' . str_replace('.', '_', $v);
                include_once($app['basepath'] . '/src/db/maj/' . str_replace('.', '_', $v) . '.php');
                if (function_exists($fonction_maj)) {
                    $num_version_actuel = $fonction_maj();
                    if ($num_version_actuel === $v) {
                        changer_version($v);
                    } else {
                        exit('ERREUR dans le passage à la version ' . $v);
                    }
                }
            }
        }

    } else {
        echo('Votre version est à jour, rien ne change.');

    }

}


function afficher_telephone($numero, $sep=false)
{
    $numero = preg_replace('/[^0-9]/','', $numero);

    if(strlen($numero) != 10)
        return $numero;
    else
    {
        return chunk_split($numero, 2, ' ');
    }
}


function arbre($temp, $prefixe = '', $direct = true)
{
    if (is_array($temp)) {
        echo('<ul>');
        foreach ($temp as $key => $niveau) {
            echo '<li><strong>' . $prefixe . $key . '</strong>';
            if (is_array($niveau)) {
                arbre($temp[$key], $prefixe);
            } else {
                echo ' - <span class="type_var">' . gettype($niveau) . " : </span>";
                if (is_object($niveau)) {
                    echo '- est un objet' . '<BR>' . $prefixe . '<span class="type_var">objet</span>';
                    dump_capture($niveau);
                    echo '<BR>' . $prefixe . '** fin de l\'objet';
                } else {
                    echo $niveau;
                }
            }
            echo('</li>');
        }
        echo('</ul>');
    } else {
        echo '<ul><li>' . $temp . '</li></ul>';
    }
    return;
}
/*
function dump($var = '', $mes = '')
{
    echo '<div class="dump">';
    if (is_string($var) or is_numeric($var)) {
        echo($var);
    } else {
        dump_capture($var);
    }
    m($mes);
    echo('</div>');
}*/

function dump_habillage($var)
{
    echo($var);
    $temp = preg_replace('`^([string|integer]) \-(.*)`', '<span class="type_var">$1</span> $2', $var);
    return (empty($temp)) ? $var : $temp;

}

function dump_capture($var)
{
    ob_start("dump_habillage");
    var_dump($var);
    $content = ob_get_contents();

    ob_end_clean();
    var_dump($var);
    return $content;
}

function m($m)
{

    if (!empty($m)) {
        echo '<div>___<br/>' . $m . '<br/> __________________________________________</div>';
    }

}

function chaineAleatoire($nb_car, $chaine = 'azertyuiopqsdfghjklmwxcvbn0123456789')
{
    $nb_lettres = strlen($chaine) - 1;
    $generation = '';
    for ($i = 0; $i < $nb_car; $i++) {
        $pos = mt_rand(0, $nb_lettres);
        $car = $chaine[$pos];
        $generation .= $car;
    }
    return $generation;
}

function creer_uniqid()
{
    static $seeded;

    if (!$seeded) {
        $seed = (double)(microtime() + 1) * time();
        mt_srand($seed);
        srand($seed);
        $seeded = true;
    }

    $s = mt_rand();
    if (!$s) {
        $s = rand();
    }
    return uniqid($s, 1);
}


function array2htmltable($array, $options = array())
{


    $table = '';
    if (count($array) >= 2) {
        $table = '<table class="table table-striped">';
        foreach ($array as $key => $row) {
            $thead = ($key == 0 && isset($options['th']) && $options['th']);
            $table .= ($thead)?'<thead><tr>':'<tr>';
            $i = 0;
            $style = "";
            foreach ($row as $key2=>$row2) {
                if (isset($options['largeur'][$i])) {
                    $style = ' width="' . $options['largeur'][$i] . '"';
                }

                if ($key == 0 && $thead) {
                    $table .= '<th' . $style . ' align="left">' . $row2 . '</th>';
                } else {
                    $table .= '<td class="td_style'.$key2.'"' . $style . '>' . $row2 . '</td>';
                }
                $i++;
            }
            $table .= ($thead)?'</tr></thead>':'</tr>';
        }
        $table .= '</table>';
    }
    return $table;
}




function courriel_twig($to, $twig, $args_twig, $options = '')
{

    global $app;
    if ($app['email']) {
        $template = $app['twig']->loadTemplate('mail/' . $twig . '.html.twig');
        $args_twig['email_prefixe_sujet'] = $app['email_prefixe_sujet'];
        $sujet = trim($template->renderBlock('sujet', $args_twig));
        $html = $template->renderBlock('contenu', $args_twig);
        courriel($to, $sujet, $html, $options);
    }

}


function preparer_piece_jointe_avant_envoi($tab_ged){

    global $app;
    $tab = [];
    foreach($tab_ged as $ged){

        $file = $ged->saveFile($app['tmp.path'].'/');
        $nom_fichier = $ged->getNom().'.'.$ged->getExtension();
        $tab[$nom_fichier]=$app['tmp.path'].'/'.$file;
    }

    return $tab;

}



function courriel($to, $sujet, $html, $options = '')
{

    global $app;

    if ($app['email']) {
        $txt = new \Html2Text\Html2Text($html);

        if (empty($from)) {
            $from = $app['email_from'];
        }

        if (!empty($app['email_redirect'])) {
            $to = $app['email_redirect'];
        }

        $message = Swift_Message::newInstance()
            ->setSubject($sujet)
            ->setFrom($from)
            ->setTo($to)
            ->setBody($txt->getText())
            ->addPart($html, 'text/html');

        if (isset($options['pieces_jointes'])) {

            $tab_pj = (is_array($options['pieces_jointes'])) ? $options['pieces_jointes'] : array($options['pieces_jointes']);
            foreach ($tab_pj as $k => $pj) {
                if (is_int($k)) {
                    $k = basename($pj);
                }
                $message->attach(Swift_Attachment::fromPath($pj)->setFilename($k));
            }
        }

        $app->mail($message);

        if (!empty($app['email_copie'])) {
            $message->setTo($app['email_copie']);
            $app->mail($message);
        }
    }

}