<?php


use Symfony\Component\Form\Extension\Core\Type\CheckboxType;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\Extension\Core\Type\DateType;
use Symfony\Component\Form\Extension\Core\Type\FileType;
use Symfony\Component\Form\Extension\Core\Type\HiddenType;
use Symfony\Component\Form\Extension\Core\Type\IntegerType;
use Symfony\Component\Form\Extension\Core\Type\TextareaType;
use Symfony\Component\Form\Extension\Core\Type\FormType;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\FormError as FormError;
use Symfony\Component\Validator\Constraints as Assert;


function transforme_data_suivant_entree($data,$tab_champs){


    foreach ($tab_champs as $champs => $valeur) {
        if (is_array($valeur) && isset($valeur['type_champs']))
        {
            if ($valeur['type_champs']=='multiple_cle_valeur'){
                foreach($data[$champs] as $k => &$v)
                    $v = $k.':'.$v;
                $data[$champs]= implode(PHP_EOL,$data[$champs]);
            }
        } elseif(is_array($valeur)){
            $data[$champs] = transforme_data_suivant_entree($data[$champs],$tab_champs[$champs]);
        }
    }

    return $data;
}


function transforme_en_formulaire($nom,$entree,$data,$fonction_enregistrement,$niveau_max=1,$args=array()){

    global $app;
    $request=$app['request'];
    if(!is_array($entree)){
        $entree = array($nom=>$entree);
        $data = array($nom=>$data);
    }

    $data = transforme_data_suivant_entree($data,$entree);

    $builder = $app['form.factory']->createNamedBuilder('form_cfpf_'.str_replace('.','_point_',$nom),FormType::class,$data);
    formSetAction($builder,true,$args);
    $builder->setRequired(false);

    $builder = builder_ajoute_champs($builder, $entree,$nom,1,$niveau_max);
    $builder->add('bt_enregistrer',SubmitType::class, array( 'attr' => array('class' => 'btn-primary')));
    $form = $builder->getForm();

    $form->handleRequest($request);
    if ($form->isSubmitted()) {
        $data = $form->getData();
        if ($form->isValid()){
            $fonction_enregistrement($nom,$data);
        }
    }
    return $form;
}



function builder_ajoute_champs($builder, $tab_champs,$nom_complet,$niveau=1,$niveau_max=100) {

    global $app;

    foreach ($tab_champs as $champs => $valeur) {

        if (is_array($valeur) && !isset($valeur['type_champs']))
        {
            if ($niveau<$niveau_max){
                $subform = $app['form.factory']->createNamedBuilder(str_replace('.','_point_',$champs),FormType::class);
                $subform = builder_ajoute_champs($subform, $valeur,$nom_complet,$niveau+1,$niveau_max);
                $builder->add($subform, $champs, array('label' => ' '));
            }
        } else {
            if (is_bool($valeur))
                $type_champs='oui_non';
            elseif (is_array($valeur)){
                $type_champs=$valeur['type_champs'];
                $args=$valeur;
                $valeur=$valeur['valeur'];
            }
            elseif (is_a($valeur,'DateTime')){
                $type_champs = 'date';
            }
            else{
                $type_champs = 'text';
            }

            if (is_array($valeur)){
                $chaine="";
                foreach($valeur as $kv=>$v)
                    $chaine .= $kv.':'.$v.PHP_EOL;
                $valeur = $chaine;
            }

            switch ($type_champs) {
                case 'oui_non':
                    $builder->add($champs, ChoiceType::class, array(
                        // 'constraints' => new Assert\NotBlank(),
                        'label'=>$app->trans($champs),
                        'expanded' => true,
                        'label_attr' => array('class' => 'radio-inline'),
                        'choices' => array('oui' => true, 'non' => false),
                        'attr' => array('inline' => true)));
                    break;

                case 'selection':
                    $choices=['courante'=>'courante'];
                    $tab_select = pref('selection.'.$args['objet']);
                    if ($nom_complet == 'imprimante.etiquette.membre') {
                        $choices[$app->trans('carte d adherent')] = 'carte_adh1';
                    }
                    if(is_array($tab_select)){
                        foreach($tab_select as $i=>$s)
                            $choices[$s['nom']]=$i;
                    }
                    $builder->add($champs, ChoiceType::class, array(
                        'choices' => $choices,
                        'multiple' =>false,
                        'expanded' => false,
                        'attr' => array( )));
                    break;
                case 'textarea'://observations text area
                case 'multiple': //  valeurs
                case 'multiple_cle_valeur': // combo avec couple valeurs M->Monsieur Mme ->Madame
                    $builder->add($champs, TextareaType::class, array(
                        'label'=>$app->trans($champs),
                        'constraints' => new Assert\NotBlank(),
                        'attr' => array( 'rows'=>(isset($args['nb_ligne']))?$args['nb_ligne']:5)));
                    break;
                case 'case_a_cocher':
                     $builder->add($champs, ChoiceType::class, array(
                        'label'=>$app->trans($champs),
                        'choices' => $args['choices'],
                         'multiple' =>true,
                         'expanded' => ($type_champs=='case_a_cocher'),
                        'attr' => array( )));
                    break;
                case 'select':
                case 'radio':
                    $builder->add($champs, ChoiceType::class, array(
                        'label'=>$app->trans($champs),
                        'choices' => $args['choices'],
                        'expanded' => ($type_champs=='radio'),
                        'label_attr' => array('class' => ($type_champs=='radio')?'radio-inline':''),
                        'attr' => array( )));
                    break;
                case 'select_multiple':
                case 'radio_multiple':
                    $builder->add($champs, ChoiceType::class, array(
                        'label'=>$app->trans($champs),
                        'choices' => $args['choices'],
                        'multiple' => true,
//                        'expanded' => ($type_champs== 'radio'),
                        'label_attr' => array('class' => 'radio-inline'),
                        'attr' => array('class' => 'multiselectshow')));
//                'expanded' => true,
//            'required' => false,

                    break;
                case 'date':
                    $builder->add($champs, DateType::class, array(
                        'label'=>$app->trans($champs),
                        'widget' => 'single_text',
                        'format' => 'dd/MM/yyyy',
                        'attr' => array('class' => 'datepickerb')
                    ));
                    break;
                case 'bouton_selection_courante':

                    break;

                default:
                    $builder->add($champs, TextType::class, array(
                      //  'constraints' => new Assert\NotBlank(),
                        'label'=>$app->trans($champs),
                        'attr' => array( )));
                    break;
            }
        }
    }
    return $builder;
}









// sert a ajouter dans les formulaires  les items de data
/*


function builder_ajoute_champs($builder, $champ, $valeur, $options) {
    global $app;


    switch ($champ) {

        case 'selection_petition':
            $temp2 = substr($champ, 10);
            if ($temp2 == 'petition') {
                $temp = array();// todo passer par les tunnels en attente
//            $sql = "SELECT id_petition as id,titre as nom
//            FROM spip_petitions pe left join spip_articles ar on pe.id_article=ar.id_article
//            WHERE pe.statut='publie'";
//            $statement = $app['db']->executeQuery($sql);
//            $tab_petitions = array();
//            while ($val = $statement->fetch()) {
//                $tab_petitions[$val['nom']] = $val['id'];
//            }
            }
            if (!empty($temp)) {
                $builder->add($champ, ChoiceType::class, array(
                    'label' => $champ,
                    'choices' => array_flip($temp),
                    'expanded' => false
                ));
            }
            break;


        case 'selection_individu':
        case 'selection_membre':
            $temp2 = substr($champ, 10);
            $temp = getSelections($temp2);
            if ($options['pref'] == 'imprimante.etiquette.membre') {
                $temp[$app->trans('carte d adherent')] = 'carte_adh1';
            }
            $builder->add($champ, ChoiceType::class, array(
                'label' => $champ,
                'choices' => $temp,
                'expanded' => false
            ));

            break;

            break;

        case 'affichage_histo' :
        case 'difference':
        case 'export_resultat':
        case 'identifiant_interne' :
        case 'valide':
        case 'nomcourt':
            $builder->add($champ, ChoiceType::class, array(
                'label' => $champ,
                'expanded' => true,
                'label_attr' => array('class' => 'radio-inline'),
                'choices' => array('oui' => 1, 'non' => 0),

                'attr' => array('inline' => true)

            ));
            break;




        case 'annee':
            $builder->add($champ, ChoiceType::class, array(
                'label' => $champ,
                'choices' => array(array('Une' => 0, 'Toutes' => 1)),
                'expanded' => true, 'label_attr' => array('class' => 'radio-inline'),
                'extra_fields_message' => 'foobar'
            ));
            break;


        case 'classement':
            $builder->add($champ, ChoiceType::class, array(
                'label' => $champ,
                'expanded' => true,
                'label_attr' => array('class' => 'radio-inline'),
                'choices' => array('Nom' => 0, 'Code postal+ville+nom' => 1, 'Ville+nom' => 2),
                'attr' => array('inline' => true)

            ));
            break;

        case 'enchainement':
            $builder->add($champ, ChoiceType::class, array(
                'label' => $champ,
                'expanded' => true,
                'label_attr' => array('class' => 'radio-inline'),
                'choices' => array('cotisation' => 'sr_cotisation',  'adhesion' => 'sr_adhesion'),
                'attr' => array('inline' => true)

            ));
            break;

        case 'prestations'://or is_null($data['prestation':// decomposer en 2 prestion_type ????

            $nom = array_search($options['prestation_type'], getPrestationTypeSimple());
            $tab_prestation = array_flip(table_simplifier(getPrestationDeType($nom)));
            if ($tab_prestation) {
                $builder->add('prestations', ChoiceType::class, array(
                    'label' => $app->trans($options['prestation_type']),
                    'expanded' => true,
                    'label_attr' => array('class' => 'radio-inline'),
                    'choices' => $tab_prestation,
                    'attr' => array('inline' => true)
                ));
            }
            break;

        case 'fichier':
            if (isset($options['a_suivre'])) {
                if (substr($options['a_suivre'], 0, 6) == 'import') {
                    $builder->add('fichier', FileType::class,
                        array(
                            'data' => '',
                            'label' => $app->trans('fichier_import'),
                            'constraints' => new Assert\NotBlank()
                        ));
                }
            }

            break;

        case 'affichage':
            $builder->add('affichage', ChoiceType::class, array(
                'expanded' => true,
                'label_attr' => array('class' => 'radio-inline'),
                'choices' => array('oui' => 1, 'non' => 0),

                'attr' => array('inline' => true)
            ));
            break;


        case 'idem_precedent2':
            $builder->add('idem_precedent2', ChoiceType::class, array(
                'label' => $app->trans('idem_precedent2'),
                'choices' => array(
                    $app->trans('identique_enregistrement_precedent') => '1', $app->trans('choix_ci_dessous') => '2'
                ),
                'expanded' => true, 'label_attr' => array('class' => 'radio-inline'),
                'extra_fields_message' => 'foobar'
            ));
            break;

        case 'idem_precedent3':
            $builder->add('idem_precedent3', ChoiceType::class, array(
                'label' => $app->trans('idem_precedent3'),
                'choices' => array(
                    $app->trans('identique_enregistrement_precedent') => '1', $app->trans('choix_ci_dessous') => '2',
                    $app->trans('dernier_du_membre') => '3'
                ),
                'expanded' => true, 'label_attr' => array('class' => 'radio-inline'),
                'extra_fields_message' => 'foobar'
            ));
            break;

        case 'mode':
            $builder->add('mode', ChoiceType::class, array(
                'label' => $app->trans('mode'),
                'choices' => array($app->trans('mode0') => '0', $app->trans('mode1') => '1'),
                'expanded' => true, 'label_attr' => array('class' => 'radio-inline')
            ));
            break;

        case 'mot_motgroupe1':
        case 'mot_motgroupe2':
        case 'mot_motgroupe3':
            $builder->add($champ, ChoiceType::class, array(
                'label' => $champ,
                'choices' => array('Aucun' => 0) + array_flip(table_simplifier(tab('motgroupe'))),
                'expanded' => true, 'label_attr' => array('class' => 'radio-inline')
            ));

            break;

        case 'ardent_nb_annee':
        case 'depart':
            $builder->add($champ, IntegerType::class, array(
                'label' =>  $champ,
                'attr' => array('data-help' => 'etiquette_' . $champ . '_help'),
            ));
            break;

        case 'nb_exemplaire':
            $builder->add($champ, IntegerType::class, array(
                'label' => 'nb_exemplaire',
                'attr' => array(
                    'class' => 'span2',
                    'placeholder' => 'nom'
                ),
                'extra_fields_message' => 'foobar'
            ));
            break;

        case 'nb_lentete':
            $builder->add($champ, IntegerType::class, array(
                'label' => 'nb_ligne_entete',
                'attr' => array(
                    'class' => 'span2',
                    'placeholder' => 'nombre'
                ),
                'extra_fields_message' => 'foobar'
            ));
            break;

        case 'nb_ligne':
            $builder->add($champ, IntegerType::class, array(
                'label' => $app->trans('nb_ligne'),
                'attr' => array(
                    'class' => 'span2',
                    'placeholder' => 'nom'
                ),
                
            ));
            break;

        case 'nb_lignes':
            $builder->add('nb_lignes', IntegerType::class, array(
                'label' => 'nb_lignes',
                'attr' => array(
                    'class' => 'span2',
                    'placeholder' => 'nom'
                ),

            ));
            break;

        case 'nom_colonne':
            foreach ($valeur as $k => $nom) {

                $builder->add('nc_' . $k, TextType::class, array(
                    'data' => $nom,
                    'label' => $app->trans($k),
                    'attr' => array('class' => 'span2', 'placeholder' => 'Nom colonne dans le documment a intégrer'),
                    'extra_fields_message' => 'Nom de la colonne dans le documment a intégrer'
                ));
            }
            break;

        case 'option1':
        case 'option2':
        case 'option3':
            $builder->add($champ, ChoiceType::class, array(
                'label' => $champ,
                'expanded' => true,
                'label_attr' => array('class' => 'radio-inline'),
                'choices' => array($app->trans('oui') => '1', $app->trans('non') => '0'),
                'extra_fields_message' => 'Valeur à renseigner dans les 5 colonnes',
                'attr' => array('inline' => true)
            ));
            break;
        case 'cotid_motgroupe':
            $builder->add('cotid_motgroupe', ChoiceType::class, array(
                'label' => $app->trans('cotid_motgroupe'),
                'choices' => array('Aucun' => '0') + array_flip(table_simplifier(tab('motgroupe'))),
                'expanded' => true, 'label_attr' => array('class' => 'radio-inline')
            ));

            break;
        case 'cotp_defaut'://or is_null($data['prestation':// decomposer en 2 prestion_type ????

//            $nom = array_search($options['prestation_type'], tab('prestation_type'));
//            $tab_prestation = table_simplifier(getPrestationDeType($nom));
            $tab_prestation = table_simplifier(getPrestationDeType(1));
            if ($tab_prestation) {
                $builder->add('cotp_defaut', ChoiceType::class, array(
                    'label' => $app->trans('cotisation'),
                    'expanded' => true,
                    'label_attr' => array('class' => 'radio-inline'),
                    'choices' => array_flip($tab_prestation),
                    'attr' => array('inline' => true)
                ));
            }
            break;
        case 'cott_defaut':
            $builder->add('cott_defaut', ChoiceType::class, array(
                'label' => $app->trans('tresor'),
                'expanded' => true,
                'label_attr' => array('class' => 'radio-inline'),
                'choices' => array_flip(suc('tresor')[pref('en_cours.id_entite')]),
                'attr' => array('inline' => true)
            ));
            break;


        case 'adhid_motgroupe':
            $builder->add('adhid_motgroupe', ChoiceType::class, array(
                'label' => $app->trans('adhid_motgroupe'),
                'choices' => array('Aucun' => '0') + array_flip(table_simplifier(tab('motgroupe'))),
                'expanded' => true, 'label_attr' => array('class' => 'radio-inline'),
                'extra_fields_message' => 'foobar'
            ));
            break;
        case 'adhp_defaut'://or is_null($data['prestation':// decomposer en 2 prestion_type ????

//            $nom = array_search($options['prestation_type'], tab('prestation_type'));
//            $tab_prestation = table_simplifier(getPrestationDeType($nom));
//todo revoir parametrage comme prestations plus haut
            $tab_prestation = table_simplifier(getPrestationDeType(6));
            if ($tab_prestation) {
                $builder->add('adpp_defaut', ChoiceType::class, array(
                    'label' => $app->trans('adhesion') . ' ' . $app->trans('defaut'),
                    'expanded' => true,
                    'label_attr' => array('class' => 'radio-inline'),
                    'choices' => array_flip($tab_prestation),
                    'attr' => array('inline' => true)
                ));
            }
            break;
        case 'adht_defaut':

            $builder->add('adht_defaut', ChoiceType::class, array(
                'label' => $app->trans('tresor'),
                'expanded' => true,
                'label_attr' => array('class' => 'radio-inline'),
                'choices' => array_flip(suc('tresor')[pref('en_cours.id_entite')]),
                'attr' => array('inline' => true)
            ));
            break;


        //import
//	case 'typefichier':
//        $builder->add( 'typefichier',ChoiceType::class, array(
//            'label' => $app->trans('typefichier'),
//            'expanded' => true,
//            'label_attr' => array('class' => 'radio-inline'),
////            'choices' => array( $app->trans('libre')=> 'ods', $app->trans('microsoft 2003') => 'xls',$app->trans('microsoft 2007') => 'xlsx'),
//            'choices' => array( $app->trans('csv-tabulation')=> '.csv'),
//            'attr' => array('inline' => true)
//        ));
//    break;
        // stat
        case 'paiement':
            $builder->add('paiement', ChoiceType::class, array(
                'label' => $app->trans('paiement'),
                'expanded' => true,
                'label_attr' => array('class' => 'radio-inline'),
                'choices' => array($app->trans('gerenon') => 'non', $app->trans('gereoui') => 'oui'),

                'attr' => array('inline' => true)
            ));
            break;

        case 'paiement_dd':
            $builder->add('paiement', DateType::class, array(
                'label' => $app->trans('date_debut') . ' ' . $app->trans('paiement'),
                'widget' => 'single_text',
                'format' => 'dd/MM/yyyy',
                'attr' => array('class' => 'datepickerb')
            ));
            break;

        case 'pays':
            $builder->add('pays', ChoiceType::class, array(
                'label' => $app->trans('pays'),
                'choices' => array_flip(tab('pays')),
                'attr' => array()
            ));
            break;
        // TODO ajouter javascrpt entre entite et prestation
        case 'prix_controle':
            $builder->add($champ, ChoiceType::class, array(
                'label' => $champ,
                'expanded' => true,
                'label_attr' => array('class' => 'radio-inline'),
                'choices' => array('oui' => 1, 'non' => 0),

                'attr' => array('inline' => true)

            ));
            break;

        case 'servicerendu':
            $builder->add('servicerendu', ChoiceType::class, array(
                'label' => $app->trans('servicerendu'),
                'expanded' => true,
                'label_attr' => array('class' => 'radio-inline'),
                'choices' => array('non' => $app->trans('gerenon'), 'oui' => $app->trans('gereoui')),

                'attr' => array('inline' => true)
            ));
            break;

        case 'servicerendu_dd':
            $builder->add('servicerendu', DateType::class, array(
                'label' => $app->trans('date_debut') . ' ' . $app->trans('servicerendu'),
                'widget' => 'single_text',
                'format' => 'dd/MM/yyyy',
                'attr' => array('class' => 'datepickerb')
            ));
            break;

        case 'sortie':
            $builder->add('sortie', ChoiceType::class, array(
                'label' => $app->trans('sortie'),
                'expanded' => true,
                'label_attr' => array('class' => 'radio-inline'),
                'choices' => array(
                    $app->trans('pdf') => '1', $app->trans('tableur') => '0', $app->trans('pdf et tableur') => '2'
                ),

                'attr' => array('inline' => true)

            ));
            break;

        case 'type'://avec filtre gere de choix tous
            foreach (getPrestationTypeActive() as $k => $nom) {
                if (pref('' . $nom . '.affichage') ) {
                    $builder->add('type' . $k, ChoiceType::class, array(
                        'label' => $app->trans($nom),
                        'expanded' => true,
                        'label_attr' => array('class' => 'radio-inline'),
                        'choices' => array('oui' => 1, 'non' => 0),
                        'attr' => array('inline' => true)
                    ));
                }
            }
            break;


        case 'avec_individu':
        case 'champs_titulaire':
        case 'champs_nom_membre':
        case 'champs_civilite':
        case 'champs_numero':
            $builder->add($champ, CheckboxType::class, array(
                'label' => $champ,
                'required' => false,
                'attr' => array('align_with_widget' => true, 'class' => 'bs_switch')
            ));

            break;
        case 'numero_premiere':
        case 'nb_colonne':
        case 'nb_ligne':
        case 'largeur_page':
        case 'hauteur_page':
        case 'marge_haut_page':
        case 'marge_bas_page':
        case 'marge_droite_page':
        case 'marge_gauche_page':
        case 'marge_haut_etiquette':
        case 'marge_gauche_etiquette':
        case 'marge_droite_etiquette ':
        case 'espace_etiquettesl ':
            $builder->add($champ, IntegerType::class, array(
                'label' => $app->trans($champ)
            ));

            break;
        default:
//            echo '<br>non traité :'.$champ;var_dump($valeur);var_dump($options);

    }
    return $builder;

}
*/

///**
// * <<utiliser par formulaire_modif_config et configfiche alimente les modales de saisie
// * @param $clef
// * @param $data_m
// * @param $id
// * @param null $id_entite
// * @param null $id_individu
// * @param bool|true $appel_config
// * @return array
// * @throws Exception
// * @throws PropelException
// */
//function formulaire_modif($clef, $data_m, $id_entite = null, $id_individu = null) {
//    global $app;
//    $form_ok = false;
//    $request = $app['request'];
//    $tab_clef = explode('.', $clef);
//    $nom = $tab_clef[0];
//    $builder_modifie = $app['form.factory']->createNamedBuilder('form_config_' . str_replace('.', '_', $clef),
//        FormType::class, $data_m);
//    $builder_modifie->setRequired(false);
//
//    if (is_array($data_m)) {
//        foreach ($data_m as $key => $temp) {
//            $builder_modifie->add($key, TextType::class, array('label' => $key, 'attr' => array('class' => 'span2')));
//        }
//    }
//    $form_modifie = $builder_modifie
//        ->add('save_modifie', SubmitType::class,
//            array('label' => $app->trans('Enregistrer'), 'attr' => array('class' => 'btn-primary')))
//        ->getForm();
//
//    $form_modifie->handleRequest($request);
//    if ($form_modifie->isSubmitted()) {
//        if ($form_modifie->get('save_modifie')->isClicked()) {
//            if ($form_modifie->isValid()) {
//                $objet_data = ConfigQuery::create()->findByNom($nom);
//                //todo cela devient  enregistrement utilisateur , societe si c'est un appel suivant parametrage
//                array_shift($tab_clef);
//                $valeur = modifieConfigValeur($objet_data->getValeur(), $tab_clef, $form_modifie->getData());
//                $objet_data->setValeur($valeur);
//                enregistre_config($objet_data, $id_individu, $id_entite, true);
//                if (count(array_keys($clef)) == 3) {//todo trouver une solution plus élégante dans la fenetre membre_liste
//                    if ($clef[0] == 'imprimante') {
//                        if ($clef[1] == 'etiquette') {
//                            if ($clef[2] == 'depart') {
//                                action_membre_liste_etiquettes();
//                            }
//                        }
//                    }
//                }
//
//                $app['session']->getFlashBag()->add('success', $app->trans(sac('objet_action') . '_ok'));
//                return array(true, sac('retour_form'));
//            } else {
//                $app['session']->getFlashBag()->add('info', 'Saisie à corriger');
//            }
//        }
//
//    }
//    $tab_param['form'] = $form_modifie->createView();
//    $tab_param['nom_form'] = 'form_config_' . str_replace('.', '_', $clef);
//
//    return array($form_ok, $app['twig']->render('inclure/form.html.twig', $tab_param));
//
//}
//



