<?php




/**
 * autoriser
 * @param $profils
 * @param int $id_entite
 * @return bool
 */
function autoriser($profils, $id_entite = 0)
{
    global $app;
    if (!is_array($profils)) {
        $profils = array($profils);
    }
    foreach ($profils as $profil) {
        $tab_roles = $app['user']->getRoles();
        if ($id_entite > 0) {
            if (in_array($profil . '-' . $id_entite, $tab_roles)) {
                return true;
            }
        } else {
            $tab_roles = rolesSansEntite($tab_roles);
            if (in_array($profil, $tab_roles)) {
                return true;
            }
        }
    }
    return false;
}



/**
 * pageAutorisee
 * @param string $page
 * @return bool
 */
function pageAutorisee($page = '')
{
    global $app;
    if (empty($page)) {
        $page = sac('page');
    }
    if (strrpos($page, "_")) {
        $objet = substr($page, 0, strrpos($page, "_"));
        $temp_type_page = substr($page, strrpos($page, "_") + 1);
        if (!in_array($temp_type_page, array('form', 'liste'))) {
            $objet = $page;
        }
    } else {
        $objet = $page;
    }
    $description_objet = descr($objet);
    // on verifie s'il s'agit d'un objet

    if (!empty($description_objet)) {
        $droits = (isset($description_objet['droits'])) ? $description_objet['droits'] : array('all');
    } else {
        $droits = sac("pages." . $page . ".droits");
    }
    if (!$droits) {
        $droits = array();
    }
    if (empty(array_intersect($droits, rolesSansEntite($app['user']->getRoles()))) && !in_array('all', $droits)) {
        return false;
    }
    return true;

}





function rolesSansEntite($tab_roles)
{

    foreach ($tab_roles as &$roles) {
        $roles = substr($roles, 0, strpos($roles, '-'));
    }
    return array_unique($tab_roles);
}



function rolesNiveaux($tab_roles)
{

    foreach ($tab_roles as &$roles) {
        $temp = substr($roles, 1, (strpos($roles, '-') - 1));
        if ($temp > '') $niveaux[] = $temp;
    }
    $niveaux[] = 6;
    return array_unique($niveaux);
}



function rolesProfils($tab_roles)
{

    foreach ($tab_roles as &$roles) {
        $roles = $roles[0];
    }
    return array_unique($tab_roles);
}



function entitesParProfil($tab_roles,$profil = '')
{

    $res = array();
    foreach ($tab_roles as $roles) {
        $pos = strpos($roles, '-');
        if (!$pos === false) {
            $p = substr($roles, 0, $pos);
            if ($p == $profil)
                $res[] = substr($roles, $pos + 1);
        }

    }

    return array_unique($res);
}


function autorisationMenu($menu)
{
    global $app;
    $tab_roles = rolesSansEntite($app['user']->getRoles());
    foreach ($menu as $i => &$m) {

        if (isset($m['menu'])) {


            if (isset($m['permission'])) {
                $droits = is_array($m['permission'])?$m['permission']:[$m['permission']];
                if (!empty(array_intersect($droits, $tab_roles))) {
                    $m['menu'] = autorisationMenu($m['menu']);
                } else {
                    unset($menu[$i]);
                }

            }

        } else {
            if (!(substr($i, 0, 4) == 'http' or substr($i, 0, 1) == '#' or pageAutorisee($i))) {
                unset($menu[$i]);
            }

        }

    }

    return $menu;
}