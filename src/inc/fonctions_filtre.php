<?php


function donneTabCol($objet){
    global $app;
    if (!$objet)
        $objet=sac('objet');
    $fonction = $objet.'_colonnes';
    if (function_exists($fonction)){
        return $fonction();
    }else{
        include_once($app['basepath'].'/src/'.fichier_php($objet,'liste'));
        if (function_exists($fonction)) {
            return $fonction();
        }
    }

    return [];
}



function donneTabFiltre($objet){

    global $app;
    if (!$objet)
        $objet=sac('objet');
    $fonction = $objet.'_filtres';
    if (function_exists($fonction)){
        return $fonction();

    }else{
        include_once($app['basepath'].'/src/'.fichier_php($objet,'liste'));
        if (function_exists($fonction)) {
            return $fonction();
        }
    }

    return getFiltresSimples();

}


function liste_filtre($tab_filtres)
{

if (isset($tab_filtres['premier']) ){
        $result = array_keys($tab_filtres['premier']);
        if(isset($tab_filtres['second'])){
            foreach ($tab_filtres['second'] as $filtres) {
                $result = array_merge($result, array_keys($filtres));
            }
        }
        return $result;
    }
}

function filtre_ajouter_limitation_id(){
    global $app;
    $request = $app['request'];
    $valeur = $request->get('limitation_id');
    return [
        'id' => 'limitation_id',
        'nom' => 'limitation_id',
        'placeholder' => 'limitation_id',
        'type' => 'text',
        'class' => '',
        'value' => $valeur];
}




function filtre_ajouter_inverse(){

    global $app;
    $request = $app['request'];
    $checked = !($request->get('inverse')===null);
    $options = [['on' => $checked, 'valeur' => 'inverse', 'libelle' => 'inverse_selection']];

    return [
        'id' => 'inverse_selection',
        'nom' => 'inverse_selection',
        'type' => 'checkbox',
        'options' => $options];
}

function filtre_prepare_donnee($nom, $tab_valeur, $valeur)
{
    global $app;
    $request = $app['request'];

    $filtre = [
        'nom' => $nom
    ];
    if($tab_valeur){
        foreach ($tab_valeur as $k => $val) {
            $filtre['options'][] = array('libelle' => $k, 'valeur' => $val, 'on' => ($valeur == $val));
        }
    }else
    {
            $checked = !($request->get('inverse')===null);
        $options = [['on' => $checked, 'valeur' => 'inverse', 'libelle' => $nom]];

        $filtre =  [
            'id' => $nom,
            'nom' => $nom,
            'type' => 'checkbox',
            'options' => $options];

    }
    return $filtre;
}


function session_enregistrer_selection_courante($objet=null){


    global $app;
    if (!$objet)
        $objet=sac('objet');
    $request = $app['request'];
    $tab_champs_filtres = liste_filtre(donneTabFiltre($objet));

    $tab_request = [];
    foreach ($tab_champs_filtres as $filtre) {
        $temp = $request->get($filtre);
        if (!empty($temp)) {
            $tab_request[$filtre] = $temp;
        }
    }

    $app['session']->set('selection_courante_'.$objet, $tab_request);
}


function filtre_ajouter_geo()
{

    $tab_zonegroupe = table_simplifier(tab('zonegroupe'));
    $tab_zone=tab('zone');
    $tab=[];
    foreach($tab_zonegroupe as $k=>$titre){
        $tz=table_simplifier(table_filtrer_valeur($tab_zone,'id_zonegroupe',$k));
            foreach($tz as $j=>$z) {
                $tab[$titre][$j] = ['valeur'=>$j,'libelle'=>$z];
            }
    }


    return [
        'commune' => ['nom' => 'commune', 'class' => 'select2_autocomplete_commune', 'options' => [], 'multiple' => true],
        'arrondissement' => ['nom' => 'arrondissement', 'class' => 'select2_autocomplete_arrondissement', 'options' => [], 'multiple' => true],
        'departement' => ['nom' => 'departement', 'class' => 'select2_autocomplete_departement', 'options' => [], 'multiple' => true],
        'region' => ['nom' => 'region', 'class' => 'select2_autocomplete_region', 'options' => [], 'multiple' => true],
        'zone' => ['nom' => 'zone', 'class' => 'select2', 'options' => $tab, 'multiple' => true]
    ];
}



function filtre_ajouter_abonnement(){

    global $app;
    $request = $app['request'];
    $tab_valeur = ['ok', 'futur', 'echu', 'jamais'];
    $tab_filtres =[];
    $tab_prestation = getPrestationDeType('abonnement');
    foreach($tab_prestation as $id => $prestation){
        $nom='abonnement'.$id;
        $value = $request->get($nom);
        $options = filtre_options_select($tab_valeur,$value);
        $tab_filtres[$nom] = ['nom' => $nom, 'class' => 'select2', 'options' => $options, 'multiple' => false];
    }
    return $tab_filtres;

}


function selection_ajouter_limitation_id($objet,$where,$params){

    $pr=descr($objet.'.nom_sql');
    $cle=descr($objet.'.cle_sql');
    $restreindre_id = request_ou_options('limitation_id', $params);

    if (!empty($restreindre_id)) {
        if(strpos($restreindre_id,',')===false) {
            $restreindre_id = [ $restreindre_id ];
        }
        else {
            $restreindre_id = explode(',', $restreindre_id);
        }
            $where .= ' and ( ';
        foreach ($restreindre_id as $key => $id) {
            if ($key != 0) {
                $where .= ' or ';
            }

            if(strpos($id,'-')===false) {
                $where .= $pr.'.'.$cle.' =' . $id;
            }
            else {
                $ids = explode('-', $id);
                $where .= '('.$pr.'.'.$cle.' BETWEEN ' . $ids[0] . ' AND ' . $ids[1] . ')';
            }

        }
        $where .= ')';
    }

    return $where;
}


function filtre_ajouter_recherche($valeur)
{
    return [
        'id' => 'recherche',
        'nom' => 'search[value]',
        'placeholder' => 'rechercher',
        'label' => '<span class="glyphicon glyphicon-search"></span>',
        'type' => 'text',
        'class' => '',
        'value' => $valeur];
}




function liste_motgroupe($objet)
{
    $tab_motgroupe = table_filtrer_valeur_existe(tab('motgroupe'), 'objets_en_lien', $objet);
    $tab_motgroupe = table_filtrer_valeur($tab_motgroupe, 'systeme', false);
    $tab_groupe = ['mots'];
    foreach ($tab_motgroupe as $groupe) {
        if (!empty($groupe['options'][$objet]['nom'])){
            if (!isset($groupe['options'][$objet]['operateur']))
                $groupe['options'][$objet]['operateur']='AND';
            $tab_groupe['mots'.$groupe['options'][$objet]['nom']] = $groupe['options'][$objet]['operateur'];
        }
    }
    return $tab_groupe;
}


function filtre_ajouter_mots($objet,$prefixe='')
{

    global $app;
    $request = $app['request'];
    $mots = $request->get($prefixe.'mots');
    $filtres = array();
    $tab_motgroupe = table_filtrer_valeur_existe(tab('motgroupe'), 'objets_en_lien', $objet);
    $tab_motgroupe = table_filtrer_valeur($tab_motgroupe, 'systeme', false);
    $tab_mot_ind = table_filtrer_valeur_existe(tab('mot'), 'objet_en_lien', $objet);
    $tab_mot_ind = table_filtrer_valeur($tab_mot_ind, 'systeme', false);
    $tab_mots = [];
    if (!empty($tab_mot_ind)) {


        $mots = (is_array($mots)) ? $mots : array($mots);

        foreach ($tab_mot_ind as $k => $el) {
            $tab_mots[$k] = array('libelle' => $el['nom'], 'valeur' => $k);
            if (in_array($k, $mots)) {
                $tab_mots[$k]['on'] = true;
            }
        }
        $options_defaut = ['nom' => 'mots', 'opt_group' => true, 'classement' => '', 'indice' => ''];
        foreach ($tab_motgroupe as $id_groupe => $groupe) {

            $options = $options_defaut;
            if (isset($groupe['options'][$objet])) {
                $options = array_merge($options, $groupe['options'][$objet]);
            }

            $nom_filtre = ($options['nom']!='mots')? $prefixe.'mots'.$options['nom']:$prefixe.'mots';

            if (!isset($tab_m[$nom_filtre])) {
                $tab_m[$nom_filtre] = array();
            }
            $tab_mot = array_intersect_key($tab_mots, table_filtrer_valeur($tab_mot_ind, 'id_motgroupe', $id_groupe));
            $nb_mot = count($tab_mot);
            if ($options['opt_group']) {
                if (!isset($tab_m[$nom_filtre][$groupe['nom']])) {
                    $tab_m[$nom_filtre][$groupe['nom']] = array();
                }
                $tab_m[$nom_filtre][$groupe['nom']] = array_merge($tab_m[$nom_filtre][$groupe['nom']], $tab_mot);
            } else {
                $tab_m[$nom_filtre] = array_merge($tab_m[$nom_filtre], $tab_mot);
            }

            if ($nb_mot > 0) {
                $filtres[$nom_filtre] = array(
                    'nom' => $nom_filtre, 'options' => $tab_m[$nom_filtre], 'multiple' => true
                );
            }

        }
    }

    return $filtres;
}


function filtre_ajouter_mots_stat($objet, $mots, $sous_requete)
{


    $filtres = array();
    $tab_motgroupe_ind = table_filtrer_valeur_existe(tab('motgroupe'), 'objets_en_lien', $objet);
    $tab_mot_ind = table_filtrer_valeur_existe(tab('mot'), 'objet_en_lien', $objet);
    $tab_mot_ind = table_filtrer_valeur($tab_mot_ind, 'systeme', false);
    $tab_mots = [];
    if (!empty($tab_mot_ind)) {
        $tab_mots = MotLienQuery::getStat(array_keys($tab_mot_ind), $objet, $sous_requete);

        $mots = (is_array($mots)) ? $mots : array($mots);

        foreach ($tab_mots as $k => &$el) {
            $el = array('libelle' => $tab_mot_ind[$k]['nom'], 'nb' => $el, 'valeur' => $k);
            if (in_array($k, $mots)) {
                $el['on'] = true;
            }
        }
        $options_defaut = ['nom' => 'mots', 'opt_group' => true, 'classement' => '', 'indice' => ''];
        foreach ($tab_motgroupe_ind as $id_groupe => $groupe) {

            $options = $options_defaut;
            if (isset($groupe['options'][$objet])) {
                $options = array_merge($options, $groupe['options'][$objet]);
            }

            $nom_filtre = $options['nom'];

            if (!isset($tab_m[$nom_filtre])) {
                $tab_m[$nom_filtre] = array();
            }
            $tab_mot = array_intersect_key($tab_mots, table_filtrer_valeur($tab_mot_ind, 'id_motgroupe', $id_groupe));
            $nb_mot = count($tab_mot);
            if ($options['opt_group']) {
                if (!isset($tab_m[$nom_filtre][$groupe['nom']])) {
                    $tab_m[$nom_filtre][$groupe['nom']] = array();
                }
                $tab_m[$nom_filtre][$groupe['nom']] = array_merge($tab_m[$nom_filtre][$groupe['nom']], $tab_mot);
            } else {
                $tab_m[$nom_filtre] = array_merge($tab_m[$nom_filtre], $tab_mot);
            }

            if ($nb_mot > 0) {
                $filtres[$nom_filtre] = array(
                    'nom' => $nom_filtre, 'options' => $tab_m[$nom_filtre], 'multiple' => true
                );
            }

        }
    }

    return $filtres;
}


function getFiltresSimples()
{

    global $app;
    $request = $app['request'];
    $rechercher = $request->get('search');
    $tab_filtres['premier']['search'] = filtre_ajouter_recherche($rechercher['value']);

    return $tab_filtres;
}