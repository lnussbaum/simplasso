<?php

use Symfony\Component\Validator\Constraints as Assert;



function compte_rendu(&$proposition, $id,$fichier, $etape, $action){
    global $app;
    $proposition['nb']['lignes_lues'] = ($proposition['nb']['lignes_lues'] - $proposition['nb']['ligne_entete']);
    $proposition['nb']['nonconforme'] = ($proposition['nb']['lignes_lues'] - ($proposition['nb']['cot'] + $proposition['nb']['deja_enregistre']));
    $proposition['message'] .= '<br>Ligne des noms de colonne :' . $proposition['nb']['ligne_entete']  ;
    $proposition['message'] .= '<br>' . $proposition['nb']['lignes_lues'] . ' lignes lues ';
    $proposition['message'] .= '<br>' . ($proposition['nb']['lignes_traitees']) . ' lignes traitees ';
    $proposition['message'] .= '<br>' . $proposition['nb']['non_conforme'] . ' lignes Non Conforme ( NC )';
    $proposition['message'] .= '<br>' . $proposition['nb']['deja_enregistre'] . ' lignes déja enregistrées et sans modification ( ok )';
    $proposition['message'] .= '<br>' .($proposition['nb']['ind']) . ' Individus ' ;
    if (!isset($proposition['nb']['cot'])and $proposition['nb']['cot']>0) {
        $proposition['message'] .= '<br>' . $proposition['nb']['mem'] . ' membres ';
        if ($proposition['nb']['cot'] == 0) {
            $proposition['message'] .= ' <br>Pas de cotisation enregistrée';

        } else {
            $proposition['message'] .= ' <br><br>' . $proposition['nb']['cot'] . ' Cotisations enregistrées ( OK )';
        }
        $proposition['message'] .= '<br>' . $proposition['nb']['cotp'] . ' paiements';
    }
    $proposition['message'] .= '<br>  fin   a : ' . date('Y-m-d H:i:s');
    $proposition['message'] .= '<br>Source    : ' .$fichier;
    if (!isset($proposition['param']['typefichier'])) {
        $proposition['param']['typefichier'] = 'xlsx';
    }
    $export_extension = $proposition['param']['typefichier'];
    if (!$export_extension) {
        $erreur = '<br>le type de sortie n\'est pas décrit ou non autorisé';
        $proposition['message'] .= $erreur;
        return $erreur;
    }
    $proposition['nom_output'] = $id . '_' . trim(substr($fichier,0,10)) . '_' . date('Y-m-d H-i-s') .'.'. $export_extension;
    $proposition['url'] = '<a href="' . $app->path('download',
            ['fichier' => $proposition['nom_output']]) . '">Télécharger le fichier résultat</a>';
    $proposition['message'] .= '<br><br>' . $proposition['url'] . '<br><br>' .
        'Résultat : ' . $proposition['nom_output'];
    Log::EnrOp(false, 'import',  $id, '', $proposition['url']);
    return 'fin valide';
}





function cotisation_validite($num_ligne, &$tab, &$message, $id_defaut, $type = 'cot')
{
    $ancien='';
    $montant=$tab[$type . 'montant'];
    if ($tab[$type . 'id_servicerendu'] >= 1) {
        $temp = "modification";
    } else {
        $temp = "creation";
    }
 // todo ajouter option d'acceptation du montant Null
    if ($montant ==0  and $temp == "creation" ) {
        $tab['OK_' . $type] = 'NC';
        $tab['compte_rendu_' . $type] = 'pas de montant cotisation ligne' . $num_ligne;
        return $ancien;
    }
    if (is_int($tab[$type . 'id_prestation'])) {// si numerique id sinon nom_court
        $prestation = tab('prestation.' . $tab[$type . 'id_prestation']);
    } elseif ($tab[$type . 'id_prestation']) {
        $prestation = PrestationsQuery::create()->findOneByNomcourt($tab[$type . 'id_prestation']);
    } else {
        $prestation = PrestationsQuery::create()->findOneByNomcourt('C' . round($tab[$type . 'montant']));
    }
    if (!$prestation) {
        $prestation = PrestationsQuery::create()->filterByPrimaryKey($id_defaut)->findone();
    }
    if ($prestation) {
        if ($tab[$type . 'id_prestation'] != $prestation->getIdPrestation())
            $ancien = $tab[$type . 'id_prestation'];
        $tab['compte_rendu_' . $type] .=  'Changement de cotisation ancien code :' . $tab[$type . 'id_prestation'] ;
        $tab[$type . 'id_prestation'] = $prestation->getIdPrestation();
        if (!$tab[$type.'date_debut']) {
            //todo si saisie controler la date et traiter le format A faire
            $tab[$type . 'date_debut'] = calculeDateDebut(null,
            tab('prestation.' . $tab[$type . 'id_prestation'] . '.retard_jours'),
            tab('prestation.' . $tab[$type . 'id_prestation'] . '.periodique'));
        }
        $tab[$type . 'date_fin'] = calculeDatefin($tab[$type . 'date_debut'],
            tab('prestation.' . $tab[$type . 'id_prestation'] . '.periodique'));
        $tab[$type . 'montanttheorique'] = tab('prestation.' . $tab[$type . 'id_prestation'] . '.prix.0.montant');
        $tab['compte_rendu_' . $type] = $temp . ' de la cotisation ' . tab('prestation.' . $tab[$type . 'id_prestation'] . '.nom');

        if (is_object($tab[$type.'date_debut']))
            $temp=$tab[$type . 'date_debut']->format('d-m-Y');
        else
            $temp=$tab[$type . 'date_debut'];
        $tab['compte_rendu_' . $type] .= ' debut le :' . $temp . ' montant :' . $tab[$type . 'montanttheorique'];
        $tab['OK_' . $type] = 'bon';
    } else {
        $tab['OK_' . $type] = 'NC';
        $tab['compte_rendu_' . $type] = 'Pb sur identification de la cotisation ligne' . $num_ligne;
    }
    return $ancien;
}





function export( $proposition,$id)
{
    global $app;
//    $inputFileType = 'Excel2007';
//    $inputFileName =$fichier;//$app['document.path'].'/matrice'.$vannee.'.xlsx';
// pour un chargement
//    $original = transfert_fichier($proposition['entete']['fichier']);
//    $original = PHPExcel_IOFactory::createReader($inputFileType);
//    $original->load($inputFileName);
// pour un nouveau fichier
    $resultat = new PHPExcel();
// si deja chargé rien

    $resultat->setActiveSheetIndex(0);  //set first sheet as active
    $pas = 1000;
//    if (isset($proposition['nb']['lignes_compte_rendu'])) $proposition['nb']['lignes_compte_rendu']= $proposition['nb']['ligne_entete'] - 2;
//    $debut = $proposition['nb']['lignes_compte_rendu'] + 1;
    $debut=11;
    $fin = min($debut + $pas, $proposition['nb']['lignes_lues']);
    for ($num_ligne = $debut; $num_ligne <= $fin; $num_ligne++) {
        $ligne_data = ImportligneQuery::create()->filterByIdImport($id)->filterByLigne($num_ligne)->findone();
        if ($ligne_data){
            $tab = json_decode($ligne_data->getVariables(), true);
        $colonne=0;
        foreach ($tab as $texte) {

////            foreach ($proposition['lignes'] as $num_ligne => $tab) {
////        foreach ($proposition['nb']['colonnes'] as $colonne => $nom) {
////            if (isset($proposition['lignes'][$num_ligne][$nom['variable']])) {
////                switch ($nom['variable']) {
////                    case 'cotdate_fin' :
////                    case 'cotdate_debut' :
////                     if (is_object($proposition['lignes'][$num_ligne][$nom['variable']])) {
////                            $texte = $proposition['lignes'][$num_ligne][$nom['variable']]->format('d/m/Y');
////                        } else {
////                            $texte = $proposition['lignes'][$num_ligne][$nom['variable']];
////                        }
////                        break;
////                    case 'cotdate_cheque' :
////                        if (strlen(($proposition['lignes'][$num_ligne][$nom['variable']])) >= 10) {
////                            $t = explode('-', $proposition['lignes'][$num_ligne][$nom['variable']]);
////                            $texte = $t[2] . '-' . $t[1] . '-' . $t[0];
////                        }
////                        break;
////                    default :
////                        $texte = $proposition['lignes'][$num_ligne][$nom['variable']];
////                }
//////                $cellule = $original->getActiveSheet()->getCellByColumnAndRow($colonne, $num_ligne)->getValue();
//////                if (!isset($cellule) or $cellule <= '  !') {
            $colonne++;
             $resultat->getActiveSheet()->setCellValueByColumnAndRow($colonne, $num_ligne, $texte);
//////                }
            }
        }
    }
     $resultat->getActiveSheet()->setCellValueByColumnAndRow(7, 6,
        'Pour prendre en compte une modification déja validée : effacer OK sur la ligne concernée');
    $resultat->getActiveSheet()->setCellValueByColumnAndRow(5, 5, date('d/m/Y'));
    $resultat->getActiveSheet()->setCellValueByColumnAndRow(7, 5, date('H-i-s'));


//prepare download
//    $filename=mt_rand(1,100000).'.xls'; //just some random filename
//    header('Content-Type: application/vnd.ms-excel');
//    header('Content-Disposition: attachment;filename="'.$filename.'"');
//    header('Cache-Control: max-age=0');
//    $objWriter = PHPExcel_IOFactory::createWriter($objTpl, 'Excel5');  //downloadable file is in Excel 2003 format (.xls)
//    $objWriter->save('php://output');  //send it to user, of course you can save it to disk also!
//    exit; //done.. exiting!
    if (!isset($proposition['nom_output'])) {
        $proposition['nom_output'] = date('Y-m-d H-i-s') . '.xlsx';
    }
    $outputFileName = $app['output.path'] . '/' . $proposition['nom_output'];
//    export_extension($outputFileType,$proposition['param']['typefichier']);
//    $objWriter = PHPExcel_IOFactory::createWriter($resultat, $outputFileType);
$objWriter = new PHPExcel_Writer_Excel2007($resultat);
$objWriter->setOffice2003Compatibility(true);
//    $objWriter->save($outputFileName);
//    echo $outputFileName;
//    exit;
    return $proposition['message'];
}

function export_csv($proposition,$id)
{
    global $app;
    $message='';
    $indice = 0;
    $pas = 5000;
    $debut = 11;
    $export ='';
    $fin = min($debut + $pas, $proposition['nb']['lignes_lues']);
    for ($num_ligne = $debut; $num_ligne <= $fin; $num_ligne++) {
        $ligne_data = ImportligneQuery::create()->filterByIdImport($id)->filterByLigne($num_ligne)->findone();
        if ($ligne_data) {
            $test=$ligne_data->getVariables();
 //            if (isSerialized($test)) $tab = json_decode($test, array(false));
            if (isSerialized($test)) $tab = json_decode($test, true);
            else
                $tab = array('Impossible de désérialize ligne '.$num_ligne.' de import n° '.$id);
//            var_dump($tab);
//            exit;
            $export .= str_replace(array("\r\n","\n", "\r", "\""), '', implode("\t", $tab)) . PHP_EOL;
             $indice++;
        }
    }
    if ($indice == 0) {
        $message .= 'Aucun ' . $app->trans('adherent') . '<BR />';
    } else {
        header("Content-type: application/vnd.ms-excel");
        header("Content-disposition: attachment; filename=\"import_resultat_" . time() . ".csv\"");
        echo($export);
    }
   return;
}



function isSerialized($str) {
    return ($str == json_encode(false) || @json_decode($str) !== false);
}

function importligne_liredata($variables,$colonnes,$nb_colonnes)
{
    $tabvariables=json_decode($variables, true);
    foreach ($colonnes as $i => $v) {
        if (!isset($tabvariables[$colonnes[$i]['variable']])) {
            if ($i < $nb_colonnes) {
                $tabvariables[$colonnes[$i]['variable']] = $tabvariables[$i];
            } else {
                $tabvariables[$colonnes[$i]['variable']] = '';
            }
        }
    }
    if ($tabvariables['OK_ind'] == 'OK') {
        $tabvariables['OK_ind'] = 'ok';
    }
//    $tab['compte_rendu_individu'] = 'non connu';//todo utilité ?

    return $tabvariables;
}


function import_controle_ligne_membre(
    $num_ligne,    &$tab_l,  $tab_entete,    &$nb,    &$message,
    &$m_nom,    &$m_individu,    &$a_individu,    &$a_membre,    &$m_cot,    &$m_cotp) {

    import_ligne_validite_membre($num_ligne, $tab_l, $message);
    if ($tab_l['OK_mem'] == "atraiter") {
        $nb['lignes_traitees']++;

        $trouve_individu = false;//1 individu
        $trouve_individus = false;//plusieurs individus

        list($membre, $individu) = membre_recherche($tab_l['nom_famille'],
            $tab_l['prenom'],
            $tab_l['id_membre'],
            $tab_l['id_individu'],
            $tab_l['email'],
            $tab_l['identifiant_interne']
        );
        if ($individu and isset($tab_l['id_individu']) and $individu->getIdindividu() <> $tab_l['id_individu'] and $tab_l['id_individu'] >0 ) {
            $message .= '<br>Distortion entre l\'individu titulaire dans la base et celui present dans l\'import : ligne ' . $num_ligne . ' base  :' . $individu->getIdindividu() . '  import :' . $tab_l['id_individu'];
            $nb['erreurs']++;
        }
//        echo '<br>ligne 323  id membre :'.$tab_l['id_membre'].' id_individu : '.$tab_l['id_individu'].'<br>';
         if ($tab_l['id_membre']) {


            $tab_l['compte_rendu_membre'] = 'mise a jour du membre ' . $tab_l['id_membre'] . '-' . $tab_l['nom_famille'];
        } else {
            $tab_l['compte_rendu_membre'] = 'creation du membre ' . $tab_l['nom_famille'] . ' ' . $tab_l['prenom'];
            if (!$membre) {
                $nb['ajout_membre']++;
                $a_membre[$nb['ajout_membre']]['ligne'] = $num_ligne;
//                $a_membre[$nb['ajout_membre']]['nom'] = $tab_l['nom'];
                $a_membre[$nb['ajout_membre']]['nom'] = $tab_l['nom_famille'] . ' ' . $tab_l['prenom'];
                $a_membre[$nb['ajout_membre']]['id'] = 'new';//$res['id_auteur'];
                $a_membre[$nb['ajout_membre']]['objet'] = 'membre';
//                $a_membre[$nb['ajout_membre']]['observation'] = $tab_l['observation'];
                // SPIP if ($tab_l['id_mot_asso_membre']) $a_membre[$nb['ajout_membre']]['mot_membre']=$tab_l['id_mot_asso_membre'];

            }
        }
//        echo '<br>ligne 343  id membre :'.$tab_l['id_membre'].' id_individu : '.$tab_l['id_individu'].'<br>';
//        var_dump($individu);
//        echo '<br><br>ligne 344  nom :'.$tab_l['nom'].' nom_famille : '.$tab_l['nom_famille'].' prenom : '.$tab_l['prenom'].'<br><br>';
//        var_dump($membre);
//        if ($num_ligne==16) exit;
        if ($individu) {
            $tab_individu = $individu->toArray();
            remplace_par($num_ligne,$nb['m_nom'], $m_nom, $tab_individu, $tab_l, 'nom_famille', 'individu', true);
            remplace_par($num_ligne,$nb['m_nom'], $m_nom, $tab_individu, $tab_l, 'nom', 'individu', true);
            remplace_par($num_ligne,$nb['m_nom'], $m_nom, $tab_individu, $tab_l, 'prenom', 'individu', true);
            remplace_par($num_ligne,$nb['m_individu'], $m_individu, $tab_individu, $tab_l, 'codepostal', 'individu', false);
            remplace_par($num_ligne,$nb['m_individu'], $m_individu, $tab_individu, $tab_l, 'email', 'individu', false);
            remplace_par($num_ligne,$nb['m_individu'], $m_individu, $tab_individu, $tab_l, 'adresse', 'individu', false);
            remplace_par($num_ligne,$nb['m_individu'], $m_individu, $tab_individu, $tab_l, 'adresse_cplt', 'individu', false);
            remplace_par($num_ligne,$nb['m_individu'], $m_individu, $tab_individu, $tab_l, 'ville', 'individu', true);
//                remplace_par($num_ligne,$nb['m_individu'],$m_individu, $tab_individu, $tab_l, 'telephone', 'individu', false);
            remplace_par($num_ligne,$nb['m_individu'], $m_individu, $tab_individu, $tab_l, 'mobile', 'individu', false);
            $tab_l['compte_rendu_individu'] = 'mise à jour individu -' . $tab_l['id_individu'] . '-' . $tab_l['nom_famille'];
        } else {
            $nb['ajout_individu']++;
            $a_individu[$nb['ajout_individu']]['ligne'] = $num_ligne;
            $a_individu[$nb['ajout_individu']]['nom'] = $tab_l['nom'];
            $a_individu[$nb['ajout_individu']]['nom_famille'] = $tab_l['nom_famille'];
            $a_individu[$nb['ajout_individu']]['prenom'] = $tab_l['prenom'];
            $a_individu[$nb['ajout_individu']]['codepostal'] = $tab_l['code_postal'];
            $a_individu[$nb['ajout_individu']]['email'] = $tab_l['email'];
            $a_individu[$nb['ajout_individu']]['ville'] = $tab_l['ville'];
            $a_individu[$nb['ajout_individu']]['adresse'] = $tab_l['adresse'];
            if (isset($tab_l['adresse_cplt']) and $tab_l['adresse_cplt']>'  !')
            $a_individu[$nb['ajout_individu']]['adresse'] = ajoute_rc($a_individu[$nb['ajout_individu']]['adresse'],$tab_l['adresse_cplt']);
            //todo xyz car sans cela la chargement dans la fenetre simplasso_inporter_trouver_id_maj ajoute <p> en début et </p> en fin de adresse ??
            $a_individu[$nb['ajout_individu']]['telephone'] = $tab_l['telephone'];
            $a_individu[$nb['ajout_individu']]['mobile'] = $tab_l['mobile'];
            $tab_l['compte_rendu_individu'] = 'creation de l\'individu ' . $tab_l['nom_famille'] . ' ' . $tab_l['prenom'];
            $a_individu[$nb['ajout_individu']]['observation'] = $tab_l['observation_i'];
            //todo voir parametrage pour individu ou membre dans les étapes 4 et 5 ici en dur dans 6
            if ($tab_l['id_mot1']) {
                $a_individu[$nb['ajout_individu']]['mot_individu'] = $tab_l['id_mot1'];
            }
            if ($tab_l['id_mot2']) {
                $a_individu[$nb['ajout_individu']]['mot_individu'] = $tab_l['id_mot2'];
            }
            $a_individu[$nb['ajout_individu']]['id'] = 'new';
            $a_individu[$nb['ajout_individu']]['objet'] = 'individu';
            $tab_l['compte_rendu_individu'] = 'creation individu ' . $tab_l['nom_famille'] . ' ' . $tab_l['prenom'];
        }
        $tab_l['OK_mem'] = "bon";
        $tab_l['OK_ind'] = "bon";
    }
    if ($tab_l['OK_mem'] != "NC") {
        if (isset($tab_entete['option1'])) {
            if (strtoupper(trim($tab_l['OK_cot'])) == "OK") {
                $tab_l['OK_cot'] = 'ok';
            } else {
                $ancien = cotisation_validite($num_ligne, $tab_l, $message, 1, 'cot');
//                $ancien = cotisation_validite($num_ligne, $tab_l, $message, $tab_entete['cotp_defaut'], 'cot');
                if ($ancien) {
                    $nb['m_cot']++;
                    $m_cot[$nb['m_cot']] = array(
                        'ligne' => $num_ligne,
                        'id' => $tab_l['cotid_servicerendu'],
                        'remplace' => $ancien,
                        'par' => $tab_l['cotid_prestation'],
                        'champ_destination' => 'id_prestation',
                        'objet' => 'servicerendu',
                        'inform' => $tab_l['nom'] . " " . $tab_l['ville'],
                        //todo  dans l'utilisation les informations de l'enregistrement sont dans la lignes
                    );
                }
            }
            if ($tab_l['OK_cot'] != "NC") {
                if (strtoupper(trim($tab_l['OK_cotp'])) == "OK") {
                    $tab_l['OK_cotp'] = 'ok';
                } else {
                    //todo ajouter condition OK ok ou bon de cotisation
                    paiement_validite($num_ligne, $tab_l, $message, $tab_entete['cott_defaut'], 'cot', $nb['cotp'], $m_cotp);
                }
            }
            mot_validite($tab_l['cotid_mot'], $tab_l['cotnom_mot'], $tab_entete['motcot']);

        }
        if(isset($tab_entete['option2'])) {
           // option adhésion a faire
        }
    }
           // SPIP pour mémoire
//           	//plusieurs auteurs meme nom meme prénom ou meme email
//                    if ($trouve_auteurs){
//                        $res1 = sql_select( $colonne_recupere_de_la_base,$fichier ,$where);
//                        while($res2 = sql_fetch($res1)){
//                            $nb_plusieurs++;
//                            $auteurs_plusieurs[$nb_plusieurs]['id_auteur']=$res2['id_auteur'];
//                            $auteurs_plusieurs[$nb_plusieurs]['nom_famille']=$res2['nom_famille'];
//                            $auteurs_plusieurs[$nb_plusieurs]['nom_fichier']=$tab['nom_famille'];
//                            $auteurs_plusieurs[$nb_plusieurs]['prenom']=$res2['prenom'];
//                            $auteurs_plusieurs[$nb_plusieurs]['prenom_fichier']=$tab['prenom'];
//                            $auteurs_plusieurs[$nb_plusieurs]['email']=$res2['email'];
//                            $auteurs_plusieurs[$nb_plusieurs]['email_fichier']=$tab['email'];
//                            $auteurs_plusieurs[$nb_plusieurs]['ligne_fichier']=$tab['ligne'];
//                            $auteurs_plusieurs[$nb_plusieurs]['ligne']=$nb_plusieurs;
//                        }
//                    } //fin plusieurs auteurs

//                    $where_membre='al.id_auteur='.sql_quote($res['id_auteur']).' and al.objet=\'asso_membre\' and al.id_objet=m.id_asso_membre';
//                    $res_nbm = sql_fetsel('count(*) as nbm',$fichier_membre ,$where_membre);
//// Ajouter la recherche par l'id membre si renseignée Signaler l'écart si c'est le cas
//                    if ($res_nbm['nbm']!='1') {
//                        if ($res_nbm['nbm']>1){
//                            $resm = sql_select($colonne_membre,$fichier_membre ,$where_membre);
//                            while($membre = sql_fetch($resm)){
//                                $nbm_plusieurs++;
//                                $membres_plusieurs[$nbm_plusieurs]['id_asso_membre']=$membre['id_asso_membre'];
//                                $membres_plusieurs[$nbm_plusieurs]['id_auteur_titulaire']=$membre['id_auteur_titulaire'];
//                                $membres_plusieurs[$nbm_plusieurs]['nom']=$membre['nom'];
//                                $membres_plusieurs[$nbm_plusieurs]['id_auteur']=$res['id_auteur'];
//                                $membres_plusieurs[$nbm_plusieurs]['nom_famille']=$res['nom_famille'];
//                                $membres_plusieurs[$nbm_plusieurs]['prenom']=$res['prenom'];
//                                $membres_plusieurs[$nbm_plusieurs]['nom_fichier']=$tab['nom_famille'];
//                                $membres_plusieurs[$nbm_plusieurs]['prenom_fichier']=$tab['prenom'];
//                                $membres_plusieurs[$nbm_plusieurs]['ligne_fichier']=$tab['ligne'];
//                                $membres_plusieurs[$nbm_plusieurs]['ligne']=$nbm_plusieurs;
//                                $message.="plus d'1 membre";
//                            }
//                        }



    if ($tab_l['OK_mem'] == "NC")  $nb['non_conforme']++;
    if ($tab_l['OK_mem'] == "ok")  $nb['deja_enregistre']++;
    return ;
}



function import_controle_ligne_individu(
    $num_ligne,    &$tab_l,   $tab_entete,    &$nb,    &$message,
    &$m_nom,    &$m_individu,    &$a_individu) {

    import_ligne_validite_individu($num_ligne, $tab_l, $message);
    if ($tab_l['OK_ind'] == "atraiter") {
        $nb['lignes_traitees']++;
        $trouve_individu = false;//1 individu
        $trouve_individus = false;//plusieurs individus
        $individu= individu_recherche($tab_l['nom_famille'],
            $tab_l['prenom'],
            $tab_l['id_individu'],
            $tab_l['email'] );
        if ($individu) {
            $tab_individu = $individu->toArray();
            remplace_par($num_ligne,$nb['m_nom'], $m_nom, $tab_individu, $tab_l, 'nom_famille', 'individu', true);
            remplace_par($num_ligne,$nb['m_nom'], $m_nom, $tab_individu, $tab_l, 'nom', 'individu', true);
            remplace_par($num_ligne,$nb['m_nom'], $m_nom, $tab_individu, $tab_l, 'prenom', 'individu', true);
            remplace_par($num_ligne,$nb['m_individu'], $m_individu, $tab_individu, $tab_l, 'codepostal', 'individu', false);
            remplace_par($num_ligne,$nb['m_individu'], $m_individu, $tab_individu, $tab_l, 'email', 'individu', false);
            remplace_par($num_ligne,$nb['m_individu'], $m_individu, $tab_individu, $tab_l, 'adresse', 'individu', false);
            remplace_par($num_ligne,$nb['m_individu'], $m_individu, $tab_individu, $tab_l, 'ville', 'individu', true);
            remplace_par($num_ligne,$nb['m_individu'],$m_individu, $tab_individu, $tab_l, 'telephone', 'individu', false);
            remplace_par($num_ligne,$nb['m_individu'], $m_individu, $tab_individu, $tab_l, 'mobile', 'individu', false);
            $tab_l['compte_rendu_individu'] = 'modification individu ' . $tab_l['nom_famille'] . ' ' . $tab_l['prenom'];
         } else {
            $nb['ajout_individu']++;
            $a_individu[$nb['ajout_individu']]['ligne'] = $num_ligne;
            $a_individu[$nb['ajout_individu']]['nom'] = $tab_l['nom'];
            $a_individu[$nb['ajout_individu']]['nom_famille'] = $tab_l['nom_famille'];
            $a_individu[$nb['ajout_individu']]['prenom'] = $tab_l['prenom'];
            $a_individu[$nb['ajout_individu']]['codepostal'] = $tab_l['code_postal'];
            $a_individu[$nb['ajout_individu']]['email'] = $tab_l['email'];
            $a_individu[$nb['ajout_individu']]['ville'] = $tab_l['ville'];
            $a_individu[$nb['ajout_individu']]['adressexyz'] = $tab_l['adresse'];//todo xyz car sans cela la chargement dans la fenetre simplasso_inporter_trouver_id_maj ajoute <p> en début et </p> en fin de adresse ??
            $a_individu[$nb['ajout_individu']]['telephone'] = $tab_l['telephone'];
            $a_individu[$nb['ajout_individu']]['mobile'] = $tab_l['mobile'];
            if (isset( $tab_l['observation_i'])) $a_individu[$nb['ajout_individu']]['observation'] = $tab_l['observation_i'];
            if (isset( $tab_l['observation_i2'])) $a_individu[$nb['ajout_individu']]['observation'] = $tab_l['observation_i2'];
            $a_individu[$nb['ajout_individu']]['id'] = 'new';
            $a_individu[$nb['ajout_individu']]['objet'] = 'individu';
            $tab_l['compte_rendu_individu'] = 'creation individu ' . $tab_l['nom_famille'] . ' ' . $tab_l['prenom'];
        }
        $tab_l['OK_ind'] = "bon";
    }
    if ($tab_l['OK_ind'] != "NC") {
        mot_validite($tab_l['id_mot1'], $tab_l['nom_mot1'], $tab_entete['mot1']);
        mot_validite($tab_l['id_mot2'], $tab_l['nom_mot2'], $tab_entete['mot2']);
    }
    if ($tab_l['OK_ind'] == "NC")  $nb['non_conforme']++;
    if ($tab_l['OK_ind'] == "ok")  $nb['deja_enregistre']++;
    return ;
}

function import_controle_reprise($num_ligne, &$tab_l, $tab_entete,  &$nb, &$message )
{
//    echo '<br>-------<br>-------<br>ligne 304 Import controle ligne membre Debut <br>  id membre :'.$tab_l['id_membre'].' ind : '.$tab_l['id_individu'].'<br>';
    import_ligne_validite_membre($num_ligne, $tab_l, $message);//qui appele individu
    if ($tab_l['OK_mem'] == "atraiter") {
        $nb['lignes_traitees']++;
        //todo creation de la famille
        // et un ' & ' ou ' et ' ou ', ' dans le prénom : Anny & Nathanaël : Philippe et Elisabeth
        // si 2 ', ' 3 prénoms : Philippe, Isabelle, Emma : Mirabelle, Raphaël, Marion et Valentine
        // et max de nbadh1994 a nbadh2017

        // civilite Fam. ? Mme et M. Ok pour l'ordre des prénoms mais pas  forcement de prénoms

        $trouve_individu = false;//1 individu
        $trouve_individus = false;//plusieurs individus

//        echo '<br>ligne 323  id membre :'.$tab_l['id_membre'].' id_individu : '.$tab_l['id_individu'].'<br>';
        $tab_mot=
        $tab_l['compte_rendu_membre'] = 'creation du membre ' . $tab_l['nom_famille'] . ' ' . $tab_l['prenom'];
        $nb['ajout_membre']++;
        $nb['ajout_individu']++;
        $tab_l['compte_rendu_individu'] = 'creation individu ' . $tab_l['nom_famille'] . ' ' . $tab_l['prenom'];
        $tab_l['OK_mem'] = "bon";
        $tab_l['OK_ind'] = "bon";
    }
    if ($tab_l['OK_mem'] == "NC")  $nb['non_conforme']++;
    if ($tab_l['OK_mem'] == "ok")  $nb['deja_enregistre']++;
    return ;

}
function import_individu()
{
    return "En_cours ligne 522 de inc/imports.php";
}
function import_individu_spip_auteur()
{
    return "En_cours a travers le tunnel</br> ligne 527 de inc/imports.php";
    global $app;
    //todo reste à verifier les créations d'individu en direct une reprise devrait etre lancé en fond au clic sur le bouton ajou de membre et d'individu
    $statement=$app['db']->executeQuery("SELECT max( id_individu ) as id_individu FROM asso_individus");
    $premier_individu_non_connu=$statement->fetch()['id_individu'];
 // todo a faire par le tunnel
    $statement=$app['db']->executeQuery("SELECT max( id_auteur ) as id_auteur FROM spip_auteurs");
    $dernier_auteur=$statement->fetch()['id_auteur'];
    $premier_individu_non_connu++;
    $cree=array();
    for ($i = $premier_individu_non_connu; $i <= $dernier_auteur; $i++) {
        $sql = "SELECT * FROM spip_auteurs where id_auteur=" . $i;
        $statement = $app['db']->executeQuery($sql);
        $val = $statement->fetch();
        if (isset($val) and $val['id_auteur'] == $i) {
            $objet_data = new Individu();
            $objet_data->fromArray($val);
            $objet_data->setnomFamille($val['nom']);
            autoincrement($i,"individu");
            $objet_data->save();
            Log::EnrOp(false, 'individu',  $i);
            $cree[] = $objet_data->getIdIndividu().' '.$val['nom'];

        }

    }
    return "Les contacts ".implode(", ",$cree).' ont été crées';
}
function import_lecture_entete($fichier,&$proposition, $operation,$id_import)
{
    $temp = array();
    $temp_v = array();
    $k = 0;
    $tca = array(
        'id_individu' => 'id_individu',
        'id_membre' => 'id_membre',
        'OK_maitre' => 'OK_maitre',
        'OK_ind' => 'OK_ind',
        'compte_rendu_individu' => 'compte_rendu_individu',
// ajout mis dans les champs a conserver ??
        'id_mot1' => 'id_mot1',
        'nom_mot1' => 'nom_mot1',
        'id_mot2' => 'id_mot2',
        'nom_mot2' => 'nom_mot2',
        'id_mot3' => 'id_mot3',
        'nom_mot3' => 'nom_mot3'
    );
    switch ($operation){
        case 'membre' :
 			$tca['OK_mem'] = 'OK_mem';
 			$tca['OK_cot'] = 'OK_cot';
 			$tca['OK_cotp'] = 'OK_cotp';
 			$tca['OK_adh'] = 'OK_adh';
 			$tca['OK_adhp'] = 'OK_adhp';
 			$tca['compte_rendu_membre'] = 'compte_rendu_membre';
 			$tca['compte_rendu_cot'] = 'compte_rendu_cot';
			$tca['compte_rendu_cotp'] = 'compte_rendu_cotp';
			$tca['compte_rendu_adh'] = 'compte_rendu_adh';
			$tca['compte_rendu_adhp'] = 'compte_rendu_adhp';
			$tca['compte_rendu_cot'] = 'compte_rendu_cot';
			$tca['cotid_mot'] = 'cotid_mot';
			$tca['cotnom_mot'] = 'cotnom_mot';
			$tca['cotid_paiement'] = 'cotid_paiement';
			$tca['cotid_servicerendu'] = 'cotid_servicerendu';
			$tca['cotid_tresor'] = 'cotid_tresor';
			$tca['adhid_paiement'] = 'adhid_paiement';
			$tca['adhid_servicerendu'] = 'adhid_servicerendu';
            $tca['adhid_tresor'] = 'adhid_tresor';
            $tca['adhid_mot'] = 'adhid_mot';
            $tca['adhnom_mot'] = 'adhnom_mot';
			$tca['cotmontanttheorique'] = 'cotmontanttheorique';
			$tca['adhmontanttheorique'] = 'adhmontanttheorique';
        case 'ecriture':
            $tab_fichier = array();
            // les champs nommés ici sont transmis de tab dans tab_value
            $tca['libelle'] = 'libelle';
            $tca['classement'] = 'classement';
            $tca['observation'] = 'observation';
        case 'reprise':
            $tca['libelle'] = 'libelle';
            $tca['classement'] = 'classement';
            $tca['observation'] = 'observation';
            $tca['compte_rendu_cot'] = 'compte_rendu_cot';
            $tca['compte_rendu_cotp'] = 'compte_rendu_cotp';
            $tca['compte_rendu_don'] = 'compte_rendu_don';
            $tca['compte_rendu_donp'] = 'compte_rendu_donp';
            $tca['compte_rendu_abo'] = 'compte_rendu_abo';
            $tca['compte_rendu_abop'] = 'compte_rendu_abop';
    }
   //todo idem dans un premier temp ajoute des colonnes inutiles mais ne gene pas le fonctionnement
   $param = pref('membre_import');
    foreach ($param['nom_colonne'] as $n => $v) {
        if ($v) {
            $tca[$v] = $n;
        }
    }
    $ligne_titres = $param['nb_lentete'];
    $debut=1;
    $nb['erreurs'] = 0;
    $nb['colonnes'] = 100;
    $nb['curseur']=0;
    $extension=substr($fichier, -4);// premiere lecture
    switch ($extension) {
        case  '.csv' :
            $original=lecture_fichier_csv_entete($fichier,$debut,$ligne_titres,$nb['curseur'],$id_import,$message);
            if (!$original)
                $nb['erreurs']++;
                $proposition['message'] .= $message.PHP_EOL;
            break;
         case "xlsx":
            $original = lecture_fichier_excel($fichier, $typefichier = 'Excel2007');
            break;
         default ://todo une seule fois
             $proposition['message'] .= "extention non connue ".$extension.PHP_EOL;
    }
    $nb['colonnes'] = count($original[$ligne_titres]);
    // les colonnes du fichier
    if($original) {
        for ($colonne = 0; $colonne < $nb['colonnes']; $colonne++) {
            $cellule=import_lecture_cellule($original,$extension,$colonne,$ligne_titres);
             if (import_lecture_entete_premier_mot($cellule)) {// pour eviter les phrases
                $k = 0;
                 if ($operation=='reprise') {
                     $cellule=strtolower($cellule);
                     $tca[$cellule] = $cellule;
                 }
                if (isset($tca[$cellule])) {
                    $temp_n[$colonne]=array_search($tca[$cellule], $tca);
                    $colonnes[$colonne] = array(
                        "colonne" => $temp_n[$colonne],
                        "variable" => $tca[$cellule]
                    );
                   $temp_v[$colonne] = $tca[$cellule];
                }
//                 Todo vient de XLSX  a retester
//            } else {
//                $k++;
//                if ($k > 10) {//nb de cellule vide consecutive sur la ligne des noms(10)
//                    $derniere_colonne = $colonne - 9;
//                    $colonne = 100;
//                }
            }
        }
//        arbre(count($colonnes)); arbre(count($temp_n)); arbre(count($temp_v));
        $i=0;
        $derniere_colonne=$nb['colonnes'];// ajoute les colonnes non trouvées
        foreach ($tca as $n => $v) {

            if (!in_array($v, $temp_v)) {
                $colonnes[$derniere_colonne] = array("colonne" => $n, "variable" => $v);
                $temp_n[$derniere_colonne] = $n;
                $temp_v[$derniere_colonne] = $v;
                $derniere_colonne++;
            }
            $i++;
        }
    }
//    arbre($temp_v);arbre($temp_n); arbre($colonnes);
//    arbre(count($colonnes));arbre(count($temp_n)); xe(count($temp_v));
    importlignes_creation($id_import,$ligne_titres,'E', $temp_n, 'imports_entete');
    importlignes_creation($id_import,$ligne_titres-1,'E', $temp_v, 'imports_entete');
    $entete='';
    if ($operation='reprise') {
        for ($groupe = 1; $groupe < 14; $groupe++) {
            $entete['mot'.$groupe] = mot_charge_motgroupes($groupe);
        }
    }else{
        // todo modifier la saisie de pref avec l'entete du fichier  colonnes 0=A 1=B ligne 1=1 si cellule non vide
        $param['cellule'] = array( //todo modifier la saisie de pref pour obtenir ce tableaux et faire des param différend pour chaque imports
            // todo les colonnes sont a ratacher aux objets avec obligatoire[ avec indication si c'es ou  id ou nom ou email ou valeur défaut ], facultatif, non utilisées
// todo individu
            array('fonction' => 'cel', 'colonne' => 2, 'ligne' => 8, 'index' => 'mot_motgroupe1', 'defaut' => '0'),
            array('fonction' => 'cel', 'colonne' => 3, 'ligne' => 8, 'index' => 'mot_motgroupe2', 'defaut' => '0'),
            array('fonction' => 'cel', 'colonne' => 4, 'ligne' => 8, 'index' => 'mot_motgroupe3', 'defaut' => '0'),
            array('fonction' => 'cel', 'colonne' => 12, 'ligne' => 3, 'index' => 'option1', 'defaut' => '1'),
            array('fonction' => 'cel', 'colonne' => 12, 'ligne' => 4, 'index' => 'option2', 'defaut' => '0'),
// todo service rendu
            array('fonction' => 'cel', 'colonne' => 23, 'ligne' => 3, 'index' => 'mot_motgroupecot', 'defaut' => '3'),
            array('fonction' => 'cel', 'colonne' => 23, 'ligne' => 5, 'index' => 'mot_motgroupeadh', 'defaut' => '0'),
// todo individu
            array('fonction' => 'mot', 'colonne' => 2, 'ligne' => 8, 'valeur' => 'mot_motgroupe1', 'option' => "1"),
            array('fonction' => 'mot', 'colonne' => 3, 'ligne' => 8, 'valeur' => 'mot_motgroupe2', 'option' => "2"),
            array('fonction' => 'mot', 'colonne' => 4, 'ligne' => 8, 'valeur' => 'mot_motgroupe3', 'option' => "3"),
// todo service rendu
            array(
                'fonction' => 'mot',
                'colonne' => 2,
                'ligne' => 9,
                'valeur' => 'mot_motgroupecot',
                'condition_nom' => "option1",
                'defaut' => '0',
                'option' => "cot"
            ),
            array(
                'fonction' => 'mot',
                'colonne' => 3,
                'ligne' => 9,
                'valeur' => 'mot_motgroupeadh',
                'condition_nom' => "option2",
                'defaut' => '0',
                'option' => "adh"
            ),
            array(
                'fonction' => 'opt',
                'colonne' => 19,
                'ligne' => 3,
                'index' => 'cotp_defaut',
                'condition_nom' => "option1"
            ),
            array(
                'fonction' => 'opt',
                'colonne' => 19,
                'ligne' => 4,
                'index' => 'cott_defaut',
                'condition_nom' => "option1"
            ),
            array(
                'fonction' => 'opt',
                'colonne' => 19,
                'ligne' => 5,
                'index' => 'adhp_defaut',
                'condition_nom' => "option2"
            ),
            array(
                'fonction' => 'opt',
                'colonne' => 19,
                'ligne' => 6,
                'index' => 'adht_defaut',
                'condition_nom' => "option2"
            ),
        );
        foreach ($param['cellule'] as $temp) {
            $defaut = import_lecture_cellule($original, $extension, $temp['colonne'], $temp['ligne']);
            switch ($temp['fonction']) {
                case 'cel':
                    $entete[$temp['index']] = ($defaut > '!') ? $defaut : $temp['defaut'];
                    break;
                case 'mot':
                    if (!isset($temp['condition_nom']) or (isset($entete[$temp['condition_nom']]) and $entete[$temp['condition_nom']])) {
                        $entete['mot' . $temp['option']] = mot_charge_motgroupes($entete[$temp['valeur']],true,true, $defaut);
                    }
//todo a revoir si necessaire
//               if (!isset($proposition['entete']['mot'.$param['ajoute']['option'].'_nom_defaut'])) {
//                    $proposition['entete']['erreur'] = 'le mot ' . $original->getActiveSheet()->getCell('C9')->getValue() . ' n est reconnu dans le groupe :' . $param['cotid_motgroupe'];
                    break;
                case 'opt':
                    if ($entete[$temp['condition_nom']]) {
                        $entete[$temp['index']] = ($defaut > '!') ? $defaut : $temp['defaut'];
                    }
                    break;
            }
        }
    }
    $nb['ligne_entete'] = $param['nb_lentete'];
    $nb['lignes_lues'] = $param['nb_lentete'];
    $nb['lignes_traitees'] = 0;
    $nb['lignes_controlees'] = $param['nb_lentete'];
    $nb['lignes_enregistrees'] = $param['nb_lentete'];
    $nb['lignes_compte_rendu'] = $param['nb_lentete']-2;
    $nb['deja_enregistre'] = 0;
    $nb['mem'] = 0;
    $nb['ind'] = 0;
    $nb['cot'] = 0;
    $nb['cotp'] = 0;
    $nb['abo']=0;
    $nb['abop']=0;
    $nb['ven']=0;
    $nb['venp']=0;
    $nb['don']=0;
    $nb['donp']=0;
    $nb['per']=0;
    $nb['perp']=0;
    $nb['adh'] = 0;
    $nb['adhp'] = 0;
    $nb['non_conforme'] = 0;
    $nb['ajout_individu'] = 0;
    $nb['ajout_membre'] = 0;
    $nb['m_nom'] = 0;
    $nb['m_individu'] = 0;
    $nb['m_cot'] = 0;
    $nb['m_cotp'] = 0;
    $nb['maitre']=0;
    $proposition['param'] = $param;
    $proposition['colonnes'] = $colonnes;
    $proposition['entete'] = $entete;
    $proposition['nb'] = $nb;
    return;

}
function import_lecture_cellule($original,$extension,$colonne,$ligne)
{
    switch ($extension) {
        case  '.csv' :
            return $original[$ligne][$colonne];
        case "xlsx":
            return $original->getActiveSheet()->getCell([$colonne][$ligne])->getValue();
    }
}
function import_lecture_entete_premier_mot(&$cellule = '')
{
//todo pour les titres de colonnes sur plusieurs mots et/ou lignes
    if (isset($cellule) and $cellule >= ' !') {
        $pos = strpos($cellule, " ");
        if ($pos === false) $pos = strpos($cellule, "\n");
        if ($pos === false) $pos = strpos($cellule, "\r");
        if ($pos === false) $pos = strpos($cellule, "\t");
        if (!$pos === false) $cellule = trim(substr($cellule, 0, $pos));
        return $cellule;
    } else {
        return false;
    }
}
function import_lignes($fichier,$extension,$id_import,$debut,$fin,$colonnes,$operation,&$curseur,$nb_colonnes){
    global $app;
    switch ($extension) {
        case  '.csv' :
        list($original,$messages,$curseur)=lecture_fichier_csv($fichier,$debut,$fin,$curseur,$id_import);
        for ($num_ligne = $debut; $num_ligne <= $fin; $num_ligne++) {
             if (is_array($original[$num_ligne])){
                 $tab = array();
                 foreach ($colonnes as $i=>$v)
                 if( $i < $nb_colonnes)
                        $tab[$colonnes[$i]['variable']] = $original[$num_ligne][$i];
                 else $tab[$colonnes[$i]['variable']]='';
                    if ($tab['OK_ind']=='OK'){ $tab['OK_ind']=='ok'; }
                $tab['compte_rendu_individu'] = 'non connu';//todo utilité ?
                //todo les colonnes obligatoire valable pour membre, individu, adav reste a modifier pour compte et écriture
                 if ($tab['nom_famille'] <= '!!!' and $tab['email'] <= '!' and !$tab['id_individu']) {
                    return $num_ligne - 1;// cette ligne n'est pas valide et ne peu etre intégré
                }

             }else{
                return $num_ligne - 1;
            }
        }
        break;
        case "xlsx":
        $original = lecture_fichier_excel($fichier, $typefichier = 'Excel2007');
        for ($num_ligne = $debut; $num_ligne <= $fin; $num_ligne++) {
            $tab = array();
            foreach ($colonnes as $i => $v) {
                if (isset($v['variable'])) {
                    $cell = $original->getActiveSheet()->getCellByColumnAndRow($i, $num_ligne);
                    If (!Is_null($cell)) {
                        If (PHPExcel_Shared_Date::isDateTime($cell)) {
                            // Cell contient une valeur de date qui doit être reformatée
                            $cellValue = PHPExcel_Shared_Date::ExcelToPHPObject($cell->getValue())->format('Y-m-d');
                        } else {
                            $cellValue = $cell->getFormattedValue();
                        }
                        //                    if ($num_ligne > 44)  Echo $i . '-' . $num_ligne . '-' . $v['variable'] . ' : ' . $cellValue . '--' . $cell->getValue() . '</br>';
                    }
                }
                $tab[$v['variable']] = $cellValue;
            }
            //      if  ($num_ligne > 44) arbre($tab);
            $tab['OK_ind'] = strtoupper($tab['OK_ind']);//en majuscules
            $tab['compte_rendu_individu'] = 'non connu';//todo utilité ?
            //todo les colonnes obligatoire
            if ($tab['nom_famille'] <= '!!!' and $tab['email'] <= '!' and !$tab['id_individu']) {
                return $num_ligne - 1;
            }
            importlignes_creation($id_import, $num_ligne, 'D', $tab, 'import_lignes');
        }
            break;
    }
   return $fin;
}

function import_ligne_validite_membre($num_ligne, &$tab, &$message)
{
    import_ligne_validite_individu($num_ligne, $tab, $message);
    if (isset($tab['OK_mem']) and strtoupper(trim($tab['OK_mem'])) == 'OK') {
        $tab['compte_rendu_membre'] = 'Traité antérieurement :'.$tab['compte_rendu_membre'];
        $tab['OK_mem'] = 'ok';
    } else {

        if ($tab['id_membre'] >= 1 or $tab['OK_ind'] == "atraiter" ) {
            $tab['OK_mem'] = "atraiter";
         } else {
            $tab['compte_rendu_membre'] = 'Pas assez de données indispensables';
            ($message .= '<br>' . $tab['compte_rendu_membre'] . ' pour la ligne' . $num_ligne);
            $tab['OK_mem'] = 'NC';
         }
    }
    return;
}
function import_ligne_validite_individu($num_ligne, &$tab, &$message)
{
    if (isset($tab['OK_ind']) and strtoupper(trim($tab['OK_ind'])) == 'OK') {
        $tab['compte_rendu_individu'] = 'Traité antérieurement :'.$tab['compte_rendu_individu'];
        $tab['OK_ind'] = 'ok';
    } else {
        //formate le nom Todo Voir si un parametrage  est utile aujourd'hui 15/12/2016 Nom majuscules + 1° lettre du prenom
        // todo pas de traitement des espace tiret et autre lettre majuscule du prénom
        $tab['nom_famille'] = strtoupper(trim($tab['nom_famille']));
        $tab['prenom'] = ucwords(strtolower(trim($tab['prenom'])));
        $tab['nom']=$tab['nom_famille'] .' '.$tab['prenom'];
        if (isset($tab['id_individu']) and $tab['id_individu'] >= 1
            or test_email_valide($tab['email'])
            or strlen($tab['nom_famille'] . $tab['prenom']) >= 6
        ) {
            $tab['OK_ind'] = "atraiter";
        } else {
            $tab['compte_rendu_individu'] = 'Pas assez de données indispensables';
            ($message .= '<br>' . $tab['compte_rendu_individu'] . ' pour la ligne' . $num_ligne);
            $tab['OK_ind'] = 'NC';
        }
    }
    return;
}
function import_ligne_validite_reprise($num_ligne, &$tab, &$message)
{
    if (isset($tab['OK_ind']) and strtoupper(trim($tab['OK_ind'])) == 'OK') {
        $tab['compte_rendu_individu'] = 'Traité antérieurement';
        $tab['OK_ind'] = 'ok';
    } else {
        //formate le nom Todo Voir si un parametrage  est utile aujourd'hui 15/12/2016 Nom majuscules + 1° lettre du prenom
        // todo pas de traitement des espace tiret et autre lettre majuscule du prénom
        $tab['nom_famille'] = strtoupper(trim($tab['nom_famille']));
        $tab['prenom'] = ucwords(strtolower(trim($tab['prenom'])));
        $tab['nom']=$tab['nom_famille'] .' '.$tab['prenom'];
        if (isset($tab_l['id_individu']) and $tab['id_individu'] >= 1
            or test_email_valide($tab['email'])
            or strlen($tab['nom_famille'] . $tab['prenom']) >= 6
        ) {
            $tab['OK_ind'] = "atraiter";
        } else {
            $tab['compte_rendu_individu'] = 'Pas assez de données indispensables';
            ($message .= '<br>' . $tab['compte_rendu_individu'] . ' pour la ligne' . $num_ligne);
            $tab['OK_ind'] = 'NC';
        }
    }
    return;
}


/*
function import_modifie($id, $avancement, $original_objet, $proposition, $action, $efface)
{
    $objet_data = ImportQuery::create()->findOneByIdImport($id);
    $modif = $objet_data->getModification();
    if ($avancement) {
        $objet_data->setAvancement($avancement);
        if ($efface) {
            if ($efface == '7compte_rendu' or $efface == '0abandon') {
                unset($modif['etapes']);
            } else {
                unset($modif['etapes'][array_search($efface, $modif['etapes'])]);
            }
        }
    }
     if ($action) {
        $modif[] = array(
            'action' => $avancement,
            'detail' => $action,
            'date' => date('H:i:s'),
            'operateur' => suc('operateur.id')
        );
    }
    $objet_data->setModifications(json_encode($modif));
    if ($original_objet) {
        $objet_data->setOriginals($original_objet);
    }
    if ($proposition) {
        $objet_data->setInfo(json_encode($proposition));
    }
    if (isset($proposition['nb']['lignes_enregistrees'])) {
        $objet_data->setNbLigne($proposition['nb']['maitre']);
    }
    $objet_data->save();
    return;
}
*/

function individu_creation(&$tab)
{
    global $app;
    $objet_data = null;
    if (isset($tab['id_individu']) and intval(($tab['id_individu']) > 0)) {
        $modification = true;
        $objet_data = IndividuQuery::create()->findpk($tab['id_individu']);
    }
    if (!$objet_data){
        $objet_data = new Individu();
        if (conf('identifiant.id_idem')) $new_id_individu=autoincrement();

        $objet_data->setPays(pref('individu.pays'));
        $modification = false;
    }
    if (isset($tab['nom_famille'])and $tab['nom_famille']>='  !') {
        $objet_data->setNomFamille($tab['nom_famille']);
    } elseif ($modification) {
        $tab['nom_famille'] = $objet_data->getNomFamille();
    }
    if (isset($tab['prenom'])and $tab['prenom']>='  !') {
        $objet_data->setPrenom($tab['prenom']);
        $objet_data->setNom($tab['nom_famille'] . ' ' . $tab['prenom']);
        $objet_data->setlogin(strtolower(substr(substr($tab['nom_famille'], 0, 6) . substr($tab['prenom'], 0, 6), 0,
            8)));
    } elseif ($modification) {
        $tab['prenom'] = $objet_data->getPrenom();
    }
    if (isset($tab['email'])and $tab['email']>='  !') {
        $objet_data->setemail($tab['email']);
    } elseif ($modification) {
        $tab['email'] = $objet_data->getEmail();
    }
    if (isset($tab['naissance'])) {
            $objet_data->setNaissance(test_date_valeur($tab['naissance']));
    } elseif ($modification) {
        $tab['naissance'] = $objet_data->getNaissance();
    }
    if (isset($tab['adresse'])and $tab['adresse']>='  !') {
        $objet_data->setAdresse($tab['adresse']);
    } elseif ($modification) {
        $tab['adresse'] = $objet_data->getAdresse();
    }
    if (isset($tab['adresse_cplt'])and $tab['adresse_cplt']>='  !') {
        $objet_data->setAdresseCplt(ajoute_rc($objet_data->getAdresse(),$tab['adresse_cplt']));
    }
    if (isset($tab['codepostal'])and $tab['codepostal']>='  !') {
        $objet_data->setCodepostal($tab['codepostal']);
    } elseif ($modification) {
        $tab['codepostal'] = $objet_data->getCodepostal();
    }
    if (isset($tab['ville'])and $tab['ville']>='  !') {
        $objet_data->setVille($tab['ville']);
    } elseif ($modification) {
        $tab['ville'] = $objet_data->getVille();
    }
    if (isset($tab['telephone'])and $tab['telephone']>='  !') {
        $objet_data->setTelephone($tab['telephone']);
    } elseif ($modification) {
        $tab['telephone'] = $objet_data->getTelephone();
    }
//    'fax' => 'fax',
    if (isset($tab['mobile'])and $tab['mobile']>='  !') {
        $objet_data->setMobile($tab['mobile']);
    } elseif ($modification) {
        $tab['mobile'] = $objet_data->getMobile();
    }
    if (isset($tab['civilite'])and $tab['civilite']>='!') {
        $objet_data->setCivilite($tab['civilite']);
    } elseif ($modification) {
        $tab['civilite'] = $objet_data->getCivilite();
    }
    if (isset($tab['profession'])and $tab['profession']>='!') {
        $objet_data->setProfession($tab['profession']);
    } elseif ($modification) {
        $tab['profession'] = $objet_data->getProfession();
    }

//    'sexe' => 'sexe',
    if (isset($tab['observation_i']) and $tab['observation_i'] > '!') {
        $objet_data->setObservation(ajoute_rc($objet_data->getObservation(),$tab['observation_i']));
    }
    if (isset($tab['observation_i2']) and $tab['observation_i2'] > '!') {
        $objet_data->setObservation(ajoute_rc($objet_data->getObservation(),$tab['observation_i2']));
    }
//    $objet_data->save();
    if (isset($new_id_individu)and $new_id_individu >0) {
        echo ' news individu :'.$new_id_individu.' tab :'.$tab['id_individu'];
        $app['db']->executeQuery("ALTER TABLE asso_individus AUTO_INCREMENT =" . $new_id_individu);
    }
    $objet_data->save();

    $tab['id_individu'] = $objet_data->getIdIndividu();
    echo ' apres id_individu :'.$tab['id_individu'];
    $tab['OK_ind'] = 'OK';
    $tab['compte_rendu_individu'] .= ' n° :' . $tab['id_individu'];
    Log::EnrOp($modification, 'individu', $tab['id_individu']);

    return $tab['id_individu'];
}
function individu_creation_import(&$data,$log=true)
{
    global $app;
    $objet='individu';
    if ($data['id_individu']) {
        $statement = $app['db']->executeQuery("ALTER TABLE asso_individus AUTO_INCREMENT =" . $data['id_individu']);
        $statement = $app['db']->executeQuery("ALTER TABLE asso_membres AUTO_INCREMENT =" . $data['id_individu']);
//        autoincrement($data['id_membre'],"individu",'asso','membre');
    }
    $objet_data = new Individu();
    $id_objet1=$data['id_individu'];
    unset($data['id_individu']);

    if (isset($data['pays'])) {
        if (trim($data['pays']) < '!') {
            $data['pays'] = 'FR';
        } else {
            switch ($data['pays']) {
                case 'Allemagne' :
                    $data['pays'] = "DE";
                    Break;
                 case 'Belgique' :
                    $data['pays'] = "BE";
                    Break;
                case 'Danemark' :
                    $data['pays'] = "DK";
                    Break;
                case 'Suisse' :
                    $data['pays'] = "CH";
                    Break;
                case 'England' :
                    $data['pays'] = "GB";
                    Break;
                case 'Nederland' :
                    $data['pays'] = "NL";
                    Break;
            }
        }
    }

    if (isset($data['activite']) and $data['activite'] > '!') {
        $data['profession'] = ajoute_rc($data['profession'],$data['activite']);
    }
    if (isset($data['telephonebureau']) and trim($data['telephonebureau']) > '!') {
        $data['telephone_pro'] = $data['telephonebureau'];
    }
    if (isset($data['telephone']) and substr(trim($data['telephone']),0,2) =="06") {
        $data['mobile'] = $data['telephone'];
        $data['telephone']='';
    }
//    $data['update_at'] = test_date_valeur($data['datemaj']);
    //vendor surcharge avec la date du jour pae
    $data['created_at'] = test_date_valeur($data['datecreation']);
    $data['naissance'] = test_date_valeur($data['datenaissance']);

    $objet_data->fromArray($data);
    $objet_data->save();
    $id_objet=$objet_data->getIdIndividu();
    if (isset($data['compte_rendu_ind']))
        $data['compte_rendu_ind'] .= ' n° :' .  $id_objet;
    else
        $data['compte_rendu_ind'] = ' n° :' .  $id_objet;
    if($log)
       Log::EnrOp(false, $objet,  $id_objet);

    return $id_objet;

}
function individu_recherche($nom, $prenom, &$id_individu, $email, $id_individu_titulaire=0)
{
    global $app;
    $individu = null;
    if ($id_individu) {
//        echo '<br>ligne 895 id individu :'.$id_individu.' ind titulaire : '.$id_individu_titulaire.' nom ;'.$nom.' prenom : '.$prenom.'<br>';
        $individu = IndividuQuery::create()->findPk($id_individu);
    }
    if (!$individu) {// la validité du mel est testée dans lacceptation de la ligne
        if ($id_individu_titulaire>0) {
//            echo '<br>ligne 900 recherche id_individu_titulaire : ' . $id_individu_titulaire . ' <br>';
            $individu = IndividuQuery::create()->findPk($id_individu_titulaire);
        }
        if (!$individu) {
            if ($email) {
//                echo "<br>ligne 905 recherche email " . $email . ' <br>';
                $individu = IndividuQuery::create()->filterByEmail($email)->findOne();
            }
            if (!$individu) {
//                echo "<br>ligne 909 recherche nom " . $nom . ' ' . $prenom . ' <br>';
                $individu = IndividuQuery::create()->filterByNomFamille($nom)->filterByPrenom($prenom)->findOne();
            }
        }
    }
    if ($individu) {
        $id_individu = $individu->getIdIndividu();
//        arbre('ligne 916 id_individu ' . $id_individu);
    }
    return  $individu;
}
// renvoi la somme des 2 textes avec true s'il faut ecrire
function lecture_fichier_csv_entete($fichier,$debut,$fin,&$curseur,$id_import,&$message)
{
    global $app;
    $data=array();
    $handle = fopen($app['upload.path'] . '/' .$fichier, "rb",true);
    if ($handle !== FALSE) {
        fseek($handle, $curseur);
        for ($i = $debut; $i <= $fin; $i++) {
            $data[$i] = fgetcsv($handle, 4096,chr(9));
        }
        $curseur=ftell($handle);
        fclose($handle);
    }else{
        $message="Le ".$fichier;
        if ($handle===false)
            $message .= ' est vide';
        else
            $message .= ' provoque une erreur de lecture';
    }
    return $data;
}


function lecture_fichier_csv_donnees($fichier,$extension,$id_import,$debut,&$fin,$colonnes,$operation,&$curseur,$nb_colonnes)
{
    global $app;
    $tab=array();
    $handle = fopen($app['upload.path'] . '/' .$fichier, "r",true);
    if ($handle !== FALSE) {
        fseek($handle, $curseur);
        for ($i = $debut; $i <= $fin; $i++) {
            $buffer = fgetcsv ( $handle ,8000 ,chr(9));
            for ($k=0;$k<$nb_colonnes;$k++){
                    $tab[$k] = $buffer[$k];
            }
            if ($buffer=feof($handle)) $fin=$i;
            else importlignes_creation($id_import, $i, 'D',json_encode($tab) , 'import_lignes');
         }
        $curseur=ftell($handle);
        fclose($handle);
    }else{
        $fin++;
    }

}
function lecture_fichier_excel($fichier, $typefichier = 'Excel2007')
{
    global $app;
    $inputFileName = $app['upload.path'] . '/' . $fichier;
    $objReader = PHPExcel_IOFactory::createReader('Excel2007');
    return $objReader->load($inputFileName);

}
//ajoute un rc_entre les 2 valeurs si elles existent
//verifie le doublon
function ajoute_rc($origine, $ajout)
{
    $ajout=trim($ajout);
    if ($origine and $ajout) {
        if (!strpos($origine, $ajout))
            $origine.= chr(10) . $ajout;
    }elseif ($ajout) {
        return $ajout;
    }
    return $origine;
}

function membre_creation(&$tab)
{
    global $app;
    $objet_data = null;
    if (isset($tab['id_membre']) and intval(($tab['id_membre']) > 0)) {
        $modification = true;
        $objet_data = MembreQuery::create()->findpk($tab['id_membre']);
    }
    if (!$objet_data) {
        if (conf('identifiant.id_idem')) {
            if ($tab['id_individu']) {
                $tab['id_membre'] =0;
                $individu = IndividuQuery::create()->findpk($tab['id_individu']);
                if ($individu) {
                    $tab['id_membre'] = $tab['id_individu'];
                 }
            } if(!intval($tab['id_membre']))
                $tab['id_membre'] = autoincrement(false, 'individu', 'membre');
        }
        $modification = false;
        $objet_data = new Membre();
        $objet_data->setIdMembre($tab['id_membre']);
    }else{
        $objet_data = MembreQuery::create()->findpk($tab['id_membre']);
        if (!$objet_data) {
            $tab['compte_rendu_membre'] .= ' ligne 1451 pas de creation de membre  n° :' .$tab['id_membre'].' id_individu :'.$tab['id_individu'];
//            return;
        }
    }
    if (strlen($tab['nom_famille'] . ' ' . $tab['prenom']) > 6) {
        $objet_data->setNom($tab['nom_famille'] . ' ' . $tab['prenom']);
        $objet_data->setnomcourt(MembreQuery::genererNomCourt($tab['nom_famille'], $tab['prenom']));
    } elseif ($modification) {
        $tab['nom'] = $objet_data->getNom();
    }
    //ajoute le texte s'il n'existe pas avec un retour a la ligne
    $objet_data->setObservation(ajoute_rc($objet_data->getObservation(), $tab['observation_m'])) ;
    if (isset($tab['observation_m2']) and $tab['observation_m2']>'!') {
        $temp="Aides :".$tab['observation_m2'];
        $objet_data->setObservation(ajoute_rc($objet_data->getObservation(), $temp)) ;
    }
//    if (isset($tab['datemail'])and $tab['datemail']>'!') {
//        $temp="Mail le ".$tab['datemail'];
//        $objet_data->setObservation(ajoute_rc($objet_data->getObservation(), $temp)) ;
//    }
//    if (isset($tab['statut'])and $tab['statut']>'!') {
//        $temp='Statut :'.$tab['statut'];
//        $objet_data->setObservation(ajoute_rc($objet_data->getObservation(), $temp)) ;
//    }
    if ($tab['id_individu']) {
        $objet_data->setIdIndividuTitulaire($tab['id_individu']);
    }
    if (isset($tab['identifiant_interne'])and $tab['identifiant_interne']>='  !') {
        $objet_data->setIdentifiantInterne($tab['identifiant_interne']);
    }
     if (isset($tab['datecreation'])){
        if ($tab['datecreation']>='!')
            $objet_data->setCreatedAt($tab['datecreation']);
        elseif ($modification)
            $tab['datecreation'] = $objet_data->getCreatedAt();
    }
    if ($objet_data->getIdMembre()==null or $objet_data->getIdMembre()==0){
//        arbre($objet_data);
//        arbre($objet_data->getIdMembre());
        echo ' ligne 1492:';

    }
//    arbre($objet_data);
    echo '<BR> 1494 id_membre :'.$tab['id_membre'];
 //   echo ' id_membre :'.$tab['id_membre'].' id_individu :'.$tab['id_individu'];
    $objet_data->save();
    $tab['id_membre']=$objet_data->getIdMembre();


    $tab['compte_rendu_membre'] .= ' n° :' . $tab['id_membre'];
    $tab['OK_mem'] = 'OK';
    Log::EnrOp($modification, 'membre', $tab['id_membre']);
    if ($tab['id_individu']) {
        $relation = MembreIndividuQuery::create()->filterByIdMembre($tab['id_membre'])->findOneByIdIndividu($tab['id_individu']);
        if (!$relation) {
            $relation = new MembreIndividus();
            $relation->setIdIndividu($tab['id_individu']);
            $relation->setIdMembre($tab['id_membre']);
            $relation->save();
        }
    }


    return;
}
function membre_recherche($nom, $prenom, &$id_membre, &$id_individu, $email,$identifiant_interne)
{
    global $app;
    $individu = array();
    $membre = array();
    $id_individu_titulaire=null;
//    echo '<br>ligne 989 id_membre :'.$id_membre.' ind : '.$id_individu. ' nom :'.$nom.' prenom : '.$prenom;
     if (!$membre) {
         if ($identifiant_interne) {
//             echo '<br>ligne 992  recherche id interne :' . $identifiant_interne;
             $membre = MembreQuery::create()->filterByIdentifiantInterne($identifiant_interne)->findOne();
         }
         if (!$membre) {
             if (strlen($nom . $prenom) >= 6) {
//                 echo '<br>ligne 997  recherche nom_prenom :' . $nom . ' ' . $prenom;
                 $membre = MembreQuery::create()->filterByNom($nom . ' ' . $prenom)->findOne();
             }
         }
     }
    if ($membre) {
        $id_membre = $membre->getIdMembre();
        $id_individu_titulaire = $membre->getIdIndividuTitulaire();
//        arbre('ligne 1005 id_membre : ' . $id_membre);
    }
    $individu=individu_recherche($nom, $prenom, $id_individu, $email,$id_individu_titulaire);
//    echo '<br>ligne 1008  id_membre :'.$id_membre.' id_individu_titulaire : '.$id_individu_titulaire.' id_individu : '.$id_individu. ' nom : '.$nom.' prenom : '.$prenom;
        //todo si plusieurs ? vient de spip

//            if ($res_nbm['nbm']!='1') {
//                if ($res_nbm['nbm']>1){
//                    $resm = sql_select($colonne_membre,$fichier_membre ,$where_membre);
//                    while($membre = sql_fetch($resm)){
//                        $nbm_plusieurs++;
//                        $membres_plusieurs[$nbm_plusieurs]['id_asso_membre']=$membre['id_asso_membre'];
//                        $membres_plusieurs[$nbm_plusieurs]['id_auteur_titulaire']=$membre['id_auteur_titulaire'];
//                        $membres_plusieurs[$nbm_plusieurs]['nom']=$membre['nom'];
//                        $membres_plusieurs[$nbm_plusieurs]['id_auteur']=$res['id_auteur'];
//                        $membres_plusieurs[$nbm_plusieurs]['nom_famille']=$res['nom_famille'];
//                        $membres_plusieurs[$nbm_plusieurs]['prenom']=$res['prenom'];
//                        $membres_plusieurs[$nbm_plusieurs]['nom_fichier']=$tab['nom_famille'];
//                        $membres_plusieurs[$nbm_plusieurs]['prenom_fichier']=$tab['prenom'];
//                        $membres_plusieurs[$nbm_plusieurs]['ligne_fichier']=$tab['ligne'];
//                        $membres_plusieurs[$nbm_plusieurs][$id_individu_titulaire'ligne']=$nbm_plusieurs;
//                        $message.="plus d'1 membre";
//                    }
//                }

    return array($membre, $individu);
}

// permet la reception danss la colonne de l'id, du nomcourt ou du nom
function mot_charge_motgroupes($id_motgroupe,$bnom_court=false,$bid=false,$nom_defaut='')
{
    global $app;
    if (intval($id_motgroupe)<1) return false;
    $mot=array();
    $sql = "SELECT distinct id_mot,lower(trim(nom)) as nom,lower(trim(nomcourt)) as nomcourt FROM asso_mots  WHERE id_motgroupe=" .$id_motgroupe .' and trim(nom)>" " order by id_mot desc';
    $m = $app['db']->executeQuery($sql);
    while ($req_ligne = $m->fetch()) {
        $tabmot[$req_ligne['nom']] = $req_ligne['id_mot'];
        if ($bnom_court)$tabmot[$req_ligne['nomcourt']] = $req_ligne['id_mot'];
        if ($bid)$tabmot[$req_ligne['id_mot']] = $req_ligne['nom'];
    }
    if(isset($tabmot) and $nom_defaut!=''){
        $mot['tab_mot'] = $tabmot;
        $id_mot_defaut=0;
//echo 'ligne 1050 : '.$id_motgroupe,$nom_defaut.'<br>';
        $nom_mot_defaut=trim($nom_defaut);
        if (is_int($nom_mot_defaut)) {
            $trouve=array_search($nom_mot_defaut,$tabmot);
            if($trouve){
                $nom_mot_defaut=$trouve;
            }
        } elseif (isset($nom_mot_defaut)  and $nom_mot_defaut>='!' and isset($tabmot[strtolower($nom_mot_defaut)])) {
            $trouve = $tabmot[strtolower($nom_mot_defaut)];
            if($trouve){
                $id_mot_defaut=$trouve;
            }
        }
        $mot['id_defaut'] = $id_mot_defaut;
        $mot['nom_defaut'] = $nom_mot_defaut;
        ;
    }
    return $mot;
}

function mot_validite(&$id_mot,&$nom_mot,$motx)
{
//    echo "arrivé dans mot validité ".$id_mot.'--'.$nom_mot.'**'.$motx['id_defaut'].'<br>';
    $nom_mot=trim($nom_mot);
//    echo '<br>ligne 1091 '. $id_mot.'--'.$nom_mot.'**'.$motx['id_defaut'].'<br>';
    if (is_int($id_mot)) {
        $trouve=array_search($id_mot,$motx['tab_mot']);
        if($trouve){
            $nom_mot=$trouve;
            return;
        }
    } elseif (isset($nom_mot)  and $nom_mot>='!' and isset($motx['tab_mot'][strtolower($nom_mot)])) {
//        echo $id_mot.'--'.$nom_mot.'**'.$motx['id_defaut'].'ligne 1084<br>';
        $trouve = $motx['tab_mot'][strtolower($nom_mot)];
        if($trouve){
            $id_mot=$trouve;
            return;
        }
    } else {
//        echo $id_mot.'--'.$nom_mot.'**'.$motx['id_defaut'].'ligne 1091<br>';
        $nom_mot=trim($id_mot);
        $id_mot=0;
        if(isset($motx['tab_mot'][strtolower($nom_mot)]))
            $id_mot = $motx['tab_mot'][$nom_mot];
    }
    if (!$id_mot and isset($motx['id_defaut'])  and $motx['id_defaut']>=1) {
        $id_mot = $motx['id_defaut'];
        $nom_mot = $motx['tab_mot'][$motx['id_defaut']];
     }
//    echo 'ligne 1116'.$id_mot.'--'.$nom_mot.'**'.$id_mot_defaut.'<br>';
    return ;
}

function mot_creation($nom_mot,$groupe){
    $objet = 'mots';
    $objet_data = new Mot();
    $objet_data->setNom($nom_mot);
    $objet_data->setNomcourt(strtolower(trim($nom_mot)));
    $objet_data->setDescriptif($nom_mot);
    $objet_data->setTexte($nom_mot);
    $objet_data->setIdMotgroupe($groupe);
    $objet_data->save();
    $id_objet = $objet_data->getIdMot();
    return $id_objet;
}
function paiement( &$tab , $type = 'cot')
{
    if (intval($tab[$type . 'id_paiement']) < 1) {
        $objet_data = new Paiement();
        $modification = false;
        $objet_data->setObservation($tab[$type . 'p_observation']);
    } else {
        $modification = true;
        $objet_data = PaiementQuery::create()->findOneByidPaiement($tab[$type . 'id_paiement']);
        $objet_data->getObservation(ajoute_rc($objet_data->getObservation(), $tab[$type . 'p_observation']));
    }
    $objet_data->fromArray($tab);
    $objet_data->setIdMembre($tab['id_membre']);
    $objet_data->setIdEntite(tab('prestation.' . $tab[$type . 'id_prestation'] . '.id_entite'));
    $objet_data->setIdTresor($tab[$type . 'id_tresor']);
    $objet_data->setMontant($tab[$type . 'montant']);
    $objet_data->setDatecheque($tab[$type . 'date_cheque']);
    $objet_data->setDateEnregistrement(new DateTime());
    $objet_data->save();
    $id = $objet_data->getPrimaryKey();
//    arbre($id);
//    arbre($objet_data);
//    xe('ligne 1327');
    Log::EnrOp(false, 'paiement',  $id);
    $tab['OK_' . $type . 'p'] = 'OK';
    $tab['compte_rendu_' . $type . 'p'] .= ' n° :' . $id ;
    if (!$modification) {
        $tab[$type . 'id_paiement'] = $id;
     }
    $objet_data1 = null;
    if ($tab[$type . 'montanttheorique']) {
        if ($modification) {
            $objet_data1 = ServicepaiementQuery::create()->filterByIdPaiement($id)
                ->filterByIdServicerendu($tab[$type . 'id_servicerendu'])->findOne();
        }
        if (!$objet_data1) {
            $objet_data1 = new Servicepaiement();
            $objet_data1->setIdServicerendu($tab[$type . 'id_servicerendu']);
            $objet_data1->setIdPaiement($id);
        }
        $objet_data1->setMontant(min($tab[$type . 'montant'], $tab[$type . 'montanttheorique']));
        $objet_data1->save();
    }
    return;
}
function paiement_creation_import( &$data , $type,$log=true)
{
    $objet='paiement';
    $objet_data = new Paiement();
    $objet_data->fromArray($data);
    $objet_data->save();
    $id_objet=$objet_data->getIdPaiement();
    if (isset($data['compte_rendu_' . $type]))
        $data['compte_rendu_' . $type] .= ' n° :' .  $id_objet;
    else
        $data['compte_rendu_' . $type] = ' n° :' .  $id_objet;

    if($log)
        Log::EnrOp(false, $objet,  $id_objet);
    $objet_data1 = new Servicepaiement();
    $objet_data1->setIdServicerendu($data['id_servicerendu']);
    $objet_data1->setIdPaiement($id_objet);
    $objet_data1->setMontant($data[ 'montant']);
    $objet_data1->save();
    return $id_objet;
}
function paiement_validite($num_ligne, &$tab, &$message, $id_defaut, $type = 'cot', &$nb, &$ret)
{
    if ($tab[$type . 'id_paiement'] >= 1) {
        $modification=true;
    } else {
        $modification=false;
    }
    $ok= 'NC';
    if ($tab[$type . 'montant'] == 0 and !$modification) {
        $msg = 'Montant paiement null';
    } else {
        if (!$tab[$type . 'id_tresor']) {
            $tab[$type . 'id_tresor'] = $id_defaut;
        }
        $modi = array();
        $modi['date_paiement']=test_date_valeur($tab[$type . 'date_cheque']);
        if ($modi['date_paiement']) {
            if ($modification) {
                $nb++;
                $modi = array();
                $modi['ligne'] = $num_ligne;
                $modi['id_membre'] = $tab['id_membre'];
                $modi['montant'] = $tab[$type . 'montant'];
                $modi['observation'] = $tab[$type . 'p_observation'];
                $modi['nom'] = $tab['nom'];
                $modi['id_tresor'] = $tab[$type . 'id_tresor'];
                $ret[$nb] = $modi;
                $msg ="modification";
            }else $msg ="creation";
            $ok = 'bon';
            $msg .= ' du paiement ' . $tab[$type . 'date_cheque'] . ' de ' . $tab[$type . 'montant'] . ' Euros ' . $tab[$type . 'p_observation'];

        } else {
            $msg = 'Date du cheque non valide :'.$tab[$type . 'date_cheque'];
        }
    }
    $tab['compte_rendu_' . $type . 'p'] = $msg;
    $tab['OK_' . $type . 'p']=$ok;
    return;
}
function servicerendu(&$tab, $type = 'cot', $p_type = '1')
{
    if (intval($tab[$type . 'id_servicerendu']) < 1) {
        $objet_data = new Servicerendu();
        $modification = false;
        $objet_data->setObservation($tab[$type . 'observation']);
    } else {
        $modification = true;
        $objet_data = ServicerenduQuery::create()->findOneByidServicerendu($tab[$type . 'id_servicerendu']);
        $objet_data->setObservation(ajoute_rc($objet_data->getObservation(), $tab[$type . 'observation'])) ;
    }
    $objet_data->fromArray($tab);
    $objet_data->setMontant($tab[$type . 'montanttheorique']);
    //todo objet a l'origine
    $objet_data->setDateDebut(test_date_valeur($tab[$type . 'date_debut']));
    $objet_data->setDateFin(test_date_valeur($tab[$type . 'date_fin']));
    if ($type == 'abo') {
        $objet_data->setPremier($tab[$type . 'premier']);
        $objet_data->setDernier($tab[$type . 'dernier']);
    }
    $objet_data->setIdEntite(tab('prestation.' . $tab[$type . 'id_prestation'] . '.id_entite'));
    $objet_data->setIdPrestation($tab[$type . 'id_prestation']);
    if ($tab[$type . 'montanttheorique'] == $tab[$type . 'montant'] or $tab[$type . 'montanttheorique'] == 0) {
        $objet_data->setRegle('2');
    } else {
        $objet_data->setRegle(regle_valeur($tab[$type . 'montant'], $tab[$type . 'montanttheorique']));
    }
    $objet_data->save();
    $tab[$type . 'id_servicerendu'] = $objet_data->getIdServicerendu();
    $tab['OK_'.$type] = 'OK';
    $tab['compte_rendu_' . $type] .= ' n° :' . $tab[$type . 'id_servicerendu'];
    Log::EnrOp(false, 'servicerendu', $tab[$type . 'id_servicerendu'], 'SR' . $p_type);
    return;
}

/**
 * regle_valeur
 * @param $paiement_montant
 * @param $servicerendu_montant
 * @return int
 */
function regle_valeur($paiement_montant, $servicerendu_montant)
{
    if ($paiement_montant == 0 or $servicerendu_montant == 0) {//paiement = service rendu
        $regle = 2; // Reglement ok
    } elseif ($paiement_montant < 0.01) { //paiement + grand
        $regle = 6;//Positif
    } elseif ($paiement_montant == 0) { // sans paiement
        $regle = 1;
    } else {
        $regle = 3;//partiel
    }
    return $regle;
}



function sr_cotisation($an_debut,$an_fin,$tab,$prefixe_montant,$options,&$nb,&$nbp)
{
    for ($annee = $an_debut; $annee <= $an_fin; $annee++) {
        $options['date_debut' . $annee] = $annee . '-01-01';
        if ($annee > 1993) {
            $options['quantite' . $annee] = $tab['nbadh' . $annee];
        }
        $options['created_at' . $annee] = test_date_valeur($tab['date' . $annee])." 00:00:00";
        if ($annee > 2007 and $tab['cheque' . $annee]) {
            $options['observation' . $annee] = 'Cheque n° ' . $tab['cheque' . $annee];
        }
    }
    $retour=sr_boucle($an_debut,$an_fin,$tab,$prefixe_montant,$options,$nb,$nbp);
    for ($annee = 2001; $annee <= 2017; $annee++) {
        $saisie=trim($tab['carte' . $annee]);
        if ($saisie != ''){
            if (isset($retour['id_servicerendu'.$annee])) {
                $argument = array(
                    'id_individu' => $tab['id_individu'],
                    'id_membre' => $tab['id_membre'],
                    'id_prestation' => $options['id_prestation'],
                    'id_servicerendu' => $retour['id_servicerendu' . $annee]
                );
                switch ($saisie) {
                    case 'oui':
                        $statut = 'oui';
                        break;
                    case 'non':
                        $statut = 'a_faire';
                        break;
                    case 'oui R':
                        $statut = 'oui R';
                        break;
                    case 'non R':
                        $statut = 'non_R';
                        break;
                    case 'attente':
                        $statut = 'attente';
                        break;
                    default :
                        $statut = $saisie;
                }
                $data=array('descriptif'=>'reprise_adav',
                'fonction'=>'envoyer_carte',
                'args'=>$argument,
                'statut'=>$statut,
                'priorite'=>'manuelle');
                tache_creation($data);
            }else{
                $tab['compte_rendu_membre'].=' carte '.$annee. 'sans cotisation';
            }
        }
    }
}
function remplace_par($num_ligne,&$nb, &$ret, $data_base, $tab, $champ, $objet, $case)
{
    $ok = false;
    if ($case) {
        if ((strtoupper($data_base[$champ]) != strtoupper($tab[$champ])) and (!empty($tab[$champ]))) {
            $ok = true;
        } else {
            if ((!empty($tab[$champ])) and ($data_base[$champ] != $tab[$champ])) {
                $ok = true;
            }
        }
    }
    if ($ok) {
        $nb++;
        $modi = array();
        $modi['ligne'] = $num_ligne;
        $modi['id'] = $data_base['id_' . $objet];
        $modi['remplace'] = $data_base[$champ];
        $modi['par'] = $tab[$champ];
        $modi['champ_destination'] = $champ;
        $modi['objet'] = $objet;
        $modi['inform'] = $data_base['nom'] . " " . $data_base['ville'];
        $ret[$nb] = $modi;
    }
    return;
}
function tache_creation($data){
    $objet = 'tache';
    $objet_data = new Tache();
    $objet_data->fromArray($data);
    $objet_data->save();
    $id_objet=$objet_data->getIdTache();
    return $id_objet;

}


function test_email_valide(&$email)
{
    if (!isset($email) and $email == '') {
        return false;
    }
    $email=trim($email);
    if (strpos($email,' ')>1) $email=substr($email,0,strpos($email,' '));
    if (strpos($email, '@') <= 1) {
        return false;
    }
    $email=trim($email);
    return true;
}


function test_date_valeur($date,$hms=false)
{
    if (is_object($date))
        $date=$date->format('Y-m-d');
    else{
    //format francais jj/mm/aaaa

        if (is_string($date)) {
//        echo 'ligne 1500 test date longueur :' . strlen($date) . '- valeur :' . $date;
            if (strlen($date) <= 7) {
                return false;
            }
            //    xe('ligne 1500');
            if (strlen($date) == 8) {
                $date = substr($date, 0, 6) . '20' . substr($date, 6);
            }
            if (substr($date, 4, 1) == '-')//yyyy-mm-jj
            {
                $test_arr = array(substr($date, 8, 2), substr($date, 5, 2), substr($date, 0, 4));
            } elseif (substr($date, 2, 1) == '-')//jj-mm-yyyy
            {
                $test_arr = array(substr($date, 0, 2), substr($date, 3, 2), substr($date, 6, 4));
            } else {
                $test_arr = explode(substr($date,2,1), $date);
                if (!isset($test_arr[1])){
                    arbre($date);
                     arbre($test_arr);
                    exit;
                }
                if ($test_arr[1] >= 13)//format mm/jj/aaaa Permet de passer mais ne résoud pas le Pb
                {
                    $date = $test_arr[2] . '-' . $test_arr[0] . '-' . $test_arr[1];
                } else {
                    $date = $test_arr[2] . '-' . $test_arr[1] . '-' . $test_arr[0];
                }

            }
            //format aaaa-mm-jj
//        var_dump($test_arr);

            if (!checkdate($test_arr[1], $test_arr[0], $test_arr[2]));// Mois jour annee retourne true si correct


            $$date=null;
        }
    }
    return $date;
}

/**
 * @param $fichier
 * @return null|PHPExcel
 * @throws PHPExcel_Reader_Exception
 */
function time_message(&$callStartTime, $option = array())
{
    $callEndTime = microtime(true);
    $callTime = $callEndTime - $callStartTime;
    echo EOL, date('H:i:s'), " ecrit ";
    if ($option) {
        arbre($option);
    }
    echo ' en ', sprintf('%.4f', $callTime), " secondes";
    Echo ' memoire utilisee : ', (memory_get_usage(true) / 1024), " caracteres ", EOL;
    $callStartTime = microtime(true);
}

function vient_de_spip_service_rendu(&$ret, $id_asso_membre, $tab, $nom_m)
{
    // controle des éléments indispensables
    if ($tab['montant'] == 0) {
        $ok = false;
    } else {
        $ok = true;
    }
    if ($ok) {
        $ret[0]++;
        $modi = array();
        $modi['ligne'] = $ret[0];
        $modi['id_asso_membre'] = $id_asso_membre;
        $modi['date_debut'] = $tab['date_debut'];
        $modi['date_fin'] = $tab['date_fin'];
        $modi['date_paiement'] = $tab['date_paiement'];
        $modi['id_asso_entite'] = $tab['id_asso_entite'];
        $modi['id_asso_prestation'] = $tab['id_asso_prestation'];
        $modi['montant'] = $tab['montant'];
        $modi['observation_sr'] = $tab['observation_sr'];
        $modi['observation_p'] = $tab['observation_p'];
        $modi['nom'] = $nom_m;
        $modi['ligne_fichier'] = $tab['ligne'];
        $ret['srp'][$ret['0']] = $modi;
    }
    return;
}

function ancien_csv()
{
//    $tab_fichier = array();
//    // les champs nommés ici sont transmis de tab dans tab_value
//    $champ_table = array('libelle', 'classement', 'observation');
//    arbre($tab_champs_autorises);
//    arbre($fichier);
//    $pointeur_fichier = fopen($fichier, "r");//lecture seule
//    $message = 'debut a : ' . date('Y-m-d H:i:s') . PHP_EOL;
//    $nb_lignes_traitees = 0;
//    $nb_lignes_lues = 0;
//    $centimes = 0;
//    $nbpiece = 0;
//    $tab_piece = array(
//        'piece' => -1,
//        'id_piece' => 0,
//        'nb_ligne' => 0,
//        'created_at' => date('Y-m-d H:i:s'),
//        'updated_at' => date('Y-m-d H:i:s'),
//        'etat' => $data['etat']
//    );
//    $tab_ecriture = array(
//        'id_piece' => 0,
//        'id_journal' => $data['id_journal'],
//        'id_entite' => $data['id_entite'],
//        'id_compte_cp' => $data['id_compte_cp'],
//        'id_poste_bilan' => $data['id_poste_bilan'],
//        'id_poste_gestion' => $data['id_poste_gestion'],
//        'created_at' => date('Y-m-d H:i:s'),
//        'updated_at' => date('Y-m-d H:i:s'),
//        'message' => "",
//        'type' => $data['type'],
//        'relevedebit' => 0,
//        'relevecredit' => 0,
//    );
//    if ($tab_ecriture['type'] == 'releve') {
//        $tab_ecriture['id_piece'] = 'releve';
//    }
//    arbre($pointeur_fichier);
//    $tab_correspondance = array();
//    if ($pointeur_fichier <> 0) {
//


//
//    }else{
//        $message ='fichier vide';
//    }
//    echo '<br><br>nb erreurs : '. $erreurs;
//    xf($message);
//    return array($message, $erreurs);
}


//CSV par ligne
//
//            for ($i = 0; $i <= $param['nb_lentete'] ; $i++) {
//                $nb_lignes_lues++;
//                $ligne = fgets($pointeur_fichier, 4096);
//                echo '<br>ligne '.$nb_lignes_lues.' data :'.$ligne;
//
//            }
//        } else {
//            echo '<br><br>ligne 103 data :'.$ligne;
//            $li=validite_ligne($ligne,$tab_correspondance,$nb_lignes_lues,$message);
//            arbre($li);
//            arbre('Intrepretation de la ligne '.($param['nb_lentete']+2).'<br> programme ligne 105');
//            if ($li) {
//                list($membre,$individu)=recherche_membre($li['nom_famille'],$li['prenom'],$li['id_membre'],$id_individu,$li['email']);
//                if ($individu  and $individu->getIdIndividu()<>$li['id_individu']){
//                    $message.='<br>Distortion entre l\'individu titulaire dans la base et celui present dans l\'import : ligne '.$nb_lignes_lues.' base  :'.$id_individu.'  import :'.$li['id_individu'];
//                    $erreurs++;
//                }
//
//                //                   validite_cotisation();
//                //                   validite_paiement();
//                // a suivre pour autres integrations
//                if ($li['id_membre']) {
//                    echo '<br>mise a jour du membre ' . $li['id_membre'];
//                } else {
//                    echo '<br>creation du membre ' . $li['nom_famille'];
//                }
//
//                if ($id_individu) {
//                    echo ' + mise à jour de l individu ' . $li['id_individu'];
//                } else {
//                    echo ' + création de l individu ' . $li['nom_famille'];
//                }
////                    if ($li['cotid_servicerendu']) {
////                        echo '<br> creation de la cotisation ' . tab('prestation.' . $li['cotid_servicerendu'] . '.nom');
////                        echo ' debut le :' . $li['cotdate_debut'];
////                    }
////                    if ($li['cotdate_cheque'] and $li['cotmontant'] >= 0.01) {
////                        echo ' + creation du paiement ' . $li['cotdate_cheque'] . ' de ' . $li['cotmontant'] . ' Euros';
////                    }
////                    if ($li['motgroupe1'] ) {
////                        echo '<br> creation du mot ' . $li['motgroupe1'] . ' pour  ' . $li['nom'] ;
////                    }
//                $nb_lignes_traitees++;
//            }
//        }
//specif ecriture
//$centimes = 0;
//$nbpiece = 0;
//$tab_piece = array(
//        'piece' => -1,
//        'id_piece' => 0,
//        'nb_ligne' => 0,
//        'created_at' => date('Y-m-d H:i:s'),
//        'updated_at' => date('Y-m-d H:i:s'),
//        'etat' => $data['etat']
//);
//$tab_ecriture = array(
//        'id_piece' => 0,
//        'id_journal' => $data['id_journal'],
//        'id_entite' => $data['id_entite'],
//        'id_compte_cp' => $data['id_compte_cp'],
//        'id_poste_bilan' => $data['id_poste_bilan'],
//        'id_poste_gestion' => $data['id_poste_gestion'],
//        'created_at' => date('Y-m-d H:i:s'),
//        'updated_at' => date('Y-m-d H:i:s'),
//        'message' => "",
//        'type' => $data['type'],
//        'relevedebit' => 0,
//        'relevecredit' => 0,
//);
//if ($tab_ecriture['type'] == 'releve') {
//    $tab_ecriture['id_piece'] = 'releve';
//}

//fin csv





// renvoie une date en texte format aaaa-mm-jj
//l'entee peux etre un objet ou une chaine
function format_date_string_objet($date, $hms = false)
{
    if (is_object($date)) {
        $date = $date->format('Y-m-d');
    } else {
        //format francais jj/mm/aaaa
        if (is_string($date)) {

//        echo '<br>importation 1245 test date longueur :' . strlen($date) . '- valeur :' . $date;
            if (strlen($date) <= 5) {
                return false;
            }
            if (strlen($date) == 16) {
                $date = substr($date, 0, 10);
            }
            if (strlen($date) == 8) {
                if (strpos($date, '/') or strpos($date, '-')) {
//                    echo '<br>ligne 1727 :'.$date.' sur 8 avec separateur ajoute le siècle jj-mm-aa ou jj/mm/aa';
                    $date = substr($date, 0, 6) . '20' . substr($date, 6);
                } elseif (substr($date, 0, 2) > 18) {//aaaammjj ou aaaajjmm
//                    echo '<br>ligne 1730 :'.$date.' sur 8 sans  separateur aaaammjj ou aaaajjmm';
                    if (substr($date, 4, 2) > 12) {
                        $date = substr($date, 0, 4) . '-' . substr($date, 6, 2) . '-' . substr($date, 4, 2);
                    } else//
                    {
                        $date = substr($date, 0, 4) . '-' . substr($date, 4, 2) . '-' . substr($date, 6, 2);
                    }
                } elseif (substr($date, 6, 2) > 18) {
//                    echo '<br>ligne 1736 :'.$date.' sur 8 sans de separateur jjmmaaaa ou mmjjaaaa';
                    if (substr($date, 2, 2) > 12) {
                        $date = substr($date, 4, 4) . '-' . substr($date, 0, 2) . '-' . substr($date, 2, 2);
                    } else//
                    {
                        $date = substr($date, 4, 4) . '-' . substr($date, 2, 2) . '-' . substr($date, 0, 2);
                    }
                }
            }

//            echo '<br>ligne 1744 :'.$date.' date sur 10';
            if (substr($date, 4, 1) == '-') {
//                echo '<br>ligne 1746 :'.$date.' yyyy-mm-jj';
                $test_arr = array(substr($date, 8, 2), substr($date, 5, 2), substr($date, 0, 4));
            } elseif (substr($date, 2, 1) == '-') {
//                echo '<br>ligne 1749 :'.$date.' jj-mm-yyyy';
                $test_arr = array(substr($date, 0, 2), substr($date, 3, 2), substr($date, 6, 4));
            } else {
//                echo '<br>ligne 1752 :'.$date.' ???????????';
                $test_arr = explode(substr($date, 2, 1), $date);

//                if (!isset($test_arr[1])){
//                    arbre($date);
//                     arbre($test_arr);
//                    exit;
//                }
                if ($test_arr[1] >= 13)//format mm/jj/aaaa Permet de passer mais ne résoud pas le Pb
                {
                    $date = $test_arr[2] . '-' . $test_arr[0] . '-' . $test_arr[1];
                } else {
                    $date = $test_arr[2] . '-' . $test_arr[1] . '-' . $test_arr[0];
                }

            }

//            exit;
            // Mois jour annee retourne true si correct
//            arbre($test_arr);
            if (!checkdate($test_arr[1], $test_arr[0], $test_arr[2])) {
                $date = null;
            }
        }
    }
//    echo '<br>renvoi la date au format aaaa-mm-jj ou null :'.$date;
//    echo '<br> correction a faire dans ce module si ce n\'est pas le cas';
//    exit;
    return $date;
}





