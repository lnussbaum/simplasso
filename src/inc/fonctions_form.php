<?php

use Symfony\Component\Form\FormError as FormError;
/**
* formSetAction
* @param $form_builder
* @param bool|true $modif
* @param array $args
* @param string $id
* @param string $page
* @return bool
*/
function formSetAction(&$form_builder, $modif = true, $args = array(), $id = '', $page = '')
{
global $app;

if ($id == '' && $modif) {
$id = sac('id');
}
$page = sac('page');
$objet = sac('objet');
if ($modif && $objet != '') {
$args[descr($objet . '.cle')] = $id;
}

$args['redirect'] = $app['request']->get('redirect');
$form_builder->setAction($app->path($page, $args));
return true;
}




/**
 * reponse_formulaire
 * @param $form
 * @param array $args_twig
 * @param string $fichier_twig
 * @return string|\Symfony\Component\HttpFoundation\JsonResponse|\Symfony\Component\HttpFoundation\RedirectResponse
 */
function reponse_formulaire($form, $args = [], $fichier_twig = '')
{

    global $app;

    // Preparation de la reponse
    if ($form->isSubmitted() && $form->isValid()) {

        $ok = true;
        $objet = sac('objet');
        $modification = !empty(sac('id')) || (isset($args['modification']) && $args['modification']);
        $code_mes = (($objet) ? $objet . '_' : '') . (($modification) ? 'modifier' : 'ajouter');

        if (isset($args['message'])) {
            $message = $app->trans($args['message']);
        } else {
            $code_mes .= '_ok';
            $message = $app->trans($code_mes);
        }

        if (isset($args['message_cplt'])) {
            $message .= $args['message_cplt'];
        }
        if (isset($args['no_log'])) {
            Log::EnrOp($modification, sac('objet'), $args['id']);
        }
        $declencheur = isset($args['declencheur'])?$args['declencheur']:null;
        if (!isset($args['url_redirect'])) {
            $id=isset($args['id'])?$args['id']:null;
            $args['url_redirect'] = composer_url_redirect($id,$declencheur);
        }
        else
        {
            $args['url_redirect'] .='&declencheur='.$declencheur;
        }

    } else {

        $ok = false;
        unset($args['url_redirect']);
        if (isset($args['message'])) {
            $message = $args['message'];
        }

        if ($form->isSubmitted()) {
            $message = $app->trans('erreur_saisie_formulaire');
            $form->addError(new FormError($message));
        }

        if (isset($args['flashbag_erreur'])) {
            $app['session']->getFlashBag()->add('warning', $app->trans($args['flashbag']));
        }

        if ($fichier_twig == '') {
            $fichier_twig = fichier_twig();
        }

        $args_twig = $args;
        $args_twig['form'] = $form->createView();
        $message = $app['twig']->render($fichier_twig, $args_twig);
    }

    // Envoi de la reponse

    if (sac('ajax') && !empty($app['request']->get('json'))) {

        $reponse['message'] = $message;
        $reponse['ok'] = $ok;
        if (isset($args['url_redirect'])) {
            $reponse['redirect'] = $args['url_redirect'];
        }
        if (isset($args['vars'])) {
            $reponse = array_merge($reponse,$args['vars']);
        }
        return $app->json($reponse);
    }

    if ($ok) {
        $app['session']->getFlashBag()->add('success', $message);
        return $app->redirect($args['url_redirect']);
    }
    return $message;
}

