<?php

use mikehaertl\wkhtmlto\Pdf;


/**
 * liste_modele_doc
 * @param $courrier_type
 * @param $canal
 * @param bool|true $aucun
 * @return array
 */
function liste_modele_doc($courrier_type, $canal, $aucun = true)
{

    global $app;
    $tab_modele = ($aucun) ? array('aucun' => '') : array();
    $dir = $app['document.path'] . '/' . $courrier_type . '/' . $canal . '/';
    foreach (glob($dir . "*.twig") as $filename) {
        $path_parts = pathinfo($filename);
        $tab_modele[$path_parts['filename']] = $path_parts['filename'];
    }
    return $tab_modele;
}


function document_rechercher_variables($texte_template)
{

    preg_match_all("/{{( *\\w+)(\\|\\w+(\\(\\'\\w+\\'\\))?)? *}}/", $texte_template, $matches);
    $tab_var = array();
    foreach ($matches[1] as $var) {
        $tab_var[trim($var)] = '';
    }
    return $tab_var;

}


function document_variables_systeme($objet = null)
{

    $tab_var = [];

    $tab_objet = ['entite', 'individu', 'membre', 'servicerendu', 'paiement', 'prestation', 'prestationslot'];
    if ($objet) {
        $tab_objet = is_array($objet) ? $objet : [$objet];
    } else {
        $tab_var = ['cotis_histo', 'cotis_abo'];
    }
    foreach ($tab_objet as $objet) {

        $tab_champs = array_keys(descr($objet . '.colonnes'));
        foreach ($tab_champs as $champs) {
            $tab_var[] = $objet . '_' . $champs;
        }
        if ($objet == 'entite') {
            $tab_var[] = $objet . '_logo';
        }
    }

    return $tab_var;


}


function envoyer_fichier_email($from, $to, $data_fichier, $nom_fichier, $modele_twig, $data)
{

    global $app;

    $sujet = $app['twig']->render('mail_' . $modele_twig . '_sujet.html.twig', $data);
    $body = $app['twig']->render('mail_' . $modele_twig . '.html.twig', $data);
    $body .= $app['twig']->render('pied_mail.html.twig', $data);

    $attachment = Swift_Attachment::newInstance($data_fichier, $nom_fichier, 'application/pdf');
    if (isset($app['email_copie']) && $app['email_copie']) {
        $body_copie = 'Envoyé à ' . $to . '<br>' . $body;
        $to_copie = $app['email_copie'];
        $html = new Html2Text\Html2Text($body);
        $app->mail(Swift_Message::newInstance()
            ->setSubject($sujet)
            ->setFrom(array($from))
            ->setTo(array($to_copie))
            ->setBody($body_copie, 'text/html')
            ->addPart($html->getText(), 'text/plain')
            ->attach($attachment));
    }


    if (isset($app['email_redirect']) && $app['email_redirect']) {
        $body = 'A envoyer à ' . $to . '<br>' . $body;
        $to = $app['email_redirect'];
    }

    $html = new Html2Text\Html2Text($body);
    $app->mail(Swift_Message::newInstance()
        ->setSubject($sujet)
        ->setFrom(array($from))
        ->setTo(array($to))
        ->setBody($body, 'text/html')
        ->addPart($html->getText(), 'text/plain')
        ->attach($attachment));

}


function charge_donnee_objet($objet, $id_objet)
{
    $objet_query = nom_propel($objet) . 'Query';
    $objet_data = $objet_query::create()->findPk($id_objet);
    $tab = [];
    if ($objet_data) {
        $tobjet_data = $objet_data->toArray();
        $tab_champs = document_variables_systeme($objet);

        foreach ($tab_champs as $c) {

            $champs = substr($c, strlen($objet) + 1);

            if ($champs == 'logo') {
                $fichier = image_preparation('entite', $id_objet, 'logo', 400);
                $tab[$c] = '<img src="' . $fichier . '" alt="logo" align="left" class="img-thumbnail" />';
            } else {
                if ($champs === 'id') {
                    $champs = descr($objet . '.cle');
                }
                $tab[$c] = $tobjet_data[$champs];
            }
        }
    }

    return [$tab, $objet_data];
}


function charge_donnee_entite($id_entite)
{
    list($tab, $objet_data) = charge_donnee_objet('entite', $id_entite);
    return $tab;
}

function charge_donnee_individu($id_individu)
{
    list($tab, $objet_data) = charge_donnee_objet('individu', $id_individu);
    return $tab;
}


function charge_donnee_membre($id_membre)
{
    list($tab, $objet_data) = charge_donnee_objet('membre', $id_membre);
    $individu = $objet_data->getIndividuTitulaire();
    $tab['membre_adresse'] = $individu->getAdresse();
    $tab['membre_codepostal'] = $individu->getCodePostal();
    $tab['membre_ville'] = $individu->getVille();
    return $tab;
}


function renseigner_variables_systeme($args, $tab_champs_demandes = [])
{

    $variables = [];
    if (isset($args['id_entite'])) {
        $variables = charge_donnee_entite($args['id_entite']);
    }
    if (isset($args['id_individu'])) {
        $variables = array_merge($variables, charge_donnee_individu($args['id_individu']));
    }
    if (isset($args['id_membre'])) {
        $variables = array_merge($variables, charge_donnee_membre($args['id_membre']));
    }
    $variables = array_intersect_key($variables, array_flip($tab_champs_demandes));
    if (in_array('cotis_histo', $tab_champs_demandes) && isset($args['id_membre'])) {
        $membre = MembreQuery::create()->findPk($args['id_membre']);
        $variables['cotis_histo'] = array2htmltable($membre->getCotisationHistorique(),
            ['th' => true, 'largeur' => array('50%', '20%', '30%')]);
    }
    return $variables;
}


function generer_document($id_courrier, $mode, $afficher = true, $id_individu = null,$test=false)
{
    global $app;
    include_once($app['basepath'] . '/src/inc/fonctions_ged.php');

    $loader = new Twig_Loader_Filesystem($app['resources.path'] . '/documents');
    $twig_page = new Twig_Environment($loader, ['autoescape' => false]);

    $courrier = CourrierQuery::create()->findPk($id_courrier);
    if ($id_individu && intval($id_individu) > 0) {
        $tab_destinataires = [CourrierLienQuery::create()->filterByIdCourrier($id_courrier)->findOneByIdIndividu($id_individu)];
    } else {
        $tab_destinataires = $courrier->getCourrierLiens();
    }

    $pages = '';

    $css = preparation_css('lettre', $courrier->getValeur());

    $tab_values = array();


    $twig = preparation_twig('lettre', $courrier->getValeur());

    $tab_vars_global = (isset($tab_vars_courrier['variables'])) ? $tab_vars_courrier['variables'] : [];


    $date_butoir = DateTime::createFromFormat('d-m-Y', '01-07-2016');
    $tab_cotisation = array_keys(getPrestationDeType('cotisation'));
    $nb=0;
    foreach ($tab_destinataires as $destinataire) {

        $args = [
            'id_entite' => $courrier->getIdEntite(),
            'id_individu' => $destinataire->getIdIndividu(),
        ];
        if ($destinataire->getIdMembre()) {
            $args['id_membre'] = $destinataire->getIdMembre();
        }

        $args_twig = renseigner_variables_systeme($args, $courrier->getListeVariables('lettre'));


        $hautdepage = '';
        $content = '';
        $basdepage = '';
        $tab_bloc = table_trier_par($courrier->getValeur()['lettre'], 'ordre');
        foreach ($tab_bloc as $bloc_id => $v) {
            $affichage = true;
            if (isset($v['condition']['nom']) && !empty($v['condition']['nom'])) {
                switch ($v['condition']['nom']) {
                    case 'nouvel_adherent':
                        $id_membre = $destinataire->getIdMembre();
                        $membre = MembreQuery::create()->findPk($id_membre);
                        $date_creation = $membre->getCreatedAt();
                        if ($date_creation < $date_butoir) {
                            $affichage = false;
                        }
                        break;
                    case 'ancien_adherent':
                        $id_membre = $destinataire->getIdMembre();
                        $membre = MembreQuery::create()->findPk($id_membre);
                        $date_creation = $membre->getCreatedAt();
                        if ($date_creation >= $date_butoir) {
                            $affichage = false;
                        }
                        break;
                    case 'membre_cotisation_periode_2017':
                        $id_membre = $destinataire->getIdMembre();

                        $membre = MembreQuery::create()->findPk($id_membre);
                        $date_creation = $membre->getCreatedAt();
                        $affichage = (ServicerenduQuery::create()
                                    ->filterByIdPrestation($tab_cotisation)
                                    ->filterByIdMembre($id_membre)
                                    ->filterByDateFin('2017-06-30')
                                    ->count() == 0) && $date_creation < $date_butoir;
                        break;

                }


            }
            if ($affichage) {
                if ($v['position'] == 'hautdepage') {
                    $hautdepage .= '<div class="hautdepage_' . str_replace(' ', '_',
                            $bloc_id) . '">' . courrier_agglomerer_texte($twig, $bloc_id, $args_twig, $v['variables'],
                            $tab_vars_global) . '</div>';
                } elseif ($v['position'] == 'basdepage') {
                    $basdepage .= '<div class="basdepage_' . str_replace(' ', '_',
                            $bloc_id) . '">' . courrier_agglomerer_texte($twig, $bloc_id, $args_twig, $v['variables'],
                            $tab_vars_global) . '</div>';
                } else {
                    $content .= '<div class="corp_' . str_replace(' ', '_',
                            $bloc_id) . '">' . courrier_agglomerer_texte($twig, $bloc_id, $args_twig, $v['variables'],
                            $tab_vars_global) . '</div>';
                }
            }
        }

        $pages [] = $twig_page->render('document.html.twig', ['content' => $content]);
        if ($nb>9 && $test ) break;
        $nb++;
    }


    if ($mode == 'html') {
        $chemin = $app->path('image');
        $pages = preg_replace('#(src=")([^"]*)(")#i', '$1' . $chemin . '?document=$2$3', $pages);
        $pages .= $twig_page->render('document_html.css.twig', $tab_values);
        return $pages;
    } else {

        $css = $twig_page->render('document_style.css.twig', ['css' => $css]);

        $sortie=($afficher)?'ecran':'fichier';

        return generer_pdf($pages,$css,$sortie);


    }


}


function generer_pdf($pages,$css,$sortie='ecran',$options_impression=[]){

global $app;
    $options = [
        'binary' => '/usr/bin/xvfb-run /usr/bin/wkhtmltopdf',
        'tmpDir' => $app['tmp.path'] . '/print',
        'margin-bottom' => 8,
        'margin-left' => 8,
        'margin-right' => 8,
        'margin-top' => 8,
        'orientation' => 'Portrait'

    ];

    $options =  array_merge($options_impression,$options);

    $pdf = new Pdf($options);

    $options_page = [];
    foreach ($pages as $page) {
        $html = '<!DOCTYPE html><html><head><meta http-equiv="Content-Type" content="text/html; charset=UTF-8">' . $css . '</head><body>' . $page . '</body></html>';

        $pdf->addPage($html, $options_page);
    }

    if ($sortie=='ecran') {
        $pdf->send();
        exit();
    } else {
        $nom_fichier = $app['tmp.path'] . "/pdf/courrier_" . uniqid() . '.pdf';
        $nom_fichier_compress = $app['tmp.path'] . "/pdf/courrier_" . uniqid() . '.pdf';
        $pdf->saveAs($nom_fichier);
        exec('ghostscript -sDEVICE=pdfwrite -dCompatibilityLevel=1.4 -dPDFSETTINGS=/ebook -dNOPAUSE -dQUIET -dBATCH -sOutputFile='.$nom_fichier_compress.' '.$nom_fichier);
        unlink($nom_fichier);
        return $nom_fichier_compress;
    }






}



function generer_document_ag($params = array())
{
    global $app;
    include_once($app['basepath'] . '/src/inc/fonctions_ged.php');


    list($tab_id, $nb_total) = objet_liste_dataliste_preselection('membre', $params, false, false);
    $tab_membre = objet_liste_dataliste_selection('membre', $tab_id, ['nom'=>'ASC']);
    $id_entite = pref('en_cours.id_entite');
    $tab_id_prestation = array_keys(getPrestationEntite('cotisation'));


    $tab_p1 = ServicerenduQuery::getBy($id_entite,$tab_id,$tab_id_prestation,'2016-01-01', '2016-06-30');
    $tab_p2 = ServicerenduQuery::getBy($id_entite,$tab_id,$tab_id_prestation,'2016-07-01', '2016-12-31');
    $tab_p3 = ServicerenduQuery::getBy($id_entite,$tab_id,$tab_id_prestation,'2017-01-01', '2017-12-31');
    $tab_pouvoir = ServicerenduQuery::getNbPouvoir($id_entite,$tab_id,$tab_id_prestation,'2017-06-30');
    $tab_individu = MembreIndividuQuery::getCompilationNomIndividu($tab_id,'2017-06-30',false);

    $tab_value = [
        [
            'ID',
            'Nom du membre',
            'Individus',
            'Adresse',
            'Ville',
            'Cotis. 2015',
            'Cotis. 2016',
            'Cotis. 2017',
            'NB pouvoir',
            'Présence',
            'Signature',
            'Recoit les pouvoirs de '
        ]
    ];
    $loader = new Twig_Loader_Filesystem($app['resources.path'] . '/documents');
    $twig_page = new Twig_Environment($loader, ['autoescape' => false]);

    foreach ($tab_membre as $membre) {

        $individu_titulaire = $membre->getIndividuTitulaire();
        $id_membre = $membre->getIdMembre();
        $tab_value[] = [
            $id_membre,
            $individu_titulaire->getNomFamille() . ' ' . $individu_titulaire->getPrenom(),
            isset($tab_individu[$id_membre])?implode('<br />',$tab_individu[$id_membre]):'',
            $individu_titulaire->getAdresse(),
            $individu_titulaire->getVille(),
            isset($tab_p1[$id_membre])?implode(' ',$tab_p1[$id_membre]):'',
            isset($tab_p2[$id_membre])?implode(' ',$tab_p2[$id_membre]):'',
            isset($tab_p3[$id_membre])?implode(' ',$tab_p3[$id_membre]):'',
            isset($tab_pouvoir[$id_membre])?$tab_pouvoir[$id_membre]:'0',
            '',
            '',
            ''

        ];


    }


    $content =  array2htmltable($tab_value, ['th' => true]);

    $page = $twig_page->render('document.html.twig', ['content' => $content]);
    $css='body{font-size:10px;}';
    $css = $twig_page->render('document_style.css.twig',['css'=>$css]).$twig_page->render('document_style_table.css.twig');
    $page = '<!DOCTYPE html><html><head><meta http-equiv="Content-Type" content="text/html; charset=UTF-8">' . $css . '</head><body>' . $page . '</body></html>';

    $options = [
        'binary' => '/usr/bin/xvfb-run /usr/bin/wkhtmltopdf',
        'tmpDir' => $app['tmp.path'] . '/print',
        'margin-bottom' => 10,
        'margin-left' => 5,
        'margin-right' => 5,
        'margin-top' => 5,
        'orientation' => 'Landscape',
        'footer-html' => $app['basepath'].'/resources/documents/bas_de_page.html',
    ];

    $pdf = new Pdf($options);



    $pdf = $pdf->addPage($page);

    $pdf->send();
    exit();


}