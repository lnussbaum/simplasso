<?php


function twig_extension(){
global $app;
$app->extend('twig', function ($twig, $app) {


    /*********************
     * GLOBAL TWIG
     * ******************/


    $twig->addGlobal('tva_oui', suc('tva_oui'));
    $twig->addGlobal('quantite_sr', suc('quantite_sr'));
    $twig->addGlobal('entite_multi', suc('entite_multi'));
    $twig->addGlobal('tresor_multi', suc('tresor_multi'));
    $twig->addGlobal('objet', sac('objet'));
    $twig->addGlobal('id', sac('id'));
    $twig->addGlobal('page', sac('page'));
    $twig->addGlobal('layout_cplt', (sac('ajax')) ? '_zero' : '');
    $twig->addGlobal('url_courante', $app['request']->getRequestUri());


    /*********************
     * FONCTIONS TWIG
     * ******************/


    $twig->addFunction(new Twig_SimpleFunction('sac', function ($var) {
        return sac($var);
    }));

    $twig->addFunction(new Twig_SimpleFunction('suc', function ($var) {
        return suc($var);
    }));

    $twig->addFunction(new Twig_SimpleFunction('descr', function ($var) {
        return descr($var);
    }));

    $twig->addFunction(new Twig_SimpleFunction('tab', function ($var) {
        return tab($var);
    }));

    $twig->addFunction(new Twig_SimpleFunction('conf', function ($var) {
        return lire_config($var);
    }));

    $twig->addFunction(new Twig_SimpleFunction('pref', function ($var, $defaut = "") {
        return pref($var, $defaut);
    }));
    $twig->addFunction(new Twig_SimpleFunction('liste_secondaire', function ($objet, $ecran) {
        return liste_secondaire($objet, $ecran);
    }));

    $twig->addFunction(new Twig_SimpleFunction('mot', function ($nomcourt) {
        return table_filtrer_valeur_premiere(tab('mot'), 'nomcourt', $nomcourt);
    }));

    $twig->addFunction(new Twig_SimpleFunction('fichier_twig', function ($type = '') {
        return fichier_twig($type);
    }));

    $twig->addFunction(new Twig_SimpleFunction('charger_objet', function ( $objet = null,$id=null) {
        return charger_objet($objet ,$id);
    }));

    $twig->addFunction(new Twig_SimpleFunction('charger_tab_objet', function ( $options = array()) {
        return charger_tab_objet($options);
    }));

    $twig->addFunction(new Twig_SimpleFunction('charger_enfant ', function ($objet_enfant) {

        return charger_enfant_direct($objet_enfant);
    }));
    $twig->addFunction(new Twig_SimpleFunction('charger_enfant_tableau', function ($objet_enfant, $tab_colonnes) {


        $tab = charger_enfant_direct($objet_enfant);
        $objet = descr($objet_enfant.'.alias');

        if (objet_liste_dataliste_detection_complement($objet)){
            $tab_data =  datatable_prepare_data($tab, $tab_colonnes, false);
            $tab_data = objet_liste_dataliste_complement($objet, $tab,$tab_data);
        }
        else
        {

            $tab_data =  datatable_prepare_data($tab, $tab_colonnes);
        }

        return $tab_data;

    }));

    $twig->addFunction(new Twig_SimpleFunction('preparation_tableau', function ($tab,$objet_enfant, $tab_colonnes) {


        $objet = descr($objet_enfant.'.alias');

        if (objet_liste_dataliste_detection_complement($objet)){
            $tab_data =  datatable_prepare_data($tab, $tab_colonnes, false);
            $tab_data = objet_liste_dataliste_complement($objet, $tab,$tab_data);
        }
        else
        {
            $tab_data =  datatable_prepare_data($tab, $tab_colonnes);
        }

        return $tab_data;

    }));


    $twig->addFunction(new Twig_SimpleFunction('documentation_lien', function ($page = '', $ancre = '') {
        return documentation_lien($page, $ancre);
    }, array('is_safe' => array('html'))));

    $twig->addFunction(new Twig_SimpleFunction('datePeriode', function ($date_debut, $date_fin) {
        return date_periode($date_debut, $date_fin);
    }, array('is_safe' => array('html'))));

    $twig->addFunction(new Twig_SimpleFunction('bouton', function ($action, $options = array()) {
        global $app;
        if ($btn = bouton($action, $options)) {
            return $app['twig']->render('inclure/lien.html.twig', $btn);
        } else {
            return '';
        }
    }, array('is_safe' => array('html'))));


    $twig->addFunction(new Twig_SimpleFunction('declencheur', function ($valeur_test = null) {
        global $app;
        $request = $app['request'];
        if ($valeur_test === null) {
            return $request->get('declencheur');
        } else {
            return ($request->get('declencheur') == $valeur_test);
        }
    }, array()));


    $twig->addFunction(new Twig_SimpleFunction('autoriser', function ($profil, $id_entite = "") {
        return autoriser($profil, $id_entite);
    }, array()));


    $twig->addFunction(new Twig_SimpleFunction('image_mini', function ($objet, $id_objet, $alt = "") {
        global $app;
        $image = GedLienQuery::create()->filterByObjet($objet)->filterByIdObjet($id_objet)->exists();
        if ($image) {
            return '<img src="' . $app->path('image', array(
                'type' => 'miniature',
                'objet' => $objet,
                'id_objet' => $id_objet
            )) . '" alt="' . $alt . '"  class="img-thumbnail" />';
        } else {
            return '<i class="fa fa-user" /></i>';
        }
    }), array('pre_escape' => false, 'is_safe' => false));







    $twig->addFunction(new Twig_SimpleFunction('arbre', function ($code) {
        if (!is_array($code)) {
            return $code;
        }
        return arbre($code);
    }));

    $twig->addFunction(new Twig_SimpleFunction('getPrestationDeType', function ($type) {
        return getPrestationDeType($type);
    }));

    $twig->addFunction(new Twig_SimpleFunction('menu_page_droite', function () {
        return menu_page_droite();
    }));
    $twig->addFunction(new Twig_SimpleFunction('menu_page_gauche', function () {
        return menu_page_gauche();
    }));
    $twig->addFunction(new Twig_SimpleFunction('decode', function ($type, $code = null) {
        $type_autorise = [
            'regle' => 'ListeRegle',
            'type_op' => 'TypeOp',
            'etat' => 'Etat'
        ];
        if (isset($type_autorise[$type])) {
            $nom_fonction = 'getListe' . $type_autorise[$type];
            return $nom_fonction($code);
        }
        return $code;
    }));
    $twig->addFunction(new Twig_SimpleFunction('log_operations', function ($objet, $id_objet) {
        return Log::log_operations($objet, $id_objet);
    }));

    $twig->addFunction(new Twig_SimpleFunction('donneTabCol', function ($objet = null) {
        return donneTabCol($objet);
    }));
    $twig->addFunction(new Twig_SimpleFunction('donneTabFiltre', function ($objet = null) {
        return donneTabFiltre($objet);
    }));

    /*********************
     * FILTRES TWIG
     * ******************/


    $twig->addFilter(new Twig_SimpleFilter('trouverObjet', function ($nom_fichier_twig) {

        if ($prefixe = strpos($nom_fichier_twig, '/')) {
            $prefixe++;
        } else {
            $prefixe = 0;
        }
        if ($pos = strrpos($nom_fichier_twig, '_')) {
            return substr($nom_fichier_twig, $prefixe, $pos - $prefixe);
        }
        return '';
    }));

    $twig->addFilter(new Twig_SimpleFilter('exclure_cles', function ($tab_value, $tab_keys) {

        foreach ($tab_keys as $key) {
            unset($tab_value[$key]);
        }
        return $tab_value;
    }));

    $twig->addFilter(new Twig_SimpleFilter('liste_filtre', function ($tab_filtre) {

        return liste_filtre($tab_filtre);
    }));


    $twig->addFilter(new Twig_SimpleFilter('array_values', function ($tab) {

        return array_values($tab);
    }));


    $twig->addFilter(new Twig_SimpleFilter('classLog', function ($code_log) {
        return class_css_action(substr($code_log, 7, 3)) . ' ' . class_css_objet(substr($code_log, 4, 3), 'bolt');
    }));

    $twig->addFilter(new Twig_SimpleFilter('transtab', function ($tab, $prefixe = '', $suffixe = '') {
        global $app;
        if (is_array($tab)) {
            foreach ($tab as &$t) {
                $t = $app->trans($prefixe . $t . $suffixe);
            }
        } else {
            $tab = $app->trans($prefixe . $tab . $suffixe);
        }
        return $tab;
    }));


    $twig->addFilter(new Twig_SimpleFilter('donneNomPHP', function ($nom) {
        return ucfirst($nom);
    }));


    $twig->addFilter(new Twig_SimpleFilter('getProfil', function ($code) {
        $tab = getProfil();
        if (isset($tab[$code])) {
            return $tab[$code];
        } else {
            return '';
        }
    }));
    $twig->addFilter(new Twig_SimpleFilter('getNiveauPrefs', function ($code) {
        $tab = array();
        $tab = getNiveauPrefs();
        if (isset($tab[$code])) {
            return $tab[$code];
        } else {
            return '';
        }
    }));
    $twig->addFilter(new Twig_SimpleFilter('getNiveau', function ($code) {
        $tab = getNiveau();
        if (isset($tab[$code])) {
            return $tab[$code];
        } else {
            return '';
        }
    }));
    $twig->addFilter(new Twig_SimpleFilter('getEcranSaisie', function ($code) {
        $tab = getListeEcranSaisie();
        if (isset($tab[$code])) {
            return $tab[$code];
        } else {
            return '';
        }
    }));
    $twig->addFilter(new Twig_SimpleFilter('getSoldeType', function ($code) {
        $tab = getListeSoldeType();
        if (isset($tab[$code])) {
            return $tab[$code];
        } else {
            return '';
        }
    }));
    $twig->addFilter(new Twig_SimpleFilter('getPieceType', function ($code) {
        $tab = getListePieceType();
        if (isset($tab[$code])) {
            return $tab[$code];
        } else {
            return '';
        }
    }));
    $twig->addFilter(new Twig_SimpleFilter('ouinon', function ($bool) {
        if ($bool == 1) {
            return 'oui';
        } else {
            return 'non';
        }
    }));
    $twig->addFilter(new Twig_SimpleFilter('nonoui', function ($bool) {
        if ($bool == 1) {
            return 'non';
        } else {
            return 'oui';
        }
    }));
    $twig->addFilter(new Twig_SimpleFilter('getEtat', function ($code) {
        $tab = getListeEtat();
        if (isset($tab[$code])) {
            return $tab[$code];
        } else {
            return '';
        }
    }));
    $twig->addFilter(new Twig_SimpleFilter('getTypeOp', function ($code) {
        $tab = getListeTypeOp();
        if (isset($tab[$code])) {
            return $tab[$code];
        } else {
            return '';
        }
    }));
    $twig->addFilter(new Twig_SimpleFilter('getRegle', function ($code) {
        $tab = getListeRegle();
        if (isset($tab[$code])) {
            return $tab[$code];
        } else {
            return '';
        }
    }));


    $twig->addFilter(new Twig_SimpleFilter('getPays', function ($id) {
        $pays = tab('pays');
        if (isset($pays[$id])) {
            return $pays[$id];
        } else {
            return '';
        }
    }));
    $twig->addFilter(new Twig_SimpleFilter('getNomMembre', function ($id) {
        $membre = MembreQuery::create()->findPk($id);
        if ($membre) {
            return $membre->getNom();
        } else {
            return $id;
        }
    }));
    $twig->addFilter(new Twig_SimpleFilter('getNomEntite', function ($id) {
        if ($id == 0) {
            return 'Pour toutes';
        }
        $tab = tab('entite.' . $id . '.nom');
        if (isset($tab)) {
            return $tab;
        } else {
            return $id;
        }
    }));
    $twig->addFilter(new Twig_SimpleFilter('getNomIntitule', function ($id) {
        $tab = tab('compte.' . $id . '.ncompte').' '.tab('compte.' . $id . '.nom');
        if ($id == 0 or $id==null) {
            return 'non defini';
        }
        if (isset($tab)) {
            return $tab;
        } else {
            return $id;
        }
    }));
    $twig->addFilter(new Twig_SimpleFilter('getNomIndividu', function ($id) {
        if ($id == 0) {
            return '';
        }
        $individu = IndividuQuery::create()->findPk($id);
        if ($individu) {
            return $individu->getNom();
        }
        return $id;
    }));
    $twig->addFilter(new Twig_SimpleFilter('getNomJournal', function ($id) {
        if ($id == 0) {
            return '';
        }
        $individu = JournalQuery::create()->findPk($id);
        if ($individu) {
            return $individu->getNom();
        }
        return $id;
    }));
    $twig->addFilter(new Twig_SimpleFilter('getNomMotparent', function ($id) {
        if ($id == 0) {
            return 'Aucun';
        }
        $nom_parent = MotgroupeQuery::create()->findPk($id);
        if ($nom_parent) {
            return $nom_parent->getNom();
        } else {
            return $id;
        }
    }));
    $twig->addFilter(new Twig_SimpleFilter('getNomPrestation', function ($id) {
        if ($id == 0) {
            return 'Pour toutes';
        }
        $tab = tab('prestation');
        if (isset($tab[$id])) {
            return $tab[$id]['nom'];
        } else {
            return $id;
        }
    }));
    $twig->addFilter(new Twig_SimpleFilter('getNomTresor', function ($id) {
        if ($id == 0) {
            return 'Pour toutes';
        }
        $tab = tab('tresor.' . $id . '.nom');
        if (isset($tab)) {
            return $tab;
        } else {
            return $id;
        }
    }));
    $twig->addFilter(new Twig_SimpleFilter('exclure_mot_systeme', function ($tab_mot) {
        $mots_systeme = table_simplifier(table_filtrer_valeur(tab('mot'), 'systeme', true));
        return array_diff_key($tab_mot, $mots_systeme);
    }));


    $twig->addFilter(new Twig_SimpleFilter('carte_adherent_etat', function ($tab_id_mot) {
        $etat = 0;
        if (!empty($tab_id_mot)) {
            $mot_groupe = table_filtrer_valeur_premiere(tab('motgroupe'), 'nomcourt', 'carte');
            $mots = table_filtrer_valeur(tab('mot'), 'id_motgroupe', $mot_groupe['id_motgroupe']);
            foreach ($mots as $id => $m) {
                if (in_array($id, $tab_id_mot)) {
                    $etat = max($etat, substr($m['nomcourt'], -1));
                }
            }
        }
        return $etat;
    }));

    $twig->addFilter(new Twig_SimpleFilter('closingTag', function ($opentag) {
        if ($pos = strpos($opentag, ' ')) {
            $opentag = substr($opentag, 0, $pos) . '>';
        }
        $opentag = str_replace('<', '</', $opentag);
        return $opentag;
    }));

    $twig->addFilter(new Twig_SimpleFilter('getNomCommune', function ($code) {

        $code_dep = substr($code, 0, 2);
        $code_com = substr($code, 2);
        $commune = CommuneQuery::create()->filterByDepartement($code_dep)->filterByCode($code_com)->findOne();

        if ($commune) {
            return $commune->getNom();
        } else {
            return '';
        }
    }, array('is_safe' => array('html'))));


    $twig->addFilter(new Twig_SimpleFilter('lien_courriel', function ($email) {
        if (!empty($email)) {
            return '<a href="mailto:' . $email . '">' . $email . '</a>';
        }
        return '';
    }, array('is_safe' => array('html'))));


    $twig->addFilter(new Twig_SimpleFilter('array2htmltable', function ($tab, $options = array()) {

        return array2htmltable($tab, $options);
    }, array('is_safe' => array('html'))));


    $twig->addFilter(new Twig_SimpleFilter('urlise', function ($tab_donnee) {
        global $app;

        if (isset($tab_donnee['id_membre'])) {

            if (strpos($tab_donnee['id_membre'], ',')) {
                $tab_membre = explode(', ', $tab_donnee['id_membre']);
                $tab_donnee['%urls_membres%'] = array();
                foreach ($tab_membre as $id_membre) {
                    $tab_donnee['%urls_membres%'][] = '<a href="' . $app->url('membre',
                            array('id_membre' => $id_membre)) . '">' . $id_membre . '</a>';
                }
                $tab_donnee['%urls_membres%'] = implode(', ', $tab_donnee['%urls_membres%']);

            } else {
                $tab_donnee['%url_membre%'] = '<a href="' . $app->url('membre',
                        array('id_membre' => $tab_donnee['id_membre'])) . '">' . $tab_donnee['id_membre'] . '</a>';
            }
        }

        return $tab_donnee;


    }));

    $twig->addFilter(new Twig_SimpleFilter('autorisation', function ($droits) {
        global $app;
        $tab_droit = $app['user']->getRolesSansEntite();
        $tab_droit[] = 'all';
        if (!is_array($droits)) {
            $droits = array($droits);
        }

        return !empty(array_intersect($droits, $tab_droit));

    }, array()));


    $twig->addFilter(new Twig_SimpleFilter('afficher_telephone', function ($num) {
        return afficher_telephone($num);

    }, array()));

    $twig->addFilter(new Twig_SimpleFilter('autorisationPage', function ($nom_page) {

        if (substr($nom_page, 0, 4) == 'http') {
            return true;
        } elseif (substr($nom_page, 0, 1) != '#') {
            return pageAutorisee($nom_page);
        } else {
            return false;
        }
    }, array()));


    return $twig;
});
}








