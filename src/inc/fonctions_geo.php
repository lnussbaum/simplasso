<?php
// reste un PB d'arrondi dans le coordonées
// enregistré  3.1173578642614                         614
// openstreetmap renvoi 3.11735786426135               6135
// fait un round en attendant l'écriture correcte dans le fichier 
function donne_coordonnee_avec_adresse($adresse, $ville)
{

    $tab = false;
    $url = conf('geo.nominatim.url');
    $param_search = conf('geo.nominatim.params_search');
    $params = conf('geo.nominatim.params');

    $fp = @fopen($url . '?' . $params . '&' . $param_search . '=' . urlencode($adresse . ', ' . $ville), "r");
    // echo($url . '?' . $params. '&' . $param_search . '=' . urlencode($adresse . ', ' . $ville) );

    if ($fp) {
        $chaine = "";
        while (($line = fgets($fp)) !== false) {
            $chaine .= $line;
        }

        $chaine = json_decode($chaine);
        if (isset($chaine[0])) {
            $tab = array('lat' => $chaine[0]->lat, 'lon' => $chaine[0]->lon, 'ad' => '');
        }

        fclose($fp);
    }
    return $tab;

}


function formatAdressePourRecherche($adresse)
{
    $adresse = str_replace("/", "", $adresse);
    $adresse = str_replace("\r\n", " ", $adresse);
    $adresse = trim(preg_replace('`(bat|batiment|appartement|apt|app|appt|appart|etage|étage|etg) [0-9]{1,}(er|ere)?`i',
        '', $adresse));
    $adresse = trim(preg_replace('`([0-9]{1,}) (bd|bvd|bvld|bvlrd) `i', '${1} boulevard ', $adresse));
    $adresse = trim(preg_replace('/ BP \d+/', '', $adresse));
    return $adresse;
}

function formatvillePourRecherche($ville)
{
    $res = str_replace("/", "", $ville);
    $res = str_replace("\r\n", " ", $res);
    $res = str_replace(" - ", " ", $res);
    $res = str_replace("CEDEX", "", $res);
    return $res;
}

function traitement_form_geo($objet_data, $data, $modification, $objet)
{
    $id = $objet_data->getPrimaryKey();
    $msg = '';
    if (!empty($data['adresse']) or !empty($data['ville'])) {
        $adres = formatAdressePourRecherche($data['adresse']);
        $ville = formatVillePourRecherche($data['ville']);
        $coord = donne_coordonnee_avec_adresse($adres, $ville);

        $msg = $id;
        if (isset($data['nom'])) {
            $msg .= " - " . $data['nom'];
        }
        $msg .= "\n       -adresse  : " . $adres;
        $msg .= "\n       -ville  : " . $ville;
        if ($coord) {
            $msg .= creer_modifier_position_lien($coord['lon'], $coord['lat'], $objet, $id);
        }
        $msg .= PHP_EOL;
    }
    return $msg;
}


function creer_modifier_position_lien($lon, $lat, $objet, $id)
{

    $objet_gis_lien_ancien = PositionLienQuery::create()->filterByObjet($objet)->filterByIdObjet($id)->findOne();
    if ($objet_gis_lien_ancien) {
        $position = $objet_gis_lien_ancien->getPosition();
        $position->setLat($lat);
        $position->setLon($lon);
        $position->save();
    } else {
        $position = new Position();
        $position->setLat($lat);
        $position->setLon($lon);
        $position->save();
        $position_lien = new PositionLien();
        $position_lien->setIdPosition($position->getIdPosition());
        $position_lien->setIdObjet($id);
        $position_lien->setObjet($objet);
        $position_lien->save();
    }
    return true;
}

function creer_corriger_les_points_en_double($lon, $lat)
{
//    echo '\r longitude'. $lon.' latitude '.$lat;
//    echo '\r longitude'. round($lon,13).' latitude '.round($lat,13);


    $tab_objet_gis = PositionQuery::create()->filterByLon(round($lon, 13))->filterByLat(round($lat, 13))->find();

    if (count($tab_objet_gis)) {

        $i = 0;
        foreach ($tab_objet_gis as $gis) {
            $i++;
            if ($i == 1) {
                $id_gis = $gis->getIdPosition();
            }
            //                echo "<br>Réafecter les liens du point en double sur le 1° point" . $lon . '  ' . $lat . ' ' . $id_gis;
            $tab_objet_gis_lien_ancien = PositionLienQuery::create()->filterByIdPosition($gis->getIdPosition())->find();
            foreach ($tab_objet_gis_lien_ancien as $temp) {
//                    arbre($temp);
                $temp->setIdPosition($id_gis);
                $temp->save();
//                    echo "<br>lien modifié <br>";
//                    arbre($temp);
            }

            if ($i != 1) {
                $id_gis = $gis->getIdPosition();
//                    echo "<br>point a effacer <br>";
//                arbre($gis);
                $gis->delete();
            }
        }
    } else {
//        echo 'ligne 200';
        $objet_gis_nouveau = new Position();
        $objet_gis_nouveau->setLat($lat);
        $objet_gis_nouveau->setLon($lon);
        $objet_gis_nouveau->save();
        $id_gis = $objet_gis_nouveau->getIdPosition();

    }
    return $id_gis;
}


function rectifie_lon($distance, $lat)
{
    return rad2deg($distance / 6378137 / cos(deg2rad($lat)));
}


function rectifie_lat($distance)
{
    return rad2deg($distance / 6356752);
}


function rechercherObjetsParZone($id_zone, $objet)
{

    $zone = ZoneQuery::create()->findPk($id_zone);
    $tab_gisl = PositionLienQuery::create()->filterByObjet($objet)->usePositionQuery()->endUse()->find();

    $tab_id_objet = [];
    if (!empty($tab_gisl)) {
        $tab_gis = [];
        foreach ($tab_gisl as $gl) {
            $g = $gl->getPosition();
            $tab_gis[$g->getLon() . ' ' . $g->getLat()][] = $gl->getIdObjet();
        }

        $trace_geo = geoPHP::load('MULTIPOINT(' . implode(',', array_keys($tab_gis)) . ')', 'wkt');
        $trace_geo = $trace_geo->out('json');
        $trace_geo = geoPHP::load($trace_geo, 'json');
        $zone_geo = $zone->getZoneGeo();
        $tab_geos = json_decode($zone_geo);
        foreach ($tab_geos as $geo) {

            if ($geo->type == 'circle') {
                $rayon = $geo->plus->radius;
                $geojson = $geo->geojson->geometry;
                $lon = $geojson->coordinates[0];
                $lat = $geojson->coordinates[1];
                $nb_segment = 60;
                $angle = 360 / $nb_segment;
                $tab_point = [];
                for ($i = 0; $i < $nb_segment; $i++) {
                    $lonp = $lon + rectifie_lon(cos(deg2rad($angle * $i)) * $rayon, $lat);
                    $latp = $lat + rectifie_lat(sin(deg2rad($angle * $i)) * $rayon);
                    $tab_point[] = [$lonp, $latp];
                }
                $tab_point[] = $tab_point[0];
                $geojson->type = 'Polygon';
                $geojson->coordinates = [$tab_point];
                $geometry = geoPHP::load($geojson, 'json');
            } else {
                $geometry = geoPHP::load($geo->geojson, 'json');
            }

            if ($geo_result = $trace_geo->intersection($geometry)) {
                $tab_point = array();
                if (get_class($geo_result) == 'Point') {
                    $tab_point[$geo_result->coords[0] . ' ' . $geo_result->coords[1]] = 1;
                } else {
                    foreach ($geo_result->components as $k => $p) {
                        $tab_point[$p->coords[0] . ' ' . $p->coords[1]] = $k;
                    }
                }
                foreach ($tab_point as $i => $k) {
                    $tab_id_objet = array_merge($tab_id_objet, array_values($tab_gis[$i]));
                }
            }
        }
    }
    return array_values($tab_id_objet);
}


function traitement_rechercher_commune($tab_ville, $ville, $code_postal)
{

    global $app;

    $id_commune = '';
    if (isset($tab_ville[$ville])) {
        $id_commune = $tab_ville[$ville];
    } else {
        $departement = substr($code_postal, 0, 2);
        $tab_args = ['nom' => $ville];
        if (intval($departement) > 0 || $departement == '2A' || $departement == '2B') {
            $tab_args['departement'] = $departement;
        }
        $sous_requete = getSelectionObjet('commune', $tab_args);
        $communes = $app['db']->fetchAll($sous_requete . ' LIMIT 0,10');

        if (isset($communes[0])) {

            $tab_ville[$ville] = $communes[0]['id_commune'];
            $id_commune = $communes[0]['id_commune'];
        }
    }
    return [$tab_ville, $id_commune];


}