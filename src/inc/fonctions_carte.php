<?php


function carte_changer_etat($objet,$id){

    global $app;
    $motgroupe = table_filtrer_valeur_premiere(tab('motgroupe'), 'nomcourt', 'carte');
    $mots = array_keys(table_filtrer_valeur(tab('mot'), 'id_motgroupe', $motgroupe['id_motgroupe']));
    $valeur_depart = 0;

    $lien = MotLienQuery::create()->filterByIdMot($mots)->filterByIdObjet($id)->findOneByObjet($objet);
    if($lien){
        $valeur_depart = substr(tab('mot.'.$lien->getIdMot())['nomcourt'], -1);
        $lien->delete();
    }
    $valeur = ($valeur_depart+1)%5;
    if ( $valeur==2 && !conf('carte_adherent.etat_a_remettre'))
        $valeur++;
    if ( $valeur==3 && !conf('carte_adherent.etat_a_envoyer'))
        $valeur++;
    $valeur_futur = ($valeur+1)%5;


    if ($valeur > 0) {
        $mot = table_filtrer_valeur_premiere(tab('mot'), 'nomcourt', 'carte_adh' . $valeur);
        $ajout = Mot::MotLien($mot['id_mot'], $id, $objet, '', false);
        if ($ajout) {
            Log::EnrOp('CAR', $objet, $id);
            $valeur = substr($mot['nomcourt'], -1);
        }
    }
    return $app->json(['ok' => true, 'valeur' => $valeur,'valeur_depart' => $valeur_depart,'valeur_futur' => $valeur_futur]);


}