<?php


function getObjetLog($id = null){


    $tab=array();
    foreach (sac('description') as $key=>$data){
        if (isset($data['objet_action'])) {
            $tab[$data['log']] = $data['objet_action'];
        } else {
            $tab[$data['log']] = $key;
        }
    }
    $tab= $tab + ['ACT' => 'activite',
    'AUT' => 'autorisation',
    'COM' => 'compte',
    'ECR' => 'ecriture',
    'ENT' => 'entite',
    'GED' => 'ged',
    'JOU' => 'journal',
    'PIE' => 'piece',
    'POS' => 'poste',
    'TRE' => 'tresor',
    'IMP' => 'import',
    'CON' => 'config',
    'PRE' => 'prestation'];
    return getValeur($tab,$id);
}





function class_css_action($code){

    $tab = [
        'CRE'=>'bg-green',
        'MOD'=>'bg-orange',
        'SUP'=>'bg-red',
    ];
    return getValeur($tab,$code);
}





function class_css_objet($code,$substition){

    $tab = [
        'IND'=>'fa-user',
        'MEM'=>'fa-users',
        'SR1'=>'fa-eur',
        'PAI'=>'fa-eur',
        'SR2'=>'fa-eur',
        'SR3'=>'fa-eur',
        'SR4'=>'fa-eur',
        'SR5'=>'fa-eur',
        'SR6'=>'fa-eur',
    ];
    return getValeur($tab,$code,null,$substition);
}



function getProfil($id = null) {

    $tab = array(
        'A' => 'Administrateur',
        'C' => 'Comptabilitè',
        'G' => 'Gestion'
    );
    return getValeur($tab,$id);
}

function getNiveau($id = null) {

    $tab = array(
        '1' => 'Voir',
        '2' => 'Modifier',
        '3' => 'Supprimer',
        '4' => 'Responsable',
        '5' => 'Superviseur',
        '6' => 'Contributeur expert'
    );
    return getValeur($tab,$id);
}

function getNiveauPrefs($id = null) {

    $tab = array(
        '1' => 'Entite',
        '2' => 'Operateur',
        '3' => 'EntiteOperateur',
        '4' => 'aucun spécifique'
    );
    return getValeur($tab,$id);
}


function getCreation($id = null,$champ = null) {

    $tab = array(
        'entite' => array('nom' => 'Entite', 'nomcourt' => 'Entite'),
        'activite' => array('nom' => 'Frais généraux', 'nomcourt' => 'FG'),
        'activite2' => array('nom' => 'Sans objet', 'nomcourt' => 'SO'),
        'compte' => array('nom' => 'Compte courant NEF', 'ncompte' => '512000', 'nomcourt' => 'CC NEF'),
        'compte_cp' => array('nom' => 'En attente', 'ncompte' => '4700', 'nomcourt' => 'Attend'),
        'journal' => array('nom' => 'Compte courant NEF', 'nomcourt' => 'BN', 'ncompte' => '512000'),
        'poste' => array('nom' => 'Bilan', 'nomcourt' => 'BI'),
        'poste_ge' => array('nom' => 'Gestion', 'nomcourt' => 'GE')
    );
    return getValeur($tab,$id,$champ);
}


function getListeEcranSaisie($id = null) {

    $tab = array(
        'st' => 'Standard',
        'ac' => 'Achats',
        'at' => 'Achats tva',
        've' => 'Ventes ',
        'vt' => 'Ventes tva',
        'pa' => 'paie',
        're' => 'reglement',
        'en' => 'encaissement',
        'ba' => 'relevé de banque'
    );
    return getValeur($tab,$id);
}


function getListePieceType($id = null) {

    $tab = array(
        '1' => 'Générale',
        '2' => 'Facture fournisseur',
        '2T' => 'Facture fournisseur Tva',
        '3' => 'Facture client',
        '3T' => 'Facture client',
        '4' => 'Paiement',
        '5' => 'Encaissement',
        '6' => 'Banque relevé',
        '7' => 'Suite',
        '8' => 'A définir'
    );
    return getValeur($tab,$id);
}

function getListeRegle($id = null) {

    $tab =array(
        '1' => 'Non',
        '2' => 'Oui',
        '3' => 'Partiel+',
        '4' => '+Don',
        '5' => '-Perte',
        '6' => '+Autre ou disponible',
        '7' => 'A revoir'
    );
    return getValeur($tab,$id);
}

function getListeSoldeType($id = null) {

    $tab =array(
        '7' => 'attente',
        '4' => '+Don',
        '5' => '-Perte'
    );
    return getValeur($tab,$id);
}


//extends="Ecritures"
function getListeEtat($id = null) {
    $tab = array(
        '1' => 'budget',
        '2' => 'brouillard',
        '3' => 'valide'
    );
    return getValeur($tab,$id);
}

function getListePeriodiqueB($id = null) {

    $tab = array(
        'Y' => 'année',
        'M' => 'mois',
        'D' => 'jour',
    );
    return getValeur($tab,$id);
}


//extends="Prestation"
function getListePeriodiqueA($id = null) {

    $tab = array(
        '0' => 'Non : Pas de durée',
        '1' => 'Fixe : suivant la durée et le ou les débuts de période',
        '2' => 'Glissant : La période commence le prochain jour prévu',
    );
    return getValeur($tab,$id);
}





function getCourrierType($id = null) {

    $tab = array(
        '1' => 'relance',
        '2' => 'convocation'
    );
    return getValeur($tab,$id);
}


//extends="Compte"
function getListeTypeOp($id = null) {

    $tab = array(
        '1' => 'credit',
        '2' => 'debit',
        '3' => 'multi'
    );
    return getValeur($tab,$id);
}


function getPreselection($id = null) {
    $tab = array(
        'membre_echu' => 'membre_echu',
        'membre_ardent_echu' => 'membre_ardent_echu'
    );
    return getValeur($tab,$id);
}


function getCourrierCanal($id = null) {

     $tab  = array(
        'S' => 'sms',
        'L' => 'lettre',
        'E' => 'email',
        'C' => 'carte'
    );
    return getValeur($tab,$id);
}


function getModelesDocuments() {

    $tab_modeles = array(
        'mail_invitation_gestionnaire' => array(
            'nom' => 'Courriel d\'invitation des gestionnaires',
            'description' => 'Courriel pour inviter les gestionnaires sur l\'outils',
            'avec_titre' => true,
            'param' => array('{{ login }}', '{{ mot_de_passe }}', '{{url_site_admin}}')
        ),
        'document_arrete' => array(
            'nom' => 'Arrêté',
            'description' => '',
            'param' => array(
                "acc_spec : accompagnement spécifique si nécessaire",
                "AA/AA/AAAA : date du jour",
                "XX/XX/XXXX : date de la demande",
                "Liste_pref : Préfecture du...",
                "liste_pref : Le préfet...",
                "Liste_del : Pour le préfet...",
                "accompagnement_convoi",
                "vu_accords : Vu l'accord du Nord ou du PdC",
                "duree_mois",
                "NPCIDC : numéro de dossier",
                "PTR",
                "LONG",
                "LARG",
                "HAUT",
                "CPEMAX",
                "PTV : à vide",
                "LONV",
                "LARV",
                "HAUV",
                "CTGR",
                "X - adresse : pétitionnaire",
                "_ITI_",
                "description_trajet",
                "_nvoyages",
                "_joint_en_annexe_2",
                "documents_joints"
            )
        ),

    );
    return $tab_modeles;


}


function getMimeTypes($id = null) {

    $tab = array(
        'txt' => 'text/plain',
        'htm' => 'text/html',
        'html' => 'text/html',
        'php' => 'text/html',
        'css' => 'text/css',
        'csv' => 'text/csv',
        'js' => 'application/javascript',
        'json' => 'application/json',
        'xml' => 'application/xml',
        'swf' => 'application/x-shockwave-flash',
        'flv' => 'video/x-flv',
        // images
        'png' => 'image/png',
        'jpe' => 'image/jpeg',
        'jpeg' => 'image/jpeg',
        'jpg' => 'image/jpeg',
        'gif' => 'image/gif',
        'bmp' => 'image/bmp',
        'ico' => 'image/vnd.microsoft.icon',
        'tiff' => 'image/tiff',
        'tif' => 'image/tiff',
        'svg' => 'image/svg+xml',
        'svgz' => 'image/svg+xml',
        // archives
        'zip' => 'application/zip',
        'rar' => 'application/x-rar-compressed',
        'exe' => 'application/x-msdownload',
        'msi' => 'application/x-msdownload',
        'cab' => 'application/vnd.ms-cab-compressed',
        // audio/video
        'mp3' => 'audio/mpeg',
        'qt' => 'video/quicktime',
        'mov' => 'video/quicktime',
        // adobe
        'pdf' => 'application/pdf',
        'psd' => 'image/vnd.adobe.photoshop',
        'ai' => 'application/postscript',
        'eps' => 'application/postscript',
        'ps' => 'application/postscript',
        // ms office
        'doc' => 'application/msword',
        'rtf' => 'application/rtf',
        'xls' => 'application/vnd.ms-excel',
        'ppt' => 'application/vnd.ms-powerpoint',
        // open office
        'odt' => 'application/vnd.oasis.opendocument.text',
        'ods' => 'application/vnd.oasis.opendocument.spreadsheet'
    );

    return getValeur($tab,$id);

}


function getValeur($tab, $id = null, $champs = null,$substitution='') {
    if ($id!==null) {
        if (isset($tab[$id])) {
            if ($champs) {
                if (isset($tab[$id][$champs])) {
                    $tab[$id][$champs];
                } else {
                    return $substitution;
                }
            } else {
                return $tab[$id];
            }
        } else {
            return $substitution;
        }
    }
    return $tab;
}


function getSuperCache() {
    return array('description', 'table', 'pages', 'droits', 'config', 'preference');
}

/*lire au lieu de get car getdescription existe dans vendor*/

function liste_des_objets() {

    return array(
//ASSC
        'activite' => array(
            'prefixe' => 'assc',
            'objet1' => array('ecriture'),//, 'piece'),
            'objet_1' => array('entite'),
        ),
        'compte' => array(
            'choix_colonnes' => array('ncompte', 'nom'),
            'prefixe' => 'assc',
            'objet1' => array('piece'),
            'objet_1' => array('entite', 'poste'),
            'joint_filtre' => array('prestation' => array('entite' => 'id_entite')),
        ),
        'ecriture' => array(
            'prefixe' => 'assc',
            'objet_1' => array('piece', 'activite', 'compte', 'journal', 'entite'),
            'objet_2' => array('compte' => array('entite', 'poste')),
        ),
        //GED
        'journal' => array(
            'prefixe' => 'assc',
            'objet1' => array('piece'),
            'objet_1' => array('entite', 'compte'),
            'objet_2' => array('compte' => array('entite')),
        ),
        'piece' => array(
            'prefixe' => 'assc',
            'objet1' => array('ecriture'),
            'objet_1' => array('journal', 'compte', 'activite'),
            'objet_2' => array('journal' => array('entite')),
        ),
        'poste' => array(
            'prefixe' => 'assc',
            'compta' => false,
            'objet1' => array('compte'),
            'objet2' => array('compte' => array('piece')),
        ),
        //ASSO
        'membre' => array(
            'droits' => array('all'),
            'objet1' => array(
                'sr_cotisation', 'sr_adhesion', 'sr_vente', 'sr_abonnement', 'paiement', 'sr_don', 'sr_perte'
            ),
            'objet_1' => array('entite', 'individu'),
        ),
        'individu' => array(
//            'prefixe' => 'spip',
//            'nom_sql' => 'auteur', //dans la gestion des tables sql
//            'cle'=>'id_auteur',// non obligatoire si identique a alias
                'joint_filtre' => array(
                'config' => array('autorisation' => 'id_individu'),
                'autorisation' => array('autorisation' => 'id_individu')
            ),
            'source' => 'asso/',// pour surcharger le prefixe (pris par defaut)
            'objet1' => array('membre', 'autorisation','sr_cotisation', 'sr_adhesion', 'sr_vente', 'sr_abonnement', 'paiement', 'sr_don', 'sr_perte')
        ),
        'autorisation' => array(
            'objet_1' => array('entite', 'individu'),
//             'nom_ecran'=> '_form',// ou s'il n'ont pas le meme nom : nom_ecran_php_voir=>'ecranxx_fairexx' (6 fois sinon modele ....
// todo ajouté car la ligne 20 de objet.html.twig bloque renvoi ver autorisation_liste au lieu de objet
            'php' => array('voir' => 'autorisation_liste', 'liste' => 'autorisation_liste', 'form' => 'autorisation_form', 'table' => 'autorisation_liste'),// pas de controle physique
            'twig' => array('voir' => 'autorisation_liste', 'liste' => 'autorisation_liste', 'form' => 'autorisation_form', 'table' => 'autorisation_table'),// pas de controle physique
        ),
        'config' => array(
            'source' => 'systeme/',
            'objet_1' => array('entite', 'individu'),
        ),
        'entite' => array(
            'objet1' => array('prestation', 'tresor'/*, 'journal', 'activite', 'compte'*/),
            'objet2' => array('prestation' => array('prix')),
        ),
        'import' => array(
            'source' => 'importation/'
        ),
        'mot' => array(
            'objet_1' => array('motgroupe'),
        ),
        'log' => array(),
        'motgroupe' => array(
            'log' => 'MOG',
            'objet_1' => array('motgroupe'),
            'objet1' => array('mot'),
        ),
        'paiement' => array(
            'objet1' => array('servicerendu'),
            'objet_1' => array('membre', 'entite', 'tresor', 'prestation'),
        ),
        'prestation' => array(
            'objet1' => array('prix'),
            'objet_1' => array('entite', 'compte'),
        ),
        'prestationslot' => array(
            'log' => 'PRL',
        ),
        'prix' => array(
            'objet_1' => array('prestation'),
            'objet_2' => array('prestation' => array('entite')),
        ),

        'courrier' => array(
            'prefixe' => 'com',
            'objet1' => array('courrierlien'),
            'objet_1' => array('membre', 'entite'),
            'image' => 'asso_servicerendu.png',
        ),

        'courrierlien' => array(
            'prefixe' => 'com',
            'log' => 'COU',
            'objet_1' => array('courrier', 'relance', 'membre'),
            'phpname' => 'CourrierLien',
            'nom_sql' => 'courriers_lien',
            'cle_sql' => 'id_courrier_lien'
        ),
        'composition' => array(
            'prefixe' => 'com'
        ),
        'bloc' => array(
            'prefixe' => 'com'
        ),
        'restriction' => array(
            'objet1' => array('autorisation'),
        ),
        'servicepaiement' => array(
            'objet_1' => array('paiement', 'servicerendu'),
        ),
        'servicerendu' => array(
            'objet1' => array('servicepaiement'),
            'objet_1' => array('membre', 'prestation', 'entite'),
            'image' => 'asso_servicerendu.png',
        ),
        'tresor' => array(
            'objet_1' => array('entite', 'compte'),
            'objet_2' => array('compte' => array('poste', 'entite')),
        ),
        'tva' => array(),
        'tvataux' => array(),
        'unite' => array(),
        'abonnement' => array(
            'log' => 'SR2',
            'alias' => 'prestation',
            'alias_valeur' => '2'

        ),
        'adhesion' => array(
            'log' => 'SR6',
            'alias' => 'prestation',
            'alias_valeur' => '6'
        ),
        'cotisation' => array(
            'log' => 'SR1',
            'alias' => 'prestation',
            'alias_valeur' => '1'
        ),
        'don' => array(
            'log' => 'SR4',
            'alias' => 'prestation',
            'alias_valeur' => '4'
        ),
        'perte' => array(
            'log' => 'SR5',
            'alias' => 'prestation',
            'alias_valeur' => '5'
        ),
        'vente' => array(
            'log' => 'SR3',
            'alias' => 'prestation',
            'alias_valeur' => '3'
        ),
        'sr_abonnement' => array(
            'log' => 'SR2',
            'alias' => 'servicerendu',
            'alias_valeur' => '2',
            'objet1' => array('paiement'),
            'objet_1' => array('prestation', 'membre', 'entite'),
        ),
        'sr_adhesion' => array(
            'log' => 'SR6',
            'alias' => 'servicerendu',
            'alias_valeur' => '6',
            'objet1' => array('paiement'),
            'objet_1' => array('prestation', 'membre', 'entite'),
        ),
        'sr_cotisation' => array(
            'log' => 'SR1',
            'alias' => 'servicerendu',
            'alias_valeur' => '1',
            'objet1' => array('paiement'),
            'objet_1' => array('prestation', 'membre', 'entite'),
        ),
        'sr_don' => array(
            'log' => 'SR4',
            'alias' => 'servicerendu',
            'alias_valeur' => '4',
            'objet1' => array('paiement'),
            'objet_1' => array('prestation', 'membre', 'entite'),
        ),
        'sr_perte' => array(
            'log' => 'SR5',
            'alias' => 'servicerendu',
            'alias_valeur' => '5',
            'objet1' => array('paiement'),
            'objet_1' => array('prestation', 'membre', 'entite'),
        ),
        'sr_vente' => array(
            'log' => 'SR3',
            'droits' => array('G', 'C', 'A', 'W'),
            'alias' => 'servicerendu',
            'alias_valeur' => '3',
            'objet1' => array('paiement'),
            'objet_1' => array('prestation', 'membre', 'entite'),
        ),
        'codespostaux' => array(
            'prefixe' => 'geo',
            'cle' => 'codepostal',
            'cle_sql' => 'id_codepostal',
            'nom_sql' => 'codespostaux',
            'table_sql' => 'geo_codespostaux',

        ),
        'commune' => [
            'prefixe' => 'geo'
        ],
        'arrondissement' => [
            'prefixe' => 'geo'
        ],
        'departement' => [
            'prefixe' => 'geo'
        ],
        'region' => [
            'prefixe' => 'geo'
        ],
        'zone' => [
            'prefixe' => 'geo'
        ],
        'zonegroupe' => [
            'prefixe' => 'geo',
            'log' => 'ZOG'
        ],
        'infolettre' => array(
            'prefixe' => 'com'

        ),
        'tache' => array(
            'source' => 'systeme/'

        ),
        'notification' => array(
            'source' => 'systeme/'

        ),
    );
}


