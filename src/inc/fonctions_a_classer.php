<?php
function membre_individu_creation($data=array(),$creation=true){
    Global $app;
    if ($data['id_membre']==0) return;
    if ($data['id_individu']==0) return;
    $membresindividus = MembreIndividuQuery::create()->filterByIdMembre($data['id_membre'])->findOneByIdIndividu($data['id_individu']);
    if (!$membresindividus) {
        $membresindividus = new MembreIndividu();
        $membresindividus->setIdIndividu($data['id_individu']);
        $membresindividus->setIdMembre($data['id_membre']);
        $ok=$membresindividus->save();
        if (!$ok){
            $app['session']->getFlashBag()->add('error', 'La liaison memnbre individu a échoué');
        }
    }elseif($creation===false){
      $membresindividus->delete();
    }
}