<?php


function variables_preference()
{

    init_sac();

    $valeur_par_defaut = [
        'id_entite' => 1,
        'tresor' => ['id_tresor' => 1, 'id_entite' => 1]
    ];

    $tab_prestation_type = getPrestationTypeSimple();
    foreach ($tab_prestation_type as $type) {
        $valeur_par_defaut[$type] = ['id_prestation' => 0, 'id_entite' => 1];
    }


    $tab_entite = array_keys(tab('entite'));
    if (isset($tab_entite[0])) {
        $valeur_par_defaut['id_entite'] = $tab_entite[0];
    }


    foreach ($tab_prestation_type as $k => $type) {

        $temp = getPrestationDeType($k);
        if (!empty($temp)) {
            $prestation = reset($temp);
            $valeur_par_defaut[$type]['id_prestation'] = key($temp);
            $valeur_par_defaut[$type]['id_entite'] = $prestation['id_entite'];
        }
    }


    $temp = tab('tresor');
    if (!empty($temp)) {
        $tresor = reset($temp);
        $valeur_par_defaut['tresor']['id_tresor'] = key($temp);
        $valeur_par_defaut['tresor']['id_entite'] = $tresor['id_entite'];

    }


    $modules_affichage=['afficher' => ['cotisation' => true,'abonnement' => false,'vente' => false,'don' => true,
            'perte' => true,'adhesion' => false,'paiement' => true,
            'newsletter' => false]];


    $tab_objet_a = ['cotisation','abonnement','vente','don','perte','adhesion','paiement','newsletter','importation'];
    foreach($tab_objet_a as $objet){
        if(conf('module.'.$objet))
            $tab_objet[$objet]=$objet;
    }

if (!isset($tab_objet))
    $tab_objet=[];

    $champs_objet=[
        'type_champs'=>'case_a_cocher',
        'choices'=> $tab_objet,
        'valeur' => $tab_objet
    ];

    $champs_selection_membre=[
        'type_champs'=>'selection',
        'valeur'=>'tous',
        'objet'=>'membre'
    ];

    $champs_selection_individu=[
        'type_champs'=>'selection',
        'valeur'=>'tous',
        'objet'=>'individu'
    ];

    $champs_choice_tresor=[
        'type_champs'=>'radio',
        'choices'=> array_flip(table_simplifier(tab('tresor'))),
        'valeur'=> $valeur_par_defaut['tresor']['id_tresor']
    ];
    $champs_choice_entite=[
        'type_champs'=>'radio',
        'choices'=> array_flip(table_simplifier(tab('entite'))),
        'valeur'=> $valeur_par_defaut['id_entite']
    ];
    $champs_choice_entite_tous_un = [
        'type_champs' => 'radio',
        'choices' => ['toutes les entités' => '0', 'Entité choisie' => '1'],
        'valeur' => 1
    ];



    $etapes = [
        '0' => 'abandon',
        '1' => 'charge_entete',
        '2' => 'charge_lignes',
        '3' => 'interprete',
        '4' => 'selection occurence',
        '5' => 'detail_modification',
        '6' => 'detail_creation',
        '7' => 'valide',
        '8' => 'compte_rendu'
    ];
    $mode = [
        'a_definir'=>'importer a définir'
                ];





    return [


        'imprimante' => [
            'variables' => [
                'carte_adherent' => [
                    'nb_colonne' => 3,
                    'nb_ligne' => 8,
                    'largeur_page' => 210,
                    'hauteur_page' => 297,
                    'marge_haut_page' => 0,
                    'marge_bas_page' => 0,
                    'marge_droite_page' => 0,
                    'marge_gauche_page' => 0,
                    'marge_haut_etiquette' => 4,
                    'marge_gauche_etiquette' => 4,
                    'marge_droite_etiquette' => 2,
                    'espace_etiquettesh' => 0,
                    'espace_etiquettesl' => 4,
                    'position_depart' => 1,
                    'id_bloc' => 0

                ],
                'etiquette' => [
                    'nb_colonne' => 3,
                    'nb_ligne' => 8,
                    'largeur_page' => 210,
                    'hauteur_page' => 297,
                    'marge_haut_page' => 0,
                    'marge_bas_page' => 0,
                    'marge_droite_page' => 0,
                    'marge_gauche_page' => 0,
                    'marge_haut_etiquette' => 4,
                    'marge_gauche_etiquette' => 4,
                    'marge_droite_etiquette' => 2,
                    'espace_etiquettesh' => 0,
                    'espace_etiquettesl' => 4,
                    'membre' => [
                        'position_depart' => 1,
                        'avec_individu' => true,
                        'selection' => $champs_selection_membre,
                        'champs_titulaire' => true,
                        'champs_nom_membre' => true,
                        'champs_civilite' => false,
                        'champs_numero' => false,
                        'ajout_nom_membre' => false,
                        'classement' => [
                            'type_champs' => 'radio',
                            'choices' => ['Nom' => 0, 'Code postal+ville+nom' => 1, 'Ville+nom' => 2],
                            'valeur' => 1
                        ],

                    ],
                    'individu' => [
                        'position_depart' => 1,
                        'selection' => $champs_selection_individu,
                        'champs_civilite' => false,
                        'champs_numero' => false,
                        'classement' => [
                            'type_champs' => 'radio',
                            'choices' => ['Nom' => 0, 'Code postal+ville+nom' => 1, 'Ville+nom' => 2],
                            'valeur' => 1
                        ],
                   ],

                ],
                'text' => [
                    'largeur_page' => 210,
                    'hauteur_page' => 297,
                    'marge_haut_page' => 5,
                    'marge_bas_page' => 5,
                    'marge_droite_page' => 5,
                    'marge_gauche_page' => 5
                ]
            ]
        ],
        'accueil' => [
            'variables' => [
                'log' => [
                    'utilisateur' => 'moi', // moi, tous
                    'regroupement' => 'jour', // par mois, semaine, jour, heure
                    'nb_element' => 100, // Nombre d'élément à traiter
                ],
                'stat' => ['afficher' => true]
            ]
        ],
        'en_cours' => [
            'variables' => [
                'id_entite' => 1
            ]
        ],
        'selection' => [
            'variables' => [
                'membre' => [],
                'individu' => []
            ],
            'parametrage_entite' => true
        ],
        'autorisation' => [
            'variables' => [
                'affichage' => true,
                'nb_ligneS' => 5,
                'nb_ligneP' => 20,
                'affichage_histo' => true
            ]
        ],
        'entite' => [
            'variables' => [
                'nb_ligneP' => 15,
            ]
        ],

        'membre' => [
            'variables' => [
                'export' => [
                    'selection' => $champs_selection_membre,
                    'annee' => [
                        'type_champs' => 'radio',
                        'choices' => ['une_annee' => 1, 'Toutes les années' => 0],
                        'valeur' => 0
                    ],
                    'amj_debut' => new DateTime('2015-09-01'),//Date de debut si une
                    'motgroupe1' => 3,
                    'type' => $champs_objet,
                    'nb_ligne_entete' => 10,
                ],
                'mode_saisie' => [
                    'type_champs' => 'radio',
                    'choices' => ['complete' => 0, 'rapide' => 1],
                    'valeur' => 0
                ],
                'mode_individu' => [
                    'type_champs' => 'radio',
                    'choices' => ['nouvel' => 'nouvel', 'existant' => 'existant'],
                    'valeur' => 'nouvel'
                ],
                'selection_defaut' => [
                    'type_champs' => 'bouton_selection_courante',
                    'valeur' => ['sorti'=>'non']
                ],
                'nb_ligneS' => 5,
                'nb_ligneP' => 30,
                // todo en attente de fonctionnement ( TRI ) pour généraliser et supprimer le dur
                'classement' => [
                    'type_champs' => 'select',
                    'choices' => ['nom'=>'nom','id' => 'id', 'created_at' => 'created_at','ville' => 'ville'],
                    'valeur' => 'nom'
                ],
                 'ordre' => [
                    'type_champs' => 'radio',
                    'choices' => ['ascendant' => 'asc', 'descendant' => 'desc'],
                    'valeur' => 'ASC'
                ],
                'affichage_histo' => true,
                'beneficiaire_membre' => $modules_affichage,
                'beneficiaire_individu' => $modules_affichage,
                'enchainement' => [
                    'type_champs' => 'select',
                    'choices' => ['aucun'=>'','cotisation' => 'sr_cotisation', 'adhesion' => 'sr_adhesion','servicerendu' => 'servicerendu'],
                    'valeur' => 'sr_adhesion'
                ]
            ],
            'parametrage_par_entite' => true
        ],

        'individu' => [
            'variables' => [
                'export' => [
                   'selection' => $champs_selection_individu,
                ],
                'mode_saisie' => [
                    'type_champs' => 'radio',
                    'choices' => ['complete' => 0, 'rapide' => 1],
                    'valeur' => 0
                ],
                'pays' => 'FR',
                'nb_ligneS' => 5,
                'nb_ligneP' => 20,
                'affichage_histo' => true,
                'beneficiaire_membre' => $modules_affichage,
                'beneficiaire_individu' =>$modules_affichage,
                ],
            'parametrage_par_entite' => true
        ],


        'courrier' => [
            'variables' => [
                'pays' => 'FR',
                'mode' => 0,
                'nb_ligneS' => 5,
                'nb_ligneP' => 20,
                'nb_lignes_liens' => 20,
                'affichage_histo' => true,
                'beneficiaire_membre' => $modules_affichage,
                'beneficiaire_individu' =>$modules_affichage,
            ]
        ],
        'zone' => [
            'variables' => [
                'nb_ligneP' => 20,
            ]
        ],



//        'individu_export_petition' => [
//            'variables' => [
//                'selection_petition' => 2,
//
//            )
//        ],
        'servicerendu' => [
            'variables' => [
                'export' => [
                    'avec_paiement' => true,
                    'date_debut' => [
                        'type_champs' => 'date',
                        'valeur' => null
                    ],
                    'date_fin' => [
                        'type_champs' => 'date',
                        'valeur' => null
                    ],
                ],
                //todo n'est plus utilisé permettait de définir le comportement de la saisie suivante
                //effacer dans les sr_ et paiement idem_precdent2
//                'idem_precedent3' => [
//                    'type_champs' => 'select',
//                    'choices' => ['precedent enregistrement'=>'1','choix' => '2', 'dernier en date' => '3'],
//                    'valeur' => 'sr_adhesion'
//                ],
                'entite_tous_un' => $champs_choice_entite_tous_un,
                'entite' => $champs_choice_entite,
                'id_prestation' => [
                    'type_champs' => 'radio',
                    'choices' => array_flip(table_simplifier(getPrestationDeType('cotisation'))),
                    'valeur' =>$valeur_par_defaut['cotisation']['id_prestation']
                ],
                'affichage' => true, // pour donner la possibilite de ne pas geré au niveau de l'utilisateur ou général
                'nb_ligneS' => 5,
                'nb_ligneP' => 20,
                'affichage_histo' => true,
                'ardent_nb_annee' => 3
            ],
            'parametrage_par_entite' => true
        ],
        'sr_cotisation' => [
            'variables' => [
                'export' => [
                    'avec_paiement' => true,
                    'date_debut' => [
                        'type_champs' => 'date',
                        'valeur' => null
                    ],
                    'date_fin' => [
                        'type_champs' => 'date',
                        'valeur' => null
                    ],
                ],
                'entite_tous_un' => $champs_choice_entite_tous_un,
                'entite' => $champs_choice_entite,
                'id_prestation' => [
                    'type_champs' => 'radio',
                    'choices' => array_flip(table_simplifier(getPrestationDeType('cotisation'))),
                    'valeur' =>$valeur_par_defaut['cotisation']['id_prestation']
                ],
                'affichage' => true, // pour donner la possibilite de ne pas geré au niveau de l'utilisateur ou général
                'nb_ligneS' => 5,
                'nb_ligneP' => 20,
                'affichage_histo' => true,
                'ardent_nb_annee' => 3
            ],
            'parametrage_par_entite' => true
        ],
        'sr_adhesion' => [
            'variables' => [
                'export' => [
                    'avec_paiement' => true,
                    'date_debut' => [
                        'type_champs' => 'date',
                        'valeur' => null
                    ],
                    'date_fin' => [
                        'type_champs' => 'date',
                        'valeur' => null
                    ],
                ],
                'entite_tous_un' => $champs_choice_entite_tous_un,
                'entite' => $champs_choice_entite,
                'id_prestation' => [
                    'type_champs' => 'radio',
                    'choices' => array_flip(table_simplifier(getPrestationDeType('adhesion'))),
                    'valeur' =>$valeur_par_defaut['adhesion']['id_prestation']
                ],
//                'prestations' => $valeur_par_defaut['adhesion']['id_prestation'],
                'affichage' => true, // pour donner la possibilite de ne pas geré au niveau de l'utilisateur ou général
                'nb_ligneS' => 5,
                'nb_ligneP' => 20,
                'affichage_histo' => true
            ],
            'parametrage_par_entite' => true
        ],
        'sr_vente' => [
            'variables' => [
                'export' => [
                    'avec_paiement' => true,
                    'date_debut' => [
                        'type_champs' => 'date',
                        'valeur' => null
                    ],
                    'date_fin' => [
                        'type_champs' => 'date',
                        'valeur' => null
                    ],
                ],
                'entite_tous_un' => $champs_choice_entite_tous_un,
                'entite' => $champs_choice_entite,
                'id_prestation' => [
                    'type_champs' => 'radio',
                    'choices' => array_flip(table_simplifier(getPrestationDeType('vente'))),
                    'valeur' =>$valeur_par_defaut['vente']['id_prestation']
                ],
//                'prestations' => $valeur_par_defaut['vente']['id_prestation'],
                'affichage' => true, // pour donner la possibilite de ne pas geré au niveau de l'utilisateur ou général
                'nb_ligneS' => 5,
                'nb_ligneP' => 20,
                'affichage_histo' => true
            ],
            'parametrage_par_entite' => true
        ],
        'sr_don' => [
            'variables' => [
                'export' => [
                    'avec_paiement' => true,
                    'date_debut' => [
                        'type_champs' => 'date',
                        'valeur' => null
                    ],
                    'date_fin' => [
                        'type_champs' => 'date',
                        'valeur' => null
                    ],
                ],
                'entite_tous_un' => $champs_choice_entite_tous_un,
                'entite' => $champs_choice_entite,
                'id_prestation' => [
                    'type_champs' => 'radio',
                    'choices' => array_flip(table_simplifier(getPrestationDeType('don'))),
                    'valeur' =>$valeur_par_defaut['don']['id_prestation']
                ],
 //               'prestations' => $valeur_par_defaut['don']['id_prestation'],
                'affichage' => true, // pour donner la possibilite de ne pas geré au niveau de l'utilisateur ou général
                'nb_ligneS' => 5,
                'nb_ligneP' => 20,
                'affichage_histo' => true
            ]

        ],
        'sr_perte' => [
            'variables' => [
                'export' => [
                    'avec_paiement' => true,
                    'date_debut' => [
                        'type_champs' => 'date',
                        'valeur' => null
                    ],
                    'date_fin' => [
                        'type_champs' => 'date',
                        'valeur' => null
                    ],
                ],
                'entite_tous_un' => $champs_choice_entite_tous_un,
                'entite' => $champs_choice_entite,
                'id_prestation' => [
                    'type_champs' => 'radio',
                    'choices' => array_flip(table_simplifier(getPrestationDeType('perte'))),
                    'valeur' =>$valeur_par_defaut['perte']['id_prestation']
                ],
                //                'prestations' => $valeur_par_defaut['perte']['id_prestation'],
                'affichage' => true, // pour donner la possibilite de ne pas geré au niveau de l'utilisateur ou général
                'nb_ligneS' => 5,
                'nb_ligneP' => 20,
                'affichage_histo' => true
            ],
            'parametrage_par_entite' => true
        ],
        'sr_abonnement' => [
            'variables' => [
                'export' => [
                    'avec_paiement' => true,
                    'date_debut' => [
                        'type_champs' => 'date',
                        'valeur' => null
                    ],
                    'date_fin' => [
                        'type_champs' => 'date',
                        'valeur' => null
                    ],
                ],
                'entite_tous_un' => $champs_choice_entite_tous_un,
                'entite' => $champs_choice_entite,
                'id_prestation' => [
                    'type_champs' => 'radio',
                    'choices' => array_flip(table_simplifier(getPrestationDeType('abonnement'))),
                    'valeur' =>$valeur_par_defaut['abonnement']['id_prestation']
                ],
//                'prestations' => $valeur_par_defaut['abonnement']['id_prestation'],
                'affichage' => true, // pour donner la possibilite de ne pas affiché au niveau de l'utilisateur ou général
                'nb_ligneS' => 5,
                'nb_ligneP' => 20,
                'affichage_histo' => true
            ],
            'parametrage_par_entite' => true
        ],
        'paiement' => [
            'variables' => [
                'export' => [
                    'entite_tous_un' => $champs_choice_entite_tous_un,
                    'entite' => $champs_choice_entite,
                    'tresor_tous_un' =>  [
                        'type_champs' => 'radio',
                        'choices' => ['tous les moyens de paiements' => '0', 'MdP choisi' => '1'],
                        'valeur' => '1'
                    ],

                    'tresor' => $champs_choice_tresor,
                    'valider' => false,//valide et comptabilise la remise 0 brouillon
                    'comptabiliser' => false,//comptabilise la remise 0 numero de remise sans comptabilisation
                    'amj_fin' => new DateTime('2015-01-01'),//Date de fin d'export
                    'sortie' => [
                        'type_champs' => 'radio',
                        'choices' => ['pdf' => '_pdf', 'tableur' => '_tableur', 'pdf et tableur' => '_pdf_tableur'],
                        'valeur' => '_pdf'
                    ],
                    'nb_ligne' => 30,//lignes par page
                    'nb_exemplaire' => 1,//+1 si validattion
                ],
                'edition_comptabilise' => [
                    'entite' => $champs_choice_entite,
                    'tresor' => $champs_choice_tresor,
                    'numero_remise' =>0,
                    'comptabiliser' => false,//comptabilise la remise 0 numero de remise sans comptabilisation
                    'sortie' => [
                        'type_champs' => 'radio',
                        'choices' => ['pdf' => '_pdf', 'tableur' => '_tableur', 'pdf et tableur' => '_pdf_tableur'],
                        'valeur' => '_pdf'
                    ],
                    'nb_ligne' => 30,//lignes par page
                ],
                'entite_tous_un' => $champs_choice_entite_tous_un,
                'entite' => $champs_choice_entite,
                'tresor_tous_un' =>  [
                    'type_champs' => 'radio',
                    'choices' => ['tous les moyens de paiements' => '0', 'MdP choisi' => '1'],
                    'valeur' => '1'
                ],
                'tresor' => $champs_choice_tresor,
                'affichage' => true, // pour donner la possibilite de ne pas geré au niveau de l'utilisateur ou général
                'nb_ligneS' => 5,
                'nb_ligneP' => 20,
                'affichage_histo' => true
            ],
            'parametrage_par_entite' => true
        ],
        'date_debut' => [
            'variables' => [
                'servicerendu_dd' => '2015-09-01',//modif du 22/08/2016
                'paiement_dd' => '2015-09-01'
            ],
            'parametrage_entite' => true
        ],
        'solde' => [
            'variables' => [
                'amj_paiement' => new DateTime('2015-09-01'),
                'amj_servicerendu' => new DateTime('2015-09-01')
]        ],
        'statistique' => [
            'variables' => [
                'entite_tous_un' => $champs_choice_entite_tous_un,
                'entite' => $champs_choice_entite,
                'par_annee' => true,//0 une 1 toutes
                'amj_debut' => new DateTime('2015-09-01'),//Date de debut si une
                'prix_controle' => 1,// 0 non 1 oui
                'difference' => 1,// o non 1 oui Un tableau avec les differences entre prix théorique et enregistré
                'objet' =>  $champs_objet,
            ]

        ],
        'import' => [
            'variables' => [
                "etapes"=>$etapes,
                "mode" => $mode,
            ],
            'parametrage_par_entite' => true
        ],
        'timeline' => [
            'variables' => [
                'decoupage' =>  [
                    'type_champs' => 'radio',
                    'choices' => ['aucun' => 'aucun', 'par jour' => 'jour', 'par heure' => 'heure'],
                    'valeur' => 'jour'
                ],
                'regroupement' =>  true,
                'filtre_operateur' =>  [
                    'type_champs' => 'radio',
                    'choices' => ['toutes les operations' => 'tout', 'mes opérations' => 'moi'],
                    'valeur' => 'tout'
                ],
                'filtre_objet' =>  [
                    'type_champs' => 'case_a_cocher',
                    'choices' => ['individu' => 'individu','membre' => 'membre', 'les autres' => 'autre'],
                    'valeur' => ['individu','membre','autre']
                ],
//                'filtre_entite' =>  [
//                    'type_champs' => 'radio',
//
//                    'valeur' => 'tout'
//                ]

            ]

        ]


    ];
}
function membre_import(){
    return [
            'variables' => [
                'nom_colonne' => [
                    'nom_famille' => 'nom_famille',
                    'prenom' => 'prenom',
                    'email' => 'email',
                    'naissance' => 'naissance',
                    'adresse' => 'adresse',
                    'codepostal' => 'codepostal',
                    'ville' => 'ville',
                    'pays' => 'pays',
                    'telephone' => 'telephone',
                    'fax' => 'fax',
                    'mobile' => 'mobile',
                    'profession' => 'profession',
                    'civilite' => 'civilite',
                    'sexe' => 'sexe',
                    'id_membre' => 'id',
                    'id_individu' => 'id_individu',
                    'identifiant_interne' => 'identifiant_interne',
                    'nom' => 'Nom',
                    'nom_court' => 'nom_court',
                    'observation_m' => 'observation_m',
                    'observation_i' => 'observation_i',
                    'observation_i2' => 'observation_i2',
                    'cotid_mot' => 'cotid_mot',
                    'cotid_prestation' => 'cotid_prestation',
                    // id , nomcourt, C+prix C14 en dur voir une cellule dans l'entete
                    'cotid_tresor' => 'cotid_tresor',
                    'cotdate_debut' => 'cotdate_debut',
                    'cotdate_cheque' => 'cotdate_cheque',
                    'cotmontant' => 'cotmontant',
                    'cotobservation' => 'cotobservation',
                    'cotp_observation' => 'cotp_observation',
                    'adhid_mot' => 'adhid_mot',
                    'adhid_prestation' => 'adhid_prestation',
                    'adhid_tresor' => 'adhid_tresor',
                    'adhdate_debut' => 'adhdate_debut',
                    'adhdate_cheque' => 'adhdate_cheque',
                    'adhmontant' => 'adhmontant',
                    'adhobservation' => 'adhobservation',
                    'adhp_observation' => 'adhp_observation',
                    'id_mot1' => 'id_mot1',
                    'nom_mot1' => 'nom_mot1',
                    'id_mot2' => 'id_mot2',
                    'nom_mot2' => 'nom_mot2',
                    'id_mot3' => 'id_mot3',
                    'nom_mot3' => 'nom_mot3',
                ],
                'nb_lentete' => 12,
                'option1' => '1',
                'cotid_motgroupe' => 2,//pour permettre les mots du groupe
                'cotp_defaut' => 1,//cotisations defaut
                'cott_defaut' => 1,//tresor defaut
                'option2' => '0',
                'adhid_motgroupe' => 3,//mot du groupe
                'adhp_defaut' => 1,//adhesion defaut
                'adht_defaut' => 1,//tresor defaut
                'option3' => '0',
                'export_resultat' => '0',
                'mot_motgroupe1' => 4,//mot du groupe
                'mot_motgroupe2' => 3,//mot du groupe
                'mot_motgroupe3' => 2,//mot du groupe

            ],
            'parametrage_par_entite' => true
        ];

}


