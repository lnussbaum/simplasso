<?php

use Symfony\Component\Validator\Constraints as Assert;

//****** pour eventuelle réutilisation ************************
//    Creation lien groupe_mot fichier 2 colonnes id_membre et id du mot du groupe_mot 180,2  puis 117,2.....
//    $l=explode(',', $ligne);
//    $sql="INSERT INTO asso_mots_liens (id_mot, id_objet, objet) VALUES (".$l[1].", ".$l[0].", 'membre')";
//    arbre($sql.';');
//    $m=$app['db']->executeQuery($sql);
// *****************************************
include_once('../vendor/phpoffice/phpexcel/Classes/PHPExcel.php');
include_once('../vendor/phpoffice/phpexcel/Classes/PHPExcel/IOFactory.php');
include_once('../vendor/phpoffice/phpexcel/Classes/PHPExcel/Writer/Excel2007.php');

//etapes import
//  debut : le fichier a été sélectionné et l'enregistrement asso_import est créé
//  1charge : le fichier est lu, contolé et enregistrée sous forme de tableau dans la chaine données
// encours : les données ont été enregistrée dans des tableaux permettant de séparées les étapes d'integrations modif de champs, création, par type de données
//           codification 'a' ajout 'm' modification suivi du nom de la procedure
//                        '_m_i' membre et individu  'option_1' cotisations  nom de 'option_2 adhesion
//           les lignes sélectionnées sont enregistrées,  marquées et masquées les autres  sont représentées jusqu'a validation partielle ou complete
// 6valide : toutes les ajouts et modifications  souhaités sont enregistrées celles non sélectionnées ne seront plus intégrables le compte rendu est généré
// 0abandon : aucune donnée n'a été enregistrée


// todo  asso sous spip    $ordre_prenom_nom=lire_config('simplasso/prenom_nom');



function ecriture_creation($tab_ecriture)
{
    $ecriture = new Ecritures();
    $ecriture->fromArray($tab_ecriture);
    $ecriture->save();
    return $ecriture->getIdEcriture();
}

function ecriture_data_defaut(&$data)
{
    $data = array_merge(lire_config('import.defaut'), $data);
    $data['id_poste_bilan'] = poste_creation(array());
    $data['id_poste_gestion'] = poste_creation(array('nomcourt'=>'GE','nom'=> 'Gestion'));
    $data['id_activite'] = activite_creation(array('activite'=>$data['activite'],'id_entite'=> $data['id_entite']));
    $data['id_compte'] = JournalsQuery::create()
        ->findpk($data['id_journal'])->getIdCompte();
    $data['entite'] = EntiteQuery::create()
        ->findpk($data['id_entite'])->getNomcourt();
    //$data['id_compte_cp'] sera utilisé si le journal n'existe pas dans la société défaut
    $objet_data1 = JournalsQuery::create()
        ->filterByIdEntite($data['id_entite'])
        ->filterByIdJournal($data['id_journal'])->findone();
    if ($objet_data1) {
        $data['id_compte_cp'] = $objet_data1->getIdCompte();
        $data['journal'] = $objet_data1->getNomcourt();
    } else {
        $data['id_compte_cp'] = JournalsQuery::create()
            ->findpk($data['compte_cp'])->getIdJournal();
    }
    return;
}

function ecriture_totaux($credit, $debit, &$tab_ecriture, &$centimes)
{
    $tab_ecriture['credit'] = str_replace(",", ".", $credit);
    $tab_ecriture['debit'] = str_replace(",", ".", $debit);
    if ($tab_ecriture['type'] == 'releve') {
        $tab_ecriture['relevedebit'] = $tab_ecriture['relevedebit'] - $tab_ecriture['credit'];
        $tab_ecriture['relevecredit'] = $tab_ecriture['relevecredit'] - $tab_ecriture['debit'];
    }
    $centimes = $centimes + ($tab_ecriture['credit'] * 100) + ($tab_ecriture['debit'] * 100);
    // echo '<br> ligne 265 :' . $debit . '*********' . $credit . '*********' . $centimes;
}



function export_extension(&$typefichier,&$extension)
{
 if ($typefichier){
     switch ($typefichier) {
        case'OOCalc':
        case'OpenDocument':
            $extension = '.ods';
            break;
        case'Excel2007':
            $extension = '.xlsx';
            break;
        case'Excel5':
            $extension = '.xlsm';
            $extension = '.xls';
            break;
        case'SYLK':
            $extension = '.slk';
            break;
        case'CVS':
            $extension = '.cvs';
            break;
        case'HTML':
            $extension = '.html';
            break;
        case'PDF':
            $extension = '.pdf';
            break;
        case'Gnumeric':
            $extension = '.gnumeric';
            break;
        default:
            $extension = null;
    }
}else {
     switch ($extension) {
         case'ods':
             $typefichier = 'OOCalc';//'OpenDocument'
             break;
         case'xlsx':
             $typefichier = 'Excel2007';
             break;
         case'xlx':
             $typefichier = 'Excel5';
             break;
         case'slk':
             $typefichier = 'SILK';
             break;
         case'cvs':
             $typefichier = 'CVS';
             break;
         case'htm':
             $typefichier = 'HTML';
             break;
         case'pdf':
             $typefichier = 'PDF';
             break;
         case'gnumeric':
             $typefichier = 'Gnumeric';
             break;
         default:
             $typefichier = null;
     }
 }
    return ;
}





function membre_creation_import(&$data,$log=true)
{
    $objet='membre';
    $objet_data = null;
    if (isset($data['id_individu']) and intval(($data['id_individu']) > 0)) {
        $objet_data = MembreQuery::create()->findpk($data['id_individu']);
        if ($objet_data) return;
    }
//    autoincrement($data['id_membre'],"individu",'asso','membre');;
    $objet_data = new Membre();

    $id_objet1=$data['id_membre'];
    unset($data['id_membre']);

    $data['id_individu_titulaire'] = $data['id_individu'];
    $data['observation']=$data['observation_m'];
    if (isset($data['observation_m2'])and trim($data['observation_m2'])>'!') {
        $data['observation'] = ajoute_rc($data['observation'],"Aides :" . $data['observation_m2']);
    }
     if (isset($data['organismefonction'])and trim($data['organismefonction'])>'!') {
        $data['observation'] = ajoute_rc($data['observation'],"Organisme Fonction :" . $data['organismefonction']);
    }


    $objet_data->fromArray($data);
    $objet_data->save();
    $id_objet=$objet_data->getIdMembre();
    if (isset($data['compte_rendu_mem']))
        $data['compte_rendu_mem'] .= ' n° :' .  $id_objet;
    else
        $data['compte_rendu_mem'] = ' n° :' .  $id_objet;
    if($log)
        Log::EnrOp(false, $objet, $id_objet);

    if ($data['id_individu']) {
        $relation = MembreIndividuQuery::create()->filterByIdMembre($id_objet)->findOneByIdIndividu($data['id_individu']);
        if (!$relation) {
            $relation = new MembreIndividus();
            $relation->setIdIndividu($data['id_individu']);
            $relation->setIdMembre($id_objet);
            $relation->save();
        }
    }
    if (isset($data['quimaj'])and trim($data['quimaj'])>'!') {
        $temp=array('descriptif'=>'reprise_adav',
            'fonction'=>'mis_a_jour_par',
            'args'=>array(
                'id_individu' => $data['id_individu'],
                'id_membre' => $id_objet,
                'operateur' =>trim($data['quimaj'])),
            'date_maj'=>$data['datemaj'],
            'statut'=>'a_faire',
            'priorite'=>'manuelle');
        tache_creation($temp);
    }
    if (isset($data['heurovelo'])and trim($data['heurovelo'])>'!') {
        $temp=array('descriptif'=>'reprise_adav',
            'fonction'=>'dernier_heuro_velo_servi',
            'args'=>array(
                'id_individu' => $data['id_individu'],
                'id_membre' => $id_objet,
                'numero'=> trim($data['heurovelo'])),
            'statut'=>'ok',
            'priorite'=>'manuelle');
        tache_creation($temp);
    }

    return $id_objet;
}




//todo voir mot_lien de modele/mot  mot::MotLien
function mot_liens($id_mot, $id_objet,$objet,$observation=null)
{
    $cr='';
    if ($id_mot>0) {
        $objet_data=MotLienQuery::create()->filterByIdMot($id_mot)->filterByIdObjet($id_objet)->findOneByObjet($objet);
        if (!$objet_data) {
            $objet_data = new MotLien();
            $objet_data->setIdObjet($id_objet);
            $objet_data->setObjet($objet);
            $objet_data->setIdMot($id_mot);
        }
        if($observation!=null)
            $objet_data->setObservation($observation);
        $objet_data->save();
        $cr = '+mot ' . $id_mot;
    }
    return $cr;
}


function mots_creation_reprise($champ,&$entete,$groupe,$methode,$objet,&$data)
{
    if (isset($data[$champ])) {
        $mot = strtolower(trim($data[$champ]));
        if ($mot > '!') {
             switch ($methode){
                 case 'decompose_9':
                     $tab=explode(chr(9),$mot);
                     break;
                 case 'lg_5_6':
                     if(strlen($mot)==5)
                        $tab=array($mot);
                     elseif (strlen($mot)==6)
                         $tab=array($mot);
                     elseif (strlen($mot)<5)
                         $tab=array($mot);
                     elseif (strlen($mot)==10)
                         $tab=array(substr($mot,0,5),substr($mot,5));
                     elseif (strlen($mot)==12)
                         $tab=array(substr($mot,0,6),substr($mot,6));
                     else
                       $tab=array($mot);
//  $tab=array(substr($mot,0,6),substr($mot,6),);
                     break;
                 case 'decompose_9_10':
                     $tab=explode(chr(9),$mot);
                     if(!$tab)$tab=explode(chr(10),$mot);
                     break;
                 case 'decompose_virgule':
                     $tab=explode(',',$mot);
                     break;
                 case 'cree_et_enleve' :
                     // recherche dans le champs les mots du groupe les enleve du champs et créera le mot
                     // le solde du champ sera mis dans observation (origine champs commentairesaides de l'adav
                     foreach($entete['mot'.$groupe]['tab_mot'] as $nom_mot=>$id){
                         $long_mot=strlen($nom_mot);
                         $pos=strpos($mot,$nom_mot);
                         if (!$pos===false) {
                             $tab[$nom_mot] = $id;
                             if ($pos == 0) {
                                 $mot = substr($mot, $long_mot);
                             } else {
                                 $mot = substr($mot,0, ($pos)) . substr($mot, ($pos  + $long_mot));
                             }
                         }
                     }
                 default :
                     $tab=array($mot);
            }
            foreach($tab as $nom_mot){
                $nom_mot=trim($nom_mot);
                if (isset($entete['mot'.$groupe]['tab_mot'][$nom_mot])) {
                    $id = $entete['mot' . $groupe]['tab_mot'][$nom_mot];
//                    echo 'creation mot ligne 1610 :'.$id.' '.$data['id_'.$objet];

                }else {
                    $id = mot_creation($nom_mot, $groupe);
                    $entete['mot'.$groupe]=mot_charge_motgroupes($groupe);
                }
                mot_liens($id,$data['id_'.$objet],$objet);
            }
            $data['compte_rendu_'.$objet].=' +mot :'.$mot;
        }
    }
    return;
}


function piece_nouvelle()
{
    $res = PieceQuery::create()->orderByidPiece('desc')->findone();
    return intval($res->getIdPiece()) + 1;

}
function piece_enregistre(&$message, &$erreurs, &$tab_piece, &$tab_ecriture, &$nbpiece, &$centimes, $enregistre)
{

    if ($tab_ecriture['relevedebit'] != 0 or $tab_ecriture['relevecredit'] != 0) {
        $tab_ecriture['libelle'] = "Releve " . $tab_piece['piece'];
        $tab_piece['libelle'] = "Releve " . $tab_piece['piece'];
        $tab_piece['classement'] = "Releve";
        $tab_ecriture['id_compte'] = $tab_ecriture['id_compte_cp'];
        $tab_ecriture['id_activite'] = $tab_ecriture['id_activite_cp'];
//        $tab_ecriture[''];

        $centimes = $centimes + ($tab_ecriture['relevecredit'] * 100) + ($tab_ecriture['relevedebit'] * 100);

        if ($tab_ecriture['relevedebit'] != 0) {
            $tab_ecriture['credit'] = 0;
            $tab_ecriture['debit'] = $tab_ecriture['relevedebit'];
            if ($enregistre) {
                $tab_piece['id_ecriture'] = ecriture_creation($tab_ecriture);
                $tab_piece['nb_ligne']++;
            }
            $tab_ecriture['relevedebit'] = 0;
        }
        if ($tab_ecriture['relevecredit'] != 0) {
            $tab_ecriture['debit'] = 0;
            $tab_ecriture['credit'] = $tab_ecriture['relevecredit'];
            if ($enregistre) {
                $tab_piece['id_ecriture'] = enrecriture_creation($tab_ecriture);
                $tab_piece['nb_ligne']++;
            }
            $tab_ecriture['relevecredit'] = 0;
        }
//        $tab_piece['id_ecriture']=
        echo '<br> ligne 301 :' . '*********' . $centimes;
        echo "Ligne 305 en_cours";
        exit;
        echo '<br> ligne 301 :' . '*********' . $centimes;
    }
    if (abs($centimes * 10) >= 1) {
        $message .= 'Ligne ' . $tab_piece['nb_ligne'] . ' non equilibre : . ' . $centimes . ' centimes  ' . PHP_EOL;
        if ($enregistre) {
            $tab_ecriture['libelle'] = "Equilibre en reprise ";
            $tab_ecriture['classement'] = "Equilibre";
            if ($centimes <= 0) {
                $tab_ecriture['credit'] = $centimes * (-0.01);
                $tab_ecriture['debit'] = 0;
            } else {
                $tab_ecriture['debit'] = $centimes * (-0.01);
                $tab_ecriture['credit'] = 0;
            }
            ecriture_enregistre($tab_ecriture);
            $tab_piece['nb_ligne']++;
        }
        $erreurs++;
    }
//    echo '<br>ligne 359 ' . $centimes.'---' ;
    if ($enregistre) {
        $piece = new Pieces();
        unset($tab_piece['id_piece']);
        $piece->fromArray($tab_piece);
        $piece->save();
//        echo 'ligne 383';
    }
    $nbpiece++;
    $centimes = 0;
    $tab_piece['piece='] = -1;
}
function piece_interprete_ligne($num_ligne, $data, $tab_correspondance)
{
    $tab = array(
        'libcompte' => '',
        'compte_cp' => '',
        'piece' => '',
        'compte' => '',
        'activite' => '',
        'compte_cp' => '',
        'activite_cp' => '',
        'observation' => '',
        'entite' => '',
        'journal' => ''
    );
    switch ($data['type']) {
        case 'ciel':
            $tab['entite'] = trim(substr($num_ligne, 3, 3));
            if (!$tab['entite']) {
                $tab['entite'] = $data['entite'];
            }

            $tab['piece'] = trim(substr($num_ligne, 5, 5));
            $tab['journal'] = trim(substr($num_ligne, 10, 2));
            $tab['date'] = substr($num_ligne, 22, 2) . '-' . substr($num_ligne, 20, 2) . '-' . substr($num_ligne, 16, 4);
            $tab['echeance'] = substr($num_ligne, 24, 8);
            $tab['classement'] = substr($num_ligne, 32, 15);
            $tab['compte'] = substr($num_ligne, 47, 13);
            $tab['libelle'] = trim(substr($num_ligne, 60, 50));
            $sens = substr($num_ligne, 123, 1);
            if ($sens == 'C') {
                $tab['credit'] = trim(substr($num_ligne, 110, 13));
                $tab['debit'] = 0;
            } else {
                $tab['credit'] = 0;
                $tab['debit'] = trim(substr($num_ligne, 110, 13)) * -1;
            }
            $tab['observation'] = substr($num_ligne, 124, 15) . ' ' . substr($num_ligne, 198, 4);
            $tab['libcompte'] = trim(substr($num_ligne, 152, 35));
            $tab['activite'] = trim(substr($num_ligne, 139, 13));
            break;
        default:
            if (strlen($num_ligne) == 0) {
                break;
            }
            $tab_n = explode("\t", $num_ligne);
            if ($num_ligne[0] == '"') {
                array_walk($tab_n, 'simplasso_trim', "\"");
            }
            foreach ($tab_correspondance as $key => $value) {
                $tab[$value] = trim(trim($tab_n[$key], '"'));// Nommer les colonnes
            }
            if (!$tab['piece']) {
                $tab['piece'] = substr($tab['date'], 3, 7);
            }
            if (!$tab['entite']) {
                $tab['entite'] = $data['entite'];
            }
            if (!$tab['journal']) {
                $tab['journal'] = $data['journal'];
            }
    }
    return $tab;
}

function activite($activite, &$tab_ecriture, $data)
{
    $id_activite = null;
    if (!$activite) {
        $id_activite = $data['id_activite'];
    } else {
//        echo '<br> activite  :' . $activite;
        $objet_data = ActiviteQuery::create()
            ->filterByIdEntite($tab_ecriture['id_entite'])
            ->filterByNomcourt($activite)->findone();
        if ($objet_data) {
            $id_activite = $objet_data->getIdActivite();
        } else {
            $id_activite = null;
        }
    }
//    echo '<br> ligne 375 -' . $activite . '-' . $id_activite;
    if (!$id_activite and $data['creer']) {
        $id_activite = activite_creation($activite, $tab_ecriture['id_entite']);
    }
    if ($id_activite) {
        $tab_ecriture['id_activite'] = $id_activite;
        return $id_activite;
    }
    $tab_ecriture['message'] .= " Pas d'activite ";
    return false;
}

function compte($tab, &$tab_ecriture, $creer)
{
    $id_compte = null;
    if (!isset($tab['compte'])) {
        $id_compte = $tab_ecriture['id_compte_cp'];
        $compte = $tab_ecriture['compte_cp'];
    } else {
        $objet_data = CompteQuery::create()
            ->filterByIdEntite($tab_ecriture['id_entite'])
            ->filterByNcompte($tab['compte'])->findone();
        if ($objet_data) {
            $id_compte = $objet_data->getIdCompte();
        }
    }
    if (!$id_compte and $creer) {
        $id_compte = compte_creation(array('id_entite'=>$tab_ecriture['id_entite'],'nom'=>$tab['libcompte']),$compte);
    }
    if ($id_compte) {
        if (!isset($tab_ecriture['compte_cp'])) {
            $tab_ecriture['compte_cp'] = CompteQuery::create()
                ->findPk($id_compte)->getNcompte();
            $tab_ecriture['id_compte_cp'] = $id_compte;
        }
        return $id_compte;
    }
    $tab_ecriture['message'] .= " Pas de compte ";
    return false;
}
function servicerendu_creation_import(&$data,$type,$log=true)
{
    $objet='servicerendu';
    $objet_data = new Servicerendu();
    $objet_data->fromArray($data);
    $objet_data->save();
    $id_objet=$objet_data->getIdServicerendu();
    if (isset($data['compte_rendu_' . $type]))
        $data['compte_rendu_' . $type] .= ' n° :' .  $id_objet;
    else
        $data['compte_rendu_' . $type] = ' n° :' .  $id_objet;
if ($log)
    Log::EnrOp(false, $objet, $id_objet, 'SR' . $data['prestation_type']);
    return $id_objet;
}
function sr_boucle($an_debut,$an_fin,$tab,$prefixe,$options,&$nb,&$nbp)
{
    $retour=array();
    for ($annee = $an_debut; $annee <= $an_fin; $annee++) {
        $data = array();
        $data['montant'] = $tab[$prefixe . $annee];
        if ($data['montant'] > 0) {
            $data['id_membre'] = $tab['id_membre'];
            if ($annee < 2003) {
                $data['montant'] = round($data['montant'] / 6.55957, 2);
           }
            $data['date_debut'] = (isset($options['date_debut'.$annee])) ? $options['date_debut'.$annee] : $annee . '-12-31';
            $data['date_fin'] = (isset($options['date_fin'.$annee])) ? $options['date_fin'.$annee] : $annee . '-12-31';
            $data['id_entite'] = 1;
            $data['id_prestation'] = (isset($options['id_prestation'.$annee])) ? $options['id_prestation'.$annee] : $options['id_prestation'] ;
            $data['id_tva'] = 0;
            $data['quantite'] = (isset($options['quantite'.$annee])) ? $options['quantite'.$annee] : 1;
            $data['prestation_type'] = $options['prestation_type'];
            $data['id_objet'] = $tab['id_membre'];
            $data['objet'] = 'membre';
            $data['created_at'] = (isset($options['created_at'.$annee])) ? $options['created_at'.$annee] : $data['date_debut'] . ' 23:59:59';
            $data['observation'] = (isset($options['observation'.$annee])) ? $options['observation'.$annee] : "";
            $data['id_servicerendu'] = servicerendu_creation_import($data, $prefixe,false);
            $retour['id_servicerendu'.$annee] = $data['id_servicerendu'];
            $nb++;
            if($options['paiement'] and  $data['montant']!=0) {
                $data['id_tresor'] = 1;
                $data['date_enregistrement'] = $data['created_at'];
                $data['date_cheque'] = $data['created_at'];
                $data['id_paiement'] = paiement_creation_import($data, $prefixe . 'p',false);
                $nbp++;
            }
        }
        }
    return $retour;
}
function sr_unique($tab,$options,&$nb,&$nbp)
{
    $data = array();
    $data['id_membre'] = $tab['id_membre'];
    $data['montant'] = (isset($options['montant'])) ? $options['montant'] : 0;
    $data['date_debut'] = (isset($options['date_debut'])) ? $options['date_debut'] : "2017-01-11";
    $data['date_fin'] = (isset($options['date_fin'])) ? $options['date_fin'] : $data['date_debut'];
    $data['id_entite'] = 1;
    $data['id_prestation'] =  $options['id_prestation'] ;
    $data['id_tva'] = 0;
    $data['quantite'] = (isset($options['quantite'])) ? $options['quantite'] : 1;
    $data['prestation_type'] = $options['prestation_type'];
    $data['id_objet'] = $tab['id_membre'];
    $data['objet'] = 'membre';
    $data['created_at'] = (isset($options['created_at'])) ? $options['created_at'] : $data['date_debut'] . ' 00:00:00';
    $data['observation'] = (isset($options['observation'])) ? $options['observation'] : "";
    $data['id_servicerendu'] = servicerendu_creation_import($data, $options['prefixe'],false);
    $nb++;
    if(isset($option['paiement'])and  $data['montant']!=0) {
        $data['id_tresor'] = 1;
        $data['date_enregistrement'] = $data['created_at'];
        $data['date_cheque'] = $data['created_at'];
        $data['id_paiement'] = paiement_creation_import($data, $options['prefixe'] . 'p',false);
        $nbp++;
    }
}


//function journal($tab, &$tab_ecriture, $creer)
//{
//    if (!$tab['journal']) {
//        $tab_ecriture['id_journal'] = $tab_ecriture['id_journal'];
//        $id_compte_cp = $tab_ecriture['id_compte_cp'];
//    } else {
////        echo '<br> id_entite :' . $tab_ecriture['id_entite'] . '--journal :' . $journal;
//        $objet_data = JournalsQuery::create()
//            ->filterByIdEntite($tab_ecriture['id_entite'])
//            ->filterByNomcourt($tab['journal'])->findone();
//        if ($objet_data) {
//            $tab_ecriture['id_journal'] = $objet_data->getIdJournal();
//            $tab_ecriture['id_compte_cp'] = $objet_data->getIdCompte();
//        } else {
//            $id_journal = null;
//        }
//    }
//    if (!$tab_ecriture['id_journal'] and $creer) {
//        if (!$id_compte_cp) {
//            $data1 = lire_config('creation.compte_cp');
//            $tab_ecriture['compte_cp'] = $data1['compte'];
//            if ($data1['compte'] > '6') {
//                $id_poste = $tab_ecriture['id_poste_gestion'];
//            } else {
//                $id_poste = $tab_ecriture['id_poste_bilan'];
//            }
//            $tab_ecriture['id_compte_cp'] = compte_creation($data1['compte'],
//                $id_poste,
//                $tab_ecriture['id_entite'],
//                $tab['libcompte']);
//        }
//        $data1 = lire_config('creation.journal');
//        $tab_ecriture['id_journal'] = journal_creation($data1['nomcourt'], $tab_ecriture['id_compte_cp'],
//            $tab_ecriture['id_entite']);
//    }
//
////    echo $journal,$id_journal,$tab_ecriture['id_entite'];
//    if ($tab_ecriture['id_journal']) {
//        return $tab_ecriture['id_journal'];
//    }
//    $tab_ecriture['message'] .= " Pas de création de journal :" . $tab['journal'];
//    return false;
//}
//
//function entite($tab, &$tab_ecriture, $creer = true)
//{
////    echo 'ligne 544 '.$entite;
////    arbre($tab_ecriture);
//    if (!$tab['entite']) {
//        $id_entite = $tab_ecriture['entite'];
//    } else {
////        echo '<br> id_entite :' . $entite;
//        $objet_data = EntiteQuery::create()
//            ->filterByNomcourt($tab['entite'])
//            ->findone();
//        if ($objet_data) {
//            $id_entite = $objet_data->getIdEntite();
//        } else {
//            $id_entite = null;
//        }
//    }
//    if (!$id_entite and $creer) {
//        $id_entite = entite_creation($tab['entite']);
//        $tab_ecriture['journal'] = '';
//        $tab_ecriture['id_journal'] = 0;
//        $tab_ecriture['compte_cp'] = '';
//        $tab_ecriture['id_compte_cp'] = 0;
//
//    }
//    if ($id_entite) {
//        $tab_ecriture['id_entite'] = $id_entite;
//        $id_journal = journal($tab, $tab_ecriture, $creer);
//        $id_compte_cp = compte($tab, $tab_ecriture, $creer);
//        $tab_ecriture['id_activite_cp'] = activite_creation($tab['activite'], $id_entite);
//        return array($id_entite, $id_journal, $id_compte_cp);
//    }
//    $tab_ecriture['message'] .= " Pas d'entité";
//    return false;
//}


/**
 * @param $fichier
 * @return null|PHPExcel
 * @throws PHPExcel_Reader_Exception
 */
function time_message(&$callStartTime, $option = array())
{
    $callEndTime = microtime(true);
    $callTime = $callEndTime - $callStartTime;
    echo EOL, date('H:i:s'), " ecrit ";
    if ($option) {
        arbre($option);
    }
    echo ' en ', sprintf('%.4f', $callTime), " secondes";
    Echo ' memoire utilisee : ', (memory_get_usage(true) / 1024), " caracteres ", EOL;
    $callStartTime = microtime(true);
}


function contenu(&$objPHPExcel)
{
// Create a first sheet, representing sales data
    echo date('H:i:s'), " Add some data", EOL;
    $objPHPExcel->setActiveSheetIndex(0);
    $objPHPExcel->getActiveSheet()->setCellValue('B1', 'Invoice');
    $objPHPExcel->getActiveSheet()->setCellValue('D1',
        PHPExcel_Shared_Date::PHPToExcel(gmmktime(0, 0, 0, date('m'), date('d'), date('Y'))));
    $objPHPExcel->getActiveSheet()->getStyle('D1')->getNumberFormat()->setFormatCode(PHPExcel_Style_NumberFormat::FORMAT_DATE_XLSX15);
    $objPHPExcel->getActiveSheet()->setCellValue('E1', '#12566');

    $objPHPExcel->getActiveSheet()->setCellValue('A3', 'Product Id');
    $objPHPExcel->getActiveSheet()->setCellValue('B3', 'Description');
    $objPHPExcel->getActiveSheet()->setCellValue('C3', 'Price');
    $objPHPExcel->getActiveSheet()->setCellValue('D3', 'Amount');
    $objPHPExcel->getActiveSheet()->setCellValue('E3', 'Total');

    $objPHPExcel->getActiveSheet()->setCellValue('A4', '1001');
    $objPHPExcel->getActiveSheet()->setCellValue('B4', 'PHP for dummies');
    $objPHPExcel->getActiveSheet()->setCellValue('C4', '20');
    $objPHPExcel->getActiveSheet()->setCellValue('D4', '1');
    $objPHPExcel->getActiveSheet()->setCellValue('E4', '=IF(D4<>"",C4*D4,"")');

    $objPHPExcel->getActiveSheet()->setCellValue('A5', '1012');
    $objPHPExcel->getActiveSheet()->setCellValue('B5', 'OpenXML for dummies');
    $objPHPExcel->getActiveSheet()->setCellValue('C5', '22');
    $objPHPExcel->getActiveSheet()->setCellValue('D5', '2');
    $objPHPExcel->getActiveSheet()->setCellValue('E5', '=IF(D5<>"",C5*D5,"")');

    $objPHPExcel->getActiveSheet()->setCellValue('E6', '=IF(D6<>"",C6*D6,"")');
    $objPHPExcel->getActiveSheet()->setCellValue('E7', '=IF(D7<>"",C7*D7,"")');
    $objPHPExcel->getActiveSheet()->setCellValue('E8', '=IF(D8<>"",C8*D8,"")');
    $objPHPExcel->getActiveSheet()->setCellValue('E9', '=IF(D9<>"",C9*D9,"")');

    $objPHPExcel->getActiveSheet()->setCellValue('D11', 'Total excl.:');
    $objPHPExcel->getActiveSheet()->setCellValue('E11', '=SUM(E4:E9)');

    $objPHPExcel->getActiveSheet()->setCellValue('D12', 'VAT:');
    $objPHPExcel->getActiveSheet()->setCellValue('E12', '=E11*0.21');

    $objPHPExcel->getActiveSheet()->setCellValue('D13', 'Total incl.:');
    $objPHPExcel->getActiveSheet()->setCellValue('E13', '=E11+E12');

// Add comment
    echo date('H:i:s'), " Add comments", EOL;

    $objPHPExcel->getActiveSheet()->getComment('E11')->setAuthor('PHPExcel');
    $objCommentRichText = $objPHPExcel->getActiveSheet()->getComment('E11')->getText()->createTextRun('PHPExcel:');
    $objCommentRichText->getFont()->setBold(true);
    $objPHPExcel->getActiveSheet()->getComment('E11')->getText()->createTextRun("\r\n");
    $objPHPExcel->getActiveSheet()->getComment('E11')->getText()->createTextRun('Total amount on the current invoice, excluding VAT.');

    $objPHPExcel->getActiveSheet()->getComment('E12')->setAuthor('PHPExcel');
    $objCommentRichText = $objPHPExcel->getActiveSheet()->getComment('E12')->getText()->createTextRun('PHPExcel:');
    $objCommentRichText->getFont()->setBold(true);
    $objPHPExcel->getActiveSheet()->getComment('E12')->getText()->createTextRun("\r\n");
    $objPHPExcel->getActiveSheet()->getComment('E12')->getText()->createTextRun('Total amount of VAT on the current invoice.');

    $objPHPExcel->getActiveSheet()->getComment('E13')->setAuthor('PHPExcel');
    $objCommentRichText = $objPHPExcel->getActiveSheet()->getComment('E13')->getText()->createTextRun('PHPExcel:');
    $objCommentRichText->getFont()->setBold(true);
    $objPHPExcel->getActiveSheet()->getComment('E13')->getText()->createTextRun("\r\n");
    $objPHPExcel->getActiveSheet()->getComment('E13')->getText()->createTextRun('Total amount on the current invoice, including VAT.');
    $objPHPExcel->getActiveSheet()->getComment('E13')->setWidth('100pt');
    $objPHPExcel->getActiveSheet()->getComment('E13')->setHeight('100pt');
    $objPHPExcel->getActiveSheet()->getComment('E13')->setMarginLeft('150pt');
    $objPHPExcel->getActiveSheet()->getComment('E13')->getFillColor()->setRGB('EEEEEE');


// Add rich-text string
    echo date('H:i:s'), " Add rich-text string", EOL;
    $objRichText = new PHPExcel_RichText();
    $objRichText->createText('This invoice is ');

    $objPayable = $objRichText->createTextRun('payable within thirty days after the end of the month');
    $objPayable->getFont()->setBold(true);
    $objPayable->getFont()->setItalic(true);
    $objPayable->getFont()->setColor(new PHPExcel_Style_Color(PHPExcel_Style_Color::COLOR_DARKGREEN));

    $objRichText->createText(', unless specified otherwise on the invoice.');

    $objPHPExcel->getActiveSheet()->getCell('A18')->setValue($objRichText);

// Merge cells
    echo date('H:i:s'), " Merge cells", EOL;
    $objPHPExcel->getActiveSheet()->mergeCells('A18:E22');
    $objPHPExcel->getActiveSheet()->mergeCells('A28:B28');        // Just to test...
    $objPHPExcel->getActiveSheet()->unmergeCells('A28:B28');    // Just to test...

// Protect cells
    echo date('H:i:s'), " Protect cells", EOL;
    $objPHPExcel->getActiveSheet()->getProtection()->setSheet(true);    // Needs to be set to true in order to enable any worksheet protection!
    $objPHPExcel->getActiveSheet()->protectCells('A3:E13', 'PHPExcel');

// Set cell number formats
    echo date('H:i:s'), " Set cell number formats", EOL;
    $objPHPExcel->getActiveSheet()->getStyle('E4:E13')->getNumberFormat()->setFormatCode(PHPExcel_Style_NumberFormat::FORMAT_CURRENCY_EUR_SIMPLE);

// Set column widths
    echo date('H:i:s'), " Set column widths", EOL;
    $objPHPExcel->getActiveSheet()->getColumnDimension('B')->setAutoSize(true);
    $objPHPExcel->getActiveSheet()->getColumnDimension('D')->setWidth(12);
    $objPHPExcel->getActiveSheet()->getColumnDimension('E')->setWidth(12);

// Set fonts
    echo date('H:i:s'), " Set fonts", EOL;
    $objPHPExcel->getActiveSheet()->getStyle('B1')->getFont()->setName('Candara');
    $objPHPExcel->getActiveSheet()->getStyle('B1')->getFont()->setSize(20);
    $objPHPExcel->getActiveSheet()->getStyle('B1')->getFont()->setBold(true);
    $objPHPExcel->getActiveSheet()->getStyle('B1')->getFont()->setUnderline(PHPExcel_Style_Font::UNDERLINE_SINGLE);
    $objPHPExcel->getActiveSheet()->getStyle('B1')->getFont()->getColor()->setARGB(PHPExcel_Style_Color::COLOR_WHITE);

    $objPHPExcel->getActiveSheet()->getStyle('D1')->getFont()->getColor()->setARGB(PHPExcel_Style_Color::COLOR_WHITE);
    $objPHPExcel->getActiveSheet()->getStyle('E1')->getFont()->getColor()->setARGB(PHPExcel_Style_Color::COLOR_WHITE);

    $objPHPExcel->getActiveSheet()->getStyle('D13')->getFont()->setBold(true);
    $objPHPExcel->getActiveSheet()->getStyle('E13')->getFont()->setBold(true);

// Set alignments
    echo date('H:i:s'), " Set alignments", EOL;
    $objPHPExcel->getActiveSheet()->getStyle('D11')->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_RIGHT);
    $objPHPExcel->getActiveSheet()->getStyle('D12')->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_RIGHT);
    $objPHPExcel->getActiveSheet()->getStyle('D13')->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_RIGHT);

    $objPHPExcel->getActiveSheet()->getStyle('A18')->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_JUSTIFY);
    $objPHPExcel->getActiveSheet()->getStyle('A18')->getAlignment()->setVertical(PHPExcel_Style_Alignment::VERTICAL_CENTER);

    $objPHPExcel->getActiveSheet()->getStyle('B5')->getAlignment()->setShrinkToFit(true);

// Set thin black border outline around column
    echo date('H:i:s'), " Set thin black border outline around column", EOL;
    $styleThinBlackBorderOutline = array(
        'borders' => array(
            'outline' => array(
                'style' => PHPExcel_Style_Border::BORDER_THIN,
                'color' => array('argb' => 'FF000000'),
            ),
        ),
    );
    $objPHPExcel->getActiveSheet()->getStyle('A4:E10')->applyFromArray($styleThinBlackBorderOutline);


// Set thick brown border outline around "Total"
    echo date('H:i:s'), " Set thick brown border outline around Total", EOL;
    $styleThickBrownBorderOutline = array(
        'borders' => array(
            'outline' => array(
                'style' => PHPExcel_Style_Border::BORDER_THICK,
                'color' => array('argb' => 'FF993300'),
            ),
        ),
    );
    $objPHPExcel->getActiveSheet()->getStyle('D13:E13')->applyFromArray($styleThickBrownBorderOutline);

// Set fills
    echo date('H:i:s'), " Set fills", EOL;
    $objPHPExcel->getActiveSheet()->getStyle('A1:E1')->getFill()->setFillType(PHPExcel_Style_Fill::FILL_SOLID);
    $objPHPExcel->getActiveSheet()->getStyle('A1:E1')->getFill()->getStartColor()->setARGB('FF808080');

// Set style for header row using alternative method
    echo date('H:i:s'), " Set style for header row using alternative method", EOL;
    $objPHPExcel->getActiveSheet()->getStyle('A3:E3')->applyFromArray(
        array(
            'font' => array(
                'bold' => true
            ),
            'alignment' => array(
                'horizontal' => PHPExcel_Style_Alignment::HORIZONTAL_RIGHT,
            ),
            'borders' => array(
                'top' => array(
                    'style' => PHPExcel_Style_Border::BORDER_THIN
                )
            ),
            'fill' => array(
                'type' => PHPExcel_Style_Fill::FILL_GRADIENT_LINEAR,
                'rotation' => 90,
                'startcolor' => array(
                    'argb' => 'FFA0A0A0'
                ),
                'endcolor' => array(
                    'argb' => 'FFFFFFFF'
                )
            )
        )
    );

    $objPHPExcel->getActiveSheet()->getStyle('A3')->applyFromArray(
        array(
            'alignment' => array(
                'horizontal' => PHPExcel_Style_Alignment::HORIZONTAL_LEFT,
            ),
            'borders' => array(
                'left' => array(
                    'style' => PHPExcel_Style_Border::BORDER_THIN
                )
            )
        )
    );

    $objPHPExcel->getActiveSheet()->getStyle('B3')->applyFromArray(
        array(
            'alignment' => array(
                'horizontal' => PHPExcel_Style_Alignment::HORIZONTAL_LEFT,
            )
        )
    );

    $objPHPExcel->getActiveSheet()->getStyle('E3')->applyFromArray(
        array(
            'borders' => array(
                'right' => array(
                    'style' => PHPExcel_Style_Border::BORDER_THIN
                )
            )
        )
    );

// Unprotect a cell
    echo date('H:i:s'), " Unprotect a cell", EOL;
    $objPHPExcel->getActiveSheet()->getStyle('B1')->getProtection()->setLocked(PHPExcel_Style_Protection::PROTECTION_UNPROTECTED);

// Add a hyperlink to the sheet
    echo date('H:i:s'), " Add a hyperlink to an external website", EOL;
    $objPHPExcel->getActiveSheet()->setCellValue('E26', 'www.phpexcel.net');
    $objPHPExcel->getActiveSheet()->getCell('E26')->getHyperlink()->setUrl('http://www.phpexcel.net');
    $objPHPExcel->getActiveSheet()->getCell('E26')->getHyperlink()->setTooltip('Navigate to website');
    $objPHPExcel->getActiveSheet()->getStyle('E26')->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_RIGHT);

    echo date('H:i:s'), " Add a hyperlink to another cell on a different worksheet within the workbook", EOL;
    $objPHPExcel->getActiveSheet()->setCellValue('E27', 'Terms and conditions');
    $objPHPExcel->getActiveSheet()->getCell('E27')->getHyperlink()->setUrl("sheet://'Terms and conditions'!A1");
    $objPHPExcel->getActiveSheet()->getCell('E27')->getHyperlink()->setTooltip('Review terms and conditions');
    $objPHPExcel->getActiveSheet()->getStyle('E27')->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_RIGHT);

//// Add a drawing to the worksheet
    echo date('H:i:s'), " Add a drawing to the worksheet", EOL;
    $objDrawing = new PHPExcel_Worksheet_Drawing();
    $objDrawing->setName('Logo');
    $objDrawing->setDescription('Logo');
    $objDrawing->setPath('./images/officelogo.jpg');
    $objDrawing->setHeight(36);
    $objDrawing->setWorksheet($objPHPExcel->getActiveSheet());

// Add a drawing to the worksheet
    echo date('H:i:s'), " Add a drawing to the worksheet", EOL;
    $objDrawing = new PHPExcel_Worksheet_Drawing();
    $objDrawing->setName('Paid');
    $objDrawing->setDescription('Paid');
    $objDrawing->setPath('./images/paid.png');
    $objDrawing->setCoordinates('B15');
    $objDrawing->setOffsetX(110);
    $objDrawing->setRotation(25);
    $objDrawing->getShadow()->setVisible(true);
    $objDrawing->getShadow()->setDirection(45);
    $objDrawing->setWorksheet($objPHPExcel->getActiveSheet());

// Add a drawing to the worksheet
    echo date('H:i:s'), " Add a drawing to the worksheet", EOL;
    $objDrawing = new PHPExcel_Worksheet_Drawing();
    $objDrawing->setName('PHPExcel logo');
    $objDrawing->setDescription('PHPExcel logo');
    $objDrawing->setPath('./images/phpexcel_logo.gif');
    $objDrawing->setHeight(36);
    $objDrawing->setCoordinates('D24');
    $objDrawing->setOffsetX(10);
    $objDrawing->setWorksheet($objPHPExcel->getActiveSheet());

// Play around with inserting and removing rows and columns
    echo date('H:i:s'), " Play around with inserting and removing rows and columns", EOL;
    $objPHPExcel->getActiveSheet()->insertNewRowBefore(6, 10);
    $objPHPExcel->getActiveSheet()->removeRow(6, 10);
    $objPHPExcel->getActiveSheet()->insertNewColumnBefore('E', 5);
    $objPHPExcel->getActiveSheet()->removeColumn('E', 5);

// Set header and footer. When no different headers for odd/even are used, odd header is assumed.
    echo date('H:i:s'), " Set header/footer", EOL;
    $objPHPExcel->getActiveSheet()->getHeaderFooter()->setOddHeader('&L&BInvoice&RPrinted on &D');
    $objPHPExcel->getActiveSheet()->getHeaderFooter()->setOddFooter('&L&B' . $objPHPExcel->getProperties()->getTitle() . '&RPage &P of &N');

// Set page orientation and size
    echo date('H:i:s'), " Set page orientation and size", EOL;
    $objPHPExcel->getActiveSheet()->getPageSetup()->setOrientation(PHPExcel_Worksheet_PageSetup::ORIENTATION_PORTRAIT);
    $objPHPExcel->getActiveSheet()->getPageSetup()->setPaperSize(PHPExcel_Worksheet_PageSetup::PAPERSIZE_A4);

// Rename first worksheet
    echo date('H:i:s'), " Rename first worksheet", EOL;
    $objPHPExcel->getActiveSheet()->setTitle('Invoice');


// Create a new worksheet, ter the default sheet
    echo date('H:i:s'), " Create a second Worksheet object", EOL;
    $objPHPExcel->createSheet();

// Llorem ipsum...
    $sLloremIpsum = 'Lorem ipsum dolor sit amet, consectetuer adipiscing elit. Vivamus eget ante. Sed cursus nunc semper tortor. Aliquam luctus purus non elit. Fusce vel elit commodo sapien dignissim dignissim. Pellentesque habitant morbi tristique senectus et netus et malesuada fames ac turpis egestas. Curabitur accumsan magna sed massa. Nullam bibendum quam ac ipsum. Lorem ipsum dolor sit amet, consectetuer adipiscing elit. Proin augue. Praesent malesuada justo sed orci. Pellentesque lacus ligula, sodales quis, ultricies a, ultricies vitae, elit. Sed luctus consectetuer dolor. Vivamus vel sem ut nisi sodales accumsan. Nunc et felis. Suspendisse semper viverra odio. Morbi at odio. Integer a orci a purus venenatis molestie. Nam mattis. Praesent rhoncus, nisi vel mattis auctor, neque nisi faucibus sem, non dapibus elit pede ac nisl. Cras turpis.';

// Add some data to the second sheet, resembling some different data types
    echo date('H:i:s'), " Add some data", EOL;
    $objPHPExcel->setActiveSheetIndex(1);
    $objPHPExcel->getActiveSheet()->setCellValue('A1', 'Terms and conditions');
    $objPHPExcel->getActiveSheet()->setCellValue('A3', $sLloremIpsum);
    $objPHPExcel->getActiveSheet()->setCellValue('A4', $sLloremIpsum);
    $objPHPExcel->getActiveSheet()->setCellValue('A5', $sLloremIpsum);
    $objPHPExcel->getActiveSheet()->setCellValue('A6', $sLloremIpsum);

// Set the worksheet tab color
    echo date('H:i:s'), " Set the worksheet tab color", EOL;
    $objPHPExcel->getActiveSheet()->getTabColor()->setARGB('FF0094FF');;

// Set alignments
    echo date('H:i:s'), " Set alignments", EOL;
    $objPHPExcel->getActiveSheet()->getStyle('A3:A6')->getAlignment()->setWrapText(true);

// Set column widths
    echo date('H:i:s'), " Set column widths", EOL;
    $objPHPExcel->getActiveSheet()->getColumnDimension('A')->setWidth(80);

// Set fonts
    echo date('H:i:s'), " Set fonts", EOL;
    $objPHPExcel->getActiveSheet()->getStyle('A1')->getFont()->setName('Candara');
    $objPHPExcel->getActiveSheet()->getStyle('A1')->getFont()->setSize(20);
    $objPHPExcel->getActiveSheet()->getStyle('A1')->getFont()->setBold(true);
    $objPHPExcel->getActiveSheet()->getStyle('A1')->getFont()->setUnderline(PHPExcel_Style_Font::UNDERLINE_SINGLE);

    $objPHPExcel->getActiveSheet()->getStyle('A3:A6')->getFont()->setSize(8);

// Add a drawing to the worksheet
    echo date('H:i:s'), " Add a drawing to the worksheet", EOL;
    $objDrawing = new PHPExcel_Worksheet_Drawing();
    $objDrawing->setName('Terms and conditions');
    $objDrawing->setDescription('Terms and conditions');
    $objDrawing->setPath('./images/termsconditions.jpg');
    $objDrawing->setCoordinates('B14');
    $objDrawing->setWorksheet($objPHPExcel->getActiveSheet());

// Set page orientation and size
    echo date('H:i:s'), " Set page orientation and size", EOL;
    $objPHPExcel->getActiveSheet()->getPageSetup()->setOrientation(PHPExcel_Worksheet_PageSetup::ORIENTATION_LANDSCAPE);
    $objPHPExcel->getActiveSheet()->getPageSetup()->setPaperSize(PHPExcel_Worksheet_PageSetup::PAPERSIZE_A4);

// Rename second worksheet
    echo date('H:i:s'), " Rename second worksheet", EOL;
    $objPHPExcel->getActiveSheet()->setTitle('Terms and conditions');


// Set active sheet index to the first sheet, so Excel opens this as the first sheet
    $objPHPExcel->setActiveSheetIndex(0);

    echo 'fin de 05 inc----------------------------------------';

}


// complement pour excel overture a la fin lecture des lignes

//    /** Error reporting */
//    error_reporting(E_ALL);
//    ini_set('display_errors', TRUE);
//    ini_set('display_startup_errors', TRUE);
//    date_default_timezone_set('Europe/London');
//    define('EOL',(PHP_SAPI == 'cli') ? PHP_EOL : '<br />');
//    date_default_timezone_set('Europe/London');
//    $callStartTime = microtime(true);
//    $objPHPExcel = new PHPExcel();
//// Set document properties
//    echo date('H:i:s') , " Set document properties" , EOL;
//    $objPHPExcel->getProperties()->setCreator("Maarten Balliauw")
//        ->setLastModifiedBy("Maarten Balliauw")
//        ->setTitle("Office 2007 XLSX Test Document")
//        ->setSubject("Office 2007 XLSX Test Document")
//        ->setDescription("Test document for Office 2007 XLSX, generated using PHP classes.")
//        ->setKeywords("office 2007 openxml php")
//        ->setCategory("Test result file");
//    $callStartTime = microtime(true);
//// ligne 44 de 05-----
//    contenu($objPHPExcel);
//    timemessage($callStartTime,array('ligne'=>'1293 fin chargement'));
//
//    $outputFileType = 'CVS';
//    $outputFileName = $app['output.path'] . '/' . 'test_'.date('H:i:s').export_extention($outputFileType);
//    $inputFileType = 'CVS';
//    $inputFileName = $outputFileName;
//    $objWriter = PHPExcel_IOFactory::createWriter($objPHPExcel, 'CSV')->setDelimiter(',')
//        ->setEnclosure('"')
//        ->setLineEnding("\r\n")
//        ->setSheetIndex(0)
//        ->save($outputFileName);
//    timemessage($callStartTime,array('Type :'=>$outputFileType,'fichier :'=>$outputFileName,'ligne'=>'1300'));
//
//    $outputFileType = 'Excel5';
//    $outputFileName = $app['output.path'] . '/' . 'test_'.date('H:i:s').export_extention($outputFileType);
//    $objWriter = PHPExcel_IOFactory::createWriter($objPHPExcel, $outputFileType);
//    $objWriter->save($outputFileName);
//    timemessage($callStartTime,array('Type :'=>$outputFileType,'fichier :'=>$outputFileName,'ligne'=>'1310'));
//
//    $csv = PHPExcel_IOFactory::createReader('CSV')->setDelimiter(',')
//        ->setEnclosure('"')
////        ->setLineEnding("\r\n")
//        ->setSheetIndex(0);
//    $csv->load($inputFileName);
//    timemessage($callStartTime,array('Type :'=>'CSV','fichier :'=>$inputFileName,'ligne'=>'1317 Lecture'));
//
//    $outputFileType = 'Excel2007';
//    $outputFileName = $app['output.path'] . '/' . 'test_'.date('H:i:s').export_extention($outputFileType);
//    $vv = PHPExcel_IOFactory::createWriter($objPHPExcel, $outputFileType);
//    $vv->save($outputFileName);
//    timemessage($callStartTime,array('Type :'=>$outputFileType,'fichier :'=>$outputFileName,'ligne'=>'1323'));
//
//    $outputFileType = 'HTML';
//    $outputFileName = $app['output.path'] . '/' . 'test_'.date('H:i:s').export_extention($outputFileType);
//    $objWriter = PHPExcel_IOFactory::createWriter($objPHPExcel, $outputFileType);
//    $objWriter->save($outputFileName);
//    timemessage($callStartTime,array('Type :'=>$outputFileType,'fichier :'=>$outputFileName,'ligne'=>'1329'));

//    $outputFileType = 'PDF';
//    $outputFileName = $app['output.path'] . '/' . 'test_'.date('H:i:s').export_extention($outputFileType);
//    $objWriter = PHPExcel_IOFactory::createWriter($objPHPExcel, $outputFileType);
//    $objWriter->save($outputFileName);
//    timemessage($callStartTime,array('Type :'=>$outputFileType,'fichier :'=>$outputFileName,'ligne'=>'1318'));

//  restreint la fonction INLINE voir les lignes causes des erreurs le TYPE_INLINE // Add rich-text string  PHPExcel_RichText) {return PHPExcel_Cell_DataType::TYPE_INLINE
//    $outputFileType = 'OpenDocument';
//    $outputFileName = $app['output.path'] . '/' . 'test_'.date('H:i:s').export_extention($outputFileType);
//    $objWriter = PHPExcel_IOFactory::createWriter($objPHPExcel, $outputFileType);
//    $objWriter->save($outputFileName);
//    timemessage($callStartTime,array('Type :'=>$outputFileType,'fichier :'=>$outputFileName,'ligne'=>'1318'));

//                        //affiche la feuille
//                        include_once('../vendor/phpoffice/phpexcel/Classes/PHPExcel/IOFactory.php');
//                        // require_once '.Classes/PHPExcel/IOFactory.php';
//                        $inputFileName = $app['upload.path'] . '/' . $fichier;
//                        $objPHPExcel = PHPExcel_IOFactory::load($inputFileName);
//                        /**
//                         * récupération de la première feuille du fichier Excel
//                         * @var PHPExcel_Worksheet $sheet
//                         */
//                        $sheet = $objPHPExcel->getSheet(0);
//                        echo '<table border="1">';
//                        // On boucle sur les lignes
//                        foreach($sheet->getRowIterator() as $row) {
//                            echo '<tr>';
//                            // On boucle sur les cellule de la ligne
//                            foreach ($row->getCellIterator() as $cell) {
//                                echo '<td>';
//                                print_r($cell->getValue());
//                                echo '</td>';
//                                echo '<td>';
//                                print_r($cell->getFormattedValue());
//                                echo '</td>';
//                            }
//
//                            echo '</tr>';
//                        }
//                        echo '</table>';


