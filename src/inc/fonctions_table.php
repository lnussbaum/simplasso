<?php

function table_trier_par($array, $on, $order = SORT_ASC)
{
    $new_array = array();
    $sortable_array = array();
    if (count($array) > 0) {
        foreach ($array as $k => $v) {
            if (is_array($v)) {
                foreach ($v as $k2 => $v2) {
                    if ($k2 == $on) {
                        $sortable_array[$k] = $v2;
                    }
                }
            } else {
                $sortable_array[$k] = $v;
            }
        }

        switch ($order) {
            case SORT_ASC:
                asort($sortable_array);
                break;
            case SORT_DESC:
                arsort($sortable_array);
                break;
        }

        foreach ($sortable_array as $k => $v) {
            $new_array[$k] = $array[$k];
        }
    }

    return $new_array;
}


function table_filtrer_valeur($table, $cle, $value)
{

    if (!is_array($value)) {
        $value = array($value);
    }
    $tmp = array();
    $sub = (strpos('.', $cle) > -1);
    foreach ($table as $k => $v) {
        if ($sub) {
            $val = tableauChemin($v, $cle);
            if ($val === null) {
                continue;
            }
        } else {
            $val = $v[$cle];
        }
        if (in_array($val, $value)) {
            $tmp[$k] = $v;
        }
    }
    return $tmp;
}


function table_filtrer_valeur_existe($table, $cle, $value)
{

    if (!is_array($value)) {
        $value = array($value);
    }
    $tmp = array();
    $sub = (strpos('.', $cle) > -1);
    foreach ($table as $k => $v) {
        if ($sub) {
            $val = tableauChemin($v, $cle);
            if ($val === null) {
                continue;
            }
        } else {
            $val = $v[$cle];
        }

        if (count(array_intersect($val, $value)) > 0) {
            $tmp[$k] = $v;
        }
    }
    return $tmp;
}

function table_filtrer_valeur_premiere($table, $cle, $value)
{
    $res = table_filtrer_valeur($table, $cle, $value);
    return array_shift($res);
}

function table_valeur_date($tab, $champs_valeur = 'montant', $timestamp = '', $champs_date = 'date_fin')
{
    if (!is_int($timestamp)) {
        $timestamp = time();
    }
    $result = $tab[0][$champs_valeur];
    foreach ($tab as $t) {
        if ($t[$champs_date] > $timestamp) {
            $result = $t[$champs_valeur];
        }
    }
    return $result;

}

function table_simplifier($tab, $nom = 'nom')
{

    $temp = array();
    foreach ($tab as $k => $v) {
        if (isset($v[$nom])){
            $temp[$k] = $v[$nom];
        }
    }
    return $temp;
}


function table_merge($arr1, $arr2)
{
    if (is_array($arr1) && is_array($arr2)) {
        if (!empty($arr1)) {
            foreach ($arr1 as $key => $value) {
                if (isset($arr2[$key])) {
                    if (is_array($arr2[$key]) && is_array($value)) {
                        $arr1[$key] = table_merge($arr1[$key], $arr2[$key]);
                    } elseif (!is_array($arr2[$key]) && !is_array($value)) {
                        $arr1[$key] = $arr2[$key];
                    }
                }
            }
        } else {
            $arr1 = $arr2;
        }
    } elseif (!is_array($arr1) && !is_array($arr2)) {
        $arr1 = $arr2;
    } elseif (!is_array($arr1) && is_array($arr2)) {
        $arr1 = $arr2;
    }
    return $arr1;
}


function table_inserer($tab, $position, $tab_plus)
{
    return array_slice($tab, 0, $position, true) + $tab_plus + array_slice($tab, $position, null, true);
}


function table_extraire_colonne($tab, $indice)
{
    $tab2 = $tab;
    foreach ($tab2 as $k => &$t) {
        $t = $t[$indice];
    }
    return $tab2;
}

function table_colonne_cle_valeur($tab, $indice_k, $indice)
{
    $tab2 = [];
    foreach ($tab as &$t) {
        $tab2[$t[$indice_k]] = $t[$indice];
    }
    return $tab2;
}
function table_colonne_cle($tab, $indice_k)
{
    $tab2 = [];
    foreach ($tab as &$t) {
        $t2=$t;
        unset($t2[$indice_k]);
        $tab2[$t[$indice_k]] = $t2;
    }
    return $tab2;
}