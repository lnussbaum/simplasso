<?php

use PHPImageWorkshop\ImageWorkshop;

function traitement_form_ged($nom_champs,$id_objet,$objet_nom='individu',$usage="",$multiple=false) {
    global $app;

    $tab_fichier=$app['session']->get('file_upload_tmp_' . $nom_champs);
    if(isset($tab_fichier[0])){
        $ged=false;
        if(!$multiple)
            $ged = GedQuery::getOneByObjet($objet_nom,$id_objet);
        $modif_ged=true;
        if(!$ged){
            $modif_ged=false;
            $ged=new Ged();
        }else{
            $ged->nettoyage_cache();
        }

        $ged->setFile($app['upload.path'].'/'.$tab_fichier[0]);
        $ged->save();
        $ged_liens = new GedLien();

        if(!$modif_ged) {
            $ged_liens->fromArray([
                'objet' => $objet_nom,
                'id_objet' => $id_objet,
                'id_ged' => $ged->getPrimaryKey(),
                'usage' => $usage
            ]);
            $ged_liens->save();
    //            arbre($ged_liens);
        }
    //        arbre($ged);
    //        exit;

    }
}


function image_preparation($objet,$id_objet,$usage,$taille){
    global $app;
    $ged = GedQuery::getOneByObjet($objet,$id_objet,$usage);
    $image_dir = $app['image.path'].'/';
    if($taille)
        $image_dir = $app['image.path'].'/';
    if($ged){
        $nom_fichier = $ged->saveFile($image_dir,"200x200");
        $layer = ImageWorkshop::initFromPath($image_dir.$nom_fichier);
        $layer->resizeInPixel($taille, null, true);
        $image = $layer->getResult("ffffff");
        imagejpeg($image, $image_dir.$nom_fichier, 95);
        return $image_dir.$nom_fichier;
    }
    return false;
}



function ged_televerser($champs_fichier,$tab_liens=array()){

    global $app;

    $fileName = md5(uniqid()).'.'.$champs_fichier->guessExtension();
    $champs_fichier->move( $app['upload.path'],  $fileName  );
    return ged_enregister($app['upload.path'].'/'.$fileName,$tab_liens);

}


function ged_enregister($path_fichier,$tab_liens=array()){



    $ged=new Ged();
    $ged->setFile( $path_fichier );
    $ged->save();

    foreach($tab_liens as $objet=>$liens){
        if(!empty($liens)) {

            $liens = is_array($liens)?$liens:[$liens];
            foreach ($liens as $id_objet) {
                $ged_liens = new GedLien();
                $ged_liens->fromArray(array(
                    'objet' => $objet,
                    'id_objet' => $id_objet,
                    'id_ged' => $ged->getIdGed()
                ));
                $ged_liens->save();


            }
        }
    }
    return true;

}
