<?php

function menu(){
global $app;

$menu = array(


    'membres'=>
        array(
            'label'=> $app->trans('membres'),
            'permission'=>'G',
            'menu'=>array(
                'membre_liste'=>'Liste des '.$app->trans('membres'),
                'individu_liste'=>'Liste des '.$app->trans('individus'),
                '#sep10'=>'#sep#',
                'courrier_liste'=>$app->trans('courrier_liste')
            )
        ),
    'services'=>
        array(
            'label'=> $app->trans('services'),
            'permission'=>'G',
            'menu'=>array(
                'sr_adhesion_liste'=>$app->trans('adhesions'),
                'sr_cotisation_liste'=>$app->trans('cotisations'),
                'sr_abonnement_liste'=>$app->trans('abonnements'),
                'sr_vente_liste'=>$app->trans('ventes'),
                'sr_don_liste'=>$app->trans('dons'),
                'sr_perte_liste'=>$app->trans('pertes'),
                //todo le 23/09/2017 mis en commentaire si ajoute  id du membre ou de l'individu n'estpas repris sauf si on laisse id et non le nom du ..
                //todo de ce fait le paiement n'est pas enregistré fonctionne di nouvel onglet et on laisse le numero dans le champ
//              'servicerendu_liste'=>$app->trans('servicerendus'),
                '#sep1'=>'#sep#',
                'paiement_liste'=>$app->trans('paiements'),
                '#sep2'=>'#sep#',
                'statistique'=>$app->trans('statistiques'),
                'inspecteur'=>$app->trans('inspecteur')

            )
        ),
    'pre_compta'=>
        array(
            'label'=> $app->trans('pre_compta'),
            'permission'=>'C',
            'menu'=>array(
                'ecriture_liste'=>'Écritures',
                'piece_liste'=>'Pièces',
                'compte_liste'=>'Comptes',
                'journal_liste'=>'Journaux',
                'activite_liste'=>'Activités',
                'poste_liste'=>'Postes',

            )
        ),


     'administration'=>
        array(
            'label'=> $app->trans('administration'),
            'permission'=>array('A3','C3','G3'),
            'menu'=>array(
                'entite_liste'=>'Les entités',
                'prestation_liste'=>'Les prestations',
                'prestationslot_liste'=>'Les lots de prestations',
                'tresor_liste'=>'Les moyens de paiements',
                'motgroupes'=>$app->trans('Mots clefs'),
                'tva_liste'=>'Taux de tva',
                'unite_liste'=>'Unites des quantités',
                'import_liste'=>'Liste des '.$app->trans('imports'),
                'autorisation_liste'=>$app->trans('autorisations'),
                'restriction_liste'=>$app->trans('restrictions'),
                'infolettre_liste'=>$app->trans('infolettres'),
                'composition_liste'=>$app->trans('compositions'),
                'bloc_liste'=>$app->trans('bloc'),
                'zonegroupe_liste'=>$app->trans('zones'),

                'configs'=>$app->trans('configs'),
                'outils'=>$app->trans('outils'),
                'memoire'=>'Mémoire'
            )
        ),

    'info'=>
        array(
            'label'=> $app->trans('aide'),
            'menu'=>array(
                $app['documentation.url'].$app['documentation.accueil']=>'Documentation',
                'credits'=>'Crédits',

               // 'changer'=>'changeridmembre_individu'
            )
        )


);

//supprimer les entrées des modules non-activés

if (!conf('module.cotisation')) unset($menu['services']['menu']['sr_cotisation_liste']);
if (!conf('module.abonnement')) unset($menu['services']['menu']['sr_abonnement_liste']);
if (!conf('module.vente')) unset($menu['services']['menu']['sr_vente_liste']);
if (!conf('module.don')) unset($menu['services']['menu']['sr_don_liste']);
if (!conf('module.perte')) unset($menu['services']['menu']['sr_perte_liste']);
if (!conf('module.adhesion')) unset($menu['services']['menu']['sr_adhesion_liste']);

if (!conf('module.paiement')) {
    unset($menu['services']['menu']['#sep1']);
    unset($menu['services']['menu']['paiement_liste']);
}

if (!conf('module.pre_compta'))
    unset($menu['pre_compta']);

if (!conf('module.newsletter')) {
    unset($menu['services']['menu']['#sep3']);
    unset($menu['services']['menu']['infolettre_liste']);
}

if ($app['user'])
    $menu = autorisationMenu($menu);


$app["twig"]->addGlobal("menu_general", $menu);

    return $menu;
}
