<?php


function colonneEstInteger($table, $colonne)
{
    $type=typeColonne($table, $colonne);
    if ($type)
        return (substr($type, 0, 3) == 'Int');
    return false;
}




function typeColonne($table, $colonne){
    $tab_cols = donne_liste_colonne($table);
    if(isset($tab_cols[$colonne]['type']))
        return $tab_cols[$colonne]['type'];
    return false;
}



function donne_liste_colonne($table){
    global $app;
    $sm = $app['db']->getSchemaManager();
    $columns = $sm->listTableColumns($table);

    $tab_cols = array();
    foreach ($columns as $column) {
        $tab_cols[$column->getName()] = [
            'autoincrement'=>$column->getAutoincrement(),
            'type'=>$column->getType(),
            'notnull'=>$column->getNotnull(),
            'default'=>$column->getDefault(),
            'length'=>$column->getLength(),
            'precision'=>$column->getPrecision(),
            'scale'=>$column->getScale(),
            'fixed'=>$column->getFixed(),
            'unsigned'=>$column->getUnsigned(),
            'comment'=>$column->getComment(),
        ];
    }
    return $tab_cols;
}


//Force la création de l'objet suite  a pilote +1 ou laisse a suite +1
//exemple pilote 3106 suite 3107 renvoi 0 donc increment normal
//exemple pilote 3107 suite 3107 renvoi 0 donc increment normal
//exemple pilote 3108 suite 3107 renvoi 3109 force a 3109

/**
 * autoincrement
 * @param bool|true $premier
 * @param string $objet_pilote
 * @param string $objet_suite
 */
function autoincrement($premier = true, $objet_pilote = "membre", $objet_suite = 'individu')
{
    global $app;
    $pilote = descr($objet_pilote . '.prefixe') . "_" . $objet_pilote;
    $suite = descr($objet_suite . '.prefixe') . "_" . $objet_suite;
    $statement = $app['db']->executeQuery("SELECT max( id_" . $objet_pilote . ") as id FROM " . $pilote . "s");
    $id_pilote = $statement->fetch()['id'] + $premier;//
    $statement = $app['db']->executeQuery("SELECT max( id_" . $objet_suite . ") as id FROM " . $suite . "s");
    $id_suite = $statement->fetch()['id'] + 1;
//    echo '<br> fin autoincrement pilote ' . $objet_pilote .' : '.$id_pilote.' suite '. $objet_suite .' : '.$id_suite;
    if ($id_suite < $id_pilote) {
        return $id_pilote;
    }
    return;
}

/**
 * autoincrement_idem
 * @param string $objet_1
 * @param string $prefixe_1
 * @param string $objet_2
 * @param string $prefixe_2
 */
function autoincrement_idem($objet_1 = "membre", $prefixe_1 = 'asso', $objet_2 = 'individu', $prefixe_2 = 'asso')
{
    global $app;
    // ne sert plus sauf import a verifier
    // todo ajouter le test de l'option numéro membre identique au n° d'individu donc des trous dans membre
    // todo pour avoir un numero de membre identique au numéro d'individu si un seul individu
    $statement = $app['db']->executeQuery("SELECT max( id_" . $objet_1 . ") as id_1 FROM " . $prefixe_1 . "_" . $objet_1 . "s");
    $id_1 = $statement->fetch()['id_1'] + 1;
    $statement = $app['db']->executeQuery("SELECT max( id_" . $objet_2 . ") as id_2 FROM " . $prefixe_2 . "_" . $objet_2 . "s");
    $id_2 = $statement->fetch()['id_2'] + 1;
    if ($id_1 < $id_2) {
        $id = $id_2;
    } else {
        $id = $id_1;
    }
    // todo traiter les erreurs des 2 lignes ci dessous
    $statement = $app['db']->executeQuery("ALTER TABLE " . $prefixe_1 . "_" . $objet_1 . "s AUTO_INCREMENT =" . $id);
    $statement = $app['db']->executeQuery("ALTER TABLE " . $prefixe_2 . "_" . $objet_2 . "s AUTO_INCREMENT =" . $id);
    return;
}


