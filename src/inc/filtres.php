<?php


function filtre_modele($nom)
{

    $tab_filtre_modele = [
        'email' => ['Avec email' => 'oui', 'Sans email' => 'non','Email incorrect'=>'npai'],
        'adresse' => ['Avec adresse' => 'oui', 'Adresse non renseigné' => 'non','Adresse non valide'=>'npai'],
        'telephone' => ['Avec telephone fixe' => 'oui', 'Pas de téléphone fixe' => 'non','Numéro de téléphone incorrect'=>'npai'],
        'telephone_pro' => ['Avec telephone pro' => 'oui', 'Pas de téléphone pro' => 'non','Numéro de téléphone pro incorrect'=>'npai'],
        'mobile' => ['Avec mobile' => 'oui', 'Pas de téléphone mobile' => 'non','Numéro de téléphone mobile incorrect'=>'npai'],
        'ardent' => ['Ardent' => 'ardent', 'Perdu de vue' => 'non'],
        'sorti' => ['Ancien adhérent' => 'oui', 'Adhérent actuel' => 'non'],
        'est_decede' => ['Vivant' => 'non', 'Décédé' => 'oui']
    ];


    if (isset($tab_filtre_modele[$nom])) {
        return $tab_filtre_modele[$nom];
    }

    return null;
}




function filtre_ajouter_cotisation($cotis)
{
    $tab_cotisation = ['ok', 'futur', 'echu', 'jamais'];
    $options = filtre_options_select($tab_cotisation,$cotis);
    return ['nom' => 'cotisation', 'options' => $options, 'class' => 'select2'];
}


function filtre_ajouter_cotisation_second()
{
    global $app;
    $tab=[];
    $request = $app['request'];
    $tab_carte = tab('mot_arbre.membre.Carte');
    if (conf('carte_adherent.etat_a_remettre'))
        unset($tab_carte[ trans('carte_adh2')]);
    if (conf('carte_adherent.etat_a_envoyer'))
        unset($tab_carte[ trans('carte_adh3')]);
    $tab_carte = array_flip($tab_carte);
      if(!empty($tab_carte)){
        $options = filtre_options_select($tab_carte,$request->get('carte'),true);
          $tab['carte'] = ['nom' => 'carte', 'options' => $options, 'class' => 'select2'];
      }

    $tab_log = LogQuery::create()->filterByCode('IMPCAR')->orderByDateOperation(\Propel\Runtime\ActiveQuery\Criteria::DESC)->find();
    $tab_impression=[];
    foreach($tab_log as $log){
            $tab_impression[$log->getIdLog()] = $log->getDateOperation()->format('Y/m/d - H:i');
    }

    if(!empty($tab_impression)){
        $options = filtre_options_select($tab_impression,$request->get('log_impcar'),true);
        $tab['log_impcar'] = ['nom' => 'log_impcar', 'options' => $options, 'class' => 'select2'];
    }


    if($tab_prestation = tab('prestation_type_calendrier.cotisation')){
        $tab_periode_cotisation= table_simplifier($tab_prestation);
        $options_periode_cotisation=  filtre_options_select($tab_periode_cotisation,$request->get('periode_cotisation'),true);
        $tab ['periode_cotisation'] = ['nom' => 'periode_cotisation', 'options' => $options_periode_cotisation, 'class' => 'select2_simple','multiple'=>true];
    }

    return $tab;
}


function filtre_options_select($tab,$values,$complexe=false){

    $t=[];
    foreach ($tab as $k=>$val) {
        if($complexe){
            $t[$k] = array('libelle' => $val, 'valeur' => $k);
            if ($values == $k) {
                $t[$k]['on'] = true;
            }
        }
        else{
            $t[$val] = array('libelle' => $val, 'valeur' => $val);
            if ($values == $val) {
                $t[$val]['on'] = true;
            }
        }


    }
    return $t;
}



function filtre_ajouter_cotisation_epurer($cotis, $sous_requete)
{
    $tab_cotisation = MembreQuery::liste_filtre_statut_cotis($sous_requete, 1);
    foreach ($tab_cotisation as $k => &$t) {
        $t = array('libelle' => $k, 'nb' => $t, 'valeur' => $k);
        if ($cotis == $k) {
            $t['on'] = true;
        }
    }
    return array('nom' => 'cotisation', 'options' => $tab_cotisation, 'class' => 'select2');
}





function filtre_ajouter_est_adherent($valeur)
{
    $options = [['on' => $valeur, 'valeur' => 'adherent', 'libelle' => 'est_adherent']];
    return array('nom' => 'est_adherent', 'type' => 'checkbox', 'options' => $options);
}



