<?php

use Propel\Runtime\ActiveQuery\Criteria;


function liste_secondaire($objet = "", $ecran = "")
{
    if ($objet and $ecran) {
        if ($objet == $ecran) {
            $nb = pref($objet . '.nb_ligneP', 25);
        } else {
            $nb = pref($ecran . '.nb_ligneS', 6);
        }
    } else {
        $nb = 7;
    }
    return $nb;
}



/**
 * construire_redirection
 * @param string $page
 * @param array $params
 * @return string
 */
function construire_redirection($page, $params = array())
{
    global $app;
    if ($url_redirect = $app['request']->get('redirect')) {
        return $url_redirect;
    } else {
        return $app->path($page, $params);
    }
}


function reponse_ejection()
{
    global $app;

    if (sac('id')) {
        $page = (sac('type_page') == 'form') ? sac('objet') : sac('page');
        $url_redirect = $app->path($page, [descr(sac('objet') . '.cle_sql') . '' => sac('id')]);
    } else {
        $page = (sac('objet')) ? sac('objet') . '_liste' : sac('page');
        $url_redirect = $app->path($page);
    }

    return $app['twig']->render('oust.html.twig', [ 'redirect' => $url_redirect ]);
}



function preparation_declencheur($tab_declencheur_possible){
    global $app;
    $declencheur = $app['request']->get('declencheur');
    if (in_array($declencheur,$tab_declencheur_possible))
        return $declencheur;
    return '';
}



/**
 * fichier_twig
 * @param string $type_page
 * @param null $objet
 * @return string
 */
function fichier_twig($type_page = '', $objet = null)
{
    if (!$objet) {
        $objet = sac('objet');
    }
    if ($objet) {
        $type_page = ($type_page === "") ? sac('type_page') : $type_page;
        $page = descr($objet . '.twig.' . $type_page);
        $source = (substr($page, 0, 5) == 'objet') ? '' : descr($objet . '.source');
    } else {
        $page = sac('pages.' . sac('page') . '.twig');
        $source = (substr($page, 0, 4) == 'page') ? '' : sac('pages.' . sac('page') . '.source');
    }
    return $source . $page . '.html.twig';
}


/**
 * @return bool|string
 */
function fichier_php($objet = null, $type_page = null)
{

    if (empty($objet)) {
        $objet = sac('objet');
    }
    if ($objet) {
        if (empty($type_page)) {
            $type_page = sac('type_page');
        }
        $page = descr($objet . '.php.' . $type_page);
        $source = (substr($page, 0, 5) == 'objet') ? '' : descr($objet . '.source');
    } else {
        $page = sac('pages.' . sac('page') . '.php');
        $source = (substr($page, 0, 4) == 'page') ? '' : sac('pages.' . sac('page') . '.source');
    }
    return $source . $page . '.php';

}

/**
 * Rassemble et définit des propriétés nessécaire à la construction d'un bouton
 * @param string $action permet de définir un ensemble de propriétés par défaut du bouton
 * @param array $options surcharge ou modifie les propriétéspar défaut du bouton
 * @return array tableau de propriété d'un bouton ou d'un lien (généralement pour envoi vers la vue lien.html.twig )
 */
function bouton($action, $options = array())
{

    global $app;
// pour gagner du temps renvoyer si pas de droits
    $objet = (isset($options['objet'])) ? $options['objet'] : sac('objet');// souhaité sinon principal
    switch ($action) {
        case 'voir':
        case 'imprimer':
            if (!(autoriser('A1') or autoriser('C1') or autoriser('G1'))) {
                return '';
            }
            break;
        case 'tagguer':
        case 'ajouter':
        case 'modifier':
        case 'dupliquer':
        case 'changer_mdp':
        case 'retour_courrier':
            if (!(autoriser('A2') or autoriser('C2') or autoriser('G2'))) {
                return '';
            }
            break;
        case 'sortir':
        case 'fusion':
        case 'supprimer':
            if (!(autoriser('A3') or autoriser('G3') or autoriser('C3'))) {
                return '';
            }
    }

    $id = (isset($options['id'])) ? $options['id'] : sac('id');// souhaité sinon principal
    $cle = (isset($options['cle'])) ? $options['cle'] : descr($objet . '.cle');
    $args_url = array($cle => $id);
    if (!isset($options['class_cplt'])) {
        $options['class_cplt'] = '';
    }
    if (isset($options['action'])) {
        $args_url = array_merge($args_url, array('action' => $options['action']));
    }
    if (!isset($options['nom'])) {
        $options['nom'] = $objet . '_' . $action;
    }
    if (isset($options['args'])) {
        $args_url = array_merge($args_url, $options['args']);
    }
    if (isset($options['redirect'])) {
        $args_url['redirect'] = $options['redirect'];
    }

    $options['action'] = $action;
    $page = $objet . '_form';

    switch ($action) {
        case 'voir':
            $options['nom'] = $objet;
            $page = $objet;
            break;

        case 'imprimer':
            $args_url['id_' . $objet] = $id;
            $args_url['action'] = $objet;
            $page = 'impression';
            break;

        case 'ajouter':
        case 'modifier':
        case 'sortir':
        case 'fusion':
            $options['class_cplt'] .= ' modal_form';
            break;

        case 'supprimer':
        case 'changer_mdp':
        case 'retour_courrier':
             $args_url['action'] = $action;
            break;

        case 'dupliquer':
            $args_url['action'] = $action;
            $options['class_cplt'] .= ' modal_form';
            break;

        case 'annuler':
            break;

        case 'tagguer':
            $options['class_cplt'] .= ' modal_form';
            $args_url['action'] = 'associer';
            $args_url['objet'] = $options['objet'];
            $args_url['id_objet'] = $options['id_objet'];
            $page = 'mot_form';
            break;
    }
    if (!isset($options['url'])) {
        $options['url'] = $app->path($page, $args_url);
    }
    return $options;
}


/**
 * menu_page_gauche
 * @return array
 */
function menu_page_gauche()
{
    global $app;
    $tab_fil = array();
    $page = sac('page');
    $type_page = sac('type_page');
    $objet = sac('objet');
    $id = sac('id');
    $action = sac('action');
    $url_pages = str_replace('objet', $objet, descr($objet . '.php'));
    if (empty($objet)) {

        $tab_fil['a'] = array(
            'action' => $action,
            'nom' => $app->trans($page),
            'url' => $app->path($page)
        );

    } else {

        switch ($type_page) {
            case 'liste':
            case 'multi':
                $tab_fil['9'] = array('action' => $action, 'nom' => $app->trans($objet . '_liste'));
                break;
            case 'voir':
            case 'form':
                $tab_fil['a'] = array(
                    'action' => $action,
                    'nom' => $app->trans($objet . '_liste'),
                    'url' => $app->path($url_pages['liste'])
                );
                if ($type_page == 'voir') {
                    $tab_fil['b'] = array('action' => $action, 'nom' => $app->trans($objet));
                } else {
                    $tab_fil['b'] = array('action' => $action, 'nom' => $app->trans($objet . '_' . $action));

                }
                if ($id) {
                    if (descr($objet . '.alias')) {
                        $objet = descr($objet . '.alias');
                    }

                    if (sac($objet . '.nomcplt')) {
                        $tab_fil['b']['nom'] .= ' <span class="badge">' . sac($objet . '.nomcplt') . '</span>';
                    }
                }
                break;
        }
    }
    ksort($tab_fil);
    return $tab_fil;
}




/**
 * formUrlRedirect
 * @param string $page
 * @param array $args
 * @return string
 */
function composer_url_redirect($id,$declencheur='')
{
    global $app;

    $redirect = $app['request']->get('redirect');
    $params = [];
    if(!empty($declencheur))
        $params['declencheur']=$declencheur;

    if (!empty($redirect)) {
        $url_redirect = $redirect;
    } elseif ($id) {
        $page = (sac('type_page') == 'form') ? sac('objet') : sac('page');
        $params[descr(sac('objet') . '.cle_sql') . ''] = $id;
        $url_redirect = $app->path($page,$params );
    } elseif (sac('id')) {
        $params[descr(sac('objet') . '.cle_sql') . ''] = sac('id');
        $page = (sac('type_page') == 'form') ? sac('objet') : sac('page');
        $url_redirect = $app->path($page, $params);
    } else {
        $page = (sac('objet')) ? sac('objet') . '_liste' : sac('page');
        $url_redirect = $app->path($page,$params);
    }

    return $url_redirect;
}

/**
 * documentation_lien
 * @param string $page
 * @param string $ancre
 * @return string
 */
function documentation_lien($page = '', $ancre = '')
{

    global $app;
    if (empty($page)) {
        $page = $app['documentation.accueil'];
    }
    $ancre = (!empty($ancre)) ? '#' . $ancre : '';
    return $app['documentation.url'] . $page . $ancre;

}


/**
 * action_supprimer_une_instance
 * @param string $retour
 * @return \Symfony\Component\HttpFoundation\RedirectResponse
 */
function action_supprimer_une_instance($retour = "")
{
    global $app;
    $objet = sac('objet');
    $id = sac('id');
    if ($retour == "") {
        $retour = $objet . '_liste';
    }
    $url_redirect = construire_redirection($retour);
    if ($id > 0) {
        $class = nom_query($objet);
        if (!class_exists($class)) {

            $app['session']->getFlashBag()->add('info', 'La classe de ' . $objet . ' n\'existe pas');
            return $app->redirect($url_redirect);

        }

        $objet_data = $class::create()->findPK($id);
        if ($objet_data) {
            $objet_data->delete();
            $app['session']->getFlashBag()->add('info',
                'La suppression de ' . $app->trans('du_' . $objet) . ' : ' . nommer_objet($objet,
                    $objet_data) . ' est effective');
            Log::EnrOp('SUP', $objet,  $id);
        }

    }

    return $app->redirect($url_redirect);
}


/**
 * nommer_objet
 * @return array
 */
function nommer_objet($objet, $objet_data)
{
    $champs_nom = descr($objet . '.champs_nom');
    if (empty($champs_no)) {
        $champs_nom = [descr($objet . '.cle_sql')];
    } elseif (!is_array($champs_nom)) {
        $champs_nom = [$champs_nom];
    }
    $tab = array_intersect_key($champs_nom, $objet_data->toArray());
    return implode(' ', $tab);
}

/**
 * menu_page_droite
 * @return array
 */
function menu_page_droite()
{
    global $app;
    $tab_fil = array();
    $type_page = sac('type_page');
    $objet = sac('objet');
    $url_pages = str_replace('objet', $objet, descr($objet . '.php'));
    $nomcplt = sac($objet . '.nomcplt');
    $id = sac('id');
    if (!empty($objet)) {

        if ($type_page != 'form' or $id) {
            $tab_fil['ajouter'] = [
                'action' => 'ajouter',
                'bt_texte' => true,
                'class_cplt' => 'modal_form',
                'url' => $app->path($objet . '_form')
            ];
            if ($type_page == 'voir') {
                $tab_fil['ajouter']['nom'] = $app->trans($objet . '_ajouter');
            }
            $tp = ($type_page == 'voir') ? '&id_' . sac('objet') . '=' . sac('id') : '_' . $type_page;
        }

        switch ($type_page) {

            case 'voir':
                if ($id) {
                    $tab_fil['x'] = [
                        'action' => 'modifier',
                        'url' => $app->path($url_pages['form'], [descr($objet . '.cle') => $id]),
                        'class_cplt' => 'modal_form',
                    ];
                    if ($nomcplt) {
                        $tab_fil['x']['nom'] = $app->trans($objet . '_modifier') . ' <span class="badge">' . $nomcplt . '</span>';
                    }
                }
                $tab_fil['x']['bt_texte'] = true;
                break;
            case 'form':

                if ($id) {
                    $tab_fil['x']['action'] = 'voir';
                    $tab_fil['x']['nom'] = $app->trans('retour_fiche');
                    $tab_fil['x']['url'] = $app->path($url_pages['voir'], array('id_' . $objet => $id));
                    if ($nomcplt) {
                        $tab_fil['x']['nom'] .= ' <span class="badge">' . $nomcplt . '</span>';
                    }

                } else {
                    $tab_fil['x']['action'] = 'liste';
                    $tab_fil['x']['nom'] = $app->trans('retour_liste');
                    $tab_fil['x']['url'] = $app->path($url_pages['liste']);
                }
                $tab_fil['x']['bt_texte'] = true;
                break;
        }
        if ($type_page != 'form' or $id) {
            $tab_fil['preference'] = [
                'action' => 'pref',
                'url' => $app->path('preferences', array('pref' => $objet, 'redirect' => $objet . $tp))
            ];
        }
    }
    return $tab_fil;
}

/**
 * tri_dataliste
 * @return array
 */
function tri_dataliste()
{
    global $app;
    $tab_tri = array();
    $tab_col = $app['request']->get('columns');
    $tab_order = $app['request']->get('order');
    if (is_array($tab_order) && !empty($tab_order)) {

        foreach ($tab_order as $k => $odr) {
            $num_col = $odr['column'];
            if (isset($tab_col[$num_col]['name']) && $tab_col[$num_col]['orderable']) {
                $tab_tri[strtolower($tab_col[$num_col]['name'])] = strtoupper($odr['dir']);
            }
        }
    }
    return $tab_tri;
}


/**
 * tri_dataliste_sql
 * @param $objet
 * @param $tab_tri
 * @return string
 */
function tri_dataliste_sql($objet, $tab_tri)
{

    $objet = is_array($objet) ? $objet : array($objet);

    $tri_sql = "";
    if (is_array($tab_tri) && !empty($tab_tri)) {
        $tri_sql = ' ORDER BY ';
        $tab = array();
        foreach ($tab_tri as $k => &$odr) {
            foreach ($objet as $o) {

                if (getColonneNom($o, $k)) {
                    $tab[] = descr($o . '.nom_sql') . '.' . getColonneNom($o, $k) . ' ' . $odr;
                    break;
                }
            }

        }
        if (!empty($tab)) {
            $tri_sql .= implode(',', $tab);
        } else {
            $tri_sql = '';
        }
    }
    return $tri_sql;
}


/**
 * getColonneNomPhp
 * @param $table
 * @param $name
 * @return mixed
 */
function getColonneNomPhp($table, $name)
{
    $class = 'Map\\' . ucfirst($table . 'TableMap');
    if (class_exists($class)) {
        return $class::getTableMap()->getColumnByName($name)->getPhpName();
    }
    return $name;

}


/**
 * getColonneNom
 * @param $objet
 * @param $namePHP
 * @return bool
 */
function getColonneNom($objet, $namePHP)
{
    $class = 'Map\\' . descr($objet . '.phpname') . 'TableMap';
    if (class_exists($class)) {
        if ($class::getTableMap()->hasColumn($namePHP)) {
            return $class::getTableMap()->getColumn($namePHP)->getName();
        }
    }
    return false;
}


/**
 * request_ou_options
 * @param $nom
 * @param array $params
 * @return string
 */
function request_ou_options($nom, $params = array())
{
    global $app;
    if (!isset($params[$nom]) && $app['request'] && $value = $app['request']->get($nom)) {
        return $value;
    } elseif (isset($params[$nom])) {
        return $params[$nom];
    }
    return '';
}





/**
 * liste_des_pages_complete
 * @return array
 */
function liste_des_pages_complete()
{
    global $app;
    $liste_des_pages = liste_des_pages();
    foreach ($liste_des_pages as $page => &$p) {

        $tab_phptwig = array(
            'twig' => array('rep' => '/resources/views/', 'sufixe' => '.html.twig'),
            'php' => array('rep' => '/src/', 'sufixe' => '.php')
        );

        if (!isset($p['source'])) {
            $p['source'] = '';
        }

        if (isset($p['twig'])) {
            unset($tab_phptwig['twig']);
        }
        $action = '';
        if (substr($page, -5) == '_form') {
            $action = '_form';
            $page = substr($page, 0, -5);
        }
        $path = $app['basepath'];
        foreach ($tab_phptwig as $k_pt => $v_pt) {
            $p[$k_pt] = source($path, $v_pt['rep'], $p['source'], $page, $action, $v_pt['sufixe'], $page, 'page');
        }
    }
    return $liste_des_pages;
}


/**
 * description_complete
 * @return array
 */
function description_complete()
{
    global $app;
    $description = liste_des_objets();
    ksort($description);
    $tab_alias = array();
    foreach ($description as $objet => &$do) {

        $do['prefixe'] = (isset($do['prefixe'])) ? $do['prefixe'] : 'asso';
        if ($do['prefixe'] == 'assc') {
            if (!isset($do['compta'])) {
                $do['compta'] = true;
            }
        }

        $do['objet'] = $objet;

        if (!isset($do['source'])) {
            $do['source'] = $do['prefixe'] . '/';
        }
        if (!isset($do['objet_action'])) {
            $do['objet_action'] = $objet;
        }
        if (isset($do['alias'])) {
            $do['objet'] = (isset($do['alias'])) ? $do['alias'] : $objet;
            $do['alias_valeur'] = isset($do['alias_valeur_class']) ? constant($do['alias_valeur_class']) : $do['alias_valeur'];
            $tab_alias[$do['alias']][$objet] = $do['alias_valeur'];
        }
        if (substr($objet, 0, 3) == 'sr_') {
            $do['objet_sans_sr'] = substr($objet, 3);
        } else {
            $do['objet_sans_sr'] = $objet;
        }
        $do['phpname'] = (isset($do['phpname'])) ? $do['phpname'] : ucfirst($do['objet']);
        $do['cle'] = (isset($do['cle'])) ? 'id_' . $do['cle'] : 'id_' . $do['objet'];//objet_alias
        $do['nom_sql'] = (isset($do['nom_sql'])) ? $do['nom_sql'] : $do['objet'];
        if (!isset($do['table_sql'])) {
            $do['table_sql'] = $do['prefixe'] . '_' . $do['nom_sql'] . 's';

        }
        if (!isset($do['cle_sql'])) {
            $do['cle_sql'] = (isset($do['nom_sql'])) ? 'id_' . $do['nom_sql'] : 'id_' . $do['objet'];
        }

        $sm = $app['db']->getSchemaManager();
        $columns = $sm->listTableColumns($do['table_sql']);
        $tab_cols = array();
        foreach ($columns as $column) {
            $tab_cols[$column->getName()] = $column->getType() . "";
        }

        $do['colonnes'] = $tab_cols;


        // le nom des fichiers sources
        $tab_action = array('voir' => '', 'liste' => '_liste', 'form' => '_form', 'table' => '_table');
        $tab_phptwig = array(
            'twig' => array('rep' => '/resources/views/', 'sufixe' => '.html.twig'),
            'php' => array('rep' => '/src/', 'sufixe' => '.php')
        );
        if (isset($do['nom_ecran'])) {
            $tab_action = array(
                'voir' => $do['nom_ecran'],
                'liste' => $do['nom_ecran'],
                'form' => $do['nom_ecran'],
                'table' => $do['nom_ecran']
            );
        }
        if (isset($do['php'])) {
            unset($tab_phptwig['php']);
        }
        if (isset($do['twig'])) {
            unset($tab_phptwig['twig']);
        }

        $path = $app['basepath'];

        foreach ($tab_action as $k_action => $v_action) {
            foreach ($tab_phptwig as $k_pt => $v_pt) {
                $do[$k_pt][$k_action] = source($path, $v_pt['rep'], $do['source'], $do['objet'], $v_action,
                    $v_pt['sufixe'], $objet);
            }
        }
        if (isset($do['choix_colonne'])) {
            $cc = $do['choix_colonne'];
            $do['choix_colonne'] = (is_array($cc)) ? $cc : array($cc);
        } else {
            $champs_ordre_possible = array_values(array_intersect(array_keys($do['colonnes']),
                ['code', 'nom', 'libelle', 'libelle1']));
            $do['choix_colonne'] = empty($champs_ordre_possible) ? [''] : [$champs_ordre_possible[0]];
        }
        if (!isset($do['champs_nom'])) {
            $do['champs_nom'] = $do['choix_colonne'];
        }
        if (!isset($do['choix_ordre'])) {
            $champs_ordre_possible = array_intersect(array_keys($do['colonnes']),
                ['nom', 'libelle', 'libelle1', 'code']);
            $do['choix_ordre'] = array_shift($champs_ordre_possible);
        }


        $do['nomcplt'] = ' ';// a justifier l'espace et la présence nomcplt = nomcourt ou nom sur 6 ou id en dernier ressort
        $do['log'] = (isset($do['log'])) ? $do['log'] : strtoupper(substr($objet, 0, 3));
        ksort($do);
    }
    foreach ($tab_alias as $obj => $v) {
        if (isset($description[$obj])) {
            $description[$obj]['tab_alias'] = $v;

        }
    }
    return $description;
}


/**
 * getSelections
 * @param $objet
 * @return array
 */
function getSelections($objet)
{

    global $app;

    $temp = array(
        $app->trans('Selection courante') => 'courante',
        $app->trans('Tous les ' . $app->trans($objet . 's')) => 'tous'
    );
    $prefs = pref('selection.' . $objet);
    foreach ($prefs as $k => $pref) {
        $temp[$pref['nom']] = $k;
    }
    return $temp;

}


/**
 * source
 * @param $path
 * @param $rep
 * @param $source
 * @param $objet
 * @param $action
 * @param $sufixe
 * @param $objet_origine
 * @return string
 */
function source($path, $rep, $source, $objet, $action, $sufixe, $objet_origine = "", $type_vue = 'objet')
{

    if (file_exists($path . $rep . $source . $objet_origine . $action . $sufixe)) {
        return $objet_origine . $action;
    } elseif (file_exists($path . $rep . $source . $objet . $action . $sufixe)) {
        return $objet . $action;
    }
    return $type_vue . $action;
}


/**
 * suc
 * @param $var
 * @return null
 */
function suc($var = '')
{

    global $app;
    $temp = $app['session']->get('variable');

    if ($var == '') {
        return $temp;
    }

    $path_array = explode('.', $var);


    foreach ($path_array as $key) {
        if (isset($temp[$key])) {
            $temp = $temp[$key];
        } else {
            return null;
        }
    }
    return $temp;

}


/**
 * mot
 * @param $valeur
 * @param string $filtre
 * @param string $retour
 * @return mixed
 */
function mot($valeur, $filtre = 'nomcourt', $retour = 'id_mot')
{
    return table_filtrer_valeur_premiere(tab('mot'), $filtre, $valeur)[$retour];
}

/**
 * mots
 * @param $nomcourt_groupe
 * @param string $objet
 * @return array|null
 */
function mots($nomcourt_groupe, $objet = 'individu')
{
    return tab('mot_arbre.' . $objet . '.' . $nomcourt_groupe);
}


/**
 * descr
 * @param $var
 * @return array|null
 */
function descr($var = '')
{
    $var = (!empty($var)) ? '.' . $var : '';
    return sac('description' . $var );
}

/**
 * tab
 * @param $var
 * @return array|null
 */
function tab($var)
{
    return sac('table.' . $var);
}


/**
 * sac
 * @param string $var
 * @return array|null
 */
function sac($var = '')
{

    global $app;
    //Renvoie toutes les variables stockées dans contexte et le supercache
    if ($var == '') {
        $tab = getSuperCache();
        $temp = array();
        foreach ($tab as $var) {
            if ($app['cache']->exists($var)) {
                $temp[$var] = $app['cache']->fetch($var);
            }
        }
        return array_merge($app['contexte'], $temp);
    }

    $path_array = explode('.', $var);
    if (in_array($path_array[0], getSuperCache())) {
        $var0 = $path_array[0];
        array_shift($path_array);
        if ($app['cache']->exists($var0)) {
            $temp = $app['cache']->fetch($var0);
        } else {
            return null;
        }
    } else {
        if (isset($app['contexte'])) {
            $temp = $app['contexte'];
        } else {
            return null;
        }
    }
    return tableauChemin($temp, $path_array);
}


/**
 * setContexte
 * @param $var
 * @param $value
 * @return bool
 */
function setContexte($var, $value)
{
    global $app;
    $path_array = explode('.', $var);
    if (in_array($path_array[0], getSuperCache())) {
        $var0 = $path_array[0];
        array_shift($path_array);
        if (!empty($path_array)) {
            $temp_a = sac($var);
            $temp = &$temp_a;
            foreach ($path_array as $key) {
                if (!isset($temp[$key])) {
                    $temp[$key] = array();
                }
                $temp = &$temp[$key];
            }
            $temp = $temp_a;
        }
        $temp = $value;
        $app['cache']->store($var, $temp);
//        if($var=='table.entite')    isac('');
    } else {
        $temp_a = isset($app['contexte']) ? $app['contexte'] : array();
        $app['contexte'] = dessinerUneBranche($temp_a, $var, $value);
    }
    return true;
}

/**
 * dessinerUneBranche
 * @param $arbre
 * @param $noeud
 * @param $branche
 * @return mixed
 */
function dessinerUneBranche($arbre, $noeud, $branche)
{

    $path_array = explode('.', $noeud);

    $temp = &$arbre;
    foreach ($path_array as $key) {
        if (!isset($temp[$key])) {
            $temp[$key] = array();
        }
        $temp = &$temp[$key];
    }
    $temp = $branche;
    return $arbre;
}


/**
 * getListePages
 * @return array
 */
function getListePages()
{
    $tab_pages = array_keys(liste_des_pages());
    $liste_objet = array_keys(liste_des_objets());
    foreach ($liste_objet as $k) {
        $k = strtolower($k);
        $tab_pages[] = $k;
        $tab_pages[] = $k . '_form';
        $tab_pages[] = $k . '_liste';
    }

    return $tab_pages;
}


/**
 * getValeurObjetAlias
 * @param $objet
 * @param $alias
 * @return string
 */
function getValeurObjetAlias($objet, $alias)
{
    $tab_alias = descr($objet . '.tab_alias');
    return isset($tab_alias[$alias]) ? $tab_alias[$alias] : '';

}

/**
 * nom_table_sql
 * @param $objet
 * @return string
 */
function nom_table_sql($objet)
{
    $des = descr($objet);
    return $des['prefixe'] . '_' . $des['nom_sql'] . 's';
}

// $valeur '' exclus les valide et abandon
/**
 * import_attente
 * @param string $valeur
 * @return bool
 */
function import_attente($valeur = '')
{
    return ImportQuery::create()->filterByAvancement(array('7compte_rendu', '0abandon'), Criteria::NOT_IN)->exists();

}

/**
 * datatable_complete_liste_colonne
 * @param $tab_colonne
 * @return mixed
 */
function datatable_complete_liste_colonne($tab_colonne)
{
    global $app;
    foreach ($tab_colonne as $k => &$col) {
        if (!isset($col['name'])) {
            $col['name'] = $k;
        }
        if (!isset($col['title'])) {
            $col['title'] = $k;
        }
        $col['title'] = $app->trans($col['title']);

    }
    return $tab_colonne;
}


/**
 * datatable_prepare_data
 * @param $tab_objets
 * @param $tab_colonnes
 * @param bool|true $only_value
 * @return array
 */
function datatable_prepare_data($tab_objets, $tab_colonnes, $only_value = true)
{
    $tab_data = array();
    foreach ($tab_objets as $o) {

        $data = $o->toArray();
        $temp = [];
        foreach ($tab_colonnes as $nom_col => $col) {
            if (strpos($nom_col, '.') > -1) {
                $table = substr($nom_col, 0, strpos($nom_col, '.'));
                $nom_fonction = 'get' . ucfirst($table);
                $nom_col_table = substr($nom_col, strpos($nom_col, '.') + 1);
                $data2 = array();
                if ($o2 = $o->$nom_fonction()) {
                    $data2 = $o2->toArray();
                }
                if (isset($data2[$nom_col_table])) {

                    $temp[$nom_col] = $data2[$nom_col_table];
                }
            } else {
                if (isset($data[$nom_col])) {
                    if (isset($col['type']) && $col['type'] == 'date-eu') {
                        $val = $o->getByName($nom_col);
                        $temp[$nom_col] = ($val) ? $val->format('d/m/Y') : '';
                    } else {
                        $temp[$nom_col] = $data[$nom_col];
                    }
                }
            }


            if (!isset($temp[$nom_col])) {

                $temp[$nom_col] = '';
            }

        }
        if ($only_value) {
            $tab_data[] = array_values($temp);
        } else {
            $tab_data[] = $temp;
        }
    }
    return $tab_data;
}
