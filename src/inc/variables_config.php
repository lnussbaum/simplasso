<?php


function variables_config()
{

    init_sac();

    return array(

        'bdd_version' => array(
            'variables' => bdd_version(),
            'systeme' => true
        ),
        'config_version' => array(
            'variables' => config_version(),
            'systeme' => true
        ),
        'pays' => array(
            'variables' => 'FR'

        ),
        'langue' => array(
            'variables' => 'fr'
        ),
        'module' => array(
            'variables' => array(
                'cotisation' => true,
                'abonnement' => false,
                'vente' => false,
                'don' => true,
                'perte' => true,
                'adhesion' => false,
                'paiement' => true,
                'pre_compta' => false,
                'newsletter' => false,
                'importation' => false,
                'restriction' => false,
            ),
            'parametrage_par_entite' => true
        ),
        'champs' => array(
            'variables' => array(
                'membre' => array(
                    'identifiant_interne' => false,
                    'nomcourt' => false
                ),
                'individu' => array(
                    'nomcourt' => [
                        'type_champs' => 'oui_non',
                        'valeur' => false
                    ]
                ),
                'courrier' => array(
                    'nomcourt' => false
                ),
                'mot' => array(
                    'nomcourt' => false
                ),
                'motgroupe' => array(
                    'nomcourt' => false
                ),
            ),
            'parametrage_par_entite' => true
        ),
        'civilite' => array(
            'variables' => array(
                'membre' => [
                    'libre' => false,
                    'valeurs' => [
                        'type_champs' => 'multiple_cle_valeur',
                        'valeur' => [
                            'M' => 'Monsieur',
                            'Mme' => 'Madame',
                            'fam' => 'Famille',
                            'MMm' => 'Monsieur et Madame'
                        ]
                    ],
                ],
                'individu' => [
                    'libre' => false,
                    'valeurs' => [
                        'type_champs' => 'multiple_cle_valeur',
                        'valeur' => [
                            'M' => 'Monsieur',
                            'Mme' => 'Madame',
                        ]
                    ],
                ]
            ),
            'parametrage_par_entite' => true
        ),
        'npai' => array(
            'variables' => array(
                'saisie_unique' => false,
                'saisie_date' => false
            ),
            'parametrage_par_entite' => true
        ),
        'carte_adherent' => array(
            'variables' => array(
                'actif' => true,
                'etat_a_remettre' => false,
                'etat_a_envoyer' => false,
                'objet' => [
                    'type_champs' => 'radio',
                    'choices' => ['individu' => 'individu', 'membre' => 'membre'],
                    'valeur' => 'individu'
                ],
                'automatique_cotisation' => false,
                'automatique_adhesion' => true
            ),
            'parametrage_par_entite' => true
        ),
        'identifiant' => array(
            'variables' => array(
                'idem_individu_membre' => false,
                'individu' => true,
                'membre' => true,
            ),
            'parametrage_par_entite' => true
        ),
        'restriction_mode' => array(
            'variables' => array(
                'nom' => [
                'type_champs' => 'radio',
                    'choices' => ['Aucun' => 'aucun',
                        'Mot de l\'operateur' => 'mot_operateur',
                        'Une restriction' => 'restriction'],
                'valeur' => 'individu'
                ],
                'non_concerne'=>[
                    'type_champs' => 'radio_multiple',
                     'choices' => array_flip(tab('operateur')),
                    'valeur'=>array()
                ]
            ),
            'parametrage_par_entite' => false
        ),
        'geo' => [
            'variables' => [
                'nominatim' => [
                    'url' => 'http://nominatim.openstreetmap.org/',
                    'params' => '&format=json',
                    'params_search' => 'q'
                ]
            ]
        ]

    );
}



