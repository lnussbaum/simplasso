<?php


function nettoyer_donnees(&$data){
    $data=trim($data);
    $data= preg_replace("/(<br>)*$/", "", $data);
    $data= preg_replace("/^(<br>)*/", "", $data);
    return $data;
}

function affecterCharset(&$data){
    $data =  mb_convert_encoding($data, "UTF-8", "UTF-8");
    $data =  stripAccents($data);
    return $data;
}

function stripAccents($string){
    return strtr($string,'àáâãäçèéêëìíîïñòóôõöùúûüýÿÀÁÂÃÄÇÈÉÊËÌÍÎÏÑÒÓÔÕÖÙÚÛÜÝ',
        'aaaaaceeeeiiiinooooouuuuyyAAAAACEEEEIIIINOOOOOUUUUY');
}



function extraire_donnee_multiple($tab_correspondance, $tab_data){

    $result=array();
    foreach($tab_correspondance as $cle=>$t_cor){
        $result[$cle] = extraire_donnee($t_cor, $tab_data);
    }
    return $result;
}


function extraire_donnee($tab_correspondance, $tab_data){

    $result=array();
    foreach($tab_correspondance as $cle=>$val){
        if (!isset($val['i']))
            $val=array('i'=>$val);
        $tab_indice = is_array($val['i'])?$val['i']:[$val['i']];
        $tmp=[];
        $glue = isset($val['glue'])?$val['glue']:' ';
        foreach($tab_indice as $i){
            if(!empty($tab_data[$i]))
                $tmp[]=$tab_data[$i];
        }

        if (!empty($tmp)){
            $result[$cle]=implode($glue,$tmp);
            nettoyer_donnees($result[$cle]);
            if (isset($val['date'])){
                $result[$cle]=DateTime::createFromFormat($val['date'],$result[$cle]);
            }
        }


    }
    return $result;
}

function appliquerAttibtut($tab,$attr){

    foreach($tab as $k=>&$t){
        foreach($attr as $cle=>$val){
            if (is_integer($t[$cle]))
                $t[$cle]=['i'=>$t[$cle]];
            $t[$cle] =array_merge($t[$cle],$val);
        }

    }
    return $tab;

}


function dissocier_individu($data){

    $tab_individu= array();
    if (isset($data['prenom']) && isset($data['nom_famille'])){
        $prenoms = preg_split('/&|\ et\ |\ -|-\ |\ -\ |\,|\,\ /', $data['prenom']);
        $tab_nom = preg_split('/&|\ et\ |\ -|-\ |\ -\ |\,|\,\ /', $data['nom_famille']);
        $nb_nom = count($tab_nom);
        foreach( $prenoms as $k => $prenom) {
            $temp = $data;
            $temp['prenom'] = trim($prenom);
            if($k<count($tab_nom))
                $temp['nom_famille'] = trim($tab_nom[$k]);
            else
                $temp['nom_famille'] = trim($tab_nom[0]);
            $temp['nom'] = $temp['nom_famille'] . ' ' . $temp['prenom'];
            if ($k > 0) {
                $temp['email'] = '';
                $temp['naissance'] = '';
                $temp['mobile'] = '';
            }



            $tab_individu[]=$temp;
        }
    }
    else{
        return [$data];
    }

    return $tab_individu;
}


function demeler_numeros_telephones($data){

    $data = demeler_numero_telephone($data,'telephone');
    $data = demeler_numero_telephone($data,'telephone_pro');
    return demeler_numero_telephone($data,'mobile');



}


function demeler_numero_telephone($data,$type)
{
    if (!empty($data[$type])) {
        $tab_tel = preg_split('/\/|ou/', $data[$type]);
        array_walk($tab_tel, 'nettoyer_donnees');
        $tab_temp= array();
        foreach ($tab_tel as $i => &$tel) {
            $tel = str_replace(['.', ' '], '', $tel);
            if($type!='mobile'){
                if ((substr($tel, 0, 2) == '06'||substr($tel, 0, 2) == '07')  ) {
                    if((!isset($data['mobile']) or trim($data['mobile']) == '')){
                        $data['mobile'] = $tel;
                        continue;
                    }
                }
            }
            else{
                if (substr($tel, 0, 2) != '06' && substr($tel, 0, 2) != '07' ) {
                    if((!isset($data['telephone']) or $data['telephone'] == '')){
                        $data['telephone'] = $tel;
                        continue;
                    }elseif((!isset($data['telephone_pro']) or $data['telephone_pro'] == '')){
                        $data['telephone_pro'] = $tel;
                        continue;
                    }
                }

            }
            $tab_temp[]=$tel;
        }
        $data[$type] = array_shift($tab_temp);
        if (!empty($tab_temp)) {
            if (!isset($data['observation']))
                $data['observation'] = '';
            foreach ($tab_temp as $tel) {
                $data['observation'] .= 'Telephone suppl : ' . $tel . '<br>';
            }
        }
    }

    return $data;
}


function rectifier_pays($data){

    global $app;
    if (!isset($data['pays']) or $data['pays']==''){
        $data['pays']=conf('pays');
        return $data;
    }else{

        $tab_pays = tab('pays');
        if(!isset($tab_pays[$data['pays']])){
            $tab_pays=array_flip($tab_pays);
            $data['pays']=ucwords(strtolower($data['pays']));
            if(isset($tab_pays[$data['pays']])){
                $data['pays'] = $tab_pays[$data['pays']];}
            else{
                $pays = (include($app['basepath'].'/vendor/umpirsky/country-list/data/en/country.php'));
                $tab_pays=array_flip($pays);
                if(isset($tab_pays[$data['pays']])){
                    $data['pays'] = $tab_pays[$data['pays']];}
                else {
                    $pays = (include($app['basepath'].'/vendor/umpirsky/country-list/data/nl/country.php'));
                    $tab_pays=array_flip($pays);
                    if(isset($tab_pays[$data['pays']])){
                        $data['pays'] = $tab_pays[$data['pays']];}
                    else {
                        echo('Impossible d\'attacher l\'individu a son pays ' . $data['pays'].PHP_EOL);
                        unset($data['pays']);
                    }
                }
            }
        }
    }

return $data;


}

function rectifierCivilite($valeur){
    nettoyer_donnees($valeur);
    $valeur= preg_replace('/(\ )*(et|&)(\ )*/', " et ", $valeur);
    $valeur= trim(preg_replace('/\./', '', $valeur));
    return $valeur;
}

function remplacerValeur($valeur,$tab_remplacement){

    if (isset($tab_remplacement[$valeur]))
        return $tab_remplacement[$valeur];
    return $valeur;
}

function tabRemplacerValeur($tab,$tab_remplacement){
    foreach($tab as &$valeur)
        $valeur = remplacerValeur($valeur,$tab_remplacement);
    return $tab;
}

function autoriserValeur($champs,$valeur,$tab_civilite_autorise){

    if (in_array($valeur,$tab_civilite_autorise))
        return $valeur;
        echo("la valeur $valeur n'est pas autorisé dans le champs $champs".PHP_EOL);
    return "";
}





function motgroupe_creation($nom,$objet_en_lien){
    $objet_data = new Motgroupe();
    $objet_data->setNom(substr($nom,0,40));
    $objet_data->setNomcourt(strtolower(stripAccents(substr($nom,0,6))));
    if(strlen($nom)>40)
        $objet_data->setDescriptif($nom);
    $objet_data->setTexte("");
    $objet_data->setSysteme(false);
    $objet_data->setobjetsEnLien('individu');
    $objet_data->setIdParent(0);
    $objet_data->setobjetsEnLien($objet_en_lien);
    $objet_data->save();
    return $objet_data->getIdMotgroupe();

}


function mot_creation($nom_mot,$id_groupe){
    $objet_data = new Mot();
    $objet_data->setNom(substr($nom_mot,0,40));
    $objet_data->setNomcourt(strtolower(stripAccents(substr($nom_mot,0,6))));
    if(strlen($nom_mot)>40)
        $objet_data->setDescriptif($nom_mot);
    $objet_data->setTexte("");
    $objet_data->setIdMotgroupe($id_groupe);
    $objet_data->save();
    return $objet_data->getIdMot();

}



function controlerRectifierDate($tab_col, $tab){


    foreach($tab_col as $annee=>$col){
        $indice=$col['date']['i'];

        $date=$tab[$indice];

        if (!empty($date)){

            if(strlen($date)==10 && substr($date,6,4)<1950 || substr($date,6,4)>2070){
                if(substr($date,8,2)>70)
                    $date=substr($date,0,6).'19'.substr($date,8,2);
                else
                    $date=substr($date,0,6).'20'.substr($date,8,2);
                }
            elseif(strlen($date)==8){

            if(substr($date,6,2)>20)
                $date=substr($date,0,6).'19'.substr($date,6,2);
            }

            $tab[$indice]=$date;
        }

    }

    return $tab;
}



function ajouterPaiement($data,$tab_servicerendu)
{

    $objet_data = new Paiement();
    $objet_data->setCreatedAt($data['created_at']);
    unset($data['created_at']);
    $objet_data->fromArray($data);
    $objet_data->save();
    $id_objet=$objet_data->getIdPaiement();

    foreach($tab_servicerendu as $servicerendu){
        $objet_data1 = new Servicepaiement();
        $objet_data1->setIdServicerendu($servicerendu['id_servicerendu']);
        $objet_data1->setIdPaiement($id_objet);
        $objet_data1->setMontant($servicerendu['montant']);
        $objet_data1->save();
    }
    return $id_objet;
}
