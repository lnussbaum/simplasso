<?php
require_once($app['basepath'] . '/src/inc/variables.php');
require_once($app['basepath'] . '/src/inc/fonctions.php');
require_once($app['basepath'] . '/src/db/base_migration_fonctions.php');



function estUneAncienneVersion ($num_version, $num_de_version_reference=''){

    global $app;
    if (empty($num_de_version_reference)){
        $num_de_version_reference = $app['bdd_version'];
    }
    if (empty($num_de_version_reference)){
        $num_de_version_reference='0.0.0';
    }

    return  version_compare($num_version, $num_de_version_reference) < 0;
}


function changer_version($v){

    global $app;
    $sql = "SELECT variables FROM asso_configs WHERE nom = 'bdd_version' AND id_individu IS NULL  AND id_entite IS NULL  LIMIT 1";
    $sth = $app['db']->query($sql);
    // ne faisait jamais l'insert car $sth n'était pas null
    if ($sth)    $num_version_actuel = json_decode($sth->fetchColumn(), true);
    else   $num_version_actuel=null;
    if ($num_version_actuel){
        $app['db']->query("UPDATE asso_configs SET variables = '".json_encode($v)."' WHERE nom='bdd_version'");
    } else {
        $app['db']->query("INSERT INTO asso_configs (nom, niveau_prefs, variables) VALUES ('bdd_version',4,'".json_encode($v)."')");
    }
    return $v;
}

function versionSort($a, $b) {
    return  version_compare($a, $b);
}


function liste_maj($chemin){

    $files = scandir($chemin);
    $tab = [];
    foreach($files as $file) {
        if(!is_dir($file)){
            $info = pathinfo($file);
            if ($info['extension']=='php'){
                $tab[]=$info['filename'];
            }
        }
    }
    usort($tab, 'versionSort');

    return $tab;
}


function migration_base() {
    global $app;

    $sql = "SELECT variables FROM asso_configs WHERE nom = 'bdd_version' AND id_individu IS NULL  AND id_entite IS NULL  LIMIT 1";
    $sth = $app['db']->query($sql);
    if ($sth){
        $num_version_actuel = json_decode($sth->fetchColumn(), true);
    } else {
        $num_version_actuel='0.0.0';
    };




    echo('Version en cours : '.$num_version_actuel).PHP_EOL;

    if ( estUneAncienneVersion ($num_version_actuel)  ){

        // Série de mise à jour
        $serie_maj= liste_maj($app['basepath'] . '/src/db/maj/');

        foreach($serie_maj as $v){
            $num_version_fichier = str_replace('_','.',$v);

            if ( estUneAncienneVersion ($num_version_actuel,$num_version_fichier)  ) {
                echo('Passage de la version '.$num_version_actuel. ' à la version '.$num_version_fichier).PHP_EOL;
                include_once($app['basepath'] . '/src/db/maj/'.$v.'.php');
                $nom_fonction='maj'.$v;
                if (function_exists($nom_fonction)) {
                    $num_version_actuel = $nom_fonction();
                    if  ($num_version_actuel === $num_version_fichier ) {
                        changer_version($num_version_fichier);
                    }else{
                        echo('version actuelle bdd :'.$num_version_actuel).PHP_EOL;
                        exit('ERREUR dans le passage à la version '.$num_version_fichier);
                    }
                }
            }
        }
    } else {
        echo('Votre version est à jour, rien ne change.');

    }

}
