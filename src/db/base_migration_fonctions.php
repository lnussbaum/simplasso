<?php





function nettoyage($chaine) {

    $chaine = str_replace(array('"', "\n", "\r"), ' ', $chaine);
    $chaine = str_replace(array('’'), '\'', $chaine);
    $chaine = str_replace(array('–'), '-', $chaine);
    $chaine = str_replace("\x92", "'", $chaine);
    $chaine = html_entity_decode($chaine);
    $chaine = str_replace(array("\xc9"), 'E', $chaine);
    $chaine = str_replace(array("É"), 'E', $chaine);

    // $chaine=mb_convert_encoding ($chaine,"UTF-8","ISO-8859-15");
    //$chaine=utf8_encode($chaine);

    return $chaine;
}


function nettoyage_simple($chaine) {

    $chaine = str_replace(array('’'), '\'', $chaine);
    $chaine = str_replace(array('–'), '-', $chaine);
    $chaine = str_replace("\x92", "'", $chaine);
    $chaine = html_entity_decode($chaine);
    $chaine = str_replace(array("\xc9"), 'E', $chaine);
    $chaine = str_replace(array("É"), 'E', $chaine);

    // $chaine=mb_convert_encoding ($chaine,"UTF-8","ISO-8859-15");
    //$chaine=utf8_encode($chaine);

    return $chaine;
}


function  recherche_commune($nom, $departement = "") {
    global $app;
    $sql = 'SELECT concat(departement,code) as code FROM spip_cog_communes where trim(nom) like \'' . str_replace('\'', '\'\'', $nom) . '%\'';
    if (!empty($departement))
        $sql .= ' and departement=' . $departement;

    $code_commune = $app['dbs']['new']->fetchAll($sql);
    if (!empty($code_commune)) {
        return $code_commune[0]['code'];
    }
    return '';
}


function extraire_coords_gmap($json) {
    if (!empty($json)) {
        $tab_coord = json_decode($json, true);
        if (!isset($tab_coord->routes[0]))
            return '';
        $tab_coord = $tab_coord->routes[0]->overview_path;
        foreach ($tab_coord as &$coord) {
            $coord = substr($coord, 12);
            if (substr($coord, 0, 1) == 0)
                $coord = substr($coord, 6);
        }
        return implode(';', $tab_coord);
    }
    return '';

}


function extraire_points_chemin($json) {
    if (!empty($json)) {
        $tab_coord = json_decode($json, true);
        $t_coords = array();
        if (!isset($tab_coord->routes[0]))
            return '';
        $inter = array();
        if (isset($tab_coord->Nb->waypoints))
            $inter = $tab_coord->Nb->waypoints;
        if (isset($tab_coord->Lb->waypoints))
            $inter = $tab_coord->Lb->waypoints;

        $longueur = substr($tab_coord->routes[0]->legs[0]->start_location, 0, 12) == "LatLngPlacH " ? 12 : 18;
        $t_coords[] = substr($tab_coord->routes[0]->legs[0]->start_location, $longueur);

        foreach ($inter as $points) {
            $longueur = substr($points->location, 0, 12) == "LatLngPlacH " ? 12 : 18;
            $t_coords[] = substr($points->location, $longueur);
        }
        $longueur = substr($tab_coord->routes[0]->legs[0]->end_location, 0, 12) == "LatLngPlacH " ? 12 : 18;
        $t_coords[] = substr($tab_coord->routes[0]->legs[0]->end_location, $longueur);
        return implode(';', $t_coords);
    }
    return '';

}


function decripter_adresse($chaine)
{
    $tab_pays = array('belgique' => 24, 'france' => 70);

    $codepostal = "";
    $ville = "";
    $pays = 70;
    $tpet = explode('- ', $chaine);
    $nom = $tpet[0];
    $long = count($tpet);
    $long--;
    if ($long > 2) {
        if (isset($tab_pays[strtolower($tpet[$long])])) {
            $pays = $tab_pays[strtolower($tpet[$long])];
            $long--;
        }
    }
    $ville = trim($tpet[$long]);
    $pos = strpos($ville, ' ');
    $codepostal = trim(substr($ville, 0, $pos));
    $ville = trim(substr($ville, $pos));
    $adresse = array();
    if ($long > 1 && $pays == 70 && $tpet[$long - 1] > 0 && $codepostal == "") {
        $codepostal = $tpet[$long - 1];
        $long--;
    }
    for ($i = 1; $i < $long; $i++)
        $adresse[] = $tpet[$i];

    //var_dump(array($nom,implode(PHP_EOL,$adresse),$codepostal,$ville,$pays));
    return array($nom, implode(PHP_EOL, $adresse), $codepostal, $ville, $pays);
}


function ajouter_modifier_auteur_individus($data)
{
    if (isset($data['email'])) {
        $individu = IndividuQuery::create()->filterByEmail($data['email'])->findOne();
        if (!$individu)
            $individu = new Individu();
        $individu->fromArray($data);
        $individu->save();

        return $individu->getIdAuteur();
    }
    return '';


}
function ajouter_modifier_auteur($data)
{
    if (isset($data['email'])) {
        $individu = IndividuQuery::create()->filterByEmail($data['email'])->findOne();
        if (!$individu)
            $individu = new Individu();
        $individu->fromArray($data);
        $individu->save();

        return $individu->getIdAuteur();
    }
    return '';


}


function transforme_timestampable($table)
{

    change_nom_colonne($table, 'date_creation', 'created_at', 'datetime');
    change_nom_colonne($table, 'date_maj', 'updated_at', 'datetime');

}


function ajoute_archivable($table)
{
    global $app;

}
// la date "0000-00-00" bloque DROP INDEX `prenom` ON `asso_individus`; avec un message erronné
function corrige_naissance()
{
    global $app;
    $statement = $app['db']->executeQuery('SELECT id_individu FROM asso_individus where naissance="0000-00-00"');
    while ($val = $statement->fetch()) {
        $sql = 'UPDATE asso_individus SET naissance = NULL WHERE id_individu=' . $val['id_individu'];
        $app['db']->executeQuery($sql);
    }
    $statement = $app['db']->executeQuery("SELECT id_individu FROM asso_individus where created_at like '%0000%'");
    while ($val = $statement->fetch()) {
        $sql = 'UPDATE asso_individus SET created_at = null WHERE id_individu=' . $val['id_individu'];
        $app['db']->executeQuery($sql);
    }
}

function resultat_execute_query($cde)
{
    global $app;
    $ok = $app['db']->executeQuery($cde);
    $export = "";

    while ($val = $ok->fetch()) {
        $export .= str_replace(array("\n", "\r", "\""), '', implode("\t", $val)) . PHP_EOL;
    }

    return $export;
}

function execute_query($cde)
{
    global $app;
    $ok = $app['db']->executeQuery($cde);
    if ($ok)
        return true;
    else
        echo 'la commande ' . $cde . ' a échoué';
    return false;

}



function creer_une_table($table, $tab_tables, $tableancien)
{
    global $app;
    $cle = array_search($tableancien, $tab_tables);
    if ($cle && !in_array($table, $tab_tables)) {
        $app['db']->executeQuery("RENAME TABLE " . $tableancien . " TO " . $table);
        $tab_tables[$cle] = $table;
        return true;
    }
    return false;
}

function dupliquer_table($table_modele,$table )
{
    global $app;
    if (!table_existe($table_modele))
        return false;
    if (table_existe($table))
        return false;

    $app['db']->executeQuery("CREATE TABLE " . $table . " LIKE " . $table_modele);
    $app['db']->executeQuery("INSERT " . $table . " SELECT * FROM " . $table_modele);
    return true;

    return false;
}



function renomme_table($table, $tab_tables, $tableancien)
{
    global $app;
    $cle = array_search($tableancien, $tab_tables);
    if ($cle && !in_array($table, $tab_tables)) {
        $app['db']->executeQuery("RENAME TABLE " . $tableancien . " TO " . $table);
        $tab_tables[$cle] = $table;
        return true;
    }
    return false;
}

function creer_table_archive($table, $tab_tables = array())
{
    global $app;
    $cle = array_search($table, $tab_tables);
    if (!is_bool($cle)) {
        $cle = array_search($table . '_archive', $tab_tables);
        if (!is_bool($cle)) {
//            echo "La table " . $table . "_archive existe" . PHP_EOL;
            return false;
        }
        $app['db']->executeQuery("CREATE TABLE " . $table . "_archive LIKE " . $table);
        $tab_tables[] = $table . '_archive';
        echo "Création de la table " . $table . "_archive" . PHP_EOL;
        return true;

    }
    echo "La table " . $table . " n'existe pas" . PHP_EOL;
    return false;
}

function enleve_autoincrement_table_archive($table, $colonne)
{
    global $app;
    $app['db']->executeQuery("ALTER TABLE " . $table . " CHANGE " . $colonne . " " . $colonne . " BIGINT( 21 ) NOT NULL DEFAULT 1 ");
    return true;
}

function truncate_table($table)
{
    global $app;
    $app['db']->executeQuery("TRUNCATE TABLE $table");
    echo "La table  " . $table . " a été vidé " . PHP_EOL;
    return true;
}

function modifier_autoincrement($table,$id=1){
    global $app;
    $app['db']->executeQuery("ALTER TABLE " . $table . " AUTO_INCREMENT =" . $id);
return true;
}

function table_max_id($table,$col){
    global $app;
    return  $app['db']->fetchColumn("SELECT MAX(".$col.") FROM  " . $table . ";")+0;

}

function changer_moteur($table)
{
    global $app;
    $app['db']->executeQuery("ALTER TABLE $table ENGINE='MyISAM'");
    echo "Le moteur de la table  " . $table . " a été changé en MyISAM " . PHP_EOL;
    return false;
}


function ajouter_index($table, $nom, $colonne)
{
    global $app;
    $sm = $app['db']->getSchemaManager();
    $indexes = $sm->listTableIndexes($table);
    if ($indexes) {
        $tab_index = array();
        foreach ($indexes as $index) {
            $tab_index[] = $index->getName();
        }
        if (array_search($nom, $tab_index) > '') {
            echo "L'index " . $nom . " ne peut etre créé il existe dans la table " . $table . PHP_EOL;
            return false;
        }
        $app['db']->executeQuery("ALTER TABLE " . $table . " ADD INDEX " . $nom . " ( " . $colonne . " ) ");
        echo "Création de l'index " . $nom . " pour la colonne " . $colonne . " de la table " . $table . PHP_EOL;
        return true;

    }
    echo "La table  " . $table . " n'existe pas impossible de creer l'index " . $nom . PHP_EOL;
    return false;
}


function ajouter_fulltext($table, $nom, $colonne)
{
    global $app;
    $sm = $app['db']->getSchemaManager();
    $indexes = $sm->listTableIndexes($table);
    if ($indexes) {
        $tab_index = array();
        foreach ($indexes as $index) {
            $tab_index[] = $index->getName();
        }
        if (array_search($nom, $tab_index) > '') {
            echo "L'index " . $nom . " ne peut etre créé il existe dans la table " . $table . PHP_EOL;
            return false;
        }
        echo("ALTER TABLE " . $table . " ADD FULLTEXT ( " . $colonne . " ) ");
        $app['db']->executeQuery("ALTER TABLE " . $table . " ADD " . $nom . " ( " . $colonne . " ) ");
        echo "Création de l'index " . $nom . " pour la colonne " . $colonne . " de la table " . $table . PHP_EOL;
        return true;

    }
    echo "La table  " . $table . " n'existe pas impossible de creer l'index " . $nom . PHP_EOL;
    return false;
}

function rename_index($table, $nom, $nom_ancien, $colonne)
{
    global $app;
    $sm = $app['db']->getSchemaManager();
    $indexes = $sm->listTableIndexes($table);
    if ($indexes) {
        $tab_index = array();
        foreach ($indexes as $index) {
            $tab_index[] = $index->getName();
        }
        if (array_search($nom_ancien, $tab_index) > '')
            execute_query("ALTER TABLE " . $table . " DROP INDEX " . $nom_ancien);
        else
            echo "L'index " . $nom_ancien . " ne peut etre renomé il n'existe pas dans la table " . $table . PHP_EOL;

    }
    $indexes = $sm->listTableIndexes($table);
    if ($indexes) {
        $tab_index = array();
        foreach ($indexes as $index) {
            $tab_index[] = $index->getName();
        }
        if (!array_search($nom, $tab_index))
            execute_query("ALTER TABLE " . $table . " ADD INDEX " . $nom . " ( " . $colonne . " ) ");
        else
            echo "L'index " . $nom . " ne peut etre crée pour la colonne " . $colonne . " de la table " . $table . ". il existe déja" . PHP_EOL;
    } else
        echo "La table  " . $table . " n'existe pas impossible de creer l'index " . $nom . PHP_EOL;
    return false;
}



function lister_table(){
    global $app;
    $sm = $app['db']->getSchemaManager();
    $tables = $sm->listTables();
    $tab_tables = array();
    foreach ($tables as $key => $table) {
        $tab_tables[] = $table->getName();
    }

    return $tab_tables;

}




function table_existe($table)
{
    $tab_tables=lister_table();
    return in_array($table, $tab_tables);

}



function colonne_existe($table, $colonne)
{
    global $app;
    $sm = $app['db']->getSchemaManager();
    if (table_existe($table)){
        $columns = $sm->listTableColumns($table);
        $tab_cols = array();
        foreach ($columns as $column) {
            $tab_cols[$column->getName()] = $column->getType();
        }

        return isset($tab_cols[$colonne]);
    }
    return false;
}




function ajoute_colonne($table, $colonne, $type)
{
    global $app;
    $sm = $app['db']->getSchemaManager();
    $columns = $sm->listTableColumns($table);
    $tab_cols = array();
    foreach ($columns as $column) {
        $tab_cols[$column->getName()] = $column->getType();
    }
    if (!isset($tab_cols[$colonne])) {
        $app['db']->executeQuery('ALTER TABLE ' . $table . ' ADD ' . $colonne . '  ' . $type);
        return true;

    }
    echo 'la colonne ' . $colonne . ' de ' . $table . ' existe' . PHP_EOL;
    return false;
}

function Transfert_supprime($table, $colonne)
{
    global $app;
    $sm = $app['db']->getSchemaManager();
    $columns = $sm->listTableColumns($table);
    $tab_cols = array();
    foreach ($columns as $column) {
        $tab_cols[$column->getName()] = $column->getType();
    }
    if (isset($tab_cols[$colonne])) {
        print_r('copy des enregistrements suppimés de ' . $table . PHP_EOL);
        $app['db']->executeQuery("INSERT INTO " . $table . "_archive SELECT * FROM " . $table . " WHERE supprime=1");
        $app['db']->executeQuery("DELETE " . $table . " FROM " . $table . " WHERE supprime=1");
        supprime_colonne($table, 'supprime');
        supprime_colonne($table . "_archive", 'supprime');

    }
    return true;
}

function ecrit_presta($table, $colonne, $valeur, $ecrit)
{
    global $app;
    $app['db']->executeQuery('UPDATE ' . $table . ' SET presta = "' . $ecrit . '" WHERE ' . $colonne . ' = "' . $valeur . '" ');
    return true;
}

function conversionType($type,$taille=''){
    if(strtolower($type)=='string'){
        $type = 'VARCHAR('.$taille.')';
    }
    return $type;
}

function change_notnull_colonne($table, $colonne,$notnull=false){
    global $app;
    $tab_cols = donne_liste_colonne($table);
    $sql= ($notnull)?'NOT NULL':'NULL';

    if (isset($tab_cols[$colonne]) )
        return $app['db']->executeQuery('ALTER TABLE ' . $table . ' CHANGE ' . $colonne . ' ' . $colonne . ' ' . conversionType($tab_cols[$colonne]['type'],$tab_cols[$colonne]['length']).' '.$sql);
    print_r('la colonne ' . $colonne . ' de ' . $table . ' n existe pas ' . PHP_EOL);
    return false;

}



function change_type_colonne($table, $colonne,  $type)
{
    global $app;
    $tab_cols = donne_liste_colonne($table);
    if (isset($tab_cols[$colonne]) )
        return $app['db']->executeQuery('ALTER TABLE ' . $table . ' CHANGE ' . $colonne . ' ' . $colonne . ' ' . $type);
    print_r('la colonne ' . $colonne . ' de ' . $table . ' n existe pas ' . PHP_EOL);
    return false;
}


function change_nom_colonne($table, $colonne, $new_colonne, $type)
{
    global $app;
    $tab_cols = donne_liste_colonne($table);
    if (isset($tab_cols[$colonne]) && !isset($tab_cols[$new_colonne]))
        $app['db']->executeQuery('ALTER TABLE ' . $table . ' CHANGE ' . $colonne . ' ' . $new_colonne . ' ' . $type);
    else print_r('la colonne ' . $colonne . ' de ' . $table . ' n existe pas ou la colonne ' . $new_colonne . ' existe déja' . PHP_EOL);
    return true;
}

function supprime_colonne($table, $colonne)
{
    global $app;
    $sm = $app['db']->getSchemaManager();
    $columns = $sm->listTableColumns($table);
    $tab_cols = array();
    foreach ($columns as $column) {
        $tab_cols[$column->getName()] = $column->getType();
    }
    if (isset($tab_cols[$colonne])) {
        $app['db']->executeQuery('ALTER TABLE ' . $table . ' DROP COLUMN ' . $colonne);
        return true;
    }
    print_r('la colonne ' . $colonne . ' de ' . $table . ' n\'existe pas' . PHP_EOL);
    return false;
}

