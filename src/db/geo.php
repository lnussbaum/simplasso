<?php
require_once($app['basepath'] . '/src/inc/variables.php');
require_once($app['basepath'] . '/src/inc/fonctions.php');
require_once($app['basepath'] . '/src/inc/fonctions_geo.php');

function telecharger_fichier_distant($source, $destination)
{
    try {
        $data = file_get_contents($source);
        file_put_contents($destination,$data);
        return true;
    } catch (Exception $e) {
        echo 'Caught exception: ', $e->getMessage(), "\n";
    }
    return false;
}

// todo sur la machine de claude la decompression ne se fait pas le fichier compressé est renommé comprésser avec tous les inconvénients pour la suite.
//todo Idem pour les autres fichiers téléchargés
function geo_telecharger_fichier_distant($source, $destination = 'truc.txt', $force_zip = false)
{

    global $app;
    $emplacement = $app['upload.path'] . '/geo';
    @mkdir($emplacement);
    $infos_fichier = pathinfo($source);
    $extension=(isset($infos_fichier['extension']))?$infos_fichier['extension']:'';

    $emplacement .= '/' . uniqid().(($force_zip||$extension == 'zip')?'.zip':$extension);
    if (telecharger_fichier_distant($source, $emplacement)) {

        $infos_fichier = pathinfo($emplacement);
        $extension=isset($infos_fichier['extension'])?$infos_fichier['extension']:'txt';
        if (file_exists($emplacement)) {

            // Si c'est un zip on l'extrait
            if ($force_zip or $extension == 'zip') {

                $archive = new PclZip($emplacement);
                $archive->extract($app['upload.path'] . '/geo/');
                $contenu = joindre_decrire_contenu_zip($archive);

                if (isset($contenu[0])) {
                    foreach ($contenu[0] as $f) {
                        if ($f['filename'] != "readme.txt") {
                            rename($app['upload.path'] . '/geo/' . $f['filename'],
                                $app['upload.path'] . '/geo/' . $destination);
                        }
                    }
                }
                unlink($emplacement);
            } else {
                rename($emplacement, $app['upload.path'] . '/geo/' . $destination);
            }


            return true;
        }
    }
    return false;
}


function joindre_decrire_contenu_zip($zip)
{

    // si pas possible de decompacter: installer comme fichier zip joint
    if (!$list = $zip->listContent()) {
        return false;
    }

    // Verifier si le contenu peut etre uploade (verif extension)
    $fichiers = array();
    $erreurs = array();
    foreach ($list as $file) {
        $f = $file['stored_filename'];
        $fichiers[$f] = $file;

    }

    ksort($fichiers);

    return array($fichiers, $erreurs);
}


function geo_commune()
{
    global $app;

    $fichier = "comsimp2017.csv";
    $chemin_fichier = $app['upload.path'] . '/geo/' . $fichier;
    if (!file_exists($chemin_fichier)) {
        geo_telecharger_fichier_distant("https://www.insee.fr/fr/statistiques/fichier/2666684/comsimp2017-txt.zip",
            $fichier);
    }
    CommuneQuery::create()->deleteAll();
    $nb_ligne = 0;
    $pointeur_fichier = fopen($app['upload.path'] . '/geo/' . $fichier, "r");
    if ($pointeur_fichier <> 0) {
        fgets($pointeur_fichier, 4096);
        $nb_ligne = 0;
        while (!feof($pointeur_fichier)) {
            $ligne = fgets($pointeur_fichier, 4096);
            $tab = explode("\t", $ligne);
            if (count($tab) > 1) {
                $article = $tab[10];
                if (!empty($article)) {
                    if (substr($article, 0, 1) == '(') {
                        $article = substr($article, 1);
                    }
                    if (substr($article, -1, 1) == ')') {
                        $article = substr($article, 0, -1);
                    }
                    if (substr($article, -1, 1) != '\'') {
                        $article .= ' ';
                    }
                }

                $el = new Commune();
                $el->setPays('FR');
                $el->setNom(trim(mb_convert_encoding($tab[11],'UTF8','Windows-1252')));
                $el->setCode($tab[4]);
                $el->setRegion($tab[2]);
                $el->setDepartement($tab[3]);
                $el->setArrondissement($tab[5]);
                $el->setCanton($tab[6]);
                $el->setTypeCharniere($tab[7]);
                $el->setArticle($article);
                $el->save();
                $nb_ligne++;
            }
        }
    }
    echo($nb_ligne . ' communes importées' . PHP_EOL);
    //


}

function geo_arrondissement()
{
    global $app;
    $nb_ligne = 0;
    $fichier = 'arrond2017.csv';
    $chemin_fichier = $app['upload.path'] . '/geo/' . $fichier;
    if (!file_exists($chemin_fichier)) {
        geo_telecharger_fichier_distant("https://www.insee.fr/fr/statistiques/fichier/2666684/arrond2017-txt.zip",
        $fichier);
    }
    ArrondissementQuery::create()->deleteAll();

    $pointeur_fichier = fopen($app['upload.path'] . '/geo/' . $fichier, "r");
    if ($pointeur_fichier <> 0) {
        $ligne = fgets($pointeur_fichier, 4096);
        $nb_ligne = 0;
        while (!feof($pointeur_fichier)) {
            $ligne = fgets($pointeur_fichier, 4096);
            $tab = explode("\t", $ligne);

            if (count($tab) > 1) {
                $el = new Arrondissement();
                $el->setPays('FR');
                $el->setNom(utf8_encode($tab[6]));
                $el->setCode($tab[2]);
                $el->setRegion($tab[0]);
                $el->setDepartement($tab[1]);
                $el->setChefLieu($tab[3]);
                $el->setTypeCharniere($tab[4]);
                $el->save();
                $nb_ligne++;
            }

        }
    }
    echo($nb_ligne . ' arrondissements importés' . PHP_EOL);
    //Pour renseigner la zone geo
    //geo_telecharger_fichier_distant("https://www.data.gouv.fr/s/resources/projet-de-redecoupages-des-regions/20150302-231345/regions-2016-json-simplifie.zip",'region.zip');
    //regions-2016-simplifie.json


}


function geo_departement()
{
    global $app;
    $nb_ligne = 0;
    $fichier = 'depts2017.csv';
    $chemin_fichier = $app['upload.path'] . '/geo/' . $fichier;

    if (!file_exists($chemin_fichier)) {
        geo_telecharger_fichier_distant("https://www.insee.fr/fr/statistiques/fichier/2666684/depts2017-txt.zip", $fichier);
    }
    DepartementQuery::create()->deleteAll();

    $pointeur_fichier = fopen($app['upload.path'] . '/geo/' . $fichier, "r");
    if ($pointeur_fichier) {
        $ligne = fgets($pointeur_fichier, 4096);
        $nb_ligne = 0;
        while (!feof($pointeur_fichier)) {
            $ligne = utf8_encode(fgets($pointeur_fichier, 4096));
            $tab = explode("\t", $ligne);
            if (count($tab) > 1) {
                $el = new Departement();
                $el->setPays('FR');
                $el->setNom($tab[5]);
                $el->setCode($tab[1]);
                $el->setRegion($tab[0]);
                $el->setChefLieu($tab[2]);
                $el->setTypeCharniere($tab[3]);
                $el->save();
                $nb_ligne++;
            }

        }
    }
    echo($nb_ligne . ' départements importés' . PHP_EOL);


}


function geo_region()
{

    global $app;
    $nb_ligne = 0;
    $fichier = 'reg2017.csv';
    $chemin_fichier = $app['upload.path'] . '/geo/' . $fichier;
    if (!file_exists($chemin_fichier)) {
        geo_telecharger_fichier_distant("https://www.insee.fr/fr/statistiques/fichier/2666684/reg2017-txt.zip",
           $fichier);
    }
    RegionQuery::create()->deleteAll();
    $pointeur_fichier = fopen($app['upload.path'] . '/geo/' . $fichier, "r");
    if ($pointeur_fichier <> 0) {
        $ligne = fgets($pointeur_fichier, 4096);
        $nb_ligne = 0;
        while (!feof($pointeur_fichier)) {
            $ligne = utf8_encode(fgets($pointeur_fichier, 4096));
            $tab = explode("\t", $ligne);

            if (count($tab) > 1) {
                $el = new Region();
                $el->setPays('FR');
                $el->setNom($tab[4]);
                $el->setCode($tab[0]);
                $el->setChefLieu($tab[1]);
                $el->setTypeCharniere($tab[2]);
                $el->save();
                $nb_ligne++;
            }

        }
    }
    echo($nb_ligne . ' régions importées' . PHP_EOL);

    //Pour renseigner la zone geo
    //geo_telecharger_fichier_distant("https://www.data.gouv.fr/s/resources/projet-de-redecoupages-des-regions/20150302-231345/regions-2016-json-simplifie.zip",'region.zip');
    //regions-2016-simplifie.json

}


function geo_cp()
{
    global $app;
    $nb_ligne = 0;
    $fichier = "cp2017.csv";
    $chemin_fichier = $app['upload.path'] . '/geo/' . $fichier;
    if (!file_exists($chemin_fichier)) {
        geo_telecharger_fichier_distant("http://datanova.legroupe.laposte.fr/explore/dataset/laposte_hexasmal/download/?format=csv&timezone=Europe/Berlin&use_labels_for_header=true",
            $fichier,false);
    }
    $nb_ligne = 0;
    truncate_table('geo_codespostaux');
    CommuneLienQuery::create()->filterByObjet('codespostaux')->find();

    $pointeur_fichier = fopen($chemin_fichier, "r");
    if ($pointeur_fichier) {
        $ligne = fgets($pointeur_fichier, 4096);

        $nb_ligne = 0;
        while (!feof($pointeur_fichier)) {
            $ligne = utf8_encode(fgets($pointeur_fichier, 4096));
            $tab = explode(";", $ligne);

            if (count($tab) > 1) {
                $el = new Codespostaux();
                $el->setPays('FR');
                $el->setNom($tab[3]);
                $el->setNomSuite($tab[4]);
                $el->setCodepostal($tab[2]);
                $el->save();

                $commune = CommuneQuery::create()->filterByPays('FR')->filterByDepartement(substr($tab[0], 0, 2))->filterByCode(substr($tab[0], 2))->findOne();

                if ($commune) {
                    if (!CommuneLienQuery::create()->filterByObjet('codespostaux')->filterByIdObjet($el->getPrimaryKey())->filterByIdCommune($commune->getPrimaryKey())->exists()) {
                        $lien = new CommuneLien();
                        $lien->setIdObjet($el->getPrimaryKey());
                        $lien->setObjet('codespostaux');
                        $lien->setIdCommune($commune->getPrimaryKey());
                        $lien->save();
                    }
                }
                $nb_ligne++;
            }

        }
    }
    echo($nb_ligne . ' codes postaux importés' . PHP_EOL);


}


function lier_commune_individu()
{

    $tab_ind = IndividuQuery::create()->find();
    CommuneLienQuery::create()->filterByObjet('individu')->delete();
    $tab_ville=[];
    foreach ($tab_ind as $ind) {
        $ville = $ind->getVille();
        if (empty($ville)) {
            continue;
        }
        list($tab_ville,$id_commune) = traitement_rechercher_commune($tab_ville,$ville,$ind->getCodepostal());
        if (!empty($id_commune)) {
            $cl = new CommuneLien();

            $cl->setIdObjet($ind->getPrimaryKey());
            $cl->setObjet('individu');
            $cl->setIdCommune($id_commune);
            $cl->save();
        }
    }


}