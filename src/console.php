<?php

use Propel\Runtime\ActiveQuery\Criteria;
use Symfony\Component\Console\Application;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Output\OutputInterface;
use Symfony\Component\Filesystem\Filesystem;
use Symfony\Component\Finder\Finder;


$console = new Application($app['name'], $app['version']);

$app->boot();

if ($app['debug']) {
    $con = \Propel\Runtime\Propel::getServiceContainer()->getConnection();
    $con->useDebug(true);
}

if (isset($app['cache.path'])) {
    $console
        ->register('cache:clear')
        ->setDescription('Clears the cache')
        ->setCode(function (InputInterface $input, OutputInterface $output) use ($app) {

            $cacheDir = $app['cache.path'];
            $finder = Finder::create()->in($cacheDir)->notName('.gitkeep');

            $filesystem = new Filesystem();
            $filesystem->remove($finder);

            $output->writeln(sprintf("%s <info>success</info>", 'cache:clear'));
        });
}


$console
    ->register('base:affichage')
    ->setDescription('Afficher le schema de la base de données')
    ->setCode(function (InputInterface $input, OutputInterface $output) use ($app) {
        $schema = require __DIR__ . '/../src/db/schema.php';

        foreach ($schema->toSql($app['db']->getDatabasePlatform()) as $sql) {
            $output->writeln($sql . ';');
        }
    });

$console
    ->register('base:creation')
    ->setDescription('Créer la base de données')
    ->setCode(function (InputInterface $input, OutputInterface $output) use ($app) {
        $schema = require __DIR__ . '/../src/db/schema.php';

        foreach ($schema->toSql($app['db']->getDatabasePlatform()) as $sql) {
            $app['db']->exec($sql . ';');
        }
    });


$console
    ->register('base:migration')
    ->setDescription('Migrer la base de données')
    ->setHelp(
        <<<EOT
La commande <info>migration_base</info> migre la base de données vers la version la plus rescente


EOT
    )
    ->setCode(function (InputInterface $input, OutputInterface $output) use ($app) {
        require __DIR__ . '/../src/db/base_migration.php';
        migration_base();


    });


$console
    ->register('base:collation')
    ->setDescription('Changer la base de données')
    ->setHelp(
        <<<EOT
La commande <info>migration_base</info> migre la base de données vers la version la plus rescente


EOT
    )
    ->setCode(function (InputInterface $input, OutputInterface $output) use ($app) {
        require __DIR__ . '/../src/db/base_migration.php';

        $database_name = $app['db.options']['new']['dbname'];

        $app['db']->exec("ALTER DATABASE `$database_name` CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci");
        $tab_req = $app['db']->fetchAll("SELECT concat('ALTER TABLE `', TABLE_SCHEMA, '`.`', table_name, '` CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci;') as req from information_schema.tables where TABLE_SCHEMA = '" . $database_name . "'");
        foreach ($tab_req as $req) {
            $app['db']->exec($req['req']);
        }

        $tab_req = $app['db']->fetchAll("SELECT concat('ALTER TABLE `', t1.TABLE_SCHEMA, '`.`', t1.table_name, '` MODIFY `', t1.column_name, '` ', t1.data_type , '(' , t1.CHARACTER_MAXIMUM_LENGTH , ')' , ' CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci;')as req from information_schema.columns t1 where t1.data_type<>'longtext' and t1.TABLE_SCHEMA = '" . $database_name . "' and (t1.COLLATION_NAME = 'latin1_swedish_ci' or t1.COLLATION_NAME = 'utf8_general_ci');");
        foreach ($tab_req as $req) {
            echo($req['req']);
            $app['db']->exec($req['req']);
        }
        $tab_req = $app['db']->fetchAll("SELECT concat('ALTER TABLE `', t1.TABLE_SCHEMA, '`.`', t1.table_name, '` MODIFY `', t1.column_name, '` ', t1.data_type  , ' CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci;')as req from information_schema.columns t1 where t1.data_type='longtext' and t1.TABLE_SCHEMA = '" . $database_name . "' and (t1.COLLATION_NAME = 'latin1_swedish_ci' or t1.COLLATION_NAME = 'utf8_general_ci');");
        foreach ($tab_req as $req) {
            echo($req['req']);
            $app['db']->exec($req['req']);
        }

    });


$console
    ->register('geo:individu')
    ->setDescription('Géoréférencer les individus')
    ->setHelp(
        <<<EOT
La commande <info>geo:cp</info> géoréférencer les individus et les membres à partir de l'adresse postal de l'individu titulaire

>
EOT
    )
    ->setCode(function (InputInterface $input, OutputInterface $output) use ($app) {
        require __DIR__ . '/../src/geo/position.php';
        georefenrer_individu();

    });

$console
    ->register('geo:membre')
    ->setDescription('Géoréférencer les membres')
    ->setHelp(
        <<<EOT
La commande <info>geo:membre</info> géoréférencer les membres à partir de l'adresse postale de l'individu titulaire

>
EOT
    )
    ->setCode(function (InputInterface $input, OutputInterface $output) use ($app) {
        require __DIR__ . '/../src/geo/position.php';
        georefenrer_membre();

    });
$console
    ->register('geo:entite')
    ->setDescription('Géoréférencer les entites')
    ->setHelp(
        <<<EOT
La commande <info>geo:entite</info> géoréférencer les entites à partir de l'adresse postale des entités

>
EOT
    )
    ->setCode(function (InputInterface $input, OutputInterface $output) use ($app) {
        require __DIR__ . '/../src/geo/position.php';
        georefenrer_entite();

    });


$console
    ->register('geo:cp')
    ->setDescription('Importer les codes postaux')
    ->setHelp(
        <<<EOT
La commande <info>geo:cp</info> télécharge la liste des codes postaux dans votre base de données

>
EOT
    )
    ->setCode(function (InputInterface $input, OutputInterface $output) use ($app) {
        require __DIR__ . '/../src/db/geo.php';
        require __DIR__ . '/../src/db/base_migration_fonctions.php';
        geo_cp();

    });


$console
    ->register('geo:cog')
    ->setDescription('Importer les communes, départements, régions et arrondissements français')
    ->setHelp(
        <<<EOT
La commande <info>geo:cog</info> télécharge la liste des communes,regions, départements et arrondissements français dans votre base de données

>
EOT
    )
    ->setCode(function (InputInterface $input, OutputInterface $output) use ($app) {
        require __DIR__ . '/../src/db/geo.php';
        require __DIR__ . '/../src/db/base_migration_fonctions.php';
        truncate_table('geo_departements');
        truncate_table('geo_regions');
        truncate_table('geo_arrondissements');
        truncate_table('geo_communes');
        geo_departement();
        geo_region();
        geo_arrondissement();
        geo_commune();

    });



$console
    ->register('geo:liaison_commune')
    ->setDescription('Lier les individus aux communes')
    ->setHelp("<<<EOT   La commande <info>geo:cp</info> géoréférencer les individus et les membres à partir de l'adresse postal de l'individu titulaire> EOT ")
    ->setCode(function (InputInterface $input, OutputInterface $output) use ($app) {
        require __DIR__ . '/../src/db/geo.php';
        lier_commune_individu();

    });



$console
    ->register('lang:objets')
    ->setDescription('Prémacher un fichiers de traductions des objets')
    ->setCode(function (InputInterface $input, OutputInterface $output) use ($app) {
        $tab_objet = array_keys(liste_des_objets());
        ksort($tab_objet);


        $tab_trad = array(
            '@objet@ :  @objett@',
            '@Objet@ :  @Objett@',
            '@objet@s :  @objett@@s@',
            '@Objet@s :  @Objett@@s@',
            '@objet@_form :  Création/Modification d\'@un@ @objett@',
            '@objet@_export :  Options d\'exportation des @objett@s',
            '@objet@_import :  Options d\'importation  des @objett@s',
            '@objet@_liste :  Liste des @objett@s',
            '@objet@_numero :  Numéro @de@ @objett@',
            '@objet@_nouveau :  Nouv@eau@ @objett@',
            '@objet@_ajouter :  Ajout d\'@un@ @objett@',
            '@objet@_ajouter_ok :  La création @du@@objett@ est réalisée',
            '@objet@_modifier :  Modification d\'@un@ @objett@',
            '@objet@_modifier_ok :  La modification @du@@objett@ est réalisée',
            '@objet@_supprimer :  Supprimer la fiche @du@ @objett@',
            '@objet@_retour :  Retour @au@@objett@',
            'LOG_@objet3@CRE :  Création d\'@un@ @objett@ %url_@objett@%',
            'LOG_@objet3@MOD :  Modification d\'@un@ @objett@ %url_@objett@%',
            'LOG_@objet3@SUP :  Supression d\'@un@ @objett@ %url_@objett@%',
            'LOL_@objet3@CRE :  Création de @objett@s %url_@objett@%',
            'LOL_@objet3@MOD :  Modification de @objett@s %url_@objett@%',
            'LOL_@objet3@SUP :  Supression de @objett@s %url_@objett@%',

        );

        $chaine = "";

        foreach ($tab_objet as $o) {
            $mots = explode('_', $o);
            $x = substr($mots[0], -1) == 'x';
            $feminin = substr($mots[0], -1) == 'e';
            $voyelle = in_array(substr($o, 0, 1), ['a', 'e', 'é', 'u', 'i', 'o', 'h', 'y']);


            $s = ($x) ? '' : 's';
            $un = ($feminin) ? 'une' : 'un';
            $le = ($feminin) ? 'la' : 'le';
            $le = ($voyelle) ? 'l\'' : $le;
            $cet = ($feminin) ? 'cette' : 'cet';
            $du = ($feminin) ? 'de la ' : 'du ';
            $du = ($voyelle) ? 'de l\'' : $du;
            $de = ($voyelle) ? 'd\'' : 'de ';
            $au = ($feminin) ? 'à la ' : 'au ';
            $au = ($voyelle) ? 'à l\'' : $au;
            $eau = ($feminin) ? 'elle' : 'el';
            $eau = ($voyelle) ? 'eau' : $eau;

            foreach ($tab_trad as $trad) {


                $tmp = str_replace(['@objet@', '@Objet@', '@objett@', '@Objett@', '@objet3@'], [$o, ucfirst($o), str_replace('_', ' ', $o), str_replace('_', ' ', ucfirst($o)), substr(strtoupper($o),0,3)], $trad) . PHP_EOL;

                $chaine .= str_replace(['@un@', '@le@', '@cet@', '@du@', '@de@', '@au@', '@el@', '@s@'], [$un, $le, $cet, $du, $de, $au, $eau, $s], $tmp);


            }
            $chaine .= PHP_EOL . PHP_EOL;
            echo("$o préparé" . PHP_EOL);
        }


        file_put_contents($app['tmp.path'] . '/lang_objet.txt', $chaine);

    });


$console
    ->register('generate:objet_liste')
    ->setDescription('Prémacher l\'écriture des listes')
    ->setCode(function (InputInterface $input, OutputInterface $output) use ($app) {
        require_once($app['basepath'] . '/src/db/base_migration_fonctions.php');
        $tab_objet = description_complete();
        @mkdir($app['tmp.path'] . '/generate/');
        @mkdir($app['tmp.path'] . '/generate/php/');
        @mkdir($app['tmp.path'] . '/generate/php/objet_liste/');

        foreach ($tab_objet as $nom => $objet) {
            $chaine = file_get_contents($app['resources.path'] . '/skeleton/objet_liste.php');
            $champs = "";
            $tab_champs = donne_liste_colonne($objet['table_sql']);
            $tab_type = array();

            foreach ($tab_champs as $n => $c) {

                $nom_champs = $n;
                $type = 'TextType';
                $class = array();
                $options = array();
                if (!(in_array($n, ['updated_at']))) {


                    switch ($n) {
                        case $objet['cle_sql']:
                            $options['title'] = 'id';
                            break;

                    }
                    if (substr($n, 0, 5) == 'date_') {
                        $options['type'] = 'date-eu';
                    }

                    if (colonneEstInteger($objet['table_sql'], $n) && substr($n, 0, 3) == 'id_') {


                    }

                    $tmp = [];
                    foreach ($options as $k => $v) {
                        $tmp[] = "'$k'=> '$v',";
                    }

                    $champs .= "\t" . '$tab_colonne[\'' . $nom_champs . '\'] = [' . implode(',', $tmp) . '];' . PHP_EOL;
                }
            }


            $chaine = str_replace('##OBJET##', $nom, $chaine);
            $chaine = str_replace('##CLASS##', $objet['phpname'], $chaine);
            $chaine = str_replace('##COLONNES##', $champs, $chaine);

            file_put_contents($app['tmp.path'] . '/generate/php/objet_liste/' . $nom . '_liste' . '.php', $chaine);
            echo("$nom préparé" . PHP_EOL);

        }


    });


$console
    ->register('generate:formtype')
    ->setDescription('Prémacher l\'écriture des formulaires')
    ->setCode(function (InputInterface $input, OutputInterface $output) use ($app) {
        require_once($app['basepath'] . '/src/db/base_migration_fonctions.php');
        $tab_objet = description_complete();
        @mkdir($app['tmp.path'] . '/generate/');
        @mkdir($app['tmp.path'] . '/generate/php/');
        @mkdir($app['tmp.path'] . '/generate/php/formtype/');

        foreach ($tab_objet as $nom => $objet) {
            $chaine = file_get_contents($app['resources.path'] . '/skeleton/formtype.php');
            $champs = "";
            $tab_champs = donne_liste_colonne($objet['table_sql']);
            $tab_type = array();

            foreach ($tab_champs as $n => $c) {

                $nom_champs = $n;
                $type = 'TextType';
                $class = array();
                $options = array();
                if (!(in_array($n, ['created_at', 'updated_at']) or $n == $objet['cle_sql'])) {


                    switch ($n) {
                        case 'observation':
                            $type = 'TextAreaType';
                            break;

                    }
                    if (substr($n, 0, 5) == 'date_') {

                        $type = 'DateType';
                        $class[] = 'datepickerb';
                        $options['widget'] = 'single_text';
                        $options['format'] = 'dd/MM/yyyy';
                    }

                    if (colonneEstInteger($objet['table_sql'], $n) && substr($n, 0, 3) == 'id_') {
                        $type = 'ChoiceType';
                        $options['data'] = '$tab_' . $n;
                        $options['expanded'] = 'false';
                        $options['multiple'] = 'false';
                    }

                    $tmp = "";
                    foreach ($options as $k => $v) {
                        $tmp .= "'$k'=> '$v',";
                    }

                    $options = "array('label' => '" . ucfirst($n) . "',$tmp 'attr' => ['class'=>'" . implode(' ', $class) . "'])";
                    $tab_type[$type] = true;
                    $champs .= PHP_EOL . "\t\t\t" . '$builder->add(\'' . $nom_champs . '\', ' . $type . '::CLASS,' . $options . ');';
                }
            }

            $use = "";
            foreach (array_keys($tab_type) as $type) {
                $use .= 'use Symfony\Component\Form\Extension\Core\Type\\' . $type . ';' . PHP_EOL;

            }


            $chaine = str_replace('##CLASS##', $objet['phpname'] . 'Form', $chaine);
            $chaine = str_replace('##TYPE_NAME##', $nom, $chaine);
            $chaine = str_replace('##FQCN##', $nom, $chaine);
            $chaine = str_replace('##USE_TYPE##', $use, $chaine);
            $chaine = str_replace('##BUILD_CODE##', $champs, $chaine);

            file_put_contents($app['tmp.path'] . '/generate/php/formtype/' . $objet['phpname'] . 'Form' . '.php', $chaine);
            echo("$nom préparé" . PHP_EOL);

        }


    });


$console
    ->register('generate:form')
    ->setDescription('Prémacher l\'écriture des formulaires')
    ->setCode(function (InputInterface $input, OutputInterface $output) use ($app) {
        require_once($app['basepath'] . '/src/db/base_migration_fonctions.php');
        $tab_objet = description_complete();
        @mkdir($app['tmp.path'] . '/generate/');
        @mkdir($app['tmp.path'] . '/generate/php/');
        @mkdir($app['tmp.path'] . '/generate/php/form/');

        foreach ($tab_objet as $nom => $objet) {
            $chaine = file_get_contents($app['resources.path'] . '/skeleton/form.php');
            $champs = "";
            $chaine = str_replace('##OBJET##', $nom, $chaine);
            $chaine = str_replace('##TYPE_NAME##', $nom, $chaine);
            $chaine = str_replace('##PROPEL_OBJET##', $objet['phpname'], $chaine);
            $chaine = str_replace('##FIELDS##', $objet['phpname'] . 'Form', $chaine);


            file_put_contents($app['tmp.path'] . '/generate/php/form/' . $nom . '_form' . '.php', $chaine);
            echo("$nom préparé" . PHP_EOL);

        }


    });


$console
    ->register('com:synchro_infolettre')
    ->setDescription('Synchroniser la liste des inforlettres à partir l\'API ovh')
    ->setCode(function (InputInterface $input, OutputInterface $output) use ($app) {


        if ($app['apiovh']) {
            $nb = 0;
            $tab_infolettre = InfolettreQuery::create()->find();
            $tab_il = array();
            foreach ($tab_infolettre as $il) {
                $tab_il[$il->getIdentifiant()] = $il;

            }
            $tab_infolettre = $app['API_ovh']->get('/email/domain/' . $app['apiovh_infolettre_domaine'] . '/mailingList');
            foreach ($tab_infolettre as $id) {
                $info_infolettre = $app['API_ovh']->get('/email/domain/' . $app['apiovh_infolettre_domaine'] . '/mailingList/' . $id);

                if (isset($tab_il[$info_infolettre['id']])) {
                    $infolettre = $tab_il[$info_infolettre['id']];
                } else {
                    $infolettre = new Infolettres();
                }
                $infolettre->setNom($info_infolettre['name']);
                $infolettre->setIdentifiant($info_infolettre['id']);
                $infolettre->save();


                $nb++;

            }
            echo("$nb infolettres synchronisées" . PHP_EOL);


        }

    });


$console
    ->register('com:synchro_infolettre_inscrit_ovh')
    ->setDescription('Synchroniser la liste des inscrits aux infolettre à partir l\'API ovh')
    ->setCode(function (InputInterface $input, OutputInterface $output) use ($app) {
        if ($app['apiovh']) {
            require_once($app['basepath'] . '/src/db/base_migration_fonctions.php');
            truncate_table('com_infolettres_inscrits');
            $nb = 0;
            $tab_infolettre = InfolettreQuery::create()->find();
            $tab_il = array();
            foreach ($tab_infolettre as $il) {
                $tab_il[$il->getNom()] = $il->getPrimaryKey();

            }
            $tab_infolettre = $app['API_ovh']->get('/email/domain/' . $app['apiovh_infolettre_domaine'] . '/mailingList');
            foreach ($tab_infolettre as $id) {
                //$info_infolettre=$app['API_ovh']->get('/email/domain/'.$app['apiovh_infolettre_domaine'].'/mailingList/'.$id);
                $tab_inscrit = $app['API_ovh']->get('/email/domain/' . $app['apiovh_infolettre_domaine'] . '/mailingList/' . $id . '/subscriber');

                foreach ($tab_inscrit as $email) {
                    $inscrit = new InfolettreInscrit();
                    $inscrit->setEmail($email);
                    $inscrit->setIdInfolettre($tab_il[$id]);
                    $inscrit->save();
                    $nb++;
                }

            }
            echo("$nb infolettres synchronisées" . PHP_EOL);


        }

    });


$console
    ->register('com:synchro_infolettre_inscrit_spip')
    ->setDescription('Synchroniser la liste des inscrits à SPIP')
    ->setCode(function (InputInterface $input, OutputInterface $output) use ($app) {
        if ($app['API_spip']) {
            require_once($app['basepath'] . '/src/db/base_migration_fonctions.php');
            truncate_table('com_infolettres_inscrits');
            $nb = 0;
            $tab_infolettre = InfolettreQuery::create()->find();
            $tab_il = array();
            foreach ($tab_infolettre as $il) {
                $tab_il[substr($il->getIdentifiant(), 5)] = $il->getPrimaryKey();

            }
            $tab_infolettre = $app['API_spip']->newsletter_inscrits();
            var_dump($tab_infolettre);
            foreach ($tab_infolettre as $id => $tab_inscrit) {

                foreach ($tab_inscrit as $email) {
                    $inscrit = new InfolettreInscrit();
                    $inscrit->setEmail($email);
                    $inscrit->setIdInfolettre($tab_il[$id]);
                    $inscrit->save();
                    $nb++;
                }

            }
            echo("$nb infolettres synchronisées" . PHP_EOL);


        }

    });


$console
    ->register('entretien:login')
    ->setDescription('')
    ->setCode(function (InputInterface $input, OutputInterface $output) use ($app) {
        $tab_individus = IndividuQuery::create()->filterByLogin(null, Criteria::ISNULL)->find();
        $nb = 0;
        foreach ($tab_individus as $ind) {
            $login = IndividuQuery::donnerLogin($ind->getNom(), $ind->getPrenom());
            $ind->setLogin($login);
            $ind->save();
            $nb++;
        }
        echo($nb . ' individu de plus ont maintenant un login' . PHP_EOL);
    });


function serial2json($table, $champs_id, $champs_var)
{
    global $app;

    $tab = $app['db']->fetchAll('SELECT ' . $champs_id . ' as id, ' . $champs_var . ' as vars FROM ' . $table);
    foreach ($tab as $t) {
        $app['db']->update($table, [$champs_var => json_encode(unserialize($t['vars']))], [$champs_id => $t['id']]);
    }


}


$console
    ->register('base:serial2json')
    ->setDescription('')
    ->setCode(function (InputInterface $input, OutputInterface $output) use ($app) {

        serial2json('asso_logs', 'id_log', 'variables');
        serial2json('asso_taches', 'id_tache', 'args');
        serial2json('asso_motgroupes', 'id_motgroupe', 'options');
        serial2json('asso_courriers', 'id_courrier', 'variables');
        serial2json('asso_imports', 'id_import', 'modifications');
        serial2json('asso_imports', 'id_import', 'propositions');
        serial2json('asso_configs', 'id_config', 'variables');

    });
// todo si oubli dans la mutation d'une base Fait yoganpdc en placant ce code en debut de fenetre paiement_form
//    $tab = $app['db']->fetchAll('SELECT  id_log  as id,  variables as vars FROM asso_logs where id_log<4633');
//    foreach ($tab as $t) {
//        $app['db']->update('asso_logs', ['variables' => json_encode(unserialize($t['vars']))], ['id_log' => $t['id']]);
//    }
//    echo 'fait asso_logs < 4633';
//    $tab = $app['db']->fetchAll('SELECT  id_motgroupe  as id,  options as vars FROM asso_motgroupes where id_motgroupe<3');
//    foreach ($tab as $t) {
//        $app['db']->update('asso_motgroupes', ['options' => json_encode(unserialize($t['vars']))], ['id_motgroupe' => $t['id']]);
//    }
//    echo 'fait asso_motgroupes < 3';
// exit;


$console
    ->register('base:reparer_mot_systeme')
    ->setDescription('')
    ->setCode(function (InputInterface $input, OutputInterface $output) use ($app) {

        $app['db']->getDatabasePlatform()->registerDoctrineTypeMapping('enum', 'string');
        $mg = MotgroupeQuery::create()->findOneByNomcourt('systeme');
        if (empty($mg))
            $mg = new Motgroupe();

        $mg->setNom('Système');
        $mg->setNomcourt('systeme');
        $mg->setSysteme(true);
        $mg->setobjetsEnLien('membre;individu');
        $mg->setImportance('1');
        $mg->setIdParent(0);
        $mg->save();

        $mg0 = MotgroupeQuery::create()->findOneByNomcourt('npai');
        if (empty($mg0))
            $mg0 = new Motgroupe();

        $mg0->setNom('Changement coordonnées');
        $mg0->setNomcourt('npai');
        $mg0->setSysteme(true);
        $mg0->setobjetsEnLien('individu');
        $mg0->setImportance('1');
        $mg0->setIdParent($mg->getPrimaryKey());
        $mg0->save();


        $tab_mot = ['npai_adres' => 'Adresse non valide', 'npai_email' => 'Email non valide', 'npai_telep' => 'Téléphone non valide', 'npai_tele2' => 'Téléphone pro non valide', 'npai_mobil' => 'Mobile non valide'];
        foreach ($tab_mot as $k => $mot) {
            $m = MotQuery::create()->findOneByNomcourt($k);
            if (empty($m))
                $m = new Mot();
            $m->setNom($mot);
            $m->setNomcourt($k);
            $m->setIdMotgroupe($mg0->getPrimaryKey());
            $m->setImportance('1');
            $m->save();
        }


        $mg0 = MotgroupeQuery::create()->findOneByNomcourt('carte');
        if (empty($mg0))
            $mg0 = new Motgroupe();
        $mg0->setNom('Carte');
        $mg0->setNomcourt('carte');
        $mg0->setSysteme(true);
        $mg0->setobjetsEnLien('membre;individu');
        $mg0->setImportance('1');
        $mg0->setIdParent($mg->getPrimaryKey());
        $mg0->save();

        $tab_mot = ['Carte à imprimer', 'Carte à remettre', 'Carte à envoyer', 'Carte d\'adherent OK'];
        foreach ($tab_mot as $k => $mot) {
            $m = MotQuery::create()->findOneByNomcourt($k);
            if (empty($m))
                $m = new Mot();
            $m->setNom($mot);
            $m->setNomcourt('carte_adh' . ($k + 1));
            $m->setIdMotgroupe($mg0->getPrimaryKey());
            $m->setImportance('1');
            $m->save();
        }

        $mg0 = MotgroupeQuery::create()->findOneByNomcourt('individu');
        if (empty($mg0))
            $mg0 = new Motgroupe();
        $mg0->setNom('Individu');
        $mg0->setNomcourt('individu');
        $mg0->setSysteme(true);
        $mg0->setobjetsEnLien('individu');
        $mg0->setImportance('1');
        $mg0->setIdParent($mg->getPrimaryKey());
        $mg0->save();

        $tab_mot = ['dcd'=>'Décédé'];
        foreach ($tab_mot as $k => $mot) {
            $m = MotQuery::create()->findOneByNomcourt($k);
            if (empty($m))
                $m = new Mot();
            $m->setNom($mot);
            $m->setNomcourt('ind_dcd' . $k);
            $m->setIdMotgroupe($mg0->getPrimaryKey());
            $m->setImportance('1');
            $m->save();
        }


    });


return $console;
