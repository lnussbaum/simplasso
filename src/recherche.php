<?php



function recherche(){


    global $app;

    $request=$app['request'];
    $search=array('value'=>$request->get('recherche'));


    list($sous_requete, $nb_total_membre) = getSelectionObjetNb('membre',array('search'=>$search));
    $tab_membre = MembreQuery::create()->where('id_membre IN ('.$sous_requete.')')->limit(20)->find();


    list($sous_requete, $nb_total_individu) = getSelectionObjetNb('individu',array('search'=>$search));
    $tab_individu = IndividuQuery::create()->where('id_individu IN ('.$sous_requete.')')->limit(20)->find();

    $tab_courrier=array();
    $nb_total_courrier=0;

    $args_twig=array(
        'nb_membre'=>$nb_total_membre,
        'nb_individu'=>$nb_total_individu,
        'nb_courrier'=>$nb_total_courrier,
        'tab_membre'=>$tab_membre,
        'tab_individu'=>$tab_individu,
        'tab_courrier'=>$tab_courrier
    );
    return $app['twig']->render(fichier_twig(), $args_twig);





}
function action_recherche_json()
{


    global $app;



    $tab_data = array();

    $sous_requete = getSelectionObjet('membre');
    $tab_membre = MembreQuery::create()->where('id_membre IN ('.$sous_requete.')')->limit(10)->find();

    foreach ($tab_membre as $membre) {

        $tab_data[] = array(
            'objet'=>'membre',
            'id'=>$membre->getPrimaryKey(),
            'nom'=>$membre->getNom(),
            'url'=>$app->path('membre',['id_membre'=>$membre->getPrimaryKey()]),
            'description'=>'',

        );

    }

    return $app->json($tab_data);

}
