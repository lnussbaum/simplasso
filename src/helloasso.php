<?php


use \Propel\Runtime\ActiveQuery\Criteria;

function helloasso(){
    global $app;
    $args_twig=[];




    $args_twig['tab_import_en_cours']=ImportQuery::create()->filterByOperation('helloasso')->filterByAvancement([0,1,2])->orderByCreatedAt()->find();
    $tab_prop=ImportQuery::create()->joinWithImportligne()->filterByOperation('helloasso')->filterByAvancement([3])->orderByCreatedAt()->find();

    $tab_ligne=[];
    foreach($tab_prop as $import){
        $tab_entete=ImportligneQuery::create()->filterByStatut('E')->filterByIdImport($import->getPrimaryKey())->findOne()->getVariables();
        foreach($import->getImportlignes() as $ligne) {
            if ($ligne->getStatut()=='D'){

                 $tab_info = array_combine($tab_entete,$ligne->getVariables());
                $prop =$ligne->getPropositions();


                    if(isset($prop['id_membre'])){
                        $prop['membre']= MembreQuery::create()->findPks($prop['id_membre'])->toKeyIndex();
                    }
                    if(isset($prop['id_individu'])){
                        $prop['individu']= IndividuQuery::create()->findPks($prop['id_individu'])->toKeyIndex();
                    }


                $tab_ligne[$import->getPrimaryKey()][$ligne->getLigne()] = [
                    'variables' => [
                        'id'=>$tab_info['Numéro'],
                        'date'=>$tab_info['Date'],
                        'type_prestation'=>$tab_info['Type'],
                        'montant'=>$tab_info['Montant'],
                        'nom'=>$tab_info['Nom'].' '.$tab_info['Prénom'],
                    ],
                    'propositions' => $prop
                ];
            }

        }
    }


    $args_twig['tab_import_proposition_ligne'] = $tab_ligne;
    $args_twig['tab_import_proposition'] = $tab_prop;
    $args_twig['tab_import_histo'] = LogQuery::create()->filterByCode('HELCLO')->orderByDateOperation()->find();
    return $app['twig']->render( 'helloasso.html.twig',$args_twig);
}

