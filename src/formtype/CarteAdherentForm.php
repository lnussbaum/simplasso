<?php

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;



class CarteAdherentForm extends AbstractType
{
    /**
     * {@inheritdoc}
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
            $tab_bloc = array_flip(table_simplifier(table_filtrer_valeur(tab('bloc'),'canal','C'),'nom'));
			$builder->add('position_depart', TextType::CLASS,array('label' => 'position_depart', 'attr' => ['data-rows'=>$options['rows'],'data-cols'=>$options['cols'],'class'=>'position_table']));
			$builder->add('id_bloc', ChoiceType::CLASS,['label' => 'modele','choices'=>$tab_bloc, 'attr' => ['class'=>'']]);
	}
    /**
     * {@inheritdoc}
     */
    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults([
         //   'data_class' => 'bloc',
            'name'       => 'carte_adherent',
            'cols'       => 3,
            'rows'       => 8
        ]);
    }
}