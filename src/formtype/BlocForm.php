<?php

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\Extension\Core\Type\TextareaType;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;


class BlocForm extends AbstractType
{
    /**
     * {@inheritdoc}
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $tab_choice =array_flip(getCourrierCanal());
			$builder->add('nom', TextType::CLASS,array('label' => 'Nom', 'attr' => ['class'=>'']));
			$builder->add('texte', TextareaType::CLASS,array('label' => 'Texte', 'attr' => ['class'=>'']));
			$builder->add('canal', ChoiceType::CLASS,array('label' => 'Canal','choices'=>$tab_choice, 'attr' => ['class'=>'']));
    }
    /**
     * {@inheritdoc}
     */
    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults([
         //   'data_class' => 'bloc',
            'name'       => 'bloc',
        ]);
    }
}