<?php

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\Extension\Core\Type\DateType;


class InfolettresForm extends AbstractType
{
    /**
     * {@inheritdoc}
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
			$builder->add('identifiant', TextType::CLASS,array('label' => 'Identifiant', 'attr' => ['class'=>'']));
			$builder->add('nom', TextType::CLASS,array('label' => 'Nom', 'attr' => ['class'=>'']));
			$builder->add('descriptif', TextType::CLASS,array('label' => 'Descriptif', 'attr' => ['class'=>'']));
			$builder->add('automatique', TextType::CLASS,array('label' => 'Automatique', 'attr' => ['class'=>'']));
			$builder->add('date_synchro', DateType::CLASS,array('label' => 'Date_synchro','widget'=> 'single_text','format'=> 'dd/MM/yyyy', 'attr' => ['class'=>'datepickerb']));
			$builder->add('active', TextType::CLASS,array('label' => 'Active', 'attr' => ['class'=>'']));
			$builder->add('liaison_auto', TextType::CLASS,array('label' => 'Liaison_auto', 'attr' => ['class'=>'']));
			$builder->add('options', TextType::CLASS,array('label' => 'Options', 'attr' => ['class'=>'']));
			$builder->add('position', TextType::CLASS,array('label' => 'Position', 'attr' => ['class'=>'']));
    }
    /**
     * {@inheritdoc}
     */
    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults([
         //   'data_class' => 'infolettre',
            'name'       => 'infolettre',
        ]);
    }
}