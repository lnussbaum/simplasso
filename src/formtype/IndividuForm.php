<?php

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\Extension\Core\Type\TextareaType;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\Extension\Core\Type\CheckboxType;
use Symfony\Component\Form\Extension\Core\Type\EmailType;
use Symfony\Component\Form\Extension\Core\Type\DateType;
use Symfony\Component\Form\Extension\Core\Type\FileType;


class IndividuForm extends AbstractType
{
    /**
     * {@inheritdoc}
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {


        if (conf('civilite.individu.libre')) {
            $builder->add('civilite', TextType::class, array(
                'label' => 'civilite individu',
                'label_attr' => array('class' => ' secondaire'),
                'attr' => array('class' => '', 'placeholder' => 'civilite')));
        } else {
            $choices_civilite = array_flip(conf('civilite.individu.valeurs'));
            $builder->add('civilite', ChoiceType::class, array(
                'label' => 'civilite individu',
                'choices' => $choices_civilite,
                'expanded' => true,
                'label_attr' => array('class' => 'radio-inline secondaire')));
        }

        $builder->add('nom_famille', TextType::class, array(
            'label' => 'nom_famille',
            'attr' => array('class' => '', 'placeholder' => 'nom de famille')))
            ->add('prenom', TextType::class, array(
                'attr' => array('class' => '', 'placeholder' => 'prenom')))
            ->add('adresse', TextareaType::class, array(
                'label' => 'adresse',
                'attr' => array('placeholder' => '')))
            ->add('codepostal', TextType::class, array(
                'label' => 'Code postal',
                'attr' => array('class' => 'autocomplete_cp', 'data-champ_commun' => 'ville',
                    'placeholder' => 'Code postal ou ville et choisir')))
            ->add('ville', TextType::class, array(
                'label' => 'Ville',
                'attr' => array( 'class' => 'autocomplete_ville', 'data-champ_commun' => 'codepostal',
                    'placeholder' => 'Code postal ou ville et choisir')))
            ->add('pays', ChoiceType::class, array(
                'label' => 'pays',
                'choices' => array_flip(tab('pays')),
                'attr' => array('class' => 'secondaire')))
            ->add('telephone', TextType::class, array(
                'label' => 'telephone',
                'attr' => array('class' => 'telephone', 'placeholder' => '')))
            ->add('telephone_pro', TextType::class, array(
                'label' => 'telephone_pro',
                'attr' => array('class' => 'telephone', 'placeholder' => '')))
            ->add('mobile', TextType::class, array(
                'label' => 'mobile',
                'attr' => array('class' => 'telephone','placeholder' => '')))
            ->add('fax', TextType::class, array(
                'label' => 'fax',
                'attr' => array('class' => 'telephone secondaire','placeholder' => '')))
            ->add('email', EmailType::class, array(
                'label' => 'email',
                'attr' => array('placeholder' => 'adresse@email.fr') ))
            ->add('contact_souhait', CheckboxType::class, array(
                'label' => 'contact_souhait',
                'required' => false,
                'attr' => array('align_with_widget' => true, 'class' => 'bs_switch')))
            ->add('profession', TextType::class, array(
                'label' => 'profession',
                'attr' => array('class' => 'secondaire', 'placeholder' => 'profession')))
            ->add('naissance', DateType::class, array(
                'label' => 'naissance',
                'widget' => 'single_text','format' => 'dd/MM/yyyy',
                'attr' => array('class' => 'datepickerb secondaire')))
            ->add('login', TextType::class, array(
                'label' => 'login',
                'attr' => ['class' => 'secondaire', 'placeholder' => 'login']))
            ->add('observation', TextareaType::class, array(
                'label' => 'observation individu','attr'=>['class'=>'secondaire','placeholder' => 'Observation sur l\'individu']))
            ->add('image', FileType::class, array('label' => 'Photo', 'attr' => array('class' => 'jq-ufs secondaire')));

    }
    /**
     * {@inheritdoc}
     */
    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults([
            'name'       => 'individu'
        ]);
    }
}