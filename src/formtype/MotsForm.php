<?php

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Form\Extension\Core\Type\TextType;


class MotsForm extends AbstractType
{
    /**
     * {@inheritdoc}
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
			$builder->add('nom', TextType::CLASS,array('label' => 'Nom', 'attr' => ['class'=>'']));
			$builder->add('nomcourt', TextType::CLASS,array('label' => 'Nomcourt', 'attr' => ['class'=>'']));
			$builder->add('descriptif', TextType::CLASS,array('label' => 'Descriptif', 'attr' => ['class'=>'']));
			$builder->add('texte', TextType::CLASS,array('label' => 'Texte', 'attr' => ['class'=>'']));
			$builder->add('importance', TextType::CLASS,array('label' => 'Importance', 'attr' => ['class'=>'']));
			$builder->add('id_motgroupe', TextType::CLASS,array('label' => 'Id_motgroupe', 'attr' => ['class'=>'']));
    }
    /**
     * {@inheritdoc}
     */
    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults([
         //   'data_class' => 'mot',
            'name'       => 'mot',
        ]);
    }
}