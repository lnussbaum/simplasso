<?php

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\Extension\Core\Type\TextareaType;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\Extension\Core\Type\HiddenType;


class MembreForm extends AbstractType
{
    /**
     * {@inheritdoc}
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {


        if (conf('civilite.membre.libre')) {
            $builder->add('civilite', TextType::class, array(
                'label' => 'civilite membre',
                'label_attr' => array('class' => ' secondaire'),
                'attr' => array('class' => '', 'placeholder' => 'civilite')));
        } else {
            $choices_civilite = array_flip(conf('civilite.membre.valeurs'));
            $builder->add('civilite', ChoiceType::class, array(
                'label' => 'civilite membre',
                'choices' => $choices_civilite,
                'expanded' => true,
                'label_attr' => array('class' => 'radio-inline secondaire')
            ));
        }


            $builder->add('nom', TextType::class, array(
                'label' => 'nom prenom',
                'attr' => array('placeholder' => 'nom prenom membre')));

        if (conf('champs.membre.nomcourt')) {
            $builder->add('nomcourt', TextType::class, array(
//                'constraints' => [new Assert\NotBlank(), new Assert\Length(['max' => 6])],
                'label' => 'nomcourt',
                'attr' => array('maxlength' => 6,
                    'placeholder' => 'Nom court du membre', 'class' => 'secondaire'),
            ));
        }
        if (conf('champs.membre.identifiant_interne')) {
            $builder->add('identifiant_interne', TextType::class,
                array(/*'constraints' => new Assert\NotBlank(),*/
                    'label' => 'identifiant_interne',
                    'attr' => array('class' => 'secondaire')
                ));
        }
        $builder->add('observation', TextareaType::class, array('label' => 'observation',
            'attr' => array('placeholder' => 'Observation membre')));
    }
    /**
     * {@inheritdoc}
     */
    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults([
         //   'data_class' => 'mot',
            'name'       => 'mot'
        ]);
    }
}