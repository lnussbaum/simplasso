<?php

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\Extension\Core\Type\CheckboxType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;


class EtiquetteForm extends AbstractType
{
    /**
     * {@inheritdoc}
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $choices = ['courante' => 'courante'];
        $tab_select = pref('selection.' . $options['objet']);
        if (is_array($tab_select)) {
            foreach ($tab_select as $i => $s) {
                $choices[$s['nom']] = $i;
            }
        }
        $builder->add("selection", ChoiceType::class, [
                'choices' => $choices,
                'multiple' => false,
                'expanded' => false,
                'attr' => []
            ]
        )
            ->add('avec_individu', CheckboxType::class, array(
                'label' => 'avec_individu',
                'attr' => array('align_with_widget' => true, 'class' => 'bs_switch')
            ))
            ->add('champs_civilite', CheckboxType::class, array(
                'attr' => array('align_with_widget' => true, 'class' => 'bs_switch')
            ))
            ->add('champs_numero', CheckboxType::class, array(
                'attr' => array('align_with_widget' => true, 'class' => 'bs_switch')
            ))
            ->add('champs_titulaire', CheckboxType::class, array(
                'attr' => array('align_with_widget' => true, 'class' => 'bs_switch')
            ))
            ->add('champs_nom_membre', CheckboxType::class, array(
                'attr' => array('align_with_widget' => true, 'class' => 'bs_switch')
            ))
            ->add('ajout_nom_membre', CheckboxType::class, array(
                'attr' => array('align_with_widget' => true, 'class' => 'bs_switch')
            ))
            ->add('classement', ChoiceType::class, array(
                  'expanded' => true,
                  'label_attr' => array('class' => 'radio-inline'),
                  'choices' => array('Nom' => 0, 'Code postal+ville+nom' => 1, 'Ville+nom' => 2),
                  'attr' => array('inline' => true)

              ))
            ->add('position_depart', TextType::CLASS, array(
                'label' => 'position_depart',
                'attr' => [
                    'data-rows' => $options['rows'],
                    'data-cols' => $options['cols'],
                    'class' => 'position_table'
                ]
            ));

    }

    /**
     * {@inheritdoc}
     */
    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults([
            //   'data_class' => 'bloc',
            'name' => 'etiquette',
            'objet' => 'membre',
            'cols' => 3,
            'rows' => 8
        ]);
    }
}