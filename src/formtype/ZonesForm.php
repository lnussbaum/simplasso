<?php

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\Extension\Core\Type\HiddenType;


class ZonesForm extends AbstractType
{
    /**
     * {@inheritdoc}
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
		$tab_zonegroupe = array_flip(table_simplifier(tab('zonegroupe')));

        $builder->add('id_zonegroupe', ChoiceType::CLASS,array('label' => 'Zonage','choices'=> $tab_zonegroupe,'expanded'=> false,'multiple'=> false, 'attr' => ['class'=>'']));
        $builder->add('nom', TextType::CLASS,array('label' => 'Nom', 'attr' => ['class'=>'']));
        $builder->add('descriptif', TextType::CLASS,array('label' => 'Descriptif', 'attr' => ['class'=>'']));
        $builder->add('zone_geo', HiddenType::CLASS,array('label' => 'Zone_geo', 'attr' => ['class'=>'']));
    }
    /**
     * {@inheritdoc}
     */
    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults([
         //   'data_class' => 'zone',
            'name'       => 'zone',
        ]);
    }
}