<?php

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\Extension\Core\Type\RepeatedType;
use Symfony\Component\Form\Extension\Core\Type\PasswordType;
use Symfony\Component\Validator\Constraints as Assert;



class MotDePasseForm extends AbstractType
{
    /**
     * {@inheritdoc}
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('username', TextType::class,array('label_attr'=>['class'=>'hide'],'attr' => array('class'=>'hide','disabled'=>'disabled')))
            ->add('password', RepeatedType::class, array(
                'type' => PasswordType::class,
                'invalid_message' => 'Les mots de passe saisis ne sont pas identique',
                'required' => true,
                'first_options' => ['constraints' => [new Assert\NotBlank(), new Assert\Length(['min' => 8])],'label' => 'Nouveau mot de passe','attr' => ['class' => 'password-field','readonly'=>'readonly','autocomplete'=>'new-password']],
                'second_options' => ['constraints' => [new Assert\NotBlank(), new Assert\Length(['min' => 8])],'label' => 'Vérification mot de passe','attr' => ['class' => 'password-field','readonly'=>'readonly','autocomplete'=>'new-password']],
            ));

     	}
    /**
     * {@inheritdoc}
     */
    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults([
            'name'       => 'mot_de_passe',
        ]);
    }
}