<?php

function upload(){
	global $app;


    $output_dir = $app['upload.path'].'/';
    $nom_form=$_GET['nom_form'];
    $ok=false;

    if( isset($_FILES['fichiers']) ) {
        $ret = array();


        if (!file_exists($output_dir))
            mkdir($output_dir);

        $prefixe = uniqid();
        $error = $_FILES['fichiers']["error"];
        //You need to handle  both cases
        //If Any browser does not support serializing of multiple files using FormData()
        if(!is_array($_FILES['fichiers']["name"])) //single file
        {

            $fileName = $prefixe.'_'.$_FILES['fichiers']["name"];
            move_uploaded_file($_FILES['fichiers']["tmp_name"],$output_dir.$fileName);
            $ret[]= $fileName;
        }
        else  //Multiple files, file[]
        {
            $fileCount = count($_FILES['fichiers']["name"]);
            for($i=0; $i < $fileCount; $i++)
            {
                $fileName = $prefixe.'_'.$_FILES['fichiers']["name"][$i];
                move_uploaded_file($_FILES['fichiers']["tmp_name"][$i],$output_dir.$fileName);

                $ret[]= $fileName;
            }

        }

        if(!$error){


            $app['session']->set('file_upload_tmp_'.$nom_form,$ret);
            print_r($nom_form);
            print_r($ret);
            exit();
        }
    }



        $app['session']->set('file_upload_tmp_'.$nom_form,"");
    exit();
}

