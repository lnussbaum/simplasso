<?php


use Propel\Runtime\Map\TableMap;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\Form\FormError as FormError;
use Symfony\Component\Validator\Constraints as Assert;

function bloc_form()
{
    global $app;
    $args_rep = [];
    $request = $app['request'];
    if (sac('id')) {
        $modification = true;
        $objet_data = charger_objet();
        $data = $objet_data->toArray(TableMap::TYPE_FIELDNAME);
    } else {
        $modification = false;
        $objet_data = array();
        $data = array();
    }
    $builder = $app['form.factory']->createNamedBuilder('bloc', BlocForm::class, $data);
    $builder->setRequired(false);
    formSetAction($builder, $modification);


    $form = $builder->add('submit', SubmitType::class,
        array('label' => $app->trans('Enregistrer'), 'attr' => array('class' => 'btn-primary')))
        ->getForm();


    $form->handleRequest($request);
    if ($form->isSubmitted()) {
        $data = $form->getData();
        if ($form->isValid()) {
            if (!$modification) {
                $objet_data = new Bloc();
            }
            $objet_data->fromArray($data);
            $objet_data->save();
            $args_rep = [
                'id' => $objet_data->getPrimaryKey()
            ];
            chargement_table();
        }
    }

    return reponse_formulaire($form, $args_rep);
}
