<?php
function composition_liste()
{
    global $app;
    $args_twig=[
        'tab_col'=>composition_colonnes()
        ];

    return $app['twig']->render(fichier_twig(), $args_twig);

}


function composition_colonnes(){

    $tab_colonne= array();
	$tab_colonne['id_composition'] = ['title'=> 'id'];
	$tab_colonne['nom'] = [];

    $tab_colonne['action']=["orderable"=> false];
    $tab_colonne = datatable_complete_liste_colonne($tab_colonne);

    return $tab_colonne;

}





function action_composition_liste_dataliste()
{

    return objet_liste_dataliste('composition');

}


