<?php
function bloc_liste()
{
    global $app;
    $args_twig=[
        'tab_col'=>bloc_colonnes()
        ];
    return $app['twig']->render(fichier_twig(), $args_twig);

}


function bloc_colonnes(){

    $tab_colonne= array();
	$tab_colonne['id_bloc'] = ['title'=> 'id',];
	$tab_colonne['nom'] = [];
	$tab_colonne['canal'] = [];
	$tab_colonne['valeurs'] = [];
	$tab_colonne['texte'] = [];

    $tab_colonne['action']=["orderable"=> false];
    $tab_colonne = datatable_complete_liste_colonne($tab_colonne);

    return $tab_colonne;

}





function action_bloc_liste_dataliste()
{

    global $app;
    $request = $app['request'];
    $args = array();
    if (sac('objet') != 'bloc') {
        $args = array('prestation_type' => descr( sac('objet') . '.alias_valeur'));
    }
    list($sous_requete, $nb_total) = getSelectionObjetNb('bloc',$args);

    $tab_data = array();
    $start = request_ou_options('start');
    $length = request_ou_options('length');

    $tab_tri = tri_dataliste();
    $tab_tri_sql = tri_dataliste_sql( 'bloc', $tab_tri);
    $tab_id = $app['db']->fetchAll($sous_requete . $tab_tri_sql . ' LIMIT ' . (intval($start)) . ',' . $length);

    foreach ($tab_id as &$result) {
        $result = $result['id_bloc'];
    }

    $tab_colonnes = bloc_colonnes();
    $tab_data=array();

    if (!empty($tab_id)) {

        $tab = BlocQuery::getAll($tab_id,tri_dataliste($tab_tri));
        $tab_data= datatable_prepare_data($tab,$tab_colonnes);

    }



    return $app->json([
        'draw' => $request->get('draw'),
        'recordsTotal' => $nb_total,
        'recordsFiltered' => $nb_total,
        'data' => $tab_data,
        'error' => ''
    ]);

}


