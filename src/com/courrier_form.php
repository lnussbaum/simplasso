<?php

use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\Extension\Core\Type\FormType;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\Form\Extension\Core\Type\CheckboxType;
use Symfony\Component\Form\Extension\Core\Type\TextareaType;
use Symfony\Component\Form\Extension\Core\Type\FileType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\FormError as FormError;
use Symfony\Component\Validator\Constraints as Assert;

include_once($app['basepath'].'/src/inc/fonctions_document.php');

function courrier_form()
{

    global $app;
    $request = $app['request'];
    $args_rep = [];

    if (sac('id')) {
        $objet_data = charger_objet();
        $modification = true;
        $data = $objet_data->toArray();

    } else {
        $modification = false;
        $data = sac('idinit');
    }

    include_once($app['basepath'] . '/src/inc/fonctions_document.php');
    $builder = $app['form.factory']->createNamedBuilder('courrier', FormType::class, $data);
    formSetAction($builder, $modification, $data);
    $builder->setRequired(false);


    $tab_composition = array_flip(table_simplifier(tab('composition')));
    $tab_type = [
        0 => 'Relance',
        1 => 'Convocation AG',
        2 => 'Attestation',
        3 => 'Divers',
    ];
    //array_flip(table_simplifier(tab('composition')));
    $tab_modele_lettre_signature = liste_modele_doc('signature', 'lettre');
    $tab_modele_lettre_basdepage = liste_modele_doc('basdepage', 'lettre');
    $tab_modele_email = liste_modele_doc('relance', 'email');
    $tab_modele_email_signature = liste_modele_doc('signature', 'email');


    $builder
        ->add('nom', TextType::class,
            array('constraints' => new Assert\NotBlank(), 'attr' => array('placeholder' => 'Nom du courrier')));
    if (conf('champs.courrier.nomcourt')) {
        $builder->add('nomcourt', TextType::class, array(
            'constraints' => [new Assert\NotBlank(), new Assert\Length(['max' => 6])],
            'attr' => array('maxlength' => 6, 'class' => 'small6', 'placeholder' => '')
        ));
    }
    $form = $builder
        ->add('type', ChoiceType::class, array(
            'label' => 'Type de courrier',
            'choices' => array_flip($tab_type),
            'expanded' => false,
            'attr' => array('inline' => true, 'placeholder' => 'nom')
        ))
        ->add('id_composition', ChoiceType::class, array(
            'label' => 'Modele de document',
            'choices' => $tab_composition,
            'expanded' => false,
            'attr' => array('inline' => true, 'placeholder' => 'nom')
        ))
        ->add('canal', ChoiceType::class, array(
            'label' => 'Canaux',
            'choices' => array_flip(getCourrierCanal()),
            'expanded' => true,
            'multiple' => true,
            'attr' => array('class' => 'checkbox-inline', 'placeholder' => 'nom'),
            'label_attr' => array('inline' => true, 'class' => 'checkbox-inline')
        ))
        ->add('observation', TextareaType::class,
            array('attr' => array('placeholder' => 'Observation à usage interne')))
        ->add('submit', SubmitType::class,
            array('label' => $app->trans('Enregistrer'), 'attr' => array('class' => 'btn-primary')))
        ->getForm();

    $form->handleRequest($request);
    if ($form->isSubmitted()) {
        $data = $form->getData();
        if ($form->isValid()) {
            if (!$modification) {
                $objet_data = new Courrier();
            }
            $variables = [];
            $tab_canal = getCourrierCanal();
            if (!$modification) {
                foreach ($data['canal'] as $canal) {
                    include_once($app['basepath'] . '/src/inc/fonctions_document.php');
                    if (isset($tab_canal[$canal])) {
                        $c = $tab_canal[$canal];
                        $tab_comp = tab('composition.' . $data['id_composition'] . '.blocs');
                        $tab_bloc = array_intersect_key(table_filtrer_valeur(tab('bloc'), 'canal', $canal), $tab_comp);
                        foreach ($tab_bloc as $k => $bloc) {
                            $variables[$c][$bloc['nom']]['texte'] = $bloc['texte'];
                            $variables[$c][$bloc['nom']]['ordre'] = $tab_comp[$k];
                            $variables[$c][$bloc['nom']]['position'] = 'corp';
                            $variables[$c][$bloc['nom']]['variables'] = document_rechercher_variables($bloc['texte']);
                        }
                    }
                }
                $objet_data->setValeur($variables);
            }


            $objet_data->fromArray($data);
            $objet_data->setStatut('redaction');
            $objet_data->setIdEntite(pref('en_cours.id_entite'));
            $objet_data->save();
            $args_rep['id']=$objet_data->getPrimaryKey();
        }
    }
    $args_rep['js_init']='courrier_form';
    return reponse_formulaire($form, $args_rep);

}


function action_courrier_form_trier()
{
    global $app;
    $request = $app['request'];
    $ok = false;
    $index = $request->get('index');
    $bloc_id_a = $request->get('bloc_id');
    $canal = $request->get('canal');
    $objet_data = charger_objet();

    $valeur = $objet_data->getValeur();
    $tab_bloc = table_trier_par($valeur[getCourrierCanal($canal)], 'ordre');
    $i = 0;
    foreach ($tab_bloc as $bloc_id => &$bloc) {
        if ($bloc_id_a == $bloc_id) {
            if ($i < $index) {
                $i--;
            }
            $bloc['ordre'] = $index;
        } else {
            if ($i == $index) {
                $i++;
            }
            $bloc['ordre'] = $i;
        }
        $i++;
    }
    $valeur[getCourrierCanal($canal)] = $tab_bloc;
    $objet_data->setValeur($valeur);
    $objet_data->save();
    return $app->json(['ok' => true]);
}



function action_courrier_form_condition()
{
    global $app;
    $request = $app['request'];
    $args_rep = [];
    $canal = $request->get('canal');
    $bloc_id = $request->get('bloc_id');
    $tab_canal = getCourrierCanal();
    if (sac('id')) {
        $objet_data = charger_objet();
        $modification = true;
        $valeur =$objet_data->getValeur();
        $data=[];
        if(isset($valeur[$tab_canal[$canal]][$bloc_id]['condition']))
            $data = $valeur[$tab_canal[$canal]][$bloc_id]['condition'];
    }
    $builder = $app['form.factory']->createNamedBuilder('courrier_condition', FormType::class, $data);
    formSetAction($builder, $modification, array('action' => 'condition', 'canal' => $canal, 'bloc_id' => $bloc_id));
    $builder->setRequired(false);

    $form = $builder
        ->add('nom', TextType::class,
            ['attr' => [ 'placeholder' => 'Condition en toute lettre']])
        ->add('submit', SubmitType::class,
            array('label' => $app->trans('Enregistrer'), 'attr' => array('class' => 'btn-primary')))
        ->getForm();

    $form->handleRequest($request);
    if ($form->isSubmitted()) {
        $data = $form->getData();
        if ($form->isValid()) {
            $valeur = $objet_data->getValeur();
            $valeur[$tab_canal[$canal]][$bloc_id]['condition'] = $data;
            $objet_data->setValeur($valeur);
            $objet_data->save();
        }
    }

    return reponse_formulaire($form, $args_rep);

}


function action_courrier_form_options_bloc_email(){
    global $app;
    $request = $app['request'];
    $args_rep = [];
    $canal = 'email';

    $data=[];
    if (sac('id')) {
        $objet_data = charger_objet();
        $modification = true;
        $valeur =$objet_data->getValeur();

        if(isset($valeur['options'][$canal]))
            $data = $valeur['options'][$canal];
    }
    $builder = $app['form.factory']->createNamedBuilder('options_bloc_email', FormType::class, $data);
    formSetAction($builder, true, array('action' => 'options_bloc_email'));
    $builder->setRequired(false);

    $form = $builder
        ->add('pj_courrier_pdf', CheckboxType::class, array(
            'label' => 'Joindre le courrier en pdf',
            'required' => false,
            'attr' => array('align_with_widget' => true, 'class' => 'bs_switch')))
        ->add('pj_courrier_pdf_nom_fichier', TextType::class,
            ['attr' => [ 'placeholder' => '']])
        ->add('submit', SubmitType::class,
            array('label' => $app->trans('Enregistrer'), 'attr' => array('class' => 'btn-primary')))
        ->getForm();

    $form->handleRequest($request);
    if ($form->isSubmitted()) {
        $data = $form->getData();
        if ($form->isValid()) {
            $valeur = $objet_data->getValeur();
            $valeur['options'][$canal] = $data;
            $objet_data->setValeur($valeur);
            $objet_data->save();
        }
    }

    return reponse_formulaire($form, $args_rep);


}



function action_courrier_form_style()
{
    global $app;
    $request = $app['request'];
    $args_rep = [];
    $canal = $request->get('canal');
    $bloc_id = $request->get('bloc_id');
    $tab_canal = getCourrierCanal();
    if (sac('id')) {
        $objet_data = charger_objet();
        $modification = true;
        $valeur =$objet_data->getValeur();
        $data=[];
        if(isset($valeur[$tab_canal[$canal]][$bloc_id]['css']))
            $data = $valeur[$tab_canal[$canal]][$bloc_id]['css'];
    }
    $builder = $app['form.factory']->createNamedBuilder('courrier_style', FormType::class, $data);
    formSetAction($builder, $modification, array('action' => 'style', 'canal' => $canal, 'bloc_id' => $bloc_id));
    $builder->setRequired(false);

    $form = $builder
        ->add('position', TextType::class,
            ['attr' => [ 'placeholder' => 'absolute ou relative']])
        ->add('top', TextType::class,
            ['attr' => [ 'placeholder' => 'en px ou %']])
        ->add('left', TextType::class,
            ['attr' => [ 'placeholder' => 'en px ou %']])
        ->add('width', TextType::class,
            ['label'=>'Largeur','attr' => [ 'placeholder' => 'en px ou %']])
        ->add('height', TextType::class,
            ['label'=>'Hauteur','attr' => [ 'placeholder' => 'en px']])
        ->add('style', TextareaType::class,
            array('attr' => array('rows' => 10, 'placeholder' => 'css')))
        ->add('submit', SubmitType::class,
            array('label' => $app->trans('Enregistrer'), 'attr' => array('class' => 'btn-primary')))
        ->getForm();

    $form->handleRequest($request);
    if ($form->isSubmitted()) {
        $data = $form->getData();
        if ($form->isValid()) {
            $valeur = $objet_data->getValeur();
            $valeur[$tab_canal[$canal]][$bloc_id]['css'] = $data;
            $objet_data->setValeur($valeur);
            $objet_data->save();
        }
    }

    return reponse_formulaire($form, $args_rep);

}



function action_courrier_form_texte()
{
    global $app;
    $request = $app['request'];
    $args_rep = [];
    $canal = $request->get('canal');
    $bloc_id = $request->get('bloc_id');
    $tab_canal = getCourrierCanal();
    if (sac('id')) {
        $objet_data = charger_objet();
        $modification = true;
        $data['texte'] = $objet_data->getValeur()[$tab_canal[$canal]][$bloc_id]['texte'];
    }
    $builder = $app['form.factory']->createNamedBuilder('courrierTexte', FormType::class, $data);
    formSetAction($builder, $modification, array('action' => 'texte', 'canal' => $canal, 'bloc_id' => $bloc_id));
    $builder->setRequired(false);

    $form = $builder
        ->add('texte', TextareaType::class,
            array('attr' => array('rows' => 10, 'placeholder' => 'Texte', 'class' => 'wysiwyg')))
        ->add('submit', SubmitType::class,
            array('label' => $app->trans('Enregistrer'), 'attr' => array('class' => 'btn-primary')))
        ->getForm();

    $form->handleRequest($request);
    if ($form->isSubmitted()) {
        $data = $form->getData();
        if ($form->isValid()) {
            include_once($app['basepath'] . '/src/inc/fonctions_document.php');

            $valeur = $objet_data->getValeur();
            $tab_vars = document_rechercher_variables($data['texte']);
            if (isset($valeur[$tab_canal[$canal]][$bloc_id]['variables'])) {
                $tab_vars_temp = array_intersect_key($valeur[$tab_canal[$canal]][$bloc_id]['variables'], $tab_vars);
                $tab_vars = array_merge($tab_vars, $tab_vars_temp);
            }
            $valeur[$tab_canal[$canal]][$bloc_id]['texte'] = $data['texte'];
            $valeur[$tab_canal[$canal]][$bloc_id]['variables'] = $tab_vars;
            $objet_data->setValeur($valeur);
            $objet_data->save();
        }
    }

    $args_rep['js_init']='composant_wysi';
    return reponse_formulaire($form, $args_rep);

}

function ajouter_formulaire_upload_piece_jointe($objet,$id_objet){


    global $app;
    $request = $app['request'];
    $args_rep = [];
    $modification=true;
    $builder = $app['form.factory']->createNamedBuilder('upload_piece_jointe', FormType::class);
    formSetAction($builder, $modification);
    $builder->setRequired(false);

    $form = $builder
        ->add('document', FileType::class, array('label' => 'Pieces jointes', 'attr' => array('class' => 'jq-ufs')))
        ->add('submit', SubmitType::class,
            array('label' => $app->trans('Enregistrer'), 'attr' => array('class' => 'btn-primary')))
        ->getForm();

    $form->handleRequest($request);
    if ($form->isSubmitted()) {
        $data = $form->getData();
        if ($form->isValid()) {
            include_once($app['basepath'] . '/src/inc/fonctions_ged.php');
            traitement_form_ged('upload_piece_jointe_document',$id_objet,$objet,'pj',true);
        }
    }

    return $form;
}



function action_courrier_form_variable()
{
    global $app;
    $request = $app['request'];
    $ok = false;
    $canal = $request->get('canal');
    $bloc_id = $request->get('bloc_id');
    $id_courrier = $request->get('id_courrier');
    $name = $_POST['name'];
    $value = $_POST['value'];
    $objet_data = CourrierQuery::create()->findPk($id_courrier);
    $valeur = $objet_data->getValeur();


    $valeur[$canal][$bloc_id]['variables'][$name] = $value;

    $objet_data->setValeur($valeur);
    $objet_data->save();
    return $app->json('ok');

}


function action_courrier_form_email_sujet()
{
    global $app;
    include_once($app['basepath'] . '/src/inc/fonctions_document.php');
    $request = $app['request'];
    $id_courrier = $request->get('id_courrier');
    $value = $_POST['value'];
    $objet_data = CourrierQuery::create()->findPk($id_courrier);
    $valeur = $objet_data->getValeur();
    $valeur['email_sujet']['texte'] = $value;
    $valeur['email_sujet']['variables'] = document_rechercher_variables($value);
    $objet_data->setValeur($valeur);
    $objet_data->save();
    return $app->json('ok');

}


function action_courrier_form_position_bloc()
{
    global $app;

    $request = $app['request'];
    $id_courrier = $request->get('id_courrier');
    $bloc_id = $_POST['name'];
    $value = $_POST['value'];
    $objet_data = CourrierQuery::create()->findPk($id_courrier);
    $valeur = $objet_data->getValeur();
    if (isset($valeur['lettre'][$bloc_id])) {
        $valeur['lettre'][$bloc_id]['position'] = $value;
    }
    $objet_data->setValeur($valeur);
    $objet_data->save();
    return $app->json('ok');

}


function action_courrier_form_ajouter_bloc()
{


    global $app;
    $args_rep = [];
    $request = $app['request'];
    $id_courrier = sac('id');
    $canal = $request->get('canal');
    $modification = true;
    $data = array();

    $builder = $app['form.factory']->createNamedBuilder('courrier_ajouter_bloc', FormType::class, $data);
    $builder->setRequired(false);
    formSetAction($builder, $modification,
        ['action' => 'ajouter_bloc', 'canal' => $canal, 'id_courrier' => $id_courrier]);

    $tab_bloc = array_flip(table_simplifier(table_filtrer_valeur(tab('bloc'), 'canal', $canal)));
    $tab_bloc['libre'] = 'libre';
    $form = $builder
        ->add('id_bloc', ChoiceType::class,
            array('label' => 'Bloc', 'choices' => $tab_bloc, 'expanded' => false, 'attr' => array('inline' => true)))
        ->add('id_du_bloc', TextType::class, array('label' => 'Identifiant du bloc'))
        ->add('submit', SubmitType::class,
            array('label' => $app->trans('Enregistrer'), 'attr' => array('class' => 'btn-primary')))
        ->getForm();


    $form->handleRequest($request);
    if ($form->isSubmitted()) {
        $data = $form->getData();
        if ($form->isValid()) {
            $objet_data = charger_objet();
            if ($data['id_bloc'] != 'libre') {
                $bloc = BlocQuery::create()->findPk($data['id_bloc']);
                $champs = [
                    'texte' => $bloc->getTexte(),
                    'variables' => $bloc->getValeurs(),
                    'position' => 'corp'
                ];
            } else {
                $champs = [
                    'texte' => '',
                    'variables' => [],
                    'position' => 'corp'
                ];
            }
            $canal_nom = getCourrierCanal($canal);
            $valeur = $objet_data->getValeur();
            if (isset($valeur[$canal_nom]) && is_array($valeur[$canal_nom])) {
                $valeur_top = table_trier_par($valeur[$canal_nom . ''], 'ordre');
                $ordre = array_pop($valeur_top);
                $champs['ordre'] = $ordre['ordre'] + 1;
            } else {
                $champs['ordre'] = 1;
            }
            $valeur[$canal_nom][$data['id_du_bloc']] = $champs;
            $objet_data->setValeur($valeur);
            $objet_data->save();
            $args_rep['id'] = $id_courrier;

        }
    }
    return reponse_formulaire($form, $args_rep);
}


function action_courrier_form_supprimer_bloc()
{

    global $app;
    $request = $app['request'];
    $bloc_id = $request->get('bloc_id');
    $canal = $request->get('canal');
    $canal_nom = getCourrierCanal($canal);
    $objet_data = charger_objet();
    $valeur = $objet_data->getValeur();
    unset($valeur[$canal_nom][$bloc_id]);
    $objet_data->setValeur($valeur);
    $objet_data->save();
    $url_redirect = $app->path('courrier', array(descr(sac('objet') . '.cle') => sac('id')));


    return $app->redirect($url_redirect);
}


function action_courrier_form_supprimer()
{
    return action_supprimer_une_instance();
}
