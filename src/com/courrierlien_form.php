<?php


use Symfony\Component\Form\FormError as FormError;
use Symfony\Component\Form\Extension\Core\Type\FormType;
use Symfony\Component\Form\Extension\Core\Type\TextareaType;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\Validator\Constraints as Assert;
use Propel\Runtime\Map\TableMap;

function courrierlien_form()
{

    global $app;
    $args_rep=[];
    $request = $app['request'];
    $modification = false;
    $id_courrier=$request->get('id_courrier');
    if (sac('id')) {
        $objet_data = charger_objet();
        $modification = true;
        $data=$objet_data->toArray(TableMap::TYPE_FIELDNAME);
    }
    else{
        $data=array();
        $objet=$request->get('objet');
        $tab_selection = getSelections($objet);
    }


    $builder = $app['form.factory']->createNamedBuilder('courriermembre',FormType::class,$data);
    $args = array();
    if (!$modification){
        $args = array('id_courrier'=>$id_courrier,'objet'=>$objet);
    }

    formSetAction($builder,$modification,$args);
    $builder->setRequired(false);

    if (!$modification){

        $builder=$builder
            ->add('selection',ChoiceType::class, array('label' => 'Sélection', 'choices' => $tab_selection, 'expanded' => false, 'attr' => array('inline' => true, 'placeholder' => 'nom')));
    //        ->add('id_list',TextType::class, array( 'label' => 'id_membre'));
        if ($objet=='membre'){
            $builder ->add('inclure_individu',ChoiceType::class, array(
                'label' => $app->trans('inclure_individu'),
                'expanded' => true,
                'label_attr' => array('class' => 'radio-inline'),
                'choices' => array('oui' =>1,  'non' => 0),
                'attr' => array('inline' => true)

            ));}
    }

    $form = $builder
        ->add('message_particulier',TextareaType::class, array('label' => 'Observation', 'attr' => array('placeholder' => '')))

        ->add('submit',SubmitType::class,
            array('label' => $app->trans('Enregistrer'), 'attr' => array('class' => 'btn-primary')))
        ->getForm();

    $form->handleRequest($request);
    if ($form->isSubmitted()) {
        $data = $form->getData();
        if ($form->isValid()) {

            if (!$modification) {

                if ($data['selection'] === 'courante') {
                    $valeur_selection = $app['session']->get('selection_courante_' . $objet);
                }
                elseif ($data['selection'] !== 'tous') {
                    $valeur_selection = $prefs = pref('selection.' . $objet . '.' . $data['selection'] . '.valeurs');
                }

                $courrier = CourrierQuery::create()->findPk($id_courrier);

                switch($objet){
                    case 'individu':
                         $q = IndividuQuery::create();
                        if(!empty($valeur_selection))
                            $q->where('id_individu IN ('.$valeur_selection.')');
                        $tab_individu=$q->find();
                        foreach($tab_individu as $key=>$individu) {
                            if ($individu) {
                                ecrire_courrierlien($courrier,$individu,$id_courrier);
                            }
                        }
                        break;
                    case 'membre':
                        $sous_requete = getSelectionObjet('membre',$valeur_selection);

                        $tab_membres = MembreQuery::create()->where('id_membre IN ('.$sous_requete.')')->find();

                        $tab_individu=[];
                        foreach($tab_membres as $membre) {
                            if ($data['inclure_individu']) {
                                    $inds=$membre->getIndividus();
                                    foreach($inds as $ind) {
                                        $tab_individu[$membre->getPrimaryKey()][] = $ind;
                                    }
                                }
                            else{
                                $ind = $membre->getIndividuTitulaire();

                                if($ind)
                                    $tab_individu[$membre->getPrimaryKey()][]=$ind;
                            }
                        }
                        foreach($tab_individu as $id_membre=>$individus) {
                            foreach($individus as $key=>$individu) {
                                if ($individu) {
                                    ecrire_courrierlien($courrier, $individu, $id_courrier, $id_membre);
                                }
                            }
                        }
                    break;
                }
            }  else {
                $objet_data->fromArray($data);
                $objet_data->save();
            }
            $courrier = CourrierQuery::create()->findPk($id_courrier);
            $nom_type=getCourrierType($courrier->getType());
            $args_rep=[
                'url_redirect'=>$app->path('courrier',array('id_courrier' => $id_courrier))
            ];
         }
    }
    return reponse_formulaire($form,$args_rep);
}


function action_courrierlien_form_supprimer(){
    return action_supprimer_une_instance();
}

function ecrire_courrierlien($courrier,$individu,$id_courrier,$id_membre=null){

    $rel = CourrierLienQuery::create()
        ->filterByIdIndividu($individu->getPrimaryKey())
        ->filterByIdMembre($id_membre)
        ->findOneByIdCourrier($id_courrier);

    if (!$rel) {
        $rel = new CourrierLien();
        if ($id_membre) {
            $rel->setIdMembre($id_membre);
        }

        $rel->setIdIndividu($individu->getPrimaryKey());
        $rel->setIdCourrier($id_courrier);

    }
    $canal = 'L';

    if ($courrier->estSms() && $individu->getMobile() != '') {
        $canal = 'S';
    } elseif ($courrier->estEmail() && $individu->getEmail() != '') {
        $canal = 'E';
    }

    $rel->setCanal($canal);
    $rel->save();

    return;
}
