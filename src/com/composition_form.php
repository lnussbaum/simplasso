<?php


use Propel\Runtime\ActiveQuery\Criteria;
use Propel\Runtime\Map\TableMap;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\Extension\Core\Type\FormType;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\Validator\Constraints as Assert;

function composition_form()
{
    global $app;
    $args_rep = [];
    $request = $app['request'];
    if (sac('id')) {
        $modification = true;
        $objet_data = charger_objet();
        $data = $objet_data->toArray(TableMap::TYPE_FIELDNAME);
    } else {
        $modification = false;
        $objet_data = array();
        $data = array();
    }
    $builder = $app['form.factory']->createNamedBuilder('composition', CompositionForm::class, $data);
    $builder->setRequired(false);
    formSetAction($builder, $modification);


    $form = $builder->add('submit', SubmitType::class,
        array('label' => $app->trans('Enregistrer'), 'attr' => array('class' => 'btn-primary')))
        ->getForm();


    $form->handleRequest($request);
    if ($form->isSubmitted()) {
        $data = $form->getData();
        if ($form->isValid()) {
            if (!$modification) {
                $objet_data = new Composition();
            }
            $objet_data->fromArray($data);
            $objet_data->save();
            $id = $objet_data->getPrimaryKey();
            $args_rep = ['id' => $id];
            chargement_table();
        }
    }

    return reponse_formulaire($form, $args_rep);
}


function action_composition_form_ajouter()
{


    global $app;
    $args_rep = [];
    $request = $app['request'];
    $id_composition = sac('id');
    $canal = $request->get('canal');
    $modification = true;
    $data = array('canal'=>$canal);
    $data = array();
    $bloc_a=CompositionsBlocQuery::create()
        ->filterByIdComposition($id_composition)
        ->useBlocQuery()
        ->filterByCanal($data['canal'])
        ->endUse()
        ->orderByOrdre(Criteria::ASC)
        ->find();
    foreach ($bloc_a as $k=>$v) {
         $data['bloc'][]=$v->getIdBloc();
    }
    $builder = $app['form.factory']->createNamedBuilder('composition', FormType::class, $data);
    $builder->setRequired(false);
    formSetAction($builder, $modification,
        ['action' => 'ajouter', 'canal' => $canal, 'id_composition' => $id_composition]);

    $tab_bloc = array_flip(table_simplifier(table_filtrer_valeur(tab('bloc'), 'canal', $data['canal'])));
    $tab_bloc['aucun']=0;
    $form = $builder
        ->add('bloc', ChoiceType::class, array(
        'label'=>$app->trans('bloc'),
        'choices' => $tab_bloc,
        'multiple' => true,
        'label_attr' => array('class' => 'radio-inline'),
        'attr' => array('class' => 'multiselectshow')))
        ->add('submit', SubmitType::class,
            array('label' => $app->trans('Enregistrer'), 'attr' => array('class' => 'btn-primary')))
        ->getForm();


    $form->handleRequest($request);
    if ($form->isSubmitted()) {
        $data = $form->getData();
        if ($form->isValid()) {
            $bloc_anc_repris=array();
            $ids_ancien=CompositionsBlocQuery::create()
                ->filterByIdComposition($id_composition)
                ->useBlocQuery()
                ->filterByCanal($data['canal'])
                ->endUse()
                ->orderByOrdre(Criteria::DESC)
                ->find();
            foreach ($ids_ancien as $k=>$v){
                $id=$v->getIdBloc();
                $ok=array_search($id,$data['bloc']);
                if (!$ok){
                    $v->delete();
                }else{
                    $bloc_anc_repris[$id]=$v->getOrdre();
                }
            }
            $max= ($bloc_anc_repris) ? max($bloc_anc_repris) :0;

            foreach ($data['bloc'] as $v){
                $ok=array_key_exists($v,$bloc_anc_repris);
                if (!$ok and $v>0){
                    $composition=new CompositionsBloc();
                    $composition->setOrdre($max);
                    $composition->setIdBloc($v);
                    $composition->setIdComposition($id_composition);
                    $composition->save();
                    $max++;
                }
            }
            $args_rep['id'] = $id_composition;
        }
    }

    return reponse_formulaire($form, $args_rep);
}


function action_composition_form_trier()
{
    global $app;
    $request = $app['request'];
    $index = $request->get('index');
    $id_bloc_a = $request->get('bloc_id');
    $canal = $request->get('canal');
    $tab_liens = CompositionsBlocQuery::create()->orderByOrdre()->findByIdComposition(sac('id'));
    $i = 0;
    foreach ($tab_liens as $lien) {
        if ($lien->getBloc()->getCanal() == $canal) {

            if ($id_bloc_a == $lien->getIdBloc()) {
                if ($i < $index) {
                    $i--;
                }
                $lien->setOrdre($index);
            } else {
                if ($i == $index) {
                    $i++;
                }
                $lien->setOrdre($i);
            }
            $lien->save();
            $i++;
        }
    }
    return $app->json(['ok' => true]);
}


function action_composition_form_import_document()
{

    global $app;
    $tab_canal = getCourrierCanal();

    foreach ($tab_canal as $c => $canal) {
        $rep = $app['document.path'] . '/' . $canal . '/';
        foreach (glob($rep . "*.html.twig") as $f) {
            $nom = substr(basename($f), 0, -10);
            $bloc = BlocQuery::create()->filterByNom($nom)->filterByCanal($c)->findOne();
            if(!$bloc){

                $bloc = new Bloc();
                $bloc->setNom($nom);
                $bloc->setCanal($c);
            }
            $bloc->setTexte(file_get_contents($f));
            $bloc->save();
        }
    }
    return $app->redirect($app->path('composition_liste'));

}


function action_composition_form_supprimer()
{
    return action_supprimer_une_instance();
}

