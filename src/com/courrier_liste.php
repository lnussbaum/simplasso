<?php
function courrier_liste()
{
    global $app;

    $args_twig = ['tab_cols'=>courrier_colonnes()];
    return $app['twig']->render(fichier_twig(), $args_twig);

}


function courrier_colonnes()
{

    $tab_colonne = array();
    $tab_colonne['id_courrier'] = ['title' => 'id'];
    $tab_colonne['nom'] = [];
 //   $tab_colonne['nomcourt'] = [];

    $tab_colonne['observation'] = [];
    $tab_colonne['date_envoi'] = ['type' => 'date-eu'];

    $tab_colonne['statut'] = [];

    $tab_colonne['created_at'] = ["type" => "date-eu"];

    $tab_colonne['canal'] = [];

    $tab_colonne['action'] = ["orderable" => false];
    $tab_colonne = datatable_complete_liste_colonne($tab_colonne);

    return $tab_colonne;

}


function action_courrier_liste_dataliste()
{

    return objet_liste_dataliste();

}


