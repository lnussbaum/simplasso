<?php


function action_courrierlien_liste_dataliste()
{

  list($tab_id, $nb_total) = objet_liste_dataliste_preselection();
  $tab = objet_liste_dataliste_selection('courrierlien', $tab_id,tri_dataliste());
  $tab_data = objet_liste_dataliste_preparation('courrierlien',$tab,false);
  foreach ($tab_data as $k=>&$t) {
    $individu = $tab[$k]->getIndividu();
    if ($individu) {
      $t['id_individu'] = $individu->getNom();
    }
    $tab_data[$k] = array_values($tab_data[$k]);
  }

  return  objet_liste_dataliste_envoi($tab_data,$nb_total);

}


function courrierlien_colonnes(){

  $tab_colonne= array();
  $tab_colonne['id_courrier_lien'] = ['title'=> 'id'];
  $tab_colonne['id_membre'] = [];
  $tab_colonne['id_individu'] = ['title'=> 'individu'];
  $tab_colonne['id_courrier'] = [];
  $tab_colonne['canal'] = [];
  $tab_colonne['date_envoi'] = ['type'=> 'date-eu'];
  $tab_colonne['message_particulier'] = [];
  $tab_colonne['statut'] = [];
  $tab_colonne['variables'] = [];


  $tab_colonne['action']=["orderable"=> false];
  $tab_colonne = datatable_complete_liste_colonne($tab_colonne);

  return $tab_colonne;

}

