<?php

function infolettre_liste(){

    global $app;
    $tab_colonnes = infolettre_colonnes();
    $tab_filtres=getFiltresSimples();
    $args_twig=[
        'tab_filtres' => $tab_filtres,
        'liste_filtres' => liste_filtre($tab_filtres),
        'api_ovh'=>$app['apiovh'],
        'api_spip'=>$app['apispip_mailsubscriber'],
        'tab_col'=>$tab_colonnes
    ];
    return $app['twig']->render(fichier_twig(), $args_twig);
}




function infolettre_colonnes(){

    $tab_colonne= array();
    $tab_colonne['id_infolettre'] = ['title'=> 'id',];
    $tab_colonne['identifiant'] = [];
    $tab_colonne['nom'] = [];
    $tab_colonne['descriptif'] = [];
    $tab_colonne['automatique'] = [];
    $tab_colonne['date_synchro'] = ['type'=> 'date-eu'];
    $tab_colonne['active'] = [];
    $tab_colonne['liaison_auto'] = [];
    $tab_colonne['options'] = [];
    $tab_colonne['position'] = [];
    $tab_colonne['created_at'] = ['type'=> 'date-eu'];

    $tab_colonne['action']=["orderable"=> false];
    $tab_colonne = datatable_complete_liste_colonne($tab_colonne);

    return $tab_colonne;

}





function action_infolettre_liste_dataliste()
{

    return objet_liste_dataliste('infolettre');

}
