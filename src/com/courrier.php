<?php

use Symfony\Component\Form\Extension\Core\Type\FormType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\Extension\Core\Type\EmailType;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\Validator\Constraints as Assert;
use \Propel\Runtime\ActiveQuery\Criteria as Criteria;


function courrier(){
    global $app;
    $request=$app['request'];
    $id_courrier= $request->get('id_courrier');
    include_once($app['basepath'].'/src/com/courrierlien_liste.php');
    include_once($app['basepath'].'/src/com/courrier_form.php');
    include_once($app['basepath'].'/src/inc/fonctions_document.php');
    $tab_colonnes = courrierlien_colonnes();
     $form_upload = ajouter_formulaire_upload_piece_jointe('courrier',$id_courrier);
    twig_extension_document();
    $args_twig=[
        'tab_col_courrierlien'=>$tab_colonnes,
        'form_upload_piece_jointe' =>$form_upload->createView()
    ];
    return $app['twig']->render(fichier_twig(),$args_twig);
}



function twig_extension_document(){

    global $app;


        $app['twig']->addFilter(new Twig_SimpleFilter('vars_specifiques', function ($tab) {
            if(is_array($tab))
                return array_diff_key($tab,array_flip(document_variables_systeme()));
            return [];
        }));



}


function action_courrier_imprimer(){
    global $app;
    include_once($app['basepath'].'/src/inc/fonctions_document.php');
    $request = $app['request'];
    $id_courrier= $request->get('id_courrier');
    $format = $request->get('format');
    if($format=='')
        $format='html';
    $html = generer_document($id_courrier,$format,true,null,true);
    return $html;
}



function preparation_twig($canal,$valeur){

    $tab_champs_demandes=[];
    $tab_load=[];
    if ($canal=='email'){
        $tab_load = ['email_sujet' => $valeur['email_sujet']['texte']];
        $tab_champs_demandes = $valeur['email_sujet']['variables'];
    }
    $courrier_valeur = table_trier_par($valeur[$canal],'ordre');
    foreach ($courrier_valeur as $k => $v) {
        $tab_load[$k] =  $v['texte'];
        }
    $loader = new Twig_Loader_Array($tab_load);
    $twig = new Twig_Environment($loader,['autoescape'=>false]);


    return $twig;


}



function preparation_css($canal,$valeur)
{
    $css='';
    foreach ($valeur[$canal] as $bloc_id=>$bloc){
        if(isset($bloc['css']))
            $css .= courrier_agglomerer_style($bloc_id,$bloc).PHP_EOL;
    }
    return $css;
}


function courrier_agglomerer_style($bloc_id,$val){

    $css = '.'.$val['position'].'_'.str_replace(' ','_',$bloc_id).'{';
    $tab_prop = ['position','height','width','left','top'];
    foreach($tab_prop as $prop){
        $css .= (!empty($val['css'][$prop]))?$prop.':'.$val['css'][$prop].';':'';
    }
    $css .= (!empty($val['css']['style']))?$val['css']['style']:'';
    $css .= '}'.PHP_EOL;
    return $css;
}



function courrier_agglomerer_texte($twig,$bloc_id,$args_twig,$tab_vars=null,$tab_vars_global=null){

    $vars=[];
    if(is_array($tab_vars)) {
        $vars = $tab_vars;
    }
    if(is_array($tab_vars_global)){
        $vars = array_merge($tab_vars_global,$vars);
    }
    $args_twig_temp = array_intersect_key($args_twig,$vars);
    $args_twig_temp = array_merge($vars,$args_twig_temp);
    return $twig->render( $bloc_id, $args_twig_temp);
}



function courrier_generer_texte($canal,$args,$courrier){

    $tab_vars_courrier = $courrier->getValeur();
    $twig = preparation_twig($canal,$tab_vars_courrier);
    $args_twig = renseigner_variables_systeme($args,$courrier->getListeVariables($canal));

    $sujet='';
    $tab_vars_global = (isset($tab_vars_courrier['variables']))?$tab_vars_courrier['variables']:[];
    if ($canal =='email' ){
        $tab_vars_email_sujet = (isset($tab_vars_courrier['email_sujet']['variables']))?$tab_vars_courrier['email_sujet']['variables']:[];
        $sujet = courrier_agglomerer_texte($twig,'email_sujet',$args_twig,$tab_vars_email_sujet,$tab_vars_global);
    }
    $tab_bloc = table_trier_par($tab_vars_courrier['email'],'ordre');
    $html='';
    foreach ($tab_bloc as $bloc_id => $v) {
        $tab_temp = isset($v['variables'])?$v['variables']:[];
        $html .= courrier_agglomerer_texte($twig,$bloc_id,$args_twig,$tab_temp,$tab_vars_global);
    }

    return [$html,$sujet];

}




function action_courrier_envoyer_test(){

    global $app;
    include_once($app['basepath'].'/src/inc/fonctions_document.php');
    $request = $app['request'];
    $args_rep=[];
    $id_courrier= $request->get('id_courrier');
    $operateur = IndividuQuery::create()->findPk(suc('operateur.id'));

    $data=array('id_courrier'=>$id_courrier,'action'=>'envoyer_test','email'=> $operateur->getEmail() );

    $builder = $app['form.factory']->createNamedBuilder('envoyer_test',FormType::class,$data);
    formSetAction($builder,false,$data);
    $builder->setRequired(false);

    $form = $builder
        ->add('email',EmailType::class, array('constraints' => new Assert\NotBlank(), 'attr' => array('placeholder' => 'Email de test')))
        ->add('id_individu',TextType::class, array('constraints' => new Assert\NotBlank(), 'attr' => array('placeholder' => 'Identifiant d\'un individu')))
        ->add('submit',SubmitType::class,
            array('label' => $app->trans('Envoyer un test'), 'attr' => array('class' => 'btn-primary')))
        ->getForm();

    $form->handleRequest($request);
    if ($form->isSubmitted()) {
        $data = $form->getData();
        if ($form->isValid()) {

            $email=$data['email'];
            $id_individu=$data['id_individu'];
            $html='';
            $courrier= CourrierQuery::create()->findPk($id_courrier);
            $tab_cl= CourrierLienQuery::create()->filterByIdCourrier($id_courrier)->filterByCanal('E')->findOne();
            $args=[
                'id_entite'=>$courrier->getIdEntite(),
            ];

            if(empty($id_individu)){
                if($tab_cl){
                    $args['id_individu'] = $tab_cl->getIdIndividu();
                    $args['id_membre']=$tab_cl->getIdMembre();
                }
                else{
                    $individu=IndividuQuery::create()->filterByEmail(null,Criteria::ISNOTNULL)->findOne();
                    $args['id_individu'] = $individu->getIdIndividu();
                }
            }
            else
            {
                $args['id_individu']=$data['id_individu'];
            }

            if((!isset($args['id_membre'])) && $args['id_individu']){
                $args['id_membre'] = MembreIndividuQuery::create()->findOneByIdIndividu($args['id_individu'])->getIdMembre();
            }
            envoyer_courrier_email($id_courrier,$args,$email);
            $args_rep['message']='L\'envoi des emails a été effectué avec succès';
        }
    }
    $args_rep['js_init']='relance_form';
    $args_rep['modification']=false;
    return reponse_formulaire($form,$args_rep,'inclure/form.html.twig');

}

function envoyer_courrier_email($id_courrier,$args,$email=null){

    global $app;
    $courrier = CourrierQuery::create()->findPk($id_courrier);
    include_once($app['basepath'] . '/src/inc/fonctions_ged.php');
    list($html,$sujet)=courrier_generer_texte('email',$args,$courrier);

    $options = ['pieces_jointes'=>preparer_piece_jointe_avant_envoi($courrier->getPieceJointeEmail())];
    if ($options_email = $courrier->getOptions('email')){
        if(isset($options_email['pj_courrier_pdf']) && $options_email['pj_courrier_pdf']){
            $options['pieces_jointes'][$options_email['pj_courrier_pdf_nom_fichier']] = generer_document($id_courrier,'pdf',false,$args['id_individu']);
        }

    }
    if(empty($email)){
        $email = IndividuQuery::create()->findPk($args['id_individu'])->getEmail();
    }
    if(!empty($email)) {
        return courriel($email, $sujet, $html, $options);
    }
    return false;
}


function action_courrier_envoyer_decollage(){
    global $app;
    $request = $app['request'];
    $id_courrier= $request->get('id_courrier');

    $args_twig=[
        'url'=>$app->path('courrier',['action'=>'envoyer','id_courrier'=>$id_courrier]),
        'url_redirect'=>$app->path('courrier',['id_courrier'=>$id_courrier]),
        'progression_pourcent'=>0,
        'message'=>'Envoi des emails',
        'titre'=>'Envoi des emails'
    ];
    return $app['twig']->render('attente.html.twig',$args_twig);
}


function action_courrier_envoyer(){
    global $app;
    $request = $app['request'];
    $id_courrier= $request->get('id_courrier');
    $cl = CourrierLienQuery::create()->filterByIdCourrier($id_courrier)->filterByCanal('E')->filterByDateEnvoi(null,Criteria::ISNULL)->findOne();
    $courrier= CourrierQuery::create()->findPk($id_courrier);
    $args=[
        'id_entite'=>$courrier->getIdEntite(),
        'id_membre'=>$cl->getIdMembre(),
        'id_individu'=>$cl->getIdIndividu()
    ];
    envoyer_courrier_email($id_courrier,$args);
    $cl->setDateEnvoi(new DateTime())->save();
    $nb_envoi = CourrierLienQuery::create()->filterByIdCourrier($id_courrier)->filterByCanal('E')->count();
    $nb_envoye = CourrierLienQuery::create()->filterByIdCourrier($id_courrier)->filterByCanal('E')->filterByDateEnvoi(null,Criteria::ISNOTNULL)->count();
    $args_rep=[
        'progression_pourcent'=>floor($nb_envoye/$nb_envoi*100),
        'message'=>$nb_envoye.' emails envoyés'
    ];

   // $message = 'L\'envoi des '.$nb.' emails a été effectué avec succès';
    return $app->json($args_rep);
}



function action_config_form_supprimer(){
    return action_supprimer_une_instance();
}