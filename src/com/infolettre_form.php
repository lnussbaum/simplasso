<?php


use Propel\Runtime\Map\TableMap;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\Form\FormError as FormError;
use Symfony\Component\Validator\Constraints as Assert;

function infolettre_form() {
    global $app;
    $args_rep = [];
    $request = $app['request'];
    if (sac('id')) {
        $modification = true;
        $objet_data = charger_objet();
        $data = $objet_data->toArray(TableMap::TYPE_FIELDNAME);
    } else {
        $modification = false;
        $objet_data = array();
        $data = array();
    }
    $builder = $app['form.factory']->createNamedBuilder('infolettre', InfolettresForm::class, $data);
    $builder->setRequired(false);
    formSetAction($builder, $modification);


    $form = $builder->add('submit', SubmitType::class,
        array('label' => $app->trans('Enregistrer'), 'attr' => array('class' => 'btn-primary')))
        ->getForm();


    $form->handleRequest($request);
    if ($form->isSubmitted()) {
        $data = $form->getData();
        if ($form->isValid()) {
            if (!$modification) {
                $objet_data = new Infolettre();
            }
            $objet_data->fromArray($data);
            $objet_data->save();
            $args_rep['id'] = $objet_data->getPrimaryKey();
            chargement_table();
        }
    }

    return reponse_formulaire($form, $args_rep);
}





function action_infolettre_form_changer_inscription()
{
    global $app;
    $request = $app['request'];
    $message = '';
    $email = $request->get('email');
    $nom = $request->get('nom');
    $tab_id_liste = explode(',',$request->get('id_infolettre'));
    $unique = !empty($request->get('unique'));

    $individu = null;
    if (!empty($email)) {
        include_once($app['basepath'] . '/src/inc/fonctions_infolettre.php');
        changer_inscription($email,$tab_id_liste,$nom,$unique);
        $message = 'L\'inscription au infolettre a été effectuée';
    }
    $url_redirect = construire_redirection('individu');
    if (sac('ajax')) {
        return $app->json(array('message' => $message, 'ok' => true,'ajout'=>count($tab_id_liste)));
    } else {
        return $app->redirect($url_redirect);
    }
}





function action_infolettre_form_import_spip() {

    global $app;
    if ($app['apispip']) {
        $tab_liste = $app['API_spip']->newsletter_liste();
        foreach ($tab_liste as $k => $liste) {

            $existe = InfolettreQuery::create()->filterByIdentifiant('spip_' . $liste['id_liste'])->exists();
            if (!$existe) {
                $data = [
                    'identifiant' => 'spip_' . $liste['id_liste'],
                    'nom' => $liste['titre'],
                    'descriptif' => $liste['descriptif'],
                    'automatique' => true,
                    'active' => true,
                    'position' => $k
                ];
                $objet_data = new Infolettre();
                $objet_data->fromArray($data);
                $objet_data->save();
                chargement_table();
            }
        }
    }
    return $app->redirect($app->path('infolettre_liste'));
}


function action_infolettre_form_newsletter_subscribe() {
    global $app;
    $request = $app['request'];
    $message = '';
    $id = $request->get('id_individu');
    $id_liste = $request->get('id_liste');
    $individu = null;
    if (!empty($id)) {
        $individu = IndividuQuery::create()->findPk($id);
        $result = $app['API_spip']->newsletter_inscription($id_liste, $individu->getEmail(), $individu->getNom());
    }
    $url_redirect = construire_redirection('individu',['id_individu'=>$id]);

    if (sac('ajax')) {
        $url = $app->path('individu_form',
            ['action' => 'newsletter_unsubscribe', 'id_liste' => $id_liste, 'id_individu' => $id]);
        return $app->json(array('message' => $message, 'ok' => true, 'url' => $url));
    } else {
        return $app->redirect($url_redirect);
    }
}

function action_infolettre_form_newsletter_unsubscribe() {

    global $app;
    $request = $app['request'];
    $id = $request->get('id_individu');
    $message = '';
    $id_liste = $request->get('id_liste');
    $individu = null;
    if (!empty($id)) {
        $individu = IndividuQuery::create()->findPk($id);
        $result = $app['API_spip']->newsletter_desinscription($id_liste, $individu->getEmail());

    }

    $url_redirect = construire_redirection('individu',['id_individu'=>$id]);
    if (sac('ajax')) {
        $url = $app->path('individu_form',
            ['action' => 'newsletter_subscribe', 'id_liste' => $id_liste, 'id_individu' => $id]);
        return $app->json(array('message' => $message, 'ok' => true, 'url' => $url));
    } else {
        return $app->redirect($url_redirect);
    }

}


