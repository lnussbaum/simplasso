<?php

use Symfony\Component\Validator\Constraints as Assert;


function importation_liste_modele()
{

    global $app;
    $tab = [];
    foreach (glob($app['basepath'] . '/src/importation/modele/*.php') as $modele) {

        $tab[] = basename($modele, ".php");
    }
    return $tab;
}


function importation_variables($modele = null)
{
    global $app;
    $tab = [
        'nom' => 'a définir',
        'test' => 0,
        'objets' => [],
        'separateur_csv' => ';',
        'nb_ligne_entete' => 1,
        'nb_colonne' => 40,
        'nom_colonne_premier_mot' => false,
        'resultat' => false,
        'import_plusieurs' => false,
        'caractere' => 'UTF8',
        'extension' => 'csv',
        'curseur' => 0
    ];

    if ($modele) {
        include_once($app['basepath'] . '/src/importation/modele/' . $modele . '.php');
        $fonction = 'importation_variables_' . $modele;
        $tab_modele = $fonction();
        $tab = table_merge($tab, $tab_modele);
    }
    return $tab;
}


function importation_objets($modele = null)
{
    global $app;
    $tab = [];

    if ($modele) {
        include_once($app['basepath'] . '/src/importation/modele/' . $modele . '.php');
        $fonction = 'importation_objets_' . $modele;
        $tab_modele = $fonction();
        $tab = array_merge($tab_modele, $tab);
    }
    return $tab;
}


function champ_propel($v)
{// format php name partant du field php
    if (is_array($v)) {
        $v = $v['o_c'];
    }
    while (strpos($v, '_') > 0) :
        $v = substr($v, 0, strpos($v, '_')) . ucfirst(substr($v, (strpos($v, '_') + 1)));
    endwhile;

    return ucfirst($v);
}


function etapeSuivante(Import $import)
{

    $modification = $import->getModification();
    if (count($modification['etapes']) > 1) {
        reset($modification['etapes']);
        $key = key($modification['etapes']);
        unset($modification['etapes'][$key]);
        $avancement = key($modification['etapes']);
        $import
            ->setModification($modification)
            ->setAvancement($avancement)
            ->setLigneEnCours(1);
    }
    return $import;
}


function reorganiserVariablesModele($modele)
{

    $modele_vars_objet = importation_objets($modele);
    $origine = [];
    foreach ($modele_vars_objet as $objet => $var_objet) {

        foreach ($var_objet['nom_colonne'] as $nom_champs => $nom_colonne) {
            if (descr($objet . '.colonnes.' . $nom_champs) > '' or substr($nom_champs, 0, 10) == 'temporaire') {
                if (!is_array($nom_colonne)) {
                    $nom_colonne = [$nom_colonne];
                }

                if (!is_array($nom_colonne[0])) {
                    $nom_colonne[0] = [$nom_colonne[0]];
                }

                $i = 0;
                foreach ($nom_colonne[0] as $k => $nom) {

                    if (!is_array($nom)) {
                        $origine[$nom][$objet][$nom_champs]=[];
                        if (isset($nom_colonne[1])) {
                            $origine[$nom][$objet][$nom_champs] = $nom_colonne[1];
                        }
                    } else {
                        $temp_options = isset($nom[1]) ? $nom[1] : [];
                        $origine[$nom[0]][$objet][$nom_champs] = $temp_options;
                        if (isset($nom_colonne[1])) {
                            $i++;
                            $origine[$nom[0]][$objet][$nom_champs]['options'] = $nom_colonne[1];
                            if (isset($nom_colonne[1]['separateur'])) {
                                $origine[$nom[0]][$objet][$nom_champs]['options']['rang'] = $i;
                            }
                        }
                    }
                }
            }
        }
    }
    return $origine;
}


function importation_charge_entete($modele, $fichier, $info, $id_import)
{
    global $app;
    include_once($app['basepath'] . '/src/inc/fonctions_import.php');
    include_once($app['basepath'] . '/src/inc/fonctions_bdd.php');
    $k = 0;

    $nb = [
        'erreurs' => 0,
        'colonnes' => 100,
        'curseur' => 0,
        'ligne_entete' => 0,
        'lignes_ecrites' => 0,
        'ecritures_echoues' => 0,//ecritutes échouées
        'lignes_interprete' => 0,
        'lignes_valide' => 0,
        'lignes_compte_rendu' => 0
    ];
    $config = array();
    $tab_etat = array();
    $options = $info['options'];
    $origine = reorganiserVariablesModele($modele);
    $modele_vars_objet = importation_objets($modele);
    foreach ($options['objets'] as $objet) {

        $tab_champs = donne_liste_colonne(descr($objet . '.table_sql'));


        foreach ($tab_champs as $n => $c) {


            if (isset($modele_vars_objet[$objet]['nom_colonne'][$n])) {
                $config[$objet]['champs'][$n] = [];
            }
            if ($c['notnull'] and $c['default'] != null) {
                $config[$objet]['champs'][$n]['default'] = $c['default'];
                $config[$objet]['champs'][$n]['type'] = 'TextType';
            }
            if (!(in_array($n, ['updated_at']))) {
                switch ($n) {
                    case descr($objet . '.cle_sql'):
                        $config[$objet]['champs'][$n]['title'] = 'id';
                        break;
                }
                if (isset($config[$objet][$n]['options']['nom_colonne'])) {
                    if (substr($n, 0, 5) == 'date_') {
                        $config[$objet]['champs'][$n]['type'] = 'date-eu';
                    }
                    if (colonneEstInteger(descr($objet . '.table_sql'), $n) && substr($n, 0, 3) == 'id_') {
                        $config[$objet]['champs'][$n]['type'] = 'ChoiceType';
                        $config[$objet]['champs'][$n]['data'] = '$tab_' . $n;
                        $config[$objet]['champs'][$n]['expanded'] = 'false';
                        $config[$objet]['champs'][$n]['multiple'] = 'false';
                    }
                }
            }
        }
        $tab_etat[$objet] = [
            'etat' => ['data' => 'LU', 'defaut' => 'LU'],
            'compte_rendu' => ['data' => ''],
            'modification' => false
        ];
        if (isset($modele_vars_objet[$objet]['options'])) {
            $config[$objet]['options'] = $modele_vars_objet[$objet]['options'];
        } else {
            //les options obligatoires
            $config[$objet]['options'] = array(
                'recherche' => 'id_' . $objet,
                'obligatoire' => array(
                    'ou' => array('id_' . $objet),
                    'et' => array('id_' . $objet),
                    'creation_autorise' => true
                ),
                'detail_creation' => false,
                'detail_modification' => false
            );
        }
        $ecrit_modification[$objet] = (isset($modele_vars_objet[$objet]['options']['ecrit_modification'])) ? false : true;
        $ecrit_creation[$objet] = (isset($modele_vars_objet[$objet]['options']['ecrit_creation'])) ? false : true;
        $nb[$objet] = [
            'LU' => 0,//lu
            'AR' => 0,//Archive
            'ok' => 0,//déja enregistre
            'NC' => 0,//non conforme
            'DC' => 0,//détail création
            'DM' => 0,//détail modification
            'EX' => 0,//Existant pas de modification
            'VA' => 0,// pret a écrire
            'EC' => 0,// ecriture faite EC  Erreur en ecriture création
            'EE' => 0,// ecriture échouée
            'AJ' => 0,// ajouté
            'MO' => 0
        ];// modifié
        /*  } else {
              $nb['erreurs']++;
              $info['message'] .= ' objet ' . $objet . ' non trouve :';
              echo '<br>Nb erreurs  :' . $nb['erreurs'];
              arbre($info['message']);

          }*/

    };


    $extension = substr($fichier, strrpos($fichier, '.'));// premiere lecture
    $options = $info['options'];
    switch ($extension) {
        case  '.csv' :
            list($tab_ligne_entete, $erreur) = importation_charge_entete_lignes($fichier, 0, $options);
            if (!$tab_ligne_entete) {
                $nb['erreurs']++;
                $info['message'] .= implode('<br>', $erreur);
            }
            break;
        case "xlsx":
            $tab_ligne_entete = lecture_fichier_excel($fichier, $typefichier = 'Excel2007');
            //todo était dans la bouble if ($contenu_fic) a la place de foreach
            ////             for ($colonne = 0; $colonne < $nb['colonnes']; $colonne++) {
            //                 $colonne_1 = $colonne + 1;
            //                 $cellule = import_lecture_cellule($contenu_fic, $extension, $colonne, $nb_lignes_entete);
            ////            echo '<br>  valeur  '.$cellule. ' colonne '.$colonne_1.' ligne de l\'entete '.$nb_lignes_entete;
            //                 if ($info['options']['nom_colonne_premier_mot']) {
            //                     $cellule = import_lecture_entete_premier_mot($cellule);// pour eviter les phrases
            //                 }
            break;
        default ://todo une seule fois
            $info['message'] .= "extention non connue " . $extension . PHP_EOL;
    }
    $nb_ligne_entete = $options['nb_ligne_entete'];


    foreach ($tab_ligne_entete as $ligne_entete) {
        $derniere_colonne = 0;
        $nb['ligne_entete']++;
        //$colonnes = array(0 => array('logiciel' => array('num_ligne' => array('nom_colonne' => array(0 => 'num_ligne')))));
        foreach ($ligne_entete as $num_colonne0 => $nom_colonne) {
            $num_colonne = $num_colonne0 + 1;
            if ($nom_colonne) {

                if (isset($origine[$nom_colonne])) {

                    if (is_array($origine[$nom_colonne])) {

                        foreach ($origine[$nom_colonne] as $objet => $tab_champs_base) {


                            $colonnes[$num_colonne]['nom'] = $nom_colonne;
                            //$temp_options = isset($tab_champs_base['options'])? $tab_champs_base['options']:[];
                            //$colonnes[$num_colonne]['objet'][$objet]['options']=$temp_options;
                            foreach ($tab_champs_base as $nom_champs => $options_champs) {

                                $colonnes[$num_colonne]['objet'][$objet][$nom_champs] = $options_champs;

                                $derniere_colonne = max($num_colonne, $derniere_colonne);
                                if (isset($ecrit_creation[$nom_champs])) {
                                    $ok = false;
                                    if (isset($config[$nom_champs]['options']['ecrit_creation'])) {
                                        if (array_search($objet,
                                                $config[$nom_champs]['options']['ecrit_creation']) === false
                                        ) {
                                            $ok = true;
                                        }
                                    } else {
                                        $ok = true;
                                    }
                                    if ($ok) {
                                        $config[$nom_champs]['options']['ecrit_creation'][] = $objet;
                                    }
                                }
                                if (isset($ecrit_modification[$nom_champs])) {
                                    $ok = false;
                                    if (isset($config[$nom_champs]['options']['ecrit_modification'])) {
                                        if (array_search($objet,
                                                $config[$nom_champs]['options']['ecrit_modification']) === false
                                        ) {
                                            $ok = true;
                                        }
                                    } else {
                                        $ok = true;
                                    }
                                    if ($ok) {
                                        $config[$nom_champs]['options']['ecrit_modification'][] = $objet;
                                    }
                                }
                                /*
                                                                if ($objet == 'logiciel') {
                                                                    $colonnes[$num_colonne]['nom'] = $nom_colonne;
                                                                    $colonnes[$num_colonne]['logiciel'] = $config[$nom_champs][$objet];
                                                                }*/
                            }
                        }
                        //}
                    }
                } else {

                    $nb['erreurs']++;
                    $info['message'] .= ' colonne ' . $num_colonne . ' non utilisée :' . $nom_colonne . ', ' . PHP_EOL;
                    $colonnes[$num_colonne]['nom'] = $nom_colonne;
                    $colonnes[$num_colonne]['logiciel'][$nom_colonne] = [];

                }
            } else {
                $k++;
                if ($k > 10) {//nb de cellule vide consecutive sur la ligne des noms(10)
                    $derniere_colonne = $num_colonne - 9;
                    $colonne = 100;
                }
            }
        }
        $nb['colonnes'] = max(count($ligne_entete), $nb['colonnes']);


    }
//    arbre($tca_service);// ajoute les colonnes non trouvées
//    echo 'derniere_colonne :'.$derniere_colonne;
//    arbre($origine);arbre(count($origine));arbre($colonnes);arbre($contenu_fic[$nb_lignes_entete]);
//    arbre($colonnes);echo 'arbre $config';
//    arbre($config);


    // importligne_creation($id_import, $nb_lignes_entete, 'E', $config, 'entete');
    // importligne_creation($id_import, $nb_lignes_entete - 1, 'E', $colonnes, 'entete');


    $info['colonnes'] = $colonnes;
    $info['config'] = $config;
    $info['nb'] = $nb;
    $info['etat'] = $tab_etat;


    return $info;

}

function positionne_sur_fichier($fichier, $debut)
{

    global $app;
    $handle = fopen($app['upload.path'] . '/' . $fichier, 'r');
    for ($i = 1; $i < $debut; $i++) {
        fgets($handle);
    }
    $pos = ftell($handle);
    fclose($handle);
    return $pos;
}


function importation_charge_entete_lignes($fichier, $debut, $options)
{
    global $app;
    $nb_ligne_entete = $options['nb_ligne_entete'];
    $separateur_csv = $options['separateur_csv'];
    $tab = array();
    $erreur = array();
    $handle = fopen($app['upload.path'] . '/' . $fichier, 'r');

    if ($handle !== false) {

        for ($i = 0; $i < $debut; $i++) {
            fgets($handle);
        }
        for ($i = 1; $i <= $nb_ligne_entete; $i++) {
            $buffer = fgets($handle, 4096);
            $buffer = utf8_encode(trim($buffer));
            $tab[$i] = explode($separateur_csv, $buffer);
        }

    } else {
        $message = 'Le fichier ' . $fichier;
        if ($handle === false) {
            $message .= ' est vide';
        } else {
            $message .= ' provoque une erreur de lecture';
        }
        $erreur[] = $message;
    }
    return [$tab, $erreur];
}


function importation_charge_lignes_csv($fichier, $id_import, $debut, $fin, $curseur, $options)
{
    global $app;
    $fin_du_fichier = false;
    $separateur_csv = $options['separateur_csv'];
    $handle = fopen($app['upload.path'] . '/' . $fichier, 'r');
    if ($handle !== false) {
        fseek($handle, $curseur);
        for ($i = $debut; $i <= $fin; $i++) {
            $buffer = fgets($handle, 4096);
            $buffer = utf8_encode($buffer);
            $tab = explode($separateur_csv, $buffer);
            if (feof($handle)) {
                $fin_du_fichier = true;
                break;
            } else {
                importligne_creation($id_import, $i, 'D', $tab, 'import_lignes');
            }
        }
        $curseur = ftell($handle);
        fclose($handle);
    }
    return [$fin_du_fichier, $i - 1, $curseur];
}


function importation_correspondance_champs($tab, $tab_etat, $val_objet, $objet, $i)
{

    $tab_valeurs = [];


    return true;
}


function importation_rafraichir_ligne($id_import, $num_ligne, $import_info, $modif){







}




function importation_analyse_ligne($tab_l, $tab_etat,$import_info, $modif){

    $config = $import_info['config'];
    $nb = $import_info['nb'];
    foreach ($import_info['options']['objets'] as $objet) {


        list($ok, $tab_l, $tab_etat, $nb) = importation_interprete_ligne_recherche($tab_l, $tab_etat, $nb, $objet,
            $config[$objet]);
        /*if (!$ok and isset($import_info['champs'][$objet]['options']['archive'])) {
            list($msg,$tab_l,$tab_etat,$nb) =importation_interprete_ligne_recherche($tab_l,$tab_etat, $nb, $objet,
                $config[$objet], true);
        }*/


        list($msg, $tab_l, $tab_etat, $nb) = importation_interprete_ligne_ajoute_fichiers_lies($tab_l, $tab_etat,
            $nb,
            $objet,
            $config[$objet]['options']);


        list($msg, $tab_l, $tab_etat, $nb) = importation_interprete_ligne_compare($tab_l, $tab_etat, $nb, $objet,
            $config[$objet]);

        if (isset($config[$objet]['options']['detail_modification'])) {
            $detail = $config[$objet]['options']['detail_modification'];
            list($msg, $tab_l, $tab_etat, $nb) = importation_interprete_prepare_detail_modif($tab_l, $tab_etat, $nb,
                $objet,
                $detail, $modif);
        }
        list($msg, $tab_l, $tab_etat, $nb) = (importation_interprete_ligne_data_obligatoire($tab_l, $tab_etat, $nb,
            $objet, $config[$objet]['options']['obligatoire']));
        if ($msg) {
            $import_info['message'] .= $msg;
        }


//                                    if (array_keys($modification['etapes'],'detail_creation'))
        if (isset($config[$objet]['options']['detail_creation'])) {
            list($msg, $tab_l, $tab_etat, $nb) = importation_interprete_prepare_detail_creation($tab_l, $tab_etat,
                $nb,
                $objet,
                $config[$objet], $original);
        }


        list($msg, $tab_l, $tab_etat, $nb) = importation_interprete_vers_etape_7($tab_l, $tab_etat, $nb, $objet,
            $config);
    }

    return [$msg, $tab_l, $tab_etat, $nb];


}


function importation_interprete_ligne($id_import, $num_ligne, $import_info, $modif)
{


    $ligne_data = ImportligneQuery::create()->filterByIdImport($id_import)->filterByLigne($num_ligne)->findOne();
    if (!$ligne_data) {
        $import_info['message'] .= 'erreur de lecture import ligne ' . $num_ligne;
    } else {


        $tab = $ligne_data->getVariable();

        list($tab_l, $tab_etat) = importation_interprete_met_ligne_dans_objets($tab, $import_info['etat'],
            $import_info['colonnes']);



        list($msg, $tab_l, $tab_etat, $nb) = importation_analyse_ligne($tab_l, $tab_etat,$import_info, $modif);


        $ligne_data->setProposition($tab_l)->save();
    }
    $import_info['etat']=$tab_etat;
    $import_info['nb']=$nb;

    // $ligne_data->setObservation($etape);

//                            echo '<br><br> import_form 299 La ligne ' . $i.' est ecrite avant etape 4,5 ou 6<br>';arbre($tab_l['logiciel']);arbre($tab_l['servicerendu']['montant']);arbre($tab_l['prestation']);
    //                        if ($i==$j and $objet='servicerendu') {arbre($tab_l['servicerendu']);}
    //                        if ($i==$j){echo '<br>Import_form 327';exit;}
//                            arbre($ligne_data);//arbre($config);
//                            arbre($original);


    return [$msg,$import_info,$modif];
}


function importation_interprete_met_ligne_dans_objets($tab, $tab_etat, $colonnes)
{
    $tab_valeurs = [];

    foreach ($colonnes as $num_col => $col) {
        $i = $num_col - 1;
        $tab_objet = [];
        if (isset($col['objet'])) {
            $tab_objet[] = 'objet';
        }
        if (isset($col['logiciel'])) {
            $tab_objet[] = 'logiciel';
        }
        foreach ($tab_objet as $o) {
            if ($o == 'logiciel') {
                $colo = $col;
            } else {
                $colo = $col[$o];
            }

            foreach ($colo as $objet => $val_objet) {
                if (is_array($val_objet)) {

                    foreach ($val_objet as $nom_champs => $vvv) {

                        if (isset($tab[$i])) {

                            if (isset($vvv['traitement'])) {
                                $traitement =  (is_array($vvv['traitement']))? $vvv['traitement']:[$vvv['traitement']];
                                switch($traitement[0]){
                                    case 'tout_majuscule':
                                        $tab[$i] = strtoupper($tab[$i]);
                                    break;
                                    case 'minuscule':
                                        $tab[$i] = strtolower($tab[$i]);
                                    break;
                                    case 'majuscule':
                                        $tab[$i] = ucfirst(strtolower($tab[$i]));
                                        break;
                                    case 'date':
                                        $tab_temp =  DateTime::createFromFormat($vvv['traitement'][1]['format'], $tab[$i]);
                                        $tab[$i] = ($tab_temp)?$tab_temp->format('Y-m-d') : '';
                                    break;

                                }
                            }

                            if (isset($tab_valeurs[$objet][$nom_champs])) {
                                $separateur = '';
                                $avant = false;

                                if (isset($vvv['options']['separateur'])) {
                                    $separateur = $vvv['options']['separateur'];
                                }

                                //todo  a revoir pour MAJ Min min champs composé et multiple
                                //                            if(isset($vvv['nom_colonne']['options']['avant']))echo '<br>i :'.$i.' objet :'.$ii.' champs :'.$iiv.' avant '.$vvv['nom_colonne']['options']['avant'];arbre($vv);
                                //                            if(isset($vvv['nom_colonne']['options']['MAJ']))  echo '<br>i :'.$i.' objet :'.$ii.' champs :'.$iiv.' MAJ '.$vvv['nom_colonne']['options']['MAJ'];
                                //                            if(isset($vvv['nom_colonne']['options']['Min']))  echo '<br>i :'.$i.' objet :'.$ii.' champs :'.$iiv.' Min '.$vvv['nom_colonne']['options']['Min'];
                                //                            if(isset($vvv['nom_colonne']['options']['min']))  echo '<br>i :'.$i.' objet :'.$ii.' champs :'.$iiv.' min '.$vvv['nom_colonne']['options']['min'];
                                if (isset($vvv['options']['avant']) and $nom_champs == $vvv['options']['avant']) {
                                    $avant = true;
                                }

                                if ($avant) {
                                    $tab_valeurs[$objet][$nom_champs] = $tab[$i] . $separateur . $tab_valeurs[$objet][$nom_champs];
                                } else {
                                    $tab_valeurs[$objet][$nom_champs] .= $separateur . $tab[$i];
                                }
                            } else {
                                $tab_valeurs[$objet][$nom_champs] = $tab[$i];
                            }

                            //                        echo '<br>Importation 343 i :' . $i . '  ' . $ii . '  ' . $iiv . '  ' . $tab[$i] . '<br>';
                            if (isset($tab_valeurs[$objet][$nom_champs]) and $tab_valeurs[$objet][$nom_champs]) {
                                //appliquer les options de la valeur importées
                                $separateur = '';
                                if (isset($vvv['nom_colonne']['options'])) {
                                    //                                echo 'todo ajouter plusieurs champs et partie d\'un champs'; arbre($vvv['nom_colonne']['options']);
                                    if (isset($vvv['options'])) {
                                        foreach ($vvv['options'] as $k_options => $ov_options) {
                                            switch ($k_options) {
                                                case 'remplace':
                                                    switch ($tab_valeurs[$objet][$nom_champs]) {
                                                        case 'Adhésion' :
                                                            $tab_valeurs[$objet][$nom_champs] = 1;
                                                            break;
                                                        case 'Don unique' :
                                                            $tab_valeurs[$objet][$nom_champs] = 4;
                                                            break;
                                                    }
                                                    break;
                                                case 'prefixemoins':
                                                    $tab_valeurs[$objet][$nom_champs] = substr($tab_valeurs[$objet][$nom_champs],
                                                        $ov_options);
                                                    break;
                                                case 'fin':
                                                    $tab_valeurs[$objet][$nom_champs] = substr($tab_valeurs[$objet][$nom_champs],
                                                        '-' . $ov_options);
                                                    break;
                                                case 'separateur':// ne sert pas a été repris plus haut voir pour des cas plus complexe
                                                    $separateur = $ov_options;
                                                    break;
                                            }
                                        }
                                    }

                                }
                                if (isset($vvv)) {
                                    //                                echo 'Importation 291 :'.$v['nom'];arbre($vvv['nom_colonne']);
                                    // foreach ($vvv as $ind_colonne => $champs_colonnes) {
                                    // if (is_int($ind_colonne) and $ind_colonne > 0 and $v['nom'] == $champs_colonnes) {
                                    if (isset($multiple[$objet][$nom_champs])) {
                                        $tab_valeurs[$objet][$nom_champs] = $multiple[$objet][$nom_champs] . $separateur . $tab_valeurs[$objet][$nom_champs];
                                    }
                                    //                                        echo '<br>Importation 292 '.$separateur.$tab_valeurs[$ii][$iiv];
                                    //}
                                    // }
                                }
                                if (isset($vvv['type'])) {
                                    switch ($vvv['type']) {
                                        case 'date-eu':
                                            //                                        arbre($tab_valeurs[$ii][$iiv]);
                                            $tab_valeurs[$objet][$nom_champs] = format_date_string_objet($tab_valeurs[$objet][$nom_champs]);
                                            break;
                                    }
                                }
                            }
                        } else {// appliquer la valeur defaut
                            //                        echo '<br>defaut i :' . $i . ' ii :' . $ii . ' iiv :' . $iiv ;arbre($vvv);
                            //                            if(isset($vvv['nom_colonne']['options']['o_defaut'])) {
                            //                                $tab[$i]= $vvv['nom_colonne']['options']['o_defaut'];
                            //                                exit;
                            //                            }
                            //                            else
                            if (isset($vvv['defaut'])) {
                                //                            echo 'defaut----------------------------------$i :' . $i . ' ii ' . $ii . ' iiv ' . $iiv;

                                if (isset($vv[$objet]['type'])) {
                                    switch ($vvv['type']) {
                                        case 'date-eu':
                                            $tab_valeurs[$objet][$nom_champs] = format_date_string_objet($vv[$objet]['defaut']);
                                            break;
                                        default :
                                            $tab_valeurs[$objet][$nom_champs] = format_date_string_objet($tab_valeurs[$objet][$nom_champs]);
                                    }
                                } else {
                                    //                                echo 'pas de type';
                                    $tab_valeurs[$objet][$nom_champs] = $vvv['defaut'];
                                }
                            }
                        }
                    }
                }


                if (isset($tab_etat[$objet]['etat']) && $tab_etat[$objet]['etat'] == 'ok') {
                    $tab_etat[$objet]['compte_rendu'] = 'Traité antérieurement :';
                } else {
                    $tab_etat[$objet]['etat'] = 'LU';
                }


            }
        }

    }


    $tab_valeurs['paiement']['id_tresor'] = '4';

    return [$tab_valeurs, $tab_etat];
}



function importation_recherche_liaison($tab_l,$objet,$config,$nom_champs){

    $options = $config['options']['recherche_liaison'][$nom_champs];
    $nomQuery = (isset($options['fichier'])) ? $options['fichier'] . 'Query' : descr($objet . '.phpname') . 'Query';
    $f_o = (isset($options['origine']['objet'])) ? $options['origine']['objet'] : $objet;
    $c_o = (isset($options['origine']['champs'])) ? $options['origine']['champs'] : $nom_champs;

    if (isset($tab_l[$f_o][$c_o])) {

        $q = $nomQuery::create();
        $valeur = $tab_l[$f_o][$c_o];
        $filtre = (isset($options['filtre'])) ?  'filterBy' .champ_propel($options['filtre']) :  'filterBy' .champ_propel($c_o);
        $q  = $q->$filtre($valeur);
        if (isset($options['filtre'])) {
            if (isset($options['filtre']['o_c'])) {
                $filtre_c = 'filterBy' . champ_propel($options['filtre']['o_c']);
            }
            if (isset($options['filtre']['o_v'])) {
                $filtre_v = $options['filtre']['o_v'];
            }
            $q = $q->$filtre_c($filtre_v);
        }
        if (isset($options['filtre_order'])) {
            $order = 'orderBy' . champ_propel($options['filtre_order']['champs']);
            $order_sens = $options['filtre_order']['sens'];
            $q = $q->$order($order_sens);
        }

        $tab_objet_data_r = $q->find();

        $tab_id=[];
        if ($tab_objet_data_r){
            foreach ($tab_objet_data_r as $objet_data_r) {
                $get = (isset($options['retour'])) ? champ_propel($options['retour']) : champ_propel(descr($objet . '.cle'));
                $get = 'get' . $get;
                $data = $objet_data_r->$get();

                $tab_id[] = $data;

            }
        }


        return $tab_id;

    }

}




function importation_interprete_ligne_recherche( $tab_l, $tab_etat, $nb, $objet, $config, $archive = false) {




    if (!isset($tab_etat[$objet]['modification'])) {
        $tab_etat[$objet]['modification'] = false;
    }

    if ($archive)
        $nomQuery = ucfirst(descr($objet . '.prefixe')) . descr($objet . '.phpname') . 'sArchiveQuery';
     else
        $nomQuery = descr($objet . '.phpname') . 'Query';


    // Si l'id de l'objet est renseigné, on le recherche
    if (isset($tab_l[$objet]['id_' . $objet]) and is_array($tab_l[$objet]['id_' . $objet]) && count($tab_l[$objet]['id_' . $objet])==1) {

        $objet_data_r = $nomQuery::create()->findpk($tab_l[$objet]['id_' . $objet][0]);
    }

    // Sinon  on recherche avec les colonnes défini dans $config['options']['recherche']
    if (!(isset($objet_data_r) && $objet_data_r)) {
        if (isset($config['options']['recherche'])) {
            foreach ($config['options']['recherche'] as $nom_champs_recherche) {


                // recherche avec liaison
                if (isset($config['options']['recherche_liaison'][$nom_champs_recherche])) {

                    $tab_id = importation_recherche_liaison($tab_l,$objet,$config,$nom_champs_recherche);
                    if(!empty($tab_id)){
                        $c_i = (isset($options['retourimporte'])) ? $options['retourimporte'] : 'id_' . $objet;
                        $tab_l[$objet][$c_i]=$tab_id;
                        $tab_objet_data = $nomQuery::create()->findPks($tab_id);

                    }

                }
                // recherche sur champs de la table
                else {

                    if (!is_array($nom_champs_recherche))
                        $nom_champs_recherche = [$nom_champs_recherche];
                    $query = $nomQuery::create();
                    foreach($nom_champs_recherche as $n_champs){
                        $champ_propel = champ_propel($n_champs);
                        $valeur = (isset($tab_l[$objet][$n_champs])) ? $tab_l[$objet][$n_champs] : '';
                        $filtre = 'filterBy' . $champ_propel;
                        $query = $query->$filtre($valeur);
                    }
                    $tab_objet_data = $query->find();
                }


                // On trouvé quelques choses !

                if (isset($tab_objet_data) and $tab_objet_data->count()>0) {


                    $tab_etat[$objet]['creation_autorise'] = false;
                    foreach ($tab_objet_data as $objet_data) {
                        $tab_l[$objet]['id_' . $objet][] = $objet_data->getPrimaryKey();
                        $tab_l[$objet]['data'][$objet_data->getPrimaryKey()] = $objet_data->toarray();
                        if (isset($config['options']['pasdemodification']) and $config['options']['pasdemodification']) {
                            if ($tab_l[$objet]['date_fin'] == $objet_data->getdateFin()->format('Y-m-d')) {
                                $tab_etat[$objet]['etat'][$objet_data->getPrimaryKey() . ''] = 'EX';
                                $tab_etat[$objet]['compte_rendu'][$objet_data->getPrimaryKey() . ''] = 'Existant et  pas de modification ' . $tab_l['logiciel']['num_ligne'] . ' id :' . $tab_l[$objet][descr($objet . '.cle')];
                                $ok = false;
                            }
                        }
                        if ($archive) {
                            $tab_etat[$objet]['etat'][$objet_data->getPrimaryKey() . ''] = 'AR';

                            $ok = false;

                        }
                        $tab_etat[$objet]['modification'] = true;
                    }


                    $ok = true;
                    return [$ok, $tab_l, $tab_etat, $nb];
                }



            }



        }
    }

//    echo '<br>Importation 547 ligne en sortie avec retour faux';arbre($tab_l);
    $ok = false;
    return [$ok, $tab_l, $tab_etat, $nb];
}

function importation_interprete_ligne_ajoute_fichiers_lies($tab_l, $tab_etat, $nb, $objet, $config)
{
    if (isset($tab_etat[$objet]['etat']) && $tab_etat[$objet]['etat'] == "LU") {
//       echo '<br>Importation 559 :';
        if (isset($config['liens'])) {
//            echo '<br>Importation 563 :'; arbre($config['lien_prestation']);
            foreach ($config['liens'] as $k => $v) {//'prestation'=>'observation'
                $nomQuery = descr($k . '.phpname') . 'Query';
                $q = $nomQuery::create();
                $valeur = (isset($tab_l[$objet][$v])) ? $tab_l[$objet][$v] : '';
                if (isset($config['lien_' . $k])) {
                    $options = $config['lien_' . $k];
                }
                $nomQuery = (isset($options['cherche']['o_f'])) ? descr($options['cherche']['o_f'] . '.phpname') . 'Query' : descr($k . '.phpname') . 'Query';
                $findOne = (isset($options['cherche']['o_c'])) ? 'findOneBy' . champ_propel($options['cherche']['o_c']) : 'findOneBy' . champ_propel(descr($v));
                if ($valeur == '' and isset($options['cherche']['o_defaut'])) {
                    $valeur = $options['cherche']['o_defaut'];
                }
                $q = $nomQuery::create();
                if (isset($options['filtre'])) {
                    if (isset($options['filtre']['o_c'])) {
                        $filtre_c = 'filterBy' . champ_propel($options['filtre']['o_c']);
                    }
                    if (isset($options['filtre']['o_v'])) {
                        $filtre_v = $options['filtre']['o_v'];
                    }
                    $q = $q->$filtre_c($filtre_v);
                }
                $valeur = $tab_l[$objet]['montant'];
//                    echo '<br>Importation 545 ' . $q.' valeur '.$valeur.' '.$nomQuery.' filtre :'.$filtre_c.' ( '.$filtre_v.' ) retour :'.$findOne;arbre($options);
                $objet_data_l = $q->$findOne($valeur);
                if ($objet_data_l) {//l'objet lié
                    if (isset($options['alimente'])) {
                        foreach ($options['alimente'] as $k1 => $v1) {
                            $get = 'get' . champ_propel($options['alimente'][$k1]['o_c']);
                            $tab_l[$options['alimente'][$k1]['d_o']][$options['alimente'][$k1]['d_c']] =
                                $objet_data_l->$get();
//                                echo '<br> Importation 553 ' . $k1;arbre($v1);arbre($objet_data_l->$get());
                        }
                    } else {
                        $tab_l[$k] = $objet_data_l->toarray();
                    }
//                        echo '<br> Importation 557 lien : recherche le dernier lien';
                    if (isset($options['recherche_dernier'])) {//recherche a ameliorer
                        $nomQuery = nom_query($options['recherche_dernier']['origine']['objet']);
                        $f_o = (isset($options['recherche_dernier']['origine']['valeur'])) ? $options['recherche_dernier']['origine']['valeur'] : $objet;
                        $c_o = (isset($options['recherche_dernier']['origine']['champs'])) ? $options['recherche_dernier']['origine']['champs'] : $v;
//                          exemple   $valeur = (isset($tab_l['membre']['id_membre'])) ? $tab_l['membre']['id_membre'] : '';
                        $valeur = (isset($tab_l[$options['recherche_dernier']['origine']['valeur']][$c_o])) ? $tab_l[$f_o][$c_o] : '';
                        if ($valeur) {
                            $q = $nomQuery::create();
                            $filtre = 'filterby' . champ_propel($options['recherche_dernier']['origine']['champs']);
                            if ($valeur) {
                                $q = $q->$filtre($valeur);
                            }
                            $filtre2 = 'filterby' . champ_propel('id_prestation');
                            if (isset($options['recherche_dernier']['filtre_order'])) {
                                $order = 'orderBy' . champ_propel($options['recherche_dernier']['filtre_order']['champs']);
                                $order_sens = $options['recherche_dernier']['filtre_order']['sens'];
                            }
//                               echo '<br>Importation 573 '.$nomQuery.' '.$filtre.' '.$valeur.' '.$filtre2.' '.$tab_l[$objet]['id_prestation'].' '.$order.' '.$order_sens;
                            $objet_data1 = $q->findOne();
//                               arbre($objet_data1);
                            if ($objet_data1) {
                                $tab_l[$objet]['dataprecedent'] = $objet_data1->toarray();
                                if ($tab_l[$objet]['dataprecedent']['date_fin']) {
                                    $df = $tab_l[$objet]['dataprecedent']['date_fin'];
                                }

                                //TODO : Probème de format de date

                                $df = null;
                                $dd = calculeDateDebut($df,
                                    tab('prestation.' . $tab_l[$objet]['id_prestation'] . '.retard_jours'),
                                    tab('prestation.' . $tab_l[$objet]['id_prestation'] . '.periodique'));
                                echo '<br>Importation 634 probleme sur l\'envoi du parametre date a calculdatedebut <br>';
                                var_dump($dd);
                                $tab_l[$objet]['date_debut'] = $dd;
                                $tab_l[$objet]['date_fin'] = calculeDatefin($dd,
                                    tab('prestation.' . $tab_l[$objet]['id_prestation'] . '.periodique'));
                            }

                        }
                    }
                }
            }
            $tab = $tab_l[$objet];
            if (isset($tab_l[$objet]['date_fin']) and !isset($tab_l[$objet]['date_debut'])) {// model ccfd on récupere la date de fin de la  derniere cotisation annuel date flotante le jour si retard >30 jour
                $tab_l[$objet]['date_debut'] = calcule_DateDebut_origine_DateFin(new DateTime($tab_l[$objet]['date_fin']),
                    $tab_l['prestation']['periodique']);
            }
        }
    }
    $ok = true;
    return [$ok, $tab_l, $tab_etat, $nb];
}

Function importation_interprete_ligne_compare($tab_l, $tab_etat, &$nb, $objet, $config)
{
//    echo '<br>Importation compare 602';arbre($tab_l[$objet]); arbre($config);//   exit;
    if (isset($tab_l[$objet]['modification']) and $tab_l[$objet]['modification'] === true and isset($tab_etat[$objet]['etat']) && $tab_etat[$objet]['etat'] == "LU") {
//        echo '<br>Importation 659 compare :'.$tab_l[$objet]['modification'];
//        arbre($tab_l[$objet]);
        if (!isset($tab_l[$objet]['data'])) {
//         echo '<br>Importation 662 compare :'.$tab_l[$objet]['modification']; exit;
        }
//    echo '<br>Importation 665 debut ligne compare '.$objet;
        foreach ($tab_l[$objet] as $k => $v) {
            if (is_array($v)) {
                if ($k != 'data') {
                    echo '<br>tableau :' . $k;
                    arbre($v);
                };
            } else {
                $tab_l[$objet][$k] = trim($tab_l[$objet][$k]);
                $v = trim($v);
                if (array_key_exists($k, $tab_l[$objet]['data'])) {
                    $tab_l[$objet]['data'][$k] = trim($tab_l[$objet]['data'][$k]);
                    $v1 = $tab_l[$objet]['data'][$k];
                    //Non utilisé actuellement
//                    if (isset($config[$k]['options']['type'])) {
//                        switch ($config[$k]['options']['type']) {
//                            case 'date-eu' :
//                                $v1 = substr($v1, 0, 10);
////                                    break;
//                            case 'TextType' :
//                                $v1 = substr($v1, 0, 10);
//                        }
//                            echo '<br><br>' . $config[$k]['options']['type'] . ' dans le fichier  objet ';
//                            echo $data[$k] . ' date eu ' . $v1 . ' -> ' .var_dump( $v1);
//                    }
                    if ($v1 != $v) {
//                        echo '<br>Importation 688 différence objet :' . $objet . ' champs :' . $k . ' :' . $v1 . '->' . $v.'-';
                        if ($v1 == '') {
                            $tab_l[$objet]['ajoute'][$k] = $v;
                        } else {
                            $tab_l[$objet]['modifie'][$k] = $v;
                        }
                    } else {
//                        echo '<br>Importation 692 idem - - - - objet :' . $objet . ' champs :' . $k . ' :' . $v;
                    }
                } else {
//                    echo '<br>Importation 695 autre- - - - objet :' . $objet . ' champs :' . $k . ' :' . $v;
                }
            }
        }
//        foreach ($config['options']['obligatoire'] as $k => $v) {
//            if (is_array($v)) {
//                if ($k!='data') {echo '<br>tableau :' . $k;arbre($v);};
//            } else {
//                echo '<br>Importation compare obligatoire 653  :' . $objet . ' champs :' . $k . ' :' . $v;
//            }
//        }

//// a voir        $id_individu_titulaire = $objet_data->getidIndividuTitulaire();
    }
    $ok = true;
    return [$ok, $tab_l, $tab_etat, $nb];
}

function importation_interprete_ligne_data_obligatoire($tab_l, $tab_etat, $nb, $objet, $obligatoire)
{
//    echo '<br>Importation  656 '.$objet.' etat :'.$tab_l[$objet]['etat'].' modification :'.$tab_l[$objet]['modification'].' debut de obligatoire ';arbre($tab_l[$objet]);
    $ret = false;
    if (isset($tab_etat[$objet]['etat']) && $tab_etat[$objet]['etat'] == "LU") {
        $msg = 'Données obligatoires non trouvées :';
        $ret = $msg;
        $rapport = (array_keys($obligatoire, 'rapport', 'strict')) ? $obligatoire['rapport'] : false;
        $detail = (array_keys($obligatoire, 'detail', 'strict')) ? $obligatoire['rapport'] : false;
        $detail = true;
        $ou = (array_keys($obligatoire, 'ou', 'strict')) ? false : true;
        $et = (array_keys($obligatoire, 'et', 'strict')) ? false : true;
        foreach ($obligatoire as $k => $v) {
            if (!is_array($v)) {
                $v = array($v);
            }
            foreach ($v as $k1 => $v1) {
//                echo '<br> Importation obligatoire 669 Objet '.$objet.' champs '.$k1.' :'.$k ;//var_dump($tab_l[$objet][$v1]);
                switch ($k) {
                    case 'ou' :
                        if (isset($tab_l[$objet][$v1]) and $tab_l[$objet][$v1]) {
//                            echo '<br> Importation obligatoire 673 Objet '.$objet.' champs '.$v1.' :' ;var_dump($tab_l[$objet][$v1]);
                            $ou = true;
                        } else {
                            if ($detail) {
                                $ret .= ' ' . $v1 . ', ';
                            }
                            $msg .= ' ' . $v1;
                        }
                        break;
                    case 'et' :
                        if (!isset($tab_l[$objet][$v1]) or !$tab_l[$objet][$v1]) {
                            if (!isset($tab_l[$objet]['data'][$v1])) {
                                if ($detail) {
                                    $ret .= ' ' . $v1 . ', ';
                                }
                            }
                            $ou = false;
                            $msg .= ' ' . $v1;
                        }
                        break;
                    case 'creation_autorise' :
                        if ($ou and $v1) {
                            $tab_l[$objet][$k] = $v1;
                        }
                        break;
                }
//                echo '<br> operateur $k1 :' . $k1 . ' champs :' . $v1 . ' ou :' . $ou . ' et ' . $et;
            }
        }
        if ($ou and $et) {
            $ret = false;
        } else {
            $tab_etat[$objet]['etat'] = 'NC';
            $tab_l[$objet]['compte_rendu'] = $msg;
            $nb[$objet]['NC']++;
        }
//        echo '<br>Importation 710';arbre($tab_l[$objet]['etat']);echo '<br> message '.$msg.' erreur :'.$ret;
    }
    $ok = true;
    return [$ok, $tab_l, $tab_etat, $nb];
}

Function importation_interprete_prepare_detail_modif($tab_l, $tab_etat, $nb, $objet, $detail_modification, &$modif)
{

    if (isset($tab_etat[$objet]['etat']) && $tab_etat[$objet]['etat'] == "LU" and $tab_etat[$objet]['modification']) {
//        echo '<br>Importation 712 '.$objet.' etat :'.$tab_l[$objet]['etat'].' modification :'.$tab_l[$objet]['modification'].' debut de detail_modif ';//arbre($tab_l[$objet]);
        $i = 0;
        foreach ($detail_modification as $v) {
            //todo ajouter des options de format si necessaire MAJ, Min, min
            if (isset($tab_l[$objet][$v])) {
                if (isset($tab_l[$objet]['data'][$v])) {
                    if ($tab_l[$objet]['data'][$v] != $tab_l[$objet][$v]) {

                        /*      $modif['DM'][$objet][$tab_l['logiciel']['num_ligne']][$v]['data'] = $tab_l[$objet]['data'][$v];
                              $modif['DM'][$objet][$tab_l['logiciel']['num_ligne']][$v]['neuf'] = $tab_l[$objet][$v];*/
                        $i++;
                    }
                } else {
                    /*  $modif['DM'][$objet][$tab_l['logiciel']['num_ligne']][$v]['data'] = '';
                      $modif['DM'][$objet][$tab_l['logiciel']['num_ligne']][$v]['neuf'] = $tab_l[$objet][$v];*/

                }
            }
        }
        $tab_etat[$objet]['etat'] = 'DM';
        $nb[$objet]['DM'] = $nb[$objet]['DM'] + $i;
    }
    $ok = true;
    return [$ok, $tab_l, $tab_etat, $nb];
}

Function importation_interprete_prepare_detail_creation($tab_l, $tab_etat, $nb, $objet, $champs_objet, &$modif)
{
    if (isset($tab_etat[$objet]['creation_autorise']) and $tab_etat[$objet]['creation_autorise'] == true and
        $tab_etat[$objet]['etat'] = "LU"
    ) {
//        echo '<br>Importation 746 '.$objet.' etat :'.$tab_l[$objet]['etat'].' modification :'.$tab_l[$objet]['modification'].' debut de detail_creation ';//arbre($tab_l[$objet]);
        $i = 0;
        foreach ($champs_objet as $k => $v) {
//            echo '<br>Importation 753 '.$k; if (isset($v['options'])) arbre($v['options']);
            if (isset($v['options']['nom_colonne']['options']['champs'])) {
                $ch = $v['options']['nom_colonne']['options']['champs'];
            } else {
                $ch = $k;
            }
            if ($ch > ' ') {
                if (isset($tab_l[$objet][$k]) and $tab_l[$objet][$k] > '') {
                    $va = $tab_l[$objet][$k];
                    if (isset($v['options']['nom_colonne']['options']['devant']) and $v['options']['nom_colonne']['options']['devant']) {
                        $va = $v['options']['nom_colonne']['options']['devant'] . $va;
                    }
                    if (isset($v['options']['nom_colonne']['options']['derriere']) and $v['options']['nom_colonne']['options']['derriere']) {
                        $va = $va . $v['options']['nom_colonne']['options']['derriere'];
                    }
                    if (isset($champs_objet[$ch]['options']['nom_colonne']['options']['concat']) and $champs_objet[$ch]['options']['nom_colonne']['options']['concat']) {
                        if (isset($modif['DC'][$objet][$tab_l['logiciel']['num_ligne']][$ch]['neuf'])) {
                            $modif['DC'][$objet][$tab_l['logiciel']['num_ligne']][$ch]['neuf'] .= '-' . $va;
                        } else {
                            $modif['DC'][$objet][$tab_l['logiciel']['num_ligne']][$ch]['neuf'] = $va;
                        }
                    } else {
                        $modif['DC'][$objet][$tab_l['logiciel']['num_ligne']][$ch]['neuf'] = $va;
                        $i++;
                    }
//                    var_dump( $modif['DC'][$objet][$tab_l['logiciel']['num_ligne']][$ch]['neuf']);
                }

            }

        }
//        echo '<br>'.$i.' ';
        if ($i) {
            $tab_etat[$objet]['etat'] = 'DC';
            $nb[$objet]['DC'] = $nb[$objet]['DC'] + $i;
        }
    }
    $ok = true;
    return [$ok, $tab_l, $tab_etat, $nb];
}

function importation_interprete_vers_etape_7($tab_l, $tab_etat, $nb, $objet, $config)
{
    if (isset($tab_etat[$objet]['etat']) && $tab_etat[$objet]['etat'] == "LU") {
 //       echo '<br>Importation 766 ' . $objet . ' etat :' . $tab_etat[$objet]['etat'] . ' modification :' . $tab_etat[$objet]['modification'] . ' debut de vers etape 7 ';//arbre($tab_l[$objet]);
//    import_controle_ligne_individu(
//        $num_ligne,    &$tab_l, $objet,  $tab_entete,    &$nb,    &$message,
//        &$m_nom,    &$m_individu,    &$a_individu) {

// todo individu
//    $individu=individu_recherche($nom, $prenom, $id_individu, $email,$id_individu_titulaire);
//    return array($membre, $individu);
////        $message .= '<br>Distortion entre l\'individu titulaire dans la base et celui present dans l\'importation : ligne ' . $num_ligne . ' base  :' . $individu->getidindividu() . '  import :' . $tab_l['id_individu'];

// todo cotisation
//                $ancien = cotisation_validite($num_ligne, $tab_l, $message, 1, 'cot', $options);
//                if ($ancien and $tab_l[$objet]['OKcot'] == 'LU') {//prestation trouvée et SR n'existe pas
//                    $nb['m_cot']++;
//                    $m_cot[$nb['m_cot']] = array(
//                        'ligne' => $num_ligne,
//                        'id' => $tab_l['cotid_servicerendu'],
//                        'remplace' => $ancien,
//                        'par' => $tab_l['cotid_prestation'],
//                        'champ_destination' => 'id_prestation',
//                        'objet' => 'servicerendu',
//                        'inform' => $tab_l['nom'] . " " . $tab_l['ville'],
//                        //todo  dans l'utilisation les informations de l'enregistrement sont dans la lignes
//
//todo paiement                    ajouter  OK ok ou bon de cotisation
//                    paiement_validite($num_ligne, $tab_l, $message, $options, 'cot', $nb['cotp'], $m_cotp);
//
// todo mot
//             if ($options['avec_mot']) {
//                mot_validite($tab_l['cotid_mot'], $tab_l['cotnom_mot'], $tab_entete['motcot']);
        $tab_etat[$objet]['etat'] = 'VA';
        $nb[$objet]['VA']++;
        $mes = '';
        $mes2 = '';
        $mes2 = (isset($tab_l[$objet]['modification']) and $tab_l[$objet]['modification'] == true) ? ' Mise à jour ' : ' Création ';
        if (isset($tab_l[$objet][descr($objet . '.cle')])) {
            if (is_array($tab_l[$objet]['id_' . $objet])) {
               // arbre($tab_l[$objet]['id_' . $objet]);
               // exit;
            }
            $mes = '-' . $tab_l[$objet]['id_' . $objet];
        }
        if (isset($tab_l[$objet]['nom'])) {
            $mes = '-' . $tab_l[$objet]['nom'];
        }
        $tab_l[$objet]['compte_rendu'] = $mes2 . $mes;
    } else {
        if (isset($tab_l[$objet]['id_' . $objet])) {
            $temp = ' objet id :' . implode(',', $tab_l[$objet]['id_' . $objet]);
        } else {
            $temp = '';
        }
//        echo '<br> etat :' . $tab_l[$objet]['etat'] . ' ligne :' . $tab_l['logiciel']['num_ligne'].$temp;
    }
    $ok = true;
    return [$ok, $tab_l, $tab_etat, $nb];
}

function importation_valide($tab_l, $tab_etat, $nb, $objet, $config, $num_ligne)
{
    echo '<br>Importation 829 importation_valide les variables :';
    arbre($tab_l[$objet]);
    if ($tab_etat[$objet]['etat'] == 'VA') {
        global $app;
        $ok = false;
        $cle = descr($objet . '.cle');
        $objet_phpname = descr($objet . '.phpname');
        $nomQuery = descr($objet . '.phpname') . 'Query';
        echo '<br>Importation 838 ecrit la ligne :' . $num_ligne . ' objet :' . $objet . ' php name :' . $objet_phpname;
        if (!isset($tab_l[$objet]['compte_rendu'])) {
            $tab_l[$objet]['compte_rendu'] = ' ';
        }
        $objet_data = null;
        if (isset($tab_l[$objet]['modification']) and $tab_l[$objet]['modification']) {
            $modification = true;
            $objet_data = $nomQuery::create()->findpk($tab_l[$objet][$cle]);
            echo ' **** Modifie';
            arbre($config['options']['ecrit_modification']);
            foreach ($config['options']['ecrit_modification'] as $v) {
                if (isset($tab_l[$objet][$v])) {
                    $vpropel = 'get' . champ_propel($v);
                    $spropel = 'set' . champ_propel($v);
                    if ($tab_l[$objet][$v] != $objet_data->$vpropel()) {
                        echo '<br>modif ' . $v . '-' . $spropel . ' :' . $tab_l[$objet][$v];
                        $objet_data->$spropel($tab_l[$objet][$v]);
                        $ok = true;
                        $nb[$objet]['MO']++;
                    }
                }
            }
        }
        if (isset($tab_l[$objet]['creation_autorise']) and $tab_l[$objet]['creation_autorise']) {
            $modification = false;
            $objet_data = new $objet_phpname();
            echo ' **** Ajout';
            arbre($config['options']['ecrit_creation']);
            foreach ($config['options']['ecrit_creation'] as $v) {
                if (isset($tab_l[$objet][$v]) and $tab_l[$objet][$v]) {
                    $spropel = 'set' . champ_propel($v);
                    if ($v == 'temporaire1') {
                        exit;
                    }
                    $objet_data->$spropel($tab_l[$objet][$v]);
                    echo '<br>cree ' . $v . ' - ' . $spropel . ' :' . $tab_l[$objet][$v];
                    $ok = true;
                    $nb[$objet]['AJ']++;

                }
            }
        }
        if ($ok) {
            echo '<br> Sauve';
            $ok1 = $objet_data->save();
            if ($ok1) {
                if (!$modification) {
                    $clepropel = 'get' . champ_propel($cle);
                    $tab_l[$objet][$cle] = $objet_data->$clepropel();
                }
                $tab_etat[$objet]['etat'] = 'EC';
                $tab_etat[$objet]['compte_rendu'] .= ' Importation 866 pas d\'enregistrement de ' . $objet . ' ligne ' . $num_ligne;
                $tab_etat[$objet]['etat'] = 'EC';
                $nb[$objet]['EC']++;
                if (isset($config['options']['pas_de_log']) and $config['options']['pas_de_log'] == false) {
                    echo '<br> Ecrit Log';
                } else {
                    Log::EnrOp($modification, $objet, $tab_l[$objet][$cle]);
                }
            } else {//Todo exit pour vérification de l'incident
                $nb[$objet]['EE']++;
                $nb['ecritures_echoues']++;
                exit;
            }
            if ($objet = 'individu' and isset($tab_l['membre'])) {
                // Todo a parametrer
                if (!isset($tab_l['membre']['id_individu'])) {
                    $tab_l['membre']['id_individu'] = $tab_l[$objet]['id_individu'];
                }
            }
            if ($objet = 'membre' and $tab_l[$objet]['id_individu'] and $tab_l[$objet]['id_membre']) {
                // Todo a parametrer
                $relation = MembreIndividuQuery::create()->filterByidMembre($tab_l[$objet]['id_membre'])->findOneByidIndividu($tab_l[$objet]['id_individu']);
                if (!$relation) {
                    $relation = new Membreindividu();
                    $relation->setidIndividu($tab_l[$objet]['id_individu']);
                    $relation->setidMembre($tab_l[$objet]['id_membre']);
                    $relation->save();
                }
            }

        } else {
            $tab_l['compte_rendu'] .= ' rien a créer ou modifier sur ' . $objet . ' ligne ' . $num_ligne;
        }
    } else {
//        echo '<br> objet :'.$objet.' ligne ' . $num_ligne. ' etat :'.$tab_l['etat'].' compte rendu :';
//        if (isset($tab_l['compte_rendu'])and $tab_l['compte_rendu']) echo $tab_l['compte_rendu'];
    }
    return [$ok, $tab_l, $tab_etat, $nb];
}

function compte_rendu1(&$proposition, $id, $fichier, $etape, $action)
{
    global $app;
    $proposition['message'] .= 'Ligne des noms de colonne :' . $proposition['nb']['ligne_entete'];
    $proposition['message'] .= '<br>' . $proposition['nb']['lignes_lues'] . ' lignes lues ';
    $proposition['message'] .= '<br>' . ($proposition['nb']['lignes_interprete']) . ' lignes interprétées ';
    $proposition['message'] .= '<br>' . ($proposition['nb']['lignes_ecrites']) . ' lignes écrites ';
    $proposition['message'] .= '<br>' . ($proposition['nb']['lignes_compte_rendu']) . ' lignes compte rendus ';
    $proposition['message'] .= '<br>' . $proposition['nb']['ecritures_echoues'] . ' Erreurs  ecritures echouées';
//    $proposition['message'] .= '<br>' . $proposition['nb']['ok'] . ' lignes déja enregistrées et sans modification ( ok )';
    $proposition['message'] .= '<br>  fin   a : ' . date('Y-m-d H:i:s');
    $proposition['message'] .= '<br>Source    : ' . $fichier;
    $proposition['message'] .= '<br>Detail par  objet ';
    foreach ($proposition['objets'] as $objet) {
        $proposition['message'] .= '<br> ' . $objet;
        foreach ($proposition['nb'][$objet] as $k => $v) {
            if ($v > 0) {
                $proposition['message'] .= '<br>' . $k . '  : ' . $v;
            }
        }
    }
    $proposition['nom_output'] = $id . '_' . trim(substr($fichier, 0,
            10)) . '_' . date('Y-m-d H-i-s') . '.' . 'txt';// $export_extension;
    $proposition['url'] = '<a href="' . $app->path('download',
            ['fichier' => $proposition['nom_output']]) . '">Télécharger le fichier résultat</a>';
    $proposition['message'] .= '<br><br>' . $proposition['url'] . '<br><br>' .
        'Résultat : ' . $proposition['nom_output'];
    Log::EnrOp(false, 'import', $id, '', $proposition['url']);
    return 'fin valide';
}

function date_texte($date)
{
    if (is_object($date)) {
        return $date->format('d-m-Y');
    }

    return $date;
}

/*
function export($proposition, $id)
{
    global $app;
//    $inputFileType = 'Excel2007';
//    $inputFileName =$fichier;//$app['document.path'].'/matrice'.$vannee.'.xlsx';
// pour un chargement
//    $contenu_fic = transfert_fichier($proposition['champs']['fichier']);
//    $contenu_fic = PHPExcel_IOFactory::createReader($inputFileType);
//    $contenu_fic->load($inputFileName);
// pour un nouveau fichier
    $resultat = new PHPExcel();
// si deja chargé rien

    $resultat->setActiveSheetIndex(0);  //set first sheet as active
    $pas = 1000;
//    if (isset($proposition['nb']['lignes_compte_rendu'])) $proposition['nb']['lignes_compte_rendu']= $proposition['nb']['ligne_entete'] - 2;
//    $debut = $proposition['nb']['lignes_compte_rendu'] + 1;
    $debut = 11;
    $fin = min($debut + $pas, $proposition['nb']['lignes_lues']);
    for ($num_ligne = $debut; $num_ligne <= $fin; $num_ligne++) {
        $ligne_data = importligneQuery::create()->filterByidImport($id)->filterByLigne($num_ligne)->findone();
        if ($ligne_data) {
            $tab = $ligne_data->getVariables();
            $colonne = 0;
            foreach ($tab as $texte) {

////            foreach ($proposition['lignes'] as $num_ligne => $tab) {
////        foreach ($proposition['nb']['colonnes'] as $colonne => $nom) {
////            if (isset($proposition['lignes'][$num_ligne][$nom['variable']])) {
////                switch ($nom['variable']) {
////                    case 'cotdate_fin' :
////                    case 'cotdate_debut' :
////                     if (is_object($proposition['lignes'][$num_ligne][$nom['variable']])) {
////                            $texte = $proposition['lignes'][$num_ligne][$nom['variable']]->format('d/m/Y');
////                        } else {
////                            $texte = $proposition['lignes'][$num_ligne][$nom['variable']];
////                        }
////                        break;
////                    case 'cotdate_cheque' :
////                        if (strlen(($proposition['lignes'][$num_ligne][$nom['variable']])) >= 10) {
////                            $t = explode('-', $proposition['lignes'][$num_ligne][$nom['variable']]);
////                            $texte = $t[2] . '-' . $t[1] . '-' . $t[0];
////                        }
////                        break;
////                    default :
////                        $texte = $proposition['lignes'][$num_ligne][$nom['variable']];
////                }
//////                $cellule = $contenu_fic->getActiveSheet()->getCellByColumnAndRow($colonne, $num_ligne)->getValue();
//////                if (!isset($cellule) or $cellule <= '  !') {
                $colonne++;
                $resultat->getActiveSheet()->setCellValueByColumnAndRow($colonne, $num_ligne, $texte);
//////                }
            }
        }
    }
    $resultat->getActiveSheet()->setCellValueByColumnAndRow(7, 6,
        'Pour prendre en compte une modification déja validée : effacer OK sur la ligne concernée');
    $resultat->getActiveSheet()->setCellValueByColumnAndRow(5, 5, date('d/m/Y'));
    $resultat->getActiveSheet()->setCellValueByColumnAndRow(7, 5, date('H-i-s'));


//prepare download
//    $filename=mt_rand(1,100000).'.xls'; //just some random filename
//    header('Content-Type: application/vnd.ms-excel');
//    header('Content-Disposition: attachment;filename="'.$filename.'"');
//    header('Cache-Control: max-age=0');
//    $objWriter = PHPExcel_IOFactory::createWriter($objTpl, 'Excel5');  //downloadable file is in Excel 2003 format (.xls)
//    $objWriter->save('php://output');  //send it to user, of course you can save it to disk also!
//    exit; //done.. exiting!
    if (!isset($proposition['nom_output'])) {
        $proposition['nom_output'] = date('Y-m-d H-i-s') . '.xlsx';
    }
    $outputFileName = $app['output.path'] . '/' . $proposition['nom_output'];
//    export_extension($outputFileType,$proposition['param']['typefichier']);
//    $objWriter = PHPExcel_IOFactory::createWriter($resultat, $outputFileType);
    $objWriter = new PHPExcel_Writer_Excel2007($resultat);
    $objWriter->setOffice2003Compatibility(true);
//    $objWriter->save($outputFileName);
//    echo $outputFileName;
//    exit;
    return $proposition['message'];
}
*/
function rapportfinal_csv($proposition, $id)
{
    global $app;
    $message = '';
    $indice = 0;
    $pas = 5000;
    $debut = 1;
    if (isset($proposition['nb']['ligne_entete'])) {
        $debut = $proposition['nb']['ligne_entete'] + 1;
    }
    $export = '';
    $fin = min($debut + $pas, $proposition['nb']['lignes_lues']);
    echo 'Importations 1064 id :' . $id . ' debut ' . $debut . ' fin : ' . $fin;
    for ($num_ligne = $debut; $num_ligne <= $fin; $num_ligne++) {
        $ligne_data = ImportligneQuery::create()->filterByidImport($id)->filterByLigne($num_ligne)->findone();
        if ($ligne_data) {
            $tab = json_decode($ligne_data->getVariables(), true);
//            arbre($tab);
//            if ( is_array($tab))
//                $export .='tableau ligne '.$indice. PHP_EOL;
//            else {
//            $export .= str_replace(array("\r\n", "\n", "\r", "\""), '', implode("\t", $tab)) . PHP_EOL;
//             }
            $indice++;
        }
    }
//    echo'Importation 1047';
//    exit;
//    if ($indice == 0) {
//        $message .= 'Aucun ' . $app->trans('adherent') . '<BR />';
//    } else {
//        header("Content-type: application/vnd.ms-excel");
//        header("Content-disposition: attachment; filename=\"import_resultat_" . time() . ".csv\"");
//        echo($export);
//    }
    return;
}

/*
function isSerialized($str)
{
    return ($str == json_encode(false) || @json_decode($str) !== false);
}
*/

function importligne_creation($id_import, $num_ligne, $statut, $tab, $observation)
{
    $objet_data = ImportligneQuery::create()->filterByIdImport($id_import)->filterByLigne($num_ligne)->findOne();
    if (!$objet_data) {
        $objet_data = new importligne();
    }
    $objet_data->setidImport($id_import);
    $objet_data->setligne($num_ligne);
    $objet_data->setStatut($statut);
    $objet_data->setVariable($tab);

    $objet_data->setObservation($observation);
    $objet_data->save();
}

/*
function import_lecture_cellule($data_fic, $extension, $colonne, $ligne)
{
    switch ($extension) {
        case "xlsx":
            return $data_fic->getActiveSheet()->getCell([$colonne][$ligne])->getValue();
    }
}


function import_lecture_entete_premier_mot(&$cellule = '')
{
//todo pour les titres de colonnes sur plusieurs mots et/ou lignes
    if (isset($cellule) and $cellule >= ' !') {
        $pos = strpos($cellule, " ");
        if ($pos === false) {
            $pos = strpos($cellule, "\n");
        }
        if ($pos === false) {
            $pos = strpos($cellule, "\r");
        }
        if ($pos === false) {
            $pos = strpos($cellule, "\t");
        }
        if (!$pos === false) {
            $cellule = trim(substr($cellule, 0, $pos));
        }
    }
    return $cellule;
}

*/

function import_modifie($id, $etape, $original, $info, $fin = 0, $etape_termine = false)
{
    $objet_data = ImportQuery::create()->findOneByidImport($id);
    $modif = $objet_data->getModification();
    $objet_data->setAvancement($etape);
    if ($etape_termine) {
        if ($etape == '7') {
            unset($modif['etapes']);
        } else {
            for ($i = 1; $i <= $etape; $i++) {
                unset($modif['etapes'][$i]);
            }

            if (isset($info['nb']['ligne_entete'])) {
                $ligne_en_cours = $info['nb']['ligne_entete'];
            } else {
                $ligne_en_cours = 1;
            }
        }
    } else {
        $ligne_en_cours = $fin;
    }
    $objet_data->setLigneEnCours($ligne_en_cours);
    if (isset($info['nb']['lignes_valide'])) {
        $objet_data->setNbLigne($info['nb']['lignes_valide']);
    }

    //   echo '<br>Importation 1209 Modifie  etape termine :'.$etape_termine.' $ligne_en_cours :'.$ligne_en_cours.' $fin :'.$fin. ' etape :'.$etape;    arbre($modif['etapes']);

    $modif[] = array(
        'action' => $etape,
        'detail' => $etape . ' fin ligne ' . $fin,
        'date' => date('H:i:s'),
        'operateur' => suc('operateur.id')
    );
    $objet_data->setModifications(json_encode($modif));
    if ($original) {
        $objet_data->setOriginals(json_encode($original));
    }
    if ($info) {
//        unset($info['message']);  echo 'avant save 1226<br>xx'.json_encode($info);
        $objet_data->setInfo($info);
    }

    $objet_data->save();
    return;
}
/*
function mot_validite(&$id_mot, &$nom_mot, $motx, $objet)
{
//    echo "arrivé dans mot validité ".$id_mot.'--'.$nom_mot.'**'.$motx['id_defaut'].'<br>';
    $nom_mot = trim($nom_mot);
//    echo '<br>ligne 1091 '. $id_mot.'--'.$nom_mot.'**'.$motx['id_defaut'].'<br>';
    if (is_int($id_mot)) {
        $trouve = array_search($id_mot, $motx['tab_mot']);
        if ($trouve) {
            $nom_mot = $trouve;
            return;
        }
    } elseif (isset($nom_mot) and $nom_mot >= '!' and isset($motx['tab_mot'][strtolower($nom_mot)])) {
//        echo $id_mot.'--'.$nom_mot.'**'.$motx['id_defaut'].'ligne 1084<br>';
        $trouve = $motx['tab_mot'][strtolower($nom_mot)];
        if ($trouve) {
            $id_mot = $trouve;
            return;
        }
    } else {
//        echo $id_mot.'--'.$nom_mot.'**'.$motx['id_defaut'].'ligne 1091<br>';
        $nom_mot = trim($id_mot);
        $id_mot = 0;
        if (isset($motx['tab_mot'][strtolower($nom_mot)])) {
            $id_mot = $motx['tab_mot'][$nom_mot];
        }
    }
    if (!$id_mot and isset($motx['id_defaut']) and $motx['id_defaut'] >= 1) {
        $id_mot = $motx['id_defaut'];
        $nom_mot = $motx['tab_mot'][$motx['id_defaut']];
    }
//    echo 'ligne 1525'.$id_mot.'--'.$nom_mot.'**'.$id_mot_defaut.'<br>';
    return;
}
*/