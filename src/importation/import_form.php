<?php
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\Extension\Core\Type\FileType;
use Symfony\Component\Form\Extension\Core\Type\FormType;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\Form\FormError as FormError;
use Symfony\Component\Validator\Constraints as Assert;

function import_formation()
{
    return;

}


function import_form()
{
    global $app;
    $args_rep = [];
    $request = $app['request'];
    $data = array();
    $modification = false;
    $id = sac('id');
    $param = pref('import');
    include_once($app['basepath'] . '/src/importation/importations.php');

    $tab_modele = array_flip(importation_liste_modele());
    foreach($tab_modele as $modele=>&$val){
        $val= importation_variables($modele);
        $tab_modele_choice[$val['nom']]=$modele;
    }

    $data['modele'] = $request->get('modele');
    if ($id) {
        $objet_data = charger_objet();
        $modification = true;
        $data = $objet_data->toArray();
        $data['etapes'] = $objet_data->getModification()['etapes'];
    } else {
        $data['import_etapes'] = $param['etapes'];
        $data['avancement'] = '0';
    }
    $builder = $app['form.factory']->createNamedBuilder('import', FormType::class, $data);
    formSetAction($builder);
    $bt = 'Lancer (1 à 2 secondes par lignes)';
    if (!$modification) {
        //todo voir gestion en js auto effacer import etapes
        $builder->add('fichier', FileType::class,
            array(
                'data' => '',
                'label' => $app->trans('import_fichier'),
                'constraints' => new Assert\NotBlank()
            ));
    }
    if (!$data['modele']) {
        $builder->add('modele', ChoiceType::class, array(
            'label' => $app->trans('importation_modele'),
            'choices' => $tab_modele_choice,
            'extra_fields_message' => 'foobar',
        ));
    }
    $form = $builder
        ->add('import_etapes', ChoiceType::class, array(
            'label' => $app->trans('importation_etapes'),
            'multiple' => true,
            'expanded' => true,
            'choices' => choix_etape($data),
            'extra_fields_message' => 'foobar',
            'attr' => array('inline' => true)

        ))
        ->add('submit', SubmitType::class,
            array('label' => $app->trans($bt), 'attr' => array('class' => 'btn-primary')))
        ->getForm();

    $form->handleRequest($request);

    if ($form->isSubmitted()) {
        $data = $form->getData();
        if (!$modification) {
            $file = $data['fichier'];
            if ($file) {
                $nom = $file->getClientOriginalName();
                $extension = substr($nom, strrpos($nom, '.'));
                if ($extension) {
                    $extension = substr($extension, 1);
                }

                //todo ajouter par la suite la detection dans le fichier du format (quelque soit sont extention)  et le controle des formats traités dans importations
                if ($tab_modele[$data['modele']]['extension'] != $extension) {
                    $builder->setRequired(false);
                    $form->addError(new FormError($app->trans('import_extention_nongeree') . ' ' . $nom . ' ' . array_search($extension,
                            $param['extension']) . ' ' . $extension));
                }
            }
        }

        if ($form->isValid()) {// todo limite de 2Mo pour CSV le 5/1/2017 Adav
            if (!$modification) {
                $file = $data['fichier'];
                $nom = $data['modele'] . "." . $file->getClientOriginalName();
                $mime = $file->getmimeType();
                $size = $file->getsize();
                $error = $file->geterror();
                $pathname = $file->getpathName();
                $nomprovisoire = $file->getfileName();
                $tempextension = $file->guessExtension();
                $controlemd5 = $file->guessExtension();
                if (!$tempextension) {
                    $Extension = 'bin';
                }//                     Extension ne peut pas être deviné
                $file->move($app['upload.path'], $nom);
                //todo ajouter MD5
                $objet_data = new Import();
                $objet_data->setModele($data['modele']);
                $objet_data->setAvancement(1);
                $objet_data->setNom($nom);
                $objet_data->setNomprovisoire($nomprovisoire);
                $objet_data->setTaille($size);
                $objet_data->setControlemd5("xx");

                $objet_data->setOriginals(json_encode(array('DC' => array(), 'DM' => array())));
                $tab_info = [
                    'options' => $tab_modele[$data['modele']],
                    'message' => 'debut',
                    'nb' => [
                        'valide' => 0,
                        'curseur'=>0,
                            ],
                    'url' => '<a href=" ">Pas de compte rendu a telecharger</a>'
                ];

                $objet_data->setInformation($tab_info);
                $objet_data->setNbLigne(0);
            }
            $temp = $objet_data->getModification();
            if ($data['import_etapes']) {
                foreach ($data['import_etapes'] as $k => $v) {
                    $temp['etapes'][$v] = pref('import.etapes.' . $v);
                }
                $objet_data->setModifications(json_encode($temp));

            }
            $objet_data->save();
            $id = $objet_data->getidImport();

            $ok = true;
            // Met au meme niveau les autoincrement
            autoincrement_idem('membre', 'asso', 'individu', 'asso');
            $url_redirect = $app->path('import_form',
                ['action' => 'attente', 'id_import' => $id, 'modele' => $data['modele']]);
        }
    }
    return reponse_formulaire($form, $args_rep);
}




function action_import_form_attente()
{
    global $app;
    $request = $app['request'];
    $id = $request->get('id_import');
    $url = $app->path('import_form', ['action' => 'moteur', 'id_import' => $id, 'modele' => $request->get('modele')]);
    $url_redirect = $app->path('import_form', ['action' => 'resultat', 'id_import' => $id]);
    $args_twig = array(
        'titre' => 'Importation',
        'url' => $url,
        'url_redirect' => $url_redirect,
        'progression_pourcent' => 0,
        // todo ligne 178 ne s'affiche pas donc modif de attente.html.twing avec un un affichage permanent
        'message' => "n°" . $id
    );
// todo permet l'entrée avec les message d'erreurs mais sans l'affichage de la progression;
    //todo Voir le return ligne 660
    if ($id) {
        $objet_data = ImportQuery::create()->findOneByidImport($id);
        $import_infos = $objet_data->getInformation();
        if (isset($import_infos['options']['test']) && $import_infos['options']['test']) {
            for ($i = 1; $i <= 20; $i++) {
            //    echo '<br>Import_form action_attente n° ' . $i . ' passage option test boucle attente :';
                action_import_form_moteur();
             //   echo '<br>fin action_attente n° ' . $i;

            }
        }
    }
    return $app['twig']->render('attente.html.twig', $args_twig);
}





function action_import_form_moteur()
{
    global $app;
    $request = $app['request'];
    $id = $request->get('id_import');
    include_once($app['basepath'] . '/src/importation/importations.php');
    $reponse = [
        'message' => '',
        'pourcentage' => 0
    ];

    if ($id) {
        $import = ImportQuery::create()->findOneByidImport($id);
        $modification = $import->getModification();
        $modele = $import->getModele();


        if (count(array_keys($modification['etapes']))) {
            $etape = array_keys($modification['etapes'])[0];
            $reponse['message'] .= ' Modele : ' . $modele . ' - ' . pref('import.etapes.' . $etape);
            $fonction_etape = 'importation_etape' . $etape;
            $reponse = $fonction_etape($import, $reponse);

        } else {
            $reponse['message'] .= 'Pas d\'etape<br/>';
        }
    } else {
        $reponse['message'] .= 'Id non renseignée<br/>';

    }

    return $app->json($reponse);

}



function importation_abandon($import, $reponse)
{

    $reponse['pourcentage']= 0;
    $import->setEtape(0)->save();
    return [$reponse, true];
}

function importation_etape1(Import $import, $reponse)
{
    $fichier = $import->getNom();
    $modele = $import->getModele();
    $var_modele = importation_variables($modele);
    $import_info = $import->getInformation();
    $id = $import->getPrimaryKey();
    $reponse['message'] .= ' debut a : ' . date('Y-m-d H:i:s');
    $import_info = importation_charge_entete($modele,$fichier, $import_info,  $id);
    $fin = $var_modele['nb_ligne_entete'];
    $reponse['message'] .= ' ligne entete : ' . $fin;
    $reponse['pourcentage'] = 10;
    $import = etapeSuivante($import);
    $import->setInformation($import_info)->save();
    return [$reponse, true];
}


function importation_etape2(Import $import, $reponse)
{
    $etape_termine = false;
    $id_import = $import->getPrimaryKey();
    $import_info=$import->getInformation();
    $fichier = $import->getNom();
    $extension = substr($fichier, strrpos($fichier, '.'));
    $debut = $import->getLigneEnCours();
    if ($debut==1) {
        $import_info['nb']['curseur'] = positionne_sur_fichier($fichier, $import_info['nb']['ligne_entete']+1);
    }
    $pas = 100;
    $fin = $debut + $pas;
    switch ($extension) {
        case  '.csv' :
            $options = $import_info['options'];
            list($fin_du_fichier,$fin,$import_info['nb']['curseur'])=importation_charge_lignes_csv($fichier, $id_import, $debut, $fin,
                $import_info['nb']['curseur'], $options);
            break;
        default :
            echo '<br>Import_form erreur extention non traitée :' . $extension;
    }
    $reponse['pourcentage'] = floor(20 + (20 / ($import->getTaille()) * $import_info['nb']['curseur']));

    $import_info['nb']['lignes_lues'] = $fin;
    if ($fin_du_fichier) {
        $etape_termine = true;
        $import = etapeSuivante($import);
        $import->setNbLigne($fin+1);
        $import->setInformation($import_info)->save();

    } elseif ($fin > ($debut + $pas)) {
        echo '<br> pb de lecture ' . $fin;


    }else
    {
        $import->setLigneEnCours($fin);
        $import->setInformation($import_info)->save();
    }
    return [$reponse, $etape_termine];
}


function importation_etape3(Import $import, $reponse)
{
    global $app;
    $etape_termine = false;
    include_once($app['basepath'] . '/src/inc/fonctions_import.php');
    $id_import  = $import->getPrimaryKey();
    $info = $import->getInformation();
    $modification = $import->getModification();
    $debut = $import->getLigneEnCours();
    if ($import->getAvancement() < '2') {
        $reponse['message'] .= 'l\'étape de chargement n\'est pas réalisée pour import :' . $id_import;
    } else {

        $pas = round(max(5, min(50, ($info['nb']['lignes_lues'] / 10))));//entre 5 et 50
        $fin = min(($debut + $pas), $info['nb']['lignes_lues']);
        for ($i = $debut; $i <= $fin; $i++) {
            list($msg,$info,$modification) = importation_interprete_ligne($id_import,$i,$info,$modification);
            $reponse['message'].=$msg;
        }

        $reponse['message'] .=  " debut : " . $debut . " fin : " . $fin . " nombre lues : " . $info['nb']['lignes_lues'];
        // todo $individus_plusieurs et $membres_plusieurs en commentaire dans controle ligne

        $import->setModification($modification)->save();
        if (($info['nb']['lignes_lues']) == $fin) {
            $reponse['message'] .= " etape suivante :  Validation des modifications et créations à réaliser<br/>";
            $etape_termine = true;
            $reponse['pourcentage'] = 100;
            $import_info['nb']['lignes_interprete'] = $fin;
            $import = etapeSuivante($import);
        } else {
            $import->setLigneEnCours($fin);
            $reponse['pourcentage'] = floor(40 + (20 / $info['nb']['lignes_lues'] * $fin));
        }


        $import->setInformation($info)->save();
    }

    return [$reponse, $etape_termine];
}


function importation_etape4(Import $import, $reponse)
{
    global $app;
    $id_import=$import->getPrimaryKey();
    $ligne_data = ImportligneQuery::create()->filterByIdImport($id_import)->find();
    $args_twig=['info'=>$import->getInformation()];
    foreach($ligne_data as $ligne){
        $args_twig['tab_ligne'][$ligne->getLigne().""]=[
            'valeurs'=>$ligne->getVariable(),
            'propositions'=>$ligne->getProposition(),
        ];


    }

    $reponse['message'] = $app['twig']->render('importation/importation_selection.html.twig',$args_twig);
    echo($reponse['message']);
    exit();
    return [$reponse, true];
}

function importation_etape5($import, $reponse)
{


    return [$reponse, true];
}

function importation_etape6($import, $reponse)
{
    $import_info = $import->getInformation();
// todo validation auto en attente de la procedure interactive l'etape 4 est focée si etape 3
    $debut = $import_info['nb']['lignes_valide'] + 1;
    $fin = $import_info['nb']['lignes_lues'];
    echo '<br>Import_form 329 import_form.php 4, 5 et 6 debut:' . $debut . ' fin :' . $fin . ' affichage desdonnées dispo 2 (non touvé) 3 (trouvé)  pour voir suite à donner';
    for ($i = $debut; $i <= 3; $i++) {// le groupe de ligne
        $ligne_data = ImportligneQuery::create()->filterByidImport($id)->filterByLigne($i)->findone();
        if ($ligne_data) {
            $tab_l = json_decode($ligne_data->getVariables(), true);
            arbre($tab_l);
        }
    }

    echo '<br>import_form 327';
    exit;


    for ($i = $debut; $i <= $fin; $i++) {// le groupe de ligne
        $ligne_data = ImportligneQuery::create()->filterByidImport($id)->filterByLigne($i)->findone();
        if ($ligne_data) {
            $tab_l = $ligne_data->getVariables();
            foreach ($import_info['objets'] as $objet) {//pour chaque objet
                if (isset($tab_l[$objet]['etat']) == "DM") {
                    $tab_l[$objet]['etat'] = 'VA';
                    $import_info['nb'][$objet]['VA']++;
                }
                if (isset($tab_l[$objet]['etat']) == "DC") {
                    $tab_l[$objet]['etat'] = 'VA';
                    $import_info['nb'][$objet]['VA']++;
                }
            }
        }
    }
    $etape = '6';
    $etape_termine = true;
    echo 'ligne 337';

    return [$reponse, true];
}

function importation_etape7($import, $reponse)
{
    $pas = 120;
    $modele = $import->getModele();
    $fin = min($debut + $pas, $import_info['nb']['lignes_lues']);
    echo '<br>Import_form 347 import_form.php ligne debut :' . $debut . ' ligne fin :' . $fin . ' modele : ' . $modele . ' ';
    $config = $import_info['config'];
    for ($i = $debut; $i <= $fin; $i++) {
        $ligne_data = ImportligneQuery::create()->filterByidImport($id)->filterByLigne($i)->findone();
        $tab_l = $ligne_data->getVariables();
        echo '<br>Import_form 352 Variables :';
        arbre($tab_l);
        $tab_p = $ligne_data->getProposition();
        echo '<br>Import_form 354 proposition :';
        arbre($tab_p);
        $tab_o = $ligne_data->getObservation();
        echo '<br>Import_form 356 observation :';
        arbre($tab_o);
        foreach ($import_info['objets'] as $objet) {//pour chaque objet
            echo '<br>Import_form 358 objet :' . $objet;
            importation_valide($tab_l, $import_info['nb'], $objet,
                $config[$objet], $tab_l['logiciel']['num_ligne']);
        }
        $import_info['nb']['lignes_ecrites']++;
        $ligne_data->setValeur($tab_l);
        $ligne_data->setObservation($etape);
        echo '<br>Import_form 365 avant save :';
        arbre($ligne_data);
        exit;
        $ligne_data->save();
//                        arbre($tab_l);
//                        $tab_p = $ligne_data->getProposition();
//                        arbre($tab_p);
//                        $tab_o = $ligne_data->getObservation();
//                        arbre($tab_o);

    }
    $reponse['pourcentage'] = (floor(60 + (35 / ($import_info['nb']['lignes_lues']) * $fin)));
    $import_info['nb']['lignes_valide'] = $fin;
    if (($import_info['nb']['lignes_lues']) == $fin) {
        $reponse['message'] .= compte_rendu1($import_info, $id, $fichier, $etape, "6");
        $etape_termine = true;
    } else {
        $reponse['message'] .= $etape . " debut : " . $debut . " fin : " . $fin;
    }

    return [$reponse, true];
}


function action_import_form_resultat()
{
    global $app;
    $request = $app['request'];
    $id = $request->get('id_import');
    $j = 0;
    $ok = false;
    $args_twig = array();
    if ($id) {
        $objet_data = ImportQuery::create()->findPk($id);
        $fichier = $objet_data->getNom();
        $import_info = $objet_data->getProposition();
        $etape = $objet_data->getAvancement();
        if (isset($import_info['nom_output'])) {
            $outputFileName = $app['output.path'] . '/' . $import_info['nom_output'];
            if (!file_exists($outputFileName)) {
                $ok = true;
            }
        } elseif ($etape > '6') {
            $outputFileName = $app['output.path'] . '/' . $id . '*.*';
            if (!file_exists($outputFileName)) {
                $ok = true;
            }
        }
        if (!$ok and is_array($import_info)) {// si necessite un retour dans le fichier d'origine
            include_once($app['basepath'] . '/src/importation/importations.php');
//             compte_rendu1($import_info, $id,$etape, $etape, "6valide");
            rapportfinal_csv($import_info, $id);
            exit;
            $url_redirect = $app->path('import_form', ['action' => 'resultat', 'id_import' => $id]);
        }
        $args_twig = array('message' => $import_info['message']);
    }
    return $app['twig']->render('import_resultat.html.twig', $args_twig);
}

function action_import_form_supprimer()
{
    return action_supprimer_une_instance();
}

//restreint les propositons d'étapes en fonction de l'avancement ou la création
function choix_etape($data)
{
    $etapes = pref('import.etapes');
    if (isset($data['etapes'][0])) {
        $k = array_keys($data['etapes']);
        if (isset($k)) {
            $j = array_keys($data['etapes'])[0];
        } else {
            $j = $data['avancement'];
        }
    } else {
        $j = $data['avancement'] + 1;
    }
    if (is_null($data['modele'])) {
        $k = 0;
    } else {
        $k = 1;
    }

    for ($i = $k; $i < min($j, 8); $i++) {
        unset($etapes[$i]);
    }
    return array_flip($etapes);

}



function action_import_form_variable_ligne(){
    global $app;
    $request = $app['request'];
    $id_import = sac('id');
    $num = $request->get('num');
    $param = $request->get('name');
    $valeur = $request->get('value');
    list($objet,$champs)=explode('.',$param);
    $objet_data = ImportligneQuery::create()->filterByIdImport($id_import)->findOneByLigne($num);
    $prop = $objet_data->getProposition();
    $prop[$objet][$champs]=$valeur;
    $objet_data->setProposition($prop);
    $objet_data->save();
    return $app->json($valeur);

}


function action_import_form_rafraichir_ligne(){
    global $app;
    $request = $app['request'];
    $id_import = sac('id');
    $num = $request->get('num');
    list($info,$modification) = importation_interprete_ligne($id_import,$i,$info,$modification);

    return $app->json($valeur);
}
