<?php


function importation_variables_cotisation()
{
    return [
        'nom' => 'Importation des fichier de cotisation du CCFD',
        'objets' => ['individu', 'membre', 'servicerendu', 'paiement'],
        'test' => 1,
        'separateur_csv' => '\t',
        'nb_ligne_entete' => 3
    ];
}


function importation_objets_cotisation()
{

    return array(
        'membre' => array(
            'nom_colonne' => array(
                'nom' => array(
                    'Prénom',
                    'Noms',
                    'options' => array('separateur' => "espace", 'avant' => 'nom', 'Min' => 'nom', 'Min' => 'prenom')
                ),
                'civilite' => array('Civilité'),
                'id_membre' => array('id_membre'),
                'identifiant_interne' => array('N° client'),
                'observation' => array('Engagement religieux')
            ),
            'options' => array(
                'recherche' => array('identifiant_interne', 'nom'),
                'archive' => true,
                'obligatoire' => array(
                    'et' => array('identifiant_interne', 'nom', 'id_individu_titulaire'),
                    'creation' => true
                ),
                'detail_creation' => array('nom', 'identifiant_interne'),
                'detail_modification' => array('identifiant_interne', 'nom', 'observation'),
                'ecrit_modification' => array('identifiant_interne', 'nom', 'observation', 'id_individu_titulaire'),
                'pas_de_log' => false
            ),
        ),
        'individu' => array(
            'nom_colonne' => array(
                'nom' => array(
                    'Prénom',
                    'Nom',
                    'options' => array('separateur' => "espace", 'avant' => 'nom', 'Min' => 'nom', 'Min' => 'prenom')
                ),
                'nom_famille' => array('Nom', 'options' => array('Min' => 'nom')),
                'prenom' => array('Prénom', 'options' => array('Min' => 'prenom')),
                'email' => array('E-Mail'),
                'naissance' => array('naissance'),
                'adresse' => array(
                    'Adresse 1',
                    'Adresse 2',
                    'Adresse 3',
                    'Adresse 4',
                    'options' => array('separateur' => 'lfcr')
                ),
                'codepostal' => array('Code postal'),
                'ville' => array('Ville'),
                'pays' => 'pays',
                'telephone' => array('N° Téléphone'),
                'civilite' => array('Civilité'),
                'id_individu' => array('id_individu')

            ),
            'options' => array(
                'recherche' => array('id_membre', 'nom', 'email', 'nom_famille'),
                'archive' => true,
                'recherche_id_membre' => array(
                    'fichier' => 'MembreIndividu',
                    'origine' => array('objet' => 'membre', 'champ' => 'id_membre'),
                    //dans les données entrée ou déja calculée couple objet champ
//                    'filtre'=>'id_membre',//par defaut id_ de origine
//                    'retour'=>'id_individu',//par defaut id_ de l'objet
//                    'retourimporte'=>'id_individu',//par defaut id_ de l'objet
                ),
//                'recherche_nom'=>array(
//                    'fichier'=>'individu',
//                    'origine'=>array('objet'=>'membre','champ'=>'id_membre'),//dans les données entrée ou déja calculée couple objet champ
//                    'filtre'=>'id_membre',//par defaut id_ de origine
//                    'retour'=>'id_individu',//par defaut id_ de l'objet
//                    'retourimporte'=>'id_individu',//par defaut id_ de l'objet
//                ),
                'obligatoire' => array(
                    'ou' => array('nom', 'email'),
                    'creation' => true,
                    'detail' => true,
                    'rapport' => true
                ),
                'detail_creation' => array('ville', 'email'),
                'detail_modification' => array('prenom', 'email', 'ville', 'adresse'),
            )
        ),
        //todo resultat fin de phase 3 le 23/-06/2017 à 12 h idem avec sr_cotisation
// todo il faut ajouter id_prestation et date debut
        // verifier le date de fin si difference passer en création travail a realiser sur service rendu


        'servicerendu' => array(
            'nom_colonne' => array(
                'id_membre' => array('id_membre'),
                'temporaire1' => array('Code Article', 'options' => array('prefixemoins' => 3)),
                //exeption 10 premiers acarctéres = 'temporaire  sinon champs de l'objet
                'date_fin' => array('Echéance'),
                'date_debut' => array('date_debut'),
                'montant' => array('montant'),
                'id_prestation' => array('id_prestation')
            ),
            'options' => array(
                'recherche' => array('id_membre'),
                'recherche_id_membre' => array(
                    'origine' => array('objet' => 'membre', 'champ' => 'id_membre'),
                    'filtre_order' => array('champ' => 'date_fin', 'sens' => 'desc')
                    // en plus du filtre ou filtre defaut
                ),
                'liens' => array('prestation' => 'observation'),
                'lien_prestation' => array(
                    'cherche' => array('o_f' => 'prestation', 'o_c' => 'nomcourt', 'o_defaut' => '1N'),
                    //o_defaut si le champ n'est pas renseigner ici observation
                    'filtre' => array('o_c' => 'prestation_type', 'o_v' => 1),
                    //par defaut o_f= fichier o_c= id_$objet o_v= valeur de id_$objet
                    'alimente' => array(
                        array(
                            'o_f' => 'prestation',
                            'o_c' => 'id_prestation',
                            'd_o' => 'servicerendu',
                            'd_c' => 'id_prestation'
                        ),
                        array(
                            'o_f' => 'prestation',
                            'o_c' => 'prestation_type',
                            'd_o' => 'prestation',
                            'd_c' => 'prestation_type'
                        ),
                        array(
                            'o_f' => 'prestation',
                            'o_c' => 'periodique',
                            'd_o' => 'prestation',
                            'd_c' => 'id_prestation'
                        ),
                        array(
                            'o_f' => 'prestation',
                            'o_c' => 'periodique',
                            'd_o' => 'prestation',
                            'd_c' => 'periodique'
                        )
                    ),
                ),
                'obligatoire' => array('ou' => array('date_fin'), 'et' => array('id_prestation'), 'creation' => true),
                'detail_creation' => array('id_pestation', 'date_fin'),
                'detail_modification' => array('observation'),
                'pasdemodification' => true,
            ),
        ),

    );
}
