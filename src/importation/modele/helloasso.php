<?php

function importation_variables_helloasso()
{


    $tab = [
        'nom' => 'Importation des fichier d\'export HelloAsso',
        'objets' => ['individu', 'membre', 'servicerendu'],
        'test' => true,
        'separateur_csv' => ';',
        'nb_ligne_entete' => 1,
        'nb_colonne' => 27
    ];

    return $tab;
}


function importation_objets_helloasso()
{
    //Campagne;   toujours Adhérer
    //Don anonyme;    toujours non
    //Société;  une fois 1991 pour luc cordier sur 1 ligne la ligne suivante pas de N° de societe
    //Numéro de reçu; de 1 a 24 un numero sur 7 digit en colonne Reçu
    //Adresse acheteur;Code Postal acheteur;Ville acheteur;Pays acheteur;

    return [
        'individu' => [
            'nom_colonne' => [
                'prenom' => ['Prénom',  ['Min' => 'prenom']],
                'nom_famille' => ['Nom', ['Min' => 'nom']],
                'nom' => [
                    [
                        ['Nom', ['traitement' => 'min']],
                        ['Prénom', ['traitement' => 'min']]
                    ],
                    ['separateur' => ' ', 'avant' => 'nom']
                ],
                'adresse' => 'Adresse',
                'codepostal' => 'Code Postal',
                'ville' => 'Ville',
                'pays' => 'Pays',
                'email' => 'Email',
                'observation' => 'Commentaire',
                'naissance' => ['Date de naissance', ['traitement' => ['date',['format'=>'d/m/Y']]]],
                'conjoint' => ['Nom prénom du conjoint',['temporaire' => true]],
                'telephone' => 'Numéro de téléphone',
                'profession' => 'Profession',
                'telephone_pro' => 'Téléphone',
                'id_individu' => 'id_individu'

            ],
            'options' => [
                'recherche' => ['email', ['nom','naissance'],'nom'],
                'archive' => false,
                'obligatoire' => [
                    'ou' => ['nom', 'email'],
                    'creation_autorise' => true,
                    'detail' => true,
                    'rapport' => true
                ],
                'detail_creation' => ['ville', 'email'],
                'detail_modification' => ['prenom', 'email', 'ville', 'adresse'],
            ]
        ],
        'membre' => [
            'nom_colonne' => [
                'nom' => [
                    [
                        ['Nom', ['traitement' => 'min']],
                        ['Prénom', ['traitement' => 'min']]
                    ],
                    ['separateur' => ' ', 'avant' => 'nom']
                ],
                'id_membre' => ['id_membre'],
                'id_individu_titulaire' => ['id_individu_titulaire'],
            ],
            'options' => [
                'recherche' => ['nom','id_individu'],
                'recherche_liaison' => [
                    'id_individu'=>[
                        'fichier' => 'MembreIndividu',
                        'origine' => ['objet' => 'individu', 'champs' => 'id_individu'],
                    ]
                    //dans les données entrée ou déja calculée couple objet champs
                ],
                'archive' => true,
                'obligatoire' => ['et' => ['nom', 'id_individu_titulaire'], 'creation_autorise' => true],
                'detail_creation' => ['nom'],
                'detail_modification' => ['nom', 'observation'],
                'ecrit_modification' => ['nom', 'observation', 'id_individu_titulaire'],
                'pas_de_log' => false
            ],
        ],
        'servicerendu' => [
            'nom_colonne' => [
                'temporairenumero' => ['Numéro', 'options' => ['champs' => '']],
                'temporaireprestype' => ['Type', 'options' => ['remplace' => 'xx', 'champs' => 'observation']],
                //xx ne sert pas les valeurs sont mise en dur voir pour amméliorer// Adhésion -> 2(en réalité cotisation], Don unique -> 5
                'montant' => 'Montant',
                'temporaireattestation' => [
                    'Attestation',
                    'options' => ['fin' => 7, 'champs' => 'observation', 'devant' => 'Attestation n° :']
                ],
                //les 7 derniers caractères
                'observation' => [
                    'Reçu',
                    'options' => ['fin' => 7, 'concat' => true, 'devant' => ' Don n° :']
                ],
                //les 7 derniers caractères],
                'id_servicerendu' => 'id_servicerendu',
                'id_prestation' => 'id_prestation',
                'id_membre' => 'id_membre',
                'date_enregistrement' => 'Date',
                'date_fin' => 'date_fin',
                'date_debut' => 'date_debut'
            ],
            'options' => [
                'liens' => ['prestation' => 'montant'],//montant = valeur du lien
                'lien_prestation' => [
                    'cherche' => ['o_f' => 'prestation', 'o_c' => 'descriptif', 'o_defaut' => '14'],
                    //o_defaut si le champs n'est pas renseigner ici observation
                    'filtre' => ['o_c' => 'prestation_type', 'o_v' => 1],
                    //par defaut o_f= fichier o_c= id_$objet o_v= valeur de id_$objet
                    'recherche_dernier' => [
                        'origine' => ['objet' => 'servicerendu', 'champs' => 'id_membre', 'valeur' => 'membre'],
                        'filtre_order' => ['champs' => 'date_fin', 'sens' => 'desc']
                        // en plus du filtre ou filtre defaut
                    ],
                    'alimente' => [
                        [
                            'o_f' => 'prestation',
                            'o_c' => 'id_prestation',
                            'd_o' => 'servicerendu',
                            'd_c' => 'id_prestation'
                        ],
                        [
                            'o_f' => 'prestation',
                            'o_c' => 'prestation_type',
                            'd_o' => 'prestation',
                            'd_c' => 'prestation_type'
                        ],
                        [
                            'o_f' => 'prestation',
                            'o_c' => 'periodique',
                            'd_o' => 'prestation',
                            'd_c' => 'periodique'
                        ],
                        [
                            'o_f' => 'prestation',
                            'o_c' => 'id_prestation',
                            'd_o' => 'prestation',
                            'd_c' => 'id_prestation'
                        ],
                    ],
                ],
                'obligatoire' => [
                    'ou' => ['date_debut'],
                    'et' => ['id_prestation'],
                    'creation_autorise' => true
                ],
                'detail_creation' => ['id_prestation', 'date_debut', 'id_membre', 'montant'],
                'detail_modification' => ['observation'],
                'pasdemodification' => true,
            ],
        ],
        'paiement' => [
            'nom_colonne' => [
                'temporairenumero' => ['Numéro', 'options' => ['champs' => '']],
                'temporaireprestype' => ['Type', 'options' => ['remplace' => 'xx', 'champs' => 'observation']],
                //xx ne sert pas les valeurs sont mise en dur voir pour amméliorer// Adhésion -> 2(en réalité cotisation], Don unique -> 5
                'montant' => 'Montant',
                'temporaireattestation' => [
                    'Attestation',
                    'options' => ['fin' => 7, 'champs' => 'observation', 'devant' => 'Attestation n° :']
                ],
                //les 7 derniers caractères
                'observation' => [
                    'Reçu',
                    'options' => ['fin' => 7, 'concat' => true, 'devant' => ' Don n° :']
                ],
                //les 7 derniers caractères],
                'id_prestation' => 'id_prestation',
                'id_paiement' => 'id_paiement',
                'id_tresor' => ['id_tresor', 'options' => ['o_defaut' => '4']],
                'date_cheque' => 'Date',
                'date_enregistrement' => 'Date',
                'id_objet' => 'id_objet',
                'objet' => 'objet',
            ],
            'options' => [
                'liens' => ['tresor' => 'id_tresor'],//montant = valeur du lien
                'lien_tresor' => [
                    'cherche' => ['o_f' => 'tresor', 'o_c' => 'id_tresor', 'o_defaut' => '4'],
                    //o_defaut si le champs n'est pas renseigner ici observation
                    'alimente' => [
                        [
                            'o_f' => 'tresor',
                            'o_c' => 'id_tresor',
                            'd_o' => 'paiement',
                            'd_c' => 'id_tresor'
                        ],
                    ],
                ],
                'obligatoire' => [
                    'ou' => ['date_cheque'],
                    'et' => ['id_tresor', 'montant'],
                    'creation_autorise' => true
                ],
                'detail_creation' => ['id_tresor', 'date_cheque', 'id_objet', 'montant', 'objet' => 'membre'],
                'detail_modification' => ['observation'],
                'pasdemodification' => true,
            ],
        ],

    ];
}
