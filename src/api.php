<?php

use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Security\Core\Exception\UsernameNotFoundException;
use Symfony\Component\Security\Core\User\User;


class SimplassoApi
{


    public function login($username, $password) {

        global $app;


        $vars['username'] = $username;
        $vars['password'] = $password;


        try {
            if (empty($vars['username']) || empty($vars['password'])) {
                echo('erreur');
            }

            /**
             * @var $user User
             */
            $user = $app['users_api']->loadUserByUsername($vars['username'], false);

            if (!$app['security.default_encoder']->isPasswordValid($user->getPassword(), $vars['password'],
                $user->getSalt())
            ) {
                throw new UsernameNotFoundException(sprintf('Username "%s" does not exist.', $vars['username']));
            } else {
                $app->log($vars['username'].' a reussi à se s\'authentifier, on lui transmet un token' );
                $response = [
                    'success' => true,
                    'token' => $app['security.jwt.encoder']->encode(['name' => $user->getUsername()]),
                ];
            }

        } catch (UsernameNotFoundException $e) {
            $response = [
                'success' => false,
                'error' => 'Invalid credentials',
            ];
        }

        return $response;


    }


    public function activation_adherent($email, $nom_site, $url_activation,$activation=false) {

        global $app;
        $ok = false;
        $message = "";
        $vars['email'] = $email;
        if (empty($vars['email'])) {
            return [
                'ok' => false,
                'message' => "L'adresse email est manquante",
            ];
        }

        $user = IndividuQuery::create()->findOneByEmail($email);
        if ($user) {
            // Initialize and send the password reset request.
            $user->setTokenTime(time());
            if (!$user->getToken()) {
                $user->setToken(uniqid());
            }
            if (empty($user->getLogin())) {
                $nom = $user->getNomFamille();
                $prenom = $user->getPrenom();
                $user->setLogin(IndividuQuery::donnerLogin($nom,$prenom));
            }

            $user->save();
            $args_twig = array(
                'login' => $user->getLogin(),
                'url_activation' => $url_activation . '&token=' . $user->getToken()
            );
            if ($activation){
                courriel_twig($email, 'login_activation_api', $args_twig);
            }
            else {
                courriel_twig($email, 'login_motdepasseperdu_api', $args_twig);
            }

            $ok = true;
            $message = $app->trans('Instructions pour réinitialiser son mot de passe - message');


        } else {

            $message = $app->trans('Aucun utilisateur ne corresponds à cette adresse');

        }
        return [
            'ok' => $ok,
            'message' => $message,
        ];

    }


    public function activation_adherent_test($email) {


        $ok = false;
        $message = "";
        $vars['email'] = $email;
        if (empty($vars['email'])) {
            return [
                'ok' => false,
                'message' => "L'adresse email est manquante",
            ];
        } else {
            $ok = IndividuQuery::create()->filterByEmail($email)->exists();
        }
        return [
            'ok' => $ok
        ];

    }


    public function activation_adherent_mot_de_passe($email, $token, $password) {
        $ok = false;
        $message = "";
        $vars['email'] = $email;
        $ok = false;
        if (empty($vars['email'])) {
            $message = "";
        } else {
            $individu = IndividuQuery::create()->filterByEmail($email)->findOneByToken($token);
            $individu->setPass($password)->save();
            $ok = true;
        }
        return [
            'ok' => $ok
        ];
    }


    public function liste_entite() {
        return tab('entite');
    }


    public function liste_pays() {
        return tab('pays');
    }


    public function info_individu() {
        global $app;

        if($app['user']) {
           $individu = IndividuQuery::create()->findOneByLogin($app['user']->getUsername())->toArray();
            $tab_info = ['id_individu', 'login', 'nom'];
        }
        return array_intersect_key($individu, array_flip($tab_info));
    }


    public function info_adherent() {
        global $app;

        if($app['user']) {
            $individu = IndividuQuery::create()->findOneByLogin($app['user']->getUsername());
            $tab_info = [
                'nom',
                'nom_famille',
                'civilite',
                'naissance',
                'sexe',
                'prenom',
                'email',
                'telephone',
                'mobile',
                'adresse',
                'codepostal',
                'ville',
                'pays',
                'fax',
                'contact_souhait'
            ];
            $result = array_intersect_key($individu->toArray(), array_flip($tab_info));
            $tab_membre_sql = $individu->getMembre();
            $tab_info_membre = array_flip(['id_membre', 'nom']);
            $tab_membre = array();
            foreach ($tab_membre_sql as $membre) {
                $tab_membre[] = array_intersect_key($membre->toArray(), $tab_info_membre);

            }
            $tab_ent = array_keys(tab('entite'));
            foreach ($tab_ent as $id_entite) {
                $result['a_jour'][$id_entite] = $individu->getEtatCotisation($id_entite);
            }
            $result['membre'] = $tab_membre;
            return $result;
        }
    }

    public function info_cotisation() {
        global $app;
        if($app['user']) {
            $tab_cotisations  = IndividuQuery::create()->findOneByLogin($app['user']->getUsername())->getCotisations();
            $tab_result = [];
            foreach ($tab_cotisations as $cotisation) {
                $id_entite = $cotisation->getIdEntite();
                $tab_result[$id_entite][] = [
                    'date' => $cotisation->getDateEnregistrement()->format('d/m/Y'),
                    'designation' => $cotisation->getDesignation(),
                    'periode' => date_periode($cotisation->getDateDebut(), $cotisation->getDateFin()),
                    'montant' => $cotisation->getMontant(),
                    'membre' => $cotisation->getIdMembre()
                ];

            }
        }

        return $tab_result;


    }

    public function getDons() {
        /*    global $app;
            $token = $app['security.token_storage']->getToken();
            $user = $token->getUser();
            $individu = IndividuQuery::create()->findPk($user->getId());
            $individu->getServicerendues();

            return array_intersect_key($individu,array_flip($tab_info));
    */

    }


    function adherent_modification($adresse, $codepostal, $ville, $pays, $telephone, $mobile, $email) {
        global $app;
        $ok = true;
        if($app['user']) {
            $ind = IndividuQuery::create()->findOneByLogin($app['user']->getUsername());
            $tab_champs = ['adresse', 'codepostal', 'ville', 'pays', 'telephone', 'mobile', 'email'];
            $nb_args = min(func_num_args(), count($tab_champs));
            foreach ($tab_champs as $k => $champs) {
                $ind->setByName($champs, func_get_arg($k));
                if ($k >= $nb_args) {
                    break;
                }
            }

            try {
                $ind->save();
            } catch (Exception $e) {

                $ok = false;
            }
        }
        return $ok;
    }


    public function adherent_test_email($email) {
        global $app;
        $ind = IndividuQuery::create()->findOneByLogin($app['user']->getUsername());
        if (empty($email) or $ind->getEmail() === trim($email)) {
            $ok = true;
        } else {
            $ok = !(IndividuQuery::create()->filterByEmail($email)->exists());
        }
        return $ok;

    }


    function change_mdp($mot_de_passe) {
        global $app;
        $ind = IndividuQuery::create()->findOneByLogin($app['user']->getUsername());
        $ind->setPass($mot_de_passe);
        try {
            $ind->save();
            $ok = true;
        } catch (Exception $e) {
            $ok = false;
        }
        return $ok;
    }

    function coordonnees() {
        global $app;
        $ind = IndividuQuery::create()->findOneByLogin($app['user']->getUsername());
        return $app->json(['content' => $ind->getNom()]);
    }



}