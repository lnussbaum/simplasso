<?php

function statistique()
{
    global $app;
    $param = pref('statistique');
    $annee = ($param['par_annee']) ? '' : date('Y');//$app['request']->get('annee');
    $debut_annee = $param['amj_debut']->format('Y');
    //simplasso_stat_nb_adherent("2000");
    foreach (getPrestationTypeActive() as $k => $nom) {


        $tab_prestation = getPrestationDeType($k);
        if (!empty($tab_prestation)) {


            $titre = array($app->trans('date_debut'), $app->trans('nombre'), $app->trans('montant'));

            $from = ' FROM asso_servicerendus s,asso_prestations p';
            $where = ' WHERE p.id_prestation IN(' . implode(',',
                    array_keys($tab_prestation)) . ') and s.id_prestation=p.id_prestation';
            if ($annee) {
                $where .= " and (year(s.date_debut)= " . $debut_annee . ")";
            }



            $select = 'SELECT distinct s.date_debut as datex';
            $select2 = 'SELECT distinct DATE_ADD(s.date_fin, INTERVAL 1 DAY) as datex';

            $sql_date = $select . $from . $where .  ' UNION ' . $select2 . $from . $where;
            $from .= ',(' . $sql_date . ') as tab_date ';
            $where_cplt='';
            if ($nom=='adhesion') {

                $where_cplt .= ' AND s.origine is NULL';

            }
            $where .= ' AND s.date_debut <= tab_date.datex AND tab_date.datex <=  s.date_fin';
           // $where .= ' AND origine is NULL';

            $select = 'SELECT tab_date.datex as datex,sum(p.nb_voix*s.quantite) as nb, SUM(s.montant*s.quantite) as montant';
            $groupby = ' GROUP BY s.id_entite,datex';
            $sql = $select . $from . $where. $where_cplt . $groupby;

            $data = $app['db']->fetchAll($sql);

            if ($nom=='adhesion') {

                $select = 'SELECT distinct s.date_debut as datex';
                $select2 = 'SELECT distinct DATE_ADD(s.date_fin, INTERVAL 1 DAY) as datex';
                $from = ' FROM asso_servicerendus s,asso_prestations p';
                $where = ' WHERE p.id_prestation IN(' . implode(',',
                        array_keys($tab_prestation)) . ') and s.id_prestation=p.id_prestation ';

                $sql_date = $select . $from . $where.  ' UNION ' . $select2 . $from . $where;
                $from .= ',(' . $sql_date . ') as tab_date ';
                $where .= ' AND s.date_debut <= tab_date.datex';
                // $where .= ' AND origine is NULL';

                $select = 'SELECT tab_date.datex as datex,sum(p.nb_voix*s.quantite) as nb, SUM(s.montant*s.quantite) as montant';
                $groupby = ' GROUP BY s.id_entite,datex';
                $where_cplt = ' AND montant = -46';
                $sql = $select . $from . $where. $where_cplt. $groupby;
                $data_m = $app['db']->fetchAll($sql);

                $data_m = table_colonne_cle($data_m,'datex');
                foreach($data as $k=>&$d){
                    if (isset($data_m[$d['datex']])){
                        $d['nb']-=$data_m[$d['datex']]['nb'];
                        $d['montant']+=$data_m[$d['datex']]['montant'];
                    }
                }
            }



            $cumul=0;
            /*foreach($data as &$d){
                $d['nb']=$d['cumul']-$cumul;
                $cumul=$d['cumul'];
            }*/
            $data_chart_nb = table_colonne_cle_valeur($data, 'datex', 'nb');
            $tab_stat[] = array(
                'identifiant' => 'nb_'.$nom,
                'titre' => $app->trans($nom),
                'donnees' => array_merge(array($titre), $data),
                'donnees_charts' => $data_chart_nb
            );


            // Paiement
            $titre = array($app->trans('date_debut'),  $app->trans('montant'));

            $select = 'SELECT concat(YEAR(pa.date_enregistrement),\' \',FLOOR((MONTH(pa.date_enregistrement)-1)/6)) as datex,SUM(spa.montant) as montant';
            $from = ' FROM asso_paiements pa,asso_prestations p, asso_servicepaiements spa,asso_servicerendus s';
            $where = ' WHERE p.id_prestation IN(' . implode(',',array_keys($tab_prestation)) . ')'.
                ' and pa.id_paiement=spa.id_paiement and spa.id_servicerendu = s.id_servicerendu'.
                ' and s.id_prestation=p.id_prestation';
            $groupby = ' GROUP BY datex';
            $sql = $select . $from . $where . $groupby;


            $data = $app['db']->fetchAll($sql);
            $data_chart_nb = table_colonne_cle_valeur($data, 'datex', 'montant');
            $tab_stat[] = array(
                'identifiant' => 'paiement_'.$nom,
                'titre' => $app->trans($nom),
                'donnees' => array_merge(array($titre), $data),
                'donnees_charts' => $data_chart_nb
            );

        }
    }


    if ($tab_stat) {
        $args_twig['tab_stat'] = $tab_stat;
    }

    $args_twig['js_suppl']='stat';

    return $app['twig']->render('statistique.html.twig', $args_twig);
}

function simplasso_stat_nb_adherent($annee, $debut_annee = "-01-01", $details = false)
{
    global $app;
    $params['entite'] = 1;
    $query_presta = AdhesionQuery::create();

    if (suc('entite_multi')) {//une seule ou les autorisees

        $entite = ($entite = request_ou_options('entite', $params)) ? $entite : sac('en_cours.id_entite');
        $query_presta = $query_presta->filterByIdEntite($entite);
    }

    $tab_prestations = array_keys(tab('prestation'));
    if (!empty($tab_prestations)) {
        $where = 'p.id_prestation IN(' . implode(',', $tab_prestations) . ') and s.id_prestation=p.id_prestation';
        $sel = 'YEAR(date_debut) as annee  ,count(*) as nb ,SUM(s.montant)';
        if ($details) {
            $sel = 'p.nom,' . $sel;
        }
        $res = $app['db']->fetchAll('SELECT ' . $sel . ' FROM asso_servicerendus s,asso_prestations p WHERE ' . $where . ' GROUP BY s.id_entite,p.id_prestation,annee');

        array_unshift($res, array('annee', 'Nombre', 'Montant'));//_T('simplasso:nombre_de_cotisations')
        if ($details) {
            array_unshift($res[0], 'Adhérent');
        }
        return $res;
    }
    return array();
}


function simplasso_array2flotr($array, $id, $forme, $options = array())
{

    $js = "";
    switch ($forme) {
        case 'histogramme' :

            $tab_val0 = array();
            $tab_col = array_shift($array);
            foreach ($array as $key => $a) {
                $a0 = array_shift($a);
                if ($pos = strpos($a0, '-')) {
                    $a0 = substr($a0, 0, $pos);
                }
                $a1 = array_shift($a);
                $tab_val0[] = $a0;
                $data[] = '[' . $a0 . ', ' . $a1 . ']';
            }
            if (!empty($data)) {
                $data = '[[' . implode(', ', $data) . ']]';
                $min_annee = min($tab_val0);

                $js = 'Flotr.draw(
		container, ' . $data . ', {
			HtmlText : false,
			bars: {
				show: true,
				horizontal: false,
				shadowSize: 0,
				barWidth: 1
			},
			mouse: {
				track: true,
				relative: true
			},
			xaxis: {
				min: ' . $min_annee . ',
				autoscaleMargin: 1,
				title:\'' . str_replace('\'', '\\\'', ($tab_col[0])) . '\',
				tickDecimals:0
			},
			yaxis: {
				min: 0,
				autoscaleMargin: 1,
				title:\'' . str_replace('\'', '\\\'', ($tab_col[1])) . '\',
				tickDecimals:0,
				titleAngle:90
			},
			grid: {
				verticalLines: true,
				horizontalLines: true
			},
			legend: {
				container:$("#chart_legend_' . $id . '")
				}
		});';
            }

            break;
        case 'camenbert' :


            $tot_a = 0;

            if (count($array) > 2) {
                $tab_col = array_shift($array);
                $data = array();
                $data2 = array();
                foreach ($array as $ky => $ar) {
                    $i = 0;
                    $a0 = array_shift($ar);
                    foreach ($ar as $key => $a) {
                        $data2[$i][] = '[' . $a0 . ', ' . $a . ']';
                        $tot_a += $a;
                        $i++;
                    }
                }

                $i = 0;
                foreach ($data2 as $key => $d) {
                    $i++;
                    $data[] = '{ data: [' . implode(', ', $d) . '], label: \'' . str_replace('\'', '\\\'',
                            ($tab_col[$i])) . '\'}';
                }


                if (!empty($data) && $tot_a != 0) {
                    $data = '[' . implode(',', $data) . ']';


                    $js = 'Flotr.draw(container,
			' . $data . ', {
			legend: {
				backgroundColor: \'#D2E8FF\',
				container: $("#chart_legend_' . $id . '")

			},
			bars: {
				show: true,
				stacked: true,
				horizontal: false,
				barWidth: 0.6,
				lineWidth: 1,
				shadowSize: 0
			},
			grid: {
				verticalLines: false,
				horizontalLines: true
			},
			xaxis: {
				autoscaleMargin: 1,
				title:\'' . $tab_col[0] . '\',
				tickDecimals:0
			},
			yaxis: {
				autoscaleMargin: 1,
				tickDecimals:0,
			},
			mouse: {
				track: true
			},
		})';
                }
            } else {

                foreach ($array as &$a) {
                    $tab_row[] = array_shift($a);
                }
                $tab_col = array_shift($array);

                foreach ($array as $ky => $ar) {
                    $i = 0;
                    foreach ($ar as $key => $a) {
                        $data[] = '{ data: [[0,' . $a . ']], label: \'' . str_replace('\'', '\\\'',
                                ($tab_col[$i])) . '\'}';
                        $tot_a += $a;
                        $i++;
                    }
                }
                if (!empty($data) && $tot_a != 0) {
                    $data = '[' . implode(',', $data) . ']';


                    $js = 'Flotr.draw(container, ' . $data . ', {
			HtmlText: false,
			grid: {
				verticalLines: false,
				horizontalLines: false
			},
			xaxis: {
				showLabels: false
			},
			yaxis: {
				showLabels: false
			},
			pie: {
				show: true,
				explode: 6
			},
			mouse: {
				track: true
			},
			legend: {
				backgroundColor: \'#D2E8FF\',
				container:$("#chart_legend_' . $id . '")
			}
			})';
                }
            }
            break;
    }

    if ($js != '') {
        $js = '<div id="chart_' . $id . '" class="chart"></div><div id="chart_legend_' . $id . '" class="chart_legend"></div>
	<script type="text/javascript">
	var container=document.getElementById("chart_' . $id . '");
	' . $js . '
	</script>';
    }
    return $js;
}


//SELECT '2004' as annee,count(*) as nb FROM asso_membres m WHERE statut_asso<>'nouveau' and (date_sortie >'2005-01-01' OR date_sortie is null) and ((date_adhesion <='2005-01-01') OR date_adhesion is null)
//SELECT date_debut, count(*) as nb,s.id_entite,e.nom FROM asso_servicerendus s,asso_entites e,asso_prestations p where (date_debut >='2012-01-01' and date_fin <='2016-12-31' )and s.supprime<1 and prestation_type=1 and p.id_prestation=s.id_prestation and s.id_entite=e.id_entite group by s.id_entite,date_debut
function simplasso_stat_nb_cotisation_encaisse($annee, $debut_annee = "-01-01")
{
    $where = '';
    if ($annee) {
        $where .= 'date_debut <=' . $annee . $debut_annee . ' and date_fin >\'' . ($annee) . $debut_annee . '\'';
    }
    $sel = 'YEAR(DATE_SUB(date_cotisation,INTERVAL DATEDIFF(DATE(CONCAT(YEAR(date_cotisation),\'' . $debut_annee . '\')),DATE(CONCAT(YEAR(date_cotisation),\'-01-01\'))) DAY))';
    if ($debut_annee != "-01-01") {
        $sel = 'CONCAT(' . $sel . ',\' - \',' . $sel . '+1)';
    }
    $res = sql_allfetsel($sel . ' as annee,count(*) as nb', 'asso_cotisations c', $where, 'annee', 'annee');
    array_unshift($res, array(_T('simplasso:annee'), _T('simplasso:nombre_de_cotisations')));
    return $res;
}


function simplasso_stat_nouveau_adherent($annee, $debut_annee = "-01-01")
{
    $where = 'statut_asso<>\'nouveau\' and YEAR(date_adhesion)<>\'\' and date_adhesion is not null ';
    if ($annee) {
        $where .= 'and date_adhesion >=' . $annee . $debut_annee . ' and date_adhesion <\'' . ($annee + 1) . $debut_annee . '\'';
    }
    $sel = 'YEAR(DATE_SUB(date_adhesion,INTERVAL DATEDIFF(DATE(CONCAT(YEAR(date_adhesion),\'' . $debut_annee . '\')),DATE(CONCAT(YEAR(date_adhesion),\'-01-01\'))) DAY))';
    if ($debut_annee != "-01-01") {
        $sel = 'CONCAT(' . $sel . ',\' - \',' . $sel . '+1)';
    }
    $res = sql_allfetsel($sel . ' as annee,count(*) as nb', 'asso_membres m', $where, 'annee', 'annee');
    array_unshift($res, array(_T('simplasso:annee'), _T('simplasso:nombre_de_nouveaux_adherents')));
    return $res;
}

function simplasso_stat_nombre_de_depart($annee, $debut_annee = "-01-01")
{
    $where = 'statut_asso<>\'nouveau\' and YEAR(date_sortie)<>\'\' and date_sortie is not null ';
    if ($annee) {
        $where .= 'and date_sortie >=' . $annee . $debut_annee . ' and date_sortie <\'' . ($annee + 1) . $debut_annee . '\'';
    }
    $sel = 'YEAR(DATE_SUB(date_sortie,INTERVAL DATEDIFF(DATE(CONCAT(YEAR(date_sortie),\'' . $debut_annee . '\')),DATE(CONCAT(YEAR(date_sortie),\'-01-01\'))) DAY))';
    if ($debut_annee != "-01-01") {
        $sel = 'CONCAT(' . $sel . ',\' - \',' . $sel . '+1)';
    }
    $res = sql_allfetsel($sel . '  as annee,count(*) as nb', 'asso_membres m', $where, 'annee', 'annee');
    array_unshift($res, array(_T('simplasso:annee'), _T('simplasso:nombre_de_departs')));
    return $res;
}