<?php

/**
 * @return mixed
 * @throws Exception
 * @throws PropelException
 */
function accueil()
{
    global $app;
    $args_twig=array(setContexte('page','accueil'));

    $args_twig['tab_operations'] = Log::getTimeline(['decoupage'=>'heure','filtre_operateur'=>'tous','regroupement'=>true],10);
    $stat = MembreQuery::getStat();
    $args_twig['url_rss']=$app['simplasso.rss'];
    $args_twig['nb_adherent']=$stat['nb'];
    $args_twig['nb_adherent_ok']=$stat['nb_ok'];
    $args_twig['import_en_attente'] = import_attente();


    $args_twig['nb_adherent_ok_pourcentage']= ($stat['nb']>0) ? floor(($stat['nb_ok']/$stat['nb'])*100) : 0;
//     arbre($args_twig);
    return $app['twig']->render('accueil.html.twig', $args_twig);

}




function action_accueil_choisir()
{
    global $app;
    $request = $app['request'];
    $id_entite = $request->get('id_entite');
    ecrire_preference('en_cours.id_entite',$id_entite);
    initUserPreference(suc('operateur.id'));
    return $app->redirect('accueil');
}
