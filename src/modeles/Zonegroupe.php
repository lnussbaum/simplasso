<?php

use Base\Zonegroupe as BaseZonegroupe;

/**
 * Skeleton subclass for representing a row from the 'geo_zonegroupes' table.
 *
 *
 *
 * You should add additional methods to this class to meet the
 * application requirements.  This class will only be generated as
 * long as it does not already exist in the output directory.
 *
 */
class Zonegroupe extends BaseZonegroupe
{
    public function getQuiCree() {
        return Log::getQuiCree('zonegroupe', $this->id_zonegroupe);
    }

    public function getQuiModifie() {
        return Log::getQuiModifie('zonegroupe', $this->id_zonegroupe);
    }

}
