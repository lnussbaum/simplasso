<?php

use Base\MotLienQuery as BaseMotLienQuery;
use Propel\Runtime\Map\TableMap;

/**
 * Skeleton subclass for performing query and update operations on the 'asso_mots_liens' table.
 *
 *
 *
 * You should add additional methods to this class to meet the
 * application requirements.  This class will only be generated as
 * long as it does not already exist in the output directory.
 *
 */
class MotLienQuery extends BaseMotLienQuery
{
    static function getStat($tab_id_mot,$objet,$where_objet)
    {
        global $app;
        $tab = array();

        $select = 'SELECT id_mot, count(id_objet) as nb FROM asso_mots_liens ';
        $select .= ' WHERE id_objet IN (' . $where_objet . ') and objet=\''.$objet.'\'';
        $select .= ' and id_mot IN ('.implode(',',$tab_id_mot).') GROUP BY id_mot';

        $tab_mot=$app['db']->fetchAll($select);
        foreach ($tab_mot as $mot) {

            if ($mot['nb']>0)
                $tab[$mot['id_mot']] = $mot['nb'];
        }

        return $tab;
    }


    /**
     * mot_liens
     * @param integer $id_mot
     * @param integer $id_objet
     * @param string $objet
     * @param string $observation
     * @param bool|false $suppresion
     * @return bool
     * @throws \Propel\Runtime\Exception\PropelException
     */
    static function mot_liens($id_mot, $id_objet, $objet, $observation = '', $suppresion = false)
    {

        $lien = MotLienQuery::create()->filterByIdMot($id_mot)->filterByIdObjet($id_objet)->findOneByObjet($objet);
        $ajout = true;
        if ($suppresion && $lien) {
            $lien->delete();
            $ajout = false;
        } else {
            if (!$lien) {
                $lien = new MotLien();
                $lien->setIdObjet($id_objet);
                $lien->setObjet($objet);
                $lien->setIdMot($id_mot);
            }
            $lien->setObservation($observation);
            $lien->save();
        }
        return $ajout;
    }


    /**
     * supprimer_mot_liens
     * @param integer $id_mot
     * @param integer $id_objet
     * @param string $objet
     * @return bool
     * @throws \Propel\Runtime\Exception\PropelException
     */
    static function supprimer_mot_liens($id_mot, $id_objet, $objet)
    {

        $liens = MotLienQuery::create()->filterByIdMot($id_mot)->filterByIdObjet($id_objet)->findByObjet($objet);
        foreach ($liens as $lien) {
            $lien->delete();
        }
        return true;
    }




    static function getByIdmot($id_mot){
        $tab_ml = MotLienQuery::create()->findByIdMot($id_mot)->toArray(null,null,TableMap::TYPE_FIELDNAME);
        foreach($tab_ml as &$ml){
            $ml = $ml['objet'].';'.$ml['id_objet'];
        }
        return $tab_ml;
    }

    static function fusion($id_mot,$id_mot_dispararition){
        $tab_objet_ajout = array_diff(MotLienQuery::getByIdmot($id_mot_dispararition),MotLienQuery::getByIdmot($id_mot));
        foreach($tab_objet_ajout as $ob){
            list($objet,$id_objet)=explode(';',$ob);
            MotLienQuery::mot_liens($id_mot,$id_objet,$objet);
        }
        MotLienQuery::create()->filterByIdMot($id_mot_dispararition)->delete();
        MotQuery::create()->filterByIdMot($id_mot_dispararition)->delete();
    }

}
