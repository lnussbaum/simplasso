<?php

use Base\MotQuery as BaseMotQuery;

/**
 * Skeleton subclass for performing query and update operations on the 'asso_mots' table.
 *
 *
 *
 * You should add additional methods to this class to meet the
 * application requirements.  This class will only be generated as
 * long as it does not already exist in the output directory.
 *
 */
class MotQuery extends BaseMotQuery
{

    public static $champs_recherche = ['nom','descriptif'];


    static function getAllByObjet($objet="", $lister = false, $grouper = false, $valuekey = false)
    {


        $tab_groupe = MotgroupeQuery::getByObjet($objet);

        $filtrer = array();

        if ($lister) {
            $tab = array();

            foreach ($tab_groupe as $groupe) {
                $option = $groupe['options'];
                $tab_mots = table_filtrer_valeur(tab('mot'),'id_motgroupe',$groupe['id_motgroupe']);
                $tab_objet_en_lien = explode(';',$groupe['objets_en_lien']);
                if(!empty($objet))
                    $tab_objet_en_lien =array_intersect([$objet],$tab_objet_en_lien);

                foreach($tab_objet_en_lien as $objet_en_lien) {

                    if (!$option) {
                        $nom_filtre = 'filtrer';
                        $tab_mots = table_trier_par($tab_mots,'nom');

                    } else {
                        $nom_filtre = $option[$objet_en_lien]['nom'];
                        $grouper = $option[$objet_en_lien]['optgroup'];
                        $classement = $option[$objet_en_lien]['classement'];
                        $indice = $option[$objet_en_lien]['indice'];// position des donnees groupe 1 et 2 exemple equipe locale ou doyennée lequel est avant
                        $tab_mots = table_trier_par($tab_mots,$classement);

                    }
                    if (!array_search($nom_filtre, $filtrer)) {
                        $filter[]=$nom_filtre;
                    }

                }

                if (!empty($tab_mots)) {
                    foreach ($tab_mots as $mot) {


                        foreach($tab_objet_en_lien as $objet_en_lien){
                            if ($grouper) {
                                $tab[$groupe['id_motgroupe']][$mot['id_mot']] = $mot;
                                //   $tab[$objet_en_lien][$nom_filtre][$groupe->getNom()][$k] = $v;

                            } else {
                                $tab[$mot['id_mot']] = $mot;
                                //  $tab[$objet_en_lien][$nom_filtre][$k] = $v;

                            }
                        }
                    }

                }
            }
            asort($tab);

        } else {
            $tab_mots = MotQuery::create()->filterByIdMotgroupe($tab_groupe->getPrimaryKeys())->orderByNom()->find();
            return $tab_mots;
        }
        return $tab;
    }


    public static function getWhere($params = array()) {

        list($from, $where) = getWhereObjet('mot',$params);
        $pr = descr('mot.nom_sql');
        if ( $id_motgroupe = request_ou_options('id_motgroupe', $params)){
            $where .= ' AND '.$pr.'.id_motgroupe = '.intval($id_motgroupe);
        }

        return [$from, $where];
    }

    public static function getAll($tab_id, $order = array(), $offset = 0, $limit = 0)
    {
        return getAllObjet('mot', $tab_id, $order, $offset, $limit);

    }






}
