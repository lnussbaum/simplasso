<?php

use Base\Prix as BasePrix;

/**
 * Skeleton subclass for representing a row from the 'asso_prixs' table.
 *
 *
 *
 * You should add additional methods to this class to meet the
 * application requirements.  This class will only be generated as
 * long as it does not already exist in the output directory.
 *
 */
class Prix extends BasePrix
{





    public function coherence_date()
    {
        $i = 1;
        $d = new datetime();
        $tab_prixs = PrixQuery::create()->filterByIdPrestation($this->getIdPrestation())->orderByDateDebut('DESC')->find();
        foreach ($tab_prixs as $p) {
            if ($i > 1) {
                if ($p->getDateFin()->format('Y-m-d') != $d->format('Y-m-d')) {
                    $prix1 = PrixQuery::create()->findPk($p->getIdPrix());
                    $prix1->setDateFin($d);
                    $prix1->save();
                }
            } elseif ($p->getDateFin()->format('Y-m-d') != '3000-12-31') {
                $prix1 = PrixQuery::create()->findPk($p->getIdPrix());
                $prix1->setDateFin('3000-12-31');
                $prix1->save();
            }
            $d = $p->getDateDebut()->sub(new DateInterval('P1D'));
            $i++;
        }
        if ($d->format('Y-m-d') != '1899-12-31' and $i > 1) {
            $prix1 = PrixQuery::create()->findPk($p->getIdPrix());
            $prix1->setDateDebut('1900-01-01');
            $prix1->save();
        }
        return;
    }


}
