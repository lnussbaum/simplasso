<?php

use Base\Autorisation as BaseAutorisation;

/**
 * Skeleton subclass for representing a row from the 'asso_autorisations' table.
 *
 *
 *
 * You should add additional methods to this class to meet the
 * application requirements.  This class will only be generated as
 * long as it does not already exist in the output directory.
 *
 */
class Autorisation extends BaseAutorisation
{

}
