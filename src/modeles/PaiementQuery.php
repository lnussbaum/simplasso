<?php

use Base\PaiementQuery as BasePaiementQuery;

/**
 * Skeleton subclass for performing query and update operations on the 'asso_paiements' table.
 *
 *
 *
 * You should add additional methods to this class to meet the
 * application requirements.  This class will only be generated as
 * long as it does not already exist in the output directory.
 *
 */
class PaiementQuery extends BasePaiementQuery
{
    public static $champs_recherche = ['numero','observation'];

    public static function getAll($tab_id, $order = array(), $offset = 0, $limit = 0)
    {
        return getAllObjet('paiement', $tab_id, $order, $offset, $limit);

    }


    public static function getWhere($params = array()) {

        list($from, $where) = getWhereObjet('paiement',$params);
        $where .= objet_selection_restriction_mot('paiement');
        $pr = descr('paiement.nom_sql');
        if ( $id_membre = request_ou_options('id_membre', $params)){
            $where .= ' AND '.$pr.'.objet = \'membre\'  AND '.$pr.'.id_objet='.intval($id_membre);
        }

        if ( $id_servicerendu = request_ou_options('id_servicerendu', $params)){
            $from['sp'] = 'asso_servicepaiements sp';
            $where .= ' AND '.$pr.'.id_paiement = sp.id_paiement AND sp.id_servicerendu = '.$id_servicerendu;
        }

        $statut = request_ou_options('statut', $params);
        if ( !empty($statut)){
            switch ($statut) {
                case 'valide':
                    $where .= ' AND '.$pr.'.comptabilise>0';
                    break;
                case 'depose':
                    $where .= ' AND '.$pr.'.remise>0 and '.$pr.'.comptabilise=0';
                    break;
                default : // en attente
                    $where .= ' AND '.$pr.'.remise = 0';
                    break;
            };
        }
        return [$from, $where];


    }








    static function listePaiementsDUnMembre($id_objet,$objet='membre')
    {

        global $app;
        $sql = 'SELECT DATE_FORMAT(pa.date_cheque,"%d/%m/%Y") AS tdc,
                 DATE_FORMAT(pa.date_enregistrement,"%d/%m/%Y") AS tde,pa.id_paiement as id_paiement ,pa.id_entite as id_entite,
                 en.nom as nomen , pa.montant ,  sum(sp.montant) as montantutilise, pa.id_tresor, tr.nom as nomtr
                FROM asso_paiements pa
                left join asso_servicepaiements sp on pa.id_paiement=sp.id_paiement
                left join asso_entites en on  pa.id_entite=en.id_entite
                left join asso_tresors tr on  pa.id_tresor=tr.id_tresor
                WHERE  pa.objet=\'' . $objet . '\'  and pa.id_objet=' . $id_objet . '
                and pa.id_entite IN  (' . implode(',', suc('entite')) . ')';
        if (pref('paiement.tresor_tous_un')) {
            $sql .=  'and pa . id_tresor IN(' . pref('paiement.tresor') . ')';
                }
        $sql .=  'GROUP BY pa.id_paiement ORDER BY pa.id_paiement ASC';
        return $app['db']->fetchAll($sql);
    }



    static function listeDesPaiementsPourUnServiceRendu($id_service_rendu,$id_membre=0)
    {
        global $app;
        $sql =  'SELECT DATE_FORMAT(sr.date_debut,"%d/%m/%Y") AS tdd,DATE_FORMAT(sr.date_fin,"%d/%m/%Y") AS tdf,
                sr.id_servicerendu ,sr.id_entite ,en.nom as nomen, pr.nom as nompr
                ,pr.prestation_type as prestationtype,pr.prixlibre as prixlibre ,sr.regle as regle , sr.montant,  sp.montant as montantpaye,sp.id_servicepaiement as id_servicepaiement
                FROM asso_servicerendus sr
                left join asso_servicepaiements sp on sr.id_servicerendu=sp.id_servicerendu
                left join asso_entites en on  sr.id_entite=en.id_entite
                left join asso_prestations pr on sr.id_prestation=pr.id_prestation';

        $sql .= ($id_membre)? ' WHERE sr.id_membre=' . $id_membre :' WHERE 1=1';
        if (!empty($id_service_rendu)) {
            $sql .= ' AND  sp.id_servicerendu=' . $id_service_rendu ;
            $sql .= ' ORDER BY sr.date_fin';
        }
        else{
            $sql='';
        }
        if (!empty($sql))
            return $app['db']->fetchAll($sql);
        return false;
    }

}
