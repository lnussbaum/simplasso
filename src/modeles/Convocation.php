<?php

use \Courrier;
use Map\CourrierTableMap;


/**
 * Skeleton subclass for representing a row from one of the subclasses of the 'asso_courriers' table.
 *
 *
 *
 * You should add additional methods to this class to meet the
 * application requirements.  This class will only be generated as
 * long as it does not already exist in the output directory.
 *
 */
class Convocation extends Courrier
{

    /**
     * Constructs a new Convocation class, setting the type column to CourrierTableMap::CLASSKEY_2.
     */
    public function __construct()
    {
        parent::__construct();
        $this->setType(CourrierTableMap::CLASSKEY_2);
    }

} // Convocation
