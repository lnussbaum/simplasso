<?php

use Base\Motgroupe as BaseMotgroupe;

/**
 * Skeleton subclass for representing a row from the 'asso_motgroupes' table.
 *
 *
 *
 * You should add additional methods to this class to meet the
 * application requirements.  This class will only be generated as
 * long as it does not already exist in the output directory.
 *
 */
class Motgroupe extends BaseMotgroupe
{
    function enLienAvec($objet){
        return in_array($objet,explode(';',parent::getObjetsEnLien()));
    }

    function getOption(){

        return json_decode($this->options,true);
    }

    function setOption($valeur){
        return $this->setOptions(json_encode($valeur));
    }

}
