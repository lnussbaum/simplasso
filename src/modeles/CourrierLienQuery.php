<?php

use Base\CourrierLienQuery as BaseCourrierLienQuery;

/**
 * Skeleton subclass for performing query and update operations on the 'asso_courriers_liens' table.
 *
 *
 *
 * You should add additional methods to this class to meet the
 * application requirements.  This class will only be generated as
 * long as it does not already exist in the output directory.
 *
 */
class CourrierLienQuery extends BaseCourrierLienQuery
{

    public static function getAll($tab_id, $order = array(), $offset = 0, $limit = 0)
    {
        return getAllObjet('courrierlien', $tab_id, $order, $offset, $limit);

    }


    public static function getWhere($params = array())
    {



        $where='1=1';
        $pr=descr('courrierlien.nom_sql');
        $from = array($pr => descr('courrierlien.table_sql').' '.$pr);

        // La recherche


        if ($search = request_ou_options('search', $params)) {

            $recherche = trim($search['value']);

            if (!empty($recherche)) {
                $pr_i=descr('individu.nom_sql');
                $from[$pr_i]=descr('individu.table_sql').' '.$pr_i;
                $where .= ' AND '.$pr.'.id_individu ='.$pr_i.'.id_individu ';
                $colle = 'AND (';
                if (intval($recherche) > 0) {
                    $where .= ' AND ( '.$pr.'.id_individu = \'' . $recherche . '\'';
                    $colle = 'OR';
                }
                $recherche_m = '+' . str_replace(' ', '* +', str_replace(array('@', '.'), ' ', $recherche)) . "*";
                $where .= ' ' . $colle . '( email like \'%' . $recherche . '%\' OR MATCH (nom,nom_famille,prenom) AGAINST (\'' . $recherche_m . '\' IN BOOLEAN MODE)))';

            }
        }

        if ($id_courrier = request_ou_options('id_courrier', $params)) {
            $id_courrier = is_array($id_courrier)?$id_courrier:[$id_courrier];
            $where .= " AND id_courrier IN (".implode(',',$id_courrier).')';
        }



        return [$from, $where];


    }




}
