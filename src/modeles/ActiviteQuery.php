<?php

use Base\ActiviteQuery as BaseActiviteQuery;

/**
 * Skeleton subclass for performing query and update operations on the 'assc_activites' table.
 *
 *
 *
 * You should add additional methods to this class to meet the
 * application requirements.  This class will only be generated as
 * long as it does not already exist in the output directory.
 *
 */
class ActiviteQuery extends BaseActiviteQuery
{

}
