<?php

use Base\Compte as BaseCompte;

/**
 * Skeleton subclass for representing a row from the 'assc_comptes' table.
 *
 *
 *
 * You should add additional methods to this class to meet the
 * application requirements.  This class will only be generated as
 * long as it does not already exist in the output directory.
 *
 */
class Compte extends BaseCompte
{

    function getcountecritures(){
        return EcritureQuery::create()->findByidCompte($this->id_compte)->count();
    }

    public function getQuiCree(){
        return Log::getQuiCree('compte',$this->id_compte);
    }

    public function getQuiModifie(){
        return Log::getQuiModifie('compte',$this->id_compte);
    }

}
