<?php

use Base\Notification as BaseNotification;

/**
 * Skeleton subclass for representing a row from the 'asso_notifications' table.
 *
 *
 *
 * You should add additional methods to this class to meet the
 * application requirements.  This class will only be generated as
 * long as it does not already exist in the output directory.
 *
 */
class Notification extends BaseNotification
{
    public function getQuiCree() {
        return Log::getQuiCree('zone', $this->id_zone);
    }

    public function getQuiModifie() {
        return Log::getQuiModifie('zone', $this->id_zone);
    }

}
