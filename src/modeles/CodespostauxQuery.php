<?php

use Base\CodespostauxQuery as BaseCodespostauxQuery;

/**
 * Skeleton subclass for performing query and update operations on the 'geo_codes_postaux' table.
 *
 *
 *
 * You should add additional methods to this class to meet the
 * application requirements.  This class will only be generated as
 * long as it does not already exist in the output directory.
 *
 */
class CodespostauxQuery extends BaseCodespostauxQuery
{


    public static $champs_recherche = ['codepostal','nom','nom_suite'];


    public static function getWhere($params = array()) {
        global $app;

        $where = "1=1";
        $pr = descr('codespostaux.nom_sql');
        $cle = descr('codespostaux.cle_sql');
        $tables = array($pr => descr('codespostaux.table_sql'));

        //print_r(request_ou_options('search', $params));
        // La recherche
        if (($search = request_ou_options('search', $params))) {

            $recherche = trim($search['value']);
            if (!empty($recherche)) {
                $colle = 'AND';
                if (intval($recherche) > 0) {
                    $where = $cle . ' = \'' . $recherche . '\'';
                    $colle = 'OR';
                }
                $where .= ' ' . $colle . ' ( ' . $pr . '.' . implode(' like '.$app['db']->quote($recherche.'%').' OR ' . $pr . '.',
                        CodespostauxQuery::$champs_recherche) . ' like '.$app['db']->quote($recherche.'%').')';
            }

        }



        $where = selection_ajouter_limitation_id('individu',$where,$params);

        if (request_ou_options('inverse_selection', $params)) {
            $where = 'NOT ('.$where.')';
        }


        foreach ($tables as $k => $t) {
            $from[$k] = $t . ' ' . $k;
        }


        return [$from, $where];

    }



/*
    public static function getSelection($params=array())
    {


        $where="1=1";
        $pr = descr('codespostaux.nom_sql');
       $tables = array($pr => 'geo_codespostaux');


        // La recherche
        if( $search = request_ou_options('search', $params)){
            $recherche = trim($search['value']);


            if (!empty($recherche)) {

                if (intval($recherche)>0){
                   $where .= ' AND codepostal like \''.$recherche.'%\' ';
                }
                else {
                    $where .= ' AND CONCAT(nom,\' \',nom_suite) like \'' . $recherche . '%\' ';

                }
            }
        }



        foreach($tables  as $k => $t)
            $from[]=$t.' '.$k;


        return getRequeteObjetNb('codespostaux',$from,$where);



    }
*/
}
