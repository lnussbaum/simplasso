<?php

use Base\Activite as BaseActivite;

/**
 * Skeleton subclass for representing a row from the 'assc_activites' table.
 *
 *
 *
 * You should add additional methods to this class to meet the
 * application requirements.  This class will only be generated as
 * long as it does not already exist in the output directory.
 *
 */
class Activite extends BaseActivite
{
    function getActivitenom(){
        return $this->nomcourt.' '.$this->nom;
    }

    public function getQuiCree(){
        return Log::getQuiCree('activite',$this->id_activite);
    }

    public function getQuiModifie(){
        return Log::getQuiModifie('activite',$this->id_activite);
    }

}
