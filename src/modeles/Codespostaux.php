<?php

use Base\Codespostaux as BaseCodespostaux;

/**
 * Skeleton subclass for representing a row from the 'geo_codes_postaux' table.
 *
 *
 *
 * You should add additional methods to this class to meet the
 * application requirements.  This class will only be generated as
 * long as it does not already exist in the output directory.
 *
 */
class Codespostaux extends BaseCodespostaux
{

    public static function getAll($tab_codes_postaux,$order=array(),$offset=0,$limit=0)
    {


        $q = CodespostauxQuery::create()->filterByPrimaryKeys($tab_codes_postaux);


        if (!empty($order) && is_array($order)) {
            foreach($order as $k=>$o){
                $q=$q->orderBy($k,$o);
            }
        }
        if ($offset > 0)
            $q = $q->offset($offset);
        if ($limit > 0)
            $q = $q->limit($limit);


        return $q->find();


    }
}
