<?php

use Base\Prestation as BasePrestation;

/**
 * Skeleton subclass for representing a row from the 'asso_prestations' table.
 *
 *
 *
 * You should add additional methods to this class to meet the
 * application requirements.  This class will only be generated as
 * long as it does not already exist in the output directory.
 *
 */
class Prestation extends BasePrestation
{
    public function calculeDateDebut($date_ori = null)
    {
        //TODO: Ecrire l'algorithme de calcul des date
        // la date origine est J+1 de la derniere cotisation ou la date du jour en cas d'absence.
//pour test        $date_ori = new DateTime("2014-04-01");
        $date_jour = new DateTime();
        if ($date_ori == null) {
            $date_ori = $date_jour;
            $premiere = true;
//            echo '<BR>date origine  : aucune ';
        } else {
            $premiere = false;
//            echo '<BR>date origine  :' . date_format($date_ori, 'Y-m-d') . '  ';
        }
        $format1 = 'Y-m-d';
        $format2 = "";
        if ($date_ori <= $date_jour or $premiere) {
            //           echo ' date du jour inferieure à la date de la prochaine cotisation on ne modife rien '.date_format($date_jour, 'Y-m-d').'**<BR>';
            $temp = $this->periodique;
            if (substr_count($temp, ',') < 3) {
                $temp = '0,M,12,1,1,fin';
            }
            list($periodique,
                $unite,
                $duree,
                $mois_debut,
                $jour_debut
                ) = explode(',', $this->periodique);
            $retard_jours = intval($this->getRetardJours());
            $jour_debut = str_pad($jour_debut, 2, '0', STR_PAD_LEFT);
            $mois_debut = str_pad($mois_debut, 2, '0', STR_PAD_LEFT);
//pour test            $mois_debut = "04";
//            echo '  jour_debut :' . $jour_debut . ' mois debut :' . $mois_debut . '  retard autorise :' . $retard_jours . '<BR>periodique :' . $periodique . ' :';
            switch ($periodique) {
                case 0: //pas de duree
                    $duree_jour = 0;
                    $format1 = 'Y';
                    $format2 = '-01-01';
//                    echo "0 Pas de duree ";
                    break;
                case 1: //fixe
                    $format1 = 'Y';
                    $format2 = '-' . $mois_debut . '-' . $jour_debut;
                    $duree_jour = 365;
                    if ($unite == 'M') {
                        if (intval($mois_debut) == 0) {
                            $format1 = 'Y-m';
                            $format2 = '-' . $jour_debut;
                        } else {
                            $format1 = 'Y';
                            $format2 = '-' . $mois_debut . '-' . $jour_debut;
                        }
                        $duree_jour = 30.5 * $duree;
                    } elseif ($unite == 'D') {
                        $format1 = 'Y-m';
                        $format2 = '-' . $jour_debut;
                        $duree_jour = $duree;
                    }
//                    echo "1 duree fixe ";
                    break;
                case 2: //glissant
                    $duree_jour = 365;
                    if ($unite == 'M') {
                        $duree_jour = 30.5 * $duree;
                    } elseif ($unite == 'D') {
                        $duree_jour = $duree;
                    }
                    $format1 = 'Y-m-d';
                    $format2 = '';
//                    echo "2 duree Glissant";
                    break;
            }
            $format1 = $format1 . $format2;
            $format = $format1 . ' 00:00:00';
//            echo " format :" . $format . '  ajoute  :' . $ajout . ' Duree en jours :' . $duree_jour;
            if ($periodique > 0) {
                $ajout = 'P' . $duree . $unite;
                if ($date_ori == $date_jour) {
                    $date_ori = DateTime::createFromFormat('Y-m-d H:i:s', $date_ori->format($format));
                }
                $retard_reel = $date_ori->diff($date_jour)->format('%a');
                If ($date_jour <= $date_ori){ // la différence est toujours positive mettre en négatif les date suppérieure quand il n'y a pas de service rendu antérieur pour cette prestation
                    $retard_reel = -$retard_reel;
                }
//                echo '  difference entre origine et date debut  :' . $retard_reel . ' jours';
                $retard_modulo = ($retard_reel % $duree_jour);
                $retard_nbperiode = (int)($retard_reel / $duree_jour);
//                echo '<BR> retard autorise :' . $retard_jours . ' Nombre de periode de retard ' . $retard_nbperiode . '  modulo retard modulo  : ' . $retard_modulo;
                if ($retard_modulo >= $retard_jours) {
                    $retard_nbperiode++;
                }
//                echo '<BR> retard autorise :' . $retard_jours . ' Nombre de periode de retard ' . $retard_nbperiode . '  modulo retard modulo  : ' . $retard_modulo;
                if ($retard_nbperiode <= 0) {
                    $date_debut = clone $date_ori;
                } else {
                    $modif = 'P' . ($retard_nbperiode * $duree) . $unite;
                    $date_debut = $date_ori->add(new DateInterval($modif));
//                    echo '  modification :' . $modif ;
                }
//                echo '  resultat :' . date_format($date_debut, 'Y-m-d');
            } else {
                $date_debut = clone $date_jour;
            }
        } else {
            $date_debut = clone $date_ori;
        }

//        $format = $format1 . ' 00:00:00';
//        //ajouter le controle du format
//    echo  '<BR><BR> Avant application du format : '. date_format($date_debut, 'Y-m-d') ;
//		echo '<BR> Format '. $format ;
//        $date_debut = DateTime::createFromFormat('Y-m-d H:i:s', $date_debut->format('Y-m-d'));
//        echo '<BR>date renvoyee :' . date_format($date_debut, 'Y-m-d');
        return $date_debut;
    }

    public function calculeDateFin($date)
    {
        if (is_a($date, 'DateTime')) {
            $date_fin = clone $date;
            list($periodique,
                $unite,
                $duree,
                $mois_debut,
                $jour_debut,
                $truc
                ) = explode(',', $this->periodique);
            if ($periodique == 1 || $periodique == 2) {
                $ajout = 'P' . $duree . $unite;
                $date = $date_fin->add(new DateInterval($ajout))->sub(new DateInterval('P1D'));
            } else {
                $date = new DateTime('3000-01-01');
            }
        }
        //       echo '<BR>date fin  ' . date_format($date ,'Y-m-d') . '**';
        return $date;
    }


    public function getPeriodiqueLibelle(){
        $data = array();
        if (substr_count($this->getPeriodique(), ',') >= 5) {
            list($data['periodiquea'],
                $data['periodiqueb'],
                $data['duree'],
                $data['mois_debut'],
                $data['jour_debut'],
                $data['fin'],
                ) = explode(',', $this->getPeriodique());
            $tempA = getListePeriodiqueA();
            $tempB = getListePeriodiqueB();
            return $tempA[$data['periodiquea']]
            . ' durée de ' . $data['duree'] . ' ' . $tempB[$data['periodiqueb']] .
            ' debut le ' . $data['jour_debut'].' '. strtolower(date("F", mktime(0, 0, 0, $data['mois_debut'], $data['jour_debut'], 2000)));
//$app-trans(strt...
        }
        return "non renseigné";

    }





    public function getPrix($date){



    }

}
