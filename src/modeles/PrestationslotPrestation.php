<?php

use Base\PrestationslotPrestation as BasePrestationslotPrestation;

/**
 * Skeleton subclass for representing a row from the 'asso_prestationslots_prestations' table.
 *
 *
 *
 * You should add additional methods to this class to meet the
 * application requirements.  This class will only be generated as
 * long as it does not already exist in the output directory.
 *
 */
class PrestationslotPrestation extends BasePrestationslotPrestation
{

}
