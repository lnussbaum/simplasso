<?php

use Base\Mot as BaseMot;

/**
 * Skeleton subclass for representing a row from the 'asso_mots' table.
 *
 *
 *
 * You should add additional methods to this class to meet the
 * application requirements.  This class will only be generated as
 * long as it does not already exist in the output directory.
 *
 */
class Mot extends BaseMot
{
    static function associerMotsObjet($tab_id_mot,$objet,$id_objet){

        MotLienQuery::create()->filterByObjet($objet)->filterByIdObjet($id_objet)->delete();
        foreach($tab_id_mot as $mot){
            $mot_lien = new MotLien();
            $mot_lien->setIdMot($mot);
            $mot_lien->setIdObjet($id_objet);
            $mot_lien->setObjet($objet);
            $mot_lien->save();
        }
        return true;
    }
    /**
     * MotLien
     * @param integer $id_mot
     * @param integer $id_objet
     * @param string $objet
     * @param string $observation
     * @param bool|false $suppresion
     * @return bool
     * @throws \Propel\Runtime\Exception\PropelException
     */
    static function MotLien($id_mot, $id_objet, $objet, $observation = '', $suppresion = false)
    {
        $lien = MotLienQuery::create()->filterByIdMot($id_mot)->filterByIdObjet($id_objet)->findOneByObjet($objet);
        $ajout = true;
        if ($suppresion && $lien) {
            $lien->delete();
            $ajout = false;
        } else {
            if (!$lien) {
                $lien = new MotLien();
                $lien->setIdObjet($id_objet);
                $lien->setObjet($objet);
                $lien->setIdMot($id_mot);
            }
            $lien->setObservation($observation);
            $ok=$lien->save();
        }
        return $ajout;
    }
    static function MotLienstransfere($objet_ids)
    {
        $objet_ids = (is_array($objet_ids)) ? $objet_ids : array($objet_ids);
        foreach ($objet_ids as  $el) {
            $liens = MotLienQuery::create()->filterByIdObjet($el['de'])->findByObjet($el['objet']);
            foreach ($liens as $lien) {
                $mot = $lien->getIdMot();
                Mot::MotLien($lien->getIdMot(), $el['de'], $el['objet'], '', true);
                Mot::MotLien($mot, $el['vers'], 'membre', 'fusion ' . $el['de']);
            }
        }
    }
}
