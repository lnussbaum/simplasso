<?php

use Base\PreferenceQuery as BasePreferenceQuery;
use Propel\Runtime\ActiveQuery\Criteria as Criteria;
/**
 * Skeleton subclass for performing query and update operations on the 'asso_preferences' table.
 *
 *
 *
 * You should add additional methods to this class to meet the
 * application requirements.  This class will only be generated as
 * long as it does not already exist in the output directory.
 *
 */
class PreferenceQuery extends BasePreferenceQuery
{
    function getValeur()
    {
        return json_decode($this->variables, true);
    }


    static  function getPreferenceUtilisateur($id){
        $tab=array();
        $tab_config = \PreferenceQuery::create()->filterByIdIndividu($id)->filterByIdEntite(null,Criteria::ISNULL)->find();
        foreach($tab_config as $c){
            if ($c->getIdEntite())
                $tab['entite'.$c->getIdEntite()][$c->getNom()]=$c->getValeur();
            else
                $tab['generique'][$c->getNom()]=$c->getValeur();
        }
       return $tab;
    }

}
