<?php

use Base\Composition as BaseComposition;
use Propel\Runtime\Map\TableMap;
/**
 * Skeleton subclass for representing a row from the 'com_compositions' table.
 *
 *
 *
 * You should add additional methods to this class to meet the
 * application requirements.  This class will only be generated as
 * long as it does not already exist in the output directory.
 *
 */
class Composition extends BaseComposition
{


    function getBlocsSMS(){
        return $this->getBlocsByCanal('S');
    }

    function getBlocsLettre(){
        return $this->getBlocsByCanal('L');
    }

    function getBlocsEmail(){
        return $this->getBlocsByCanal('E');
    }

    function getBlocsByCanal($canal){
        $tab_id_bloc = tab('composition.' . $this->getPrimaryKey() . '.blocs');
        $tab_bloc = tab('bloc');
        $tab_tmp=[];
        foreach($tab_id_bloc as $id_bloc=>$ordre){
            if($canal == $tab_bloc[$id_bloc]['canal']){
                $tab_tmp[$id_bloc] = $tab_bloc[$id_bloc];
                $tab_tmp[$id_bloc]['ordre']=$ordre;
            }
        }
        $tab_tmp = table_trier_par($tab_tmp,'ordre');
        return $tab_tmp;
    }

}
