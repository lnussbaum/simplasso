<?php

use \Prestation;
use Map\PrestationTableMap;


/**
 * Skeleton subclass for representing a row from one of the subclasses of the 'asso_prestations' table.
 *
 *
 *
 * You should add additional methods to this class to meet the
 * application requirements.  This class will only be generated as
 * long as it does not already exist in the output directory.
 *
 */
class Adhesion extends Prestation
{

    /**
     * Constructs a new Adhesion class, setting the prestation_type column to PrestationTableMap::CLASSKEY_6.
     */
    public function __construct()
    {
        parent::__construct();
        $this->setPrestationType(PrestationTableMap::CLASSKEY_6);
    }

} // Adhesion
