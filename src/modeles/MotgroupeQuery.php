<?php

use Base\MotgroupeQuery as BaseMotgroupeQuery;

/**
 * Skeleton subclass for performing query and update operations on the 'asso_motgroupes' table.
 *
 *
 *
 * You should add additional methods to this class to meet the
 * application requirements.  This class will only be generated as
 * long as it does not already exist in the output directory.
 *
 */
class MotgroupeQuery extends BaseMotgroupeQuery
{




    public static $champs_recherche = ['nom','descriptif'];


    public static function getAll($tab_id, $order = array(), $offset = 0, $limit = 0)
    {
        return getAllObjet('motgroupe', $tab_id, $order, $offset, $limit);

    }


    static function getByObjet($objet="")
    {

        $tab_groupe = tab('motgroupe');
        if (!empty($objet)) {
            foreach ($tab_groupe as $key => $groupe) {
                $tab=explode(',',$groupe['objets_en_lien']);
                if (!in_array($objet,$tab)) {
                    unset($tab_groupe[$key]);
                }
            }

        }

        return $tab_groupe;
    }


    public static function getWhere($params = array()) {



        list($from, $where) = getWhereObjet('motgroupe', $params );
        $pr = descr('motgroupe.nom_sql');
        if ($systeme = request_ou_options('systeme', $params)){
            $where .= ' AND '.$pr.'.systeme ='.$systeme;
        };
        return [$from, $where];

    }


}
