<?php

use Base\Importligne as BaseImportligne;

/**
 * Skeleton subclass for representing a row from the 'asso_importlignes' table.
 *
 *
 *
 * You should add additional methods to this class to meet the
 * application requirements.  This class will only be generated as
 * long as it does not already exist in the output directory.
 *
 */
class Importligne extends BaseImportligne
{
    function getProposition($format_array=true)
    {
        return json_decode($this->getPropositions(), $format_array);
    }


    function setProposition($args) {
        return $this->setPropositions(json_encode($args));
    }

    function getVariable() {
        return json_decode($this->getVariables(),true);
    }

    function setVariable($args) {
        return $this->setVariables(json_encode($args));
    }

}
