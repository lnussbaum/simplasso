<?php

use Base\Import as BaseImport;

/**
 * Skeleton subclass for representing a row from the 'asso_imports' table.
 *
 *
 *
 * You should add additional methods to this class to meet the
 * application requirements.  This class will only be generated as
 * long as it does not already exist in the output directory.
 *
 */

use Propel\Runtime\Connection\ConnectionInterface;

class Import extends BaseImport
{
    function getModification()
    {
        return json_decode($this->getModifications(), true);
    }

    function setModification($args)
    {
        return $this->setModifications(json_encode($args));
    }

    function getInformation()
    {
        return json_decode($this->getInformations(), true);
    }


    function setInformation($args)
    {
        return $this->setInformations(json_encode($args));
    }
}
