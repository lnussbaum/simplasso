<?php

use Base\Infolettre as BaseInfolettre;

/**
 * Skeleton subclass for representing a row from the 'com_infolettres' table.
 *
 *
 *
 * You should add additional methods to this class to meet the
 * application requirements.  This class will only be generated as
 * long as it does not already exist in the output directory.
 *
 */
class Infolettre extends BaseInfolettre
{

}
