<?php

use Base\Ged as BaseGed;

/**
 * Skeleton subclass for representing a row from the 'assc_geds' table.
 *
 *
 *
 * You should add additional methods to this class to meet the
 * application requirements.  This class will only be generated as
 * long as it does not already exist in the output directory.
 *
 */
class Ged extends BaseGed
{

    function setFile($fichier){


        $path_parts = pathinfo($fichier);
        $this->setExtension($path_parts['extension']);
        $this->setNom($path_parts['filename']);
        $this->save();
        $fp = fopen($fichier, 'rb');
        $this->setImage($fp);


    }

    function nettoyage_cache(){
        global $app;
        $image_dir = $app['image.path'].'/';
        $tmp_name = $image_dir.md5($this->getIdGed().'-original').'.'.$this->getExtension();
        if (file_exists($tmp_name))
        { unlink($tmp_name); }
        $tmp_name = $image_dir.md5($this->getIdGed().'-200x200').'.'.$this->getExtension();
        if (file_exists($tmp_name))
        { unlink($tmp_name);}

    }


    function saveFile($repertoire,$taille="original"){

        $tmp_name = md5($this->getIdGed().'-'.$taille).'.'.$this->getExtension();
        if (!file_exists($repertoire.$tmp_name)) {
            if (!file_exists($repertoire)) {
                mkdir($repertoire);
            }
            file_put_contents($repertoire . $tmp_name, $this->getImage());
        }
        return $tmp_name;
    }

}
