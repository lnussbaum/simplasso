<?php

use Base\Tva as BaseTva;

/**
 * Skeleton subclass for representing a row from the 'asso_tvas' table.
 *
 *
 *
 * You should add additional methods to this class to meet the
 * application requirements.  This class will only be generated as
 * long as it does not already exist in the output directory.
 *
 */
class Tva extends BaseTva
{
    public function getQuiCree(){
        return Log::getQuiCree('tva',$this->id_tva);
    }

    public function getQuiModifie(){
        return Log::getQuiModifie('tva',$this->id_tva);
    }


    function tva_coherence_date()
    {
        $i = 1;
        $d = new datetime();
        $tab = TvatauxQuery::create()->filterByIdTva($this->getPrimaryKey())->orderByDateDebut('DESC')->find();
        foreach ($tab as $objet_data) {
            if ($i > 1) {
                if ($objet_data->getDateFin()->format('Y-m-d') != $d->format('Y-m-d')) {
                    $objet_data1 = TvatauxQuery::create()->findPk($objet_data->getIdTvataux());
                    $objet_data1->setDateFin($d);
                    $objet_data1->save();
                }
            } elseif ($objet_data->getDateFin()->format('Y-m-d') != '3000-12-31') {
                $objet_data1 = TvatauxQuery::create()->findPk($objet_data->getIdTvataux());
                $objet_data1->setDateFin('3000-12-31');
                $objet_data1->save();
            }
            $d = $objet_data->getDateDebut()->sub(new DateInterval('P1D'));
            $i++;
        }
        if ($d->format('Y-m-d') != '1899-12-31' and $i > 1) {
            $objet_data1 = TvatauxQuery::create()->findPk($objet_data->getIdTvataux());
            $objet_data1->setDateDebut('1900-01-01');
            $objet_data1->save();
        }
        return;
    }


}
