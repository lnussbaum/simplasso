<?php

use Base\Membre as BaseMembre;
use Propel\Runtime\ActiveQuery\Criteria as Criteria;
/**
 * Skeleton subclass for representing a row from the 'asso_membres' table.
 *
 *
 *
 * You should add additional methods to this class to meet the
 * application requirements.  This class will only be generated as
 * long as it does not already exist in the output directory.
 *
 */
class Membre extends BaseMembre
{


    public function getIndividusEnCours()
    {
        $datetime = new DateTime();
        return IndividuQuery::create()
            ->useMembreIndividuQuery()
            ->filterByDateDebut($datetime, Criteria::LESS_EQUAL)
            ->filterByDateFin(null, Criteria::ISNULL)
            ->filterByIdMembre($this->getPrimaryKey())
            ->endUse()
            ->find();
    }



    function getCompilationNomIndividu($avec_titulaire=true){
        $tab_individu = $this->getIndividusEnCours();
        $tab_nom=[];

        foreach($tab_individu as $individu){
            if($this->getIdIndividuTitulaire()<>$individu->getIdIndividu()||$avec_titulaire)
            $tab_nom[] = $individu->getNomFamille() . ' ' . $individu->getPrenom();
        }
        return implode(', ',$tab_nom);
    }


    public function solde()
    {
        return $this->paiementMontant() - $this->servicerenduMontant();
    }

    public function paiementMontant()
    {
        global $app;
        $date_debut = pref('solde.amj_paiement')->format('Y-m-d');
        $sql = 'SELECT    sum(montant) as solde FROM asso_paiements WHERE objet = \'membre\' AND id_objet=' . $this->id_membre . ' and date_enregistrement>="' . $date_debut . '"';
        return $app['db']->fetchColumn($sql);

    }

    public function servicerenduMontant()
    {
        global $app;
        $date_debut = pref('solde.amj_servicerendu')->format('Y-m-d');
        $sql = 'SELECT   sum(montant*quantite) as depuis FROM asso_servicerendus WHERE id_membre=' . $this->id_membre . ' and date_debut>="' . $date_debut . '"';
        return $app['db']->fetchColumn($sql);
    }

    public function getAdhesionRemboursable()
    {

        $derniere_adhesion = ServicerenduQuery::create()
            ->filterByIdMembre($this->id_membre)
            ->filterByIdPrestation(array_keys(getPrestationDeType('adhesion')))
            ->orderByCreatedAt('DESC')
            ->findOne();

        if (isset($derniere_adhesion) and $derniere_adhesion->getMontant() > 0) {
            return array(
                $derniere_adhesion,
                ServicepaiementQuery::create()
                    ->filterByPrimaryKeys($derniere_adhesion->getServicepaiements()->getPrimaryKeys())
                    ->withColumn('SUM(montant)', 'total')
                    ->findOne()
                    ->getTotal()
            );
        }
        return array($derniere_adhesion, 0);
    }


    public function getQuiCree()
    {
        return Log::getQuiCree('membre', $this->getPrimaryKey());
    }

    public function getQuiModifie()
    {
        return Log::getQuiModifie('membre', $this->getPrimaryKey());
    }

    public function getMots()
    {
        return MotLienQuery::create()
            ->filterByObjet('membre')
            ->filterByIdObjet($this->getPrimaryKey())
            ->find()->toKeyValue('IdMot', 'IdMot');


    }

    public function estSorti()
    {
        return !($this->date_sortie === null);
    }

    public function setMots($tab_mots)
    {
        MotLienQuery::create()
            ->filterByObjet('membre')
            ->filterByIdObjet($this->getPrimaryKey())
            ->delete();

        foreach ($tab_mots as $mot) {
            $motlien = new MotLien();
            $motlien->setIdMot($mot)->setIdObjet($this->getIdMembre())->setObjet('membre')->save();
        }

        return true;
    }

    public function getEtatAdhesion($id_entite = null)
    {
        return $this->getEtatServicerendu('adhesion', $id_entite);
    }

    public function getEtatServicerendu($type_prestation = null, $id_entite = null)
    {
        $req = ServicerenduQuery::create()
            ->filterByIdMembre($this->getIdMembre());
        if ($type_prestation) {
            $tab_prestations = array_keys(getPrestationEntite($type_prestation));
            $req = $req->filterByIdPrestation($tab_prestations);
        }
        $req = $req
            ->filterByDateDebut(new DateTime(), Criteria::LESS_EQUAL)
            ->filterByDateFin(new DateTime(), Criteria::GREATER_THAN);
        if ($id_entite) {
            $req = $req->filterByIdEntite($id_entite);
        }
        return $req->exists();
    }

    public function getEtatCotisation($id_entite = null)
    {
        return $this->getEtatServicerendu('cotisation', $id_entite);
    }

    public function getAbonnement($id_entite = null, $en_cours = false)
    {
        return $this->getServicerendu('abonnement', $id_entite, $en_cours);
    }

    public function getServicerendu($type_prestation = null, $id_entite = null, $en_cours = false)
    {

        $tab_servicerendu = $this->getServicerendus();
        if($type_prestation)
            $tab_prestations = array_keys(getPrestationEntite($type_prestation));
        $date_du_jour=new DateTime();
        $tab_sr=[];
        foreach($tab_servicerendu as $sr){
            if ($type_prestation && !in_array($sr->getIdPrestation(),$tab_prestations)){
                continue;
            }
            if ($id_entite && $id_entite!=$sr->getIdEntite()){
                continue;
            }
            if ($en_cours && $sr->getDateDebut()> $date_du_jour && $date_du_jour < $sr->getDateFin()){
                continue;
            }
            $tab_sr[] = $sr;
        }
        return $tab_sr;
       /* $req = ServicerenduQuery::create()
            ->filterByIdMembre($this->getIdMembre());
        if ($type_prestation) {
            $tab_prestations = array_keys(getPrestationEntite($type_prestation));
            $req = $req->filterByIdPrestation($tab_prestations);
        }
        if ($en_cours) {
            $req = $req->filterByDateDebut(new DateTime(), Criteria::LESS_EQUAL)
                ->filterByDateFin(new DateTime(), Criteria::GREATER_THAN);
        }
        if ($id_entite) {
            $req = $req->filterByIdEntite($id_entite);
        }
        return $req->orderByDateFin('DESC')->find();*/
    }

    public function getDon($id_entite = null, $en_cours = false)
    {
        return $this->getServicerendu('don', $id_entite, $en_cours);
    }

    public function getVente($id_entite = null, $en_cours = false)
    {
        return $this->getServicerendu('vente', $id_entite, $en_cours);
    }

    public function getPerte($id_entite = null, $en_cours = false)
    {
        return $this->getServicerendu('perte', $id_entite, $en_cours);
    }

    public function getAdhesion($id_entite = null, $en_cours = false)
    {
        return $this->getServicerendu('adhesion', $id_entite, $en_cours);
    }

    public function getCotisation($id_entite = null, $en_cours = false)
    {
        return $this->getServicerendu('cotisation', $id_entite, $en_cours);
    }


    public function getCotisationHistorique($nb_annee = 4)
    {
        $tab[] = ['Période', 'Montant', 'Date d\'enregistrement'];
        $tab_cotis = $this->getCotisation();
        $i = 1;
        foreach ($tab_cotis as $cotis) {
            if ($i > $nb_annee) {
                break;
            }
            $tab[] = [
                $cotis->getDateDebut()->format('d/m/Y') . ' à ' . $cotis->getDateFin()->format('d/m/Y'),
                $cotis->getMontant() . '€',
                $cotis->getCreatedAt()->format('d/m/Y')
            ];
            $i++;

        }
        return $tab;
    }

    public function getStat(){
        global $app;
        $sql = 'SELECT   pr.nom,sum(sr.quantite) as quantite FROM asso_servicerendus sr ,asso_prestations pr WHERE sr.id_membre=' . $this->id_membre.' and sr.id_prestation = pr.id_prestation and sr.id_prestation > 0 group by sr.id_prestation ';// '. ' and date_debut>="' . $date_debut . '"';
        $statement = $app['db']->executeQuery($sql);
        $tab_result = array();
        while ($val = $statement->fetch()) {
             $tab_result[$val['nom']] = intval($val['quantite']);
        }
        return $tab_result;
    }

    public function getStatAncien(){
        $tab_sr = tab('prestation');
        $tab_type_prestation = table_simplifier(tab('prestation_type'));
        foreach($tab_type_prestation as $k=>$nom){
            $tab_prestation = array_keys(getPrestationDeType($k));
            $tab_sr2 = table_filtrer_valeur($tab_sr,'id_prestation',$tab_prestation);
            $tab_result[$nom] = count($tab_sr2);
        }
        return $tab_result;
    }

function getCotisationPeriode($date_debut,$date_fin){

    $tab_cotisation = [];
    $tab_cotis = $this->getCotisation();
    $date_debut = date_create_from_format('Y-m-d',$date_debut);
    $date_fin = date_create_from_format('Y-m-d',$date_fin);
    $tab_nomcourt_prestation = table_simplifier(tab('prestation'),'nomcourt');
    foreach($tab_cotis as $cotis){


        if ($date_debut <= $cotis->getDateFin() && $cotis->getDateDebut() >= $date_fin ){
            $tab_cotisation[]= $tab_nomcourt_prestation[$cotis->getIdPrestation()];
        }

    }
    return implode(', ',$tab_cotisation);
}


    function getNbPouvoir($date){

        $nb_voix = 0;
        $tab_cotis = $this->getCotisation();
        $date = date_create_from_format('Y-m-d',$date);
        $tab_nomcourt_prestation = table_simplifier(tab('prestation'),'nb_voix');
        foreach($tab_cotis as $cotis){


            if ($date>= $cotis->getDateDebut() && $cotis->getDateFin() > $date){
                $nb_voix+= $tab_nomcourt_prestation[$cotis->getIdPrestation()];
            }

        }
        return $nb_voix;
    }



}
