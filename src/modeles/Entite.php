<?php

use Base\Entite as BaseEntite;
use Propel\Runtime\ActiveQuery\Criteria as Criteria;
/**
 * Skeleton subclass for representing a row from the 'asso_entites' table.
 *
 *
 *
 * You should add additional methods to this class to meet the
 * application requirements.  This class will only be generated as
 * long as it does not already exist in the output directory.
 *
 */
class Entite extends BaseEntite
{
    public function estGeoreference(){

        return PositionLienQuery::create()->filterByObjet('entite')->filterByIdObjet($this->getPrimaryKey())->exists();
    }
    public function getQuiCree()
    {
        return Log::getQuiCree('entite', $this->id_entite);
    }

    public function getQuiModifie()
    {
        return Log::getQuiModifie('entite', $this->id_entite);
    }
    public static function getAll($tab_id_entite, $order = array(), $offset = 0, $limit = 0, $options = array()) {

        $q = EntiteQuery::create();
        if ($tab_id_entite) {
            if (isset($options['not_in']) && $options['not_in']) {
                $q = $q->filterBy('IdEntite', $tab_id_entite, Criteria::NOT_IN);
            } else {
                $q = $q->filterByPrimaryKeys($tab_id_entite);
            }
        }

        if (!empty($order) && is_array($order)) {
            foreach ($order as $k => $o) {
                $q = $q->orderBy($k, $o);
            }
        }
        if ($offset > 0) {
            $q = $q->offset($offset);
        }
        if ($limit > 0) {
            $q = $q->limit($limit);
        }
        return $q->find();
    }

}
