<?php

use Base\Zone as BaseZone;

/**
 * Skeleton subclass for representing a row from the 'geo_zones' table.
 *
 *
 *
 * You should add additional methods to this class to meet the
 * application requirements.  This class will only be generated as
 * long as it does not already exist in the output directory.
 *
 */
class Zone extends BaseZone
{
    public function getQuiCree() {
        return Log::getQuiCree('zone', $this->id_zone);
    }

    public function getQuiModifie() {
        return Log::getQuiModifie('zone', $this->id_zone);
    }


}
