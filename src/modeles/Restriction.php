<?php

use Base\Restriction as BaseRestriction;

/**
 * Skeleton subclass for representing a row from the 'asso_restrictions' table.
 *
 *
 *
 * You should add additional methods to this class to meet the
 * application requirements.  This class will only be generated as
 * long as it does not already exist in the output directory.
 *
 */
class Restriction extends BaseRestriction
{
    public function getQuiCree(){
        return Log::getQuiCree('restriction',$this->id_restriction);
    }

    public function getQuiModifie(){
        return Log::getQuiModifie('restriction',$this->id_restriction);
    }

}
