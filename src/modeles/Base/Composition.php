<?php

namespace Base;

use \Bloc as ChildBloc;
use \BlocQuery as ChildBlocQuery;
use \Composition as ChildComposition;
use \CompositionQuery as ChildCompositionQuery;
use \CompositionsBloc as ChildCompositionsBloc;
use \CompositionsBlocQuery as ChildCompositionsBlocQuery;
use \Courrier as ChildCourrier;
use \CourrierQuery as ChildCourrierQuery;
use \Exception;
use \PDO;
use Map\CompositionTableMap;
use Map\CompositionsBlocTableMap;
use Map\CourrierTableMap;
use Propel\Runtime\Propel;
use Propel\Runtime\ActiveQuery\Criteria;
use Propel\Runtime\ActiveQuery\ModelCriteria;
use Propel\Runtime\ActiveRecord\ActiveRecordInterface;
use Propel\Runtime\Collection\Collection;
use Propel\Runtime\Collection\ObjectCollection;
use Propel\Runtime\Connection\ConnectionInterface;
use Propel\Runtime\Exception\BadMethodCallException;
use Propel\Runtime\Exception\LogicException;
use Propel\Runtime\Exception\PropelException;
use Propel\Runtime\Map\TableMap;
use Propel\Runtime\Parser\AbstractParser;

/**
 * Base class that represents a row from the 'com_compositions' table.
 *
 *
 *
 * @package    propel.generator..Base
 */
abstract class Composition implements ActiveRecordInterface
{
    /**
     * TableMap class name
     */
    const TABLE_MAP = '\\Map\\CompositionTableMap';


    /**
     * attribute to determine if this object has previously been saved.
     * @var boolean
     */
    protected $new = true;

    /**
     * attribute to determine whether this object has been deleted.
     * @var boolean
     */
    protected $deleted = false;

    /**
     * The columns that have been modified in current object.
     * Tracking modified columns allows us to only update modified columns.
     * @var array
     */
    protected $modifiedColumns = array();

    /**
     * The (virtual) columns that are added at runtime
     * The formatters can add supplementary columns based on a resultset
     * @var array
     */
    protected $virtualColumns = array();

    /**
     * The value for the id_composition field.
     *
     * @var        int
     */
    protected $id_composition;

    /**
     * The value for the nom field.
     *
     * @var        string
     */
    protected $nom;

    /**
     * @var        ObjectCollection|ChildCourrier[] Collection to store aggregation of ChildCourrier objects.
     */
    protected $collCourriers;
    protected $collCourriersPartial;

    /**
     * @var        ObjectCollection|ChildCompositionsBloc[] Collection to store aggregation of ChildCompositionsBloc objects.
     */
    protected $collCompositionsBlocs;
    protected $collCompositionsBlocsPartial;

    /**
     * @var        ObjectCollection|ChildBloc[] Cross Collection to store aggregation of ChildBloc objects.
     */
    protected $collBlocs;

    /**
     * @var bool
     */
    protected $collBlocsPartial;

    /**
     * Flag to prevent endless save loop, if this object is referenced
     * by another object which falls in this transaction.
     *
     * @var boolean
     */
    protected $alreadyInSave = false;

    /**
     * An array of objects scheduled for deletion.
     * @var ObjectCollection|ChildBloc[]
     */
    protected $blocsScheduledForDeletion = null;

    /**
     * An array of objects scheduled for deletion.
     * @var ObjectCollection|ChildCourrier[]
     */
    protected $courriersScheduledForDeletion = null;

    /**
     * An array of objects scheduled for deletion.
     * @var ObjectCollection|ChildCompositionsBloc[]
     */
    protected $compositionsBlocsScheduledForDeletion = null;

    /**
     * Initializes internal state of Base\Composition object.
     */
    public function __construct()
    {
    }

    /**
     * Returns whether the object has been modified.
     *
     * @return boolean True if the object has been modified.
     */
    public function isModified()
    {
        return !!$this->modifiedColumns;
    }

    /**
     * Has specified column been modified?
     *
     * @param  string  $col column fully qualified name (TableMap::TYPE_COLNAME), e.g. Book::AUTHOR_ID
     * @return boolean True if $col has been modified.
     */
    public function isColumnModified($col)
    {
        return $this->modifiedColumns && isset($this->modifiedColumns[$col]);
    }

    /**
     * Get the columns that have been modified in this object.
     * @return array A unique list of the modified column names for this object.
     */
    public function getModifiedColumns()
    {
        return $this->modifiedColumns ? array_keys($this->modifiedColumns) : [];
    }

    /**
     * Returns whether the object has ever been saved.  This will
     * be false, if the object was retrieved from storage or was created
     * and then saved.
     *
     * @return boolean true, if the object has never been persisted.
     */
    public function isNew()
    {
        return $this->new;
    }

    /**
     * Setter for the isNew attribute.  This method will be called
     * by Propel-generated children and objects.
     *
     * @param boolean $b the state of the object.
     */
    public function setNew($b)
    {
        $this->new = (boolean) $b;
    }

    /**
     * Whether this object has been deleted.
     * @return boolean The deleted state of this object.
     */
    public function isDeleted()
    {
        return $this->deleted;
    }

    /**
     * Specify whether this object has been deleted.
     * @param  boolean $b The deleted state of this object.
     * @return void
     */
    public function setDeleted($b)
    {
        $this->deleted = (boolean) $b;
    }

    /**
     * Sets the modified state for the object to be false.
     * @param  string $col If supplied, only the specified column is reset.
     * @return void
     */
    public function resetModified($col = null)
    {
        if (null !== $col) {
            if (isset($this->modifiedColumns[$col])) {
                unset($this->modifiedColumns[$col]);
            }
        } else {
            $this->modifiedColumns = array();
        }
    }

    /**
     * Compares this with another <code>Composition</code> instance.  If
     * <code>obj</code> is an instance of <code>Composition</code>, delegates to
     * <code>equals(Composition)</code>.  Otherwise, returns <code>false</code>.
     *
     * @param  mixed   $obj The object to compare to.
     * @return boolean Whether equal to the object specified.
     */
    public function equals($obj)
    {
        if (!$obj instanceof static) {
            return false;
        }

        if ($this === $obj) {
            return true;
        }

        if (null === $this->getPrimaryKey() || null === $obj->getPrimaryKey()) {
            return false;
        }

        return $this->getPrimaryKey() === $obj->getPrimaryKey();
    }

    /**
     * Get the associative array of the virtual columns in this object
     *
     * @return array
     */
    public function getVirtualColumns()
    {
        return $this->virtualColumns;
    }

    /**
     * Checks the existence of a virtual column in this object
     *
     * @param  string  $name The virtual column name
     * @return boolean
     */
    public function hasVirtualColumn($name)
    {
        return array_key_exists($name, $this->virtualColumns);
    }

    /**
     * Get the value of a virtual column in this object
     *
     * @param  string $name The virtual column name
     * @return mixed
     *
     * @throws PropelException
     */
    public function getVirtualColumn($name)
    {
        if (!$this->hasVirtualColumn($name)) {
            throw new PropelException(sprintf('Cannot get value of inexistent virtual column %s.', $name));
        }

        return $this->virtualColumns[$name];
    }

    /**
     * Set the value of a virtual column in this object
     *
     * @param string $name  The virtual column name
     * @param mixed  $value The value to give to the virtual column
     *
     * @return $this|Composition The current object, for fluid interface
     */
    public function setVirtualColumn($name, $value)
    {
        $this->virtualColumns[$name] = $value;

        return $this;
    }

    /**
     * Logs a message using Propel::log().
     *
     * @param  string  $msg
     * @param  int     $priority One of the Propel::LOG_* logging levels
     * @return boolean
     */
    protected function log($msg, $priority = Propel::LOG_INFO)
    {
        return Propel::log(get_class($this) . ': ' . $msg, $priority);
    }

    /**
     * Export the current object properties to a string, using a given parser format
     * <code>
     * $book = BookQuery::create()->findPk(9012);
     * echo $book->exportTo('JSON');
     *  => {"Id":9012,"Title":"Don Juan","ISBN":"0140422161","Price":12.99,"PublisherId":1234,"AuthorId":5678}');
     * </code>
     *
     * @param  mixed   $parser                 A AbstractParser instance, or a format name ('XML', 'YAML', 'JSON', 'CSV')
     * @param  boolean $includeLazyLoadColumns (optional) Whether to include lazy load(ed) columns. Defaults to TRUE.
     * @return string  The exported data
     */
    public function exportTo($parser, $includeLazyLoadColumns = true)
    {
        if (!$parser instanceof AbstractParser) {
            $parser = AbstractParser::getParser($parser);
        }

        return $parser->fromArray($this->toArray(TableMap::TYPE_PHPNAME, $includeLazyLoadColumns, array(), true));
    }

    /**
     * Clean up internal collections prior to serializing
     * Avoids recursive loops that turn into segmentation faults when serializing
     */
    public function __sleep()
    {
        $this->clearAllReferences();

        $cls = new \ReflectionClass($this);
        $propertyNames = [];
        $serializableProperties = array_diff($cls->getProperties(), $cls->getProperties(\ReflectionProperty::IS_STATIC));

        foreach($serializableProperties as $property) {
            $propertyNames[] = $property->getName();
        }

        return $propertyNames;
    }

    /**
     * Get the [id_composition] column value.
     *
     * @return int
     */
    public function getIdComposition()
    {
        return $this->id_composition;
    }

    /**
     * Get the [nom] column value.
     *
     * @return string
     */
    public function getNom()
    {
        return $this->nom;
    }

    /**
     * Set the value of [id_composition] column.
     *
     * @param int $v new value
     * @return $this|\Composition The current object (for fluent API support)
     */
    public function setIdComposition($v)
    {
        if ($v !== null) {
            $v = (int) $v;
        }

        if ($this->id_composition !== $v) {
            $this->id_composition = $v;
            $this->modifiedColumns[CompositionTableMap::COL_ID_COMPOSITION] = true;
        }

        return $this;
    } // setIdComposition()

    /**
     * Set the value of [nom] column.
     *
     * @param string $v new value
     * @return $this|\Composition The current object (for fluent API support)
     */
    public function setNom($v)
    {
        if ($v !== null) {
            $v = (string) $v;
        }

        if ($this->nom !== $v) {
            $this->nom = $v;
            $this->modifiedColumns[CompositionTableMap::COL_NOM] = true;
        }

        return $this;
    } // setNom()

    /**
     * Indicates whether the columns in this object are only set to default values.
     *
     * This method can be used in conjunction with isModified() to indicate whether an object is both
     * modified _and_ has some values set which are non-default.
     *
     * @return boolean Whether the columns in this object are only been set with default values.
     */
    public function hasOnlyDefaultValues()
    {
        // otherwise, everything was equal, so return TRUE
        return true;
    } // hasOnlyDefaultValues()

    /**
     * Hydrates (populates) the object variables with values from the database resultset.
     *
     * An offset (0-based "start column") is specified so that objects can be hydrated
     * with a subset of the columns in the resultset rows.  This is needed, for example,
     * for results of JOIN queries where the resultset row includes columns from two or
     * more tables.
     *
     * @param array   $row       The row returned by DataFetcher->fetch().
     * @param int     $startcol  0-based offset column which indicates which restultset column to start with.
     * @param boolean $rehydrate Whether this object is being re-hydrated from the database.
     * @param string  $indexType The index type of $row. Mostly DataFetcher->getIndexType().
                                  One of the class type constants TableMap::TYPE_PHPNAME, TableMap::TYPE_CAMELNAME
     *                            TableMap::TYPE_COLNAME, TableMap::TYPE_FIELDNAME, TableMap::TYPE_NUM.
     *
     * @return int             next starting column
     * @throws PropelException - Any caught Exception will be rewrapped as a PropelException.
     */
    public function hydrate($row, $startcol = 0, $rehydrate = false, $indexType = TableMap::TYPE_NUM)
    {
        try {

            $col = $row[TableMap::TYPE_NUM == $indexType ? 0 + $startcol : CompositionTableMap::translateFieldName('IdComposition', TableMap::TYPE_PHPNAME, $indexType)];
            $this->id_composition = (null !== $col) ? (int) $col : null;

            $col = $row[TableMap::TYPE_NUM == $indexType ? 1 + $startcol : CompositionTableMap::translateFieldName('Nom', TableMap::TYPE_PHPNAME, $indexType)];
            $this->nom = (null !== $col) ? (string) $col : null;
            $this->resetModified();

            $this->setNew(false);

            if ($rehydrate) {
                $this->ensureConsistency();
            }

            return $startcol + 2; // 2 = CompositionTableMap::NUM_HYDRATE_COLUMNS.

        } catch (Exception $e) {
            throw new PropelException(sprintf('Error populating %s object', '\\Composition'), 0, $e);
        }
    }

    /**
     * Checks and repairs the internal consistency of the object.
     *
     * This method is executed after an already-instantiated object is re-hydrated
     * from the database.  It exists to check any foreign keys to make sure that
     * the objects related to the current object are correct based on foreign key.
     *
     * You can override this method in the stub class, but you should always invoke
     * the base method from the overridden method (i.e. parent::ensureConsistency()),
     * in case your model changes.
     *
     * @throws PropelException
     */
    public function ensureConsistency()
    {
    } // ensureConsistency

    /**
     * Reloads this object from datastore based on primary key and (optionally) resets all associated objects.
     *
     * This will only work if the object has been saved and has a valid primary key set.
     *
     * @param      boolean $deep (optional) Whether to also de-associated any related objects.
     * @param      ConnectionInterface $con (optional) The ConnectionInterface connection to use.
     * @return void
     * @throws PropelException - if this object is deleted, unsaved or doesn't have pk match in db
     */
    public function reload($deep = false, ConnectionInterface $con = null)
    {
        if ($this->isDeleted()) {
            throw new PropelException("Cannot reload a deleted object.");
        }

        if ($this->isNew()) {
            throw new PropelException("Cannot reload an unsaved object.");
        }

        if ($con === null) {
            $con = Propel::getServiceContainer()->getReadConnection(CompositionTableMap::DATABASE_NAME);
        }

        // We don't need to alter the object instance pool; we're just modifying this instance
        // already in the pool.

        $dataFetcher = ChildCompositionQuery::create(null, $this->buildPkeyCriteria())->setFormatter(ModelCriteria::FORMAT_STATEMENT)->find($con);
        $row = $dataFetcher->fetch();
        $dataFetcher->close();
        if (!$row) {
            throw new PropelException('Cannot find matching row in the database to reload object values.');
        }
        $this->hydrate($row, 0, true, $dataFetcher->getIndexType()); // rehydrate

        if ($deep) {  // also de-associate any related objects?

            $this->collCourriers = null;

            $this->collCompositionsBlocs = null;

            $this->collBlocs = null;
        } // if (deep)
    }

    /**
     * Removes this object from datastore and sets delete attribute.
     *
     * @param      ConnectionInterface $con
     * @return void
     * @throws PropelException
     * @see Composition::setDeleted()
     * @see Composition::isDeleted()
     */
    public function delete(ConnectionInterface $con = null)
    {
        if ($this->isDeleted()) {
            throw new PropelException("This object has already been deleted.");
        }

        if ($con === null) {
            $con = Propel::getServiceContainer()->getWriteConnection(CompositionTableMap::DATABASE_NAME);
        }

        $con->transaction(function () use ($con) {
            $deleteQuery = ChildCompositionQuery::create()
                ->filterByPrimaryKey($this->getPrimaryKey());
            $ret = $this->preDelete($con);
            if ($ret) {
                $deleteQuery->delete($con);
                $this->postDelete($con);
                $this->setDeleted(true);
            }
        });
    }

    /**
     * Persists this object to the database.
     *
     * If the object is new, it inserts it; otherwise an update is performed.
     * All modified related objects will also be persisted in the doSave()
     * method.  This method wraps all precipitate database operations in a
     * single transaction.
     *
     * @param      ConnectionInterface $con
     * @return int             The number of rows affected by this insert/update and any referring fk objects' save() operations.
     * @throws PropelException
     * @see doSave()
     */
    public function save(ConnectionInterface $con = null)
    {
        if ($this->isDeleted()) {
            throw new PropelException("You cannot save an object that has been deleted.");
        }

        if ($this->alreadyInSave) {
            return 0;
        }

        if ($con === null) {
            $con = Propel::getServiceContainer()->getWriteConnection(CompositionTableMap::DATABASE_NAME);
        }

        return $con->transaction(function () use ($con) {
            $ret = $this->preSave($con);
            $isInsert = $this->isNew();
            if ($isInsert) {
                $ret = $ret && $this->preInsert($con);
            } else {
                $ret = $ret && $this->preUpdate($con);
            }
            if ($ret) {
                $affectedRows = $this->doSave($con);
                if ($isInsert) {
                    $this->postInsert($con);
                } else {
                    $this->postUpdate($con);
                }
                $this->postSave($con);
                CompositionTableMap::addInstanceToPool($this);
            } else {
                $affectedRows = 0;
            }

            return $affectedRows;
        });
    }

    /**
     * Performs the work of inserting or updating the row in the database.
     *
     * If the object is new, it inserts it; otherwise an update is performed.
     * All related objects are also updated in this method.
     *
     * @param      ConnectionInterface $con
     * @return int             The number of rows affected by this insert/update and any referring fk objects' save() operations.
     * @throws PropelException
     * @see save()
     */
    protected function doSave(ConnectionInterface $con)
    {
        $affectedRows = 0; // initialize var to track total num of affected rows
        if (!$this->alreadyInSave) {
            $this->alreadyInSave = true;

            if ($this->isNew() || $this->isModified()) {
                // persist changes
                if ($this->isNew()) {
                    $this->doInsert($con);
                    $affectedRows += 1;
                } else {
                    $affectedRows += $this->doUpdate($con);
                }
                $this->resetModified();
            }

            if ($this->blocsScheduledForDeletion !== null) {
                if (!$this->blocsScheduledForDeletion->isEmpty()) {
                    $pks = array();
                    foreach ($this->blocsScheduledForDeletion as $entry) {
                        $entryPk = [];

                        $entryPk[0] = $this->getIdComposition();
                        $entryPk[1] = $entry->getIdBloc();
                        $pks[] = $entryPk;
                    }

                    \CompositionsBlocQuery::create()
                        ->filterByPrimaryKeys($pks)
                        ->delete($con);

                    $this->blocsScheduledForDeletion = null;
                }

            }

            if ($this->collBlocs) {
                foreach ($this->collBlocs as $bloc) {
                    if (!$bloc->isDeleted() && ($bloc->isNew() || $bloc->isModified())) {
                        $bloc->save($con);
                    }
                }
            }


            if ($this->courriersScheduledForDeletion !== null) {
                if (!$this->courriersScheduledForDeletion->isEmpty()) {
                    \CourrierQuery::create()
                        ->filterByPrimaryKeys($this->courriersScheduledForDeletion->getPrimaryKeys(false))
                        ->delete($con);
                    $this->courriersScheduledForDeletion = null;
                }
            }

            if ($this->collCourriers !== null) {
                foreach ($this->collCourriers as $referrerFK) {
                    if (!$referrerFK->isDeleted() && ($referrerFK->isNew() || $referrerFK->isModified())) {
                        $affectedRows += $referrerFK->save($con);
                    }
                }
            }

            if ($this->compositionsBlocsScheduledForDeletion !== null) {
                if (!$this->compositionsBlocsScheduledForDeletion->isEmpty()) {
                    \CompositionsBlocQuery::create()
                        ->filterByPrimaryKeys($this->compositionsBlocsScheduledForDeletion->getPrimaryKeys(false))
                        ->delete($con);
                    $this->compositionsBlocsScheduledForDeletion = null;
                }
            }

            if ($this->collCompositionsBlocs !== null) {
                foreach ($this->collCompositionsBlocs as $referrerFK) {
                    if (!$referrerFK->isDeleted() && ($referrerFK->isNew() || $referrerFK->isModified())) {
                        $affectedRows += $referrerFK->save($con);
                    }
                }
            }

            $this->alreadyInSave = false;

        }

        return $affectedRows;
    } // doSave()

    /**
     * Insert the row in the database.
     *
     * @param      ConnectionInterface $con
     *
     * @throws PropelException
     * @see doSave()
     */
    protected function doInsert(ConnectionInterface $con)
    {
        $modifiedColumns = array();
        $index = 0;

        $this->modifiedColumns[CompositionTableMap::COL_ID_COMPOSITION] = true;
        if (null !== $this->id_composition) {
            throw new PropelException('Cannot insert a value for auto-increment primary key (' . CompositionTableMap::COL_ID_COMPOSITION . ')');
        }

         // check the columns in natural order for more readable SQL queries
        if ($this->isColumnModified(CompositionTableMap::COL_ID_COMPOSITION)) {
            $modifiedColumns[':p' . $index++]  = '`id_composition`';
        }
        if ($this->isColumnModified(CompositionTableMap::COL_NOM)) {
            $modifiedColumns[':p' . $index++]  = '`nom`';
        }

        $sql = sprintf(
            'INSERT INTO `com_compositions` (%s) VALUES (%s)',
            implode(', ', $modifiedColumns),
            implode(', ', array_keys($modifiedColumns))
        );

        try {
            $stmt = $con->prepare($sql);
            foreach ($modifiedColumns as $identifier => $columnName) {
                switch ($columnName) {
                    case '`id_composition`':
                        $stmt->bindValue($identifier, $this->id_composition, PDO::PARAM_INT);
                        break;
                    case '`nom`':
                        $stmt->bindValue($identifier, $this->nom, PDO::PARAM_STR);
                        break;
                }
            }
            $stmt->execute();
        } catch (Exception $e) {
            Propel::log($e->getMessage(), Propel::LOG_ERR);
            throw new PropelException(sprintf('Unable to execute INSERT statement [%s]', $sql), 0, $e);
        }

        try {
            $pk = $con->lastInsertId();
        } catch (Exception $e) {
            throw new PropelException('Unable to get autoincrement id.', 0, $e);
        }
        $this->setIdComposition($pk);

        $this->setNew(false);
    }

    /**
     * Update the row in the database.
     *
     * @param      ConnectionInterface $con
     *
     * @return Integer Number of updated rows
     * @see doSave()
     */
    protected function doUpdate(ConnectionInterface $con)
    {
        $selectCriteria = $this->buildPkeyCriteria();
        $valuesCriteria = $this->buildCriteria();

        return $selectCriteria->doUpdate($valuesCriteria, $con);
    }

    /**
     * Retrieves a field from the object by name passed in as a string.
     *
     * @param      string $name name
     * @param      string $type The type of fieldname the $name is of:
     *                     one of the class type constants TableMap::TYPE_PHPNAME, TableMap::TYPE_CAMELNAME
     *                     TableMap::TYPE_COLNAME, TableMap::TYPE_FIELDNAME, TableMap::TYPE_NUM.
     *                     Defaults to TableMap::TYPE_FIELDNAME.
     * @return mixed Value of field.
     */
    public function getByName($name, $type = TableMap::TYPE_FIELDNAME)
    {
        $pos = CompositionTableMap::translateFieldName($name, $type, TableMap::TYPE_NUM);
        $field = $this->getByPosition($pos);

        return $field;
    }

    /**
     * Retrieves a field from the object by Position as specified in the xml schema.
     * Zero-based.
     *
     * @param      int $pos position in xml schema
     * @return mixed Value of field at $pos
     */
    public function getByPosition($pos)
    {
        switch ($pos) {
            case 0:
                return $this->getIdComposition();
                break;
            case 1:
                return $this->getNom();
                break;
            default:
                return null;
                break;
        } // switch()
    }

    /**
     * Exports the object as an array.
     *
     * You can specify the key type of the array by passing one of the class
     * type constants.
     *
     * @param     string  $keyType (optional) One of the class type constants TableMap::TYPE_PHPNAME, TableMap::TYPE_CAMELNAME,
     *                    TableMap::TYPE_COLNAME, TableMap::TYPE_FIELDNAME, TableMap::TYPE_NUM.
     *                    Defaults to TableMap::TYPE_FIELDNAME.
     * @param     boolean $includeLazyLoadColumns (optional) Whether to include lazy loaded columns. Defaults to TRUE.
     * @param     array $alreadyDumpedObjects List of objects to skip to avoid recursion
     * @param     boolean $includeForeignObjects (optional) Whether to include hydrated related objects. Default to FALSE.
     *
     * @return array an associative array containing the field names (as keys) and field values
     */
    public function toArray($keyType = TableMap::TYPE_FIELDNAME, $includeLazyLoadColumns = true, $alreadyDumpedObjects = array(), $includeForeignObjects = false)
    {

        if (isset($alreadyDumpedObjects['Composition'][$this->hashCode()])) {
            return '*RECURSION*';
        }
        $alreadyDumpedObjects['Composition'][$this->hashCode()] = true;
        $keys = CompositionTableMap::getFieldNames($keyType);
        $result = array(
            $keys[0] => $this->getIdComposition(),
            $keys[1] => $this->getNom(),
        );
        $virtualColumns = $this->virtualColumns;
        foreach ($virtualColumns as $key => $virtualColumn) {
            $result[$key] = $virtualColumn;
        }

        if ($includeForeignObjects) {
            if (null !== $this->collCourriers) {

                switch ($keyType) {
                    case TableMap::TYPE_CAMELNAME:
                        $key = 'courriers';
                        break;
                    case TableMap::TYPE_FIELDNAME:
                        $key = 'com_courrierss';
                        break;
                    default:
                        $key = 'Courriers';
                }

                $result[$key] = $this->collCourriers->toArray(null, false, $keyType, $includeLazyLoadColumns, $alreadyDumpedObjects);
            }
            if (null !== $this->collCompositionsBlocs) {

                switch ($keyType) {
                    case TableMap::TYPE_CAMELNAME:
                        $key = 'compositionsBlocs';
                        break;
                    case TableMap::TYPE_FIELDNAME:
                        $key = 'com_compositions_blocss';
                        break;
                    default:
                        $key = 'CompositionsBlocs';
                }

                $result[$key] = $this->collCompositionsBlocs->toArray(null, false, $keyType, $includeLazyLoadColumns, $alreadyDumpedObjects);
            }
        }

        return $result;
    }

    /**
     * Sets a field from the object by name passed in as a string.
     *
     * @param  string $name
     * @param  mixed  $value field value
     * @param  string $type The type of fieldname the $name is of:
     *                one of the class type constants TableMap::TYPE_PHPNAME, TableMap::TYPE_CAMELNAME
     *                TableMap::TYPE_COLNAME, TableMap::TYPE_FIELDNAME, TableMap::TYPE_NUM.
     *                Defaults to TableMap::TYPE_FIELDNAME.
     * @return $this|\Composition
     */
    public function setByName($name, $value, $type = TableMap::TYPE_FIELDNAME)
    {
        $pos = CompositionTableMap::translateFieldName($name, $type, TableMap::TYPE_NUM);

        return $this->setByPosition($pos, $value);
    }

    /**
     * Sets a field from the object by Position as specified in the xml schema.
     * Zero-based.
     *
     * @param  int $pos position in xml schema
     * @param  mixed $value field value
     * @return $this|\Composition
     */
    public function setByPosition($pos, $value)
    {
        switch ($pos) {
            case 0:
                $this->setIdComposition($value);
                break;
            case 1:
                $this->setNom($value);
                break;
        } // switch()

        return $this;
    }

    /**
     * Populates the object using an array.
     *
     * This is particularly useful when populating an object from one of the
     * request arrays (e.g. $_POST).  This method goes through the column
     * names, checking to see whether a matching key exists in populated
     * array. If so the setByName() method is called for that column.
     *
     * You can specify the key type of the array by additionally passing one
     * of the class type constants TableMap::TYPE_PHPNAME, TableMap::TYPE_CAMELNAME,
     * TableMap::TYPE_COLNAME, TableMap::TYPE_FIELDNAME, TableMap::TYPE_NUM.
     * The default key type is the column's TableMap::TYPE_FIELDNAME.
     *
     * @param      array  $arr     An array to populate the object from.
     * @param      string $keyType The type of keys the array uses.
     * @return void
     */
    public function fromArray($arr, $keyType = TableMap::TYPE_FIELDNAME)
    {
        $keys = CompositionTableMap::getFieldNames($keyType);

        if (array_key_exists($keys[0], $arr)) {
            $this->setIdComposition($arr[$keys[0]]);
        }
        if (array_key_exists($keys[1], $arr)) {
            $this->setNom($arr[$keys[1]]);
        }
    }

     /**
     * Populate the current object from a string, using a given parser format
     * <code>
     * $book = new Book();
     * $book->importFrom('JSON', '{"Id":9012,"Title":"Don Juan","ISBN":"0140422161","Price":12.99,"PublisherId":1234,"AuthorId":5678}');
     * </code>
     *
     * You can specify the key type of the array by additionally passing one
     * of the class type constants TableMap::TYPE_PHPNAME, TableMap::TYPE_CAMELNAME,
     * TableMap::TYPE_COLNAME, TableMap::TYPE_FIELDNAME, TableMap::TYPE_NUM.
     * The default key type is the column's TableMap::TYPE_FIELDNAME.
     *
     * @param mixed $parser A AbstractParser instance,
     *                       or a format name ('XML', 'YAML', 'JSON', 'CSV')
     * @param string $data The source data to import from
     * @param string $keyType The type of keys the array uses.
     *
     * @return $this|\Composition The current object, for fluid interface
     */
    public function importFrom($parser, $data, $keyType = TableMap::TYPE_FIELDNAME)
    {
        if (!$parser instanceof AbstractParser) {
            $parser = AbstractParser::getParser($parser);
        }

        $this->fromArray($parser->toArray($data), $keyType);

        return $this;
    }

    /**
     * Build a Criteria object containing the values of all modified columns in this object.
     *
     * @return Criteria The Criteria object containing all modified values.
     */
    public function buildCriteria()
    {
        $criteria = new Criteria(CompositionTableMap::DATABASE_NAME);

        if ($this->isColumnModified(CompositionTableMap::COL_ID_COMPOSITION)) {
            $criteria->add(CompositionTableMap::COL_ID_COMPOSITION, $this->id_composition);
        }
        if ($this->isColumnModified(CompositionTableMap::COL_NOM)) {
            $criteria->add(CompositionTableMap::COL_NOM, $this->nom);
        }

        return $criteria;
    }

    /**
     * Builds a Criteria object containing the primary key for this object.
     *
     * Unlike buildCriteria() this method includes the primary key values regardless
     * of whether or not they have been modified.
     *
     * @throws LogicException if no primary key is defined
     *
     * @return Criteria The Criteria object containing value(s) for primary key(s).
     */
    public function buildPkeyCriteria()
    {
        $criteria = ChildCompositionQuery::create();
        $criteria->add(CompositionTableMap::COL_ID_COMPOSITION, $this->id_composition);

        return $criteria;
    }

    /**
     * If the primary key is not null, return the hashcode of the
     * primary key. Otherwise, return the hash code of the object.
     *
     * @return int Hashcode
     */
    public function hashCode()
    {
        $validPk = null !== $this->getIdComposition();

        $validPrimaryKeyFKs = 0;
        $primaryKeyFKs = [];

        if ($validPk) {
            return crc32(json_encode($this->getPrimaryKey(), JSON_UNESCAPED_UNICODE));
        } elseif ($validPrimaryKeyFKs) {
            return crc32(json_encode($primaryKeyFKs, JSON_UNESCAPED_UNICODE));
        }

        return spl_object_hash($this);
    }

    /**
     * Returns the primary key for this object (row).
     * @return int
     */
    public function getPrimaryKey()
    {
        return $this->getIdComposition();
    }

    /**
     * Generic method to set the primary key (id_composition column).
     *
     * @param       int $key Primary key.
     * @return void
     */
    public function setPrimaryKey($key)
    {
        $this->setIdComposition($key);
    }

    /**
     * Returns true if the primary key for this object is null.
     * @return boolean
     */
    public function isPrimaryKeyNull()
    {
        return null === $this->getIdComposition();
    }

    /**
     * Sets contents of passed object to values from current object.
     *
     * If desired, this method can also make copies of all associated (fkey referrers)
     * objects.
     *
     * @param      object $copyObj An object of \Composition (or compatible) type.
     * @param      boolean $deepCopy Whether to also copy all rows that refer (by fkey) to the current row.
     * @param      boolean $makeNew Whether to reset autoincrement PKs and make the object new.
     * @throws PropelException
     */
    public function copyInto($copyObj, $deepCopy = false, $makeNew = true)
    {
        $copyObj->setNom($this->getNom());

        if ($deepCopy) {
            // important: temporarily setNew(false) because this affects the behavior of
            // the getter/setter methods for fkey referrer objects.
            $copyObj->setNew(false);

            foreach ($this->getCourriers() as $relObj) {
                if ($relObj !== $this) {  // ensure that we don't try to copy a reference to ourselves
                    $copyObj->addCourrier($relObj->copy($deepCopy));
                }
            }

            foreach ($this->getCompositionsBlocs() as $relObj) {
                if ($relObj !== $this) {  // ensure that we don't try to copy a reference to ourselves
                    $copyObj->addCompositionsBloc($relObj->copy($deepCopy));
                }
            }

        } // if ($deepCopy)

        if ($makeNew) {
            $copyObj->setNew(true);
            $copyObj->setIdComposition(NULL); // this is a auto-increment column, so set to default value
        }
    }

    /**
     * Makes a copy of this object that will be inserted as a new row in table when saved.
     * It creates a new object filling in the simple attributes, but skipping any primary
     * keys that are defined for the table.
     *
     * If desired, this method can also make copies of all associated (fkey referrers)
     * objects.
     *
     * @param  boolean $deepCopy Whether to also copy all rows that refer (by fkey) to the current row.
     * @return \Composition Clone of current object.
     * @throws PropelException
     */
    public function copy($deepCopy = false)
    {
        // we use get_class(), because this might be a subclass
        $clazz = get_class($this);
        $copyObj = new $clazz();
        $this->copyInto($copyObj, $deepCopy);

        return $copyObj;
    }


    /**
     * Initializes a collection based on the name of a relation.
     * Avoids crafting an 'init[$relationName]s' method name
     * that wouldn't work when StandardEnglishPluralizer is used.
     *
     * @param      string $relationName The name of the relation to initialize
     * @return void
     */
    public function initRelation($relationName)
    {
        if ('Courrier' == $relationName) {
            $this->initCourriers();
            return;
        }
        if ('CompositionsBloc' == $relationName) {
            $this->initCompositionsBlocs();
            return;
        }
    }

    /**
     * Clears out the collCourriers collection
     *
     * This does not modify the database; however, it will remove any associated objects, causing
     * them to be refetched by subsequent calls to accessor method.
     *
     * @return void
     * @see        addCourriers()
     */
    public function clearCourriers()
    {
        $this->collCourriers = null; // important to set this to NULL since that means it is uninitialized
    }

    /**
     * Reset is the collCourriers collection loaded partially.
     */
    public function resetPartialCourriers($v = true)
    {
        $this->collCourriersPartial = $v;
    }

    /**
     * Initializes the collCourriers collection.
     *
     * By default this just sets the collCourriers collection to an empty array (like clearcollCourriers());
     * however, you may wish to override this method in your stub class to provide setting appropriate
     * to your application -- for example, setting the initial array to the values stored in database.
     *
     * @param      boolean $overrideExisting If set to true, the method call initializes
     *                                        the collection even if it is not empty
     *
     * @return void
     */
    public function initCourriers($overrideExisting = true)
    {
        if (null !== $this->collCourriers && !$overrideExisting) {
            return;
        }

        $collectionClassName = CourrierTableMap::getTableMap()->getCollectionClassName();

        $this->collCourriers = new $collectionClassName;
        $this->collCourriers->setModel('\Courrier');
    }

    /**
     * Gets an array of ChildCourrier objects which contain a foreign key that references this object.
     *
     * If the $criteria is not null, it is used to always fetch the results from the database.
     * Otherwise the results are fetched from the database the first time, then cached.
     * Next time the same method is called without $criteria, the cached collection is returned.
     * If this ChildComposition is new, it will return
     * an empty collection or the current collection; the criteria is ignored on a new object.
     *
     * @param      Criteria $criteria optional Criteria object to narrow the query
     * @param      ConnectionInterface $con optional connection object
     * @return ObjectCollection|ChildCourrier[] List of ChildCourrier objects
     * @throws PropelException
     */
    public function getCourriers(Criteria $criteria = null, ConnectionInterface $con = null)
    {
        $partial = $this->collCourriersPartial && !$this->isNew();
        if (null === $this->collCourriers || null !== $criteria  || $partial) {
            if ($this->isNew() && null === $this->collCourriers) {
                // return empty collection
                $this->initCourriers();
            } else {
                $collCourriers = ChildCourrierQuery::create(null, $criteria)
                    ->filterByComposition($this)
                    ->find($con);

                if (null !== $criteria) {
                    if (false !== $this->collCourriersPartial && count($collCourriers)) {
                        $this->initCourriers(false);

                        foreach ($collCourriers as $obj) {
                            if (false == $this->collCourriers->contains($obj)) {
                                $this->collCourriers->append($obj);
                            }
                        }

                        $this->collCourriersPartial = true;
                    }

                    return $collCourriers;
                }

                if ($partial && $this->collCourriers) {
                    foreach ($this->collCourriers as $obj) {
                        if ($obj->isNew()) {
                            $collCourriers[] = $obj;
                        }
                    }
                }

                $this->collCourriers = $collCourriers;
                $this->collCourriersPartial = false;
            }
        }

        return $this->collCourriers;
    }

    /**
     * Sets a collection of ChildCourrier objects related by a one-to-many relationship
     * to the current object.
     * It will also schedule objects for deletion based on a diff between old objects (aka persisted)
     * and new objects from the given Propel collection.
     *
     * @param      Collection $courriers A Propel collection.
     * @param      ConnectionInterface $con Optional connection object
     * @return $this|ChildComposition The current object (for fluent API support)
     */
    public function setCourriers(Collection $courriers, ConnectionInterface $con = null)
    {
        /** @var ChildCourrier[] $courriersToDelete */
        $courriersToDelete = $this->getCourriers(new Criteria(), $con)->diff($courriers);


        $this->courriersScheduledForDeletion = $courriersToDelete;

        foreach ($courriersToDelete as $courrierRemoved) {
            $courrierRemoved->setComposition(null);
        }

        $this->collCourriers = null;
        foreach ($courriers as $courrier) {
            $this->addCourrier($courrier);
        }

        $this->collCourriers = $courriers;
        $this->collCourriersPartial = false;

        return $this;
    }

    /**
     * Returns the number of related Courrier objects.
     *
     * @param      Criteria $criteria
     * @param      boolean $distinct
     * @param      ConnectionInterface $con
     * @return int             Count of related Courrier objects.
     * @throws PropelException
     */
    public function countCourriers(Criteria $criteria = null, $distinct = false, ConnectionInterface $con = null)
    {
        $partial = $this->collCourriersPartial && !$this->isNew();
        if (null === $this->collCourriers || null !== $criteria || $partial) {
            if ($this->isNew() && null === $this->collCourriers) {
                return 0;
            }

            if ($partial && !$criteria) {
                return count($this->getCourriers());
            }

            $query = ChildCourrierQuery::create(null, $criteria);
            if ($distinct) {
                $query->distinct();
            }

            return $query
                ->filterByComposition($this)
                ->count($con);
        }

        return count($this->collCourriers);
    }

    /**
     * Method called to associate a ChildCourrier object to this object
     * through the ChildCourrier foreign key attribute.
     *
     * @param  ChildCourrier $l ChildCourrier
     * @return $this|\Composition The current object (for fluent API support)
     */
    public function addCourrier(ChildCourrier $l)
    {
        if ($this->collCourriers === null) {
            $this->initCourriers();
            $this->collCourriersPartial = true;
        }

        if (!$this->collCourriers->contains($l)) {
            $this->doAddCourrier($l);

            if ($this->courriersScheduledForDeletion and $this->courriersScheduledForDeletion->contains($l)) {
                $this->courriersScheduledForDeletion->remove($this->courriersScheduledForDeletion->search($l));
            }
        }

        return $this;
    }

    /**
     * @param ChildCourrier $courrier The ChildCourrier object to add.
     */
    protected function doAddCourrier(ChildCourrier $courrier)
    {
        $this->collCourriers[]= $courrier;
        $courrier->setComposition($this);
    }

    /**
     * @param  ChildCourrier $courrier The ChildCourrier object to remove.
     * @return $this|ChildComposition The current object (for fluent API support)
     */
    public function removeCourrier(ChildCourrier $courrier)
    {
        if ($this->getCourriers()->contains($courrier)) {
            $pos = $this->collCourriers->search($courrier);
            $this->collCourriers->remove($pos);
            if (null === $this->courriersScheduledForDeletion) {
                $this->courriersScheduledForDeletion = clone $this->collCourriers;
                $this->courriersScheduledForDeletion->clear();
            }
            $this->courriersScheduledForDeletion[]= $courrier;
            $courrier->setComposition(null);
        }

        return $this;
    }


    /**
     * If this collection has already been initialized with
     * an identical criteria, it returns the collection.
     * Otherwise if this Composition is new, it will return
     * an empty collection; or if this Composition has previously
     * been saved, it will retrieve related Courriers from storage.
     *
     * This method is protected by default in order to keep the public
     * api reasonable.  You can provide public methods for those you
     * actually need in Composition.
     *
     * @param      Criteria $criteria optional Criteria object to narrow the query
     * @param      ConnectionInterface $con optional connection object
     * @param      string $joinBehavior optional join type to use (defaults to Criteria::LEFT_JOIN)
     * @return ObjectCollection|ChildCourrier[] List of ChildCourrier objects
     */
    public function getCourriersJoinEntite(Criteria $criteria = null, ConnectionInterface $con = null, $joinBehavior = Criteria::LEFT_JOIN)
    {
        $query = ChildCourrierQuery::create(null, $criteria);
        $query->joinWith('Entite', $joinBehavior);

        return $this->getCourriers($query, $con);
    }

    /**
     * Clears out the collCompositionsBlocs collection
     *
     * This does not modify the database; however, it will remove any associated objects, causing
     * them to be refetched by subsequent calls to accessor method.
     *
     * @return void
     * @see        addCompositionsBlocs()
     */
    public function clearCompositionsBlocs()
    {
        $this->collCompositionsBlocs = null; // important to set this to NULL since that means it is uninitialized
    }

    /**
     * Reset is the collCompositionsBlocs collection loaded partially.
     */
    public function resetPartialCompositionsBlocs($v = true)
    {
        $this->collCompositionsBlocsPartial = $v;
    }

    /**
     * Initializes the collCompositionsBlocs collection.
     *
     * By default this just sets the collCompositionsBlocs collection to an empty array (like clearcollCompositionsBlocs());
     * however, you may wish to override this method in your stub class to provide setting appropriate
     * to your application -- for example, setting the initial array to the values stored in database.
     *
     * @param      boolean $overrideExisting If set to true, the method call initializes
     *                                        the collection even if it is not empty
     *
     * @return void
     */
    public function initCompositionsBlocs($overrideExisting = true)
    {
        if (null !== $this->collCompositionsBlocs && !$overrideExisting) {
            return;
        }

        $collectionClassName = CompositionsBlocTableMap::getTableMap()->getCollectionClassName();

        $this->collCompositionsBlocs = new $collectionClassName;
        $this->collCompositionsBlocs->setModel('\CompositionsBloc');
    }

    /**
     * Gets an array of ChildCompositionsBloc objects which contain a foreign key that references this object.
     *
     * If the $criteria is not null, it is used to always fetch the results from the database.
     * Otherwise the results are fetched from the database the first time, then cached.
     * Next time the same method is called without $criteria, the cached collection is returned.
     * If this ChildComposition is new, it will return
     * an empty collection or the current collection; the criteria is ignored on a new object.
     *
     * @param      Criteria $criteria optional Criteria object to narrow the query
     * @param      ConnectionInterface $con optional connection object
     * @return ObjectCollection|ChildCompositionsBloc[] List of ChildCompositionsBloc objects
     * @throws PropelException
     */
    public function getCompositionsBlocs(Criteria $criteria = null, ConnectionInterface $con = null)
    {
        $partial = $this->collCompositionsBlocsPartial && !$this->isNew();
        if (null === $this->collCompositionsBlocs || null !== $criteria  || $partial) {
            if ($this->isNew() && null === $this->collCompositionsBlocs) {
                // return empty collection
                $this->initCompositionsBlocs();
            } else {
                $collCompositionsBlocs = ChildCompositionsBlocQuery::create(null, $criteria)
                    ->filterByComposition($this)
                    ->find($con);

                if (null !== $criteria) {
                    if (false !== $this->collCompositionsBlocsPartial && count($collCompositionsBlocs)) {
                        $this->initCompositionsBlocs(false);

                        foreach ($collCompositionsBlocs as $obj) {
                            if (false == $this->collCompositionsBlocs->contains($obj)) {
                                $this->collCompositionsBlocs->append($obj);
                            }
                        }

                        $this->collCompositionsBlocsPartial = true;
                    }

                    return $collCompositionsBlocs;
                }

                if ($partial && $this->collCompositionsBlocs) {
                    foreach ($this->collCompositionsBlocs as $obj) {
                        if ($obj->isNew()) {
                            $collCompositionsBlocs[] = $obj;
                        }
                    }
                }

                $this->collCompositionsBlocs = $collCompositionsBlocs;
                $this->collCompositionsBlocsPartial = false;
            }
        }

        return $this->collCompositionsBlocs;
    }

    /**
     * Sets a collection of ChildCompositionsBloc objects related by a one-to-many relationship
     * to the current object.
     * It will also schedule objects for deletion based on a diff between old objects (aka persisted)
     * and new objects from the given Propel collection.
     *
     * @param      Collection $compositionsBlocs A Propel collection.
     * @param      ConnectionInterface $con Optional connection object
     * @return $this|ChildComposition The current object (for fluent API support)
     */
    public function setCompositionsBlocs(Collection $compositionsBlocs, ConnectionInterface $con = null)
    {
        /** @var ChildCompositionsBloc[] $compositionsBlocsToDelete */
        $compositionsBlocsToDelete = $this->getCompositionsBlocs(new Criteria(), $con)->diff($compositionsBlocs);


        //since at least one column in the foreign key is at the same time a PK
        //we can not just set a PK to NULL in the lines below. We have to store
        //a backup of all values, so we are able to manipulate these items based on the onDelete value later.
        $this->compositionsBlocsScheduledForDeletion = clone $compositionsBlocsToDelete;

        foreach ($compositionsBlocsToDelete as $compositionsBlocRemoved) {
            $compositionsBlocRemoved->setComposition(null);
        }

        $this->collCompositionsBlocs = null;
        foreach ($compositionsBlocs as $compositionsBloc) {
            $this->addCompositionsBloc($compositionsBloc);
        }

        $this->collCompositionsBlocs = $compositionsBlocs;
        $this->collCompositionsBlocsPartial = false;

        return $this;
    }

    /**
     * Returns the number of related CompositionsBloc objects.
     *
     * @param      Criteria $criteria
     * @param      boolean $distinct
     * @param      ConnectionInterface $con
     * @return int             Count of related CompositionsBloc objects.
     * @throws PropelException
     */
    public function countCompositionsBlocs(Criteria $criteria = null, $distinct = false, ConnectionInterface $con = null)
    {
        $partial = $this->collCompositionsBlocsPartial && !$this->isNew();
        if (null === $this->collCompositionsBlocs || null !== $criteria || $partial) {
            if ($this->isNew() && null === $this->collCompositionsBlocs) {
                return 0;
            }

            if ($partial && !$criteria) {
                return count($this->getCompositionsBlocs());
            }

            $query = ChildCompositionsBlocQuery::create(null, $criteria);
            if ($distinct) {
                $query->distinct();
            }

            return $query
                ->filterByComposition($this)
                ->count($con);
        }

        return count($this->collCompositionsBlocs);
    }

    /**
     * Method called to associate a ChildCompositionsBloc object to this object
     * through the ChildCompositionsBloc foreign key attribute.
     *
     * @param  ChildCompositionsBloc $l ChildCompositionsBloc
     * @return $this|\Composition The current object (for fluent API support)
     */
    public function addCompositionsBloc(ChildCompositionsBloc $l)
    {
        if ($this->collCompositionsBlocs === null) {
            $this->initCompositionsBlocs();
            $this->collCompositionsBlocsPartial = true;
        }

        if (!$this->collCompositionsBlocs->contains($l)) {
            $this->doAddCompositionsBloc($l);

            if ($this->compositionsBlocsScheduledForDeletion and $this->compositionsBlocsScheduledForDeletion->contains($l)) {
                $this->compositionsBlocsScheduledForDeletion->remove($this->compositionsBlocsScheduledForDeletion->search($l));
            }
        }

        return $this;
    }

    /**
     * @param ChildCompositionsBloc $compositionsBloc The ChildCompositionsBloc object to add.
     */
    protected function doAddCompositionsBloc(ChildCompositionsBloc $compositionsBloc)
    {
        $this->collCompositionsBlocs[]= $compositionsBloc;
        $compositionsBloc->setComposition($this);
    }

    /**
     * @param  ChildCompositionsBloc $compositionsBloc The ChildCompositionsBloc object to remove.
     * @return $this|ChildComposition The current object (for fluent API support)
     */
    public function removeCompositionsBloc(ChildCompositionsBloc $compositionsBloc)
    {
        if ($this->getCompositionsBlocs()->contains($compositionsBloc)) {
            $pos = $this->collCompositionsBlocs->search($compositionsBloc);
            $this->collCompositionsBlocs->remove($pos);
            if (null === $this->compositionsBlocsScheduledForDeletion) {
                $this->compositionsBlocsScheduledForDeletion = clone $this->collCompositionsBlocs;
                $this->compositionsBlocsScheduledForDeletion->clear();
            }
            $this->compositionsBlocsScheduledForDeletion[]= clone $compositionsBloc;
            $compositionsBloc->setComposition(null);
        }

        return $this;
    }


    /**
     * If this collection has already been initialized with
     * an identical criteria, it returns the collection.
     * Otherwise if this Composition is new, it will return
     * an empty collection; or if this Composition has previously
     * been saved, it will retrieve related CompositionsBlocs from storage.
     *
     * This method is protected by default in order to keep the public
     * api reasonable.  You can provide public methods for those you
     * actually need in Composition.
     *
     * @param      Criteria $criteria optional Criteria object to narrow the query
     * @param      ConnectionInterface $con optional connection object
     * @param      string $joinBehavior optional join type to use (defaults to Criteria::LEFT_JOIN)
     * @return ObjectCollection|ChildCompositionsBloc[] List of ChildCompositionsBloc objects
     */
    public function getCompositionsBlocsJoinBloc(Criteria $criteria = null, ConnectionInterface $con = null, $joinBehavior = Criteria::LEFT_JOIN)
    {
        $query = ChildCompositionsBlocQuery::create(null, $criteria);
        $query->joinWith('Bloc', $joinBehavior);

        return $this->getCompositionsBlocs($query, $con);
    }

    /**
     * Clears out the collBlocs collection
     *
     * This does not modify the database; however, it will remove any associated objects, causing
     * them to be refetched by subsequent calls to accessor method.
     *
     * @return void
     * @see        addBlocs()
     */
    public function clearBlocs()
    {
        $this->collBlocs = null; // important to set this to NULL since that means it is uninitialized
    }

    /**
     * Initializes the collBlocs crossRef collection.
     *
     * By default this just sets the collBlocs collection to an empty collection (like clearBlocs());
     * however, you may wish to override this method in your stub class to provide setting appropriate
     * to your application -- for example, setting the initial array to the values stored in database.
     *
     * @return void
     */
    public function initBlocs()
    {
        $collectionClassName = CompositionsBlocTableMap::getTableMap()->getCollectionClassName();

        $this->collBlocs = new $collectionClassName;
        $this->collBlocsPartial = true;
        $this->collBlocs->setModel('\Bloc');
    }

    /**
     * Checks if the collBlocs collection is loaded.
     *
     * @return bool
     */
    public function isBlocsLoaded()
    {
        return null !== $this->collBlocs;
    }

    /**
     * Gets a collection of ChildBloc objects related by a many-to-many relationship
     * to the current object by way of the com_compositions_blocs cross-reference table.
     *
     * If the $criteria is not null, it is used to always fetch the results from the database.
     * Otherwise the results are fetched from the database the first time, then cached.
     * Next time the same method is called without $criteria, the cached collection is returned.
     * If this ChildComposition is new, it will return
     * an empty collection or the current collection; the criteria is ignored on a new object.
     *
     * @param      Criteria $criteria Optional query object to filter the query
     * @param      ConnectionInterface $con Optional connection object
     *
     * @return ObjectCollection|ChildBloc[] List of ChildBloc objects
     */
    public function getBlocs(Criteria $criteria = null, ConnectionInterface $con = null)
    {
        $partial = $this->collBlocsPartial && !$this->isNew();
        if (null === $this->collBlocs || null !== $criteria || $partial) {
            if ($this->isNew()) {
                // return empty collection
                if (null === $this->collBlocs) {
                    $this->initBlocs();
                }
            } else {

                $query = ChildBlocQuery::create(null, $criteria)
                    ->filterByComposition($this);
                $collBlocs = $query->find($con);
                if (null !== $criteria) {
                    return $collBlocs;
                }

                if ($partial && $this->collBlocs) {
                    //make sure that already added objects gets added to the list of the database.
                    foreach ($this->collBlocs as $obj) {
                        if (!$collBlocs->contains($obj)) {
                            $collBlocs[] = $obj;
                        }
                    }
                }

                $this->collBlocs = $collBlocs;
                $this->collBlocsPartial = false;
            }
        }

        return $this->collBlocs;
    }

    /**
     * Sets a collection of Bloc objects related by a many-to-many relationship
     * to the current object by way of the com_compositions_blocs cross-reference table.
     * It will also schedule objects for deletion based on a diff between old objects (aka persisted)
     * and new objects from the given Propel collection.
     *
     * @param  Collection $blocs A Propel collection.
     * @param  ConnectionInterface $con Optional connection object
     * @return $this|ChildComposition The current object (for fluent API support)
     */
    public function setBlocs(Collection $blocs, ConnectionInterface $con = null)
    {
        $this->clearBlocs();
        $currentBlocs = $this->getBlocs();

        $blocsScheduledForDeletion = $currentBlocs->diff($blocs);

        foreach ($blocsScheduledForDeletion as $toDelete) {
            $this->removeBloc($toDelete);
        }

        foreach ($blocs as $bloc) {
            if (!$currentBlocs->contains($bloc)) {
                $this->doAddBloc($bloc);
            }
        }

        $this->collBlocsPartial = false;
        $this->collBlocs = $blocs;

        return $this;
    }

    /**
     * Gets the number of Bloc objects related by a many-to-many relationship
     * to the current object by way of the com_compositions_blocs cross-reference table.
     *
     * @param      Criteria $criteria Optional query object to filter the query
     * @param      boolean $distinct Set to true to force count distinct
     * @param      ConnectionInterface $con Optional connection object
     *
     * @return int the number of related Bloc objects
     */
    public function countBlocs(Criteria $criteria = null, $distinct = false, ConnectionInterface $con = null)
    {
        $partial = $this->collBlocsPartial && !$this->isNew();
        if (null === $this->collBlocs || null !== $criteria || $partial) {
            if ($this->isNew() && null === $this->collBlocs) {
                return 0;
            } else {

                if ($partial && !$criteria) {
                    return count($this->getBlocs());
                }

                $query = ChildBlocQuery::create(null, $criteria);
                if ($distinct) {
                    $query->distinct();
                }

                return $query
                    ->filterByComposition($this)
                    ->count($con);
            }
        } else {
            return count($this->collBlocs);
        }
    }

    /**
     * Associate a ChildBloc to this object
     * through the com_compositions_blocs cross reference table.
     *
     * @param ChildBloc $bloc
     * @return ChildComposition The current object (for fluent API support)
     */
    public function addBloc(ChildBloc $bloc)
    {
        if ($this->collBlocs === null) {
            $this->initBlocs();
        }

        if (!$this->getBlocs()->contains($bloc)) {
            // only add it if the **same** object is not already associated
            $this->collBlocs->push($bloc);
            $this->doAddBloc($bloc);
        }

        return $this;
    }

    /**
     *
     * @param ChildBloc $bloc
     */
    protected function doAddBloc(ChildBloc $bloc)
    {
        $compositionsBloc = new ChildCompositionsBloc();

        $compositionsBloc->setBloc($bloc);

        $compositionsBloc->setComposition($this);

        $this->addCompositionsBloc($compositionsBloc);

        // set the back reference to this object directly as using provided method either results
        // in endless loop or in multiple relations
        if (!$bloc->isCompositionsLoaded()) {
            $bloc->initCompositions();
            $bloc->getCompositions()->push($this);
        } elseif (!$bloc->getCompositions()->contains($this)) {
            $bloc->getCompositions()->push($this);
        }

    }

    /**
     * Remove bloc of this object
     * through the com_compositions_blocs cross reference table.
     *
     * @param ChildBloc $bloc
     * @return ChildComposition The current object (for fluent API support)
     */
    public function removeBloc(ChildBloc $bloc)
    {
        if ($this->getBlocs()->contains($bloc)) {
            $compositionsBloc = new ChildCompositionsBloc();
            $compositionsBloc->setBloc($bloc);
            if ($bloc->isCompositionsLoaded()) {
                //remove the back reference if available
                $bloc->getCompositions()->removeObject($this);
            }

            $compositionsBloc->setComposition($this);
            $this->removeCompositionsBloc(clone $compositionsBloc);
            $compositionsBloc->clear();

            $this->collBlocs->remove($this->collBlocs->search($bloc));

            if (null === $this->blocsScheduledForDeletion) {
                $this->blocsScheduledForDeletion = clone $this->collBlocs;
                $this->blocsScheduledForDeletion->clear();
            }

            $this->blocsScheduledForDeletion->push($bloc);
        }


        return $this;
    }

    /**
     * Clears the current object, sets all attributes to their default values and removes
     * outgoing references as well as back-references (from other objects to this one. Results probably in a database
     * change of those foreign objects when you call `save` there).
     */
    public function clear()
    {
        $this->id_composition = null;
        $this->nom = null;
        $this->alreadyInSave = false;
        $this->clearAllReferences();
        $this->resetModified();
        $this->setNew(true);
        $this->setDeleted(false);
    }

    /**
     * Resets all references and back-references to other model objects or collections of model objects.
     *
     * This method is used to reset all php object references (not the actual reference in the database).
     * Necessary for object serialisation.
     *
     * @param      boolean $deep Whether to also clear the references on all referrer objects.
     */
    public function clearAllReferences($deep = false)
    {
        if ($deep) {
            if ($this->collCourriers) {
                foreach ($this->collCourriers as $o) {
                    $o->clearAllReferences($deep);
                }
            }
            if ($this->collCompositionsBlocs) {
                foreach ($this->collCompositionsBlocs as $o) {
                    $o->clearAllReferences($deep);
                }
            }
            if ($this->collBlocs) {
                foreach ($this->collBlocs as $o) {
                    $o->clearAllReferences($deep);
                }
            }
        } // if ($deep)

        $this->collCourriers = null;
        $this->collCompositionsBlocs = null;
        $this->collBlocs = null;
    }

    /**
     * Return the string representation of this object
     *
     * @return string
     */
    public function __toString()
    {
        return (string) $this->exportTo(CompositionTableMap::DEFAULT_STRING_FORMAT);
    }

    /**
     * Code to be run before persisting the object
     * @param  ConnectionInterface $con
     * @return boolean
     */
    public function preSave(ConnectionInterface $con = null)
    {
        if (is_callable('parent::preSave')) {
            return parent::preSave($con);
        }
        return true;
    }

    /**
     * Code to be run after persisting the object
     * @param ConnectionInterface $con
     */
    public function postSave(ConnectionInterface $con = null)
    {
        if (is_callable('parent::postSave')) {
            parent::postSave($con);
        }
    }

    /**
     * Code to be run before inserting to database
     * @param  ConnectionInterface $con
     * @return boolean
     */
    public function preInsert(ConnectionInterface $con = null)
    {
        if (is_callable('parent::preInsert')) {
            return parent::preInsert($con);
        }
        return true;
    }

    /**
     * Code to be run after inserting to database
     * @param ConnectionInterface $con
     */
    public function postInsert(ConnectionInterface $con = null)
    {
        if (is_callable('parent::postInsert')) {
            parent::postInsert($con);
        }
    }

    /**
     * Code to be run before updating the object in database
     * @param  ConnectionInterface $con
     * @return boolean
     */
    public function preUpdate(ConnectionInterface $con = null)
    {
        if (is_callable('parent::preUpdate')) {
            return parent::preUpdate($con);
        }
        return true;
    }

    /**
     * Code to be run after updating the object in database
     * @param ConnectionInterface $con
     */
    public function postUpdate(ConnectionInterface $con = null)
    {
        if (is_callable('parent::postUpdate')) {
            parent::postUpdate($con);
        }
    }

    /**
     * Code to be run before deleting the object in database
     * @param  ConnectionInterface $con
     * @return boolean
     */
    public function preDelete(ConnectionInterface $con = null)
    {
        if (is_callable('parent::preDelete')) {
            return parent::preDelete($con);
        }
        return true;
    }

    /**
     * Code to be run after deleting the object in database
     * @param ConnectionInterface $con
     */
    public function postDelete(ConnectionInterface $con = null)
    {
        if (is_callable('parent::postDelete')) {
            parent::postDelete($con);
        }
    }


    /**
     * Derived method to catches calls to undefined methods.
     *
     * Provides magic import/export method support (fromXML()/toXML(), fromYAML()/toYAML(), etc.).
     * Allows to define default __call() behavior if you overwrite __call()
     *
     * @param string $name
     * @param mixed  $params
     *
     * @return array|string
     */
    public function __call($name, $params)
    {
        if (0 === strpos($name, 'get')) {
            $virtualColumn = substr($name, 3);
            if ($this->hasVirtualColumn($virtualColumn)) {
                return $this->getVirtualColumn($virtualColumn);
            }

            $virtualColumn = lcfirst($virtualColumn);
            if ($this->hasVirtualColumn($virtualColumn)) {
                return $this->getVirtualColumn($virtualColumn);
            }
        }

        if (0 === strpos($name, 'from')) {
            $format = substr($name, 4);

            return $this->importFrom($format, reset($params));
        }

        if (0 === strpos($name, 'to')) {
            $format = substr($name, 2);
            $includeLazyLoadColumns = isset($params[0]) ? $params[0] : true;

            return $this->exportTo($format, $includeLazyLoadColumns);
        }

        throw new BadMethodCallException(sprintf('Call to undefined method: %s.', $name));
    }

}
