<?php

namespace Base;

use \AssoServicepaiementsArchive as ChildAssoServicepaiementsArchive;
use \Servicepaiement as ChildServicepaiement;
use \ServicepaiementQuery as ChildServicepaiementQuery;
use \Exception;
use \PDO;
use Map\ServicepaiementTableMap;
use Propel\Runtime\Propel;
use Propel\Runtime\ActiveQuery\Criteria;
use Propel\Runtime\ActiveQuery\ModelCriteria;
use Propel\Runtime\ActiveQuery\ModelJoin;
use Propel\Runtime\Collection\ObjectCollection;
use Propel\Runtime\Connection\ConnectionInterface;
use Propel\Runtime\Exception\PropelException;

/**
 * Base class that represents a query for the 'asso_servicepaiements' table.
 *
 *
 *
 * @method     ChildServicepaiementQuery orderByIdServicepaiement($order = Criteria::ASC) Order by the id_servicepaiement column
 * @method     ChildServicepaiementQuery orderByIdServicerendu($order = Criteria::ASC) Order by the id_servicerendu column
 * @method     ChildServicepaiementQuery orderByIdPaiement($order = Criteria::ASC) Order by the id_paiement column
 * @method     ChildServicepaiementQuery orderByMontant($order = Criteria::ASC) Order by the montant column
 * @method     ChildServicepaiementQuery orderByCreatedAt($order = Criteria::ASC) Order by the created_at column
 * @method     ChildServicepaiementQuery orderByUpdatedAt($order = Criteria::ASC) Order by the updated_at column
 *
 * @method     ChildServicepaiementQuery groupByIdServicepaiement() Group by the id_servicepaiement column
 * @method     ChildServicepaiementQuery groupByIdServicerendu() Group by the id_servicerendu column
 * @method     ChildServicepaiementQuery groupByIdPaiement() Group by the id_paiement column
 * @method     ChildServicepaiementQuery groupByMontant() Group by the montant column
 * @method     ChildServicepaiementQuery groupByCreatedAt() Group by the created_at column
 * @method     ChildServicepaiementQuery groupByUpdatedAt() Group by the updated_at column
 *
 * @method     ChildServicepaiementQuery leftJoin($relation) Adds a LEFT JOIN clause to the query
 * @method     ChildServicepaiementQuery rightJoin($relation) Adds a RIGHT JOIN clause to the query
 * @method     ChildServicepaiementQuery innerJoin($relation) Adds a INNER JOIN clause to the query
 *
 * @method     ChildServicepaiementQuery leftJoinWith($relation) Adds a LEFT JOIN clause and with to the query
 * @method     ChildServicepaiementQuery rightJoinWith($relation) Adds a RIGHT JOIN clause and with to the query
 * @method     ChildServicepaiementQuery innerJoinWith($relation) Adds a INNER JOIN clause and with to the query
 *
 * @method     ChildServicepaiementQuery leftJoinServicerendu($relationAlias = null) Adds a LEFT JOIN clause to the query using the Servicerendu relation
 * @method     ChildServicepaiementQuery rightJoinServicerendu($relationAlias = null) Adds a RIGHT JOIN clause to the query using the Servicerendu relation
 * @method     ChildServicepaiementQuery innerJoinServicerendu($relationAlias = null) Adds a INNER JOIN clause to the query using the Servicerendu relation
 *
 * @method     ChildServicepaiementQuery joinWithServicerendu($joinType = Criteria::INNER_JOIN) Adds a join clause and with to the query using the Servicerendu relation
 *
 * @method     ChildServicepaiementQuery leftJoinWithServicerendu() Adds a LEFT JOIN clause and with to the query using the Servicerendu relation
 * @method     ChildServicepaiementQuery rightJoinWithServicerendu() Adds a RIGHT JOIN clause and with to the query using the Servicerendu relation
 * @method     ChildServicepaiementQuery innerJoinWithServicerendu() Adds a INNER JOIN clause and with to the query using the Servicerendu relation
 *
 * @method     ChildServicepaiementQuery leftJoinPaiement($relationAlias = null) Adds a LEFT JOIN clause to the query using the Paiement relation
 * @method     ChildServicepaiementQuery rightJoinPaiement($relationAlias = null) Adds a RIGHT JOIN clause to the query using the Paiement relation
 * @method     ChildServicepaiementQuery innerJoinPaiement($relationAlias = null) Adds a INNER JOIN clause to the query using the Paiement relation
 *
 * @method     ChildServicepaiementQuery joinWithPaiement($joinType = Criteria::INNER_JOIN) Adds a join clause and with to the query using the Paiement relation
 *
 * @method     ChildServicepaiementQuery leftJoinWithPaiement() Adds a LEFT JOIN clause and with to the query using the Paiement relation
 * @method     ChildServicepaiementQuery rightJoinWithPaiement() Adds a RIGHT JOIN clause and with to the query using the Paiement relation
 * @method     ChildServicepaiementQuery innerJoinWithPaiement() Adds a INNER JOIN clause and with to the query using the Paiement relation
 *
 * @method     \ServicerenduQuery|\PaiementQuery endUse() Finalizes a secondary criteria and merges it with its primary Criteria
 *
 * @method     ChildServicepaiement findOne(ConnectionInterface $con = null) Return the first ChildServicepaiement matching the query
 * @method     ChildServicepaiement findOneOrCreate(ConnectionInterface $con = null) Return the first ChildServicepaiement matching the query, or a new ChildServicepaiement object populated from the query conditions when no match is found
 *
 * @method     ChildServicepaiement findOneByIdServicepaiement(string $id_servicepaiement) Return the first ChildServicepaiement filtered by the id_servicepaiement column
 * @method     ChildServicepaiement findOneByIdServicerendu(string $id_servicerendu) Return the first ChildServicepaiement filtered by the id_servicerendu column
 * @method     ChildServicepaiement findOneByIdPaiement(string $id_paiement) Return the first ChildServicepaiement filtered by the id_paiement column
 * @method     ChildServicepaiement findOneByMontant(double $montant) Return the first ChildServicepaiement filtered by the montant column
 * @method     ChildServicepaiement findOneByCreatedAt(string $created_at) Return the first ChildServicepaiement filtered by the created_at column
 * @method     ChildServicepaiement findOneByUpdatedAt(string $updated_at) Return the first ChildServicepaiement filtered by the updated_at column *

 * @method     ChildServicepaiement requirePk($key, ConnectionInterface $con = null) Return the ChildServicepaiement by primary key and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildServicepaiement requireOne(ConnectionInterface $con = null) Return the first ChildServicepaiement matching the query and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 *
 * @method     ChildServicepaiement requireOneByIdServicepaiement(string $id_servicepaiement) Return the first ChildServicepaiement filtered by the id_servicepaiement column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildServicepaiement requireOneByIdServicerendu(string $id_servicerendu) Return the first ChildServicepaiement filtered by the id_servicerendu column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildServicepaiement requireOneByIdPaiement(string $id_paiement) Return the first ChildServicepaiement filtered by the id_paiement column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildServicepaiement requireOneByMontant(double $montant) Return the first ChildServicepaiement filtered by the montant column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildServicepaiement requireOneByCreatedAt(string $created_at) Return the first ChildServicepaiement filtered by the created_at column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildServicepaiement requireOneByUpdatedAt(string $updated_at) Return the first ChildServicepaiement filtered by the updated_at column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 *
 * @method     ChildServicepaiement[]|ObjectCollection find(ConnectionInterface $con = null) Return ChildServicepaiement objects based on current ModelCriteria
 * @method     ChildServicepaiement[]|ObjectCollection findByIdServicepaiement(string $id_servicepaiement) Return ChildServicepaiement objects filtered by the id_servicepaiement column
 * @method     ChildServicepaiement[]|ObjectCollection findByIdServicerendu(string $id_servicerendu) Return ChildServicepaiement objects filtered by the id_servicerendu column
 * @method     ChildServicepaiement[]|ObjectCollection findByIdPaiement(string $id_paiement) Return ChildServicepaiement objects filtered by the id_paiement column
 * @method     ChildServicepaiement[]|ObjectCollection findByMontant(double $montant) Return ChildServicepaiement objects filtered by the montant column
 * @method     ChildServicepaiement[]|ObjectCollection findByCreatedAt(string $created_at) Return ChildServicepaiement objects filtered by the created_at column
 * @method     ChildServicepaiement[]|ObjectCollection findByUpdatedAt(string $updated_at) Return ChildServicepaiement objects filtered by the updated_at column
 * @method     ChildServicepaiement[]|\Propel\Runtime\Util\PropelModelPager paginate($page = 1, $maxPerPage = 10, ConnectionInterface $con = null) Issue a SELECT query based on the current ModelCriteria and uses a page and a maximum number of results per page to compute an offset and a limit
 *
 */
abstract class ServicepaiementQuery extends ModelCriteria
{

    // archivable behavior
    protected $archiveOnDelete = true;
protected $entityNotFoundExceptionClass = '\\Propel\\Runtime\\Exception\\EntityNotFoundException';

    /**
     * Initializes internal state of \Base\ServicepaiementQuery object.
     *
     * @param     string $dbName The database name
     * @param     string $modelName The phpName of a model, e.g. 'Book'
     * @param     string $modelAlias The alias for the model in this query, e.g. 'b'
     */
    public function __construct($dbName = 'simplasso', $modelName = '\\Servicepaiement', $modelAlias = null)
    {
        parent::__construct($dbName, $modelName, $modelAlias);
    }

    /**
     * Returns a new ChildServicepaiementQuery object.
     *
     * @param     string $modelAlias The alias of a model in the query
     * @param     Criteria $criteria Optional Criteria to build the query from
     *
     * @return ChildServicepaiementQuery
     */
    public static function create($modelAlias = null, Criteria $criteria = null)
    {
        if ($criteria instanceof ChildServicepaiementQuery) {
            return $criteria;
        }
        $query = new ChildServicepaiementQuery();
        if (null !== $modelAlias) {
            $query->setModelAlias($modelAlias);
        }
        if ($criteria instanceof Criteria) {
            $query->mergeWith($criteria);
        }

        return $query;
    }

    /**
     * Find object by primary key.
     * Propel uses the instance pool to skip the database if the object exists.
     * Go fast if the query is untouched.
     *
     * <code>
     * $obj  = $c->findPk(12, $con);
     * </code>
     *
     * @param mixed $key Primary key to use for the query
     * @param ConnectionInterface $con an optional connection object
     *
     * @return ChildServicepaiement|array|mixed the result, formatted by the current formatter
     */
    public function findPk($key, ConnectionInterface $con = null)
    {
        if ($key === null) {
            return null;
        }

        if ($con === null) {
            $con = Propel::getServiceContainer()->getReadConnection(ServicepaiementTableMap::DATABASE_NAME);
        }

        $this->basePreSelect($con);

        if (
            $this->formatter || $this->modelAlias || $this->with || $this->select
            || $this->selectColumns || $this->asColumns || $this->selectModifiers
            || $this->map || $this->having || $this->joins
        ) {
            return $this->findPkComplex($key, $con);
        }

        if ((null !== ($obj = ServicepaiementTableMap::getInstanceFromPool(null === $key || is_scalar($key) || is_callable([$key, '__toString']) ? (string) $key : $key)))) {
            // the object is already in the instance pool
            return $obj;
        }

        return $this->findPkSimple($key, $con);
    }

    /**
     * Find object by primary key using raw SQL to go fast.
     * Bypass doSelect() and the object formatter by using generated code.
     *
     * @param     mixed $key Primary key to use for the query
     * @param     ConnectionInterface $con A connection object
     *
     * @throws \Propel\Runtime\Exception\PropelException
     *
     * @return ChildServicepaiement A model object, or null if the key is not found
     */
    protected function findPkSimple($key, ConnectionInterface $con)
    {
        $sql = 'SELECT `id_servicepaiement`, `id_servicerendu`, `id_paiement`, `montant`, `created_at`, `updated_at` FROM `asso_servicepaiements` WHERE `id_servicepaiement` = :p0';
        try {
            $stmt = $con->prepare($sql);
            $stmt->bindValue(':p0', $key, PDO::PARAM_INT);
            $stmt->execute();
        } catch (Exception $e) {
            Propel::log($e->getMessage(), Propel::LOG_ERR);
            throw new PropelException(sprintf('Unable to execute SELECT statement [%s]', $sql), 0, $e);
        }
        $obj = null;
        if ($row = $stmt->fetch(\PDO::FETCH_NUM)) {
            /** @var ChildServicepaiement $obj */
            $obj = new ChildServicepaiement();
            $obj->hydrate($row);
            ServicepaiementTableMap::addInstanceToPool($obj, null === $key || is_scalar($key) || is_callable([$key, '__toString']) ? (string) $key : $key);
        }
        $stmt->closeCursor();

        return $obj;
    }

    /**
     * Find object by primary key.
     *
     * @param     mixed $key Primary key to use for the query
     * @param     ConnectionInterface $con A connection object
     *
     * @return ChildServicepaiement|array|mixed the result, formatted by the current formatter
     */
    protected function findPkComplex($key, ConnectionInterface $con)
    {
        // As the query uses a PK condition, no limit(1) is necessary.
        $criteria = $this->isKeepQuery() ? clone $this : $this;
        $dataFetcher = $criteria
            ->filterByPrimaryKey($key)
            ->doSelect($con);

        return $criteria->getFormatter()->init($criteria)->formatOne($dataFetcher);
    }

    /**
     * Find objects by primary key
     * <code>
     * $objs = $c->findPks(array(12, 56, 832), $con);
     * </code>
     * @param     array $keys Primary keys to use for the query
     * @param     ConnectionInterface $con an optional connection object
     *
     * @return ObjectCollection|array|mixed the list of results, formatted by the current formatter
     */
    public function findPks($keys, ConnectionInterface $con = null)
    {
        if (null === $con) {
            $con = Propel::getServiceContainer()->getReadConnection($this->getDbName());
        }
        $this->basePreSelect($con);
        $criteria = $this->isKeepQuery() ? clone $this : $this;
        $dataFetcher = $criteria
            ->filterByPrimaryKeys($keys)
            ->doSelect($con);

        return $criteria->getFormatter()->init($criteria)->format($dataFetcher);
    }

    /**
     * Filter the query by primary key
     *
     * @param     mixed $key Primary key to use for the query
     *
     * @return $this|ChildServicepaiementQuery The current query, for fluid interface
     */
    public function filterByPrimaryKey($key)
    {

        return $this->addUsingAlias(ServicepaiementTableMap::COL_ID_SERVICEPAIEMENT, $key, Criteria::EQUAL);
    }

    /**
     * Filter the query by a list of primary keys
     *
     * @param     array $keys The list of primary key to use for the query
     *
     * @return $this|ChildServicepaiementQuery The current query, for fluid interface
     */
    public function filterByPrimaryKeys($keys)
    {

        return $this->addUsingAlias(ServicepaiementTableMap::COL_ID_SERVICEPAIEMENT, $keys, Criteria::IN);
    }

    /**
     * Filter the query on the id_servicepaiement column
     *
     * Example usage:
     * <code>
     * $query->filterByIdServicepaiement(1234); // WHERE id_servicepaiement = 1234
     * $query->filterByIdServicepaiement(array(12, 34)); // WHERE id_servicepaiement IN (12, 34)
     * $query->filterByIdServicepaiement(array('min' => 12)); // WHERE id_servicepaiement > 12
     * </code>
     *
     * @param     mixed $idServicepaiement The value to use as filter.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildServicepaiementQuery The current query, for fluid interface
     */
    public function filterByIdServicepaiement($idServicepaiement = null, $comparison = null)
    {
        if (is_array($idServicepaiement)) {
            $useMinMax = false;
            if (isset($idServicepaiement['min'])) {
                $this->addUsingAlias(ServicepaiementTableMap::COL_ID_SERVICEPAIEMENT, $idServicepaiement['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($idServicepaiement['max'])) {
                $this->addUsingAlias(ServicepaiementTableMap::COL_ID_SERVICEPAIEMENT, $idServicepaiement['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(ServicepaiementTableMap::COL_ID_SERVICEPAIEMENT, $idServicepaiement, $comparison);
    }

    /**
     * Filter the query on the id_servicerendu column
     *
     * Example usage:
     * <code>
     * $query->filterByIdServicerendu(1234); // WHERE id_servicerendu = 1234
     * $query->filterByIdServicerendu(array(12, 34)); // WHERE id_servicerendu IN (12, 34)
     * $query->filterByIdServicerendu(array('min' => 12)); // WHERE id_servicerendu > 12
     * </code>
     *
     * @see       filterByServicerendu()
     *
     * @param     mixed $idServicerendu The value to use as filter.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildServicepaiementQuery The current query, for fluid interface
     */
    public function filterByIdServicerendu($idServicerendu = null, $comparison = null)
    {
        if (is_array($idServicerendu)) {
            $useMinMax = false;
            if (isset($idServicerendu['min'])) {
                $this->addUsingAlias(ServicepaiementTableMap::COL_ID_SERVICERENDU, $idServicerendu['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($idServicerendu['max'])) {
                $this->addUsingAlias(ServicepaiementTableMap::COL_ID_SERVICERENDU, $idServicerendu['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(ServicepaiementTableMap::COL_ID_SERVICERENDU, $idServicerendu, $comparison);
    }

    /**
     * Filter the query on the id_paiement column
     *
     * Example usage:
     * <code>
     * $query->filterByIdPaiement(1234); // WHERE id_paiement = 1234
     * $query->filterByIdPaiement(array(12, 34)); // WHERE id_paiement IN (12, 34)
     * $query->filterByIdPaiement(array('min' => 12)); // WHERE id_paiement > 12
     * </code>
     *
     * @see       filterByPaiement()
     *
     * @param     mixed $idPaiement The value to use as filter.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildServicepaiementQuery The current query, for fluid interface
     */
    public function filterByIdPaiement($idPaiement = null, $comparison = null)
    {
        if (is_array($idPaiement)) {
            $useMinMax = false;
            if (isset($idPaiement['min'])) {
                $this->addUsingAlias(ServicepaiementTableMap::COL_ID_PAIEMENT, $idPaiement['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($idPaiement['max'])) {
                $this->addUsingAlias(ServicepaiementTableMap::COL_ID_PAIEMENT, $idPaiement['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(ServicepaiementTableMap::COL_ID_PAIEMENT, $idPaiement, $comparison);
    }

    /**
     * Filter the query on the montant column
     *
     * Example usage:
     * <code>
     * $query->filterByMontant(1234); // WHERE montant = 1234
     * $query->filterByMontant(array(12, 34)); // WHERE montant IN (12, 34)
     * $query->filterByMontant(array('min' => 12)); // WHERE montant > 12
     * </code>
     *
     * @param     mixed $montant The value to use as filter.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildServicepaiementQuery The current query, for fluid interface
     */
    public function filterByMontant($montant = null, $comparison = null)
    {
        if (is_array($montant)) {
            $useMinMax = false;
            if (isset($montant['min'])) {
                $this->addUsingAlias(ServicepaiementTableMap::COL_MONTANT, $montant['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($montant['max'])) {
                $this->addUsingAlias(ServicepaiementTableMap::COL_MONTANT, $montant['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(ServicepaiementTableMap::COL_MONTANT, $montant, $comparison);
    }

    /**
     * Filter the query on the created_at column
     *
     * Example usage:
     * <code>
     * $query->filterByCreatedAt('2011-03-14'); // WHERE created_at = '2011-03-14'
     * $query->filterByCreatedAt('now'); // WHERE created_at = '2011-03-14'
     * $query->filterByCreatedAt(array('max' => 'yesterday')); // WHERE created_at > '2011-03-13'
     * </code>
     *
     * @param     mixed $createdAt The value to use as filter.
     *              Values can be integers (unix timestamps), DateTime objects, or strings.
     *              Empty strings are treated as NULL.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildServicepaiementQuery The current query, for fluid interface
     */
    public function filterByCreatedAt($createdAt = null, $comparison = null)
    {
        if (is_array($createdAt)) {
            $useMinMax = false;
            if (isset($createdAt['min'])) {
                $this->addUsingAlias(ServicepaiementTableMap::COL_CREATED_AT, $createdAt['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($createdAt['max'])) {
                $this->addUsingAlias(ServicepaiementTableMap::COL_CREATED_AT, $createdAt['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(ServicepaiementTableMap::COL_CREATED_AT, $createdAt, $comparison);
    }

    /**
     * Filter the query on the updated_at column
     *
     * Example usage:
     * <code>
     * $query->filterByUpdatedAt('2011-03-14'); // WHERE updated_at = '2011-03-14'
     * $query->filterByUpdatedAt('now'); // WHERE updated_at = '2011-03-14'
     * $query->filterByUpdatedAt(array('max' => 'yesterday')); // WHERE updated_at > '2011-03-13'
     * </code>
     *
     * @param     mixed $updatedAt The value to use as filter.
     *              Values can be integers (unix timestamps), DateTime objects, or strings.
     *              Empty strings are treated as NULL.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildServicepaiementQuery The current query, for fluid interface
     */
    public function filterByUpdatedAt($updatedAt = null, $comparison = null)
    {
        if (is_array($updatedAt)) {
            $useMinMax = false;
            if (isset($updatedAt['min'])) {
                $this->addUsingAlias(ServicepaiementTableMap::COL_UPDATED_AT, $updatedAt['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($updatedAt['max'])) {
                $this->addUsingAlias(ServicepaiementTableMap::COL_UPDATED_AT, $updatedAt['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(ServicepaiementTableMap::COL_UPDATED_AT, $updatedAt, $comparison);
    }

    /**
     * Filter the query by a related \Servicerendu object
     *
     * @param \Servicerendu|ObjectCollection $servicerendu The related object(s) to use as filter
     * @param string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @throws \Propel\Runtime\Exception\PropelException
     *
     * @return ChildServicepaiementQuery The current query, for fluid interface
     */
    public function filterByServicerendu($servicerendu, $comparison = null)
    {
        if ($servicerendu instanceof \Servicerendu) {
            return $this
                ->addUsingAlias(ServicepaiementTableMap::COL_ID_SERVICERENDU, $servicerendu->getIdServicerendu(), $comparison);
        } elseif ($servicerendu instanceof ObjectCollection) {
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }

            return $this
                ->addUsingAlias(ServicepaiementTableMap::COL_ID_SERVICERENDU, $servicerendu->toKeyValue('PrimaryKey', 'IdServicerendu'), $comparison);
        } else {
            throw new PropelException('filterByServicerendu() only accepts arguments of type \Servicerendu or Collection');
        }
    }

    /**
     * Adds a JOIN clause to the query using the Servicerendu relation
     *
     * @param     string $relationAlias optional alias for the relation
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return $this|ChildServicepaiementQuery The current query, for fluid interface
     */
    public function joinServicerendu($relationAlias = null, $joinType = Criteria::INNER_JOIN)
    {
        $tableMap = $this->getTableMap();
        $relationMap = $tableMap->getRelation('Servicerendu');

        // create a ModelJoin object for this join
        $join = new ModelJoin();
        $join->setJoinType($joinType);
        $join->setRelationMap($relationMap, $this->useAliasInSQL ? $this->getModelAlias() : null, $relationAlias);
        if ($previousJoin = $this->getPreviousJoin()) {
            $join->setPreviousJoin($previousJoin);
        }

        // add the ModelJoin to the current object
        if ($relationAlias) {
            $this->addAlias($relationAlias, $relationMap->getRightTable()->getName());
            $this->addJoinObject($join, $relationAlias);
        } else {
            $this->addJoinObject($join, 'Servicerendu');
        }

        return $this;
    }

    /**
     * Use the Servicerendu relation Servicerendu object
     *
     * @see useQuery()
     *
     * @param     string $relationAlias optional alias for the relation,
     *                                   to be used as main alias in the secondary query
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return \ServicerenduQuery A secondary query class using the current class as primary query
     */
    public function useServicerenduQuery($relationAlias = null, $joinType = Criteria::INNER_JOIN)
    {
        return $this
            ->joinServicerendu($relationAlias, $joinType)
            ->useQuery($relationAlias ? $relationAlias : 'Servicerendu', '\ServicerenduQuery');
    }

    /**
     * Filter the query by a related \Paiement object
     *
     * @param \Paiement|ObjectCollection $paiement The related object(s) to use as filter
     * @param string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @throws \Propel\Runtime\Exception\PropelException
     *
     * @return ChildServicepaiementQuery The current query, for fluid interface
     */
    public function filterByPaiement($paiement, $comparison = null)
    {
        if ($paiement instanceof \Paiement) {
            return $this
                ->addUsingAlias(ServicepaiementTableMap::COL_ID_PAIEMENT, $paiement->getIdPaiement(), $comparison);
        } elseif ($paiement instanceof ObjectCollection) {
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }

            return $this
                ->addUsingAlias(ServicepaiementTableMap::COL_ID_PAIEMENT, $paiement->toKeyValue('PrimaryKey', 'IdPaiement'), $comparison);
        } else {
            throw new PropelException('filterByPaiement() only accepts arguments of type \Paiement or Collection');
        }
    }

    /**
     * Adds a JOIN clause to the query using the Paiement relation
     *
     * @param     string $relationAlias optional alias for the relation
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return $this|ChildServicepaiementQuery The current query, for fluid interface
     */
    public function joinPaiement($relationAlias = null, $joinType = Criteria::INNER_JOIN)
    {
        $tableMap = $this->getTableMap();
        $relationMap = $tableMap->getRelation('Paiement');

        // create a ModelJoin object for this join
        $join = new ModelJoin();
        $join->setJoinType($joinType);
        $join->setRelationMap($relationMap, $this->useAliasInSQL ? $this->getModelAlias() : null, $relationAlias);
        if ($previousJoin = $this->getPreviousJoin()) {
            $join->setPreviousJoin($previousJoin);
        }

        // add the ModelJoin to the current object
        if ($relationAlias) {
            $this->addAlias($relationAlias, $relationMap->getRightTable()->getName());
            $this->addJoinObject($join, $relationAlias);
        } else {
            $this->addJoinObject($join, 'Paiement');
        }

        return $this;
    }

    /**
     * Use the Paiement relation Paiement object
     *
     * @see useQuery()
     *
     * @param     string $relationAlias optional alias for the relation,
     *                                   to be used as main alias in the secondary query
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return \PaiementQuery A secondary query class using the current class as primary query
     */
    public function usePaiementQuery($relationAlias = null, $joinType = Criteria::INNER_JOIN)
    {
        return $this
            ->joinPaiement($relationAlias, $joinType)
            ->useQuery($relationAlias ? $relationAlias : 'Paiement', '\PaiementQuery');
    }

    /**
     * Exclude object from result
     *
     * @param   ChildServicepaiement $servicepaiement Object to remove from the list of results
     *
     * @return $this|ChildServicepaiementQuery The current query, for fluid interface
     */
    public function prune($servicepaiement = null)
    {
        if ($servicepaiement) {
            $this->addUsingAlias(ServicepaiementTableMap::COL_ID_SERVICEPAIEMENT, $servicepaiement->getIdServicepaiement(), Criteria::NOT_EQUAL);
        }

        return $this;
    }

    /**
     * Code to execute before every DELETE statement
     *
     * @param     ConnectionInterface $con The connection object used by the query
     */
    protected function basePreDelete(ConnectionInterface $con)
    {
        // archivable behavior

        if ($this->archiveOnDelete) {
            $this->archive($con);
        } else {
            $this->archiveOnDelete = true;
        }


        return $this->preDelete($con);
    }

    /**
     * Deletes all rows from the asso_servicepaiements table.
     *
     * @param ConnectionInterface $con the connection to use
     * @return int The number of affected rows (if supported by underlying database driver).
     */
    public function doDeleteAll(ConnectionInterface $con = null)
    {
        if (null === $con) {
            $con = Propel::getServiceContainer()->getWriteConnection(ServicepaiementTableMap::DATABASE_NAME);
        }

        // use transaction because $criteria could contain info
        // for more than one table or we could emulating ON DELETE CASCADE, etc.
        return $con->transaction(function () use ($con) {
            $affectedRows = 0; // initialize var to track total num of affected rows
            $affectedRows += parent::doDeleteAll($con);
            // Because this db requires some delete cascade/set null emulation, we have to
            // clear the cached instance *after* the emulation has happened (since
            // instances get re-added by the select statement contained therein).
            ServicepaiementTableMap::clearInstancePool();
            ServicepaiementTableMap::clearRelatedInstancePool();

            return $affectedRows;
        });
    }

    /**
     * Performs a DELETE on the database based on the current ModelCriteria
     *
     * @param ConnectionInterface $con the connection to use
     * @return int             The number of affected rows (if supported by underlying database driver).  This includes CASCADE-related rows
     *                         if supported by native driver or if emulated using Propel.
     * @throws PropelException Any exceptions caught during processing will be
     *                         rethrown wrapped into a PropelException.
     */
    public function delete(ConnectionInterface $con = null)
    {
        if (null === $con) {
            $con = Propel::getServiceContainer()->getWriteConnection(ServicepaiementTableMap::DATABASE_NAME);
        }

        $criteria = $this;

        // Set the correct dbName
        $criteria->setDbName(ServicepaiementTableMap::DATABASE_NAME);

        // use transaction because $criteria could contain info
        // for more than one table or we could emulating ON DELETE CASCADE, etc.
        return $con->transaction(function () use ($con, $criteria) {
            $affectedRows = 0; // initialize var to track total num of affected rows

            ServicepaiementTableMap::removeInstanceFromPool($criteria);

            $affectedRows += ModelCriteria::delete($con);
            ServicepaiementTableMap::clearRelatedInstancePool();

            return $affectedRows;
        });
    }

    // timestampable behavior

    /**
     * Filter by the latest updated
     *
     * @param      int $nbDays Maximum age of the latest update in days
     *
     * @return     $this|ChildServicepaiementQuery The current query, for fluid interface
     */
    public function recentlyUpdated($nbDays = 7)
    {
        return $this->addUsingAlias(ServicepaiementTableMap::COL_UPDATED_AT, time() - $nbDays * 24 * 60 * 60, Criteria::GREATER_EQUAL);
    }

    /**
     * Order by update date desc
     *
     * @return     $this|ChildServicepaiementQuery The current query, for fluid interface
     */
    public function lastUpdatedFirst()
    {
        return $this->addDescendingOrderByColumn(ServicepaiementTableMap::COL_UPDATED_AT);
    }

    /**
     * Order by update date asc
     *
     * @return     $this|ChildServicepaiementQuery The current query, for fluid interface
     */
    public function firstUpdatedFirst()
    {
        return $this->addAscendingOrderByColumn(ServicepaiementTableMap::COL_UPDATED_AT);
    }

    /**
     * Order by create date desc
     *
     * @return     $this|ChildServicepaiementQuery The current query, for fluid interface
     */
    public function lastCreatedFirst()
    {
        return $this->addDescendingOrderByColumn(ServicepaiementTableMap::COL_CREATED_AT);
    }

    /**
     * Filter by the latest created
     *
     * @param      int $nbDays Maximum age of in days
     *
     * @return     $this|ChildServicepaiementQuery The current query, for fluid interface
     */
    public function recentlyCreated($nbDays = 7)
    {
        return $this->addUsingAlias(ServicepaiementTableMap::COL_CREATED_AT, time() - $nbDays * 24 * 60 * 60, Criteria::GREATER_EQUAL);
    }

    /**
     * Order by create date asc
     *
     * @return     $this|ChildServicepaiementQuery The current query, for fluid interface
     */
    public function firstCreatedFirst()
    {
        return $this->addAscendingOrderByColumn(ServicepaiementTableMap::COL_CREATED_AT);
    }

    // archivable behavior

    /**
     * Copy the data of the objects satisfying the query into ChildAssoServicepaiementsArchive archive objects.
     * The archived objects are then saved.
     * If any of the objects has already been archived, the archived object
     * is updated and not duplicated.
     * Warning: This termination methods issues 2n+1 queries.
     *
     * @param      ConnectionInterface $con    Connection to use.
     * @param      Boolean $useLittleMemory    Whether or not to use OnDemandFormatter to retrieve objects.
     *               Set to false if the identity map matters.
     *               Set to true (default) to use less memory.
     *
     * @return     int the number of archived objects
     */
    public function archive($con = null, $useLittleMemory = true)
    {
        $criteria = clone $this;
        // prepare the query
        $criteria->setWith(array());
        if ($useLittleMemory) {
            $criteria->setFormatter(ModelCriteria::FORMAT_ON_DEMAND);
        }
        if ($con === null) {
            $con = Propel::getServiceContainer()->getWriteConnection(ServicepaiementTableMap::DATABASE_NAME);
        }

        return $con->transaction(function () use ($con, $criteria) {
            $totalArchivedObjects = 0;

            // archive all results one by one
            foreach ($criteria->find($con) as $object) {
                $object->archive($con);
                $totalArchivedObjects++;
            }

            return $totalArchivedObjects;
        });
    }

    /**
     * Enable/disable auto-archiving on delete for the next query.
     *
     * @param boolean True if the query must archive deleted objects, false otherwise.
     */
    public function setArchiveOnDelete($archiveOnDelete)
    {
        $this->archiveOnDelete = $archiveOnDelete;
    }

    /**
     * Delete records matching the current query without archiving them.
     *
     * @param      ConnectionInterface $con    Connection to use.
     *
     * @return integer the number of deleted rows
     */
    public function deleteWithoutArchive($con = null)
    {
        $this->archiveOnDelete = false;

        return $this->delete($con);
    }

    /**
     * Delete all records without archiving them.
     *
     * @param      ConnectionInterface $con    Connection to use.
     *
     * @return integer the number of deleted rows
     */
    public function deleteAllWithoutArchive($con = null)
    {
        $this->archiveOnDelete = false;

        return $this->deleteAll($con);
    }

} // ServicepaiementQuery
