<?php

namespace Base;

use \AssoImportsArchive as ChildAssoImportsArchive;
use \AssoImportsArchiveQuery as ChildAssoImportsArchiveQuery;
use \Import as ChildImport;
use \ImportQuery as ChildImportQuery;
use \Importligne as ChildImportligne;
use \ImportligneQuery as ChildImportligneQuery;
use \DateTime;
use \Exception;
use \PDO;
use Map\ImportTableMap;
use Map\ImportligneTableMap;
use Propel\Runtime\Propel;
use Propel\Runtime\ActiveQuery\Criteria;
use Propel\Runtime\ActiveQuery\ModelCriteria;
use Propel\Runtime\ActiveRecord\ActiveRecordInterface;
use Propel\Runtime\Collection\Collection;
use Propel\Runtime\Collection\ObjectCollection;
use Propel\Runtime\Connection\ConnectionInterface;
use Propel\Runtime\Exception\BadMethodCallException;
use Propel\Runtime\Exception\LogicException;
use Propel\Runtime\Exception\PropelException;
use Propel\Runtime\Map\TableMap;
use Propel\Runtime\Parser\AbstractParser;
use Propel\Runtime\Util\PropelDateTime;

/**
 * Base class that represents a row from the 'asso_imports' table.
 *
 *
 *
 * @package    propel.generator..Base
 */
abstract class Import implements ActiveRecordInterface
{
    /**
     * TableMap class name
     */
    const TABLE_MAP = '\\Map\\ImportTableMap';


    /**
     * attribute to determine if this object has previously been saved.
     * @var boolean
     */
    protected $new = true;

    /**
     * attribute to determine whether this object has been deleted.
     * @var boolean
     */
    protected $deleted = false;

    /**
     * The columns that have been modified in current object.
     * Tracking modified columns allows us to only update modified columns.
     * @var array
     */
    protected $modifiedColumns = array();

    /**
     * The (virtual) columns that are added at runtime
     * The formatters can add supplementary columns based on a resultset
     * @var array
     */
    protected $virtualColumns = array();

    /**
     * The value for the id_import field.
     *
     * @var        string
     */
    protected $id_import;

    /**
     * The value for the modele field.
     *
     * @var        string
     */
    protected $modele;

    /**
     * The value for the avancement field.
     * Avancement
     * Note: this column has a database default value of: 0
     * @var        int
     */
    protected $avancement;

    /**
     * The value for the nom field.
     * nom
     * @var        string
     */
    protected $nom;

    /**
     * The value for the nomprovisoire field.
     * nomprovisoire
     * @var        string
     */
    protected $nomprovisoire;

    /**
     * The value for the taille field.
     *
     * @var        string
     */
    protected $taille;

    /**
     * The value for the controlemd5 field.
     * somme
     * @var        string
     */
    protected $controlemd5;

    /**
     * The value for the originals field.
     * fichier original
     * @var        string
     */
    protected $originals;

    /**
     * Whether the lazy-loaded $originals value has been loaded from database.
     * This is necessary to avoid repeated lookups if $originals column is NULL in the db.
     * @var boolean
     */
    protected $originals_isLoaded = false;

    /**
     * The value for the informations field.
     *
     * @var        string
     */
    protected $informations;

    /**
     * Whether the lazy-loaded $informations value has been loaded from database.
     * This is necessary to avoid repeated lookups if $informations column is NULL in the db.
     * @var boolean
     */
    protected $informations_isLoaded = false;

    /**
     * The value for the modifications field.
     *
     * @var        string
     */
    protected $modifications;

    /**
     * Whether the lazy-loaded $modifications value has been loaded from database.
     * This is necessary to avoid repeated lookups if $modifications column is NULL in the db.
     * @var boolean
     */
    protected $modifications_isLoaded = false;

    /**
     * The value for the nb_ligne field.
     * Nb_ligne
     * @var        string
     */
    protected $nb_ligne;

    /**
     * The value for the ligne_en_cours field.
     * Nb_ligne
     * Note: this column has a database default value of: '0'
     * @var        string
     */
    protected $ligne_en_cours;

    /**
     * The value for the created_at field.
     *
     * @var        DateTime
     */
    protected $created_at;

    /**
     * The value for the updated_at field.
     *
     * @var        DateTime
     */
    protected $updated_at;

    /**
     * @var        ObjectCollection|ChildImportligne[] Collection to store aggregation of ChildImportligne objects.
     */
    protected $collImportlignes;
    protected $collImportlignesPartial;

    /**
     * Flag to prevent endless save loop, if this object is referenced
     * by another object which falls in this transaction.
     *
     * @var boolean
     */
    protected $alreadyInSave = false;

    // archivable behavior
    protected $archiveOnDelete = true;

    /**
     * An array of objects scheduled for deletion.
     * @var ObjectCollection|ChildImportligne[]
     */
    protected $importlignesScheduledForDeletion = null;

    /**
     * Applies default values to this object.
     * This method should be called from the object's constructor (or
     * equivalent initialization method).
     * @see __construct()
     */
    public function applyDefaultValues()
    {
        $this->avancement = 0;
        $this->ligne_en_cours = '0';
    }

    /**
     * Initializes internal state of Base\Import object.
     * @see applyDefaults()
     */
    public function __construct()
    {
        $this->applyDefaultValues();
    }

    /**
     * Returns whether the object has been modified.
     *
     * @return boolean True if the object has been modified.
     */
    public function isModified()
    {
        return !!$this->modifiedColumns;
    }

    /**
     * Has specified column been modified?
     *
     * @param  string  $col column fully qualified name (TableMap::TYPE_COLNAME), e.g. Book::AUTHOR_ID
     * @return boolean True if $col has been modified.
     */
    public function isColumnModified($col)
    {
        return $this->modifiedColumns && isset($this->modifiedColumns[$col]);
    }

    /**
     * Get the columns that have been modified in this object.
     * @return array A unique list of the modified column names for this object.
     */
    public function getModifiedColumns()
    {
        return $this->modifiedColumns ? array_keys($this->modifiedColumns) : [];
    }

    /**
     * Returns whether the object has ever been saved.  This will
     * be false, if the object was retrieved from storage or was created
     * and then saved.
     *
     * @return boolean true, if the object has never been persisted.
     */
    public function isNew()
    {
        return $this->new;
    }

    /**
     * Setter for the isNew attribute.  This method will be called
     * by Propel-generated children and objects.
     *
     * @param boolean $b the state of the object.
     */
    public function setNew($b)
    {
        $this->new = (boolean) $b;
    }

    /**
     * Whether this object has been deleted.
     * @return boolean The deleted state of this object.
     */
    public function isDeleted()
    {
        return $this->deleted;
    }

    /**
     * Specify whether this object has been deleted.
     * @param  boolean $b The deleted state of this object.
     * @return void
     */
    public function setDeleted($b)
    {
        $this->deleted = (boolean) $b;
    }

    /**
     * Sets the modified state for the object to be false.
     * @param  string $col If supplied, only the specified column is reset.
     * @return void
     */
    public function resetModified($col = null)
    {
        if (null !== $col) {
            if (isset($this->modifiedColumns[$col])) {
                unset($this->modifiedColumns[$col]);
            }
        } else {
            $this->modifiedColumns = array();
        }
    }

    /**
     * Compares this with another <code>Import</code> instance.  If
     * <code>obj</code> is an instance of <code>Import</code>, delegates to
     * <code>equals(Import)</code>.  Otherwise, returns <code>false</code>.
     *
     * @param  mixed   $obj The object to compare to.
     * @return boolean Whether equal to the object specified.
     */
    public function equals($obj)
    {
        if (!$obj instanceof static) {
            return false;
        }

        if ($this === $obj) {
            return true;
        }

        if (null === $this->getPrimaryKey() || null === $obj->getPrimaryKey()) {
            return false;
        }

        return $this->getPrimaryKey() === $obj->getPrimaryKey();
    }

    /**
     * Get the associative array of the virtual columns in this object
     *
     * @return array
     */
    public function getVirtualColumns()
    {
        return $this->virtualColumns;
    }

    /**
     * Checks the existence of a virtual column in this object
     *
     * @param  string  $name The virtual column name
     * @return boolean
     */
    public function hasVirtualColumn($name)
    {
        return array_key_exists($name, $this->virtualColumns);
    }

    /**
     * Get the value of a virtual column in this object
     *
     * @param  string $name The virtual column name
     * @return mixed
     *
     * @throws PropelException
     */
    public function getVirtualColumn($name)
    {
        if (!$this->hasVirtualColumn($name)) {
            throw new PropelException(sprintf('Cannot get value of inexistent virtual column %s.', $name));
        }

        return $this->virtualColumns[$name];
    }

    /**
     * Set the value of a virtual column in this object
     *
     * @param string $name  The virtual column name
     * @param mixed  $value The value to give to the virtual column
     *
     * @return $this|Import The current object, for fluid interface
     */
    public function setVirtualColumn($name, $value)
    {
        $this->virtualColumns[$name] = $value;

        return $this;
    }

    /**
     * Logs a message using Propel::log().
     *
     * @param  string  $msg
     * @param  int     $priority One of the Propel::LOG_* logging levels
     * @return boolean
     */
    protected function log($msg, $priority = Propel::LOG_INFO)
    {
        return Propel::log(get_class($this) . ': ' . $msg, $priority);
    }

    /**
     * Export the current object properties to a string, using a given parser format
     * <code>
     * $book = BookQuery::create()->findPk(9012);
     * echo $book->exportTo('JSON');
     *  => {"Id":9012,"Title":"Don Juan","ISBN":"0140422161","Price":12.99,"PublisherId":1234,"AuthorId":5678}');
     * </code>
     *
     * @param  mixed   $parser                 A AbstractParser instance, or a format name ('XML', 'YAML', 'JSON', 'CSV')
     * @param  boolean $includeLazyLoadColumns (optional) Whether to include lazy load(ed) columns. Defaults to TRUE.
     * @return string  The exported data
     */
    public function exportTo($parser, $includeLazyLoadColumns = true)
    {
        if (!$parser instanceof AbstractParser) {
            $parser = AbstractParser::getParser($parser);
        }

        return $parser->fromArray($this->toArray(TableMap::TYPE_PHPNAME, $includeLazyLoadColumns, array(), true));
    }

    /**
     * Clean up internal collections prior to serializing
     * Avoids recursive loops that turn into segmentation faults when serializing
     */
    public function __sleep()
    {
        $this->clearAllReferences();

        $cls = new \ReflectionClass($this);
        $propertyNames = [];
        $serializableProperties = array_diff($cls->getProperties(), $cls->getProperties(\ReflectionProperty::IS_STATIC));

        foreach($serializableProperties as $property) {
            $propertyNames[] = $property->getName();
        }

        return $propertyNames;
    }

    /**
     * Get the [id_import] column value.
     *
     * @return string
     */
    public function getIdImport()
    {
        return $this->id_import;
    }

    /**
     * Get the [modele] column value.
     *
     * @return string
     */
    public function getModele()
    {
        return $this->modele;
    }

    /**
     * Get the [avancement] column value.
     * Avancement
     * @return int
     */
    public function getAvancement()
    {
        return $this->avancement;
    }

    /**
     * Get the [nom] column value.
     * nom
     * @return string
     */
    public function getNom()
    {
        return $this->nom;
    }

    /**
     * Get the [nomprovisoire] column value.
     * nomprovisoire
     * @return string
     */
    public function getNomprovisoire()
    {
        return $this->nomprovisoire;
    }

    /**
     * Get the [taille] column value.
     *
     * @return string
     */
    public function getTaille()
    {
        return $this->taille;
    }

    /**
     * Get the [controlemd5] column value.
     * somme
     * @return string
     */
    public function getControlemd5()
    {
        return $this->controlemd5;
    }

    /**
     * Get the [originals] column value.
     * fichier original
     * @param      ConnectionInterface $con An optional ConnectionInterface connection to use for fetching this lazy-loaded column.
     * @return string
     */
    public function getOriginals(ConnectionInterface $con = null)
    {
        if (!$this->originals_isLoaded && $this->originals === null && !$this->isNew()) {
            $this->loadOriginals($con);
        }

        return $this->originals;
    }

    /**
     * Load the value for the lazy-loaded [originals] column.
     *
     * This method performs an additional query to return the value for
     * the [originals] column, since it is not populated by
     * the hydrate() method.
     *
     * @param      $con ConnectionInterface (optional) The ConnectionInterface connection to use.
     * @return void
     * @throws PropelException - any underlying error will be wrapped and re-thrown.
     */
    protected function loadOriginals(ConnectionInterface $con = null)
    {
        $c = $this->buildPkeyCriteria();
        $c->addSelectColumn(ImportTableMap::COL_ORIGINALS);
        try {
            $dataFetcher = ChildImportQuery::create(null, $c)->setFormatter(ModelCriteria::FORMAT_STATEMENT)->find($con);
            $row = $dataFetcher->fetch();
            $dataFetcher->close();

        $firstColumn = $row ? current($row) : null;

            $this->originals = ($firstColumn !== null) ? (string) $firstColumn : null;
            $this->originals_isLoaded = true;
        } catch (Exception $e) {
            throw new PropelException("Error loading value for [originals] column on demand.", 0, $e);
        }
    }
    /**
     * Get the [informations] column value.
     *
     * @param      ConnectionInterface $con An optional ConnectionInterface connection to use for fetching this lazy-loaded column.
     * @return string
     */
    public function getInformations(ConnectionInterface $con = null)
    {
        if (!$this->informations_isLoaded && $this->informations === null && !$this->isNew()) {
            $this->loadInformations($con);
        }

        return $this->informations;
    }

    /**
     * Load the value for the lazy-loaded [informations] column.
     *
     * This method performs an additional query to return the value for
     * the [informations] column, since it is not populated by
     * the hydrate() method.
     *
     * @param      $con ConnectionInterface (optional) The ConnectionInterface connection to use.
     * @return void
     * @throws PropelException - any underlying error will be wrapped and re-thrown.
     */
    protected function loadInformations(ConnectionInterface $con = null)
    {
        $c = $this->buildPkeyCriteria();
        $c->addSelectColumn(ImportTableMap::COL_INFORMATIONS);
        try {
            $dataFetcher = ChildImportQuery::create(null, $c)->setFormatter(ModelCriteria::FORMAT_STATEMENT)->find($con);
            $row = $dataFetcher->fetch();
            $dataFetcher->close();

        $firstColumn = $row ? current($row) : null;

            $this->informations = ($firstColumn !== null) ? (string) $firstColumn : null;
            $this->informations_isLoaded = true;
        } catch (Exception $e) {
            throw new PropelException("Error loading value for [informations] column on demand.", 0, $e);
        }
    }
    /**
     * Get the [modifications] column value.
     *
     * @param      ConnectionInterface $con An optional ConnectionInterface connection to use for fetching this lazy-loaded column.
     * @return string
     */
    public function getModifications(ConnectionInterface $con = null)
    {
        if (!$this->modifications_isLoaded && $this->modifications === null && !$this->isNew()) {
            $this->loadModifications($con);
        }

        return $this->modifications;
    }

    /**
     * Load the value for the lazy-loaded [modifications] column.
     *
     * This method performs an additional query to return the value for
     * the [modifications] column, since it is not populated by
     * the hydrate() method.
     *
     * @param      $con ConnectionInterface (optional) The ConnectionInterface connection to use.
     * @return void
     * @throws PropelException - any underlying error will be wrapped and re-thrown.
     */
    protected function loadModifications(ConnectionInterface $con = null)
    {
        $c = $this->buildPkeyCriteria();
        $c->addSelectColumn(ImportTableMap::COL_MODIFICATIONS);
        try {
            $dataFetcher = ChildImportQuery::create(null, $c)->setFormatter(ModelCriteria::FORMAT_STATEMENT)->find($con);
            $row = $dataFetcher->fetch();
            $dataFetcher->close();

        $firstColumn = $row ? current($row) : null;

            $this->modifications = ($firstColumn !== null) ? (string) $firstColumn : null;
            $this->modifications_isLoaded = true;
        } catch (Exception $e) {
            throw new PropelException("Error loading value for [modifications] column on demand.", 0, $e);
        }
    }
    /**
     * Get the [nb_ligne] column value.
     * Nb_ligne
     * @return string
     */
    public function getNbLigne()
    {
        return $this->nb_ligne;
    }

    /**
     * Get the [ligne_en_cours] column value.
     * Nb_ligne
     * @return string
     */
    public function getLigneEnCours()
    {
        return $this->ligne_en_cours;
    }

    /**
     * Get the [optionally formatted] temporal [created_at] column value.
     *
     *
     * @param      string $format The date/time format string (either date()-style or strftime()-style).
     *                            If format is NULL, then the raw DateTime object will be returned.
     *
     * @return string|DateTime Formatted date/time value as string or DateTime object (if format is NULL), NULL if column is NULL, and 0 if column value is 0000-00-00 00:00:00
     *
     * @throws PropelException - if unable to parse/validate the date/time value.
     */
    public function getCreatedAt($format = NULL)
    {
        if ($format === null) {
            return $this->created_at;
        } else {
            return $this->created_at instanceof \DateTimeInterface ? $this->created_at->format($format) : null;
        }
    }

    /**
     * Get the [optionally formatted] temporal [updated_at] column value.
     *
     *
     * @param      string $format The date/time format string (either date()-style or strftime()-style).
     *                            If format is NULL, then the raw DateTime object will be returned.
     *
     * @return string|DateTime Formatted date/time value as string or DateTime object (if format is NULL), NULL if column is NULL, and 0 if column value is 0000-00-00 00:00:00
     *
     * @throws PropelException - if unable to parse/validate the date/time value.
     */
    public function getUpdatedAt($format = NULL)
    {
        if ($format === null) {
            return $this->updated_at;
        } else {
            return $this->updated_at instanceof \DateTimeInterface ? $this->updated_at->format($format) : null;
        }
    }

    /**
     * Set the value of [id_import] column.
     *
     * @param string $v new value
     * @return $this|\Import The current object (for fluent API support)
     */
    public function setIdImport($v)
    {
        if ($v !== null) {
            $v = (string) $v;
        }

        if ($this->id_import !== $v) {
            $this->id_import = $v;
            $this->modifiedColumns[ImportTableMap::COL_ID_IMPORT] = true;
        }

        return $this;
    } // setIdImport()

    /**
     * Set the value of [modele] column.
     *
     * @param string $v new value
     * @return $this|\Import The current object (for fluent API support)
     */
    public function setModele($v)
    {
        if ($v !== null) {
            $v = (string) $v;
        }

        if ($this->modele !== $v) {
            $this->modele = $v;
            $this->modifiedColumns[ImportTableMap::COL_MODELE] = true;
        }

        return $this;
    } // setModele()

    /**
     * Set the value of [avancement] column.
     * Avancement
     * @param int $v new value
     * @return $this|\Import The current object (for fluent API support)
     */
    public function setAvancement($v)
    {
        if ($v !== null) {
            $v = (int) $v;
        }

        if ($this->avancement !== $v) {
            $this->avancement = $v;
            $this->modifiedColumns[ImportTableMap::COL_AVANCEMENT] = true;
        }

        return $this;
    } // setAvancement()

    /**
     * Set the value of [nom] column.
     * nom
     * @param string $v new value
     * @return $this|\Import The current object (for fluent API support)
     */
    public function setNom($v)
    {
        if ($v !== null) {
            $v = (string) $v;
        }

        if ($this->nom !== $v) {
            $this->nom = $v;
            $this->modifiedColumns[ImportTableMap::COL_NOM] = true;
        }

        return $this;
    } // setNom()

    /**
     * Set the value of [nomprovisoire] column.
     * nomprovisoire
     * @param string $v new value
     * @return $this|\Import The current object (for fluent API support)
     */
    public function setNomprovisoire($v)
    {
        if ($v !== null) {
            $v = (string) $v;
        }

        if ($this->nomprovisoire !== $v) {
            $this->nomprovisoire = $v;
            $this->modifiedColumns[ImportTableMap::COL_NOMPROVISOIRE] = true;
        }

        return $this;
    } // setNomprovisoire()

    /**
     * Set the value of [taille] column.
     *
     * @param string $v new value
     * @return $this|\Import The current object (for fluent API support)
     */
    public function setTaille($v)
    {
        if ($v !== null) {
            $v = (string) $v;
        }

        if ($this->taille !== $v) {
            $this->taille = $v;
            $this->modifiedColumns[ImportTableMap::COL_TAILLE] = true;
        }

        return $this;
    } // setTaille()

    /**
     * Set the value of [controlemd5] column.
     * somme
     * @param string $v new value
     * @return $this|\Import The current object (for fluent API support)
     */
    public function setControlemd5($v)
    {
        if ($v !== null) {
            $v = (string) $v;
        }

        if ($this->controlemd5 !== $v) {
            $this->controlemd5 = $v;
            $this->modifiedColumns[ImportTableMap::COL_CONTROLEMD5] = true;
        }

        return $this;
    } // setControlemd5()

    /**
     * Set the value of [originals] column.
     * fichier original
     * @param string $v new value
     * @return $this|\Import The current object (for fluent API support)
     */
    public function setOriginals($v)
    {
        // explicitly set the is-loaded flag to true for this lazy load col;
        // it doesn't matter if the value is actually set or not (logic below) as
        // any attempt to set the value means that no db lookup should be performed
        // when the getOriginals() method is called.
        $this->originals_isLoaded = true;

        if ($v !== null) {
            $v = (string) $v;
        }

        if ($this->originals !== $v) {
            $this->originals = $v;
            $this->modifiedColumns[ImportTableMap::COL_ORIGINALS] = true;
        }

        return $this;
    } // setOriginals()

    /**
     * Set the value of [informations] column.
     *
     * @param string $v new value
     * @return $this|\Import The current object (for fluent API support)
     */
    public function setInformations($v)
    {
        // explicitly set the is-loaded flag to true for this lazy load col;
        // it doesn't matter if the value is actually set or not (logic below) as
        // any attempt to set the value means that no db lookup should be performed
        // when the getInformations() method is called.
        $this->informations_isLoaded = true;

        if ($v !== null) {
            $v = (string) $v;
        }

        if ($this->informations !== $v) {
            $this->informations = $v;
            $this->modifiedColumns[ImportTableMap::COL_INFORMATIONS] = true;
        }

        return $this;
    } // setInformations()

    /**
     * Set the value of [modifications] column.
     *
     * @param string $v new value
     * @return $this|\Import The current object (for fluent API support)
     */
    public function setModifications($v)
    {
        // explicitly set the is-loaded flag to true for this lazy load col;
        // it doesn't matter if the value is actually set or not (logic below) as
        // any attempt to set the value means that no db lookup should be performed
        // when the getModifications() method is called.
        $this->modifications_isLoaded = true;

        if ($v !== null) {
            $v = (string) $v;
        }

        if ($this->modifications !== $v) {
            $this->modifications = $v;
            $this->modifiedColumns[ImportTableMap::COL_MODIFICATIONS] = true;
        }

        return $this;
    } // setModifications()

    /**
     * Set the value of [nb_ligne] column.
     * Nb_ligne
     * @param string $v new value
     * @return $this|\Import The current object (for fluent API support)
     */
    public function setNbLigne($v)
    {
        if ($v !== null) {
            $v = (string) $v;
        }

        if ($this->nb_ligne !== $v) {
            $this->nb_ligne = $v;
            $this->modifiedColumns[ImportTableMap::COL_NB_LIGNE] = true;
        }

        return $this;
    } // setNbLigne()

    /**
     * Set the value of [ligne_en_cours] column.
     * Nb_ligne
     * @param string $v new value
     * @return $this|\Import The current object (for fluent API support)
     */
    public function setLigneEnCours($v)
    {
        if ($v !== null) {
            $v = (string) $v;
        }

        if ($this->ligne_en_cours !== $v) {
            $this->ligne_en_cours = $v;
            $this->modifiedColumns[ImportTableMap::COL_LIGNE_EN_COURS] = true;
        }

        return $this;
    } // setLigneEnCours()

    /**
     * Sets the value of [created_at] column to a normalized version of the date/time value specified.
     *
     * @param  mixed $v string, integer (timestamp), or \DateTimeInterface value.
     *               Empty strings are treated as NULL.
     * @return $this|\Import The current object (for fluent API support)
     */
    public function setCreatedAt($v)
    {
        $dt = PropelDateTime::newInstance($v, null, 'DateTime');
        if ($this->created_at !== null || $dt !== null) {
            if ($this->created_at === null || $dt === null || $dt->format("Y-m-d H:i:s.u") !== $this->created_at->format("Y-m-d H:i:s.u")) {
                $this->created_at = $dt === null ? null : clone $dt;
                $this->modifiedColumns[ImportTableMap::COL_CREATED_AT] = true;
            }
        } // if either are not null

        return $this;
    } // setCreatedAt()

    /**
     * Sets the value of [updated_at] column to a normalized version of the date/time value specified.
     *
     * @param  mixed $v string, integer (timestamp), or \DateTimeInterface value.
     *               Empty strings are treated as NULL.
     * @return $this|\Import The current object (for fluent API support)
     */
    public function setUpdatedAt($v)
    {
        $dt = PropelDateTime::newInstance($v, null, 'DateTime');
        if ($this->updated_at !== null || $dt !== null) {
            if ($this->updated_at === null || $dt === null || $dt->format("Y-m-d H:i:s.u") !== $this->updated_at->format("Y-m-d H:i:s.u")) {
                $this->updated_at = $dt === null ? null : clone $dt;
                $this->modifiedColumns[ImportTableMap::COL_UPDATED_AT] = true;
            }
        } // if either are not null

        return $this;
    } // setUpdatedAt()

    /**
     * Indicates whether the columns in this object are only set to default values.
     *
     * This method can be used in conjunction with isModified() to indicate whether an object is both
     * modified _and_ has some values set which are non-default.
     *
     * @return boolean Whether the columns in this object are only been set with default values.
     */
    public function hasOnlyDefaultValues()
    {
            if ($this->avancement !== 0) {
                return false;
            }

            if ($this->ligne_en_cours !== '0') {
                return false;
            }

        // otherwise, everything was equal, so return TRUE
        return true;
    } // hasOnlyDefaultValues()

    /**
     * Hydrates (populates) the object variables with values from the database resultset.
     *
     * An offset (0-based "start column") is specified so that objects can be hydrated
     * with a subset of the columns in the resultset rows.  This is needed, for example,
     * for results of JOIN queries where the resultset row includes columns from two or
     * more tables.
     *
     * @param array   $row       The row returned by DataFetcher->fetch().
     * @param int     $startcol  0-based offset column which indicates which restultset column to start with.
     * @param boolean $rehydrate Whether this object is being re-hydrated from the database.
     * @param string  $indexType The index type of $row. Mostly DataFetcher->getIndexType().
                                  One of the class type constants TableMap::TYPE_PHPNAME, TableMap::TYPE_CAMELNAME
     *                            TableMap::TYPE_COLNAME, TableMap::TYPE_FIELDNAME, TableMap::TYPE_NUM.
     *
     * @return int             next starting column
     * @throws PropelException - Any caught Exception will be rewrapped as a PropelException.
     */
    public function hydrate($row, $startcol = 0, $rehydrate = false, $indexType = TableMap::TYPE_NUM)
    {
        try {

            $col = $row[TableMap::TYPE_NUM == $indexType ? 0 + $startcol : ImportTableMap::translateFieldName('IdImport', TableMap::TYPE_PHPNAME, $indexType)];
            $this->id_import = (null !== $col) ? (string) $col : null;

            $col = $row[TableMap::TYPE_NUM == $indexType ? 1 + $startcol : ImportTableMap::translateFieldName('Modele', TableMap::TYPE_PHPNAME, $indexType)];
            $this->modele = (null !== $col) ? (string) $col : null;

            $col = $row[TableMap::TYPE_NUM == $indexType ? 2 + $startcol : ImportTableMap::translateFieldName('Avancement', TableMap::TYPE_PHPNAME, $indexType)];
            $this->avancement = (null !== $col) ? (int) $col : null;

            $col = $row[TableMap::TYPE_NUM == $indexType ? 3 + $startcol : ImportTableMap::translateFieldName('Nom', TableMap::TYPE_PHPNAME, $indexType)];
            $this->nom = (null !== $col) ? (string) $col : null;

            $col = $row[TableMap::TYPE_NUM == $indexType ? 4 + $startcol : ImportTableMap::translateFieldName('Nomprovisoire', TableMap::TYPE_PHPNAME, $indexType)];
            $this->nomprovisoire = (null !== $col) ? (string) $col : null;

            $col = $row[TableMap::TYPE_NUM == $indexType ? 5 + $startcol : ImportTableMap::translateFieldName('Taille', TableMap::TYPE_PHPNAME, $indexType)];
            $this->taille = (null !== $col) ? (string) $col : null;

            $col = $row[TableMap::TYPE_NUM == $indexType ? 6 + $startcol : ImportTableMap::translateFieldName('Controlemd5', TableMap::TYPE_PHPNAME, $indexType)];
            $this->controlemd5 = (null !== $col) ? (string) $col : null;

            $col = $row[TableMap::TYPE_NUM == $indexType ? 7 + $startcol : ImportTableMap::translateFieldName('NbLigne', TableMap::TYPE_PHPNAME, $indexType)];
            $this->nb_ligne = (null !== $col) ? (string) $col : null;

            $col = $row[TableMap::TYPE_NUM == $indexType ? 8 + $startcol : ImportTableMap::translateFieldName('LigneEnCours', TableMap::TYPE_PHPNAME, $indexType)];
            $this->ligne_en_cours = (null !== $col) ? (string) $col : null;

            $col = $row[TableMap::TYPE_NUM == $indexType ? 9 + $startcol : ImportTableMap::translateFieldName('CreatedAt', TableMap::TYPE_PHPNAME, $indexType)];
            if ($col === '0000-00-00 00:00:00') {
                $col = null;
            }
            $this->created_at = (null !== $col) ? PropelDateTime::newInstance($col, null, 'DateTime') : null;

            $col = $row[TableMap::TYPE_NUM == $indexType ? 10 + $startcol : ImportTableMap::translateFieldName('UpdatedAt', TableMap::TYPE_PHPNAME, $indexType)];
            if ($col === '0000-00-00 00:00:00') {
                $col = null;
            }
            $this->updated_at = (null !== $col) ? PropelDateTime::newInstance($col, null, 'DateTime') : null;
            $this->resetModified();

            $this->setNew(false);

            if ($rehydrate) {
                $this->ensureConsistency();
            }

            return $startcol + 11; // 11 = ImportTableMap::NUM_HYDRATE_COLUMNS.

        } catch (Exception $e) {
            throw new PropelException(sprintf('Error populating %s object', '\\Import'), 0, $e);
        }
    }

    /**
     * Checks and repairs the internal consistency of the object.
     *
     * This method is executed after an already-instantiated object is re-hydrated
     * from the database.  It exists to check any foreign keys to make sure that
     * the objects related to the current object are correct based on foreign key.
     *
     * You can override this method in the stub class, but you should always invoke
     * the base method from the overridden method (i.e. parent::ensureConsistency()),
     * in case your model changes.
     *
     * @throws PropelException
     */
    public function ensureConsistency()
    {
    } // ensureConsistency

    /**
     * Reloads this object from datastore based on primary key and (optionally) resets all associated objects.
     *
     * This will only work if the object has been saved and has a valid primary key set.
     *
     * @param      boolean $deep (optional) Whether to also de-associated any related objects.
     * @param      ConnectionInterface $con (optional) The ConnectionInterface connection to use.
     * @return void
     * @throws PropelException - if this object is deleted, unsaved or doesn't have pk match in db
     */
    public function reload($deep = false, ConnectionInterface $con = null)
    {
        if ($this->isDeleted()) {
            throw new PropelException("Cannot reload a deleted object.");
        }

        if ($this->isNew()) {
            throw new PropelException("Cannot reload an unsaved object.");
        }

        if ($con === null) {
            $con = Propel::getServiceContainer()->getReadConnection(ImportTableMap::DATABASE_NAME);
        }

        // We don't need to alter the object instance pool; we're just modifying this instance
        // already in the pool.

        $dataFetcher = ChildImportQuery::create(null, $this->buildPkeyCriteria())->setFormatter(ModelCriteria::FORMAT_STATEMENT)->find($con);
        $row = $dataFetcher->fetch();
        $dataFetcher->close();
        if (!$row) {
            throw new PropelException('Cannot find matching row in the database to reload object values.');
        }
        $this->hydrate($row, 0, true, $dataFetcher->getIndexType()); // rehydrate

        // Reset the originals lazy-load column
        $this->originals = null;
        $this->originals_isLoaded = false;

        // Reset the informations lazy-load column
        $this->informations = null;
        $this->informations_isLoaded = false;

        // Reset the modifications lazy-load column
        $this->modifications = null;
        $this->modifications_isLoaded = false;

        if ($deep) {  // also de-associate any related objects?

            $this->collImportlignes = null;

        } // if (deep)
    }

    /**
     * Removes this object from datastore and sets delete attribute.
     *
     * @param      ConnectionInterface $con
     * @return void
     * @throws PropelException
     * @see Import::setDeleted()
     * @see Import::isDeleted()
     */
    public function delete(ConnectionInterface $con = null)
    {
        if ($this->isDeleted()) {
            throw new PropelException("This object has already been deleted.");
        }

        if ($con === null) {
            $con = Propel::getServiceContainer()->getWriteConnection(ImportTableMap::DATABASE_NAME);
        }

        $con->transaction(function () use ($con) {
            $deleteQuery = ChildImportQuery::create()
                ->filterByPrimaryKey($this->getPrimaryKey());
            $ret = $this->preDelete($con);
            // archivable behavior
            if ($ret) {
                if ($this->archiveOnDelete) {
                    // do nothing yet. The object will be archived later when calling ChildImportQuery::delete().
                } else {
                    $deleteQuery->setArchiveOnDelete(false);
                    $this->archiveOnDelete = true;
                }
            }

            if ($ret) {
                $deleteQuery->delete($con);
                $this->postDelete($con);
                $this->setDeleted(true);
            }
        });
    }

    /**
     * Persists this object to the database.
     *
     * If the object is new, it inserts it; otherwise an update is performed.
     * All modified related objects will also be persisted in the doSave()
     * method.  This method wraps all precipitate database operations in a
     * single transaction.
     *
     * @param      ConnectionInterface $con
     * @return int             The number of rows affected by this insert/update and any referring fk objects' save() operations.
     * @throws PropelException
     * @see doSave()
     */
    public function save(ConnectionInterface $con = null)
    {
        if ($this->isDeleted()) {
            throw new PropelException("You cannot save an object that has been deleted.");
        }

        if ($this->alreadyInSave) {
            return 0;
        }

        if ($con === null) {
            $con = Propel::getServiceContainer()->getWriteConnection(ImportTableMap::DATABASE_NAME);
        }

        return $con->transaction(function () use ($con) {
            $ret = $this->preSave($con);
            $isInsert = $this->isNew();
            if ($isInsert) {
                $ret = $ret && $this->preInsert($con);
                // timestampable behavior

                if (!$this->isColumnModified(ImportTableMap::COL_CREATED_AT)) {
                    $this->setCreatedAt(\Propel\Runtime\Util\PropelDateTime::createHighPrecision());
                }
                if (!$this->isColumnModified(ImportTableMap::COL_UPDATED_AT)) {
                    $this->setUpdatedAt(\Propel\Runtime\Util\PropelDateTime::createHighPrecision());
                }
            } else {
                $ret = $ret && $this->preUpdate($con);
                // timestampable behavior
                if ($this->isModified() && !$this->isColumnModified(ImportTableMap::COL_UPDATED_AT)) {
                    $this->setUpdatedAt(\Propel\Runtime\Util\PropelDateTime::createHighPrecision());
                }
            }
            if ($ret) {
                $affectedRows = $this->doSave($con);
                if ($isInsert) {
                    $this->postInsert($con);
                } else {
                    $this->postUpdate($con);
                }
                $this->postSave($con);
                ImportTableMap::addInstanceToPool($this);
            } else {
                $affectedRows = 0;
            }

            return $affectedRows;
        });
    }

    /**
     * Performs the work of inserting or updating the row in the database.
     *
     * If the object is new, it inserts it; otherwise an update is performed.
     * All related objects are also updated in this method.
     *
     * @param      ConnectionInterface $con
     * @return int             The number of rows affected by this insert/update and any referring fk objects' save() operations.
     * @throws PropelException
     * @see save()
     */
    protected function doSave(ConnectionInterface $con)
    {
        $affectedRows = 0; // initialize var to track total num of affected rows
        if (!$this->alreadyInSave) {
            $this->alreadyInSave = true;

            if ($this->isNew() || $this->isModified()) {
                // persist changes
                if ($this->isNew()) {
                    $this->doInsert($con);
                    $affectedRows += 1;
                } else {
                    $affectedRows += $this->doUpdate($con);
                }
                $this->resetModified();
            }

            if ($this->importlignesScheduledForDeletion !== null) {
                if (!$this->importlignesScheduledForDeletion->isEmpty()) {
                    \ImportligneQuery::create()
                        ->filterByPrimaryKeys($this->importlignesScheduledForDeletion->getPrimaryKeys(false))
                        ->delete($con);
                    $this->importlignesScheduledForDeletion = null;
                }
            }

            if ($this->collImportlignes !== null) {
                foreach ($this->collImportlignes as $referrerFK) {
                    if (!$referrerFK->isDeleted() && ($referrerFK->isNew() || $referrerFK->isModified())) {
                        $affectedRows += $referrerFK->save($con);
                    }
                }
            }

            $this->alreadyInSave = false;

        }

        return $affectedRows;
    } // doSave()

    /**
     * Insert the row in the database.
     *
     * @param      ConnectionInterface $con
     *
     * @throws PropelException
     * @see doSave()
     */
    protected function doInsert(ConnectionInterface $con)
    {
        $modifiedColumns = array();
        $index = 0;

        $this->modifiedColumns[ImportTableMap::COL_ID_IMPORT] = true;
        if (null !== $this->id_import) {
            throw new PropelException('Cannot insert a value for auto-increment primary key (' . ImportTableMap::COL_ID_IMPORT . ')');
        }

         // check the columns in natural order for more readable SQL queries
        if ($this->isColumnModified(ImportTableMap::COL_ID_IMPORT)) {
            $modifiedColumns[':p' . $index++]  = '`id_import`';
        }
        if ($this->isColumnModified(ImportTableMap::COL_MODELE)) {
            $modifiedColumns[':p' . $index++]  = '`modele`';
        }
        if ($this->isColumnModified(ImportTableMap::COL_AVANCEMENT)) {
            $modifiedColumns[':p' . $index++]  = '`avancement`';
        }
        if ($this->isColumnModified(ImportTableMap::COL_NOM)) {
            $modifiedColumns[':p' . $index++]  = '`nom`';
        }
        if ($this->isColumnModified(ImportTableMap::COL_NOMPROVISOIRE)) {
            $modifiedColumns[':p' . $index++]  = '`nomprovisoire`';
        }
        if ($this->isColumnModified(ImportTableMap::COL_TAILLE)) {
            $modifiedColumns[':p' . $index++]  = '`taille`';
        }
        if ($this->isColumnModified(ImportTableMap::COL_CONTROLEMD5)) {
            $modifiedColumns[':p' . $index++]  = '`controlemd5`';
        }
        if ($this->isColumnModified(ImportTableMap::COL_ORIGINALS)) {
            $modifiedColumns[':p' . $index++]  = '`originals`';
        }
        if ($this->isColumnModified(ImportTableMap::COL_INFORMATIONS)) {
            $modifiedColumns[':p' . $index++]  = '`informations`';
        }
        if ($this->isColumnModified(ImportTableMap::COL_MODIFICATIONS)) {
            $modifiedColumns[':p' . $index++]  = '`modifications`';
        }
        if ($this->isColumnModified(ImportTableMap::COL_NB_LIGNE)) {
            $modifiedColumns[':p' . $index++]  = '`nb_ligne`';
        }
        if ($this->isColumnModified(ImportTableMap::COL_LIGNE_EN_COURS)) {
            $modifiedColumns[':p' . $index++]  = '`ligne_en_cours`';
        }
        if ($this->isColumnModified(ImportTableMap::COL_CREATED_AT)) {
            $modifiedColumns[':p' . $index++]  = '`created_at`';
        }
        if ($this->isColumnModified(ImportTableMap::COL_UPDATED_AT)) {
            $modifiedColumns[':p' . $index++]  = '`updated_at`';
        }

        $sql = sprintf(
            'INSERT INTO `asso_imports` (%s) VALUES (%s)',
            implode(', ', $modifiedColumns),
            implode(', ', array_keys($modifiedColumns))
        );

        try {
            $stmt = $con->prepare($sql);
            foreach ($modifiedColumns as $identifier => $columnName) {
                switch ($columnName) {
                    case '`id_import`':
                        $stmt->bindValue($identifier, $this->id_import, PDO::PARAM_INT);
                        break;
                    case '`modele`':
                        $stmt->bindValue($identifier, $this->modele, PDO::PARAM_STR);
                        break;
                    case '`avancement`':
                        $stmt->bindValue($identifier, $this->avancement, PDO::PARAM_INT);
                        break;
                    case '`nom`':
                        $stmt->bindValue($identifier, $this->nom, PDO::PARAM_STR);
                        break;
                    case '`nomprovisoire`':
                        $stmt->bindValue($identifier, $this->nomprovisoire, PDO::PARAM_STR);
                        break;
                    case '`taille`':
                        $stmt->bindValue($identifier, $this->taille, PDO::PARAM_INT);
                        break;
                    case '`controlemd5`':
                        $stmt->bindValue($identifier, $this->controlemd5, PDO::PARAM_STR);
                        break;
                    case '`originals`':
                        $stmt->bindValue($identifier, $this->originals, PDO::PARAM_STR);
                        break;
                    case '`informations`':
                        $stmt->bindValue($identifier, $this->informations, PDO::PARAM_STR);
                        break;
                    case '`modifications`':
                        $stmt->bindValue($identifier, $this->modifications, PDO::PARAM_STR);
                        break;
                    case '`nb_ligne`':
                        $stmt->bindValue($identifier, $this->nb_ligne, PDO::PARAM_INT);
                        break;
                    case '`ligne_en_cours`':
                        $stmt->bindValue($identifier, $this->ligne_en_cours, PDO::PARAM_INT);
                        break;
                    case '`created_at`':
                        $stmt->bindValue($identifier, $this->created_at ? $this->created_at->format("Y-m-d H:i:s.u") : null, PDO::PARAM_STR);
                        break;
                    case '`updated_at`':
                        $stmt->bindValue($identifier, $this->updated_at ? $this->updated_at->format("Y-m-d H:i:s.u") : null, PDO::PARAM_STR);
                        break;
                }
            }
            $stmt->execute();
        } catch (Exception $e) {
            Propel::log($e->getMessage(), Propel::LOG_ERR);
            throw new PropelException(sprintf('Unable to execute INSERT statement [%s]', $sql), 0, $e);
        }

        try {
            $pk = $con->lastInsertId();
        } catch (Exception $e) {
            throw new PropelException('Unable to get autoincrement id.', 0, $e);
        }
        $this->setIdImport($pk);

        $this->setNew(false);
    }

    /**
     * Update the row in the database.
     *
     * @param      ConnectionInterface $con
     *
     * @return Integer Number of updated rows
     * @see doSave()
     */
    protected function doUpdate(ConnectionInterface $con)
    {
        $selectCriteria = $this->buildPkeyCriteria();
        $valuesCriteria = $this->buildCriteria();

        return $selectCriteria->doUpdate($valuesCriteria, $con);
    }

    /**
     * Retrieves a field from the object by name passed in as a string.
     *
     * @param      string $name name
     * @param      string $type The type of fieldname the $name is of:
     *                     one of the class type constants TableMap::TYPE_PHPNAME, TableMap::TYPE_CAMELNAME
     *                     TableMap::TYPE_COLNAME, TableMap::TYPE_FIELDNAME, TableMap::TYPE_NUM.
     *                     Defaults to TableMap::TYPE_FIELDNAME.
     * @return mixed Value of field.
     */
    public function getByName($name, $type = TableMap::TYPE_FIELDNAME)
    {
        $pos = ImportTableMap::translateFieldName($name, $type, TableMap::TYPE_NUM);
        $field = $this->getByPosition($pos);

        return $field;
    }

    /**
     * Retrieves a field from the object by Position as specified in the xml schema.
     * Zero-based.
     *
     * @param      int $pos position in xml schema
     * @return mixed Value of field at $pos
     */
    public function getByPosition($pos)
    {
        switch ($pos) {
            case 0:
                return $this->getIdImport();
                break;
            case 1:
                return $this->getModele();
                break;
            case 2:
                return $this->getAvancement();
                break;
            case 3:
                return $this->getNom();
                break;
            case 4:
                return $this->getNomprovisoire();
                break;
            case 5:
                return $this->getTaille();
                break;
            case 6:
                return $this->getControlemd5();
                break;
            case 7:
                return $this->getOriginals();
                break;
            case 8:
                return $this->getInformations();
                break;
            case 9:
                return $this->getModifications();
                break;
            case 10:
                return $this->getNbLigne();
                break;
            case 11:
                return $this->getLigneEnCours();
                break;
            case 12:
                return $this->getCreatedAt();
                break;
            case 13:
                return $this->getUpdatedAt();
                break;
            default:
                return null;
                break;
        } // switch()
    }

    /**
     * Exports the object as an array.
     *
     * You can specify the key type of the array by passing one of the class
     * type constants.
     *
     * @param     string  $keyType (optional) One of the class type constants TableMap::TYPE_PHPNAME, TableMap::TYPE_CAMELNAME,
     *                    TableMap::TYPE_COLNAME, TableMap::TYPE_FIELDNAME, TableMap::TYPE_NUM.
     *                    Defaults to TableMap::TYPE_FIELDNAME.
     * @param     boolean $includeLazyLoadColumns (optional) Whether to include lazy loaded columns. Defaults to TRUE.
     * @param     array $alreadyDumpedObjects List of objects to skip to avoid recursion
     * @param     boolean $includeForeignObjects (optional) Whether to include hydrated related objects. Default to FALSE.
     *
     * @return array an associative array containing the field names (as keys) and field values
     */
    public function toArray($keyType = TableMap::TYPE_FIELDNAME, $includeLazyLoadColumns = true, $alreadyDumpedObjects = array(), $includeForeignObjects = false)
    {

        if (isset($alreadyDumpedObjects['Import'][$this->hashCode()])) {
            return '*RECURSION*';
        }
        $alreadyDumpedObjects['Import'][$this->hashCode()] = true;
        $keys = ImportTableMap::getFieldNames($keyType);
        $result = array(
            $keys[0] => $this->getIdImport(),
            $keys[1] => $this->getModele(),
            $keys[2] => $this->getAvancement(),
            $keys[3] => $this->getNom(),
            $keys[4] => $this->getNomprovisoire(),
            $keys[5] => $this->getTaille(),
            $keys[6] => $this->getControlemd5(),
            $keys[7] => ($includeLazyLoadColumns) ? $this->getOriginals() : null,
            $keys[8] => ($includeLazyLoadColumns) ? $this->getInformations() : null,
            $keys[9] => ($includeLazyLoadColumns) ? $this->getModifications() : null,
            $keys[10] => $this->getNbLigne(),
            $keys[11] => $this->getLigneEnCours(),
            $keys[12] => $this->getCreatedAt(),
            $keys[13] => $this->getUpdatedAt(),
        );
        if ($result[$keys[12]] instanceof \DateTimeInterface) {
            $result[$keys[12]] = $result[$keys[12]]->format('c');
        }

        if ($result[$keys[13]] instanceof \DateTimeInterface) {
            $result[$keys[13]] = $result[$keys[13]]->format('c');
        }

        $virtualColumns = $this->virtualColumns;
        foreach ($virtualColumns as $key => $virtualColumn) {
            $result[$key] = $virtualColumn;
        }

        if ($includeForeignObjects) {
            if (null !== $this->collImportlignes) {

                switch ($keyType) {
                    case TableMap::TYPE_CAMELNAME:
                        $key = 'importlignes';
                        break;
                    case TableMap::TYPE_FIELDNAME:
                        $key = 'asso_importligness';
                        break;
                    default:
                        $key = 'Importlignes';
                }

                $result[$key] = $this->collImportlignes->toArray(null, false, $keyType, $includeLazyLoadColumns, $alreadyDumpedObjects);
            }
        }

        return $result;
    }

    /**
     * Sets a field from the object by name passed in as a string.
     *
     * @param  string $name
     * @param  mixed  $value field value
     * @param  string $type The type of fieldname the $name is of:
     *                one of the class type constants TableMap::TYPE_PHPNAME, TableMap::TYPE_CAMELNAME
     *                TableMap::TYPE_COLNAME, TableMap::TYPE_FIELDNAME, TableMap::TYPE_NUM.
     *                Defaults to TableMap::TYPE_FIELDNAME.
     * @return $this|\Import
     */
    public function setByName($name, $value, $type = TableMap::TYPE_FIELDNAME)
    {
        $pos = ImportTableMap::translateFieldName($name, $type, TableMap::TYPE_NUM);

        return $this->setByPosition($pos, $value);
    }

    /**
     * Sets a field from the object by Position as specified in the xml schema.
     * Zero-based.
     *
     * @param  int $pos position in xml schema
     * @param  mixed $value field value
     * @return $this|\Import
     */
    public function setByPosition($pos, $value)
    {
        switch ($pos) {
            case 0:
                $this->setIdImport($value);
                break;
            case 1:
                $this->setModele($value);
                break;
            case 2:
                $this->setAvancement($value);
                break;
            case 3:
                $this->setNom($value);
                break;
            case 4:
                $this->setNomprovisoire($value);
                break;
            case 5:
                $this->setTaille($value);
                break;
            case 6:
                $this->setControlemd5($value);
                break;
            case 7:
                $this->setOriginals($value);
                break;
            case 8:
                $this->setInformations($value);
                break;
            case 9:
                $this->setModifications($value);
                break;
            case 10:
                $this->setNbLigne($value);
                break;
            case 11:
                $this->setLigneEnCours($value);
                break;
            case 12:
                $this->setCreatedAt($value);
                break;
            case 13:
                $this->setUpdatedAt($value);
                break;
        } // switch()

        return $this;
    }

    /**
     * Populates the object using an array.
     *
     * This is particularly useful when populating an object from one of the
     * request arrays (e.g. $_POST).  This method goes through the column
     * names, checking to see whether a matching key exists in populated
     * array. If so the setByName() method is called for that column.
     *
     * You can specify the key type of the array by additionally passing one
     * of the class type constants TableMap::TYPE_PHPNAME, TableMap::TYPE_CAMELNAME,
     * TableMap::TYPE_COLNAME, TableMap::TYPE_FIELDNAME, TableMap::TYPE_NUM.
     * The default key type is the column's TableMap::TYPE_FIELDNAME.
     *
     * @param      array  $arr     An array to populate the object from.
     * @param      string $keyType The type of keys the array uses.
     * @return void
     */
    public function fromArray($arr, $keyType = TableMap::TYPE_FIELDNAME)
    {
        $keys = ImportTableMap::getFieldNames($keyType);

        if (array_key_exists($keys[0], $arr)) {
            $this->setIdImport($arr[$keys[0]]);
        }
        if (array_key_exists($keys[1], $arr)) {
            $this->setModele($arr[$keys[1]]);
        }
        if (array_key_exists($keys[2], $arr)) {
            $this->setAvancement($arr[$keys[2]]);
        }
        if (array_key_exists($keys[3], $arr)) {
            $this->setNom($arr[$keys[3]]);
        }
        if (array_key_exists($keys[4], $arr)) {
            $this->setNomprovisoire($arr[$keys[4]]);
        }
        if (array_key_exists($keys[5], $arr)) {
            $this->setTaille($arr[$keys[5]]);
        }
        if (array_key_exists($keys[6], $arr)) {
            $this->setControlemd5($arr[$keys[6]]);
        }
        if (array_key_exists($keys[7], $arr)) {
            $this->setOriginals($arr[$keys[7]]);
        }
        if (array_key_exists($keys[8], $arr)) {
            $this->setInformations($arr[$keys[8]]);
        }
        if (array_key_exists($keys[9], $arr)) {
            $this->setModifications($arr[$keys[9]]);
        }
        if (array_key_exists($keys[10], $arr)) {
            $this->setNbLigne($arr[$keys[10]]);
        }
        if (array_key_exists($keys[11], $arr)) {
            $this->setLigneEnCours($arr[$keys[11]]);
        }
        if (array_key_exists($keys[12], $arr)) {
            $this->setCreatedAt($arr[$keys[12]]);
        }
        if (array_key_exists($keys[13], $arr)) {
            $this->setUpdatedAt($arr[$keys[13]]);
        }
    }

     /**
     * Populate the current object from a string, using a given parser format
     * <code>
     * $book = new Book();
     * $book->importFrom('JSON', '{"Id":9012,"Title":"Don Juan","ISBN":"0140422161","Price":12.99,"PublisherId":1234,"AuthorId":5678}');
     * </code>
     *
     * You can specify the key type of the array by additionally passing one
     * of the class type constants TableMap::TYPE_PHPNAME, TableMap::TYPE_CAMELNAME,
     * TableMap::TYPE_COLNAME, TableMap::TYPE_FIELDNAME, TableMap::TYPE_NUM.
     * The default key type is the column's TableMap::TYPE_FIELDNAME.
     *
     * @param mixed $parser A AbstractParser instance,
     *                       or a format name ('XML', 'YAML', 'JSON', 'CSV')
     * @param string $data The source data to import from
     * @param string $keyType The type of keys the array uses.
     *
     * @return $this|\Import The current object, for fluid interface
     */
    public function importFrom($parser, $data, $keyType = TableMap::TYPE_FIELDNAME)
    {
        if (!$parser instanceof AbstractParser) {
            $parser = AbstractParser::getParser($parser);
        }

        $this->fromArray($parser->toArray($data), $keyType);

        return $this;
    }

    /**
     * Build a Criteria object containing the values of all modified columns in this object.
     *
     * @return Criteria The Criteria object containing all modified values.
     */
    public function buildCriteria()
    {
        $criteria = new Criteria(ImportTableMap::DATABASE_NAME);

        if ($this->isColumnModified(ImportTableMap::COL_ID_IMPORT)) {
            $criteria->add(ImportTableMap::COL_ID_IMPORT, $this->id_import);
        }
        if ($this->isColumnModified(ImportTableMap::COL_MODELE)) {
            $criteria->add(ImportTableMap::COL_MODELE, $this->modele);
        }
        if ($this->isColumnModified(ImportTableMap::COL_AVANCEMENT)) {
            $criteria->add(ImportTableMap::COL_AVANCEMENT, $this->avancement);
        }
        if ($this->isColumnModified(ImportTableMap::COL_NOM)) {
            $criteria->add(ImportTableMap::COL_NOM, $this->nom);
        }
        if ($this->isColumnModified(ImportTableMap::COL_NOMPROVISOIRE)) {
            $criteria->add(ImportTableMap::COL_NOMPROVISOIRE, $this->nomprovisoire);
        }
        if ($this->isColumnModified(ImportTableMap::COL_TAILLE)) {
            $criteria->add(ImportTableMap::COL_TAILLE, $this->taille);
        }
        if ($this->isColumnModified(ImportTableMap::COL_CONTROLEMD5)) {
            $criteria->add(ImportTableMap::COL_CONTROLEMD5, $this->controlemd5);
        }
        if ($this->isColumnModified(ImportTableMap::COL_ORIGINALS)) {
            $criteria->add(ImportTableMap::COL_ORIGINALS, $this->originals);
        }
        if ($this->isColumnModified(ImportTableMap::COL_INFORMATIONS)) {
            $criteria->add(ImportTableMap::COL_INFORMATIONS, $this->informations);
        }
        if ($this->isColumnModified(ImportTableMap::COL_MODIFICATIONS)) {
            $criteria->add(ImportTableMap::COL_MODIFICATIONS, $this->modifications);
        }
        if ($this->isColumnModified(ImportTableMap::COL_NB_LIGNE)) {
            $criteria->add(ImportTableMap::COL_NB_LIGNE, $this->nb_ligne);
        }
        if ($this->isColumnModified(ImportTableMap::COL_LIGNE_EN_COURS)) {
            $criteria->add(ImportTableMap::COL_LIGNE_EN_COURS, $this->ligne_en_cours);
        }
        if ($this->isColumnModified(ImportTableMap::COL_CREATED_AT)) {
            $criteria->add(ImportTableMap::COL_CREATED_AT, $this->created_at);
        }
        if ($this->isColumnModified(ImportTableMap::COL_UPDATED_AT)) {
            $criteria->add(ImportTableMap::COL_UPDATED_AT, $this->updated_at);
        }

        return $criteria;
    }

    /**
     * Builds a Criteria object containing the primary key for this object.
     *
     * Unlike buildCriteria() this method includes the primary key values regardless
     * of whether or not they have been modified.
     *
     * @throws LogicException if no primary key is defined
     *
     * @return Criteria The Criteria object containing value(s) for primary key(s).
     */
    public function buildPkeyCriteria()
    {
        $criteria = ChildImportQuery::create();
        $criteria->add(ImportTableMap::COL_ID_IMPORT, $this->id_import);

        return $criteria;
    }

    /**
     * If the primary key is not null, return the hashcode of the
     * primary key. Otherwise, return the hash code of the object.
     *
     * @return int Hashcode
     */
    public function hashCode()
    {
        $validPk = null !== $this->getIdImport();

        $validPrimaryKeyFKs = 0;
        $primaryKeyFKs = [];

        if ($validPk) {
            return crc32(json_encode($this->getPrimaryKey(), JSON_UNESCAPED_UNICODE));
        } elseif ($validPrimaryKeyFKs) {
            return crc32(json_encode($primaryKeyFKs, JSON_UNESCAPED_UNICODE));
        }

        return spl_object_hash($this);
    }

    /**
     * Returns the primary key for this object (row).
     * @return string
     */
    public function getPrimaryKey()
    {
        return $this->getIdImport();
    }

    /**
     * Generic method to set the primary key (id_import column).
     *
     * @param       string $key Primary key.
     * @return void
     */
    public function setPrimaryKey($key)
    {
        $this->setIdImport($key);
    }

    /**
     * Returns true if the primary key for this object is null.
     * @return boolean
     */
    public function isPrimaryKeyNull()
    {
        return null === $this->getIdImport();
    }

    /**
     * Sets contents of passed object to values from current object.
     *
     * If desired, this method can also make copies of all associated (fkey referrers)
     * objects.
     *
     * @param      object $copyObj An object of \Import (or compatible) type.
     * @param      boolean $deepCopy Whether to also copy all rows that refer (by fkey) to the current row.
     * @param      boolean $makeNew Whether to reset autoincrement PKs and make the object new.
     * @throws PropelException
     */
    public function copyInto($copyObj, $deepCopy = false, $makeNew = true)
    {
        $copyObj->setModele($this->getModele());
        $copyObj->setAvancement($this->getAvancement());
        $copyObj->setNom($this->getNom());
        $copyObj->setNomprovisoire($this->getNomprovisoire());
        $copyObj->setTaille($this->getTaille());
        $copyObj->setControlemd5($this->getControlemd5());
        $copyObj->setOriginals($this->getOriginals());
        $copyObj->setInformations($this->getInformations());
        $copyObj->setModifications($this->getModifications());
        $copyObj->setNbLigne($this->getNbLigne());
        $copyObj->setLigneEnCours($this->getLigneEnCours());
        $copyObj->setCreatedAt($this->getCreatedAt());
        $copyObj->setUpdatedAt($this->getUpdatedAt());

        if ($deepCopy) {
            // important: temporarily setNew(false) because this affects the behavior of
            // the getter/setter methods for fkey referrer objects.
            $copyObj->setNew(false);

            foreach ($this->getImportlignes() as $relObj) {
                if ($relObj !== $this) {  // ensure that we don't try to copy a reference to ourselves
                    $copyObj->addImportligne($relObj->copy($deepCopy));
                }
            }

        } // if ($deepCopy)

        if ($makeNew) {
            $copyObj->setNew(true);
            $copyObj->setIdImport(NULL); // this is a auto-increment column, so set to default value
        }
    }

    /**
     * Makes a copy of this object that will be inserted as a new row in table when saved.
     * It creates a new object filling in the simple attributes, but skipping any primary
     * keys that are defined for the table.
     *
     * If desired, this method can also make copies of all associated (fkey referrers)
     * objects.
     *
     * @param  boolean $deepCopy Whether to also copy all rows that refer (by fkey) to the current row.
     * @return \Import Clone of current object.
     * @throws PropelException
     */
    public function copy($deepCopy = false)
    {
        // we use get_class(), because this might be a subclass
        $clazz = get_class($this);
        $copyObj = new $clazz();
        $this->copyInto($copyObj, $deepCopy);

        return $copyObj;
    }


    /**
     * Initializes a collection based on the name of a relation.
     * Avoids crafting an 'init[$relationName]s' method name
     * that wouldn't work when StandardEnglishPluralizer is used.
     *
     * @param      string $relationName The name of the relation to initialize
     * @return void
     */
    public function initRelation($relationName)
    {
        if ('Importligne' == $relationName) {
            $this->initImportlignes();
            return;
        }
    }

    /**
     * Clears out the collImportlignes collection
     *
     * This does not modify the database; however, it will remove any associated objects, causing
     * them to be refetched by subsequent calls to accessor method.
     *
     * @return void
     * @see        addImportlignes()
     */
    public function clearImportlignes()
    {
        $this->collImportlignes = null; // important to set this to NULL since that means it is uninitialized
    }

    /**
     * Reset is the collImportlignes collection loaded partially.
     */
    public function resetPartialImportlignes($v = true)
    {
        $this->collImportlignesPartial = $v;
    }

    /**
     * Initializes the collImportlignes collection.
     *
     * By default this just sets the collImportlignes collection to an empty array (like clearcollImportlignes());
     * however, you may wish to override this method in your stub class to provide setting appropriate
     * to your application -- for example, setting the initial array to the values stored in database.
     *
     * @param      boolean $overrideExisting If set to true, the method call initializes
     *                                        the collection even if it is not empty
     *
     * @return void
     */
    public function initImportlignes($overrideExisting = true)
    {
        if (null !== $this->collImportlignes && !$overrideExisting) {
            return;
        }

        $collectionClassName = ImportligneTableMap::getTableMap()->getCollectionClassName();

        $this->collImportlignes = new $collectionClassName;
        $this->collImportlignes->setModel('\Importligne');
    }

    /**
     * Gets an array of ChildImportligne objects which contain a foreign key that references this object.
     *
     * If the $criteria is not null, it is used to always fetch the results from the database.
     * Otherwise the results are fetched from the database the first time, then cached.
     * Next time the same method is called without $criteria, the cached collection is returned.
     * If this ChildImport is new, it will return
     * an empty collection or the current collection; the criteria is ignored on a new object.
     *
     * @param      Criteria $criteria optional Criteria object to narrow the query
     * @param      ConnectionInterface $con optional connection object
     * @return ObjectCollection|ChildImportligne[] List of ChildImportligne objects
     * @throws PropelException
     */
    public function getImportlignes(Criteria $criteria = null, ConnectionInterface $con = null)
    {
        $partial = $this->collImportlignesPartial && !$this->isNew();
        if (null === $this->collImportlignes || null !== $criteria  || $partial) {
            if ($this->isNew() && null === $this->collImportlignes) {
                // return empty collection
                $this->initImportlignes();
            } else {
                $collImportlignes = ChildImportligneQuery::create(null, $criteria)
                    ->filterByImport($this)
                    ->find($con);

                if (null !== $criteria) {
                    if (false !== $this->collImportlignesPartial && count($collImportlignes)) {
                        $this->initImportlignes(false);

                        foreach ($collImportlignes as $obj) {
                            if (false == $this->collImportlignes->contains($obj)) {
                                $this->collImportlignes->append($obj);
                            }
                        }

                        $this->collImportlignesPartial = true;
                    }

                    return $collImportlignes;
                }

                if ($partial && $this->collImportlignes) {
                    foreach ($this->collImportlignes as $obj) {
                        if ($obj->isNew()) {
                            $collImportlignes[] = $obj;
                        }
                    }
                }

                $this->collImportlignes = $collImportlignes;
                $this->collImportlignesPartial = false;
            }
        }

        return $this->collImportlignes;
    }

    /**
     * Sets a collection of ChildImportligne objects related by a one-to-many relationship
     * to the current object.
     * It will also schedule objects for deletion based on a diff between old objects (aka persisted)
     * and new objects from the given Propel collection.
     *
     * @param      Collection $importlignes A Propel collection.
     * @param      ConnectionInterface $con Optional connection object
     * @return $this|ChildImport The current object (for fluent API support)
     */
    public function setImportlignes(Collection $importlignes, ConnectionInterface $con = null)
    {
        /** @var ChildImportligne[] $importlignesToDelete */
        $importlignesToDelete = $this->getImportlignes(new Criteria(), $con)->diff($importlignes);


        $this->importlignesScheduledForDeletion = $importlignesToDelete;

        foreach ($importlignesToDelete as $importligneRemoved) {
            $importligneRemoved->setImport(null);
        }

        $this->collImportlignes = null;
        foreach ($importlignes as $importligne) {
            $this->addImportligne($importligne);
        }

        $this->collImportlignes = $importlignes;
        $this->collImportlignesPartial = false;

        return $this;
    }

    /**
     * Returns the number of related Importligne objects.
     *
     * @param      Criteria $criteria
     * @param      boolean $distinct
     * @param      ConnectionInterface $con
     * @return int             Count of related Importligne objects.
     * @throws PropelException
     */
    public function countImportlignes(Criteria $criteria = null, $distinct = false, ConnectionInterface $con = null)
    {
        $partial = $this->collImportlignesPartial && !$this->isNew();
        if (null === $this->collImportlignes || null !== $criteria || $partial) {
            if ($this->isNew() && null === $this->collImportlignes) {
                return 0;
            }

            if ($partial && !$criteria) {
                return count($this->getImportlignes());
            }

            $query = ChildImportligneQuery::create(null, $criteria);
            if ($distinct) {
                $query->distinct();
            }

            return $query
                ->filterByImport($this)
                ->count($con);
        }

        return count($this->collImportlignes);
    }

    /**
     * Method called to associate a ChildImportligne object to this object
     * through the ChildImportligne foreign key attribute.
     *
     * @param  ChildImportligne $l ChildImportligne
     * @return $this|\Import The current object (for fluent API support)
     */
    public function addImportligne(ChildImportligne $l)
    {
        if ($this->collImportlignes === null) {
            $this->initImportlignes();
            $this->collImportlignesPartial = true;
        }

        if (!$this->collImportlignes->contains($l)) {
            $this->doAddImportligne($l);

            if ($this->importlignesScheduledForDeletion and $this->importlignesScheduledForDeletion->contains($l)) {
                $this->importlignesScheduledForDeletion->remove($this->importlignesScheduledForDeletion->search($l));
            }
        }

        return $this;
    }

    /**
     * @param ChildImportligne $importligne The ChildImportligne object to add.
     */
    protected function doAddImportligne(ChildImportligne $importligne)
    {
        $this->collImportlignes[]= $importligne;
        $importligne->setImport($this);
    }

    /**
     * @param  ChildImportligne $importligne The ChildImportligne object to remove.
     * @return $this|ChildImport The current object (for fluent API support)
     */
    public function removeImportligne(ChildImportligne $importligne)
    {
        if ($this->getImportlignes()->contains($importligne)) {
            $pos = $this->collImportlignes->search($importligne);
            $this->collImportlignes->remove($pos);
            if (null === $this->importlignesScheduledForDeletion) {
                $this->importlignesScheduledForDeletion = clone $this->collImportlignes;
                $this->importlignesScheduledForDeletion->clear();
            }
            $this->importlignesScheduledForDeletion[]= clone $importligne;
            $importligne->setImport(null);
        }

        return $this;
    }

    /**
     * Clears the current object, sets all attributes to their default values and removes
     * outgoing references as well as back-references (from other objects to this one. Results probably in a database
     * change of those foreign objects when you call `save` there).
     */
    public function clear()
    {
        $this->id_import = null;
        $this->modele = null;
        $this->avancement = null;
        $this->nom = null;
        $this->nomprovisoire = null;
        $this->taille = null;
        $this->controlemd5 = null;
        $this->originals = null;
        $this->originals_isLoaded = false;
        $this->informations = null;
        $this->informations_isLoaded = false;
        $this->modifications = null;
        $this->modifications_isLoaded = false;
        $this->nb_ligne = null;
        $this->ligne_en_cours = null;
        $this->created_at = null;
        $this->updated_at = null;
        $this->alreadyInSave = false;
        $this->clearAllReferences();
        $this->applyDefaultValues();
        $this->resetModified();
        $this->setNew(true);
        $this->setDeleted(false);
    }

    /**
     * Resets all references and back-references to other model objects or collections of model objects.
     *
     * This method is used to reset all php object references (not the actual reference in the database).
     * Necessary for object serialisation.
     *
     * @param      boolean $deep Whether to also clear the references on all referrer objects.
     */
    public function clearAllReferences($deep = false)
    {
        if ($deep) {
            if ($this->collImportlignes) {
                foreach ($this->collImportlignes as $o) {
                    $o->clearAllReferences($deep);
                }
            }
        } // if ($deep)

        $this->collImportlignes = null;
    }

    /**
     * Return the string representation of this object
     *
     * @return string
     */
    public function __toString()
    {
        return (string) $this->exportTo(ImportTableMap::DEFAULT_STRING_FORMAT);
    }

    // timestampable behavior

    /**
     * Mark the current object so that the update date doesn't get updated during next save
     *
     * @return     $this|ChildImport The current object (for fluent API support)
     */
    public function keepUpdateDateUnchanged()
    {
        $this->modifiedColumns[ImportTableMap::COL_UPDATED_AT] = true;

        return $this;
    }

    // archivable behavior

    /**
     * Get an archived version of the current object.
     *
     * @param ConnectionInterface $con Optional connection object
     *
     * @return     ChildAssoImportsArchive An archive object, or null if the current object was never archived
     */
    public function getArchive(ConnectionInterface $con = null)
    {
        if ($this->isNew()) {
            return null;
        }
        $archive = ChildAssoImportsArchiveQuery::create()
            ->filterByPrimaryKey($this->getPrimaryKey())
            ->findOne($con);

        return $archive;
    }
    /**
     * Copy the data of the current object into a $archiveTablePhpName archive object.
     * The archived object is then saved.
     * If the current object has already been archived, the archived object
     * is updated and not duplicated.
     *
     * @param ConnectionInterface $con Optional connection object
     *
     * @throws PropelException If the object is new
     *
     * @return     ChildAssoImportsArchive The archive object based on this object
     */
    public function archive(ConnectionInterface $con = null)
    {
        if ($this->isNew()) {
            throw new PropelException('New objects cannot be archived. You must save the current object before calling archive().');
        }
        $archive = $this->getArchive($con);
        if (!$archive) {
            $archive = new ChildAssoImportsArchive();
            $archive->setPrimaryKey($this->getPrimaryKey());
        }
        $this->copyInto($archive, $deepCopy = false, $makeNew = false);
        $archive->setArchivedAt(time());
        $archive->save($con);

        return $archive;
    }

    /**
     * Revert the the current object to the state it had when it was last archived.
     * The object must be saved afterwards if the changes must persist.
     *
     * @param ConnectionInterface $con Optional connection object
     *
     * @throws PropelException If the object has no corresponding archive.
     *
     * @return $this|ChildImport The current object (for fluent API support)
     */
    public function restoreFromArchive(ConnectionInterface $con = null)
    {
        $archive = $this->getArchive($con);
        if (!$archive) {
            throw new PropelException('The current object has never been archived and cannot be restored');
        }
        $this->populateFromArchive($archive);

        return $this;
    }

    /**
     * Populates the the current object based on a $archiveTablePhpName archive object.
     *
     * @param      ChildAssoImportsArchive $archive An archived object based on the same class
      * @param      Boolean $populateAutoIncrementPrimaryKeys
     *               If true, autoincrement columns are copied from the archive object.
     *               If false, autoincrement columns are left intact.
      *
     * @return     ChildImport The current object (for fluent API support)
     */
    public function populateFromArchive($archive, $populateAutoIncrementPrimaryKeys = false) {
        if ($populateAutoIncrementPrimaryKeys) {
            $this->setIdImport($archive->getIdImport());
        }
        $this->setModele($archive->getModele());
        $this->setAvancement($archive->getAvancement());
        $this->setNom($archive->getNom());
        $this->setNomprovisoire($archive->getNomprovisoire());
        $this->setTaille($archive->getTaille());
        $this->setControlemd5($archive->getControlemd5());
        $this->setOriginals($archive->getOriginals());
        $this->setInformations($archive->getInformations());
        $this->setModifications($archive->getModifications());
        $this->setNbLigne($archive->getNbLigne());
        $this->setLigneEnCours($archive->getLigneEnCours());
        $this->setCreatedAt($archive->getCreatedAt());
        $this->setUpdatedAt($archive->getUpdatedAt());

        return $this;
    }

    /**
     * Removes the object from the database without archiving it.
     *
     * @param ConnectionInterface $con Optional connection object
     *
     * @return $this|ChildImport The current object (for fluent API support)
     */
    public function deleteWithoutArchive(ConnectionInterface $con = null)
    {
        $this->archiveOnDelete = false;

        return $this->delete($con);
    }

    /**
     * Code to be run before persisting the object
     * @param  ConnectionInterface $con
     * @return boolean
     */
    public function preSave(ConnectionInterface $con = null)
    {
        if (is_callable('parent::preSave')) {
            return parent::preSave($con);
        }
        return true;
    }

    /**
     * Code to be run after persisting the object
     * @param ConnectionInterface $con
     */
    public function postSave(ConnectionInterface $con = null)
    {
        if (is_callable('parent::postSave')) {
            parent::postSave($con);
        }
    }

    /**
     * Code to be run before inserting to database
     * @param  ConnectionInterface $con
     * @return boolean
     */
    public function preInsert(ConnectionInterface $con = null)
    {
        if (is_callable('parent::preInsert')) {
            return parent::preInsert($con);
        }
        return true;
    }

    /**
     * Code to be run after inserting to database
     * @param ConnectionInterface $con
     */
    public function postInsert(ConnectionInterface $con = null)
    {
        if (is_callable('parent::postInsert')) {
            parent::postInsert($con);
        }
    }

    /**
     * Code to be run before updating the object in database
     * @param  ConnectionInterface $con
     * @return boolean
     */
    public function preUpdate(ConnectionInterface $con = null)
    {
        if (is_callable('parent::preUpdate')) {
            return parent::preUpdate($con);
        }
        return true;
    }

    /**
     * Code to be run after updating the object in database
     * @param ConnectionInterface $con
     */
    public function postUpdate(ConnectionInterface $con = null)
    {
        if (is_callable('parent::postUpdate')) {
            parent::postUpdate($con);
        }
    }

    /**
     * Code to be run before deleting the object in database
     * @param  ConnectionInterface $con
     * @return boolean
     */
    public function preDelete(ConnectionInterface $con = null)
    {
        if (is_callable('parent::preDelete')) {
            return parent::preDelete($con);
        }
        return true;
    }

    /**
     * Code to be run after deleting the object in database
     * @param ConnectionInterface $con
     */
    public function postDelete(ConnectionInterface $con = null)
    {
        if (is_callable('parent::postDelete')) {
            parent::postDelete($con);
        }
    }


    /**
     * Derived method to catches calls to undefined methods.
     *
     * Provides magic import/export method support (fromXML()/toXML(), fromYAML()/toYAML(), etc.).
     * Allows to define default __call() behavior if you overwrite __call()
     *
     * @param string $name
     * @param mixed  $params
     *
     * @return array|string
     */
    public function __call($name, $params)
    {
        if (0 === strpos($name, 'get')) {
            $virtualColumn = substr($name, 3);
            if ($this->hasVirtualColumn($virtualColumn)) {
                return $this->getVirtualColumn($virtualColumn);
            }

            $virtualColumn = lcfirst($virtualColumn);
            if ($this->hasVirtualColumn($virtualColumn)) {
                return $this->getVirtualColumn($virtualColumn);
            }
        }

        if (0 === strpos($name, 'from')) {
            $format = substr($name, 4);

            return $this->importFrom($format, reset($params));
        }

        if (0 === strpos($name, 'to')) {
            $format = substr($name, 2);
            $includeLazyLoadColumns = isset($params[0]) ? $params[0] : true;

            return $this->exportTo($format, $includeLazyLoadColumns);
        }

        throw new BadMethodCallException(sprintf('Call to undefined method: %s.', $name));
    }

}
