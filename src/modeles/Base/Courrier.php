<?php

namespace Base;

use \Composition as ChildComposition;
use \CompositionQuery as ChildCompositionQuery;
use \Courrier as ChildCourrier;
use \CourrierLien as ChildCourrierLien;
use \CourrierLienQuery as ChildCourrierLienQuery;
use \CourrierQuery as ChildCourrierQuery;
use \Entite as ChildEntite;
use \EntiteQuery as ChildEntiteQuery;
use \DateTime;
use \Exception;
use \PDO;
use Map\CourrierLienTableMap;
use Map\CourrierTableMap;
use Propel\Runtime\Propel;
use Propel\Runtime\ActiveQuery\Criteria;
use Propel\Runtime\ActiveQuery\ModelCriteria;
use Propel\Runtime\ActiveRecord\ActiveRecordInterface;
use Propel\Runtime\Collection\Collection;
use Propel\Runtime\Collection\ObjectCollection;
use Propel\Runtime\Connection\ConnectionInterface;
use Propel\Runtime\Exception\BadMethodCallException;
use Propel\Runtime\Exception\LogicException;
use Propel\Runtime\Exception\PropelException;
use Propel\Runtime\Map\TableMap;
use Propel\Runtime\Parser\AbstractParser;
use Propel\Runtime\Util\PropelDateTime;

/**
 * Base class that represents a row from the 'com_courriers' table.
 *
 *
 *
 * @package    propel.generator..Base
 */
abstract class Courrier implements ActiveRecordInterface
{
    /**
     * TableMap class name
     */
    const TABLE_MAP = '\\Map\\CourrierTableMap';


    /**
     * attribute to determine if this object has previously been saved.
     * @var boolean
     */
    protected $new = true;

    /**
     * attribute to determine whether this object has been deleted.
     * @var boolean
     */
    protected $deleted = false;

    /**
     * The columns that have been modified in current object.
     * Tracking modified columns allows us to only update modified columns.
     * @var array
     */
    protected $modifiedColumns = array();

    /**
     * The (virtual) columns that are added at runtime
     * The formatters can add supplementary columns based on a resultset
     * @var array
     */
    protected $virtualColumns = array();

    /**
     * The value for the id_courrier field.
     *
     * @var        string
     */
    protected $id_courrier;

    /**
     * The value for the type field.
     *
     * @var        int
     */
    protected $type;

    /**
     * The value for the canal field.
     *
     * @var        string
     */
    protected $canal;

    /**
     * The value for the id_entite field.
     *
     * @var        string
     */
    protected $id_entite;

    /**
     * The value for the nom field.
     *
     * @var        string
     */
    protected $nom;

    /**
     * The value for the nomcourt field.
     *
     * @var        string
     */
    protected $nomcourt;

    /**
     * The value for the id_composition field.
     *
     * @var        int
     */
    protected $id_composition;

    /**
     * The value for the variables field.
     *
     * @var        string
     */
    protected $variables;

    /**
     * The value for the observation field.
     *
     * @var        string
     */
    protected $observation;

    /**
     * The value for the date_envoi field.
     *
     * @var        DateTime
     */
    protected $date_envoi;

    /**
     * The value for the statut field.
     *
     * Note: this column has a database default value of: 'redaction'
     * @var        string
     */
    protected $statut;

    /**
     * The value for the created_at field.
     *
     * @var        DateTime
     */
    protected $created_at;

    /**
     * The value for the updated_at field.
     *
     * @var        DateTime
     */
    protected $updated_at;

    /**
     * @var        ChildEntite
     */
    protected $aEntite;

    /**
     * @var        ChildComposition
     */
    protected $aComposition;

    /**
     * @var        ObjectCollection|ChildCourrierLien[] Collection to store aggregation of ChildCourrierLien objects.
     */
    protected $collCourrierLiens;
    protected $collCourrierLiensPartial;

    /**
     * Flag to prevent endless save loop, if this object is referenced
     * by another object which falls in this transaction.
     *
     * @var boolean
     */
    protected $alreadyInSave = false;

    /**
     * An array of objects scheduled for deletion.
     * @var ObjectCollection|ChildCourrierLien[]
     */
    protected $courrierLiensScheduledForDeletion = null;

    /**
     * Applies default values to this object.
     * This method should be called from the object's constructor (or
     * equivalent initialization method).
     * @see __construct()
     */
    public function applyDefaultValues()
    {
        $this->statut = 'redaction';
    }

    /**
     * Initializes internal state of Base\Courrier object.
     * @see applyDefaults()
     */
    public function __construct()
    {
        $this->applyDefaultValues();
    }

    /**
     * Returns whether the object has been modified.
     *
     * @return boolean True if the object has been modified.
     */
    public function isModified()
    {
        return !!$this->modifiedColumns;
    }

    /**
     * Has specified column been modified?
     *
     * @param  string  $col column fully qualified name (TableMap::TYPE_COLNAME), e.g. Book::AUTHOR_ID
     * @return boolean True if $col has been modified.
     */
    public function isColumnModified($col)
    {
        return $this->modifiedColumns && isset($this->modifiedColumns[$col]);
    }

    /**
     * Get the columns that have been modified in this object.
     * @return array A unique list of the modified column names for this object.
     */
    public function getModifiedColumns()
    {
        return $this->modifiedColumns ? array_keys($this->modifiedColumns) : [];
    }

    /**
     * Returns whether the object has ever been saved.  This will
     * be false, if the object was retrieved from storage or was created
     * and then saved.
     *
     * @return boolean true, if the object has never been persisted.
     */
    public function isNew()
    {
        return $this->new;
    }

    /**
     * Setter for the isNew attribute.  This method will be called
     * by Propel-generated children and objects.
     *
     * @param boolean $b the state of the object.
     */
    public function setNew($b)
    {
        $this->new = (boolean) $b;
    }

    /**
     * Whether this object has been deleted.
     * @return boolean The deleted state of this object.
     */
    public function isDeleted()
    {
        return $this->deleted;
    }

    /**
     * Specify whether this object has been deleted.
     * @param  boolean $b The deleted state of this object.
     * @return void
     */
    public function setDeleted($b)
    {
        $this->deleted = (boolean) $b;
    }

    /**
     * Sets the modified state for the object to be false.
     * @param  string $col If supplied, only the specified column is reset.
     * @return void
     */
    public function resetModified($col = null)
    {
        if (null !== $col) {
            if (isset($this->modifiedColumns[$col])) {
                unset($this->modifiedColumns[$col]);
            }
        } else {
            $this->modifiedColumns = array();
        }
    }

    /**
     * Compares this with another <code>Courrier</code> instance.  If
     * <code>obj</code> is an instance of <code>Courrier</code>, delegates to
     * <code>equals(Courrier)</code>.  Otherwise, returns <code>false</code>.
     *
     * @param  mixed   $obj The object to compare to.
     * @return boolean Whether equal to the object specified.
     */
    public function equals($obj)
    {
        if (!$obj instanceof static) {
            return false;
        }

        if ($this === $obj) {
            return true;
        }

        if (null === $this->getPrimaryKey() || null === $obj->getPrimaryKey()) {
            return false;
        }

        return $this->getPrimaryKey() === $obj->getPrimaryKey();
    }

    /**
     * Get the associative array of the virtual columns in this object
     *
     * @return array
     */
    public function getVirtualColumns()
    {
        return $this->virtualColumns;
    }

    /**
     * Checks the existence of a virtual column in this object
     *
     * @param  string  $name The virtual column name
     * @return boolean
     */
    public function hasVirtualColumn($name)
    {
        return array_key_exists($name, $this->virtualColumns);
    }

    /**
     * Get the value of a virtual column in this object
     *
     * @param  string $name The virtual column name
     * @return mixed
     *
     * @throws PropelException
     */
    public function getVirtualColumn($name)
    {
        if (!$this->hasVirtualColumn($name)) {
            throw new PropelException(sprintf('Cannot get value of inexistent virtual column %s.', $name));
        }

        return $this->virtualColumns[$name];
    }

    /**
     * Set the value of a virtual column in this object
     *
     * @param string $name  The virtual column name
     * @param mixed  $value The value to give to the virtual column
     *
     * @return $this|Courrier The current object, for fluid interface
     */
    public function setVirtualColumn($name, $value)
    {
        $this->virtualColumns[$name] = $value;

        return $this;
    }

    /**
     * Logs a message using Propel::log().
     *
     * @param  string  $msg
     * @param  int     $priority One of the Propel::LOG_* logging levels
     * @return boolean
     */
    protected function log($msg, $priority = Propel::LOG_INFO)
    {
        return Propel::log(get_class($this) . ': ' . $msg, $priority);
    }

    /**
     * Export the current object properties to a string, using a given parser format
     * <code>
     * $book = BookQuery::create()->findPk(9012);
     * echo $book->exportTo('JSON');
     *  => {"Id":9012,"Title":"Don Juan","ISBN":"0140422161","Price":12.99,"PublisherId":1234,"AuthorId":5678}');
     * </code>
     *
     * @param  mixed   $parser                 A AbstractParser instance, or a format name ('XML', 'YAML', 'JSON', 'CSV')
     * @param  boolean $includeLazyLoadColumns (optional) Whether to include lazy load(ed) columns. Defaults to TRUE.
     * @return string  The exported data
     */
    public function exportTo($parser, $includeLazyLoadColumns = true)
    {
        if (!$parser instanceof AbstractParser) {
            $parser = AbstractParser::getParser($parser);
        }

        return $parser->fromArray($this->toArray(TableMap::TYPE_PHPNAME, $includeLazyLoadColumns, array(), true));
    }

    /**
     * Clean up internal collections prior to serializing
     * Avoids recursive loops that turn into segmentation faults when serializing
     */
    public function __sleep()
    {
        $this->clearAllReferences();

        $cls = new \ReflectionClass($this);
        $propertyNames = [];
        $serializableProperties = array_diff($cls->getProperties(), $cls->getProperties(\ReflectionProperty::IS_STATIC));

        foreach($serializableProperties as $property) {
            $propertyNames[] = $property->getName();
        }

        return $propertyNames;
    }

    /**
     * Get the [id_courrier] column value.
     *
     * @return string
     */
    public function getIdCourrier()
    {
        return $this->id_courrier;
    }

    /**
     * Get the [type] column value.
     *
     * @return int
     */
    public function getType()
    {
        return $this->type;
    }

    /**
     * Get the [canal] column value.
     *
     * @return string
     */
    public function getCanal()
    {
        return $this->canal;
    }

    /**
     * Get the [id_entite] column value.
     *
     * @return string
     */
    public function getIdEntite()
    {
        return $this->id_entite;
    }

    /**
     * Get the [nom] column value.
     *
     * @return string
     */
    public function getNom()
    {
        return $this->nom;
    }

    /**
     * Get the [nomcourt] column value.
     *
     * @return string
     */
    public function getNomcourt()
    {
        return $this->nomcourt;
    }

    /**
     * Get the [id_composition] column value.
     *
     * @return int
     */
    public function getIdComposition()
    {
        return $this->id_composition;
    }

    /**
     * Get the [variables] column value.
     *
     * @return string
     */
    public function getVariables()
    {
        return $this->variables;
    }

    /**
     * Get the [observation] column value.
     *
     * @return string
     */
    public function getObservation()
    {
        return $this->observation;
    }

    /**
     * Get the [optionally formatted] temporal [date_envoi] column value.
     *
     *
     * @param      string $format The date/time format string (either date()-style or strftime()-style).
     *                            If format is NULL, then the raw DateTime object will be returned.
     *
     * @return string|DateTime Formatted date/time value as string or DateTime object (if format is NULL), NULL if column is NULL, and 0 if column value is 0000-00-00 00:00:00
     *
     * @throws PropelException - if unable to parse/validate the date/time value.
     */
    public function getDateEnvoi($format = NULL)
    {
        if ($format === null) {
            return $this->date_envoi;
        } else {
            return $this->date_envoi instanceof \DateTimeInterface ? $this->date_envoi->format($format) : null;
        }
    }

    /**
     * Get the [statut] column value.
     *
     * @return string
     */
    public function getStatut()
    {
        return $this->statut;
    }

    /**
     * Get the [optionally formatted] temporal [created_at] column value.
     *
     *
     * @param      string $format The date/time format string (either date()-style or strftime()-style).
     *                            If format is NULL, then the raw DateTime object will be returned.
     *
     * @return string|DateTime Formatted date/time value as string or DateTime object (if format is NULL), NULL if column is NULL, and 0 if column value is 0000-00-00 00:00:00
     *
     * @throws PropelException - if unable to parse/validate the date/time value.
     */
    public function getCreatedAt($format = NULL)
    {
        if ($format === null) {
            return $this->created_at;
        } else {
            return $this->created_at instanceof \DateTimeInterface ? $this->created_at->format($format) : null;
        }
    }

    /**
     * Get the [optionally formatted] temporal [updated_at] column value.
     *
     *
     * @param      string $format The date/time format string (either date()-style or strftime()-style).
     *                            If format is NULL, then the raw DateTime object will be returned.
     *
     * @return string|DateTime Formatted date/time value as string or DateTime object (if format is NULL), NULL if column is NULL, and 0 if column value is 0000-00-00 00:00:00
     *
     * @throws PropelException - if unable to parse/validate the date/time value.
     */
    public function getUpdatedAt($format = NULL)
    {
        if ($format === null) {
            return $this->updated_at;
        } else {
            return $this->updated_at instanceof \DateTimeInterface ? $this->updated_at->format($format) : null;
        }
    }

    /**
     * Set the value of [id_courrier] column.
     *
     * @param string $v new value
     * @return $this|\Courrier The current object (for fluent API support)
     */
    public function setIdCourrier($v)
    {
        if ($v !== null) {
            $v = (string) $v;
        }

        if ($this->id_courrier !== $v) {
            $this->id_courrier = $v;
            $this->modifiedColumns[CourrierTableMap::COL_ID_COURRIER] = true;
        }

        return $this;
    } // setIdCourrier()

    /**
     * Set the value of [type] column.
     *
     * @param int $v new value
     * @return $this|\Courrier The current object (for fluent API support)
     */
    public function setType($v)
    {
        if ($v !== null) {
            $v = (int) $v;
        }

        if ($this->type !== $v) {
            $this->type = $v;
            $this->modifiedColumns[CourrierTableMap::COL_TYPE] = true;
        }

        return $this;
    } // setType()

    /**
     * Set the value of [canal] column.
     *
     * @param string $v new value
     * @return $this|\Courrier The current object (for fluent API support)
     */
    public function setCanal($v)
    {
        if ($v !== null) {
            $v = (string) $v;
        }

        if ($this->canal !== $v) {
            $this->canal = $v;
            $this->modifiedColumns[CourrierTableMap::COL_CANAL] = true;
        }

        return $this;
    } // setCanal()

    /**
     * Set the value of [id_entite] column.
     *
     * @param string $v new value
     * @return $this|\Courrier The current object (for fluent API support)
     */
    public function setIdEntite($v)
    {
        if ($v !== null) {
            $v = (string) $v;
        }

        if ($this->id_entite !== $v) {
            $this->id_entite = $v;
            $this->modifiedColumns[CourrierTableMap::COL_ID_ENTITE] = true;
        }

        if ($this->aEntite !== null && $this->aEntite->getIdEntite() !== $v) {
            $this->aEntite = null;
        }

        return $this;
    } // setIdEntite()

    /**
     * Set the value of [nom] column.
     *
     * @param string $v new value
     * @return $this|\Courrier The current object (for fluent API support)
     */
    public function setNom($v)
    {
        if ($v !== null) {
            $v = (string) $v;
        }

        if ($this->nom !== $v) {
            $this->nom = $v;
            $this->modifiedColumns[CourrierTableMap::COL_NOM] = true;
        }

        return $this;
    } // setNom()

    /**
     * Set the value of [nomcourt] column.
     *
     * @param string $v new value
     * @return $this|\Courrier The current object (for fluent API support)
     */
    public function setNomcourt($v)
    {
        if ($v !== null) {
            $v = (string) $v;
        }

        if ($this->nomcourt !== $v) {
            $this->nomcourt = $v;
            $this->modifiedColumns[CourrierTableMap::COL_NOMCOURT] = true;
        }

        return $this;
    } // setNomcourt()

    /**
     * Set the value of [id_composition] column.
     *
     * @param int $v new value
     * @return $this|\Courrier The current object (for fluent API support)
     */
    public function setIdComposition($v)
    {
        if ($v !== null) {
            $v = (int) $v;
        }

        if ($this->id_composition !== $v) {
            $this->id_composition = $v;
            $this->modifiedColumns[CourrierTableMap::COL_ID_COMPOSITION] = true;
        }

        if ($this->aComposition !== null && $this->aComposition->getIdComposition() !== $v) {
            $this->aComposition = null;
        }

        return $this;
    } // setIdComposition()

    /**
     * Set the value of [variables] column.
     *
     * @param string $v new value
     * @return $this|\Courrier The current object (for fluent API support)
     */
    public function setVariables($v)
    {
        if ($v !== null) {
            $v = (string) $v;
        }

        if ($this->variables !== $v) {
            $this->variables = $v;
            $this->modifiedColumns[CourrierTableMap::COL_VARIABLES] = true;
        }

        return $this;
    } // setVariables()

    /**
     * Set the value of [observation] column.
     *
     * @param string $v new value
     * @return $this|\Courrier The current object (for fluent API support)
     */
    public function setObservation($v)
    {
        if ($v !== null) {
            $v = (string) $v;
        }

        if ($this->observation !== $v) {
            $this->observation = $v;
            $this->modifiedColumns[CourrierTableMap::COL_OBSERVATION] = true;
        }

        return $this;
    } // setObservation()

    /**
     * Sets the value of [date_envoi] column to a normalized version of the date/time value specified.
     *
     * @param  mixed $v string, integer (timestamp), or \DateTimeInterface value.
     *               Empty strings are treated as NULL.
     * @return $this|\Courrier The current object (for fluent API support)
     */
    public function setDateEnvoi($v)
    {
        $dt = PropelDateTime::newInstance($v, null, 'DateTime');
        if ($this->date_envoi !== null || $dt !== null) {
            if ($this->date_envoi === null || $dt === null || $dt->format("Y-m-d H:i:s.u") !== $this->date_envoi->format("Y-m-d H:i:s.u")) {
                $this->date_envoi = $dt === null ? null : clone $dt;
                $this->modifiedColumns[CourrierTableMap::COL_DATE_ENVOI] = true;
            }
        } // if either are not null

        return $this;
    } // setDateEnvoi()

    /**
     * Set the value of [statut] column.
     *
     * @param string $v new value
     * @return $this|\Courrier The current object (for fluent API support)
     */
    public function setStatut($v)
    {
        if ($v !== null) {
            $v = (string) $v;
        }

        if ($this->statut !== $v) {
            $this->statut = $v;
            $this->modifiedColumns[CourrierTableMap::COL_STATUT] = true;
        }

        return $this;
    } // setStatut()

    /**
     * Sets the value of [created_at] column to a normalized version of the date/time value specified.
     *
     * @param  mixed $v string, integer (timestamp), or \DateTimeInterface value.
     *               Empty strings are treated as NULL.
     * @return $this|\Courrier The current object (for fluent API support)
     */
    public function setCreatedAt($v)
    {
        $dt = PropelDateTime::newInstance($v, null, 'DateTime');
        if ($this->created_at !== null || $dt !== null) {
            if ($this->created_at === null || $dt === null || $dt->format("Y-m-d H:i:s.u") !== $this->created_at->format("Y-m-d H:i:s.u")) {
                $this->created_at = $dt === null ? null : clone $dt;
                $this->modifiedColumns[CourrierTableMap::COL_CREATED_AT] = true;
            }
        } // if either are not null

        return $this;
    } // setCreatedAt()

    /**
     * Sets the value of [updated_at] column to a normalized version of the date/time value specified.
     *
     * @param  mixed $v string, integer (timestamp), or \DateTimeInterface value.
     *               Empty strings are treated as NULL.
     * @return $this|\Courrier The current object (for fluent API support)
     */
    public function setUpdatedAt($v)
    {
        $dt = PropelDateTime::newInstance($v, null, 'DateTime');
        if ($this->updated_at !== null || $dt !== null) {
            if ($this->updated_at === null || $dt === null || $dt->format("Y-m-d H:i:s.u") !== $this->updated_at->format("Y-m-d H:i:s.u")) {
                $this->updated_at = $dt === null ? null : clone $dt;
                $this->modifiedColumns[CourrierTableMap::COL_UPDATED_AT] = true;
            }
        } // if either are not null

        return $this;
    } // setUpdatedAt()

    /**
     * Indicates whether the columns in this object are only set to default values.
     *
     * This method can be used in conjunction with isModified() to indicate whether an object is both
     * modified _and_ has some values set which are non-default.
     *
     * @return boolean Whether the columns in this object are only been set with default values.
     */
    public function hasOnlyDefaultValues()
    {
            if ($this->statut !== 'redaction') {
                return false;
            }

        // otherwise, everything was equal, so return TRUE
        return true;
    } // hasOnlyDefaultValues()

    /**
     * Hydrates (populates) the object variables with values from the database resultset.
     *
     * An offset (0-based "start column") is specified so that objects can be hydrated
     * with a subset of the columns in the resultset rows.  This is needed, for example,
     * for results of JOIN queries where the resultset row includes columns from two or
     * more tables.
     *
     * @param array   $row       The row returned by DataFetcher->fetch().
     * @param int     $startcol  0-based offset column which indicates which restultset column to start with.
     * @param boolean $rehydrate Whether this object is being re-hydrated from the database.
     * @param string  $indexType The index type of $row. Mostly DataFetcher->getIndexType().
                                  One of the class type constants TableMap::TYPE_PHPNAME, TableMap::TYPE_CAMELNAME
     *                            TableMap::TYPE_COLNAME, TableMap::TYPE_FIELDNAME, TableMap::TYPE_NUM.
     *
     * @return int             next starting column
     * @throws PropelException - Any caught Exception will be rewrapped as a PropelException.
     */
    public function hydrate($row, $startcol = 0, $rehydrate = false, $indexType = TableMap::TYPE_NUM)
    {
        try {

            $col = $row[TableMap::TYPE_NUM == $indexType ? 0 + $startcol : CourrierTableMap::translateFieldName('IdCourrier', TableMap::TYPE_PHPNAME, $indexType)];
            $this->id_courrier = (null !== $col) ? (string) $col : null;

            $col = $row[TableMap::TYPE_NUM == $indexType ? 1 + $startcol : CourrierTableMap::translateFieldName('Type', TableMap::TYPE_PHPNAME, $indexType)];
            $this->type = (null !== $col) ? (int) $col : null;

            $col = $row[TableMap::TYPE_NUM == $indexType ? 2 + $startcol : CourrierTableMap::translateFieldName('Canal', TableMap::TYPE_PHPNAME, $indexType)];
            $this->canal = (null !== $col) ? (string) $col : null;

            $col = $row[TableMap::TYPE_NUM == $indexType ? 3 + $startcol : CourrierTableMap::translateFieldName('IdEntite', TableMap::TYPE_PHPNAME, $indexType)];
            $this->id_entite = (null !== $col) ? (string) $col : null;

            $col = $row[TableMap::TYPE_NUM == $indexType ? 4 + $startcol : CourrierTableMap::translateFieldName('Nom', TableMap::TYPE_PHPNAME, $indexType)];
            $this->nom = (null !== $col) ? (string) $col : null;

            $col = $row[TableMap::TYPE_NUM == $indexType ? 5 + $startcol : CourrierTableMap::translateFieldName('Nomcourt', TableMap::TYPE_PHPNAME, $indexType)];
            $this->nomcourt = (null !== $col) ? (string) $col : null;

            $col = $row[TableMap::TYPE_NUM == $indexType ? 6 + $startcol : CourrierTableMap::translateFieldName('IdComposition', TableMap::TYPE_PHPNAME, $indexType)];
            $this->id_composition = (null !== $col) ? (int) $col : null;

            $col = $row[TableMap::TYPE_NUM == $indexType ? 7 + $startcol : CourrierTableMap::translateFieldName('Variables', TableMap::TYPE_PHPNAME, $indexType)];
            $this->variables = (null !== $col) ? (string) $col : null;

            $col = $row[TableMap::TYPE_NUM == $indexType ? 8 + $startcol : CourrierTableMap::translateFieldName('Observation', TableMap::TYPE_PHPNAME, $indexType)];
            $this->observation = (null !== $col) ? (string) $col : null;

            $col = $row[TableMap::TYPE_NUM == $indexType ? 9 + $startcol : CourrierTableMap::translateFieldName('DateEnvoi', TableMap::TYPE_PHPNAME, $indexType)];
            if ($col === '0000-00-00 00:00:00') {
                $col = null;
            }
            $this->date_envoi = (null !== $col) ? PropelDateTime::newInstance($col, null, 'DateTime') : null;

            $col = $row[TableMap::TYPE_NUM == $indexType ? 10 + $startcol : CourrierTableMap::translateFieldName('Statut', TableMap::TYPE_PHPNAME, $indexType)];
            $this->statut = (null !== $col) ? (string) $col : null;

            $col = $row[TableMap::TYPE_NUM == $indexType ? 11 + $startcol : CourrierTableMap::translateFieldName('CreatedAt', TableMap::TYPE_PHPNAME, $indexType)];
            if ($col === '0000-00-00 00:00:00') {
                $col = null;
            }
            $this->created_at = (null !== $col) ? PropelDateTime::newInstance($col, null, 'DateTime') : null;

            $col = $row[TableMap::TYPE_NUM == $indexType ? 12 + $startcol : CourrierTableMap::translateFieldName('UpdatedAt', TableMap::TYPE_PHPNAME, $indexType)];
            if ($col === '0000-00-00 00:00:00') {
                $col = null;
            }
            $this->updated_at = (null !== $col) ? PropelDateTime::newInstance($col, null, 'DateTime') : null;
            $this->resetModified();

            $this->setNew(false);

            if ($rehydrate) {
                $this->ensureConsistency();
            }

            return $startcol + 13; // 13 = CourrierTableMap::NUM_HYDRATE_COLUMNS.

        } catch (Exception $e) {
            throw new PropelException(sprintf('Error populating %s object', '\\Courrier'), 0, $e);
        }
    }

    /**
     * Checks and repairs the internal consistency of the object.
     *
     * This method is executed after an already-instantiated object is re-hydrated
     * from the database.  It exists to check any foreign keys to make sure that
     * the objects related to the current object are correct based on foreign key.
     *
     * You can override this method in the stub class, but you should always invoke
     * the base method from the overridden method (i.e. parent::ensureConsistency()),
     * in case your model changes.
     *
     * @throws PropelException
     */
    public function ensureConsistency()
    {
        if ($this->aEntite !== null && $this->id_entite !== $this->aEntite->getIdEntite()) {
            $this->aEntite = null;
        }
        if ($this->aComposition !== null && $this->id_composition !== $this->aComposition->getIdComposition()) {
            $this->aComposition = null;
        }
    } // ensureConsistency

    /**
     * Reloads this object from datastore based on primary key and (optionally) resets all associated objects.
     *
     * This will only work if the object has been saved and has a valid primary key set.
     *
     * @param      boolean $deep (optional) Whether to also de-associated any related objects.
     * @param      ConnectionInterface $con (optional) The ConnectionInterface connection to use.
     * @return void
     * @throws PropelException - if this object is deleted, unsaved or doesn't have pk match in db
     */
    public function reload($deep = false, ConnectionInterface $con = null)
    {
        if ($this->isDeleted()) {
            throw new PropelException("Cannot reload a deleted object.");
        }

        if ($this->isNew()) {
            throw new PropelException("Cannot reload an unsaved object.");
        }

        if ($con === null) {
            $con = Propel::getServiceContainer()->getReadConnection(CourrierTableMap::DATABASE_NAME);
        }

        // We don't need to alter the object instance pool; we're just modifying this instance
        // already in the pool.

        $dataFetcher = ChildCourrierQuery::create(null, $this->buildPkeyCriteria())->setFormatter(ModelCriteria::FORMAT_STATEMENT)->find($con);
        $row = $dataFetcher->fetch();
        $dataFetcher->close();
        if (!$row) {
            throw new PropelException('Cannot find matching row in the database to reload object values.');
        }
        $this->hydrate($row, 0, true, $dataFetcher->getIndexType()); // rehydrate

        if ($deep) {  // also de-associate any related objects?

            $this->aEntite = null;
            $this->aComposition = null;
            $this->collCourrierLiens = null;

        } // if (deep)
    }

    /**
     * Removes this object from datastore and sets delete attribute.
     *
     * @param      ConnectionInterface $con
     * @return void
     * @throws PropelException
     * @see Courrier::setDeleted()
     * @see Courrier::isDeleted()
     */
    public function delete(ConnectionInterface $con = null)
    {
        if ($this->isDeleted()) {
            throw new PropelException("This object has already been deleted.");
        }

        if ($con === null) {
            $con = Propel::getServiceContainer()->getWriteConnection(CourrierTableMap::DATABASE_NAME);
        }

        $con->transaction(function () use ($con) {
            $deleteQuery = ChildCourrierQuery::create()
                ->filterByPrimaryKey($this->getPrimaryKey());
            $ret = $this->preDelete($con);
            if ($ret) {
                $deleteQuery->delete($con);
                $this->postDelete($con);
                $this->setDeleted(true);
            }
        });
    }

    /**
     * Persists this object to the database.
     *
     * If the object is new, it inserts it; otherwise an update is performed.
     * All modified related objects will also be persisted in the doSave()
     * method.  This method wraps all precipitate database operations in a
     * single transaction.
     *
     * @param      ConnectionInterface $con
     * @return int             The number of rows affected by this insert/update and any referring fk objects' save() operations.
     * @throws PropelException
     * @see doSave()
     */
    public function save(ConnectionInterface $con = null)
    {
        if ($this->isDeleted()) {
            throw new PropelException("You cannot save an object that has been deleted.");
        }

        if ($this->alreadyInSave) {
            return 0;
        }

        if ($con === null) {
            $con = Propel::getServiceContainer()->getWriteConnection(CourrierTableMap::DATABASE_NAME);
        }

        return $con->transaction(function () use ($con) {
            $ret = $this->preSave($con);
            $isInsert = $this->isNew();
            if ($isInsert) {
                $ret = $ret && $this->preInsert($con);
                // timestampable behavior

                if (!$this->isColumnModified(CourrierTableMap::COL_CREATED_AT)) {
                    $this->setCreatedAt(\Propel\Runtime\Util\PropelDateTime::createHighPrecision());
                }
                if (!$this->isColumnModified(CourrierTableMap::COL_UPDATED_AT)) {
                    $this->setUpdatedAt(\Propel\Runtime\Util\PropelDateTime::createHighPrecision());
                }
            } else {
                $ret = $ret && $this->preUpdate($con);
                // timestampable behavior
                if ($this->isModified() && !$this->isColumnModified(CourrierTableMap::COL_UPDATED_AT)) {
                    $this->setUpdatedAt(\Propel\Runtime\Util\PropelDateTime::createHighPrecision());
                }
            }
            if ($ret) {
                $affectedRows = $this->doSave($con);
                if ($isInsert) {
                    $this->postInsert($con);
                } else {
                    $this->postUpdate($con);
                }
                $this->postSave($con);
                CourrierTableMap::addInstanceToPool($this);
            } else {
                $affectedRows = 0;
            }

            return $affectedRows;
        });
    }

    /**
     * Performs the work of inserting or updating the row in the database.
     *
     * If the object is new, it inserts it; otherwise an update is performed.
     * All related objects are also updated in this method.
     *
     * @param      ConnectionInterface $con
     * @return int             The number of rows affected by this insert/update and any referring fk objects' save() operations.
     * @throws PropelException
     * @see save()
     */
    protected function doSave(ConnectionInterface $con)
    {
        $affectedRows = 0; // initialize var to track total num of affected rows
        if (!$this->alreadyInSave) {
            $this->alreadyInSave = true;

            // We call the save method on the following object(s) if they
            // were passed to this object by their corresponding set
            // method.  This object relates to these object(s) by a
            // foreign key reference.

            if ($this->aEntite !== null) {
                if ($this->aEntite->isModified() || $this->aEntite->isNew()) {
                    $affectedRows += $this->aEntite->save($con);
                }
                $this->setEntite($this->aEntite);
            }

            if ($this->aComposition !== null) {
                if ($this->aComposition->isModified() || $this->aComposition->isNew()) {
                    $affectedRows += $this->aComposition->save($con);
                }
                $this->setComposition($this->aComposition);
            }

            if ($this->isNew() || $this->isModified()) {
                // persist changes
                if ($this->isNew()) {
                    $this->doInsert($con);
                    $affectedRows += 1;
                } else {
                    $affectedRows += $this->doUpdate($con);
                }
                $this->resetModified();
            }

            if ($this->courrierLiensScheduledForDeletion !== null) {
                if (!$this->courrierLiensScheduledForDeletion->isEmpty()) {
                    \CourrierLienQuery::create()
                        ->filterByPrimaryKeys($this->courrierLiensScheduledForDeletion->getPrimaryKeys(false))
                        ->delete($con);
                    $this->courrierLiensScheduledForDeletion = null;
                }
            }

            if ($this->collCourrierLiens !== null) {
                foreach ($this->collCourrierLiens as $referrerFK) {
                    if (!$referrerFK->isDeleted() && ($referrerFK->isNew() || $referrerFK->isModified())) {
                        $affectedRows += $referrerFK->save($con);
                    }
                }
            }

            $this->alreadyInSave = false;

        }

        return $affectedRows;
    } // doSave()

    /**
     * Insert the row in the database.
     *
     * @param      ConnectionInterface $con
     *
     * @throws PropelException
     * @see doSave()
     */
    protected function doInsert(ConnectionInterface $con)
    {
        $modifiedColumns = array();
        $index = 0;

        $this->modifiedColumns[CourrierTableMap::COL_ID_COURRIER] = true;
        if (null !== $this->id_courrier) {
            throw new PropelException('Cannot insert a value for auto-increment primary key (' . CourrierTableMap::COL_ID_COURRIER . ')');
        }

         // check the columns in natural order for more readable SQL queries
        if ($this->isColumnModified(CourrierTableMap::COL_ID_COURRIER)) {
            $modifiedColumns[':p' . $index++]  = '`id_courrier`';
        }
        if ($this->isColumnModified(CourrierTableMap::COL_TYPE)) {
            $modifiedColumns[':p' . $index++]  = '`type`';
        }
        if ($this->isColumnModified(CourrierTableMap::COL_CANAL)) {
            $modifiedColumns[':p' . $index++]  = '`canal`';
        }
        if ($this->isColumnModified(CourrierTableMap::COL_ID_ENTITE)) {
            $modifiedColumns[':p' . $index++]  = '`id_entite`';
        }
        if ($this->isColumnModified(CourrierTableMap::COL_NOM)) {
            $modifiedColumns[':p' . $index++]  = '`nom`';
        }
        if ($this->isColumnModified(CourrierTableMap::COL_NOMCOURT)) {
            $modifiedColumns[':p' . $index++]  = '`nomcourt`';
        }
        if ($this->isColumnModified(CourrierTableMap::COL_ID_COMPOSITION)) {
            $modifiedColumns[':p' . $index++]  = '`id_composition`';
        }
        if ($this->isColumnModified(CourrierTableMap::COL_VARIABLES)) {
            $modifiedColumns[':p' . $index++]  = '`variables`';
        }
        if ($this->isColumnModified(CourrierTableMap::COL_OBSERVATION)) {
            $modifiedColumns[':p' . $index++]  = '`observation`';
        }
        if ($this->isColumnModified(CourrierTableMap::COL_DATE_ENVOI)) {
            $modifiedColumns[':p' . $index++]  = '`date_envoi`';
        }
        if ($this->isColumnModified(CourrierTableMap::COL_STATUT)) {
            $modifiedColumns[':p' . $index++]  = '`statut`';
        }
        if ($this->isColumnModified(CourrierTableMap::COL_CREATED_AT)) {
            $modifiedColumns[':p' . $index++]  = '`created_at`';
        }
        if ($this->isColumnModified(CourrierTableMap::COL_UPDATED_AT)) {
            $modifiedColumns[':p' . $index++]  = '`updated_at`';
        }

        $sql = sprintf(
            'INSERT INTO `com_courriers` (%s) VALUES (%s)',
            implode(', ', $modifiedColumns),
            implode(', ', array_keys($modifiedColumns))
        );

        try {
            $stmt = $con->prepare($sql);
            foreach ($modifiedColumns as $identifier => $columnName) {
                switch ($columnName) {
                    case '`id_courrier`':
                        $stmt->bindValue($identifier, $this->id_courrier, PDO::PARAM_INT);
                        break;
                    case '`type`':
                        $stmt->bindValue($identifier, $this->type, PDO::PARAM_INT);
                        break;
                    case '`canal`':
                        $stmt->bindValue($identifier, $this->canal, PDO::PARAM_STR);
                        break;
                    case '`id_entite`':
                        $stmt->bindValue($identifier, $this->id_entite, PDO::PARAM_INT);
                        break;
                    case '`nom`':
                        $stmt->bindValue($identifier, $this->nom, PDO::PARAM_STR);
                        break;
                    case '`nomcourt`':
                        $stmt->bindValue($identifier, $this->nomcourt, PDO::PARAM_STR);
                        break;
                    case '`id_composition`':
                        $stmt->bindValue($identifier, $this->id_composition, PDO::PARAM_INT);
                        break;
                    case '`variables`':
                        $stmt->bindValue($identifier, $this->variables, PDO::PARAM_STR);
                        break;
                    case '`observation`':
                        $stmt->bindValue($identifier, $this->observation, PDO::PARAM_STR);
                        break;
                    case '`date_envoi`':
                        $stmt->bindValue($identifier, $this->date_envoi ? $this->date_envoi->format("Y-m-d H:i:s.u") : null, PDO::PARAM_STR);
                        break;
                    case '`statut`':
                        $stmt->bindValue($identifier, $this->statut, PDO::PARAM_STR);
                        break;
                    case '`created_at`':
                        $stmt->bindValue($identifier, $this->created_at ? $this->created_at->format("Y-m-d H:i:s.u") : null, PDO::PARAM_STR);
                        break;
                    case '`updated_at`':
                        $stmt->bindValue($identifier, $this->updated_at ? $this->updated_at->format("Y-m-d H:i:s.u") : null, PDO::PARAM_STR);
                        break;
                }
            }
            $stmt->execute();
        } catch (Exception $e) {
            Propel::log($e->getMessage(), Propel::LOG_ERR);
            throw new PropelException(sprintf('Unable to execute INSERT statement [%s]', $sql), 0, $e);
        }

        try {
            $pk = $con->lastInsertId();
        } catch (Exception $e) {
            throw new PropelException('Unable to get autoincrement id.', 0, $e);
        }
        $this->setIdCourrier($pk);

        $this->setNew(false);
    }

    /**
     * Update the row in the database.
     *
     * @param      ConnectionInterface $con
     *
     * @return Integer Number of updated rows
     * @see doSave()
     */
    protected function doUpdate(ConnectionInterface $con)
    {
        $selectCriteria = $this->buildPkeyCriteria();
        $valuesCriteria = $this->buildCriteria();

        return $selectCriteria->doUpdate($valuesCriteria, $con);
    }

    /**
     * Retrieves a field from the object by name passed in as a string.
     *
     * @param      string $name name
     * @param      string $type The type of fieldname the $name is of:
     *                     one of the class type constants TableMap::TYPE_PHPNAME, TableMap::TYPE_CAMELNAME
     *                     TableMap::TYPE_COLNAME, TableMap::TYPE_FIELDNAME, TableMap::TYPE_NUM.
     *                     Defaults to TableMap::TYPE_FIELDNAME.
     * @return mixed Value of field.
     */
    public function getByName($name, $type = TableMap::TYPE_FIELDNAME)
    {
        $pos = CourrierTableMap::translateFieldName($name, $type, TableMap::TYPE_NUM);
        $field = $this->getByPosition($pos);

        return $field;
    }

    /**
     * Retrieves a field from the object by Position as specified in the xml schema.
     * Zero-based.
     *
     * @param      int $pos position in xml schema
     * @return mixed Value of field at $pos
     */
    public function getByPosition($pos)
    {
        switch ($pos) {
            case 0:
                return $this->getIdCourrier();
                break;
            case 1:
                return $this->getType();
                break;
            case 2:
                return $this->getCanal();
                break;
            case 3:
                return $this->getIdEntite();
                break;
            case 4:
                return $this->getNom();
                break;
            case 5:
                return $this->getNomcourt();
                break;
            case 6:
                return $this->getIdComposition();
                break;
            case 7:
                return $this->getVariables();
                break;
            case 8:
                return $this->getObservation();
                break;
            case 9:
                return $this->getDateEnvoi();
                break;
            case 10:
                return $this->getStatut();
                break;
            case 11:
                return $this->getCreatedAt();
                break;
            case 12:
                return $this->getUpdatedAt();
                break;
            default:
                return null;
                break;
        } // switch()
    }

    /**
     * Exports the object as an array.
     *
     * You can specify the key type of the array by passing one of the class
     * type constants.
     *
     * @param     string  $keyType (optional) One of the class type constants TableMap::TYPE_PHPNAME, TableMap::TYPE_CAMELNAME,
     *                    TableMap::TYPE_COLNAME, TableMap::TYPE_FIELDNAME, TableMap::TYPE_NUM.
     *                    Defaults to TableMap::TYPE_FIELDNAME.
     * @param     boolean $includeLazyLoadColumns (optional) Whether to include lazy loaded columns. Defaults to TRUE.
     * @param     array $alreadyDumpedObjects List of objects to skip to avoid recursion
     * @param     boolean $includeForeignObjects (optional) Whether to include hydrated related objects. Default to FALSE.
     *
     * @return array an associative array containing the field names (as keys) and field values
     */
    public function toArray($keyType = TableMap::TYPE_FIELDNAME, $includeLazyLoadColumns = true, $alreadyDumpedObjects = array(), $includeForeignObjects = false)
    {

        if (isset($alreadyDumpedObjects['Courrier'][$this->hashCode()])) {
            return '*RECURSION*';
        }
        $alreadyDumpedObjects['Courrier'][$this->hashCode()] = true;
        $keys = CourrierTableMap::getFieldNames($keyType);
        $result = array(
            $keys[0] => $this->getIdCourrier(),
            $keys[1] => $this->getType(),
            $keys[2] => $this->getCanal(),
            $keys[3] => $this->getIdEntite(),
            $keys[4] => $this->getNom(),
            $keys[5] => $this->getNomcourt(),
            $keys[6] => $this->getIdComposition(),
            $keys[7] => $this->getVariables(),
            $keys[8] => $this->getObservation(),
            $keys[9] => $this->getDateEnvoi(),
            $keys[10] => $this->getStatut(),
            $keys[11] => $this->getCreatedAt(),
            $keys[12] => $this->getUpdatedAt(),
        );
        if ($result[$keys[9]] instanceof \DateTimeInterface) {
            $result[$keys[9]] = $result[$keys[9]]->format('c');
        }

        if ($result[$keys[11]] instanceof \DateTimeInterface) {
            $result[$keys[11]] = $result[$keys[11]]->format('c');
        }

        if ($result[$keys[12]] instanceof \DateTimeInterface) {
            $result[$keys[12]] = $result[$keys[12]]->format('c');
        }

        $virtualColumns = $this->virtualColumns;
        foreach ($virtualColumns as $key => $virtualColumn) {
            $result[$key] = $virtualColumn;
        }

        if ($includeForeignObjects) {
            if (null !== $this->aEntite) {

                switch ($keyType) {
                    case TableMap::TYPE_CAMELNAME:
                        $key = 'entite';
                        break;
                    case TableMap::TYPE_FIELDNAME:
                        $key = 'asso_entites';
                        break;
                    default:
                        $key = 'Entite';
                }

                $result[$key] = $this->aEntite->toArray($keyType, $includeLazyLoadColumns,  $alreadyDumpedObjects, true);
            }
            if (null !== $this->aComposition) {

                switch ($keyType) {
                    case TableMap::TYPE_CAMELNAME:
                        $key = 'composition';
                        break;
                    case TableMap::TYPE_FIELDNAME:
                        $key = 'com_compositions';
                        break;
                    default:
                        $key = 'Composition';
                }

                $result[$key] = $this->aComposition->toArray($keyType, $includeLazyLoadColumns,  $alreadyDumpedObjects, true);
            }
            if (null !== $this->collCourrierLiens) {

                switch ($keyType) {
                    case TableMap::TYPE_CAMELNAME:
                        $key = 'courrierLiens';
                        break;
                    case TableMap::TYPE_FIELDNAME:
                        $key = 'com_courriers_lienss';
                        break;
                    default:
                        $key = 'CourrierLiens';
                }

                $result[$key] = $this->collCourrierLiens->toArray(null, false, $keyType, $includeLazyLoadColumns, $alreadyDumpedObjects);
            }
        }

        return $result;
    }

    /**
     * Sets a field from the object by name passed in as a string.
     *
     * @param  string $name
     * @param  mixed  $value field value
     * @param  string $type The type of fieldname the $name is of:
     *                one of the class type constants TableMap::TYPE_PHPNAME, TableMap::TYPE_CAMELNAME
     *                TableMap::TYPE_COLNAME, TableMap::TYPE_FIELDNAME, TableMap::TYPE_NUM.
     *                Defaults to TableMap::TYPE_FIELDNAME.
     * @return $this|\Courrier
     */
    public function setByName($name, $value, $type = TableMap::TYPE_FIELDNAME)
    {
        $pos = CourrierTableMap::translateFieldName($name, $type, TableMap::TYPE_NUM);

        return $this->setByPosition($pos, $value);
    }

    /**
     * Sets a field from the object by Position as specified in the xml schema.
     * Zero-based.
     *
     * @param  int $pos position in xml schema
     * @param  mixed $value field value
     * @return $this|\Courrier
     */
    public function setByPosition($pos, $value)
    {
        switch ($pos) {
            case 0:
                $this->setIdCourrier($value);
                break;
            case 1:
                $this->setType($value);
                break;
            case 2:
                $this->setCanal($value);
                break;
            case 3:
                $this->setIdEntite($value);
                break;
            case 4:
                $this->setNom($value);
                break;
            case 5:
                $this->setNomcourt($value);
                break;
            case 6:
                $this->setIdComposition($value);
                break;
            case 7:
                $this->setVariables($value);
                break;
            case 8:
                $this->setObservation($value);
                break;
            case 9:
                $this->setDateEnvoi($value);
                break;
            case 10:
                $this->setStatut($value);
                break;
            case 11:
                $this->setCreatedAt($value);
                break;
            case 12:
                $this->setUpdatedAt($value);
                break;
        } // switch()

        return $this;
    }

    /**
     * Populates the object using an array.
     *
     * This is particularly useful when populating an object from one of the
     * request arrays (e.g. $_POST).  This method goes through the column
     * names, checking to see whether a matching key exists in populated
     * array. If so the setByName() method is called for that column.
     *
     * You can specify the key type of the array by additionally passing one
     * of the class type constants TableMap::TYPE_PHPNAME, TableMap::TYPE_CAMELNAME,
     * TableMap::TYPE_COLNAME, TableMap::TYPE_FIELDNAME, TableMap::TYPE_NUM.
     * The default key type is the column's TableMap::TYPE_FIELDNAME.
     *
     * @param      array  $arr     An array to populate the object from.
     * @param      string $keyType The type of keys the array uses.
     * @return void
     */
    public function fromArray($arr, $keyType = TableMap::TYPE_FIELDNAME)
    {
        $keys = CourrierTableMap::getFieldNames($keyType);

        if (array_key_exists($keys[0], $arr)) {
            $this->setIdCourrier($arr[$keys[0]]);
        }
        if (array_key_exists($keys[1], $arr)) {
            $this->setType($arr[$keys[1]]);
        }
        if (array_key_exists($keys[2], $arr)) {
            $this->setCanal($arr[$keys[2]]);
        }
        if (array_key_exists($keys[3], $arr)) {
            $this->setIdEntite($arr[$keys[3]]);
        }
        if (array_key_exists($keys[4], $arr)) {
            $this->setNom($arr[$keys[4]]);
        }
        if (array_key_exists($keys[5], $arr)) {
            $this->setNomcourt($arr[$keys[5]]);
        }
        if (array_key_exists($keys[6], $arr)) {
            $this->setIdComposition($arr[$keys[6]]);
        }
        if (array_key_exists($keys[7], $arr)) {
            $this->setVariables($arr[$keys[7]]);
        }
        if (array_key_exists($keys[8], $arr)) {
            $this->setObservation($arr[$keys[8]]);
        }
        if (array_key_exists($keys[9], $arr)) {
            $this->setDateEnvoi($arr[$keys[9]]);
        }
        if (array_key_exists($keys[10], $arr)) {
            $this->setStatut($arr[$keys[10]]);
        }
        if (array_key_exists($keys[11], $arr)) {
            $this->setCreatedAt($arr[$keys[11]]);
        }
        if (array_key_exists($keys[12], $arr)) {
            $this->setUpdatedAt($arr[$keys[12]]);
        }
    }

     /**
     * Populate the current object from a string, using a given parser format
     * <code>
     * $book = new Book();
     * $book->importFrom('JSON', '{"Id":9012,"Title":"Don Juan","ISBN":"0140422161","Price":12.99,"PublisherId":1234,"AuthorId":5678}');
     * </code>
     *
     * You can specify the key type of the array by additionally passing one
     * of the class type constants TableMap::TYPE_PHPNAME, TableMap::TYPE_CAMELNAME,
     * TableMap::TYPE_COLNAME, TableMap::TYPE_FIELDNAME, TableMap::TYPE_NUM.
     * The default key type is the column's TableMap::TYPE_FIELDNAME.
     *
     * @param mixed $parser A AbstractParser instance,
     *                       or a format name ('XML', 'YAML', 'JSON', 'CSV')
     * @param string $data The source data to import from
     * @param string $keyType The type of keys the array uses.
     *
     * @return $this|\Courrier The current object, for fluid interface
     */
    public function importFrom($parser, $data, $keyType = TableMap::TYPE_FIELDNAME)
    {
        if (!$parser instanceof AbstractParser) {
            $parser = AbstractParser::getParser($parser);
        }

        $this->fromArray($parser->toArray($data), $keyType);

        return $this;
    }

    /**
     * Build a Criteria object containing the values of all modified columns in this object.
     *
     * @return Criteria The Criteria object containing all modified values.
     */
    public function buildCriteria()
    {
        $criteria = new Criteria(CourrierTableMap::DATABASE_NAME);

        if ($this->isColumnModified(CourrierTableMap::COL_ID_COURRIER)) {
            $criteria->add(CourrierTableMap::COL_ID_COURRIER, $this->id_courrier);
        }
        if ($this->isColumnModified(CourrierTableMap::COL_TYPE)) {
            $criteria->add(CourrierTableMap::COL_TYPE, $this->type);
        }
        if ($this->isColumnModified(CourrierTableMap::COL_CANAL)) {
            $criteria->add(CourrierTableMap::COL_CANAL, $this->canal);
        }
        if ($this->isColumnModified(CourrierTableMap::COL_ID_ENTITE)) {
            $criteria->add(CourrierTableMap::COL_ID_ENTITE, $this->id_entite);
        }
        if ($this->isColumnModified(CourrierTableMap::COL_NOM)) {
            $criteria->add(CourrierTableMap::COL_NOM, $this->nom);
        }
        if ($this->isColumnModified(CourrierTableMap::COL_NOMCOURT)) {
            $criteria->add(CourrierTableMap::COL_NOMCOURT, $this->nomcourt);
        }
        if ($this->isColumnModified(CourrierTableMap::COL_ID_COMPOSITION)) {
            $criteria->add(CourrierTableMap::COL_ID_COMPOSITION, $this->id_composition);
        }
        if ($this->isColumnModified(CourrierTableMap::COL_VARIABLES)) {
            $criteria->add(CourrierTableMap::COL_VARIABLES, $this->variables);
        }
        if ($this->isColumnModified(CourrierTableMap::COL_OBSERVATION)) {
            $criteria->add(CourrierTableMap::COL_OBSERVATION, $this->observation);
        }
        if ($this->isColumnModified(CourrierTableMap::COL_DATE_ENVOI)) {
            $criteria->add(CourrierTableMap::COL_DATE_ENVOI, $this->date_envoi);
        }
        if ($this->isColumnModified(CourrierTableMap::COL_STATUT)) {
            $criteria->add(CourrierTableMap::COL_STATUT, $this->statut);
        }
        if ($this->isColumnModified(CourrierTableMap::COL_CREATED_AT)) {
            $criteria->add(CourrierTableMap::COL_CREATED_AT, $this->created_at);
        }
        if ($this->isColumnModified(CourrierTableMap::COL_UPDATED_AT)) {
            $criteria->add(CourrierTableMap::COL_UPDATED_AT, $this->updated_at);
        }

        return $criteria;
    }

    /**
     * Builds a Criteria object containing the primary key for this object.
     *
     * Unlike buildCriteria() this method includes the primary key values regardless
     * of whether or not they have been modified.
     *
     * @throws LogicException if no primary key is defined
     *
     * @return Criteria The Criteria object containing value(s) for primary key(s).
     */
    public function buildPkeyCriteria()
    {
        $criteria = ChildCourrierQuery::create();
        $criteria->add(CourrierTableMap::COL_ID_COURRIER, $this->id_courrier);

        return $criteria;
    }

    /**
     * If the primary key is not null, return the hashcode of the
     * primary key. Otherwise, return the hash code of the object.
     *
     * @return int Hashcode
     */
    public function hashCode()
    {
        $validPk = null !== $this->getIdCourrier();

        $validPrimaryKeyFKs = 0;
        $primaryKeyFKs = [];

        if ($validPk) {
            return crc32(json_encode($this->getPrimaryKey(), JSON_UNESCAPED_UNICODE));
        } elseif ($validPrimaryKeyFKs) {
            return crc32(json_encode($primaryKeyFKs, JSON_UNESCAPED_UNICODE));
        }

        return spl_object_hash($this);
    }

    /**
     * Returns the primary key for this object (row).
     * @return string
     */
    public function getPrimaryKey()
    {
        return $this->getIdCourrier();
    }

    /**
     * Generic method to set the primary key (id_courrier column).
     *
     * @param       string $key Primary key.
     * @return void
     */
    public function setPrimaryKey($key)
    {
        $this->setIdCourrier($key);
    }

    /**
     * Returns true if the primary key for this object is null.
     * @return boolean
     */
    public function isPrimaryKeyNull()
    {
        return null === $this->getIdCourrier();
    }

    /**
     * Sets contents of passed object to values from current object.
     *
     * If desired, this method can also make copies of all associated (fkey referrers)
     * objects.
     *
     * @param      object $copyObj An object of \Courrier (or compatible) type.
     * @param      boolean $deepCopy Whether to also copy all rows that refer (by fkey) to the current row.
     * @param      boolean $makeNew Whether to reset autoincrement PKs and make the object new.
     * @throws PropelException
     */
    public function copyInto($copyObj, $deepCopy = false, $makeNew = true)
    {
        $copyObj->setType($this->getType());
        $copyObj->setCanal($this->getCanal());
        $copyObj->setIdEntite($this->getIdEntite());
        $copyObj->setNom($this->getNom());
        $copyObj->setNomcourt($this->getNomcourt());
        $copyObj->setIdComposition($this->getIdComposition());
        $copyObj->setVariables($this->getVariables());
        $copyObj->setObservation($this->getObservation());
        $copyObj->setDateEnvoi($this->getDateEnvoi());
        $copyObj->setStatut($this->getStatut());
        $copyObj->setCreatedAt($this->getCreatedAt());
        $copyObj->setUpdatedAt($this->getUpdatedAt());

        if ($deepCopy) {
            // important: temporarily setNew(false) because this affects the behavior of
            // the getter/setter methods for fkey referrer objects.
            $copyObj->setNew(false);

            foreach ($this->getCourrierLiens() as $relObj) {
                if ($relObj !== $this) {  // ensure that we don't try to copy a reference to ourselves
                    $copyObj->addCourrierLien($relObj->copy($deepCopy));
                }
            }

        } // if ($deepCopy)

        if ($makeNew) {
            $copyObj->setNew(true);
            $copyObj->setIdCourrier(NULL); // this is a auto-increment column, so set to default value
        }
    }

    /**
     * Makes a copy of this object that will be inserted as a new row in table when saved.
     * It creates a new object filling in the simple attributes, but skipping any primary
     * keys that are defined for the table.
     *
     * If desired, this method can also make copies of all associated (fkey referrers)
     * objects.
     *
     * @param  boolean $deepCopy Whether to also copy all rows that refer (by fkey) to the current row.
     * @return \Courrier Clone of current object.
     * @throws PropelException
     */
    public function copy($deepCopy = false)
    {
        // we use get_class(), because this might be a subclass
        $clazz = get_class($this);
        $copyObj = new $clazz();
        $this->copyInto($copyObj, $deepCopy);

        return $copyObj;
    }

    /**
     * Declares an association between this object and a ChildEntite object.
     *
     * @param  ChildEntite $v
     * @return $this|\Courrier The current object (for fluent API support)
     * @throws PropelException
     */
    public function setEntite(ChildEntite $v = null)
    {
        if ($v === null) {
            $this->setIdEntite(NULL);
        } else {
            $this->setIdEntite($v->getIdEntite());
        }

        $this->aEntite = $v;

        // Add binding for other direction of this n:n relationship.
        // If this object has already been added to the ChildEntite object, it will not be re-added.
        if ($v !== null) {
            $v->addCourrier($this);
        }


        return $this;
    }


    /**
     * Get the associated ChildEntite object
     *
     * @param  ConnectionInterface $con Optional Connection object.
     * @return ChildEntite The associated ChildEntite object.
     * @throws PropelException
     */
    public function getEntite(ConnectionInterface $con = null)
    {
        if ($this->aEntite === null && (($this->id_entite !== "" && $this->id_entite !== null))) {
            $this->aEntite = ChildEntiteQuery::create()->findPk($this->id_entite, $con);
            /* The following can be used additionally to
                guarantee the related object contains a reference
                to this object.  This level of coupling may, however, be
                undesirable since it could result in an only partially populated collection
                in the referenced object.
                $this->aEntite->addCourriers($this);
             */
        }

        return $this->aEntite;
    }

    /**
     * Declares an association between this object and a ChildComposition object.
     *
     * @param  ChildComposition $v
     * @return $this|\Courrier The current object (for fluent API support)
     * @throws PropelException
     */
    public function setComposition(ChildComposition $v = null)
    {
        if ($v === null) {
            $this->setIdComposition(NULL);
        } else {
            $this->setIdComposition($v->getIdComposition());
        }

        $this->aComposition = $v;

        // Add binding for other direction of this n:n relationship.
        // If this object has already been added to the ChildComposition object, it will not be re-added.
        if ($v !== null) {
            $v->addCourrier($this);
        }


        return $this;
    }


    /**
     * Get the associated ChildComposition object
     *
     * @param  ConnectionInterface $con Optional Connection object.
     * @return ChildComposition The associated ChildComposition object.
     * @throws PropelException
     */
    public function getComposition(ConnectionInterface $con = null)
    {
        if ($this->aComposition === null && ($this->id_composition != 0)) {
            $this->aComposition = ChildCompositionQuery::create()->findPk($this->id_composition, $con);
            /* The following can be used additionally to
                guarantee the related object contains a reference
                to this object.  This level of coupling may, however, be
                undesirable since it could result in an only partially populated collection
                in the referenced object.
                $this->aComposition->addCourriers($this);
             */
        }

        return $this->aComposition;
    }


    /**
     * Initializes a collection based on the name of a relation.
     * Avoids crafting an 'init[$relationName]s' method name
     * that wouldn't work when StandardEnglishPluralizer is used.
     *
     * @param      string $relationName The name of the relation to initialize
     * @return void
     */
    public function initRelation($relationName)
    {
        if ('CourrierLien' == $relationName) {
            $this->initCourrierLiens();
            return;
        }
    }

    /**
     * Clears out the collCourrierLiens collection
     *
     * This does not modify the database; however, it will remove any associated objects, causing
     * them to be refetched by subsequent calls to accessor method.
     *
     * @return void
     * @see        addCourrierLiens()
     */
    public function clearCourrierLiens()
    {
        $this->collCourrierLiens = null; // important to set this to NULL since that means it is uninitialized
    }

    /**
     * Reset is the collCourrierLiens collection loaded partially.
     */
    public function resetPartialCourrierLiens($v = true)
    {
        $this->collCourrierLiensPartial = $v;
    }

    /**
     * Initializes the collCourrierLiens collection.
     *
     * By default this just sets the collCourrierLiens collection to an empty array (like clearcollCourrierLiens());
     * however, you may wish to override this method in your stub class to provide setting appropriate
     * to your application -- for example, setting the initial array to the values stored in database.
     *
     * @param      boolean $overrideExisting If set to true, the method call initializes
     *                                        the collection even if it is not empty
     *
     * @return void
     */
    public function initCourrierLiens($overrideExisting = true)
    {
        if (null !== $this->collCourrierLiens && !$overrideExisting) {
            return;
        }

        $collectionClassName = CourrierLienTableMap::getTableMap()->getCollectionClassName();

        $this->collCourrierLiens = new $collectionClassName;
        $this->collCourrierLiens->setModel('\CourrierLien');
    }

    /**
     * Gets an array of ChildCourrierLien objects which contain a foreign key that references this object.
     *
     * If the $criteria is not null, it is used to always fetch the results from the database.
     * Otherwise the results are fetched from the database the first time, then cached.
     * Next time the same method is called without $criteria, the cached collection is returned.
     * If this ChildCourrier is new, it will return
     * an empty collection or the current collection; the criteria is ignored on a new object.
     *
     * @param      Criteria $criteria optional Criteria object to narrow the query
     * @param      ConnectionInterface $con optional connection object
     * @return ObjectCollection|ChildCourrierLien[] List of ChildCourrierLien objects
     * @throws PropelException
     */
    public function getCourrierLiens(Criteria $criteria = null, ConnectionInterface $con = null)
    {
        $partial = $this->collCourrierLiensPartial && !$this->isNew();
        if (null === $this->collCourrierLiens || null !== $criteria  || $partial) {
            if ($this->isNew() && null === $this->collCourrierLiens) {
                // return empty collection
                $this->initCourrierLiens();
            } else {
                $collCourrierLiens = ChildCourrierLienQuery::create(null, $criteria)
                    ->filterByCourrier($this)
                    ->find($con);

                if (null !== $criteria) {
                    if (false !== $this->collCourrierLiensPartial && count($collCourrierLiens)) {
                        $this->initCourrierLiens(false);

                        foreach ($collCourrierLiens as $obj) {
                            if (false == $this->collCourrierLiens->contains($obj)) {
                                $this->collCourrierLiens->append($obj);
                            }
                        }

                        $this->collCourrierLiensPartial = true;
                    }

                    return $collCourrierLiens;
                }

                if ($partial && $this->collCourrierLiens) {
                    foreach ($this->collCourrierLiens as $obj) {
                        if ($obj->isNew()) {
                            $collCourrierLiens[] = $obj;
                        }
                    }
                }

                $this->collCourrierLiens = $collCourrierLiens;
                $this->collCourrierLiensPartial = false;
            }
        }

        return $this->collCourrierLiens;
    }

    /**
     * Sets a collection of ChildCourrierLien objects related by a one-to-many relationship
     * to the current object.
     * It will also schedule objects for deletion based on a diff between old objects (aka persisted)
     * and new objects from the given Propel collection.
     *
     * @param      Collection $courrierLiens A Propel collection.
     * @param      ConnectionInterface $con Optional connection object
     * @return $this|ChildCourrier The current object (for fluent API support)
     */
    public function setCourrierLiens(Collection $courrierLiens, ConnectionInterface $con = null)
    {
        /** @var ChildCourrierLien[] $courrierLiensToDelete */
        $courrierLiensToDelete = $this->getCourrierLiens(new Criteria(), $con)->diff($courrierLiens);


        $this->courrierLiensScheduledForDeletion = $courrierLiensToDelete;

        foreach ($courrierLiensToDelete as $courrierLienRemoved) {
            $courrierLienRemoved->setCourrier(null);
        }

        $this->collCourrierLiens = null;
        foreach ($courrierLiens as $courrierLien) {
            $this->addCourrierLien($courrierLien);
        }

        $this->collCourrierLiens = $courrierLiens;
        $this->collCourrierLiensPartial = false;

        return $this;
    }

    /**
     * Returns the number of related CourrierLien objects.
     *
     * @param      Criteria $criteria
     * @param      boolean $distinct
     * @param      ConnectionInterface $con
     * @return int             Count of related CourrierLien objects.
     * @throws PropelException
     */
    public function countCourrierLiens(Criteria $criteria = null, $distinct = false, ConnectionInterface $con = null)
    {
        $partial = $this->collCourrierLiensPartial && !$this->isNew();
        if (null === $this->collCourrierLiens || null !== $criteria || $partial) {
            if ($this->isNew() && null === $this->collCourrierLiens) {
                return 0;
            }

            if ($partial && !$criteria) {
                return count($this->getCourrierLiens());
            }

            $query = ChildCourrierLienQuery::create(null, $criteria);
            if ($distinct) {
                $query->distinct();
            }

            return $query
                ->filterByCourrier($this)
                ->count($con);
        }

        return count($this->collCourrierLiens);
    }

    /**
     * Method called to associate a ChildCourrierLien object to this object
     * through the ChildCourrierLien foreign key attribute.
     *
     * @param  ChildCourrierLien $l ChildCourrierLien
     * @return $this|\Courrier The current object (for fluent API support)
     */
    public function addCourrierLien(ChildCourrierLien $l)
    {
        if ($this->collCourrierLiens === null) {
            $this->initCourrierLiens();
            $this->collCourrierLiensPartial = true;
        }

        if (!$this->collCourrierLiens->contains($l)) {
            $this->doAddCourrierLien($l);

            if ($this->courrierLiensScheduledForDeletion and $this->courrierLiensScheduledForDeletion->contains($l)) {
                $this->courrierLiensScheduledForDeletion->remove($this->courrierLiensScheduledForDeletion->search($l));
            }
        }

        return $this;
    }

    /**
     * @param ChildCourrierLien $courrierLien The ChildCourrierLien object to add.
     */
    protected function doAddCourrierLien(ChildCourrierLien $courrierLien)
    {
        $this->collCourrierLiens[]= $courrierLien;
        $courrierLien->setCourrier($this);
    }

    /**
     * @param  ChildCourrierLien $courrierLien The ChildCourrierLien object to remove.
     * @return $this|ChildCourrier The current object (for fluent API support)
     */
    public function removeCourrierLien(ChildCourrierLien $courrierLien)
    {
        if ($this->getCourrierLiens()->contains($courrierLien)) {
            $pos = $this->collCourrierLiens->search($courrierLien);
            $this->collCourrierLiens->remove($pos);
            if (null === $this->courrierLiensScheduledForDeletion) {
                $this->courrierLiensScheduledForDeletion = clone $this->collCourrierLiens;
                $this->courrierLiensScheduledForDeletion->clear();
            }
            $this->courrierLiensScheduledForDeletion[]= clone $courrierLien;
            $courrierLien->setCourrier(null);
        }

        return $this;
    }


    /**
     * If this collection has already been initialized with
     * an identical criteria, it returns the collection.
     * Otherwise if this Courrier is new, it will return
     * an empty collection; or if this Courrier has previously
     * been saved, it will retrieve related CourrierLiens from storage.
     *
     * This method is protected by default in order to keep the public
     * api reasonable.  You can provide public methods for those you
     * actually need in Courrier.
     *
     * @param      Criteria $criteria optional Criteria object to narrow the query
     * @param      ConnectionInterface $con optional connection object
     * @param      string $joinBehavior optional join type to use (defaults to Criteria::LEFT_JOIN)
     * @return ObjectCollection|ChildCourrierLien[] List of ChildCourrierLien objects
     */
    public function getCourrierLiensJoinIndividu(Criteria $criteria = null, ConnectionInterface $con = null, $joinBehavior = Criteria::LEFT_JOIN)
    {
        $query = ChildCourrierLienQuery::create(null, $criteria);
        $query->joinWith('Individu', $joinBehavior);

        return $this->getCourrierLiens($query, $con);
    }


    /**
     * If this collection has already been initialized with
     * an identical criteria, it returns the collection.
     * Otherwise if this Courrier is new, it will return
     * an empty collection; or if this Courrier has previously
     * been saved, it will retrieve related CourrierLiens from storage.
     *
     * This method is protected by default in order to keep the public
     * api reasonable.  You can provide public methods for those you
     * actually need in Courrier.
     *
     * @param      Criteria $criteria optional Criteria object to narrow the query
     * @param      ConnectionInterface $con optional connection object
     * @param      string $joinBehavior optional join type to use (defaults to Criteria::LEFT_JOIN)
     * @return ObjectCollection|ChildCourrierLien[] List of ChildCourrierLien objects
     */
    public function getCourrierLiensJoinMembre(Criteria $criteria = null, ConnectionInterface $con = null, $joinBehavior = Criteria::LEFT_JOIN)
    {
        $query = ChildCourrierLienQuery::create(null, $criteria);
        $query->joinWith('Membre', $joinBehavior);

        return $this->getCourrierLiens($query, $con);
    }

    /**
     * Clears the current object, sets all attributes to their default values and removes
     * outgoing references as well as back-references (from other objects to this one. Results probably in a database
     * change of those foreign objects when you call `save` there).
     */
    public function clear()
    {
        if (null !== $this->aEntite) {
            $this->aEntite->removeCourrier($this);
        }
        if (null !== $this->aComposition) {
            $this->aComposition->removeCourrier($this);
        }
        $this->id_courrier = null;
        $this->type = null;
        $this->canal = null;
        $this->id_entite = null;
        $this->nom = null;
        $this->nomcourt = null;
        $this->id_composition = null;
        $this->variables = null;
        $this->observation = null;
        $this->date_envoi = null;
        $this->statut = null;
        $this->created_at = null;
        $this->updated_at = null;
        $this->alreadyInSave = false;
        $this->clearAllReferences();
        $this->applyDefaultValues();
        $this->resetModified();
        $this->setNew(true);
        $this->setDeleted(false);
    }

    /**
     * Resets all references and back-references to other model objects or collections of model objects.
     *
     * This method is used to reset all php object references (not the actual reference in the database).
     * Necessary for object serialisation.
     *
     * @param      boolean $deep Whether to also clear the references on all referrer objects.
     */
    public function clearAllReferences($deep = false)
    {
        if ($deep) {
            if ($this->collCourrierLiens) {
                foreach ($this->collCourrierLiens as $o) {
                    $o->clearAllReferences($deep);
                }
            }
        } // if ($deep)

        $this->collCourrierLiens = null;
        $this->aEntite = null;
        $this->aComposition = null;
    }

    /**
     * Return the string representation of this object
     *
     * @return string
     */
    public function __toString()
    {
        return (string) $this->exportTo(CourrierTableMap::DEFAULT_STRING_FORMAT);
    }

    // timestampable behavior

    /**
     * Mark the current object so that the update date doesn't get updated during next save
     *
     * @return     $this|ChildCourrier The current object (for fluent API support)
     */
    public function keepUpdateDateUnchanged()
    {
        $this->modifiedColumns[CourrierTableMap::COL_UPDATED_AT] = true;

        return $this;
    }

    /**
     * Code to be run before persisting the object
     * @param  ConnectionInterface $con
     * @return boolean
     */
    public function preSave(ConnectionInterface $con = null)
    {
        if (is_callable('parent::preSave')) {
            return parent::preSave($con);
        }
        return true;
    }

    /**
     * Code to be run after persisting the object
     * @param ConnectionInterface $con
     */
    public function postSave(ConnectionInterface $con = null)
    {
        if (is_callable('parent::postSave')) {
            parent::postSave($con);
        }
    }

    /**
     * Code to be run before inserting to database
     * @param  ConnectionInterface $con
     * @return boolean
     */
    public function preInsert(ConnectionInterface $con = null)
    {
        if (is_callable('parent::preInsert')) {
            return parent::preInsert($con);
        }
        return true;
    }

    /**
     * Code to be run after inserting to database
     * @param ConnectionInterface $con
     */
    public function postInsert(ConnectionInterface $con = null)
    {
        if (is_callable('parent::postInsert')) {
            parent::postInsert($con);
        }
    }

    /**
     * Code to be run before updating the object in database
     * @param  ConnectionInterface $con
     * @return boolean
     */
    public function preUpdate(ConnectionInterface $con = null)
    {
        if (is_callable('parent::preUpdate')) {
            return parent::preUpdate($con);
        }
        return true;
    }

    /**
     * Code to be run after updating the object in database
     * @param ConnectionInterface $con
     */
    public function postUpdate(ConnectionInterface $con = null)
    {
        if (is_callable('parent::postUpdate')) {
            parent::postUpdate($con);
        }
    }

    /**
     * Code to be run before deleting the object in database
     * @param  ConnectionInterface $con
     * @return boolean
     */
    public function preDelete(ConnectionInterface $con = null)
    {
        if (is_callable('parent::preDelete')) {
            return parent::preDelete($con);
        }
        return true;
    }

    /**
     * Code to be run after deleting the object in database
     * @param ConnectionInterface $con
     */
    public function postDelete(ConnectionInterface $con = null)
    {
        if (is_callable('parent::postDelete')) {
            parent::postDelete($con);
        }
    }


    /**
     * Derived method to catches calls to undefined methods.
     *
     * Provides magic import/export method support (fromXML()/toXML(), fromYAML()/toYAML(), etc.).
     * Allows to define default __call() behavior if you overwrite __call()
     *
     * @param string $name
     * @param mixed  $params
     *
     * @return array|string
     */
    public function __call($name, $params)
    {
        if (0 === strpos($name, 'get')) {
            $virtualColumn = substr($name, 3);
            if ($this->hasVirtualColumn($virtualColumn)) {
                return $this->getVirtualColumn($virtualColumn);
            }

            $virtualColumn = lcfirst($virtualColumn);
            if ($this->hasVirtualColumn($virtualColumn)) {
                return $this->getVirtualColumn($virtualColumn);
            }
        }

        if (0 === strpos($name, 'from')) {
            $format = substr($name, 4);

            return $this->importFrom($format, reset($params));
        }

        if (0 === strpos($name, 'to')) {
            $format = substr($name, 2);
            $includeLazyLoadColumns = isset($params[0]) ? $params[0] : true;

            return $this->exportTo($format, $includeLazyLoadColumns);
        }

        throw new BadMethodCallException(sprintf('Call to undefined method: %s.', $name));
    }

}
