<?php

namespace Base;

use \InfolettreInscrit as ChildInfolettreInscrit;
use \InfolettreInscritQuery as ChildInfolettreInscritQuery;
use \Exception;
use \PDO;
use Map\InfolettreInscritTableMap;
use Propel\Runtime\Propel;
use Propel\Runtime\ActiveQuery\Criteria;
use Propel\Runtime\ActiveQuery\ModelCriteria;
use Propel\Runtime\ActiveQuery\ModelJoin;
use Propel\Runtime\Collection\ObjectCollection;
use Propel\Runtime\Connection\ConnectionInterface;
use Propel\Runtime\Exception\PropelException;

/**
 * Base class that represents a query for the 'com_infolettres_inscrits' table.
 *
 *
 *
 * @method     ChildInfolettreInscritQuery orderByIdInfolettreInscrit($order = Criteria::ASC) Order by the id_infolettre_inscrit column
 * @method     ChildInfolettreInscritQuery orderByEmail($order = Criteria::ASC) Order by the email column
 * @method     ChildInfolettreInscritQuery orderByIdInfolettre($order = Criteria::ASC) Order by the id_infolettre column
 * @method     ChildInfolettreInscritQuery orderByDateSynchro($order = Criteria::ASC) Order by the date_synchro column
 * @method     ChildInfolettreInscritQuery orderBySynchro($order = Criteria::ASC) Order by the synchro column
 *
 * @method     ChildInfolettreInscritQuery groupByIdInfolettreInscrit() Group by the id_infolettre_inscrit column
 * @method     ChildInfolettreInscritQuery groupByEmail() Group by the email column
 * @method     ChildInfolettreInscritQuery groupByIdInfolettre() Group by the id_infolettre column
 * @method     ChildInfolettreInscritQuery groupByDateSynchro() Group by the date_synchro column
 * @method     ChildInfolettreInscritQuery groupBySynchro() Group by the synchro column
 *
 * @method     ChildInfolettreInscritQuery leftJoin($relation) Adds a LEFT JOIN clause to the query
 * @method     ChildInfolettreInscritQuery rightJoin($relation) Adds a RIGHT JOIN clause to the query
 * @method     ChildInfolettreInscritQuery innerJoin($relation) Adds a INNER JOIN clause to the query
 *
 * @method     ChildInfolettreInscritQuery leftJoinWith($relation) Adds a LEFT JOIN clause and with to the query
 * @method     ChildInfolettreInscritQuery rightJoinWith($relation) Adds a RIGHT JOIN clause and with to the query
 * @method     ChildInfolettreInscritQuery innerJoinWith($relation) Adds a INNER JOIN clause and with to the query
 *
 * @method     ChildInfolettreInscritQuery leftJoinInfolettre($relationAlias = null) Adds a LEFT JOIN clause to the query using the Infolettre relation
 * @method     ChildInfolettreInscritQuery rightJoinInfolettre($relationAlias = null) Adds a RIGHT JOIN clause to the query using the Infolettre relation
 * @method     ChildInfolettreInscritQuery innerJoinInfolettre($relationAlias = null) Adds a INNER JOIN clause to the query using the Infolettre relation
 *
 * @method     ChildInfolettreInscritQuery joinWithInfolettre($joinType = Criteria::INNER_JOIN) Adds a join clause and with to the query using the Infolettre relation
 *
 * @method     ChildInfolettreInscritQuery leftJoinWithInfolettre() Adds a LEFT JOIN clause and with to the query using the Infolettre relation
 * @method     ChildInfolettreInscritQuery rightJoinWithInfolettre() Adds a RIGHT JOIN clause and with to the query using the Infolettre relation
 * @method     ChildInfolettreInscritQuery innerJoinWithInfolettre() Adds a INNER JOIN clause and with to the query using the Infolettre relation
 *
 * @method     \InfolettreQuery endUse() Finalizes a secondary criteria and merges it with its primary Criteria
 *
 * @method     ChildInfolettreInscrit findOne(ConnectionInterface $con = null) Return the first ChildInfolettreInscrit matching the query
 * @method     ChildInfolettreInscrit findOneOrCreate(ConnectionInterface $con = null) Return the first ChildInfolettreInscrit matching the query, or a new ChildInfolettreInscrit object populated from the query conditions when no match is found
 *
 * @method     ChildInfolettreInscrit findOneByIdInfolettreInscrit(string $id_infolettre_inscrit) Return the first ChildInfolettreInscrit filtered by the id_infolettre_inscrit column
 * @method     ChildInfolettreInscrit findOneByEmail(string $email) Return the first ChildInfolettreInscrit filtered by the email column
 * @method     ChildInfolettreInscrit findOneByIdInfolettre(string $id_infolettre) Return the first ChildInfolettreInscrit filtered by the id_infolettre column
 * @method     ChildInfolettreInscrit findOneByDateSynchro(string $date_synchro) Return the first ChildInfolettreInscrit filtered by the date_synchro column
 * @method     ChildInfolettreInscrit findOneBySynchro(int $synchro) Return the first ChildInfolettreInscrit filtered by the synchro column *

 * @method     ChildInfolettreInscrit requirePk($key, ConnectionInterface $con = null) Return the ChildInfolettreInscrit by primary key and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildInfolettreInscrit requireOne(ConnectionInterface $con = null) Return the first ChildInfolettreInscrit matching the query and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 *
 * @method     ChildInfolettreInscrit requireOneByIdInfolettreInscrit(string $id_infolettre_inscrit) Return the first ChildInfolettreInscrit filtered by the id_infolettre_inscrit column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildInfolettreInscrit requireOneByEmail(string $email) Return the first ChildInfolettreInscrit filtered by the email column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildInfolettreInscrit requireOneByIdInfolettre(string $id_infolettre) Return the first ChildInfolettreInscrit filtered by the id_infolettre column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildInfolettreInscrit requireOneByDateSynchro(string $date_synchro) Return the first ChildInfolettreInscrit filtered by the date_synchro column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildInfolettreInscrit requireOneBySynchro(int $synchro) Return the first ChildInfolettreInscrit filtered by the synchro column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 *
 * @method     ChildInfolettreInscrit[]|ObjectCollection find(ConnectionInterface $con = null) Return ChildInfolettreInscrit objects based on current ModelCriteria
 * @method     ChildInfolettreInscrit[]|ObjectCollection findByIdInfolettreInscrit(string $id_infolettre_inscrit) Return ChildInfolettreInscrit objects filtered by the id_infolettre_inscrit column
 * @method     ChildInfolettreInscrit[]|ObjectCollection findByEmail(string $email) Return ChildInfolettreInscrit objects filtered by the email column
 * @method     ChildInfolettreInscrit[]|ObjectCollection findByIdInfolettre(string $id_infolettre) Return ChildInfolettreInscrit objects filtered by the id_infolettre column
 * @method     ChildInfolettreInscrit[]|ObjectCollection findByDateSynchro(string $date_synchro) Return ChildInfolettreInscrit objects filtered by the date_synchro column
 * @method     ChildInfolettreInscrit[]|ObjectCollection findBySynchro(int $synchro) Return ChildInfolettreInscrit objects filtered by the synchro column
 * @method     ChildInfolettreInscrit[]|\Propel\Runtime\Util\PropelModelPager paginate($page = 1, $maxPerPage = 10, ConnectionInterface $con = null) Issue a SELECT query based on the current ModelCriteria and uses a page and a maximum number of results per page to compute an offset and a limit
 *
 */
abstract class InfolettreInscritQuery extends ModelCriteria
{
    protected $entityNotFoundExceptionClass = '\\Propel\\Runtime\\Exception\\EntityNotFoundException';

    /**
     * Initializes internal state of \Base\InfolettreInscritQuery object.
     *
     * @param     string $dbName The database name
     * @param     string $modelName The phpName of a model, e.g. 'Book'
     * @param     string $modelAlias The alias for the model in this query, e.g. 'b'
     */
    public function __construct($dbName = 'simplasso', $modelName = '\\InfolettreInscrit', $modelAlias = null)
    {
        parent::__construct($dbName, $modelName, $modelAlias);
    }

    /**
     * Returns a new ChildInfolettreInscritQuery object.
     *
     * @param     string $modelAlias The alias of a model in the query
     * @param     Criteria $criteria Optional Criteria to build the query from
     *
     * @return ChildInfolettreInscritQuery
     */
    public static function create($modelAlias = null, Criteria $criteria = null)
    {
        if ($criteria instanceof ChildInfolettreInscritQuery) {
            return $criteria;
        }
        $query = new ChildInfolettreInscritQuery();
        if (null !== $modelAlias) {
            $query->setModelAlias($modelAlias);
        }
        if ($criteria instanceof Criteria) {
            $query->mergeWith($criteria);
        }

        return $query;
    }

    /**
     * Find object by primary key.
     * Propel uses the instance pool to skip the database if the object exists.
     * Go fast if the query is untouched.
     *
     * <code>
     * $obj  = $c->findPk(12, $con);
     * </code>
     *
     * @param mixed $key Primary key to use for the query
     * @param ConnectionInterface $con an optional connection object
     *
     * @return ChildInfolettreInscrit|array|mixed the result, formatted by the current formatter
     */
    public function findPk($key, ConnectionInterface $con = null)
    {
        if ($key === null) {
            return null;
        }

        if ($con === null) {
            $con = Propel::getServiceContainer()->getReadConnection(InfolettreInscritTableMap::DATABASE_NAME);
        }

        $this->basePreSelect($con);

        if (
            $this->formatter || $this->modelAlias || $this->with || $this->select
            || $this->selectColumns || $this->asColumns || $this->selectModifiers
            || $this->map || $this->having || $this->joins
        ) {
            return $this->findPkComplex($key, $con);
        }

        if ((null !== ($obj = InfolettreInscritTableMap::getInstanceFromPool(null === $key || is_scalar($key) || is_callable([$key, '__toString']) ? (string) $key : $key)))) {
            // the object is already in the instance pool
            return $obj;
        }

        return $this->findPkSimple($key, $con);
    }

    /**
     * Find object by primary key using raw SQL to go fast.
     * Bypass doSelect() and the object formatter by using generated code.
     *
     * @param     mixed $key Primary key to use for the query
     * @param     ConnectionInterface $con A connection object
     *
     * @throws \Propel\Runtime\Exception\PropelException
     *
     * @return ChildInfolettreInscrit A model object, or null if the key is not found
     */
    protected function findPkSimple($key, ConnectionInterface $con)
    {
        $sql = 'SELECT `id_infolettre_inscrit`, `email`, `id_infolettre`, `date_synchro`, `synchro` FROM `com_infolettres_inscrits` WHERE `id_infolettre_inscrit` = :p0';
        try {
            $stmt = $con->prepare($sql);
            $stmt->bindValue(':p0', $key, PDO::PARAM_INT);
            $stmt->execute();
        } catch (Exception $e) {
            Propel::log($e->getMessage(), Propel::LOG_ERR);
            throw new PropelException(sprintf('Unable to execute SELECT statement [%s]', $sql), 0, $e);
        }
        $obj = null;
        if ($row = $stmt->fetch(\PDO::FETCH_NUM)) {
            /** @var ChildInfolettreInscrit $obj */
            $obj = new ChildInfolettreInscrit();
            $obj->hydrate($row);
            InfolettreInscritTableMap::addInstanceToPool($obj, null === $key || is_scalar($key) || is_callable([$key, '__toString']) ? (string) $key : $key);
        }
        $stmt->closeCursor();

        return $obj;
    }

    /**
     * Find object by primary key.
     *
     * @param     mixed $key Primary key to use for the query
     * @param     ConnectionInterface $con A connection object
     *
     * @return ChildInfolettreInscrit|array|mixed the result, formatted by the current formatter
     */
    protected function findPkComplex($key, ConnectionInterface $con)
    {
        // As the query uses a PK condition, no limit(1) is necessary.
        $criteria = $this->isKeepQuery() ? clone $this : $this;
        $dataFetcher = $criteria
            ->filterByPrimaryKey($key)
            ->doSelect($con);

        return $criteria->getFormatter()->init($criteria)->formatOne($dataFetcher);
    }

    /**
     * Find objects by primary key
     * <code>
     * $objs = $c->findPks(array(12, 56, 832), $con);
     * </code>
     * @param     array $keys Primary keys to use for the query
     * @param     ConnectionInterface $con an optional connection object
     *
     * @return ObjectCollection|array|mixed the list of results, formatted by the current formatter
     */
    public function findPks($keys, ConnectionInterface $con = null)
    {
        if (null === $con) {
            $con = Propel::getServiceContainer()->getReadConnection($this->getDbName());
        }
        $this->basePreSelect($con);
        $criteria = $this->isKeepQuery() ? clone $this : $this;
        $dataFetcher = $criteria
            ->filterByPrimaryKeys($keys)
            ->doSelect($con);

        return $criteria->getFormatter()->init($criteria)->format($dataFetcher);
    }

    /**
     * Filter the query by primary key
     *
     * @param     mixed $key Primary key to use for the query
     *
     * @return $this|ChildInfolettreInscritQuery The current query, for fluid interface
     */
    public function filterByPrimaryKey($key)
    {

        return $this->addUsingAlias(InfolettreInscritTableMap::COL_ID_INFOLETTRE_INSCRIT, $key, Criteria::EQUAL);
    }

    /**
     * Filter the query by a list of primary keys
     *
     * @param     array $keys The list of primary key to use for the query
     *
     * @return $this|ChildInfolettreInscritQuery The current query, for fluid interface
     */
    public function filterByPrimaryKeys($keys)
    {

        return $this->addUsingAlias(InfolettreInscritTableMap::COL_ID_INFOLETTRE_INSCRIT, $keys, Criteria::IN);
    }

    /**
     * Filter the query on the id_infolettre_inscrit column
     *
     * Example usage:
     * <code>
     * $query->filterByIdInfolettreInscrit(1234); // WHERE id_infolettre_inscrit = 1234
     * $query->filterByIdInfolettreInscrit(array(12, 34)); // WHERE id_infolettre_inscrit IN (12, 34)
     * $query->filterByIdInfolettreInscrit(array('min' => 12)); // WHERE id_infolettre_inscrit > 12
     * </code>
     *
     * @param     mixed $idInfolettreInscrit The value to use as filter.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildInfolettreInscritQuery The current query, for fluid interface
     */
    public function filterByIdInfolettreInscrit($idInfolettreInscrit = null, $comparison = null)
    {
        if (is_array($idInfolettreInscrit)) {
            $useMinMax = false;
            if (isset($idInfolettreInscrit['min'])) {
                $this->addUsingAlias(InfolettreInscritTableMap::COL_ID_INFOLETTRE_INSCRIT, $idInfolettreInscrit['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($idInfolettreInscrit['max'])) {
                $this->addUsingAlias(InfolettreInscritTableMap::COL_ID_INFOLETTRE_INSCRIT, $idInfolettreInscrit['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(InfolettreInscritTableMap::COL_ID_INFOLETTRE_INSCRIT, $idInfolettreInscrit, $comparison);
    }

    /**
     * Filter the query on the email column
     *
     * Example usage:
     * <code>
     * $query->filterByEmail('fooValue');   // WHERE email = 'fooValue'
     * $query->filterByEmail('%fooValue%', Criteria::LIKE); // WHERE email LIKE '%fooValue%'
     * </code>
     *
     * @param     string $email The value to use as filter.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildInfolettreInscritQuery The current query, for fluid interface
     */
    public function filterByEmail($email = null, $comparison = null)
    {
        if (null === $comparison) {
            if (is_array($email)) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(InfolettreInscritTableMap::COL_EMAIL, $email, $comparison);
    }

    /**
     * Filter the query on the id_infolettre column
     *
     * Example usage:
     * <code>
     * $query->filterByIdInfolettre(1234); // WHERE id_infolettre = 1234
     * $query->filterByIdInfolettre(array(12, 34)); // WHERE id_infolettre IN (12, 34)
     * $query->filterByIdInfolettre(array('min' => 12)); // WHERE id_infolettre > 12
     * </code>
     *
     * @see       filterByInfolettre()
     *
     * @param     mixed $idInfolettre The value to use as filter.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildInfolettreInscritQuery The current query, for fluid interface
     */
    public function filterByIdInfolettre($idInfolettre = null, $comparison = null)
    {
        if (is_array($idInfolettre)) {
            $useMinMax = false;
            if (isset($idInfolettre['min'])) {
                $this->addUsingAlias(InfolettreInscritTableMap::COL_ID_INFOLETTRE, $idInfolettre['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($idInfolettre['max'])) {
                $this->addUsingAlias(InfolettreInscritTableMap::COL_ID_INFOLETTRE, $idInfolettre['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(InfolettreInscritTableMap::COL_ID_INFOLETTRE, $idInfolettre, $comparison);
    }

    /**
     * Filter the query on the date_synchro column
     *
     * Example usage:
     * <code>
     * $query->filterByDateSynchro('2011-03-14'); // WHERE date_synchro = '2011-03-14'
     * $query->filterByDateSynchro('now'); // WHERE date_synchro = '2011-03-14'
     * $query->filterByDateSynchro(array('max' => 'yesterday')); // WHERE date_synchro > '2011-03-13'
     * </code>
     *
     * @param     mixed $dateSynchro The value to use as filter.
     *              Values can be integers (unix timestamps), DateTime objects, or strings.
     *              Empty strings are treated as NULL.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildInfolettreInscritQuery The current query, for fluid interface
     */
    public function filterByDateSynchro($dateSynchro = null, $comparison = null)
    {
        if (is_array($dateSynchro)) {
            $useMinMax = false;
            if (isset($dateSynchro['min'])) {
                $this->addUsingAlias(InfolettreInscritTableMap::COL_DATE_SYNCHRO, $dateSynchro['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($dateSynchro['max'])) {
                $this->addUsingAlias(InfolettreInscritTableMap::COL_DATE_SYNCHRO, $dateSynchro['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(InfolettreInscritTableMap::COL_DATE_SYNCHRO, $dateSynchro, $comparison);
    }

    /**
     * Filter the query on the synchro column
     *
     * Example usage:
     * <code>
     * $query->filterBySynchro(1234); // WHERE synchro = 1234
     * $query->filterBySynchro(array(12, 34)); // WHERE synchro IN (12, 34)
     * $query->filterBySynchro(array('min' => 12)); // WHERE synchro > 12
     * </code>
     *
     * @param     mixed $synchro The value to use as filter.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildInfolettreInscritQuery The current query, for fluid interface
     */
    public function filterBySynchro($synchro = null, $comparison = null)
    {
        if (is_array($synchro)) {
            $useMinMax = false;
            if (isset($synchro['min'])) {
                $this->addUsingAlias(InfolettreInscritTableMap::COL_SYNCHRO, $synchro['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($synchro['max'])) {
                $this->addUsingAlias(InfolettreInscritTableMap::COL_SYNCHRO, $synchro['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(InfolettreInscritTableMap::COL_SYNCHRO, $synchro, $comparison);
    }

    /**
     * Filter the query by a related \Infolettre object
     *
     * @param \Infolettre|ObjectCollection $infolettre The related object(s) to use as filter
     * @param string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @throws \Propel\Runtime\Exception\PropelException
     *
     * @return ChildInfolettreInscritQuery The current query, for fluid interface
     */
    public function filterByInfolettre($infolettre, $comparison = null)
    {
        if ($infolettre instanceof \Infolettre) {
            return $this
                ->addUsingAlias(InfolettreInscritTableMap::COL_ID_INFOLETTRE, $infolettre->getIdInfolettre(), $comparison);
        } elseif ($infolettre instanceof ObjectCollection) {
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }

            return $this
                ->addUsingAlias(InfolettreInscritTableMap::COL_ID_INFOLETTRE, $infolettre->toKeyValue('PrimaryKey', 'IdInfolettre'), $comparison);
        } else {
            throw new PropelException('filterByInfolettre() only accepts arguments of type \Infolettre or Collection');
        }
    }

    /**
     * Adds a JOIN clause to the query using the Infolettre relation
     *
     * @param     string $relationAlias optional alias for the relation
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return $this|ChildInfolettreInscritQuery The current query, for fluid interface
     */
    public function joinInfolettre($relationAlias = null, $joinType = Criteria::INNER_JOIN)
    {
        $tableMap = $this->getTableMap();
        $relationMap = $tableMap->getRelation('Infolettre');

        // create a ModelJoin object for this join
        $join = new ModelJoin();
        $join->setJoinType($joinType);
        $join->setRelationMap($relationMap, $this->useAliasInSQL ? $this->getModelAlias() : null, $relationAlias);
        if ($previousJoin = $this->getPreviousJoin()) {
            $join->setPreviousJoin($previousJoin);
        }

        // add the ModelJoin to the current object
        if ($relationAlias) {
            $this->addAlias($relationAlias, $relationMap->getRightTable()->getName());
            $this->addJoinObject($join, $relationAlias);
        } else {
            $this->addJoinObject($join, 'Infolettre');
        }

        return $this;
    }

    /**
     * Use the Infolettre relation Infolettre object
     *
     * @see useQuery()
     *
     * @param     string $relationAlias optional alias for the relation,
     *                                   to be used as main alias in the secondary query
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return \InfolettreQuery A secondary query class using the current class as primary query
     */
    public function useInfolettreQuery($relationAlias = null, $joinType = Criteria::INNER_JOIN)
    {
        return $this
            ->joinInfolettre($relationAlias, $joinType)
            ->useQuery($relationAlias ? $relationAlias : 'Infolettre', '\InfolettreQuery');
    }

    /**
     * Exclude object from result
     *
     * @param   ChildInfolettreInscrit $infolettreInscrit Object to remove from the list of results
     *
     * @return $this|ChildInfolettreInscritQuery The current query, for fluid interface
     */
    public function prune($infolettreInscrit = null)
    {
        if ($infolettreInscrit) {
            $this->addUsingAlias(InfolettreInscritTableMap::COL_ID_INFOLETTRE_INSCRIT, $infolettreInscrit->getIdInfolettreInscrit(), Criteria::NOT_EQUAL);
        }

        return $this;
    }

    /**
     * Deletes all rows from the com_infolettres_inscrits table.
     *
     * @param ConnectionInterface $con the connection to use
     * @return int The number of affected rows (if supported by underlying database driver).
     */
    public function doDeleteAll(ConnectionInterface $con = null)
    {
        if (null === $con) {
            $con = Propel::getServiceContainer()->getWriteConnection(InfolettreInscritTableMap::DATABASE_NAME);
        }

        // use transaction because $criteria could contain info
        // for more than one table or we could emulating ON DELETE CASCADE, etc.
        return $con->transaction(function () use ($con) {
            $affectedRows = 0; // initialize var to track total num of affected rows
            $affectedRows += parent::doDeleteAll($con);
            // Because this db requires some delete cascade/set null emulation, we have to
            // clear the cached instance *after* the emulation has happened (since
            // instances get re-added by the select statement contained therein).
            InfolettreInscritTableMap::clearInstancePool();
            InfolettreInscritTableMap::clearRelatedInstancePool();

            return $affectedRows;
        });
    }

    /**
     * Performs a DELETE on the database based on the current ModelCriteria
     *
     * @param ConnectionInterface $con the connection to use
     * @return int             The number of affected rows (if supported by underlying database driver).  This includes CASCADE-related rows
     *                         if supported by native driver or if emulated using Propel.
     * @throws PropelException Any exceptions caught during processing will be
     *                         rethrown wrapped into a PropelException.
     */
    public function delete(ConnectionInterface $con = null)
    {
        if (null === $con) {
            $con = Propel::getServiceContainer()->getWriteConnection(InfolettreInscritTableMap::DATABASE_NAME);
        }

        $criteria = $this;

        // Set the correct dbName
        $criteria->setDbName(InfolettreInscritTableMap::DATABASE_NAME);

        // use transaction because $criteria could contain info
        // for more than one table or we could emulating ON DELETE CASCADE, etc.
        return $con->transaction(function () use ($con, $criteria) {
            $affectedRows = 0; // initialize var to track total num of affected rows

            InfolettreInscritTableMap::removeInstanceFromPool($criteria);

            $affectedRows += ModelCriteria::delete($con);
            InfolettreInscritTableMap::clearRelatedInstancePool();

            return $affectedRows;
        });
    }

} // InfolettreInscritQuery
