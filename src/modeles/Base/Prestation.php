<?php

namespace Base;

use \AssoPrestationsArchive as ChildAssoPrestationsArchive;
use \AssoPrestationsArchiveQuery as ChildAssoPrestationsArchiveQuery;
use \Compte as ChildCompte;
use \CompteQuery as ChildCompteQuery;
use \Entite as ChildEntite;
use \EntiteQuery as ChildEntiteQuery;
use \Prestation as ChildPrestation;
use \PrestationQuery as ChildPrestationQuery;
use \Prestationslot as ChildPrestationslot;
use \PrestationslotPrestation as ChildPrestationslotPrestation;
use \PrestationslotPrestationQuery as ChildPrestationslotPrestationQuery;
use \PrestationslotQuery as ChildPrestationslotQuery;
use \Prix as ChildPrix;
use \PrixQuery as ChildPrixQuery;
use \Servicerendu as ChildServicerendu;
use \ServicerenduQuery as ChildServicerenduQuery;
use \DateTime;
use \Exception;
use \PDO;
use Map\PrestationTableMap;
use Map\PrestationslotPrestationTableMap;
use Map\PrixTableMap;
use Map\ServicerenduTableMap;
use Propel\Runtime\Propel;
use Propel\Runtime\ActiveQuery\Criteria;
use Propel\Runtime\ActiveQuery\ModelCriteria;
use Propel\Runtime\ActiveRecord\ActiveRecordInterface;
use Propel\Runtime\Collection\Collection;
use Propel\Runtime\Collection\ObjectCollection;
use Propel\Runtime\Connection\ConnectionInterface;
use Propel\Runtime\Exception\BadMethodCallException;
use Propel\Runtime\Exception\LogicException;
use Propel\Runtime\Exception\PropelException;
use Propel\Runtime\Map\TableMap;
use Propel\Runtime\Parser\AbstractParser;
use Propel\Runtime\Util\PropelDateTime;

/**
 * Base class that represents a row from the 'asso_prestations' table.
 *
 *
 *
 * @package    propel.generator..Base
 */
abstract class Prestation implements ActiveRecordInterface
{
    /**
     * TableMap class name
     */
    const TABLE_MAP = '\\Map\\PrestationTableMap';


    /**
     * attribute to determine if this object has previously been saved.
     * @var boolean
     */
    protected $new = true;

    /**
     * attribute to determine whether this object has been deleted.
     * @var boolean
     */
    protected $deleted = false;

    /**
     * The columns that have been modified in current object.
     * Tracking modified columns allows us to only update modified columns.
     * @var array
     */
    protected $modifiedColumns = array();

    /**
     * The (virtual) columns that are added at runtime
     * The formatters can add supplementary columns based on a resultset
     * @var array
     */
    protected $virtualColumns = array();

    /**
     * The value for the id_prestation field.
     *
     * @var        string
     */
    protected $id_prestation;

    /**
     * The value for the nom_groupe field.
     *
     * Note: this column has a database default value of: ''
     * @var        string
     */
    protected $nom_groupe;

    /**
     * The value for the nom field.
     *
     * @var        string
     */
    protected $nom;

    /**
     * The value for the nomcourt field.
     *
     * @var        string
     */
    protected $nomcourt;

    /**
     * The value for the descriptif field.
     *
     * @var        string
     */
    protected $descriptif;

    /**
     * The value for the id_entite field.
     * Entite
     * @var        string
     */
    protected $id_entite;

    /**
     * The value for the id_tva field.
     *
     * @var        string
     */
    protected $id_tva;

    /**
     * The value for the retard_jours field.
     * pour cotisation en retard en jours
     * @var        int
     */
    protected $retard_jours;

    /**
     * The value for the nombre_numero field.
     *
     * @var        int
     */
    protected $nombre_numero;

    /**
     * The value for the prochain_numero field.
     *
     * @var        int
     */
    protected $prochain_numero;

    /**
     * The value for the prixlibre field.
     *
     * Note: this column has a database default value of: false
     * @var        boolean
     */
    protected $prixlibre;

    /**
     * The value for the quantite field.
     *
     * Note: this column has a database default value of: false
     * @var        boolean
     */
    protected $quantite;

    /**
     * The value for the id_unite field.
     *
     * Note: this column has a database default value of: '0'
     * @var        string
     */
    protected $id_unite;

    /**
     * The value for the id_compte field.
     *
     * @var        string
     */
    protected $id_compte;

    /**
     * The value for the prestation_type field.
     *
     * @var        int
     */
    protected $prestation_type;

    /**
     * The value for the active field.
     *
     * Note: this column has a database default value of: true
     * @var        boolean
     */
    protected $active;

    /**
     * The value for the nb_voix field.
     *
     * Note: this column has a database default value of: 1
     * @var        int
     */
    protected $nb_voix;

    /**
     * The value for the periodique field.
     *
     * @var        string
     */
    protected $periodique;

    /**
     * The value for the signe field.
     * +Vente-Perte_ou_achat
     * Note: this column has a database default value of: '+'
     * @var        string
     */
    protected $signe;

    /**
     * The value for the objet_beneficiaire field.
     *
     * Note: this column has a database default value of: 'membre'
     * @var        string
     */
    protected $objet_beneficiaire;

    /**
     * The value for the created_at field.
     *
     * @var        DateTime
     */
    protected $created_at;

    /**
     * The value for the updated_at field.
     *
     * @var        DateTime
     */
    protected $updated_at;

    /**
     * @var        ChildEntite
     */
    protected $aEntite;

    /**
     * @var        ChildCompte
     */
    protected $aCompte;

    /**
     * @var        ObjectCollection|ChildPrix[] Collection to store aggregation of ChildPrix objects.
     */
    protected $collPrixes;
    protected $collPrixesPartial;

    /**
     * @var        ObjectCollection|ChildPrestationslotPrestation[] Collection to store aggregation of ChildPrestationslotPrestation objects.
     */
    protected $collPrestationslotPrestations;
    protected $collPrestationslotPrestationsPartial;

    /**
     * @var        ObjectCollection|ChildServicerendu[] Collection to store aggregation of ChildServicerendu objects.
     */
    protected $collServicerendus;
    protected $collServicerendusPartial;

    /**
     * @var        ObjectCollection|ChildPrestationslot[] Cross Collection to store aggregation of ChildPrestationslot objects.
     */
    protected $collPrestationslots;

    /**
     * @var bool
     */
    protected $collPrestationslotsPartial;

    /**
     * Flag to prevent endless save loop, if this object is referenced
     * by another object which falls in this transaction.
     *
     * @var boolean
     */
    protected $alreadyInSave = false;

    // archivable behavior
    protected $archiveOnDelete = true;

    /**
     * An array of objects scheduled for deletion.
     * @var ObjectCollection|ChildPrestationslot[]
     */
    protected $prestationslotsScheduledForDeletion = null;

    /**
     * An array of objects scheduled for deletion.
     * @var ObjectCollection|ChildPrix[]
     */
    protected $prixesScheduledForDeletion = null;

    /**
     * An array of objects scheduled for deletion.
     * @var ObjectCollection|ChildPrestationslotPrestation[]
     */
    protected $prestationslotPrestationsScheduledForDeletion = null;

    /**
     * An array of objects scheduled for deletion.
     * @var ObjectCollection|ChildServicerendu[]
     */
    protected $servicerendusScheduledForDeletion = null;

    /**
     * Applies default values to this object.
     * This method should be called from the object's constructor (or
     * equivalent initialization method).
     * @see __construct()
     */
    public function applyDefaultValues()
    {
        $this->nom_groupe = '';
        $this->prixlibre = false;
        $this->quantite = false;
        $this->id_unite = '0';
        $this->active = true;
        $this->nb_voix = 1;
        $this->signe = '+';
        $this->objet_beneficiaire = 'membre';
    }

    /**
     * Initializes internal state of Base\Prestation object.
     * @see applyDefaults()
     */
    public function __construct()
    {
        $this->applyDefaultValues();
    }

    /**
     * Returns whether the object has been modified.
     *
     * @return boolean True if the object has been modified.
     */
    public function isModified()
    {
        return !!$this->modifiedColumns;
    }

    /**
     * Has specified column been modified?
     *
     * @param  string  $col column fully qualified name (TableMap::TYPE_COLNAME), e.g. Book::AUTHOR_ID
     * @return boolean True if $col has been modified.
     */
    public function isColumnModified($col)
    {
        return $this->modifiedColumns && isset($this->modifiedColumns[$col]);
    }

    /**
     * Get the columns that have been modified in this object.
     * @return array A unique list of the modified column names for this object.
     */
    public function getModifiedColumns()
    {
        return $this->modifiedColumns ? array_keys($this->modifiedColumns) : [];
    }

    /**
     * Returns whether the object has ever been saved.  This will
     * be false, if the object was retrieved from storage or was created
     * and then saved.
     *
     * @return boolean true, if the object has never been persisted.
     */
    public function isNew()
    {
        return $this->new;
    }

    /**
     * Setter for the isNew attribute.  This method will be called
     * by Propel-generated children and objects.
     *
     * @param boolean $b the state of the object.
     */
    public function setNew($b)
    {
        $this->new = (boolean) $b;
    }

    /**
     * Whether this object has been deleted.
     * @return boolean The deleted state of this object.
     */
    public function isDeleted()
    {
        return $this->deleted;
    }

    /**
     * Specify whether this object has been deleted.
     * @param  boolean $b The deleted state of this object.
     * @return void
     */
    public function setDeleted($b)
    {
        $this->deleted = (boolean) $b;
    }

    /**
     * Sets the modified state for the object to be false.
     * @param  string $col If supplied, only the specified column is reset.
     * @return void
     */
    public function resetModified($col = null)
    {
        if (null !== $col) {
            if (isset($this->modifiedColumns[$col])) {
                unset($this->modifiedColumns[$col]);
            }
        } else {
            $this->modifiedColumns = array();
        }
    }

    /**
     * Compares this with another <code>Prestation</code> instance.  If
     * <code>obj</code> is an instance of <code>Prestation</code>, delegates to
     * <code>equals(Prestation)</code>.  Otherwise, returns <code>false</code>.
     *
     * @param  mixed   $obj The object to compare to.
     * @return boolean Whether equal to the object specified.
     */
    public function equals($obj)
    {
        if (!$obj instanceof static) {
            return false;
        }

        if ($this === $obj) {
            return true;
        }

        if (null === $this->getPrimaryKey() || null === $obj->getPrimaryKey()) {
            return false;
        }

        return $this->getPrimaryKey() === $obj->getPrimaryKey();
    }

    /**
     * Get the associative array of the virtual columns in this object
     *
     * @return array
     */
    public function getVirtualColumns()
    {
        return $this->virtualColumns;
    }

    /**
     * Checks the existence of a virtual column in this object
     *
     * @param  string  $name The virtual column name
     * @return boolean
     */
    public function hasVirtualColumn($name)
    {
        return array_key_exists($name, $this->virtualColumns);
    }

    /**
     * Get the value of a virtual column in this object
     *
     * @param  string $name The virtual column name
     * @return mixed
     *
     * @throws PropelException
     */
    public function getVirtualColumn($name)
    {
        if (!$this->hasVirtualColumn($name)) {
            throw new PropelException(sprintf('Cannot get value of inexistent virtual column %s.', $name));
        }

        return $this->virtualColumns[$name];
    }

    /**
     * Set the value of a virtual column in this object
     *
     * @param string $name  The virtual column name
     * @param mixed  $value The value to give to the virtual column
     *
     * @return $this|Prestation The current object, for fluid interface
     */
    public function setVirtualColumn($name, $value)
    {
        $this->virtualColumns[$name] = $value;

        return $this;
    }

    /**
     * Logs a message using Propel::log().
     *
     * @param  string  $msg
     * @param  int     $priority One of the Propel::LOG_* logging levels
     * @return boolean
     */
    protected function log($msg, $priority = Propel::LOG_INFO)
    {
        return Propel::log(get_class($this) . ': ' . $msg, $priority);
    }

    /**
     * Export the current object properties to a string, using a given parser format
     * <code>
     * $book = BookQuery::create()->findPk(9012);
     * echo $book->exportTo('JSON');
     *  => {"Id":9012,"Title":"Don Juan","ISBN":"0140422161","Price":12.99,"PublisherId":1234,"AuthorId":5678}');
     * </code>
     *
     * @param  mixed   $parser                 A AbstractParser instance, or a format name ('XML', 'YAML', 'JSON', 'CSV')
     * @param  boolean $includeLazyLoadColumns (optional) Whether to include lazy load(ed) columns. Defaults to TRUE.
     * @return string  The exported data
     */
    public function exportTo($parser, $includeLazyLoadColumns = true)
    {
        if (!$parser instanceof AbstractParser) {
            $parser = AbstractParser::getParser($parser);
        }

        return $parser->fromArray($this->toArray(TableMap::TYPE_PHPNAME, $includeLazyLoadColumns, array(), true));
    }

    /**
     * Clean up internal collections prior to serializing
     * Avoids recursive loops that turn into segmentation faults when serializing
     */
    public function __sleep()
    {
        $this->clearAllReferences();

        $cls = new \ReflectionClass($this);
        $propertyNames = [];
        $serializableProperties = array_diff($cls->getProperties(), $cls->getProperties(\ReflectionProperty::IS_STATIC));

        foreach($serializableProperties as $property) {
            $propertyNames[] = $property->getName();
        }

        return $propertyNames;
    }

    /**
     * Get the [id_prestation] column value.
     *
     * @return string
     */
    public function getIdPrestation()
    {
        return $this->id_prestation;
    }

    /**
     * Get the [nom_groupe] column value.
     *
     * @return string
     */
    public function getNomGroupe()
    {
        return $this->nom_groupe;
    }

    /**
     * Get the [nom] column value.
     *
     * @return string
     */
    public function getNom()
    {
        return $this->nom;
    }

    /**
     * Get the [nomcourt] column value.
     *
     * @return string
     */
    public function getNomcourt()
    {
        return $this->nomcourt;
    }

    /**
     * Get the [descriptif] column value.
     *
     * @return string
     */
    public function getDescriptif()
    {
        return $this->descriptif;
    }

    /**
     * Get the [id_entite] column value.
     * Entite
     * @return string
     */
    public function getIdEntite()
    {
        return $this->id_entite;
    }

    /**
     * Get the [id_tva] column value.
     *
     * @return string
     */
    public function getIdTva()
    {
        return $this->id_tva;
    }

    /**
     * Get the [retard_jours] column value.
     * pour cotisation en retard en jours
     * @return int
     */
    public function getRetardJours()
    {
        return $this->retard_jours;
    }

    /**
     * Get the [nombre_numero] column value.
     *
     * @return int
     */
    public function getNombreNumero()
    {
        return $this->nombre_numero;
    }

    /**
     * Get the [prochain_numero] column value.
     *
     * @return int
     */
    public function getProchainNumero()
    {
        return $this->prochain_numero;
    }

    /**
     * Get the [prixlibre] column value.
     *
     * @return boolean
     */
    public function getPrixlibre()
    {
        return $this->prixlibre;
    }

    /**
     * Get the [prixlibre] column value.
     *
     * @return boolean
     */
    public function isPrixlibre()
    {
        return $this->getPrixlibre();
    }

    /**
     * Get the [quantite] column value.
     *
     * @return boolean
     */
    public function getQuantite()
    {
        return $this->quantite;
    }

    /**
     * Get the [quantite] column value.
     *
     * @return boolean
     */
    public function isQuantite()
    {
        return $this->getQuantite();
    }

    /**
     * Get the [id_unite] column value.
     *
     * @return string
     */
    public function getIdUnite()
    {
        return $this->id_unite;
    }

    /**
     * Get the [id_compte] column value.
     *
     * @return string
     */
    public function getIdCompte()
    {
        return $this->id_compte;
    }

    /**
     * Get the [prestation_type] column value.
     *
     * @return int
     */
    public function getPrestationType()
    {
        return $this->prestation_type;
    }

    /**
     * Get the [active] column value.
     *
     * @return boolean
     */
    public function getActive()
    {
        return $this->active;
    }

    /**
     * Get the [active] column value.
     *
     * @return boolean
     */
    public function isActive()
    {
        return $this->getActive();
    }

    /**
     * Get the [nb_voix] column value.
     *
     * @return int
     */
    public function getNbVoix()
    {
        return $this->nb_voix;
    }

    /**
     * Get the [periodique] column value.
     *
     * @return string
     */
    public function getPeriodique()
    {
        return $this->periodique;
    }

    /**
     * Get the [signe] column value.
     * +Vente-Perte_ou_achat
     * @return string
     */
    public function getSigne()
    {
        return $this->signe;
    }

    /**
     * Get the [objet_beneficiaire] column value.
     *
     * @return string
     */
    public function getObjetBeneficiaire()
    {
        return $this->objet_beneficiaire;
    }

    /**
     * Get the [optionally formatted] temporal [created_at] column value.
     *
     *
     * @param      string $format The date/time format string (either date()-style or strftime()-style).
     *                            If format is NULL, then the raw DateTime object will be returned.
     *
     * @return string|DateTime Formatted date/time value as string or DateTime object (if format is NULL), NULL if column is NULL, and 0 if column value is 0000-00-00 00:00:00
     *
     * @throws PropelException - if unable to parse/validate the date/time value.
     */
    public function getCreatedAt($format = NULL)
    {
        if ($format === null) {
            return $this->created_at;
        } else {
            return $this->created_at instanceof \DateTimeInterface ? $this->created_at->format($format) : null;
        }
    }

    /**
     * Get the [optionally formatted] temporal [updated_at] column value.
     *
     *
     * @param      string $format The date/time format string (either date()-style or strftime()-style).
     *                            If format is NULL, then the raw DateTime object will be returned.
     *
     * @return string|DateTime Formatted date/time value as string or DateTime object (if format is NULL), NULL if column is NULL, and 0 if column value is 0000-00-00 00:00:00
     *
     * @throws PropelException - if unable to parse/validate the date/time value.
     */
    public function getUpdatedAt($format = NULL)
    {
        if ($format === null) {
            return $this->updated_at;
        } else {
            return $this->updated_at instanceof \DateTimeInterface ? $this->updated_at->format($format) : null;
        }
    }

    /**
     * Set the value of [id_prestation] column.
     *
     * @param string $v new value
     * @return $this|\Prestation The current object (for fluent API support)
     */
    public function setIdPrestation($v)
    {
        if ($v !== null) {
            $v = (string) $v;
        }

        if ($this->id_prestation !== $v) {
            $this->id_prestation = $v;
            $this->modifiedColumns[PrestationTableMap::COL_ID_PRESTATION] = true;
        }

        return $this;
    } // setIdPrestation()

    /**
     * Set the value of [nom_groupe] column.
     *
     * @param string $v new value
     * @return $this|\Prestation The current object (for fluent API support)
     */
    public function setNomGroupe($v)
    {
        if ($v !== null) {
            $v = (string) $v;
        }

        if ($this->nom_groupe !== $v) {
            $this->nom_groupe = $v;
            $this->modifiedColumns[PrestationTableMap::COL_NOM_GROUPE] = true;
        }

        return $this;
    } // setNomGroupe()

    /**
     * Set the value of [nom] column.
     *
     * @param string $v new value
     * @return $this|\Prestation The current object (for fluent API support)
     */
    public function setNom($v)
    {
        if ($v !== null) {
            $v = (string) $v;
        }

        if ($this->nom !== $v) {
            $this->nom = $v;
            $this->modifiedColumns[PrestationTableMap::COL_NOM] = true;
        }

        return $this;
    } // setNom()

    /**
     * Set the value of [nomcourt] column.
     *
     * @param string $v new value
     * @return $this|\Prestation The current object (for fluent API support)
     */
    public function setNomcourt($v)
    {
        if ($v !== null) {
            $v = (string) $v;
        }

        if ($this->nomcourt !== $v) {
            $this->nomcourt = $v;
            $this->modifiedColumns[PrestationTableMap::COL_NOMCOURT] = true;
        }

        return $this;
    } // setNomcourt()

    /**
     * Set the value of [descriptif] column.
     *
     * @param string $v new value
     * @return $this|\Prestation The current object (for fluent API support)
     */
    public function setDescriptif($v)
    {
        if ($v !== null) {
            $v = (string) $v;
        }

        if ($this->descriptif !== $v) {
            $this->descriptif = $v;
            $this->modifiedColumns[PrestationTableMap::COL_DESCRIPTIF] = true;
        }

        return $this;
    } // setDescriptif()

    /**
     * Set the value of [id_entite] column.
     * Entite
     * @param string $v new value
     * @return $this|\Prestation The current object (for fluent API support)
     */
    public function setIdEntite($v)
    {
        if ($v !== null) {
            $v = (string) $v;
        }

        if ($this->id_entite !== $v) {
            $this->id_entite = $v;
            $this->modifiedColumns[PrestationTableMap::COL_ID_ENTITE] = true;
        }

        if ($this->aEntite !== null && $this->aEntite->getIdEntite() !== $v) {
            $this->aEntite = null;
        }

        return $this;
    } // setIdEntite()

    /**
     * Set the value of [id_tva] column.
     *
     * @param string $v new value
     * @return $this|\Prestation The current object (for fluent API support)
     */
    public function setIdTva($v)
    {
        if ($v !== null) {
            $v = (string) $v;
        }

        if ($this->id_tva !== $v) {
            $this->id_tva = $v;
            $this->modifiedColumns[PrestationTableMap::COL_ID_TVA] = true;
        }

        return $this;
    } // setIdTva()

    /**
     * Set the value of [retard_jours] column.
     * pour cotisation en retard en jours
     * @param int $v new value
     * @return $this|\Prestation The current object (for fluent API support)
     */
    public function setRetardJours($v)
    {
        if ($v !== null) {
            $v = (int) $v;
        }

        if ($this->retard_jours !== $v) {
            $this->retard_jours = $v;
            $this->modifiedColumns[PrestationTableMap::COL_RETARD_JOURS] = true;
        }

        return $this;
    } // setRetardJours()

    /**
     * Set the value of [nombre_numero] column.
     *
     * @param int $v new value
     * @return $this|\Prestation The current object (for fluent API support)
     */
    public function setNombreNumero($v)
    {
        if ($v !== null) {
            $v = (int) $v;
        }

        if ($this->nombre_numero !== $v) {
            $this->nombre_numero = $v;
            $this->modifiedColumns[PrestationTableMap::COL_NOMBRE_NUMERO] = true;
        }

        return $this;
    } // setNombreNumero()

    /**
     * Set the value of [prochain_numero] column.
     *
     * @param int $v new value
     * @return $this|\Prestation The current object (for fluent API support)
     */
    public function setProchainNumero($v)
    {
        if ($v !== null) {
            $v = (int) $v;
        }

        if ($this->prochain_numero !== $v) {
            $this->prochain_numero = $v;
            $this->modifiedColumns[PrestationTableMap::COL_PROCHAIN_NUMERO] = true;
        }

        return $this;
    } // setProchainNumero()

    /**
     * Sets the value of the [prixlibre] column.
     * Non-boolean arguments are converted using the following rules:
     *   * 1, '1', 'true',  'on',  and 'yes' are converted to boolean true
     *   * 0, '0', 'false', 'off', and 'no'  are converted to boolean false
     * Check on string values is case insensitive (so 'FaLsE' is seen as 'false').
     *
     * @param  boolean|integer|string $v The new value
     * @return $this|\Prestation The current object (for fluent API support)
     */
    public function setPrixlibre($v)
    {
        if ($v !== null) {
            if (is_string($v)) {
                $v = in_array(strtolower($v), array('false', 'off', '-', 'no', 'n', '0', '')) ? false : true;
            } else {
                $v = (boolean) $v;
            }
        }

        if ($this->prixlibre !== $v) {
            $this->prixlibre = $v;
            $this->modifiedColumns[PrestationTableMap::COL_PRIXLIBRE] = true;
        }

        return $this;
    } // setPrixlibre()

    /**
     * Sets the value of the [quantite] column.
     * Non-boolean arguments are converted using the following rules:
     *   * 1, '1', 'true',  'on',  and 'yes' are converted to boolean true
     *   * 0, '0', 'false', 'off', and 'no'  are converted to boolean false
     * Check on string values is case insensitive (so 'FaLsE' is seen as 'false').
     *
     * @param  boolean|integer|string $v The new value
     * @return $this|\Prestation The current object (for fluent API support)
     */
    public function setQuantite($v)
    {
        if ($v !== null) {
            if (is_string($v)) {
                $v = in_array(strtolower($v), array('false', 'off', '-', 'no', 'n', '0', '')) ? false : true;
            } else {
                $v = (boolean) $v;
            }
        }

        if ($this->quantite !== $v) {
            $this->quantite = $v;
            $this->modifiedColumns[PrestationTableMap::COL_QUANTITE] = true;
        }

        return $this;
    } // setQuantite()

    /**
     * Set the value of [id_unite] column.
     *
     * @param string $v new value
     * @return $this|\Prestation The current object (for fluent API support)
     */
    public function setIdUnite($v)
    {
        if ($v !== null) {
            $v = (string) $v;
        }

        if ($this->id_unite !== $v) {
            $this->id_unite = $v;
            $this->modifiedColumns[PrestationTableMap::COL_ID_UNITE] = true;
        }

        return $this;
    } // setIdUnite()

    /**
     * Set the value of [id_compte] column.
     *
     * @param string $v new value
     * @return $this|\Prestation The current object (for fluent API support)
     */
    public function setIdCompte($v)
    {
        if ($v !== null) {
            $v = (string) $v;
        }

        if ($this->id_compte !== $v) {
            $this->id_compte = $v;
            $this->modifiedColumns[PrestationTableMap::COL_ID_COMPTE] = true;
        }

        if ($this->aCompte !== null && $this->aCompte->getIdCompte() !== $v) {
            $this->aCompte = null;
        }

        return $this;
    } // setIdCompte()

    /**
     * Set the value of [prestation_type] column.
     *
     * @param int $v new value
     * @return $this|\Prestation The current object (for fluent API support)
     */
    public function setPrestationType($v)
    {
        if ($v !== null) {
            $v = (int) $v;
        }

        if ($this->prestation_type !== $v) {
            $this->prestation_type = $v;
            $this->modifiedColumns[PrestationTableMap::COL_PRESTATION_TYPE] = true;
        }

        return $this;
    } // setPrestationType()

    /**
     * Sets the value of the [active] column.
     * Non-boolean arguments are converted using the following rules:
     *   * 1, '1', 'true',  'on',  and 'yes' are converted to boolean true
     *   * 0, '0', 'false', 'off', and 'no'  are converted to boolean false
     * Check on string values is case insensitive (so 'FaLsE' is seen as 'false').
     *
     * @param  boolean|integer|string $v The new value
     * @return $this|\Prestation The current object (for fluent API support)
     */
    public function setActive($v)
    {
        if ($v !== null) {
            if (is_string($v)) {
                $v = in_array(strtolower($v), array('false', 'off', '-', 'no', 'n', '0', '')) ? false : true;
            } else {
                $v = (boolean) $v;
            }
        }

        if ($this->active !== $v) {
            $this->active = $v;
            $this->modifiedColumns[PrestationTableMap::COL_ACTIVE] = true;
        }

        return $this;
    } // setActive()

    /**
     * Set the value of [nb_voix] column.
     *
     * @param int $v new value
     * @return $this|\Prestation The current object (for fluent API support)
     */
    public function setNbVoix($v)
    {
        if ($v !== null) {
            $v = (int) $v;
        }

        if ($this->nb_voix !== $v) {
            $this->nb_voix = $v;
            $this->modifiedColumns[PrestationTableMap::COL_NB_VOIX] = true;
        }

        return $this;
    } // setNbVoix()

    /**
     * Set the value of [periodique] column.
     *
     * @param string $v new value
     * @return $this|\Prestation The current object (for fluent API support)
     */
    public function setPeriodique($v)
    {
        if ($v !== null) {
            $v = (string) $v;
        }

        if ($this->periodique !== $v) {
            $this->periodique = $v;
            $this->modifiedColumns[PrestationTableMap::COL_PERIODIQUE] = true;
        }

        return $this;
    } // setPeriodique()

    /**
     * Set the value of [signe] column.
     * +Vente-Perte_ou_achat
     * @param string $v new value
     * @return $this|\Prestation The current object (for fluent API support)
     */
    public function setSigne($v)
    {
        if ($v !== null) {
            $v = (string) $v;
        }

        if ($this->signe !== $v) {
            $this->signe = $v;
            $this->modifiedColumns[PrestationTableMap::COL_SIGNE] = true;
        }

        return $this;
    } // setSigne()

    /**
     * Set the value of [objet_beneficiaire] column.
     *
     * @param string $v new value
     * @return $this|\Prestation The current object (for fluent API support)
     */
    public function setObjetBeneficiaire($v)
    {
        if ($v !== null) {
            $v = (string) $v;
        }

        if ($this->objet_beneficiaire !== $v) {
            $this->objet_beneficiaire = $v;
            $this->modifiedColumns[PrestationTableMap::COL_OBJET_BENEFICIAIRE] = true;
        }

        return $this;
    } // setObjetBeneficiaire()

    /**
     * Sets the value of [created_at] column to a normalized version of the date/time value specified.
     *
     * @param  mixed $v string, integer (timestamp), or \DateTimeInterface value.
     *               Empty strings are treated as NULL.
     * @return $this|\Prestation The current object (for fluent API support)
     */
    public function setCreatedAt($v)
    {
        $dt = PropelDateTime::newInstance($v, null, 'DateTime');
        if ($this->created_at !== null || $dt !== null) {
            if ($this->created_at === null || $dt === null || $dt->format("Y-m-d H:i:s.u") !== $this->created_at->format("Y-m-d H:i:s.u")) {
                $this->created_at = $dt === null ? null : clone $dt;
                $this->modifiedColumns[PrestationTableMap::COL_CREATED_AT] = true;
            }
        } // if either are not null

        return $this;
    } // setCreatedAt()

    /**
     * Sets the value of [updated_at] column to a normalized version of the date/time value specified.
     *
     * @param  mixed $v string, integer (timestamp), or \DateTimeInterface value.
     *               Empty strings are treated as NULL.
     * @return $this|\Prestation The current object (for fluent API support)
     */
    public function setUpdatedAt($v)
    {
        $dt = PropelDateTime::newInstance($v, null, 'DateTime');
        if ($this->updated_at !== null || $dt !== null) {
            if ($this->updated_at === null || $dt === null || $dt->format("Y-m-d H:i:s.u") !== $this->updated_at->format("Y-m-d H:i:s.u")) {
                $this->updated_at = $dt === null ? null : clone $dt;
                $this->modifiedColumns[PrestationTableMap::COL_UPDATED_AT] = true;
            }
        } // if either are not null

        return $this;
    } // setUpdatedAt()

    /**
     * Indicates whether the columns in this object are only set to default values.
     *
     * This method can be used in conjunction with isModified() to indicate whether an object is both
     * modified _and_ has some values set which are non-default.
     *
     * @return boolean Whether the columns in this object are only been set with default values.
     */
    public function hasOnlyDefaultValues()
    {
            if ($this->nom_groupe !== '') {
                return false;
            }

            if ($this->prixlibre !== false) {
                return false;
            }

            if ($this->quantite !== false) {
                return false;
            }

            if ($this->id_unite !== '0') {
                return false;
            }

            if ($this->active !== true) {
                return false;
            }

            if ($this->nb_voix !== 1) {
                return false;
            }

            if ($this->signe !== '+') {
                return false;
            }

            if ($this->objet_beneficiaire !== 'membre') {
                return false;
            }

        // otherwise, everything was equal, so return TRUE
        return true;
    } // hasOnlyDefaultValues()

    /**
     * Hydrates (populates) the object variables with values from the database resultset.
     *
     * An offset (0-based "start column") is specified so that objects can be hydrated
     * with a subset of the columns in the resultset rows.  This is needed, for example,
     * for results of JOIN queries where the resultset row includes columns from two or
     * more tables.
     *
     * @param array   $row       The row returned by DataFetcher->fetch().
     * @param int     $startcol  0-based offset column which indicates which restultset column to start with.
     * @param boolean $rehydrate Whether this object is being re-hydrated from the database.
     * @param string  $indexType The index type of $row. Mostly DataFetcher->getIndexType().
                                  One of the class type constants TableMap::TYPE_PHPNAME, TableMap::TYPE_CAMELNAME
     *                            TableMap::TYPE_COLNAME, TableMap::TYPE_FIELDNAME, TableMap::TYPE_NUM.
     *
     * @return int             next starting column
     * @throws PropelException - Any caught Exception will be rewrapped as a PropelException.
     */
    public function hydrate($row, $startcol = 0, $rehydrate = false, $indexType = TableMap::TYPE_NUM)
    {
        try {

            $col = $row[TableMap::TYPE_NUM == $indexType ? 0 + $startcol : PrestationTableMap::translateFieldName('IdPrestation', TableMap::TYPE_PHPNAME, $indexType)];
            $this->id_prestation = (null !== $col) ? (string) $col : null;

            $col = $row[TableMap::TYPE_NUM == $indexType ? 1 + $startcol : PrestationTableMap::translateFieldName('NomGroupe', TableMap::TYPE_PHPNAME, $indexType)];
            $this->nom_groupe = (null !== $col) ? (string) $col : null;

            $col = $row[TableMap::TYPE_NUM == $indexType ? 2 + $startcol : PrestationTableMap::translateFieldName('Nom', TableMap::TYPE_PHPNAME, $indexType)];
            $this->nom = (null !== $col) ? (string) $col : null;

            $col = $row[TableMap::TYPE_NUM == $indexType ? 3 + $startcol : PrestationTableMap::translateFieldName('Nomcourt', TableMap::TYPE_PHPNAME, $indexType)];
            $this->nomcourt = (null !== $col) ? (string) $col : null;

            $col = $row[TableMap::TYPE_NUM == $indexType ? 4 + $startcol : PrestationTableMap::translateFieldName('Descriptif', TableMap::TYPE_PHPNAME, $indexType)];
            $this->descriptif = (null !== $col) ? (string) $col : null;

            $col = $row[TableMap::TYPE_NUM == $indexType ? 5 + $startcol : PrestationTableMap::translateFieldName('IdEntite', TableMap::TYPE_PHPNAME, $indexType)];
            $this->id_entite = (null !== $col) ? (string) $col : null;

            $col = $row[TableMap::TYPE_NUM == $indexType ? 6 + $startcol : PrestationTableMap::translateFieldName('IdTva', TableMap::TYPE_PHPNAME, $indexType)];
            $this->id_tva = (null !== $col) ? (string) $col : null;

            $col = $row[TableMap::TYPE_NUM == $indexType ? 7 + $startcol : PrestationTableMap::translateFieldName('RetardJours', TableMap::TYPE_PHPNAME, $indexType)];
            $this->retard_jours = (null !== $col) ? (int) $col : null;

            $col = $row[TableMap::TYPE_NUM == $indexType ? 8 + $startcol : PrestationTableMap::translateFieldName('NombreNumero', TableMap::TYPE_PHPNAME, $indexType)];
            $this->nombre_numero = (null !== $col) ? (int) $col : null;

            $col = $row[TableMap::TYPE_NUM == $indexType ? 9 + $startcol : PrestationTableMap::translateFieldName('ProchainNumero', TableMap::TYPE_PHPNAME, $indexType)];
            $this->prochain_numero = (null !== $col) ? (int) $col : null;

            $col = $row[TableMap::TYPE_NUM == $indexType ? 10 + $startcol : PrestationTableMap::translateFieldName('Prixlibre', TableMap::TYPE_PHPNAME, $indexType)];
            $this->prixlibre = (null !== $col) ? (boolean) $col : null;

            $col = $row[TableMap::TYPE_NUM == $indexType ? 11 + $startcol : PrestationTableMap::translateFieldName('Quantite', TableMap::TYPE_PHPNAME, $indexType)];
            $this->quantite = (null !== $col) ? (boolean) $col : null;

            $col = $row[TableMap::TYPE_NUM == $indexType ? 12 + $startcol : PrestationTableMap::translateFieldName('IdUnite', TableMap::TYPE_PHPNAME, $indexType)];
            $this->id_unite = (null !== $col) ? (string) $col : null;

            $col = $row[TableMap::TYPE_NUM == $indexType ? 13 + $startcol : PrestationTableMap::translateFieldName('IdCompte', TableMap::TYPE_PHPNAME, $indexType)];
            $this->id_compte = (null !== $col) ? (string) $col : null;

            $col = $row[TableMap::TYPE_NUM == $indexType ? 14 + $startcol : PrestationTableMap::translateFieldName('PrestationType', TableMap::TYPE_PHPNAME, $indexType)];
            $this->prestation_type = (null !== $col) ? (int) $col : null;

            $col = $row[TableMap::TYPE_NUM == $indexType ? 15 + $startcol : PrestationTableMap::translateFieldName('Active', TableMap::TYPE_PHPNAME, $indexType)];
            $this->active = (null !== $col) ? (boolean) $col : null;

            $col = $row[TableMap::TYPE_NUM == $indexType ? 16 + $startcol : PrestationTableMap::translateFieldName('NbVoix', TableMap::TYPE_PHPNAME, $indexType)];
            $this->nb_voix = (null !== $col) ? (int) $col : null;

            $col = $row[TableMap::TYPE_NUM == $indexType ? 17 + $startcol : PrestationTableMap::translateFieldName('Periodique', TableMap::TYPE_PHPNAME, $indexType)];
            $this->periodique = (null !== $col) ? (string) $col : null;

            $col = $row[TableMap::TYPE_NUM == $indexType ? 18 + $startcol : PrestationTableMap::translateFieldName('Signe', TableMap::TYPE_PHPNAME, $indexType)];
            $this->signe = (null !== $col) ? (string) $col : null;

            $col = $row[TableMap::TYPE_NUM == $indexType ? 19 + $startcol : PrestationTableMap::translateFieldName('ObjetBeneficiaire', TableMap::TYPE_PHPNAME, $indexType)];
            $this->objet_beneficiaire = (null !== $col) ? (string) $col : null;

            $col = $row[TableMap::TYPE_NUM == $indexType ? 20 + $startcol : PrestationTableMap::translateFieldName('CreatedAt', TableMap::TYPE_PHPNAME, $indexType)];
            if ($col === '0000-00-00 00:00:00') {
                $col = null;
            }
            $this->created_at = (null !== $col) ? PropelDateTime::newInstance($col, null, 'DateTime') : null;

            $col = $row[TableMap::TYPE_NUM == $indexType ? 21 + $startcol : PrestationTableMap::translateFieldName('UpdatedAt', TableMap::TYPE_PHPNAME, $indexType)];
            if ($col === '0000-00-00 00:00:00') {
                $col = null;
            }
            $this->updated_at = (null !== $col) ? PropelDateTime::newInstance($col, null, 'DateTime') : null;
            $this->resetModified();

            $this->setNew(false);

            if ($rehydrate) {
                $this->ensureConsistency();
            }

            return $startcol + 22; // 22 = PrestationTableMap::NUM_HYDRATE_COLUMNS.

        } catch (Exception $e) {
            throw new PropelException(sprintf('Error populating %s object', '\\Prestation'), 0, $e);
        }
    }

    /**
     * Checks and repairs the internal consistency of the object.
     *
     * This method is executed after an already-instantiated object is re-hydrated
     * from the database.  It exists to check any foreign keys to make sure that
     * the objects related to the current object are correct based on foreign key.
     *
     * You can override this method in the stub class, but you should always invoke
     * the base method from the overridden method (i.e. parent::ensureConsistency()),
     * in case your model changes.
     *
     * @throws PropelException
     */
    public function ensureConsistency()
    {
        if ($this->aEntite !== null && $this->id_entite !== $this->aEntite->getIdEntite()) {
            $this->aEntite = null;
        }
        if ($this->aCompte !== null && $this->id_compte !== $this->aCompte->getIdCompte()) {
            $this->aCompte = null;
        }
    } // ensureConsistency

    /**
     * Reloads this object from datastore based on primary key and (optionally) resets all associated objects.
     *
     * This will only work if the object has been saved and has a valid primary key set.
     *
     * @param      boolean $deep (optional) Whether to also de-associated any related objects.
     * @param      ConnectionInterface $con (optional) The ConnectionInterface connection to use.
     * @return void
     * @throws PropelException - if this object is deleted, unsaved or doesn't have pk match in db
     */
    public function reload($deep = false, ConnectionInterface $con = null)
    {
        if ($this->isDeleted()) {
            throw new PropelException("Cannot reload a deleted object.");
        }

        if ($this->isNew()) {
            throw new PropelException("Cannot reload an unsaved object.");
        }

        if ($con === null) {
            $con = Propel::getServiceContainer()->getReadConnection(PrestationTableMap::DATABASE_NAME);
        }

        // We don't need to alter the object instance pool; we're just modifying this instance
        // already in the pool.

        $dataFetcher = ChildPrestationQuery::create(null, $this->buildPkeyCriteria())->setFormatter(ModelCriteria::FORMAT_STATEMENT)->find($con);
        $row = $dataFetcher->fetch();
        $dataFetcher->close();
        if (!$row) {
            throw new PropelException('Cannot find matching row in the database to reload object values.');
        }
        $this->hydrate($row, 0, true, $dataFetcher->getIndexType()); // rehydrate

        if ($deep) {  // also de-associate any related objects?

            $this->aEntite = null;
            $this->aCompte = null;
            $this->collPrixes = null;

            $this->collPrestationslotPrestations = null;

            $this->collServicerendus = null;

            $this->collPrestationslots = null;
        } // if (deep)
    }

    /**
     * Removes this object from datastore and sets delete attribute.
     *
     * @param      ConnectionInterface $con
     * @return void
     * @throws PropelException
     * @see Prestation::setDeleted()
     * @see Prestation::isDeleted()
     */
    public function delete(ConnectionInterface $con = null)
    {
        if ($this->isDeleted()) {
            throw new PropelException("This object has already been deleted.");
        }

        if ($con === null) {
            $con = Propel::getServiceContainer()->getWriteConnection(PrestationTableMap::DATABASE_NAME);
        }

        $con->transaction(function () use ($con) {
            $deleteQuery = ChildPrestationQuery::create()
                ->filterByPrimaryKey($this->getPrimaryKey());
            $ret = $this->preDelete($con);
            // archivable behavior
            if ($ret) {
                if ($this->archiveOnDelete) {
                    // do nothing yet. The object will be archived later when calling ChildPrestationQuery::delete().
                } else {
                    $deleteQuery->setArchiveOnDelete(false);
                    $this->archiveOnDelete = true;
                }
            }

            if ($ret) {
                $deleteQuery->delete($con);
                $this->postDelete($con);
                $this->setDeleted(true);
            }
        });
    }

    /**
     * Persists this object to the database.
     *
     * If the object is new, it inserts it; otherwise an update is performed.
     * All modified related objects will also be persisted in the doSave()
     * method.  This method wraps all precipitate database operations in a
     * single transaction.
     *
     * @param      ConnectionInterface $con
     * @return int             The number of rows affected by this insert/update and any referring fk objects' save() operations.
     * @throws PropelException
     * @see doSave()
     */
    public function save(ConnectionInterface $con = null)
    {
        if ($this->isDeleted()) {
            throw new PropelException("You cannot save an object that has been deleted.");
        }

        if ($this->alreadyInSave) {
            return 0;
        }

        if ($con === null) {
            $con = Propel::getServiceContainer()->getWriteConnection(PrestationTableMap::DATABASE_NAME);
        }

        return $con->transaction(function () use ($con) {
            $ret = $this->preSave($con);
            $isInsert = $this->isNew();
            if ($isInsert) {
                $ret = $ret && $this->preInsert($con);
                // timestampable behavior

                if (!$this->isColumnModified(PrestationTableMap::COL_CREATED_AT)) {
                    $this->setCreatedAt(\Propel\Runtime\Util\PropelDateTime::createHighPrecision());
                }
                if (!$this->isColumnModified(PrestationTableMap::COL_UPDATED_AT)) {
                    $this->setUpdatedAt(\Propel\Runtime\Util\PropelDateTime::createHighPrecision());
                }
            } else {
                $ret = $ret && $this->preUpdate($con);
                // timestampable behavior
                if ($this->isModified() && !$this->isColumnModified(PrestationTableMap::COL_UPDATED_AT)) {
                    $this->setUpdatedAt(\Propel\Runtime\Util\PropelDateTime::createHighPrecision());
                }
            }
            if ($ret) {
                $affectedRows = $this->doSave($con);
                if ($isInsert) {
                    $this->postInsert($con);
                } else {
                    $this->postUpdate($con);
                }
                $this->postSave($con);
                PrestationTableMap::addInstanceToPool($this);
            } else {
                $affectedRows = 0;
            }

            return $affectedRows;
        });
    }

    /**
     * Performs the work of inserting or updating the row in the database.
     *
     * If the object is new, it inserts it; otherwise an update is performed.
     * All related objects are also updated in this method.
     *
     * @param      ConnectionInterface $con
     * @return int             The number of rows affected by this insert/update and any referring fk objects' save() operations.
     * @throws PropelException
     * @see save()
     */
    protected function doSave(ConnectionInterface $con)
    {
        $affectedRows = 0; // initialize var to track total num of affected rows
        if (!$this->alreadyInSave) {
            $this->alreadyInSave = true;

            // We call the save method on the following object(s) if they
            // were passed to this object by their corresponding set
            // method.  This object relates to these object(s) by a
            // foreign key reference.

            if ($this->aEntite !== null) {
                if ($this->aEntite->isModified() || $this->aEntite->isNew()) {
                    $affectedRows += $this->aEntite->save($con);
                }
                $this->setEntite($this->aEntite);
            }

            if ($this->aCompte !== null) {
                if ($this->aCompte->isModified() || $this->aCompte->isNew()) {
                    $affectedRows += $this->aCompte->save($con);
                }
                $this->setCompte($this->aCompte);
            }

            if ($this->isNew() || $this->isModified()) {
                // persist changes
                if ($this->isNew()) {
                    $this->doInsert($con);
                    $affectedRows += 1;
                } else {
                    $affectedRows += $this->doUpdate($con);
                }
                $this->resetModified();
            }

            if ($this->prestationslotsScheduledForDeletion !== null) {
                if (!$this->prestationslotsScheduledForDeletion->isEmpty()) {
                    $pks = array();
                    foreach ($this->prestationslotsScheduledForDeletion as $entry) {
                        $entryPk = [];

                        $entryPk[1] = $this->getIdPrestation();
                        $entryPk[0] = $entry->getIdPrestationslot();
                        $pks[] = $entryPk;
                    }

                    \PrestationslotPrestationQuery::create()
                        ->filterByPrimaryKeys($pks)
                        ->delete($con);

                    $this->prestationslotsScheduledForDeletion = null;
                }

            }

            if ($this->collPrestationslots) {
                foreach ($this->collPrestationslots as $prestationslot) {
                    if (!$prestationslot->isDeleted() && ($prestationslot->isNew() || $prestationslot->isModified())) {
                        $prestationslot->save($con);
                    }
                }
            }


            if ($this->prixesScheduledForDeletion !== null) {
                if (!$this->prixesScheduledForDeletion->isEmpty()) {
                    \PrixQuery::create()
                        ->filterByPrimaryKeys($this->prixesScheduledForDeletion->getPrimaryKeys(false))
                        ->delete($con);
                    $this->prixesScheduledForDeletion = null;
                }
            }

            if ($this->collPrixes !== null) {
                foreach ($this->collPrixes as $referrerFK) {
                    if (!$referrerFK->isDeleted() && ($referrerFK->isNew() || $referrerFK->isModified())) {
                        $affectedRows += $referrerFK->save($con);
                    }
                }
            }

            if ($this->prestationslotPrestationsScheduledForDeletion !== null) {
                if (!$this->prestationslotPrestationsScheduledForDeletion->isEmpty()) {
                    \PrestationslotPrestationQuery::create()
                        ->filterByPrimaryKeys($this->prestationslotPrestationsScheduledForDeletion->getPrimaryKeys(false))
                        ->delete($con);
                    $this->prestationslotPrestationsScheduledForDeletion = null;
                }
            }

            if ($this->collPrestationslotPrestations !== null) {
                foreach ($this->collPrestationslotPrestations as $referrerFK) {
                    if (!$referrerFK->isDeleted() && ($referrerFK->isNew() || $referrerFK->isModified())) {
                        $affectedRows += $referrerFK->save($con);
                    }
                }
            }

            if ($this->servicerendusScheduledForDeletion !== null) {
                if (!$this->servicerendusScheduledForDeletion->isEmpty()) {
                    \ServicerenduQuery::create()
                        ->filterByPrimaryKeys($this->servicerendusScheduledForDeletion->getPrimaryKeys(false))
                        ->delete($con);
                    $this->servicerendusScheduledForDeletion = null;
                }
            }

            if ($this->collServicerendus !== null) {
                foreach ($this->collServicerendus as $referrerFK) {
                    if (!$referrerFK->isDeleted() && ($referrerFK->isNew() || $referrerFK->isModified())) {
                        $affectedRows += $referrerFK->save($con);
                    }
                }
            }

            $this->alreadyInSave = false;

        }

        return $affectedRows;
    } // doSave()

    /**
     * Insert the row in the database.
     *
     * @param      ConnectionInterface $con
     *
     * @throws PropelException
     * @see doSave()
     */
    protected function doInsert(ConnectionInterface $con)
    {
        $modifiedColumns = array();
        $index = 0;

        $this->modifiedColumns[PrestationTableMap::COL_ID_PRESTATION] = true;
        if (null !== $this->id_prestation) {
            throw new PropelException('Cannot insert a value for auto-increment primary key (' . PrestationTableMap::COL_ID_PRESTATION . ')');
        }

         // check the columns in natural order for more readable SQL queries
        if ($this->isColumnModified(PrestationTableMap::COL_ID_PRESTATION)) {
            $modifiedColumns[':p' . $index++]  = '`id_prestation`';
        }
        if ($this->isColumnModified(PrestationTableMap::COL_NOM_GROUPE)) {
            $modifiedColumns[':p' . $index++]  = '`nom_groupe`';
        }
        if ($this->isColumnModified(PrestationTableMap::COL_NOM)) {
            $modifiedColumns[':p' . $index++]  = '`nom`';
        }
        if ($this->isColumnModified(PrestationTableMap::COL_NOMCOURT)) {
            $modifiedColumns[':p' . $index++]  = '`nomcourt`';
        }
        if ($this->isColumnModified(PrestationTableMap::COL_DESCRIPTIF)) {
            $modifiedColumns[':p' . $index++]  = '`descriptif`';
        }
        if ($this->isColumnModified(PrestationTableMap::COL_ID_ENTITE)) {
            $modifiedColumns[':p' . $index++]  = '`id_entite`';
        }
        if ($this->isColumnModified(PrestationTableMap::COL_ID_TVA)) {
            $modifiedColumns[':p' . $index++]  = '`id_tva`';
        }
        if ($this->isColumnModified(PrestationTableMap::COL_RETARD_JOURS)) {
            $modifiedColumns[':p' . $index++]  = '`retard_jours`';
        }
        if ($this->isColumnModified(PrestationTableMap::COL_NOMBRE_NUMERO)) {
            $modifiedColumns[':p' . $index++]  = '`nombre_numero`';
        }
        if ($this->isColumnModified(PrestationTableMap::COL_PROCHAIN_NUMERO)) {
            $modifiedColumns[':p' . $index++]  = '`prochain_numero`';
        }
        if ($this->isColumnModified(PrestationTableMap::COL_PRIXLIBRE)) {
            $modifiedColumns[':p' . $index++]  = '`prixlibre`';
        }
        if ($this->isColumnModified(PrestationTableMap::COL_QUANTITE)) {
            $modifiedColumns[':p' . $index++]  = '`quantite`';
        }
        if ($this->isColumnModified(PrestationTableMap::COL_ID_UNITE)) {
            $modifiedColumns[':p' . $index++]  = '`id_unite`';
        }
        if ($this->isColumnModified(PrestationTableMap::COL_ID_COMPTE)) {
            $modifiedColumns[':p' . $index++]  = '`id_compte`';
        }
        if ($this->isColumnModified(PrestationTableMap::COL_PRESTATION_TYPE)) {
            $modifiedColumns[':p' . $index++]  = '`prestation_type`';
        }
        if ($this->isColumnModified(PrestationTableMap::COL_ACTIVE)) {
            $modifiedColumns[':p' . $index++]  = '`active`';
        }
        if ($this->isColumnModified(PrestationTableMap::COL_NB_VOIX)) {
            $modifiedColumns[':p' . $index++]  = '`nb_voix`';
        }
        if ($this->isColumnModified(PrestationTableMap::COL_PERIODIQUE)) {
            $modifiedColumns[':p' . $index++]  = '`periodique`';
        }
        if ($this->isColumnModified(PrestationTableMap::COL_SIGNE)) {
            $modifiedColumns[':p' . $index++]  = '`signe`';
        }
        if ($this->isColumnModified(PrestationTableMap::COL_OBJET_BENEFICIAIRE)) {
            $modifiedColumns[':p' . $index++]  = '`objet_beneficiaire`';
        }
        if ($this->isColumnModified(PrestationTableMap::COL_CREATED_AT)) {
            $modifiedColumns[':p' . $index++]  = '`created_at`';
        }
        if ($this->isColumnModified(PrestationTableMap::COL_UPDATED_AT)) {
            $modifiedColumns[':p' . $index++]  = '`updated_at`';
        }

        $sql = sprintf(
            'INSERT INTO `asso_prestations` (%s) VALUES (%s)',
            implode(', ', $modifiedColumns),
            implode(', ', array_keys($modifiedColumns))
        );

        try {
            $stmt = $con->prepare($sql);
            foreach ($modifiedColumns as $identifier => $columnName) {
                switch ($columnName) {
                    case '`id_prestation`':
                        $stmt->bindValue($identifier, $this->id_prestation, PDO::PARAM_INT);
                        break;
                    case '`nom_groupe`':
                        $stmt->bindValue($identifier, $this->nom_groupe, PDO::PARAM_STR);
                        break;
                    case '`nom`':
                        $stmt->bindValue($identifier, $this->nom, PDO::PARAM_STR);
                        break;
                    case '`nomcourt`':
                        $stmt->bindValue($identifier, $this->nomcourt, PDO::PARAM_STR);
                        break;
                    case '`descriptif`':
                        $stmt->bindValue($identifier, $this->descriptif, PDO::PARAM_STR);
                        break;
                    case '`id_entite`':
                        $stmt->bindValue($identifier, $this->id_entite, PDO::PARAM_INT);
                        break;
                    case '`id_tva`':
                        $stmt->bindValue($identifier, $this->id_tva, PDO::PARAM_INT);
                        break;
                    case '`retard_jours`':
                        $stmt->bindValue($identifier, $this->retard_jours, PDO::PARAM_INT);
                        break;
                    case '`nombre_numero`':
                        $stmt->bindValue($identifier, $this->nombre_numero, PDO::PARAM_INT);
                        break;
                    case '`prochain_numero`':
                        $stmt->bindValue($identifier, $this->prochain_numero, PDO::PARAM_INT);
                        break;
                    case '`prixlibre`':
                        $stmt->bindValue($identifier, (int) $this->prixlibre, PDO::PARAM_INT);
                        break;
                    case '`quantite`':
                        $stmt->bindValue($identifier, (int) $this->quantite, PDO::PARAM_INT);
                        break;
                    case '`id_unite`':
                        $stmt->bindValue($identifier, $this->id_unite, PDO::PARAM_INT);
                        break;
                    case '`id_compte`':
                        $stmt->bindValue($identifier, $this->id_compte, PDO::PARAM_INT);
                        break;
                    case '`prestation_type`':
                        $stmt->bindValue($identifier, $this->prestation_type, PDO::PARAM_INT);
                        break;
                    case '`active`':
                        $stmt->bindValue($identifier, (int) $this->active, PDO::PARAM_INT);
                        break;
                    case '`nb_voix`':
                        $stmt->bindValue($identifier, $this->nb_voix, PDO::PARAM_INT);
                        break;
                    case '`periodique`':
                        $stmt->bindValue($identifier, $this->periodique, PDO::PARAM_STR);
                        break;
                    case '`signe`':
                        $stmt->bindValue($identifier, $this->signe, PDO::PARAM_STR);
                        break;
                    case '`objet_beneficiaire`':
                        $stmt->bindValue($identifier, $this->objet_beneficiaire, PDO::PARAM_STR);
                        break;
                    case '`created_at`':
                        $stmt->bindValue($identifier, $this->created_at ? $this->created_at->format("Y-m-d H:i:s.u") : null, PDO::PARAM_STR);
                        break;
                    case '`updated_at`':
                        $stmt->bindValue($identifier, $this->updated_at ? $this->updated_at->format("Y-m-d H:i:s.u") : null, PDO::PARAM_STR);
                        break;
                }
            }
            $stmt->execute();
        } catch (Exception $e) {
            Propel::log($e->getMessage(), Propel::LOG_ERR);
            throw new PropelException(sprintf('Unable to execute INSERT statement [%s]', $sql), 0, $e);
        }

        try {
            $pk = $con->lastInsertId();
        } catch (Exception $e) {
            throw new PropelException('Unable to get autoincrement id.', 0, $e);
        }
        $this->setIdPrestation($pk);

        $this->setNew(false);
    }

    /**
     * Update the row in the database.
     *
     * @param      ConnectionInterface $con
     *
     * @return Integer Number of updated rows
     * @see doSave()
     */
    protected function doUpdate(ConnectionInterface $con)
    {
        $selectCriteria = $this->buildPkeyCriteria();
        $valuesCriteria = $this->buildCriteria();

        return $selectCriteria->doUpdate($valuesCriteria, $con);
    }

    /**
     * Retrieves a field from the object by name passed in as a string.
     *
     * @param      string $name name
     * @param      string $type The type of fieldname the $name is of:
     *                     one of the class type constants TableMap::TYPE_PHPNAME, TableMap::TYPE_CAMELNAME
     *                     TableMap::TYPE_COLNAME, TableMap::TYPE_FIELDNAME, TableMap::TYPE_NUM.
     *                     Defaults to TableMap::TYPE_FIELDNAME.
     * @return mixed Value of field.
     */
    public function getByName($name, $type = TableMap::TYPE_FIELDNAME)
    {
        $pos = PrestationTableMap::translateFieldName($name, $type, TableMap::TYPE_NUM);
        $field = $this->getByPosition($pos);

        return $field;
    }

    /**
     * Retrieves a field from the object by Position as specified in the xml schema.
     * Zero-based.
     *
     * @param      int $pos position in xml schema
     * @return mixed Value of field at $pos
     */
    public function getByPosition($pos)
    {
        switch ($pos) {
            case 0:
                return $this->getIdPrestation();
                break;
            case 1:
                return $this->getNomGroupe();
                break;
            case 2:
                return $this->getNom();
                break;
            case 3:
                return $this->getNomcourt();
                break;
            case 4:
                return $this->getDescriptif();
                break;
            case 5:
                return $this->getIdEntite();
                break;
            case 6:
                return $this->getIdTva();
                break;
            case 7:
                return $this->getRetardJours();
                break;
            case 8:
                return $this->getNombreNumero();
                break;
            case 9:
                return $this->getProchainNumero();
                break;
            case 10:
                return $this->getPrixlibre();
                break;
            case 11:
                return $this->getQuantite();
                break;
            case 12:
                return $this->getIdUnite();
                break;
            case 13:
                return $this->getIdCompte();
                break;
            case 14:
                return $this->getPrestationType();
                break;
            case 15:
                return $this->getActive();
                break;
            case 16:
                return $this->getNbVoix();
                break;
            case 17:
                return $this->getPeriodique();
                break;
            case 18:
                return $this->getSigne();
                break;
            case 19:
                return $this->getObjetBeneficiaire();
                break;
            case 20:
                return $this->getCreatedAt();
                break;
            case 21:
                return $this->getUpdatedAt();
                break;
            default:
                return null;
                break;
        } // switch()
    }

    /**
     * Exports the object as an array.
     *
     * You can specify the key type of the array by passing one of the class
     * type constants.
     *
     * @param     string  $keyType (optional) One of the class type constants TableMap::TYPE_PHPNAME, TableMap::TYPE_CAMELNAME,
     *                    TableMap::TYPE_COLNAME, TableMap::TYPE_FIELDNAME, TableMap::TYPE_NUM.
     *                    Defaults to TableMap::TYPE_FIELDNAME.
     * @param     boolean $includeLazyLoadColumns (optional) Whether to include lazy loaded columns. Defaults to TRUE.
     * @param     array $alreadyDumpedObjects List of objects to skip to avoid recursion
     * @param     boolean $includeForeignObjects (optional) Whether to include hydrated related objects. Default to FALSE.
     *
     * @return array an associative array containing the field names (as keys) and field values
     */
    public function toArray($keyType = TableMap::TYPE_FIELDNAME, $includeLazyLoadColumns = true, $alreadyDumpedObjects = array(), $includeForeignObjects = false)
    {

        if (isset($alreadyDumpedObjects['Prestation'][$this->hashCode()])) {
            return '*RECURSION*';
        }
        $alreadyDumpedObjects['Prestation'][$this->hashCode()] = true;
        $keys = PrestationTableMap::getFieldNames($keyType);
        $result = array(
            $keys[0] => $this->getIdPrestation(),
            $keys[1] => $this->getNomGroupe(),
            $keys[2] => $this->getNom(),
            $keys[3] => $this->getNomcourt(),
            $keys[4] => $this->getDescriptif(),
            $keys[5] => $this->getIdEntite(),
            $keys[6] => $this->getIdTva(),
            $keys[7] => $this->getRetardJours(),
            $keys[8] => $this->getNombreNumero(),
            $keys[9] => $this->getProchainNumero(),
            $keys[10] => $this->getPrixlibre(),
            $keys[11] => $this->getQuantite(),
            $keys[12] => $this->getIdUnite(),
            $keys[13] => $this->getIdCompte(),
            $keys[14] => $this->getPrestationType(),
            $keys[15] => $this->getActive(),
            $keys[16] => $this->getNbVoix(),
            $keys[17] => $this->getPeriodique(),
            $keys[18] => $this->getSigne(),
            $keys[19] => $this->getObjetBeneficiaire(),
            $keys[20] => $this->getCreatedAt(),
            $keys[21] => $this->getUpdatedAt(),
        );
        if ($result[$keys[20]] instanceof \DateTimeInterface) {
            $result[$keys[20]] = $result[$keys[20]]->format('c');
        }

        if ($result[$keys[21]] instanceof \DateTimeInterface) {
            $result[$keys[21]] = $result[$keys[21]]->format('c');
        }

        $virtualColumns = $this->virtualColumns;
        foreach ($virtualColumns as $key => $virtualColumn) {
            $result[$key] = $virtualColumn;
        }

        if ($includeForeignObjects) {
            if (null !== $this->aEntite) {

                switch ($keyType) {
                    case TableMap::TYPE_CAMELNAME:
                        $key = 'entite';
                        break;
                    case TableMap::TYPE_FIELDNAME:
                        $key = 'asso_entites';
                        break;
                    default:
                        $key = 'Entite';
                }

                $result[$key] = $this->aEntite->toArray($keyType, $includeLazyLoadColumns,  $alreadyDumpedObjects, true);
            }
            if (null !== $this->aCompte) {

                switch ($keyType) {
                    case TableMap::TYPE_CAMELNAME:
                        $key = 'compte';
                        break;
                    case TableMap::TYPE_FIELDNAME:
                        $key = 'assc_comptes';
                        break;
                    default:
                        $key = 'Compte';
                }

                $result[$key] = $this->aCompte->toArray($keyType, $includeLazyLoadColumns,  $alreadyDumpedObjects, true);
            }
            if (null !== $this->collPrixes) {

                switch ($keyType) {
                    case TableMap::TYPE_CAMELNAME:
                        $key = 'prixes';
                        break;
                    case TableMap::TYPE_FIELDNAME:
                        $key = 'asso_prixss';
                        break;
                    default:
                        $key = 'Prixes';
                }

                $result[$key] = $this->collPrixes->toArray(null, false, $keyType, $includeLazyLoadColumns, $alreadyDumpedObjects);
            }
            if (null !== $this->collPrestationslotPrestations) {

                switch ($keyType) {
                    case TableMap::TYPE_CAMELNAME:
                        $key = 'prestationslotPrestations';
                        break;
                    case TableMap::TYPE_FIELDNAME:
                        $key = 'asso_prestationslots_prestationss';
                        break;
                    default:
                        $key = 'PrestationslotPrestations';
                }

                $result[$key] = $this->collPrestationslotPrestations->toArray(null, false, $keyType, $includeLazyLoadColumns, $alreadyDumpedObjects);
            }
            if (null !== $this->collServicerendus) {

                switch ($keyType) {
                    case TableMap::TYPE_CAMELNAME:
                        $key = 'servicerendus';
                        break;
                    case TableMap::TYPE_FIELDNAME:
                        $key = 'asso_servicerenduses';
                        break;
                    default:
                        $key = 'Servicerendus';
                }

                $result[$key] = $this->collServicerendus->toArray(null, false, $keyType, $includeLazyLoadColumns, $alreadyDumpedObjects);
            }
        }

        return $result;
    }

    /**
     * Sets a field from the object by name passed in as a string.
     *
     * @param  string $name
     * @param  mixed  $value field value
     * @param  string $type The type of fieldname the $name is of:
     *                one of the class type constants TableMap::TYPE_PHPNAME, TableMap::TYPE_CAMELNAME
     *                TableMap::TYPE_COLNAME, TableMap::TYPE_FIELDNAME, TableMap::TYPE_NUM.
     *                Defaults to TableMap::TYPE_FIELDNAME.
     * @return $this|\Prestation
     */
    public function setByName($name, $value, $type = TableMap::TYPE_FIELDNAME)
    {
        $pos = PrestationTableMap::translateFieldName($name, $type, TableMap::TYPE_NUM);

        return $this->setByPosition($pos, $value);
    }

    /**
     * Sets a field from the object by Position as specified in the xml schema.
     * Zero-based.
     *
     * @param  int $pos position in xml schema
     * @param  mixed $value field value
     * @return $this|\Prestation
     */
    public function setByPosition($pos, $value)
    {
        switch ($pos) {
            case 0:
                $this->setIdPrestation($value);
                break;
            case 1:
                $this->setNomGroupe($value);
                break;
            case 2:
                $this->setNom($value);
                break;
            case 3:
                $this->setNomcourt($value);
                break;
            case 4:
                $this->setDescriptif($value);
                break;
            case 5:
                $this->setIdEntite($value);
                break;
            case 6:
                $this->setIdTva($value);
                break;
            case 7:
                $this->setRetardJours($value);
                break;
            case 8:
                $this->setNombreNumero($value);
                break;
            case 9:
                $this->setProchainNumero($value);
                break;
            case 10:
                $this->setPrixlibre($value);
                break;
            case 11:
                $this->setQuantite($value);
                break;
            case 12:
                $this->setIdUnite($value);
                break;
            case 13:
                $this->setIdCompte($value);
                break;
            case 14:
                $this->setPrestationType($value);
                break;
            case 15:
                $this->setActive($value);
                break;
            case 16:
                $this->setNbVoix($value);
                break;
            case 17:
                $this->setPeriodique($value);
                break;
            case 18:
                $this->setSigne($value);
                break;
            case 19:
                $this->setObjetBeneficiaire($value);
                break;
            case 20:
                $this->setCreatedAt($value);
                break;
            case 21:
                $this->setUpdatedAt($value);
                break;
        } // switch()

        return $this;
    }

    /**
     * Populates the object using an array.
     *
     * This is particularly useful when populating an object from one of the
     * request arrays (e.g. $_POST).  This method goes through the column
     * names, checking to see whether a matching key exists in populated
     * array. If so the setByName() method is called for that column.
     *
     * You can specify the key type of the array by additionally passing one
     * of the class type constants TableMap::TYPE_PHPNAME, TableMap::TYPE_CAMELNAME,
     * TableMap::TYPE_COLNAME, TableMap::TYPE_FIELDNAME, TableMap::TYPE_NUM.
     * The default key type is the column's TableMap::TYPE_FIELDNAME.
     *
     * @param      array  $arr     An array to populate the object from.
     * @param      string $keyType The type of keys the array uses.
     * @return void
     */
    public function fromArray($arr, $keyType = TableMap::TYPE_FIELDNAME)
    {
        $keys = PrestationTableMap::getFieldNames($keyType);

        if (array_key_exists($keys[0], $arr)) {
            $this->setIdPrestation($arr[$keys[0]]);
        }
        if (array_key_exists($keys[1], $arr)) {
            $this->setNomGroupe($arr[$keys[1]]);
        }
        if (array_key_exists($keys[2], $arr)) {
            $this->setNom($arr[$keys[2]]);
        }
        if (array_key_exists($keys[3], $arr)) {
            $this->setNomcourt($arr[$keys[3]]);
        }
        if (array_key_exists($keys[4], $arr)) {
            $this->setDescriptif($arr[$keys[4]]);
        }
        if (array_key_exists($keys[5], $arr)) {
            $this->setIdEntite($arr[$keys[5]]);
        }
        if (array_key_exists($keys[6], $arr)) {
            $this->setIdTva($arr[$keys[6]]);
        }
        if (array_key_exists($keys[7], $arr)) {
            $this->setRetardJours($arr[$keys[7]]);
        }
        if (array_key_exists($keys[8], $arr)) {
            $this->setNombreNumero($arr[$keys[8]]);
        }
        if (array_key_exists($keys[9], $arr)) {
            $this->setProchainNumero($arr[$keys[9]]);
        }
        if (array_key_exists($keys[10], $arr)) {
            $this->setPrixlibre($arr[$keys[10]]);
        }
        if (array_key_exists($keys[11], $arr)) {
            $this->setQuantite($arr[$keys[11]]);
        }
        if (array_key_exists($keys[12], $arr)) {
            $this->setIdUnite($arr[$keys[12]]);
        }
        if (array_key_exists($keys[13], $arr)) {
            $this->setIdCompte($arr[$keys[13]]);
        }
        if (array_key_exists($keys[14], $arr)) {
            $this->setPrestationType($arr[$keys[14]]);
        }
        if (array_key_exists($keys[15], $arr)) {
            $this->setActive($arr[$keys[15]]);
        }
        if (array_key_exists($keys[16], $arr)) {
            $this->setNbVoix($arr[$keys[16]]);
        }
        if (array_key_exists($keys[17], $arr)) {
            $this->setPeriodique($arr[$keys[17]]);
        }
        if (array_key_exists($keys[18], $arr)) {
            $this->setSigne($arr[$keys[18]]);
        }
        if (array_key_exists($keys[19], $arr)) {
            $this->setObjetBeneficiaire($arr[$keys[19]]);
        }
        if (array_key_exists($keys[20], $arr)) {
            $this->setCreatedAt($arr[$keys[20]]);
        }
        if (array_key_exists($keys[21], $arr)) {
            $this->setUpdatedAt($arr[$keys[21]]);
        }
    }

     /**
     * Populate the current object from a string, using a given parser format
     * <code>
     * $book = new Book();
     * $book->importFrom('JSON', '{"Id":9012,"Title":"Don Juan","ISBN":"0140422161","Price":12.99,"PublisherId":1234,"AuthorId":5678}');
     * </code>
     *
     * You can specify the key type of the array by additionally passing one
     * of the class type constants TableMap::TYPE_PHPNAME, TableMap::TYPE_CAMELNAME,
     * TableMap::TYPE_COLNAME, TableMap::TYPE_FIELDNAME, TableMap::TYPE_NUM.
     * The default key type is the column's TableMap::TYPE_FIELDNAME.
     *
     * @param mixed $parser A AbstractParser instance,
     *                       or a format name ('XML', 'YAML', 'JSON', 'CSV')
     * @param string $data The source data to import from
     * @param string $keyType The type of keys the array uses.
     *
     * @return $this|\Prestation The current object, for fluid interface
     */
    public function importFrom($parser, $data, $keyType = TableMap::TYPE_FIELDNAME)
    {
        if (!$parser instanceof AbstractParser) {
            $parser = AbstractParser::getParser($parser);
        }

        $this->fromArray($parser->toArray($data), $keyType);

        return $this;
    }

    /**
     * Build a Criteria object containing the values of all modified columns in this object.
     *
     * @return Criteria The Criteria object containing all modified values.
     */
    public function buildCriteria()
    {
        $criteria = new Criteria(PrestationTableMap::DATABASE_NAME);

        if ($this->isColumnModified(PrestationTableMap::COL_ID_PRESTATION)) {
            $criteria->add(PrestationTableMap::COL_ID_PRESTATION, $this->id_prestation);
        }
        if ($this->isColumnModified(PrestationTableMap::COL_NOM_GROUPE)) {
            $criteria->add(PrestationTableMap::COL_NOM_GROUPE, $this->nom_groupe);
        }
        if ($this->isColumnModified(PrestationTableMap::COL_NOM)) {
            $criteria->add(PrestationTableMap::COL_NOM, $this->nom);
        }
        if ($this->isColumnModified(PrestationTableMap::COL_NOMCOURT)) {
            $criteria->add(PrestationTableMap::COL_NOMCOURT, $this->nomcourt);
        }
        if ($this->isColumnModified(PrestationTableMap::COL_DESCRIPTIF)) {
            $criteria->add(PrestationTableMap::COL_DESCRIPTIF, $this->descriptif);
        }
        if ($this->isColumnModified(PrestationTableMap::COL_ID_ENTITE)) {
            $criteria->add(PrestationTableMap::COL_ID_ENTITE, $this->id_entite);
        }
        if ($this->isColumnModified(PrestationTableMap::COL_ID_TVA)) {
            $criteria->add(PrestationTableMap::COL_ID_TVA, $this->id_tva);
        }
        if ($this->isColumnModified(PrestationTableMap::COL_RETARD_JOURS)) {
            $criteria->add(PrestationTableMap::COL_RETARD_JOURS, $this->retard_jours);
        }
        if ($this->isColumnModified(PrestationTableMap::COL_NOMBRE_NUMERO)) {
            $criteria->add(PrestationTableMap::COL_NOMBRE_NUMERO, $this->nombre_numero);
        }
        if ($this->isColumnModified(PrestationTableMap::COL_PROCHAIN_NUMERO)) {
            $criteria->add(PrestationTableMap::COL_PROCHAIN_NUMERO, $this->prochain_numero);
        }
        if ($this->isColumnModified(PrestationTableMap::COL_PRIXLIBRE)) {
            $criteria->add(PrestationTableMap::COL_PRIXLIBRE, $this->prixlibre);
        }
        if ($this->isColumnModified(PrestationTableMap::COL_QUANTITE)) {
            $criteria->add(PrestationTableMap::COL_QUANTITE, $this->quantite);
        }
        if ($this->isColumnModified(PrestationTableMap::COL_ID_UNITE)) {
            $criteria->add(PrestationTableMap::COL_ID_UNITE, $this->id_unite);
        }
        if ($this->isColumnModified(PrestationTableMap::COL_ID_COMPTE)) {
            $criteria->add(PrestationTableMap::COL_ID_COMPTE, $this->id_compte);
        }
        if ($this->isColumnModified(PrestationTableMap::COL_PRESTATION_TYPE)) {
            $criteria->add(PrestationTableMap::COL_PRESTATION_TYPE, $this->prestation_type);
        }
        if ($this->isColumnModified(PrestationTableMap::COL_ACTIVE)) {
            $criteria->add(PrestationTableMap::COL_ACTIVE, $this->active);
        }
        if ($this->isColumnModified(PrestationTableMap::COL_NB_VOIX)) {
            $criteria->add(PrestationTableMap::COL_NB_VOIX, $this->nb_voix);
        }
        if ($this->isColumnModified(PrestationTableMap::COL_PERIODIQUE)) {
            $criteria->add(PrestationTableMap::COL_PERIODIQUE, $this->periodique);
        }
        if ($this->isColumnModified(PrestationTableMap::COL_SIGNE)) {
            $criteria->add(PrestationTableMap::COL_SIGNE, $this->signe);
        }
        if ($this->isColumnModified(PrestationTableMap::COL_OBJET_BENEFICIAIRE)) {
            $criteria->add(PrestationTableMap::COL_OBJET_BENEFICIAIRE, $this->objet_beneficiaire);
        }
        if ($this->isColumnModified(PrestationTableMap::COL_CREATED_AT)) {
            $criteria->add(PrestationTableMap::COL_CREATED_AT, $this->created_at);
        }
        if ($this->isColumnModified(PrestationTableMap::COL_UPDATED_AT)) {
            $criteria->add(PrestationTableMap::COL_UPDATED_AT, $this->updated_at);
        }

        return $criteria;
    }

    /**
     * Builds a Criteria object containing the primary key for this object.
     *
     * Unlike buildCriteria() this method includes the primary key values regardless
     * of whether or not they have been modified.
     *
     * @throws LogicException if no primary key is defined
     *
     * @return Criteria The Criteria object containing value(s) for primary key(s).
     */
    public function buildPkeyCriteria()
    {
        $criteria = ChildPrestationQuery::create();
        $criteria->add(PrestationTableMap::COL_ID_PRESTATION, $this->id_prestation);

        return $criteria;
    }

    /**
     * If the primary key is not null, return the hashcode of the
     * primary key. Otherwise, return the hash code of the object.
     *
     * @return int Hashcode
     */
    public function hashCode()
    {
        $validPk = null !== $this->getIdPrestation();

        $validPrimaryKeyFKs = 0;
        $primaryKeyFKs = [];

        if ($validPk) {
            return crc32(json_encode($this->getPrimaryKey(), JSON_UNESCAPED_UNICODE));
        } elseif ($validPrimaryKeyFKs) {
            return crc32(json_encode($primaryKeyFKs, JSON_UNESCAPED_UNICODE));
        }

        return spl_object_hash($this);
    }

    /**
     * Returns the primary key for this object (row).
     * @return string
     */
    public function getPrimaryKey()
    {
        return $this->getIdPrestation();
    }

    /**
     * Generic method to set the primary key (id_prestation column).
     *
     * @param       string $key Primary key.
     * @return void
     */
    public function setPrimaryKey($key)
    {
        $this->setIdPrestation($key);
    }

    /**
     * Returns true if the primary key for this object is null.
     * @return boolean
     */
    public function isPrimaryKeyNull()
    {
        return null === $this->getIdPrestation();
    }

    /**
     * Sets contents of passed object to values from current object.
     *
     * If desired, this method can also make copies of all associated (fkey referrers)
     * objects.
     *
     * @param      object $copyObj An object of \Prestation (or compatible) type.
     * @param      boolean $deepCopy Whether to also copy all rows that refer (by fkey) to the current row.
     * @param      boolean $makeNew Whether to reset autoincrement PKs and make the object new.
     * @throws PropelException
     */
    public function copyInto($copyObj, $deepCopy = false, $makeNew = true)
    {
        $copyObj->setNomGroupe($this->getNomGroupe());
        $copyObj->setNom($this->getNom());
        $copyObj->setNomcourt($this->getNomcourt());
        $copyObj->setDescriptif($this->getDescriptif());
        $copyObj->setIdEntite($this->getIdEntite());
        $copyObj->setIdTva($this->getIdTva());
        $copyObj->setRetardJours($this->getRetardJours());
        $copyObj->setNombreNumero($this->getNombreNumero());
        $copyObj->setProchainNumero($this->getProchainNumero());
        $copyObj->setPrixlibre($this->getPrixlibre());
        $copyObj->setQuantite($this->getQuantite());
        $copyObj->setIdUnite($this->getIdUnite());
        $copyObj->setIdCompte($this->getIdCompte());
        $copyObj->setPrestationType($this->getPrestationType());
        $copyObj->setActive($this->getActive());
        $copyObj->setNbVoix($this->getNbVoix());
        $copyObj->setPeriodique($this->getPeriodique());
        $copyObj->setSigne($this->getSigne());
        $copyObj->setObjetBeneficiaire($this->getObjetBeneficiaire());
        $copyObj->setCreatedAt($this->getCreatedAt());
        $copyObj->setUpdatedAt($this->getUpdatedAt());

        if ($deepCopy) {
            // important: temporarily setNew(false) because this affects the behavior of
            // the getter/setter methods for fkey referrer objects.
            $copyObj->setNew(false);

            foreach ($this->getPrixes() as $relObj) {
                if ($relObj !== $this) {  // ensure that we don't try to copy a reference to ourselves
                    $copyObj->addPrix($relObj->copy($deepCopy));
                }
            }

            foreach ($this->getPrestationslotPrestations() as $relObj) {
                if ($relObj !== $this) {  // ensure that we don't try to copy a reference to ourselves
                    $copyObj->addPrestationslotPrestation($relObj->copy($deepCopy));
                }
            }

            foreach ($this->getServicerendus() as $relObj) {
                if ($relObj !== $this) {  // ensure that we don't try to copy a reference to ourselves
                    $copyObj->addServicerendu($relObj->copy($deepCopy));
                }
            }

        } // if ($deepCopy)

        if ($makeNew) {
            $copyObj->setNew(true);
            $copyObj->setIdPrestation(NULL); // this is a auto-increment column, so set to default value
        }
    }

    /**
     * Makes a copy of this object that will be inserted as a new row in table when saved.
     * It creates a new object filling in the simple attributes, but skipping any primary
     * keys that are defined for the table.
     *
     * If desired, this method can also make copies of all associated (fkey referrers)
     * objects.
     *
     * @param  boolean $deepCopy Whether to also copy all rows that refer (by fkey) to the current row.
     * @return \Prestation Clone of current object.
     * @throws PropelException
     */
    public function copy($deepCopy = false)
    {
        // we use get_class(), because this might be a subclass
        $clazz = get_class($this);
        $copyObj = new $clazz();
        $this->copyInto($copyObj, $deepCopy);

        return $copyObj;
    }

    /**
     * Declares an association between this object and a ChildEntite object.
     *
     * @param  ChildEntite $v
     * @return $this|\Prestation The current object (for fluent API support)
     * @throws PropelException
     */
    public function setEntite(ChildEntite $v = null)
    {
        if ($v === null) {
            $this->setIdEntite(NULL);
        } else {
            $this->setIdEntite($v->getIdEntite());
        }

        $this->aEntite = $v;

        // Add binding for other direction of this n:n relationship.
        // If this object has already been added to the ChildEntite object, it will not be re-added.
        if ($v !== null) {
            $v->addPrestation($this);
        }


        return $this;
    }


    /**
     * Get the associated ChildEntite object
     *
     * @param  ConnectionInterface $con Optional Connection object.
     * @return ChildEntite The associated ChildEntite object.
     * @throws PropelException
     */
    public function getEntite(ConnectionInterface $con = null)
    {
        if ($this->aEntite === null && (($this->id_entite !== "" && $this->id_entite !== null))) {
            $this->aEntite = ChildEntiteQuery::create()->findPk($this->id_entite, $con);
            /* The following can be used additionally to
                guarantee the related object contains a reference
                to this object.  This level of coupling may, however, be
                undesirable since it could result in an only partially populated collection
                in the referenced object.
                $this->aEntite->addPrestations($this);
             */
        }

        return $this->aEntite;
    }

    /**
     * Declares an association between this object and a ChildCompte object.
     *
     * @param  ChildCompte $v
     * @return $this|\Prestation The current object (for fluent API support)
     * @throws PropelException
     */
    public function setCompte(ChildCompte $v = null)
    {
        if ($v === null) {
            $this->setIdCompte(NULL);
        } else {
            $this->setIdCompte($v->getIdCompte());
        }

        $this->aCompte = $v;

        // Add binding for other direction of this n:n relationship.
        // If this object has already been added to the ChildCompte object, it will not be re-added.
        if ($v !== null) {
            $v->addPrestation($this);
        }


        return $this;
    }


    /**
     * Get the associated ChildCompte object
     *
     * @param  ConnectionInterface $con Optional Connection object.
     * @return ChildCompte The associated ChildCompte object.
     * @throws PropelException
     */
    public function getCompte(ConnectionInterface $con = null)
    {
        if ($this->aCompte === null && (($this->id_compte !== "" && $this->id_compte !== null))) {
            $this->aCompte = ChildCompteQuery::create()->findPk($this->id_compte, $con);
            /* The following can be used additionally to
                guarantee the related object contains a reference
                to this object.  This level of coupling may, however, be
                undesirable since it could result in an only partially populated collection
                in the referenced object.
                $this->aCompte->addPrestations($this);
             */
        }

        return $this->aCompte;
    }


    /**
     * Initializes a collection based on the name of a relation.
     * Avoids crafting an 'init[$relationName]s' method name
     * that wouldn't work when StandardEnglishPluralizer is used.
     *
     * @param      string $relationName The name of the relation to initialize
     * @return void
     */
    public function initRelation($relationName)
    {
        if ('Prix' == $relationName) {
            $this->initPrixes();
            return;
        }
        if ('PrestationslotPrestation' == $relationName) {
            $this->initPrestationslotPrestations();
            return;
        }
        if ('Servicerendu' == $relationName) {
            $this->initServicerendus();
            return;
        }
    }

    /**
     * Clears out the collPrixes collection
     *
     * This does not modify the database; however, it will remove any associated objects, causing
     * them to be refetched by subsequent calls to accessor method.
     *
     * @return void
     * @see        addPrixes()
     */
    public function clearPrixes()
    {
        $this->collPrixes = null; // important to set this to NULL since that means it is uninitialized
    }

    /**
     * Reset is the collPrixes collection loaded partially.
     */
    public function resetPartialPrixes($v = true)
    {
        $this->collPrixesPartial = $v;
    }

    /**
     * Initializes the collPrixes collection.
     *
     * By default this just sets the collPrixes collection to an empty array (like clearcollPrixes());
     * however, you may wish to override this method in your stub class to provide setting appropriate
     * to your application -- for example, setting the initial array to the values stored in database.
     *
     * @param      boolean $overrideExisting If set to true, the method call initializes
     *                                        the collection even if it is not empty
     *
     * @return void
     */
    public function initPrixes($overrideExisting = true)
    {
        if (null !== $this->collPrixes && !$overrideExisting) {
            return;
        }

        $collectionClassName = PrixTableMap::getTableMap()->getCollectionClassName();

        $this->collPrixes = new $collectionClassName;
        $this->collPrixes->setModel('\Prix');
    }

    /**
     * Gets an array of ChildPrix objects which contain a foreign key that references this object.
     *
     * If the $criteria is not null, it is used to always fetch the results from the database.
     * Otherwise the results are fetched from the database the first time, then cached.
     * Next time the same method is called without $criteria, the cached collection is returned.
     * If this ChildPrestation is new, it will return
     * an empty collection or the current collection; the criteria is ignored on a new object.
     *
     * @param      Criteria $criteria optional Criteria object to narrow the query
     * @param      ConnectionInterface $con optional connection object
     * @return ObjectCollection|ChildPrix[] List of ChildPrix objects
     * @throws PropelException
     */
    public function getPrixes(Criteria $criteria = null, ConnectionInterface $con = null)
    {
        $partial = $this->collPrixesPartial && !$this->isNew();
        if (null === $this->collPrixes || null !== $criteria  || $partial) {
            if ($this->isNew() && null === $this->collPrixes) {
                // return empty collection
                $this->initPrixes();
            } else {
                $collPrixes = ChildPrixQuery::create(null, $criteria)
                    ->filterByPrestation($this)
                    ->find($con);

                if (null !== $criteria) {
                    if (false !== $this->collPrixesPartial && count($collPrixes)) {
                        $this->initPrixes(false);

                        foreach ($collPrixes as $obj) {
                            if (false == $this->collPrixes->contains($obj)) {
                                $this->collPrixes->append($obj);
                            }
                        }

                        $this->collPrixesPartial = true;
                    }

                    return $collPrixes;
                }

                if ($partial && $this->collPrixes) {
                    foreach ($this->collPrixes as $obj) {
                        if ($obj->isNew()) {
                            $collPrixes[] = $obj;
                        }
                    }
                }

                $this->collPrixes = $collPrixes;
                $this->collPrixesPartial = false;
            }
        }

        return $this->collPrixes;
    }

    /**
     * Sets a collection of ChildPrix objects related by a one-to-many relationship
     * to the current object.
     * It will also schedule objects for deletion based on a diff between old objects (aka persisted)
     * and new objects from the given Propel collection.
     *
     * @param      Collection $prixes A Propel collection.
     * @param      ConnectionInterface $con Optional connection object
     * @return $this|ChildPrestation The current object (for fluent API support)
     */
    public function setPrixes(Collection $prixes, ConnectionInterface $con = null)
    {
        /** @var ChildPrix[] $prixesToDelete */
        $prixesToDelete = $this->getPrixes(new Criteria(), $con)->diff($prixes);


        $this->prixesScheduledForDeletion = $prixesToDelete;

        foreach ($prixesToDelete as $prixRemoved) {
            $prixRemoved->setPrestation(null);
        }

        $this->collPrixes = null;
        foreach ($prixes as $prix) {
            $this->addPrix($prix);
        }

        $this->collPrixes = $prixes;
        $this->collPrixesPartial = false;

        return $this;
    }

    /**
     * Returns the number of related Prix objects.
     *
     * @param      Criteria $criteria
     * @param      boolean $distinct
     * @param      ConnectionInterface $con
     * @return int             Count of related Prix objects.
     * @throws PropelException
     */
    public function countPrixes(Criteria $criteria = null, $distinct = false, ConnectionInterface $con = null)
    {
        $partial = $this->collPrixesPartial && !$this->isNew();
        if (null === $this->collPrixes || null !== $criteria || $partial) {
            if ($this->isNew() && null === $this->collPrixes) {
                return 0;
            }

            if ($partial && !$criteria) {
                return count($this->getPrixes());
            }

            $query = ChildPrixQuery::create(null, $criteria);
            if ($distinct) {
                $query->distinct();
            }

            return $query
                ->filterByPrestation($this)
                ->count($con);
        }

        return count($this->collPrixes);
    }

    /**
     * Method called to associate a ChildPrix object to this object
     * through the ChildPrix foreign key attribute.
     *
     * @param  ChildPrix $l ChildPrix
     * @return $this|\Prestation The current object (for fluent API support)
     */
    public function addPrix(ChildPrix $l)
    {
        if ($this->collPrixes === null) {
            $this->initPrixes();
            $this->collPrixesPartial = true;
        }

        if (!$this->collPrixes->contains($l)) {
            $this->doAddPrix($l);

            if ($this->prixesScheduledForDeletion and $this->prixesScheduledForDeletion->contains($l)) {
                $this->prixesScheduledForDeletion->remove($this->prixesScheduledForDeletion->search($l));
            }
        }

        return $this;
    }

    /**
     * @param ChildPrix $prix The ChildPrix object to add.
     */
    protected function doAddPrix(ChildPrix $prix)
    {
        $this->collPrixes[]= $prix;
        $prix->setPrestation($this);
    }

    /**
     * @param  ChildPrix $prix The ChildPrix object to remove.
     * @return $this|ChildPrestation The current object (for fluent API support)
     */
    public function removePrix(ChildPrix $prix)
    {
        if ($this->getPrixes()->contains($prix)) {
            $pos = $this->collPrixes->search($prix);
            $this->collPrixes->remove($pos);
            if (null === $this->prixesScheduledForDeletion) {
                $this->prixesScheduledForDeletion = clone $this->collPrixes;
                $this->prixesScheduledForDeletion->clear();
            }
            $this->prixesScheduledForDeletion[]= clone $prix;
            $prix->setPrestation(null);
        }

        return $this;
    }

    /**
     * Clears out the collPrestationslotPrestations collection
     *
     * This does not modify the database; however, it will remove any associated objects, causing
     * them to be refetched by subsequent calls to accessor method.
     *
     * @return void
     * @see        addPrestationslotPrestations()
     */
    public function clearPrestationslotPrestations()
    {
        $this->collPrestationslotPrestations = null; // important to set this to NULL since that means it is uninitialized
    }

    /**
     * Reset is the collPrestationslotPrestations collection loaded partially.
     */
    public function resetPartialPrestationslotPrestations($v = true)
    {
        $this->collPrestationslotPrestationsPartial = $v;
    }

    /**
     * Initializes the collPrestationslotPrestations collection.
     *
     * By default this just sets the collPrestationslotPrestations collection to an empty array (like clearcollPrestationslotPrestations());
     * however, you may wish to override this method in your stub class to provide setting appropriate
     * to your application -- for example, setting the initial array to the values stored in database.
     *
     * @param      boolean $overrideExisting If set to true, the method call initializes
     *                                        the collection even if it is not empty
     *
     * @return void
     */
    public function initPrestationslotPrestations($overrideExisting = true)
    {
        if (null !== $this->collPrestationslotPrestations && !$overrideExisting) {
            return;
        }

        $collectionClassName = PrestationslotPrestationTableMap::getTableMap()->getCollectionClassName();

        $this->collPrestationslotPrestations = new $collectionClassName;
        $this->collPrestationslotPrestations->setModel('\PrestationslotPrestation');
    }

    /**
     * Gets an array of ChildPrestationslotPrestation objects which contain a foreign key that references this object.
     *
     * If the $criteria is not null, it is used to always fetch the results from the database.
     * Otherwise the results are fetched from the database the first time, then cached.
     * Next time the same method is called without $criteria, the cached collection is returned.
     * If this ChildPrestation is new, it will return
     * an empty collection or the current collection; the criteria is ignored on a new object.
     *
     * @param      Criteria $criteria optional Criteria object to narrow the query
     * @param      ConnectionInterface $con optional connection object
     * @return ObjectCollection|ChildPrestationslotPrestation[] List of ChildPrestationslotPrestation objects
     * @throws PropelException
     */
    public function getPrestationslotPrestations(Criteria $criteria = null, ConnectionInterface $con = null)
    {
        $partial = $this->collPrestationslotPrestationsPartial && !$this->isNew();
        if (null === $this->collPrestationslotPrestations || null !== $criteria  || $partial) {
            if ($this->isNew() && null === $this->collPrestationslotPrestations) {
                // return empty collection
                $this->initPrestationslotPrestations();
            } else {
                $collPrestationslotPrestations = ChildPrestationslotPrestationQuery::create(null, $criteria)
                    ->filterByPrestation($this)
                    ->find($con);

                if (null !== $criteria) {
                    if (false !== $this->collPrestationslotPrestationsPartial && count($collPrestationslotPrestations)) {
                        $this->initPrestationslotPrestations(false);

                        foreach ($collPrestationslotPrestations as $obj) {
                            if (false == $this->collPrestationslotPrestations->contains($obj)) {
                                $this->collPrestationslotPrestations->append($obj);
                            }
                        }

                        $this->collPrestationslotPrestationsPartial = true;
                    }

                    return $collPrestationslotPrestations;
                }

                if ($partial && $this->collPrestationslotPrestations) {
                    foreach ($this->collPrestationslotPrestations as $obj) {
                        if ($obj->isNew()) {
                            $collPrestationslotPrestations[] = $obj;
                        }
                    }
                }

                $this->collPrestationslotPrestations = $collPrestationslotPrestations;
                $this->collPrestationslotPrestationsPartial = false;
            }
        }

        return $this->collPrestationslotPrestations;
    }

    /**
     * Sets a collection of ChildPrestationslotPrestation objects related by a one-to-many relationship
     * to the current object.
     * It will also schedule objects for deletion based on a diff between old objects (aka persisted)
     * and new objects from the given Propel collection.
     *
     * @param      Collection $prestationslotPrestations A Propel collection.
     * @param      ConnectionInterface $con Optional connection object
     * @return $this|ChildPrestation The current object (for fluent API support)
     */
    public function setPrestationslotPrestations(Collection $prestationslotPrestations, ConnectionInterface $con = null)
    {
        /** @var ChildPrestationslotPrestation[] $prestationslotPrestationsToDelete */
        $prestationslotPrestationsToDelete = $this->getPrestationslotPrestations(new Criteria(), $con)->diff($prestationslotPrestations);


        //since at least one column in the foreign key is at the same time a PK
        //we can not just set a PK to NULL in the lines below. We have to store
        //a backup of all values, so we are able to manipulate these items based on the onDelete value later.
        $this->prestationslotPrestationsScheduledForDeletion = clone $prestationslotPrestationsToDelete;

        foreach ($prestationslotPrestationsToDelete as $prestationslotPrestationRemoved) {
            $prestationslotPrestationRemoved->setPrestation(null);
        }

        $this->collPrestationslotPrestations = null;
        foreach ($prestationslotPrestations as $prestationslotPrestation) {
            $this->addPrestationslotPrestation($prestationslotPrestation);
        }

        $this->collPrestationslotPrestations = $prestationslotPrestations;
        $this->collPrestationslotPrestationsPartial = false;

        return $this;
    }

    /**
     * Returns the number of related PrestationslotPrestation objects.
     *
     * @param      Criteria $criteria
     * @param      boolean $distinct
     * @param      ConnectionInterface $con
     * @return int             Count of related PrestationslotPrestation objects.
     * @throws PropelException
     */
    public function countPrestationslotPrestations(Criteria $criteria = null, $distinct = false, ConnectionInterface $con = null)
    {
        $partial = $this->collPrestationslotPrestationsPartial && !$this->isNew();
        if (null === $this->collPrestationslotPrestations || null !== $criteria || $partial) {
            if ($this->isNew() && null === $this->collPrestationslotPrestations) {
                return 0;
            }

            if ($partial && !$criteria) {
                return count($this->getPrestationslotPrestations());
            }

            $query = ChildPrestationslotPrestationQuery::create(null, $criteria);
            if ($distinct) {
                $query->distinct();
            }

            return $query
                ->filterByPrestation($this)
                ->count($con);
        }

        return count($this->collPrestationslotPrestations);
    }

    /**
     * Method called to associate a ChildPrestationslotPrestation object to this object
     * through the ChildPrestationslotPrestation foreign key attribute.
     *
     * @param  ChildPrestationslotPrestation $l ChildPrestationslotPrestation
     * @return $this|\Prestation The current object (for fluent API support)
     */
    public function addPrestationslotPrestation(ChildPrestationslotPrestation $l)
    {
        if ($this->collPrestationslotPrestations === null) {
            $this->initPrestationslotPrestations();
            $this->collPrestationslotPrestationsPartial = true;
        }

        if (!$this->collPrestationslotPrestations->contains($l)) {
            $this->doAddPrestationslotPrestation($l);

            if ($this->prestationslotPrestationsScheduledForDeletion and $this->prestationslotPrestationsScheduledForDeletion->contains($l)) {
                $this->prestationslotPrestationsScheduledForDeletion->remove($this->prestationslotPrestationsScheduledForDeletion->search($l));
            }
        }

        return $this;
    }

    /**
     * @param ChildPrestationslotPrestation $prestationslotPrestation The ChildPrestationslotPrestation object to add.
     */
    protected function doAddPrestationslotPrestation(ChildPrestationslotPrestation $prestationslotPrestation)
    {
        $this->collPrestationslotPrestations[]= $prestationslotPrestation;
        $prestationslotPrestation->setPrestation($this);
    }

    /**
     * @param  ChildPrestationslotPrestation $prestationslotPrestation The ChildPrestationslotPrestation object to remove.
     * @return $this|ChildPrestation The current object (for fluent API support)
     */
    public function removePrestationslotPrestation(ChildPrestationslotPrestation $prestationslotPrestation)
    {
        if ($this->getPrestationslotPrestations()->contains($prestationslotPrestation)) {
            $pos = $this->collPrestationslotPrestations->search($prestationslotPrestation);
            $this->collPrestationslotPrestations->remove($pos);
            if (null === $this->prestationslotPrestationsScheduledForDeletion) {
                $this->prestationslotPrestationsScheduledForDeletion = clone $this->collPrestationslotPrestations;
                $this->prestationslotPrestationsScheduledForDeletion->clear();
            }
            $this->prestationslotPrestationsScheduledForDeletion[]= clone $prestationslotPrestation;
            $prestationslotPrestation->setPrestation(null);
        }

        return $this;
    }


    /**
     * If this collection has already been initialized with
     * an identical criteria, it returns the collection.
     * Otherwise if this Prestation is new, it will return
     * an empty collection; or if this Prestation has previously
     * been saved, it will retrieve related PrestationslotPrestations from storage.
     *
     * This method is protected by default in order to keep the public
     * api reasonable.  You can provide public methods for those you
     * actually need in Prestation.
     *
     * @param      Criteria $criteria optional Criteria object to narrow the query
     * @param      ConnectionInterface $con optional connection object
     * @param      string $joinBehavior optional join type to use (defaults to Criteria::LEFT_JOIN)
     * @return ObjectCollection|ChildPrestationslotPrestation[] List of ChildPrestationslotPrestation objects
     */
    public function getPrestationslotPrestationsJoinPrestationslot(Criteria $criteria = null, ConnectionInterface $con = null, $joinBehavior = Criteria::LEFT_JOIN)
    {
        $query = ChildPrestationslotPrestationQuery::create(null, $criteria);
        $query->joinWith('Prestationslot', $joinBehavior);

        return $this->getPrestationslotPrestations($query, $con);
    }

    /**
     * Clears out the collServicerendus collection
     *
     * This does not modify the database; however, it will remove any associated objects, causing
     * them to be refetched by subsequent calls to accessor method.
     *
     * @return void
     * @see        addServicerendus()
     */
    public function clearServicerendus()
    {
        $this->collServicerendus = null; // important to set this to NULL since that means it is uninitialized
    }

    /**
     * Reset is the collServicerendus collection loaded partially.
     */
    public function resetPartialServicerendus($v = true)
    {
        $this->collServicerendusPartial = $v;
    }

    /**
     * Initializes the collServicerendus collection.
     *
     * By default this just sets the collServicerendus collection to an empty array (like clearcollServicerendus());
     * however, you may wish to override this method in your stub class to provide setting appropriate
     * to your application -- for example, setting the initial array to the values stored in database.
     *
     * @param      boolean $overrideExisting If set to true, the method call initializes
     *                                        the collection even if it is not empty
     *
     * @return void
     */
    public function initServicerendus($overrideExisting = true)
    {
        if (null !== $this->collServicerendus && !$overrideExisting) {
            return;
        }

        $collectionClassName = ServicerenduTableMap::getTableMap()->getCollectionClassName();

        $this->collServicerendus = new $collectionClassName;
        $this->collServicerendus->setModel('\Servicerendu');
    }

    /**
     * Gets an array of ChildServicerendu objects which contain a foreign key that references this object.
     *
     * If the $criteria is not null, it is used to always fetch the results from the database.
     * Otherwise the results are fetched from the database the first time, then cached.
     * Next time the same method is called without $criteria, the cached collection is returned.
     * If this ChildPrestation is new, it will return
     * an empty collection or the current collection; the criteria is ignored on a new object.
     *
     * @param      Criteria $criteria optional Criteria object to narrow the query
     * @param      ConnectionInterface $con optional connection object
     * @return ObjectCollection|ChildServicerendu[] List of ChildServicerendu objects
     * @throws PropelException
     */
    public function getServicerendus(Criteria $criteria = null, ConnectionInterface $con = null)
    {
        $partial = $this->collServicerendusPartial && !$this->isNew();
        if (null === $this->collServicerendus || null !== $criteria  || $partial) {
            if ($this->isNew() && null === $this->collServicerendus) {
                // return empty collection
                $this->initServicerendus();
            } else {
                $collServicerendus = ChildServicerenduQuery::create(null, $criteria)
                    ->filterByPrestation($this)
                    ->find($con);

                if (null !== $criteria) {
                    if (false !== $this->collServicerendusPartial && count($collServicerendus)) {
                        $this->initServicerendus(false);

                        foreach ($collServicerendus as $obj) {
                            if (false == $this->collServicerendus->contains($obj)) {
                                $this->collServicerendus->append($obj);
                            }
                        }

                        $this->collServicerendusPartial = true;
                    }

                    return $collServicerendus;
                }

                if ($partial && $this->collServicerendus) {
                    foreach ($this->collServicerendus as $obj) {
                        if ($obj->isNew()) {
                            $collServicerendus[] = $obj;
                        }
                    }
                }

                $this->collServicerendus = $collServicerendus;
                $this->collServicerendusPartial = false;
            }
        }

        return $this->collServicerendus;
    }

    /**
     * Sets a collection of ChildServicerendu objects related by a one-to-many relationship
     * to the current object.
     * It will also schedule objects for deletion based on a diff between old objects (aka persisted)
     * and new objects from the given Propel collection.
     *
     * @param      Collection $servicerendus A Propel collection.
     * @param      ConnectionInterface $con Optional connection object
     * @return $this|ChildPrestation The current object (for fluent API support)
     */
    public function setServicerendus(Collection $servicerendus, ConnectionInterface $con = null)
    {
        /** @var ChildServicerendu[] $servicerendusToDelete */
        $servicerendusToDelete = $this->getServicerendus(new Criteria(), $con)->diff($servicerendus);


        $this->servicerendusScheduledForDeletion = $servicerendusToDelete;

        foreach ($servicerendusToDelete as $servicerenduRemoved) {
            $servicerenduRemoved->setPrestation(null);
        }

        $this->collServicerendus = null;
        foreach ($servicerendus as $servicerendu) {
            $this->addServicerendu($servicerendu);
        }

        $this->collServicerendus = $servicerendus;
        $this->collServicerendusPartial = false;

        return $this;
    }

    /**
     * Returns the number of related Servicerendu objects.
     *
     * @param      Criteria $criteria
     * @param      boolean $distinct
     * @param      ConnectionInterface $con
     * @return int             Count of related Servicerendu objects.
     * @throws PropelException
     */
    public function countServicerendus(Criteria $criteria = null, $distinct = false, ConnectionInterface $con = null)
    {
        $partial = $this->collServicerendusPartial && !$this->isNew();
        if (null === $this->collServicerendus || null !== $criteria || $partial) {
            if ($this->isNew() && null === $this->collServicerendus) {
                return 0;
            }

            if ($partial && !$criteria) {
                return count($this->getServicerendus());
            }

            $query = ChildServicerenduQuery::create(null, $criteria);
            if ($distinct) {
                $query->distinct();
            }

            return $query
                ->filterByPrestation($this)
                ->count($con);
        }

        return count($this->collServicerendus);
    }

    /**
     * Method called to associate a ChildServicerendu object to this object
     * through the ChildServicerendu foreign key attribute.
     *
     * @param  ChildServicerendu $l ChildServicerendu
     * @return $this|\Prestation The current object (for fluent API support)
     */
    public function addServicerendu(ChildServicerendu $l)
    {
        if ($this->collServicerendus === null) {
            $this->initServicerendus();
            $this->collServicerendusPartial = true;
        }

        if (!$this->collServicerendus->contains($l)) {
            $this->doAddServicerendu($l);

            if ($this->servicerendusScheduledForDeletion and $this->servicerendusScheduledForDeletion->contains($l)) {
                $this->servicerendusScheduledForDeletion->remove($this->servicerendusScheduledForDeletion->search($l));
            }
        }

        return $this;
    }

    /**
     * @param ChildServicerendu $servicerendu The ChildServicerendu object to add.
     */
    protected function doAddServicerendu(ChildServicerendu $servicerendu)
    {
        $this->collServicerendus[]= $servicerendu;
        $servicerendu->setPrestation($this);
    }

    /**
     * @param  ChildServicerendu $servicerendu The ChildServicerendu object to remove.
     * @return $this|ChildPrestation The current object (for fluent API support)
     */
    public function removeServicerendu(ChildServicerendu $servicerendu)
    {
        if ($this->getServicerendus()->contains($servicerendu)) {
            $pos = $this->collServicerendus->search($servicerendu);
            $this->collServicerendus->remove($pos);
            if (null === $this->servicerendusScheduledForDeletion) {
                $this->servicerendusScheduledForDeletion = clone $this->collServicerendus;
                $this->servicerendusScheduledForDeletion->clear();
            }
            $this->servicerendusScheduledForDeletion[]= clone $servicerendu;
            $servicerendu->setPrestation(null);
        }

        return $this;
    }


    /**
     * If this collection has already been initialized with
     * an identical criteria, it returns the collection.
     * Otherwise if this Prestation is new, it will return
     * an empty collection; or if this Prestation has previously
     * been saved, it will retrieve related Servicerendus from storage.
     *
     * This method is protected by default in order to keep the public
     * api reasonable.  You can provide public methods for those you
     * actually need in Prestation.
     *
     * @param      Criteria $criteria optional Criteria object to narrow the query
     * @param      ConnectionInterface $con optional connection object
     * @param      string $joinBehavior optional join type to use (defaults to Criteria::LEFT_JOIN)
     * @return ObjectCollection|ChildServicerendu[] List of ChildServicerendu objects
     */
    public function getServicerendusJoinEntite(Criteria $criteria = null, ConnectionInterface $con = null, $joinBehavior = Criteria::LEFT_JOIN)
    {
        $query = ChildServicerenduQuery::create(null, $criteria);
        $query->joinWith('Entite', $joinBehavior);

        return $this->getServicerendus($query, $con);
    }


    /**
     * If this collection has already been initialized with
     * an identical criteria, it returns the collection.
     * Otherwise if this Prestation is new, it will return
     * an empty collection; or if this Prestation has previously
     * been saved, it will retrieve related Servicerendus from storage.
     *
     * This method is protected by default in order to keep the public
     * api reasonable.  You can provide public methods for those you
     * actually need in Prestation.
     *
     * @param      Criteria $criteria optional Criteria object to narrow the query
     * @param      ConnectionInterface $con optional connection object
     * @param      string $joinBehavior optional join type to use (defaults to Criteria::LEFT_JOIN)
     * @return ObjectCollection|ChildServicerendu[] List of ChildServicerendu objects
     */
    public function getServicerendusJoinMembre(Criteria $criteria = null, ConnectionInterface $con = null, $joinBehavior = Criteria::LEFT_JOIN)
    {
        $query = ChildServicerenduQuery::create(null, $criteria);
        $query->joinWith('Membre', $joinBehavior);

        return $this->getServicerendus($query, $con);
    }


    /**
     * If this collection has already been initialized with
     * an identical criteria, it returns the collection.
     * Otherwise if this Prestation is new, it will return
     * an empty collection; or if this Prestation has previously
     * been saved, it will retrieve related Servicerendus from storage.
     *
     * This method is protected by default in order to keep the public
     * api reasonable.  You can provide public methods for those you
     * actually need in Prestation.
     *
     * @param      Criteria $criteria optional Criteria object to narrow the query
     * @param      ConnectionInterface $con optional connection object
     * @param      string $joinBehavior optional join type to use (defaults to Criteria::LEFT_JOIN)
     * @return ObjectCollection|ChildServicerendu[] List of ChildServicerendu objects
     */
    public function getServicerendusJoinServicerenduRelatedByOrigine(Criteria $criteria = null, ConnectionInterface $con = null, $joinBehavior = Criteria::LEFT_JOIN)
    {
        $query = ChildServicerenduQuery::create(null, $criteria);
        $query->joinWith('ServicerenduRelatedByOrigine', $joinBehavior);

        return $this->getServicerendus($query, $con);
    }

    /**
     * Clears out the collPrestationslots collection
     *
     * This does not modify the database; however, it will remove any associated objects, causing
     * them to be refetched by subsequent calls to accessor method.
     *
     * @return void
     * @see        addPrestationslots()
     */
    public function clearPrestationslots()
    {
        $this->collPrestationslots = null; // important to set this to NULL since that means it is uninitialized
    }

    /**
     * Initializes the collPrestationslots crossRef collection.
     *
     * By default this just sets the collPrestationslots collection to an empty collection (like clearPrestationslots());
     * however, you may wish to override this method in your stub class to provide setting appropriate
     * to your application -- for example, setting the initial array to the values stored in database.
     *
     * @return void
     */
    public function initPrestationslots()
    {
        $collectionClassName = PrestationslotPrestationTableMap::getTableMap()->getCollectionClassName();

        $this->collPrestationslots = new $collectionClassName;
        $this->collPrestationslotsPartial = true;
        $this->collPrestationslots->setModel('\Prestationslot');
    }

    /**
     * Checks if the collPrestationslots collection is loaded.
     *
     * @return bool
     */
    public function isPrestationslotsLoaded()
    {
        return null !== $this->collPrestationslots;
    }

    /**
     * Gets a collection of ChildPrestationslot objects related by a many-to-many relationship
     * to the current object by way of the asso_prestationslots_prestations cross-reference table.
     *
     * If the $criteria is not null, it is used to always fetch the results from the database.
     * Otherwise the results are fetched from the database the first time, then cached.
     * Next time the same method is called without $criteria, the cached collection is returned.
     * If this ChildPrestation is new, it will return
     * an empty collection or the current collection; the criteria is ignored on a new object.
     *
     * @param      Criteria $criteria Optional query object to filter the query
     * @param      ConnectionInterface $con Optional connection object
     *
     * @return ObjectCollection|ChildPrestationslot[] List of ChildPrestationslot objects
     */
    public function getPrestationslots(Criteria $criteria = null, ConnectionInterface $con = null)
    {
        $partial = $this->collPrestationslotsPartial && !$this->isNew();
        if (null === $this->collPrestationslots || null !== $criteria || $partial) {
            if ($this->isNew()) {
                // return empty collection
                if (null === $this->collPrestationslots) {
                    $this->initPrestationslots();
                }
            } else {

                $query = ChildPrestationslotQuery::create(null, $criteria)
                    ->filterByPrestation($this);
                $collPrestationslots = $query->find($con);
                if (null !== $criteria) {
                    return $collPrestationslots;
                }

                if ($partial && $this->collPrestationslots) {
                    //make sure that already added objects gets added to the list of the database.
                    foreach ($this->collPrestationslots as $obj) {
                        if (!$collPrestationslots->contains($obj)) {
                            $collPrestationslots[] = $obj;
                        }
                    }
                }

                $this->collPrestationslots = $collPrestationslots;
                $this->collPrestationslotsPartial = false;
            }
        }

        return $this->collPrestationslots;
    }

    /**
     * Sets a collection of Prestationslot objects related by a many-to-many relationship
     * to the current object by way of the asso_prestationslots_prestations cross-reference table.
     * It will also schedule objects for deletion based on a diff between old objects (aka persisted)
     * and new objects from the given Propel collection.
     *
     * @param  Collection $prestationslots A Propel collection.
     * @param  ConnectionInterface $con Optional connection object
     * @return $this|ChildPrestation The current object (for fluent API support)
     */
    public function setPrestationslots(Collection $prestationslots, ConnectionInterface $con = null)
    {
        $this->clearPrestationslots();
        $currentPrestationslots = $this->getPrestationslots();

        $prestationslotsScheduledForDeletion = $currentPrestationslots->diff($prestationslots);

        foreach ($prestationslotsScheduledForDeletion as $toDelete) {
            $this->removePrestationslot($toDelete);
        }

        foreach ($prestationslots as $prestationslot) {
            if (!$currentPrestationslots->contains($prestationslot)) {
                $this->doAddPrestationslot($prestationslot);
            }
        }

        $this->collPrestationslotsPartial = false;
        $this->collPrestationslots = $prestationslots;

        return $this;
    }

    /**
     * Gets the number of Prestationslot objects related by a many-to-many relationship
     * to the current object by way of the asso_prestationslots_prestations cross-reference table.
     *
     * @param      Criteria $criteria Optional query object to filter the query
     * @param      boolean $distinct Set to true to force count distinct
     * @param      ConnectionInterface $con Optional connection object
     *
     * @return int the number of related Prestationslot objects
     */
    public function countPrestationslots(Criteria $criteria = null, $distinct = false, ConnectionInterface $con = null)
    {
        $partial = $this->collPrestationslotsPartial && !$this->isNew();
        if (null === $this->collPrestationslots || null !== $criteria || $partial) {
            if ($this->isNew() && null === $this->collPrestationslots) {
                return 0;
            } else {

                if ($partial && !$criteria) {
                    return count($this->getPrestationslots());
                }

                $query = ChildPrestationslotQuery::create(null, $criteria);
                if ($distinct) {
                    $query->distinct();
                }

                return $query
                    ->filterByPrestation($this)
                    ->count($con);
            }
        } else {
            return count($this->collPrestationslots);
        }
    }

    /**
     * Associate a ChildPrestationslot to this object
     * through the asso_prestationslots_prestations cross reference table.
     *
     * @param ChildPrestationslot $prestationslot
     * @return ChildPrestation The current object (for fluent API support)
     */
    public function addPrestationslot(ChildPrestationslot $prestationslot)
    {
        if ($this->collPrestationslots === null) {
            $this->initPrestationslots();
        }

        if (!$this->getPrestationslots()->contains($prestationslot)) {
            // only add it if the **same** object is not already associated
            $this->collPrestationslots->push($prestationslot);
            $this->doAddPrestationslot($prestationslot);
        }

        return $this;
    }

    /**
     *
     * @param ChildPrestationslot $prestationslot
     */
    protected function doAddPrestationslot(ChildPrestationslot $prestationslot)
    {
        $prestationslotPrestation = new ChildPrestationslotPrestation();

        $prestationslotPrestation->setPrestationslot($prestationslot);

        $prestationslotPrestation->setPrestation($this);

        $this->addPrestationslotPrestation($prestationslotPrestation);

        // set the back reference to this object directly as using provided method either results
        // in endless loop or in multiple relations
        if (!$prestationslot->isPrestationsLoaded()) {
            $prestationslot->initPrestations();
            $prestationslot->getPrestations()->push($this);
        } elseif (!$prestationslot->getPrestations()->contains($this)) {
            $prestationslot->getPrestations()->push($this);
        }

    }

    /**
     * Remove prestationslot of this object
     * through the asso_prestationslots_prestations cross reference table.
     *
     * @param ChildPrestationslot $prestationslot
     * @return ChildPrestation The current object (for fluent API support)
     */
    public function removePrestationslot(ChildPrestationslot $prestationslot)
    {
        if ($this->getPrestationslots()->contains($prestationslot)) {
            $prestationslotPrestation = new ChildPrestationslotPrestation();
            $prestationslotPrestation->setPrestationslot($prestationslot);
            if ($prestationslot->isPrestationsLoaded()) {
                //remove the back reference if available
                $prestationslot->getPrestations()->removeObject($this);
            }

            $prestationslotPrestation->setPrestation($this);
            $this->removePrestationslotPrestation(clone $prestationslotPrestation);
            $prestationslotPrestation->clear();

            $this->collPrestationslots->remove($this->collPrestationslots->search($prestationslot));

            if (null === $this->prestationslotsScheduledForDeletion) {
                $this->prestationslotsScheduledForDeletion = clone $this->collPrestationslots;
                $this->prestationslotsScheduledForDeletion->clear();
            }

            $this->prestationslotsScheduledForDeletion->push($prestationslot);
        }


        return $this;
    }

    /**
     * Clears the current object, sets all attributes to their default values and removes
     * outgoing references as well as back-references (from other objects to this one. Results probably in a database
     * change of those foreign objects when you call `save` there).
     */
    public function clear()
    {
        if (null !== $this->aEntite) {
            $this->aEntite->removePrestation($this);
        }
        if (null !== $this->aCompte) {
            $this->aCompte->removePrestation($this);
        }
        $this->id_prestation = null;
        $this->nom_groupe = null;
        $this->nom = null;
        $this->nomcourt = null;
        $this->descriptif = null;
        $this->id_entite = null;
        $this->id_tva = null;
        $this->retard_jours = null;
        $this->nombre_numero = null;
        $this->prochain_numero = null;
        $this->prixlibre = null;
        $this->quantite = null;
        $this->id_unite = null;
        $this->id_compte = null;
        $this->prestation_type = null;
        $this->active = null;
        $this->nb_voix = null;
        $this->periodique = null;
        $this->signe = null;
        $this->objet_beneficiaire = null;
        $this->created_at = null;
        $this->updated_at = null;
        $this->alreadyInSave = false;
        $this->clearAllReferences();
        $this->applyDefaultValues();
        $this->resetModified();
        $this->setNew(true);
        $this->setDeleted(false);
    }

    /**
     * Resets all references and back-references to other model objects or collections of model objects.
     *
     * This method is used to reset all php object references (not the actual reference in the database).
     * Necessary for object serialisation.
     *
     * @param      boolean $deep Whether to also clear the references on all referrer objects.
     */
    public function clearAllReferences($deep = false)
    {
        if ($deep) {
            if ($this->collPrixes) {
                foreach ($this->collPrixes as $o) {
                    $o->clearAllReferences($deep);
                }
            }
            if ($this->collPrestationslotPrestations) {
                foreach ($this->collPrestationslotPrestations as $o) {
                    $o->clearAllReferences($deep);
                }
            }
            if ($this->collServicerendus) {
                foreach ($this->collServicerendus as $o) {
                    $o->clearAllReferences($deep);
                }
            }
            if ($this->collPrestationslots) {
                foreach ($this->collPrestationslots as $o) {
                    $o->clearAllReferences($deep);
                }
            }
        } // if ($deep)

        $this->collPrixes = null;
        $this->collPrestationslotPrestations = null;
        $this->collServicerendus = null;
        $this->collPrestationslots = null;
        $this->aEntite = null;
        $this->aCompte = null;
    }

    /**
     * Return the string representation of this object
     *
     * @return string
     */
    public function __toString()
    {
        return (string) $this->exportTo(PrestationTableMap::DEFAULT_STRING_FORMAT);
    }

    // timestampable behavior

    /**
     * Mark the current object so that the update date doesn't get updated during next save
     *
     * @return     $this|ChildPrestation The current object (for fluent API support)
     */
    public function keepUpdateDateUnchanged()
    {
        $this->modifiedColumns[PrestationTableMap::COL_UPDATED_AT] = true;

        return $this;
    }

    // archivable behavior

    /**
     * Get an archived version of the current object.
     *
     * @param ConnectionInterface $con Optional connection object
     *
     * @return     ChildAssoPrestationsArchive An archive object, or null if the current object was never archived
     */
    public function getArchive(ConnectionInterface $con = null)
    {
        if ($this->isNew()) {
            return null;
        }
        $archive = ChildAssoPrestationsArchiveQuery::create()
            ->filterByPrimaryKey($this->getPrimaryKey())
            ->findOne($con);

        return $archive;
    }
    /**
     * Copy the data of the current object into a $archiveTablePhpName archive object.
     * The archived object is then saved.
     * If the current object has already been archived, the archived object
     * is updated and not duplicated.
     *
     * @param ConnectionInterface $con Optional connection object
     *
     * @throws PropelException If the object is new
     *
     * @return     ChildAssoPrestationsArchive The archive object based on this object
     */
    public function archive(ConnectionInterface $con = null)
    {
        if ($this->isNew()) {
            throw new PropelException('New objects cannot be archived. You must save the current object before calling archive().');
        }
        $archive = $this->getArchive($con);
        if (!$archive) {
            $archive = new ChildAssoPrestationsArchive();
            $archive->setPrimaryKey($this->getPrimaryKey());
        }
        $this->copyInto($archive, $deepCopy = false, $makeNew = false);
        $archive->setArchivedAt(time());
        $archive->save($con);

        return $archive;
    }

    /**
     * Revert the the current object to the state it had when it was last archived.
     * The object must be saved afterwards if the changes must persist.
     *
     * @param ConnectionInterface $con Optional connection object
     *
     * @throws PropelException If the object has no corresponding archive.
     *
     * @return $this|ChildPrestation The current object (for fluent API support)
     */
    public function restoreFromArchive(ConnectionInterface $con = null)
    {
        $archive = $this->getArchive($con);
        if (!$archive) {
            throw new PropelException('The current object has never been archived and cannot be restored');
        }
        $this->populateFromArchive($archive);

        return $this;
    }

    /**
     * Populates the the current object based on a $archiveTablePhpName archive object.
     *
     * @param      ChildAssoPrestationsArchive $archive An archived object based on the same class
      * @param      Boolean $populateAutoIncrementPrimaryKeys
     *               If true, autoincrement columns are copied from the archive object.
     *               If false, autoincrement columns are left intact.
      *
     * @return     ChildPrestation The current object (for fluent API support)
     */
    public function populateFromArchive($archive, $populateAutoIncrementPrimaryKeys = false) {
        if ($populateAutoIncrementPrimaryKeys) {
            $this->setIdPrestation($archive->getIdPrestation());
        }
        $this->setNomGroupe($archive->getNomGroupe());
        $this->setNom($archive->getNom());
        $this->setNomcourt($archive->getNomcourt());
        $this->setDescriptif($archive->getDescriptif());
        $this->setIdEntite($archive->getIdEntite());
        $this->setIdTva($archive->getIdTva());
        $this->setRetardJours($archive->getRetardJours());
        $this->setNombreNumero($archive->getNombreNumero());
        $this->setProchainNumero($archive->getProchainNumero());
        $this->setPrixlibre($archive->getPrixlibre());
        $this->setQuantite($archive->getQuantite());
        $this->setIdUnite($archive->getIdUnite());
        $this->setIdCompte($archive->getIdCompte());
        $this->setPrestationType($archive->getPrestationType());
        $this->setActive($archive->getActive());
        $this->setNbVoix($archive->getNbVoix());
        $this->setPeriodique($archive->getPeriodique());
        $this->setSigne($archive->getSigne());
        $this->setObjetBeneficiaire($archive->getObjetBeneficiaire());
        $this->setCreatedAt($archive->getCreatedAt());
        $this->setUpdatedAt($archive->getUpdatedAt());

        return $this;
    }

    /**
     * Removes the object from the database without archiving it.
     *
     * @param ConnectionInterface $con Optional connection object
     *
     * @return $this|ChildPrestation The current object (for fluent API support)
     */
    public function deleteWithoutArchive(ConnectionInterface $con = null)
    {
        $this->archiveOnDelete = false;

        return $this->delete($con);
    }

    /**
     * Code to be run before persisting the object
     * @param  ConnectionInterface $con
     * @return boolean
     */
    public function preSave(ConnectionInterface $con = null)
    {
        if (is_callable('parent::preSave')) {
            return parent::preSave($con);
        }
        return true;
    }

    /**
     * Code to be run after persisting the object
     * @param ConnectionInterface $con
     */
    public function postSave(ConnectionInterface $con = null)
    {
        if (is_callable('parent::postSave')) {
            parent::postSave($con);
        }
    }

    /**
     * Code to be run before inserting to database
     * @param  ConnectionInterface $con
     * @return boolean
     */
    public function preInsert(ConnectionInterface $con = null)
    {
        if (is_callable('parent::preInsert')) {
            return parent::preInsert($con);
        }
        return true;
    }

    /**
     * Code to be run after inserting to database
     * @param ConnectionInterface $con
     */
    public function postInsert(ConnectionInterface $con = null)
    {
        if (is_callable('parent::postInsert')) {
            parent::postInsert($con);
        }
    }

    /**
     * Code to be run before updating the object in database
     * @param  ConnectionInterface $con
     * @return boolean
     */
    public function preUpdate(ConnectionInterface $con = null)
    {
        if (is_callable('parent::preUpdate')) {
            return parent::preUpdate($con);
        }
        return true;
    }

    /**
     * Code to be run after updating the object in database
     * @param ConnectionInterface $con
     */
    public function postUpdate(ConnectionInterface $con = null)
    {
        if (is_callable('parent::postUpdate')) {
            parent::postUpdate($con);
        }
    }

    /**
     * Code to be run before deleting the object in database
     * @param  ConnectionInterface $con
     * @return boolean
     */
    public function preDelete(ConnectionInterface $con = null)
    {
        if (is_callable('parent::preDelete')) {
            return parent::preDelete($con);
        }
        return true;
    }

    /**
     * Code to be run after deleting the object in database
     * @param ConnectionInterface $con
     */
    public function postDelete(ConnectionInterface $con = null)
    {
        if (is_callable('parent::postDelete')) {
            parent::postDelete($con);
        }
    }


    /**
     * Derived method to catches calls to undefined methods.
     *
     * Provides magic import/export method support (fromXML()/toXML(), fromYAML()/toYAML(), etc.).
     * Allows to define default __call() behavior if you overwrite __call()
     *
     * @param string $name
     * @param mixed  $params
     *
     * @return array|string
     */
    public function __call($name, $params)
    {
        if (0 === strpos($name, 'get')) {
            $virtualColumn = substr($name, 3);
            if ($this->hasVirtualColumn($virtualColumn)) {
                return $this->getVirtualColumn($virtualColumn);
            }

            $virtualColumn = lcfirst($virtualColumn);
            if ($this->hasVirtualColumn($virtualColumn)) {
                return $this->getVirtualColumn($virtualColumn);
            }
        }

        if (0 === strpos($name, 'from')) {
            $format = substr($name, 4);

            return $this->importFrom($format, reset($params));
        }

        if (0 === strpos($name, 'to')) {
            $format = substr($name, 2);
            $includeLazyLoadColumns = isset($params[0]) ? $params[0] : true;

            return $this->exportTo($format, $includeLazyLoadColumns);
        }

        throw new BadMethodCallException(sprintf('Call to undefined method: %s.', $name));
    }

}
