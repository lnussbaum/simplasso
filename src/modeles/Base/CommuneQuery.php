<?php

namespace Base;

use \Commune as ChildCommune;
use \CommuneQuery as ChildCommuneQuery;
use \Exception;
use \PDO;
use Map\CommuneTableMap;
use Propel\Runtime\Propel;
use Propel\Runtime\ActiveQuery\Criteria;
use Propel\Runtime\ActiveQuery\ModelCriteria;
use Propel\Runtime\Collection\ObjectCollection;
use Propel\Runtime\Connection\ConnectionInterface;
use Propel\Runtime\Exception\PropelException;

/**
 * Base class that represents a query for the 'geo_communes' table.
 *
 *
 *
 * @method     ChildCommuneQuery orderByIdCommune($order = Criteria::ASC) Order by the id_commune column
 * @method     ChildCommuneQuery orderByPays($order = Criteria::ASC) Order by the pays column
 * @method     ChildCommuneQuery orderByRegion($order = Criteria::ASC) Order by the region column
 * @method     ChildCommuneQuery orderByDepartement($order = Criteria::ASC) Order by the departement column
 * @method     ChildCommuneQuery orderByCode($order = Criteria::ASC) Order by the code column
 * @method     ChildCommuneQuery orderByArrondissement($order = Criteria::ASC) Order by the arrondissement column
 * @method     ChildCommuneQuery orderByCanton($order = Criteria::ASC) Order by the canton column
 * @method     ChildCommuneQuery orderByTypeCharniere($order = Criteria::ASC) Order by the type_charniere column
 * @method     ChildCommuneQuery orderByArticle($order = Criteria::ASC) Order by the article column
 * @method     ChildCommuneQuery orderByNom($order = Criteria::ASC) Order by the nom column
 * @method     ChildCommuneQuery orderByLon($order = Criteria::ASC) Order by the lon column
 * @method     ChildCommuneQuery orderByLat($order = Criteria::ASC) Order by the lat column
 * @method     ChildCommuneQuery orderByZoom($order = Criteria::ASC) Order by the zoom column
 * @method     ChildCommuneQuery orderByElevation($order = Criteria::ASC) Order by the elevation column
 * @method     ChildCommuneQuery orderByElevationMoyenne($order = Criteria::ASC) Order by the elevation_moyenne column
 * @method     ChildCommuneQuery orderByPopulation($order = Criteria::ASC) Order by the population column
 * @method     ChildCommuneQuery orderByAutreNom($order = Criteria::ASC) Order by the autre_nom column
 * @method     ChildCommuneQuery orderByUrl($order = Criteria::ASC) Order by the url column
 * @method     ChildCommuneQuery orderByZonage($order = Criteria::ASC) Order by the zonage column
 * @method     ChildCommuneQuery orderByCreatedAt($order = Criteria::ASC) Order by the created_at column
 * @method     ChildCommuneQuery orderByUpdatedAt($order = Criteria::ASC) Order by the updated_at column
 *
 * @method     ChildCommuneQuery groupByIdCommune() Group by the id_commune column
 * @method     ChildCommuneQuery groupByPays() Group by the pays column
 * @method     ChildCommuneQuery groupByRegion() Group by the region column
 * @method     ChildCommuneQuery groupByDepartement() Group by the departement column
 * @method     ChildCommuneQuery groupByCode() Group by the code column
 * @method     ChildCommuneQuery groupByArrondissement() Group by the arrondissement column
 * @method     ChildCommuneQuery groupByCanton() Group by the canton column
 * @method     ChildCommuneQuery groupByTypeCharniere() Group by the type_charniere column
 * @method     ChildCommuneQuery groupByArticle() Group by the article column
 * @method     ChildCommuneQuery groupByNom() Group by the nom column
 * @method     ChildCommuneQuery groupByLon() Group by the lon column
 * @method     ChildCommuneQuery groupByLat() Group by the lat column
 * @method     ChildCommuneQuery groupByZoom() Group by the zoom column
 * @method     ChildCommuneQuery groupByElevation() Group by the elevation column
 * @method     ChildCommuneQuery groupByElevationMoyenne() Group by the elevation_moyenne column
 * @method     ChildCommuneQuery groupByPopulation() Group by the population column
 * @method     ChildCommuneQuery groupByAutreNom() Group by the autre_nom column
 * @method     ChildCommuneQuery groupByUrl() Group by the url column
 * @method     ChildCommuneQuery groupByZonage() Group by the zonage column
 * @method     ChildCommuneQuery groupByCreatedAt() Group by the created_at column
 * @method     ChildCommuneQuery groupByUpdatedAt() Group by the updated_at column
 *
 * @method     ChildCommuneQuery leftJoin($relation) Adds a LEFT JOIN clause to the query
 * @method     ChildCommuneQuery rightJoin($relation) Adds a RIGHT JOIN clause to the query
 * @method     ChildCommuneQuery innerJoin($relation) Adds a INNER JOIN clause to the query
 *
 * @method     ChildCommuneQuery leftJoinWith($relation) Adds a LEFT JOIN clause and with to the query
 * @method     ChildCommuneQuery rightJoinWith($relation) Adds a RIGHT JOIN clause and with to the query
 * @method     ChildCommuneQuery innerJoinWith($relation) Adds a INNER JOIN clause and with to the query
 *
 * @method     ChildCommune findOne(ConnectionInterface $con = null) Return the first ChildCommune matching the query
 * @method     ChildCommune findOneOrCreate(ConnectionInterface $con = null) Return the first ChildCommune matching the query, or a new ChildCommune object populated from the query conditions when no match is found
 *
 * @method     ChildCommune findOneByIdCommune(int $id_commune) Return the first ChildCommune filtered by the id_commune column
 * @method     ChildCommune findOneByPays(string $pays) Return the first ChildCommune filtered by the pays column
 * @method     ChildCommune findOneByRegion(int $region) Return the first ChildCommune filtered by the region column
 * @method     ChildCommune findOneByDepartement(string $departement) Return the first ChildCommune filtered by the departement column
 * @method     ChildCommune findOneByCode(int $code) Return the first ChildCommune filtered by the code column
 * @method     ChildCommune findOneByArrondissement(boolean $arrondissement) Return the first ChildCommune filtered by the arrondissement column
 * @method     ChildCommune findOneByCanton(int $canton) Return the first ChildCommune filtered by the canton column
 * @method     ChildCommune findOneByTypeCharniere(boolean $type_charniere) Return the first ChildCommune filtered by the type_charniere column
 * @method     ChildCommune findOneByArticle(string $article) Return the first ChildCommune filtered by the article column
 * @method     ChildCommune findOneByNom(string $nom) Return the first ChildCommune filtered by the nom column
 * @method     ChildCommune findOneByLon(string $lon) Return the first ChildCommune filtered by the lon column
 * @method     ChildCommune findOneByLat(string $lat) Return the first ChildCommune filtered by the lat column
 * @method     ChildCommune findOneByZoom(int $zoom) Return the first ChildCommune filtered by the zoom column
 * @method     ChildCommune findOneByElevation(int $elevation) Return the first ChildCommune filtered by the elevation column
 * @method     ChildCommune findOneByElevationMoyenne(int $elevation_moyenne) Return the first ChildCommune filtered by the elevation_moyenne column
 * @method     ChildCommune findOneByPopulation(string $population) Return the first ChildCommune filtered by the population column
 * @method     ChildCommune findOneByAutreNom(string $autre_nom) Return the first ChildCommune filtered by the autre_nom column
 * @method     ChildCommune findOneByUrl(string $url) Return the first ChildCommune filtered by the url column
 * @method     ChildCommune findOneByZonage(string $zonage) Return the first ChildCommune filtered by the zonage column
 * @method     ChildCommune findOneByCreatedAt(string $created_at) Return the first ChildCommune filtered by the created_at column
 * @method     ChildCommune findOneByUpdatedAt(string $updated_at) Return the first ChildCommune filtered by the updated_at column *

 * @method     ChildCommune requirePk($key, ConnectionInterface $con = null) Return the ChildCommune by primary key and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildCommune requireOne(ConnectionInterface $con = null) Return the first ChildCommune matching the query and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 *
 * @method     ChildCommune requireOneByIdCommune(int $id_commune) Return the first ChildCommune filtered by the id_commune column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildCommune requireOneByPays(string $pays) Return the first ChildCommune filtered by the pays column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildCommune requireOneByRegion(int $region) Return the first ChildCommune filtered by the region column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildCommune requireOneByDepartement(string $departement) Return the first ChildCommune filtered by the departement column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildCommune requireOneByCode(int $code) Return the first ChildCommune filtered by the code column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildCommune requireOneByArrondissement(boolean $arrondissement) Return the first ChildCommune filtered by the arrondissement column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildCommune requireOneByCanton(int $canton) Return the first ChildCommune filtered by the canton column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildCommune requireOneByTypeCharniere(boolean $type_charniere) Return the first ChildCommune filtered by the type_charniere column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildCommune requireOneByArticle(string $article) Return the first ChildCommune filtered by the article column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildCommune requireOneByNom(string $nom) Return the first ChildCommune filtered by the nom column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildCommune requireOneByLon(string $lon) Return the first ChildCommune filtered by the lon column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildCommune requireOneByLat(string $lat) Return the first ChildCommune filtered by the lat column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildCommune requireOneByZoom(int $zoom) Return the first ChildCommune filtered by the zoom column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildCommune requireOneByElevation(int $elevation) Return the first ChildCommune filtered by the elevation column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildCommune requireOneByElevationMoyenne(int $elevation_moyenne) Return the first ChildCommune filtered by the elevation_moyenne column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildCommune requireOneByPopulation(string $population) Return the first ChildCommune filtered by the population column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildCommune requireOneByAutreNom(string $autre_nom) Return the first ChildCommune filtered by the autre_nom column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildCommune requireOneByUrl(string $url) Return the first ChildCommune filtered by the url column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildCommune requireOneByZonage(string $zonage) Return the first ChildCommune filtered by the zonage column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildCommune requireOneByCreatedAt(string $created_at) Return the first ChildCommune filtered by the created_at column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildCommune requireOneByUpdatedAt(string $updated_at) Return the first ChildCommune filtered by the updated_at column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 *
 * @method     ChildCommune[]|ObjectCollection find(ConnectionInterface $con = null) Return ChildCommune objects based on current ModelCriteria
 * @method     ChildCommune[]|ObjectCollection findByIdCommune(int $id_commune) Return ChildCommune objects filtered by the id_commune column
 * @method     ChildCommune[]|ObjectCollection findByPays(string $pays) Return ChildCommune objects filtered by the pays column
 * @method     ChildCommune[]|ObjectCollection findByRegion(int $region) Return ChildCommune objects filtered by the region column
 * @method     ChildCommune[]|ObjectCollection findByDepartement(string $departement) Return ChildCommune objects filtered by the departement column
 * @method     ChildCommune[]|ObjectCollection findByCode(int $code) Return ChildCommune objects filtered by the code column
 * @method     ChildCommune[]|ObjectCollection findByArrondissement(boolean $arrondissement) Return ChildCommune objects filtered by the arrondissement column
 * @method     ChildCommune[]|ObjectCollection findByCanton(int $canton) Return ChildCommune objects filtered by the canton column
 * @method     ChildCommune[]|ObjectCollection findByTypeCharniere(boolean $type_charniere) Return ChildCommune objects filtered by the type_charniere column
 * @method     ChildCommune[]|ObjectCollection findByArticle(string $article) Return ChildCommune objects filtered by the article column
 * @method     ChildCommune[]|ObjectCollection findByNom(string $nom) Return ChildCommune objects filtered by the nom column
 * @method     ChildCommune[]|ObjectCollection findByLon(string $lon) Return ChildCommune objects filtered by the lon column
 * @method     ChildCommune[]|ObjectCollection findByLat(string $lat) Return ChildCommune objects filtered by the lat column
 * @method     ChildCommune[]|ObjectCollection findByZoom(int $zoom) Return ChildCommune objects filtered by the zoom column
 * @method     ChildCommune[]|ObjectCollection findByElevation(int $elevation) Return ChildCommune objects filtered by the elevation column
 * @method     ChildCommune[]|ObjectCollection findByElevationMoyenne(int $elevation_moyenne) Return ChildCommune objects filtered by the elevation_moyenne column
 * @method     ChildCommune[]|ObjectCollection findByPopulation(string $population) Return ChildCommune objects filtered by the population column
 * @method     ChildCommune[]|ObjectCollection findByAutreNom(string $autre_nom) Return ChildCommune objects filtered by the autre_nom column
 * @method     ChildCommune[]|ObjectCollection findByUrl(string $url) Return ChildCommune objects filtered by the url column
 * @method     ChildCommune[]|ObjectCollection findByZonage(string $zonage) Return ChildCommune objects filtered by the zonage column
 * @method     ChildCommune[]|ObjectCollection findByCreatedAt(string $created_at) Return ChildCommune objects filtered by the created_at column
 * @method     ChildCommune[]|ObjectCollection findByUpdatedAt(string $updated_at) Return ChildCommune objects filtered by the updated_at column
 * @method     ChildCommune[]|\Propel\Runtime\Util\PropelModelPager paginate($page = 1, $maxPerPage = 10, ConnectionInterface $con = null) Issue a SELECT query based on the current ModelCriteria and uses a page and a maximum number of results per page to compute an offset and a limit
 *
 */
abstract class CommuneQuery extends ModelCriteria
{
    protected $entityNotFoundExceptionClass = '\\Propel\\Runtime\\Exception\\EntityNotFoundException';

    /**
     * Initializes internal state of \Base\CommuneQuery object.
     *
     * @param     string $dbName The database name
     * @param     string $modelName The phpName of a model, e.g. 'Book'
     * @param     string $modelAlias The alias for the model in this query, e.g. 'b'
     */
    public function __construct($dbName = 'simplasso', $modelName = '\\Commune', $modelAlias = null)
    {
        parent::__construct($dbName, $modelName, $modelAlias);
    }

    /**
     * Returns a new ChildCommuneQuery object.
     *
     * @param     string $modelAlias The alias of a model in the query
     * @param     Criteria $criteria Optional Criteria to build the query from
     *
     * @return ChildCommuneQuery
     */
    public static function create($modelAlias = null, Criteria $criteria = null)
    {
        if ($criteria instanceof ChildCommuneQuery) {
            return $criteria;
        }
        $query = new ChildCommuneQuery();
        if (null !== $modelAlias) {
            $query->setModelAlias($modelAlias);
        }
        if ($criteria instanceof Criteria) {
            $query->mergeWith($criteria);
        }

        return $query;
    }

    /**
     * Find object by primary key.
     * Propel uses the instance pool to skip the database if the object exists.
     * Go fast if the query is untouched.
     *
     * <code>
     * $obj  = $c->findPk(12, $con);
     * </code>
     *
     * @param mixed $key Primary key to use for the query
     * @param ConnectionInterface $con an optional connection object
     *
     * @return ChildCommune|array|mixed the result, formatted by the current formatter
     */
    public function findPk($key, ConnectionInterface $con = null)
    {
        if ($key === null) {
            return null;
        }

        if ($con === null) {
            $con = Propel::getServiceContainer()->getReadConnection(CommuneTableMap::DATABASE_NAME);
        }

        $this->basePreSelect($con);

        if (
            $this->formatter || $this->modelAlias || $this->with || $this->select
            || $this->selectColumns || $this->asColumns || $this->selectModifiers
            || $this->map || $this->having || $this->joins
        ) {
            return $this->findPkComplex($key, $con);
        }

        if ((null !== ($obj = CommuneTableMap::getInstanceFromPool(null === $key || is_scalar($key) || is_callable([$key, '__toString']) ? (string) $key : $key)))) {
            // the object is already in the instance pool
            return $obj;
        }

        return $this->findPkSimple($key, $con);
    }

    /**
     * Find object by primary key using raw SQL to go fast.
     * Bypass doSelect() and the object formatter by using generated code.
     *
     * @param     mixed $key Primary key to use for the query
     * @param     ConnectionInterface $con A connection object
     *
     * @throws \Propel\Runtime\Exception\PropelException
     *
     * @return ChildCommune A model object, or null if the key is not found
     */
    protected function findPkSimple($key, ConnectionInterface $con)
    {
        $sql = 'SELECT `id_commune`, `pays`, `region`, `departement`, `code`, `arrondissement`, `canton`, `type_charniere`, `article`, `nom`, `lon`, `lat`, `zoom`, `elevation`, `elevation_moyenne`, `population`, `autre_nom`, `url`, `zonage`, `created_at`, `updated_at` FROM `geo_communes` WHERE `id_commune` = :p0';
        try {
            $stmt = $con->prepare($sql);
            $stmt->bindValue(':p0', $key, PDO::PARAM_INT);
            $stmt->execute();
        } catch (Exception $e) {
            Propel::log($e->getMessage(), Propel::LOG_ERR);
            throw new PropelException(sprintf('Unable to execute SELECT statement [%s]', $sql), 0, $e);
        }
        $obj = null;
        if ($row = $stmt->fetch(\PDO::FETCH_NUM)) {
            /** @var ChildCommune $obj */
            $obj = new ChildCommune();
            $obj->hydrate($row);
            CommuneTableMap::addInstanceToPool($obj, null === $key || is_scalar($key) || is_callable([$key, '__toString']) ? (string) $key : $key);
        }
        $stmt->closeCursor();

        return $obj;
    }

    /**
     * Find object by primary key.
     *
     * @param     mixed $key Primary key to use for the query
     * @param     ConnectionInterface $con A connection object
     *
     * @return ChildCommune|array|mixed the result, formatted by the current formatter
     */
    protected function findPkComplex($key, ConnectionInterface $con)
    {
        // As the query uses a PK condition, no limit(1) is necessary.
        $criteria = $this->isKeepQuery() ? clone $this : $this;
        $dataFetcher = $criteria
            ->filterByPrimaryKey($key)
            ->doSelect($con);

        return $criteria->getFormatter()->init($criteria)->formatOne($dataFetcher);
    }

    /**
     * Find objects by primary key
     * <code>
     * $objs = $c->findPks(array(12, 56, 832), $con);
     * </code>
     * @param     array $keys Primary keys to use for the query
     * @param     ConnectionInterface $con an optional connection object
     *
     * @return ObjectCollection|array|mixed the list of results, formatted by the current formatter
     */
    public function findPks($keys, ConnectionInterface $con = null)
    {
        if (null === $con) {
            $con = Propel::getServiceContainer()->getReadConnection($this->getDbName());
        }
        $this->basePreSelect($con);
        $criteria = $this->isKeepQuery() ? clone $this : $this;
        $dataFetcher = $criteria
            ->filterByPrimaryKeys($keys)
            ->doSelect($con);

        return $criteria->getFormatter()->init($criteria)->format($dataFetcher);
    }

    /**
     * Filter the query by primary key
     *
     * @param     mixed $key Primary key to use for the query
     *
     * @return $this|ChildCommuneQuery The current query, for fluid interface
     */
    public function filterByPrimaryKey($key)
    {

        return $this->addUsingAlias(CommuneTableMap::COL_ID_COMMUNE, $key, Criteria::EQUAL);
    }

    /**
     * Filter the query by a list of primary keys
     *
     * @param     array $keys The list of primary key to use for the query
     *
     * @return $this|ChildCommuneQuery The current query, for fluid interface
     */
    public function filterByPrimaryKeys($keys)
    {

        return $this->addUsingAlias(CommuneTableMap::COL_ID_COMMUNE, $keys, Criteria::IN);
    }

    /**
     * Filter the query on the id_commune column
     *
     * Example usage:
     * <code>
     * $query->filterByIdCommune(1234); // WHERE id_commune = 1234
     * $query->filterByIdCommune(array(12, 34)); // WHERE id_commune IN (12, 34)
     * $query->filterByIdCommune(array('min' => 12)); // WHERE id_commune > 12
     * </code>
     *
     * @param     mixed $idCommune The value to use as filter.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildCommuneQuery The current query, for fluid interface
     */
    public function filterByIdCommune($idCommune = null, $comparison = null)
    {
        if (is_array($idCommune)) {
            $useMinMax = false;
            if (isset($idCommune['min'])) {
                $this->addUsingAlias(CommuneTableMap::COL_ID_COMMUNE, $idCommune['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($idCommune['max'])) {
                $this->addUsingAlias(CommuneTableMap::COL_ID_COMMUNE, $idCommune['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(CommuneTableMap::COL_ID_COMMUNE, $idCommune, $comparison);
    }

    /**
     * Filter the query on the pays column
     *
     * Example usage:
     * <code>
     * $query->filterByPays('fooValue');   // WHERE pays = 'fooValue'
     * $query->filterByPays('%fooValue%', Criteria::LIKE); // WHERE pays LIKE '%fooValue%'
     * </code>
     *
     * @param     string $pays The value to use as filter.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildCommuneQuery The current query, for fluid interface
     */
    public function filterByPays($pays = null, $comparison = null)
    {
        if (null === $comparison) {
            if (is_array($pays)) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(CommuneTableMap::COL_PAYS, $pays, $comparison);
    }

    /**
     * Filter the query on the region column
     *
     * Example usage:
     * <code>
     * $query->filterByRegion(1234); // WHERE region = 1234
     * $query->filterByRegion(array(12, 34)); // WHERE region IN (12, 34)
     * $query->filterByRegion(array('min' => 12)); // WHERE region > 12
     * </code>
     *
     * @param     mixed $region The value to use as filter.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildCommuneQuery The current query, for fluid interface
     */
    public function filterByRegion($region = null, $comparison = null)
    {
        if (is_array($region)) {
            $useMinMax = false;
            if (isset($region['min'])) {
                $this->addUsingAlias(CommuneTableMap::COL_REGION, $region['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($region['max'])) {
                $this->addUsingAlias(CommuneTableMap::COL_REGION, $region['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(CommuneTableMap::COL_REGION, $region, $comparison);
    }

    /**
     * Filter the query on the departement column
     *
     * Example usage:
     * <code>
     * $query->filterByDepartement('fooValue');   // WHERE departement = 'fooValue'
     * $query->filterByDepartement('%fooValue%', Criteria::LIKE); // WHERE departement LIKE '%fooValue%'
     * </code>
     *
     * @param     string $departement The value to use as filter.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildCommuneQuery The current query, for fluid interface
     */
    public function filterByDepartement($departement = null, $comparison = null)
    {
        if (null === $comparison) {
            if (is_array($departement)) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(CommuneTableMap::COL_DEPARTEMENT, $departement, $comparison);
    }

    /**
     * Filter the query on the code column
     *
     * Example usage:
     * <code>
     * $query->filterByCode(1234); // WHERE code = 1234
     * $query->filterByCode(array(12, 34)); // WHERE code IN (12, 34)
     * $query->filterByCode(array('min' => 12)); // WHERE code > 12
     * </code>
     *
     * @param     mixed $code The value to use as filter.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildCommuneQuery The current query, for fluid interface
     */
    public function filterByCode($code = null, $comparison = null)
    {
        if (is_array($code)) {
            $useMinMax = false;
            if (isset($code['min'])) {
                $this->addUsingAlias(CommuneTableMap::COL_CODE, $code['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($code['max'])) {
                $this->addUsingAlias(CommuneTableMap::COL_CODE, $code['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(CommuneTableMap::COL_CODE, $code, $comparison);
    }

    /**
     * Filter the query on the arrondissement column
     *
     * Example usage:
     * <code>
     * $query->filterByArrondissement(true); // WHERE arrondissement = true
     * $query->filterByArrondissement('yes'); // WHERE arrondissement = true
     * </code>
     *
     * @param     boolean|string $arrondissement The value to use as filter.
     *              Non-boolean arguments are converted using the following rules:
     *                * 1, '1', 'true',  'on',  and 'yes' are converted to boolean true
     *                * 0, '0', 'false', 'off', and 'no'  are converted to boolean false
     *              Check on string values is case insensitive (so 'FaLsE' is seen as 'false').
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildCommuneQuery The current query, for fluid interface
     */
    public function filterByArrondissement($arrondissement = null, $comparison = null)
    {
        if (is_string($arrondissement)) {
            $arrondissement = in_array(strtolower($arrondissement), array('false', 'off', '-', 'no', 'n', '0', '')) ? false : true;
        }

        return $this->addUsingAlias(CommuneTableMap::COL_ARRONDISSEMENT, $arrondissement, $comparison);
    }

    /**
     * Filter the query on the canton column
     *
     * Example usage:
     * <code>
     * $query->filterByCanton(1234); // WHERE canton = 1234
     * $query->filterByCanton(array(12, 34)); // WHERE canton IN (12, 34)
     * $query->filterByCanton(array('min' => 12)); // WHERE canton > 12
     * </code>
     *
     * @param     mixed $canton The value to use as filter.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildCommuneQuery The current query, for fluid interface
     */
    public function filterByCanton($canton = null, $comparison = null)
    {
        if (is_array($canton)) {
            $useMinMax = false;
            if (isset($canton['min'])) {
                $this->addUsingAlias(CommuneTableMap::COL_CANTON, $canton['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($canton['max'])) {
                $this->addUsingAlias(CommuneTableMap::COL_CANTON, $canton['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(CommuneTableMap::COL_CANTON, $canton, $comparison);
    }

    /**
     * Filter the query on the type_charniere column
     *
     * Example usage:
     * <code>
     * $query->filterByTypeCharniere(true); // WHERE type_charniere = true
     * $query->filterByTypeCharniere('yes'); // WHERE type_charniere = true
     * </code>
     *
     * @param     boolean|string $typeCharniere The value to use as filter.
     *              Non-boolean arguments are converted using the following rules:
     *                * 1, '1', 'true',  'on',  and 'yes' are converted to boolean true
     *                * 0, '0', 'false', 'off', and 'no'  are converted to boolean false
     *              Check on string values is case insensitive (so 'FaLsE' is seen as 'false').
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildCommuneQuery The current query, for fluid interface
     */
    public function filterByTypeCharniere($typeCharniere = null, $comparison = null)
    {
        if (is_string($typeCharniere)) {
            $typeCharniere = in_array(strtolower($typeCharniere), array('false', 'off', '-', 'no', 'n', '0', '')) ? false : true;
        }

        return $this->addUsingAlias(CommuneTableMap::COL_TYPE_CHARNIERE, $typeCharniere, $comparison);
    }

    /**
     * Filter the query on the article column
     *
     * Example usage:
     * <code>
     * $query->filterByArticle('fooValue');   // WHERE article = 'fooValue'
     * $query->filterByArticle('%fooValue%', Criteria::LIKE); // WHERE article LIKE '%fooValue%'
     * </code>
     *
     * @param     string $article The value to use as filter.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildCommuneQuery The current query, for fluid interface
     */
    public function filterByArticle($article = null, $comparison = null)
    {
        if (null === $comparison) {
            if (is_array($article)) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(CommuneTableMap::COL_ARTICLE, $article, $comparison);
    }

    /**
     * Filter the query on the nom column
     *
     * Example usage:
     * <code>
     * $query->filterByNom('fooValue');   // WHERE nom = 'fooValue'
     * $query->filterByNom('%fooValue%', Criteria::LIKE); // WHERE nom LIKE '%fooValue%'
     * </code>
     *
     * @param     string $nom The value to use as filter.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildCommuneQuery The current query, for fluid interface
     */
    public function filterByNom($nom = null, $comparison = null)
    {
        if (null === $comparison) {
            if (is_array($nom)) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(CommuneTableMap::COL_NOM, $nom, $comparison);
    }

    /**
     * Filter the query on the lon column
     *
     * Example usage:
     * <code>
     * $query->filterByLon(1234); // WHERE lon = 1234
     * $query->filterByLon(array(12, 34)); // WHERE lon IN (12, 34)
     * $query->filterByLon(array('min' => 12)); // WHERE lon > 12
     * </code>
     *
     * @param     mixed $lon The value to use as filter.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildCommuneQuery The current query, for fluid interface
     */
    public function filterByLon($lon = null, $comparison = null)
    {
        if (is_array($lon)) {
            $useMinMax = false;
            if (isset($lon['min'])) {
                $this->addUsingAlias(CommuneTableMap::COL_LON, $lon['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($lon['max'])) {
                $this->addUsingAlias(CommuneTableMap::COL_LON, $lon['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(CommuneTableMap::COL_LON, $lon, $comparison);
    }

    /**
     * Filter the query on the lat column
     *
     * Example usage:
     * <code>
     * $query->filterByLat(1234); // WHERE lat = 1234
     * $query->filterByLat(array(12, 34)); // WHERE lat IN (12, 34)
     * $query->filterByLat(array('min' => 12)); // WHERE lat > 12
     * </code>
     *
     * @param     mixed $lat The value to use as filter.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildCommuneQuery The current query, for fluid interface
     */
    public function filterByLat($lat = null, $comparison = null)
    {
        if (is_array($lat)) {
            $useMinMax = false;
            if (isset($lat['min'])) {
                $this->addUsingAlias(CommuneTableMap::COL_LAT, $lat['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($lat['max'])) {
                $this->addUsingAlias(CommuneTableMap::COL_LAT, $lat['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(CommuneTableMap::COL_LAT, $lat, $comparison);
    }

    /**
     * Filter the query on the zoom column
     *
     * Example usage:
     * <code>
     * $query->filterByZoom(1234); // WHERE zoom = 1234
     * $query->filterByZoom(array(12, 34)); // WHERE zoom IN (12, 34)
     * $query->filterByZoom(array('min' => 12)); // WHERE zoom > 12
     * </code>
     *
     * @param     mixed $zoom The value to use as filter.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildCommuneQuery The current query, for fluid interface
     */
    public function filterByZoom($zoom = null, $comparison = null)
    {
        if (is_array($zoom)) {
            $useMinMax = false;
            if (isset($zoom['min'])) {
                $this->addUsingAlias(CommuneTableMap::COL_ZOOM, $zoom['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($zoom['max'])) {
                $this->addUsingAlias(CommuneTableMap::COL_ZOOM, $zoom['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(CommuneTableMap::COL_ZOOM, $zoom, $comparison);
    }

    /**
     * Filter the query on the elevation column
     *
     * Example usage:
     * <code>
     * $query->filterByElevation(1234); // WHERE elevation = 1234
     * $query->filterByElevation(array(12, 34)); // WHERE elevation IN (12, 34)
     * $query->filterByElevation(array('min' => 12)); // WHERE elevation > 12
     * </code>
     *
     * @param     mixed $elevation The value to use as filter.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildCommuneQuery The current query, for fluid interface
     */
    public function filterByElevation($elevation = null, $comparison = null)
    {
        if (is_array($elevation)) {
            $useMinMax = false;
            if (isset($elevation['min'])) {
                $this->addUsingAlias(CommuneTableMap::COL_ELEVATION, $elevation['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($elevation['max'])) {
                $this->addUsingAlias(CommuneTableMap::COL_ELEVATION, $elevation['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(CommuneTableMap::COL_ELEVATION, $elevation, $comparison);
    }

    /**
     * Filter the query on the elevation_moyenne column
     *
     * Example usage:
     * <code>
     * $query->filterByElevationMoyenne(1234); // WHERE elevation_moyenne = 1234
     * $query->filterByElevationMoyenne(array(12, 34)); // WHERE elevation_moyenne IN (12, 34)
     * $query->filterByElevationMoyenne(array('min' => 12)); // WHERE elevation_moyenne > 12
     * </code>
     *
     * @param     mixed $elevationMoyenne The value to use as filter.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildCommuneQuery The current query, for fluid interface
     */
    public function filterByElevationMoyenne($elevationMoyenne = null, $comparison = null)
    {
        if (is_array($elevationMoyenne)) {
            $useMinMax = false;
            if (isset($elevationMoyenne['min'])) {
                $this->addUsingAlias(CommuneTableMap::COL_ELEVATION_MOYENNE, $elevationMoyenne['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($elevationMoyenne['max'])) {
                $this->addUsingAlias(CommuneTableMap::COL_ELEVATION_MOYENNE, $elevationMoyenne['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(CommuneTableMap::COL_ELEVATION_MOYENNE, $elevationMoyenne, $comparison);
    }

    /**
     * Filter the query on the population column
     *
     * Example usage:
     * <code>
     * $query->filterByPopulation(1234); // WHERE population = 1234
     * $query->filterByPopulation(array(12, 34)); // WHERE population IN (12, 34)
     * $query->filterByPopulation(array('min' => 12)); // WHERE population > 12
     * </code>
     *
     * @param     mixed $population The value to use as filter.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildCommuneQuery The current query, for fluid interface
     */
    public function filterByPopulation($population = null, $comparison = null)
    {
        if (is_array($population)) {
            $useMinMax = false;
            if (isset($population['min'])) {
                $this->addUsingAlias(CommuneTableMap::COL_POPULATION, $population['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($population['max'])) {
                $this->addUsingAlias(CommuneTableMap::COL_POPULATION, $population['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(CommuneTableMap::COL_POPULATION, $population, $comparison);
    }

    /**
     * Filter the query on the autre_nom column
     *
     * Example usage:
     * <code>
     * $query->filterByAutreNom('fooValue');   // WHERE autre_nom = 'fooValue'
     * $query->filterByAutreNom('%fooValue%', Criteria::LIKE); // WHERE autre_nom LIKE '%fooValue%'
     * </code>
     *
     * @param     string $autreNom The value to use as filter.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildCommuneQuery The current query, for fluid interface
     */
    public function filterByAutreNom($autreNom = null, $comparison = null)
    {
        if (null === $comparison) {
            if (is_array($autreNom)) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(CommuneTableMap::COL_AUTRE_NOM, $autreNom, $comparison);
    }

    /**
     * Filter the query on the url column
     *
     * Example usage:
     * <code>
     * $query->filterByUrl('fooValue');   // WHERE url = 'fooValue'
     * $query->filterByUrl('%fooValue%', Criteria::LIKE); // WHERE url LIKE '%fooValue%'
     * </code>
     *
     * @param     string $url The value to use as filter.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildCommuneQuery The current query, for fluid interface
     */
    public function filterByUrl($url = null, $comparison = null)
    {
        if (null === $comparison) {
            if (is_array($url)) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(CommuneTableMap::COL_URL, $url, $comparison);
    }

    /**
     * Filter the query on the zonage column
     *
     * Example usage:
     * <code>
     * $query->filterByZonage('fooValue');   // WHERE zonage = 'fooValue'
     * $query->filterByZonage('%fooValue%', Criteria::LIKE); // WHERE zonage LIKE '%fooValue%'
     * </code>
     *
     * @param     string $zonage The value to use as filter.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildCommuneQuery The current query, for fluid interface
     */
    public function filterByZonage($zonage = null, $comparison = null)
    {
        if (null === $comparison) {
            if (is_array($zonage)) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(CommuneTableMap::COL_ZONAGE, $zonage, $comparison);
    }

    /**
     * Filter the query on the created_at column
     *
     * Example usage:
     * <code>
     * $query->filterByCreatedAt('2011-03-14'); // WHERE created_at = '2011-03-14'
     * $query->filterByCreatedAt('now'); // WHERE created_at = '2011-03-14'
     * $query->filterByCreatedAt(array('max' => 'yesterday')); // WHERE created_at > '2011-03-13'
     * </code>
     *
     * @param     mixed $createdAt The value to use as filter.
     *              Values can be integers (unix timestamps), DateTime objects, or strings.
     *              Empty strings are treated as NULL.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildCommuneQuery The current query, for fluid interface
     */
    public function filterByCreatedAt($createdAt = null, $comparison = null)
    {
        if (is_array($createdAt)) {
            $useMinMax = false;
            if (isset($createdAt['min'])) {
                $this->addUsingAlias(CommuneTableMap::COL_CREATED_AT, $createdAt['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($createdAt['max'])) {
                $this->addUsingAlias(CommuneTableMap::COL_CREATED_AT, $createdAt['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(CommuneTableMap::COL_CREATED_AT, $createdAt, $comparison);
    }

    /**
     * Filter the query on the updated_at column
     *
     * Example usage:
     * <code>
     * $query->filterByUpdatedAt('2011-03-14'); // WHERE updated_at = '2011-03-14'
     * $query->filterByUpdatedAt('now'); // WHERE updated_at = '2011-03-14'
     * $query->filterByUpdatedAt(array('max' => 'yesterday')); // WHERE updated_at > '2011-03-13'
     * </code>
     *
     * @param     mixed $updatedAt The value to use as filter.
     *              Values can be integers (unix timestamps), DateTime objects, or strings.
     *              Empty strings are treated as NULL.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildCommuneQuery The current query, for fluid interface
     */
    public function filterByUpdatedAt($updatedAt = null, $comparison = null)
    {
        if (is_array($updatedAt)) {
            $useMinMax = false;
            if (isset($updatedAt['min'])) {
                $this->addUsingAlias(CommuneTableMap::COL_UPDATED_AT, $updatedAt['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($updatedAt['max'])) {
                $this->addUsingAlias(CommuneTableMap::COL_UPDATED_AT, $updatedAt['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(CommuneTableMap::COL_UPDATED_AT, $updatedAt, $comparison);
    }

    /**
     * Exclude object from result
     *
     * @param   ChildCommune $commune Object to remove from the list of results
     *
     * @return $this|ChildCommuneQuery The current query, for fluid interface
     */
    public function prune($commune = null)
    {
        if ($commune) {
            $this->addUsingAlias(CommuneTableMap::COL_ID_COMMUNE, $commune->getIdCommune(), Criteria::NOT_EQUAL);
        }

        return $this;
    }

    /**
     * Deletes all rows from the geo_communes table.
     *
     * @param ConnectionInterface $con the connection to use
     * @return int The number of affected rows (if supported by underlying database driver).
     */
    public function doDeleteAll(ConnectionInterface $con = null)
    {
        if (null === $con) {
            $con = Propel::getServiceContainer()->getWriteConnection(CommuneTableMap::DATABASE_NAME);
        }

        // use transaction because $criteria could contain info
        // for more than one table or we could emulating ON DELETE CASCADE, etc.
        return $con->transaction(function () use ($con) {
            $affectedRows = 0; // initialize var to track total num of affected rows
            $affectedRows += parent::doDeleteAll($con);
            // Because this db requires some delete cascade/set null emulation, we have to
            // clear the cached instance *after* the emulation has happened (since
            // instances get re-added by the select statement contained therein).
            CommuneTableMap::clearInstancePool();
            CommuneTableMap::clearRelatedInstancePool();

            return $affectedRows;
        });
    }

    /**
     * Performs a DELETE on the database based on the current ModelCriteria
     *
     * @param ConnectionInterface $con the connection to use
     * @return int             The number of affected rows (if supported by underlying database driver).  This includes CASCADE-related rows
     *                         if supported by native driver or if emulated using Propel.
     * @throws PropelException Any exceptions caught during processing will be
     *                         rethrown wrapped into a PropelException.
     */
    public function delete(ConnectionInterface $con = null)
    {
        if (null === $con) {
            $con = Propel::getServiceContainer()->getWriteConnection(CommuneTableMap::DATABASE_NAME);
        }

        $criteria = $this;

        // Set the correct dbName
        $criteria->setDbName(CommuneTableMap::DATABASE_NAME);

        // use transaction because $criteria could contain info
        // for more than one table or we could emulating ON DELETE CASCADE, etc.
        return $con->transaction(function () use ($con, $criteria) {
            $affectedRows = 0; // initialize var to track total num of affected rows

            CommuneTableMap::removeInstanceFromPool($criteria);

            $affectedRows += ModelCriteria::delete($con);
            CommuneTableMap::clearRelatedInstancePool();

            return $affectedRows;
        });
    }

    // timestampable behavior

    /**
     * Filter by the latest updated
     *
     * @param      int $nbDays Maximum age of the latest update in days
     *
     * @return     $this|ChildCommuneQuery The current query, for fluid interface
     */
    public function recentlyUpdated($nbDays = 7)
    {
        return $this->addUsingAlias(CommuneTableMap::COL_UPDATED_AT, time() - $nbDays * 24 * 60 * 60, Criteria::GREATER_EQUAL);
    }

    /**
     * Order by update date desc
     *
     * @return     $this|ChildCommuneQuery The current query, for fluid interface
     */
    public function lastUpdatedFirst()
    {
        return $this->addDescendingOrderByColumn(CommuneTableMap::COL_UPDATED_AT);
    }

    /**
     * Order by update date asc
     *
     * @return     $this|ChildCommuneQuery The current query, for fluid interface
     */
    public function firstUpdatedFirst()
    {
        return $this->addAscendingOrderByColumn(CommuneTableMap::COL_UPDATED_AT);
    }

    /**
     * Order by create date desc
     *
     * @return     $this|ChildCommuneQuery The current query, for fluid interface
     */
    public function lastCreatedFirst()
    {
        return $this->addDescendingOrderByColumn(CommuneTableMap::COL_CREATED_AT);
    }

    /**
     * Filter by the latest created
     *
     * @param      int $nbDays Maximum age of in days
     *
     * @return     $this|ChildCommuneQuery The current query, for fluid interface
     */
    public function recentlyCreated($nbDays = 7)
    {
        return $this->addUsingAlias(CommuneTableMap::COL_CREATED_AT, time() - $nbDays * 24 * 60 * 60, Criteria::GREATER_EQUAL);
    }

    /**
     * Order by create date asc
     *
     * @return     $this|ChildCommuneQuery The current query, for fluid interface
     */
    public function firstCreatedFirst()
    {
        return $this->addAscendingOrderByColumn(CommuneTableMap::COL_CREATED_AT);
    }

} // CommuneQuery
