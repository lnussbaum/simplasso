<?php

namespace Base;

use \AsscComptesArchive as ChildAsscComptesArchive;
use \AsscComptesArchiveQuery as ChildAsscComptesArchiveQuery;
use \Exception;
use \PDO;
use Map\AsscComptesArchiveTableMap;
use Propel\Runtime\Propel;
use Propel\Runtime\ActiveQuery\Criteria;
use Propel\Runtime\ActiveQuery\ModelCriteria;
use Propel\Runtime\Collection\ObjectCollection;
use Propel\Runtime\Connection\ConnectionInterface;
use Propel\Runtime\Exception\PropelException;

/**
 * Base class that represents a query for the 'assc_comptes_archive' table.
 *
 *
 *
 * @method     ChildAsscComptesArchiveQuery orderByIdCompte($order = Criteria::ASC) Order by the id_compte column
 * @method     ChildAsscComptesArchiveQuery orderByNcompte($order = Criteria::ASC) Order by the ncompte column
 * @method     ChildAsscComptesArchiveQuery orderByNom($order = Criteria::ASC) Order by the nom column
 * @method     ChildAsscComptesArchiveQuery orderByNomcourt($order = Criteria::ASC) Order by the nomcourt column
 * @method     ChildAsscComptesArchiveQuery orderByIdPoste($order = Criteria::ASC) Order by the id_poste column
 * @method     ChildAsscComptesArchiveQuery orderByTypeOp($order = Criteria::ASC) Order by the type_op column
 * @method     ChildAsscComptesArchiveQuery orderBySoldeAnterieur($order = Criteria::ASC) Order by the solde_anterieur column
 * @method     ChildAsscComptesArchiveQuery orderByDateAnterieure($order = Criteria::ASC) Order by the date_anterieure column
 * @method     ChildAsscComptesArchiveQuery orderByIdEntite($order = Criteria::ASC) Order by the id_entite column
 * @method     ChildAsscComptesArchiveQuery orderByObservation($order = Criteria::ASC) Order by the observation column
 * @method     ChildAsscComptesArchiveQuery orderByCreatedAt($order = Criteria::ASC) Order by the created_at column
 * @method     ChildAsscComptesArchiveQuery orderByUpdatedAt($order = Criteria::ASC) Order by the updated_at column
 * @method     ChildAsscComptesArchiveQuery orderByArchivedAt($order = Criteria::ASC) Order by the archived_at column
 *
 * @method     ChildAsscComptesArchiveQuery groupByIdCompte() Group by the id_compte column
 * @method     ChildAsscComptesArchiveQuery groupByNcompte() Group by the ncompte column
 * @method     ChildAsscComptesArchiveQuery groupByNom() Group by the nom column
 * @method     ChildAsscComptesArchiveQuery groupByNomcourt() Group by the nomcourt column
 * @method     ChildAsscComptesArchiveQuery groupByIdPoste() Group by the id_poste column
 * @method     ChildAsscComptesArchiveQuery groupByTypeOp() Group by the type_op column
 * @method     ChildAsscComptesArchiveQuery groupBySoldeAnterieur() Group by the solde_anterieur column
 * @method     ChildAsscComptesArchiveQuery groupByDateAnterieure() Group by the date_anterieure column
 * @method     ChildAsscComptesArchiveQuery groupByIdEntite() Group by the id_entite column
 * @method     ChildAsscComptesArchiveQuery groupByObservation() Group by the observation column
 * @method     ChildAsscComptesArchiveQuery groupByCreatedAt() Group by the created_at column
 * @method     ChildAsscComptesArchiveQuery groupByUpdatedAt() Group by the updated_at column
 * @method     ChildAsscComptesArchiveQuery groupByArchivedAt() Group by the archived_at column
 *
 * @method     ChildAsscComptesArchiveQuery leftJoin($relation) Adds a LEFT JOIN clause to the query
 * @method     ChildAsscComptesArchiveQuery rightJoin($relation) Adds a RIGHT JOIN clause to the query
 * @method     ChildAsscComptesArchiveQuery innerJoin($relation) Adds a INNER JOIN clause to the query
 *
 * @method     ChildAsscComptesArchiveQuery leftJoinWith($relation) Adds a LEFT JOIN clause and with to the query
 * @method     ChildAsscComptesArchiveQuery rightJoinWith($relation) Adds a RIGHT JOIN clause and with to the query
 * @method     ChildAsscComptesArchiveQuery innerJoinWith($relation) Adds a INNER JOIN clause and with to the query
 *
 * @method     ChildAsscComptesArchive findOne(ConnectionInterface $con = null) Return the first ChildAsscComptesArchive matching the query
 * @method     ChildAsscComptesArchive findOneOrCreate(ConnectionInterface $con = null) Return the first ChildAsscComptesArchive matching the query, or a new ChildAsscComptesArchive object populated from the query conditions when no match is found
 *
 * @method     ChildAsscComptesArchive findOneByIdCompte(string $id_compte) Return the first ChildAsscComptesArchive filtered by the id_compte column
 * @method     ChildAsscComptesArchive findOneByNcompte(string $ncompte) Return the first ChildAsscComptesArchive filtered by the ncompte column
 * @method     ChildAsscComptesArchive findOneByNom(string $nom) Return the first ChildAsscComptesArchive filtered by the nom column
 * @method     ChildAsscComptesArchive findOneByNomcourt(string $nomcourt) Return the first ChildAsscComptesArchive filtered by the nomcourt column
 * @method     ChildAsscComptesArchive findOneByIdPoste(string $id_poste) Return the first ChildAsscComptesArchive filtered by the id_poste column
 * @method     ChildAsscComptesArchive findOneByTypeOp(int $type_op) Return the first ChildAsscComptesArchive filtered by the type_op column
 * @method     ChildAsscComptesArchive findOneBySoldeAnterieur(double $solde_anterieur) Return the first ChildAsscComptesArchive filtered by the solde_anterieur column
 * @method     ChildAsscComptesArchive findOneByDateAnterieure(string $date_anterieure) Return the first ChildAsscComptesArchive filtered by the date_anterieure column
 * @method     ChildAsscComptesArchive findOneByIdEntite(string $id_entite) Return the first ChildAsscComptesArchive filtered by the id_entite column
 * @method     ChildAsscComptesArchive findOneByObservation(string $observation) Return the first ChildAsscComptesArchive filtered by the observation column
 * @method     ChildAsscComptesArchive findOneByCreatedAt(string $created_at) Return the first ChildAsscComptesArchive filtered by the created_at column
 * @method     ChildAsscComptesArchive findOneByUpdatedAt(string $updated_at) Return the first ChildAsscComptesArchive filtered by the updated_at column
 * @method     ChildAsscComptesArchive findOneByArchivedAt(string $archived_at) Return the first ChildAsscComptesArchive filtered by the archived_at column *

 * @method     ChildAsscComptesArchive requirePk($key, ConnectionInterface $con = null) Return the ChildAsscComptesArchive by primary key and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildAsscComptesArchive requireOne(ConnectionInterface $con = null) Return the first ChildAsscComptesArchive matching the query and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 *
 * @method     ChildAsscComptesArchive requireOneByIdCompte(string $id_compte) Return the first ChildAsscComptesArchive filtered by the id_compte column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildAsscComptesArchive requireOneByNcompte(string $ncompte) Return the first ChildAsscComptesArchive filtered by the ncompte column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildAsscComptesArchive requireOneByNom(string $nom) Return the first ChildAsscComptesArchive filtered by the nom column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildAsscComptesArchive requireOneByNomcourt(string $nomcourt) Return the first ChildAsscComptesArchive filtered by the nomcourt column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildAsscComptesArchive requireOneByIdPoste(string $id_poste) Return the first ChildAsscComptesArchive filtered by the id_poste column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildAsscComptesArchive requireOneByTypeOp(int $type_op) Return the first ChildAsscComptesArchive filtered by the type_op column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildAsscComptesArchive requireOneBySoldeAnterieur(double $solde_anterieur) Return the first ChildAsscComptesArchive filtered by the solde_anterieur column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildAsscComptesArchive requireOneByDateAnterieure(string $date_anterieure) Return the first ChildAsscComptesArchive filtered by the date_anterieure column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildAsscComptesArchive requireOneByIdEntite(string $id_entite) Return the first ChildAsscComptesArchive filtered by the id_entite column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildAsscComptesArchive requireOneByObservation(string $observation) Return the first ChildAsscComptesArchive filtered by the observation column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildAsscComptesArchive requireOneByCreatedAt(string $created_at) Return the first ChildAsscComptesArchive filtered by the created_at column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildAsscComptesArchive requireOneByUpdatedAt(string $updated_at) Return the first ChildAsscComptesArchive filtered by the updated_at column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildAsscComptesArchive requireOneByArchivedAt(string $archived_at) Return the first ChildAsscComptesArchive filtered by the archived_at column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 *
 * @method     ChildAsscComptesArchive[]|ObjectCollection find(ConnectionInterface $con = null) Return ChildAsscComptesArchive objects based on current ModelCriteria
 * @method     ChildAsscComptesArchive[]|ObjectCollection findByIdCompte(string $id_compte) Return ChildAsscComptesArchive objects filtered by the id_compte column
 * @method     ChildAsscComptesArchive[]|ObjectCollection findByNcompte(string $ncompte) Return ChildAsscComptesArchive objects filtered by the ncompte column
 * @method     ChildAsscComptesArchive[]|ObjectCollection findByNom(string $nom) Return ChildAsscComptesArchive objects filtered by the nom column
 * @method     ChildAsscComptesArchive[]|ObjectCollection findByNomcourt(string $nomcourt) Return ChildAsscComptesArchive objects filtered by the nomcourt column
 * @method     ChildAsscComptesArchive[]|ObjectCollection findByIdPoste(string $id_poste) Return ChildAsscComptesArchive objects filtered by the id_poste column
 * @method     ChildAsscComptesArchive[]|ObjectCollection findByTypeOp(int $type_op) Return ChildAsscComptesArchive objects filtered by the type_op column
 * @method     ChildAsscComptesArchive[]|ObjectCollection findBySoldeAnterieur(double $solde_anterieur) Return ChildAsscComptesArchive objects filtered by the solde_anterieur column
 * @method     ChildAsscComptesArchive[]|ObjectCollection findByDateAnterieure(string $date_anterieure) Return ChildAsscComptesArchive objects filtered by the date_anterieure column
 * @method     ChildAsscComptesArchive[]|ObjectCollection findByIdEntite(string $id_entite) Return ChildAsscComptesArchive objects filtered by the id_entite column
 * @method     ChildAsscComptesArchive[]|ObjectCollection findByObservation(string $observation) Return ChildAsscComptesArchive objects filtered by the observation column
 * @method     ChildAsscComptesArchive[]|ObjectCollection findByCreatedAt(string $created_at) Return ChildAsscComptesArchive objects filtered by the created_at column
 * @method     ChildAsscComptesArchive[]|ObjectCollection findByUpdatedAt(string $updated_at) Return ChildAsscComptesArchive objects filtered by the updated_at column
 * @method     ChildAsscComptesArchive[]|ObjectCollection findByArchivedAt(string $archived_at) Return ChildAsscComptesArchive objects filtered by the archived_at column
 * @method     ChildAsscComptesArchive[]|\Propel\Runtime\Util\PropelModelPager paginate($page = 1, $maxPerPage = 10, ConnectionInterface $con = null) Issue a SELECT query based on the current ModelCriteria and uses a page and a maximum number of results per page to compute an offset and a limit
 *
 */
abstract class AsscComptesArchiveQuery extends ModelCriteria
{
    protected $entityNotFoundExceptionClass = '\\Propel\\Runtime\\Exception\\EntityNotFoundException';

    /**
     * Initializes internal state of \Base\AsscComptesArchiveQuery object.
     *
     * @param     string $dbName The database name
     * @param     string $modelName The phpName of a model, e.g. 'Book'
     * @param     string $modelAlias The alias for the model in this query, e.g. 'b'
     */
    public function __construct($dbName = 'simplasso', $modelName = '\\AsscComptesArchive', $modelAlias = null)
    {
        parent::__construct($dbName, $modelName, $modelAlias);
    }

    /**
     * Returns a new ChildAsscComptesArchiveQuery object.
     *
     * @param     string $modelAlias The alias of a model in the query
     * @param     Criteria $criteria Optional Criteria to build the query from
     *
     * @return ChildAsscComptesArchiveQuery
     */
    public static function create($modelAlias = null, Criteria $criteria = null)
    {
        if ($criteria instanceof ChildAsscComptesArchiveQuery) {
            return $criteria;
        }
        $query = new ChildAsscComptesArchiveQuery();
        if (null !== $modelAlias) {
            $query->setModelAlias($modelAlias);
        }
        if ($criteria instanceof Criteria) {
            $query->mergeWith($criteria);
        }

        return $query;
    }

    /**
     * Find object by primary key.
     * Propel uses the instance pool to skip the database if the object exists.
     * Go fast if the query is untouched.
     *
     * <code>
     * $obj  = $c->findPk(12, $con);
     * </code>
     *
     * @param mixed $key Primary key to use for the query
     * @param ConnectionInterface $con an optional connection object
     *
     * @return ChildAsscComptesArchive|array|mixed the result, formatted by the current formatter
     */
    public function findPk($key, ConnectionInterface $con = null)
    {
        if ($key === null) {
            return null;
        }

        if ($con === null) {
            $con = Propel::getServiceContainer()->getReadConnection(AsscComptesArchiveTableMap::DATABASE_NAME);
        }

        $this->basePreSelect($con);

        if (
            $this->formatter || $this->modelAlias || $this->with || $this->select
            || $this->selectColumns || $this->asColumns || $this->selectModifiers
            || $this->map || $this->having || $this->joins
        ) {
            return $this->findPkComplex($key, $con);
        }

        if ((null !== ($obj = AsscComptesArchiveTableMap::getInstanceFromPool(null === $key || is_scalar($key) || is_callable([$key, '__toString']) ? (string) $key : $key)))) {
            // the object is already in the instance pool
            return $obj;
        }

        return $this->findPkSimple($key, $con);
    }

    /**
     * Find object by primary key using raw SQL to go fast.
     * Bypass doSelect() and the object formatter by using generated code.
     *
     * @param     mixed $key Primary key to use for the query
     * @param     ConnectionInterface $con A connection object
     *
     * @throws \Propel\Runtime\Exception\PropelException
     *
     * @return ChildAsscComptesArchive A model object, or null if the key is not found
     */
    protected function findPkSimple($key, ConnectionInterface $con)
    {
        $sql = 'SELECT `id_compte`, `ncompte`, `nom`, `nomcourt`, `id_poste`, `type_op`, `solde_anterieur`, `date_anterieure`, `id_entite`, `observation`, `created_at`, `updated_at`, `archived_at` FROM `assc_comptes_archive` WHERE `id_compte` = :p0';
        try {
            $stmt = $con->prepare($sql);
            $stmt->bindValue(':p0', $key, PDO::PARAM_INT);
            $stmt->execute();
        } catch (Exception $e) {
            Propel::log($e->getMessage(), Propel::LOG_ERR);
            throw new PropelException(sprintf('Unable to execute SELECT statement [%s]', $sql), 0, $e);
        }
        $obj = null;
        if ($row = $stmt->fetch(\PDO::FETCH_NUM)) {
            /** @var ChildAsscComptesArchive $obj */
            $obj = new ChildAsscComptesArchive();
            $obj->hydrate($row);
            AsscComptesArchiveTableMap::addInstanceToPool($obj, null === $key || is_scalar($key) || is_callable([$key, '__toString']) ? (string) $key : $key);
        }
        $stmt->closeCursor();

        return $obj;
    }

    /**
     * Find object by primary key.
     *
     * @param     mixed $key Primary key to use for the query
     * @param     ConnectionInterface $con A connection object
     *
     * @return ChildAsscComptesArchive|array|mixed the result, formatted by the current formatter
     */
    protected function findPkComplex($key, ConnectionInterface $con)
    {
        // As the query uses a PK condition, no limit(1) is necessary.
        $criteria = $this->isKeepQuery() ? clone $this : $this;
        $dataFetcher = $criteria
            ->filterByPrimaryKey($key)
            ->doSelect($con);

        return $criteria->getFormatter()->init($criteria)->formatOne($dataFetcher);
    }

    /**
     * Find objects by primary key
     * <code>
     * $objs = $c->findPks(array(12, 56, 832), $con);
     * </code>
     * @param     array $keys Primary keys to use for the query
     * @param     ConnectionInterface $con an optional connection object
     *
     * @return ObjectCollection|array|mixed the list of results, formatted by the current formatter
     */
    public function findPks($keys, ConnectionInterface $con = null)
    {
        if (null === $con) {
            $con = Propel::getServiceContainer()->getReadConnection($this->getDbName());
        }
        $this->basePreSelect($con);
        $criteria = $this->isKeepQuery() ? clone $this : $this;
        $dataFetcher = $criteria
            ->filterByPrimaryKeys($keys)
            ->doSelect($con);

        return $criteria->getFormatter()->init($criteria)->format($dataFetcher);
    }

    /**
     * Filter the query by primary key
     *
     * @param     mixed $key Primary key to use for the query
     *
     * @return $this|ChildAsscComptesArchiveQuery The current query, for fluid interface
     */
    public function filterByPrimaryKey($key)
    {

        return $this->addUsingAlias(AsscComptesArchiveTableMap::COL_ID_COMPTE, $key, Criteria::EQUAL);
    }

    /**
     * Filter the query by a list of primary keys
     *
     * @param     array $keys The list of primary key to use for the query
     *
     * @return $this|ChildAsscComptesArchiveQuery The current query, for fluid interface
     */
    public function filterByPrimaryKeys($keys)
    {

        return $this->addUsingAlias(AsscComptesArchiveTableMap::COL_ID_COMPTE, $keys, Criteria::IN);
    }

    /**
     * Filter the query on the id_compte column
     *
     * Example usage:
     * <code>
     * $query->filterByIdCompte(1234); // WHERE id_compte = 1234
     * $query->filterByIdCompte(array(12, 34)); // WHERE id_compte IN (12, 34)
     * $query->filterByIdCompte(array('min' => 12)); // WHERE id_compte > 12
     * </code>
     *
     * @param     mixed $idCompte The value to use as filter.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildAsscComptesArchiveQuery The current query, for fluid interface
     */
    public function filterByIdCompte($idCompte = null, $comparison = null)
    {
        if (is_array($idCompte)) {
            $useMinMax = false;
            if (isset($idCompte['min'])) {
                $this->addUsingAlias(AsscComptesArchiveTableMap::COL_ID_COMPTE, $idCompte['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($idCompte['max'])) {
                $this->addUsingAlias(AsscComptesArchiveTableMap::COL_ID_COMPTE, $idCompte['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(AsscComptesArchiveTableMap::COL_ID_COMPTE, $idCompte, $comparison);
    }

    /**
     * Filter the query on the ncompte column
     *
     * Example usage:
     * <code>
     * $query->filterByNcompte('fooValue');   // WHERE ncompte = 'fooValue'
     * $query->filterByNcompte('%fooValue%', Criteria::LIKE); // WHERE ncompte LIKE '%fooValue%'
     * </code>
     *
     * @param     string $ncompte The value to use as filter.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildAsscComptesArchiveQuery The current query, for fluid interface
     */
    public function filterByNcompte($ncompte = null, $comparison = null)
    {
        if (null === $comparison) {
            if (is_array($ncompte)) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(AsscComptesArchiveTableMap::COL_NCOMPTE, $ncompte, $comparison);
    }

    /**
     * Filter the query on the nom column
     *
     * Example usage:
     * <code>
     * $query->filterByNom('fooValue');   // WHERE nom = 'fooValue'
     * $query->filterByNom('%fooValue%', Criteria::LIKE); // WHERE nom LIKE '%fooValue%'
     * </code>
     *
     * @param     string $nom The value to use as filter.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildAsscComptesArchiveQuery The current query, for fluid interface
     */
    public function filterByNom($nom = null, $comparison = null)
    {
        if (null === $comparison) {
            if (is_array($nom)) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(AsscComptesArchiveTableMap::COL_NOM, $nom, $comparison);
    }

    /**
     * Filter the query on the nomcourt column
     *
     * Example usage:
     * <code>
     * $query->filterByNomcourt('fooValue');   // WHERE nomcourt = 'fooValue'
     * $query->filterByNomcourt('%fooValue%', Criteria::LIKE); // WHERE nomcourt LIKE '%fooValue%'
     * </code>
     *
     * @param     string $nomcourt The value to use as filter.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildAsscComptesArchiveQuery The current query, for fluid interface
     */
    public function filterByNomcourt($nomcourt = null, $comparison = null)
    {
        if (null === $comparison) {
            if (is_array($nomcourt)) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(AsscComptesArchiveTableMap::COL_NOMCOURT, $nomcourt, $comparison);
    }

    /**
     * Filter the query on the id_poste column
     *
     * Example usage:
     * <code>
     * $query->filterByIdPoste(1234); // WHERE id_poste = 1234
     * $query->filterByIdPoste(array(12, 34)); // WHERE id_poste IN (12, 34)
     * $query->filterByIdPoste(array('min' => 12)); // WHERE id_poste > 12
     * </code>
     *
     * @param     mixed $idPoste The value to use as filter.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildAsscComptesArchiveQuery The current query, for fluid interface
     */
    public function filterByIdPoste($idPoste = null, $comparison = null)
    {
        if (is_array($idPoste)) {
            $useMinMax = false;
            if (isset($idPoste['min'])) {
                $this->addUsingAlias(AsscComptesArchiveTableMap::COL_ID_POSTE, $idPoste['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($idPoste['max'])) {
                $this->addUsingAlias(AsscComptesArchiveTableMap::COL_ID_POSTE, $idPoste['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(AsscComptesArchiveTableMap::COL_ID_POSTE, $idPoste, $comparison);
    }

    /**
     * Filter the query on the type_op column
     *
     * Example usage:
     * <code>
     * $query->filterByTypeOp(1234); // WHERE type_op = 1234
     * $query->filterByTypeOp(array(12, 34)); // WHERE type_op IN (12, 34)
     * $query->filterByTypeOp(array('min' => 12)); // WHERE type_op > 12
     * </code>
     *
     * @param     mixed $typeOp The value to use as filter.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildAsscComptesArchiveQuery The current query, for fluid interface
     */
    public function filterByTypeOp($typeOp = null, $comparison = null)
    {
        if (is_array($typeOp)) {
            $useMinMax = false;
            if (isset($typeOp['min'])) {
                $this->addUsingAlias(AsscComptesArchiveTableMap::COL_TYPE_OP, $typeOp['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($typeOp['max'])) {
                $this->addUsingAlias(AsscComptesArchiveTableMap::COL_TYPE_OP, $typeOp['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(AsscComptesArchiveTableMap::COL_TYPE_OP, $typeOp, $comparison);
    }

    /**
     * Filter the query on the solde_anterieur column
     *
     * Example usage:
     * <code>
     * $query->filterBySoldeAnterieur(1234); // WHERE solde_anterieur = 1234
     * $query->filterBySoldeAnterieur(array(12, 34)); // WHERE solde_anterieur IN (12, 34)
     * $query->filterBySoldeAnterieur(array('min' => 12)); // WHERE solde_anterieur > 12
     * </code>
     *
     * @param     mixed $soldeAnterieur The value to use as filter.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildAsscComptesArchiveQuery The current query, for fluid interface
     */
    public function filterBySoldeAnterieur($soldeAnterieur = null, $comparison = null)
    {
        if (is_array($soldeAnterieur)) {
            $useMinMax = false;
            if (isset($soldeAnterieur['min'])) {
                $this->addUsingAlias(AsscComptesArchiveTableMap::COL_SOLDE_ANTERIEUR, $soldeAnterieur['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($soldeAnterieur['max'])) {
                $this->addUsingAlias(AsscComptesArchiveTableMap::COL_SOLDE_ANTERIEUR, $soldeAnterieur['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(AsscComptesArchiveTableMap::COL_SOLDE_ANTERIEUR, $soldeAnterieur, $comparison);
    }

    /**
     * Filter the query on the date_anterieure column
     *
     * Example usage:
     * <code>
     * $query->filterByDateAnterieure('2011-03-14'); // WHERE date_anterieure = '2011-03-14'
     * $query->filterByDateAnterieure('now'); // WHERE date_anterieure = '2011-03-14'
     * $query->filterByDateAnterieure(array('max' => 'yesterday')); // WHERE date_anterieure > '2011-03-13'
     * </code>
     *
     * @param     mixed $dateAnterieure The value to use as filter.
     *              Values can be integers (unix timestamps), DateTime objects, or strings.
     *              Empty strings are treated as NULL.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildAsscComptesArchiveQuery The current query, for fluid interface
     */
    public function filterByDateAnterieure($dateAnterieure = null, $comparison = null)
    {
        if (is_array($dateAnterieure)) {
            $useMinMax = false;
            if (isset($dateAnterieure['min'])) {
                $this->addUsingAlias(AsscComptesArchiveTableMap::COL_DATE_ANTERIEURE, $dateAnterieure['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($dateAnterieure['max'])) {
                $this->addUsingAlias(AsscComptesArchiveTableMap::COL_DATE_ANTERIEURE, $dateAnterieure['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(AsscComptesArchiveTableMap::COL_DATE_ANTERIEURE, $dateAnterieure, $comparison);
    }

    /**
     * Filter the query on the id_entite column
     *
     * Example usage:
     * <code>
     * $query->filterByIdEntite(1234); // WHERE id_entite = 1234
     * $query->filterByIdEntite(array(12, 34)); // WHERE id_entite IN (12, 34)
     * $query->filterByIdEntite(array('min' => 12)); // WHERE id_entite > 12
     * </code>
     *
     * @param     mixed $idEntite The value to use as filter.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildAsscComptesArchiveQuery The current query, for fluid interface
     */
    public function filterByIdEntite($idEntite = null, $comparison = null)
    {
        if (is_array($idEntite)) {
            $useMinMax = false;
            if (isset($idEntite['min'])) {
                $this->addUsingAlias(AsscComptesArchiveTableMap::COL_ID_ENTITE, $idEntite['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($idEntite['max'])) {
                $this->addUsingAlias(AsscComptesArchiveTableMap::COL_ID_ENTITE, $idEntite['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(AsscComptesArchiveTableMap::COL_ID_ENTITE, $idEntite, $comparison);
    }

    /**
     * Filter the query on the observation column
     *
     * Example usage:
     * <code>
     * $query->filterByObservation('fooValue');   // WHERE observation = 'fooValue'
     * $query->filterByObservation('%fooValue%', Criteria::LIKE); // WHERE observation LIKE '%fooValue%'
     * </code>
     *
     * @param     string $observation The value to use as filter.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildAsscComptesArchiveQuery The current query, for fluid interface
     */
    public function filterByObservation($observation = null, $comparison = null)
    {
        if (null === $comparison) {
            if (is_array($observation)) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(AsscComptesArchiveTableMap::COL_OBSERVATION, $observation, $comparison);
    }

    /**
     * Filter the query on the created_at column
     *
     * Example usage:
     * <code>
     * $query->filterByCreatedAt('2011-03-14'); // WHERE created_at = '2011-03-14'
     * $query->filterByCreatedAt('now'); // WHERE created_at = '2011-03-14'
     * $query->filterByCreatedAt(array('max' => 'yesterday')); // WHERE created_at > '2011-03-13'
     * </code>
     *
     * @param     mixed $createdAt The value to use as filter.
     *              Values can be integers (unix timestamps), DateTime objects, or strings.
     *              Empty strings are treated as NULL.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildAsscComptesArchiveQuery The current query, for fluid interface
     */
    public function filterByCreatedAt($createdAt = null, $comparison = null)
    {
        if (is_array($createdAt)) {
            $useMinMax = false;
            if (isset($createdAt['min'])) {
                $this->addUsingAlias(AsscComptesArchiveTableMap::COL_CREATED_AT, $createdAt['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($createdAt['max'])) {
                $this->addUsingAlias(AsscComptesArchiveTableMap::COL_CREATED_AT, $createdAt['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(AsscComptesArchiveTableMap::COL_CREATED_AT, $createdAt, $comparison);
    }

    /**
     * Filter the query on the updated_at column
     *
     * Example usage:
     * <code>
     * $query->filterByUpdatedAt('2011-03-14'); // WHERE updated_at = '2011-03-14'
     * $query->filterByUpdatedAt('now'); // WHERE updated_at = '2011-03-14'
     * $query->filterByUpdatedAt(array('max' => 'yesterday')); // WHERE updated_at > '2011-03-13'
     * </code>
     *
     * @param     mixed $updatedAt The value to use as filter.
     *              Values can be integers (unix timestamps), DateTime objects, or strings.
     *              Empty strings are treated as NULL.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildAsscComptesArchiveQuery The current query, for fluid interface
     */
    public function filterByUpdatedAt($updatedAt = null, $comparison = null)
    {
        if (is_array($updatedAt)) {
            $useMinMax = false;
            if (isset($updatedAt['min'])) {
                $this->addUsingAlias(AsscComptesArchiveTableMap::COL_UPDATED_AT, $updatedAt['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($updatedAt['max'])) {
                $this->addUsingAlias(AsscComptesArchiveTableMap::COL_UPDATED_AT, $updatedAt['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(AsscComptesArchiveTableMap::COL_UPDATED_AT, $updatedAt, $comparison);
    }

    /**
     * Filter the query on the archived_at column
     *
     * Example usage:
     * <code>
     * $query->filterByArchivedAt('2011-03-14'); // WHERE archived_at = '2011-03-14'
     * $query->filterByArchivedAt('now'); // WHERE archived_at = '2011-03-14'
     * $query->filterByArchivedAt(array('max' => 'yesterday')); // WHERE archived_at > '2011-03-13'
     * </code>
     *
     * @param     mixed $archivedAt The value to use as filter.
     *              Values can be integers (unix timestamps), DateTime objects, or strings.
     *              Empty strings are treated as NULL.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildAsscComptesArchiveQuery The current query, for fluid interface
     */
    public function filterByArchivedAt($archivedAt = null, $comparison = null)
    {
        if (is_array($archivedAt)) {
            $useMinMax = false;
            if (isset($archivedAt['min'])) {
                $this->addUsingAlias(AsscComptesArchiveTableMap::COL_ARCHIVED_AT, $archivedAt['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($archivedAt['max'])) {
                $this->addUsingAlias(AsscComptesArchiveTableMap::COL_ARCHIVED_AT, $archivedAt['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(AsscComptesArchiveTableMap::COL_ARCHIVED_AT, $archivedAt, $comparison);
    }

    /**
     * Exclude object from result
     *
     * @param   ChildAsscComptesArchive $asscComptesArchive Object to remove from the list of results
     *
     * @return $this|ChildAsscComptesArchiveQuery The current query, for fluid interface
     */
    public function prune($asscComptesArchive = null)
    {
        if ($asscComptesArchive) {
            $this->addUsingAlias(AsscComptesArchiveTableMap::COL_ID_COMPTE, $asscComptesArchive->getIdCompte(), Criteria::NOT_EQUAL);
        }

        return $this;
    }

    /**
     * Deletes all rows from the assc_comptes_archive table.
     *
     * @param ConnectionInterface $con the connection to use
     * @return int The number of affected rows (if supported by underlying database driver).
     */
    public function doDeleteAll(ConnectionInterface $con = null)
    {
        if (null === $con) {
            $con = Propel::getServiceContainer()->getWriteConnection(AsscComptesArchiveTableMap::DATABASE_NAME);
        }

        // use transaction because $criteria could contain info
        // for more than one table or we could emulating ON DELETE CASCADE, etc.
        return $con->transaction(function () use ($con) {
            $affectedRows = 0; // initialize var to track total num of affected rows
            $affectedRows += parent::doDeleteAll($con);
            // Because this db requires some delete cascade/set null emulation, we have to
            // clear the cached instance *after* the emulation has happened (since
            // instances get re-added by the select statement contained therein).
            AsscComptesArchiveTableMap::clearInstancePool();
            AsscComptesArchiveTableMap::clearRelatedInstancePool();

            return $affectedRows;
        });
    }

    /**
     * Performs a DELETE on the database based on the current ModelCriteria
     *
     * @param ConnectionInterface $con the connection to use
     * @return int             The number of affected rows (if supported by underlying database driver).  This includes CASCADE-related rows
     *                         if supported by native driver or if emulated using Propel.
     * @throws PropelException Any exceptions caught during processing will be
     *                         rethrown wrapped into a PropelException.
     */
    public function delete(ConnectionInterface $con = null)
    {
        if (null === $con) {
            $con = Propel::getServiceContainer()->getWriteConnection(AsscComptesArchiveTableMap::DATABASE_NAME);
        }

        $criteria = $this;

        // Set the correct dbName
        $criteria->setDbName(AsscComptesArchiveTableMap::DATABASE_NAME);

        // use transaction because $criteria could contain info
        // for more than one table or we could emulating ON DELETE CASCADE, etc.
        return $con->transaction(function () use ($con, $criteria) {
            $affectedRows = 0; // initialize var to track total num of affected rows

            AsscComptesArchiveTableMap::removeInstanceFromPool($criteria);

            $affectedRows += ModelCriteria::delete($con);
            AsscComptesArchiveTableMap::clearRelatedInstancePool();

            return $affectedRows;
        });
    }

} // AsscComptesArchiveQuery
