<?php

namespace Base;

use \AssoTvatauxsArchive as ChildAssoTvatauxsArchive;
use \Tvataux as ChildTvataux;
use \TvatauxQuery as ChildTvatauxQuery;
use \Exception;
use \PDO;
use Map\TvatauxTableMap;
use Propel\Runtime\Propel;
use Propel\Runtime\ActiveQuery\Criteria;
use Propel\Runtime\ActiveQuery\ModelCriteria;
use Propel\Runtime\ActiveQuery\ModelJoin;
use Propel\Runtime\Collection\ObjectCollection;
use Propel\Runtime\Connection\ConnectionInterface;
use Propel\Runtime\Exception\PropelException;

/**
 * Base class that represents a query for the 'asso_tvatauxs' table.
 *
 *
 *
 * @method     ChildTvatauxQuery orderByIdTvataux($order = Criteria::ASC) Order by the id_tvataux column
 * @method     ChildTvatauxQuery orderByIdTva($order = Criteria::ASC) Order by the id_tva column
 * @method     ChildTvatauxQuery orderByTaux($order = Criteria::ASC) Order by the taux column
 * @method     ChildTvatauxQuery orderByDateDebut($order = Criteria::ASC) Order by the date_debut column
 * @method     ChildTvatauxQuery orderByDateFin($order = Criteria::ASC) Order by the date_fin column
 * @method     ChildTvatauxQuery orderByObservation($order = Criteria::ASC) Order by the observation column
 * @method     ChildTvatauxQuery orderByCreatedAt($order = Criteria::ASC) Order by the created_at column
 * @method     ChildTvatauxQuery orderByUpdatedAt($order = Criteria::ASC) Order by the updated_at column
 *
 * @method     ChildTvatauxQuery groupByIdTvataux() Group by the id_tvataux column
 * @method     ChildTvatauxQuery groupByIdTva() Group by the id_tva column
 * @method     ChildTvatauxQuery groupByTaux() Group by the taux column
 * @method     ChildTvatauxQuery groupByDateDebut() Group by the date_debut column
 * @method     ChildTvatauxQuery groupByDateFin() Group by the date_fin column
 * @method     ChildTvatauxQuery groupByObservation() Group by the observation column
 * @method     ChildTvatauxQuery groupByCreatedAt() Group by the created_at column
 * @method     ChildTvatauxQuery groupByUpdatedAt() Group by the updated_at column
 *
 * @method     ChildTvatauxQuery leftJoin($relation) Adds a LEFT JOIN clause to the query
 * @method     ChildTvatauxQuery rightJoin($relation) Adds a RIGHT JOIN clause to the query
 * @method     ChildTvatauxQuery innerJoin($relation) Adds a INNER JOIN clause to the query
 *
 * @method     ChildTvatauxQuery leftJoinWith($relation) Adds a LEFT JOIN clause and with to the query
 * @method     ChildTvatauxQuery rightJoinWith($relation) Adds a RIGHT JOIN clause and with to the query
 * @method     ChildTvatauxQuery innerJoinWith($relation) Adds a INNER JOIN clause and with to the query
 *
 * @method     ChildTvatauxQuery leftJoinTva($relationAlias = null) Adds a LEFT JOIN clause to the query using the Tva relation
 * @method     ChildTvatauxQuery rightJoinTva($relationAlias = null) Adds a RIGHT JOIN clause to the query using the Tva relation
 * @method     ChildTvatauxQuery innerJoinTva($relationAlias = null) Adds a INNER JOIN clause to the query using the Tva relation
 *
 * @method     ChildTvatauxQuery joinWithTva($joinType = Criteria::INNER_JOIN) Adds a join clause and with to the query using the Tva relation
 *
 * @method     ChildTvatauxQuery leftJoinWithTva() Adds a LEFT JOIN clause and with to the query using the Tva relation
 * @method     ChildTvatauxQuery rightJoinWithTva() Adds a RIGHT JOIN clause and with to the query using the Tva relation
 * @method     ChildTvatauxQuery innerJoinWithTva() Adds a INNER JOIN clause and with to the query using the Tva relation
 *
 * @method     \TvaQuery endUse() Finalizes a secondary criteria and merges it with its primary Criteria
 *
 * @method     ChildTvataux findOne(ConnectionInterface $con = null) Return the first ChildTvataux matching the query
 * @method     ChildTvataux findOneOrCreate(ConnectionInterface $con = null) Return the first ChildTvataux matching the query, or a new ChildTvataux object populated from the query conditions when no match is found
 *
 * @method     ChildTvataux findOneByIdTvataux(string $id_tvataux) Return the first ChildTvataux filtered by the id_tvataux column
 * @method     ChildTvataux findOneByIdTva(string $id_tva) Return the first ChildTvataux filtered by the id_tva column
 * @method     ChildTvataux findOneByTaux(double $taux) Return the first ChildTvataux filtered by the taux column
 * @method     ChildTvataux findOneByDateDebut(string $date_debut) Return the first ChildTvataux filtered by the date_debut column
 * @method     ChildTvataux findOneByDateFin(string $date_fin) Return the first ChildTvataux filtered by the date_fin column
 * @method     ChildTvataux findOneByObservation(string $observation) Return the first ChildTvataux filtered by the observation column
 * @method     ChildTvataux findOneByCreatedAt(string $created_at) Return the first ChildTvataux filtered by the created_at column
 * @method     ChildTvataux findOneByUpdatedAt(string $updated_at) Return the first ChildTvataux filtered by the updated_at column *

 * @method     ChildTvataux requirePk($key, ConnectionInterface $con = null) Return the ChildTvataux by primary key and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildTvataux requireOne(ConnectionInterface $con = null) Return the first ChildTvataux matching the query and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 *
 * @method     ChildTvataux requireOneByIdTvataux(string $id_tvataux) Return the first ChildTvataux filtered by the id_tvataux column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildTvataux requireOneByIdTva(string $id_tva) Return the first ChildTvataux filtered by the id_tva column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildTvataux requireOneByTaux(double $taux) Return the first ChildTvataux filtered by the taux column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildTvataux requireOneByDateDebut(string $date_debut) Return the first ChildTvataux filtered by the date_debut column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildTvataux requireOneByDateFin(string $date_fin) Return the first ChildTvataux filtered by the date_fin column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildTvataux requireOneByObservation(string $observation) Return the first ChildTvataux filtered by the observation column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildTvataux requireOneByCreatedAt(string $created_at) Return the first ChildTvataux filtered by the created_at column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildTvataux requireOneByUpdatedAt(string $updated_at) Return the first ChildTvataux filtered by the updated_at column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 *
 * @method     ChildTvataux[]|ObjectCollection find(ConnectionInterface $con = null) Return ChildTvataux objects based on current ModelCriteria
 * @method     ChildTvataux[]|ObjectCollection findByIdTvataux(string $id_tvataux) Return ChildTvataux objects filtered by the id_tvataux column
 * @method     ChildTvataux[]|ObjectCollection findByIdTva(string $id_tva) Return ChildTvataux objects filtered by the id_tva column
 * @method     ChildTvataux[]|ObjectCollection findByTaux(double $taux) Return ChildTvataux objects filtered by the taux column
 * @method     ChildTvataux[]|ObjectCollection findByDateDebut(string $date_debut) Return ChildTvataux objects filtered by the date_debut column
 * @method     ChildTvataux[]|ObjectCollection findByDateFin(string $date_fin) Return ChildTvataux objects filtered by the date_fin column
 * @method     ChildTvataux[]|ObjectCollection findByObservation(string $observation) Return ChildTvataux objects filtered by the observation column
 * @method     ChildTvataux[]|ObjectCollection findByCreatedAt(string $created_at) Return ChildTvataux objects filtered by the created_at column
 * @method     ChildTvataux[]|ObjectCollection findByUpdatedAt(string $updated_at) Return ChildTvataux objects filtered by the updated_at column
 * @method     ChildTvataux[]|\Propel\Runtime\Util\PropelModelPager paginate($page = 1, $maxPerPage = 10, ConnectionInterface $con = null) Issue a SELECT query based on the current ModelCriteria and uses a page and a maximum number of results per page to compute an offset and a limit
 *
 */
abstract class TvatauxQuery extends ModelCriteria
{

    // archivable behavior
    protected $archiveOnDelete = true;
protected $entityNotFoundExceptionClass = '\\Propel\\Runtime\\Exception\\EntityNotFoundException';

    /**
     * Initializes internal state of \Base\TvatauxQuery object.
     *
     * @param     string $dbName The database name
     * @param     string $modelName The phpName of a model, e.g. 'Book'
     * @param     string $modelAlias The alias for the model in this query, e.g. 'b'
     */
    public function __construct($dbName = 'simplasso', $modelName = '\\Tvataux', $modelAlias = null)
    {
        parent::__construct($dbName, $modelName, $modelAlias);
    }

    /**
     * Returns a new ChildTvatauxQuery object.
     *
     * @param     string $modelAlias The alias of a model in the query
     * @param     Criteria $criteria Optional Criteria to build the query from
     *
     * @return ChildTvatauxQuery
     */
    public static function create($modelAlias = null, Criteria $criteria = null)
    {
        if ($criteria instanceof ChildTvatauxQuery) {
            return $criteria;
        }
        $query = new ChildTvatauxQuery();
        if (null !== $modelAlias) {
            $query->setModelAlias($modelAlias);
        }
        if ($criteria instanceof Criteria) {
            $query->mergeWith($criteria);
        }

        return $query;
    }

    /**
     * Find object by primary key.
     * Propel uses the instance pool to skip the database if the object exists.
     * Go fast if the query is untouched.
     *
     * <code>
     * $obj  = $c->findPk(12, $con);
     * </code>
     *
     * @param mixed $key Primary key to use for the query
     * @param ConnectionInterface $con an optional connection object
     *
     * @return ChildTvataux|array|mixed the result, formatted by the current formatter
     */
    public function findPk($key, ConnectionInterface $con = null)
    {
        if ($key === null) {
            return null;
        }

        if ($con === null) {
            $con = Propel::getServiceContainer()->getReadConnection(TvatauxTableMap::DATABASE_NAME);
        }

        $this->basePreSelect($con);

        if (
            $this->formatter || $this->modelAlias || $this->with || $this->select
            || $this->selectColumns || $this->asColumns || $this->selectModifiers
            || $this->map || $this->having || $this->joins
        ) {
            return $this->findPkComplex($key, $con);
        }

        if ((null !== ($obj = TvatauxTableMap::getInstanceFromPool(null === $key || is_scalar($key) || is_callable([$key, '__toString']) ? (string) $key : $key)))) {
            // the object is already in the instance pool
            return $obj;
        }

        return $this->findPkSimple($key, $con);
    }

    /**
     * Find object by primary key using raw SQL to go fast.
     * Bypass doSelect() and the object formatter by using generated code.
     *
     * @param     mixed $key Primary key to use for the query
     * @param     ConnectionInterface $con A connection object
     *
     * @throws \Propel\Runtime\Exception\PropelException
     *
     * @return ChildTvataux A model object, or null if the key is not found
     */
    protected function findPkSimple($key, ConnectionInterface $con)
    {
        $sql = 'SELECT `id_tvataux`, `id_tva`, `taux`, `date_debut`, `date_fin`, `observation`, `created_at`, `updated_at` FROM `asso_tvatauxs` WHERE `id_tvataux` = :p0';
        try {
            $stmt = $con->prepare($sql);
            $stmt->bindValue(':p0', $key, PDO::PARAM_INT);
            $stmt->execute();
        } catch (Exception $e) {
            Propel::log($e->getMessage(), Propel::LOG_ERR);
            throw new PropelException(sprintf('Unable to execute SELECT statement [%s]', $sql), 0, $e);
        }
        $obj = null;
        if ($row = $stmt->fetch(\PDO::FETCH_NUM)) {
            /** @var ChildTvataux $obj */
            $obj = new ChildTvataux();
            $obj->hydrate($row);
            TvatauxTableMap::addInstanceToPool($obj, null === $key || is_scalar($key) || is_callable([$key, '__toString']) ? (string) $key : $key);
        }
        $stmt->closeCursor();

        return $obj;
    }

    /**
     * Find object by primary key.
     *
     * @param     mixed $key Primary key to use for the query
     * @param     ConnectionInterface $con A connection object
     *
     * @return ChildTvataux|array|mixed the result, formatted by the current formatter
     */
    protected function findPkComplex($key, ConnectionInterface $con)
    {
        // As the query uses a PK condition, no limit(1) is necessary.
        $criteria = $this->isKeepQuery() ? clone $this : $this;
        $dataFetcher = $criteria
            ->filterByPrimaryKey($key)
            ->doSelect($con);

        return $criteria->getFormatter()->init($criteria)->formatOne($dataFetcher);
    }

    /**
     * Find objects by primary key
     * <code>
     * $objs = $c->findPks(array(12, 56, 832), $con);
     * </code>
     * @param     array $keys Primary keys to use for the query
     * @param     ConnectionInterface $con an optional connection object
     *
     * @return ObjectCollection|array|mixed the list of results, formatted by the current formatter
     */
    public function findPks($keys, ConnectionInterface $con = null)
    {
        if (null === $con) {
            $con = Propel::getServiceContainer()->getReadConnection($this->getDbName());
        }
        $this->basePreSelect($con);
        $criteria = $this->isKeepQuery() ? clone $this : $this;
        $dataFetcher = $criteria
            ->filterByPrimaryKeys($keys)
            ->doSelect($con);

        return $criteria->getFormatter()->init($criteria)->format($dataFetcher);
    }

    /**
     * Filter the query by primary key
     *
     * @param     mixed $key Primary key to use for the query
     *
     * @return $this|ChildTvatauxQuery The current query, for fluid interface
     */
    public function filterByPrimaryKey($key)
    {

        return $this->addUsingAlias(TvatauxTableMap::COL_ID_TVATAUX, $key, Criteria::EQUAL);
    }

    /**
     * Filter the query by a list of primary keys
     *
     * @param     array $keys The list of primary key to use for the query
     *
     * @return $this|ChildTvatauxQuery The current query, for fluid interface
     */
    public function filterByPrimaryKeys($keys)
    {

        return $this->addUsingAlias(TvatauxTableMap::COL_ID_TVATAUX, $keys, Criteria::IN);
    }

    /**
     * Filter the query on the id_tvataux column
     *
     * Example usage:
     * <code>
     * $query->filterByIdTvataux(1234); // WHERE id_tvataux = 1234
     * $query->filterByIdTvataux(array(12, 34)); // WHERE id_tvataux IN (12, 34)
     * $query->filterByIdTvataux(array('min' => 12)); // WHERE id_tvataux > 12
     * </code>
     *
     * @param     mixed $idTvataux The value to use as filter.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildTvatauxQuery The current query, for fluid interface
     */
    public function filterByIdTvataux($idTvataux = null, $comparison = null)
    {
        if (is_array($idTvataux)) {
            $useMinMax = false;
            if (isset($idTvataux['min'])) {
                $this->addUsingAlias(TvatauxTableMap::COL_ID_TVATAUX, $idTvataux['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($idTvataux['max'])) {
                $this->addUsingAlias(TvatauxTableMap::COL_ID_TVATAUX, $idTvataux['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(TvatauxTableMap::COL_ID_TVATAUX, $idTvataux, $comparison);
    }

    /**
     * Filter the query on the id_tva column
     *
     * Example usage:
     * <code>
     * $query->filterByIdTva(1234); // WHERE id_tva = 1234
     * $query->filterByIdTva(array(12, 34)); // WHERE id_tva IN (12, 34)
     * $query->filterByIdTva(array('min' => 12)); // WHERE id_tva > 12
     * </code>
     *
     * @see       filterByTva()
     *
     * @param     mixed $idTva The value to use as filter.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildTvatauxQuery The current query, for fluid interface
     */
    public function filterByIdTva($idTva = null, $comparison = null)
    {
        if (is_array($idTva)) {
            $useMinMax = false;
            if (isset($idTva['min'])) {
                $this->addUsingAlias(TvatauxTableMap::COL_ID_TVA, $idTva['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($idTva['max'])) {
                $this->addUsingAlias(TvatauxTableMap::COL_ID_TVA, $idTva['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(TvatauxTableMap::COL_ID_TVA, $idTva, $comparison);
    }

    /**
     * Filter the query on the taux column
     *
     * Example usage:
     * <code>
     * $query->filterByTaux(1234); // WHERE taux = 1234
     * $query->filterByTaux(array(12, 34)); // WHERE taux IN (12, 34)
     * $query->filterByTaux(array('min' => 12)); // WHERE taux > 12
     * </code>
     *
     * @param     mixed $taux The value to use as filter.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildTvatauxQuery The current query, for fluid interface
     */
    public function filterByTaux($taux = null, $comparison = null)
    {
        if (is_array($taux)) {
            $useMinMax = false;
            if (isset($taux['min'])) {
                $this->addUsingAlias(TvatauxTableMap::COL_TAUX, $taux['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($taux['max'])) {
                $this->addUsingAlias(TvatauxTableMap::COL_TAUX, $taux['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(TvatauxTableMap::COL_TAUX, $taux, $comparison);
    }

    /**
     * Filter the query on the date_debut column
     *
     * Example usage:
     * <code>
     * $query->filterByDateDebut('2011-03-14'); // WHERE date_debut = '2011-03-14'
     * $query->filterByDateDebut('now'); // WHERE date_debut = '2011-03-14'
     * $query->filterByDateDebut(array('max' => 'yesterday')); // WHERE date_debut > '2011-03-13'
     * </code>
     *
     * @param     mixed $dateDebut The value to use as filter.
     *              Values can be integers (unix timestamps), DateTime objects, or strings.
     *              Empty strings are treated as NULL.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildTvatauxQuery The current query, for fluid interface
     */
    public function filterByDateDebut($dateDebut = null, $comparison = null)
    {
        if (is_array($dateDebut)) {
            $useMinMax = false;
            if (isset($dateDebut['min'])) {
                $this->addUsingAlias(TvatauxTableMap::COL_DATE_DEBUT, $dateDebut['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($dateDebut['max'])) {
                $this->addUsingAlias(TvatauxTableMap::COL_DATE_DEBUT, $dateDebut['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(TvatauxTableMap::COL_DATE_DEBUT, $dateDebut, $comparison);
    }

    /**
     * Filter the query on the date_fin column
     *
     * Example usage:
     * <code>
     * $query->filterByDateFin('2011-03-14'); // WHERE date_fin = '2011-03-14'
     * $query->filterByDateFin('now'); // WHERE date_fin = '2011-03-14'
     * $query->filterByDateFin(array('max' => 'yesterday')); // WHERE date_fin > '2011-03-13'
     * </code>
     *
     * @param     mixed $dateFin The value to use as filter.
     *              Values can be integers (unix timestamps), DateTime objects, or strings.
     *              Empty strings are treated as NULL.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildTvatauxQuery The current query, for fluid interface
     */
    public function filterByDateFin($dateFin = null, $comparison = null)
    {
        if (is_array($dateFin)) {
            $useMinMax = false;
            if (isset($dateFin['min'])) {
                $this->addUsingAlias(TvatauxTableMap::COL_DATE_FIN, $dateFin['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($dateFin['max'])) {
                $this->addUsingAlias(TvatauxTableMap::COL_DATE_FIN, $dateFin['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(TvatauxTableMap::COL_DATE_FIN, $dateFin, $comparison);
    }

    /**
     * Filter the query on the observation column
     *
     * Example usage:
     * <code>
     * $query->filterByObservation('fooValue');   // WHERE observation = 'fooValue'
     * $query->filterByObservation('%fooValue%', Criteria::LIKE); // WHERE observation LIKE '%fooValue%'
     * </code>
     *
     * @param     string $observation The value to use as filter.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildTvatauxQuery The current query, for fluid interface
     */
    public function filterByObservation($observation = null, $comparison = null)
    {
        if (null === $comparison) {
            if (is_array($observation)) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(TvatauxTableMap::COL_OBSERVATION, $observation, $comparison);
    }

    /**
     * Filter the query on the created_at column
     *
     * Example usage:
     * <code>
     * $query->filterByCreatedAt('2011-03-14'); // WHERE created_at = '2011-03-14'
     * $query->filterByCreatedAt('now'); // WHERE created_at = '2011-03-14'
     * $query->filterByCreatedAt(array('max' => 'yesterday')); // WHERE created_at > '2011-03-13'
     * </code>
     *
     * @param     mixed $createdAt The value to use as filter.
     *              Values can be integers (unix timestamps), DateTime objects, or strings.
     *              Empty strings are treated as NULL.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildTvatauxQuery The current query, for fluid interface
     */
    public function filterByCreatedAt($createdAt = null, $comparison = null)
    {
        if (is_array($createdAt)) {
            $useMinMax = false;
            if (isset($createdAt['min'])) {
                $this->addUsingAlias(TvatauxTableMap::COL_CREATED_AT, $createdAt['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($createdAt['max'])) {
                $this->addUsingAlias(TvatauxTableMap::COL_CREATED_AT, $createdAt['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(TvatauxTableMap::COL_CREATED_AT, $createdAt, $comparison);
    }

    /**
     * Filter the query on the updated_at column
     *
     * Example usage:
     * <code>
     * $query->filterByUpdatedAt('2011-03-14'); // WHERE updated_at = '2011-03-14'
     * $query->filterByUpdatedAt('now'); // WHERE updated_at = '2011-03-14'
     * $query->filterByUpdatedAt(array('max' => 'yesterday')); // WHERE updated_at > '2011-03-13'
     * </code>
     *
     * @param     mixed $updatedAt The value to use as filter.
     *              Values can be integers (unix timestamps), DateTime objects, or strings.
     *              Empty strings are treated as NULL.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildTvatauxQuery The current query, for fluid interface
     */
    public function filterByUpdatedAt($updatedAt = null, $comparison = null)
    {
        if (is_array($updatedAt)) {
            $useMinMax = false;
            if (isset($updatedAt['min'])) {
                $this->addUsingAlias(TvatauxTableMap::COL_UPDATED_AT, $updatedAt['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($updatedAt['max'])) {
                $this->addUsingAlias(TvatauxTableMap::COL_UPDATED_AT, $updatedAt['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(TvatauxTableMap::COL_UPDATED_AT, $updatedAt, $comparison);
    }

    /**
     * Filter the query by a related \Tva object
     *
     * @param \Tva|ObjectCollection $tva The related object(s) to use as filter
     * @param string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @throws \Propel\Runtime\Exception\PropelException
     *
     * @return ChildTvatauxQuery The current query, for fluid interface
     */
    public function filterByTva($tva, $comparison = null)
    {
        if ($tva instanceof \Tva) {
            return $this
                ->addUsingAlias(TvatauxTableMap::COL_ID_TVA, $tva->getIdTva(), $comparison);
        } elseif ($tva instanceof ObjectCollection) {
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }

            return $this
                ->addUsingAlias(TvatauxTableMap::COL_ID_TVA, $tva->toKeyValue('PrimaryKey', 'IdTva'), $comparison);
        } else {
            throw new PropelException('filterByTva() only accepts arguments of type \Tva or Collection');
        }
    }

    /**
     * Adds a JOIN clause to the query using the Tva relation
     *
     * @param     string $relationAlias optional alias for the relation
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return $this|ChildTvatauxQuery The current query, for fluid interface
     */
    public function joinTva($relationAlias = null, $joinType = Criteria::INNER_JOIN)
    {
        $tableMap = $this->getTableMap();
        $relationMap = $tableMap->getRelation('Tva');

        // create a ModelJoin object for this join
        $join = new ModelJoin();
        $join->setJoinType($joinType);
        $join->setRelationMap($relationMap, $this->useAliasInSQL ? $this->getModelAlias() : null, $relationAlias);
        if ($previousJoin = $this->getPreviousJoin()) {
            $join->setPreviousJoin($previousJoin);
        }

        // add the ModelJoin to the current object
        if ($relationAlias) {
            $this->addAlias($relationAlias, $relationMap->getRightTable()->getName());
            $this->addJoinObject($join, $relationAlias);
        } else {
            $this->addJoinObject($join, 'Tva');
        }

        return $this;
    }

    /**
     * Use the Tva relation Tva object
     *
     * @see useQuery()
     *
     * @param     string $relationAlias optional alias for the relation,
     *                                   to be used as main alias in the secondary query
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return \TvaQuery A secondary query class using the current class as primary query
     */
    public function useTvaQuery($relationAlias = null, $joinType = Criteria::INNER_JOIN)
    {
        return $this
            ->joinTva($relationAlias, $joinType)
            ->useQuery($relationAlias ? $relationAlias : 'Tva', '\TvaQuery');
    }

    /**
     * Exclude object from result
     *
     * @param   ChildTvataux $tvataux Object to remove from the list of results
     *
     * @return $this|ChildTvatauxQuery The current query, for fluid interface
     */
    public function prune($tvataux = null)
    {
        if ($tvataux) {
            $this->addUsingAlias(TvatauxTableMap::COL_ID_TVATAUX, $tvataux->getIdTvataux(), Criteria::NOT_EQUAL);
        }

        return $this;
    }

    /**
     * Code to execute before every DELETE statement
     *
     * @param     ConnectionInterface $con The connection object used by the query
     */
    protected function basePreDelete(ConnectionInterface $con)
    {
        // archivable behavior

        if ($this->archiveOnDelete) {
            $this->archive($con);
        } else {
            $this->archiveOnDelete = true;
        }


        return $this->preDelete($con);
    }

    /**
     * Deletes all rows from the asso_tvatauxs table.
     *
     * @param ConnectionInterface $con the connection to use
     * @return int The number of affected rows (if supported by underlying database driver).
     */
    public function doDeleteAll(ConnectionInterface $con = null)
    {
        if (null === $con) {
            $con = Propel::getServiceContainer()->getWriteConnection(TvatauxTableMap::DATABASE_NAME);
        }

        // use transaction because $criteria could contain info
        // for more than one table or we could emulating ON DELETE CASCADE, etc.
        return $con->transaction(function () use ($con) {
            $affectedRows = 0; // initialize var to track total num of affected rows
            $affectedRows += parent::doDeleteAll($con);
            // Because this db requires some delete cascade/set null emulation, we have to
            // clear the cached instance *after* the emulation has happened (since
            // instances get re-added by the select statement contained therein).
            TvatauxTableMap::clearInstancePool();
            TvatauxTableMap::clearRelatedInstancePool();

            return $affectedRows;
        });
    }

    /**
     * Performs a DELETE on the database based on the current ModelCriteria
     *
     * @param ConnectionInterface $con the connection to use
     * @return int             The number of affected rows (if supported by underlying database driver).  This includes CASCADE-related rows
     *                         if supported by native driver or if emulated using Propel.
     * @throws PropelException Any exceptions caught during processing will be
     *                         rethrown wrapped into a PropelException.
     */
    public function delete(ConnectionInterface $con = null)
    {
        if (null === $con) {
            $con = Propel::getServiceContainer()->getWriteConnection(TvatauxTableMap::DATABASE_NAME);
        }

        $criteria = $this;

        // Set the correct dbName
        $criteria->setDbName(TvatauxTableMap::DATABASE_NAME);

        // use transaction because $criteria could contain info
        // for more than one table or we could emulating ON DELETE CASCADE, etc.
        return $con->transaction(function () use ($con, $criteria) {
            $affectedRows = 0; // initialize var to track total num of affected rows

            TvatauxTableMap::removeInstanceFromPool($criteria);

            $affectedRows += ModelCriteria::delete($con);
            TvatauxTableMap::clearRelatedInstancePool();

            return $affectedRows;
        });
    }

    // timestampable behavior

    /**
     * Filter by the latest updated
     *
     * @param      int $nbDays Maximum age of the latest update in days
     *
     * @return     $this|ChildTvatauxQuery The current query, for fluid interface
     */
    public function recentlyUpdated($nbDays = 7)
    {
        return $this->addUsingAlias(TvatauxTableMap::COL_UPDATED_AT, time() - $nbDays * 24 * 60 * 60, Criteria::GREATER_EQUAL);
    }

    /**
     * Order by update date desc
     *
     * @return     $this|ChildTvatauxQuery The current query, for fluid interface
     */
    public function lastUpdatedFirst()
    {
        return $this->addDescendingOrderByColumn(TvatauxTableMap::COL_UPDATED_AT);
    }

    /**
     * Order by update date asc
     *
     * @return     $this|ChildTvatauxQuery The current query, for fluid interface
     */
    public function firstUpdatedFirst()
    {
        return $this->addAscendingOrderByColumn(TvatauxTableMap::COL_UPDATED_AT);
    }

    /**
     * Order by create date desc
     *
     * @return     $this|ChildTvatauxQuery The current query, for fluid interface
     */
    public function lastCreatedFirst()
    {
        return $this->addDescendingOrderByColumn(TvatauxTableMap::COL_CREATED_AT);
    }

    /**
     * Filter by the latest created
     *
     * @param      int $nbDays Maximum age of in days
     *
     * @return     $this|ChildTvatauxQuery The current query, for fluid interface
     */
    public function recentlyCreated($nbDays = 7)
    {
        return $this->addUsingAlias(TvatauxTableMap::COL_CREATED_AT, time() - $nbDays * 24 * 60 * 60, Criteria::GREATER_EQUAL);
    }

    /**
     * Order by create date asc
     *
     * @return     $this|ChildTvatauxQuery The current query, for fluid interface
     */
    public function firstCreatedFirst()
    {
        return $this->addAscendingOrderByColumn(TvatauxTableMap::COL_CREATED_AT);
    }

    // archivable behavior

    /**
     * Copy the data of the objects satisfying the query into ChildAssoTvatauxsArchive archive objects.
     * The archived objects are then saved.
     * If any of the objects has already been archived, the archived object
     * is updated and not duplicated.
     * Warning: This termination methods issues 2n+1 queries.
     *
     * @param      ConnectionInterface $con    Connection to use.
     * @param      Boolean $useLittleMemory    Whether or not to use OnDemandFormatter to retrieve objects.
     *               Set to false if the identity map matters.
     *               Set to true (default) to use less memory.
     *
     * @return     int the number of archived objects
     */
    public function archive($con = null, $useLittleMemory = true)
    {
        $criteria = clone $this;
        // prepare the query
        $criteria->setWith(array());
        if ($useLittleMemory) {
            $criteria->setFormatter(ModelCriteria::FORMAT_ON_DEMAND);
        }
        if ($con === null) {
            $con = Propel::getServiceContainer()->getWriteConnection(TvatauxTableMap::DATABASE_NAME);
        }

        return $con->transaction(function () use ($con, $criteria) {
            $totalArchivedObjects = 0;

            // archive all results one by one
            foreach ($criteria->find($con) as $object) {
                $object->archive($con);
                $totalArchivedObjects++;
            }

            return $totalArchivedObjects;
        });
    }

    /**
     * Enable/disable auto-archiving on delete for the next query.
     *
     * @param boolean True if the query must archive deleted objects, false otherwise.
     */
    public function setArchiveOnDelete($archiveOnDelete)
    {
        $this->archiveOnDelete = $archiveOnDelete;
    }

    /**
     * Delete records matching the current query without archiving them.
     *
     * @param      ConnectionInterface $con    Connection to use.
     *
     * @return integer the number of deleted rows
     */
    public function deleteWithoutArchive($con = null)
    {
        $this->archiveOnDelete = false;

        return $this->delete($con);
    }

    /**
     * Delete all records without archiving them.
     *
     * @param      ConnectionInterface $con    Connection to use.
     *
     * @return integer the number of deleted rows
     */
    public function deleteAllWithoutArchive($con = null)
    {
        $this->archiveOnDelete = false;

        return $this->deleteAll($con);
    }

} // TvatauxQuery
