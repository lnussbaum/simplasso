<?php

namespace Base;

use \AssoEntitesArchive as ChildAssoEntitesArchive;
use \Entite as ChildEntite;
use \EntiteQuery as ChildEntiteQuery;
use \Exception;
use \PDO;
use Map\EntiteTableMap;
use Propel\Runtime\Propel;
use Propel\Runtime\ActiveQuery\Criteria;
use Propel\Runtime\ActiveQuery\ModelCriteria;
use Propel\Runtime\ActiveQuery\ModelJoin;
use Propel\Runtime\Collection\ObjectCollection;
use Propel\Runtime\Connection\ConnectionInterface;
use Propel\Runtime\Exception\PropelException;

/**
 * Base class that represents a query for the 'asso_entites' table.
 *
 *
 *
 * @method     ChildEntiteQuery orderByIdEntite($order = Criteria::ASC) Order by the id_entite column
 * @method     ChildEntiteQuery orderByNom($order = Criteria::ASC) Order by the nom column
 * @method     ChildEntiteQuery orderByNomcourt($order = Criteria::ASC) Order by the nomcourt column
 * @method     ChildEntiteQuery orderByAdresse($order = Criteria::ASC) Order by the adresse column
 * @method     ChildEntiteQuery orderByCodepostal($order = Criteria::ASC) Order by the codepostal column
 * @method     ChildEntiteQuery orderByVille($order = Criteria::ASC) Order by the ville column
 * @method     ChildEntiteQuery orderByPays($order = Criteria::ASC) Order by the pays column
 * @method     ChildEntiteQuery orderByTelephone($order = Criteria::ASC) Order by the telephone column
 * @method     ChildEntiteQuery orderByFax($order = Criteria::ASC) Order by the fax column
 * @method     ChildEntiteQuery orderByUrl($order = Criteria::ASC) Order by the url column
 * @method     ChildEntiteQuery orderByAssujettitva($order = Criteria::ASC) Order by the assujettitva column
 * @method     ChildEntiteQuery orderByEmail($order = Criteria::ASC) Order by the email column
 * @method     ChildEntiteQuery orderByCreatedAt($order = Criteria::ASC) Order by the created_at column
 * @method     ChildEntiteQuery orderByUpdatedAt($order = Criteria::ASC) Order by the updated_at column
 *
 * @method     ChildEntiteQuery groupByIdEntite() Group by the id_entite column
 * @method     ChildEntiteQuery groupByNom() Group by the nom column
 * @method     ChildEntiteQuery groupByNomcourt() Group by the nomcourt column
 * @method     ChildEntiteQuery groupByAdresse() Group by the adresse column
 * @method     ChildEntiteQuery groupByCodepostal() Group by the codepostal column
 * @method     ChildEntiteQuery groupByVille() Group by the ville column
 * @method     ChildEntiteQuery groupByPays() Group by the pays column
 * @method     ChildEntiteQuery groupByTelephone() Group by the telephone column
 * @method     ChildEntiteQuery groupByFax() Group by the fax column
 * @method     ChildEntiteQuery groupByUrl() Group by the url column
 * @method     ChildEntiteQuery groupByAssujettitva() Group by the assujettitva column
 * @method     ChildEntiteQuery groupByEmail() Group by the email column
 * @method     ChildEntiteQuery groupByCreatedAt() Group by the created_at column
 * @method     ChildEntiteQuery groupByUpdatedAt() Group by the updated_at column
 *
 * @method     ChildEntiteQuery leftJoin($relation) Adds a LEFT JOIN clause to the query
 * @method     ChildEntiteQuery rightJoin($relation) Adds a RIGHT JOIN clause to the query
 * @method     ChildEntiteQuery innerJoin($relation) Adds a INNER JOIN clause to the query
 *
 * @method     ChildEntiteQuery leftJoinWith($relation) Adds a LEFT JOIN clause and with to the query
 * @method     ChildEntiteQuery rightJoinWith($relation) Adds a RIGHT JOIN clause and with to the query
 * @method     ChildEntiteQuery innerJoinWith($relation) Adds a INNER JOIN clause and with to the query
 *
 * @method     ChildEntiteQuery leftJoinActivite($relationAlias = null) Adds a LEFT JOIN clause to the query using the Activite relation
 * @method     ChildEntiteQuery rightJoinActivite($relationAlias = null) Adds a RIGHT JOIN clause to the query using the Activite relation
 * @method     ChildEntiteQuery innerJoinActivite($relationAlias = null) Adds a INNER JOIN clause to the query using the Activite relation
 *
 * @method     ChildEntiteQuery joinWithActivite($joinType = Criteria::INNER_JOIN) Adds a join clause and with to the query using the Activite relation
 *
 * @method     ChildEntiteQuery leftJoinWithActivite() Adds a LEFT JOIN clause and with to the query using the Activite relation
 * @method     ChildEntiteQuery rightJoinWithActivite() Adds a RIGHT JOIN clause and with to the query using the Activite relation
 * @method     ChildEntiteQuery innerJoinWithActivite() Adds a INNER JOIN clause and with to the query using the Activite relation
 *
 * @method     ChildEntiteQuery leftJoinCompte($relationAlias = null) Adds a LEFT JOIN clause to the query using the Compte relation
 * @method     ChildEntiteQuery rightJoinCompte($relationAlias = null) Adds a RIGHT JOIN clause to the query using the Compte relation
 * @method     ChildEntiteQuery innerJoinCompte($relationAlias = null) Adds a INNER JOIN clause to the query using the Compte relation
 *
 * @method     ChildEntiteQuery joinWithCompte($joinType = Criteria::INNER_JOIN) Adds a join clause and with to the query using the Compte relation
 *
 * @method     ChildEntiteQuery leftJoinWithCompte() Adds a LEFT JOIN clause and with to the query using the Compte relation
 * @method     ChildEntiteQuery rightJoinWithCompte() Adds a RIGHT JOIN clause and with to the query using the Compte relation
 * @method     ChildEntiteQuery innerJoinWithCompte() Adds a INNER JOIN clause and with to the query using the Compte relation
 *
 * @method     ChildEntiteQuery leftJoinEcriture($relationAlias = null) Adds a LEFT JOIN clause to the query using the Ecriture relation
 * @method     ChildEntiteQuery rightJoinEcriture($relationAlias = null) Adds a RIGHT JOIN clause to the query using the Ecriture relation
 * @method     ChildEntiteQuery innerJoinEcriture($relationAlias = null) Adds a INNER JOIN clause to the query using the Ecriture relation
 *
 * @method     ChildEntiteQuery joinWithEcriture($joinType = Criteria::INNER_JOIN) Adds a join clause and with to the query using the Ecriture relation
 *
 * @method     ChildEntiteQuery leftJoinWithEcriture() Adds a LEFT JOIN clause and with to the query using the Ecriture relation
 * @method     ChildEntiteQuery rightJoinWithEcriture() Adds a RIGHT JOIN clause and with to the query using the Ecriture relation
 * @method     ChildEntiteQuery innerJoinWithEcriture() Adds a INNER JOIN clause and with to the query using the Ecriture relation
 *
 * @method     ChildEntiteQuery leftJoinJournal($relationAlias = null) Adds a LEFT JOIN clause to the query using the Journal relation
 * @method     ChildEntiteQuery rightJoinJournal($relationAlias = null) Adds a RIGHT JOIN clause to the query using the Journal relation
 * @method     ChildEntiteQuery innerJoinJournal($relationAlias = null) Adds a INNER JOIN clause to the query using the Journal relation
 *
 * @method     ChildEntiteQuery joinWithJournal($joinType = Criteria::INNER_JOIN) Adds a join clause and with to the query using the Journal relation
 *
 * @method     ChildEntiteQuery leftJoinWithJournal() Adds a LEFT JOIN clause and with to the query using the Journal relation
 * @method     ChildEntiteQuery rightJoinWithJournal() Adds a RIGHT JOIN clause and with to the query using the Journal relation
 * @method     ChildEntiteQuery innerJoinWithJournal() Adds a INNER JOIN clause and with to the query using the Journal relation
 *
 * @method     ChildEntiteQuery leftJoinPiece($relationAlias = null) Adds a LEFT JOIN clause to the query using the Piece relation
 * @method     ChildEntiteQuery rightJoinPiece($relationAlias = null) Adds a RIGHT JOIN clause to the query using the Piece relation
 * @method     ChildEntiteQuery innerJoinPiece($relationAlias = null) Adds a INNER JOIN clause to the query using the Piece relation
 *
 * @method     ChildEntiteQuery joinWithPiece($joinType = Criteria::INNER_JOIN) Adds a join clause and with to the query using the Piece relation
 *
 * @method     ChildEntiteQuery leftJoinWithPiece() Adds a LEFT JOIN clause and with to the query using the Piece relation
 * @method     ChildEntiteQuery rightJoinWithPiece() Adds a RIGHT JOIN clause and with to the query using the Piece relation
 * @method     ChildEntiteQuery innerJoinWithPiece() Adds a INNER JOIN clause and with to the query using the Piece relation
 *
 * @method     ChildEntiteQuery leftJoinAutorisation($relationAlias = null) Adds a LEFT JOIN clause to the query using the Autorisation relation
 * @method     ChildEntiteQuery rightJoinAutorisation($relationAlias = null) Adds a RIGHT JOIN clause to the query using the Autorisation relation
 * @method     ChildEntiteQuery innerJoinAutorisation($relationAlias = null) Adds a INNER JOIN clause to the query using the Autorisation relation
 *
 * @method     ChildEntiteQuery joinWithAutorisation($joinType = Criteria::INNER_JOIN) Adds a join clause and with to the query using the Autorisation relation
 *
 * @method     ChildEntiteQuery leftJoinWithAutorisation() Adds a LEFT JOIN clause and with to the query using the Autorisation relation
 * @method     ChildEntiteQuery rightJoinWithAutorisation() Adds a RIGHT JOIN clause and with to the query using the Autorisation relation
 * @method     ChildEntiteQuery innerJoinWithAutorisation() Adds a INNER JOIN clause and with to the query using the Autorisation relation
 *
 * @method     ChildEntiteQuery leftJoinPaiement($relationAlias = null) Adds a LEFT JOIN clause to the query using the Paiement relation
 * @method     ChildEntiteQuery rightJoinPaiement($relationAlias = null) Adds a RIGHT JOIN clause to the query using the Paiement relation
 * @method     ChildEntiteQuery innerJoinPaiement($relationAlias = null) Adds a INNER JOIN clause to the query using the Paiement relation
 *
 * @method     ChildEntiteQuery joinWithPaiement($joinType = Criteria::INNER_JOIN) Adds a join clause and with to the query using the Paiement relation
 *
 * @method     ChildEntiteQuery leftJoinWithPaiement() Adds a LEFT JOIN clause and with to the query using the Paiement relation
 * @method     ChildEntiteQuery rightJoinWithPaiement() Adds a RIGHT JOIN clause and with to the query using the Paiement relation
 * @method     ChildEntiteQuery innerJoinWithPaiement() Adds a INNER JOIN clause and with to the query using the Paiement relation
 *
 * @method     ChildEntiteQuery leftJoinPrestation($relationAlias = null) Adds a LEFT JOIN clause to the query using the Prestation relation
 * @method     ChildEntiteQuery rightJoinPrestation($relationAlias = null) Adds a RIGHT JOIN clause to the query using the Prestation relation
 * @method     ChildEntiteQuery innerJoinPrestation($relationAlias = null) Adds a INNER JOIN clause to the query using the Prestation relation
 *
 * @method     ChildEntiteQuery joinWithPrestation($joinType = Criteria::INNER_JOIN) Adds a join clause and with to the query using the Prestation relation
 *
 * @method     ChildEntiteQuery leftJoinWithPrestation() Adds a LEFT JOIN clause and with to the query using the Prestation relation
 * @method     ChildEntiteQuery rightJoinWithPrestation() Adds a RIGHT JOIN clause and with to the query using the Prestation relation
 * @method     ChildEntiteQuery innerJoinWithPrestation() Adds a INNER JOIN clause and with to the query using the Prestation relation
 *
 * @method     ChildEntiteQuery leftJoinPrestationslot($relationAlias = null) Adds a LEFT JOIN clause to the query using the Prestationslot relation
 * @method     ChildEntiteQuery rightJoinPrestationslot($relationAlias = null) Adds a RIGHT JOIN clause to the query using the Prestationslot relation
 * @method     ChildEntiteQuery innerJoinPrestationslot($relationAlias = null) Adds a INNER JOIN clause to the query using the Prestationslot relation
 *
 * @method     ChildEntiteQuery joinWithPrestationslot($joinType = Criteria::INNER_JOIN) Adds a join clause and with to the query using the Prestationslot relation
 *
 * @method     ChildEntiteQuery leftJoinWithPrestationslot() Adds a LEFT JOIN clause and with to the query using the Prestationslot relation
 * @method     ChildEntiteQuery rightJoinWithPrestationslot() Adds a RIGHT JOIN clause and with to the query using the Prestationslot relation
 * @method     ChildEntiteQuery innerJoinWithPrestationslot() Adds a INNER JOIN clause and with to the query using the Prestationslot relation
 *
 * @method     ChildEntiteQuery leftJoinServicerendu($relationAlias = null) Adds a LEFT JOIN clause to the query using the Servicerendu relation
 * @method     ChildEntiteQuery rightJoinServicerendu($relationAlias = null) Adds a RIGHT JOIN clause to the query using the Servicerendu relation
 * @method     ChildEntiteQuery innerJoinServicerendu($relationAlias = null) Adds a INNER JOIN clause to the query using the Servicerendu relation
 *
 * @method     ChildEntiteQuery joinWithServicerendu($joinType = Criteria::INNER_JOIN) Adds a join clause and with to the query using the Servicerendu relation
 *
 * @method     ChildEntiteQuery leftJoinWithServicerendu() Adds a LEFT JOIN clause and with to the query using the Servicerendu relation
 * @method     ChildEntiteQuery rightJoinWithServicerendu() Adds a RIGHT JOIN clause and with to the query using the Servicerendu relation
 * @method     ChildEntiteQuery innerJoinWithServicerendu() Adds a INNER JOIN clause and with to the query using the Servicerendu relation
 *
 * @method     ChildEntiteQuery leftJoinTresor($relationAlias = null) Adds a LEFT JOIN clause to the query using the Tresor relation
 * @method     ChildEntiteQuery rightJoinTresor($relationAlias = null) Adds a RIGHT JOIN clause to the query using the Tresor relation
 * @method     ChildEntiteQuery innerJoinTresor($relationAlias = null) Adds a INNER JOIN clause to the query using the Tresor relation
 *
 * @method     ChildEntiteQuery joinWithTresor($joinType = Criteria::INNER_JOIN) Adds a join clause and with to the query using the Tresor relation
 *
 * @method     ChildEntiteQuery leftJoinWithTresor() Adds a LEFT JOIN clause and with to the query using the Tresor relation
 * @method     ChildEntiteQuery rightJoinWithTresor() Adds a RIGHT JOIN clause and with to the query using the Tresor relation
 * @method     ChildEntiteQuery innerJoinWithTresor() Adds a INNER JOIN clause and with to the query using the Tresor relation
 *
 * @method     ChildEntiteQuery leftJoinTva($relationAlias = null) Adds a LEFT JOIN clause to the query using the Tva relation
 * @method     ChildEntiteQuery rightJoinTva($relationAlias = null) Adds a RIGHT JOIN clause to the query using the Tva relation
 * @method     ChildEntiteQuery innerJoinTva($relationAlias = null) Adds a INNER JOIN clause to the query using the Tva relation
 *
 * @method     ChildEntiteQuery joinWithTva($joinType = Criteria::INNER_JOIN) Adds a join clause and with to the query using the Tva relation
 *
 * @method     ChildEntiteQuery leftJoinWithTva() Adds a LEFT JOIN clause and with to the query using the Tva relation
 * @method     ChildEntiteQuery rightJoinWithTva() Adds a RIGHT JOIN clause and with to the query using the Tva relation
 * @method     ChildEntiteQuery innerJoinWithTva() Adds a INNER JOIN clause and with to the query using the Tva relation
 *
 * @method     ChildEntiteQuery leftJoinCourrier($relationAlias = null) Adds a LEFT JOIN clause to the query using the Courrier relation
 * @method     ChildEntiteQuery rightJoinCourrier($relationAlias = null) Adds a RIGHT JOIN clause to the query using the Courrier relation
 * @method     ChildEntiteQuery innerJoinCourrier($relationAlias = null) Adds a INNER JOIN clause to the query using the Courrier relation
 *
 * @method     ChildEntiteQuery joinWithCourrier($joinType = Criteria::INNER_JOIN) Adds a join clause and with to the query using the Courrier relation
 *
 * @method     ChildEntiteQuery leftJoinWithCourrier() Adds a LEFT JOIN clause and with to the query using the Courrier relation
 * @method     ChildEntiteQuery rightJoinWithCourrier() Adds a RIGHT JOIN clause and with to the query using the Courrier relation
 * @method     ChildEntiteQuery innerJoinWithCourrier() Adds a INNER JOIN clause and with to the query using the Courrier relation
 *
 * @method     \ActiviteQuery|\CompteQuery|\EcritureQuery|\JournalQuery|\PieceQuery|\AutorisationQuery|\PaiementQuery|\PrestationQuery|\PrestationslotQuery|\ServicerenduQuery|\TresorQuery|\TvaQuery|\CourrierQuery endUse() Finalizes a secondary criteria and merges it with its primary Criteria
 *
 * @method     ChildEntite findOne(ConnectionInterface $con = null) Return the first ChildEntite matching the query
 * @method     ChildEntite findOneOrCreate(ConnectionInterface $con = null) Return the first ChildEntite matching the query, or a new ChildEntite object populated from the query conditions when no match is found
 *
 * @method     ChildEntite findOneByIdEntite(string $id_entite) Return the first ChildEntite filtered by the id_entite column
 * @method     ChildEntite findOneByNom(string $nom) Return the first ChildEntite filtered by the nom column
 * @method     ChildEntite findOneByNomcourt(string $nomcourt) Return the first ChildEntite filtered by the nomcourt column
 * @method     ChildEntite findOneByAdresse(string $adresse) Return the first ChildEntite filtered by the adresse column
 * @method     ChildEntite findOneByCodepostal(string $codepostal) Return the first ChildEntite filtered by the codepostal column
 * @method     ChildEntite findOneByVille(string $ville) Return the first ChildEntite filtered by the ville column
 * @method     ChildEntite findOneByPays(string $pays) Return the first ChildEntite filtered by the pays column
 * @method     ChildEntite findOneByTelephone(string $telephone) Return the first ChildEntite filtered by the telephone column
 * @method     ChildEntite findOneByFax(string $fax) Return the first ChildEntite filtered by the fax column
 * @method     ChildEntite findOneByUrl(string $url) Return the first ChildEntite filtered by the url column
 * @method     ChildEntite findOneByAssujettitva(int $assujettitva) Return the first ChildEntite filtered by the assujettitva column
 * @method     ChildEntite findOneByEmail(string $email) Return the first ChildEntite filtered by the email column
 * @method     ChildEntite findOneByCreatedAt(string $created_at) Return the first ChildEntite filtered by the created_at column
 * @method     ChildEntite findOneByUpdatedAt(string $updated_at) Return the first ChildEntite filtered by the updated_at column *

 * @method     ChildEntite requirePk($key, ConnectionInterface $con = null) Return the ChildEntite by primary key and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildEntite requireOne(ConnectionInterface $con = null) Return the first ChildEntite matching the query and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 *
 * @method     ChildEntite requireOneByIdEntite(string $id_entite) Return the first ChildEntite filtered by the id_entite column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildEntite requireOneByNom(string $nom) Return the first ChildEntite filtered by the nom column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildEntite requireOneByNomcourt(string $nomcourt) Return the first ChildEntite filtered by the nomcourt column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildEntite requireOneByAdresse(string $adresse) Return the first ChildEntite filtered by the adresse column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildEntite requireOneByCodepostal(string $codepostal) Return the first ChildEntite filtered by the codepostal column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildEntite requireOneByVille(string $ville) Return the first ChildEntite filtered by the ville column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildEntite requireOneByPays(string $pays) Return the first ChildEntite filtered by the pays column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildEntite requireOneByTelephone(string $telephone) Return the first ChildEntite filtered by the telephone column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildEntite requireOneByFax(string $fax) Return the first ChildEntite filtered by the fax column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildEntite requireOneByUrl(string $url) Return the first ChildEntite filtered by the url column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildEntite requireOneByAssujettitva(int $assujettitva) Return the first ChildEntite filtered by the assujettitva column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildEntite requireOneByEmail(string $email) Return the first ChildEntite filtered by the email column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildEntite requireOneByCreatedAt(string $created_at) Return the first ChildEntite filtered by the created_at column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildEntite requireOneByUpdatedAt(string $updated_at) Return the first ChildEntite filtered by the updated_at column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 *
 * @method     ChildEntite[]|ObjectCollection find(ConnectionInterface $con = null) Return ChildEntite objects based on current ModelCriteria
 * @method     ChildEntite[]|ObjectCollection findByIdEntite(string $id_entite) Return ChildEntite objects filtered by the id_entite column
 * @method     ChildEntite[]|ObjectCollection findByNom(string $nom) Return ChildEntite objects filtered by the nom column
 * @method     ChildEntite[]|ObjectCollection findByNomcourt(string $nomcourt) Return ChildEntite objects filtered by the nomcourt column
 * @method     ChildEntite[]|ObjectCollection findByAdresse(string $adresse) Return ChildEntite objects filtered by the adresse column
 * @method     ChildEntite[]|ObjectCollection findByCodepostal(string $codepostal) Return ChildEntite objects filtered by the codepostal column
 * @method     ChildEntite[]|ObjectCollection findByVille(string $ville) Return ChildEntite objects filtered by the ville column
 * @method     ChildEntite[]|ObjectCollection findByPays(string $pays) Return ChildEntite objects filtered by the pays column
 * @method     ChildEntite[]|ObjectCollection findByTelephone(string $telephone) Return ChildEntite objects filtered by the telephone column
 * @method     ChildEntite[]|ObjectCollection findByFax(string $fax) Return ChildEntite objects filtered by the fax column
 * @method     ChildEntite[]|ObjectCollection findByUrl(string $url) Return ChildEntite objects filtered by the url column
 * @method     ChildEntite[]|ObjectCollection findByAssujettitva(int $assujettitva) Return ChildEntite objects filtered by the assujettitva column
 * @method     ChildEntite[]|ObjectCollection findByEmail(string $email) Return ChildEntite objects filtered by the email column
 * @method     ChildEntite[]|ObjectCollection findByCreatedAt(string $created_at) Return ChildEntite objects filtered by the created_at column
 * @method     ChildEntite[]|ObjectCollection findByUpdatedAt(string $updated_at) Return ChildEntite objects filtered by the updated_at column
 * @method     ChildEntite[]|\Propel\Runtime\Util\PropelModelPager paginate($page = 1, $maxPerPage = 10, ConnectionInterface $con = null) Issue a SELECT query based on the current ModelCriteria and uses a page and a maximum number of results per page to compute an offset and a limit
 *
 */
abstract class EntiteQuery extends ModelCriteria
{

    // archivable behavior
    protected $archiveOnDelete = true;
protected $entityNotFoundExceptionClass = '\\Propel\\Runtime\\Exception\\EntityNotFoundException';

    /**
     * Initializes internal state of \Base\EntiteQuery object.
     *
     * @param     string $dbName The database name
     * @param     string $modelName The phpName of a model, e.g. 'Book'
     * @param     string $modelAlias The alias for the model in this query, e.g. 'b'
     */
    public function __construct($dbName = 'simplasso', $modelName = '\\Entite', $modelAlias = null)
    {
        parent::__construct($dbName, $modelName, $modelAlias);
    }

    /**
     * Returns a new ChildEntiteQuery object.
     *
     * @param     string $modelAlias The alias of a model in the query
     * @param     Criteria $criteria Optional Criteria to build the query from
     *
     * @return ChildEntiteQuery
     */
    public static function create($modelAlias = null, Criteria $criteria = null)
    {
        if ($criteria instanceof ChildEntiteQuery) {
            return $criteria;
        }
        $query = new ChildEntiteQuery();
        if (null !== $modelAlias) {
            $query->setModelAlias($modelAlias);
        }
        if ($criteria instanceof Criteria) {
            $query->mergeWith($criteria);
        }

        return $query;
    }

    /**
     * Find object by primary key.
     * Propel uses the instance pool to skip the database if the object exists.
     * Go fast if the query is untouched.
     *
     * <code>
     * $obj  = $c->findPk(12, $con);
     * </code>
     *
     * @param mixed $key Primary key to use for the query
     * @param ConnectionInterface $con an optional connection object
     *
     * @return ChildEntite|array|mixed the result, formatted by the current formatter
     */
    public function findPk($key, ConnectionInterface $con = null)
    {
        if ($key === null) {
            return null;
        }

        if ($con === null) {
            $con = Propel::getServiceContainer()->getReadConnection(EntiteTableMap::DATABASE_NAME);
        }

        $this->basePreSelect($con);

        if (
            $this->formatter || $this->modelAlias || $this->with || $this->select
            || $this->selectColumns || $this->asColumns || $this->selectModifiers
            || $this->map || $this->having || $this->joins
        ) {
            return $this->findPkComplex($key, $con);
        }

        if ((null !== ($obj = EntiteTableMap::getInstanceFromPool(null === $key || is_scalar($key) || is_callable([$key, '__toString']) ? (string) $key : $key)))) {
            // the object is already in the instance pool
            return $obj;
        }

        return $this->findPkSimple($key, $con);
    }

    /**
     * Find object by primary key using raw SQL to go fast.
     * Bypass doSelect() and the object formatter by using generated code.
     *
     * @param     mixed $key Primary key to use for the query
     * @param     ConnectionInterface $con A connection object
     *
     * @throws \Propel\Runtime\Exception\PropelException
     *
     * @return ChildEntite A model object, or null if the key is not found
     */
    protected function findPkSimple($key, ConnectionInterface $con)
    {
        $sql = 'SELECT `id_entite`, `nom`, `nomcourt`, `adresse`, `codepostal`, `ville`, `pays`, `telephone`, `fax`, `url`, `assujettitva`, `email`, `created_at`, `updated_at` FROM `asso_entites` WHERE `id_entite` = :p0';
        try {
            $stmt = $con->prepare($sql);
            $stmt->bindValue(':p0', $key, PDO::PARAM_INT);
            $stmt->execute();
        } catch (Exception $e) {
            Propel::log($e->getMessage(), Propel::LOG_ERR);
            throw new PropelException(sprintf('Unable to execute SELECT statement [%s]', $sql), 0, $e);
        }
        $obj = null;
        if ($row = $stmt->fetch(\PDO::FETCH_NUM)) {
            /** @var ChildEntite $obj */
            $obj = new ChildEntite();
            $obj->hydrate($row);
            EntiteTableMap::addInstanceToPool($obj, null === $key || is_scalar($key) || is_callable([$key, '__toString']) ? (string) $key : $key);
        }
        $stmt->closeCursor();

        return $obj;
    }

    /**
     * Find object by primary key.
     *
     * @param     mixed $key Primary key to use for the query
     * @param     ConnectionInterface $con A connection object
     *
     * @return ChildEntite|array|mixed the result, formatted by the current formatter
     */
    protected function findPkComplex($key, ConnectionInterface $con)
    {
        // As the query uses a PK condition, no limit(1) is necessary.
        $criteria = $this->isKeepQuery() ? clone $this : $this;
        $dataFetcher = $criteria
            ->filterByPrimaryKey($key)
            ->doSelect($con);

        return $criteria->getFormatter()->init($criteria)->formatOne($dataFetcher);
    }

    /**
     * Find objects by primary key
     * <code>
     * $objs = $c->findPks(array(12, 56, 832), $con);
     * </code>
     * @param     array $keys Primary keys to use for the query
     * @param     ConnectionInterface $con an optional connection object
     *
     * @return ObjectCollection|array|mixed the list of results, formatted by the current formatter
     */
    public function findPks($keys, ConnectionInterface $con = null)
    {
        if (null === $con) {
            $con = Propel::getServiceContainer()->getReadConnection($this->getDbName());
        }
        $this->basePreSelect($con);
        $criteria = $this->isKeepQuery() ? clone $this : $this;
        $dataFetcher = $criteria
            ->filterByPrimaryKeys($keys)
            ->doSelect($con);

        return $criteria->getFormatter()->init($criteria)->format($dataFetcher);
    }

    /**
     * Filter the query by primary key
     *
     * @param     mixed $key Primary key to use for the query
     *
     * @return $this|ChildEntiteQuery The current query, for fluid interface
     */
    public function filterByPrimaryKey($key)
    {

        return $this->addUsingAlias(EntiteTableMap::COL_ID_ENTITE, $key, Criteria::EQUAL);
    }

    /**
     * Filter the query by a list of primary keys
     *
     * @param     array $keys The list of primary key to use for the query
     *
     * @return $this|ChildEntiteQuery The current query, for fluid interface
     */
    public function filterByPrimaryKeys($keys)
    {

        return $this->addUsingAlias(EntiteTableMap::COL_ID_ENTITE, $keys, Criteria::IN);
    }

    /**
     * Filter the query on the id_entite column
     *
     * Example usage:
     * <code>
     * $query->filterByIdEntite(1234); // WHERE id_entite = 1234
     * $query->filterByIdEntite(array(12, 34)); // WHERE id_entite IN (12, 34)
     * $query->filterByIdEntite(array('min' => 12)); // WHERE id_entite > 12
     * </code>
     *
     * @param     mixed $idEntite The value to use as filter.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildEntiteQuery The current query, for fluid interface
     */
    public function filterByIdEntite($idEntite = null, $comparison = null)
    {
        if (is_array($idEntite)) {
            $useMinMax = false;
            if (isset($idEntite['min'])) {
                $this->addUsingAlias(EntiteTableMap::COL_ID_ENTITE, $idEntite['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($idEntite['max'])) {
                $this->addUsingAlias(EntiteTableMap::COL_ID_ENTITE, $idEntite['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(EntiteTableMap::COL_ID_ENTITE, $idEntite, $comparison);
    }

    /**
     * Filter the query on the nom column
     *
     * Example usage:
     * <code>
     * $query->filterByNom('fooValue');   // WHERE nom = 'fooValue'
     * $query->filterByNom('%fooValue%', Criteria::LIKE); // WHERE nom LIKE '%fooValue%'
     * </code>
     *
     * @param     string $nom The value to use as filter.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildEntiteQuery The current query, for fluid interface
     */
    public function filterByNom($nom = null, $comparison = null)
    {
        if (null === $comparison) {
            if (is_array($nom)) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(EntiteTableMap::COL_NOM, $nom, $comparison);
    }

    /**
     * Filter the query on the nomcourt column
     *
     * Example usage:
     * <code>
     * $query->filterByNomcourt('fooValue');   // WHERE nomcourt = 'fooValue'
     * $query->filterByNomcourt('%fooValue%', Criteria::LIKE); // WHERE nomcourt LIKE '%fooValue%'
     * </code>
     *
     * @param     string $nomcourt The value to use as filter.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildEntiteQuery The current query, for fluid interface
     */
    public function filterByNomcourt($nomcourt = null, $comparison = null)
    {
        if (null === $comparison) {
            if (is_array($nomcourt)) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(EntiteTableMap::COL_NOMCOURT, $nomcourt, $comparison);
    }

    /**
     * Filter the query on the adresse column
     *
     * Example usage:
     * <code>
     * $query->filterByAdresse('fooValue');   // WHERE adresse = 'fooValue'
     * $query->filterByAdresse('%fooValue%', Criteria::LIKE); // WHERE adresse LIKE '%fooValue%'
     * </code>
     *
     * @param     string $adresse The value to use as filter.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildEntiteQuery The current query, for fluid interface
     */
    public function filterByAdresse($adresse = null, $comparison = null)
    {
        if (null === $comparison) {
            if (is_array($adresse)) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(EntiteTableMap::COL_ADRESSE, $adresse, $comparison);
    }

    /**
     * Filter the query on the codepostal column
     *
     * Example usage:
     * <code>
     * $query->filterByCodepostal('fooValue');   // WHERE codepostal = 'fooValue'
     * $query->filterByCodepostal('%fooValue%', Criteria::LIKE); // WHERE codepostal LIKE '%fooValue%'
     * </code>
     *
     * @param     string $codepostal The value to use as filter.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildEntiteQuery The current query, for fluid interface
     */
    public function filterByCodepostal($codepostal = null, $comparison = null)
    {
        if (null === $comparison) {
            if (is_array($codepostal)) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(EntiteTableMap::COL_CODEPOSTAL, $codepostal, $comparison);
    }

    /**
     * Filter the query on the ville column
     *
     * Example usage:
     * <code>
     * $query->filterByVille('fooValue');   // WHERE ville = 'fooValue'
     * $query->filterByVille('%fooValue%', Criteria::LIKE); // WHERE ville LIKE '%fooValue%'
     * </code>
     *
     * @param     string $ville The value to use as filter.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildEntiteQuery The current query, for fluid interface
     */
    public function filterByVille($ville = null, $comparison = null)
    {
        if (null === $comparison) {
            if (is_array($ville)) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(EntiteTableMap::COL_VILLE, $ville, $comparison);
    }

    /**
     * Filter the query on the pays column
     *
     * Example usage:
     * <code>
     * $query->filterByPays('fooValue');   // WHERE pays = 'fooValue'
     * $query->filterByPays('%fooValue%', Criteria::LIKE); // WHERE pays LIKE '%fooValue%'
     * </code>
     *
     * @param     string $pays The value to use as filter.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildEntiteQuery The current query, for fluid interface
     */
    public function filterByPays($pays = null, $comparison = null)
    {
        if (null === $comparison) {
            if (is_array($pays)) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(EntiteTableMap::COL_PAYS, $pays, $comparison);
    }

    /**
     * Filter the query on the telephone column
     *
     * Example usage:
     * <code>
     * $query->filterByTelephone('fooValue');   // WHERE telephone = 'fooValue'
     * $query->filterByTelephone('%fooValue%', Criteria::LIKE); // WHERE telephone LIKE '%fooValue%'
     * </code>
     *
     * @param     string $telephone The value to use as filter.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildEntiteQuery The current query, for fluid interface
     */
    public function filterByTelephone($telephone = null, $comparison = null)
    {
        if (null === $comparison) {
            if (is_array($telephone)) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(EntiteTableMap::COL_TELEPHONE, $telephone, $comparison);
    }

    /**
     * Filter the query on the fax column
     *
     * Example usage:
     * <code>
     * $query->filterByFax('fooValue');   // WHERE fax = 'fooValue'
     * $query->filterByFax('%fooValue%', Criteria::LIKE); // WHERE fax LIKE '%fooValue%'
     * </code>
     *
     * @param     string $fax The value to use as filter.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildEntiteQuery The current query, for fluid interface
     */
    public function filterByFax($fax = null, $comparison = null)
    {
        if (null === $comparison) {
            if (is_array($fax)) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(EntiteTableMap::COL_FAX, $fax, $comparison);
    }

    /**
     * Filter the query on the url column
     *
     * Example usage:
     * <code>
     * $query->filterByUrl('fooValue');   // WHERE url = 'fooValue'
     * $query->filterByUrl('%fooValue%', Criteria::LIKE); // WHERE url LIKE '%fooValue%'
     * </code>
     *
     * @param     string $url The value to use as filter.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildEntiteQuery The current query, for fluid interface
     */
    public function filterByUrl($url = null, $comparison = null)
    {
        if (null === $comparison) {
            if (is_array($url)) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(EntiteTableMap::COL_URL, $url, $comparison);
    }

    /**
     * Filter the query on the assujettitva column
     *
     * Example usage:
     * <code>
     * $query->filterByAssujettitva(1234); // WHERE assujettitva = 1234
     * $query->filterByAssujettitva(array(12, 34)); // WHERE assujettitva IN (12, 34)
     * $query->filterByAssujettitva(array('min' => 12)); // WHERE assujettitva > 12
     * </code>
     *
     * @param     mixed $assujettitva The value to use as filter.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildEntiteQuery The current query, for fluid interface
     */
    public function filterByAssujettitva($assujettitva = null, $comparison = null)
    {
        if (is_array($assujettitva)) {
            $useMinMax = false;
            if (isset($assujettitva['min'])) {
                $this->addUsingAlias(EntiteTableMap::COL_ASSUJETTITVA, $assujettitva['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($assujettitva['max'])) {
                $this->addUsingAlias(EntiteTableMap::COL_ASSUJETTITVA, $assujettitva['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(EntiteTableMap::COL_ASSUJETTITVA, $assujettitva, $comparison);
    }

    /**
     * Filter the query on the email column
     *
     * Example usage:
     * <code>
     * $query->filterByEmail('fooValue');   // WHERE email = 'fooValue'
     * $query->filterByEmail('%fooValue%', Criteria::LIKE); // WHERE email LIKE '%fooValue%'
     * </code>
     *
     * @param     string $email The value to use as filter.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildEntiteQuery The current query, for fluid interface
     */
    public function filterByEmail($email = null, $comparison = null)
    {
        if (null === $comparison) {
            if (is_array($email)) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(EntiteTableMap::COL_EMAIL, $email, $comparison);
    }

    /**
     * Filter the query on the created_at column
     *
     * Example usage:
     * <code>
     * $query->filterByCreatedAt('2011-03-14'); // WHERE created_at = '2011-03-14'
     * $query->filterByCreatedAt('now'); // WHERE created_at = '2011-03-14'
     * $query->filterByCreatedAt(array('max' => 'yesterday')); // WHERE created_at > '2011-03-13'
     * </code>
     *
     * @param     mixed $createdAt The value to use as filter.
     *              Values can be integers (unix timestamps), DateTime objects, or strings.
     *              Empty strings are treated as NULL.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildEntiteQuery The current query, for fluid interface
     */
    public function filterByCreatedAt($createdAt = null, $comparison = null)
    {
        if (is_array($createdAt)) {
            $useMinMax = false;
            if (isset($createdAt['min'])) {
                $this->addUsingAlias(EntiteTableMap::COL_CREATED_AT, $createdAt['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($createdAt['max'])) {
                $this->addUsingAlias(EntiteTableMap::COL_CREATED_AT, $createdAt['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(EntiteTableMap::COL_CREATED_AT, $createdAt, $comparison);
    }

    /**
     * Filter the query on the updated_at column
     *
     * Example usage:
     * <code>
     * $query->filterByUpdatedAt('2011-03-14'); // WHERE updated_at = '2011-03-14'
     * $query->filterByUpdatedAt('now'); // WHERE updated_at = '2011-03-14'
     * $query->filterByUpdatedAt(array('max' => 'yesterday')); // WHERE updated_at > '2011-03-13'
     * </code>
     *
     * @param     mixed $updatedAt The value to use as filter.
     *              Values can be integers (unix timestamps), DateTime objects, or strings.
     *              Empty strings are treated as NULL.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildEntiteQuery The current query, for fluid interface
     */
    public function filterByUpdatedAt($updatedAt = null, $comparison = null)
    {
        if (is_array($updatedAt)) {
            $useMinMax = false;
            if (isset($updatedAt['min'])) {
                $this->addUsingAlias(EntiteTableMap::COL_UPDATED_AT, $updatedAt['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($updatedAt['max'])) {
                $this->addUsingAlias(EntiteTableMap::COL_UPDATED_AT, $updatedAt['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(EntiteTableMap::COL_UPDATED_AT, $updatedAt, $comparison);
    }

    /**
     * Filter the query by a related \Activite object
     *
     * @param \Activite|ObjectCollection $activite the related object to use as filter
     * @param string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return ChildEntiteQuery The current query, for fluid interface
     */
    public function filterByActivite($activite, $comparison = null)
    {
        if ($activite instanceof \Activite) {
            return $this
                ->addUsingAlias(EntiteTableMap::COL_ID_ENTITE, $activite->getIdEntite(), $comparison);
        } elseif ($activite instanceof ObjectCollection) {
            return $this
                ->useActiviteQuery()
                ->filterByPrimaryKeys($activite->getPrimaryKeys())
                ->endUse();
        } else {
            throw new PropelException('filterByActivite() only accepts arguments of type \Activite or Collection');
        }
    }

    /**
     * Adds a JOIN clause to the query using the Activite relation
     *
     * @param     string $relationAlias optional alias for the relation
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return $this|ChildEntiteQuery The current query, for fluid interface
     */
    public function joinActivite($relationAlias = null, $joinType = Criteria::INNER_JOIN)
    {
        $tableMap = $this->getTableMap();
        $relationMap = $tableMap->getRelation('Activite');

        // create a ModelJoin object for this join
        $join = new ModelJoin();
        $join->setJoinType($joinType);
        $join->setRelationMap($relationMap, $this->useAliasInSQL ? $this->getModelAlias() : null, $relationAlias);
        if ($previousJoin = $this->getPreviousJoin()) {
            $join->setPreviousJoin($previousJoin);
        }

        // add the ModelJoin to the current object
        if ($relationAlias) {
            $this->addAlias($relationAlias, $relationMap->getRightTable()->getName());
            $this->addJoinObject($join, $relationAlias);
        } else {
            $this->addJoinObject($join, 'Activite');
        }

        return $this;
    }

    /**
     * Use the Activite relation Activite object
     *
     * @see useQuery()
     *
     * @param     string $relationAlias optional alias for the relation,
     *                                   to be used as main alias in the secondary query
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return \ActiviteQuery A secondary query class using the current class as primary query
     */
    public function useActiviteQuery($relationAlias = null, $joinType = Criteria::INNER_JOIN)
    {
        return $this
            ->joinActivite($relationAlias, $joinType)
            ->useQuery($relationAlias ? $relationAlias : 'Activite', '\ActiviteQuery');
    }

    /**
     * Filter the query by a related \Compte object
     *
     * @param \Compte|ObjectCollection $compte the related object to use as filter
     * @param string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return ChildEntiteQuery The current query, for fluid interface
     */
    public function filterByCompte($compte, $comparison = null)
    {
        if ($compte instanceof \Compte) {
            return $this
                ->addUsingAlias(EntiteTableMap::COL_ID_ENTITE, $compte->getIdEntite(), $comparison);
        } elseif ($compte instanceof ObjectCollection) {
            return $this
                ->useCompteQuery()
                ->filterByPrimaryKeys($compte->getPrimaryKeys())
                ->endUse();
        } else {
            throw new PropelException('filterByCompte() only accepts arguments of type \Compte or Collection');
        }
    }

    /**
     * Adds a JOIN clause to the query using the Compte relation
     *
     * @param     string $relationAlias optional alias for the relation
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return $this|ChildEntiteQuery The current query, for fluid interface
     */
    public function joinCompte($relationAlias = null, $joinType = Criteria::INNER_JOIN)
    {
        $tableMap = $this->getTableMap();
        $relationMap = $tableMap->getRelation('Compte');

        // create a ModelJoin object for this join
        $join = new ModelJoin();
        $join->setJoinType($joinType);
        $join->setRelationMap($relationMap, $this->useAliasInSQL ? $this->getModelAlias() : null, $relationAlias);
        if ($previousJoin = $this->getPreviousJoin()) {
            $join->setPreviousJoin($previousJoin);
        }

        // add the ModelJoin to the current object
        if ($relationAlias) {
            $this->addAlias($relationAlias, $relationMap->getRightTable()->getName());
            $this->addJoinObject($join, $relationAlias);
        } else {
            $this->addJoinObject($join, 'Compte');
        }

        return $this;
    }

    /**
     * Use the Compte relation Compte object
     *
     * @see useQuery()
     *
     * @param     string $relationAlias optional alias for the relation,
     *                                   to be used as main alias in the secondary query
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return \CompteQuery A secondary query class using the current class as primary query
     */
    public function useCompteQuery($relationAlias = null, $joinType = Criteria::INNER_JOIN)
    {
        return $this
            ->joinCompte($relationAlias, $joinType)
            ->useQuery($relationAlias ? $relationAlias : 'Compte', '\CompteQuery');
    }

    /**
     * Filter the query by a related \Ecriture object
     *
     * @param \Ecriture|ObjectCollection $ecriture the related object to use as filter
     * @param string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return ChildEntiteQuery The current query, for fluid interface
     */
    public function filterByEcriture($ecriture, $comparison = null)
    {
        if ($ecriture instanceof \Ecriture) {
            return $this
                ->addUsingAlias(EntiteTableMap::COL_ID_ENTITE, $ecriture->getIdEntite(), $comparison);
        } elseif ($ecriture instanceof ObjectCollection) {
            return $this
                ->useEcritureQuery()
                ->filterByPrimaryKeys($ecriture->getPrimaryKeys())
                ->endUse();
        } else {
            throw new PropelException('filterByEcriture() only accepts arguments of type \Ecriture or Collection');
        }
    }

    /**
     * Adds a JOIN clause to the query using the Ecriture relation
     *
     * @param     string $relationAlias optional alias for the relation
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return $this|ChildEntiteQuery The current query, for fluid interface
     */
    public function joinEcriture($relationAlias = null, $joinType = Criteria::INNER_JOIN)
    {
        $tableMap = $this->getTableMap();
        $relationMap = $tableMap->getRelation('Ecriture');

        // create a ModelJoin object for this join
        $join = new ModelJoin();
        $join->setJoinType($joinType);
        $join->setRelationMap($relationMap, $this->useAliasInSQL ? $this->getModelAlias() : null, $relationAlias);
        if ($previousJoin = $this->getPreviousJoin()) {
            $join->setPreviousJoin($previousJoin);
        }

        // add the ModelJoin to the current object
        if ($relationAlias) {
            $this->addAlias($relationAlias, $relationMap->getRightTable()->getName());
            $this->addJoinObject($join, $relationAlias);
        } else {
            $this->addJoinObject($join, 'Ecriture');
        }

        return $this;
    }

    /**
     * Use the Ecriture relation Ecriture object
     *
     * @see useQuery()
     *
     * @param     string $relationAlias optional alias for the relation,
     *                                   to be used as main alias in the secondary query
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return \EcritureQuery A secondary query class using the current class as primary query
     */
    public function useEcritureQuery($relationAlias = null, $joinType = Criteria::INNER_JOIN)
    {
        return $this
            ->joinEcriture($relationAlias, $joinType)
            ->useQuery($relationAlias ? $relationAlias : 'Ecriture', '\EcritureQuery');
    }

    /**
     * Filter the query by a related \Journal object
     *
     * @param \Journal|ObjectCollection $journal the related object to use as filter
     * @param string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return ChildEntiteQuery The current query, for fluid interface
     */
    public function filterByJournal($journal, $comparison = null)
    {
        if ($journal instanceof \Journal) {
            return $this
                ->addUsingAlias(EntiteTableMap::COL_ID_ENTITE, $journal->getIdEntite(), $comparison);
        } elseif ($journal instanceof ObjectCollection) {
            return $this
                ->useJournalQuery()
                ->filterByPrimaryKeys($journal->getPrimaryKeys())
                ->endUse();
        } else {
            throw new PropelException('filterByJournal() only accepts arguments of type \Journal or Collection');
        }
    }

    /**
     * Adds a JOIN clause to the query using the Journal relation
     *
     * @param     string $relationAlias optional alias for the relation
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return $this|ChildEntiteQuery The current query, for fluid interface
     */
    public function joinJournal($relationAlias = null, $joinType = Criteria::INNER_JOIN)
    {
        $tableMap = $this->getTableMap();
        $relationMap = $tableMap->getRelation('Journal');

        // create a ModelJoin object for this join
        $join = new ModelJoin();
        $join->setJoinType($joinType);
        $join->setRelationMap($relationMap, $this->useAliasInSQL ? $this->getModelAlias() : null, $relationAlias);
        if ($previousJoin = $this->getPreviousJoin()) {
            $join->setPreviousJoin($previousJoin);
        }

        // add the ModelJoin to the current object
        if ($relationAlias) {
            $this->addAlias($relationAlias, $relationMap->getRightTable()->getName());
            $this->addJoinObject($join, $relationAlias);
        } else {
            $this->addJoinObject($join, 'Journal');
        }

        return $this;
    }

    /**
     * Use the Journal relation Journal object
     *
     * @see useQuery()
     *
     * @param     string $relationAlias optional alias for the relation,
     *                                   to be used as main alias in the secondary query
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return \JournalQuery A secondary query class using the current class as primary query
     */
    public function useJournalQuery($relationAlias = null, $joinType = Criteria::INNER_JOIN)
    {
        return $this
            ->joinJournal($relationAlias, $joinType)
            ->useQuery($relationAlias ? $relationAlias : 'Journal', '\JournalQuery');
    }

    /**
     * Filter the query by a related \Piece object
     *
     * @param \Piece|ObjectCollection $piece the related object to use as filter
     * @param string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return ChildEntiteQuery The current query, for fluid interface
     */
    public function filterByPiece($piece, $comparison = null)
    {
        if ($piece instanceof \Piece) {
            return $this
                ->addUsingAlias(EntiteTableMap::COL_ID_ENTITE, $piece->getIdEntite(), $comparison);
        } elseif ($piece instanceof ObjectCollection) {
            return $this
                ->usePieceQuery()
                ->filterByPrimaryKeys($piece->getPrimaryKeys())
                ->endUse();
        } else {
            throw new PropelException('filterByPiece() only accepts arguments of type \Piece or Collection');
        }
    }

    /**
     * Adds a JOIN clause to the query using the Piece relation
     *
     * @param     string $relationAlias optional alias for the relation
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return $this|ChildEntiteQuery The current query, for fluid interface
     */
    public function joinPiece($relationAlias = null, $joinType = Criteria::INNER_JOIN)
    {
        $tableMap = $this->getTableMap();
        $relationMap = $tableMap->getRelation('Piece');

        // create a ModelJoin object for this join
        $join = new ModelJoin();
        $join->setJoinType($joinType);
        $join->setRelationMap($relationMap, $this->useAliasInSQL ? $this->getModelAlias() : null, $relationAlias);
        if ($previousJoin = $this->getPreviousJoin()) {
            $join->setPreviousJoin($previousJoin);
        }

        // add the ModelJoin to the current object
        if ($relationAlias) {
            $this->addAlias($relationAlias, $relationMap->getRightTable()->getName());
            $this->addJoinObject($join, $relationAlias);
        } else {
            $this->addJoinObject($join, 'Piece');
        }

        return $this;
    }

    /**
     * Use the Piece relation Piece object
     *
     * @see useQuery()
     *
     * @param     string $relationAlias optional alias for the relation,
     *                                   to be used as main alias in the secondary query
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return \PieceQuery A secondary query class using the current class as primary query
     */
    public function usePieceQuery($relationAlias = null, $joinType = Criteria::INNER_JOIN)
    {
        return $this
            ->joinPiece($relationAlias, $joinType)
            ->useQuery($relationAlias ? $relationAlias : 'Piece', '\PieceQuery');
    }

    /**
     * Filter the query by a related \Autorisation object
     *
     * @param \Autorisation|ObjectCollection $autorisation the related object to use as filter
     * @param string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return ChildEntiteQuery The current query, for fluid interface
     */
    public function filterByAutorisation($autorisation, $comparison = null)
    {
        if ($autorisation instanceof \Autorisation) {
            return $this
                ->addUsingAlias(EntiteTableMap::COL_ID_ENTITE, $autorisation->getIdEntite(), $comparison);
        } elseif ($autorisation instanceof ObjectCollection) {
            return $this
                ->useAutorisationQuery()
                ->filterByPrimaryKeys($autorisation->getPrimaryKeys())
                ->endUse();
        } else {
            throw new PropelException('filterByAutorisation() only accepts arguments of type \Autorisation or Collection');
        }
    }

    /**
     * Adds a JOIN clause to the query using the Autorisation relation
     *
     * @param     string $relationAlias optional alias for the relation
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return $this|ChildEntiteQuery The current query, for fluid interface
     */
    public function joinAutorisation($relationAlias = null, $joinType = Criteria::INNER_JOIN)
    {
        $tableMap = $this->getTableMap();
        $relationMap = $tableMap->getRelation('Autorisation');

        // create a ModelJoin object for this join
        $join = new ModelJoin();
        $join->setJoinType($joinType);
        $join->setRelationMap($relationMap, $this->useAliasInSQL ? $this->getModelAlias() : null, $relationAlias);
        if ($previousJoin = $this->getPreviousJoin()) {
            $join->setPreviousJoin($previousJoin);
        }

        // add the ModelJoin to the current object
        if ($relationAlias) {
            $this->addAlias($relationAlias, $relationMap->getRightTable()->getName());
            $this->addJoinObject($join, $relationAlias);
        } else {
            $this->addJoinObject($join, 'Autorisation');
        }

        return $this;
    }

    /**
     * Use the Autorisation relation Autorisation object
     *
     * @see useQuery()
     *
     * @param     string $relationAlias optional alias for the relation,
     *                                   to be used as main alias in the secondary query
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return \AutorisationQuery A secondary query class using the current class as primary query
     */
    public function useAutorisationQuery($relationAlias = null, $joinType = Criteria::INNER_JOIN)
    {
        return $this
            ->joinAutorisation($relationAlias, $joinType)
            ->useQuery($relationAlias ? $relationAlias : 'Autorisation', '\AutorisationQuery');
    }

    /**
     * Filter the query by a related \Paiement object
     *
     * @param \Paiement|ObjectCollection $paiement the related object to use as filter
     * @param string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return ChildEntiteQuery The current query, for fluid interface
     */
    public function filterByPaiement($paiement, $comparison = null)
    {
        if ($paiement instanceof \Paiement) {
            return $this
                ->addUsingAlias(EntiteTableMap::COL_ID_ENTITE, $paiement->getIdEntite(), $comparison);
        } elseif ($paiement instanceof ObjectCollection) {
            return $this
                ->usePaiementQuery()
                ->filterByPrimaryKeys($paiement->getPrimaryKeys())
                ->endUse();
        } else {
            throw new PropelException('filterByPaiement() only accepts arguments of type \Paiement or Collection');
        }
    }

    /**
     * Adds a JOIN clause to the query using the Paiement relation
     *
     * @param     string $relationAlias optional alias for the relation
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return $this|ChildEntiteQuery The current query, for fluid interface
     */
    public function joinPaiement($relationAlias = null, $joinType = Criteria::INNER_JOIN)
    {
        $tableMap = $this->getTableMap();
        $relationMap = $tableMap->getRelation('Paiement');

        // create a ModelJoin object for this join
        $join = new ModelJoin();
        $join->setJoinType($joinType);
        $join->setRelationMap($relationMap, $this->useAliasInSQL ? $this->getModelAlias() : null, $relationAlias);
        if ($previousJoin = $this->getPreviousJoin()) {
            $join->setPreviousJoin($previousJoin);
        }

        // add the ModelJoin to the current object
        if ($relationAlias) {
            $this->addAlias($relationAlias, $relationMap->getRightTable()->getName());
            $this->addJoinObject($join, $relationAlias);
        } else {
            $this->addJoinObject($join, 'Paiement');
        }

        return $this;
    }

    /**
     * Use the Paiement relation Paiement object
     *
     * @see useQuery()
     *
     * @param     string $relationAlias optional alias for the relation,
     *                                   to be used as main alias in the secondary query
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return \PaiementQuery A secondary query class using the current class as primary query
     */
    public function usePaiementQuery($relationAlias = null, $joinType = Criteria::INNER_JOIN)
    {
        return $this
            ->joinPaiement($relationAlias, $joinType)
            ->useQuery($relationAlias ? $relationAlias : 'Paiement', '\PaiementQuery');
    }

    /**
     * Filter the query by a related \Prestation object
     *
     * @param \Prestation|ObjectCollection $prestation the related object to use as filter
     * @param string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return ChildEntiteQuery The current query, for fluid interface
     */
    public function filterByPrestation($prestation, $comparison = null)
    {
        if ($prestation instanceof \Prestation) {
            return $this
                ->addUsingAlias(EntiteTableMap::COL_ID_ENTITE, $prestation->getIdEntite(), $comparison);
        } elseif ($prestation instanceof ObjectCollection) {
            return $this
                ->usePrestationQuery()
                ->filterByPrimaryKeys($prestation->getPrimaryKeys())
                ->endUse();
        } else {
            throw new PropelException('filterByPrestation() only accepts arguments of type \Prestation or Collection');
        }
    }

    /**
     * Adds a JOIN clause to the query using the Prestation relation
     *
     * @param     string $relationAlias optional alias for the relation
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return $this|ChildEntiteQuery The current query, for fluid interface
     */
    public function joinPrestation($relationAlias = null, $joinType = Criteria::INNER_JOIN)
    {
        $tableMap = $this->getTableMap();
        $relationMap = $tableMap->getRelation('Prestation');

        // create a ModelJoin object for this join
        $join = new ModelJoin();
        $join->setJoinType($joinType);
        $join->setRelationMap($relationMap, $this->useAliasInSQL ? $this->getModelAlias() : null, $relationAlias);
        if ($previousJoin = $this->getPreviousJoin()) {
            $join->setPreviousJoin($previousJoin);
        }

        // add the ModelJoin to the current object
        if ($relationAlias) {
            $this->addAlias($relationAlias, $relationMap->getRightTable()->getName());
            $this->addJoinObject($join, $relationAlias);
        } else {
            $this->addJoinObject($join, 'Prestation');
        }

        return $this;
    }

    /**
     * Use the Prestation relation Prestation object
     *
     * @see useQuery()
     *
     * @param     string $relationAlias optional alias for the relation,
     *                                   to be used as main alias in the secondary query
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return \PrestationQuery A secondary query class using the current class as primary query
     */
    public function usePrestationQuery($relationAlias = null, $joinType = Criteria::INNER_JOIN)
    {
        return $this
            ->joinPrestation($relationAlias, $joinType)
            ->useQuery($relationAlias ? $relationAlias : 'Prestation', '\PrestationQuery');
    }

    /**
     * Filter the query by a related \Prestationslot object
     *
     * @param \Prestationslot|ObjectCollection $prestationslot the related object to use as filter
     * @param string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return ChildEntiteQuery The current query, for fluid interface
     */
    public function filterByPrestationslot($prestationslot, $comparison = null)
    {
        if ($prestationslot instanceof \Prestationslot) {
            return $this
                ->addUsingAlias(EntiteTableMap::COL_ID_ENTITE, $prestationslot->getIdEntite(), $comparison);
        } elseif ($prestationslot instanceof ObjectCollection) {
            return $this
                ->usePrestationslotQuery()
                ->filterByPrimaryKeys($prestationslot->getPrimaryKeys())
                ->endUse();
        } else {
            throw new PropelException('filterByPrestationslot() only accepts arguments of type \Prestationslot or Collection');
        }
    }

    /**
     * Adds a JOIN clause to the query using the Prestationslot relation
     *
     * @param     string $relationAlias optional alias for the relation
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return $this|ChildEntiteQuery The current query, for fluid interface
     */
    public function joinPrestationslot($relationAlias = null, $joinType = Criteria::INNER_JOIN)
    {
        $tableMap = $this->getTableMap();
        $relationMap = $tableMap->getRelation('Prestationslot');

        // create a ModelJoin object for this join
        $join = new ModelJoin();
        $join->setJoinType($joinType);
        $join->setRelationMap($relationMap, $this->useAliasInSQL ? $this->getModelAlias() : null, $relationAlias);
        if ($previousJoin = $this->getPreviousJoin()) {
            $join->setPreviousJoin($previousJoin);
        }

        // add the ModelJoin to the current object
        if ($relationAlias) {
            $this->addAlias($relationAlias, $relationMap->getRightTable()->getName());
            $this->addJoinObject($join, $relationAlias);
        } else {
            $this->addJoinObject($join, 'Prestationslot');
        }

        return $this;
    }

    /**
     * Use the Prestationslot relation Prestationslot object
     *
     * @see useQuery()
     *
     * @param     string $relationAlias optional alias for the relation,
     *                                   to be used as main alias in the secondary query
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return \PrestationslotQuery A secondary query class using the current class as primary query
     */
    public function usePrestationslotQuery($relationAlias = null, $joinType = Criteria::INNER_JOIN)
    {
        return $this
            ->joinPrestationslot($relationAlias, $joinType)
            ->useQuery($relationAlias ? $relationAlias : 'Prestationslot', '\PrestationslotQuery');
    }

    /**
     * Filter the query by a related \Servicerendu object
     *
     * @param \Servicerendu|ObjectCollection $servicerendu the related object to use as filter
     * @param string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return ChildEntiteQuery The current query, for fluid interface
     */
    public function filterByServicerendu($servicerendu, $comparison = null)
    {
        if ($servicerendu instanceof \Servicerendu) {
            return $this
                ->addUsingAlias(EntiteTableMap::COL_ID_ENTITE, $servicerendu->getIdEntite(), $comparison);
        } elseif ($servicerendu instanceof ObjectCollection) {
            return $this
                ->useServicerenduQuery()
                ->filterByPrimaryKeys($servicerendu->getPrimaryKeys())
                ->endUse();
        } else {
            throw new PropelException('filterByServicerendu() only accepts arguments of type \Servicerendu or Collection');
        }
    }

    /**
     * Adds a JOIN clause to the query using the Servicerendu relation
     *
     * @param     string $relationAlias optional alias for the relation
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return $this|ChildEntiteQuery The current query, for fluid interface
     */
    public function joinServicerendu($relationAlias = null, $joinType = Criteria::INNER_JOIN)
    {
        $tableMap = $this->getTableMap();
        $relationMap = $tableMap->getRelation('Servicerendu');

        // create a ModelJoin object for this join
        $join = new ModelJoin();
        $join->setJoinType($joinType);
        $join->setRelationMap($relationMap, $this->useAliasInSQL ? $this->getModelAlias() : null, $relationAlias);
        if ($previousJoin = $this->getPreviousJoin()) {
            $join->setPreviousJoin($previousJoin);
        }

        // add the ModelJoin to the current object
        if ($relationAlias) {
            $this->addAlias($relationAlias, $relationMap->getRightTable()->getName());
            $this->addJoinObject($join, $relationAlias);
        } else {
            $this->addJoinObject($join, 'Servicerendu');
        }

        return $this;
    }

    /**
     * Use the Servicerendu relation Servicerendu object
     *
     * @see useQuery()
     *
     * @param     string $relationAlias optional alias for the relation,
     *                                   to be used as main alias in the secondary query
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return \ServicerenduQuery A secondary query class using the current class as primary query
     */
    public function useServicerenduQuery($relationAlias = null, $joinType = Criteria::INNER_JOIN)
    {
        return $this
            ->joinServicerendu($relationAlias, $joinType)
            ->useQuery($relationAlias ? $relationAlias : 'Servicerendu', '\ServicerenduQuery');
    }

    /**
     * Filter the query by a related \Tresor object
     *
     * @param \Tresor|ObjectCollection $tresor the related object to use as filter
     * @param string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return ChildEntiteQuery The current query, for fluid interface
     */
    public function filterByTresor($tresor, $comparison = null)
    {
        if ($tresor instanceof \Tresor) {
            return $this
                ->addUsingAlias(EntiteTableMap::COL_ID_ENTITE, $tresor->getIdEntite(), $comparison);
        } elseif ($tresor instanceof ObjectCollection) {
            return $this
                ->useTresorQuery()
                ->filterByPrimaryKeys($tresor->getPrimaryKeys())
                ->endUse();
        } else {
            throw new PropelException('filterByTresor() only accepts arguments of type \Tresor or Collection');
        }
    }

    /**
     * Adds a JOIN clause to the query using the Tresor relation
     *
     * @param     string $relationAlias optional alias for the relation
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return $this|ChildEntiteQuery The current query, for fluid interface
     */
    public function joinTresor($relationAlias = null, $joinType = Criteria::INNER_JOIN)
    {
        $tableMap = $this->getTableMap();
        $relationMap = $tableMap->getRelation('Tresor');

        // create a ModelJoin object for this join
        $join = new ModelJoin();
        $join->setJoinType($joinType);
        $join->setRelationMap($relationMap, $this->useAliasInSQL ? $this->getModelAlias() : null, $relationAlias);
        if ($previousJoin = $this->getPreviousJoin()) {
            $join->setPreviousJoin($previousJoin);
        }

        // add the ModelJoin to the current object
        if ($relationAlias) {
            $this->addAlias($relationAlias, $relationMap->getRightTable()->getName());
            $this->addJoinObject($join, $relationAlias);
        } else {
            $this->addJoinObject($join, 'Tresor');
        }

        return $this;
    }

    /**
     * Use the Tresor relation Tresor object
     *
     * @see useQuery()
     *
     * @param     string $relationAlias optional alias for the relation,
     *                                   to be used as main alias in the secondary query
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return \TresorQuery A secondary query class using the current class as primary query
     */
    public function useTresorQuery($relationAlias = null, $joinType = Criteria::INNER_JOIN)
    {
        return $this
            ->joinTresor($relationAlias, $joinType)
            ->useQuery($relationAlias ? $relationAlias : 'Tresor', '\TresorQuery');
    }

    /**
     * Filter the query by a related \Tva object
     *
     * @param \Tva|ObjectCollection $tva the related object to use as filter
     * @param string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return ChildEntiteQuery The current query, for fluid interface
     */
    public function filterByTva($tva, $comparison = null)
    {
        if ($tva instanceof \Tva) {
            return $this
                ->addUsingAlias(EntiteTableMap::COL_ID_ENTITE, $tva->getIdEntite(), $comparison);
        } elseif ($tva instanceof ObjectCollection) {
            return $this
                ->useTvaQuery()
                ->filterByPrimaryKeys($tva->getPrimaryKeys())
                ->endUse();
        } else {
            throw new PropelException('filterByTva() only accepts arguments of type \Tva or Collection');
        }
    }

    /**
     * Adds a JOIN clause to the query using the Tva relation
     *
     * @param     string $relationAlias optional alias for the relation
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return $this|ChildEntiteQuery The current query, for fluid interface
     */
    public function joinTva($relationAlias = null, $joinType = Criteria::INNER_JOIN)
    {
        $tableMap = $this->getTableMap();
        $relationMap = $tableMap->getRelation('Tva');

        // create a ModelJoin object for this join
        $join = new ModelJoin();
        $join->setJoinType($joinType);
        $join->setRelationMap($relationMap, $this->useAliasInSQL ? $this->getModelAlias() : null, $relationAlias);
        if ($previousJoin = $this->getPreviousJoin()) {
            $join->setPreviousJoin($previousJoin);
        }

        // add the ModelJoin to the current object
        if ($relationAlias) {
            $this->addAlias($relationAlias, $relationMap->getRightTable()->getName());
            $this->addJoinObject($join, $relationAlias);
        } else {
            $this->addJoinObject($join, 'Tva');
        }

        return $this;
    }

    /**
     * Use the Tva relation Tva object
     *
     * @see useQuery()
     *
     * @param     string $relationAlias optional alias for the relation,
     *                                   to be used as main alias in the secondary query
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return \TvaQuery A secondary query class using the current class as primary query
     */
    public function useTvaQuery($relationAlias = null, $joinType = Criteria::INNER_JOIN)
    {
        return $this
            ->joinTva($relationAlias, $joinType)
            ->useQuery($relationAlias ? $relationAlias : 'Tva', '\TvaQuery');
    }

    /**
     * Filter the query by a related \Courrier object
     *
     * @param \Courrier|ObjectCollection $courrier the related object to use as filter
     * @param string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return ChildEntiteQuery The current query, for fluid interface
     */
    public function filterByCourrier($courrier, $comparison = null)
    {
        if ($courrier instanceof \Courrier) {
            return $this
                ->addUsingAlias(EntiteTableMap::COL_ID_ENTITE, $courrier->getIdEntite(), $comparison);
        } elseif ($courrier instanceof ObjectCollection) {
            return $this
                ->useCourrierQuery()
                ->filterByPrimaryKeys($courrier->getPrimaryKeys())
                ->endUse();
        } else {
            throw new PropelException('filterByCourrier() only accepts arguments of type \Courrier or Collection');
        }
    }

    /**
     * Adds a JOIN clause to the query using the Courrier relation
     *
     * @param     string $relationAlias optional alias for the relation
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return $this|ChildEntiteQuery The current query, for fluid interface
     */
    public function joinCourrier($relationAlias = null, $joinType = Criteria::INNER_JOIN)
    {
        $tableMap = $this->getTableMap();
        $relationMap = $tableMap->getRelation('Courrier');

        // create a ModelJoin object for this join
        $join = new ModelJoin();
        $join->setJoinType($joinType);
        $join->setRelationMap($relationMap, $this->useAliasInSQL ? $this->getModelAlias() : null, $relationAlias);
        if ($previousJoin = $this->getPreviousJoin()) {
            $join->setPreviousJoin($previousJoin);
        }

        // add the ModelJoin to the current object
        if ($relationAlias) {
            $this->addAlias($relationAlias, $relationMap->getRightTable()->getName());
            $this->addJoinObject($join, $relationAlias);
        } else {
            $this->addJoinObject($join, 'Courrier');
        }

        return $this;
    }

    /**
     * Use the Courrier relation Courrier object
     *
     * @see useQuery()
     *
     * @param     string $relationAlias optional alias for the relation,
     *                                   to be used as main alias in the secondary query
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return \CourrierQuery A secondary query class using the current class as primary query
     */
    public function useCourrierQuery($relationAlias = null, $joinType = Criteria::INNER_JOIN)
    {
        return $this
            ->joinCourrier($relationAlias, $joinType)
            ->useQuery($relationAlias ? $relationAlias : 'Courrier', '\CourrierQuery');
    }

    /**
     * Exclude object from result
     *
     * @param   ChildEntite $entite Object to remove from the list of results
     *
     * @return $this|ChildEntiteQuery The current query, for fluid interface
     */
    public function prune($entite = null)
    {
        if ($entite) {
            $this->addUsingAlias(EntiteTableMap::COL_ID_ENTITE, $entite->getIdEntite(), Criteria::NOT_EQUAL);
        }

        return $this;
    }

    /**
     * Code to execute before every DELETE statement
     *
     * @param     ConnectionInterface $con The connection object used by the query
     */
    protected function basePreDelete(ConnectionInterface $con)
    {
        // archivable behavior

        if ($this->archiveOnDelete) {
            $this->archive($con);
        } else {
            $this->archiveOnDelete = true;
        }


        return $this->preDelete($con);
    }

    /**
     * Deletes all rows from the asso_entites table.
     *
     * @param ConnectionInterface $con the connection to use
     * @return int The number of affected rows (if supported by underlying database driver).
     */
    public function doDeleteAll(ConnectionInterface $con = null)
    {
        if (null === $con) {
            $con = Propel::getServiceContainer()->getWriteConnection(EntiteTableMap::DATABASE_NAME);
        }

        // use transaction because $criteria could contain info
        // for more than one table or we could emulating ON DELETE CASCADE, etc.
        return $con->transaction(function () use ($con) {
            $affectedRows = 0; // initialize var to track total num of affected rows
            $affectedRows += parent::doDeleteAll($con);
            // Because this db requires some delete cascade/set null emulation, we have to
            // clear the cached instance *after* the emulation has happened (since
            // instances get re-added by the select statement contained therein).
            EntiteTableMap::clearInstancePool();
            EntiteTableMap::clearRelatedInstancePool();

            return $affectedRows;
        });
    }

    /**
     * Performs a DELETE on the database based on the current ModelCriteria
     *
     * @param ConnectionInterface $con the connection to use
     * @return int             The number of affected rows (if supported by underlying database driver).  This includes CASCADE-related rows
     *                         if supported by native driver or if emulated using Propel.
     * @throws PropelException Any exceptions caught during processing will be
     *                         rethrown wrapped into a PropelException.
     */
    public function delete(ConnectionInterface $con = null)
    {
        if (null === $con) {
            $con = Propel::getServiceContainer()->getWriteConnection(EntiteTableMap::DATABASE_NAME);
        }

        $criteria = $this;

        // Set the correct dbName
        $criteria->setDbName(EntiteTableMap::DATABASE_NAME);

        // use transaction because $criteria could contain info
        // for more than one table or we could emulating ON DELETE CASCADE, etc.
        return $con->transaction(function () use ($con, $criteria) {
            $affectedRows = 0; // initialize var to track total num of affected rows

            EntiteTableMap::removeInstanceFromPool($criteria);

            $affectedRows += ModelCriteria::delete($con);
            EntiteTableMap::clearRelatedInstancePool();

            return $affectedRows;
        });
    }

    // timestampable behavior

    /**
     * Filter by the latest updated
     *
     * @param      int $nbDays Maximum age of the latest update in days
     *
     * @return     $this|ChildEntiteQuery The current query, for fluid interface
     */
    public function recentlyUpdated($nbDays = 7)
    {
        return $this->addUsingAlias(EntiteTableMap::COL_UPDATED_AT, time() - $nbDays * 24 * 60 * 60, Criteria::GREATER_EQUAL);
    }

    /**
     * Order by update date desc
     *
     * @return     $this|ChildEntiteQuery The current query, for fluid interface
     */
    public function lastUpdatedFirst()
    {
        return $this->addDescendingOrderByColumn(EntiteTableMap::COL_UPDATED_AT);
    }

    /**
     * Order by update date asc
     *
     * @return     $this|ChildEntiteQuery The current query, for fluid interface
     */
    public function firstUpdatedFirst()
    {
        return $this->addAscendingOrderByColumn(EntiteTableMap::COL_UPDATED_AT);
    }

    /**
     * Order by create date desc
     *
     * @return     $this|ChildEntiteQuery The current query, for fluid interface
     */
    public function lastCreatedFirst()
    {
        return $this->addDescendingOrderByColumn(EntiteTableMap::COL_CREATED_AT);
    }

    /**
     * Filter by the latest created
     *
     * @param      int $nbDays Maximum age of in days
     *
     * @return     $this|ChildEntiteQuery The current query, for fluid interface
     */
    public function recentlyCreated($nbDays = 7)
    {
        return $this->addUsingAlias(EntiteTableMap::COL_CREATED_AT, time() - $nbDays * 24 * 60 * 60, Criteria::GREATER_EQUAL);
    }

    /**
     * Order by create date asc
     *
     * @return     $this|ChildEntiteQuery The current query, for fluid interface
     */
    public function firstCreatedFirst()
    {
        return $this->addAscendingOrderByColumn(EntiteTableMap::COL_CREATED_AT);
    }

    // archivable behavior

    /**
     * Copy the data of the objects satisfying the query into ChildAssoEntitesArchive archive objects.
     * The archived objects are then saved.
     * If any of the objects has already been archived, the archived object
     * is updated and not duplicated.
     * Warning: This termination methods issues 2n+1 queries.
     *
     * @param      ConnectionInterface $con    Connection to use.
     * @param      Boolean $useLittleMemory    Whether or not to use OnDemandFormatter to retrieve objects.
     *               Set to false if the identity map matters.
     *               Set to true (default) to use less memory.
     *
     * @return     int the number of archived objects
     */
    public function archive($con = null, $useLittleMemory = true)
    {
        $criteria = clone $this;
        // prepare the query
        $criteria->setWith(array());
        if ($useLittleMemory) {
            $criteria->setFormatter(ModelCriteria::FORMAT_ON_DEMAND);
        }
        if ($con === null) {
            $con = Propel::getServiceContainer()->getWriteConnection(EntiteTableMap::DATABASE_NAME);
        }

        return $con->transaction(function () use ($con, $criteria) {
            $totalArchivedObjects = 0;

            // archive all results one by one
            foreach ($criteria->find($con) as $object) {
                $object->archive($con);
                $totalArchivedObjects++;
            }

            return $totalArchivedObjects;
        });
    }

    /**
     * Enable/disable auto-archiving on delete for the next query.
     *
     * @param boolean True if the query must archive deleted objects, false otherwise.
     */
    public function setArchiveOnDelete($archiveOnDelete)
    {
        $this->archiveOnDelete = $archiveOnDelete;
    }

    /**
     * Delete records matching the current query without archiving them.
     *
     * @param      ConnectionInterface $con    Connection to use.
     *
     * @return integer the number of deleted rows
     */
    public function deleteWithoutArchive($con = null)
    {
        $this->archiveOnDelete = false;

        return $this->delete($con);
    }

    /**
     * Delete all records without archiving them.
     *
     * @param      ConnectionInterface $con    Connection to use.
     *
     * @return integer the number of deleted rows
     */
    public function deleteAllWithoutArchive($con = null)
    {
        $this->archiveOnDelete = false;

        return $this->deleteAll($con);
    }

} // EntiteQuery
