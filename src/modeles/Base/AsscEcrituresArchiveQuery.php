<?php

namespace Base;

use \AsscEcrituresArchive as ChildAsscEcrituresArchive;
use \AsscEcrituresArchiveQuery as ChildAsscEcrituresArchiveQuery;
use \Exception;
use \PDO;
use Map\AsscEcrituresArchiveTableMap;
use Propel\Runtime\Propel;
use Propel\Runtime\ActiveQuery\Criteria;
use Propel\Runtime\ActiveQuery\ModelCriteria;
use Propel\Runtime\Collection\ObjectCollection;
use Propel\Runtime\Connection\ConnectionInterface;
use Propel\Runtime\Exception\PropelException;

/**
 * Base class that represents a query for the 'assc_ecritures_archive' table.
 *
 *
 *
 * @method     ChildAsscEcrituresArchiveQuery orderByIdEcriture($order = Criteria::ASC) Order by the id_ecriture column
 * @method     ChildAsscEcrituresArchiveQuery orderByClassement($order = Criteria::ASC) Order by the classement column
 * @method     ChildAsscEcrituresArchiveQuery orderByCredit($order = Criteria::ASC) Order by the credit column
 * @method     ChildAsscEcrituresArchiveQuery orderByDebit($order = Criteria::ASC) Order by the debit column
 * @method     ChildAsscEcrituresArchiveQuery orderByNom($order = Criteria::ASC) Order by the nom column
 * @method     ChildAsscEcrituresArchiveQuery orderByIdCompte($order = Criteria::ASC) Order by the id_compte column
 * @method     ChildAsscEcrituresArchiveQuery orderByIdJournal($order = Criteria::ASC) Order by the id_journal column
 * @method     ChildAsscEcrituresArchiveQuery orderByIdActivite($order = Criteria::ASC) Order by the id_activite column
 * @method     ChildAsscEcrituresArchiveQuery orderByIdEntite($order = Criteria::ASC) Order by the id_entite column
 * @method     ChildAsscEcrituresArchiveQuery orderByLettrage($order = Criteria::ASC) Order by the lettrage column
 * @method     ChildAsscEcrituresArchiveQuery orderByDateLettrage($order = Criteria::ASC) Order by the date_lettrage column
 * @method     ChildAsscEcrituresArchiveQuery orderByDateEcriture($order = Criteria::ASC) Order by the date_ecriture column
 * @method     ChildAsscEcrituresArchiveQuery orderByEtat($order = Criteria::ASC) Order by the etat column
 * @method     ChildAsscEcrituresArchiveQuery orderByObservation($order = Criteria::ASC) Order by the observation column
 * @method     ChildAsscEcrituresArchiveQuery orderByIdPiece($order = Criteria::ASC) Order by the id_piece column
 * @method     ChildAsscEcrituresArchiveQuery orderByEcranSaisie($order = Criteria::ASC) Order by the ecran_saisie column
 * @method     ChildAsscEcrituresArchiveQuery orderByContrePartie($order = Criteria::ASC) Order by the contre_partie column
 * @method     ChildAsscEcrituresArchiveQuery orderByQuantite($order = Criteria::ASC) Order by the quantite column
 * @method     ChildAsscEcrituresArchiveQuery orderByCreatedAt($order = Criteria::ASC) Order by the created_at column
 * @method     ChildAsscEcrituresArchiveQuery orderByUpdatedAt($order = Criteria::ASC) Order by the updated_at column
 * @method     ChildAsscEcrituresArchiveQuery orderByArchivedAt($order = Criteria::ASC) Order by the archived_at column
 *
 * @method     ChildAsscEcrituresArchiveQuery groupByIdEcriture() Group by the id_ecriture column
 * @method     ChildAsscEcrituresArchiveQuery groupByClassement() Group by the classement column
 * @method     ChildAsscEcrituresArchiveQuery groupByCredit() Group by the credit column
 * @method     ChildAsscEcrituresArchiveQuery groupByDebit() Group by the debit column
 * @method     ChildAsscEcrituresArchiveQuery groupByNom() Group by the nom column
 * @method     ChildAsscEcrituresArchiveQuery groupByIdCompte() Group by the id_compte column
 * @method     ChildAsscEcrituresArchiveQuery groupByIdJournal() Group by the id_journal column
 * @method     ChildAsscEcrituresArchiveQuery groupByIdActivite() Group by the id_activite column
 * @method     ChildAsscEcrituresArchiveQuery groupByIdEntite() Group by the id_entite column
 * @method     ChildAsscEcrituresArchiveQuery groupByLettrage() Group by the lettrage column
 * @method     ChildAsscEcrituresArchiveQuery groupByDateLettrage() Group by the date_lettrage column
 * @method     ChildAsscEcrituresArchiveQuery groupByDateEcriture() Group by the date_ecriture column
 * @method     ChildAsscEcrituresArchiveQuery groupByEtat() Group by the etat column
 * @method     ChildAsscEcrituresArchiveQuery groupByObservation() Group by the observation column
 * @method     ChildAsscEcrituresArchiveQuery groupByIdPiece() Group by the id_piece column
 * @method     ChildAsscEcrituresArchiveQuery groupByEcranSaisie() Group by the ecran_saisie column
 * @method     ChildAsscEcrituresArchiveQuery groupByContrePartie() Group by the contre_partie column
 * @method     ChildAsscEcrituresArchiveQuery groupByQuantite() Group by the quantite column
 * @method     ChildAsscEcrituresArchiveQuery groupByCreatedAt() Group by the created_at column
 * @method     ChildAsscEcrituresArchiveQuery groupByUpdatedAt() Group by the updated_at column
 * @method     ChildAsscEcrituresArchiveQuery groupByArchivedAt() Group by the archived_at column
 *
 * @method     ChildAsscEcrituresArchiveQuery leftJoin($relation) Adds a LEFT JOIN clause to the query
 * @method     ChildAsscEcrituresArchiveQuery rightJoin($relation) Adds a RIGHT JOIN clause to the query
 * @method     ChildAsscEcrituresArchiveQuery innerJoin($relation) Adds a INNER JOIN clause to the query
 *
 * @method     ChildAsscEcrituresArchiveQuery leftJoinWith($relation) Adds a LEFT JOIN clause and with to the query
 * @method     ChildAsscEcrituresArchiveQuery rightJoinWith($relation) Adds a RIGHT JOIN clause and with to the query
 * @method     ChildAsscEcrituresArchiveQuery innerJoinWith($relation) Adds a INNER JOIN clause and with to the query
 *
 * @method     ChildAsscEcrituresArchive findOne(ConnectionInterface $con = null) Return the first ChildAsscEcrituresArchive matching the query
 * @method     ChildAsscEcrituresArchive findOneOrCreate(ConnectionInterface $con = null) Return the first ChildAsscEcrituresArchive matching the query, or a new ChildAsscEcrituresArchive object populated from the query conditions when no match is found
 *
 * @method     ChildAsscEcrituresArchive findOneByIdEcriture(string $id_ecriture) Return the first ChildAsscEcrituresArchive filtered by the id_ecriture column
 * @method     ChildAsscEcrituresArchive findOneByClassement(string $classement) Return the first ChildAsscEcrituresArchive filtered by the classement column
 * @method     ChildAsscEcrituresArchive findOneByCredit(double $credit) Return the first ChildAsscEcrituresArchive filtered by the credit column
 * @method     ChildAsscEcrituresArchive findOneByDebit(double $debit) Return the first ChildAsscEcrituresArchive filtered by the debit column
 * @method     ChildAsscEcrituresArchive findOneByNom(string $nom) Return the first ChildAsscEcrituresArchive filtered by the nom column
 * @method     ChildAsscEcrituresArchive findOneByIdCompte(string $id_compte) Return the first ChildAsscEcrituresArchive filtered by the id_compte column
 * @method     ChildAsscEcrituresArchive findOneByIdJournal(int $id_journal) Return the first ChildAsscEcrituresArchive filtered by the id_journal column
 * @method     ChildAsscEcrituresArchive findOneByIdActivite(string $id_activite) Return the first ChildAsscEcrituresArchive filtered by the id_activite column
 * @method     ChildAsscEcrituresArchive findOneByIdEntite(string $id_entite) Return the first ChildAsscEcrituresArchive filtered by the id_entite column
 * @method     ChildAsscEcrituresArchive findOneByLettrage(string $lettrage) Return the first ChildAsscEcrituresArchive filtered by the lettrage column
 * @method     ChildAsscEcrituresArchive findOneByDateLettrage(string $date_lettrage) Return the first ChildAsscEcrituresArchive filtered by the date_lettrage column
 * @method     ChildAsscEcrituresArchive findOneByDateEcriture(string $date_ecriture) Return the first ChildAsscEcrituresArchive filtered by the date_ecriture column
 * @method     ChildAsscEcrituresArchive findOneByEtat(int $etat) Return the first ChildAsscEcrituresArchive filtered by the etat column
 * @method     ChildAsscEcrituresArchive findOneByObservation(string $observation) Return the first ChildAsscEcrituresArchive filtered by the observation column
 * @method     ChildAsscEcrituresArchive findOneByIdPiece(string $id_piece) Return the first ChildAsscEcrituresArchive filtered by the id_piece column
 * @method     ChildAsscEcrituresArchive findOneByEcranSaisie(string $ecran_saisie) Return the first ChildAsscEcrituresArchive filtered by the ecran_saisie column
 * @method     ChildAsscEcrituresArchive findOneByContrePartie(boolean $contre_partie) Return the first ChildAsscEcrituresArchive filtered by the contre_partie column
 * @method     ChildAsscEcrituresArchive findOneByQuantite(double $quantite) Return the first ChildAsscEcrituresArchive filtered by the quantite column
 * @method     ChildAsscEcrituresArchive findOneByCreatedAt(string $created_at) Return the first ChildAsscEcrituresArchive filtered by the created_at column
 * @method     ChildAsscEcrituresArchive findOneByUpdatedAt(string $updated_at) Return the first ChildAsscEcrituresArchive filtered by the updated_at column
 * @method     ChildAsscEcrituresArchive findOneByArchivedAt(string $archived_at) Return the first ChildAsscEcrituresArchive filtered by the archived_at column *

 * @method     ChildAsscEcrituresArchive requirePk($key, ConnectionInterface $con = null) Return the ChildAsscEcrituresArchive by primary key and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildAsscEcrituresArchive requireOne(ConnectionInterface $con = null) Return the first ChildAsscEcrituresArchive matching the query and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 *
 * @method     ChildAsscEcrituresArchive requireOneByIdEcriture(string $id_ecriture) Return the first ChildAsscEcrituresArchive filtered by the id_ecriture column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildAsscEcrituresArchive requireOneByClassement(string $classement) Return the first ChildAsscEcrituresArchive filtered by the classement column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildAsscEcrituresArchive requireOneByCredit(double $credit) Return the first ChildAsscEcrituresArchive filtered by the credit column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildAsscEcrituresArchive requireOneByDebit(double $debit) Return the first ChildAsscEcrituresArchive filtered by the debit column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildAsscEcrituresArchive requireOneByNom(string $nom) Return the first ChildAsscEcrituresArchive filtered by the nom column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildAsscEcrituresArchive requireOneByIdCompte(string $id_compte) Return the first ChildAsscEcrituresArchive filtered by the id_compte column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildAsscEcrituresArchive requireOneByIdJournal(int $id_journal) Return the first ChildAsscEcrituresArchive filtered by the id_journal column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildAsscEcrituresArchive requireOneByIdActivite(string $id_activite) Return the first ChildAsscEcrituresArchive filtered by the id_activite column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildAsscEcrituresArchive requireOneByIdEntite(string $id_entite) Return the first ChildAsscEcrituresArchive filtered by the id_entite column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildAsscEcrituresArchive requireOneByLettrage(string $lettrage) Return the first ChildAsscEcrituresArchive filtered by the lettrage column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildAsscEcrituresArchive requireOneByDateLettrage(string $date_lettrage) Return the first ChildAsscEcrituresArchive filtered by the date_lettrage column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildAsscEcrituresArchive requireOneByDateEcriture(string $date_ecriture) Return the first ChildAsscEcrituresArchive filtered by the date_ecriture column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildAsscEcrituresArchive requireOneByEtat(int $etat) Return the first ChildAsscEcrituresArchive filtered by the etat column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildAsscEcrituresArchive requireOneByObservation(string $observation) Return the first ChildAsscEcrituresArchive filtered by the observation column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildAsscEcrituresArchive requireOneByIdPiece(string $id_piece) Return the first ChildAsscEcrituresArchive filtered by the id_piece column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildAsscEcrituresArchive requireOneByEcranSaisie(string $ecran_saisie) Return the first ChildAsscEcrituresArchive filtered by the ecran_saisie column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildAsscEcrituresArchive requireOneByContrePartie(boolean $contre_partie) Return the first ChildAsscEcrituresArchive filtered by the contre_partie column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildAsscEcrituresArchive requireOneByQuantite(double $quantite) Return the first ChildAsscEcrituresArchive filtered by the quantite column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildAsscEcrituresArchive requireOneByCreatedAt(string $created_at) Return the first ChildAsscEcrituresArchive filtered by the created_at column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildAsscEcrituresArchive requireOneByUpdatedAt(string $updated_at) Return the first ChildAsscEcrituresArchive filtered by the updated_at column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildAsscEcrituresArchive requireOneByArchivedAt(string $archived_at) Return the first ChildAsscEcrituresArchive filtered by the archived_at column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 *
 * @method     ChildAsscEcrituresArchive[]|ObjectCollection find(ConnectionInterface $con = null) Return ChildAsscEcrituresArchive objects based on current ModelCriteria
 * @method     ChildAsscEcrituresArchive[]|ObjectCollection findByIdEcriture(string $id_ecriture) Return ChildAsscEcrituresArchive objects filtered by the id_ecriture column
 * @method     ChildAsscEcrituresArchive[]|ObjectCollection findByClassement(string $classement) Return ChildAsscEcrituresArchive objects filtered by the classement column
 * @method     ChildAsscEcrituresArchive[]|ObjectCollection findByCredit(double $credit) Return ChildAsscEcrituresArchive objects filtered by the credit column
 * @method     ChildAsscEcrituresArchive[]|ObjectCollection findByDebit(double $debit) Return ChildAsscEcrituresArchive objects filtered by the debit column
 * @method     ChildAsscEcrituresArchive[]|ObjectCollection findByNom(string $nom) Return ChildAsscEcrituresArchive objects filtered by the nom column
 * @method     ChildAsscEcrituresArchive[]|ObjectCollection findByIdCompte(string $id_compte) Return ChildAsscEcrituresArchive objects filtered by the id_compte column
 * @method     ChildAsscEcrituresArchive[]|ObjectCollection findByIdJournal(int $id_journal) Return ChildAsscEcrituresArchive objects filtered by the id_journal column
 * @method     ChildAsscEcrituresArchive[]|ObjectCollection findByIdActivite(string $id_activite) Return ChildAsscEcrituresArchive objects filtered by the id_activite column
 * @method     ChildAsscEcrituresArchive[]|ObjectCollection findByIdEntite(string $id_entite) Return ChildAsscEcrituresArchive objects filtered by the id_entite column
 * @method     ChildAsscEcrituresArchive[]|ObjectCollection findByLettrage(string $lettrage) Return ChildAsscEcrituresArchive objects filtered by the lettrage column
 * @method     ChildAsscEcrituresArchive[]|ObjectCollection findByDateLettrage(string $date_lettrage) Return ChildAsscEcrituresArchive objects filtered by the date_lettrage column
 * @method     ChildAsscEcrituresArchive[]|ObjectCollection findByDateEcriture(string $date_ecriture) Return ChildAsscEcrituresArchive objects filtered by the date_ecriture column
 * @method     ChildAsscEcrituresArchive[]|ObjectCollection findByEtat(int $etat) Return ChildAsscEcrituresArchive objects filtered by the etat column
 * @method     ChildAsscEcrituresArchive[]|ObjectCollection findByObservation(string $observation) Return ChildAsscEcrituresArchive objects filtered by the observation column
 * @method     ChildAsscEcrituresArchive[]|ObjectCollection findByIdPiece(string $id_piece) Return ChildAsscEcrituresArchive objects filtered by the id_piece column
 * @method     ChildAsscEcrituresArchive[]|ObjectCollection findByEcranSaisie(string $ecran_saisie) Return ChildAsscEcrituresArchive objects filtered by the ecran_saisie column
 * @method     ChildAsscEcrituresArchive[]|ObjectCollection findByContrePartie(boolean $contre_partie) Return ChildAsscEcrituresArchive objects filtered by the contre_partie column
 * @method     ChildAsscEcrituresArchive[]|ObjectCollection findByQuantite(double $quantite) Return ChildAsscEcrituresArchive objects filtered by the quantite column
 * @method     ChildAsscEcrituresArchive[]|ObjectCollection findByCreatedAt(string $created_at) Return ChildAsscEcrituresArchive objects filtered by the created_at column
 * @method     ChildAsscEcrituresArchive[]|ObjectCollection findByUpdatedAt(string $updated_at) Return ChildAsscEcrituresArchive objects filtered by the updated_at column
 * @method     ChildAsscEcrituresArchive[]|ObjectCollection findByArchivedAt(string $archived_at) Return ChildAsscEcrituresArchive objects filtered by the archived_at column
 * @method     ChildAsscEcrituresArchive[]|\Propel\Runtime\Util\PropelModelPager paginate($page = 1, $maxPerPage = 10, ConnectionInterface $con = null) Issue a SELECT query based on the current ModelCriteria and uses a page and a maximum number of results per page to compute an offset and a limit
 *
 */
abstract class AsscEcrituresArchiveQuery extends ModelCriteria
{
    protected $entityNotFoundExceptionClass = '\\Propel\\Runtime\\Exception\\EntityNotFoundException';

    /**
     * Initializes internal state of \Base\AsscEcrituresArchiveQuery object.
     *
     * @param     string $dbName The database name
     * @param     string $modelName The phpName of a model, e.g. 'Book'
     * @param     string $modelAlias The alias for the model in this query, e.g. 'b'
     */
    public function __construct($dbName = 'simplasso', $modelName = '\\AsscEcrituresArchive', $modelAlias = null)
    {
        parent::__construct($dbName, $modelName, $modelAlias);
    }

    /**
     * Returns a new ChildAsscEcrituresArchiveQuery object.
     *
     * @param     string $modelAlias The alias of a model in the query
     * @param     Criteria $criteria Optional Criteria to build the query from
     *
     * @return ChildAsscEcrituresArchiveQuery
     */
    public static function create($modelAlias = null, Criteria $criteria = null)
    {
        if ($criteria instanceof ChildAsscEcrituresArchiveQuery) {
            return $criteria;
        }
        $query = new ChildAsscEcrituresArchiveQuery();
        if (null !== $modelAlias) {
            $query->setModelAlias($modelAlias);
        }
        if ($criteria instanceof Criteria) {
            $query->mergeWith($criteria);
        }

        return $query;
    }

    /**
     * Find object by primary key.
     * Propel uses the instance pool to skip the database if the object exists.
     * Go fast if the query is untouched.
     *
     * <code>
     * $obj  = $c->findPk(12, $con);
     * </code>
     *
     * @param mixed $key Primary key to use for the query
     * @param ConnectionInterface $con an optional connection object
     *
     * @return ChildAsscEcrituresArchive|array|mixed the result, formatted by the current formatter
     */
    public function findPk($key, ConnectionInterface $con = null)
    {
        if ($key === null) {
            return null;
        }

        if ($con === null) {
            $con = Propel::getServiceContainer()->getReadConnection(AsscEcrituresArchiveTableMap::DATABASE_NAME);
        }

        $this->basePreSelect($con);

        if (
            $this->formatter || $this->modelAlias || $this->with || $this->select
            || $this->selectColumns || $this->asColumns || $this->selectModifiers
            || $this->map || $this->having || $this->joins
        ) {
            return $this->findPkComplex($key, $con);
        }

        if ((null !== ($obj = AsscEcrituresArchiveTableMap::getInstanceFromPool(null === $key || is_scalar($key) || is_callable([$key, '__toString']) ? (string) $key : $key)))) {
            // the object is already in the instance pool
            return $obj;
        }

        return $this->findPkSimple($key, $con);
    }

    /**
     * Find object by primary key using raw SQL to go fast.
     * Bypass doSelect() and the object formatter by using generated code.
     *
     * @param     mixed $key Primary key to use for the query
     * @param     ConnectionInterface $con A connection object
     *
     * @throws \Propel\Runtime\Exception\PropelException
     *
     * @return ChildAsscEcrituresArchive A model object, or null if the key is not found
     */
    protected function findPkSimple($key, ConnectionInterface $con)
    {
        $sql = 'SELECT `id_ecriture`, `classement`, `credit`, `debit`, `nom`, `id_compte`, `id_journal`, `id_activite`, `id_entite`, `lettrage`, `date_lettrage`, `date_ecriture`, `etat`, `observation`, `id_piece`, `ecran_saisie`, `contre_partie`, `quantite`, `created_at`, `updated_at`, `archived_at` FROM `assc_ecritures_archive` WHERE `id_ecriture` = :p0';
        try {
            $stmt = $con->prepare($sql);
            $stmt->bindValue(':p0', $key, PDO::PARAM_INT);
            $stmt->execute();
        } catch (Exception $e) {
            Propel::log($e->getMessage(), Propel::LOG_ERR);
            throw new PropelException(sprintf('Unable to execute SELECT statement [%s]', $sql), 0, $e);
        }
        $obj = null;
        if ($row = $stmt->fetch(\PDO::FETCH_NUM)) {
            /** @var ChildAsscEcrituresArchive $obj */
            $obj = new ChildAsscEcrituresArchive();
            $obj->hydrate($row);
            AsscEcrituresArchiveTableMap::addInstanceToPool($obj, null === $key || is_scalar($key) || is_callable([$key, '__toString']) ? (string) $key : $key);
        }
        $stmt->closeCursor();

        return $obj;
    }

    /**
     * Find object by primary key.
     *
     * @param     mixed $key Primary key to use for the query
     * @param     ConnectionInterface $con A connection object
     *
     * @return ChildAsscEcrituresArchive|array|mixed the result, formatted by the current formatter
     */
    protected function findPkComplex($key, ConnectionInterface $con)
    {
        // As the query uses a PK condition, no limit(1) is necessary.
        $criteria = $this->isKeepQuery() ? clone $this : $this;
        $dataFetcher = $criteria
            ->filterByPrimaryKey($key)
            ->doSelect($con);

        return $criteria->getFormatter()->init($criteria)->formatOne($dataFetcher);
    }

    /**
     * Find objects by primary key
     * <code>
     * $objs = $c->findPks(array(12, 56, 832), $con);
     * </code>
     * @param     array $keys Primary keys to use for the query
     * @param     ConnectionInterface $con an optional connection object
     *
     * @return ObjectCollection|array|mixed the list of results, formatted by the current formatter
     */
    public function findPks($keys, ConnectionInterface $con = null)
    {
        if (null === $con) {
            $con = Propel::getServiceContainer()->getReadConnection($this->getDbName());
        }
        $this->basePreSelect($con);
        $criteria = $this->isKeepQuery() ? clone $this : $this;
        $dataFetcher = $criteria
            ->filterByPrimaryKeys($keys)
            ->doSelect($con);

        return $criteria->getFormatter()->init($criteria)->format($dataFetcher);
    }

    /**
     * Filter the query by primary key
     *
     * @param     mixed $key Primary key to use for the query
     *
     * @return $this|ChildAsscEcrituresArchiveQuery The current query, for fluid interface
     */
    public function filterByPrimaryKey($key)
    {

        return $this->addUsingAlias(AsscEcrituresArchiveTableMap::COL_ID_ECRITURE, $key, Criteria::EQUAL);
    }

    /**
     * Filter the query by a list of primary keys
     *
     * @param     array $keys The list of primary key to use for the query
     *
     * @return $this|ChildAsscEcrituresArchiveQuery The current query, for fluid interface
     */
    public function filterByPrimaryKeys($keys)
    {

        return $this->addUsingAlias(AsscEcrituresArchiveTableMap::COL_ID_ECRITURE, $keys, Criteria::IN);
    }

    /**
     * Filter the query on the id_ecriture column
     *
     * Example usage:
     * <code>
     * $query->filterByIdEcriture(1234); // WHERE id_ecriture = 1234
     * $query->filterByIdEcriture(array(12, 34)); // WHERE id_ecriture IN (12, 34)
     * $query->filterByIdEcriture(array('min' => 12)); // WHERE id_ecriture > 12
     * </code>
     *
     * @param     mixed $idEcriture The value to use as filter.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildAsscEcrituresArchiveQuery The current query, for fluid interface
     */
    public function filterByIdEcriture($idEcriture = null, $comparison = null)
    {
        if (is_array($idEcriture)) {
            $useMinMax = false;
            if (isset($idEcriture['min'])) {
                $this->addUsingAlias(AsscEcrituresArchiveTableMap::COL_ID_ECRITURE, $idEcriture['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($idEcriture['max'])) {
                $this->addUsingAlias(AsscEcrituresArchiveTableMap::COL_ID_ECRITURE, $idEcriture['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(AsscEcrituresArchiveTableMap::COL_ID_ECRITURE, $idEcriture, $comparison);
    }

    /**
     * Filter the query on the classement column
     *
     * Example usage:
     * <code>
     * $query->filterByClassement('fooValue');   // WHERE classement = 'fooValue'
     * $query->filterByClassement('%fooValue%', Criteria::LIKE); // WHERE classement LIKE '%fooValue%'
     * </code>
     *
     * @param     string $classement The value to use as filter.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildAsscEcrituresArchiveQuery The current query, for fluid interface
     */
    public function filterByClassement($classement = null, $comparison = null)
    {
        if (null === $comparison) {
            if (is_array($classement)) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(AsscEcrituresArchiveTableMap::COL_CLASSEMENT, $classement, $comparison);
    }

    /**
     * Filter the query on the credit column
     *
     * Example usage:
     * <code>
     * $query->filterByCredit(1234); // WHERE credit = 1234
     * $query->filterByCredit(array(12, 34)); // WHERE credit IN (12, 34)
     * $query->filterByCredit(array('min' => 12)); // WHERE credit > 12
     * </code>
     *
     * @param     mixed $credit The value to use as filter.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildAsscEcrituresArchiveQuery The current query, for fluid interface
     */
    public function filterByCredit($credit = null, $comparison = null)
    {
        if (is_array($credit)) {
            $useMinMax = false;
            if (isset($credit['min'])) {
                $this->addUsingAlias(AsscEcrituresArchiveTableMap::COL_CREDIT, $credit['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($credit['max'])) {
                $this->addUsingAlias(AsscEcrituresArchiveTableMap::COL_CREDIT, $credit['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(AsscEcrituresArchiveTableMap::COL_CREDIT, $credit, $comparison);
    }

    /**
     * Filter the query on the debit column
     *
     * Example usage:
     * <code>
     * $query->filterByDebit(1234); // WHERE debit = 1234
     * $query->filterByDebit(array(12, 34)); // WHERE debit IN (12, 34)
     * $query->filterByDebit(array('min' => 12)); // WHERE debit > 12
     * </code>
     *
     * @param     mixed $debit The value to use as filter.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildAsscEcrituresArchiveQuery The current query, for fluid interface
     */
    public function filterByDebit($debit = null, $comparison = null)
    {
        if (is_array($debit)) {
            $useMinMax = false;
            if (isset($debit['min'])) {
                $this->addUsingAlias(AsscEcrituresArchiveTableMap::COL_DEBIT, $debit['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($debit['max'])) {
                $this->addUsingAlias(AsscEcrituresArchiveTableMap::COL_DEBIT, $debit['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(AsscEcrituresArchiveTableMap::COL_DEBIT, $debit, $comparison);
    }

    /**
     * Filter the query on the nom column
     *
     * Example usage:
     * <code>
     * $query->filterByNom('fooValue');   // WHERE nom = 'fooValue'
     * $query->filterByNom('%fooValue%', Criteria::LIKE); // WHERE nom LIKE '%fooValue%'
     * </code>
     *
     * @param     string $nom The value to use as filter.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildAsscEcrituresArchiveQuery The current query, for fluid interface
     */
    public function filterByNom($nom = null, $comparison = null)
    {
        if (null === $comparison) {
            if (is_array($nom)) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(AsscEcrituresArchiveTableMap::COL_NOM, $nom, $comparison);
    }

    /**
     * Filter the query on the id_compte column
     *
     * Example usage:
     * <code>
     * $query->filterByIdCompte(1234); // WHERE id_compte = 1234
     * $query->filterByIdCompte(array(12, 34)); // WHERE id_compte IN (12, 34)
     * $query->filterByIdCompte(array('min' => 12)); // WHERE id_compte > 12
     * </code>
     *
     * @param     mixed $idCompte The value to use as filter.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildAsscEcrituresArchiveQuery The current query, for fluid interface
     */
    public function filterByIdCompte($idCompte = null, $comparison = null)
    {
        if (is_array($idCompte)) {
            $useMinMax = false;
            if (isset($idCompte['min'])) {
                $this->addUsingAlias(AsscEcrituresArchiveTableMap::COL_ID_COMPTE, $idCompte['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($idCompte['max'])) {
                $this->addUsingAlias(AsscEcrituresArchiveTableMap::COL_ID_COMPTE, $idCompte['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(AsscEcrituresArchiveTableMap::COL_ID_COMPTE, $idCompte, $comparison);
    }

    /**
     * Filter the query on the id_journal column
     *
     * Example usage:
     * <code>
     * $query->filterByIdJournal(1234); // WHERE id_journal = 1234
     * $query->filterByIdJournal(array(12, 34)); // WHERE id_journal IN (12, 34)
     * $query->filterByIdJournal(array('min' => 12)); // WHERE id_journal > 12
     * </code>
     *
     * @param     mixed $idJournal The value to use as filter.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildAsscEcrituresArchiveQuery The current query, for fluid interface
     */
    public function filterByIdJournal($idJournal = null, $comparison = null)
    {
        if (is_array($idJournal)) {
            $useMinMax = false;
            if (isset($idJournal['min'])) {
                $this->addUsingAlias(AsscEcrituresArchiveTableMap::COL_ID_JOURNAL, $idJournal['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($idJournal['max'])) {
                $this->addUsingAlias(AsscEcrituresArchiveTableMap::COL_ID_JOURNAL, $idJournal['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(AsscEcrituresArchiveTableMap::COL_ID_JOURNAL, $idJournal, $comparison);
    }

    /**
     * Filter the query on the id_activite column
     *
     * Example usage:
     * <code>
     * $query->filterByIdActivite(1234); // WHERE id_activite = 1234
     * $query->filterByIdActivite(array(12, 34)); // WHERE id_activite IN (12, 34)
     * $query->filterByIdActivite(array('min' => 12)); // WHERE id_activite > 12
     * </code>
     *
     * @param     mixed $idActivite The value to use as filter.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildAsscEcrituresArchiveQuery The current query, for fluid interface
     */
    public function filterByIdActivite($idActivite = null, $comparison = null)
    {
        if (is_array($idActivite)) {
            $useMinMax = false;
            if (isset($idActivite['min'])) {
                $this->addUsingAlias(AsscEcrituresArchiveTableMap::COL_ID_ACTIVITE, $idActivite['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($idActivite['max'])) {
                $this->addUsingAlias(AsscEcrituresArchiveTableMap::COL_ID_ACTIVITE, $idActivite['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(AsscEcrituresArchiveTableMap::COL_ID_ACTIVITE, $idActivite, $comparison);
    }

    /**
     * Filter the query on the id_entite column
     *
     * Example usage:
     * <code>
     * $query->filterByIdEntite(1234); // WHERE id_entite = 1234
     * $query->filterByIdEntite(array(12, 34)); // WHERE id_entite IN (12, 34)
     * $query->filterByIdEntite(array('min' => 12)); // WHERE id_entite > 12
     * </code>
     *
     * @param     mixed $idEntite The value to use as filter.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildAsscEcrituresArchiveQuery The current query, for fluid interface
     */
    public function filterByIdEntite($idEntite = null, $comparison = null)
    {
        if (is_array($idEntite)) {
            $useMinMax = false;
            if (isset($idEntite['min'])) {
                $this->addUsingAlias(AsscEcrituresArchiveTableMap::COL_ID_ENTITE, $idEntite['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($idEntite['max'])) {
                $this->addUsingAlias(AsscEcrituresArchiveTableMap::COL_ID_ENTITE, $idEntite['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(AsscEcrituresArchiveTableMap::COL_ID_ENTITE, $idEntite, $comparison);
    }

    /**
     * Filter the query on the lettrage column
     *
     * Example usage:
     * <code>
     * $query->filterByLettrage('fooValue');   // WHERE lettrage = 'fooValue'
     * $query->filterByLettrage('%fooValue%', Criteria::LIKE); // WHERE lettrage LIKE '%fooValue%'
     * </code>
     *
     * @param     string $lettrage The value to use as filter.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildAsscEcrituresArchiveQuery The current query, for fluid interface
     */
    public function filterByLettrage($lettrage = null, $comparison = null)
    {
        if (null === $comparison) {
            if (is_array($lettrage)) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(AsscEcrituresArchiveTableMap::COL_LETTRAGE, $lettrage, $comparison);
    }

    /**
     * Filter the query on the date_lettrage column
     *
     * Example usage:
     * <code>
     * $query->filterByDateLettrage('2011-03-14'); // WHERE date_lettrage = '2011-03-14'
     * $query->filterByDateLettrage('now'); // WHERE date_lettrage = '2011-03-14'
     * $query->filterByDateLettrage(array('max' => 'yesterday')); // WHERE date_lettrage > '2011-03-13'
     * </code>
     *
     * @param     mixed $dateLettrage The value to use as filter.
     *              Values can be integers (unix timestamps), DateTime objects, or strings.
     *              Empty strings are treated as NULL.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildAsscEcrituresArchiveQuery The current query, for fluid interface
     */
    public function filterByDateLettrage($dateLettrage = null, $comparison = null)
    {
        if (is_array($dateLettrage)) {
            $useMinMax = false;
            if (isset($dateLettrage['min'])) {
                $this->addUsingAlias(AsscEcrituresArchiveTableMap::COL_DATE_LETTRAGE, $dateLettrage['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($dateLettrage['max'])) {
                $this->addUsingAlias(AsscEcrituresArchiveTableMap::COL_DATE_LETTRAGE, $dateLettrage['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(AsscEcrituresArchiveTableMap::COL_DATE_LETTRAGE, $dateLettrage, $comparison);
    }

    /**
     * Filter the query on the date_ecriture column
     *
     * Example usage:
     * <code>
     * $query->filterByDateEcriture('2011-03-14'); // WHERE date_ecriture = '2011-03-14'
     * $query->filterByDateEcriture('now'); // WHERE date_ecriture = '2011-03-14'
     * $query->filterByDateEcriture(array('max' => 'yesterday')); // WHERE date_ecriture > '2011-03-13'
     * </code>
     *
     * @param     mixed $dateEcriture The value to use as filter.
     *              Values can be integers (unix timestamps), DateTime objects, or strings.
     *              Empty strings are treated as NULL.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildAsscEcrituresArchiveQuery The current query, for fluid interface
     */
    public function filterByDateEcriture($dateEcriture = null, $comparison = null)
    {
        if (is_array($dateEcriture)) {
            $useMinMax = false;
            if (isset($dateEcriture['min'])) {
                $this->addUsingAlias(AsscEcrituresArchiveTableMap::COL_DATE_ECRITURE, $dateEcriture['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($dateEcriture['max'])) {
                $this->addUsingAlias(AsscEcrituresArchiveTableMap::COL_DATE_ECRITURE, $dateEcriture['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(AsscEcrituresArchiveTableMap::COL_DATE_ECRITURE, $dateEcriture, $comparison);
    }

    /**
     * Filter the query on the etat column
     *
     * Example usage:
     * <code>
     * $query->filterByEtat(1234); // WHERE etat = 1234
     * $query->filterByEtat(array(12, 34)); // WHERE etat IN (12, 34)
     * $query->filterByEtat(array('min' => 12)); // WHERE etat > 12
     * </code>
     *
     * @param     mixed $etat The value to use as filter.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildAsscEcrituresArchiveQuery The current query, for fluid interface
     */
    public function filterByEtat($etat = null, $comparison = null)
    {
        if (is_array($etat)) {
            $useMinMax = false;
            if (isset($etat['min'])) {
                $this->addUsingAlias(AsscEcrituresArchiveTableMap::COL_ETAT, $etat['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($etat['max'])) {
                $this->addUsingAlias(AsscEcrituresArchiveTableMap::COL_ETAT, $etat['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(AsscEcrituresArchiveTableMap::COL_ETAT, $etat, $comparison);
    }

    /**
     * Filter the query on the observation column
     *
     * Example usage:
     * <code>
     * $query->filterByObservation('fooValue');   // WHERE observation = 'fooValue'
     * $query->filterByObservation('%fooValue%', Criteria::LIKE); // WHERE observation LIKE '%fooValue%'
     * </code>
     *
     * @param     string $observation The value to use as filter.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildAsscEcrituresArchiveQuery The current query, for fluid interface
     */
    public function filterByObservation($observation = null, $comparison = null)
    {
        if (null === $comparison) {
            if (is_array($observation)) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(AsscEcrituresArchiveTableMap::COL_OBSERVATION, $observation, $comparison);
    }

    /**
     * Filter the query on the id_piece column
     *
     * Example usage:
     * <code>
     * $query->filterByIdPiece(1234); // WHERE id_piece = 1234
     * $query->filterByIdPiece(array(12, 34)); // WHERE id_piece IN (12, 34)
     * $query->filterByIdPiece(array('min' => 12)); // WHERE id_piece > 12
     * </code>
     *
     * @param     mixed $idPiece The value to use as filter.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildAsscEcrituresArchiveQuery The current query, for fluid interface
     */
    public function filterByIdPiece($idPiece = null, $comparison = null)
    {
        if (is_array($idPiece)) {
            $useMinMax = false;
            if (isset($idPiece['min'])) {
                $this->addUsingAlias(AsscEcrituresArchiveTableMap::COL_ID_PIECE, $idPiece['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($idPiece['max'])) {
                $this->addUsingAlias(AsscEcrituresArchiveTableMap::COL_ID_PIECE, $idPiece['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(AsscEcrituresArchiveTableMap::COL_ID_PIECE, $idPiece, $comparison);
    }

    /**
     * Filter the query on the ecran_saisie column
     *
     * Example usage:
     * <code>
     * $query->filterByEcranSaisie('fooValue');   // WHERE ecran_saisie = 'fooValue'
     * $query->filterByEcranSaisie('%fooValue%', Criteria::LIKE); // WHERE ecran_saisie LIKE '%fooValue%'
     * </code>
     *
     * @param     string $ecranSaisie The value to use as filter.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildAsscEcrituresArchiveQuery The current query, for fluid interface
     */
    public function filterByEcranSaisie($ecranSaisie = null, $comparison = null)
    {
        if (null === $comparison) {
            if (is_array($ecranSaisie)) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(AsscEcrituresArchiveTableMap::COL_ECRAN_SAISIE, $ecranSaisie, $comparison);
    }

    /**
     * Filter the query on the contre_partie column
     *
     * Example usage:
     * <code>
     * $query->filterByContrePartie(true); // WHERE contre_partie = true
     * $query->filterByContrePartie('yes'); // WHERE contre_partie = true
     * </code>
     *
     * @param     boolean|string $contrePartie The value to use as filter.
     *              Non-boolean arguments are converted using the following rules:
     *                * 1, '1', 'true',  'on',  and 'yes' are converted to boolean true
     *                * 0, '0', 'false', 'off', and 'no'  are converted to boolean false
     *              Check on string values is case insensitive (so 'FaLsE' is seen as 'false').
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildAsscEcrituresArchiveQuery The current query, for fluid interface
     */
    public function filterByContrePartie($contrePartie = null, $comparison = null)
    {
        if (is_string($contrePartie)) {
            $contrePartie = in_array(strtolower($contrePartie), array('false', 'off', '-', 'no', 'n', '0', '')) ? false : true;
        }

        return $this->addUsingAlias(AsscEcrituresArchiveTableMap::COL_CONTRE_PARTIE, $contrePartie, $comparison);
    }

    /**
     * Filter the query on the quantite column
     *
     * Example usage:
     * <code>
     * $query->filterByQuantite(1234); // WHERE quantite = 1234
     * $query->filterByQuantite(array(12, 34)); // WHERE quantite IN (12, 34)
     * $query->filterByQuantite(array('min' => 12)); // WHERE quantite > 12
     * </code>
     *
     * @param     mixed $quantite The value to use as filter.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildAsscEcrituresArchiveQuery The current query, for fluid interface
     */
    public function filterByQuantite($quantite = null, $comparison = null)
    {
        if (is_array($quantite)) {
            $useMinMax = false;
            if (isset($quantite['min'])) {
                $this->addUsingAlias(AsscEcrituresArchiveTableMap::COL_QUANTITE, $quantite['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($quantite['max'])) {
                $this->addUsingAlias(AsscEcrituresArchiveTableMap::COL_QUANTITE, $quantite['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(AsscEcrituresArchiveTableMap::COL_QUANTITE, $quantite, $comparison);
    }

    /**
     * Filter the query on the created_at column
     *
     * Example usage:
     * <code>
     * $query->filterByCreatedAt('2011-03-14'); // WHERE created_at = '2011-03-14'
     * $query->filterByCreatedAt('now'); // WHERE created_at = '2011-03-14'
     * $query->filterByCreatedAt(array('max' => 'yesterday')); // WHERE created_at > '2011-03-13'
     * </code>
     *
     * @param     mixed $createdAt The value to use as filter.
     *              Values can be integers (unix timestamps), DateTime objects, or strings.
     *              Empty strings are treated as NULL.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildAsscEcrituresArchiveQuery The current query, for fluid interface
     */
    public function filterByCreatedAt($createdAt = null, $comparison = null)
    {
        if (is_array($createdAt)) {
            $useMinMax = false;
            if (isset($createdAt['min'])) {
                $this->addUsingAlias(AsscEcrituresArchiveTableMap::COL_CREATED_AT, $createdAt['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($createdAt['max'])) {
                $this->addUsingAlias(AsscEcrituresArchiveTableMap::COL_CREATED_AT, $createdAt['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(AsscEcrituresArchiveTableMap::COL_CREATED_AT, $createdAt, $comparison);
    }

    /**
     * Filter the query on the updated_at column
     *
     * Example usage:
     * <code>
     * $query->filterByUpdatedAt('2011-03-14'); // WHERE updated_at = '2011-03-14'
     * $query->filterByUpdatedAt('now'); // WHERE updated_at = '2011-03-14'
     * $query->filterByUpdatedAt(array('max' => 'yesterday')); // WHERE updated_at > '2011-03-13'
     * </code>
     *
     * @param     mixed $updatedAt The value to use as filter.
     *              Values can be integers (unix timestamps), DateTime objects, or strings.
     *              Empty strings are treated as NULL.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildAsscEcrituresArchiveQuery The current query, for fluid interface
     */
    public function filterByUpdatedAt($updatedAt = null, $comparison = null)
    {
        if (is_array($updatedAt)) {
            $useMinMax = false;
            if (isset($updatedAt['min'])) {
                $this->addUsingAlias(AsscEcrituresArchiveTableMap::COL_UPDATED_AT, $updatedAt['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($updatedAt['max'])) {
                $this->addUsingAlias(AsscEcrituresArchiveTableMap::COL_UPDATED_AT, $updatedAt['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(AsscEcrituresArchiveTableMap::COL_UPDATED_AT, $updatedAt, $comparison);
    }

    /**
     * Filter the query on the archived_at column
     *
     * Example usage:
     * <code>
     * $query->filterByArchivedAt('2011-03-14'); // WHERE archived_at = '2011-03-14'
     * $query->filterByArchivedAt('now'); // WHERE archived_at = '2011-03-14'
     * $query->filterByArchivedAt(array('max' => 'yesterday')); // WHERE archived_at > '2011-03-13'
     * </code>
     *
     * @param     mixed $archivedAt The value to use as filter.
     *              Values can be integers (unix timestamps), DateTime objects, or strings.
     *              Empty strings are treated as NULL.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildAsscEcrituresArchiveQuery The current query, for fluid interface
     */
    public function filterByArchivedAt($archivedAt = null, $comparison = null)
    {
        if (is_array($archivedAt)) {
            $useMinMax = false;
            if (isset($archivedAt['min'])) {
                $this->addUsingAlias(AsscEcrituresArchiveTableMap::COL_ARCHIVED_AT, $archivedAt['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($archivedAt['max'])) {
                $this->addUsingAlias(AsscEcrituresArchiveTableMap::COL_ARCHIVED_AT, $archivedAt['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(AsscEcrituresArchiveTableMap::COL_ARCHIVED_AT, $archivedAt, $comparison);
    }

    /**
     * Exclude object from result
     *
     * @param   ChildAsscEcrituresArchive $asscEcrituresArchive Object to remove from the list of results
     *
     * @return $this|ChildAsscEcrituresArchiveQuery The current query, for fluid interface
     */
    public function prune($asscEcrituresArchive = null)
    {
        if ($asscEcrituresArchive) {
            $this->addUsingAlias(AsscEcrituresArchiveTableMap::COL_ID_ECRITURE, $asscEcrituresArchive->getIdEcriture(), Criteria::NOT_EQUAL);
        }

        return $this;
    }

    /**
     * Deletes all rows from the assc_ecritures_archive table.
     *
     * @param ConnectionInterface $con the connection to use
     * @return int The number of affected rows (if supported by underlying database driver).
     */
    public function doDeleteAll(ConnectionInterface $con = null)
    {
        if (null === $con) {
            $con = Propel::getServiceContainer()->getWriteConnection(AsscEcrituresArchiveTableMap::DATABASE_NAME);
        }

        // use transaction because $criteria could contain info
        // for more than one table or we could emulating ON DELETE CASCADE, etc.
        return $con->transaction(function () use ($con) {
            $affectedRows = 0; // initialize var to track total num of affected rows
            $affectedRows += parent::doDeleteAll($con);
            // Because this db requires some delete cascade/set null emulation, we have to
            // clear the cached instance *after* the emulation has happened (since
            // instances get re-added by the select statement contained therein).
            AsscEcrituresArchiveTableMap::clearInstancePool();
            AsscEcrituresArchiveTableMap::clearRelatedInstancePool();

            return $affectedRows;
        });
    }

    /**
     * Performs a DELETE on the database based on the current ModelCriteria
     *
     * @param ConnectionInterface $con the connection to use
     * @return int             The number of affected rows (if supported by underlying database driver).  This includes CASCADE-related rows
     *                         if supported by native driver or if emulated using Propel.
     * @throws PropelException Any exceptions caught during processing will be
     *                         rethrown wrapped into a PropelException.
     */
    public function delete(ConnectionInterface $con = null)
    {
        if (null === $con) {
            $con = Propel::getServiceContainer()->getWriteConnection(AsscEcrituresArchiveTableMap::DATABASE_NAME);
        }

        $criteria = $this;

        // Set the correct dbName
        $criteria->setDbName(AsscEcrituresArchiveTableMap::DATABASE_NAME);

        // use transaction because $criteria could contain info
        // for more than one table or we could emulating ON DELETE CASCADE, etc.
        return $con->transaction(function () use ($con, $criteria) {
            $affectedRows = 0; // initialize var to track total num of affected rows

            AsscEcrituresArchiveTableMap::removeInstanceFromPool($criteria);

            $affectedRows += ModelCriteria::delete($con);
            AsscEcrituresArchiveTableMap::clearRelatedInstancePool();

            return $affectedRows;
        });
    }

} // AsscEcrituresArchiveQuery
