<?php

namespace Base;

use \AssoIndividusArchive as ChildAssoIndividusArchive;
use \AssoIndividusArchiveQuery as ChildAssoIndividusArchiveQuery;
use \Exception;
use \PDO;
use Map\AssoIndividusArchiveTableMap;
use Propel\Runtime\Propel;
use Propel\Runtime\ActiveQuery\Criteria;
use Propel\Runtime\ActiveQuery\ModelCriteria;
use Propel\Runtime\Collection\ObjectCollection;
use Propel\Runtime\Connection\ConnectionInterface;
use Propel\Runtime\Exception\PropelException;

/**
 * Base class that represents a query for the 'asso_individus_archive' table.
 *
 *
 *
 * @method     ChildAssoIndividusArchiveQuery orderByIdIndividu($order = Criteria::ASC) Order by the id_individu column
 * @method     ChildAssoIndividusArchiveQuery orderByNom($order = Criteria::ASC) Order by the nom column
 * @method     ChildAssoIndividusArchiveQuery orderByBio($order = Criteria::ASC) Order by the bio column
 * @method     ChildAssoIndividusArchiveQuery orderByEmail($order = Criteria::ASC) Order by the email column
 * @method     ChildAssoIndividusArchiveQuery orderByLogin($order = Criteria::ASC) Order by the login column
 * @method     ChildAssoIndividusArchiveQuery orderByPass($order = Criteria::ASC) Order by the pass column
 * @method     ChildAssoIndividusArchiveQuery orderByAleaActuel($order = Criteria::ASC) Order by the alea_actuel column
 * @method     ChildAssoIndividusArchiveQuery orderByAleaFutur($order = Criteria::ASC) Order by the alea_futur column
 * @method     ChildAssoIndividusArchiveQuery orderByToken($order = Criteria::ASC) Order by the token column
 * @method     ChildAssoIndividusArchiveQuery orderByTokenTime($order = Criteria::ASC) Order by the token_time column
 * @method     ChildAssoIndividusArchiveQuery orderByCivilite($order = Criteria::ASC) Order by the civilite column
 * @method     ChildAssoIndividusArchiveQuery orderBySexe($order = Criteria::ASC) Order by the sexe column
 * @method     ChildAssoIndividusArchiveQuery orderByNomFamille($order = Criteria::ASC) Order by the nom_famille column
 * @method     ChildAssoIndividusArchiveQuery orderByPrenom($order = Criteria::ASC) Order by the prenom column
 * @method     ChildAssoIndividusArchiveQuery orderByNaissance($order = Criteria::ASC) Order by the naissance column
 * @method     ChildAssoIndividusArchiveQuery orderByAdresse($order = Criteria::ASC) Order by the adresse column
 * @method     ChildAssoIndividusArchiveQuery orderByCodepostal($order = Criteria::ASC) Order by the codepostal column
 * @method     ChildAssoIndividusArchiveQuery orderByVille($order = Criteria::ASC) Order by the ville column
 * @method     ChildAssoIndividusArchiveQuery orderByPays($order = Criteria::ASC) Order by the pays column
 * @method     ChildAssoIndividusArchiveQuery orderByTelephone($order = Criteria::ASC) Order by the telephone column
 * @method     ChildAssoIndividusArchiveQuery orderByTelephonePro($order = Criteria::ASC) Order by the telephone_pro column
 * @method     ChildAssoIndividusArchiveQuery orderByFax($order = Criteria::ASC) Order by the fax column
 * @method     ChildAssoIndividusArchiveQuery orderByMobile($order = Criteria::ASC) Order by the mobile column
 * @method     ChildAssoIndividusArchiveQuery orderByUrl($order = Criteria::ASC) Order by the url column
 * @method     ChildAssoIndividusArchiveQuery orderByProfession($order = Criteria::ASC) Order by the profession column
 * @method     ChildAssoIndividusArchiveQuery orderByContactSouhait($order = Criteria::ASC) Order by the contact_souhait column
 * @method     ChildAssoIndividusArchiveQuery orderByObservation($order = Criteria::ASC) Order by the observation column
 * @method     ChildAssoIndividusArchiveQuery orderByCreatedAt($order = Criteria::ASC) Order by the created_at column
 * @method     ChildAssoIndividusArchiveQuery orderByUpdatedAt($order = Criteria::ASC) Order by the updated_at column
 * @method     ChildAssoIndividusArchiveQuery orderByArchivedAt($order = Criteria::ASC) Order by the archived_at column
 *
 * @method     ChildAssoIndividusArchiveQuery groupByIdIndividu() Group by the id_individu column
 * @method     ChildAssoIndividusArchiveQuery groupByNom() Group by the nom column
 * @method     ChildAssoIndividusArchiveQuery groupByBio() Group by the bio column
 * @method     ChildAssoIndividusArchiveQuery groupByEmail() Group by the email column
 * @method     ChildAssoIndividusArchiveQuery groupByLogin() Group by the login column
 * @method     ChildAssoIndividusArchiveQuery groupByPass() Group by the pass column
 * @method     ChildAssoIndividusArchiveQuery groupByAleaActuel() Group by the alea_actuel column
 * @method     ChildAssoIndividusArchiveQuery groupByAleaFutur() Group by the alea_futur column
 * @method     ChildAssoIndividusArchiveQuery groupByToken() Group by the token column
 * @method     ChildAssoIndividusArchiveQuery groupByTokenTime() Group by the token_time column
 * @method     ChildAssoIndividusArchiveQuery groupByCivilite() Group by the civilite column
 * @method     ChildAssoIndividusArchiveQuery groupBySexe() Group by the sexe column
 * @method     ChildAssoIndividusArchiveQuery groupByNomFamille() Group by the nom_famille column
 * @method     ChildAssoIndividusArchiveQuery groupByPrenom() Group by the prenom column
 * @method     ChildAssoIndividusArchiveQuery groupByNaissance() Group by the naissance column
 * @method     ChildAssoIndividusArchiveQuery groupByAdresse() Group by the adresse column
 * @method     ChildAssoIndividusArchiveQuery groupByCodepostal() Group by the codepostal column
 * @method     ChildAssoIndividusArchiveQuery groupByVille() Group by the ville column
 * @method     ChildAssoIndividusArchiveQuery groupByPays() Group by the pays column
 * @method     ChildAssoIndividusArchiveQuery groupByTelephone() Group by the telephone column
 * @method     ChildAssoIndividusArchiveQuery groupByTelephonePro() Group by the telephone_pro column
 * @method     ChildAssoIndividusArchiveQuery groupByFax() Group by the fax column
 * @method     ChildAssoIndividusArchiveQuery groupByMobile() Group by the mobile column
 * @method     ChildAssoIndividusArchiveQuery groupByUrl() Group by the url column
 * @method     ChildAssoIndividusArchiveQuery groupByProfession() Group by the profession column
 * @method     ChildAssoIndividusArchiveQuery groupByContactSouhait() Group by the contact_souhait column
 * @method     ChildAssoIndividusArchiveQuery groupByObservation() Group by the observation column
 * @method     ChildAssoIndividusArchiveQuery groupByCreatedAt() Group by the created_at column
 * @method     ChildAssoIndividusArchiveQuery groupByUpdatedAt() Group by the updated_at column
 * @method     ChildAssoIndividusArchiveQuery groupByArchivedAt() Group by the archived_at column
 *
 * @method     ChildAssoIndividusArchiveQuery leftJoin($relation) Adds a LEFT JOIN clause to the query
 * @method     ChildAssoIndividusArchiveQuery rightJoin($relation) Adds a RIGHT JOIN clause to the query
 * @method     ChildAssoIndividusArchiveQuery innerJoin($relation) Adds a INNER JOIN clause to the query
 *
 * @method     ChildAssoIndividusArchiveQuery leftJoinWith($relation) Adds a LEFT JOIN clause and with to the query
 * @method     ChildAssoIndividusArchiveQuery rightJoinWith($relation) Adds a RIGHT JOIN clause and with to the query
 * @method     ChildAssoIndividusArchiveQuery innerJoinWith($relation) Adds a INNER JOIN clause and with to the query
 *
 * @method     ChildAssoIndividusArchive findOne(ConnectionInterface $con = null) Return the first ChildAssoIndividusArchive matching the query
 * @method     ChildAssoIndividusArchive findOneOrCreate(ConnectionInterface $con = null) Return the first ChildAssoIndividusArchive matching the query, or a new ChildAssoIndividusArchive object populated from the query conditions when no match is found
 *
 * @method     ChildAssoIndividusArchive findOneByIdIndividu(string $id_individu) Return the first ChildAssoIndividusArchive filtered by the id_individu column
 * @method     ChildAssoIndividusArchive findOneByNom(string $nom) Return the first ChildAssoIndividusArchive filtered by the nom column
 * @method     ChildAssoIndividusArchive findOneByBio(string $bio) Return the first ChildAssoIndividusArchive filtered by the bio column
 * @method     ChildAssoIndividusArchive findOneByEmail(string $email) Return the first ChildAssoIndividusArchive filtered by the email column
 * @method     ChildAssoIndividusArchive findOneByLogin(string $login) Return the first ChildAssoIndividusArchive filtered by the login column
 * @method     ChildAssoIndividusArchive findOneByPass(string $pass) Return the first ChildAssoIndividusArchive filtered by the pass column
 * @method     ChildAssoIndividusArchive findOneByAleaActuel(string $alea_actuel) Return the first ChildAssoIndividusArchive filtered by the alea_actuel column
 * @method     ChildAssoIndividusArchive findOneByAleaFutur(string $alea_futur) Return the first ChildAssoIndividusArchive filtered by the alea_futur column
 * @method     ChildAssoIndividusArchive findOneByToken(string $token) Return the first ChildAssoIndividusArchive filtered by the token column
 * @method     ChildAssoIndividusArchive findOneByTokenTime(string $token_time) Return the first ChildAssoIndividusArchive filtered by the token_time column
 * @method     ChildAssoIndividusArchive findOneByCivilite(string $civilite) Return the first ChildAssoIndividusArchive filtered by the civilite column
 * @method     ChildAssoIndividusArchive findOneBySexe(string $sexe) Return the first ChildAssoIndividusArchive filtered by the sexe column
 * @method     ChildAssoIndividusArchive findOneByNomFamille(string $nom_famille) Return the first ChildAssoIndividusArchive filtered by the nom_famille column
 * @method     ChildAssoIndividusArchive findOneByPrenom(string $prenom) Return the first ChildAssoIndividusArchive filtered by the prenom column
 * @method     ChildAssoIndividusArchive findOneByNaissance(string $naissance) Return the first ChildAssoIndividusArchive filtered by the naissance column
 * @method     ChildAssoIndividusArchive findOneByAdresse(string $adresse) Return the first ChildAssoIndividusArchive filtered by the adresse column
 * @method     ChildAssoIndividusArchive findOneByCodepostal(string $codepostal) Return the first ChildAssoIndividusArchive filtered by the codepostal column
 * @method     ChildAssoIndividusArchive findOneByVille(string $ville) Return the first ChildAssoIndividusArchive filtered by the ville column
 * @method     ChildAssoIndividusArchive findOneByPays(string $pays) Return the first ChildAssoIndividusArchive filtered by the pays column
 * @method     ChildAssoIndividusArchive findOneByTelephone(string $telephone) Return the first ChildAssoIndividusArchive filtered by the telephone column
 * @method     ChildAssoIndividusArchive findOneByTelephonePro(string $telephone_pro) Return the first ChildAssoIndividusArchive filtered by the telephone_pro column
 * @method     ChildAssoIndividusArchive findOneByFax(string $fax) Return the first ChildAssoIndividusArchive filtered by the fax column
 * @method     ChildAssoIndividusArchive findOneByMobile(string $mobile) Return the first ChildAssoIndividusArchive filtered by the mobile column
 * @method     ChildAssoIndividusArchive findOneByUrl(string $url) Return the first ChildAssoIndividusArchive filtered by the url column
 * @method     ChildAssoIndividusArchive findOneByProfession(string $profession) Return the first ChildAssoIndividusArchive filtered by the profession column
 * @method     ChildAssoIndividusArchive findOneByContactSouhait(boolean $contact_souhait) Return the first ChildAssoIndividusArchive filtered by the contact_souhait column
 * @method     ChildAssoIndividusArchive findOneByObservation(string $observation) Return the first ChildAssoIndividusArchive filtered by the observation column
 * @method     ChildAssoIndividusArchive findOneByCreatedAt(string $created_at) Return the first ChildAssoIndividusArchive filtered by the created_at column
 * @method     ChildAssoIndividusArchive findOneByUpdatedAt(string $updated_at) Return the first ChildAssoIndividusArchive filtered by the updated_at column
 * @method     ChildAssoIndividusArchive findOneByArchivedAt(string $archived_at) Return the first ChildAssoIndividusArchive filtered by the archived_at column *

 * @method     ChildAssoIndividusArchive requirePk($key, ConnectionInterface $con = null) Return the ChildAssoIndividusArchive by primary key and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildAssoIndividusArchive requireOne(ConnectionInterface $con = null) Return the first ChildAssoIndividusArchive matching the query and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 *
 * @method     ChildAssoIndividusArchive requireOneByIdIndividu(string $id_individu) Return the first ChildAssoIndividusArchive filtered by the id_individu column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildAssoIndividusArchive requireOneByNom(string $nom) Return the first ChildAssoIndividusArchive filtered by the nom column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildAssoIndividusArchive requireOneByBio(string $bio) Return the first ChildAssoIndividusArchive filtered by the bio column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildAssoIndividusArchive requireOneByEmail(string $email) Return the first ChildAssoIndividusArchive filtered by the email column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildAssoIndividusArchive requireOneByLogin(string $login) Return the first ChildAssoIndividusArchive filtered by the login column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildAssoIndividusArchive requireOneByPass(string $pass) Return the first ChildAssoIndividusArchive filtered by the pass column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildAssoIndividusArchive requireOneByAleaActuel(string $alea_actuel) Return the first ChildAssoIndividusArchive filtered by the alea_actuel column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildAssoIndividusArchive requireOneByAleaFutur(string $alea_futur) Return the first ChildAssoIndividusArchive filtered by the alea_futur column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildAssoIndividusArchive requireOneByToken(string $token) Return the first ChildAssoIndividusArchive filtered by the token column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildAssoIndividusArchive requireOneByTokenTime(string $token_time) Return the first ChildAssoIndividusArchive filtered by the token_time column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildAssoIndividusArchive requireOneByCivilite(string $civilite) Return the first ChildAssoIndividusArchive filtered by the civilite column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildAssoIndividusArchive requireOneBySexe(string $sexe) Return the first ChildAssoIndividusArchive filtered by the sexe column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildAssoIndividusArchive requireOneByNomFamille(string $nom_famille) Return the first ChildAssoIndividusArchive filtered by the nom_famille column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildAssoIndividusArchive requireOneByPrenom(string $prenom) Return the first ChildAssoIndividusArchive filtered by the prenom column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildAssoIndividusArchive requireOneByNaissance(string $naissance) Return the first ChildAssoIndividusArchive filtered by the naissance column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildAssoIndividusArchive requireOneByAdresse(string $adresse) Return the first ChildAssoIndividusArchive filtered by the adresse column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildAssoIndividusArchive requireOneByCodepostal(string $codepostal) Return the first ChildAssoIndividusArchive filtered by the codepostal column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildAssoIndividusArchive requireOneByVille(string $ville) Return the first ChildAssoIndividusArchive filtered by the ville column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildAssoIndividusArchive requireOneByPays(string $pays) Return the first ChildAssoIndividusArchive filtered by the pays column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildAssoIndividusArchive requireOneByTelephone(string $telephone) Return the first ChildAssoIndividusArchive filtered by the telephone column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildAssoIndividusArchive requireOneByTelephonePro(string $telephone_pro) Return the first ChildAssoIndividusArchive filtered by the telephone_pro column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildAssoIndividusArchive requireOneByFax(string $fax) Return the first ChildAssoIndividusArchive filtered by the fax column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildAssoIndividusArchive requireOneByMobile(string $mobile) Return the first ChildAssoIndividusArchive filtered by the mobile column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildAssoIndividusArchive requireOneByUrl(string $url) Return the first ChildAssoIndividusArchive filtered by the url column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildAssoIndividusArchive requireOneByProfession(string $profession) Return the first ChildAssoIndividusArchive filtered by the profession column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildAssoIndividusArchive requireOneByContactSouhait(boolean $contact_souhait) Return the first ChildAssoIndividusArchive filtered by the contact_souhait column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildAssoIndividusArchive requireOneByObservation(string $observation) Return the first ChildAssoIndividusArchive filtered by the observation column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildAssoIndividusArchive requireOneByCreatedAt(string $created_at) Return the first ChildAssoIndividusArchive filtered by the created_at column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildAssoIndividusArchive requireOneByUpdatedAt(string $updated_at) Return the first ChildAssoIndividusArchive filtered by the updated_at column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildAssoIndividusArchive requireOneByArchivedAt(string $archived_at) Return the first ChildAssoIndividusArchive filtered by the archived_at column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 *
 * @method     ChildAssoIndividusArchive[]|ObjectCollection find(ConnectionInterface $con = null) Return ChildAssoIndividusArchive objects based on current ModelCriteria
 * @method     ChildAssoIndividusArchive[]|ObjectCollection findByIdIndividu(string $id_individu) Return ChildAssoIndividusArchive objects filtered by the id_individu column
 * @method     ChildAssoIndividusArchive[]|ObjectCollection findByNom(string $nom) Return ChildAssoIndividusArchive objects filtered by the nom column
 * @method     ChildAssoIndividusArchive[]|ObjectCollection findByBio(string $bio) Return ChildAssoIndividusArchive objects filtered by the bio column
 * @method     ChildAssoIndividusArchive[]|ObjectCollection findByEmail(string $email) Return ChildAssoIndividusArchive objects filtered by the email column
 * @method     ChildAssoIndividusArchive[]|ObjectCollection findByLogin(string $login) Return ChildAssoIndividusArchive objects filtered by the login column
 * @method     ChildAssoIndividusArchive[]|ObjectCollection findByPass(string $pass) Return ChildAssoIndividusArchive objects filtered by the pass column
 * @method     ChildAssoIndividusArchive[]|ObjectCollection findByAleaActuel(string $alea_actuel) Return ChildAssoIndividusArchive objects filtered by the alea_actuel column
 * @method     ChildAssoIndividusArchive[]|ObjectCollection findByAleaFutur(string $alea_futur) Return ChildAssoIndividusArchive objects filtered by the alea_futur column
 * @method     ChildAssoIndividusArchive[]|ObjectCollection findByToken(string $token) Return ChildAssoIndividusArchive objects filtered by the token column
 * @method     ChildAssoIndividusArchive[]|ObjectCollection findByTokenTime(string $token_time) Return ChildAssoIndividusArchive objects filtered by the token_time column
 * @method     ChildAssoIndividusArchive[]|ObjectCollection findByCivilite(string $civilite) Return ChildAssoIndividusArchive objects filtered by the civilite column
 * @method     ChildAssoIndividusArchive[]|ObjectCollection findBySexe(string $sexe) Return ChildAssoIndividusArchive objects filtered by the sexe column
 * @method     ChildAssoIndividusArchive[]|ObjectCollection findByNomFamille(string $nom_famille) Return ChildAssoIndividusArchive objects filtered by the nom_famille column
 * @method     ChildAssoIndividusArchive[]|ObjectCollection findByPrenom(string $prenom) Return ChildAssoIndividusArchive objects filtered by the prenom column
 * @method     ChildAssoIndividusArchive[]|ObjectCollection findByNaissance(string $naissance) Return ChildAssoIndividusArchive objects filtered by the naissance column
 * @method     ChildAssoIndividusArchive[]|ObjectCollection findByAdresse(string $adresse) Return ChildAssoIndividusArchive objects filtered by the adresse column
 * @method     ChildAssoIndividusArchive[]|ObjectCollection findByCodepostal(string $codepostal) Return ChildAssoIndividusArchive objects filtered by the codepostal column
 * @method     ChildAssoIndividusArchive[]|ObjectCollection findByVille(string $ville) Return ChildAssoIndividusArchive objects filtered by the ville column
 * @method     ChildAssoIndividusArchive[]|ObjectCollection findByPays(string $pays) Return ChildAssoIndividusArchive objects filtered by the pays column
 * @method     ChildAssoIndividusArchive[]|ObjectCollection findByTelephone(string $telephone) Return ChildAssoIndividusArchive objects filtered by the telephone column
 * @method     ChildAssoIndividusArchive[]|ObjectCollection findByTelephonePro(string $telephone_pro) Return ChildAssoIndividusArchive objects filtered by the telephone_pro column
 * @method     ChildAssoIndividusArchive[]|ObjectCollection findByFax(string $fax) Return ChildAssoIndividusArchive objects filtered by the fax column
 * @method     ChildAssoIndividusArchive[]|ObjectCollection findByMobile(string $mobile) Return ChildAssoIndividusArchive objects filtered by the mobile column
 * @method     ChildAssoIndividusArchive[]|ObjectCollection findByUrl(string $url) Return ChildAssoIndividusArchive objects filtered by the url column
 * @method     ChildAssoIndividusArchive[]|ObjectCollection findByProfession(string $profession) Return ChildAssoIndividusArchive objects filtered by the profession column
 * @method     ChildAssoIndividusArchive[]|ObjectCollection findByContactSouhait(boolean $contact_souhait) Return ChildAssoIndividusArchive objects filtered by the contact_souhait column
 * @method     ChildAssoIndividusArchive[]|ObjectCollection findByObservation(string $observation) Return ChildAssoIndividusArchive objects filtered by the observation column
 * @method     ChildAssoIndividusArchive[]|ObjectCollection findByCreatedAt(string $created_at) Return ChildAssoIndividusArchive objects filtered by the created_at column
 * @method     ChildAssoIndividusArchive[]|ObjectCollection findByUpdatedAt(string $updated_at) Return ChildAssoIndividusArchive objects filtered by the updated_at column
 * @method     ChildAssoIndividusArchive[]|ObjectCollection findByArchivedAt(string $archived_at) Return ChildAssoIndividusArchive objects filtered by the archived_at column
 * @method     ChildAssoIndividusArchive[]|\Propel\Runtime\Util\PropelModelPager paginate($page = 1, $maxPerPage = 10, ConnectionInterface $con = null) Issue a SELECT query based on the current ModelCriteria and uses a page and a maximum number of results per page to compute an offset and a limit
 *
 */
abstract class AssoIndividusArchiveQuery extends ModelCriteria
{
    protected $entityNotFoundExceptionClass = '\\Propel\\Runtime\\Exception\\EntityNotFoundException';

    /**
     * Initializes internal state of \Base\AssoIndividusArchiveQuery object.
     *
     * @param     string $dbName The database name
     * @param     string $modelName The phpName of a model, e.g. 'Book'
     * @param     string $modelAlias The alias for the model in this query, e.g. 'b'
     */
    public function __construct($dbName = 'simplasso', $modelName = '\\AssoIndividusArchive', $modelAlias = null)
    {
        parent::__construct($dbName, $modelName, $modelAlias);
    }

    /**
     * Returns a new ChildAssoIndividusArchiveQuery object.
     *
     * @param     string $modelAlias The alias of a model in the query
     * @param     Criteria $criteria Optional Criteria to build the query from
     *
     * @return ChildAssoIndividusArchiveQuery
     */
    public static function create($modelAlias = null, Criteria $criteria = null)
    {
        if ($criteria instanceof ChildAssoIndividusArchiveQuery) {
            return $criteria;
        }
        $query = new ChildAssoIndividusArchiveQuery();
        if (null !== $modelAlias) {
            $query->setModelAlias($modelAlias);
        }
        if ($criteria instanceof Criteria) {
            $query->mergeWith($criteria);
        }

        return $query;
    }

    /**
     * Find object by primary key.
     * Propel uses the instance pool to skip the database if the object exists.
     * Go fast if the query is untouched.
     *
     * <code>
     * $obj  = $c->findPk(12, $con);
     * </code>
     *
     * @param mixed $key Primary key to use for the query
     * @param ConnectionInterface $con an optional connection object
     *
     * @return ChildAssoIndividusArchive|array|mixed the result, formatted by the current formatter
     */
    public function findPk($key, ConnectionInterface $con = null)
    {
        if ($key === null) {
            return null;
        }

        if ($con === null) {
            $con = Propel::getServiceContainer()->getReadConnection(AssoIndividusArchiveTableMap::DATABASE_NAME);
        }

        $this->basePreSelect($con);

        if (
            $this->formatter || $this->modelAlias || $this->with || $this->select
            || $this->selectColumns || $this->asColumns || $this->selectModifiers
            || $this->map || $this->having || $this->joins
        ) {
            return $this->findPkComplex($key, $con);
        }

        if ((null !== ($obj = AssoIndividusArchiveTableMap::getInstanceFromPool(null === $key || is_scalar($key) || is_callable([$key, '__toString']) ? (string) $key : $key)))) {
            // the object is already in the instance pool
            return $obj;
        }

        return $this->findPkSimple($key, $con);
    }

    /**
     * Find object by primary key using raw SQL to go fast.
     * Bypass doSelect() and the object formatter by using generated code.
     *
     * @param     mixed $key Primary key to use for the query
     * @param     ConnectionInterface $con A connection object
     *
     * @throws \Propel\Runtime\Exception\PropelException
     *
     * @return ChildAssoIndividusArchive A model object, or null if the key is not found
     */
    protected function findPkSimple($key, ConnectionInterface $con)
    {
        $sql = 'SELECT `id_individu`, `nom`, `bio`, `email`, `login`, `pass`, `alea_actuel`, `alea_futur`, `token`, `token_time`, `civilite`, `sexe`, `nom_famille`, `prenom`, `naissance`, `adresse`, `codepostal`, `ville`, `pays`, `telephone`, `telephone_pro`, `fax`, `mobile`, `url`, `profession`, `contact_souhait`, `observation`, `created_at`, `updated_at`, `archived_at` FROM `asso_individus_archive` WHERE `id_individu` = :p0';
        try {
            $stmt = $con->prepare($sql);
            $stmt->bindValue(':p0', $key, PDO::PARAM_INT);
            $stmt->execute();
        } catch (Exception $e) {
            Propel::log($e->getMessage(), Propel::LOG_ERR);
            throw new PropelException(sprintf('Unable to execute SELECT statement [%s]', $sql), 0, $e);
        }
        $obj = null;
        if ($row = $stmt->fetch(\PDO::FETCH_NUM)) {
            /** @var ChildAssoIndividusArchive $obj */
            $obj = new ChildAssoIndividusArchive();
            $obj->hydrate($row);
            AssoIndividusArchiveTableMap::addInstanceToPool($obj, null === $key || is_scalar($key) || is_callable([$key, '__toString']) ? (string) $key : $key);
        }
        $stmt->closeCursor();

        return $obj;
    }

    /**
     * Find object by primary key.
     *
     * @param     mixed $key Primary key to use for the query
     * @param     ConnectionInterface $con A connection object
     *
     * @return ChildAssoIndividusArchive|array|mixed the result, formatted by the current formatter
     */
    protected function findPkComplex($key, ConnectionInterface $con)
    {
        // As the query uses a PK condition, no limit(1) is necessary.
        $criteria = $this->isKeepQuery() ? clone $this : $this;
        $dataFetcher = $criteria
            ->filterByPrimaryKey($key)
            ->doSelect($con);

        return $criteria->getFormatter()->init($criteria)->formatOne($dataFetcher);
    }

    /**
     * Find objects by primary key
     * <code>
     * $objs = $c->findPks(array(12, 56, 832), $con);
     * </code>
     * @param     array $keys Primary keys to use for the query
     * @param     ConnectionInterface $con an optional connection object
     *
     * @return ObjectCollection|array|mixed the list of results, formatted by the current formatter
     */
    public function findPks($keys, ConnectionInterface $con = null)
    {
        if (null === $con) {
            $con = Propel::getServiceContainer()->getReadConnection($this->getDbName());
        }
        $this->basePreSelect($con);
        $criteria = $this->isKeepQuery() ? clone $this : $this;
        $dataFetcher = $criteria
            ->filterByPrimaryKeys($keys)
            ->doSelect($con);

        return $criteria->getFormatter()->init($criteria)->format($dataFetcher);
    }

    /**
     * Filter the query by primary key
     *
     * @param     mixed $key Primary key to use for the query
     *
     * @return $this|ChildAssoIndividusArchiveQuery The current query, for fluid interface
     */
    public function filterByPrimaryKey($key)
    {

        return $this->addUsingAlias(AssoIndividusArchiveTableMap::COL_ID_INDIVIDU, $key, Criteria::EQUAL);
    }

    /**
     * Filter the query by a list of primary keys
     *
     * @param     array $keys The list of primary key to use for the query
     *
     * @return $this|ChildAssoIndividusArchiveQuery The current query, for fluid interface
     */
    public function filterByPrimaryKeys($keys)
    {

        return $this->addUsingAlias(AssoIndividusArchiveTableMap::COL_ID_INDIVIDU, $keys, Criteria::IN);
    }

    /**
     * Filter the query on the id_individu column
     *
     * Example usage:
     * <code>
     * $query->filterByIdIndividu(1234); // WHERE id_individu = 1234
     * $query->filterByIdIndividu(array(12, 34)); // WHERE id_individu IN (12, 34)
     * $query->filterByIdIndividu(array('min' => 12)); // WHERE id_individu > 12
     * </code>
     *
     * @param     mixed $idIndividu The value to use as filter.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildAssoIndividusArchiveQuery The current query, for fluid interface
     */
    public function filterByIdIndividu($idIndividu = null, $comparison = null)
    {
        if (is_array($idIndividu)) {
            $useMinMax = false;
            if (isset($idIndividu['min'])) {
                $this->addUsingAlias(AssoIndividusArchiveTableMap::COL_ID_INDIVIDU, $idIndividu['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($idIndividu['max'])) {
                $this->addUsingAlias(AssoIndividusArchiveTableMap::COL_ID_INDIVIDU, $idIndividu['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(AssoIndividusArchiveTableMap::COL_ID_INDIVIDU, $idIndividu, $comparison);
    }

    /**
     * Filter the query on the nom column
     *
     * Example usage:
     * <code>
     * $query->filterByNom('fooValue');   // WHERE nom = 'fooValue'
     * $query->filterByNom('%fooValue%', Criteria::LIKE); // WHERE nom LIKE '%fooValue%'
     * </code>
     *
     * @param     string $nom The value to use as filter.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildAssoIndividusArchiveQuery The current query, for fluid interface
     */
    public function filterByNom($nom = null, $comparison = null)
    {
        if (null === $comparison) {
            if (is_array($nom)) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(AssoIndividusArchiveTableMap::COL_NOM, $nom, $comparison);
    }

    /**
     * Filter the query on the bio column
     *
     * Example usage:
     * <code>
     * $query->filterByBio('fooValue');   // WHERE bio = 'fooValue'
     * $query->filterByBio('%fooValue%', Criteria::LIKE); // WHERE bio LIKE '%fooValue%'
     * </code>
     *
     * @param     string $bio The value to use as filter.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildAssoIndividusArchiveQuery The current query, for fluid interface
     */
    public function filterByBio($bio = null, $comparison = null)
    {
        if (null === $comparison) {
            if (is_array($bio)) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(AssoIndividusArchiveTableMap::COL_BIO, $bio, $comparison);
    }

    /**
     * Filter the query on the email column
     *
     * Example usage:
     * <code>
     * $query->filterByEmail('fooValue');   // WHERE email = 'fooValue'
     * $query->filterByEmail('%fooValue%', Criteria::LIKE); // WHERE email LIKE '%fooValue%'
     * </code>
     *
     * @param     string $email The value to use as filter.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildAssoIndividusArchiveQuery The current query, for fluid interface
     */
    public function filterByEmail($email = null, $comparison = null)
    {
        if (null === $comparison) {
            if (is_array($email)) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(AssoIndividusArchiveTableMap::COL_EMAIL, $email, $comparison);
    }

    /**
     * Filter the query on the login column
     *
     * Example usage:
     * <code>
     * $query->filterByLogin('fooValue');   // WHERE login = 'fooValue'
     * $query->filterByLogin('%fooValue%', Criteria::LIKE); // WHERE login LIKE '%fooValue%'
     * </code>
     *
     * @param     string $login The value to use as filter.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildAssoIndividusArchiveQuery The current query, for fluid interface
     */
    public function filterByLogin($login = null, $comparison = null)
    {
        if (null === $comparison) {
            if (is_array($login)) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(AssoIndividusArchiveTableMap::COL_LOGIN, $login, $comparison);
    }

    /**
     * Filter the query on the pass column
     *
     * Example usage:
     * <code>
     * $query->filterByPass('fooValue');   // WHERE pass = 'fooValue'
     * $query->filterByPass('%fooValue%', Criteria::LIKE); // WHERE pass LIKE '%fooValue%'
     * </code>
     *
     * @param     string $pass The value to use as filter.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildAssoIndividusArchiveQuery The current query, for fluid interface
     */
    public function filterByPass($pass = null, $comparison = null)
    {
        if (null === $comparison) {
            if (is_array($pass)) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(AssoIndividusArchiveTableMap::COL_PASS, $pass, $comparison);
    }

    /**
     * Filter the query on the alea_actuel column
     *
     * Example usage:
     * <code>
     * $query->filterByAleaActuel('fooValue');   // WHERE alea_actuel = 'fooValue'
     * $query->filterByAleaActuel('%fooValue%', Criteria::LIKE); // WHERE alea_actuel LIKE '%fooValue%'
     * </code>
     *
     * @param     string $aleaActuel The value to use as filter.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildAssoIndividusArchiveQuery The current query, for fluid interface
     */
    public function filterByAleaActuel($aleaActuel = null, $comparison = null)
    {
        if (null === $comparison) {
            if (is_array($aleaActuel)) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(AssoIndividusArchiveTableMap::COL_ALEA_ACTUEL, $aleaActuel, $comparison);
    }

    /**
     * Filter the query on the alea_futur column
     *
     * Example usage:
     * <code>
     * $query->filterByAleaFutur('fooValue');   // WHERE alea_futur = 'fooValue'
     * $query->filterByAleaFutur('%fooValue%', Criteria::LIKE); // WHERE alea_futur LIKE '%fooValue%'
     * </code>
     *
     * @param     string $aleaFutur The value to use as filter.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildAssoIndividusArchiveQuery The current query, for fluid interface
     */
    public function filterByAleaFutur($aleaFutur = null, $comparison = null)
    {
        if (null === $comparison) {
            if (is_array($aleaFutur)) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(AssoIndividusArchiveTableMap::COL_ALEA_FUTUR, $aleaFutur, $comparison);
    }

    /**
     * Filter the query on the token column
     *
     * Example usage:
     * <code>
     * $query->filterByToken('fooValue');   // WHERE token = 'fooValue'
     * $query->filterByToken('%fooValue%', Criteria::LIKE); // WHERE token LIKE '%fooValue%'
     * </code>
     *
     * @param     string $token The value to use as filter.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildAssoIndividusArchiveQuery The current query, for fluid interface
     */
    public function filterByToken($token = null, $comparison = null)
    {
        if (null === $comparison) {
            if (is_array($token)) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(AssoIndividusArchiveTableMap::COL_TOKEN, $token, $comparison);
    }

    /**
     * Filter the query on the token_time column
     *
     * Example usage:
     * <code>
     * $query->filterByTokenTime(1234); // WHERE token_time = 1234
     * $query->filterByTokenTime(array(12, 34)); // WHERE token_time IN (12, 34)
     * $query->filterByTokenTime(array('min' => 12)); // WHERE token_time > 12
     * </code>
     *
     * @param     mixed $tokenTime The value to use as filter.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildAssoIndividusArchiveQuery The current query, for fluid interface
     */
    public function filterByTokenTime($tokenTime = null, $comparison = null)
    {
        if (is_array($tokenTime)) {
            $useMinMax = false;
            if (isset($tokenTime['min'])) {
                $this->addUsingAlias(AssoIndividusArchiveTableMap::COL_TOKEN_TIME, $tokenTime['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($tokenTime['max'])) {
                $this->addUsingAlias(AssoIndividusArchiveTableMap::COL_TOKEN_TIME, $tokenTime['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(AssoIndividusArchiveTableMap::COL_TOKEN_TIME, $tokenTime, $comparison);
    }

    /**
     * Filter the query on the civilite column
     *
     * Example usage:
     * <code>
     * $query->filterByCivilite('fooValue');   // WHERE civilite = 'fooValue'
     * $query->filterByCivilite('%fooValue%', Criteria::LIKE); // WHERE civilite LIKE '%fooValue%'
     * </code>
     *
     * @param     string $civilite The value to use as filter.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildAssoIndividusArchiveQuery The current query, for fluid interface
     */
    public function filterByCivilite($civilite = null, $comparison = null)
    {
        if (null === $comparison) {
            if (is_array($civilite)) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(AssoIndividusArchiveTableMap::COL_CIVILITE, $civilite, $comparison);
    }

    /**
     * Filter the query on the sexe column
     *
     * Example usage:
     * <code>
     * $query->filterBySexe('fooValue');   // WHERE sexe = 'fooValue'
     * $query->filterBySexe('%fooValue%', Criteria::LIKE); // WHERE sexe LIKE '%fooValue%'
     * </code>
     *
     * @param     string $sexe The value to use as filter.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildAssoIndividusArchiveQuery The current query, for fluid interface
     */
    public function filterBySexe($sexe = null, $comparison = null)
    {
        if (null === $comparison) {
            if (is_array($sexe)) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(AssoIndividusArchiveTableMap::COL_SEXE, $sexe, $comparison);
    }

    /**
     * Filter the query on the nom_famille column
     *
     * Example usage:
     * <code>
     * $query->filterByNomFamille('fooValue');   // WHERE nom_famille = 'fooValue'
     * $query->filterByNomFamille('%fooValue%', Criteria::LIKE); // WHERE nom_famille LIKE '%fooValue%'
     * </code>
     *
     * @param     string $nomFamille The value to use as filter.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildAssoIndividusArchiveQuery The current query, for fluid interface
     */
    public function filterByNomFamille($nomFamille = null, $comparison = null)
    {
        if (null === $comparison) {
            if (is_array($nomFamille)) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(AssoIndividusArchiveTableMap::COL_NOM_FAMILLE, $nomFamille, $comparison);
    }

    /**
     * Filter the query on the prenom column
     *
     * Example usage:
     * <code>
     * $query->filterByPrenom('fooValue');   // WHERE prenom = 'fooValue'
     * $query->filterByPrenom('%fooValue%', Criteria::LIKE); // WHERE prenom LIKE '%fooValue%'
     * </code>
     *
     * @param     string $prenom The value to use as filter.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildAssoIndividusArchiveQuery The current query, for fluid interface
     */
    public function filterByPrenom($prenom = null, $comparison = null)
    {
        if (null === $comparison) {
            if (is_array($prenom)) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(AssoIndividusArchiveTableMap::COL_PRENOM, $prenom, $comparison);
    }

    /**
     * Filter the query on the naissance column
     *
     * Example usage:
     * <code>
     * $query->filterByNaissance('2011-03-14'); // WHERE naissance = '2011-03-14'
     * $query->filterByNaissance('now'); // WHERE naissance = '2011-03-14'
     * $query->filterByNaissance(array('max' => 'yesterday')); // WHERE naissance > '2011-03-13'
     * </code>
     *
     * @param     mixed $naissance The value to use as filter.
     *              Values can be integers (unix timestamps), DateTime objects, or strings.
     *              Empty strings are treated as NULL.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildAssoIndividusArchiveQuery The current query, for fluid interface
     */
    public function filterByNaissance($naissance = null, $comparison = null)
    {
        if (is_array($naissance)) {
            $useMinMax = false;
            if (isset($naissance['min'])) {
                $this->addUsingAlias(AssoIndividusArchiveTableMap::COL_NAISSANCE, $naissance['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($naissance['max'])) {
                $this->addUsingAlias(AssoIndividusArchiveTableMap::COL_NAISSANCE, $naissance['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(AssoIndividusArchiveTableMap::COL_NAISSANCE, $naissance, $comparison);
    }

    /**
     * Filter the query on the adresse column
     *
     * Example usage:
     * <code>
     * $query->filterByAdresse('fooValue');   // WHERE adresse = 'fooValue'
     * $query->filterByAdresse('%fooValue%', Criteria::LIKE); // WHERE adresse LIKE '%fooValue%'
     * </code>
     *
     * @param     string $adresse The value to use as filter.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildAssoIndividusArchiveQuery The current query, for fluid interface
     */
    public function filterByAdresse($adresse = null, $comparison = null)
    {
        if (null === $comparison) {
            if (is_array($adresse)) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(AssoIndividusArchiveTableMap::COL_ADRESSE, $adresse, $comparison);
    }

    /**
     * Filter the query on the codepostal column
     *
     * Example usage:
     * <code>
     * $query->filterByCodepostal('fooValue');   // WHERE codepostal = 'fooValue'
     * $query->filterByCodepostal('%fooValue%', Criteria::LIKE); // WHERE codepostal LIKE '%fooValue%'
     * </code>
     *
     * @param     string $codepostal The value to use as filter.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildAssoIndividusArchiveQuery The current query, for fluid interface
     */
    public function filterByCodepostal($codepostal = null, $comparison = null)
    {
        if (null === $comparison) {
            if (is_array($codepostal)) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(AssoIndividusArchiveTableMap::COL_CODEPOSTAL, $codepostal, $comparison);
    }

    /**
     * Filter the query on the ville column
     *
     * Example usage:
     * <code>
     * $query->filterByVille('fooValue');   // WHERE ville = 'fooValue'
     * $query->filterByVille('%fooValue%', Criteria::LIKE); // WHERE ville LIKE '%fooValue%'
     * </code>
     *
     * @param     string $ville The value to use as filter.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildAssoIndividusArchiveQuery The current query, for fluid interface
     */
    public function filterByVille($ville = null, $comparison = null)
    {
        if (null === $comparison) {
            if (is_array($ville)) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(AssoIndividusArchiveTableMap::COL_VILLE, $ville, $comparison);
    }

    /**
     * Filter the query on the pays column
     *
     * Example usage:
     * <code>
     * $query->filterByPays('fooValue');   // WHERE pays = 'fooValue'
     * $query->filterByPays('%fooValue%', Criteria::LIKE); // WHERE pays LIKE '%fooValue%'
     * </code>
     *
     * @param     string $pays The value to use as filter.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildAssoIndividusArchiveQuery The current query, for fluid interface
     */
    public function filterByPays($pays = null, $comparison = null)
    {
        if (null === $comparison) {
            if (is_array($pays)) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(AssoIndividusArchiveTableMap::COL_PAYS, $pays, $comparison);
    }

    /**
     * Filter the query on the telephone column
     *
     * Example usage:
     * <code>
     * $query->filterByTelephone('fooValue');   // WHERE telephone = 'fooValue'
     * $query->filterByTelephone('%fooValue%', Criteria::LIKE); // WHERE telephone LIKE '%fooValue%'
     * </code>
     *
     * @param     string $telephone The value to use as filter.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildAssoIndividusArchiveQuery The current query, for fluid interface
     */
    public function filterByTelephone($telephone = null, $comparison = null)
    {
        if (null === $comparison) {
            if (is_array($telephone)) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(AssoIndividusArchiveTableMap::COL_TELEPHONE, $telephone, $comparison);
    }

    /**
     * Filter the query on the telephone_pro column
     *
     * Example usage:
     * <code>
     * $query->filterByTelephonePro('fooValue');   // WHERE telephone_pro = 'fooValue'
     * $query->filterByTelephonePro('%fooValue%', Criteria::LIKE); // WHERE telephone_pro LIKE '%fooValue%'
     * </code>
     *
     * @param     string $telephonePro The value to use as filter.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildAssoIndividusArchiveQuery The current query, for fluid interface
     */
    public function filterByTelephonePro($telephonePro = null, $comparison = null)
    {
        if (null === $comparison) {
            if (is_array($telephonePro)) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(AssoIndividusArchiveTableMap::COL_TELEPHONE_PRO, $telephonePro, $comparison);
    }

    /**
     * Filter the query on the fax column
     *
     * Example usage:
     * <code>
     * $query->filterByFax('fooValue');   // WHERE fax = 'fooValue'
     * $query->filterByFax('%fooValue%', Criteria::LIKE); // WHERE fax LIKE '%fooValue%'
     * </code>
     *
     * @param     string $fax The value to use as filter.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildAssoIndividusArchiveQuery The current query, for fluid interface
     */
    public function filterByFax($fax = null, $comparison = null)
    {
        if (null === $comparison) {
            if (is_array($fax)) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(AssoIndividusArchiveTableMap::COL_FAX, $fax, $comparison);
    }

    /**
     * Filter the query on the mobile column
     *
     * Example usage:
     * <code>
     * $query->filterByMobile('fooValue');   // WHERE mobile = 'fooValue'
     * $query->filterByMobile('%fooValue%', Criteria::LIKE); // WHERE mobile LIKE '%fooValue%'
     * </code>
     *
     * @param     string $mobile The value to use as filter.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildAssoIndividusArchiveQuery The current query, for fluid interface
     */
    public function filterByMobile($mobile = null, $comparison = null)
    {
        if (null === $comparison) {
            if (is_array($mobile)) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(AssoIndividusArchiveTableMap::COL_MOBILE, $mobile, $comparison);
    }

    /**
     * Filter the query on the url column
     *
     * Example usage:
     * <code>
     * $query->filterByUrl('fooValue');   // WHERE url = 'fooValue'
     * $query->filterByUrl('%fooValue%', Criteria::LIKE); // WHERE url LIKE '%fooValue%'
     * </code>
     *
     * @param     string $url The value to use as filter.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildAssoIndividusArchiveQuery The current query, for fluid interface
     */
    public function filterByUrl($url = null, $comparison = null)
    {
        if (null === $comparison) {
            if (is_array($url)) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(AssoIndividusArchiveTableMap::COL_URL, $url, $comparison);
    }

    /**
     * Filter the query on the profession column
     *
     * Example usage:
     * <code>
     * $query->filterByProfession('fooValue');   // WHERE profession = 'fooValue'
     * $query->filterByProfession('%fooValue%', Criteria::LIKE); // WHERE profession LIKE '%fooValue%'
     * </code>
     *
     * @param     string $profession The value to use as filter.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildAssoIndividusArchiveQuery The current query, for fluid interface
     */
    public function filterByProfession($profession = null, $comparison = null)
    {
        if (null === $comparison) {
            if (is_array($profession)) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(AssoIndividusArchiveTableMap::COL_PROFESSION, $profession, $comparison);
    }

    /**
     * Filter the query on the contact_souhait column
     *
     * Example usage:
     * <code>
     * $query->filterByContactSouhait(true); // WHERE contact_souhait = true
     * $query->filterByContactSouhait('yes'); // WHERE contact_souhait = true
     * </code>
     *
     * @param     boolean|string $contactSouhait The value to use as filter.
     *              Non-boolean arguments are converted using the following rules:
     *                * 1, '1', 'true',  'on',  and 'yes' are converted to boolean true
     *                * 0, '0', 'false', 'off', and 'no'  are converted to boolean false
     *              Check on string values is case insensitive (so 'FaLsE' is seen as 'false').
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildAssoIndividusArchiveQuery The current query, for fluid interface
     */
    public function filterByContactSouhait($contactSouhait = null, $comparison = null)
    {
        if (is_string($contactSouhait)) {
            $contactSouhait = in_array(strtolower($contactSouhait), array('false', 'off', '-', 'no', 'n', '0', '')) ? false : true;
        }

        return $this->addUsingAlias(AssoIndividusArchiveTableMap::COL_CONTACT_SOUHAIT, $contactSouhait, $comparison);
    }

    /**
     * Filter the query on the observation column
     *
     * Example usage:
     * <code>
     * $query->filterByObservation('fooValue');   // WHERE observation = 'fooValue'
     * $query->filterByObservation('%fooValue%', Criteria::LIKE); // WHERE observation LIKE '%fooValue%'
     * </code>
     *
     * @param     string $observation The value to use as filter.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildAssoIndividusArchiveQuery The current query, for fluid interface
     */
    public function filterByObservation($observation = null, $comparison = null)
    {
        if (null === $comparison) {
            if (is_array($observation)) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(AssoIndividusArchiveTableMap::COL_OBSERVATION, $observation, $comparison);
    }

    /**
     * Filter the query on the created_at column
     *
     * Example usage:
     * <code>
     * $query->filterByCreatedAt('2011-03-14'); // WHERE created_at = '2011-03-14'
     * $query->filterByCreatedAt('now'); // WHERE created_at = '2011-03-14'
     * $query->filterByCreatedAt(array('max' => 'yesterday')); // WHERE created_at > '2011-03-13'
     * </code>
     *
     * @param     mixed $createdAt The value to use as filter.
     *              Values can be integers (unix timestamps), DateTime objects, or strings.
     *              Empty strings are treated as NULL.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildAssoIndividusArchiveQuery The current query, for fluid interface
     */
    public function filterByCreatedAt($createdAt = null, $comparison = null)
    {
        if (is_array($createdAt)) {
            $useMinMax = false;
            if (isset($createdAt['min'])) {
                $this->addUsingAlias(AssoIndividusArchiveTableMap::COL_CREATED_AT, $createdAt['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($createdAt['max'])) {
                $this->addUsingAlias(AssoIndividusArchiveTableMap::COL_CREATED_AT, $createdAt['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(AssoIndividusArchiveTableMap::COL_CREATED_AT, $createdAt, $comparison);
    }

    /**
     * Filter the query on the updated_at column
     *
     * Example usage:
     * <code>
     * $query->filterByUpdatedAt('2011-03-14'); // WHERE updated_at = '2011-03-14'
     * $query->filterByUpdatedAt('now'); // WHERE updated_at = '2011-03-14'
     * $query->filterByUpdatedAt(array('max' => 'yesterday')); // WHERE updated_at > '2011-03-13'
     * </code>
     *
     * @param     mixed $updatedAt The value to use as filter.
     *              Values can be integers (unix timestamps), DateTime objects, or strings.
     *              Empty strings are treated as NULL.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildAssoIndividusArchiveQuery The current query, for fluid interface
     */
    public function filterByUpdatedAt($updatedAt = null, $comparison = null)
    {
        if (is_array($updatedAt)) {
            $useMinMax = false;
            if (isset($updatedAt['min'])) {
                $this->addUsingAlias(AssoIndividusArchiveTableMap::COL_UPDATED_AT, $updatedAt['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($updatedAt['max'])) {
                $this->addUsingAlias(AssoIndividusArchiveTableMap::COL_UPDATED_AT, $updatedAt['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(AssoIndividusArchiveTableMap::COL_UPDATED_AT, $updatedAt, $comparison);
    }

    /**
     * Filter the query on the archived_at column
     *
     * Example usage:
     * <code>
     * $query->filterByArchivedAt('2011-03-14'); // WHERE archived_at = '2011-03-14'
     * $query->filterByArchivedAt('now'); // WHERE archived_at = '2011-03-14'
     * $query->filterByArchivedAt(array('max' => 'yesterday')); // WHERE archived_at > '2011-03-13'
     * </code>
     *
     * @param     mixed $archivedAt The value to use as filter.
     *              Values can be integers (unix timestamps), DateTime objects, or strings.
     *              Empty strings are treated as NULL.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildAssoIndividusArchiveQuery The current query, for fluid interface
     */
    public function filterByArchivedAt($archivedAt = null, $comparison = null)
    {
        if (is_array($archivedAt)) {
            $useMinMax = false;
            if (isset($archivedAt['min'])) {
                $this->addUsingAlias(AssoIndividusArchiveTableMap::COL_ARCHIVED_AT, $archivedAt['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($archivedAt['max'])) {
                $this->addUsingAlias(AssoIndividusArchiveTableMap::COL_ARCHIVED_AT, $archivedAt['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(AssoIndividusArchiveTableMap::COL_ARCHIVED_AT, $archivedAt, $comparison);
    }

    /**
     * Exclude object from result
     *
     * @param   ChildAssoIndividusArchive $assoIndividusArchive Object to remove from the list of results
     *
     * @return $this|ChildAssoIndividusArchiveQuery The current query, for fluid interface
     */
    public function prune($assoIndividusArchive = null)
    {
        if ($assoIndividusArchive) {
            $this->addUsingAlias(AssoIndividusArchiveTableMap::COL_ID_INDIVIDU, $assoIndividusArchive->getIdIndividu(), Criteria::NOT_EQUAL);
        }

        return $this;
    }

    /**
     * Deletes all rows from the asso_individus_archive table.
     *
     * @param ConnectionInterface $con the connection to use
     * @return int The number of affected rows (if supported by underlying database driver).
     */
    public function doDeleteAll(ConnectionInterface $con = null)
    {
        if (null === $con) {
            $con = Propel::getServiceContainer()->getWriteConnection(AssoIndividusArchiveTableMap::DATABASE_NAME);
        }

        // use transaction because $criteria could contain info
        // for more than one table or we could emulating ON DELETE CASCADE, etc.
        return $con->transaction(function () use ($con) {
            $affectedRows = 0; // initialize var to track total num of affected rows
            $affectedRows += parent::doDeleteAll($con);
            // Because this db requires some delete cascade/set null emulation, we have to
            // clear the cached instance *after* the emulation has happened (since
            // instances get re-added by the select statement contained therein).
            AssoIndividusArchiveTableMap::clearInstancePool();
            AssoIndividusArchiveTableMap::clearRelatedInstancePool();

            return $affectedRows;
        });
    }

    /**
     * Performs a DELETE on the database based on the current ModelCriteria
     *
     * @param ConnectionInterface $con the connection to use
     * @return int             The number of affected rows (if supported by underlying database driver).  This includes CASCADE-related rows
     *                         if supported by native driver or if emulated using Propel.
     * @throws PropelException Any exceptions caught during processing will be
     *                         rethrown wrapped into a PropelException.
     */
    public function delete(ConnectionInterface $con = null)
    {
        if (null === $con) {
            $con = Propel::getServiceContainer()->getWriteConnection(AssoIndividusArchiveTableMap::DATABASE_NAME);
        }

        $criteria = $this;

        // Set the correct dbName
        $criteria->setDbName(AssoIndividusArchiveTableMap::DATABASE_NAME);

        // use transaction because $criteria could contain info
        // for more than one table or we could emulating ON DELETE CASCADE, etc.
        return $con->transaction(function () use ($con, $criteria) {
            $affectedRows = 0; // initialize var to track total num of affected rows

            AssoIndividusArchiveTableMap::removeInstanceFromPool($criteria);

            $affectedRows += ModelCriteria::delete($con);
            AssoIndividusArchiveTableMap::clearRelatedInstancePool();

            return $affectedRows;
        });
    }

} // AssoIndividusArchiveQuery
