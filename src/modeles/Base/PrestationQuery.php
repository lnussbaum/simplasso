<?php

namespace Base;

use \AssoPrestationsArchive as ChildAssoPrestationsArchive;
use \Prestation as ChildPrestation;
use \PrestationQuery as ChildPrestationQuery;
use \Exception;
use \PDO;
use Map\PrestationTableMap;
use Propel\Runtime\Propel;
use Propel\Runtime\ActiveQuery\Criteria;
use Propel\Runtime\ActiveQuery\ModelCriteria;
use Propel\Runtime\ActiveQuery\ModelJoin;
use Propel\Runtime\Collection\ObjectCollection;
use Propel\Runtime\Connection\ConnectionInterface;
use Propel\Runtime\Exception\PropelException;

/**
 * Base class that represents a query for the 'asso_prestations' table.
 *
 *
 *
 * @method     ChildPrestationQuery orderByIdPrestation($order = Criteria::ASC) Order by the id_prestation column
 * @method     ChildPrestationQuery orderByNomGroupe($order = Criteria::ASC) Order by the nom_groupe column
 * @method     ChildPrestationQuery orderByNom($order = Criteria::ASC) Order by the nom column
 * @method     ChildPrestationQuery orderByNomcourt($order = Criteria::ASC) Order by the nomcourt column
 * @method     ChildPrestationQuery orderByDescriptif($order = Criteria::ASC) Order by the descriptif column
 * @method     ChildPrestationQuery orderByIdEntite($order = Criteria::ASC) Order by the id_entite column
 * @method     ChildPrestationQuery orderByIdTva($order = Criteria::ASC) Order by the id_tva column
 * @method     ChildPrestationQuery orderByRetardJours($order = Criteria::ASC) Order by the retard_jours column
 * @method     ChildPrestationQuery orderByNombreNumero($order = Criteria::ASC) Order by the nombre_numero column
 * @method     ChildPrestationQuery orderByProchainNumero($order = Criteria::ASC) Order by the prochain_numero column
 * @method     ChildPrestationQuery orderByPrixlibre($order = Criteria::ASC) Order by the prixlibre column
 * @method     ChildPrestationQuery orderByQuantite($order = Criteria::ASC) Order by the quantite column
 * @method     ChildPrestationQuery orderByIdUnite($order = Criteria::ASC) Order by the id_unite column
 * @method     ChildPrestationQuery orderByIdCompte($order = Criteria::ASC) Order by the id_compte column
 * @method     ChildPrestationQuery orderByPrestationType($order = Criteria::ASC) Order by the prestation_type column
 * @method     ChildPrestationQuery orderByActive($order = Criteria::ASC) Order by the active column
 * @method     ChildPrestationQuery orderByNbVoix($order = Criteria::ASC) Order by the nb_voix column
 * @method     ChildPrestationQuery orderByPeriodique($order = Criteria::ASC) Order by the periodique column
 * @method     ChildPrestationQuery orderBySigne($order = Criteria::ASC) Order by the signe column
 * @method     ChildPrestationQuery orderByObjetBeneficiaire($order = Criteria::ASC) Order by the objet_beneficiaire column
 * @method     ChildPrestationQuery orderByCreatedAt($order = Criteria::ASC) Order by the created_at column
 * @method     ChildPrestationQuery orderByUpdatedAt($order = Criteria::ASC) Order by the updated_at column
 *
 * @method     ChildPrestationQuery groupByIdPrestation() Group by the id_prestation column
 * @method     ChildPrestationQuery groupByNomGroupe() Group by the nom_groupe column
 * @method     ChildPrestationQuery groupByNom() Group by the nom column
 * @method     ChildPrestationQuery groupByNomcourt() Group by the nomcourt column
 * @method     ChildPrestationQuery groupByDescriptif() Group by the descriptif column
 * @method     ChildPrestationQuery groupByIdEntite() Group by the id_entite column
 * @method     ChildPrestationQuery groupByIdTva() Group by the id_tva column
 * @method     ChildPrestationQuery groupByRetardJours() Group by the retard_jours column
 * @method     ChildPrestationQuery groupByNombreNumero() Group by the nombre_numero column
 * @method     ChildPrestationQuery groupByProchainNumero() Group by the prochain_numero column
 * @method     ChildPrestationQuery groupByPrixlibre() Group by the prixlibre column
 * @method     ChildPrestationQuery groupByQuantite() Group by the quantite column
 * @method     ChildPrestationQuery groupByIdUnite() Group by the id_unite column
 * @method     ChildPrestationQuery groupByIdCompte() Group by the id_compte column
 * @method     ChildPrestationQuery groupByPrestationType() Group by the prestation_type column
 * @method     ChildPrestationQuery groupByActive() Group by the active column
 * @method     ChildPrestationQuery groupByNbVoix() Group by the nb_voix column
 * @method     ChildPrestationQuery groupByPeriodique() Group by the periodique column
 * @method     ChildPrestationQuery groupBySigne() Group by the signe column
 * @method     ChildPrestationQuery groupByObjetBeneficiaire() Group by the objet_beneficiaire column
 * @method     ChildPrestationQuery groupByCreatedAt() Group by the created_at column
 * @method     ChildPrestationQuery groupByUpdatedAt() Group by the updated_at column
 *
 * @method     ChildPrestationQuery leftJoin($relation) Adds a LEFT JOIN clause to the query
 * @method     ChildPrestationQuery rightJoin($relation) Adds a RIGHT JOIN clause to the query
 * @method     ChildPrestationQuery innerJoin($relation) Adds a INNER JOIN clause to the query
 *
 * @method     ChildPrestationQuery leftJoinWith($relation) Adds a LEFT JOIN clause and with to the query
 * @method     ChildPrestationQuery rightJoinWith($relation) Adds a RIGHT JOIN clause and with to the query
 * @method     ChildPrestationQuery innerJoinWith($relation) Adds a INNER JOIN clause and with to the query
 *
 * @method     ChildPrestationQuery leftJoinEntite($relationAlias = null) Adds a LEFT JOIN clause to the query using the Entite relation
 * @method     ChildPrestationQuery rightJoinEntite($relationAlias = null) Adds a RIGHT JOIN clause to the query using the Entite relation
 * @method     ChildPrestationQuery innerJoinEntite($relationAlias = null) Adds a INNER JOIN clause to the query using the Entite relation
 *
 * @method     ChildPrestationQuery joinWithEntite($joinType = Criteria::INNER_JOIN) Adds a join clause and with to the query using the Entite relation
 *
 * @method     ChildPrestationQuery leftJoinWithEntite() Adds a LEFT JOIN clause and with to the query using the Entite relation
 * @method     ChildPrestationQuery rightJoinWithEntite() Adds a RIGHT JOIN clause and with to the query using the Entite relation
 * @method     ChildPrestationQuery innerJoinWithEntite() Adds a INNER JOIN clause and with to the query using the Entite relation
 *
 * @method     ChildPrestationQuery leftJoinCompte($relationAlias = null) Adds a LEFT JOIN clause to the query using the Compte relation
 * @method     ChildPrestationQuery rightJoinCompte($relationAlias = null) Adds a RIGHT JOIN clause to the query using the Compte relation
 * @method     ChildPrestationQuery innerJoinCompte($relationAlias = null) Adds a INNER JOIN clause to the query using the Compte relation
 *
 * @method     ChildPrestationQuery joinWithCompte($joinType = Criteria::INNER_JOIN) Adds a join clause and with to the query using the Compte relation
 *
 * @method     ChildPrestationQuery leftJoinWithCompte() Adds a LEFT JOIN clause and with to the query using the Compte relation
 * @method     ChildPrestationQuery rightJoinWithCompte() Adds a RIGHT JOIN clause and with to the query using the Compte relation
 * @method     ChildPrestationQuery innerJoinWithCompte() Adds a INNER JOIN clause and with to the query using the Compte relation
 *
 * @method     ChildPrestationQuery leftJoinPrix($relationAlias = null) Adds a LEFT JOIN clause to the query using the Prix relation
 * @method     ChildPrestationQuery rightJoinPrix($relationAlias = null) Adds a RIGHT JOIN clause to the query using the Prix relation
 * @method     ChildPrestationQuery innerJoinPrix($relationAlias = null) Adds a INNER JOIN clause to the query using the Prix relation
 *
 * @method     ChildPrestationQuery joinWithPrix($joinType = Criteria::INNER_JOIN) Adds a join clause and with to the query using the Prix relation
 *
 * @method     ChildPrestationQuery leftJoinWithPrix() Adds a LEFT JOIN clause and with to the query using the Prix relation
 * @method     ChildPrestationQuery rightJoinWithPrix() Adds a RIGHT JOIN clause and with to the query using the Prix relation
 * @method     ChildPrestationQuery innerJoinWithPrix() Adds a INNER JOIN clause and with to the query using the Prix relation
 *
 * @method     ChildPrestationQuery leftJoinPrestationslotPrestation($relationAlias = null) Adds a LEFT JOIN clause to the query using the PrestationslotPrestation relation
 * @method     ChildPrestationQuery rightJoinPrestationslotPrestation($relationAlias = null) Adds a RIGHT JOIN clause to the query using the PrestationslotPrestation relation
 * @method     ChildPrestationQuery innerJoinPrestationslotPrestation($relationAlias = null) Adds a INNER JOIN clause to the query using the PrestationslotPrestation relation
 *
 * @method     ChildPrestationQuery joinWithPrestationslotPrestation($joinType = Criteria::INNER_JOIN) Adds a join clause and with to the query using the PrestationslotPrestation relation
 *
 * @method     ChildPrestationQuery leftJoinWithPrestationslotPrestation() Adds a LEFT JOIN clause and with to the query using the PrestationslotPrestation relation
 * @method     ChildPrestationQuery rightJoinWithPrestationslotPrestation() Adds a RIGHT JOIN clause and with to the query using the PrestationslotPrestation relation
 * @method     ChildPrestationQuery innerJoinWithPrestationslotPrestation() Adds a INNER JOIN clause and with to the query using the PrestationslotPrestation relation
 *
 * @method     ChildPrestationQuery leftJoinServicerendu($relationAlias = null) Adds a LEFT JOIN clause to the query using the Servicerendu relation
 * @method     ChildPrestationQuery rightJoinServicerendu($relationAlias = null) Adds a RIGHT JOIN clause to the query using the Servicerendu relation
 * @method     ChildPrestationQuery innerJoinServicerendu($relationAlias = null) Adds a INNER JOIN clause to the query using the Servicerendu relation
 *
 * @method     ChildPrestationQuery joinWithServicerendu($joinType = Criteria::INNER_JOIN) Adds a join clause and with to the query using the Servicerendu relation
 *
 * @method     ChildPrestationQuery leftJoinWithServicerendu() Adds a LEFT JOIN clause and with to the query using the Servicerendu relation
 * @method     ChildPrestationQuery rightJoinWithServicerendu() Adds a RIGHT JOIN clause and with to the query using the Servicerendu relation
 * @method     ChildPrestationQuery innerJoinWithServicerendu() Adds a INNER JOIN clause and with to the query using the Servicerendu relation
 *
 * @method     \EntiteQuery|\CompteQuery|\PrixQuery|\PrestationslotPrestationQuery|\ServicerenduQuery endUse() Finalizes a secondary criteria and merges it with its primary Criteria
 *
 * @method     ChildPrestation findOne(ConnectionInterface $con = null) Return the first ChildPrestation matching the query
 * @method     ChildPrestation findOneOrCreate(ConnectionInterface $con = null) Return the first ChildPrestation matching the query, or a new ChildPrestation object populated from the query conditions when no match is found
 *
 * @method     ChildPrestation findOneByIdPrestation(string $id_prestation) Return the first ChildPrestation filtered by the id_prestation column
 * @method     ChildPrestation findOneByNomGroupe(string $nom_groupe) Return the first ChildPrestation filtered by the nom_groupe column
 * @method     ChildPrestation findOneByNom(string $nom) Return the first ChildPrestation filtered by the nom column
 * @method     ChildPrestation findOneByNomcourt(string $nomcourt) Return the first ChildPrestation filtered by the nomcourt column
 * @method     ChildPrestation findOneByDescriptif(string $descriptif) Return the first ChildPrestation filtered by the descriptif column
 * @method     ChildPrestation findOneByIdEntite(string $id_entite) Return the first ChildPrestation filtered by the id_entite column
 * @method     ChildPrestation findOneByIdTva(string $id_tva) Return the first ChildPrestation filtered by the id_tva column
 * @method     ChildPrestation findOneByRetardJours(int $retard_jours) Return the first ChildPrestation filtered by the retard_jours column
 * @method     ChildPrestation findOneByNombreNumero(int $nombre_numero) Return the first ChildPrestation filtered by the nombre_numero column
 * @method     ChildPrestation findOneByProchainNumero(int $prochain_numero) Return the first ChildPrestation filtered by the prochain_numero column
 * @method     ChildPrestation findOneByPrixlibre(boolean $prixlibre) Return the first ChildPrestation filtered by the prixlibre column
 * @method     ChildPrestation findOneByQuantite(boolean $quantite) Return the first ChildPrestation filtered by the quantite column
 * @method     ChildPrestation findOneByIdUnite(string $id_unite) Return the first ChildPrestation filtered by the id_unite column
 * @method     ChildPrestation findOneByIdCompte(string $id_compte) Return the first ChildPrestation filtered by the id_compte column
 * @method     ChildPrestation findOneByPrestationType(int $prestation_type) Return the first ChildPrestation filtered by the prestation_type column
 * @method     ChildPrestation findOneByActive(boolean $active) Return the first ChildPrestation filtered by the active column
 * @method     ChildPrestation findOneByNbVoix(int $nb_voix) Return the first ChildPrestation filtered by the nb_voix column
 * @method     ChildPrestation findOneByPeriodique(string $periodique) Return the first ChildPrestation filtered by the periodique column
 * @method     ChildPrestation findOneBySigne(string $signe) Return the first ChildPrestation filtered by the signe column
 * @method     ChildPrestation findOneByObjetBeneficiaire(string $objet_beneficiaire) Return the first ChildPrestation filtered by the objet_beneficiaire column
 * @method     ChildPrestation findOneByCreatedAt(string $created_at) Return the first ChildPrestation filtered by the created_at column
 * @method     ChildPrestation findOneByUpdatedAt(string $updated_at) Return the first ChildPrestation filtered by the updated_at column *

 * @method     ChildPrestation requirePk($key, ConnectionInterface $con = null) Return the ChildPrestation by primary key and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildPrestation requireOne(ConnectionInterface $con = null) Return the first ChildPrestation matching the query and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 *
 * @method     ChildPrestation requireOneByIdPrestation(string $id_prestation) Return the first ChildPrestation filtered by the id_prestation column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildPrestation requireOneByNomGroupe(string $nom_groupe) Return the first ChildPrestation filtered by the nom_groupe column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildPrestation requireOneByNom(string $nom) Return the first ChildPrestation filtered by the nom column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildPrestation requireOneByNomcourt(string $nomcourt) Return the first ChildPrestation filtered by the nomcourt column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildPrestation requireOneByDescriptif(string $descriptif) Return the first ChildPrestation filtered by the descriptif column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildPrestation requireOneByIdEntite(string $id_entite) Return the first ChildPrestation filtered by the id_entite column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildPrestation requireOneByIdTva(string $id_tva) Return the first ChildPrestation filtered by the id_tva column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildPrestation requireOneByRetardJours(int $retard_jours) Return the first ChildPrestation filtered by the retard_jours column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildPrestation requireOneByNombreNumero(int $nombre_numero) Return the first ChildPrestation filtered by the nombre_numero column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildPrestation requireOneByProchainNumero(int $prochain_numero) Return the first ChildPrestation filtered by the prochain_numero column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildPrestation requireOneByPrixlibre(boolean $prixlibre) Return the first ChildPrestation filtered by the prixlibre column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildPrestation requireOneByQuantite(boolean $quantite) Return the first ChildPrestation filtered by the quantite column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildPrestation requireOneByIdUnite(string $id_unite) Return the first ChildPrestation filtered by the id_unite column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildPrestation requireOneByIdCompte(string $id_compte) Return the first ChildPrestation filtered by the id_compte column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildPrestation requireOneByPrestationType(int $prestation_type) Return the first ChildPrestation filtered by the prestation_type column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildPrestation requireOneByActive(boolean $active) Return the first ChildPrestation filtered by the active column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildPrestation requireOneByNbVoix(int $nb_voix) Return the first ChildPrestation filtered by the nb_voix column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildPrestation requireOneByPeriodique(string $periodique) Return the first ChildPrestation filtered by the periodique column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildPrestation requireOneBySigne(string $signe) Return the first ChildPrestation filtered by the signe column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildPrestation requireOneByObjetBeneficiaire(string $objet_beneficiaire) Return the first ChildPrestation filtered by the objet_beneficiaire column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildPrestation requireOneByCreatedAt(string $created_at) Return the first ChildPrestation filtered by the created_at column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildPrestation requireOneByUpdatedAt(string $updated_at) Return the first ChildPrestation filtered by the updated_at column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 *
 * @method     ChildPrestation[]|ObjectCollection find(ConnectionInterface $con = null) Return ChildPrestation objects based on current ModelCriteria
 * @method     ChildPrestation[]|ObjectCollection findByIdPrestation(string $id_prestation) Return ChildPrestation objects filtered by the id_prestation column
 * @method     ChildPrestation[]|ObjectCollection findByNomGroupe(string $nom_groupe) Return ChildPrestation objects filtered by the nom_groupe column
 * @method     ChildPrestation[]|ObjectCollection findByNom(string $nom) Return ChildPrestation objects filtered by the nom column
 * @method     ChildPrestation[]|ObjectCollection findByNomcourt(string $nomcourt) Return ChildPrestation objects filtered by the nomcourt column
 * @method     ChildPrestation[]|ObjectCollection findByDescriptif(string $descriptif) Return ChildPrestation objects filtered by the descriptif column
 * @method     ChildPrestation[]|ObjectCollection findByIdEntite(string $id_entite) Return ChildPrestation objects filtered by the id_entite column
 * @method     ChildPrestation[]|ObjectCollection findByIdTva(string $id_tva) Return ChildPrestation objects filtered by the id_tva column
 * @method     ChildPrestation[]|ObjectCollection findByRetardJours(int $retard_jours) Return ChildPrestation objects filtered by the retard_jours column
 * @method     ChildPrestation[]|ObjectCollection findByNombreNumero(int $nombre_numero) Return ChildPrestation objects filtered by the nombre_numero column
 * @method     ChildPrestation[]|ObjectCollection findByProchainNumero(int $prochain_numero) Return ChildPrestation objects filtered by the prochain_numero column
 * @method     ChildPrestation[]|ObjectCollection findByPrixlibre(boolean $prixlibre) Return ChildPrestation objects filtered by the prixlibre column
 * @method     ChildPrestation[]|ObjectCollection findByQuantite(boolean $quantite) Return ChildPrestation objects filtered by the quantite column
 * @method     ChildPrestation[]|ObjectCollection findByIdUnite(string $id_unite) Return ChildPrestation objects filtered by the id_unite column
 * @method     ChildPrestation[]|ObjectCollection findByIdCompte(string $id_compte) Return ChildPrestation objects filtered by the id_compte column
 * @method     ChildPrestation[]|ObjectCollection findByPrestationType(int $prestation_type) Return ChildPrestation objects filtered by the prestation_type column
 * @method     ChildPrestation[]|ObjectCollection findByActive(boolean $active) Return ChildPrestation objects filtered by the active column
 * @method     ChildPrestation[]|ObjectCollection findByNbVoix(int $nb_voix) Return ChildPrestation objects filtered by the nb_voix column
 * @method     ChildPrestation[]|ObjectCollection findByPeriodique(string $periodique) Return ChildPrestation objects filtered by the periodique column
 * @method     ChildPrestation[]|ObjectCollection findBySigne(string $signe) Return ChildPrestation objects filtered by the signe column
 * @method     ChildPrestation[]|ObjectCollection findByObjetBeneficiaire(string $objet_beneficiaire) Return ChildPrestation objects filtered by the objet_beneficiaire column
 * @method     ChildPrestation[]|ObjectCollection findByCreatedAt(string $created_at) Return ChildPrestation objects filtered by the created_at column
 * @method     ChildPrestation[]|ObjectCollection findByUpdatedAt(string $updated_at) Return ChildPrestation objects filtered by the updated_at column
 * @method     ChildPrestation[]|\Propel\Runtime\Util\PropelModelPager paginate($page = 1, $maxPerPage = 10, ConnectionInterface $con = null) Issue a SELECT query based on the current ModelCriteria and uses a page and a maximum number of results per page to compute an offset and a limit
 *
 */
abstract class PrestationQuery extends ModelCriteria
{

    // archivable behavior
    protected $archiveOnDelete = true;
protected $entityNotFoundExceptionClass = '\\Propel\\Runtime\\Exception\\EntityNotFoundException';

    /**
     * Initializes internal state of \Base\PrestationQuery object.
     *
     * @param     string $dbName The database name
     * @param     string $modelName The phpName of a model, e.g. 'Book'
     * @param     string $modelAlias The alias for the model in this query, e.g. 'b'
     */
    public function __construct($dbName = 'simplasso', $modelName = '\\Prestation', $modelAlias = null)
    {
        parent::__construct($dbName, $modelName, $modelAlias);
    }

    /**
     * Returns a new ChildPrestationQuery object.
     *
     * @param     string $modelAlias The alias of a model in the query
     * @param     Criteria $criteria Optional Criteria to build the query from
     *
     * @return ChildPrestationQuery
     */
    public static function create($modelAlias = null, Criteria $criteria = null)
    {
        if ($criteria instanceof ChildPrestationQuery) {
            return $criteria;
        }
        $query = new ChildPrestationQuery();
        if (null !== $modelAlias) {
            $query->setModelAlias($modelAlias);
        }
        if ($criteria instanceof Criteria) {
            $query->mergeWith($criteria);
        }

        return $query;
    }

    /**
     * Find object by primary key.
     * Propel uses the instance pool to skip the database if the object exists.
     * Go fast if the query is untouched.
     *
     * <code>
     * $obj  = $c->findPk(12, $con);
     * </code>
     *
     * @param mixed $key Primary key to use for the query
     * @param ConnectionInterface $con an optional connection object
     *
     * @return ChildPrestation|array|mixed the result, formatted by the current formatter
     */
    public function findPk($key, ConnectionInterface $con = null)
    {
        if ($key === null) {
            return null;
        }

        if ($con === null) {
            $con = Propel::getServiceContainer()->getReadConnection(PrestationTableMap::DATABASE_NAME);
        }

        $this->basePreSelect($con);

        if (
            $this->formatter || $this->modelAlias || $this->with || $this->select
            || $this->selectColumns || $this->asColumns || $this->selectModifiers
            || $this->map || $this->having || $this->joins
        ) {
            return $this->findPkComplex($key, $con);
        }

        if ((null !== ($obj = PrestationTableMap::getInstanceFromPool(null === $key || is_scalar($key) || is_callable([$key, '__toString']) ? (string) $key : $key)))) {
            // the object is already in the instance pool
            return $obj;
        }

        return $this->findPkSimple($key, $con);
    }

    /**
     * Find object by primary key using raw SQL to go fast.
     * Bypass doSelect() and the object formatter by using generated code.
     *
     * @param     mixed $key Primary key to use for the query
     * @param     ConnectionInterface $con A connection object
     *
     * @throws \Propel\Runtime\Exception\PropelException
     *
     * @return ChildPrestation A model object, or null if the key is not found
     */
    protected function findPkSimple($key, ConnectionInterface $con)
    {
        $sql = 'SELECT `id_prestation`, `nom_groupe`, `nom`, `nomcourt`, `descriptif`, `id_entite`, `id_tva`, `retard_jours`, `nombre_numero`, `prochain_numero`, `prixlibre`, `quantite`, `id_unite`, `id_compte`, `prestation_type`, `active`, `nb_voix`, `periodique`, `signe`, `objet_beneficiaire`, `created_at`, `updated_at` FROM `asso_prestations` WHERE `id_prestation` = :p0';
        try {
            $stmt = $con->prepare($sql);
            $stmt->bindValue(':p0', $key, PDO::PARAM_INT);
            $stmt->execute();
        } catch (Exception $e) {
            Propel::log($e->getMessage(), Propel::LOG_ERR);
            throw new PropelException(sprintf('Unable to execute SELECT statement [%s]', $sql), 0, $e);
        }
        $obj = null;
        if ($row = $stmt->fetch(\PDO::FETCH_NUM)) {
            $cls = PrestationTableMap::getOMClass($row, 0, false);
            /** @var ChildPrestation $obj */
            $obj = new $cls();
            $obj->hydrate($row);
            PrestationTableMap::addInstanceToPool($obj, null === $key || is_scalar($key) || is_callable([$key, '__toString']) ? (string) $key : $key);
        }
        $stmt->closeCursor();

        return $obj;
    }

    /**
     * Find object by primary key.
     *
     * @param     mixed $key Primary key to use for the query
     * @param     ConnectionInterface $con A connection object
     *
     * @return ChildPrestation|array|mixed the result, formatted by the current formatter
     */
    protected function findPkComplex($key, ConnectionInterface $con)
    {
        // As the query uses a PK condition, no limit(1) is necessary.
        $criteria = $this->isKeepQuery() ? clone $this : $this;
        $dataFetcher = $criteria
            ->filterByPrimaryKey($key)
            ->doSelect($con);

        return $criteria->getFormatter()->init($criteria)->formatOne($dataFetcher);
    }

    /**
     * Find objects by primary key
     * <code>
     * $objs = $c->findPks(array(12, 56, 832), $con);
     * </code>
     * @param     array $keys Primary keys to use for the query
     * @param     ConnectionInterface $con an optional connection object
     *
     * @return ObjectCollection|array|mixed the list of results, formatted by the current formatter
     */
    public function findPks($keys, ConnectionInterface $con = null)
    {
        if (null === $con) {
            $con = Propel::getServiceContainer()->getReadConnection($this->getDbName());
        }
        $this->basePreSelect($con);
        $criteria = $this->isKeepQuery() ? clone $this : $this;
        $dataFetcher = $criteria
            ->filterByPrimaryKeys($keys)
            ->doSelect($con);

        return $criteria->getFormatter()->init($criteria)->format($dataFetcher);
    }

    /**
     * Filter the query by primary key
     *
     * @param     mixed $key Primary key to use for the query
     *
     * @return $this|ChildPrestationQuery The current query, for fluid interface
     */
    public function filterByPrimaryKey($key)
    {

        return $this->addUsingAlias(PrestationTableMap::COL_ID_PRESTATION, $key, Criteria::EQUAL);
    }

    /**
     * Filter the query by a list of primary keys
     *
     * @param     array $keys The list of primary key to use for the query
     *
     * @return $this|ChildPrestationQuery The current query, for fluid interface
     */
    public function filterByPrimaryKeys($keys)
    {

        return $this->addUsingAlias(PrestationTableMap::COL_ID_PRESTATION, $keys, Criteria::IN);
    }

    /**
     * Filter the query on the id_prestation column
     *
     * Example usage:
     * <code>
     * $query->filterByIdPrestation(1234); // WHERE id_prestation = 1234
     * $query->filterByIdPrestation(array(12, 34)); // WHERE id_prestation IN (12, 34)
     * $query->filterByIdPrestation(array('min' => 12)); // WHERE id_prestation > 12
     * </code>
     *
     * @param     mixed $idPrestation The value to use as filter.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildPrestationQuery The current query, for fluid interface
     */
    public function filterByIdPrestation($idPrestation = null, $comparison = null)
    {
        if (is_array($idPrestation)) {
            $useMinMax = false;
            if (isset($idPrestation['min'])) {
                $this->addUsingAlias(PrestationTableMap::COL_ID_PRESTATION, $idPrestation['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($idPrestation['max'])) {
                $this->addUsingAlias(PrestationTableMap::COL_ID_PRESTATION, $idPrestation['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(PrestationTableMap::COL_ID_PRESTATION, $idPrestation, $comparison);
    }

    /**
     * Filter the query on the nom_groupe column
     *
     * Example usage:
     * <code>
     * $query->filterByNomGroupe('fooValue');   // WHERE nom_groupe = 'fooValue'
     * $query->filterByNomGroupe('%fooValue%', Criteria::LIKE); // WHERE nom_groupe LIKE '%fooValue%'
     * </code>
     *
     * @param     string $nomGroupe The value to use as filter.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildPrestationQuery The current query, for fluid interface
     */
    public function filterByNomGroupe($nomGroupe = null, $comparison = null)
    {
        if (null === $comparison) {
            if (is_array($nomGroupe)) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(PrestationTableMap::COL_NOM_GROUPE, $nomGroupe, $comparison);
    }

    /**
     * Filter the query on the nom column
     *
     * Example usage:
     * <code>
     * $query->filterByNom('fooValue');   // WHERE nom = 'fooValue'
     * $query->filterByNom('%fooValue%', Criteria::LIKE); // WHERE nom LIKE '%fooValue%'
     * </code>
     *
     * @param     string $nom The value to use as filter.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildPrestationQuery The current query, for fluid interface
     */
    public function filterByNom($nom = null, $comparison = null)
    {
        if (null === $comparison) {
            if (is_array($nom)) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(PrestationTableMap::COL_NOM, $nom, $comparison);
    }

    /**
     * Filter the query on the nomcourt column
     *
     * Example usage:
     * <code>
     * $query->filterByNomcourt('fooValue');   // WHERE nomcourt = 'fooValue'
     * $query->filterByNomcourt('%fooValue%', Criteria::LIKE); // WHERE nomcourt LIKE '%fooValue%'
     * </code>
     *
     * @param     string $nomcourt The value to use as filter.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildPrestationQuery The current query, for fluid interface
     */
    public function filterByNomcourt($nomcourt = null, $comparison = null)
    {
        if (null === $comparison) {
            if (is_array($nomcourt)) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(PrestationTableMap::COL_NOMCOURT, $nomcourt, $comparison);
    }

    /**
     * Filter the query on the descriptif column
     *
     * Example usage:
     * <code>
     * $query->filterByDescriptif('fooValue');   // WHERE descriptif = 'fooValue'
     * $query->filterByDescriptif('%fooValue%', Criteria::LIKE); // WHERE descriptif LIKE '%fooValue%'
     * </code>
     *
     * @param     string $descriptif The value to use as filter.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildPrestationQuery The current query, for fluid interface
     */
    public function filterByDescriptif($descriptif = null, $comparison = null)
    {
        if (null === $comparison) {
            if (is_array($descriptif)) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(PrestationTableMap::COL_DESCRIPTIF, $descriptif, $comparison);
    }

    /**
     * Filter the query on the id_entite column
     *
     * Example usage:
     * <code>
     * $query->filterByIdEntite(1234); // WHERE id_entite = 1234
     * $query->filterByIdEntite(array(12, 34)); // WHERE id_entite IN (12, 34)
     * $query->filterByIdEntite(array('min' => 12)); // WHERE id_entite > 12
     * </code>
     *
     * @see       filterByEntite()
     *
     * @param     mixed $idEntite The value to use as filter.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildPrestationQuery The current query, for fluid interface
     */
    public function filterByIdEntite($idEntite = null, $comparison = null)
    {
        if (is_array($idEntite)) {
            $useMinMax = false;
            if (isset($idEntite['min'])) {
                $this->addUsingAlias(PrestationTableMap::COL_ID_ENTITE, $idEntite['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($idEntite['max'])) {
                $this->addUsingAlias(PrestationTableMap::COL_ID_ENTITE, $idEntite['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(PrestationTableMap::COL_ID_ENTITE, $idEntite, $comparison);
    }

    /**
     * Filter the query on the id_tva column
     *
     * Example usage:
     * <code>
     * $query->filterByIdTva(1234); // WHERE id_tva = 1234
     * $query->filterByIdTva(array(12, 34)); // WHERE id_tva IN (12, 34)
     * $query->filterByIdTva(array('min' => 12)); // WHERE id_tva > 12
     * </code>
     *
     * @param     mixed $idTva The value to use as filter.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildPrestationQuery The current query, for fluid interface
     */
    public function filterByIdTva($idTva = null, $comparison = null)
    {
        if (is_array($idTva)) {
            $useMinMax = false;
            if (isset($idTva['min'])) {
                $this->addUsingAlias(PrestationTableMap::COL_ID_TVA, $idTva['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($idTva['max'])) {
                $this->addUsingAlias(PrestationTableMap::COL_ID_TVA, $idTva['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(PrestationTableMap::COL_ID_TVA, $idTva, $comparison);
    }

    /**
     * Filter the query on the retard_jours column
     *
     * Example usage:
     * <code>
     * $query->filterByRetardJours(1234); // WHERE retard_jours = 1234
     * $query->filterByRetardJours(array(12, 34)); // WHERE retard_jours IN (12, 34)
     * $query->filterByRetardJours(array('min' => 12)); // WHERE retard_jours > 12
     * </code>
     *
     * @param     mixed $retardJours The value to use as filter.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildPrestationQuery The current query, for fluid interface
     */
    public function filterByRetardJours($retardJours = null, $comparison = null)
    {
        if (is_array($retardJours)) {
            $useMinMax = false;
            if (isset($retardJours['min'])) {
                $this->addUsingAlias(PrestationTableMap::COL_RETARD_JOURS, $retardJours['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($retardJours['max'])) {
                $this->addUsingAlias(PrestationTableMap::COL_RETARD_JOURS, $retardJours['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(PrestationTableMap::COL_RETARD_JOURS, $retardJours, $comparison);
    }

    /**
     * Filter the query on the nombre_numero column
     *
     * Example usage:
     * <code>
     * $query->filterByNombreNumero(1234); // WHERE nombre_numero = 1234
     * $query->filterByNombreNumero(array(12, 34)); // WHERE nombre_numero IN (12, 34)
     * $query->filterByNombreNumero(array('min' => 12)); // WHERE nombre_numero > 12
     * </code>
     *
     * @param     mixed $nombreNumero The value to use as filter.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildPrestationQuery The current query, for fluid interface
     */
    public function filterByNombreNumero($nombreNumero = null, $comparison = null)
    {
        if (is_array($nombreNumero)) {
            $useMinMax = false;
            if (isset($nombreNumero['min'])) {
                $this->addUsingAlias(PrestationTableMap::COL_NOMBRE_NUMERO, $nombreNumero['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($nombreNumero['max'])) {
                $this->addUsingAlias(PrestationTableMap::COL_NOMBRE_NUMERO, $nombreNumero['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(PrestationTableMap::COL_NOMBRE_NUMERO, $nombreNumero, $comparison);
    }

    /**
     * Filter the query on the prochain_numero column
     *
     * Example usage:
     * <code>
     * $query->filterByProchainNumero(1234); // WHERE prochain_numero = 1234
     * $query->filterByProchainNumero(array(12, 34)); // WHERE prochain_numero IN (12, 34)
     * $query->filterByProchainNumero(array('min' => 12)); // WHERE prochain_numero > 12
     * </code>
     *
     * @param     mixed $prochainNumero The value to use as filter.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildPrestationQuery The current query, for fluid interface
     */
    public function filterByProchainNumero($prochainNumero = null, $comparison = null)
    {
        if (is_array($prochainNumero)) {
            $useMinMax = false;
            if (isset($prochainNumero['min'])) {
                $this->addUsingAlias(PrestationTableMap::COL_PROCHAIN_NUMERO, $prochainNumero['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($prochainNumero['max'])) {
                $this->addUsingAlias(PrestationTableMap::COL_PROCHAIN_NUMERO, $prochainNumero['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(PrestationTableMap::COL_PROCHAIN_NUMERO, $prochainNumero, $comparison);
    }

    /**
     * Filter the query on the prixlibre column
     *
     * Example usage:
     * <code>
     * $query->filterByPrixlibre(true); // WHERE prixlibre = true
     * $query->filterByPrixlibre('yes'); // WHERE prixlibre = true
     * </code>
     *
     * @param     boolean|string $prixlibre The value to use as filter.
     *              Non-boolean arguments are converted using the following rules:
     *                * 1, '1', 'true',  'on',  and 'yes' are converted to boolean true
     *                * 0, '0', 'false', 'off', and 'no'  are converted to boolean false
     *              Check on string values is case insensitive (so 'FaLsE' is seen as 'false').
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildPrestationQuery The current query, for fluid interface
     */
    public function filterByPrixlibre($prixlibre = null, $comparison = null)
    {
        if (is_string($prixlibre)) {
            $prixlibre = in_array(strtolower($prixlibre), array('false', 'off', '-', 'no', 'n', '0', '')) ? false : true;
        }

        return $this->addUsingAlias(PrestationTableMap::COL_PRIXLIBRE, $prixlibre, $comparison);
    }

    /**
     * Filter the query on the quantite column
     *
     * Example usage:
     * <code>
     * $query->filterByQuantite(true); // WHERE quantite = true
     * $query->filterByQuantite('yes'); // WHERE quantite = true
     * </code>
     *
     * @param     boolean|string $quantite The value to use as filter.
     *              Non-boolean arguments are converted using the following rules:
     *                * 1, '1', 'true',  'on',  and 'yes' are converted to boolean true
     *                * 0, '0', 'false', 'off', and 'no'  are converted to boolean false
     *              Check on string values is case insensitive (so 'FaLsE' is seen as 'false').
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildPrestationQuery The current query, for fluid interface
     */
    public function filterByQuantite($quantite = null, $comparison = null)
    {
        if (is_string($quantite)) {
            $quantite = in_array(strtolower($quantite), array('false', 'off', '-', 'no', 'n', '0', '')) ? false : true;
        }

        return $this->addUsingAlias(PrestationTableMap::COL_QUANTITE, $quantite, $comparison);
    }

    /**
     * Filter the query on the id_unite column
     *
     * Example usage:
     * <code>
     * $query->filterByIdUnite(1234); // WHERE id_unite = 1234
     * $query->filterByIdUnite(array(12, 34)); // WHERE id_unite IN (12, 34)
     * $query->filterByIdUnite(array('min' => 12)); // WHERE id_unite > 12
     * </code>
     *
     * @param     mixed $idUnite The value to use as filter.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildPrestationQuery The current query, for fluid interface
     */
    public function filterByIdUnite($idUnite = null, $comparison = null)
    {
        if (is_array($idUnite)) {
            $useMinMax = false;
            if (isset($idUnite['min'])) {
                $this->addUsingAlias(PrestationTableMap::COL_ID_UNITE, $idUnite['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($idUnite['max'])) {
                $this->addUsingAlias(PrestationTableMap::COL_ID_UNITE, $idUnite['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(PrestationTableMap::COL_ID_UNITE, $idUnite, $comparison);
    }

    /**
     * Filter the query on the id_compte column
     *
     * Example usage:
     * <code>
     * $query->filterByIdCompte(1234); // WHERE id_compte = 1234
     * $query->filterByIdCompte(array(12, 34)); // WHERE id_compte IN (12, 34)
     * $query->filterByIdCompte(array('min' => 12)); // WHERE id_compte > 12
     * </code>
     *
     * @see       filterByCompte()
     *
     * @param     mixed $idCompte The value to use as filter.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildPrestationQuery The current query, for fluid interface
     */
    public function filterByIdCompte($idCompte = null, $comparison = null)
    {
        if (is_array($idCompte)) {
            $useMinMax = false;
            if (isset($idCompte['min'])) {
                $this->addUsingAlias(PrestationTableMap::COL_ID_COMPTE, $idCompte['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($idCompte['max'])) {
                $this->addUsingAlias(PrestationTableMap::COL_ID_COMPTE, $idCompte['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(PrestationTableMap::COL_ID_COMPTE, $idCompte, $comparison);
    }

    /**
     * Filter the query on the prestation_type column
     *
     * Example usage:
     * <code>
     * $query->filterByPrestationType(1234); // WHERE prestation_type = 1234
     * $query->filterByPrestationType(array(12, 34)); // WHERE prestation_type IN (12, 34)
     * $query->filterByPrestationType(array('min' => 12)); // WHERE prestation_type > 12
     * </code>
     *
     * @param     mixed $prestationType The value to use as filter.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildPrestationQuery The current query, for fluid interface
     */
    public function filterByPrestationType($prestationType = null, $comparison = null)
    {
        if (is_array($prestationType)) {
            $useMinMax = false;
            if (isset($prestationType['min'])) {
                $this->addUsingAlias(PrestationTableMap::COL_PRESTATION_TYPE, $prestationType['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($prestationType['max'])) {
                $this->addUsingAlias(PrestationTableMap::COL_PRESTATION_TYPE, $prestationType['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(PrestationTableMap::COL_PRESTATION_TYPE, $prestationType, $comparison);
    }

    /**
     * Filter the query on the active column
     *
     * Example usage:
     * <code>
     * $query->filterByActive(true); // WHERE active = true
     * $query->filterByActive('yes'); // WHERE active = true
     * </code>
     *
     * @param     boolean|string $active The value to use as filter.
     *              Non-boolean arguments are converted using the following rules:
     *                * 1, '1', 'true',  'on',  and 'yes' are converted to boolean true
     *                * 0, '0', 'false', 'off', and 'no'  are converted to boolean false
     *              Check on string values is case insensitive (so 'FaLsE' is seen as 'false').
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildPrestationQuery The current query, for fluid interface
     */
    public function filterByActive($active = null, $comparison = null)
    {
        if (is_string($active)) {
            $active = in_array(strtolower($active), array('false', 'off', '-', 'no', 'n', '0', '')) ? false : true;
        }

        return $this->addUsingAlias(PrestationTableMap::COL_ACTIVE, $active, $comparison);
    }

    /**
     * Filter the query on the nb_voix column
     *
     * Example usage:
     * <code>
     * $query->filterByNbVoix(1234); // WHERE nb_voix = 1234
     * $query->filterByNbVoix(array(12, 34)); // WHERE nb_voix IN (12, 34)
     * $query->filterByNbVoix(array('min' => 12)); // WHERE nb_voix > 12
     * </code>
     *
     * @param     mixed $nbVoix The value to use as filter.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildPrestationQuery The current query, for fluid interface
     */
    public function filterByNbVoix($nbVoix = null, $comparison = null)
    {
        if (is_array($nbVoix)) {
            $useMinMax = false;
            if (isset($nbVoix['min'])) {
                $this->addUsingAlias(PrestationTableMap::COL_NB_VOIX, $nbVoix['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($nbVoix['max'])) {
                $this->addUsingAlias(PrestationTableMap::COL_NB_VOIX, $nbVoix['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(PrestationTableMap::COL_NB_VOIX, $nbVoix, $comparison);
    }

    /**
     * Filter the query on the periodique column
     *
     * Example usage:
     * <code>
     * $query->filterByPeriodique('fooValue');   // WHERE periodique = 'fooValue'
     * $query->filterByPeriodique('%fooValue%', Criteria::LIKE); // WHERE periodique LIKE '%fooValue%'
     * </code>
     *
     * @param     string $periodique The value to use as filter.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildPrestationQuery The current query, for fluid interface
     */
    public function filterByPeriodique($periodique = null, $comparison = null)
    {
        if (null === $comparison) {
            if (is_array($periodique)) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(PrestationTableMap::COL_PERIODIQUE, $periodique, $comparison);
    }

    /**
     * Filter the query on the signe column
     *
     * Example usage:
     * <code>
     * $query->filterBySigne('fooValue');   // WHERE signe = 'fooValue'
     * $query->filterBySigne('%fooValue%', Criteria::LIKE); // WHERE signe LIKE '%fooValue%'
     * </code>
     *
     * @param     string $signe The value to use as filter.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildPrestationQuery The current query, for fluid interface
     */
    public function filterBySigne($signe = null, $comparison = null)
    {
        if (null === $comparison) {
            if (is_array($signe)) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(PrestationTableMap::COL_SIGNE, $signe, $comparison);
    }

    /**
     * Filter the query on the objet_beneficiaire column
     *
     * Example usage:
     * <code>
     * $query->filterByObjetBeneficiaire('fooValue');   // WHERE objet_beneficiaire = 'fooValue'
     * $query->filterByObjetBeneficiaire('%fooValue%', Criteria::LIKE); // WHERE objet_beneficiaire LIKE '%fooValue%'
     * </code>
     *
     * @param     string $objetBeneficiaire The value to use as filter.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildPrestationQuery The current query, for fluid interface
     */
    public function filterByObjetBeneficiaire($objetBeneficiaire = null, $comparison = null)
    {
        if (null === $comparison) {
            if (is_array($objetBeneficiaire)) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(PrestationTableMap::COL_OBJET_BENEFICIAIRE, $objetBeneficiaire, $comparison);
    }

    /**
     * Filter the query on the created_at column
     *
     * Example usage:
     * <code>
     * $query->filterByCreatedAt('2011-03-14'); // WHERE created_at = '2011-03-14'
     * $query->filterByCreatedAt('now'); // WHERE created_at = '2011-03-14'
     * $query->filterByCreatedAt(array('max' => 'yesterday')); // WHERE created_at > '2011-03-13'
     * </code>
     *
     * @param     mixed $createdAt The value to use as filter.
     *              Values can be integers (unix timestamps), DateTime objects, or strings.
     *              Empty strings are treated as NULL.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildPrestationQuery The current query, for fluid interface
     */
    public function filterByCreatedAt($createdAt = null, $comparison = null)
    {
        if (is_array($createdAt)) {
            $useMinMax = false;
            if (isset($createdAt['min'])) {
                $this->addUsingAlias(PrestationTableMap::COL_CREATED_AT, $createdAt['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($createdAt['max'])) {
                $this->addUsingAlias(PrestationTableMap::COL_CREATED_AT, $createdAt['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(PrestationTableMap::COL_CREATED_AT, $createdAt, $comparison);
    }

    /**
     * Filter the query on the updated_at column
     *
     * Example usage:
     * <code>
     * $query->filterByUpdatedAt('2011-03-14'); // WHERE updated_at = '2011-03-14'
     * $query->filterByUpdatedAt('now'); // WHERE updated_at = '2011-03-14'
     * $query->filterByUpdatedAt(array('max' => 'yesterday')); // WHERE updated_at > '2011-03-13'
     * </code>
     *
     * @param     mixed $updatedAt The value to use as filter.
     *              Values can be integers (unix timestamps), DateTime objects, or strings.
     *              Empty strings are treated as NULL.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildPrestationQuery The current query, for fluid interface
     */
    public function filterByUpdatedAt($updatedAt = null, $comparison = null)
    {
        if (is_array($updatedAt)) {
            $useMinMax = false;
            if (isset($updatedAt['min'])) {
                $this->addUsingAlias(PrestationTableMap::COL_UPDATED_AT, $updatedAt['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($updatedAt['max'])) {
                $this->addUsingAlias(PrestationTableMap::COL_UPDATED_AT, $updatedAt['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(PrestationTableMap::COL_UPDATED_AT, $updatedAt, $comparison);
    }

    /**
     * Filter the query by a related \Entite object
     *
     * @param \Entite|ObjectCollection $entite The related object(s) to use as filter
     * @param string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @throws \Propel\Runtime\Exception\PropelException
     *
     * @return ChildPrestationQuery The current query, for fluid interface
     */
    public function filterByEntite($entite, $comparison = null)
    {
        if ($entite instanceof \Entite) {
            return $this
                ->addUsingAlias(PrestationTableMap::COL_ID_ENTITE, $entite->getIdEntite(), $comparison);
        } elseif ($entite instanceof ObjectCollection) {
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }

            return $this
                ->addUsingAlias(PrestationTableMap::COL_ID_ENTITE, $entite->toKeyValue('PrimaryKey', 'IdEntite'), $comparison);
        } else {
            throw new PropelException('filterByEntite() only accepts arguments of type \Entite or Collection');
        }
    }

    /**
     * Adds a JOIN clause to the query using the Entite relation
     *
     * @param     string $relationAlias optional alias for the relation
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return $this|ChildPrestationQuery The current query, for fluid interface
     */
    public function joinEntite($relationAlias = null, $joinType = Criteria::INNER_JOIN)
    {
        $tableMap = $this->getTableMap();
        $relationMap = $tableMap->getRelation('Entite');

        // create a ModelJoin object for this join
        $join = new ModelJoin();
        $join->setJoinType($joinType);
        $join->setRelationMap($relationMap, $this->useAliasInSQL ? $this->getModelAlias() : null, $relationAlias);
        if ($previousJoin = $this->getPreviousJoin()) {
            $join->setPreviousJoin($previousJoin);
        }

        // add the ModelJoin to the current object
        if ($relationAlias) {
            $this->addAlias($relationAlias, $relationMap->getRightTable()->getName());
            $this->addJoinObject($join, $relationAlias);
        } else {
            $this->addJoinObject($join, 'Entite');
        }

        return $this;
    }

    /**
     * Use the Entite relation Entite object
     *
     * @see useQuery()
     *
     * @param     string $relationAlias optional alias for the relation,
     *                                   to be used as main alias in the secondary query
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return \EntiteQuery A secondary query class using the current class as primary query
     */
    public function useEntiteQuery($relationAlias = null, $joinType = Criteria::INNER_JOIN)
    {
        return $this
            ->joinEntite($relationAlias, $joinType)
            ->useQuery($relationAlias ? $relationAlias : 'Entite', '\EntiteQuery');
    }

    /**
     * Filter the query by a related \Compte object
     *
     * @param \Compte|ObjectCollection $compte The related object(s) to use as filter
     * @param string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @throws \Propel\Runtime\Exception\PropelException
     *
     * @return ChildPrestationQuery The current query, for fluid interface
     */
    public function filterByCompte($compte, $comparison = null)
    {
        if ($compte instanceof \Compte) {
            return $this
                ->addUsingAlias(PrestationTableMap::COL_ID_COMPTE, $compte->getIdCompte(), $comparison);
        } elseif ($compte instanceof ObjectCollection) {
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }

            return $this
                ->addUsingAlias(PrestationTableMap::COL_ID_COMPTE, $compte->toKeyValue('PrimaryKey', 'IdCompte'), $comparison);
        } else {
            throw new PropelException('filterByCompte() only accepts arguments of type \Compte or Collection');
        }
    }

    /**
     * Adds a JOIN clause to the query using the Compte relation
     *
     * @param     string $relationAlias optional alias for the relation
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return $this|ChildPrestationQuery The current query, for fluid interface
     */
    public function joinCompte($relationAlias = null, $joinType = Criteria::LEFT_JOIN)
    {
        $tableMap = $this->getTableMap();
        $relationMap = $tableMap->getRelation('Compte');

        // create a ModelJoin object for this join
        $join = new ModelJoin();
        $join->setJoinType($joinType);
        $join->setRelationMap($relationMap, $this->useAliasInSQL ? $this->getModelAlias() : null, $relationAlias);
        if ($previousJoin = $this->getPreviousJoin()) {
            $join->setPreviousJoin($previousJoin);
        }

        // add the ModelJoin to the current object
        if ($relationAlias) {
            $this->addAlias($relationAlias, $relationMap->getRightTable()->getName());
            $this->addJoinObject($join, $relationAlias);
        } else {
            $this->addJoinObject($join, 'Compte');
        }

        return $this;
    }

    /**
     * Use the Compte relation Compte object
     *
     * @see useQuery()
     *
     * @param     string $relationAlias optional alias for the relation,
     *                                   to be used as main alias in the secondary query
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return \CompteQuery A secondary query class using the current class as primary query
     */
    public function useCompteQuery($relationAlias = null, $joinType = Criteria::LEFT_JOIN)
    {
        return $this
            ->joinCompte($relationAlias, $joinType)
            ->useQuery($relationAlias ? $relationAlias : 'Compte', '\CompteQuery');
    }

    /**
     * Filter the query by a related \Prix object
     *
     * @param \Prix|ObjectCollection $prix the related object to use as filter
     * @param string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return ChildPrestationQuery The current query, for fluid interface
     */
    public function filterByPrix($prix, $comparison = null)
    {
        if ($prix instanceof \Prix) {
            return $this
                ->addUsingAlias(PrestationTableMap::COL_ID_PRESTATION, $prix->getIdPrestation(), $comparison);
        } elseif ($prix instanceof ObjectCollection) {
            return $this
                ->usePrixQuery()
                ->filterByPrimaryKeys($prix->getPrimaryKeys())
                ->endUse();
        } else {
            throw new PropelException('filterByPrix() only accepts arguments of type \Prix or Collection');
        }
    }

    /**
     * Adds a JOIN clause to the query using the Prix relation
     *
     * @param     string $relationAlias optional alias for the relation
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return $this|ChildPrestationQuery The current query, for fluid interface
     */
    public function joinPrix($relationAlias = null, $joinType = Criteria::INNER_JOIN)
    {
        $tableMap = $this->getTableMap();
        $relationMap = $tableMap->getRelation('Prix');

        // create a ModelJoin object for this join
        $join = new ModelJoin();
        $join->setJoinType($joinType);
        $join->setRelationMap($relationMap, $this->useAliasInSQL ? $this->getModelAlias() : null, $relationAlias);
        if ($previousJoin = $this->getPreviousJoin()) {
            $join->setPreviousJoin($previousJoin);
        }

        // add the ModelJoin to the current object
        if ($relationAlias) {
            $this->addAlias($relationAlias, $relationMap->getRightTable()->getName());
            $this->addJoinObject($join, $relationAlias);
        } else {
            $this->addJoinObject($join, 'Prix');
        }

        return $this;
    }

    /**
     * Use the Prix relation Prix object
     *
     * @see useQuery()
     *
     * @param     string $relationAlias optional alias for the relation,
     *                                   to be used as main alias in the secondary query
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return \PrixQuery A secondary query class using the current class as primary query
     */
    public function usePrixQuery($relationAlias = null, $joinType = Criteria::INNER_JOIN)
    {
        return $this
            ->joinPrix($relationAlias, $joinType)
            ->useQuery($relationAlias ? $relationAlias : 'Prix', '\PrixQuery');
    }

    /**
     * Filter the query by a related \PrestationslotPrestation object
     *
     * @param \PrestationslotPrestation|ObjectCollection $prestationslotPrestation the related object to use as filter
     * @param string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return ChildPrestationQuery The current query, for fluid interface
     */
    public function filterByPrestationslotPrestation($prestationslotPrestation, $comparison = null)
    {
        if ($prestationslotPrestation instanceof \PrestationslotPrestation) {
            return $this
                ->addUsingAlias(PrestationTableMap::COL_ID_PRESTATION, $prestationslotPrestation->getIdPrestation(), $comparison);
        } elseif ($prestationslotPrestation instanceof ObjectCollection) {
            return $this
                ->usePrestationslotPrestationQuery()
                ->filterByPrimaryKeys($prestationslotPrestation->getPrimaryKeys())
                ->endUse();
        } else {
            throw new PropelException('filterByPrestationslotPrestation() only accepts arguments of type \PrestationslotPrestation or Collection');
        }
    }

    /**
     * Adds a JOIN clause to the query using the PrestationslotPrestation relation
     *
     * @param     string $relationAlias optional alias for the relation
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return $this|ChildPrestationQuery The current query, for fluid interface
     */
    public function joinPrestationslotPrestation($relationAlias = null, $joinType = Criteria::INNER_JOIN)
    {
        $tableMap = $this->getTableMap();
        $relationMap = $tableMap->getRelation('PrestationslotPrestation');

        // create a ModelJoin object for this join
        $join = new ModelJoin();
        $join->setJoinType($joinType);
        $join->setRelationMap($relationMap, $this->useAliasInSQL ? $this->getModelAlias() : null, $relationAlias);
        if ($previousJoin = $this->getPreviousJoin()) {
            $join->setPreviousJoin($previousJoin);
        }

        // add the ModelJoin to the current object
        if ($relationAlias) {
            $this->addAlias($relationAlias, $relationMap->getRightTable()->getName());
            $this->addJoinObject($join, $relationAlias);
        } else {
            $this->addJoinObject($join, 'PrestationslotPrestation');
        }

        return $this;
    }

    /**
     * Use the PrestationslotPrestation relation PrestationslotPrestation object
     *
     * @see useQuery()
     *
     * @param     string $relationAlias optional alias for the relation,
     *                                   to be used as main alias in the secondary query
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return \PrestationslotPrestationQuery A secondary query class using the current class as primary query
     */
    public function usePrestationslotPrestationQuery($relationAlias = null, $joinType = Criteria::INNER_JOIN)
    {
        return $this
            ->joinPrestationslotPrestation($relationAlias, $joinType)
            ->useQuery($relationAlias ? $relationAlias : 'PrestationslotPrestation', '\PrestationslotPrestationQuery');
    }

    /**
     * Filter the query by a related \Servicerendu object
     *
     * @param \Servicerendu|ObjectCollection $servicerendu the related object to use as filter
     * @param string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return ChildPrestationQuery The current query, for fluid interface
     */
    public function filterByServicerendu($servicerendu, $comparison = null)
    {
        if ($servicerendu instanceof \Servicerendu) {
            return $this
                ->addUsingAlias(PrestationTableMap::COL_ID_PRESTATION, $servicerendu->getIdPrestation(), $comparison);
        } elseif ($servicerendu instanceof ObjectCollection) {
            return $this
                ->useServicerenduQuery()
                ->filterByPrimaryKeys($servicerendu->getPrimaryKeys())
                ->endUse();
        } else {
            throw new PropelException('filterByServicerendu() only accepts arguments of type \Servicerendu or Collection');
        }
    }

    /**
     * Adds a JOIN clause to the query using the Servicerendu relation
     *
     * @param     string $relationAlias optional alias for the relation
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return $this|ChildPrestationQuery The current query, for fluid interface
     */
    public function joinServicerendu($relationAlias = null, $joinType = Criteria::INNER_JOIN)
    {
        $tableMap = $this->getTableMap();
        $relationMap = $tableMap->getRelation('Servicerendu');

        // create a ModelJoin object for this join
        $join = new ModelJoin();
        $join->setJoinType($joinType);
        $join->setRelationMap($relationMap, $this->useAliasInSQL ? $this->getModelAlias() : null, $relationAlias);
        if ($previousJoin = $this->getPreviousJoin()) {
            $join->setPreviousJoin($previousJoin);
        }

        // add the ModelJoin to the current object
        if ($relationAlias) {
            $this->addAlias($relationAlias, $relationMap->getRightTable()->getName());
            $this->addJoinObject($join, $relationAlias);
        } else {
            $this->addJoinObject($join, 'Servicerendu');
        }

        return $this;
    }

    /**
     * Use the Servicerendu relation Servicerendu object
     *
     * @see useQuery()
     *
     * @param     string $relationAlias optional alias for the relation,
     *                                   to be used as main alias in the secondary query
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return \ServicerenduQuery A secondary query class using the current class as primary query
     */
    public function useServicerenduQuery($relationAlias = null, $joinType = Criteria::INNER_JOIN)
    {
        return $this
            ->joinServicerendu($relationAlias, $joinType)
            ->useQuery($relationAlias ? $relationAlias : 'Servicerendu', '\ServicerenduQuery');
    }

    /**
     * Filter the query by a related Prestationslot object
     * using the asso_prestationslots_prestations table as cross reference
     *
     * @param Prestationslot $prestationslot the related object to use as filter
     * @param string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return ChildPrestationQuery The current query, for fluid interface
     */
    public function filterByPrestationslot($prestationslot, $comparison = Criteria::EQUAL)
    {
        return $this
            ->usePrestationslotPrestationQuery()
            ->filterByPrestationslot($prestationslot, $comparison)
            ->endUse();
    }

    /**
     * Exclude object from result
     *
     * @param   ChildPrestation $prestation Object to remove from the list of results
     *
     * @return $this|ChildPrestationQuery The current query, for fluid interface
     */
    public function prune($prestation = null)
    {
        if ($prestation) {
            $this->addUsingAlias(PrestationTableMap::COL_ID_PRESTATION, $prestation->getIdPrestation(), Criteria::NOT_EQUAL);
        }

        return $this;
    }

    /**
     * Code to execute before every DELETE statement
     *
     * @param     ConnectionInterface $con The connection object used by the query
     */
    protected function basePreDelete(ConnectionInterface $con)
    {
        // archivable behavior

        if ($this->archiveOnDelete) {
            $this->archive($con);
        } else {
            $this->archiveOnDelete = true;
        }


        return $this->preDelete($con);
    }

    /**
     * Deletes all rows from the asso_prestations table.
     *
     * @param ConnectionInterface $con the connection to use
     * @return int The number of affected rows (if supported by underlying database driver).
     */
    public function doDeleteAll(ConnectionInterface $con = null)
    {
        if (null === $con) {
            $con = Propel::getServiceContainer()->getWriteConnection(PrestationTableMap::DATABASE_NAME);
        }

        // use transaction because $criteria could contain info
        // for more than one table or we could emulating ON DELETE CASCADE, etc.
        return $con->transaction(function () use ($con) {
            $affectedRows = 0; // initialize var to track total num of affected rows
            $affectedRows += parent::doDeleteAll($con);
            // Because this db requires some delete cascade/set null emulation, we have to
            // clear the cached instance *after* the emulation has happened (since
            // instances get re-added by the select statement contained therein).
            PrestationTableMap::clearInstancePool();
            PrestationTableMap::clearRelatedInstancePool();

            return $affectedRows;
        });
    }

    /**
     * Performs a DELETE on the database based on the current ModelCriteria
     *
     * @param ConnectionInterface $con the connection to use
     * @return int             The number of affected rows (if supported by underlying database driver).  This includes CASCADE-related rows
     *                         if supported by native driver or if emulated using Propel.
     * @throws PropelException Any exceptions caught during processing will be
     *                         rethrown wrapped into a PropelException.
     */
    public function delete(ConnectionInterface $con = null)
    {
        if (null === $con) {
            $con = Propel::getServiceContainer()->getWriteConnection(PrestationTableMap::DATABASE_NAME);
        }

        $criteria = $this;

        // Set the correct dbName
        $criteria->setDbName(PrestationTableMap::DATABASE_NAME);

        // use transaction because $criteria could contain info
        // for more than one table or we could emulating ON DELETE CASCADE, etc.
        return $con->transaction(function () use ($con, $criteria) {
            $affectedRows = 0; // initialize var to track total num of affected rows

            PrestationTableMap::removeInstanceFromPool($criteria);

            $affectedRows += ModelCriteria::delete($con);
            PrestationTableMap::clearRelatedInstancePool();

            return $affectedRows;
        });
    }

    // timestampable behavior

    /**
     * Filter by the latest updated
     *
     * @param      int $nbDays Maximum age of the latest update in days
     *
     * @return     $this|ChildPrestationQuery The current query, for fluid interface
     */
    public function recentlyUpdated($nbDays = 7)
    {
        return $this->addUsingAlias(PrestationTableMap::COL_UPDATED_AT, time() - $nbDays * 24 * 60 * 60, Criteria::GREATER_EQUAL);
    }

    /**
     * Order by update date desc
     *
     * @return     $this|ChildPrestationQuery The current query, for fluid interface
     */
    public function lastUpdatedFirst()
    {
        return $this->addDescendingOrderByColumn(PrestationTableMap::COL_UPDATED_AT);
    }

    /**
     * Order by update date asc
     *
     * @return     $this|ChildPrestationQuery The current query, for fluid interface
     */
    public function firstUpdatedFirst()
    {
        return $this->addAscendingOrderByColumn(PrestationTableMap::COL_UPDATED_AT);
    }

    /**
     * Order by create date desc
     *
     * @return     $this|ChildPrestationQuery The current query, for fluid interface
     */
    public function lastCreatedFirst()
    {
        return $this->addDescendingOrderByColumn(PrestationTableMap::COL_CREATED_AT);
    }

    /**
     * Filter by the latest created
     *
     * @param      int $nbDays Maximum age of in days
     *
     * @return     $this|ChildPrestationQuery The current query, for fluid interface
     */
    public function recentlyCreated($nbDays = 7)
    {
        return $this->addUsingAlias(PrestationTableMap::COL_CREATED_AT, time() - $nbDays * 24 * 60 * 60, Criteria::GREATER_EQUAL);
    }

    /**
     * Order by create date asc
     *
     * @return     $this|ChildPrestationQuery The current query, for fluid interface
     */
    public function firstCreatedFirst()
    {
        return $this->addAscendingOrderByColumn(PrestationTableMap::COL_CREATED_AT);
    }

    // archivable behavior

    /**
     * Copy the data of the objects satisfying the query into ChildAssoPrestationsArchive archive objects.
     * The archived objects are then saved.
     * If any of the objects has already been archived, the archived object
     * is updated and not duplicated.
     * Warning: This termination methods issues 2n+1 queries.
     *
     * @param      ConnectionInterface $con    Connection to use.
     * @param      Boolean $useLittleMemory    Whether or not to use OnDemandFormatter to retrieve objects.
     *               Set to false if the identity map matters.
     *               Set to true (default) to use less memory.
     *
     * @return     int the number of archived objects
     */
    public function archive($con = null, $useLittleMemory = true)
    {
        $criteria = clone $this;
        // prepare the query
        $criteria->setWith(array());
        if ($useLittleMemory) {
            $criteria->setFormatter(ModelCriteria::FORMAT_ON_DEMAND);
        }
        if ($con === null) {
            $con = Propel::getServiceContainer()->getWriteConnection(PrestationTableMap::DATABASE_NAME);
        }

        return $con->transaction(function () use ($con, $criteria) {
            $totalArchivedObjects = 0;

            // archive all results one by one
            foreach ($criteria->find($con) as $object) {
                $object->archive($con);
                $totalArchivedObjects++;
            }

            return $totalArchivedObjects;
        });
    }

    /**
     * Enable/disable auto-archiving on delete for the next query.
     *
     * @param boolean True if the query must archive deleted objects, false otherwise.
     */
    public function setArchiveOnDelete($archiveOnDelete)
    {
        $this->archiveOnDelete = $archiveOnDelete;
    }

    /**
     * Delete records matching the current query without archiving them.
     *
     * @param      ConnectionInterface $con    Connection to use.
     *
     * @return integer the number of deleted rows
     */
    public function deleteWithoutArchive($con = null)
    {
        $this->archiveOnDelete = false;

        return $this->delete($con);
    }

    /**
     * Delete all records without archiving them.
     *
     * @param      ConnectionInterface $con    Connection to use.
     *
     * @return integer the number of deleted rows
     */
    public function deleteAllWithoutArchive($con = null)
    {
        $this->archiveOnDelete = false;

        return $this->deleteAll($con);
    }

} // PrestationQuery
