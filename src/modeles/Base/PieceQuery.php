<?php

namespace Base;

use \AsscPiecesArchive as ChildAsscPiecesArchive;
use \Piece as ChildPiece;
use \PieceQuery as ChildPieceQuery;
use \Exception;
use \PDO;
use Map\PieceTableMap;
use Propel\Runtime\Propel;
use Propel\Runtime\ActiveQuery\Criteria;
use Propel\Runtime\ActiveQuery\ModelCriteria;
use Propel\Runtime\ActiveQuery\ModelJoin;
use Propel\Runtime\Collection\ObjectCollection;
use Propel\Runtime\Connection\ConnectionInterface;
use Propel\Runtime\Exception\PropelException;

/**
 * Base class that represents a query for the 'assc_pieces' table.
 *
 *
 *
 * @method     ChildPieceQuery orderByIdPiece($order = Criteria::ASC) Order by the id_piece column
 * @method     ChildPieceQuery orderByIdEcriture($order = Criteria::ASC) Order by the id_ecriture column
 * @method     ChildPieceQuery orderByClassement($order = Criteria::ASC) Order by the classement column
 * @method     ChildPieceQuery orderByNom($order = Criteria::ASC) Order by the nom column
 * @method     ChildPieceQuery orderByIdJournal($order = Criteria::ASC) Order by the id_journal column
 * @method     ChildPieceQuery orderByIdEntite($order = Criteria::ASC) Order by the id_entite column
 * @method     ChildPieceQuery orderByDatePiece($order = Criteria::ASC) Order by the date_piece column
 * @method     ChildPieceQuery orderByObservation($order = Criteria::ASC) Order by the observation column
 * @method     ChildPieceQuery orderByEcranSaisie($order = Criteria::ASC) Order by the ecran_saisie column
 * @method     ChildPieceQuery orderByEtat($order = Criteria::ASC) Order by the etat column
 * @method     ChildPieceQuery orderByNbLigne($order = Criteria::ASC) Order by the nb_ligne column
 * @method     ChildPieceQuery orderByCreatedAt($order = Criteria::ASC) Order by the created_at column
 * @method     ChildPieceQuery orderByUpdatedAt($order = Criteria::ASC) Order by the updated_at column
 *
 * @method     ChildPieceQuery groupByIdPiece() Group by the id_piece column
 * @method     ChildPieceQuery groupByIdEcriture() Group by the id_ecriture column
 * @method     ChildPieceQuery groupByClassement() Group by the classement column
 * @method     ChildPieceQuery groupByNom() Group by the nom column
 * @method     ChildPieceQuery groupByIdJournal() Group by the id_journal column
 * @method     ChildPieceQuery groupByIdEntite() Group by the id_entite column
 * @method     ChildPieceQuery groupByDatePiece() Group by the date_piece column
 * @method     ChildPieceQuery groupByObservation() Group by the observation column
 * @method     ChildPieceQuery groupByEcranSaisie() Group by the ecran_saisie column
 * @method     ChildPieceQuery groupByEtat() Group by the etat column
 * @method     ChildPieceQuery groupByNbLigne() Group by the nb_ligne column
 * @method     ChildPieceQuery groupByCreatedAt() Group by the created_at column
 * @method     ChildPieceQuery groupByUpdatedAt() Group by the updated_at column
 *
 * @method     ChildPieceQuery leftJoin($relation) Adds a LEFT JOIN clause to the query
 * @method     ChildPieceQuery rightJoin($relation) Adds a RIGHT JOIN clause to the query
 * @method     ChildPieceQuery innerJoin($relation) Adds a INNER JOIN clause to the query
 *
 * @method     ChildPieceQuery leftJoinWith($relation) Adds a LEFT JOIN clause and with to the query
 * @method     ChildPieceQuery rightJoinWith($relation) Adds a RIGHT JOIN clause and with to the query
 * @method     ChildPieceQuery innerJoinWith($relation) Adds a INNER JOIN clause and with to the query
 *
 * @method     ChildPieceQuery leftJoinJournal($relationAlias = null) Adds a LEFT JOIN clause to the query using the Journal relation
 * @method     ChildPieceQuery rightJoinJournal($relationAlias = null) Adds a RIGHT JOIN clause to the query using the Journal relation
 * @method     ChildPieceQuery innerJoinJournal($relationAlias = null) Adds a INNER JOIN clause to the query using the Journal relation
 *
 * @method     ChildPieceQuery joinWithJournal($joinType = Criteria::INNER_JOIN) Adds a join clause and with to the query using the Journal relation
 *
 * @method     ChildPieceQuery leftJoinWithJournal() Adds a LEFT JOIN clause and with to the query using the Journal relation
 * @method     ChildPieceQuery rightJoinWithJournal() Adds a RIGHT JOIN clause and with to the query using the Journal relation
 * @method     ChildPieceQuery innerJoinWithJournal() Adds a INNER JOIN clause and with to the query using the Journal relation
 *
 * @method     ChildPieceQuery leftJoinEntite($relationAlias = null) Adds a LEFT JOIN clause to the query using the Entite relation
 * @method     ChildPieceQuery rightJoinEntite($relationAlias = null) Adds a RIGHT JOIN clause to the query using the Entite relation
 * @method     ChildPieceQuery innerJoinEntite($relationAlias = null) Adds a INNER JOIN clause to the query using the Entite relation
 *
 * @method     ChildPieceQuery joinWithEntite($joinType = Criteria::INNER_JOIN) Adds a join clause and with to the query using the Entite relation
 *
 * @method     ChildPieceQuery leftJoinWithEntite() Adds a LEFT JOIN clause and with to the query using the Entite relation
 * @method     ChildPieceQuery rightJoinWithEntite() Adds a RIGHT JOIN clause and with to the query using the Entite relation
 * @method     ChildPieceQuery innerJoinWithEntite() Adds a INNER JOIN clause and with to the query using the Entite relation
 *
 * @method     \JournalQuery|\EntiteQuery endUse() Finalizes a secondary criteria and merges it with its primary Criteria
 *
 * @method     ChildPiece findOne(ConnectionInterface $con = null) Return the first ChildPiece matching the query
 * @method     ChildPiece findOneOrCreate(ConnectionInterface $con = null) Return the first ChildPiece matching the query, or a new ChildPiece object populated from the query conditions when no match is found
 *
 * @method     ChildPiece findOneByIdPiece(string $id_piece) Return the first ChildPiece filtered by the id_piece column
 * @method     ChildPiece findOneByIdEcriture(string $id_ecriture) Return the first ChildPiece filtered by the id_ecriture column
 * @method     ChildPiece findOneByClassement(string $classement) Return the first ChildPiece filtered by the classement column
 * @method     ChildPiece findOneByNom(string $nom) Return the first ChildPiece filtered by the nom column
 * @method     ChildPiece findOneByIdJournal(int $id_journal) Return the first ChildPiece filtered by the id_journal column
 * @method     ChildPiece findOneByIdEntite(string $id_entite) Return the first ChildPiece filtered by the id_entite column
 * @method     ChildPiece findOneByDatePiece(string $date_piece) Return the first ChildPiece filtered by the date_piece column
 * @method     ChildPiece findOneByObservation(string $observation) Return the first ChildPiece filtered by the observation column
 * @method     ChildPiece findOneByEcranSaisie(string $ecran_saisie) Return the first ChildPiece filtered by the ecran_saisie column
 * @method     ChildPiece findOneByEtat(int $etat) Return the first ChildPiece filtered by the etat column
 * @method     ChildPiece findOneByNbLigne(int $nb_ligne) Return the first ChildPiece filtered by the nb_ligne column
 * @method     ChildPiece findOneByCreatedAt(string $created_at) Return the first ChildPiece filtered by the created_at column
 * @method     ChildPiece findOneByUpdatedAt(string $updated_at) Return the first ChildPiece filtered by the updated_at column *

 * @method     ChildPiece requirePk($key, ConnectionInterface $con = null) Return the ChildPiece by primary key and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildPiece requireOne(ConnectionInterface $con = null) Return the first ChildPiece matching the query and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 *
 * @method     ChildPiece requireOneByIdPiece(string $id_piece) Return the first ChildPiece filtered by the id_piece column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildPiece requireOneByIdEcriture(string $id_ecriture) Return the first ChildPiece filtered by the id_ecriture column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildPiece requireOneByClassement(string $classement) Return the first ChildPiece filtered by the classement column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildPiece requireOneByNom(string $nom) Return the first ChildPiece filtered by the nom column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildPiece requireOneByIdJournal(int $id_journal) Return the first ChildPiece filtered by the id_journal column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildPiece requireOneByIdEntite(string $id_entite) Return the first ChildPiece filtered by the id_entite column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildPiece requireOneByDatePiece(string $date_piece) Return the first ChildPiece filtered by the date_piece column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildPiece requireOneByObservation(string $observation) Return the first ChildPiece filtered by the observation column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildPiece requireOneByEcranSaisie(string $ecran_saisie) Return the first ChildPiece filtered by the ecran_saisie column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildPiece requireOneByEtat(int $etat) Return the first ChildPiece filtered by the etat column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildPiece requireOneByNbLigne(int $nb_ligne) Return the first ChildPiece filtered by the nb_ligne column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildPiece requireOneByCreatedAt(string $created_at) Return the first ChildPiece filtered by the created_at column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildPiece requireOneByUpdatedAt(string $updated_at) Return the first ChildPiece filtered by the updated_at column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 *
 * @method     ChildPiece[]|ObjectCollection find(ConnectionInterface $con = null) Return ChildPiece objects based on current ModelCriteria
 * @method     ChildPiece[]|ObjectCollection findByIdPiece(string $id_piece) Return ChildPiece objects filtered by the id_piece column
 * @method     ChildPiece[]|ObjectCollection findByIdEcriture(string $id_ecriture) Return ChildPiece objects filtered by the id_ecriture column
 * @method     ChildPiece[]|ObjectCollection findByClassement(string $classement) Return ChildPiece objects filtered by the classement column
 * @method     ChildPiece[]|ObjectCollection findByNom(string $nom) Return ChildPiece objects filtered by the nom column
 * @method     ChildPiece[]|ObjectCollection findByIdJournal(int $id_journal) Return ChildPiece objects filtered by the id_journal column
 * @method     ChildPiece[]|ObjectCollection findByIdEntite(string $id_entite) Return ChildPiece objects filtered by the id_entite column
 * @method     ChildPiece[]|ObjectCollection findByDatePiece(string $date_piece) Return ChildPiece objects filtered by the date_piece column
 * @method     ChildPiece[]|ObjectCollection findByObservation(string $observation) Return ChildPiece objects filtered by the observation column
 * @method     ChildPiece[]|ObjectCollection findByEcranSaisie(string $ecran_saisie) Return ChildPiece objects filtered by the ecran_saisie column
 * @method     ChildPiece[]|ObjectCollection findByEtat(int $etat) Return ChildPiece objects filtered by the etat column
 * @method     ChildPiece[]|ObjectCollection findByNbLigne(int $nb_ligne) Return ChildPiece objects filtered by the nb_ligne column
 * @method     ChildPiece[]|ObjectCollection findByCreatedAt(string $created_at) Return ChildPiece objects filtered by the created_at column
 * @method     ChildPiece[]|ObjectCollection findByUpdatedAt(string $updated_at) Return ChildPiece objects filtered by the updated_at column
 * @method     ChildPiece[]|\Propel\Runtime\Util\PropelModelPager paginate($page = 1, $maxPerPage = 10, ConnectionInterface $con = null) Issue a SELECT query based on the current ModelCriteria and uses a page and a maximum number of results per page to compute an offset and a limit
 *
 */
abstract class PieceQuery extends ModelCriteria
{

    // archivable behavior
    protected $archiveOnDelete = true;
protected $entityNotFoundExceptionClass = '\\Propel\\Runtime\\Exception\\EntityNotFoundException';

    /**
     * Initializes internal state of \Base\PieceQuery object.
     *
     * @param     string $dbName The database name
     * @param     string $modelName The phpName of a model, e.g. 'Book'
     * @param     string $modelAlias The alias for the model in this query, e.g. 'b'
     */
    public function __construct($dbName = 'simplasso', $modelName = '\\Piece', $modelAlias = null)
    {
        parent::__construct($dbName, $modelName, $modelAlias);
    }

    /**
     * Returns a new ChildPieceQuery object.
     *
     * @param     string $modelAlias The alias of a model in the query
     * @param     Criteria $criteria Optional Criteria to build the query from
     *
     * @return ChildPieceQuery
     */
    public static function create($modelAlias = null, Criteria $criteria = null)
    {
        if ($criteria instanceof ChildPieceQuery) {
            return $criteria;
        }
        $query = new ChildPieceQuery();
        if (null !== $modelAlias) {
            $query->setModelAlias($modelAlias);
        }
        if ($criteria instanceof Criteria) {
            $query->mergeWith($criteria);
        }

        return $query;
    }

    /**
     * Find object by primary key.
     * Propel uses the instance pool to skip the database if the object exists.
     * Go fast if the query is untouched.
     *
     * <code>
     * $obj  = $c->findPk(12, $con);
     * </code>
     *
     * @param mixed $key Primary key to use for the query
     * @param ConnectionInterface $con an optional connection object
     *
     * @return ChildPiece|array|mixed the result, formatted by the current formatter
     */
    public function findPk($key, ConnectionInterface $con = null)
    {
        if ($key === null) {
            return null;
        }

        if ($con === null) {
            $con = Propel::getServiceContainer()->getReadConnection(PieceTableMap::DATABASE_NAME);
        }

        $this->basePreSelect($con);

        if (
            $this->formatter || $this->modelAlias || $this->with || $this->select
            || $this->selectColumns || $this->asColumns || $this->selectModifiers
            || $this->map || $this->having || $this->joins
        ) {
            return $this->findPkComplex($key, $con);
        }

        if ((null !== ($obj = PieceTableMap::getInstanceFromPool(null === $key || is_scalar($key) || is_callable([$key, '__toString']) ? (string) $key : $key)))) {
            // the object is already in the instance pool
            return $obj;
        }

        return $this->findPkSimple($key, $con);
    }

    /**
     * Find object by primary key using raw SQL to go fast.
     * Bypass doSelect() and the object formatter by using generated code.
     *
     * @param     mixed $key Primary key to use for the query
     * @param     ConnectionInterface $con A connection object
     *
     * @throws \Propel\Runtime\Exception\PropelException
     *
     * @return ChildPiece A model object, or null if the key is not found
     */
    protected function findPkSimple($key, ConnectionInterface $con)
    {
        $sql = 'SELECT `id_piece`, `id_ecriture`, `classement`, `nom`, `id_journal`, `id_entite`, `date_piece`, `observation`, `ecran_saisie`, `etat`, `nb_ligne`, `created_at`, `updated_at` FROM `assc_pieces` WHERE `id_piece` = :p0';
        try {
            $stmt = $con->prepare($sql);
            $stmt->bindValue(':p0', $key, PDO::PARAM_INT);
            $stmt->execute();
        } catch (Exception $e) {
            Propel::log($e->getMessage(), Propel::LOG_ERR);
            throw new PropelException(sprintf('Unable to execute SELECT statement [%s]', $sql), 0, $e);
        }
        $obj = null;
        if ($row = $stmt->fetch(\PDO::FETCH_NUM)) {
            /** @var ChildPiece $obj */
            $obj = new ChildPiece();
            $obj->hydrate($row);
            PieceTableMap::addInstanceToPool($obj, null === $key || is_scalar($key) || is_callable([$key, '__toString']) ? (string) $key : $key);
        }
        $stmt->closeCursor();

        return $obj;
    }

    /**
     * Find object by primary key.
     *
     * @param     mixed $key Primary key to use for the query
     * @param     ConnectionInterface $con A connection object
     *
     * @return ChildPiece|array|mixed the result, formatted by the current formatter
     */
    protected function findPkComplex($key, ConnectionInterface $con)
    {
        // As the query uses a PK condition, no limit(1) is necessary.
        $criteria = $this->isKeepQuery() ? clone $this : $this;
        $dataFetcher = $criteria
            ->filterByPrimaryKey($key)
            ->doSelect($con);

        return $criteria->getFormatter()->init($criteria)->formatOne($dataFetcher);
    }

    /**
     * Find objects by primary key
     * <code>
     * $objs = $c->findPks(array(12, 56, 832), $con);
     * </code>
     * @param     array $keys Primary keys to use for the query
     * @param     ConnectionInterface $con an optional connection object
     *
     * @return ObjectCollection|array|mixed the list of results, formatted by the current formatter
     */
    public function findPks($keys, ConnectionInterface $con = null)
    {
        if (null === $con) {
            $con = Propel::getServiceContainer()->getReadConnection($this->getDbName());
        }
        $this->basePreSelect($con);
        $criteria = $this->isKeepQuery() ? clone $this : $this;
        $dataFetcher = $criteria
            ->filterByPrimaryKeys($keys)
            ->doSelect($con);

        return $criteria->getFormatter()->init($criteria)->format($dataFetcher);
    }

    /**
     * Filter the query by primary key
     *
     * @param     mixed $key Primary key to use for the query
     *
     * @return $this|ChildPieceQuery The current query, for fluid interface
     */
    public function filterByPrimaryKey($key)
    {

        return $this->addUsingAlias(PieceTableMap::COL_ID_PIECE, $key, Criteria::EQUAL);
    }

    /**
     * Filter the query by a list of primary keys
     *
     * @param     array $keys The list of primary key to use for the query
     *
     * @return $this|ChildPieceQuery The current query, for fluid interface
     */
    public function filterByPrimaryKeys($keys)
    {

        return $this->addUsingAlias(PieceTableMap::COL_ID_PIECE, $keys, Criteria::IN);
    }

    /**
     * Filter the query on the id_piece column
     *
     * Example usage:
     * <code>
     * $query->filterByIdPiece(1234); // WHERE id_piece = 1234
     * $query->filterByIdPiece(array(12, 34)); // WHERE id_piece IN (12, 34)
     * $query->filterByIdPiece(array('min' => 12)); // WHERE id_piece > 12
     * </code>
     *
     * @param     mixed $idPiece The value to use as filter.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildPieceQuery The current query, for fluid interface
     */
    public function filterByIdPiece($idPiece = null, $comparison = null)
    {
        if (is_array($idPiece)) {
            $useMinMax = false;
            if (isset($idPiece['min'])) {
                $this->addUsingAlias(PieceTableMap::COL_ID_PIECE, $idPiece['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($idPiece['max'])) {
                $this->addUsingAlias(PieceTableMap::COL_ID_PIECE, $idPiece['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(PieceTableMap::COL_ID_PIECE, $idPiece, $comparison);
    }

    /**
     * Filter the query on the id_ecriture column
     *
     * Example usage:
     * <code>
     * $query->filterByIdEcriture(1234); // WHERE id_ecriture = 1234
     * $query->filterByIdEcriture(array(12, 34)); // WHERE id_ecriture IN (12, 34)
     * $query->filterByIdEcriture(array('min' => 12)); // WHERE id_ecriture > 12
     * </code>
     *
     * @param     mixed $idEcriture The value to use as filter.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildPieceQuery The current query, for fluid interface
     */
    public function filterByIdEcriture($idEcriture = null, $comparison = null)
    {
        if (is_array($idEcriture)) {
            $useMinMax = false;
            if (isset($idEcriture['min'])) {
                $this->addUsingAlias(PieceTableMap::COL_ID_ECRITURE, $idEcriture['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($idEcriture['max'])) {
                $this->addUsingAlias(PieceTableMap::COL_ID_ECRITURE, $idEcriture['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(PieceTableMap::COL_ID_ECRITURE, $idEcriture, $comparison);
    }

    /**
     * Filter the query on the classement column
     *
     * Example usage:
     * <code>
     * $query->filterByClassement('fooValue');   // WHERE classement = 'fooValue'
     * $query->filterByClassement('%fooValue%', Criteria::LIKE); // WHERE classement LIKE '%fooValue%'
     * </code>
     *
     * @param     string $classement The value to use as filter.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildPieceQuery The current query, for fluid interface
     */
    public function filterByClassement($classement = null, $comparison = null)
    {
        if (null === $comparison) {
            if (is_array($classement)) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(PieceTableMap::COL_CLASSEMENT, $classement, $comparison);
    }

    /**
     * Filter the query on the nom column
     *
     * Example usage:
     * <code>
     * $query->filterByNom('fooValue');   // WHERE nom = 'fooValue'
     * $query->filterByNom('%fooValue%', Criteria::LIKE); // WHERE nom LIKE '%fooValue%'
     * </code>
     *
     * @param     string $nom The value to use as filter.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildPieceQuery The current query, for fluid interface
     */
    public function filterByNom($nom = null, $comparison = null)
    {
        if (null === $comparison) {
            if (is_array($nom)) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(PieceTableMap::COL_NOM, $nom, $comparison);
    }

    /**
     * Filter the query on the id_journal column
     *
     * Example usage:
     * <code>
     * $query->filterByIdJournal(1234); // WHERE id_journal = 1234
     * $query->filterByIdJournal(array(12, 34)); // WHERE id_journal IN (12, 34)
     * $query->filterByIdJournal(array('min' => 12)); // WHERE id_journal > 12
     * </code>
     *
     * @see       filterByJournal()
     *
     * @param     mixed $idJournal The value to use as filter.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildPieceQuery The current query, for fluid interface
     */
    public function filterByIdJournal($idJournal = null, $comparison = null)
    {
        if (is_array($idJournal)) {
            $useMinMax = false;
            if (isset($idJournal['min'])) {
                $this->addUsingAlias(PieceTableMap::COL_ID_JOURNAL, $idJournal['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($idJournal['max'])) {
                $this->addUsingAlias(PieceTableMap::COL_ID_JOURNAL, $idJournal['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(PieceTableMap::COL_ID_JOURNAL, $idJournal, $comparison);
    }

    /**
     * Filter the query on the id_entite column
     *
     * Example usage:
     * <code>
     * $query->filterByIdEntite(1234); // WHERE id_entite = 1234
     * $query->filterByIdEntite(array(12, 34)); // WHERE id_entite IN (12, 34)
     * $query->filterByIdEntite(array('min' => 12)); // WHERE id_entite > 12
     * </code>
     *
     * @see       filterByEntite()
     *
     * @param     mixed $idEntite The value to use as filter.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildPieceQuery The current query, for fluid interface
     */
    public function filterByIdEntite($idEntite = null, $comparison = null)
    {
        if (is_array($idEntite)) {
            $useMinMax = false;
            if (isset($idEntite['min'])) {
                $this->addUsingAlias(PieceTableMap::COL_ID_ENTITE, $idEntite['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($idEntite['max'])) {
                $this->addUsingAlias(PieceTableMap::COL_ID_ENTITE, $idEntite['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(PieceTableMap::COL_ID_ENTITE, $idEntite, $comparison);
    }

    /**
     * Filter the query on the date_piece column
     *
     * Example usage:
     * <code>
     * $query->filterByDatePiece('2011-03-14'); // WHERE date_piece = '2011-03-14'
     * $query->filterByDatePiece('now'); // WHERE date_piece = '2011-03-14'
     * $query->filterByDatePiece(array('max' => 'yesterday')); // WHERE date_piece > '2011-03-13'
     * </code>
     *
     * @param     mixed $datePiece The value to use as filter.
     *              Values can be integers (unix timestamps), DateTime objects, or strings.
     *              Empty strings are treated as NULL.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildPieceQuery The current query, for fluid interface
     */
    public function filterByDatePiece($datePiece = null, $comparison = null)
    {
        if (is_array($datePiece)) {
            $useMinMax = false;
            if (isset($datePiece['min'])) {
                $this->addUsingAlias(PieceTableMap::COL_DATE_PIECE, $datePiece['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($datePiece['max'])) {
                $this->addUsingAlias(PieceTableMap::COL_DATE_PIECE, $datePiece['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(PieceTableMap::COL_DATE_PIECE, $datePiece, $comparison);
    }

    /**
     * Filter the query on the observation column
     *
     * Example usage:
     * <code>
     * $query->filterByObservation('fooValue');   // WHERE observation = 'fooValue'
     * $query->filterByObservation('%fooValue%', Criteria::LIKE); // WHERE observation LIKE '%fooValue%'
     * </code>
     *
     * @param     string $observation The value to use as filter.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildPieceQuery The current query, for fluid interface
     */
    public function filterByObservation($observation = null, $comparison = null)
    {
        if (null === $comparison) {
            if (is_array($observation)) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(PieceTableMap::COL_OBSERVATION, $observation, $comparison);
    }

    /**
     * Filter the query on the ecran_saisie column
     *
     * Example usage:
     * <code>
     * $query->filterByEcranSaisie('fooValue');   // WHERE ecran_saisie = 'fooValue'
     * $query->filterByEcranSaisie('%fooValue%', Criteria::LIKE); // WHERE ecran_saisie LIKE '%fooValue%'
     * </code>
     *
     * @param     string $ecranSaisie The value to use as filter.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildPieceQuery The current query, for fluid interface
     */
    public function filterByEcranSaisie($ecranSaisie = null, $comparison = null)
    {
        if (null === $comparison) {
            if (is_array($ecranSaisie)) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(PieceTableMap::COL_ECRAN_SAISIE, $ecranSaisie, $comparison);
    }

    /**
     * Filter the query on the etat column
     *
     * Example usage:
     * <code>
     * $query->filterByEtat(1234); // WHERE etat = 1234
     * $query->filterByEtat(array(12, 34)); // WHERE etat IN (12, 34)
     * $query->filterByEtat(array('min' => 12)); // WHERE etat > 12
     * </code>
     *
     * @param     mixed $etat The value to use as filter.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildPieceQuery The current query, for fluid interface
     */
    public function filterByEtat($etat = null, $comparison = null)
    {
        if (is_array($etat)) {
            $useMinMax = false;
            if (isset($etat['min'])) {
                $this->addUsingAlias(PieceTableMap::COL_ETAT, $etat['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($etat['max'])) {
                $this->addUsingAlias(PieceTableMap::COL_ETAT, $etat['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(PieceTableMap::COL_ETAT, $etat, $comparison);
    }

    /**
     * Filter the query on the nb_ligne column
     *
     * Example usage:
     * <code>
     * $query->filterByNbLigne(1234); // WHERE nb_ligne = 1234
     * $query->filterByNbLigne(array(12, 34)); // WHERE nb_ligne IN (12, 34)
     * $query->filterByNbLigne(array('min' => 12)); // WHERE nb_ligne > 12
     * </code>
     *
     * @param     mixed $nbLigne The value to use as filter.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildPieceQuery The current query, for fluid interface
     */
    public function filterByNbLigne($nbLigne = null, $comparison = null)
    {
        if (is_array($nbLigne)) {
            $useMinMax = false;
            if (isset($nbLigne['min'])) {
                $this->addUsingAlias(PieceTableMap::COL_NB_LIGNE, $nbLigne['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($nbLigne['max'])) {
                $this->addUsingAlias(PieceTableMap::COL_NB_LIGNE, $nbLigne['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(PieceTableMap::COL_NB_LIGNE, $nbLigne, $comparison);
    }

    /**
     * Filter the query on the created_at column
     *
     * Example usage:
     * <code>
     * $query->filterByCreatedAt('2011-03-14'); // WHERE created_at = '2011-03-14'
     * $query->filterByCreatedAt('now'); // WHERE created_at = '2011-03-14'
     * $query->filterByCreatedAt(array('max' => 'yesterday')); // WHERE created_at > '2011-03-13'
     * </code>
     *
     * @param     mixed $createdAt The value to use as filter.
     *              Values can be integers (unix timestamps), DateTime objects, or strings.
     *              Empty strings are treated as NULL.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildPieceQuery The current query, for fluid interface
     */
    public function filterByCreatedAt($createdAt = null, $comparison = null)
    {
        if (is_array($createdAt)) {
            $useMinMax = false;
            if (isset($createdAt['min'])) {
                $this->addUsingAlias(PieceTableMap::COL_CREATED_AT, $createdAt['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($createdAt['max'])) {
                $this->addUsingAlias(PieceTableMap::COL_CREATED_AT, $createdAt['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(PieceTableMap::COL_CREATED_AT, $createdAt, $comparison);
    }

    /**
     * Filter the query on the updated_at column
     *
     * Example usage:
     * <code>
     * $query->filterByUpdatedAt('2011-03-14'); // WHERE updated_at = '2011-03-14'
     * $query->filterByUpdatedAt('now'); // WHERE updated_at = '2011-03-14'
     * $query->filterByUpdatedAt(array('max' => 'yesterday')); // WHERE updated_at > '2011-03-13'
     * </code>
     *
     * @param     mixed $updatedAt The value to use as filter.
     *              Values can be integers (unix timestamps), DateTime objects, or strings.
     *              Empty strings are treated as NULL.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildPieceQuery The current query, for fluid interface
     */
    public function filterByUpdatedAt($updatedAt = null, $comparison = null)
    {
        if (is_array($updatedAt)) {
            $useMinMax = false;
            if (isset($updatedAt['min'])) {
                $this->addUsingAlias(PieceTableMap::COL_UPDATED_AT, $updatedAt['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($updatedAt['max'])) {
                $this->addUsingAlias(PieceTableMap::COL_UPDATED_AT, $updatedAt['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(PieceTableMap::COL_UPDATED_AT, $updatedAt, $comparison);
    }

    /**
     * Filter the query by a related \Journal object
     *
     * @param \Journal|ObjectCollection $journal The related object(s) to use as filter
     * @param string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @throws \Propel\Runtime\Exception\PropelException
     *
     * @return ChildPieceQuery The current query, for fluid interface
     */
    public function filterByJournal($journal, $comparison = null)
    {
        if ($journal instanceof \Journal) {
            return $this
                ->addUsingAlias(PieceTableMap::COL_ID_JOURNAL, $journal->getIdJournal(), $comparison);
        } elseif ($journal instanceof ObjectCollection) {
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }

            return $this
                ->addUsingAlias(PieceTableMap::COL_ID_JOURNAL, $journal->toKeyValue('PrimaryKey', 'IdJournal'), $comparison);
        } else {
            throw new PropelException('filterByJournal() only accepts arguments of type \Journal or Collection');
        }
    }

    /**
     * Adds a JOIN clause to the query using the Journal relation
     *
     * @param     string $relationAlias optional alias for the relation
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return $this|ChildPieceQuery The current query, for fluid interface
     */
    public function joinJournal($relationAlias = null, $joinType = Criteria::INNER_JOIN)
    {
        $tableMap = $this->getTableMap();
        $relationMap = $tableMap->getRelation('Journal');

        // create a ModelJoin object for this join
        $join = new ModelJoin();
        $join->setJoinType($joinType);
        $join->setRelationMap($relationMap, $this->useAliasInSQL ? $this->getModelAlias() : null, $relationAlias);
        if ($previousJoin = $this->getPreviousJoin()) {
            $join->setPreviousJoin($previousJoin);
        }

        // add the ModelJoin to the current object
        if ($relationAlias) {
            $this->addAlias($relationAlias, $relationMap->getRightTable()->getName());
            $this->addJoinObject($join, $relationAlias);
        } else {
            $this->addJoinObject($join, 'Journal');
        }

        return $this;
    }

    /**
     * Use the Journal relation Journal object
     *
     * @see useQuery()
     *
     * @param     string $relationAlias optional alias for the relation,
     *                                   to be used as main alias in the secondary query
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return \JournalQuery A secondary query class using the current class as primary query
     */
    public function useJournalQuery($relationAlias = null, $joinType = Criteria::INNER_JOIN)
    {
        return $this
            ->joinJournal($relationAlias, $joinType)
            ->useQuery($relationAlias ? $relationAlias : 'Journal', '\JournalQuery');
    }

    /**
     * Filter the query by a related \Entite object
     *
     * @param \Entite|ObjectCollection $entite The related object(s) to use as filter
     * @param string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @throws \Propel\Runtime\Exception\PropelException
     *
     * @return ChildPieceQuery The current query, for fluid interface
     */
    public function filterByEntite($entite, $comparison = null)
    {
        if ($entite instanceof \Entite) {
            return $this
                ->addUsingAlias(PieceTableMap::COL_ID_ENTITE, $entite->getIdEntite(), $comparison);
        } elseif ($entite instanceof ObjectCollection) {
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }

            return $this
                ->addUsingAlias(PieceTableMap::COL_ID_ENTITE, $entite->toKeyValue('PrimaryKey', 'IdEntite'), $comparison);
        } else {
            throw new PropelException('filterByEntite() only accepts arguments of type \Entite or Collection');
        }
    }

    /**
     * Adds a JOIN clause to the query using the Entite relation
     *
     * @param     string $relationAlias optional alias for the relation
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return $this|ChildPieceQuery The current query, for fluid interface
     */
    public function joinEntite($relationAlias = null, $joinType = Criteria::INNER_JOIN)
    {
        $tableMap = $this->getTableMap();
        $relationMap = $tableMap->getRelation('Entite');

        // create a ModelJoin object for this join
        $join = new ModelJoin();
        $join->setJoinType($joinType);
        $join->setRelationMap($relationMap, $this->useAliasInSQL ? $this->getModelAlias() : null, $relationAlias);
        if ($previousJoin = $this->getPreviousJoin()) {
            $join->setPreviousJoin($previousJoin);
        }

        // add the ModelJoin to the current object
        if ($relationAlias) {
            $this->addAlias($relationAlias, $relationMap->getRightTable()->getName());
            $this->addJoinObject($join, $relationAlias);
        } else {
            $this->addJoinObject($join, 'Entite');
        }

        return $this;
    }

    /**
     * Use the Entite relation Entite object
     *
     * @see useQuery()
     *
     * @param     string $relationAlias optional alias for the relation,
     *                                   to be used as main alias in the secondary query
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return \EntiteQuery A secondary query class using the current class as primary query
     */
    public function useEntiteQuery($relationAlias = null, $joinType = Criteria::INNER_JOIN)
    {
        return $this
            ->joinEntite($relationAlias, $joinType)
            ->useQuery($relationAlias ? $relationAlias : 'Entite', '\EntiteQuery');
    }

    /**
     * Exclude object from result
     *
     * @param   ChildPiece $piece Object to remove from the list of results
     *
     * @return $this|ChildPieceQuery The current query, for fluid interface
     */
    public function prune($piece = null)
    {
        if ($piece) {
            $this->addUsingAlias(PieceTableMap::COL_ID_PIECE, $piece->getIdPiece(), Criteria::NOT_EQUAL);
        }

        return $this;
    }

    /**
     * Code to execute before every DELETE statement
     *
     * @param     ConnectionInterface $con The connection object used by the query
     */
    protected function basePreDelete(ConnectionInterface $con)
    {
        // archivable behavior

        if ($this->archiveOnDelete) {
            $this->archive($con);
        } else {
            $this->archiveOnDelete = true;
        }


        return $this->preDelete($con);
    }

    /**
     * Deletes all rows from the assc_pieces table.
     *
     * @param ConnectionInterface $con the connection to use
     * @return int The number of affected rows (if supported by underlying database driver).
     */
    public function doDeleteAll(ConnectionInterface $con = null)
    {
        if (null === $con) {
            $con = Propel::getServiceContainer()->getWriteConnection(PieceTableMap::DATABASE_NAME);
        }

        // use transaction because $criteria could contain info
        // for more than one table or we could emulating ON DELETE CASCADE, etc.
        return $con->transaction(function () use ($con) {
            $affectedRows = 0; // initialize var to track total num of affected rows
            $affectedRows += parent::doDeleteAll($con);
            // Because this db requires some delete cascade/set null emulation, we have to
            // clear the cached instance *after* the emulation has happened (since
            // instances get re-added by the select statement contained therein).
            PieceTableMap::clearInstancePool();
            PieceTableMap::clearRelatedInstancePool();

            return $affectedRows;
        });
    }

    /**
     * Performs a DELETE on the database based on the current ModelCriteria
     *
     * @param ConnectionInterface $con the connection to use
     * @return int             The number of affected rows (if supported by underlying database driver).  This includes CASCADE-related rows
     *                         if supported by native driver or if emulated using Propel.
     * @throws PropelException Any exceptions caught during processing will be
     *                         rethrown wrapped into a PropelException.
     */
    public function delete(ConnectionInterface $con = null)
    {
        if (null === $con) {
            $con = Propel::getServiceContainer()->getWriteConnection(PieceTableMap::DATABASE_NAME);
        }

        $criteria = $this;

        // Set the correct dbName
        $criteria->setDbName(PieceTableMap::DATABASE_NAME);

        // use transaction because $criteria could contain info
        // for more than one table or we could emulating ON DELETE CASCADE, etc.
        return $con->transaction(function () use ($con, $criteria) {
            $affectedRows = 0; // initialize var to track total num of affected rows

            PieceTableMap::removeInstanceFromPool($criteria);

            $affectedRows += ModelCriteria::delete($con);
            PieceTableMap::clearRelatedInstancePool();

            return $affectedRows;
        });
    }

    // timestampable behavior

    /**
     * Filter by the latest updated
     *
     * @param      int $nbDays Maximum age of the latest update in days
     *
     * @return     $this|ChildPieceQuery The current query, for fluid interface
     */
    public function recentlyUpdated($nbDays = 7)
    {
        return $this->addUsingAlias(PieceTableMap::COL_UPDATED_AT, time() - $nbDays * 24 * 60 * 60, Criteria::GREATER_EQUAL);
    }

    /**
     * Order by update date desc
     *
     * @return     $this|ChildPieceQuery The current query, for fluid interface
     */
    public function lastUpdatedFirst()
    {
        return $this->addDescendingOrderByColumn(PieceTableMap::COL_UPDATED_AT);
    }

    /**
     * Order by update date asc
     *
     * @return     $this|ChildPieceQuery The current query, for fluid interface
     */
    public function firstUpdatedFirst()
    {
        return $this->addAscendingOrderByColumn(PieceTableMap::COL_UPDATED_AT);
    }

    /**
     * Order by create date desc
     *
     * @return     $this|ChildPieceQuery The current query, for fluid interface
     */
    public function lastCreatedFirst()
    {
        return $this->addDescendingOrderByColumn(PieceTableMap::COL_CREATED_AT);
    }

    /**
     * Filter by the latest created
     *
     * @param      int $nbDays Maximum age of in days
     *
     * @return     $this|ChildPieceQuery The current query, for fluid interface
     */
    public function recentlyCreated($nbDays = 7)
    {
        return $this->addUsingAlias(PieceTableMap::COL_CREATED_AT, time() - $nbDays * 24 * 60 * 60, Criteria::GREATER_EQUAL);
    }

    /**
     * Order by create date asc
     *
     * @return     $this|ChildPieceQuery The current query, for fluid interface
     */
    public function firstCreatedFirst()
    {
        return $this->addAscendingOrderByColumn(PieceTableMap::COL_CREATED_AT);
    }

    // archivable behavior

    /**
     * Copy the data of the objects satisfying the query into ChildAsscPiecesArchive archive objects.
     * The archived objects are then saved.
     * If any of the objects has already been archived, the archived object
     * is updated and not duplicated.
     * Warning: This termination methods issues 2n+1 queries.
     *
     * @param      ConnectionInterface $con    Connection to use.
     * @param      Boolean $useLittleMemory    Whether or not to use OnDemandFormatter to retrieve objects.
     *               Set to false if the identity map matters.
     *               Set to true (default) to use less memory.
     *
     * @return     int the number of archived objects
     */
    public function archive($con = null, $useLittleMemory = true)
    {
        $criteria = clone $this;
        // prepare the query
        $criteria->setWith(array());
        if ($useLittleMemory) {
            $criteria->setFormatter(ModelCriteria::FORMAT_ON_DEMAND);
        }
        if ($con === null) {
            $con = Propel::getServiceContainer()->getWriteConnection(PieceTableMap::DATABASE_NAME);
        }

        return $con->transaction(function () use ($con, $criteria) {
            $totalArchivedObjects = 0;

            // archive all results one by one
            foreach ($criteria->find($con) as $object) {
                $object->archive($con);
                $totalArchivedObjects++;
            }

            return $totalArchivedObjects;
        });
    }

    /**
     * Enable/disable auto-archiving on delete for the next query.
     *
     * @param boolean True if the query must archive deleted objects, false otherwise.
     */
    public function setArchiveOnDelete($archiveOnDelete)
    {
        $this->archiveOnDelete = $archiveOnDelete;
    }

    /**
     * Delete records matching the current query without archiving them.
     *
     * @param      ConnectionInterface $con    Connection to use.
     *
     * @return integer the number of deleted rows
     */
    public function deleteWithoutArchive($con = null)
    {
        $this->archiveOnDelete = false;

        return $this->delete($con);
    }

    /**
     * Delete all records without archiving them.
     *
     * @param      ConnectionInterface $con    Connection to use.
     *
     * @return integer the number of deleted rows
     */
    public function deleteAllWithoutArchive($con = null)
    {
        $this->archiveOnDelete = false;

        return $this->deleteAll($con);
    }

} // PieceQuery
