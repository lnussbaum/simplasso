<?php

namespace Base;

use \AsscComptesArchive as ChildAsscComptesArchive;
use \Compte as ChildCompte;
use \CompteQuery as ChildCompteQuery;
use \Exception;
use \PDO;
use Map\CompteTableMap;
use Propel\Runtime\Propel;
use Propel\Runtime\ActiveQuery\Criteria;
use Propel\Runtime\ActiveQuery\ModelCriteria;
use Propel\Runtime\ActiveQuery\ModelJoin;
use Propel\Runtime\Collection\ObjectCollection;
use Propel\Runtime\Connection\ConnectionInterface;
use Propel\Runtime\Exception\PropelException;

/**
 * Base class that represents a query for the 'assc_comptes' table.
 *
 *
 *
 * @method     ChildCompteQuery orderByIdCompte($order = Criteria::ASC) Order by the id_compte column
 * @method     ChildCompteQuery orderByNcompte($order = Criteria::ASC) Order by the ncompte column
 * @method     ChildCompteQuery orderByNom($order = Criteria::ASC) Order by the nom column
 * @method     ChildCompteQuery orderByNomcourt($order = Criteria::ASC) Order by the nomcourt column
 * @method     ChildCompteQuery orderByIdPoste($order = Criteria::ASC) Order by the id_poste column
 * @method     ChildCompteQuery orderByTypeOp($order = Criteria::ASC) Order by the type_op column
 * @method     ChildCompteQuery orderBySoldeAnterieur($order = Criteria::ASC) Order by the solde_anterieur column
 * @method     ChildCompteQuery orderByDateAnterieure($order = Criteria::ASC) Order by the date_anterieure column
 * @method     ChildCompteQuery orderByIdEntite($order = Criteria::ASC) Order by the id_entite column
 * @method     ChildCompteQuery orderByObservation($order = Criteria::ASC) Order by the observation column
 * @method     ChildCompteQuery orderByCreatedAt($order = Criteria::ASC) Order by the created_at column
 * @method     ChildCompteQuery orderByUpdatedAt($order = Criteria::ASC) Order by the updated_at column
 *
 * @method     ChildCompteQuery groupByIdCompte() Group by the id_compte column
 * @method     ChildCompteQuery groupByNcompte() Group by the ncompte column
 * @method     ChildCompteQuery groupByNom() Group by the nom column
 * @method     ChildCompteQuery groupByNomcourt() Group by the nomcourt column
 * @method     ChildCompteQuery groupByIdPoste() Group by the id_poste column
 * @method     ChildCompteQuery groupByTypeOp() Group by the type_op column
 * @method     ChildCompteQuery groupBySoldeAnterieur() Group by the solde_anterieur column
 * @method     ChildCompteQuery groupByDateAnterieure() Group by the date_anterieure column
 * @method     ChildCompteQuery groupByIdEntite() Group by the id_entite column
 * @method     ChildCompteQuery groupByObservation() Group by the observation column
 * @method     ChildCompteQuery groupByCreatedAt() Group by the created_at column
 * @method     ChildCompteQuery groupByUpdatedAt() Group by the updated_at column
 *
 * @method     ChildCompteQuery leftJoin($relation) Adds a LEFT JOIN clause to the query
 * @method     ChildCompteQuery rightJoin($relation) Adds a RIGHT JOIN clause to the query
 * @method     ChildCompteQuery innerJoin($relation) Adds a INNER JOIN clause to the query
 *
 * @method     ChildCompteQuery leftJoinWith($relation) Adds a LEFT JOIN clause and with to the query
 * @method     ChildCompteQuery rightJoinWith($relation) Adds a RIGHT JOIN clause and with to the query
 * @method     ChildCompteQuery innerJoinWith($relation) Adds a INNER JOIN clause and with to the query
 *
 * @method     ChildCompteQuery leftJoinEntite($relationAlias = null) Adds a LEFT JOIN clause to the query using the Entite relation
 * @method     ChildCompteQuery rightJoinEntite($relationAlias = null) Adds a RIGHT JOIN clause to the query using the Entite relation
 * @method     ChildCompteQuery innerJoinEntite($relationAlias = null) Adds a INNER JOIN clause to the query using the Entite relation
 *
 * @method     ChildCompteQuery joinWithEntite($joinType = Criteria::INNER_JOIN) Adds a join clause and with to the query using the Entite relation
 *
 * @method     ChildCompteQuery leftJoinWithEntite() Adds a LEFT JOIN clause and with to the query using the Entite relation
 * @method     ChildCompteQuery rightJoinWithEntite() Adds a RIGHT JOIN clause and with to the query using the Entite relation
 * @method     ChildCompteQuery innerJoinWithEntite() Adds a INNER JOIN clause and with to the query using the Entite relation
 *
 * @method     ChildCompteQuery leftJoinPoste($relationAlias = null) Adds a LEFT JOIN clause to the query using the Poste relation
 * @method     ChildCompteQuery rightJoinPoste($relationAlias = null) Adds a RIGHT JOIN clause to the query using the Poste relation
 * @method     ChildCompteQuery innerJoinPoste($relationAlias = null) Adds a INNER JOIN clause to the query using the Poste relation
 *
 * @method     ChildCompteQuery joinWithPoste($joinType = Criteria::INNER_JOIN) Adds a join clause and with to the query using the Poste relation
 *
 * @method     ChildCompteQuery leftJoinWithPoste() Adds a LEFT JOIN clause and with to the query using the Poste relation
 * @method     ChildCompteQuery rightJoinWithPoste() Adds a RIGHT JOIN clause and with to the query using the Poste relation
 * @method     ChildCompteQuery innerJoinWithPoste() Adds a INNER JOIN clause and with to the query using the Poste relation
 *
 * @method     ChildCompteQuery leftJoinEcriture($relationAlias = null) Adds a LEFT JOIN clause to the query using the Ecriture relation
 * @method     ChildCompteQuery rightJoinEcriture($relationAlias = null) Adds a RIGHT JOIN clause to the query using the Ecriture relation
 * @method     ChildCompteQuery innerJoinEcriture($relationAlias = null) Adds a INNER JOIN clause to the query using the Ecriture relation
 *
 * @method     ChildCompteQuery joinWithEcriture($joinType = Criteria::INNER_JOIN) Adds a join clause and with to the query using the Ecriture relation
 *
 * @method     ChildCompteQuery leftJoinWithEcriture() Adds a LEFT JOIN clause and with to the query using the Ecriture relation
 * @method     ChildCompteQuery rightJoinWithEcriture() Adds a RIGHT JOIN clause and with to the query using the Ecriture relation
 * @method     ChildCompteQuery innerJoinWithEcriture() Adds a INNER JOIN clause and with to the query using the Ecriture relation
 *
 * @method     ChildCompteQuery leftJoinJournal($relationAlias = null) Adds a LEFT JOIN clause to the query using the Journal relation
 * @method     ChildCompteQuery rightJoinJournal($relationAlias = null) Adds a RIGHT JOIN clause to the query using the Journal relation
 * @method     ChildCompteQuery innerJoinJournal($relationAlias = null) Adds a INNER JOIN clause to the query using the Journal relation
 *
 * @method     ChildCompteQuery joinWithJournal($joinType = Criteria::INNER_JOIN) Adds a join clause and with to the query using the Journal relation
 *
 * @method     ChildCompteQuery leftJoinWithJournal() Adds a LEFT JOIN clause and with to the query using the Journal relation
 * @method     ChildCompteQuery rightJoinWithJournal() Adds a RIGHT JOIN clause and with to the query using the Journal relation
 * @method     ChildCompteQuery innerJoinWithJournal() Adds a INNER JOIN clause and with to the query using the Journal relation
 *
 * @method     ChildCompteQuery leftJoinPrestation($relationAlias = null) Adds a LEFT JOIN clause to the query using the Prestation relation
 * @method     ChildCompteQuery rightJoinPrestation($relationAlias = null) Adds a RIGHT JOIN clause to the query using the Prestation relation
 * @method     ChildCompteQuery innerJoinPrestation($relationAlias = null) Adds a INNER JOIN clause to the query using the Prestation relation
 *
 * @method     ChildCompteQuery joinWithPrestation($joinType = Criteria::INNER_JOIN) Adds a join clause and with to the query using the Prestation relation
 *
 * @method     ChildCompteQuery leftJoinWithPrestation() Adds a LEFT JOIN clause and with to the query using the Prestation relation
 * @method     ChildCompteQuery rightJoinWithPrestation() Adds a RIGHT JOIN clause and with to the query using the Prestation relation
 * @method     ChildCompteQuery innerJoinWithPrestation() Adds a INNER JOIN clause and with to the query using the Prestation relation
 *
 * @method     ChildCompteQuery leftJoinTresor($relationAlias = null) Adds a LEFT JOIN clause to the query using the Tresor relation
 * @method     ChildCompteQuery rightJoinTresor($relationAlias = null) Adds a RIGHT JOIN clause to the query using the Tresor relation
 * @method     ChildCompteQuery innerJoinTresor($relationAlias = null) Adds a INNER JOIN clause to the query using the Tresor relation
 *
 * @method     ChildCompteQuery joinWithTresor($joinType = Criteria::INNER_JOIN) Adds a join clause and with to the query using the Tresor relation
 *
 * @method     ChildCompteQuery leftJoinWithTresor() Adds a LEFT JOIN clause and with to the query using the Tresor relation
 * @method     ChildCompteQuery rightJoinWithTresor() Adds a RIGHT JOIN clause and with to the query using the Tresor relation
 * @method     ChildCompteQuery innerJoinWithTresor() Adds a INNER JOIN clause and with to the query using the Tresor relation
 *
 * @method     ChildCompteQuery leftJoinTva($relationAlias = null) Adds a LEFT JOIN clause to the query using the Tva relation
 * @method     ChildCompteQuery rightJoinTva($relationAlias = null) Adds a RIGHT JOIN clause to the query using the Tva relation
 * @method     ChildCompteQuery innerJoinTva($relationAlias = null) Adds a INNER JOIN clause to the query using the Tva relation
 *
 * @method     ChildCompteQuery joinWithTva($joinType = Criteria::INNER_JOIN) Adds a join clause and with to the query using the Tva relation
 *
 * @method     ChildCompteQuery leftJoinWithTva() Adds a LEFT JOIN clause and with to the query using the Tva relation
 * @method     ChildCompteQuery rightJoinWithTva() Adds a RIGHT JOIN clause and with to the query using the Tva relation
 * @method     ChildCompteQuery innerJoinWithTva() Adds a INNER JOIN clause and with to the query using the Tva relation
 *
 * @method     \EntiteQuery|\PosteQuery|\EcritureQuery|\JournalQuery|\PrestationQuery|\TresorQuery|\TvaQuery endUse() Finalizes a secondary criteria and merges it with its primary Criteria
 *
 * @method     ChildCompte findOne(ConnectionInterface $con = null) Return the first ChildCompte matching the query
 * @method     ChildCompte findOneOrCreate(ConnectionInterface $con = null) Return the first ChildCompte matching the query, or a new ChildCompte object populated from the query conditions when no match is found
 *
 * @method     ChildCompte findOneByIdCompte(string $id_compte) Return the first ChildCompte filtered by the id_compte column
 * @method     ChildCompte findOneByNcompte(string $ncompte) Return the first ChildCompte filtered by the ncompte column
 * @method     ChildCompte findOneByNom(string $nom) Return the first ChildCompte filtered by the nom column
 * @method     ChildCompte findOneByNomcourt(string $nomcourt) Return the first ChildCompte filtered by the nomcourt column
 * @method     ChildCompte findOneByIdPoste(string $id_poste) Return the first ChildCompte filtered by the id_poste column
 * @method     ChildCompte findOneByTypeOp(int $type_op) Return the first ChildCompte filtered by the type_op column
 * @method     ChildCompte findOneBySoldeAnterieur(double $solde_anterieur) Return the first ChildCompte filtered by the solde_anterieur column
 * @method     ChildCompte findOneByDateAnterieure(string $date_anterieure) Return the first ChildCompte filtered by the date_anterieure column
 * @method     ChildCompte findOneByIdEntite(string $id_entite) Return the first ChildCompte filtered by the id_entite column
 * @method     ChildCompte findOneByObservation(string $observation) Return the first ChildCompte filtered by the observation column
 * @method     ChildCompte findOneByCreatedAt(string $created_at) Return the first ChildCompte filtered by the created_at column
 * @method     ChildCompte findOneByUpdatedAt(string $updated_at) Return the first ChildCompte filtered by the updated_at column *

 * @method     ChildCompte requirePk($key, ConnectionInterface $con = null) Return the ChildCompte by primary key and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildCompte requireOne(ConnectionInterface $con = null) Return the first ChildCompte matching the query and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 *
 * @method     ChildCompte requireOneByIdCompte(string $id_compte) Return the first ChildCompte filtered by the id_compte column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildCompte requireOneByNcompte(string $ncompte) Return the first ChildCompte filtered by the ncompte column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildCompte requireOneByNom(string $nom) Return the first ChildCompte filtered by the nom column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildCompte requireOneByNomcourt(string $nomcourt) Return the first ChildCompte filtered by the nomcourt column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildCompte requireOneByIdPoste(string $id_poste) Return the first ChildCompte filtered by the id_poste column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildCompte requireOneByTypeOp(int $type_op) Return the first ChildCompte filtered by the type_op column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildCompte requireOneBySoldeAnterieur(double $solde_anterieur) Return the first ChildCompte filtered by the solde_anterieur column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildCompte requireOneByDateAnterieure(string $date_anterieure) Return the first ChildCompte filtered by the date_anterieure column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildCompte requireOneByIdEntite(string $id_entite) Return the first ChildCompte filtered by the id_entite column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildCompte requireOneByObservation(string $observation) Return the first ChildCompte filtered by the observation column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildCompte requireOneByCreatedAt(string $created_at) Return the first ChildCompte filtered by the created_at column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildCompte requireOneByUpdatedAt(string $updated_at) Return the first ChildCompte filtered by the updated_at column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 *
 * @method     ChildCompte[]|ObjectCollection find(ConnectionInterface $con = null) Return ChildCompte objects based on current ModelCriteria
 * @method     ChildCompte[]|ObjectCollection findByIdCompte(string $id_compte) Return ChildCompte objects filtered by the id_compte column
 * @method     ChildCompte[]|ObjectCollection findByNcompte(string $ncompte) Return ChildCompte objects filtered by the ncompte column
 * @method     ChildCompte[]|ObjectCollection findByNom(string $nom) Return ChildCompte objects filtered by the nom column
 * @method     ChildCompte[]|ObjectCollection findByNomcourt(string $nomcourt) Return ChildCompte objects filtered by the nomcourt column
 * @method     ChildCompte[]|ObjectCollection findByIdPoste(string $id_poste) Return ChildCompte objects filtered by the id_poste column
 * @method     ChildCompte[]|ObjectCollection findByTypeOp(int $type_op) Return ChildCompte objects filtered by the type_op column
 * @method     ChildCompte[]|ObjectCollection findBySoldeAnterieur(double $solde_anterieur) Return ChildCompte objects filtered by the solde_anterieur column
 * @method     ChildCompte[]|ObjectCollection findByDateAnterieure(string $date_anterieure) Return ChildCompte objects filtered by the date_anterieure column
 * @method     ChildCompte[]|ObjectCollection findByIdEntite(string $id_entite) Return ChildCompte objects filtered by the id_entite column
 * @method     ChildCompte[]|ObjectCollection findByObservation(string $observation) Return ChildCompte objects filtered by the observation column
 * @method     ChildCompte[]|ObjectCollection findByCreatedAt(string $created_at) Return ChildCompte objects filtered by the created_at column
 * @method     ChildCompte[]|ObjectCollection findByUpdatedAt(string $updated_at) Return ChildCompte objects filtered by the updated_at column
 * @method     ChildCompte[]|\Propel\Runtime\Util\PropelModelPager paginate($page = 1, $maxPerPage = 10, ConnectionInterface $con = null) Issue a SELECT query based on the current ModelCriteria and uses a page and a maximum number of results per page to compute an offset and a limit
 *
 */
abstract class CompteQuery extends ModelCriteria
{

    // archivable behavior
    protected $archiveOnDelete = true;
protected $entityNotFoundExceptionClass = '\\Propel\\Runtime\\Exception\\EntityNotFoundException';

    /**
     * Initializes internal state of \Base\CompteQuery object.
     *
     * @param     string $dbName The database name
     * @param     string $modelName The phpName of a model, e.g. 'Book'
     * @param     string $modelAlias The alias for the model in this query, e.g. 'b'
     */
    public function __construct($dbName = 'simplasso', $modelName = '\\Compte', $modelAlias = null)
    {
        parent::__construct($dbName, $modelName, $modelAlias);
    }

    /**
     * Returns a new ChildCompteQuery object.
     *
     * @param     string $modelAlias The alias of a model in the query
     * @param     Criteria $criteria Optional Criteria to build the query from
     *
     * @return ChildCompteQuery
     */
    public static function create($modelAlias = null, Criteria $criteria = null)
    {
        if ($criteria instanceof ChildCompteQuery) {
            return $criteria;
        }
        $query = new ChildCompteQuery();
        if (null !== $modelAlias) {
            $query->setModelAlias($modelAlias);
        }
        if ($criteria instanceof Criteria) {
            $query->mergeWith($criteria);
        }

        return $query;
    }

    /**
     * Find object by primary key.
     * Propel uses the instance pool to skip the database if the object exists.
     * Go fast if the query is untouched.
     *
     * <code>
     * $obj  = $c->findPk(12, $con);
     * </code>
     *
     * @param mixed $key Primary key to use for the query
     * @param ConnectionInterface $con an optional connection object
     *
     * @return ChildCompte|array|mixed the result, formatted by the current formatter
     */
    public function findPk($key, ConnectionInterface $con = null)
    {
        if ($key === null) {
            return null;
        }

        if ($con === null) {
            $con = Propel::getServiceContainer()->getReadConnection(CompteTableMap::DATABASE_NAME);
        }

        $this->basePreSelect($con);

        if (
            $this->formatter || $this->modelAlias || $this->with || $this->select
            || $this->selectColumns || $this->asColumns || $this->selectModifiers
            || $this->map || $this->having || $this->joins
        ) {
            return $this->findPkComplex($key, $con);
        }

        if ((null !== ($obj = CompteTableMap::getInstanceFromPool(null === $key || is_scalar($key) || is_callable([$key, '__toString']) ? (string) $key : $key)))) {
            // the object is already in the instance pool
            return $obj;
        }

        return $this->findPkSimple($key, $con);
    }

    /**
     * Find object by primary key using raw SQL to go fast.
     * Bypass doSelect() and the object formatter by using generated code.
     *
     * @param     mixed $key Primary key to use for the query
     * @param     ConnectionInterface $con A connection object
     *
     * @throws \Propel\Runtime\Exception\PropelException
     *
     * @return ChildCompte A model object, or null if the key is not found
     */
    protected function findPkSimple($key, ConnectionInterface $con)
    {
        $sql = 'SELECT `id_compte`, `ncompte`, `nom`, `nomcourt`, `id_poste`, `type_op`, `solde_anterieur`, `date_anterieure`, `id_entite`, `observation`, `created_at`, `updated_at` FROM `assc_comptes` WHERE `id_compte` = :p0';
        try {
            $stmt = $con->prepare($sql);
            $stmt->bindValue(':p0', $key, PDO::PARAM_INT);
            $stmt->execute();
        } catch (Exception $e) {
            Propel::log($e->getMessage(), Propel::LOG_ERR);
            throw new PropelException(sprintf('Unable to execute SELECT statement [%s]', $sql), 0, $e);
        }
        $obj = null;
        if ($row = $stmt->fetch(\PDO::FETCH_NUM)) {
            /** @var ChildCompte $obj */
            $obj = new ChildCompte();
            $obj->hydrate($row);
            CompteTableMap::addInstanceToPool($obj, null === $key || is_scalar($key) || is_callable([$key, '__toString']) ? (string) $key : $key);
        }
        $stmt->closeCursor();

        return $obj;
    }

    /**
     * Find object by primary key.
     *
     * @param     mixed $key Primary key to use for the query
     * @param     ConnectionInterface $con A connection object
     *
     * @return ChildCompte|array|mixed the result, formatted by the current formatter
     */
    protected function findPkComplex($key, ConnectionInterface $con)
    {
        // As the query uses a PK condition, no limit(1) is necessary.
        $criteria = $this->isKeepQuery() ? clone $this : $this;
        $dataFetcher = $criteria
            ->filterByPrimaryKey($key)
            ->doSelect($con);

        return $criteria->getFormatter()->init($criteria)->formatOne($dataFetcher);
    }

    /**
     * Find objects by primary key
     * <code>
     * $objs = $c->findPks(array(12, 56, 832), $con);
     * </code>
     * @param     array $keys Primary keys to use for the query
     * @param     ConnectionInterface $con an optional connection object
     *
     * @return ObjectCollection|array|mixed the list of results, formatted by the current formatter
     */
    public function findPks($keys, ConnectionInterface $con = null)
    {
        if (null === $con) {
            $con = Propel::getServiceContainer()->getReadConnection($this->getDbName());
        }
        $this->basePreSelect($con);
        $criteria = $this->isKeepQuery() ? clone $this : $this;
        $dataFetcher = $criteria
            ->filterByPrimaryKeys($keys)
            ->doSelect($con);

        return $criteria->getFormatter()->init($criteria)->format($dataFetcher);
    }

    /**
     * Filter the query by primary key
     *
     * @param     mixed $key Primary key to use for the query
     *
     * @return $this|ChildCompteQuery The current query, for fluid interface
     */
    public function filterByPrimaryKey($key)
    {

        return $this->addUsingAlias(CompteTableMap::COL_ID_COMPTE, $key, Criteria::EQUAL);
    }

    /**
     * Filter the query by a list of primary keys
     *
     * @param     array $keys The list of primary key to use for the query
     *
     * @return $this|ChildCompteQuery The current query, for fluid interface
     */
    public function filterByPrimaryKeys($keys)
    {

        return $this->addUsingAlias(CompteTableMap::COL_ID_COMPTE, $keys, Criteria::IN);
    }

    /**
     * Filter the query on the id_compte column
     *
     * Example usage:
     * <code>
     * $query->filterByIdCompte(1234); // WHERE id_compte = 1234
     * $query->filterByIdCompte(array(12, 34)); // WHERE id_compte IN (12, 34)
     * $query->filterByIdCompte(array('min' => 12)); // WHERE id_compte > 12
     * </code>
     *
     * @param     mixed $idCompte The value to use as filter.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildCompteQuery The current query, for fluid interface
     */
    public function filterByIdCompte($idCompte = null, $comparison = null)
    {
        if (is_array($idCompte)) {
            $useMinMax = false;
            if (isset($idCompte['min'])) {
                $this->addUsingAlias(CompteTableMap::COL_ID_COMPTE, $idCompte['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($idCompte['max'])) {
                $this->addUsingAlias(CompteTableMap::COL_ID_COMPTE, $idCompte['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(CompteTableMap::COL_ID_COMPTE, $idCompte, $comparison);
    }

    /**
     * Filter the query on the ncompte column
     *
     * Example usage:
     * <code>
     * $query->filterByNcompte('fooValue');   // WHERE ncompte = 'fooValue'
     * $query->filterByNcompte('%fooValue%', Criteria::LIKE); // WHERE ncompte LIKE '%fooValue%'
     * </code>
     *
     * @param     string $ncompte The value to use as filter.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildCompteQuery The current query, for fluid interface
     */
    public function filterByNcompte($ncompte = null, $comparison = null)
    {
        if (null === $comparison) {
            if (is_array($ncompte)) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(CompteTableMap::COL_NCOMPTE, $ncompte, $comparison);
    }

    /**
     * Filter the query on the nom column
     *
     * Example usage:
     * <code>
     * $query->filterByNom('fooValue');   // WHERE nom = 'fooValue'
     * $query->filterByNom('%fooValue%', Criteria::LIKE); // WHERE nom LIKE '%fooValue%'
     * </code>
     *
     * @param     string $nom The value to use as filter.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildCompteQuery The current query, for fluid interface
     */
    public function filterByNom($nom = null, $comparison = null)
    {
        if (null === $comparison) {
            if (is_array($nom)) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(CompteTableMap::COL_NOM, $nom, $comparison);
    }

    /**
     * Filter the query on the nomcourt column
     *
     * Example usage:
     * <code>
     * $query->filterByNomcourt('fooValue');   // WHERE nomcourt = 'fooValue'
     * $query->filterByNomcourt('%fooValue%', Criteria::LIKE); // WHERE nomcourt LIKE '%fooValue%'
     * </code>
     *
     * @param     string $nomcourt The value to use as filter.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildCompteQuery The current query, for fluid interface
     */
    public function filterByNomcourt($nomcourt = null, $comparison = null)
    {
        if (null === $comparison) {
            if (is_array($nomcourt)) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(CompteTableMap::COL_NOMCOURT, $nomcourt, $comparison);
    }

    /**
     * Filter the query on the id_poste column
     *
     * Example usage:
     * <code>
     * $query->filterByIdPoste(1234); // WHERE id_poste = 1234
     * $query->filterByIdPoste(array(12, 34)); // WHERE id_poste IN (12, 34)
     * $query->filterByIdPoste(array('min' => 12)); // WHERE id_poste > 12
     * </code>
     *
     * @see       filterByPoste()
     *
     * @param     mixed $idPoste The value to use as filter.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildCompteQuery The current query, for fluid interface
     */
    public function filterByIdPoste($idPoste = null, $comparison = null)
    {
        if (is_array($idPoste)) {
            $useMinMax = false;
            if (isset($idPoste['min'])) {
                $this->addUsingAlias(CompteTableMap::COL_ID_POSTE, $idPoste['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($idPoste['max'])) {
                $this->addUsingAlias(CompteTableMap::COL_ID_POSTE, $idPoste['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(CompteTableMap::COL_ID_POSTE, $idPoste, $comparison);
    }

    /**
     * Filter the query on the type_op column
     *
     * Example usage:
     * <code>
     * $query->filterByTypeOp(1234); // WHERE type_op = 1234
     * $query->filterByTypeOp(array(12, 34)); // WHERE type_op IN (12, 34)
     * $query->filterByTypeOp(array('min' => 12)); // WHERE type_op > 12
     * </code>
     *
     * @param     mixed $typeOp The value to use as filter.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildCompteQuery The current query, for fluid interface
     */
    public function filterByTypeOp($typeOp = null, $comparison = null)
    {
        if (is_array($typeOp)) {
            $useMinMax = false;
            if (isset($typeOp['min'])) {
                $this->addUsingAlias(CompteTableMap::COL_TYPE_OP, $typeOp['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($typeOp['max'])) {
                $this->addUsingAlias(CompteTableMap::COL_TYPE_OP, $typeOp['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(CompteTableMap::COL_TYPE_OP, $typeOp, $comparison);
    }

    /**
     * Filter the query on the solde_anterieur column
     *
     * Example usage:
     * <code>
     * $query->filterBySoldeAnterieur(1234); // WHERE solde_anterieur = 1234
     * $query->filterBySoldeAnterieur(array(12, 34)); // WHERE solde_anterieur IN (12, 34)
     * $query->filterBySoldeAnterieur(array('min' => 12)); // WHERE solde_anterieur > 12
     * </code>
     *
     * @param     mixed $soldeAnterieur The value to use as filter.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildCompteQuery The current query, for fluid interface
     */
    public function filterBySoldeAnterieur($soldeAnterieur = null, $comparison = null)
    {
        if (is_array($soldeAnterieur)) {
            $useMinMax = false;
            if (isset($soldeAnterieur['min'])) {
                $this->addUsingAlias(CompteTableMap::COL_SOLDE_ANTERIEUR, $soldeAnterieur['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($soldeAnterieur['max'])) {
                $this->addUsingAlias(CompteTableMap::COL_SOLDE_ANTERIEUR, $soldeAnterieur['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(CompteTableMap::COL_SOLDE_ANTERIEUR, $soldeAnterieur, $comparison);
    }

    /**
     * Filter the query on the date_anterieure column
     *
     * Example usage:
     * <code>
     * $query->filterByDateAnterieure('2011-03-14'); // WHERE date_anterieure = '2011-03-14'
     * $query->filterByDateAnterieure('now'); // WHERE date_anterieure = '2011-03-14'
     * $query->filterByDateAnterieure(array('max' => 'yesterday')); // WHERE date_anterieure > '2011-03-13'
     * </code>
     *
     * @param     mixed $dateAnterieure The value to use as filter.
     *              Values can be integers (unix timestamps), DateTime objects, or strings.
     *              Empty strings are treated as NULL.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildCompteQuery The current query, for fluid interface
     */
    public function filterByDateAnterieure($dateAnterieure = null, $comparison = null)
    {
        if (is_array($dateAnterieure)) {
            $useMinMax = false;
            if (isset($dateAnterieure['min'])) {
                $this->addUsingAlias(CompteTableMap::COL_DATE_ANTERIEURE, $dateAnterieure['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($dateAnterieure['max'])) {
                $this->addUsingAlias(CompteTableMap::COL_DATE_ANTERIEURE, $dateAnterieure['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(CompteTableMap::COL_DATE_ANTERIEURE, $dateAnterieure, $comparison);
    }

    /**
     * Filter the query on the id_entite column
     *
     * Example usage:
     * <code>
     * $query->filterByIdEntite(1234); // WHERE id_entite = 1234
     * $query->filterByIdEntite(array(12, 34)); // WHERE id_entite IN (12, 34)
     * $query->filterByIdEntite(array('min' => 12)); // WHERE id_entite > 12
     * </code>
     *
     * @see       filterByEntite()
     *
     * @param     mixed $idEntite The value to use as filter.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildCompteQuery The current query, for fluid interface
     */
    public function filterByIdEntite($idEntite = null, $comparison = null)
    {
        if (is_array($idEntite)) {
            $useMinMax = false;
            if (isset($idEntite['min'])) {
                $this->addUsingAlias(CompteTableMap::COL_ID_ENTITE, $idEntite['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($idEntite['max'])) {
                $this->addUsingAlias(CompteTableMap::COL_ID_ENTITE, $idEntite['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(CompteTableMap::COL_ID_ENTITE, $idEntite, $comparison);
    }

    /**
     * Filter the query on the observation column
     *
     * Example usage:
     * <code>
     * $query->filterByObservation('fooValue');   // WHERE observation = 'fooValue'
     * $query->filterByObservation('%fooValue%', Criteria::LIKE); // WHERE observation LIKE '%fooValue%'
     * </code>
     *
     * @param     string $observation The value to use as filter.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildCompteQuery The current query, for fluid interface
     */
    public function filterByObservation($observation = null, $comparison = null)
    {
        if (null === $comparison) {
            if (is_array($observation)) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(CompteTableMap::COL_OBSERVATION, $observation, $comparison);
    }

    /**
     * Filter the query on the created_at column
     *
     * Example usage:
     * <code>
     * $query->filterByCreatedAt('2011-03-14'); // WHERE created_at = '2011-03-14'
     * $query->filterByCreatedAt('now'); // WHERE created_at = '2011-03-14'
     * $query->filterByCreatedAt(array('max' => 'yesterday')); // WHERE created_at > '2011-03-13'
     * </code>
     *
     * @param     mixed $createdAt The value to use as filter.
     *              Values can be integers (unix timestamps), DateTime objects, or strings.
     *              Empty strings are treated as NULL.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildCompteQuery The current query, for fluid interface
     */
    public function filterByCreatedAt($createdAt = null, $comparison = null)
    {
        if (is_array($createdAt)) {
            $useMinMax = false;
            if (isset($createdAt['min'])) {
                $this->addUsingAlias(CompteTableMap::COL_CREATED_AT, $createdAt['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($createdAt['max'])) {
                $this->addUsingAlias(CompteTableMap::COL_CREATED_AT, $createdAt['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(CompteTableMap::COL_CREATED_AT, $createdAt, $comparison);
    }

    /**
     * Filter the query on the updated_at column
     *
     * Example usage:
     * <code>
     * $query->filterByUpdatedAt('2011-03-14'); // WHERE updated_at = '2011-03-14'
     * $query->filterByUpdatedAt('now'); // WHERE updated_at = '2011-03-14'
     * $query->filterByUpdatedAt(array('max' => 'yesterday')); // WHERE updated_at > '2011-03-13'
     * </code>
     *
     * @param     mixed $updatedAt The value to use as filter.
     *              Values can be integers (unix timestamps), DateTime objects, or strings.
     *              Empty strings are treated as NULL.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildCompteQuery The current query, for fluid interface
     */
    public function filterByUpdatedAt($updatedAt = null, $comparison = null)
    {
        if (is_array($updatedAt)) {
            $useMinMax = false;
            if (isset($updatedAt['min'])) {
                $this->addUsingAlias(CompteTableMap::COL_UPDATED_AT, $updatedAt['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($updatedAt['max'])) {
                $this->addUsingAlias(CompteTableMap::COL_UPDATED_AT, $updatedAt['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(CompteTableMap::COL_UPDATED_AT, $updatedAt, $comparison);
    }

    /**
     * Filter the query by a related \Entite object
     *
     * @param \Entite|ObjectCollection $entite The related object(s) to use as filter
     * @param string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @throws \Propel\Runtime\Exception\PropelException
     *
     * @return ChildCompteQuery The current query, for fluid interface
     */
    public function filterByEntite($entite, $comparison = null)
    {
        if ($entite instanceof \Entite) {
            return $this
                ->addUsingAlias(CompteTableMap::COL_ID_ENTITE, $entite->getIdEntite(), $comparison);
        } elseif ($entite instanceof ObjectCollection) {
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }

            return $this
                ->addUsingAlias(CompteTableMap::COL_ID_ENTITE, $entite->toKeyValue('PrimaryKey', 'IdEntite'), $comparison);
        } else {
            throw new PropelException('filterByEntite() only accepts arguments of type \Entite or Collection');
        }
    }

    /**
     * Adds a JOIN clause to the query using the Entite relation
     *
     * @param     string $relationAlias optional alias for the relation
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return $this|ChildCompteQuery The current query, for fluid interface
     */
    public function joinEntite($relationAlias = null, $joinType = Criteria::INNER_JOIN)
    {
        $tableMap = $this->getTableMap();
        $relationMap = $tableMap->getRelation('Entite');

        // create a ModelJoin object for this join
        $join = new ModelJoin();
        $join->setJoinType($joinType);
        $join->setRelationMap($relationMap, $this->useAliasInSQL ? $this->getModelAlias() : null, $relationAlias);
        if ($previousJoin = $this->getPreviousJoin()) {
            $join->setPreviousJoin($previousJoin);
        }

        // add the ModelJoin to the current object
        if ($relationAlias) {
            $this->addAlias($relationAlias, $relationMap->getRightTable()->getName());
            $this->addJoinObject($join, $relationAlias);
        } else {
            $this->addJoinObject($join, 'Entite');
        }

        return $this;
    }

    /**
     * Use the Entite relation Entite object
     *
     * @see useQuery()
     *
     * @param     string $relationAlias optional alias for the relation,
     *                                   to be used as main alias in the secondary query
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return \EntiteQuery A secondary query class using the current class as primary query
     */
    public function useEntiteQuery($relationAlias = null, $joinType = Criteria::INNER_JOIN)
    {
        return $this
            ->joinEntite($relationAlias, $joinType)
            ->useQuery($relationAlias ? $relationAlias : 'Entite', '\EntiteQuery');
    }

    /**
     * Filter the query by a related \Poste object
     *
     * @param \Poste|ObjectCollection $poste The related object(s) to use as filter
     * @param string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @throws \Propel\Runtime\Exception\PropelException
     *
     * @return ChildCompteQuery The current query, for fluid interface
     */
    public function filterByPoste($poste, $comparison = null)
    {
        if ($poste instanceof \Poste) {
            return $this
                ->addUsingAlias(CompteTableMap::COL_ID_POSTE, $poste->getIdPoste(), $comparison);
        } elseif ($poste instanceof ObjectCollection) {
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }

            return $this
                ->addUsingAlias(CompteTableMap::COL_ID_POSTE, $poste->toKeyValue('PrimaryKey', 'IdPoste'), $comparison);
        } else {
            throw new PropelException('filterByPoste() only accepts arguments of type \Poste or Collection');
        }
    }

    /**
     * Adds a JOIN clause to the query using the Poste relation
     *
     * @param     string $relationAlias optional alias for the relation
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return $this|ChildCompteQuery The current query, for fluid interface
     */
    public function joinPoste($relationAlias = null, $joinType = Criteria::INNER_JOIN)
    {
        $tableMap = $this->getTableMap();
        $relationMap = $tableMap->getRelation('Poste');

        // create a ModelJoin object for this join
        $join = new ModelJoin();
        $join->setJoinType($joinType);
        $join->setRelationMap($relationMap, $this->useAliasInSQL ? $this->getModelAlias() : null, $relationAlias);
        if ($previousJoin = $this->getPreviousJoin()) {
            $join->setPreviousJoin($previousJoin);
        }

        // add the ModelJoin to the current object
        if ($relationAlias) {
            $this->addAlias($relationAlias, $relationMap->getRightTable()->getName());
            $this->addJoinObject($join, $relationAlias);
        } else {
            $this->addJoinObject($join, 'Poste');
        }

        return $this;
    }

    /**
     * Use the Poste relation Poste object
     *
     * @see useQuery()
     *
     * @param     string $relationAlias optional alias for the relation,
     *                                   to be used as main alias in the secondary query
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return \PosteQuery A secondary query class using the current class as primary query
     */
    public function usePosteQuery($relationAlias = null, $joinType = Criteria::INNER_JOIN)
    {
        return $this
            ->joinPoste($relationAlias, $joinType)
            ->useQuery($relationAlias ? $relationAlias : 'Poste', '\PosteQuery');
    }

    /**
     * Filter the query by a related \Ecriture object
     *
     * @param \Ecriture|ObjectCollection $ecriture the related object to use as filter
     * @param string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return ChildCompteQuery The current query, for fluid interface
     */
    public function filterByEcriture($ecriture, $comparison = null)
    {
        if ($ecriture instanceof \Ecriture) {
            return $this
                ->addUsingAlias(CompteTableMap::COL_ID_COMPTE, $ecriture->getIdCompte(), $comparison);
        } elseif ($ecriture instanceof ObjectCollection) {
            return $this
                ->useEcritureQuery()
                ->filterByPrimaryKeys($ecriture->getPrimaryKeys())
                ->endUse();
        } else {
            throw new PropelException('filterByEcriture() only accepts arguments of type \Ecriture or Collection');
        }
    }

    /**
     * Adds a JOIN clause to the query using the Ecriture relation
     *
     * @param     string $relationAlias optional alias for the relation
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return $this|ChildCompteQuery The current query, for fluid interface
     */
    public function joinEcriture($relationAlias = null, $joinType = Criteria::INNER_JOIN)
    {
        $tableMap = $this->getTableMap();
        $relationMap = $tableMap->getRelation('Ecriture');

        // create a ModelJoin object for this join
        $join = new ModelJoin();
        $join->setJoinType($joinType);
        $join->setRelationMap($relationMap, $this->useAliasInSQL ? $this->getModelAlias() : null, $relationAlias);
        if ($previousJoin = $this->getPreviousJoin()) {
            $join->setPreviousJoin($previousJoin);
        }

        // add the ModelJoin to the current object
        if ($relationAlias) {
            $this->addAlias($relationAlias, $relationMap->getRightTable()->getName());
            $this->addJoinObject($join, $relationAlias);
        } else {
            $this->addJoinObject($join, 'Ecriture');
        }

        return $this;
    }

    /**
     * Use the Ecriture relation Ecriture object
     *
     * @see useQuery()
     *
     * @param     string $relationAlias optional alias for the relation,
     *                                   to be used as main alias in the secondary query
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return \EcritureQuery A secondary query class using the current class as primary query
     */
    public function useEcritureQuery($relationAlias = null, $joinType = Criteria::INNER_JOIN)
    {
        return $this
            ->joinEcriture($relationAlias, $joinType)
            ->useQuery($relationAlias ? $relationAlias : 'Ecriture', '\EcritureQuery');
    }

    /**
     * Filter the query by a related \Journal object
     *
     * @param \Journal|ObjectCollection $journal the related object to use as filter
     * @param string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return ChildCompteQuery The current query, for fluid interface
     */
    public function filterByJournal($journal, $comparison = null)
    {
        if ($journal instanceof \Journal) {
            return $this
                ->addUsingAlias(CompteTableMap::COL_ID_COMPTE, $journal->getIdCompte(), $comparison);
        } elseif ($journal instanceof ObjectCollection) {
            return $this
                ->useJournalQuery()
                ->filterByPrimaryKeys($journal->getPrimaryKeys())
                ->endUse();
        } else {
            throw new PropelException('filterByJournal() only accepts arguments of type \Journal or Collection');
        }
    }

    /**
     * Adds a JOIN clause to the query using the Journal relation
     *
     * @param     string $relationAlias optional alias for the relation
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return $this|ChildCompteQuery The current query, for fluid interface
     */
    public function joinJournal($relationAlias = null, $joinType = Criteria::INNER_JOIN)
    {
        $tableMap = $this->getTableMap();
        $relationMap = $tableMap->getRelation('Journal');

        // create a ModelJoin object for this join
        $join = new ModelJoin();
        $join->setJoinType($joinType);
        $join->setRelationMap($relationMap, $this->useAliasInSQL ? $this->getModelAlias() : null, $relationAlias);
        if ($previousJoin = $this->getPreviousJoin()) {
            $join->setPreviousJoin($previousJoin);
        }

        // add the ModelJoin to the current object
        if ($relationAlias) {
            $this->addAlias($relationAlias, $relationMap->getRightTable()->getName());
            $this->addJoinObject($join, $relationAlias);
        } else {
            $this->addJoinObject($join, 'Journal');
        }

        return $this;
    }

    /**
     * Use the Journal relation Journal object
     *
     * @see useQuery()
     *
     * @param     string $relationAlias optional alias for the relation,
     *                                   to be used as main alias in the secondary query
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return \JournalQuery A secondary query class using the current class as primary query
     */
    public function useJournalQuery($relationAlias = null, $joinType = Criteria::INNER_JOIN)
    {
        return $this
            ->joinJournal($relationAlias, $joinType)
            ->useQuery($relationAlias ? $relationAlias : 'Journal', '\JournalQuery');
    }

    /**
     * Filter the query by a related \Prestation object
     *
     * @param \Prestation|ObjectCollection $prestation the related object to use as filter
     * @param string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return ChildCompteQuery The current query, for fluid interface
     */
    public function filterByPrestation($prestation, $comparison = null)
    {
        if ($prestation instanceof \Prestation) {
            return $this
                ->addUsingAlias(CompteTableMap::COL_ID_COMPTE, $prestation->getIdCompte(), $comparison);
        } elseif ($prestation instanceof ObjectCollection) {
            return $this
                ->usePrestationQuery()
                ->filterByPrimaryKeys($prestation->getPrimaryKeys())
                ->endUse();
        } else {
            throw new PropelException('filterByPrestation() only accepts arguments of type \Prestation or Collection');
        }
    }

    /**
     * Adds a JOIN clause to the query using the Prestation relation
     *
     * @param     string $relationAlias optional alias for the relation
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return $this|ChildCompteQuery The current query, for fluid interface
     */
    public function joinPrestation($relationAlias = null, $joinType = Criteria::LEFT_JOIN)
    {
        $tableMap = $this->getTableMap();
        $relationMap = $tableMap->getRelation('Prestation');

        // create a ModelJoin object for this join
        $join = new ModelJoin();
        $join->setJoinType($joinType);
        $join->setRelationMap($relationMap, $this->useAliasInSQL ? $this->getModelAlias() : null, $relationAlias);
        if ($previousJoin = $this->getPreviousJoin()) {
            $join->setPreviousJoin($previousJoin);
        }

        // add the ModelJoin to the current object
        if ($relationAlias) {
            $this->addAlias($relationAlias, $relationMap->getRightTable()->getName());
            $this->addJoinObject($join, $relationAlias);
        } else {
            $this->addJoinObject($join, 'Prestation');
        }

        return $this;
    }

    /**
     * Use the Prestation relation Prestation object
     *
     * @see useQuery()
     *
     * @param     string $relationAlias optional alias for the relation,
     *                                   to be used as main alias in the secondary query
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return \PrestationQuery A secondary query class using the current class as primary query
     */
    public function usePrestationQuery($relationAlias = null, $joinType = Criteria::LEFT_JOIN)
    {
        return $this
            ->joinPrestation($relationAlias, $joinType)
            ->useQuery($relationAlias ? $relationAlias : 'Prestation', '\PrestationQuery');
    }

    /**
     * Filter the query by a related \Tresor object
     *
     * @param \Tresor|ObjectCollection $tresor the related object to use as filter
     * @param string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return ChildCompteQuery The current query, for fluid interface
     */
    public function filterByTresor($tresor, $comparison = null)
    {
        if ($tresor instanceof \Tresor) {
            return $this
                ->addUsingAlias(CompteTableMap::COL_ID_COMPTE, $tresor->getIdCompte(), $comparison);
        } elseif ($tresor instanceof ObjectCollection) {
            return $this
                ->useTresorQuery()
                ->filterByPrimaryKeys($tresor->getPrimaryKeys())
                ->endUse();
        } else {
            throw new PropelException('filterByTresor() only accepts arguments of type \Tresor or Collection');
        }
    }

    /**
     * Adds a JOIN clause to the query using the Tresor relation
     *
     * @param     string $relationAlias optional alias for the relation
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return $this|ChildCompteQuery The current query, for fluid interface
     */
    public function joinTresor($relationAlias = null, $joinType = Criteria::LEFT_JOIN)
    {
        $tableMap = $this->getTableMap();
        $relationMap = $tableMap->getRelation('Tresor');

        // create a ModelJoin object for this join
        $join = new ModelJoin();
        $join->setJoinType($joinType);
        $join->setRelationMap($relationMap, $this->useAliasInSQL ? $this->getModelAlias() : null, $relationAlias);
        if ($previousJoin = $this->getPreviousJoin()) {
            $join->setPreviousJoin($previousJoin);
        }

        // add the ModelJoin to the current object
        if ($relationAlias) {
            $this->addAlias($relationAlias, $relationMap->getRightTable()->getName());
            $this->addJoinObject($join, $relationAlias);
        } else {
            $this->addJoinObject($join, 'Tresor');
        }

        return $this;
    }

    /**
     * Use the Tresor relation Tresor object
     *
     * @see useQuery()
     *
     * @param     string $relationAlias optional alias for the relation,
     *                                   to be used as main alias in the secondary query
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return \TresorQuery A secondary query class using the current class as primary query
     */
    public function useTresorQuery($relationAlias = null, $joinType = Criteria::LEFT_JOIN)
    {
        return $this
            ->joinTresor($relationAlias, $joinType)
            ->useQuery($relationAlias ? $relationAlias : 'Tresor', '\TresorQuery');
    }

    /**
     * Filter the query by a related \Tva object
     *
     * @param \Tva|ObjectCollection $tva the related object to use as filter
     * @param string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return ChildCompteQuery The current query, for fluid interface
     */
    public function filterByTva($tva, $comparison = null)
    {
        if ($tva instanceof \Tva) {
            return $this
                ->addUsingAlias(CompteTableMap::COL_ID_COMPTE, $tva->getIdCompte(), $comparison);
        } elseif ($tva instanceof ObjectCollection) {
            return $this
                ->useTvaQuery()
                ->filterByPrimaryKeys($tva->getPrimaryKeys())
                ->endUse();
        } else {
            throw new PropelException('filterByTva() only accepts arguments of type \Tva or Collection');
        }
    }

    /**
     * Adds a JOIN clause to the query using the Tva relation
     *
     * @param     string $relationAlias optional alias for the relation
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return $this|ChildCompteQuery The current query, for fluid interface
     */
    public function joinTva($relationAlias = null, $joinType = Criteria::INNER_JOIN)
    {
        $tableMap = $this->getTableMap();
        $relationMap = $tableMap->getRelation('Tva');

        // create a ModelJoin object for this join
        $join = new ModelJoin();
        $join->setJoinType($joinType);
        $join->setRelationMap($relationMap, $this->useAliasInSQL ? $this->getModelAlias() : null, $relationAlias);
        if ($previousJoin = $this->getPreviousJoin()) {
            $join->setPreviousJoin($previousJoin);
        }

        // add the ModelJoin to the current object
        if ($relationAlias) {
            $this->addAlias($relationAlias, $relationMap->getRightTable()->getName());
            $this->addJoinObject($join, $relationAlias);
        } else {
            $this->addJoinObject($join, 'Tva');
        }

        return $this;
    }

    /**
     * Use the Tva relation Tva object
     *
     * @see useQuery()
     *
     * @param     string $relationAlias optional alias for the relation,
     *                                   to be used as main alias in the secondary query
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return \TvaQuery A secondary query class using the current class as primary query
     */
    public function useTvaQuery($relationAlias = null, $joinType = Criteria::INNER_JOIN)
    {
        return $this
            ->joinTva($relationAlias, $joinType)
            ->useQuery($relationAlias ? $relationAlias : 'Tva', '\TvaQuery');
    }

    /**
     * Exclude object from result
     *
     * @param   ChildCompte $compte Object to remove from the list of results
     *
     * @return $this|ChildCompteQuery The current query, for fluid interface
     */
    public function prune($compte = null)
    {
        if ($compte) {
            $this->addUsingAlias(CompteTableMap::COL_ID_COMPTE, $compte->getIdCompte(), Criteria::NOT_EQUAL);
        }

        return $this;
    }

    /**
     * Code to execute before every DELETE statement
     *
     * @param     ConnectionInterface $con The connection object used by the query
     */
    protected function basePreDelete(ConnectionInterface $con)
    {
        // archivable behavior

        if ($this->archiveOnDelete) {
            $this->archive($con);
        } else {
            $this->archiveOnDelete = true;
        }


        return $this->preDelete($con);
    }

    /**
     * Deletes all rows from the assc_comptes table.
     *
     * @param ConnectionInterface $con the connection to use
     * @return int The number of affected rows (if supported by underlying database driver).
     */
    public function doDeleteAll(ConnectionInterface $con = null)
    {
        if (null === $con) {
            $con = Propel::getServiceContainer()->getWriteConnection(CompteTableMap::DATABASE_NAME);
        }

        // use transaction because $criteria could contain info
        // for more than one table or we could emulating ON DELETE CASCADE, etc.
        return $con->transaction(function () use ($con) {
            $affectedRows = 0; // initialize var to track total num of affected rows
            $affectedRows += parent::doDeleteAll($con);
            // Because this db requires some delete cascade/set null emulation, we have to
            // clear the cached instance *after* the emulation has happened (since
            // instances get re-added by the select statement contained therein).
            CompteTableMap::clearInstancePool();
            CompteTableMap::clearRelatedInstancePool();

            return $affectedRows;
        });
    }

    /**
     * Performs a DELETE on the database based on the current ModelCriteria
     *
     * @param ConnectionInterface $con the connection to use
     * @return int             The number of affected rows (if supported by underlying database driver).  This includes CASCADE-related rows
     *                         if supported by native driver or if emulated using Propel.
     * @throws PropelException Any exceptions caught during processing will be
     *                         rethrown wrapped into a PropelException.
     */
    public function delete(ConnectionInterface $con = null)
    {
        if (null === $con) {
            $con = Propel::getServiceContainer()->getWriteConnection(CompteTableMap::DATABASE_NAME);
        }

        $criteria = $this;

        // Set the correct dbName
        $criteria->setDbName(CompteTableMap::DATABASE_NAME);

        // use transaction because $criteria could contain info
        // for more than one table or we could emulating ON DELETE CASCADE, etc.
        return $con->transaction(function () use ($con, $criteria) {
            $affectedRows = 0; // initialize var to track total num of affected rows

            CompteTableMap::removeInstanceFromPool($criteria);

            $affectedRows += ModelCriteria::delete($con);
            CompteTableMap::clearRelatedInstancePool();

            return $affectedRows;
        });
    }

    // timestampable behavior

    /**
     * Filter by the latest updated
     *
     * @param      int $nbDays Maximum age of the latest update in days
     *
     * @return     $this|ChildCompteQuery The current query, for fluid interface
     */
    public function recentlyUpdated($nbDays = 7)
    {
        return $this->addUsingAlias(CompteTableMap::COL_UPDATED_AT, time() - $nbDays * 24 * 60 * 60, Criteria::GREATER_EQUAL);
    }

    /**
     * Order by update date desc
     *
     * @return     $this|ChildCompteQuery The current query, for fluid interface
     */
    public function lastUpdatedFirst()
    {
        return $this->addDescendingOrderByColumn(CompteTableMap::COL_UPDATED_AT);
    }

    /**
     * Order by update date asc
     *
     * @return     $this|ChildCompteQuery The current query, for fluid interface
     */
    public function firstUpdatedFirst()
    {
        return $this->addAscendingOrderByColumn(CompteTableMap::COL_UPDATED_AT);
    }

    /**
     * Order by create date desc
     *
     * @return     $this|ChildCompteQuery The current query, for fluid interface
     */
    public function lastCreatedFirst()
    {
        return $this->addDescendingOrderByColumn(CompteTableMap::COL_CREATED_AT);
    }

    /**
     * Filter by the latest created
     *
     * @param      int $nbDays Maximum age of in days
     *
     * @return     $this|ChildCompteQuery The current query, for fluid interface
     */
    public function recentlyCreated($nbDays = 7)
    {
        return $this->addUsingAlias(CompteTableMap::COL_CREATED_AT, time() - $nbDays * 24 * 60 * 60, Criteria::GREATER_EQUAL);
    }

    /**
     * Order by create date asc
     *
     * @return     $this|ChildCompteQuery The current query, for fluid interface
     */
    public function firstCreatedFirst()
    {
        return $this->addAscendingOrderByColumn(CompteTableMap::COL_CREATED_AT);
    }

    // archivable behavior

    /**
     * Copy the data of the objects satisfying the query into ChildAsscComptesArchive archive objects.
     * The archived objects are then saved.
     * If any of the objects has already been archived, the archived object
     * is updated and not duplicated.
     * Warning: This termination methods issues 2n+1 queries.
     *
     * @param      ConnectionInterface $con    Connection to use.
     * @param      Boolean $useLittleMemory    Whether or not to use OnDemandFormatter to retrieve objects.
     *               Set to false if the identity map matters.
     *               Set to true (default) to use less memory.
     *
     * @return     int the number of archived objects
     */
    public function archive($con = null, $useLittleMemory = true)
    {
        $criteria = clone $this;
        // prepare the query
        $criteria->setWith(array());
        if ($useLittleMemory) {
            $criteria->setFormatter(ModelCriteria::FORMAT_ON_DEMAND);
        }
        if ($con === null) {
            $con = Propel::getServiceContainer()->getWriteConnection(CompteTableMap::DATABASE_NAME);
        }

        return $con->transaction(function () use ($con, $criteria) {
            $totalArchivedObjects = 0;

            // archive all results one by one
            foreach ($criteria->find($con) as $object) {
                $object->archive($con);
                $totalArchivedObjects++;
            }

            return $totalArchivedObjects;
        });
    }

    /**
     * Enable/disable auto-archiving on delete for the next query.
     *
     * @param boolean True if the query must archive deleted objects, false otherwise.
     */
    public function setArchiveOnDelete($archiveOnDelete)
    {
        $this->archiveOnDelete = $archiveOnDelete;
    }

    /**
     * Delete records matching the current query without archiving them.
     *
     * @param      ConnectionInterface $con    Connection to use.
     *
     * @return integer the number of deleted rows
     */
    public function deleteWithoutArchive($con = null)
    {
        $this->archiveOnDelete = false;

        return $this->delete($con);
    }

    /**
     * Delete all records without archiving them.
     *
     * @param      ConnectionInterface $con    Connection to use.
     *
     * @return integer the number of deleted rows
     */
    public function deleteAllWithoutArchive($con = null)
    {
        $this->archiveOnDelete = false;

        return $this->deleteAll($con);
    }

} // CompteQuery
