<?php

namespace Base;

use \AssoMembresArchive as ChildAssoMembresArchive;
use \Membre as ChildMembre;
use \MembreQuery as ChildMembreQuery;
use \Exception;
use \PDO;
use Map\MembreTableMap;
use Propel\Runtime\Propel;
use Propel\Runtime\ActiveQuery\Criteria;
use Propel\Runtime\ActiveQuery\ModelCriteria;
use Propel\Runtime\ActiveQuery\ModelJoin;
use Propel\Runtime\Collection\ObjectCollection;
use Propel\Runtime\Connection\ConnectionInterface;
use Propel\Runtime\Exception\PropelException;

/**
 * Base class that represents a query for the 'asso_membres' table.
 *
 *
 *
 * @method     ChildMembreQuery orderByIdMembre($order = Criteria::ASC) Order by the id_membre column
 * @method     ChildMembreQuery orderByIdIndividuTitulaire($order = Criteria::ASC) Order by the id_individu_titulaire column
 * @method     ChildMembreQuery orderByIdentifiantInterne($order = Criteria::ASC) Order by the identifiant_interne column
 * @method     ChildMembreQuery orderByCivilite($order = Criteria::ASC) Order by the civilite column
 * @method     ChildMembreQuery orderByNom($order = Criteria::ASC) Order by the nom column
 * @method     ChildMembreQuery orderByNomcourt($order = Criteria::ASC) Order by the nomcourt column
 * @method     ChildMembreQuery orderByDateSortie($order = Criteria::ASC) Order by the date_sortie column
 * @method     ChildMembreQuery orderByObservation($order = Criteria::ASC) Order by the observation column
 * @method     ChildMembreQuery orderByCreatedAt($order = Criteria::ASC) Order by the created_at column
 * @method     ChildMembreQuery orderByUpdatedAt($order = Criteria::ASC) Order by the updated_at column
 *
 * @method     ChildMembreQuery groupByIdMembre() Group by the id_membre column
 * @method     ChildMembreQuery groupByIdIndividuTitulaire() Group by the id_individu_titulaire column
 * @method     ChildMembreQuery groupByIdentifiantInterne() Group by the identifiant_interne column
 * @method     ChildMembreQuery groupByCivilite() Group by the civilite column
 * @method     ChildMembreQuery groupByNom() Group by the nom column
 * @method     ChildMembreQuery groupByNomcourt() Group by the nomcourt column
 * @method     ChildMembreQuery groupByDateSortie() Group by the date_sortie column
 * @method     ChildMembreQuery groupByObservation() Group by the observation column
 * @method     ChildMembreQuery groupByCreatedAt() Group by the created_at column
 * @method     ChildMembreQuery groupByUpdatedAt() Group by the updated_at column
 *
 * @method     ChildMembreQuery leftJoin($relation) Adds a LEFT JOIN clause to the query
 * @method     ChildMembreQuery rightJoin($relation) Adds a RIGHT JOIN clause to the query
 * @method     ChildMembreQuery innerJoin($relation) Adds a INNER JOIN clause to the query
 *
 * @method     ChildMembreQuery leftJoinWith($relation) Adds a LEFT JOIN clause and with to the query
 * @method     ChildMembreQuery rightJoinWith($relation) Adds a RIGHT JOIN clause and with to the query
 * @method     ChildMembreQuery innerJoinWith($relation) Adds a INNER JOIN clause and with to the query
 *
 * @method     ChildMembreQuery leftJoinIndividuTitulaire($relationAlias = null) Adds a LEFT JOIN clause to the query using the IndividuTitulaire relation
 * @method     ChildMembreQuery rightJoinIndividuTitulaire($relationAlias = null) Adds a RIGHT JOIN clause to the query using the IndividuTitulaire relation
 * @method     ChildMembreQuery innerJoinIndividuTitulaire($relationAlias = null) Adds a INNER JOIN clause to the query using the IndividuTitulaire relation
 *
 * @method     ChildMembreQuery joinWithIndividuTitulaire($joinType = Criteria::INNER_JOIN) Adds a join clause and with to the query using the IndividuTitulaire relation
 *
 * @method     ChildMembreQuery leftJoinWithIndividuTitulaire() Adds a LEFT JOIN clause and with to the query using the IndividuTitulaire relation
 * @method     ChildMembreQuery rightJoinWithIndividuTitulaire() Adds a RIGHT JOIN clause and with to the query using the IndividuTitulaire relation
 * @method     ChildMembreQuery innerJoinWithIndividuTitulaire() Adds a INNER JOIN clause and with to the query using the IndividuTitulaire relation
 *
 * @method     ChildMembreQuery leftJoinMembreIndividu($relationAlias = null) Adds a LEFT JOIN clause to the query using the MembreIndividu relation
 * @method     ChildMembreQuery rightJoinMembreIndividu($relationAlias = null) Adds a RIGHT JOIN clause to the query using the MembreIndividu relation
 * @method     ChildMembreQuery innerJoinMembreIndividu($relationAlias = null) Adds a INNER JOIN clause to the query using the MembreIndividu relation
 *
 * @method     ChildMembreQuery joinWithMembreIndividu($joinType = Criteria::INNER_JOIN) Adds a join clause and with to the query using the MembreIndividu relation
 *
 * @method     ChildMembreQuery leftJoinWithMembreIndividu() Adds a LEFT JOIN clause and with to the query using the MembreIndividu relation
 * @method     ChildMembreQuery rightJoinWithMembreIndividu() Adds a RIGHT JOIN clause and with to the query using the MembreIndividu relation
 * @method     ChildMembreQuery innerJoinWithMembreIndividu() Adds a INNER JOIN clause and with to the query using the MembreIndividu relation
 *
 * @method     ChildMembreQuery leftJoinServicerendu($relationAlias = null) Adds a LEFT JOIN clause to the query using the Servicerendu relation
 * @method     ChildMembreQuery rightJoinServicerendu($relationAlias = null) Adds a RIGHT JOIN clause to the query using the Servicerendu relation
 * @method     ChildMembreQuery innerJoinServicerendu($relationAlias = null) Adds a INNER JOIN clause to the query using the Servicerendu relation
 *
 * @method     ChildMembreQuery joinWithServicerendu($joinType = Criteria::INNER_JOIN) Adds a join clause and with to the query using the Servicerendu relation
 *
 * @method     ChildMembreQuery leftJoinWithServicerendu() Adds a LEFT JOIN clause and with to the query using the Servicerendu relation
 * @method     ChildMembreQuery rightJoinWithServicerendu() Adds a RIGHT JOIN clause and with to the query using the Servicerendu relation
 * @method     ChildMembreQuery innerJoinWithServicerendu() Adds a INNER JOIN clause and with to the query using the Servicerendu relation
 *
 * @method     ChildMembreQuery leftJoinCourrierLien($relationAlias = null) Adds a LEFT JOIN clause to the query using the CourrierLien relation
 * @method     ChildMembreQuery rightJoinCourrierLien($relationAlias = null) Adds a RIGHT JOIN clause to the query using the CourrierLien relation
 * @method     ChildMembreQuery innerJoinCourrierLien($relationAlias = null) Adds a INNER JOIN clause to the query using the CourrierLien relation
 *
 * @method     ChildMembreQuery joinWithCourrierLien($joinType = Criteria::INNER_JOIN) Adds a join clause and with to the query using the CourrierLien relation
 *
 * @method     ChildMembreQuery leftJoinWithCourrierLien() Adds a LEFT JOIN clause and with to the query using the CourrierLien relation
 * @method     ChildMembreQuery rightJoinWithCourrierLien() Adds a RIGHT JOIN clause and with to the query using the CourrierLien relation
 * @method     ChildMembreQuery innerJoinWithCourrierLien() Adds a INNER JOIN clause and with to the query using the CourrierLien relation
 *
 * @method     \IndividuQuery|\MembreIndividuQuery|\ServicerenduQuery|\CourrierLienQuery endUse() Finalizes a secondary criteria and merges it with its primary Criteria
 *
 * @method     ChildMembre findOne(ConnectionInterface $con = null) Return the first ChildMembre matching the query
 * @method     ChildMembre findOneOrCreate(ConnectionInterface $con = null) Return the first ChildMembre matching the query, or a new ChildMembre object populated from the query conditions when no match is found
 *
 * @method     ChildMembre findOneByIdMembre(string $id_membre) Return the first ChildMembre filtered by the id_membre column
 * @method     ChildMembre findOneByIdIndividuTitulaire(string $id_individu_titulaire) Return the first ChildMembre filtered by the id_individu_titulaire column
 * @method     ChildMembre findOneByIdentifiantInterne(string $identifiant_interne) Return the first ChildMembre filtered by the identifiant_interne column
 * @method     ChildMembre findOneByCivilite(string $civilite) Return the first ChildMembre filtered by the civilite column
 * @method     ChildMembre findOneByNom(string $nom) Return the first ChildMembre filtered by the nom column
 * @method     ChildMembre findOneByNomcourt(string $nomcourt) Return the first ChildMembre filtered by the nomcourt column
 * @method     ChildMembre findOneByDateSortie(string $date_sortie) Return the first ChildMembre filtered by the date_sortie column
 * @method     ChildMembre findOneByObservation(string $observation) Return the first ChildMembre filtered by the observation column
 * @method     ChildMembre findOneByCreatedAt(string $created_at) Return the first ChildMembre filtered by the created_at column
 * @method     ChildMembre findOneByUpdatedAt(string $updated_at) Return the first ChildMembre filtered by the updated_at column *

 * @method     ChildMembre requirePk($key, ConnectionInterface $con = null) Return the ChildMembre by primary key and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildMembre requireOne(ConnectionInterface $con = null) Return the first ChildMembre matching the query and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 *
 * @method     ChildMembre requireOneByIdMembre(string $id_membre) Return the first ChildMembre filtered by the id_membre column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildMembre requireOneByIdIndividuTitulaire(string $id_individu_titulaire) Return the first ChildMembre filtered by the id_individu_titulaire column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildMembre requireOneByIdentifiantInterne(string $identifiant_interne) Return the first ChildMembre filtered by the identifiant_interne column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildMembre requireOneByCivilite(string $civilite) Return the first ChildMembre filtered by the civilite column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildMembre requireOneByNom(string $nom) Return the first ChildMembre filtered by the nom column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildMembre requireOneByNomcourt(string $nomcourt) Return the first ChildMembre filtered by the nomcourt column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildMembre requireOneByDateSortie(string $date_sortie) Return the first ChildMembre filtered by the date_sortie column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildMembre requireOneByObservation(string $observation) Return the first ChildMembre filtered by the observation column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildMembre requireOneByCreatedAt(string $created_at) Return the first ChildMembre filtered by the created_at column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildMembre requireOneByUpdatedAt(string $updated_at) Return the first ChildMembre filtered by the updated_at column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 *
 * @method     ChildMembre[]|ObjectCollection find(ConnectionInterface $con = null) Return ChildMembre objects based on current ModelCriteria
 * @method     ChildMembre[]|ObjectCollection findByIdMembre(string $id_membre) Return ChildMembre objects filtered by the id_membre column
 * @method     ChildMembre[]|ObjectCollection findByIdIndividuTitulaire(string $id_individu_titulaire) Return ChildMembre objects filtered by the id_individu_titulaire column
 * @method     ChildMembre[]|ObjectCollection findByIdentifiantInterne(string $identifiant_interne) Return ChildMembre objects filtered by the identifiant_interne column
 * @method     ChildMembre[]|ObjectCollection findByCivilite(string $civilite) Return ChildMembre objects filtered by the civilite column
 * @method     ChildMembre[]|ObjectCollection findByNom(string $nom) Return ChildMembre objects filtered by the nom column
 * @method     ChildMembre[]|ObjectCollection findByNomcourt(string $nomcourt) Return ChildMembre objects filtered by the nomcourt column
 * @method     ChildMembre[]|ObjectCollection findByDateSortie(string $date_sortie) Return ChildMembre objects filtered by the date_sortie column
 * @method     ChildMembre[]|ObjectCollection findByObservation(string $observation) Return ChildMembre objects filtered by the observation column
 * @method     ChildMembre[]|ObjectCollection findByCreatedAt(string $created_at) Return ChildMembre objects filtered by the created_at column
 * @method     ChildMembre[]|ObjectCollection findByUpdatedAt(string $updated_at) Return ChildMembre objects filtered by the updated_at column
 * @method     ChildMembre[]|\Propel\Runtime\Util\PropelModelPager paginate($page = 1, $maxPerPage = 10, ConnectionInterface $con = null) Issue a SELECT query based on the current ModelCriteria and uses a page and a maximum number of results per page to compute an offset and a limit
 *
 */
abstract class MembreQuery extends ModelCriteria
{

    // archivable behavior
    protected $archiveOnDelete = true;
protected $entityNotFoundExceptionClass = '\\Propel\\Runtime\\Exception\\EntityNotFoundException';

    /**
     * Initializes internal state of \Base\MembreQuery object.
     *
     * @param     string $dbName The database name
     * @param     string $modelName The phpName of a model, e.g. 'Book'
     * @param     string $modelAlias The alias for the model in this query, e.g. 'b'
     */
    public function __construct($dbName = 'simplasso', $modelName = '\\Membre', $modelAlias = null)
    {
        parent::__construct($dbName, $modelName, $modelAlias);
    }

    /**
     * Returns a new ChildMembreQuery object.
     *
     * @param     string $modelAlias The alias of a model in the query
     * @param     Criteria $criteria Optional Criteria to build the query from
     *
     * @return ChildMembreQuery
     */
    public static function create($modelAlias = null, Criteria $criteria = null)
    {
        if ($criteria instanceof ChildMembreQuery) {
            return $criteria;
        }
        $query = new ChildMembreQuery();
        if (null !== $modelAlias) {
            $query->setModelAlias($modelAlias);
        }
        if ($criteria instanceof Criteria) {
            $query->mergeWith($criteria);
        }

        return $query;
    }

    /**
     * Find object by primary key.
     * Propel uses the instance pool to skip the database if the object exists.
     * Go fast if the query is untouched.
     *
     * <code>
     * $obj  = $c->findPk(12, $con);
     * </code>
     *
     * @param mixed $key Primary key to use for the query
     * @param ConnectionInterface $con an optional connection object
     *
     * @return ChildMembre|array|mixed the result, formatted by the current formatter
     */
    public function findPk($key, ConnectionInterface $con = null)
    {
        if ($key === null) {
            return null;
        }

        if ($con === null) {
            $con = Propel::getServiceContainer()->getReadConnection(MembreTableMap::DATABASE_NAME);
        }

        $this->basePreSelect($con);

        if (
            $this->formatter || $this->modelAlias || $this->with || $this->select
            || $this->selectColumns || $this->asColumns || $this->selectModifiers
            || $this->map || $this->having || $this->joins
        ) {
            return $this->findPkComplex($key, $con);
        }

        if ((null !== ($obj = MembreTableMap::getInstanceFromPool(null === $key || is_scalar($key) || is_callable([$key, '__toString']) ? (string) $key : $key)))) {
            // the object is already in the instance pool
            return $obj;
        }

        return $this->findPkSimple($key, $con);
    }

    /**
     * Find object by primary key using raw SQL to go fast.
     * Bypass doSelect() and the object formatter by using generated code.
     *
     * @param     mixed $key Primary key to use for the query
     * @param     ConnectionInterface $con A connection object
     *
     * @throws \Propel\Runtime\Exception\PropelException
     *
     * @return ChildMembre A model object, or null if the key is not found
     */
    protected function findPkSimple($key, ConnectionInterface $con)
    {
        $sql = 'SELECT `id_membre`, `id_individu_titulaire`, `identifiant_interne`, `civilite`, `nom`, `nomcourt`, `date_sortie`, `observation`, `created_at`, `updated_at` FROM `asso_membres` WHERE `id_membre` = :p0';
        try {
            $stmt = $con->prepare($sql);
            $stmt->bindValue(':p0', $key, PDO::PARAM_INT);
            $stmt->execute();
        } catch (Exception $e) {
            Propel::log($e->getMessage(), Propel::LOG_ERR);
            throw new PropelException(sprintf('Unable to execute SELECT statement [%s]', $sql), 0, $e);
        }
        $obj = null;
        if ($row = $stmt->fetch(\PDO::FETCH_NUM)) {
            /** @var ChildMembre $obj */
            $obj = new ChildMembre();
            $obj->hydrate($row);
            MembreTableMap::addInstanceToPool($obj, null === $key || is_scalar($key) || is_callable([$key, '__toString']) ? (string) $key : $key);
        }
        $stmt->closeCursor();

        return $obj;
    }

    /**
     * Find object by primary key.
     *
     * @param     mixed $key Primary key to use for the query
     * @param     ConnectionInterface $con A connection object
     *
     * @return ChildMembre|array|mixed the result, formatted by the current formatter
     */
    protected function findPkComplex($key, ConnectionInterface $con)
    {
        // As the query uses a PK condition, no limit(1) is necessary.
        $criteria = $this->isKeepQuery() ? clone $this : $this;
        $dataFetcher = $criteria
            ->filterByPrimaryKey($key)
            ->doSelect($con);

        return $criteria->getFormatter()->init($criteria)->formatOne($dataFetcher);
    }

    /**
     * Find objects by primary key
     * <code>
     * $objs = $c->findPks(array(12, 56, 832), $con);
     * </code>
     * @param     array $keys Primary keys to use for the query
     * @param     ConnectionInterface $con an optional connection object
     *
     * @return ObjectCollection|array|mixed the list of results, formatted by the current formatter
     */
    public function findPks($keys, ConnectionInterface $con = null)
    {
        if (null === $con) {
            $con = Propel::getServiceContainer()->getReadConnection($this->getDbName());
        }
        $this->basePreSelect($con);
        $criteria = $this->isKeepQuery() ? clone $this : $this;
        $dataFetcher = $criteria
            ->filterByPrimaryKeys($keys)
            ->doSelect($con);

        return $criteria->getFormatter()->init($criteria)->format($dataFetcher);
    }

    /**
     * Filter the query by primary key
     *
     * @param     mixed $key Primary key to use for the query
     *
     * @return $this|ChildMembreQuery The current query, for fluid interface
     */
    public function filterByPrimaryKey($key)
    {

        return $this->addUsingAlias(MembreTableMap::COL_ID_MEMBRE, $key, Criteria::EQUAL);
    }

    /**
     * Filter the query by a list of primary keys
     *
     * @param     array $keys The list of primary key to use for the query
     *
     * @return $this|ChildMembreQuery The current query, for fluid interface
     */
    public function filterByPrimaryKeys($keys)
    {

        return $this->addUsingAlias(MembreTableMap::COL_ID_MEMBRE, $keys, Criteria::IN);
    }

    /**
     * Filter the query on the id_membre column
     *
     * Example usage:
     * <code>
     * $query->filterByIdMembre(1234); // WHERE id_membre = 1234
     * $query->filterByIdMembre(array(12, 34)); // WHERE id_membre IN (12, 34)
     * $query->filterByIdMembre(array('min' => 12)); // WHERE id_membre > 12
     * </code>
     *
     * @param     mixed $idMembre The value to use as filter.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildMembreQuery The current query, for fluid interface
     */
    public function filterByIdMembre($idMembre = null, $comparison = null)
    {
        if (is_array($idMembre)) {
            $useMinMax = false;
            if (isset($idMembre['min'])) {
                $this->addUsingAlias(MembreTableMap::COL_ID_MEMBRE, $idMembre['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($idMembre['max'])) {
                $this->addUsingAlias(MembreTableMap::COL_ID_MEMBRE, $idMembre['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(MembreTableMap::COL_ID_MEMBRE, $idMembre, $comparison);
    }

    /**
     * Filter the query on the id_individu_titulaire column
     *
     * Example usage:
     * <code>
     * $query->filterByIdIndividuTitulaire(1234); // WHERE id_individu_titulaire = 1234
     * $query->filterByIdIndividuTitulaire(array(12, 34)); // WHERE id_individu_titulaire IN (12, 34)
     * $query->filterByIdIndividuTitulaire(array('min' => 12)); // WHERE id_individu_titulaire > 12
     * </code>
     *
     * @see       filterByIndividuTitulaire()
     *
     * @param     mixed $idIndividuTitulaire The value to use as filter.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildMembreQuery The current query, for fluid interface
     */
    public function filterByIdIndividuTitulaire($idIndividuTitulaire = null, $comparison = null)
    {
        if (is_array($idIndividuTitulaire)) {
            $useMinMax = false;
            if (isset($idIndividuTitulaire['min'])) {
                $this->addUsingAlias(MembreTableMap::COL_ID_INDIVIDU_TITULAIRE, $idIndividuTitulaire['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($idIndividuTitulaire['max'])) {
                $this->addUsingAlias(MembreTableMap::COL_ID_INDIVIDU_TITULAIRE, $idIndividuTitulaire['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(MembreTableMap::COL_ID_INDIVIDU_TITULAIRE, $idIndividuTitulaire, $comparison);
    }

    /**
     * Filter the query on the identifiant_interne column
     *
     * Example usage:
     * <code>
     * $query->filterByIdentifiantInterne('fooValue');   // WHERE identifiant_interne = 'fooValue'
     * $query->filterByIdentifiantInterne('%fooValue%', Criteria::LIKE); // WHERE identifiant_interne LIKE '%fooValue%'
     * </code>
     *
     * @param     string $identifiantInterne The value to use as filter.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildMembreQuery The current query, for fluid interface
     */
    public function filterByIdentifiantInterne($identifiantInterne = null, $comparison = null)
    {
        if (null === $comparison) {
            if (is_array($identifiantInterne)) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(MembreTableMap::COL_IDENTIFIANT_INTERNE, $identifiantInterne, $comparison);
    }

    /**
     * Filter the query on the civilite column
     *
     * Example usage:
     * <code>
     * $query->filterByCivilite('fooValue');   // WHERE civilite = 'fooValue'
     * $query->filterByCivilite('%fooValue%', Criteria::LIKE); // WHERE civilite LIKE '%fooValue%'
     * </code>
     *
     * @param     string $civilite The value to use as filter.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildMembreQuery The current query, for fluid interface
     */
    public function filterByCivilite($civilite = null, $comparison = null)
    {
        if (null === $comparison) {
            if (is_array($civilite)) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(MembreTableMap::COL_CIVILITE, $civilite, $comparison);
    }

    /**
     * Filter the query on the nom column
     *
     * Example usage:
     * <code>
     * $query->filterByNom('fooValue');   // WHERE nom = 'fooValue'
     * $query->filterByNom('%fooValue%', Criteria::LIKE); // WHERE nom LIKE '%fooValue%'
     * </code>
     *
     * @param     string $nom The value to use as filter.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildMembreQuery The current query, for fluid interface
     */
    public function filterByNom($nom = null, $comparison = null)
    {
        if (null === $comparison) {
            if (is_array($nom)) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(MembreTableMap::COL_NOM, $nom, $comparison);
    }

    /**
     * Filter the query on the nomcourt column
     *
     * Example usage:
     * <code>
     * $query->filterByNomcourt('fooValue');   // WHERE nomcourt = 'fooValue'
     * $query->filterByNomcourt('%fooValue%', Criteria::LIKE); // WHERE nomcourt LIKE '%fooValue%'
     * </code>
     *
     * @param     string $nomcourt The value to use as filter.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildMembreQuery The current query, for fluid interface
     */
    public function filterByNomcourt($nomcourt = null, $comparison = null)
    {
        if (null === $comparison) {
            if (is_array($nomcourt)) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(MembreTableMap::COL_NOMCOURT, $nomcourt, $comparison);
    }

    /**
     * Filter the query on the date_sortie column
     *
     * Example usage:
     * <code>
     * $query->filterByDateSortie('2011-03-14'); // WHERE date_sortie = '2011-03-14'
     * $query->filterByDateSortie('now'); // WHERE date_sortie = '2011-03-14'
     * $query->filterByDateSortie(array('max' => 'yesterday')); // WHERE date_sortie > '2011-03-13'
     * </code>
     *
     * @param     mixed $dateSortie The value to use as filter.
     *              Values can be integers (unix timestamps), DateTime objects, or strings.
     *              Empty strings are treated as NULL.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildMembreQuery The current query, for fluid interface
     */
    public function filterByDateSortie($dateSortie = null, $comparison = null)
    {
        if (is_array($dateSortie)) {
            $useMinMax = false;
            if (isset($dateSortie['min'])) {
                $this->addUsingAlias(MembreTableMap::COL_DATE_SORTIE, $dateSortie['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($dateSortie['max'])) {
                $this->addUsingAlias(MembreTableMap::COL_DATE_SORTIE, $dateSortie['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(MembreTableMap::COL_DATE_SORTIE, $dateSortie, $comparison);
    }

    /**
     * Filter the query on the observation column
     *
     * Example usage:
     * <code>
     * $query->filterByObservation('fooValue');   // WHERE observation = 'fooValue'
     * $query->filterByObservation('%fooValue%', Criteria::LIKE); // WHERE observation LIKE '%fooValue%'
     * </code>
     *
     * @param     string $observation The value to use as filter.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildMembreQuery The current query, for fluid interface
     */
    public function filterByObservation($observation = null, $comparison = null)
    {
        if (null === $comparison) {
            if (is_array($observation)) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(MembreTableMap::COL_OBSERVATION, $observation, $comparison);
    }

    /**
     * Filter the query on the created_at column
     *
     * Example usage:
     * <code>
     * $query->filterByCreatedAt('2011-03-14'); // WHERE created_at = '2011-03-14'
     * $query->filterByCreatedAt('now'); // WHERE created_at = '2011-03-14'
     * $query->filterByCreatedAt(array('max' => 'yesterday')); // WHERE created_at > '2011-03-13'
     * </code>
     *
     * @param     mixed $createdAt The value to use as filter.
     *              Values can be integers (unix timestamps), DateTime objects, or strings.
     *              Empty strings are treated as NULL.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildMembreQuery The current query, for fluid interface
     */
    public function filterByCreatedAt($createdAt = null, $comparison = null)
    {
        if (is_array($createdAt)) {
            $useMinMax = false;
            if (isset($createdAt['min'])) {
                $this->addUsingAlias(MembreTableMap::COL_CREATED_AT, $createdAt['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($createdAt['max'])) {
                $this->addUsingAlias(MembreTableMap::COL_CREATED_AT, $createdAt['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(MembreTableMap::COL_CREATED_AT, $createdAt, $comparison);
    }

    /**
     * Filter the query on the updated_at column
     *
     * Example usage:
     * <code>
     * $query->filterByUpdatedAt('2011-03-14'); // WHERE updated_at = '2011-03-14'
     * $query->filterByUpdatedAt('now'); // WHERE updated_at = '2011-03-14'
     * $query->filterByUpdatedAt(array('max' => 'yesterday')); // WHERE updated_at > '2011-03-13'
     * </code>
     *
     * @param     mixed $updatedAt The value to use as filter.
     *              Values can be integers (unix timestamps), DateTime objects, or strings.
     *              Empty strings are treated as NULL.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildMembreQuery The current query, for fluid interface
     */
    public function filterByUpdatedAt($updatedAt = null, $comparison = null)
    {
        if (is_array($updatedAt)) {
            $useMinMax = false;
            if (isset($updatedAt['min'])) {
                $this->addUsingAlias(MembreTableMap::COL_UPDATED_AT, $updatedAt['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($updatedAt['max'])) {
                $this->addUsingAlias(MembreTableMap::COL_UPDATED_AT, $updatedAt['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(MembreTableMap::COL_UPDATED_AT, $updatedAt, $comparison);
    }

    /**
     * Filter the query by a related \Individu object
     *
     * @param \Individu|ObjectCollection $individu The related object(s) to use as filter
     * @param string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @throws \Propel\Runtime\Exception\PropelException
     *
     * @return ChildMembreQuery The current query, for fluid interface
     */
    public function filterByIndividuTitulaire($individu, $comparison = null)
    {
        if ($individu instanceof \Individu) {
            return $this
                ->addUsingAlias(MembreTableMap::COL_ID_INDIVIDU_TITULAIRE, $individu->getIdIndividu(), $comparison);
        } elseif ($individu instanceof ObjectCollection) {
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }

            return $this
                ->addUsingAlias(MembreTableMap::COL_ID_INDIVIDU_TITULAIRE, $individu->toKeyValue('PrimaryKey', 'IdIndividu'), $comparison);
        } else {
            throw new PropelException('filterByIndividuTitulaire() only accepts arguments of type \Individu or Collection');
        }
    }

    /**
     * Adds a JOIN clause to the query using the IndividuTitulaire relation
     *
     * @param     string $relationAlias optional alias for the relation
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return $this|ChildMembreQuery The current query, for fluid interface
     */
    public function joinIndividuTitulaire($relationAlias = null, $joinType = Criteria::INNER_JOIN)
    {
        $tableMap = $this->getTableMap();
        $relationMap = $tableMap->getRelation('IndividuTitulaire');

        // create a ModelJoin object for this join
        $join = new ModelJoin();
        $join->setJoinType($joinType);
        $join->setRelationMap($relationMap, $this->useAliasInSQL ? $this->getModelAlias() : null, $relationAlias);
        if ($previousJoin = $this->getPreviousJoin()) {
            $join->setPreviousJoin($previousJoin);
        }

        // add the ModelJoin to the current object
        if ($relationAlias) {
            $this->addAlias($relationAlias, $relationMap->getRightTable()->getName());
            $this->addJoinObject($join, $relationAlias);
        } else {
            $this->addJoinObject($join, 'IndividuTitulaire');
        }

        return $this;
    }

    /**
     * Use the IndividuTitulaire relation Individu object
     *
     * @see useQuery()
     *
     * @param     string $relationAlias optional alias for the relation,
     *                                   to be used as main alias in the secondary query
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return \IndividuQuery A secondary query class using the current class as primary query
     */
    public function useIndividuTitulaireQuery($relationAlias = null, $joinType = Criteria::INNER_JOIN)
    {
        return $this
            ->joinIndividuTitulaire($relationAlias, $joinType)
            ->useQuery($relationAlias ? $relationAlias : 'IndividuTitulaire', '\IndividuQuery');
    }

    /**
     * Filter the query by a related \MembreIndividu object
     *
     * @param \MembreIndividu|ObjectCollection $membreIndividu the related object to use as filter
     * @param string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return ChildMembreQuery The current query, for fluid interface
     */
    public function filterByMembreIndividu($membreIndividu, $comparison = null)
    {
        if ($membreIndividu instanceof \MembreIndividu) {
            return $this
                ->addUsingAlias(MembreTableMap::COL_ID_MEMBRE, $membreIndividu->getIdMembre(), $comparison);
        } elseif ($membreIndividu instanceof ObjectCollection) {
            return $this
                ->useMembreIndividuQuery()
                ->filterByPrimaryKeys($membreIndividu->getPrimaryKeys())
                ->endUse();
        } else {
            throw new PropelException('filterByMembreIndividu() only accepts arguments of type \MembreIndividu or Collection');
        }
    }

    /**
     * Adds a JOIN clause to the query using the MembreIndividu relation
     *
     * @param     string $relationAlias optional alias for the relation
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return $this|ChildMembreQuery The current query, for fluid interface
     */
    public function joinMembreIndividu($relationAlias = null, $joinType = Criteria::INNER_JOIN)
    {
        $tableMap = $this->getTableMap();
        $relationMap = $tableMap->getRelation('MembreIndividu');

        // create a ModelJoin object for this join
        $join = new ModelJoin();
        $join->setJoinType($joinType);
        $join->setRelationMap($relationMap, $this->useAliasInSQL ? $this->getModelAlias() : null, $relationAlias);
        if ($previousJoin = $this->getPreviousJoin()) {
            $join->setPreviousJoin($previousJoin);
        }

        // add the ModelJoin to the current object
        if ($relationAlias) {
            $this->addAlias($relationAlias, $relationMap->getRightTable()->getName());
            $this->addJoinObject($join, $relationAlias);
        } else {
            $this->addJoinObject($join, 'MembreIndividu');
        }

        return $this;
    }

    /**
     * Use the MembreIndividu relation MembreIndividu object
     *
     * @see useQuery()
     *
     * @param     string $relationAlias optional alias for the relation,
     *                                   to be used as main alias in the secondary query
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return \MembreIndividuQuery A secondary query class using the current class as primary query
     */
    public function useMembreIndividuQuery($relationAlias = null, $joinType = Criteria::INNER_JOIN)
    {
        return $this
            ->joinMembreIndividu($relationAlias, $joinType)
            ->useQuery($relationAlias ? $relationAlias : 'MembreIndividu', '\MembreIndividuQuery');
    }

    /**
     * Filter the query by a related \Servicerendu object
     *
     * @param \Servicerendu|ObjectCollection $servicerendu the related object to use as filter
     * @param string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return ChildMembreQuery The current query, for fluid interface
     */
    public function filterByServicerendu($servicerendu, $comparison = null)
    {
        if ($servicerendu instanceof \Servicerendu) {
            return $this
                ->addUsingAlias(MembreTableMap::COL_ID_MEMBRE, $servicerendu->getIdMembre(), $comparison);
        } elseif ($servicerendu instanceof ObjectCollection) {
            return $this
                ->useServicerenduQuery()
                ->filterByPrimaryKeys($servicerendu->getPrimaryKeys())
                ->endUse();
        } else {
            throw new PropelException('filterByServicerendu() only accepts arguments of type \Servicerendu or Collection');
        }
    }

    /**
     * Adds a JOIN clause to the query using the Servicerendu relation
     *
     * @param     string $relationAlias optional alias for the relation
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return $this|ChildMembreQuery The current query, for fluid interface
     */
    public function joinServicerendu($relationAlias = null, $joinType = Criteria::LEFT_JOIN)
    {
        $tableMap = $this->getTableMap();
        $relationMap = $tableMap->getRelation('Servicerendu');

        // create a ModelJoin object for this join
        $join = new ModelJoin();
        $join->setJoinType($joinType);
        $join->setRelationMap($relationMap, $this->useAliasInSQL ? $this->getModelAlias() : null, $relationAlias);
        if ($previousJoin = $this->getPreviousJoin()) {
            $join->setPreviousJoin($previousJoin);
        }

        // add the ModelJoin to the current object
        if ($relationAlias) {
            $this->addAlias($relationAlias, $relationMap->getRightTable()->getName());
            $this->addJoinObject($join, $relationAlias);
        } else {
            $this->addJoinObject($join, 'Servicerendu');
        }

        return $this;
    }

    /**
     * Use the Servicerendu relation Servicerendu object
     *
     * @see useQuery()
     *
     * @param     string $relationAlias optional alias for the relation,
     *                                   to be used as main alias in the secondary query
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return \ServicerenduQuery A secondary query class using the current class as primary query
     */
    public function useServicerenduQuery($relationAlias = null, $joinType = Criteria::LEFT_JOIN)
    {
        return $this
            ->joinServicerendu($relationAlias, $joinType)
            ->useQuery($relationAlias ? $relationAlias : 'Servicerendu', '\ServicerenduQuery');
    }

    /**
     * Filter the query by a related \CourrierLien object
     *
     * @param \CourrierLien|ObjectCollection $courrierLien the related object to use as filter
     * @param string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return ChildMembreQuery The current query, for fluid interface
     */
    public function filterByCourrierLien($courrierLien, $comparison = null)
    {
        if ($courrierLien instanceof \CourrierLien) {
            return $this
                ->addUsingAlias(MembreTableMap::COL_ID_MEMBRE, $courrierLien->getIdMembre(), $comparison);
        } elseif ($courrierLien instanceof ObjectCollection) {
            return $this
                ->useCourrierLienQuery()
                ->filterByPrimaryKeys($courrierLien->getPrimaryKeys())
                ->endUse();
        } else {
            throw new PropelException('filterByCourrierLien() only accepts arguments of type \CourrierLien or Collection');
        }
    }

    /**
     * Adds a JOIN clause to the query using the CourrierLien relation
     *
     * @param     string $relationAlias optional alias for the relation
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return $this|ChildMembreQuery The current query, for fluid interface
     */
    public function joinCourrierLien($relationAlias = null, $joinType = Criteria::LEFT_JOIN)
    {
        $tableMap = $this->getTableMap();
        $relationMap = $tableMap->getRelation('CourrierLien');

        // create a ModelJoin object for this join
        $join = new ModelJoin();
        $join->setJoinType($joinType);
        $join->setRelationMap($relationMap, $this->useAliasInSQL ? $this->getModelAlias() : null, $relationAlias);
        if ($previousJoin = $this->getPreviousJoin()) {
            $join->setPreviousJoin($previousJoin);
        }

        // add the ModelJoin to the current object
        if ($relationAlias) {
            $this->addAlias($relationAlias, $relationMap->getRightTable()->getName());
            $this->addJoinObject($join, $relationAlias);
        } else {
            $this->addJoinObject($join, 'CourrierLien');
        }

        return $this;
    }

    /**
     * Use the CourrierLien relation CourrierLien object
     *
     * @see useQuery()
     *
     * @param     string $relationAlias optional alias for the relation,
     *                                   to be used as main alias in the secondary query
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return \CourrierLienQuery A secondary query class using the current class as primary query
     */
    public function useCourrierLienQuery($relationAlias = null, $joinType = Criteria::LEFT_JOIN)
    {
        return $this
            ->joinCourrierLien($relationAlias, $joinType)
            ->useQuery($relationAlias ? $relationAlias : 'CourrierLien', '\CourrierLienQuery');
    }

    /**
     * Filter the query by a related Individu object
     * using the asso_membres_individus table as cross reference
     *
     * @param Individu $individu the related object to use as filter
     * @param string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return ChildMembreQuery The current query, for fluid interface
     */
    public function filterByIndividu($individu, $comparison = Criteria::EQUAL)
    {
        return $this
            ->useMembreIndividuQuery()
            ->filterByIndividu($individu, $comparison)
            ->endUse();
    }

    /**
     * Exclude object from result
     *
     * @param   ChildMembre $membre Object to remove from the list of results
     *
     * @return $this|ChildMembreQuery The current query, for fluid interface
     */
    public function prune($membre = null)
    {
        if ($membre) {
            $this->addUsingAlias(MembreTableMap::COL_ID_MEMBRE, $membre->getIdMembre(), Criteria::NOT_EQUAL);
        }

        return $this;
    }

    /**
     * Code to execute before every DELETE statement
     *
     * @param     ConnectionInterface $con The connection object used by the query
     */
    protected function basePreDelete(ConnectionInterface $con)
    {
        // archivable behavior

        if ($this->archiveOnDelete) {
            $this->archive($con);
        } else {
            $this->archiveOnDelete = true;
        }


        return $this->preDelete($con);
    }

    /**
     * Deletes all rows from the asso_membres table.
     *
     * @param ConnectionInterface $con the connection to use
     * @return int The number of affected rows (if supported by underlying database driver).
     */
    public function doDeleteAll(ConnectionInterface $con = null)
    {
        if (null === $con) {
            $con = Propel::getServiceContainer()->getWriteConnection(MembreTableMap::DATABASE_NAME);
        }

        // use transaction because $criteria could contain info
        // for more than one table or we could emulating ON DELETE CASCADE, etc.
        return $con->transaction(function () use ($con) {
            $affectedRows = 0; // initialize var to track total num of affected rows
            $affectedRows += parent::doDeleteAll($con);
            // Because this db requires some delete cascade/set null emulation, we have to
            // clear the cached instance *after* the emulation has happened (since
            // instances get re-added by the select statement contained therein).
            MembreTableMap::clearInstancePool();
            MembreTableMap::clearRelatedInstancePool();

            return $affectedRows;
        });
    }

    /**
     * Performs a DELETE on the database based on the current ModelCriteria
     *
     * @param ConnectionInterface $con the connection to use
     * @return int             The number of affected rows (if supported by underlying database driver).  This includes CASCADE-related rows
     *                         if supported by native driver or if emulated using Propel.
     * @throws PropelException Any exceptions caught during processing will be
     *                         rethrown wrapped into a PropelException.
     */
    public function delete(ConnectionInterface $con = null)
    {
        if (null === $con) {
            $con = Propel::getServiceContainer()->getWriteConnection(MembreTableMap::DATABASE_NAME);
        }

        $criteria = $this;

        // Set the correct dbName
        $criteria->setDbName(MembreTableMap::DATABASE_NAME);

        // use transaction because $criteria could contain info
        // for more than one table or we could emulating ON DELETE CASCADE, etc.
        return $con->transaction(function () use ($con, $criteria) {
            $affectedRows = 0; // initialize var to track total num of affected rows

            MembreTableMap::removeInstanceFromPool($criteria);

            $affectedRows += ModelCriteria::delete($con);
            MembreTableMap::clearRelatedInstancePool();

            return $affectedRows;
        });
    }

    // timestampable behavior

    /**
     * Filter by the latest updated
     *
     * @param      int $nbDays Maximum age of the latest update in days
     *
     * @return     $this|ChildMembreQuery The current query, for fluid interface
     */
    public function recentlyUpdated($nbDays = 7)
    {
        return $this->addUsingAlias(MembreTableMap::COL_UPDATED_AT, time() - $nbDays * 24 * 60 * 60, Criteria::GREATER_EQUAL);
    }

    /**
     * Order by update date desc
     *
     * @return     $this|ChildMembreQuery The current query, for fluid interface
     */
    public function lastUpdatedFirst()
    {
        return $this->addDescendingOrderByColumn(MembreTableMap::COL_UPDATED_AT);
    }

    /**
     * Order by update date asc
     *
     * @return     $this|ChildMembreQuery The current query, for fluid interface
     */
    public function firstUpdatedFirst()
    {
        return $this->addAscendingOrderByColumn(MembreTableMap::COL_UPDATED_AT);
    }

    /**
     * Order by create date desc
     *
     * @return     $this|ChildMembreQuery The current query, for fluid interface
     */
    public function lastCreatedFirst()
    {
        return $this->addDescendingOrderByColumn(MembreTableMap::COL_CREATED_AT);
    }

    /**
     * Filter by the latest created
     *
     * @param      int $nbDays Maximum age of in days
     *
     * @return     $this|ChildMembreQuery The current query, for fluid interface
     */
    public function recentlyCreated($nbDays = 7)
    {
        return $this->addUsingAlias(MembreTableMap::COL_CREATED_AT, time() - $nbDays * 24 * 60 * 60, Criteria::GREATER_EQUAL);
    }

    /**
     * Order by create date asc
     *
     * @return     $this|ChildMembreQuery The current query, for fluid interface
     */
    public function firstCreatedFirst()
    {
        return $this->addAscendingOrderByColumn(MembreTableMap::COL_CREATED_AT);
    }

    // archivable behavior

    /**
     * Copy the data of the objects satisfying the query into ChildAssoMembresArchive archive objects.
     * The archived objects are then saved.
     * If any of the objects has already been archived, the archived object
     * is updated and not duplicated.
     * Warning: This termination methods issues 2n+1 queries.
     *
     * @param      ConnectionInterface $con    Connection to use.
     * @param      Boolean $useLittleMemory    Whether or not to use OnDemandFormatter to retrieve objects.
     *               Set to false if the identity map matters.
     *               Set to true (default) to use less memory.
     *
     * @return     int the number of archived objects
     */
    public function archive($con = null, $useLittleMemory = true)
    {
        $criteria = clone $this;
        // prepare the query
        $criteria->setWith(array());
        if ($useLittleMemory) {
            $criteria->setFormatter(ModelCriteria::FORMAT_ON_DEMAND);
        }
        if ($con === null) {
            $con = Propel::getServiceContainer()->getWriteConnection(MembreTableMap::DATABASE_NAME);
        }

        return $con->transaction(function () use ($con, $criteria) {
            $totalArchivedObjects = 0;

            // archive all results one by one
            foreach ($criteria->find($con) as $object) {
                $object->archive($con);
                $totalArchivedObjects++;
            }

            return $totalArchivedObjects;
        });
    }

    /**
     * Enable/disable auto-archiving on delete for the next query.
     *
     * @param boolean True if the query must archive deleted objects, false otherwise.
     */
    public function setArchiveOnDelete($archiveOnDelete)
    {
        $this->archiveOnDelete = $archiveOnDelete;
    }

    /**
     * Delete records matching the current query without archiving them.
     *
     * @param      ConnectionInterface $con    Connection to use.
     *
     * @return integer the number of deleted rows
     */
    public function deleteWithoutArchive($con = null)
    {
        $this->archiveOnDelete = false;

        return $this->delete($con);
    }

    /**
     * Delete all records without archiving them.
     *
     * @param      ConnectionInterface $con    Connection to use.
     *
     * @return integer the number of deleted rows
     */
    public function deleteAllWithoutArchive($con = null)
    {
        $this->archiveOnDelete = false;

        return $this->deleteAll($con);
    }

} // MembreQuery
