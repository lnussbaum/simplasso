<?php

namespace Base;

use \AssoMotgroupesArchive as ChildAssoMotgroupesArchive;
use \Motgroupe as ChildMotgroupe;
use \MotgroupeQuery as ChildMotgroupeQuery;
use \Exception;
use \PDO;
use Map\MotgroupeTableMap;
use Propel\Runtime\Propel;
use Propel\Runtime\ActiveQuery\Criteria;
use Propel\Runtime\ActiveQuery\ModelCriteria;
use Propel\Runtime\ActiveQuery\ModelJoin;
use Propel\Runtime\Collection\ObjectCollection;
use Propel\Runtime\Connection\ConnectionInterface;
use Propel\Runtime\Exception\PropelException;

/**
 * Base class that represents a query for the 'asso_motgroupes' table.
 *
 *
 *
 * @method     ChildMotgroupeQuery orderByIdMotgroupe($order = Criteria::ASC) Order by the id_motgroupe column
 * @method     ChildMotgroupeQuery orderByIdParent($order = Criteria::ASC) Order by the id_parent column
 * @method     ChildMotgroupeQuery orderByNom($order = Criteria::ASC) Order by the nom column
 * @method     ChildMotgroupeQuery orderByNomcourt($order = Criteria::ASC) Order by the nomcourt column
 * @method     ChildMotgroupeQuery orderByDescriptif($order = Criteria::ASC) Order by the descriptif column
 * @method     ChildMotgroupeQuery orderByTexte($order = Criteria::ASC) Order by the texte column
 * @method     ChildMotgroupeQuery orderByImportance($order = Criteria::ASC) Order by the importance column
 * @method     ChildMotgroupeQuery orderByObjetsEnLien($order = Criteria::ASC) Order by the objets_en_lien column
 * @method     ChildMotgroupeQuery orderBySysteme($order = Criteria::ASC) Order by the systeme column
 * @method     ChildMotgroupeQuery orderByActif($order = Criteria::ASC) Order by the actif column
 * @method     ChildMotgroupeQuery orderByOptions($order = Criteria::ASC) Order by the options column
 * @method     ChildMotgroupeQuery orderByCreatedAt($order = Criteria::ASC) Order by the created_at column
 * @method     ChildMotgroupeQuery orderByUpdatedAt($order = Criteria::ASC) Order by the updated_at column
 *
 * @method     ChildMotgroupeQuery groupByIdMotgroupe() Group by the id_motgroupe column
 * @method     ChildMotgroupeQuery groupByIdParent() Group by the id_parent column
 * @method     ChildMotgroupeQuery groupByNom() Group by the nom column
 * @method     ChildMotgroupeQuery groupByNomcourt() Group by the nomcourt column
 * @method     ChildMotgroupeQuery groupByDescriptif() Group by the descriptif column
 * @method     ChildMotgroupeQuery groupByTexte() Group by the texte column
 * @method     ChildMotgroupeQuery groupByImportance() Group by the importance column
 * @method     ChildMotgroupeQuery groupByObjetsEnLien() Group by the objets_en_lien column
 * @method     ChildMotgroupeQuery groupBySysteme() Group by the systeme column
 * @method     ChildMotgroupeQuery groupByActif() Group by the actif column
 * @method     ChildMotgroupeQuery groupByOptions() Group by the options column
 * @method     ChildMotgroupeQuery groupByCreatedAt() Group by the created_at column
 * @method     ChildMotgroupeQuery groupByUpdatedAt() Group by the updated_at column
 *
 * @method     ChildMotgroupeQuery leftJoin($relation) Adds a LEFT JOIN clause to the query
 * @method     ChildMotgroupeQuery rightJoin($relation) Adds a RIGHT JOIN clause to the query
 * @method     ChildMotgroupeQuery innerJoin($relation) Adds a INNER JOIN clause to the query
 *
 * @method     ChildMotgroupeQuery leftJoinWith($relation) Adds a LEFT JOIN clause and with to the query
 * @method     ChildMotgroupeQuery rightJoinWith($relation) Adds a RIGHT JOIN clause and with to the query
 * @method     ChildMotgroupeQuery innerJoinWith($relation) Adds a INNER JOIN clause and with to the query
 *
 * @method     ChildMotgroupeQuery leftJoinMot($relationAlias = null) Adds a LEFT JOIN clause to the query using the Mot relation
 * @method     ChildMotgroupeQuery rightJoinMot($relationAlias = null) Adds a RIGHT JOIN clause to the query using the Mot relation
 * @method     ChildMotgroupeQuery innerJoinMot($relationAlias = null) Adds a INNER JOIN clause to the query using the Mot relation
 *
 * @method     ChildMotgroupeQuery joinWithMot($joinType = Criteria::INNER_JOIN) Adds a join clause and with to the query using the Mot relation
 *
 * @method     ChildMotgroupeQuery leftJoinWithMot() Adds a LEFT JOIN clause and with to the query using the Mot relation
 * @method     ChildMotgroupeQuery rightJoinWithMot() Adds a RIGHT JOIN clause and with to the query using the Mot relation
 * @method     ChildMotgroupeQuery innerJoinWithMot() Adds a INNER JOIN clause and with to the query using the Mot relation
 *
 * @method     \MotQuery endUse() Finalizes a secondary criteria and merges it with its primary Criteria
 *
 * @method     ChildMotgroupe findOne(ConnectionInterface $con = null) Return the first ChildMotgroupe matching the query
 * @method     ChildMotgroupe findOneOrCreate(ConnectionInterface $con = null) Return the first ChildMotgroupe matching the query, or a new ChildMotgroupe object populated from the query conditions when no match is found
 *
 * @method     ChildMotgroupe findOneByIdMotgroupe(string $id_motgroupe) Return the first ChildMotgroupe filtered by the id_motgroupe column
 * @method     ChildMotgroupe findOneByIdParent(string $id_parent) Return the first ChildMotgroupe filtered by the id_parent column
 * @method     ChildMotgroupe findOneByNom(string $nom) Return the first ChildMotgroupe filtered by the nom column
 * @method     ChildMotgroupe findOneByNomcourt(string $nomcourt) Return the first ChildMotgroupe filtered by the nomcourt column
 * @method     ChildMotgroupe findOneByDescriptif(string $descriptif) Return the first ChildMotgroupe filtered by the descriptif column
 * @method     ChildMotgroupe findOneByTexte(string $texte) Return the first ChildMotgroupe filtered by the texte column
 * @method     ChildMotgroupe findOneByImportance(int $importance) Return the first ChildMotgroupe filtered by the importance column
 * @method     ChildMotgroupe findOneByObjetsEnLien(string $objets_en_lien) Return the first ChildMotgroupe filtered by the objets_en_lien column
 * @method     ChildMotgroupe findOneBySysteme(boolean $systeme) Return the first ChildMotgroupe filtered by the systeme column
 * @method     ChildMotgroupe findOneByActif(int $actif) Return the first ChildMotgroupe filtered by the actif column
 * @method     ChildMotgroupe findOneByOptions(string $options) Return the first ChildMotgroupe filtered by the options column
 * @method     ChildMotgroupe findOneByCreatedAt(string $created_at) Return the first ChildMotgroupe filtered by the created_at column
 * @method     ChildMotgroupe findOneByUpdatedAt(string $updated_at) Return the first ChildMotgroupe filtered by the updated_at column *

 * @method     ChildMotgroupe requirePk($key, ConnectionInterface $con = null) Return the ChildMotgroupe by primary key and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildMotgroupe requireOne(ConnectionInterface $con = null) Return the first ChildMotgroupe matching the query and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 *
 * @method     ChildMotgroupe requireOneByIdMotgroupe(string $id_motgroupe) Return the first ChildMotgroupe filtered by the id_motgroupe column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildMotgroupe requireOneByIdParent(string $id_parent) Return the first ChildMotgroupe filtered by the id_parent column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildMotgroupe requireOneByNom(string $nom) Return the first ChildMotgroupe filtered by the nom column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildMotgroupe requireOneByNomcourt(string $nomcourt) Return the first ChildMotgroupe filtered by the nomcourt column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildMotgroupe requireOneByDescriptif(string $descriptif) Return the first ChildMotgroupe filtered by the descriptif column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildMotgroupe requireOneByTexte(string $texte) Return the first ChildMotgroupe filtered by the texte column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildMotgroupe requireOneByImportance(int $importance) Return the first ChildMotgroupe filtered by the importance column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildMotgroupe requireOneByObjetsEnLien(string $objets_en_lien) Return the first ChildMotgroupe filtered by the objets_en_lien column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildMotgroupe requireOneBySysteme(boolean $systeme) Return the first ChildMotgroupe filtered by the systeme column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildMotgroupe requireOneByActif(int $actif) Return the first ChildMotgroupe filtered by the actif column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildMotgroupe requireOneByOptions(string $options) Return the first ChildMotgroupe filtered by the options column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildMotgroupe requireOneByCreatedAt(string $created_at) Return the first ChildMotgroupe filtered by the created_at column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildMotgroupe requireOneByUpdatedAt(string $updated_at) Return the first ChildMotgroupe filtered by the updated_at column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 *
 * @method     ChildMotgroupe[]|ObjectCollection find(ConnectionInterface $con = null) Return ChildMotgroupe objects based on current ModelCriteria
 * @method     ChildMotgroupe[]|ObjectCollection findByIdMotgroupe(string $id_motgroupe) Return ChildMotgroupe objects filtered by the id_motgroupe column
 * @method     ChildMotgroupe[]|ObjectCollection findByIdParent(string $id_parent) Return ChildMotgroupe objects filtered by the id_parent column
 * @method     ChildMotgroupe[]|ObjectCollection findByNom(string $nom) Return ChildMotgroupe objects filtered by the nom column
 * @method     ChildMotgroupe[]|ObjectCollection findByNomcourt(string $nomcourt) Return ChildMotgroupe objects filtered by the nomcourt column
 * @method     ChildMotgroupe[]|ObjectCollection findByDescriptif(string $descriptif) Return ChildMotgroupe objects filtered by the descriptif column
 * @method     ChildMotgroupe[]|ObjectCollection findByTexte(string $texte) Return ChildMotgroupe objects filtered by the texte column
 * @method     ChildMotgroupe[]|ObjectCollection findByImportance(int $importance) Return ChildMotgroupe objects filtered by the importance column
 * @method     ChildMotgroupe[]|ObjectCollection findByObjetsEnLien(string $objets_en_lien) Return ChildMotgroupe objects filtered by the objets_en_lien column
 * @method     ChildMotgroupe[]|ObjectCollection findBySysteme(boolean $systeme) Return ChildMotgroupe objects filtered by the systeme column
 * @method     ChildMotgroupe[]|ObjectCollection findByActif(int $actif) Return ChildMotgroupe objects filtered by the actif column
 * @method     ChildMotgroupe[]|ObjectCollection findByOptions(string $options) Return ChildMotgroupe objects filtered by the options column
 * @method     ChildMotgroupe[]|ObjectCollection findByCreatedAt(string $created_at) Return ChildMotgroupe objects filtered by the created_at column
 * @method     ChildMotgroupe[]|ObjectCollection findByUpdatedAt(string $updated_at) Return ChildMotgroupe objects filtered by the updated_at column
 * @method     ChildMotgroupe[]|\Propel\Runtime\Util\PropelModelPager paginate($page = 1, $maxPerPage = 10, ConnectionInterface $con = null) Issue a SELECT query based on the current ModelCriteria and uses a page and a maximum number of results per page to compute an offset and a limit
 *
 */
abstract class MotgroupeQuery extends ModelCriteria
{

    // archivable behavior
    protected $archiveOnDelete = true;
protected $entityNotFoundExceptionClass = '\\Propel\\Runtime\\Exception\\EntityNotFoundException';

    /**
     * Initializes internal state of \Base\MotgroupeQuery object.
     *
     * @param     string $dbName The database name
     * @param     string $modelName The phpName of a model, e.g. 'Book'
     * @param     string $modelAlias The alias for the model in this query, e.g. 'b'
     */
    public function __construct($dbName = 'simplasso', $modelName = '\\Motgroupe', $modelAlias = null)
    {
        parent::__construct($dbName, $modelName, $modelAlias);
    }

    /**
     * Returns a new ChildMotgroupeQuery object.
     *
     * @param     string $modelAlias The alias of a model in the query
     * @param     Criteria $criteria Optional Criteria to build the query from
     *
     * @return ChildMotgroupeQuery
     */
    public static function create($modelAlias = null, Criteria $criteria = null)
    {
        if ($criteria instanceof ChildMotgroupeQuery) {
            return $criteria;
        }
        $query = new ChildMotgroupeQuery();
        if (null !== $modelAlias) {
            $query->setModelAlias($modelAlias);
        }
        if ($criteria instanceof Criteria) {
            $query->mergeWith($criteria);
        }

        return $query;
    }

    /**
     * Find object by primary key.
     * Propel uses the instance pool to skip the database if the object exists.
     * Go fast if the query is untouched.
     *
     * <code>
     * $obj  = $c->findPk(12, $con);
     * </code>
     *
     * @param mixed $key Primary key to use for the query
     * @param ConnectionInterface $con an optional connection object
     *
     * @return ChildMotgroupe|array|mixed the result, formatted by the current formatter
     */
    public function findPk($key, ConnectionInterface $con = null)
    {
        if ($key === null) {
            return null;
        }

        if ($con === null) {
            $con = Propel::getServiceContainer()->getReadConnection(MotgroupeTableMap::DATABASE_NAME);
        }

        $this->basePreSelect($con);

        if (
            $this->formatter || $this->modelAlias || $this->with || $this->select
            || $this->selectColumns || $this->asColumns || $this->selectModifiers
            || $this->map || $this->having || $this->joins
        ) {
            return $this->findPkComplex($key, $con);
        }

        if ((null !== ($obj = MotgroupeTableMap::getInstanceFromPool(null === $key || is_scalar($key) || is_callable([$key, '__toString']) ? (string) $key : $key)))) {
            // the object is already in the instance pool
            return $obj;
        }

        return $this->findPkSimple($key, $con);
    }

    /**
     * Find object by primary key using raw SQL to go fast.
     * Bypass doSelect() and the object formatter by using generated code.
     *
     * @param     mixed $key Primary key to use for the query
     * @param     ConnectionInterface $con A connection object
     *
     * @throws \Propel\Runtime\Exception\PropelException
     *
     * @return ChildMotgroupe A model object, or null if the key is not found
     */
    protected function findPkSimple($key, ConnectionInterface $con)
    {
        $sql = 'SELECT `id_motgroupe`, `id_parent`, `nom`, `nomcourt`, `descriptif`, `texte`, `importance`, `objets_en_lien`, `systeme`, `actif`, `options`, `created_at`, `updated_at` FROM `asso_motgroupes` WHERE `id_motgroupe` = :p0';
        try {
            $stmt = $con->prepare($sql);
            $stmt->bindValue(':p0', $key, PDO::PARAM_INT);
            $stmt->execute();
        } catch (Exception $e) {
            Propel::log($e->getMessage(), Propel::LOG_ERR);
            throw new PropelException(sprintf('Unable to execute SELECT statement [%s]', $sql), 0, $e);
        }
        $obj = null;
        if ($row = $stmt->fetch(\PDO::FETCH_NUM)) {
            /** @var ChildMotgroupe $obj */
            $obj = new ChildMotgroupe();
            $obj->hydrate($row);
            MotgroupeTableMap::addInstanceToPool($obj, null === $key || is_scalar($key) || is_callable([$key, '__toString']) ? (string) $key : $key);
        }
        $stmt->closeCursor();

        return $obj;
    }

    /**
     * Find object by primary key.
     *
     * @param     mixed $key Primary key to use for the query
     * @param     ConnectionInterface $con A connection object
     *
     * @return ChildMotgroupe|array|mixed the result, formatted by the current formatter
     */
    protected function findPkComplex($key, ConnectionInterface $con)
    {
        // As the query uses a PK condition, no limit(1) is necessary.
        $criteria = $this->isKeepQuery() ? clone $this : $this;
        $dataFetcher = $criteria
            ->filterByPrimaryKey($key)
            ->doSelect($con);

        return $criteria->getFormatter()->init($criteria)->formatOne($dataFetcher);
    }

    /**
     * Find objects by primary key
     * <code>
     * $objs = $c->findPks(array(12, 56, 832), $con);
     * </code>
     * @param     array $keys Primary keys to use for the query
     * @param     ConnectionInterface $con an optional connection object
     *
     * @return ObjectCollection|array|mixed the list of results, formatted by the current formatter
     */
    public function findPks($keys, ConnectionInterface $con = null)
    {
        if (null === $con) {
            $con = Propel::getServiceContainer()->getReadConnection($this->getDbName());
        }
        $this->basePreSelect($con);
        $criteria = $this->isKeepQuery() ? clone $this : $this;
        $dataFetcher = $criteria
            ->filterByPrimaryKeys($keys)
            ->doSelect($con);

        return $criteria->getFormatter()->init($criteria)->format($dataFetcher);
    }

    /**
     * Filter the query by primary key
     *
     * @param     mixed $key Primary key to use for the query
     *
     * @return $this|ChildMotgroupeQuery The current query, for fluid interface
     */
    public function filterByPrimaryKey($key)
    {

        return $this->addUsingAlias(MotgroupeTableMap::COL_ID_MOTGROUPE, $key, Criteria::EQUAL);
    }

    /**
     * Filter the query by a list of primary keys
     *
     * @param     array $keys The list of primary key to use for the query
     *
     * @return $this|ChildMotgroupeQuery The current query, for fluid interface
     */
    public function filterByPrimaryKeys($keys)
    {

        return $this->addUsingAlias(MotgroupeTableMap::COL_ID_MOTGROUPE, $keys, Criteria::IN);
    }

    /**
     * Filter the query on the id_motgroupe column
     *
     * Example usage:
     * <code>
     * $query->filterByIdMotgroupe(1234); // WHERE id_motgroupe = 1234
     * $query->filterByIdMotgroupe(array(12, 34)); // WHERE id_motgroupe IN (12, 34)
     * $query->filterByIdMotgroupe(array('min' => 12)); // WHERE id_motgroupe > 12
     * </code>
     *
     * @param     mixed $idMotgroupe The value to use as filter.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildMotgroupeQuery The current query, for fluid interface
     */
    public function filterByIdMotgroupe($idMotgroupe = null, $comparison = null)
    {
        if (is_array($idMotgroupe)) {
            $useMinMax = false;
            if (isset($idMotgroupe['min'])) {
                $this->addUsingAlias(MotgroupeTableMap::COL_ID_MOTGROUPE, $idMotgroupe['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($idMotgroupe['max'])) {
                $this->addUsingAlias(MotgroupeTableMap::COL_ID_MOTGROUPE, $idMotgroupe['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(MotgroupeTableMap::COL_ID_MOTGROUPE, $idMotgroupe, $comparison);
    }

    /**
     * Filter the query on the id_parent column
     *
     * Example usage:
     * <code>
     * $query->filterByIdParent(1234); // WHERE id_parent = 1234
     * $query->filterByIdParent(array(12, 34)); // WHERE id_parent IN (12, 34)
     * $query->filterByIdParent(array('min' => 12)); // WHERE id_parent > 12
     * </code>
     *
     * @param     mixed $idParent The value to use as filter.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildMotgroupeQuery The current query, for fluid interface
     */
    public function filterByIdParent($idParent = null, $comparison = null)
    {
        if (is_array($idParent)) {
            $useMinMax = false;
            if (isset($idParent['min'])) {
                $this->addUsingAlias(MotgroupeTableMap::COL_ID_PARENT, $idParent['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($idParent['max'])) {
                $this->addUsingAlias(MotgroupeTableMap::COL_ID_PARENT, $idParent['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(MotgroupeTableMap::COL_ID_PARENT, $idParent, $comparison);
    }

    /**
     * Filter the query on the nom column
     *
     * Example usage:
     * <code>
     * $query->filterByNom('fooValue');   // WHERE nom = 'fooValue'
     * $query->filterByNom('%fooValue%', Criteria::LIKE); // WHERE nom LIKE '%fooValue%'
     * </code>
     *
     * @param     string $nom The value to use as filter.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildMotgroupeQuery The current query, for fluid interface
     */
    public function filterByNom($nom = null, $comparison = null)
    {
        if (null === $comparison) {
            if (is_array($nom)) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(MotgroupeTableMap::COL_NOM, $nom, $comparison);
    }

    /**
     * Filter the query on the nomcourt column
     *
     * Example usage:
     * <code>
     * $query->filterByNomcourt('fooValue');   // WHERE nomcourt = 'fooValue'
     * $query->filterByNomcourt('%fooValue%', Criteria::LIKE); // WHERE nomcourt LIKE '%fooValue%'
     * </code>
     *
     * @param     string $nomcourt The value to use as filter.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildMotgroupeQuery The current query, for fluid interface
     */
    public function filterByNomcourt($nomcourt = null, $comparison = null)
    {
        if (null === $comparison) {
            if (is_array($nomcourt)) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(MotgroupeTableMap::COL_NOMCOURT, $nomcourt, $comparison);
    }

    /**
     * Filter the query on the descriptif column
     *
     * Example usage:
     * <code>
     * $query->filterByDescriptif('fooValue');   // WHERE descriptif = 'fooValue'
     * $query->filterByDescriptif('%fooValue%', Criteria::LIKE); // WHERE descriptif LIKE '%fooValue%'
     * </code>
     *
     * @param     string $descriptif The value to use as filter.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildMotgroupeQuery The current query, for fluid interface
     */
    public function filterByDescriptif($descriptif = null, $comparison = null)
    {
        if (null === $comparison) {
            if (is_array($descriptif)) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(MotgroupeTableMap::COL_DESCRIPTIF, $descriptif, $comparison);
    }

    /**
     * Filter the query on the texte column
     *
     * Example usage:
     * <code>
     * $query->filterByTexte('fooValue');   // WHERE texte = 'fooValue'
     * $query->filterByTexte('%fooValue%', Criteria::LIKE); // WHERE texte LIKE '%fooValue%'
     * </code>
     *
     * @param     string $texte The value to use as filter.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildMotgroupeQuery The current query, for fluid interface
     */
    public function filterByTexte($texte = null, $comparison = null)
    {
        if (null === $comparison) {
            if (is_array($texte)) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(MotgroupeTableMap::COL_TEXTE, $texte, $comparison);
    }

    /**
     * Filter the query on the importance column
     *
     * Example usage:
     * <code>
     * $query->filterByImportance(1234); // WHERE importance = 1234
     * $query->filterByImportance(array(12, 34)); // WHERE importance IN (12, 34)
     * $query->filterByImportance(array('min' => 12)); // WHERE importance > 12
     * </code>
     *
     * @param     mixed $importance The value to use as filter.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildMotgroupeQuery The current query, for fluid interface
     */
    public function filterByImportance($importance = null, $comparison = null)
    {
        if (is_array($importance)) {
            $useMinMax = false;
            if (isset($importance['min'])) {
                $this->addUsingAlias(MotgroupeTableMap::COL_IMPORTANCE, $importance['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($importance['max'])) {
                $this->addUsingAlias(MotgroupeTableMap::COL_IMPORTANCE, $importance['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(MotgroupeTableMap::COL_IMPORTANCE, $importance, $comparison);
    }

    /**
     * Filter the query on the objets_en_lien column
     *
     * Example usage:
     * <code>
     * $query->filterByObjetsEnLien('fooValue');   // WHERE objets_en_lien = 'fooValue'
     * $query->filterByObjetsEnLien('%fooValue%', Criteria::LIKE); // WHERE objets_en_lien LIKE '%fooValue%'
     * </code>
     *
     * @param     string $objetsEnLien The value to use as filter.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildMotgroupeQuery The current query, for fluid interface
     */
    public function filterByObjetsEnLien($objetsEnLien = null, $comparison = null)
    {
        if (null === $comparison) {
            if (is_array($objetsEnLien)) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(MotgroupeTableMap::COL_OBJETS_EN_LIEN, $objetsEnLien, $comparison);
    }

    /**
     * Filter the query on the systeme column
     *
     * Example usage:
     * <code>
     * $query->filterBySysteme(true); // WHERE systeme = true
     * $query->filterBySysteme('yes'); // WHERE systeme = true
     * </code>
     *
     * @param     boolean|string $systeme The value to use as filter.
     *              Non-boolean arguments are converted using the following rules:
     *                * 1, '1', 'true',  'on',  and 'yes' are converted to boolean true
     *                * 0, '0', 'false', 'off', and 'no'  are converted to boolean false
     *              Check on string values is case insensitive (so 'FaLsE' is seen as 'false').
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildMotgroupeQuery The current query, for fluid interface
     */
    public function filterBySysteme($systeme = null, $comparison = null)
    {
        if (is_string($systeme)) {
            $systeme = in_array(strtolower($systeme), array('false', 'off', '-', 'no', 'n', '0', '')) ? false : true;
        }

        return $this->addUsingAlias(MotgroupeTableMap::COL_SYSTEME, $systeme, $comparison);
    }

    /**
     * Filter the query on the actif column
     *
     * Example usage:
     * <code>
     * $query->filterByActif(1234); // WHERE actif = 1234
     * $query->filterByActif(array(12, 34)); // WHERE actif IN (12, 34)
     * $query->filterByActif(array('min' => 12)); // WHERE actif > 12
     * </code>
     *
     * @param     mixed $actif The value to use as filter.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildMotgroupeQuery The current query, for fluid interface
     */
    public function filterByActif($actif = null, $comparison = null)
    {
        if (is_array($actif)) {
            $useMinMax = false;
            if (isset($actif['min'])) {
                $this->addUsingAlias(MotgroupeTableMap::COL_ACTIF, $actif['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($actif['max'])) {
                $this->addUsingAlias(MotgroupeTableMap::COL_ACTIF, $actif['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(MotgroupeTableMap::COL_ACTIF, $actif, $comparison);
    }

    /**
     * Filter the query on the options column
     *
     * Example usage:
     * <code>
     * $query->filterByOptions('fooValue');   // WHERE options = 'fooValue'
     * $query->filterByOptions('%fooValue%', Criteria::LIKE); // WHERE options LIKE '%fooValue%'
     * </code>
     *
     * @param     string $options The value to use as filter.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildMotgroupeQuery The current query, for fluid interface
     */
    public function filterByOptions($options = null, $comparison = null)
    {
        if (null === $comparison) {
            if (is_array($options)) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(MotgroupeTableMap::COL_OPTIONS, $options, $comparison);
    }

    /**
     * Filter the query on the created_at column
     *
     * Example usage:
     * <code>
     * $query->filterByCreatedAt('2011-03-14'); // WHERE created_at = '2011-03-14'
     * $query->filterByCreatedAt('now'); // WHERE created_at = '2011-03-14'
     * $query->filterByCreatedAt(array('max' => 'yesterday')); // WHERE created_at > '2011-03-13'
     * </code>
     *
     * @param     mixed $createdAt The value to use as filter.
     *              Values can be integers (unix timestamps), DateTime objects, or strings.
     *              Empty strings are treated as NULL.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildMotgroupeQuery The current query, for fluid interface
     */
    public function filterByCreatedAt($createdAt = null, $comparison = null)
    {
        if (is_array($createdAt)) {
            $useMinMax = false;
            if (isset($createdAt['min'])) {
                $this->addUsingAlias(MotgroupeTableMap::COL_CREATED_AT, $createdAt['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($createdAt['max'])) {
                $this->addUsingAlias(MotgroupeTableMap::COL_CREATED_AT, $createdAt['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(MotgroupeTableMap::COL_CREATED_AT, $createdAt, $comparison);
    }

    /**
     * Filter the query on the updated_at column
     *
     * Example usage:
     * <code>
     * $query->filterByUpdatedAt('2011-03-14'); // WHERE updated_at = '2011-03-14'
     * $query->filterByUpdatedAt('now'); // WHERE updated_at = '2011-03-14'
     * $query->filterByUpdatedAt(array('max' => 'yesterday')); // WHERE updated_at > '2011-03-13'
     * </code>
     *
     * @param     mixed $updatedAt The value to use as filter.
     *              Values can be integers (unix timestamps), DateTime objects, or strings.
     *              Empty strings are treated as NULL.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildMotgroupeQuery The current query, for fluid interface
     */
    public function filterByUpdatedAt($updatedAt = null, $comparison = null)
    {
        if (is_array($updatedAt)) {
            $useMinMax = false;
            if (isset($updatedAt['min'])) {
                $this->addUsingAlias(MotgroupeTableMap::COL_UPDATED_AT, $updatedAt['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($updatedAt['max'])) {
                $this->addUsingAlias(MotgroupeTableMap::COL_UPDATED_AT, $updatedAt['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(MotgroupeTableMap::COL_UPDATED_AT, $updatedAt, $comparison);
    }

    /**
     * Filter the query by a related \Mot object
     *
     * @param \Mot|ObjectCollection $mot the related object to use as filter
     * @param string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return ChildMotgroupeQuery The current query, for fluid interface
     */
    public function filterByMot($mot, $comparison = null)
    {
        if ($mot instanceof \Mot) {
            return $this
                ->addUsingAlias(MotgroupeTableMap::COL_ID_MOTGROUPE, $mot->getIdMotgroupe(), $comparison);
        } elseif ($mot instanceof ObjectCollection) {
            return $this
                ->useMotQuery()
                ->filterByPrimaryKeys($mot->getPrimaryKeys())
                ->endUse();
        } else {
            throw new PropelException('filterByMot() only accepts arguments of type \Mot or Collection');
        }
    }

    /**
     * Adds a JOIN clause to the query using the Mot relation
     *
     * @param     string $relationAlias optional alias for the relation
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return $this|ChildMotgroupeQuery The current query, for fluid interface
     */
    public function joinMot($relationAlias = null, $joinType = Criteria::INNER_JOIN)
    {
        $tableMap = $this->getTableMap();
        $relationMap = $tableMap->getRelation('Mot');

        // create a ModelJoin object for this join
        $join = new ModelJoin();
        $join->setJoinType($joinType);
        $join->setRelationMap($relationMap, $this->useAliasInSQL ? $this->getModelAlias() : null, $relationAlias);
        if ($previousJoin = $this->getPreviousJoin()) {
            $join->setPreviousJoin($previousJoin);
        }

        // add the ModelJoin to the current object
        if ($relationAlias) {
            $this->addAlias($relationAlias, $relationMap->getRightTable()->getName());
            $this->addJoinObject($join, $relationAlias);
        } else {
            $this->addJoinObject($join, 'Mot');
        }

        return $this;
    }

    /**
     * Use the Mot relation Mot object
     *
     * @see useQuery()
     *
     * @param     string $relationAlias optional alias for the relation,
     *                                   to be used as main alias in the secondary query
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return \MotQuery A secondary query class using the current class as primary query
     */
    public function useMotQuery($relationAlias = null, $joinType = Criteria::INNER_JOIN)
    {
        return $this
            ->joinMot($relationAlias, $joinType)
            ->useQuery($relationAlias ? $relationAlias : 'Mot', '\MotQuery');
    }

    /**
     * Exclude object from result
     *
     * @param   ChildMotgroupe $motgroupe Object to remove from the list of results
     *
     * @return $this|ChildMotgroupeQuery The current query, for fluid interface
     */
    public function prune($motgroupe = null)
    {
        if ($motgroupe) {
            $this->addUsingAlias(MotgroupeTableMap::COL_ID_MOTGROUPE, $motgroupe->getIdMotgroupe(), Criteria::NOT_EQUAL);
        }

        return $this;
    }

    /**
     * Code to execute before every DELETE statement
     *
     * @param     ConnectionInterface $con The connection object used by the query
     */
    protected function basePreDelete(ConnectionInterface $con)
    {
        // archivable behavior

        if ($this->archiveOnDelete) {
            $this->archive($con);
        } else {
            $this->archiveOnDelete = true;
        }


        return $this->preDelete($con);
    }

    /**
     * Deletes all rows from the asso_motgroupes table.
     *
     * @param ConnectionInterface $con the connection to use
     * @return int The number of affected rows (if supported by underlying database driver).
     */
    public function doDeleteAll(ConnectionInterface $con = null)
    {
        if (null === $con) {
            $con = Propel::getServiceContainer()->getWriteConnection(MotgroupeTableMap::DATABASE_NAME);
        }

        // use transaction because $criteria could contain info
        // for more than one table or we could emulating ON DELETE CASCADE, etc.
        return $con->transaction(function () use ($con) {
            $affectedRows = 0; // initialize var to track total num of affected rows
            $affectedRows += parent::doDeleteAll($con);
            // Because this db requires some delete cascade/set null emulation, we have to
            // clear the cached instance *after* the emulation has happened (since
            // instances get re-added by the select statement contained therein).
            MotgroupeTableMap::clearInstancePool();
            MotgroupeTableMap::clearRelatedInstancePool();

            return $affectedRows;
        });
    }

    /**
     * Performs a DELETE on the database based on the current ModelCriteria
     *
     * @param ConnectionInterface $con the connection to use
     * @return int             The number of affected rows (if supported by underlying database driver).  This includes CASCADE-related rows
     *                         if supported by native driver or if emulated using Propel.
     * @throws PropelException Any exceptions caught during processing will be
     *                         rethrown wrapped into a PropelException.
     */
    public function delete(ConnectionInterface $con = null)
    {
        if (null === $con) {
            $con = Propel::getServiceContainer()->getWriteConnection(MotgroupeTableMap::DATABASE_NAME);
        }

        $criteria = $this;

        // Set the correct dbName
        $criteria->setDbName(MotgroupeTableMap::DATABASE_NAME);

        // use transaction because $criteria could contain info
        // for more than one table or we could emulating ON DELETE CASCADE, etc.
        return $con->transaction(function () use ($con, $criteria) {
            $affectedRows = 0; // initialize var to track total num of affected rows

            MotgroupeTableMap::removeInstanceFromPool($criteria);

            $affectedRows += ModelCriteria::delete($con);
            MotgroupeTableMap::clearRelatedInstancePool();

            return $affectedRows;
        });
    }

    // timestampable behavior

    /**
     * Filter by the latest updated
     *
     * @param      int $nbDays Maximum age of the latest update in days
     *
     * @return     $this|ChildMotgroupeQuery The current query, for fluid interface
     */
    public function recentlyUpdated($nbDays = 7)
    {
        return $this->addUsingAlias(MotgroupeTableMap::COL_UPDATED_AT, time() - $nbDays * 24 * 60 * 60, Criteria::GREATER_EQUAL);
    }

    /**
     * Order by update date desc
     *
     * @return     $this|ChildMotgroupeQuery The current query, for fluid interface
     */
    public function lastUpdatedFirst()
    {
        return $this->addDescendingOrderByColumn(MotgroupeTableMap::COL_UPDATED_AT);
    }

    /**
     * Order by update date asc
     *
     * @return     $this|ChildMotgroupeQuery The current query, for fluid interface
     */
    public function firstUpdatedFirst()
    {
        return $this->addAscendingOrderByColumn(MotgroupeTableMap::COL_UPDATED_AT);
    }

    /**
     * Order by create date desc
     *
     * @return     $this|ChildMotgroupeQuery The current query, for fluid interface
     */
    public function lastCreatedFirst()
    {
        return $this->addDescendingOrderByColumn(MotgroupeTableMap::COL_CREATED_AT);
    }

    /**
     * Filter by the latest created
     *
     * @param      int $nbDays Maximum age of in days
     *
     * @return     $this|ChildMotgroupeQuery The current query, for fluid interface
     */
    public function recentlyCreated($nbDays = 7)
    {
        return $this->addUsingAlias(MotgroupeTableMap::COL_CREATED_AT, time() - $nbDays * 24 * 60 * 60, Criteria::GREATER_EQUAL);
    }

    /**
     * Order by create date asc
     *
     * @return     $this|ChildMotgroupeQuery The current query, for fluid interface
     */
    public function firstCreatedFirst()
    {
        return $this->addAscendingOrderByColumn(MotgroupeTableMap::COL_CREATED_AT);
    }

    // archivable behavior

    /**
     * Copy the data of the objects satisfying the query into ChildAssoMotgroupesArchive archive objects.
     * The archived objects are then saved.
     * If any of the objects has already been archived, the archived object
     * is updated and not duplicated.
     * Warning: This termination methods issues 2n+1 queries.
     *
     * @param      ConnectionInterface $con    Connection to use.
     * @param      Boolean $useLittleMemory    Whether or not to use OnDemandFormatter to retrieve objects.
     *               Set to false if the identity map matters.
     *               Set to true (default) to use less memory.
     *
     * @return     int the number of archived objects
     */
    public function archive($con = null, $useLittleMemory = true)
    {
        $criteria = clone $this;
        // prepare the query
        $criteria->setWith(array());
        if ($useLittleMemory) {
            $criteria->setFormatter(ModelCriteria::FORMAT_ON_DEMAND);
        }
        if ($con === null) {
            $con = Propel::getServiceContainer()->getWriteConnection(MotgroupeTableMap::DATABASE_NAME);
        }

        return $con->transaction(function () use ($con, $criteria) {
            $totalArchivedObjects = 0;

            // archive all results one by one
            foreach ($criteria->find($con) as $object) {
                $object->archive($con);
                $totalArchivedObjects++;
            }

            return $totalArchivedObjects;
        });
    }

    /**
     * Enable/disable auto-archiving on delete for the next query.
     *
     * @param boolean True if the query must archive deleted objects, false otherwise.
     */
    public function setArchiveOnDelete($archiveOnDelete)
    {
        $this->archiveOnDelete = $archiveOnDelete;
    }

    /**
     * Delete records matching the current query without archiving them.
     *
     * @param      ConnectionInterface $con    Connection to use.
     *
     * @return integer the number of deleted rows
     */
    public function deleteWithoutArchive($con = null)
    {
        $this->archiveOnDelete = false;

        return $this->delete($con);
    }

    /**
     * Delete all records without archiving them.
     *
     * @param      ConnectionInterface $con    Connection to use.
     *
     * @return integer the number of deleted rows
     */
    public function deleteAllWithoutArchive($con = null)
    {
        $this->archiveOnDelete = false;

        return $this->deleteAll($con);
    }

} // MotgroupeQuery
