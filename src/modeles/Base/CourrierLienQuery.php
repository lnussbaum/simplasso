<?php

namespace Base;

use \ComCourriersLiensArchive as ChildComCourriersLiensArchive;
use \CourrierLien as ChildCourrierLien;
use \CourrierLienQuery as ChildCourrierLienQuery;
use \Exception;
use \PDO;
use Map\CourrierLienTableMap;
use Propel\Runtime\Propel;
use Propel\Runtime\ActiveQuery\Criteria;
use Propel\Runtime\ActiveQuery\ModelCriteria;
use Propel\Runtime\ActiveQuery\ModelJoin;
use Propel\Runtime\Collection\ObjectCollection;
use Propel\Runtime\Connection\ConnectionInterface;
use Propel\Runtime\Exception\PropelException;

/**
 * Base class that represents a query for the 'com_courriers_liens' table.
 *
 *
 *
 * @method     ChildCourrierLienQuery orderByIdCourrierLien($order = Criteria::ASC) Order by the id_courrier_lien column
 * @method     ChildCourrierLienQuery orderByIdMembre($order = Criteria::ASC) Order by the id_membre column
 * @method     ChildCourrierLienQuery orderByIdIndividu($order = Criteria::ASC) Order by the id_individu column
 * @method     ChildCourrierLienQuery orderByIdCourrier($order = Criteria::ASC) Order by the id_courrier column
 * @method     ChildCourrierLienQuery orderByCanal($order = Criteria::ASC) Order by the canal column
 * @method     ChildCourrierLienQuery orderByDateEnvoi($order = Criteria::ASC) Order by the date_envoi column
 * @method     ChildCourrierLienQuery orderByMessageParticulier($order = Criteria::ASC) Order by the message_particulier column
 * @method     ChildCourrierLienQuery orderByStatut($order = Criteria::ASC) Order by the statut column
 * @method     ChildCourrierLienQuery orderByVariables($order = Criteria::ASC) Order by the variables column
 * @method     ChildCourrierLienQuery orderByCreatedAt($order = Criteria::ASC) Order by the created_at column
 * @method     ChildCourrierLienQuery orderByUpdatedAt($order = Criteria::ASC) Order by the updated_at column
 *
 * @method     ChildCourrierLienQuery groupByIdCourrierLien() Group by the id_courrier_lien column
 * @method     ChildCourrierLienQuery groupByIdMembre() Group by the id_membre column
 * @method     ChildCourrierLienQuery groupByIdIndividu() Group by the id_individu column
 * @method     ChildCourrierLienQuery groupByIdCourrier() Group by the id_courrier column
 * @method     ChildCourrierLienQuery groupByCanal() Group by the canal column
 * @method     ChildCourrierLienQuery groupByDateEnvoi() Group by the date_envoi column
 * @method     ChildCourrierLienQuery groupByMessageParticulier() Group by the message_particulier column
 * @method     ChildCourrierLienQuery groupByStatut() Group by the statut column
 * @method     ChildCourrierLienQuery groupByVariables() Group by the variables column
 * @method     ChildCourrierLienQuery groupByCreatedAt() Group by the created_at column
 * @method     ChildCourrierLienQuery groupByUpdatedAt() Group by the updated_at column
 *
 * @method     ChildCourrierLienQuery leftJoin($relation) Adds a LEFT JOIN clause to the query
 * @method     ChildCourrierLienQuery rightJoin($relation) Adds a RIGHT JOIN clause to the query
 * @method     ChildCourrierLienQuery innerJoin($relation) Adds a INNER JOIN clause to the query
 *
 * @method     ChildCourrierLienQuery leftJoinWith($relation) Adds a LEFT JOIN clause and with to the query
 * @method     ChildCourrierLienQuery rightJoinWith($relation) Adds a RIGHT JOIN clause and with to the query
 * @method     ChildCourrierLienQuery innerJoinWith($relation) Adds a INNER JOIN clause and with to the query
 *
 * @method     ChildCourrierLienQuery leftJoinIndividu($relationAlias = null) Adds a LEFT JOIN clause to the query using the Individu relation
 * @method     ChildCourrierLienQuery rightJoinIndividu($relationAlias = null) Adds a RIGHT JOIN clause to the query using the Individu relation
 * @method     ChildCourrierLienQuery innerJoinIndividu($relationAlias = null) Adds a INNER JOIN clause to the query using the Individu relation
 *
 * @method     ChildCourrierLienQuery joinWithIndividu($joinType = Criteria::INNER_JOIN) Adds a join clause and with to the query using the Individu relation
 *
 * @method     ChildCourrierLienQuery leftJoinWithIndividu() Adds a LEFT JOIN clause and with to the query using the Individu relation
 * @method     ChildCourrierLienQuery rightJoinWithIndividu() Adds a RIGHT JOIN clause and with to the query using the Individu relation
 * @method     ChildCourrierLienQuery innerJoinWithIndividu() Adds a INNER JOIN clause and with to the query using the Individu relation
 *
 * @method     ChildCourrierLienQuery leftJoinMembre($relationAlias = null) Adds a LEFT JOIN clause to the query using the Membre relation
 * @method     ChildCourrierLienQuery rightJoinMembre($relationAlias = null) Adds a RIGHT JOIN clause to the query using the Membre relation
 * @method     ChildCourrierLienQuery innerJoinMembre($relationAlias = null) Adds a INNER JOIN clause to the query using the Membre relation
 *
 * @method     ChildCourrierLienQuery joinWithMembre($joinType = Criteria::INNER_JOIN) Adds a join clause and with to the query using the Membre relation
 *
 * @method     ChildCourrierLienQuery leftJoinWithMembre() Adds a LEFT JOIN clause and with to the query using the Membre relation
 * @method     ChildCourrierLienQuery rightJoinWithMembre() Adds a RIGHT JOIN clause and with to the query using the Membre relation
 * @method     ChildCourrierLienQuery innerJoinWithMembre() Adds a INNER JOIN clause and with to the query using the Membre relation
 *
 * @method     ChildCourrierLienQuery leftJoinCourrier($relationAlias = null) Adds a LEFT JOIN clause to the query using the Courrier relation
 * @method     ChildCourrierLienQuery rightJoinCourrier($relationAlias = null) Adds a RIGHT JOIN clause to the query using the Courrier relation
 * @method     ChildCourrierLienQuery innerJoinCourrier($relationAlias = null) Adds a INNER JOIN clause to the query using the Courrier relation
 *
 * @method     ChildCourrierLienQuery joinWithCourrier($joinType = Criteria::INNER_JOIN) Adds a join clause and with to the query using the Courrier relation
 *
 * @method     ChildCourrierLienQuery leftJoinWithCourrier() Adds a LEFT JOIN clause and with to the query using the Courrier relation
 * @method     ChildCourrierLienQuery rightJoinWithCourrier() Adds a RIGHT JOIN clause and with to the query using the Courrier relation
 * @method     ChildCourrierLienQuery innerJoinWithCourrier() Adds a INNER JOIN clause and with to the query using the Courrier relation
 *
 * @method     \IndividuQuery|\MembreQuery|\CourrierQuery endUse() Finalizes a secondary criteria and merges it with its primary Criteria
 *
 * @method     ChildCourrierLien findOne(ConnectionInterface $con = null) Return the first ChildCourrierLien matching the query
 * @method     ChildCourrierLien findOneOrCreate(ConnectionInterface $con = null) Return the first ChildCourrierLien matching the query, or a new ChildCourrierLien object populated from the query conditions when no match is found
 *
 * @method     ChildCourrierLien findOneByIdCourrierLien(string $id_courrier_lien) Return the first ChildCourrierLien filtered by the id_courrier_lien column
 * @method     ChildCourrierLien findOneByIdMembre(string $id_membre) Return the first ChildCourrierLien filtered by the id_membre column
 * @method     ChildCourrierLien findOneByIdIndividu(string $id_individu) Return the first ChildCourrierLien filtered by the id_individu column
 * @method     ChildCourrierLien findOneByIdCourrier(string $id_courrier) Return the first ChildCourrierLien filtered by the id_courrier column
 * @method     ChildCourrierLien findOneByCanal(string $canal) Return the first ChildCourrierLien filtered by the canal column
 * @method     ChildCourrierLien findOneByDateEnvoi(string $date_envoi) Return the first ChildCourrierLien filtered by the date_envoi column
 * @method     ChildCourrierLien findOneByMessageParticulier(string $message_particulier) Return the first ChildCourrierLien filtered by the message_particulier column
 * @method     ChildCourrierLien findOneByStatut(string $statut) Return the first ChildCourrierLien filtered by the statut column
 * @method     ChildCourrierLien findOneByVariables(string $variables) Return the first ChildCourrierLien filtered by the variables column
 * @method     ChildCourrierLien findOneByCreatedAt(string $created_at) Return the first ChildCourrierLien filtered by the created_at column
 * @method     ChildCourrierLien findOneByUpdatedAt(string $updated_at) Return the first ChildCourrierLien filtered by the updated_at column *

 * @method     ChildCourrierLien requirePk($key, ConnectionInterface $con = null) Return the ChildCourrierLien by primary key and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildCourrierLien requireOne(ConnectionInterface $con = null) Return the first ChildCourrierLien matching the query and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 *
 * @method     ChildCourrierLien requireOneByIdCourrierLien(string $id_courrier_lien) Return the first ChildCourrierLien filtered by the id_courrier_lien column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildCourrierLien requireOneByIdMembre(string $id_membre) Return the first ChildCourrierLien filtered by the id_membre column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildCourrierLien requireOneByIdIndividu(string $id_individu) Return the first ChildCourrierLien filtered by the id_individu column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildCourrierLien requireOneByIdCourrier(string $id_courrier) Return the first ChildCourrierLien filtered by the id_courrier column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildCourrierLien requireOneByCanal(string $canal) Return the first ChildCourrierLien filtered by the canal column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildCourrierLien requireOneByDateEnvoi(string $date_envoi) Return the first ChildCourrierLien filtered by the date_envoi column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildCourrierLien requireOneByMessageParticulier(string $message_particulier) Return the first ChildCourrierLien filtered by the message_particulier column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildCourrierLien requireOneByStatut(string $statut) Return the first ChildCourrierLien filtered by the statut column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildCourrierLien requireOneByVariables(string $variables) Return the first ChildCourrierLien filtered by the variables column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildCourrierLien requireOneByCreatedAt(string $created_at) Return the first ChildCourrierLien filtered by the created_at column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildCourrierLien requireOneByUpdatedAt(string $updated_at) Return the first ChildCourrierLien filtered by the updated_at column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 *
 * @method     ChildCourrierLien[]|ObjectCollection find(ConnectionInterface $con = null) Return ChildCourrierLien objects based on current ModelCriteria
 * @method     ChildCourrierLien[]|ObjectCollection findByIdCourrierLien(string $id_courrier_lien) Return ChildCourrierLien objects filtered by the id_courrier_lien column
 * @method     ChildCourrierLien[]|ObjectCollection findByIdMembre(string $id_membre) Return ChildCourrierLien objects filtered by the id_membre column
 * @method     ChildCourrierLien[]|ObjectCollection findByIdIndividu(string $id_individu) Return ChildCourrierLien objects filtered by the id_individu column
 * @method     ChildCourrierLien[]|ObjectCollection findByIdCourrier(string $id_courrier) Return ChildCourrierLien objects filtered by the id_courrier column
 * @method     ChildCourrierLien[]|ObjectCollection findByCanal(string $canal) Return ChildCourrierLien objects filtered by the canal column
 * @method     ChildCourrierLien[]|ObjectCollection findByDateEnvoi(string $date_envoi) Return ChildCourrierLien objects filtered by the date_envoi column
 * @method     ChildCourrierLien[]|ObjectCollection findByMessageParticulier(string $message_particulier) Return ChildCourrierLien objects filtered by the message_particulier column
 * @method     ChildCourrierLien[]|ObjectCollection findByStatut(string $statut) Return ChildCourrierLien objects filtered by the statut column
 * @method     ChildCourrierLien[]|ObjectCollection findByVariables(string $variables) Return ChildCourrierLien objects filtered by the variables column
 * @method     ChildCourrierLien[]|ObjectCollection findByCreatedAt(string $created_at) Return ChildCourrierLien objects filtered by the created_at column
 * @method     ChildCourrierLien[]|ObjectCollection findByUpdatedAt(string $updated_at) Return ChildCourrierLien objects filtered by the updated_at column
 * @method     ChildCourrierLien[]|\Propel\Runtime\Util\PropelModelPager paginate($page = 1, $maxPerPage = 10, ConnectionInterface $con = null) Issue a SELECT query based on the current ModelCriteria and uses a page and a maximum number of results per page to compute an offset and a limit
 *
 */
abstract class CourrierLienQuery extends ModelCriteria
{

    // archivable behavior
    protected $archiveOnDelete = true;
protected $entityNotFoundExceptionClass = '\\Propel\\Runtime\\Exception\\EntityNotFoundException';

    /**
     * Initializes internal state of \Base\CourrierLienQuery object.
     *
     * @param     string $dbName The database name
     * @param     string $modelName The phpName of a model, e.g. 'Book'
     * @param     string $modelAlias The alias for the model in this query, e.g. 'b'
     */
    public function __construct($dbName = 'simplasso', $modelName = '\\CourrierLien', $modelAlias = null)
    {
        parent::__construct($dbName, $modelName, $modelAlias);
    }

    /**
     * Returns a new ChildCourrierLienQuery object.
     *
     * @param     string $modelAlias The alias of a model in the query
     * @param     Criteria $criteria Optional Criteria to build the query from
     *
     * @return ChildCourrierLienQuery
     */
    public static function create($modelAlias = null, Criteria $criteria = null)
    {
        if ($criteria instanceof ChildCourrierLienQuery) {
            return $criteria;
        }
        $query = new ChildCourrierLienQuery();
        if (null !== $modelAlias) {
            $query->setModelAlias($modelAlias);
        }
        if ($criteria instanceof Criteria) {
            $query->mergeWith($criteria);
        }

        return $query;
    }

    /**
     * Find object by primary key.
     * Propel uses the instance pool to skip the database if the object exists.
     * Go fast if the query is untouched.
     *
     * <code>
     * $obj  = $c->findPk(12, $con);
     * </code>
     *
     * @param mixed $key Primary key to use for the query
     * @param ConnectionInterface $con an optional connection object
     *
     * @return ChildCourrierLien|array|mixed the result, formatted by the current formatter
     */
    public function findPk($key, ConnectionInterface $con = null)
    {
        if ($key === null) {
            return null;
        }

        if ($con === null) {
            $con = Propel::getServiceContainer()->getReadConnection(CourrierLienTableMap::DATABASE_NAME);
        }

        $this->basePreSelect($con);

        if (
            $this->formatter || $this->modelAlias || $this->with || $this->select
            || $this->selectColumns || $this->asColumns || $this->selectModifiers
            || $this->map || $this->having || $this->joins
        ) {
            return $this->findPkComplex($key, $con);
        }

        if ((null !== ($obj = CourrierLienTableMap::getInstanceFromPool(null === $key || is_scalar($key) || is_callable([$key, '__toString']) ? (string) $key : $key)))) {
            // the object is already in the instance pool
            return $obj;
        }

        return $this->findPkSimple($key, $con);
    }

    /**
     * Find object by primary key using raw SQL to go fast.
     * Bypass doSelect() and the object formatter by using generated code.
     *
     * @param     mixed $key Primary key to use for the query
     * @param     ConnectionInterface $con A connection object
     *
     * @throws \Propel\Runtime\Exception\PropelException
     *
     * @return ChildCourrierLien A model object, or null if the key is not found
     */
    protected function findPkSimple($key, ConnectionInterface $con)
    {
        $sql = 'SELECT `id_courrier_lien`, `id_membre`, `id_individu`, `id_courrier`, `canal`, `date_envoi`, `message_particulier`, `statut`, `variables`, `created_at`, `updated_at` FROM `com_courriers_liens` WHERE `id_courrier_lien` = :p0';
        try {
            $stmt = $con->prepare($sql);
            $stmt->bindValue(':p0', $key, PDO::PARAM_INT);
            $stmt->execute();
        } catch (Exception $e) {
            Propel::log($e->getMessage(), Propel::LOG_ERR);
            throw new PropelException(sprintf('Unable to execute SELECT statement [%s]', $sql), 0, $e);
        }
        $obj = null;
        if ($row = $stmt->fetch(\PDO::FETCH_NUM)) {
            /** @var ChildCourrierLien $obj */
            $obj = new ChildCourrierLien();
            $obj->hydrate($row);
            CourrierLienTableMap::addInstanceToPool($obj, null === $key || is_scalar($key) || is_callable([$key, '__toString']) ? (string) $key : $key);
        }
        $stmt->closeCursor();

        return $obj;
    }

    /**
     * Find object by primary key.
     *
     * @param     mixed $key Primary key to use for the query
     * @param     ConnectionInterface $con A connection object
     *
     * @return ChildCourrierLien|array|mixed the result, formatted by the current formatter
     */
    protected function findPkComplex($key, ConnectionInterface $con)
    {
        // As the query uses a PK condition, no limit(1) is necessary.
        $criteria = $this->isKeepQuery() ? clone $this : $this;
        $dataFetcher = $criteria
            ->filterByPrimaryKey($key)
            ->doSelect($con);

        return $criteria->getFormatter()->init($criteria)->formatOne($dataFetcher);
    }

    /**
     * Find objects by primary key
     * <code>
     * $objs = $c->findPks(array(12, 56, 832), $con);
     * </code>
     * @param     array $keys Primary keys to use for the query
     * @param     ConnectionInterface $con an optional connection object
     *
     * @return ObjectCollection|array|mixed the list of results, formatted by the current formatter
     */
    public function findPks($keys, ConnectionInterface $con = null)
    {
        if (null === $con) {
            $con = Propel::getServiceContainer()->getReadConnection($this->getDbName());
        }
        $this->basePreSelect($con);
        $criteria = $this->isKeepQuery() ? clone $this : $this;
        $dataFetcher = $criteria
            ->filterByPrimaryKeys($keys)
            ->doSelect($con);

        return $criteria->getFormatter()->init($criteria)->format($dataFetcher);
    }

    /**
     * Filter the query by primary key
     *
     * @param     mixed $key Primary key to use for the query
     *
     * @return $this|ChildCourrierLienQuery The current query, for fluid interface
     */
    public function filterByPrimaryKey($key)
    {

        return $this->addUsingAlias(CourrierLienTableMap::COL_ID_COURRIER_LIEN, $key, Criteria::EQUAL);
    }

    /**
     * Filter the query by a list of primary keys
     *
     * @param     array $keys The list of primary key to use for the query
     *
     * @return $this|ChildCourrierLienQuery The current query, for fluid interface
     */
    public function filterByPrimaryKeys($keys)
    {

        return $this->addUsingAlias(CourrierLienTableMap::COL_ID_COURRIER_LIEN, $keys, Criteria::IN);
    }

    /**
     * Filter the query on the id_courrier_lien column
     *
     * Example usage:
     * <code>
     * $query->filterByIdCourrierLien(1234); // WHERE id_courrier_lien = 1234
     * $query->filterByIdCourrierLien(array(12, 34)); // WHERE id_courrier_lien IN (12, 34)
     * $query->filterByIdCourrierLien(array('min' => 12)); // WHERE id_courrier_lien > 12
     * </code>
     *
     * @param     mixed $idCourrierLien The value to use as filter.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildCourrierLienQuery The current query, for fluid interface
     */
    public function filterByIdCourrierLien($idCourrierLien = null, $comparison = null)
    {
        if (is_array($idCourrierLien)) {
            $useMinMax = false;
            if (isset($idCourrierLien['min'])) {
                $this->addUsingAlias(CourrierLienTableMap::COL_ID_COURRIER_LIEN, $idCourrierLien['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($idCourrierLien['max'])) {
                $this->addUsingAlias(CourrierLienTableMap::COL_ID_COURRIER_LIEN, $idCourrierLien['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(CourrierLienTableMap::COL_ID_COURRIER_LIEN, $idCourrierLien, $comparison);
    }

    /**
     * Filter the query on the id_membre column
     *
     * Example usage:
     * <code>
     * $query->filterByIdMembre(1234); // WHERE id_membre = 1234
     * $query->filterByIdMembre(array(12, 34)); // WHERE id_membre IN (12, 34)
     * $query->filterByIdMembre(array('min' => 12)); // WHERE id_membre > 12
     * </code>
     *
     * @see       filterByMembre()
     *
     * @param     mixed $idMembre The value to use as filter.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildCourrierLienQuery The current query, for fluid interface
     */
    public function filterByIdMembre($idMembre = null, $comparison = null)
    {
        if (is_array($idMembre)) {
            $useMinMax = false;
            if (isset($idMembre['min'])) {
                $this->addUsingAlias(CourrierLienTableMap::COL_ID_MEMBRE, $idMembre['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($idMembre['max'])) {
                $this->addUsingAlias(CourrierLienTableMap::COL_ID_MEMBRE, $idMembre['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(CourrierLienTableMap::COL_ID_MEMBRE, $idMembre, $comparison);
    }

    /**
     * Filter the query on the id_individu column
     *
     * Example usage:
     * <code>
     * $query->filterByIdIndividu(1234); // WHERE id_individu = 1234
     * $query->filterByIdIndividu(array(12, 34)); // WHERE id_individu IN (12, 34)
     * $query->filterByIdIndividu(array('min' => 12)); // WHERE id_individu > 12
     * </code>
     *
     * @see       filterByIndividu()
     *
     * @param     mixed $idIndividu The value to use as filter.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildCourrierLienQuery The current query, for fluid interface
     */
    public function filterByIdIndividu($idIndividu = null, $comparison = null)
    {
        if (is_array($idIndividu)) {
            $useMinMax = false;
            if (isset($idIndividu['min'])) {
                $this->addUsingAlias(CourrierLienTableMap::COL_ID_INDIVIDU, $idIndividu['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($idIndividu['max'])) {
                $this->addUsingAlias(CourrierLienTableMap::COL_ID_INDIVIDU, $idIndividu['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(CourrierLienTableMap::COL_ID_INDIVIDU, $idIndividu, $comparison);
    }

    /**
     * Filter the query on the id_courrier column
     *
     * Example usage:
     * <code>
     * $query->filterByIdCourrier(1234); // WHERE id_courrier = 1234
     * $query->filterByIdCourrier(array(12, 34)); // WHERE id_courrier IN (12, 34)
     * $query->filterByIdCourrier(array('min' => 12)); // WHERE id_courrier > 12
     * </code>
     *
     * @see       filterByCourrier()
     *
     * @param     mixed $idCourrier The value to use as filter.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildCourrierLienQuery The current query, for fluid interface
     */
    public function filterByIdCourrier($idCourrier = null, $comparison = null)
    {
        if (is_array($idCourrier)) {
            $useMinMax = false;
            if (isset($idCourrier['min'])) {
                $this->addUsingAlias(CourrierLienTableMap::COL_ID_COURRIER, $idCourrier['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($idCourrier['max'])) {
                $this->addUsingAlias(CourrierLienTableMap::COL_ID_COURRIER, $idCourrier['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(CourrierLienTableMap::COL_ID_COURRIER, $idCourrier, $comparison);
    }

    /**
     * Filter the query on the canal column
     *
     * Example usage:
     * <code>
     * $query->filterByCanal('fooValue');   // WHERE canal = 'fooValue'
     * $query->filterByCanal('%fooValue%', Criteria::LIKE); // WHERE canal LIKE '%fooValue%'
     * </code>
     *
     * @param     string $canal The value to use as filter.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildCourrierLienQuery The current query, for fluid interface
     */
    public function filterByCanal($canal = null, $comparison = null)
    {
        if (null === $comparison) {
            if (is_array($canal)) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(CourrierLienTableMap::COL_CANAL, $canal, $comparison);
    }

    /**
     * Filter the query on the date_envoi column
     *
     * Example usage:
     * <code>
     * $query->filterByDateEnvoi('2011-03-14'); // WHERE date_envoi = '2011-03-14'
     * $query->filterByDateEnvoi('now'); // WHERE date_envoi = '2011-03-14'
     * $query->filterByDateEnvoi(array('max' => 'yesterday')); // WHERE date_envoi > '2011-03-13'
     * </code>
     *
     * @param     mixed $dateEnvoi The value to use as filter.
     *              Values can be integers (unix timestamps), DateTime objects, or strings.
     *              Empty strings are treated as NULL.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildCourrierLienQuery The current query, for fluid interface
     */
    public function filterByDateEnvoi($dateEnvoi = null, $comparison = null)
    {
        if (is_array($dateEnvoi)) {
            $useMinMax = false;
            if (isset($dateEnvoi['min'])) {
                $this->addUsingAlias(CourrierLienTableMap::COL_DATE_ENVOI, $dateEnvoi['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($dateEnvoi['max'])) {
                $this->addUsingAlias(CourrierLienTableMap::COL_DATE_ENVOI, $dateEnvoi['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(CourrierLienTableMap::COL_DATE_ENVOI, $dateEnvoi, $comparison);
    }

    /**
     * Filter the query on the message_particulier column
     *
     * Example usage:
     * <code>
     * $query->filterByMessageParticulier('fooValue');   // WHERE message_particulier = 'fooValue'
     * $query->filterByMessageParticulier('%fooValue%', Criteria::LIKE); // WHERE message_particulier LIKE '%fooValue%'
     * </code>
     *
     * @param     string $messageParticulier The value to use as filter.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildCourrierLienQuery The current query, for fluid interface
     */
    public function filterByMessageParticulier($messageParticulier = null, $comparison = null)
    {
        if (null === $comparison) {
            if (is_array($messageParticulier)) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(CourrierLienTableMap::COL_MESSAGE_PARTICULIER, $messageParticulier, $comparison);
    }

    /**
     * Filter the query on the statut column
     *
     * Example usage:
     * <code>
     * $query->filterByStatut('fooValue');   // WHERE statut = 'fooValue'
     * $query->filterByStatut('%fooValue%', Criteria::LIKE); // WHERE statut LIKE '%fooValue%'
     * </code>
     *
     * @param     string $statut The value to use as filter.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildCourrierLienQuery The current query, for fluid interface
     */
    public function filterByStatut($statut = null, $comparison = null)
    {
        if (null === $comparison) {
            if (is_array($statut)) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(CourrierLienTableMap::COL_STATUT, $statut, $comparison);
    }

    /**
     * Filter the query on the variables column
     *
     * Example usage:
     * <code>
     * $query->filterByVariables('fooValue');   // WHERE variables = 'fooValue'
     * $query->filterByVariables('%fooValue%', Criteria::LIKE); // WHERE variables LIKE '%fooValue%'
     * </code>
     *
     * @param     string $variables The value to use as filter.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildCourrierLienQuery The current query, for fluid interface
     */
    public function filterByVariables($variables = null, $comparison = null)
    {
        if (null === $comparison) {
            if (is_array($variables)) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(CourrierLienTableMap::COL_VARIABLES, $variables, $comparison);
    }

    /**
     * Filter the query on the created_at column
     *
     * Example usage:
     * <code>
     * $query->filterByCreatedAt('2011-03-14'); // WHERE created_at = '2011-03-14'
     * $query->filterByCreatedAt('now'); // WHERE created_at = '2011-03-14'
     * $query->filterByCreatedAt(array('max' => 'yesterday')); // WHERE created_at > '2011-03-13'
     * </code>
     *
     * @param     mixed $createdAt The value to use as filter.
     *              Values can be integers (unix timestamps), DateTime objects, or strings.
     *              Empty strings are treated as NULL.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildCourrierLienQuery The current query, for fluid interface
     */
    public function filterByCreatedAt($createdAt = null, $comparison = null)
    {
        if (is_array($createdAt)) {
            $useMinMax = false;
            if (isset($createdAt['min'])) {
                $this->addUsingAlias(CourrierLienTableMap::COL_CREATED_AT, $createdAt['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($createdAt['max'])) {
                $this->addUsingAlias(CourrierLienTableMap::COL_CREATED_AT, $createdAt['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(CourrierLienTableMap::COL_CREATED_AT, $createdAt, $comparison);
    }

    /**
     * Filter the query on the updated_at column
     *
     * Example usage:
     * <code>
     * $query->filterByUpdatedAt('2011-03-14'); // WHERE updated_at = '2011-03-14'
     * $query->filterByUpdatedAt('now'); // WHERE updated_at = '2011-03-14'
     * $query->filterByUpdatedAt(array('max' => 'yesterday')); // WHERE updated_at > '2011-03-13'
     * </code>
     *
     * @param     mixed $updatedAt The value to use as filter.
     *              Values can be integers (unix timestamps), DateTime objects, or strings.
     *              Empty strings are treated as NULL.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildCourrierLienQuery The current query, for fluid interface
     */
    public function filterByUpdatedAt($updatedAt = null, $comparison = null)
    {
        if (is_array($updatedAt)) {
            $useMinMax = false;
            if (isset($updatedAt['min'])) {
                $this->addUsingAlias(CourrierLienTableMap::COL_UPDATED_AT, $updatedAt['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($updatedAt['max'])) {
                $this->addUsingAlias(CourrierLienTableMap::COL_UPDATED_AT, $updatedAt['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(CourrierLienTableMap::COL_UPDATED_AT, $updatedAt, $comparison);
    }

    /**
     * Filter the query by a related \Individu object
     *
     * @param \Individu|ObjectCollection $individu The related object(s) to use as filter
     * @param string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @throws \Propel\Runtime\Exception\PropelException
     *
     * @return ChildCourrierLienQuery The current query, for fluid interface
     */
    public function filterByIndividu($individu, $comparison = null)
    {
        if ($individu instanceof \Individu) {
            return $this
                ->addUsingAlias(CourrierLienTableMap::COL_ID_INDIVIDU, $individu->getIdIndividu(), $comparison);
        } elseif ($individu instanceof ObjectCollection) {
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }

            return $this
                ->addUsingAlias(CourrierLienTableMap::COL_ID_INDIVIDU, $individu->toKeyValue('PrimaryKey', 'IdIndividu'), $comparison);
        } else {
            throw new PropelException('filterByIndividu() only accepts arguments of type \Individu or Collection');
        }
    }

    /**
     * Adds a JOIN clause to the query using the Individu relation
     *
     * @param     string $relationAlias optional alias for the relation
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return $this|ChildCourrierLienQuery The current query, for fluid interface
     */
    public function joinIndividu($relationAlias = null, $joinType = Criteria::INNER_JOIN)
    {
        $tableMap = $this->getTableMap();
        $relationMap = $tableMap->getRelation('Individu');

        // create a ModelJoin object for this join
        $join = new ModelJoin();
        $join->setJoinType($joinType);
        $join->setRelationMap($relationMap, $this->useAliasInSQL ? $this->getModelAlias() : null, $relationAlias);
        if ($previousJoin = $this->getPreviousJoin()) {
            $join->setPreviousJoin($previousJoin);
        }

        // add the ModelJoin to the current object
        if ($relationAlias) {
            $this->addAlias($relationAlias, $relationMap->getRightTable()->getName());
            $this->addJoinObject($join, $relationAlias);
        } else {
            $this->addJoinObject($join, 'Individu');
        }

        return $this;
    }

    /**
     * Use the Individu relation Individu object
     *
     * @see useQuery()
     *
     * @param     string $relationAlias optional alias for the relation,
     *                                   to be used as main alias in the secondary query
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return \IndividuQuery A secondary query class using the current class as primary query
     */
    public function useIndividuQuery($relationAlias = null, $joinType = Criteria::INNER_JOIN)
    {
        return $this
            ->joinIndividu($relationAlias, $joinType)
            ->useQuery($relationAlias ? $relationAlias : 'Individu', '\IndividuQuery');
    }

    /**
     * Filter the query by a related \Membre object
     *
     * @param \Membre|ObjectCollection $membre The related object(s) to use as filter
     * @param string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @throws \Propel\Runtime\Exception\PropelException
     *
     * @return ChildCourrierLienQuery The current query, for fluid interface
     */
    public function filterByMembre($membre, $comparison = null)
    {
        if ($membre instanceof \Membre) {
            return $this
                ->addUsingAlias(CourrierLienTableMap::COL_ID_MEMBRE, $membre->getIdMembre(), $comparison);
        } elseif ($membre instanceof ObjectCollection) {
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }

            return $this
                ->addUsingAlias(CourrierLienTableMap::COL_ID_MEMBRE, $membre->toKeyValue('PrimaryKey', 'IdMembre'), $comparison);
        } else {
            throw new PropelException('filterByMembre() only accepts arguments of type \Membre or Collection');
        }
    }

    /**
     * Adds a JOIN clause to the query using the Membre relation
     *
     * @param     string $relationAlias optional alias for the relation
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return $this|ChildCourrierLienQuery The current query, for fluid interface
     */
    public function joinMembre($relationAlias = null, $joinType = Criteria::LEFT_JOIN)
    {
        $tableMap = $this->getTableMap();
        $relationMap = $tableMap->getRelation('Membre');

        // create a ModelJoin object for this join
        $join = new ModelJoin();
        $join->setJoinType($joinType);
        $join->setRelationMap($relationMap, $this->useAliasInSQL ? $this->getModelAlias() : null, $relationAlias);
        if ($previousJoin = $this->getPreviousJoin()) {
            $join->setPreviousJoin($previousJoin);
        }

        // add the ModelJoin to the current object
        if ($relationAlias) {
            $this->addAlias($relationAlias, $relationMap->getRightTable()->getName());
            $this->addJoinObject($join, $relationAlias);
        } else {
            $this->addJoinObject($join, 'Membre');
        }

        return $this;
    }

    /**
     * Use the Membre relation Membre object
     *
     * @see useQuery()
     *
     * @param     string $relationAlias optional alias for the relation,
     *                                   to be used as main alias in the secondary query
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return \MembreQuery A secondary query class using the current class as primary query
     */
    public function useMembreQuery($relationAlias = null, $joinType = Criteria::LEFT_JOIN)
    {
        return $this
            ->joinMembre($relationAlias, $joinType)
            ->useQuery($relationAlias ? $relationAlias : 'Membre', '\MembreQuery');
    }

    /**
     * Filter the query by a related \Courrier object
     *
     * @param \Courrier|ObjectCollection $courrier The related object(s) to use as filter
     * @param string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @throws \Propel\Runtime\Exception\PropelException
     *
     * @return ChildCourrierLienQuery The current query, for fluid interface
     */
    public function filterByCourrier($courrier, $comparison = null)
    {
        if ($courrier instanceof \Courrier) {
            return $this
                ->addUsingAlias(CourrierLienTableMap::COL_ID_COURRIER, $courrier->getIdCourrier(), $comparison);
        } elseif ($courrier instanceof ObjectCollection) {
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }

            return $this
                ->addUsingAlias(CourrierLienTableMap::COL_ID_COURRIER, $courrier->toKeyValue('PrimaryKey', 'IdCourrier'), $comparison);
        } else {
            throw new PropelException('filterByCourrier() only accepts arguments of type \Courrier or Collection');
        }
    }

    /**
     * Adds a JOIN clause to the query using the Courrier relation
     *
     * @param     string $relationAlias optional alias for the relation
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return $this|ChildCourrierLienQuery The current query, for fluid interface
     */
    public function joinCourrier($relationAlias = null, $joinType = Criteria::INNER_JOIN)
    {
        $tableMap = $this->getTableMap();
        $relationMap = $tableMap->getRelation('Courrier');

        // create a ModelJoin object for this join
        $join = new ModelJoin();
        $join->setJoinType($joinType);
        $join->setRelationMap($relationMap, $this->useAliasInSQL ? $this->getModelAlias() : null, $relationAlias);
        if ($previousJoin = $this->getPreviousJoin()) {
            $join->setPreviousJoin($previousJoin);
        }

        // add the ModelJoin to the current object
        if ($relationAlias) {
            $this->addAlias($relationAlias, $relationMap->getRightTable()->getName());
            $this->addJoinObject($join, $relationAlias);
        } else {
            $this->addJoinObject($join, 'Courrier');
        }

        return $this;
    }

    /**
     * Use the Courrier relation Courrier object
     *
     * @see useQuery()
     *
     * @param     string $relationAlias optional alias for the relation,
     *                                   to be used as main alias in the secondary query
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return \CourrierQuery A secondary query class using the current class as primary query
     */
    public function useCourrierQuery($relationAlias = null, $joinType = Criteria::INNER_JOIN)
    {
        return $this
            ->joinCourrier($relationAlias, $joinType)
            ->useQuery($relationAlias ? $relationAlias : 'Courrier', '\CourrierQuery');
    }

    /**
     * Exclude object from result
     *
     * @param   ChildCourrierLien $courrierLien Object to remove from the list of results
     *
     * @return $this|ChildCourrierLienQuery The current query, for fluid interface
     */
    public function prune($courrierLien = null)
    {
        if ($courrierLien) {
            $this->addUsingAlias(CourrierLienTableMap::COL_ID_COURRIER_LIEN, $courrierLien->getIdCourrierLien(), Criteria::NOT_EQUAL);
        }

        return $this;
    }

    /**
     * Code to execute before every DELETE statement
     *
     * @param     ConnectionInterface $con The connection object used by the query
     */
    protected function basePreDelete(ConnectionInterface $con)
    {
        // archivable behavior

        if ($this->archiveOnDelete) {
            $this->archive($con);
        } else {
            $this->archiveOnDelete = true;
        }


        return $this->preDelete($con);
    }

    /**
     * Deletes all rows from the com_courriers_liens table.
     *
     * @param ConnectionInterface $con the connection to use
     * @return int The number of affected rows (if supported by underlying database driver).
     */
    public function doDeleteAll(ConnectionInterface $con = null)
    {
        if (null === $con) {
            $con = Propel::getServiceContainer()->getWriteConnection(CourrierLienTableMap::DATABASE_NAME);
        }

        // use transaction because $criteria could contain info
        // for more than one table or we could emulating ON DELETE CASCADE, etc.
        return $con->transaction(function () use ($con) {
            $affectedRows = 0; // initialize var to track total num of affected rows
            $affectedRows += parent::doDeleteAll($con);
            // Because this db requires some delete cascade/set null emulation, we have to
            // clear the cached instance *after* the emulation has happened (since
            // instances get re-added by the select statement contained therein).
            CourrierLienTableMap::clearInstancePool();
            CourrierLienTableMap::clearRelatedInstancePool();

            return $affectedRows;
        });
    }

    /**
     * Performs a DELETE on the database based on the current ModelCriteria
     *
     * @param ConnectionInterface $con the connection to use
     * @return int             The number of affected rows (if supported by underlying database driver).  This includes CASCADE-related rows
     *                         if supported by native driver or if emulated using Propel.
     * @throws PropelException Any exceptions caught during processing will be
     *                         rethrown wrapped into a PropelException.
     */
    public function delete(ConnectionInterface $con = null)
    {
        if (null === $con) {
            $con = Propel::getServiceContainer()->getWriteConnection(CourrierLienTableMap::DATABASE_NAME);
        }

        $criteria = $this;

        // Set the correct dbName
        $criteria->setDbName(CourrierLienTableMap::DATABASE_NAME);

        // use transaction because $criteria could contain info
        // for more than one table or we could emulating ON DELETE CASCADE, etc.
        return $con->transaction(function () use ($con, $criteria) {
            $affectedRows = 0; // initialize var to track total num of affected rows

            CourrierLienTableMap::removeInstanceFromPool($criteria);

            $affectedRows += ModelCriteria::delete($con);
            CourrierLienTableMap::clearRelatedInstancePool();

            return $affectedRows;
        });
    }

    // timestampable behavior

    /**
     * Filter by the latest updated
     *
     * @param      int $nbDays Maximum age of the latest update in days
     *
     * @return     $this|ChildCourrierLienQuery The current query, for fluid interface
     */
    public function recentlyUpdated($nbDays = 7)
    {
        return $this->addUsingAlias(CourrierLienTableMap::COL_UPDATED_AT, time() - $nbDays * 24 * 60 * 60, Criteria::GREATER_EQUAL);
    }

    /**
     * Order by update date desc
     *
     * @return     $this|ChildCourrierLienQuery The current query, for fluid interface
     */
    public function lastUpdatedFirst()
    {
        return $this->addDescendingOrderByColumn(CourrierLienTableMap::COL_UPDATED_AT);
    }

    /**
     * Order by update date asc
     *
     * @return     $this|ChildCourrierLienQuery The current query, for fluid interface
     */
    public function firstUpdatedFirst()
    {
        return $this->addAscendingOrderByColumn(CourrierLienTableMap::COL_UPDATED_AT);
    }

    /**
     * Order by create date desc
     *
     * @return     $this|ChildCourrierLienQuery The current query, for fluid interface
     */
    public function lastCreatedFirst()
    {
        return $this->addDescendingOrderByColumn(CourrierLienTableMap::COL_CREATED_AT);
    }

    /**
     * Filter by the latest created
     *
     * @param      int $nbDays Maximum age of in days
     *
     * @return     $this|ChildCourrierLienQuery The current query, for fluid interface
     */
    public function recentlyCreated($nbDays = 7)
    {
        return $this->addUsingAlias(CourrierLienTableMap::COL_CREATED_AT, time() - $nbDays * 24 * 60 * 60, Criteria::GREATER_EQUAL);
    }

    /**
     * Order by create date asc
     *
     * @return     $this|ChildCourrierLienQuery The current query, for fluid interface
     */
    public function firstCreatedFirst()
    {
        return $this->addAscendingOrderByColumn(CourrierLienTableMap::COL_CREATED_AT);
    }

    // archivable behavior

    /**
     * Copy the data of the objects satisfying the query into ChildComCourriersLiensArchive archive objects.
     * The archived objects are then saved.
     * If any of the objects has already been archived, the archived object
     * is updated and not duplicated.
     * Warning: This termination methods issues 2n+1 queries.
     *
     * @param      ConnectionInterface $con    Connection to use.
     * @param      Boolean $useLittleMemory    Whether or not to use OnDemandFormatter to retrieve objects.
     *               Set to false if the identity map matters.
     *               Set to true (default) to use less memory.
     *
     * @return     int the number of archived objects
     */
    public function archive($con = null, $useLittleMemory = true)
    {
        $criteria = clone $this;
        // prepare the query
        $criteria->setWith(array());
        if ($useLittleMemory) {
            $criteria->setFormatter(ModelCriteria::FORMAT_ON_DEMAND);
        }
        if ($con === null) {
            $con = Propel::getServiceContainer()->getWriteConnection(CourrierLienTableMap::DATABASE_NAME);
        }

        return $con->transaction(function () use ($con, $criteria) {
            $totalArchivedObjects = 0;

            // archive all results one by one
            foreach ($criteria->find($con) as $object) {
                $object->archive($con);
                $totalArchivedObjects++;
            }

            return $totalArchivedObjects;
        });
    }

    /**
     * Enable/disable auto-archiving on delete for the next query.
     *
     * @param boolean True if the query must archive deleted objects, false otherwise.
     */
    public function setArchiveOnDelete($archiveOnDelete)
    {
        $this->archiveOnDelete = $archiveOnDelete;
    }

    /**
     * Delete records matching the current query without archiving them.
     *
     * @param      ConnectionInterface $con    Connection to use.
     *
     * @return integer the number of deleted rows
     */
    public function deleteWithoutArchive($con = null)
    {
        $this->archiveOnDelete = false;

        return $this->delete($con);
    }

    /**
     * Delete all records without archiving them.
     *
     * @param      ConnectionInterface $con    Connection to use.
     *
     * @return integer the number of deleted rows
     */
    public function deleteAllWithoutArchive($con = null)
    {
        $this->archiveOnDelete = false;

        return $this->deleteAll($con);
    }

} // CourrierLienQuery
