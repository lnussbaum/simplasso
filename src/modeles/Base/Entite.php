<?php

namespace Base;

use \Activite as ChildActivite;
use \ActiviteQuery as ChildActiviteQuery;
use \AssoEntitesArchive as ChildAssoEntitesArchive;
use \AssoEntitesArchiveQuery as ChildAssoEntitesArchiveQuery;
use \Autorisation as ChildAutorisation;
use \AutorisationQuery as ChildAutorisationQuery;
use \Compte as ChildCompte;
use \CompteQuery as ChildCompteQuery;
use \Courrier as ChildCourrier;
use \CourrierQuery as ChildCourrierQuery;
use \Ecriture as ChildEcriture;
use \EcritureQuery as ChildEcritureQuery;
use \Entite as ChildEntite;
use \EntiteQuery as ChildEntiteQuery;
use \Journal as ChildJournal;
use \JournalQuery as ChildJournalQuery;
use \Paiement as ChildPaiement;
use \PaiementQuery as ChildPaiementQuery;
use \Piece as ChildPiece;
use \PieceQuery as ChildPieceQuery;
use \Prestation as ChildPrestation;
use \PrestationQuery as ChildPrestationQuery;
use \Prestationslot as ChildPrestationslot;
use \PrestationslotQuery as ChildPrestationslotQuery;
use \Servicerendu as ChildServicerendu;
use \ServicerenduQuery as ChildServicerenduQuery;
use \Tresor as ChildTresor;
use \TresorQuery as ChildTresorQuery;
use \Tva as ChildTva;
use \TvaQuery as ChildTvaQuery;
use \DateTime;
use \Exception;
use \PDO;
use Map\ActiviteTableMap;
use Map\AutorisationTableMap;
use Map\CompteTableMap;
use Map\CourrierTableMap;
use Map\EcritureTableMap;
use Map\EntiteTableMap;
use Map\JournalTableMap;
use Map\PaiementTableMap;
use Map\PieceTableMap;
use Map\PrestationTableMap;
use Map\PrestationslotTableMap;
use Map\ServicerenduTableMap;
use Map\TresorTableMap;
use Map\TvaTableMap;
use Propel\Runtime\Propel;
use Propel\Runtime\ActiveQuery\Criteria;
use Propel\Runtime\ActiveQuery\ModelCriteria;
use Propel\Runtime\ActiveRecord\ActiveRecordInterface;
use Propel\Runtime\Collection\Collection;
use Propel\Runtime\Collection\ObjectCollection;
use Propel\Runtime\Connection\ConnectionInterface;
use Propel\Runtime\Exception\BadMethodCallException;
use Propel\Runtime\Exception\LogicException;
use Propel\Runtime\Exception\PropelException;
use Propel\Runtime\Map\TableMap;
use Propel\Runtime\Parser\AbstractParser;
use Propel\Runtime\Util\PropelDateTime;

/**
 * Base class that represents a row from the 'asso_entites' table.
 *
 *
 *
 * @package    propel.generator..Base
 */
abstract class Entite implements ActiveRecordInterface
{
    /**
     * TableMap class name
     */
    const TABLE_MAP = '\\Map\\EntiteTableMap';


    /**
     * attribute to determine if this object has previously been saved.
     * @var boolean
     */
    protected $new = true;

    /**
     * attribute to determine whether this object has been deleted.
     * @var boolean
     */
    protected $deleted = false;

    /**
     * The columns that have been modified in current object.
     * Tracking modified columns allows us to only update modified columns.
     * @var array
     */
    protected $modifiedColumns = array();

    /**
     * The (virtual) columns that are added at runtime
     * The formatters can add supplementary columns based on a resultset
     * @var array
     */
    protected $virtualColumns = array();

    /**
     * The value for the id_entite field.
     *
     * @var        string
     */
    protected $id_entite;

    /**
     * The value for the nom field.
     * Nom
     * @var        string
     */
    protected $nom;

    /**
     * The value for the nomcourt field.
     * nomcourt
     * Note: this column has a database default value of: 'ncourt'
     * @var        string
     */
    protected $nomcourt;

    /**
     * The value for the adresse field.
     *
     * @var        string
     */
    protected $adresse;

    /**
     * The value for the codepostal field.
     *
     * @var        string
     */
    protected $codepostal;

    /**
     * The value for the ville field.
     *
     * @var        string
     */
    protected $ville;

    /**
     * The value for the pays field.
     *
     * Note: this column has a database default value of: 'FR'
     * @var        string
     */
    protected $pays;

    /**
     * The value for the telephone field.
     *
     * @var        string
     */
    protected $telephone;

    /**
     * The value for the fax field.
     *
     * @var        string
     */
    protected $fax;

    /**
     * The value for the url field.
     *
     * @var        string
     */
    protected $url;

    /**
     * The value for the assujettitva field.
     * 0non1oui
     * Note: this column has a database default value of: 0
     * @var        int
     */
    protected $assujettitva;

    /**
     * The value for the email field.
     *
     * @var        string
     */
    protected $email;

    /**
     * The value for the created_at field.
     *
     * @var        DateTime
     */
    protected $created_at;

    /**
     * The value for the updated_at field.
     *
     * @var        DateTime
     */
    protected $updated_at;

    /**
     * @var        ObjectCollection|ChildActivite[] Collection to store aggregation of ChildActivite objects.
     */
    protected $collActivites;
    protected $collActivitesPartial;

    /**
     * @var        ObjectCollection|ChildCompte[] Collection to store aggregation of ChildCompte objects.
     */
    protected $collComptes;
    protected $collComptesPartial;

    /**
     * @var        ObjectCollection|ChildEcriture[] Collection to store aggregation of ChildEcriture objects.
     */
    protected $collEcritures;
    protected $collEcrituresPartial;

    /**
     * @var        ObjectCollection|ChildJournal[] Collection to store aggregation of ChildJournal objects.
     */
    protected $collJournals;
    protected $collJournalsPartial;

    /**
     * @var        ObjectCollection|ChildPiece[] Collection to store aggregation of ChildPiece objects.
     */
    protected $collPieces;
    protected $collPiecesPartial;

    /**
     * @var        ObjectCollection|ChildAutorisation[] Collection to store aggregation of ChildAutorisation objects.
     */
    protected $collAutorisations;
    protected $collAutorisationsPartial;

    /**
     * @var        ObjectCollection|ChildPaiement[] Collection to store aggregation of ChildPaiement objects.
     */
    protected $collPaiements;
    protected $collPaiementsPartial;

    /**
     * @var        ObjectCollection|ChildPrestation[] Collection to store aggregation of ChildPrestation objects.
     */
    protected $collPrestations;
    protected $collPrestationsPartial;

    /**
     * @var        ObjectCollection|ChildPrestationslot[] Collection to store aggregation of ChildPrestationslot objects.
     */
    protected $collPrestationslots;
    protected $collPrestationslotsPartial;

    /**
     * @var        ObjectCollection|ChildServicerendu[] Collection to store aggregation of ChildServicerendu objects.
     */
    protected $collServicerendus;
    protected $collServicerendusPartial;

    /**
     * @var        ObjectCollection|ChildTresor[] Collection to store aggregation of ChildTresor objects.
     */
    protected $collTresors;
    protected $collTresorsPartial;

    /**
     * @var        ObjectCollection|ChildTva[] Collection to store aggregation of ChildTva objects.
     */
    protected $collTvas;
    protected $collTvasPartial;

    /**
     * @var        ObjectCollection|ChildCourrier[] Collection to store aggregation of ChildCourrier objects.
     */
    protected $collCourriers;
    protected $collCourriersPartial;

    /**
     * Flag to prevent endless save loop, if this object is referenced
     * by another object which falls in this transaction.
     *
     * @var boolean
     */
    protected $alreadyInSave = false;

    // archivable behavior
    protected $archiveOnDelete = true;

    /**
     * An array of objects scheduled for deletion.
     * @var ObjectCollection|ChildActivite[]
     */
    protected $activitesScheduledForDeletion = null;

    /**
     * An array of objects scheduled for deletion.
     * @var ObjectCollection|ChildCompte[]
     */
    protected $comptesScheduledForDeletion = null;

    /**
     * An array of objects scheduled for deletion.
     * @var ObjectCollection|ChildEcriture[]
     */
    protected $ecrituresScheduledForDeletion = null;

    /**
     * An array of objects scheduled for deletion.
     * @var ObjectCollection|ChildJournal[]
     */
    protected $journalsScheduledForDeletion = null;

    /**
     * An array of objects scheduled for deletion.
     * @var ObjectCollection|ChildPiece[]
     */
    protected $piecesScheduledForDeletion = null;

    /**
     * An array of objects scheduled for deletion.
     * @var ObjectCollection|ChildAutorisation[]
     */
    protected $autorisationsScheduledForDeletion = null;

    /**
     * An array of objects scheduled for deletion.
     * @var ObjectCollection|ChildPaiement[]
     */
    protected $paiementsScheduledForDeletion = null;

    /**
     * An array of objects scheduled for deletion.
     * @var ObjectCollection|ChildPrestation[]
     */
    protected $prestationsScheduledForDeletion = null;

    /**
     * An array of objects scheduled for deletion.
     * @var ObjectCollection|ChildPrestationslot[]
     */
    protected $prestationslotsScheduledForDeletion = null;

    /**
     * An array of objects scheduled for deletion.
     * @var ObjectCollection|ChildServicerendu[]
     */
    protected $servicerendusScheduledForDeletion = null;

    /**
     * An array of objects scheduled for deletion.
     * @var ObjectCollection|ChildTresor[]
     */
    protected $tresorsScheduledForDeletion = null;

    /**
     * An array of objects scheduled for deletion.
     * @var ObjectCollection|ChildTva[]
     */
    protected $tvasScheduledForDeletion = null;

    /**
     * An array of objects scheduled for deletion.
     * @var ObjectCollection|ChildCourrier[]
     */
    protected $courriersScheduledForDeletion = null;

    /**
     * Applies default values to this object.
     * This method should be called from the object's constructor (or
     * equivalent initialization method).
     * @see __construct()
     */
    public function applyDefaultValues()
    {
        $this->nomcourt = 'ncourt';
        $this->pays = 'FR';
        $this->assujettitva = 0;
    }

    /**
     * Initializes internal state of Base\Entite object.
     * @see applyDefaults()
     */
    public function __construct()
    {
        $this->applyDefaultValues();
    }

    /**
     * Returns whether the object has been modified.
     *
     * @return boolean True if the object has been modified.
     */
    public function isModified()
    {
        return !!$this->modifiedColumns;
    }

    /**
     * Has specified column been modified?
     *
     * @param  string  $col column fully qualified name (TableMap::TYPE_COLNAME), e.g. Book::AUTHOR_ID
     * @return boolean True if $col has been modified.
     */
    public function isColumnModified($col)
    {
        return $this->modifiedColumns && isset($this->modifiedColumns[$col]);
    }

    /**
     * Get the columns that have been modified in this object.
     * @return array A unique list of the modified column names for this object.
     */
    public function getModifiedColumns()
    {
        return $this->modifiedColumns ? array_keys($this->modifiedColumns) : [];
    }

    /**
     * Returns whether the object has ever been saved.  This will
     * be false, if the object was retrieved from storage or was created
     * and then saved.
     *
     * @return boolean true, if the object has never been persisted.
     */
    public function isNew()
    {
        return $this->new;
    }

    /**
     * Setter for the isNew attribute.  This method will be called
     * by Propel-generated children and objects.
     *
     * @param boolean $b the state of the object.
     */
    public function setNew($b)
    {
        $this->new = (boolean) $b;
    }

    /**
     * Whether this object has been deleted.
     * @return boolean The deleted state of this object.
     */
    public function isDeleted()
    {
        return $this->deleted;
    }

    /**
     * Specify whether this object has been deleted.
     * @param  boolean $b The deleted state of this object.
     * @return void
     */
    public function setDeleted($b)
    {
        $this->deleted = (boolean) $b;
    }

    /**
     * Sets the modified state for the object to be false.
     * @param  string $col If supplied, only the specified column is reset.
     * @return void
     */
    public function resetModified($col = null)
    {
        if (null !== $col) {
            if (isset($this->modifiedColumns[$col])) {
                unset($this->modifiedColumns[$col]);
            }
        } else {
            $this->modifiedColumns = array();
        }
    }

    /**
     * Compares this with another <code>Entite</code> instance.  If
     * <code>obj</code> is an instance of <code>Entite</code>, delegates to
     * <code>equals(Entite)</code>.  Otherwise, returns <code>false</code>.
     *
     * @param  mixed   $obj The object to compare to.
     * @return boolean Whether equal to the object specified.
     */
    public function equals($obj)
    {
        if (!$obj instanceof static) {
            return false;
        }

        if ($this === $obj) {
            return true;
        }

        if (null === $this->getPrimaryKey() || null === $obj->getPrimaryKey()) {
            return false;
        }

        return $this->getPrimaryKey() === $obj->getPrimaryKey();
    }

    /**
     * Get the associative array of the virtual columns in this object
     *
     * @return array
     */
    public function getVirtualColumns()
    {
        return $this->virtualColumns;
    }

    /**
     * Checks the existence of a virtual column in this object
     *
     * @param  string  $name The virtual column name
     * @return boolean
     */
    public function hasVirtualColumn($name)
    {
        return array_key_exists($name, $this->virtualColumns);
    }

    /**
     * Get the value of a virtual column in this object
     *
     * @param  string $name The virtual column name
     * @return mixed
     *
     * @throws PropelException
     */
    public function getVirtualColumn($name)
    {
        if (!$this->hasVirtualColumn($name)) {
            throw new PropelException(sprintf('Cannot get value of inexistent virtual column %s.', $name));
        }

        return $this->virtualColumns[$name];
    }

    /**
     * Set the value of a virtual column in this object
     *
     * @param string $name  The virtual column name
     * @param mixed  $value The value to give to the virtual column
     *
     * @return $this|Entite The current object, for fluid interface
     */
    public function setVirtualColumn($name, $value)
    {
        $this->virtualColumns[$name] = $value;

        return $this;
    }

    /**
     * Logs a message using Propel::log().
     *
     * @param  string  $msg
     * @param  int     $priority One of the Propel::LOG_* logging levels
     * @return boolean
     */
    protected function log($msg, $priority = Propel::LOG_INFO)
    {
        return Propel::log(get_class($this) . ': ' . $msg, $priority);
    }

    /**
     * Export the current object properties to a string, using a given parser format
     * <code>
     * $book = BookQuery::create()->findPk(9012);
     * echo $book->exportTo('JSON');
     *  => {"Id":9012,"Title":"Don Juan","ISBN":"0140422161","Price":12.99,"PublisherId":1234,"AuthorId":5678}');
     * </code>
     *
     * @param  mixed   $parser                 A AbstractParser instance, or a format name ('XML', 'YAML', 'JSON', 'CSV')
     * @param  boolean $includeLazyLoadColumns (optional) Whether to include lazy load(ed) columns. Defaults to TRUE.
     * @return string  The exported data
     */
    public function exportTo($parser, $includeLazyLoadColumns = true)
    {
        if (!$parser instanceof AbstractParser) {
            $parser = AbstractParser::getParser($parser);
        }

        return $parser->fromArray($this->toArray(TableMap::TYPE_PHPNAME, $includeLazyLoadColumns, array(), true));
    }

    /**
     * Clean up internal collections prior to serializing
     * Avoids recursive loops that turn into segmentation faults when serializing
     */
    public function __sleep()
    {
        $this->clearAllReferences();

        $cls = new \ReflectionClass($this);
        $propertyNames = [];
        $serializableProperties = array_diff($cls->getProperties(), $cls->getProperties(\ReflectionProperty::IS_STATIC));

        foreach($serializableProperties as $property) {
            $propertyNames[] = $property->getName();
        }

        return $propertyNames;
    }

    /**
     * Get the [id_entite] column value.
     *
     * @return string
     */
    public function getIdEntite()
    {
        return $this->id_entite;
    }

    /**
     * Get the [nom] column value.
     * Nom
     * @return string
     */
    public function getNom()
    {
        return $this->nom;
    }

    /**
     * Get the [nomcourt] column value.
     * nomcourt
     * @return string
     */
    public function getNomcourt()
    {
        return $this->nomcourt;
    }

    /**
     * Get the [adresse] column value.
     *
     * @return string
     */
    public function getAdresse()
    {
        return $this->adresse;
    }

    /**
     * Get the [codepostal] column value.
     *
     * @return string
     */
    public function getCodepostal()
    {
        return $this->codepostal;
    }

    /**
     * Get the [ville] column value.
     *
     * @return string
     */
    public function getVille()
    {
        return $this->ville;
    }

    /**
     * Get the [pays] column value.
     *
     * @return string
     */
    public function getPays()
    {
        return $this->pays;
    }

    /**
     * Get the [telephone] column value.
     *
     * @return string
     */
    public function getTelephone()
    {
        return $this->telephone;
    }

    /**
     * Get the [fax] column value.
     *
     * @return string
     */
    public function getFax()
    {
        return $this->fax;
    }

    /**
     * Get the [url] column value.
     *
     * @return string
     */
    public function getUrl()
    {
        return $this->url;
    }

    /**
     * Get the [assujettitva] column value.
     * 0non1oui
     * @return int
     */
    public function getAssujettitva()
    {
        return $this->assujettitva;
    }

    /**
     * Get the [email] column value.
     *
     * @return string
     */
    public function getEmail()
    {
        return $this->email;
    }

    /**
     * Get the [optionally formatted] temporal [created_at] column value.
     *
     *
     * @param      string $format The date/time format string (either date()-style or strftime()-style).
     *                            If format is NULL, then the raw DateTime object will be returned.
     *
     * @return string|DateTime Formatted date/time value as string or DateTime object (if format is NULL), NULL if column is NULL, and 0 if column value is 0000-00-00 00:00:00
     *
     * @throws PropelException - if unable to parse/validate the date/time value.
     */
    public function getCreatedAt($format = NULL)
    {
        if ($format === null) {
            return $this->created_at;
        } else {
            return $this->created_at instanceof \DateTimeInterface ? $this->created_at->format($format) : null;
        }
    }

    /**
     * Get the [optionally formatted] temporal [updated_at] column value.
     *
     *
     * @param      string $format The date/time format string (either date()-style or strftime()-style).
     *                            If format is NULL, then the raw DateTime object will be returned.
     *
     * @return string|DateTime Formatted date/time value as string or DateTime object (if format is NULL), NULL if column is NULL, and 0 if column value is 0000-00-00 00:00:00
     *
     * @throws PropelException - if unable to parse/validate the date/time value.
     */
    public function getUpdatedAt($format = NULL)
    {
        if ($format === null) {
            return $this->updated_at;
        } else {
            return $this->updated_at instanceof \DateTimeInterface ? $this->updated_at->format($format) : null;
        }
    }

    /**
     * Set the value of [id_entite] column.
     *
     * @param string $v new value
     * @return $this|\Entite The current object (for fluent API support)
     */
    public function setIdEntite($v)
    {
        if ($v !== null) {
            $v = (string) $v;
        }

        if ($this->id_entite !== $v) {
            $this->id_entite = $v;
            $this->modifiedColumns[EntiteTableMap::COL_ID_ENTITE] = true;
        }

        return $this;
    } // setIdEntite()

    /**
     * Set the value of [nom] column.
     * Nom
     * @param string $v new value
     * @return $this|\Entite The current object (for fluent API support)
     */
    public function setNom($v)
    {
        if ($v !== null) {
            $v = (string) $v;
        }

        if ($this->nom !== $v) {
            $this->nom = $v;
            $this->modifiedColumns[EntiteTableMap::COL_NOM] = true;
        }

        return $this;
    } // setNom()

    /**
     * Set the value of [nomcourt] column.
     * nomcourt
     * @param string $v new value
     * @return $this|\Entite The current object (for fluent API support)
     */
    public function setNomcourt($v)
    {
        if ($v !== null) {
            $v = (string) $v;
        }

        if ($this->nomcourt !== $v) {
            $this->nomcourt = $v;
            $this->modifiedColumns[EntiteTableMap::COL_NOMCOURT] = true;
        }

        return $this;
    } // setNomcourt()

    /**
     * Set the value of [adresse] column.
     *
     * @param string $v new value
     * @return $this|\Entite The current object (for fluent API support)
     */
    public function setAdresse($v)
    {
        if ($v !== null) {
            $v = (string) $v;
        }

        if ($this->adresse !== $v) {
            $this->adresse = $v;
            $this->modifiedColumns[EntiteTableMap::COL_ADRESSE] = true;
        }

        return $this;
    } // setAdresse()

    /**
     * Set the value of [codepostal] column.
     *
     * @param string $v new value
     * @return $this|\Entite The current object (for fluent API support)
     */
    public function setCodepostal($v)
    {
        if ($v !== null) {
            $v = (string) $v;
        }

        if ($this->codepostal !== $v) {
            $this->codepostal = $v;
            $this->modifiedColumns[EntiteTableMap::COL_CODEPOSTAL] = true;
        }

        return $this;
    } // setCodepostal()

    /**
     * Set the value of [ville] column.
     *
     * @param string $v new value
     * @return $this|\Entite The current object (for fluent API support)
     */
    public function setVille($v)
    {
        if ($v !== null) {
            $v = (string) $v;
        }

        if ($this->ville !== $v) {
            $this->ville = $v;
            $this->modifiedColumns[EntiteTableMap::COL_VILLE] = true;
        }

        return $this;
    } // setVille()

    /**
     * Set the value of [pays] column.
     *
     * @param string $v new value
     * @return $this|\Entite The current object (for fluent API support)
     */
    public function setPays($v)
    {
        if ($v !== null) {
            $v = (string) $v;
        }

        if ($this->pays !== $v) {
            $this->pays = $v;
            $this->modifiedColumns[EntiteTableMap::COL_PAYS] = true;
        }

        return $this;
    } // setPays()

    /**
     * Set the value of [telephone] column.
     *
     * @param string $v new value
     * @return $this|\Entite The current object (for fluent API support)
     */
    public function setTelephone($v)
    {
        if ($v !== null) {
            $v = (string) $v;
        }

        if ($this->telephone !== $v) {
            $this->telephone = $v;
            $this->modifiedColumns[EntiteTableMap::COL_TELEPHONE] = true;
        }

        return $this;
    } // setTelephone()

    /**
     * Set the value of [fax] column.
     *
     * @param string $v new value
     * @return $this|\Entite The current object (for fluent API support)
     */
    public function setFax($v)
    {
        if ($v !== null) {
            $v = (string) $v;
        }

        if ($this->fax !== $v) {
            $this->fax = $v;
            $this->modifiedColumns[EntiteTableMap::COL_FAX] = true;
        }

        return $this;
    } // setFax()

    /**
     * Set the value of [url] column.
     *
     * @param string $v new value
     * @return $this|\Entite The current object (for fluent API support)
     */
    public function setUrl($v)
    {
        if ($v !== null) {
            $v = (string) $v;
        }

        if ($this->url !== $v) {
            $this->url = $v;
            $this->modifiedColumns[EntiteTableMap::COL_URL] = true;
        }

        return $this;
    } // setUrl()

    /**
     * Set the value of [assujettitva] column.
     * 0non1oui
     * @param int $v new value
     * @return $this|\Entite The current object (for fluent API support)
     */
    public function setAssujettitva($v)
    {
        if ($v !== null) {
            $v = (int) $v;
        }

        if ($this->assujettitva !== $v) {
            $this->assujettitva = $v;
            $this->modifiedColumns[EntiteTableMap::COL_ASSUJETTITVA] = true;
        }

        return $this;
    } // setAssujettitva()

    /**
     * Set the value of [email] column.
     *
     * @param string $v new value
     * @return $this|\Entite The current object (for fluent API support)
     */
    public function setEmail($v)
    {
        if ($v !== null) {
            $v = (string) $v;
        }

        if ($this->email !== $v) {
            $this->email = $v;
            $this->modifiedColumns[EntiteTableMap::COL_EMAIL] = true;
        }

        return $this;
    } // setEmail()

    /**
     * Sets the value of [created_at] column to a normalized version of the date/time value specified.
     *
     * @param  mixed $v string, integer (timestamp), or \DateTimeInterface value.
     *               Empty strings are treated as NULL.
     * @return $this|\Entite The current object (for fluent API support)
     */
    public function setCreatedAt($v)
    {
        $dt = PropelDateTime::newInstance($v, null, 'DateTime');
        if ($this->created_at !== null || $dt !== null) {
            if ($this->created_at === null || $dt === null || $dt->format("Y-m-d H:i:s.u") !== $this->created_at->format("Y-m-d H:i:s.u")) {
                $this->created_at = $dt === null ? null : clone $dt;
                $this->modifiedColumns[EntiteTableMap::COL_CREATED_AT] = true;
            }
        } // if either are not null

        return $this;
    } // setCreatedAt()

    /**
     * Sets the value of [updated_at] column to a normalized version of the date/time value specified.
     *
     * @param  mixed $v string, integer (timestamp), or \DateTimeInterface value.
     *               Empty strings are treated as NULL.
     * @return $this|\Entite The current object (for fluent API support)
     */
    public function setUpdatedAt($v)
    {
        $dt = PropelDateTime::newInstance($v, null, 'DateTime');
        if ($this->updated_at !== null || $dt !== null) {
            if ($this->updated_at === null || $dt === null || $dt->format("Y-m-d H:i:s.u") !== $this->updated_at->format("Y-m-d H:i:s.u")) {
                $this->updated_at = $dt === null ? null : clone $dt;
                $this->modifiedColumns[EntiteTableMap::COL_UPDATED_AT] = true;
            }
        } // if either are not null

        return $this;
    } // setUpdatedAt()

    /**
     * Indicates whether the columns in this object are only set to default values.
     *
     * This method can be used in conjunction with isModified() to indicate whether an object is both
     * modified _and_ has some values set which are non-default.
     *
     * @return boolean Whether the columns in this object are only been set with default values.
     */
    public function hasOnlyDefaultValues()
    {
            if ($this->nomcourt !== 'ncourt') {
                return false;
            }

            if ($this->pays !== 'FR') {
                return false;
            }

            if ($this->assujettitva !== 0) {
                return false;
            }

        // otherwise, everything was equal, so return TRUE
        return true;
    } // hasOnlyDefaultValues()

    /**
     * Hydrates (populates) the object variables with values from the database resultset.
     *
     * An offset (0-based "start column") is specified so that objects can be hydrated
     * with a subset of the columns in the resultset rows.  This is needed, for example,
     * for results of JOIN queries where the resultset row includes columns from two or
     * more tables.
     *
     * @param array   $row       The row returned by DataFetcher->fetch().
     * @param int     $startcol  0-based offset column which indicates which restultset column to start with.
     * @param boolean $rehydrate Whether this object is being re-hydrated from the database.
     * @param string  $indexType The index type of $row. Mostly DataFetcher->getIndexType().
                                  One of the class type constants TableMap::TYPE_PHPNAME, TableMap::TYPE_CAMELNAME
     *                            TableMap::TYPE_COLNAME, TableMap::TYPE_FIELDNAME, TableMap::TYPE_NUM.
     *
     * @return int             next starting column
     * @throws PropelException - Any caught Exception will be rewrapped as a PropelException.
     */
    public function hydrate($row, $startcol = 0, $rehydrate = false, $indexType = TableMap::TYPE_NUM)
    {
        try {

            $col = $row[TableMap::TYPE_NUM == $indexType ? 0 + $startcol : EntiteTableMap::translateFieldName('IdEntite', TableMap::TYPE_PHPNAME, $indexType)];
            $this->id_entite = (null !== $col) ? (string) $col : null;

            $col = $row[TableMap::TYPE_NUM == $indexType ? 1 + $startcol : EntiteTableMap::translateFieldName('Nom', TableMap::TYPE_PHPNAME, $indexType)];
            $this->nom = (null !== $col) ? (string) $col : null;

            $col = $row[TableMap::TYPE_NUM == $indexType ? 2 + $startcol : EntiteTableMap::translateFieldName('Nomcourt', TableMap::TYPE_PHPNAME, $indexType)];
            $this->nomcourt = (null !== $col) ? (string) $col : null;

            $col = $row[TableMap::TYPE_NUM == $indexType ? 3 + $startcol : EntiteTableMap::translateFieldName('Adresse', TableMap::TYPE_PHPNAME, $indexType)];
            $this->adresse = (null !== $col) ? (string) $col : null;

            $col = $row[TableMap::TYPE_NUM == $indexType ? 4 + $startcol : EntiteTableMap::translateFieldName('Codepostal', TableMap::TYPE_PHPNAME, $indexType)];
            $this->codepostal = (null !== $col) ? (string) $col : null;

            $col = $row[TableMap::TYPE_NUM == $indexType ? 5 + $startcol : EntiteTableMap::translateFieldName('Ville', TableMap::TYPE_PHPNAME, $indexType)];
            $this->ville = (null !== $col) ? (string) $col : null;

            $col = $row[TableMap::TYPE_NUM == $indexType ? 6 + $startcol : EntiteTableMap::translateFieldName('Pays', TableMap::TYPE_PHPNAME, $indexType)];
            $this->pays = (null !== $col) ? (string) $col : null;

            $col = $row[TableMap::TYPE_NUM == $indexType ? 7 + $startcol : EntiteTableMap::translateFieldName('Telephone', TableMap::TYPE_PHPNAME, $indexType)];
            $this->telephone = (null !== $col) ? (string) $col : null;

            $col = $row[TableMap::TYPE_NUM == $indexType ? 8 + $startcol : EntiteTableMap::translateFieldName('Fax', TableMap::TYPE_PHPNAME, $indexType)];
            $this->fax = (null !== $col) ? (string) $col : null;

            $col = $row[TableMap::TYPE_NUM == $indexType ? 9 + $startcol : EntiteTableMap::translateFieldName('Url', TableMap::TYPE_PHPNAME, $indexType)];
            $this->url = (null !== $col) ? (string) $col : null;

            $col = $row[TableMap::TYPE_NUM == $indexType ? 10 + $startcol : EntiteTableMap::translateFieldName('Assujettitva', TableMap::TYPE_PHPNAME, $indexType)];
            $this->assujettitva = (null !== $col) ? (int) $col : null;

            $col = $row[TableMap::TYPE_NUM == $indexType ? 11 + $startcol : EntiteTableMap::translateFieldName('Email', TableMap::TYPE_PHPNAME, $indexType)];
            $this->email = (null !== $col) ? (string) $col : null;

            $col = $row[TableMap::TYPE_NUM == $indexType ? 12 + $startcol : EntiteTableMap::translateFieldName('CreatedAt', TableMap::TYPE_PHPNAME, $indexType)];
            if ($col === '0000-00-00 00:00:00') {
                $col = null;
            }
            $this->created_at = (null !== $col) ? PropelDateTime::newInstance($col, null, 'DateTime') : null;

            $col = $row[TableMap::TYPE_NUM == $indexType ? 13 + $startcol : EntiteTableMap::translateFieldName('UpdatedAt', TableMap::TYPE_PHPNAME, $indexType)];
            if ($col === '0000-00-00 00:00:00') {
                $col = null;
            }
            $this->updated_at = (null !== $col) ? PropelDateTime::newInstance($col, null, 'DateTime') : null;
            $this->resetModified();

            $this->setNew(false);

            if ($rehydrate) {
                $this->ensureConsistency();
            }

            return $startcol + 14; // 14 = EntiteTableMap::NUM_HYDRATE_COLUMNS.

        } catch (Exception $e) {
            throw new PropelException(sprintf('Error populating %s object', '\\Entite'), 0, $e);
        }
    }

    /**
     * Checks and repairs the internal consistency of the object.
     *
     * This method is executed after an already-instantiated object is re-hydrated
     * from the database.  It exists to check any foreign keys to make sure that
     * the objects related to the current object are correct based on foreign key.
     *
     * You can override this method in the stub class, but you should always invoke
     * the base method from the overridden method (i.e. parent::ensureConsistency()),
     * in case your model changes.
     *
     * @throws PropelException
     */
    public function ensureConsistency()
    {
    } // ensureConsistency

    /**
     * Reloads this object from datastore based on primary key and (optionally) resets all associated objects.
     *
     * This will only work if the object has been saved and has a valid primary key set.
     *
     * @param      boolean $deep (optional) Whether to also de-associated any related objects.
     * @param      ConnectionInterface $con (optional) The ConnectionInterface connection to use.
     * @return void
     * @throws PropelException - if this object is deleted, unsaved or doesn't have pk match in db
     */
    public function reload($deep = false, ConnectionInterface $con = null)
    {
        if ($this->isDeleted()) {
            throw new PropelException("Cannot reload a deleted object.");
        }

        if ($this->isNew()) {
            throw new PropelException("Cannot reload an unsaved object.");
        }

        if ($con === null) {
            $con = Propel::getServiceContainer()->getReadConnection(EntiteTableMap::DATABASE_NAME);
        }

        // We don't need to alter the object instance pool; we're just modifying this instance
        // already in the pool.

        $dataFetcher = ChildEntiteQuery::create(null, $this->buildPkeyCriteria())->setFormatter(ModelCriteria::FORMAT_STATEMENT)->find($con);
        $row = $dataFetcher->fetch();
        $dataFetcher->close();
        if (!$row) {
            throw new PropelException('Cannot find matching row in the database to reload object values.');
        }
        $this->hydrate($row, 0, true, $dataFetcher->getIndexType()); // rehydrate

        if ($deep) {  // also de-associate any related objects?

            $this->collActivites = null;

            $this->collComptes = null;

            $this->collEcritures = null;

            $this->collJournals = null;

            $this->collPieces = null;

            $this->collAutorisations = null;

            $this->collPaiements = null;

            $this->collPrestations = null;

            $this->collPrestationslots = null;

            $this->collServicerendus = null;

            $this->collTresors = null;

            $this->collTvas = null;

            $this->collCourriers = null;

        } // if (deep)
    }

    /**
     * Removes this object from datastore and sets delete attribute.
     *
     * @param      ConnectionInterface $con
     * @return void
     * @throws PropelException
     * @see Entite::setDeleted()
     * @see Entite::isDeleted()
     */
    public function delete(ConnectionInterface $con = null)
    {
        if ($this->isDeleted()) {
            throw new PropelException("This object has already been deleted.");
        }

        if ($con === null) {
            $con = Propel::getServiceContainer()->getWriteConnection(EntiteTableMap::DATABASE_NAME);
        }

        $con->transaction(function () use ($con) {
            $deleteQuery = ChildEntiteQuery::create()
                ->filterByPrimaryKey($this->getPrimaryKey());
            $ret = $this->preDelete($con);
            // archivable behavior
            if ($ret) {
                if ($this->archiveOnDelete) {
                    // do nothing yet. The object will be archived later when calling ChildEntiteQuery::delete().
                } else {
                    $deleteQuery->setArchiveOnDelete(false);
                    $this->archiveOnDelete = true;
                }
            }

            if ($ret) {
                $deleteQuery->delete($con);
                $this->postDelete($con);
                $this->setDeleted(true);
            }
        });
    }

    /**
     * Persists this object to the database.
     *
     * If the object is new, it inserts it; otherwise an update is performed.
     * All modified related objects will also be persisted in the doSave()
     * method.  This method wraps all precipitate database operations in a
     * single transaction.
     *
     * @param      ConnectionInterface $con
     * @return int             The number of rows affected by this insert/update and any referring fk objects' save() operations.
     * @throws PropelException
     * @see doSave()
     */
    public function save(ConnectionInterface $con = null)
    {
        if ($this->isDeleted()) {
            throw new PropelException("You cannot save an object that has been deleted.");
        }

        if ($this->alreadyInSave) {
            return 0;
        }

        if ($con === null) {
            $con = Propel::getServiceContainer()->getWriteConnection(EntiteTableMap::DATABASE_NAME);
        }

        return $con->transaction(function () use ($con) {
            $ret = $this->preSave($con);
            $isInsert = $this->isNew();
            if ($isInsert) {
                $ret = $ret && $this->preInsert($con);
                // timestampable behavior

                if (!$this->isColumnModified(EntiteTableMap::COL_CREATED_AT)) {
                    $this->setCreatedAt(\Propel\Runtime\Util\PropelDateTime::createHighPrecision());
                }
                if (!$this->isColumnModified(EntiteTableMap::COL_UPDATED_AT)) {
                    $this->setUpdatedAt(\Propel\Runtime\Util\PropelDateTime::createHighPrecision());
                }
            } else {
                $ret = $ret && $this->preUpdate($con);
                // timestampable behavior
                if ($this->isModified() && !$this->isColumnModified(EntiteTableMap::COL_UPDATED_AT)) {
                    $this->setUpdatedAt(\Propel\Runtime\Util\PropelDateTime::createHighPrecision());
                }
            }
            if ($ret) {
                $affectedRows = $this->doSave($con);
                if ($isInsert) {
                    $this->postInsert($con);
                } else {
                    $this->postUpdate($con);
                }
                $this->postSave($con);
                EntiteTableMap::addInstanceToPool($this);
            } else {
                $affectedRows = 0;
            }

            return $affectedRows;
        });
    }

    /**
     * Performs the work of inserting or updating the row in the database.
     *
     * If the object is new, it inserts it; otherwise an update is performed.
     * All related objects are also updated in this method.
     *
     * @param      ConnectionInterface $con
     * @return int             The number of rows affected by this insert/update and any referring fk objects' save() operations.
     * @throws PropelException
     * @see save()
     */
    protected function doSave(ConnectionInterface $con)
    {
        $affectedRows = 0; // initialize var to track total num of affected rows
        if (!$this->alreadyInSave) {
            $this->alreadyInSave = true;

            if ($this->isNew() || $this->isModified()) {
                // persist changes
                if ($this->isNew()) {
                    $this->doInsert($con);
                    $affectedRows += 1;
                } else {
                    $affectedRows += $this->doUpdate($con);
                }
                $this->resetModified();
            }

            if ($this->activitesScheduledForDeletion !== null) {
                if (!$this->activitesScheduledForDeletion->isEmpty()) {
                    \ActiviteQuery::create()
                        ->filterByPrimaryKeys($this->activitesScheduledForDeletion->getPrimaryKeys(false))
                        ->delete($con);
                    $this->activitesScheduledForDeletion = null;
                }
            }

            if ($this->collActivites !== null) {
                foreach ($this->collActivites as $referrerFK) {
                    if (!$referrerFK->isDeleted() && ($referrerFK->isNew() || $referrerFK->isModified())) {
                        $affectedRows += $referrerFK->save($con);
                    }
                }
            }

            if ($this->comptesScheduledForDeletion !== null) {
                if (!$this->comptesScheduledForDeletion->isEmpty()) {
                    \CompteQuery::create()
                        ->filterByPrimaryKeys($this->comptesScheduledForDeletion->getPrimaryKeys(false))
                        ->delete($con);
                    $this->comptesScheduledForDeletion = null;
                }
            }

            if ($this->collComptes !== null) {
                foreach ($this->collComptes as $referrerFK) {
                    if (!$referrerFK->isDeleted() && ($referrerFK->isNew() || $referrerFK->isModified())) {
                        $affectedRows += $referrerFK->save($con);
                    }
                }
            }

            if ($this->ecrituresScheduledForDeletion !== null) {
                if (!$this->ecrituresScheduledForDeletion->isEmpty()) {
                    \EcritureQuery::create()
                        ->filterByPrimaryKeys($this->ecrituresScheduledForDeletion->getPrimaryKeys(false))
                        ->delete($con);
                    $this->ecrituresScheduledForDeletion = null;
                }
            }

            if ($this->collEcritures !== null) {
                foreach ($this->collEcritures as $referrerFK) {
                    if (!$referrerFK->isDeleted() && ($referrerFK->isNew() || $referrerFK->isModified())) {
                        $affectedRows += $referrerFK->save($con);
                    }
                }
            }

            if ($this->journalsScheduledForDeletion !== null) {
                if (!$this->journalsScheduledForDeletion->isEmpty()) {
                    \JournalQuery::create()
                        ->filterByPrimaryKeys($this->journalsScheduledForDeletion->getPrimaryKeys(false))
                        ->delete($con);
                    $this->journalsScheduledForDeletion = null;
                }
            }

            if ($this->collJournals !== null) {
                foreach ($this->collJournals as $referrerFK) {
                    if (!$referrerFK->isDeleted() && ($referrerFK->isNew() || $referrerFK->isModified())) {
                        $affectedRows += $referrerFK->save($con);
                    }
                }
            }

            if ($this->piecesScheduledForDeletion !== null) {
                if (!$this->piecesScheduledForDeletion->isEmpty()) {
                    \PieceQuery::create()
                        ->filterByPrimaryKeys($this->piecesScheduledForDeletion->getPrimaryKeys(false))
                        ->delete($con);
                    $this->piecesScheduledForDeletion = null;
                }
            }

            if ($this->collPieces !== null) {
                foreach ($this->collPieces as $referrerFK) {
                    if (!$referrerFK->isDeleted() && ($referrerFK->isNew() || $referrerFK->isModified())) {
                        $affectedRows += $referrerFK->save($con);
                    }
                }
            }

            if ($this->autorisationsScheduledForDeletion !== null) {
                if (!$this->autorisationsScheduledForDeletion->isEmpty()) {
                    \AutorisationQuery::create()
                        ->filterByPrimaryKeys($this->autorisationsScheduledForDeletion->getPrimaryKeys(false))
                        ->delete($con);
                    $this->autorisationsScheduledForDeletion = null;
                }
            }

            if ($this->collAutorisations !== null) {
                foreach ($this->collAutorisations as $referrerFK) {
                    if (!$referrerFK->isDeleted() && ($referrerFK->isNew() || $referrerFK->isModified())) {
                        $affectedRows += $referrerFK->save($con);
                    }
                }
            }

            if ($this->paiementsScheduledForDeletion !== null) {
                if (!$this->paiementsScheduledForDeletion->isEmpty()) {
                    \PaiementQuery::create()
                        ->filterByPrimaryKeys($this->paiementsScheduledForDeletion->getPrimaryKeys(false))
                        ->delete($con);
                    $this->paiementsScheduledForDeletion = null;
                }
            }

            if ($this->collPaiements !== null) {
                foreach ($this->collPaiements as $referrerFK) {
                    if (!$referrerFK->isDeleted() && ($referrerFK->isNew() || $referrerFK->isModified())) {
                        $affectedRows += $referrerFK->save($con);
                    }
                }
            }

            if ($this->prestationsScheduledForDeletion !== null) {
                if (!$this->prestationsScheduledForDeletion->isEmpty()) {
                    \PrestationQuery::create()
                        ->filterByPrimaryKeys($this->prestationsScheduledForDeletion->getPrimaryKeys(false))
                        ->delete($con);
                    $this->prestationsScheduledForDeletion = null;
                }
            }

            if ($this->collPrestations !== null) {
                foreach ($this->collPrestations as $referrerFK) {
                    if (!$referrerFK->isDeleted() && ($referrerFK->isNew() || $referrerFK->isModified())) {
                        $affectedRows += $referrerFK->save($con);
                    }
                }
            }

            if ($this->prestationslotsScheduledForDeletion !== null) {
                if (!$this->prestationslotsScheduledForDeletion->isEmpty()) {
                    \PrestationslotQuery::create()
                        ->filterByPrimaryKeys($this->prestationslotsScheduledForDeletion->getPrimaryKeys(false))
                        ->delete($con);
                    $this->prestationslotsScheduledForDeletion = null;
                }
            }

            if ($this->collPrestationslots !== null) {
                foreach ($this->collPrestationslots as $referrerFK) {
                    if (!$referrerFK->isDeleted() && ($referrerFK->isNew() || $referrerFK->isModified())) {
                        $affectedRows += $referrerFK->save($con);
                    }
                }
            }

            if ($this->servicerendusScheduledForDeletion !== null) {
                if (!$this->servicerendusScheduledForDeletion->isEmpty()) {
                    \ServicerenduQuery::create()
                        ->filterByPrimaryKeys($this->servicerendusScheduledForDeletion->getPrimaryKeys(false))
                        ->delete($con);
                    $this->servicerendusScheduledForDeletion = null;
                }
            }

            if ($this->collServicerendus !== null) {
                foreach ($this->collServicerendus as $referrerFK) {
                    if (!$referrerFK->isDeleted() && ($referrerFK->isNew() || $referrerFK->isModified())) {
                        $affectedRows += $referrerFK->save($con);
                    }
                }
            }

            if ($this->tresorsScheduledForDeletion !== null) {
                if (!$this->tresorsScheduledForDeletion->isEmpty()) {
                    \TresorQuery::create()
                        ->filterByPrimaryKeys($this->tresorsScheduledForDeletion->getPrimaryKeys(false))
                        ->delete($con);
                    $this->tresorsScheduledForDeletion = null;
                }
            }

            if ($this->collTresors !== null) {
                foreach ($this->collTresors as $referrerFK) {
                    if (!$referrerFK->isDeleted() && ($referrerFK->isNew() || $referrerFK->isModified())) {
                        $affectedRows += $referrerFK->save($con);
                    }
                }
            }

            if ($this->tvasScheduledForDeletion !== null) {
                if (!$this->tvasScheduledForDeletion->isEmpty()) {
                    \TvaQuery::create()
                        ->filterByPrimaryKeys($this->tvasScheduledForDeletion->getPrimaryKeys(false))
                        ->delete($con);
                    $this->tvasScheduledForDeletion = null;
                }
            }

            if ($this->collTvas !== null) {
                foreach ($this->collTvas as $referrerFK) {
                    if (!$referrerFK->isDeleted() && ($referrerFK->isNew() || $referrerFK->isModified())) {
                        $affectedRows += $referrerFK->save($con);
                    }
                }
            }

            if ($this->courriersScheduledForDeletion !== null) {
                if (!$this->courriersScheduledForDeletion->isEmpty()) {
                    \CourrierQuery::create()
                        ->filterByPrimaryKeys($this->courriersScheduledForDeletion->getPrimaryKeys(false))
                        ->delete($con);
                    $this->courriersScheduledForDeletion = null;
                }
            }

            if ($this->collCourriers !== null) {
                foreach ($this->collCourriers as $referrerFK) {
                    if (!$referrerFK->isDeleted() && ($referrerFK->isNew() || $referrerFK->isModified())) {
                        $affectedRows += $referrerFK->save($con);
                    }
                }
            }

            $this->alreadyInSave = false;

        }

        return $affectedRows;
    } // doSave()

    /**
     * Insert the row in the database.
     *
     * @param      ConnectionInterface $con
     *
     * @throws PropelException
     * @see doSave()
     */
    protected function doInsert(ConnectionInterface $con)
    {
        $modifiedColumns = array();
        $index = 0;

        $this->modifiedColumns[EntiteTableMap::COL_ID_ENTITE] = true;
        if (null !== $this->id_entite) {
            throw new PropelException('Cannot insert a value for auto-increment primary key (' . EntiteTableMap::COL_ID_ENTITE . ')');
        }

         // check the columns in natural order for more readable SQL queries
        if ($this->isColumnModified(EntiteTableMap::COL_ID_ENTITE)) {
            $modifiedColumns[':p' . $index++]  = '`id_entite`';
        }
        if ($this->isColumnModified(EntiteTableMap::COL_NOM)) {
            $modifiedColumns[':p' . $index++]  = '`nom`';
        }
        if ($this->isColumnModified(EntiteTableMap::COL_NOMCOURT)) {
            $modifiedColumns[':p' . $index++]  = '`nomcourt`';
        }
        if ($this->isColumnModified(EntiteTableMap::COL_ADRESSE)) {
            $modifiedColumns[':p' . $index++]  = '`adresse`';
        }
        if ($this->isColumnModified(EntiteTableMap::COL_CODEPOSTAL)) {
            $modifiedColumns[':p' . $index++]  = '`codepostal`';
        }
        if ($this->isColumnModified(EntiteTableMap::COL_VILLE)) {
            $modifiedColumns[':p' . $index++]  = '`ville`';
        }
        if ($this->isColumnModified(EntiteTableMap::COL_PAYS)) {
            $modifiedColumns[':p' . $index++]  = '`pays`';
        }
        if ($this->isColumnModified(EntiteTableMap::COL_TELEPHONE)) {
            $modifiedColumns[':p' . $index++]  = '`telephone`';
        }
        if ($this->isColumnModified(EntiteTableMap::COL_FAX)) {
            $modifiedColumns[':p' . $index++]  = '`fax`';
        }
        if ($this->isColumnModified(EntiteTableMap::COL_URL)) {
            $modifiedColumns[':p' . $index++]  = '`url`';
        }
        if ($this->isColumnModified(EntiteTableMap::COL_ASSUJETTITVA)) {
            $modifiedColumns[':p' . $index++]  = '`assujettitva`';
        }
        if ($this->isColumnModified(EntiteTableMap::COL_EMAIL)) {
            $modifiedColumns[':p' . $index++]  = '`email`';
        }
        if ($this->isColumnModified(EntiteTableMap::COL_CREATED_AT)) {
            $modifiedColumns[':p' . $index++]  = '`created_at`';
        }
        if ($this->isColumnModified(EntiteTableMap::COL_UPDATED_AT)) {
            $modifiedColumns[':p' . $index++]  = '`updated_at`';
        }

        $sql = sprintf(
            'INSERT INTO `asso_entites` (%s) VALUES (%s)',
            implode(', ', $modifiedColumns),
            implode(', ', array_keys($modifiedColumns))
        );

        try {
            $stmt = $con->prepare($sql);
            foreach ($modifiedColumns as $identifier => $columnName) {
                switch ($columnName) {
                    case '`id_entite`':
                        $stmt->bindValue($identifier, $this->id_entite, PDO::PARAM_INT);
                        break;
                    case '`nom`':
                        $stmt->bindValue($identifier, $this->nom, PDO::PARAM_STR);
                        break;
                    case '`nomcourt`':
                        $stmt->bindValue($identifier, $this->nomcourt, PDO::PARAM_STR);
                        break;
                    case '`adresse`':
                        $stmt->bindValue($identifier, $this->adresse, PDO::PARAM_STR);
                        break;
                    case '`codepostal`':
                        $stmt->bindValue($identifier, $this->codepostal, PDO::PARAM_STR);
                        break;
                    case '`ville`':
                        $stmt->bindValue($identifier, $this->ville, PDO::PARAM_STR);
                        break;
                    case '`pays`':
                        $stmt->bindValue($identifier, $this->pays, PDO::PARAM_STR);
                        break;
                    case '`telephone`':
                        $stmt->bindValue($identifier, $this->telephone, PDO::PARAM_STR);
                        break;
                    case '`fax`':
                        $stmt->bindValue($identifier, $this->fax, PDO::PARAM_STR);
                        break;
                    case '`url`':
                        $stmt->bindValue($identifier, $this->url, PDO::PARAM_STR);
                        break;
                    case '`assujettitva`':
                        $stmt->bindValue($identifier, $this->assujettitva, PDO::PARAM_INT);
                        break;
                    case '`email`':
                        $stmt->bindValue($identifier, $this->email, PDO::PARAM_STR);
                        break;
                    case '`created_at`':
                        $stmt->bindValue($identifier, $this->created_at ? $this->created_at->format("Y-m-d H:i:s.u") : null, PDO::PARAM_STR);
                        break;
                    case '`updated_at`':
                        $stmt->bindValue($identifier, $this->updated_at ? $this->updated_at->format("Y-m-d H:i:s.u") : null, PDO::PARAM_STR);
                        break;
                }
            }
            $stmt->execute();
        } catch (Exception $e) {
            Propel::log($e->getMessage(), Propel::LOG_ERR);
            throw new PropelException(sprintf('Unable to execute INSERT statement [%s]', $sql), 0, $e);
        }

        try {
            $pk = $con->lastInsertId();
        } catch (Exception $e) {
            throw new PropelException('Unable to get autoincrement id.', 0, $e);
        }
        $this->setIdEntite($pk);

        $this->setNew(false);
    }

    /**
     * Update the row in the database.
     *
     * @param      ConnectionInterface $con
     *
     * @return Integer Number of updated rows
     * @see doSave()
     */
    protected function doUpdate(ConnectionInterface $con)
    {
        $selectCriteria = $this->buildPkeyCriteria();
        $valuesCriteria = $this->buildCriteria();

        return $selectCriteria->doUpdate($valuesCriteria, $con);
    }

    /**
     * Retrieves a field from the object by name passed in as a string.
     *
     * @param      string $name name
     * @param      string $type The type of fieldname the $name is of:
     *                     one of the class type constants TableMap::TYPE_PHPNAME, TableMap::TYPE_CAMELNAME
     *                     TableMap::TYPE_COLNAME, TableMap::TYPE_FIELDNAME, TableMap::TYPE_NUM.
     *                     Defaults to TableMap::TYPE_FIELDNAME.
     * @return mixed Value of field.
     */
    public function getByName($name, $type = TableMap::TYPE_FIELDNAME)
    {
        $pos = EntiteTableMap::translateFieldName($name, $type, TableMap::TYPE_NUM);
        $field = $this->getByPosition($pos);

        return $field;
    }

    /**
     * Retrieves a field from the object by Position as specified in the xml schema.
     * Zero-based.
     *
     * @param      int $pos position in xml schema
     * @return mixed Value of field at $pos
     */
    public function getByPosition($pos)
    {
        switch ($pos) {
            case 0:
                return $this->getIdEntite();
                break;
            case 1:
                return $this->getNom();
                break;
            case 2:
                return $this->getNomcourt();
                break;
            case 3:
                return $this->getAdresse();
                break;
            case 4:
                return $this->getCodepostal();
                break;
            case 5:
                return $this->getVille();
                break;
            case 6:
                return $this->getPays();
                break;
            case 7:
                return $this->getTelephone();
                break;
            case 8:
                return $this->getFax();
                break;
            case 9:
                return $this->getUrl();
                break;
            case 10:
                return $this->getAssujettitva();
                break;
            case 11:
                return $this->getEmail();
                break;
            case 12:
                return $this->getCreatedAt();
                break;
            case 13:
                return $this->getUpdatedAt();
                break;
            default:
                return null;
                break;
        } // switch()
    }

    /**
     * Exports the object as an array.
     *
     * You can specify the key type of the array by passing one of the class
     * type constants.
     *
     * @param     string  $keyType (optional) One of the class type constants TableMap::TYPE_PHPNAME, TableMap::TYPE_CAMELNAME,
     *                    TableMap::TYPE_COLNAME, TableMap::TYPE_FIELDNAME, TableMap::TYPE_NUM.
     *                    Defaults to TableMap::TYPE_FIELDNAME.
     * @param     boolean $includeLazyLoadColumns (optional) Whether to include lazy loaded columns. Defaults to TRUE.
     * @param     array $alreadyDumpedObjects List of objects to skip to avoid recursion
     * @param     boolean $includeForeignObjects (optional) Whether to include hydrated related objects. Default to FALSE.
     *
     * @return array an associative array containing the field names (as keys) and field values
     */
    public function toArray($keyType = TableMap::TYPE_FIELDNAME, $includeLazyLoadColumns = true, $alreadyDumpedObjects = array(), $includeForeignObjects = false)
    {

        if (isset($alreadyDumpedObjects['Entite'][$this->hashCode()])) {
            return '*RECURSION*';
        }
        $alreadyDumpedObjects['Entite'][$this->hashCode()] = true;
        $keys = EntiteTableMap::getFieldNames($keyType);
        $result = array(
            $keys[0] => $this->getIdEntite(),
            $keys[1] => $this->getNom(),
            $keys[2] => $this->getNomcourt(),
            $keys[3] => $this->getAdresse(),
            $keys[4] => $this->getCodepostal(),
            $keys[5] => $this->getVille(),
            $keys[6] => $this->getPays(),
            $keys[7] => $this->getTelephone(),
            $keys[8] => $this->getFax(),
            $keys[9] => $this->getUrl(),
            $keys[10] => $this->getAssujettitva(),
            $keys[11] => $this->getEmail(),
            $keys[12] => $this->getCreatedAt(),
            $keys[13] => $this->getUpdatedAt(),
        );
        if ($result[$keys[12]] instanceof \DateTimeInterface) {
            $result[$keys[12]] = $result[$keys[12]]->format('c');
        }

        if ($result[$keys[13]] instanceof \DateTimeInterface) {
            $result[$keys[13]] = $result[$keys[13]]->format('c');
        }

        $virtualColumns = $this->virtualColumns;
        foreach ($virtualColumns as $key => $virtualColumn) {
            $result[$key] = $virtualColumn;
        }

        if ($includeForeignObjects) {
            if (null !== $this->collActivites) {

                switch ($keyType) {
                    case TableMap::TYPE_CAMELNAME:
                        $key = 'activites';
                        break;
                    case TableMap::TYPE_FIELDNAME:
                        $key = 'assc_activitess';
                        break;
                    default:
                        $key = 'Activites';
                }

                $result[$key] = $this->collActivites->toArray(null, false, $keyType, $includeLazyLoadColumns, $alreadyDumpedObjects);
            }
            if (null !== $this->collComptes) {

                switch ($keyType) {
                    case TableMap::TYPE_CAMELNAME:
                        $key = 'comptes';
                        break;
                    case TableMap::TYPE_FIELDNAME:
                        $key = 'assc_comptess';
                        break;
                    default:
                        $key = 'Comptes';
                }

                $result[$key] = $this->collComptes->toArray(null, false, $keyType, $includeLazyLoadColumns, $alreadyDumpedObjects);
            }
            if (null !== $this->collEcritures) {

                switch ($keyType) {
                    case TableMap::TYPE_CAMELNAME:
                        $key = 'ecritures';
                        break;
                    case TableMap::TYPE_FIELDNAME:
                        $key = 'assc_ecrituress';
                        break;
                    default:
                        $key = 'Ecritures';
                }

                $result[$key] = $this->collEcritures->toArray(null, false, $keyType, $includeLazyLoadColumns, $alreadyDumpedObjects);
            }
            if (null !== $this->collJournals) {

                switch ($keyType) {
                    case TableMap::TYPE_CAMELNAME:
                        $key = 'journals';
                        break;
                    case TableMap::TYPE_FIELDNAME:
                        $key = 'assc_journalss';
                        break;
                    default:
                        $key = 'Journals';
                }

                $result[$key] = $this->collJournals->toArray(null, false, $keyType, $includeLazyLoadColumns, $alreadyDumpedObjects);
            }
            if (null !== $this->collPieces) {

                switch ($keyType) {
                    case TableMap::TYPE_CAMELNAME:
                        $key = 'pieces';
                        break;
                    case TableMap::TYPE_FIELDNAME:
                        $key = 'assc_piecess';
                        break;
                    default:
                        $key = 'Pieces';
                }

                $result[$key] = $this->collPieces->toArray(null, false, $keyType, $includeLazyLoadColumns, $alreadyDumpedObjects);
            }
            if (null !== $this->collAutorisations) {

                switch ($keyType) {
                    case TableMap::TYPE_CAMELNAME:
                        $key = 'autorisations';
                        break;
                    case TableMap::TYPE_FIELDNAME:
                        $key = 'asso_autorisationss';
                        break;
                    default:
                        $key = 'Autorisations';
                }

                $result[$key] = $this->collAutorisations->toArray(null, false, $keyType, $includeLazyLoadColumns, $alreadyDumpedObjects);
            }
            if (null !== $this->collPaiements) {

                switch ($keyType) {
                    case TableMap::TYPE_CAMELNAME:
                        $key = 'paiements';
                        break;
                    case TableMap::TYPE_FIELDNAME:
                        $key = 'asso_paiementss';
                        break;
                    default:
                        $key = 'Paiements';
                }

                $result[$key] = $this->collPaiements->toArray(null, false, $keyType, $includeLazyLoadColumns, $alreadyDumpedObjects);
            }
            if (null !== $this->collPrestations) {

                switch ($keyType) {
                    case TableMap::TYPE_CAMELNAME:
                        $key = 'prestations';
                        break;
                    case TableMap::TYPE_FIELDNAME:
                        $key = 'asso_prestationss';
                        break;
                    default:
                        $key = 'Prestations';
                }

                $result[$key] = $this->collPrestations->toArray(null, false, $keyType, $includeLazyLoadColumns, $alreadyDumpedObjects);
            }
            if (null !== $this->collPrestationslots) {

                switch ($keyType) {
                    case TableMap::TYPE_CAMELNAME:
                        $key = 'prestationslots';
                        break;
                    case TableMap::TYPE_FIELDNAME:
                        $key = 'asso_prestationslotss';
                        break;
                    default:
                        $key = 'Prestationslots';
                }

                $result[$key] = $this->collPrestationslots->toArray(null, false, $keyType, $includeLazyLoadColumns, $alreadyDumpedObjects);
            }
            if (null !== $this->collServicerendus) {

                switch ($keyType) {
                    case TableMap::TYPE_CAMELNAME:
                        $key = 'servicerendus';
                        break;
                    case TableMap::TYPE_FIELDNAME:
                        $key = 'asso_servicerenduses';
                        break;
                    default:
                        $key = 'Servicerendus';
                }

                $result[$key] = $this->collServicerendus->toArray(null, false, $keyType, $includeLazyLoadColumns, $alreadyDumpedObjects);
            }
            if (null !== $this->collTresors) {

                switch ($keyType) {
                    case TableMap::TYPE_CAMELNAME:
                        $key = 'tresors';
                        break;
                    case TableMap::TYPE_FIELDNAME:
                        $key = 'asso_tresorss';
                        break;
                    default:
                        $key = 'Tresors';
                }

                $result[$key] = $this->collTresors->toArray(null, false, $keyType, $includeLazyLoadColumns, $alreadyDumpedObjects);
            }
            if (null !== $this->collTvas) {

                switch ($keyType) {
                    case TableMap::TYPE_CAMELNAME:
                        $key = 'tvas';
                        break;
                    case TableMap::TYPE_FIELDNAME:
                        $key = 'asso_tvass';
                        break;
                    default:
                        $key = 'Tvas';
                }

                $result[$key] = $this->collTvas->toArray(null, false, $keyType, $includeLazyLoadColumns, $alreadyDumpedObjects);
            }
            if (null !== $this->collCourriers) {

                switch ($keyType) {
                    case TableMap::TYPE_CAMELNAME:
                        $key = 'courriers';
                        break;
                    case TableMap::TYPE_FIELDNAME:
                        $key = 'com_courrierss';
                        break;
                    default:
                        $key = 'Courriers';
                }

                $result[$key] = $this->collCourriers->toArray(null, false, $keyType, $includeLazyLoadColumns, $alreadyDumpedObjects);
            }
        }

        return $result;
    }

    /**
     * Sets a field from the object by name passed in as a string.
     *
     * @param  string $name
     * @param  mixed  $value field value
     * @param  string $type The type of fieldname the $name is of:
     *                one of the class type constants TableMap::TYPE_PHPNAME, TableMap::TYPE_CAMELNAME
     *                TableMap::TYPE_COLNAME, TableMap::TYPE_FIELDNAME, TableMap::TYPE_NUM.
     *                Defaults to TableMap::TYPE_FIELDNAME.
     * @return $this|\Entite
     */
    public function setByName($name, $value, $type = TableMap::TYPE_FIELDNAME)
    {
        $pos = EntiteTableMap::translateFieldName($name, $type, TableMap::TYPE_NUM);

        return $this->setByPosition($pos, $value);
    }

    /**
     * Sets a field from the object by Position as specified in the xml schema.
     * Zero-based.
     *
     * @param  int $pos position in xml schema
     * @param  mixed $value field value
     * @return $this|\Entite
     */
    public function setByPosition($pos, $value)
    {
        switch ($pos) {
            case 0:
                $this->setIdEntite($value);
                break;
            case 1:
                $this->setNom($value);
                break;
            case 2:
                $this->setNomcourt($value);
                break;
            case 3:
                $this->setAdresse($value);
                break;
            case 4:
                $this->setCodepostal($value);
                break;
            case 5:
                $this->setVille($value);
                break;
            case 6:
                $this->setPays($value);
                break;
            case 7:
                $this->setTelephone($value);
                break;
            case 8:
                $this->setFax($value);
                break;
            case 9:
                $this->setUrl($value);
                break;
            case 10:
                $this->setAssujettitva($value);
                break;
            case 11:
                $this->setEmail($value);
                break;
            case 12:
                $this->setCreatedAt($value);
                break;
            case 13:
                $this->setUpdatedAt($value);
                break;
        } // switch()

        return $this;
    }

    /**
     * Populates the object using an array.
     *
     * This is particularly useful when populating an object from one of the
     * request arrays (e.g. $_POST).  This method goes through the column
     * names, checking to see whether a matching key exists in populated
     * array. If so the setByName() method is called for that column.
     *
     * You can specify the key type of the array by additionally passing one
     * of the class type constants TableMap::TYPE_PHPNAME, TableMap::TYPE_CAMELNAME,
     * TableMap::TYPE_COLNAME, TableMap::TYPE_FIELDNAME, TableMap::TYPE_NUM.
     * The default key type is the column's TableMap::TYPE_FIELDNAME.
     *
     * @param      array  $arr     An array to populate the object from.
     * @param      string $keyType The type of keys the array uses.
     * @return void
     */
    public function fromArray($arr, $keyType = TableMap::TYPE_FIELDNAME)
    {
        $keys = EntiteTableMap::getFieldNames($keyType);

        if (array_key_exists($keys[0], $arr)) {
            $this->setIdEntite($arr[$keys[0]]);
        }
        if (array_key_exists($keys[1], $arr)) {
            $this->setNom($arr[$keys[1]]);
        }
        if (array_key_exists($keys[2], $arr)) {
            $this->setNomcourt($arr[$keys[2]]);
        }
        if (array_key_exists($keys[3], $arr)) {
            $this->setAdresse($arr[$keys[3]]);
        }
        if (array_key_exists($keys[4], $arr)) {
            $this->setCodepostal($arr[$keys[4]]);
        }
        if (array_key_exists($keys[5], $arr)) {
            $this->setVille($arr[$keys[5]]);
        }
        if (array_key_exists($keys[6], $arr)) {
            $this->setPays($arr[$keys[6]]);
        }
        if (array_key_exists($keys[7], $arr)) {
            $this->setTelephone($arr[$keys[7]]);
        }
        if (array_key_exists($keys[8], $arr)) {
            $this->setFax($arr[$keys[8]]);
        }
        if (array_key_exists($keys[9], $arr)) {
            $this->setUrl($arr[$keys[9]]);
        }
        if (array_key_exists($keys[10], $arr)) {
            $this->setAssujettitva($arr[$keys[10]]);
        }
        if (array_key_exists($keys[11], $arr)) {
            $this->setEmail($arr[$keys[11]]);
        }
        if (array_key_exists($keys[12], $arr)) {
            $this->setCreatedAt($arr[$keys[12]]);
        }
        if (array_key_exists($keys[13], $arr)) {
            $this->setUpdatedAt($arr[$keys[13]]);
        }
    }

     /**
     * Populate the current object from a string, using a given parser format
     * <code>
     * $book = new Book();
     * $book->importFrom('JSON', '{"Id":9012,"Title":"Don Juan","ISBN":"0140422161","Price":12.99,"PublisherId":1234,"AuthorId":5678}');
     * </code>
     *
     * You can specify the key type of the array by additionally passing one
     * of the class type constants TableMap::TYPE_PHPNAME, TableMap::TYPE_CAMELNAME,
     * TableMap::TYPE_COLNAME, TableMap::TYPE_FIELDNAME, TableMap::TYPE_NUM.
     * The default key type is the column's TableMap::TYPE_FIELDNAME.
     *
     * @param mixed $parser A AbstractParser instance,
     *                       or a format name ('XML', 'YAML', 'JSON', 'CSV')
     * @param string $data The source data to import from
     * @param string $keyType The type of keys the array uses.
     *
     * @return $this|\Entite The current object, for fluid interface
     */
    public function importFrom($parser, $data, $keyType = TableMap::TYPE_FIELDNAME)
    {
        if (!$parser instanceof AbstractParser) {
            $parser = AbstractParser::getParser($parser);
        }

        $this->fromArray($parser->toArray($data), $keyType);

        return $this;
    }

    /**
     * Build a Criteria object containing the values of all modified columns in this object.
     *
     * @return Criteria The Criteria object containing all modified values.
     */
    public function buildCriteria()
    {
        $criteria = new Criteria(EntiteTableMap::DATABASE_NAME);

        if ($this->isColumnModified(EntiteTableMap::COL_ID_ENTITE)) {
            $criteria->add(EntiteTableMap::COL_ID_ENTITE, $this->id_entite);
        }
        if ($this->isColumnModified(EntiteTableMap::COL_NOM)) {
            $criteria->add(EntiteTableMap::COL_NOM, $this->nom);
        }
        if ($this->isColumnModified(EntiteTableMap::COL_NOMCOURT)) {
            $criteria->add(EntiteTableMap::COL_NOMCOURT, $this->nomcourt);
        }
        if ($this->isColumnModified(EntiteTableMap::COL_ADRESSE)) {
            $criteria->add(EntiteTableMap::COL_ADRESSE, $this->adresse);
        }
        if ($this->isColumnModified(EntiteTableMap::COL_CODEPOSTAL)) {
            $criteria->add(EntiteTableMap::COL_CODEPOSTAL, $this->codepostal);
        }
        if ($this->isColumnModified(EntiteTableMap::COL_VILLE)) {
            $criteria->add(EntiteTableMap::COL_VILLE, $this->ville);
        }
        if ($this->isColumnModified(EntiteTableMap::COL_PAYS)) {
            $criteria->add(EntiteTableMap::COL_PAYS, $this->pays);
        }
        if ($this->isColumnModified(EntiteTableMap::COL_TELEPHONE)) {
            $criteria->add(EntiteTableMap::COL_TELEPHONE, $this->telephone);
        }
        if ($this->isColumnModified(EntiteTableMap::COL_FAX)) {
            $criteria->add(EntiteTableMap::COL_FAX, $this->fax);
        }
        if ($this->isColumnModified(EntiteTableMap::COL_URL)) {
            $criteria->add(EntiteTableMap::COL_URL, $this->url);
        }
        if ($this->isColumnModified(EntiteTableMap::COL_ASSUJETTITVA)) {
            $criteria->add(EntiteTableMap::COL_ASSUJETTITVA, $this->assujettitva);
        }
        if ($this->isColumnModified(EntiteTableMap::COL_EMAIL)) {
            $criteria->add(EntiteTableMap::COL_EMAIL, $this->email);
        }
        if ($this->isColumnModified(EntiteTableMap::COL_CREATED_AT)) {
            $criteria->add(EntiteTableMap::COL_CREATED_AT, $this->created_at);
        }
        if ($this->isColumnModified(EntiteTableMap::COL_UPDATED_AT)) {
            $criteria->add(EntiteTableMap::COL_UPDATED_AT, $this->updated_at);
        }

        return $criteria;
    }

    /**
     * Builds a Criteria object containing the primary key for this object.
     *
     * Unlike buildCriteria() this method includes the primary key values regardless
     * of whether or not they have been modified.
     *
     * @throws LogicException if no primary key is defined
     *
     * @return Criteria The Criteria object containing value(s) for primary key(s).
     */
    public function buildPkeyCriteria()
    {
        $criteria = ChildEntiteQuery::create();
        $criteria->add(EntiteTableMap::COL_ID_ENTITE, $this->id_entite);

        return $criteria;
    }

    /**
     * If the primary key is not null, return the hashcode of the
     * primary key. Otherwise, return the hash code of the object.
     *
     * @return int Hashcode
     */
    public function hashCode()
    {
        $validPk = null !== $this->getIdEntite();

        $validPrimaryKeyFKs = 0;
        $primaryKeyFKs = [];

        if ($validPk) {
            return crc32(json_encode($this->getPrimaryKey(), JSON_UNESCAPED_UNICODE));
        } elseif ($validPrimaryKeyFKs) {
            return crc32(json_encode($primaryKeyFKs, JSON_UNESCAPED_UNICODE));
        }

        return spl_object_hash($this);
    }

    /**
     * Returns the primary key for this object (row).
     * @return string
     */
    public function getPrimaryKey()
    {
        return $this->getIdEntite();
    }

    /**
     * Generic method to set the primary key (id_entite column).
     *
     * @param       string $key Primary key.
     * @return void
     */
    public function setPrimaryKey($key)
    {
        $this->setIdEntite($key);
    }

    /**
     * Returns true if the primary key for this object is null.
     * @return boolean
     */
    public function isPrimaryKeyNull()
    {
        return null === $this->getIdEntite();
    }

    /**
     * Sets contents of passed object to values from current object.
     *
     * If desired, this method can also make copies of all associated (fkey referrers)
     * objects.
     *
     * @param      object $copyObj An object of \Entite (or compatible) type.
     * @param      boolean $deepCopy Whether to also copy all rows that refer (by fkey) to the current row.
     * @param      boolean $makeNew Whether to reset autoincrement PKs and make the object new.
     * @throws PropelException
     */
    public function copyInto($copyObj, $deepCopy = false, $makeNew = true)
    {
        $copyObj->setNom($this->getNom());
        $copyObj->setNomcourt($this->getNomcourt());
        $copyObj->setAdresse($this->getAdresse());
        $copyObj->setCodepostal($this->getCodepostal());
        $copyObj->setVille($this->getVille());
        $copyObj->setPays($this->getPays());
        $copyObj->setTelephone($this->getTelephone());
        $copyObj->setFax($this->getFax());
        $copyObj->setUrl($this->getUrl());
        $copyObj->setAssujettitva($this->getAssujettitva());
        $copyObj->setEmail($this->getEmail());
        $copyObj->setCreatedAt($this->getCreatedAt());
        $copyObj->setUpdatedAt($this->getUpdatedAt());

        if ($deepCopy) {
            // important: temporarily setNew(false) because this affects the behavior of
            // the getter/setter methods for fkey referrer objects.
            $copyObj->setNew(false);

            foreach ($this->getActivites() as $relObj) {
                if ($relObj !== $this) {  // ensure that we don't try to copy a reference to ourselves
                    $copyObj->addActivite($relObj->copy($deepCopy));
                }
            }

            foreach ($this->getComptes() as $relObj) {
                if ($relObj !== $this) {  // ensure that we don't try to copy a reference to ourselves
                    $copyObj->addCompte($relObj->copy($deepCopy));
                }
            }

            foreach ($this->getEcritures() as $relObj) {
                if ($relObj !== $this) {  // ensure that we don't try to copy a reference to ourselves
                    $copyObj->addEcriture($relObj->copy($deepCopy));
                }
            }

            foreach ($this->getJournals() as $relObj) {
                if ($relObj !== $this) {  // ensure that we don't try to copy a reference to ourselves
                    $copyObj->addJournal($relObj->copy($deepCopy));
                }
            }

            foreach ($this->getPieces() as $relObj) {
                if ($relObj !== $this) {  // ensure that we don't try to copy a reference to ourselves
                    $copyObj->addPiece($relObj->copy($deepCopy));
                }
            }

            foreach ($this->getAutorisations() as $relObj) {
                if ($relObj !== $this) {  // ensure that we don't try to copy a reference to ourselves
                    $copyObj->addAutorisation($relObj->copy($deepCopy));
                }
            }

            foreach ($this->getPaiements() as $relObj) {
                if ($relObj !== $this) {  // ensure that we don't try to copy a reference to ourselves
                    $copyObj->addPaiement($relObj->copy($deepCopy));
                }
            }

            foreach ($this->getPrestations() as $relObj) {
                if ($relObj !== $this) {  // ensure that we don't try to copy a reference to ourselves
                    $copyObj->addPrestation($relObj->copy($deepCopy));
                }
            }

            foreach ($this->getPrestationslots() as $relObj) {
                if ($relObj !== $this) {  // ensure that we don't try to copy a reference to ourselves
                    $copyObj->addPrestationslot($relObj->copy($deepCopy));
                }
            }

            foreach ($this->getServicerendus() as $relObj) {
                if ($relObj !== $this) {  // ensure that we don't try to copy a reference to ourselves
                    $copyObj->addServicerendu($relObj->copy($deepCopy));
                }
            }

            foreach ($this->getTresors() as $relObj) {
                if ($relObj !== $this) {  // ensure that we don't try to copy a reference to ourselves
                    $copyObj->addTresor($relObj->copy($deepCopy));
                }
            }

            foreach ($this->getTvas() as $relObj) {
                if ($relObj !== $this) {  // ensure that we don't try to copy a reference to ourselves
                    $copyObj->addTva($relObj->copy($deepCopy));
                }
            }

            foreach ($this->getCourriers() as $relObj) {
                if ($relObj !== $this) {  // ensure that we don't try to copy a reference to ourselves
                    $copyObj->addCourrier($relObj->copy($deepCopy));
                }
            }

        } // if ($deepCopy)

        if ($makeNew) {
            $copyObj->setNew(true);
            $copyObj->setIdEntite(NULL); // this is a auto-increment column, so set to default value
        }
    }

    /**
     * Makes a copy of this object that will be inserted as a new row in table when saved.
     * It creates a new object filling in the simple attributes, but skipping any primary
     * keys that are defined for the table.
     *
     * If desired, this method can also make copies of all associated (fkey referrers)
     * objects.
     *
     * @param  boolean $deepCopy Whether to also copy all rows that refer (by fkey) to the current row.
     * @return \Entite Clone of current object.
     * @throws PropelException
     */
    public function copy($deepCopy = false)
    {
        // we use get_class(), because this might be a subclass
        $clazz = get_class($this);
        $copyObj = new $clazz();
        $this->copyInto($copyObj, $deepCopy);

        return $copyObj;
    }


    /**
     * Initializes a collection based on the name of a relation.
     * Avoids crafting an 'init[$relationName]s' method name
     * that wouldn't work when StandardEnglishPluralizer is used.
     *
     * @param      string $relationName The name of the relation to initialize
     * @return void
     */
    public function initRelation($relationName)
    {
        if ('Activite' == $relationName) {
            $this->initActivites();
            return;
        }
        if ('Compte' == $relationName) {
            $this->initComptes();
            return;
        }
        if ('Ecriture' == $relationName) {
            $this->initEcritures();
            return;
        }
        if ('Journal' == $relationName) {
            $this->initJournals();
            return;
        }
        if ('Piece' == $relationName) {
            $this->initPieces();
            return;
        }
        if ('Autorisation' == $relationName) {
            $this->initAutorisations();
            return;
        }
        if ('Paiement' == $relationName) {
            $this->initPaiements();
            return;
        }
        if ('Prestation' == $relationName) {
            $this->initPrestations();
            return;
        }
        if ('Prestationslot' == $relationName) {
            $this->initPrestationslots();
            return;
        }
        if ('Servicerendu' == $relationName) {
            $this->initServicerendus();
            return;
        }
        if ('Tresor' == $relationName) {
            $this->initTresors();
            return;
        }
        if ('Tva' == $relationName) {
            $this->initTvas();
            return;
        }
        if ('Courrier' == $relationName) {
            $this->initCourriers();
            return;
        }
    }

    /**
     * Clears out the collActivites collection
     *
     * This does not modify the database; however, it will remove any associated objects, causing
     * them to be refetched by subsequent calls to accessor method.
     *
     * @return void
     * @see        addActivites()
     */
    public function clearActivites()
    {
        $this->collActivites = null; // important to set this to NULL since that means it is uninitialized
    }

    /**
     * Reset is the collActivites collection loaded partially.
     */
    public function resetPartialActivites($v = true)
    {
        $this->collActivitesPartial = $v;
    }

    /**
     * Initializes the collActivites collection.
     *
     * By default this just sets the collActivites collection to an empty array (like clearcollActivites());
     * however, you may wish to override this method in your stub class to provide setting appropriate
     * to your application -- for example, setting the initial array to the values stored in database.
     *
     * @param      boolean $overrideExisting If set to true, the method call initializes
     *                                        the collection even if it is not empty
     *
     * @return void
     */
    public function initActivites($overrideExisting = true)
    {
        if (null !== $this->collActivites && !$overrideExisting) {
            return;
        }

        $collectionClassName = ActiviteTableMap::getTableMap()->getCollectionClassName();

        $this->collActivites = new $collectionClassName;
        $this->collActivites->setModel('\Activite');
    }

    /**
     * Gets an array of ChildActivite objects which contain a foreign key that references this object.
     *
     * If the $criteria is not null, it is used to always fetch the results from the database.
     * Otherwise the results are fetched from the database the first time, then cached.
     * Next time the same method is called without $criteria, the cached collection is returned.
     * If this ChildEntite is new, it will return
     * an empty collection or the current collection; the criteria is ignored on a new object.
     *
     * @param      Criteria $criteria optional Criteria object to narrow the query
     * @param      ConnectionInterface $con optional connection object
     * @return ObjectCollection|ChildActivite[] List of ChildActivite objects
     * @throws PropelException
     */
    public function getActivites(Criteria $criteria = null, ConnectionInterface $con = null)
    {
        $partial = $this->collActivitesPartial && !$this->isNew();
        if (null === $this->collActivites || null !== $criteria  || $partial) {
            if ($this->isNew() && null === $this->collActivites) {
                // return empty collection
                $this->initActivites();
            } else {
                $collActivites = ChildActiviteQuery::create(null, $criteria)
                    ->filterByEntite($this)
                    ->find($con);

                if (null !== $criteria) {
                    if (false !== $this->collActivitesPartial && count($collActivites)) {
                        $this->initActivites(false);

                        foreach ($collActivites as $obj) {
                            if (false == $this->collActivites->contains($obj)) {
                                $this->collActivites->append($obj);
                            }
                        }

                        $this->collActivitesPartial = true;
                    }

                    return $collActivites;
                }

                if ($partial && $this->collActivites) {
                    foreach ($this->collActivites as $obj) {
                        if ($obj->isNew()) {
                            $collActivites[] = $obj;
                        }
                    }
                }

                $this->collActivites = $collActivites;
                $this->collActivitesPartial = false;
            }
        }

        return $this->collActivites;
    }

    /**
     * Sets a collection of ChildActivite objects related by a one-to-many relationship
     * to the current object.
     * It will also schedule objects for deletion based on a diff between old objects (aka persisted)
     * and new objects from the given Propel collection.
     *
     * @param      Collection $activites A Propel collection.
     * @param      ConnectionInterface $con Optional connection object
     * @return $this|ChildEntite The current object (for fluent API support)
     */
    public function setActivites(Collection $activites, ConnectionInterface $con = null)
    {
        /** @var ChildActivite[] $activitesToDelete */
        $activitesToDelete = $this->getActivites(new Criteria(), $con)->diff($activites);


        $this->activitesScheduledForDeletion = $activitesToDelete;

        foreach ($activitesToDelete as $activiteRemoved) {
            $activiteRemoved->setEntite(null);
        }

        $this->collActivites = null;
        foreach ($activites as $activite) {
            $this->addActivite($activite);
        }

        $this->collActivites = $activites;
        $this->collActivitesPartial = false;

        return $this;
    }

    /**
     * Returns the number of related Activite objects.
     *
     * @param      Criteria $criteria
     * @param      boolean $distinct
     * @param      ConnectionInterface $con
     * @return int             Count of related Activite objects.
     * @throws PropelException
     */
    public function countActivites(Criteria $criteria = null, $distinct = false, ConnectionInterface $con = null)
    {
        $partial = $this->collActivitesPartial && !$this->isNew();
        if (null === $this->collActivites || null !== $criteria || $partial) {
            if ($this->isNew() && null === $this->collActivites) {
                return 0;
            }

            if ($partial && !$criteria) {
                return count($this->getActivites());
            }

            $query = ChildActiviteQuery::create(null, $criteria);
            if ($distinct) {
                $query->distinct();
            }

            return $query
                ->filterByEntite($this)
                ->count($con);
        }

        return count($this->collActivites);
    }

    /**
     * Method called to associate a ChildActivite object to this object
     * through the ChildActivite foreign key attribute.
     *
     * @param  ChildActivite $l ChildActivite
     * @return $this|\Entite The current object (for fluent API support)
     */
    public function addActivite(ChildActivite $l)
    {
        if ($this->collActivites === null) {
            $this->initActivites();
            $this->collActivitesPartial = true;
        }

        if (!$this->collActivites->contains($l)) {
            $this->doAddActivite($l);

            if ($this->activitesScheduledForDeletion and $this->activitesScheduledForDeletion->contains($l)) {
                $this->activitesScheduledForDeletion->remove($this->activitesScheduledForDeletion->search($l));
            }
        }

        return $this;
    }

    /**
     * @param ChildActivite $activite The ChildActivite object to add.
     */
    protected function doAddActivite(ChildActivite $activite)
    {
        $this->collActivites[]= $activite;
        $activite->setEntite($this);
    }

    /**
     * @param  ChildActivite $activite The ChildActivite object to remove.
     * @return $this|ChildEntite The current object (for fluent API support)
     */
    public function removeActivite(ChildActivite $activite)
    {
        if ($this->getActivites()->contains($activite)) {
            $pos = $this->collActivites->search($activite);
            $this->collActivites->remove($pos);
            if (null === $this->activitesScheduledForDeletion) {
                $this->activitesScheduledForDeletion = clone $this->collActivites;
                $this->activitesScheduledForDeletion->clear();
            }
            $this->activitesScheduledForDeletion[]= clone $activite;
            $activite->setEntite(null);
        }

        return $this;
    }

    /**
     * Clears out the collComptes collection
     *
     * This does not modify the database; however, it will remove any associated objects, causing
     * them to be refetched by subsequent calls to accessor method.
     *
     * @return void
     * @see        addComptes()
     */
    public function clearComptes()
    {
        $this->collComptes = null; // important to set this to NULL since that means it is uninitialized
    }

    /**
     * Reset is the collComptes collection loaded partially.
     */
    public function resetPartialComptes($v = true)
    {
        $this->collComptesPartial = $v;
    }

    /**
     * Initializes the collComptes collection.
     *
     * By default this just sets the collComptes collection to an empty array (like clearcollComptes());
     * however, you may wish to override this method in your stub class to provide setting appropriate
     * to your application -- for example, setting the initial array to the values stored in database.
     *
     * @param      boolean $overrideExisting If set to true, the method call initializes
     *                                        the collection even if it is not empty
     *
     * @return void
     */
    public function initComptes($overrideExisting = true)
    {
        if (null !== $this->collComptes && !$overrideExisting) {
            return;
        }

        $collectionClassName = CompteTableMap::getTableMap()->getCollectionClassName();

        $this->collComptes = new $collectionClassName;
        $this->collComptes->setModel('\Compte');
    }

    /**
     * Gets an array of ChildCompte objects which contain a foreign key that references this object.
     *
     * If the $criteria is not null, it is used to always fetch the results from the database.
     * Otherwise the results are fetched from the database the first time, then cached.
     * Next time the same method is called without $criteria, the cached collection is returned.
     * If this ChildEntite is new, it will return
     * an empty collection or the current collection; the criteria is ignored on a new object.
     *
     * @param      Criteria $criteria optional Criteria object to narrow the query
     * @param      ConnectionInterface $con optional connection object
     * @return ObjectCollection|ChildCompte[] List of ChildCompte objects
     * @throws PropelException
     */
    public function getComptes(Criteria $criteria = null, ConnectionInterface $con = null)
    {
        $partial = $this->collComptesPartial && !$this->isNew();
        if (null === $this->collComptes || null !== $criteria  || $partial) {
            if ($this->isNew() && null === $this->collComptes) {
                // return empty collection
                $this->initComptes();
            } else {
                $collComptes = ChildCompteQuery::create(null, $criteria)
                    ->filterByEntite($this)
                    ->find($con);

                if (null !== $criteria) {
                    if (false !== $this->collComptesPartial && count($collComptes)) {
                        $this->initComptes(false);

                        foreach ($collComptes as $obj) {
                            if (false == $this->collComptes->contains($obj)) {
                                $this->collComptes->append($obj);
                            }
                        }

                        $this->collComptesPartial = true;
                    }

                    return $collComptes;
                }

                if ($partial && $this->collComptes) {
                    foreach ($this->collComptes as $obj) {
                        if ($obj->isNew()) {
                            $collComptes[] = $obj;
                        }
                    }
                }

                $this->collComptes = $collComptes;
                $this->collComptesPartial = false;
            }
        }

        return $this->collComptes;
    }

    /**
     * Sets a collection of ChildCompte objects related by a one-to-many relationship
     * to the current object.
     * It will also schedule objects for deletion based on a diff between old objects (aka persisted)
     * and new objects from the given Propel collection.
     *
     * @param      Collection $comptes A Propel collection.
     * @param      ConnectionInterface $con Optional connection object
     * @return $this|ChildEntite The current object (for fluent API support)
     */
    public function setComptes(Collection $comptes, ConnectionInterface $con = null)
    {
        /** @var ChildCompte[] $comptesToDelete */
        $comptesToDelete = $this->getComptes(new Criteria(), $con)->diff($comptes);


        $this->comptesScheduledForDeletion = $comptesToDelete;

        foreach ($comptesToDelete as $compteRemoved) {
            $compteRemoved->setEntite(null);
        }

        $this->collComptes = null;
        foreach ($comptes as $compte) {
            $this->addCompte($compte);
        }

        $this->collComptes = $comptes;
        $this->collComptesPartial = false;

        return $this;
    }

    /**
     * Returns the number of related Compte objects.
     *
     * @param      Criteria $criteria
     * @param      boolean $distinct
     * @param      ConnectionInterface $con
     * @return int             Count of related Compte objects.
     * @throws PropelException
     */
    public function countComptes(Criteria $criteria = null, $distinct = false, ConnectionInterface $con = null)
    {
        $partial = $this->collComptesPartial && !$this->isNew();
        if (null === $this->collComptes || null !== $criteria || $partial) {
            if ($this->isNew() && null === $this->collComptes) {
                return 0;
            }

            if ($partial && !$criteria) {
                return count($this->getComptes());
            }

            $query = ChildCompteQuery::create(null, $criteria);
            if ($distinct) {
                $query->distinct();
            }

            return $query
                ->filterByEntite($this)
                ->count($con);
        }

        return count($this->collComptes);
    }

    /**
     * Method called to associate a ChildCompte object to this object
     * through the ChildCompte foreign key attribute.
     *
     * @param  ChildCompte $l ChildCompte
     * @return $this|\Entite The current object (for fluent API support)
     */
    public function addCompte(ChildCompte $l)
    {
        if ($this->collComptes === null) {
            $this->initComptes();
            $this->collComptesPartial = true;
        }

        if (!$this->collComptes->contains($l)) {
            $this->doAddCompte($l);

            if ($this->comptesScheduledForDeletion and $this->comptesScheduledForDeletion->contains($l)) {
                $this->comptesScheduledForDeletion->remove($this->comptesScheduledForDeletion->search($l));
            }
        }

        return $this;
    }

    /**
     * @param ChildCompte $compte The ChildCompte object to add.
     */
    protected function doAddCompte(ChildCompte $compte)
    {
        $this->collComptes[]= $compte;
        $compte->setEntite($this);
    }

    /**
     * @param  ChildCompte $compte The ChildCompte object to remove.
     * @return $this|ChildEntite The current object (for fluent API support)
     */
    public function removeCompte(ChildCompte $compte)
    {
        if ($this->getComptes()->contains($compte)) {
            $pos = $this->collComptes->search($compte);
            $this->collComptes->remove($pos);
            if (null === $this->comptesScheduledForDeletion) {
                $this->comptesScheduledForDeletion = clone $this->collComptes;
                $this->comptesScheduledForDeletion->clear();
            }
            $this->comptesScheduledForDeletion[]= clone $compte;
            $compte->setEntite(null);
        }

        return $this;
    }


    /**
     * If this collection has already been initialized with
     * an identical criteria, it returns the collection.
     * Otherwise if this Entite is new, it will return
     * an empty collection; or if this Entite has previously
     * been saved, it will retrieve related Comptes from storage.
     *
     * This method is protected by default in order to keep the public
     * api reasonable.  You can provide public methods for those you
     * actually need in Entite.
     *
     * @param      Criteria $criteria optional Criteria object to narrow the query
     * @param      ConnectionInterface $con optional connection object
     * @param      string $joinBehavior optional join type to use (defaults to Criteria::LEFT_JOIN)
     * @return ObjectCollection|ChildCompte[] List of ChildCompte objects
     */
    public function getComptesJoinPoste(Criteria $criteria = null, ConnectionInterface $con = null, $joinBehavior = Criteria::LEFT_JOIN)
    {
        $query = ChildCompteQuery::create(null, $criteria);
        $query->joinWith('Poste', $joinBehavior);

        return $this->getComptes($query, $con);
    }

    /**
     * Clears out the collEcritures collection
     *
     * This does not modify the database; however, it will remove any associated objects, causing
     * them to be refetched by subsequent calls to accessor method.
     *
     * @return void
     * @see        addEcritures()
     */
    public function clearEcritures()
    {
        $this->collEcritures = null; // important to set this to NULL since that means it is uninitialized
    }

    /**
     * Reset is the collEcritures collection loaded partially.
     */
    public function resetPartialEcritures($v = true)
    {
        $this->collEcrituresPartial = $v;
    }

    /**
     * Initializes the collEcritures collection.
     *
     * By default this just sets the collEcritures collection to an empty array (like clearcollEcritures());
     * however, you may wish to override this method in your stub class to provide setting appropriate
     * to your application -- for example, setting the initial array to the values stored in database.
     *
     * @param      boolean $overrideExisting If set to true, the method call initializes
     *                                        the collection even if it is not empty
     *
     * @return void
     */
    public function initEcritures($overrideExisting = true)
    {
        if (null !== $this->collEcritures && !$overrideExisting) {
            return;
        }

        $collectionClassName = EcritureTableMap::getTableMap()->getCollectionClassName();

        $this->collEcritures = new $collectionClassName;
        $this->collEcritures->setModel('\Ecriture');
    }

    /**
     * Gets an array of ChildEcriture objects which contain a foreign key that references this object.
     *
     * If the $criteria is not null, it is used to always fetch the results from the database.
     * Otherwise the results are fetched from the database the first time, then cached.
     * Next time the same method is called without $criteria, the cached collection is returned.
     * If this ChildEntite is new, it will return
     * an empty collection or the current collection; the criteria is ignored on a new object.
     *
     * @param      Criteria $criteria optional Criteria object to narrow the query
     * @param      ConnectionInterface $con optional connection object
     * @return ObjectCollection|ChildEcriture[] List of ChildEcriture objects
     * @throws PropelException
     */
    public function getEcritures(Criteria $criteria = null, ConnectionInterface $con = null)
    {
        $partial = $this->collEcrituresPartial && !$this->isNew();
        if (null === $this->collEcritures || null !== $criteria  || $partial) {
            if ($this->isNew() && null === $this->collEcritures) {
                // return empty collection
                $this->initEcritures();
            } else {
                $collEcritures = ChildEcritureQuery::create(null, $criteria)
                    ->filterByEntite($this)
                    ->find($con);

                if (null !== $criteria) {
                    if (false !== $this->collEcrituresPartial && count($collEcritures)) {
                        $this->initEcritures(false);

                        foreach ($collEcritures as $obj) {
                            if (false == $this->collEcritures->contains($obj)) {
                                $this->collEcritures->append($obj);
                            }
                        }

                        $this->collEcrituresPartial = true;
                    }

                    return $collEcritures;
                }

                if ($partial && $this->collEcritures) {
                    foreach ($this->collEcritures as $obj) {
                        if ($obj->isNew()) {
                            $collEcritures[] = $obj;
                        }
                    }
                }

                $this->collEcritures = $collEcritures;
                $this->collEcrituresPartial = false;
            }
        }

        return $this->collEcritures;
    }

    /**
     * Sets a collection of ChildEcriture objects related by a one-to-many relationship
     * to the current object.
     * It will also schedule objects for deletion based on a diff between old objects (aka persisted)
     * and new objects from the given Propel collection.
     *
     * @param      Collection $ecritures A Propel collection.
     * @param      ConnectionInterface $con Optional connection object
     * @return $this|ChildEntite The current object (for fluent API support)
     */
    public function setEcritures(Collection $ecritures, ConnectionInterface $con = null)
    {
        /** @var ChildEcriture[] $ecrituresToDelete */
        $ecrituresToDelete = $this->getEcritures(new Criteria(), $con)->diff($ecritures);


        $this->ecrituresScheduledForDeletion = $ecrituresToDelete;

        foreach ($ecrituresToDelete as $ecritureRemoved) {
            $ecritureRemoved->setEntite(null);
        }

        $this->collEcritures = null;
        foreach ($ecritures as $ecriture) {
            $this->addEcriture($ecriture);
        }

        $this->collEcritures = $ecritures;
        $this->collEcrituresPartial = false;

        return $this;
    }

    /**
     * Returns the number of related Ecriture objects.
     *
     * @param      Criteria $criteria
     * @param      boolean $distinct
     * @param      ConnectionInterface $con
     * @return int             Count of related Ecriture objects.
     * @throws PropelException
     */
    public function countEcritures(Criteria $criteria = null, $distinct = false, ConnectionInterface $con = null)
    {
        $partial = $this->collEcrituresPartial && !$this->isNew();
        if (null === $this->collEcritures || null !== $criteria || $partial) {
            if ($this->isNew() && null === $this->collEcritures) {
                return 0;
            }

            if ($partial && !$criteria) {
                return count($this->getEcritures());
            }

            $query = ChildEcritureQuery::create(null, $criteria);
            if ($distinct) {
                $query->distinct();
            }

            return $query
                ->filterByEntite($this)
                ->count($con);
        }

        return count($this->collEcritures);
    }

    /**
     * Method called to associate a ChildEcriture object to this object
     * through the ChildEcriture foreign key attribute.
     *
     * @param  ChildEcriture $l ChildEcriture
     * @return $this|\Entite The current object (for fluent API support)
     */
    public function addEcriture(ChildEcriture $l)
    {
        if ($this->collEcritures === null) {
            $this->initEcritures();
            $this->collEcrituresPartial = true;
        }

        if (!$this->collEcritures->contains($l)) {
            $this->doAddEcriture($l);

            if ($this->ecrituresScheduledForDeletion and $this->ecrituresScheduledForDeletion->contains($l)) {
                $this->ecrituresScheduledForDeletion->remove($this->ecrituresScheduledForDeletion->search($l));
            }
        }

        return $this;
    }

    /**
     * @param ChildEcriture $ecriture The ChildEcriture object to add.
     */
    protected function doAddEcriture(ChildEcriture $ecriture)
    {
        $this->collEcritures[]= $ecriture;
        $ecriture->setEntite($this);
    }

    /**
     * @param  ChildEcriture $ecriture The ChildEcriture object to remove.
     * @return $this|ChildEntite The current object (for fluent API support)
     */
    public function removeEcriture(ChildEcriture $ecriture)
    {
        if ($this->getEcritures()->contains($ecriture)) {
            $pos = $this->collEcritures->search($ecriture);
            $this->collEcritures->remove($pos);
            if (null === $this->ecrituresScheduledForDeletion) {
                $this->ecrituresScheduledForDeletion = clone $this->collEcritures;
                $this->ecrituresScheduledForDeletion->clear();
            }
            $this->ecrituresScheduledForDeletion[]= clone $ecriture;
            $ecriture->setEntite(null);
        }

        return $this;
    }


    /**
     * If this collection has already been initialized with
     * an identical criteria, it returns the collection.
     * Otherwise if this Entite is new, it will return
     * an empty collection; or if this Entite has previously
     * been saved, it will retrieve related Ecritures from storage.
     *
     * This method is protected by default in order to keep the public
     * api reasonable.  You can provide public methods for those you
     * actually need in Entite.
     *
     * @param      Criteria $criteria optional Criteria object to narrow the query
     * @param      ConnectionInterface $con optional connection object
     * @param      string $joinBehavior optional join type to use (defaults to Criteria::LEFT_JOIN)
     * @return ObjectCollection|ChildEcriture[] List of ChildEcriture objects
     */
    public function getEcrituresJoinCompte(Criteria $criteria = null, ConnectionInterface $con = null, $joinBehavior = Criteria::LEFT_JOIN)
    {
        $query = ChildEcritureQuery::create(null, $criteria);
        $query->joinWith('Compte', $joinBehavior);

        return $this->getEcritures($query, $con);
    }


    /**
     * If this collection has already been initialized with
     * an identical criteria, it returns the collection.
     * Otherwise if this Entite is new, it will return
     * an empty collection; or if this Entite has previously
     * been saved, it will retrieve related Ecritures from storage.
     *
     * This method is protected by default in order to keep the public
     * api reasonable.  You can provide public methods for those you
     * actually need in Entite.
     *
     * @param      Criteria $criteria optional Criteria object to narrow the query
     * @param      ConnectionInterface $con optional connection object
     * @param      string $joinBehavior optional join type to use (defaults to Criteria::LEFT_JOIN)
     * @return ObjectCollection|ChildEcriture[] List of ChildEcriture objects
     */
    public function getEcrituresJoinJournal(Criteria $criteria = null, ConnectionInterface $con = null, $joinBehavior = Criteria::LEFT_JOIN)
    {
        $query = ChildEcritureQuery::create(null, $criteria);
        $query->joinWith('Journal', $joinBehavior);

        return $this->getEcritures($query, $con);
    }


    /**
     * If this collection has already been initialized with
     * an identical criteria, it returns the collection.
     * Otherwise if this Entite is new, it will return
     * an empty collection; or if this Entite has previously
     * been saved, it will retrieve related Ecritures from storage.
     *
     * This method is protected by default in order to keep the public
     * api reasonable.  You can provide public methods for those you
     * actually need in Entite.
     *
     * @param      Criteria $criteria optional Criteria object to narrow the query
     * @param      ConnectionInterface $con optional connection object
     * @param      string $joinBehavior optional join type to use (defaults to Criteria::LEFT_JOIN)
     * @return ObjectCollection|ChildEcriture[] List of ChildEcriture objects
     */
    public function getEcrituresJoinActivite(Criteria $criteria = null, ConnectionInterface $con = null, $joinBehavior = Criteria::LEFT_JOIN)
    {
        $query = ChildEcritureQuery::create(null, $criteria);
        $query->joinWith('Activite', $joinBehavior);

        return $this->getEcritures($query, $con);
    }

    /**
     * Clears out the collJournals collection
     *
     * This does not modify the database; however, it will remove any associated objects, causing
     * them to be refetched by subsequent calls to accessor method.
     *
     * @return void
     * @see        addJournals()
     */
    public function clearJournals()
    {
        $this->collJournals = null; // important to set this to NULL since that means it is uninitialized
    }

    /**
     * Reset is the collJournals collection loaded partially.
     */
    public function resetPartialJournals($v = true)
    {
        $this->collJournalsPartial = $v;
    }

    /**
     * Initializes the collJournals collection.
     *
     * By default this just sets the collJournals collection to an empty array (like clearcollJournals());
     * however, you may wish to override this method in your stub class to provide setting appropriate
     * to your application -- for example, setting the initial array to the values stored in database.
     *
     * @param      boolean $overrideExisting If set to true, the method call initializes
     *                                        the collection even if it is not empty
     *
     * @return void
     */
    public function initJournals($overrideExisting = true)
    {
        if (null !== $this->collJournals && !$overrideExisting) {
            return;
        }

        $collectionClassName = JournalTableMap::getTableMap()->getCollectionClassName();

        $this->collJournals = new $collectionClassName;
        $this->collJournals->setModel('\Journal');
    }

    /**
     * Gets an array of ChildJournal objects which contain a foreign key that references this object.
     *
     * If the $criteria is not null, it is used to always fetch the results from the database.
     * Otherwise the results are fetched from the database the first time, then cached.
     * Next time the same method is called without $criteria, the cached collection is returned.
     * If this ChildEntite is new, it will return
     * an empty collection or the current collection; the criteria is ignored on a new object.
     *
     * @param      Criteria $criteria optional Criteria object to narrow the query
     * @param      ConnectionInterface $con optional connection object
     * @return ObjectCollection|ChildJournal[] List of ChildJournal objects
     * @throws PropelException
     */
    public function getJournals(Criteria $criteria = null, ConnectionInterface $con = null)
    {
        $partial = $this->collJournalsPartial && !$this->isNew();
        if (null === $this->collJournals || null !== $criteria  || $partial) {
            if ($this->isNew() && null === $this->collJournals) {
                // return empty collection
                $this->initJournals();
            } else {
                $collJournals = ChildJournalQuery::create(null, $criteria)
                    ->filterByEntite($this)
                    ->find($con);

                if (null !== $criteria) {
                    if (false !== $this->collJournalsPartial && count($collJournals)) {
                        $this->initJournals(false);

                        foreach ($collJournals as $obj) {
                            if (false == $this->collJournals->contains($obj)) {
                                $this->collJournals->append($obj);
                            }
                        }

                        $this->collJournalsPartial = true;
                    }

                    return $collJournals;
                }

                if ($partial && $this->collJournals) {
                    foreach ($this->collJournals as $obj) {
                        if ($obj->isNew()) {
                            $collJournals[] = $obj;
                        }
                    }
                }

                $this->collJournals = $collJournals;
                $this->collJournalsPartial = false;
            }
        }

        return $this->collJournals;
    }

    /**
     * Sets a collection of ChildJournal objects related by a one-to-many relationship
     * to the current object.
     * It will also schedule objects for deletion based on a diff between old objects (aka persisted)
     * and new objects from the given Propel collection.
     *
     * @param      Collection $journals A Propel collection.
     * @param      ConnectionInterface $con Optional connection object
     * @return $this|ChildEntite The current object (for fluent API support)
     */
    public function setJournals(Collection $journals, ConnectionInterface $con = null)
    {
        /** @var ChildJournal[] $journalsToDelete */
        $journalsToDelete = $this->getJournals(new Criteria(), $con)->diff($journals);


        $this->journalsScheduledForDeletion = $journalsToDelete;

        foreach ($journalsToDelete as $journalRemoved) {
            $journalRemoved->setEntite(null);
        }

        $this->collJournals = null;
        foreach ($journals as $journal) {
            $this->addJournal($journal);
        }

        $this->collJournals = $journals;
        $this->collJournalsPartial = false;

        return $this;
    }

    /**
     * Returns the number of related Journal objects.
     *
     * @param      Criteria $criteria
     * @param      boolean $distinct
     * @param      ConnectionInterface $con
     * @return int             Count of related Journal objects.
     * @throws PropelException
     */
    public function countJournals(Criteria $criteria = null, $distinct = false, ConnectionInterface $con = null)
    {
        $partial = $this->collJournalsPartial && !$this->isNew();
        if (null === $this->collJournals || null !== $criteria || $partial) {
            if ($this->isNew() && null === $this->collJournals) {
                return 0;
            }

            if ($partial && !$criteria) {
                return count($this->getJournals());
            }

            $query = ChildJournalQuery::create(null, $criteria);
            if ($distinct) {
                $query->distinct();
            }

            return $query
                ->filterByEntite($this)
                ->count($con);
        }

        return count($this->collJournals);
    }

    /**
     * Method called to associate a ChildJournal object to this object
     * through the ChildJournal foreign key attribute.
     *
     * @param  ChildJournal $l ChildJournal
     * @return $this|\Entite The current object (for fluent API support)
     */
    public function addJournal(ChildJournal $l)
    {
        if ($this->collJournals === null) {
            $this->initJournals();
            $this->collJournalsPartial = true;
        }

        if (!$this->collJournals->contains($l)) {
            $this->doAddJournal($l);

            if ($this->journalsScheduledForDeletion and $this->journalsScheduledForDeletion->contains($l)) {
                $this->journalsScheduledForDeletion->remove($this->journalsScheduledForDeletion->search($l));
            }
        }

        return $this;
    }

    /**
     * @param ChildJournal $journal The ChildJournal object to add.
     */
    protected function doAddJournal(ChildJournal $journal)
    {
        $this->collJournals[]= $journal;
        $journal->setEntite($this);
    }

    /**
     * @param  ChildJournal $journal The ChildJournal object to remove.
     * @return $this|ChildEntite The current object (for fluent API support)
     */
    public function removeJournal(ChildJournal $journal)
    {
        if ($this->getJournals()->contains($journal)) {
            $pos = $this->collJournals->search($journal);
            $this->collJournals->remove($pos);
            if (null === $this->journalsScheduledForDeletion) {
                $this->journalsScheduledForDeletion = clone $this->collJournals;
                $this->journalsScheduledForDeletion->clear();
            }
            $this->journalsScheduledForDeletion[]= clone $journal;
            $journal->setEntite(null);
        }

        return $this;
    }


    /**
     * If this collection has already been initialized with
     * an identical criteria, it returns the collection.
     * Otherwise if this Entite is new, it will return
     * an empty collection; or if this Entite has previously
     * been saved, it will retrieve related Journals from storage.
     *
     * This method is protected by default in order to keep the public
     * api reasonable.  You can provide public methods for those you
     * actually need in Entite.
     *
     * @param      Criteria $criteria optional Criteria object to narrow the query
     * @param      ConnectionInterface $con optional connection object
     * @param      string $joinBehavior optional join type to use (defaults to Criteria::LEFT_JOIN)
     * @return ObjectCollection|ChildJournal[] List of ChildJournal objects
     */
    public function getJournalsJoinCompte(Criteria $criteria = null, ConnectionInterface $con = null, $joinBehavior = Criteria::LEFT_JOIN)
    {
        $query = ChildJournalQuery::create(null, $criteria);
        $query->joinWith('Compte', $joinBehavior);

        return $this->getJournals($query, $con);
    }

    /**
     * Clears out the collPieces collection
     *
     * This does not modify the database; however, it will remove any associated objects, causing
     * them to be refetched by subsequent calls to accessor method.
     *
     * @return void
     * @see        addPieces()
     */
    public function clearPieces()
    {
        $this->collPieces = null; // important to set this to NULL since that means it is uninitialized
    }

    /**
     * Reset is the collPieces collection loaded partially.
     */
    public function resetPartialPieces($v = true)
    {
        $this->collPiecesPartial = $v;
    }

    /**
     * Initializes the collPieces collection.
     *
     * By default this just sets the collPieces collection to an empty array (like clearcollPieces());
     * however, you may wish to override this method in your stub class to provide setting appropriate
     * to your application -- for example, setting the initial array to the values stored in database.
     *
     * @param      boolean $overrideExisting If set to true, the method call initializes
     *                                        the collection even if it is not empty
     *
     * @return void
     */
    public function initPieces($overrideExisting = true)
    {
        if (null !== $this->collPieces && !$overrideExisting) {
            return;
        }

        $collectionClassName = PieceTableMap::getTableMap()->getCollectionClassName();

        $this->collPieces = new $collectionClassName;
        $this->collPieces->setModel('\Piece');
    }

    /**
     * Gets an array of ChildPiece objects which contain a foreign key that references this object.
     *
     * If the $criteria is not null, it is used to always fetch the results from the database.
     * Otherwise the results are fetched from the database the first time, then cached.
     * Next time the same method is called without $criteria, the cached collection is returned.
     * If this ChildEntite is new, it will return
     * an empty collection or the current collection; the criteria is ignored on a new object.
     *
     * @param      Criteria $criteria optional Criteria object to narrow the query
     * @param      ConnectionInterface $con optional connection object
     * @return ObjectCollection|ChildPiece[] List of ChildPiece objects
     * @throws PropelException
     */
    public function getPieces(Criteria $criteria = null, ConnectionInterface $con = null)
    {
        $partial = $this->collPiecesPartial && !$this->isNew();
        if (null === $this->collPieces || null !== $criteria  || $partial) {
            if ($this->isNew() && null === $this->collPieces) {
                // return empty collection
                $this->initPieces();
            } else {
                $collPieces = ChildPieceQuery::create(null, $criteria)
                    ->filterByEntite($this)
                    ->find($con);

                if (null !== $criteria) {
                    if (false !== $this->collPiecesPartial && count($collPieces)) {
                        $this->initPieces(false);

                        foreach ($collPieces as $obj) {
                            if (false == $this->collPieces->contains($obj)) {
                                $this->collPieces->append($obj);
                            }
                        }

                        $this->collPiecesPartial = true;
                    }

                    return $collPieces;
                }

                if ($partial && $this->collPieces) {
                    foreach ($this->collPieces as $obj) {
                        if ($obj->isNew()) {
                            $collPieces[] = $obj;
                        }
                    }
                }

                $this->collPieces = $collPieces;
                $this->collPiecesPartial = false;
            }
        }

        return $this->collPieces;
    }

    /**
     * Sets a collection of ChildPiece objects related by a one-to-many relationship
     * to the current object.
     * It will also schedule objects for deletion based on a diff between old objects (aka persisted)
     * and new objects from the given Propel collection.
     *
     * @param      Collection $pieces A Propel collection.
     * @param      ConnectionInterface $con Optional connection object
     * @return $this|ChildEntite The current object (for fluent API support)
     */
    public function setPieces(Collection $pieces, ConnectionInterface $con = null)
    {
        /** @var ChildPiece[] $piecesToDelete */
        $piecesToDelete = $this->getPieces(new Criteria(), $con)->diff($pieces);


        $this->piecesScheduledForDeletion = $piecesToDelete;

        foreach ($piecesToDelete as $pieceRemoved) {
            $pieceRemoved->setEntite(null);
        }

        $this->collPieces = null;
        foreach ($pieces as $piece) {
            $this->addPiece($piece);
        }

        $this->collPieces = $pieces;
        $this->collPiecesPartial = false;

        return $this;
    }

    /**
     * Returns the number of related Piece objects.
     *
     * @param      Criteria $criteria
     * @param      boolean $distinct
     * @param      ConnectionInterface $con
     * @return int             Count of related Piece objects.
     * @throws PropelException
     */
    public function countPieces(Criteria $criteria = null, $distinct = false, ConnectionInterface $con = null)
    {
        $partial = $this->collPiecesPartial && !$this->isNew();
        if (null === $this->collPieces || null !== $criteria || $partial) {
            if ($this->isNew() && null === $this->collPieces) {
                return 0;
            }

            if ($partial && !$criteria) {
                return count($this->getPieces());
            }

            $query = ChildPieceQuery::create(null, $criteria);
            if ($distinct) {
                $query->distinct();
            }

            return $query
                ->filterByEntite($this)
                ->count($con);
        }

        return count($this->collPieces);
    }

    /**
     * Method called to associate a ChildPiece object to this object
     * through the ChildPiece foreign key attribute.
     *
     * @param  ChildPiece $l ChildPiece
     * @return $this|\Entite The current object (for fluent API support)
     */
    public function addPiece(ChildPiece $l)
    {
        if ($this->collPieces === null) {
            $this->initPieces();
            $this->collPiecesPartial = true;
        }

        if (!$this->collPieces->contains($l)) {
            $this->doAddPiece($l);

            if ($this->piecesScheduledForDeletion and $this->piecesScheduledForDeletion->contains($l)) {
                $this->piecesScheduledForDeletion->remove($this->piecesScheduledForDeletion->search($l));
            }
        }

        return $this;
    }

    /**
     * @param ChildPiece $piece The ChildPiece object to add.
     */
    protected function doAddPiece(ChildPiece $piece)
    {
        $this->collPieces[]= $piece;
        $piece->setEntite($this);
    }

    /**
     * @param  ChildPiece $piece The ChildPiece object to remove.
     * @return $this|ChildEntite The current object (for fluent API support)
     */
    public function removePiece(ChildPiece $piece)
    {
        if ($this->getPieces()->contains($piece)) {
            $pos = $this->collPieces->search($piece);
            $this->collPieces->remove($pos);
            if (null === $this->piecesScheduledForDeletion) {
                $this->piecesScheduledForDeletion = clone $this->collPieces;
                $this->piecesScheduledForDeletion->clear();
            }
            $this->piecesScheduledForDeletion[]= clone $piece;
            $piece->setEntite(null);
        }

        return $this;
    }


    /**
     * If this collection has already been initialized with
     * an identical criteria, it returns the collection.
     * Otherwise if this Entite is new, it will return
     * an empty collection; or if this Entite has previously
     * been saved, it will retrieve related Pieces from storage.
     *
     * This method is protected by default in order to keep the public
     * api reasonable.  You can provide public methods for those you
     * actually need in Entite.
     *
     * @param      Criteria $criteria optional Criteria object to narrow the query
     * @param      ConnectionInterface $con optional connection object
     * @param      string $joinBehavior optional join type to use (defaults to Criteria::LEFT_JOIN)
     * @return ObjectCollection|ChildPiece[] List of ChildPiece objects
     */
    public function getPiecesJoinJournal(Criteria $criteria = null, ConnectionInterface $con = null, $joinBehavior = Criteria::LEFT_JOIN)
    {
        $query = ChildPieceQuery::create(null, $criteria);
        $query->joinWith('Journal', $joinBehavior);

        return $this->getPieces($query, $con);
    }

    /**
     * Clears out the collAutorisations collection
     *
     * This does not modify the database; however, it will remove any associated objects, causing
     * them to be refetched by subsequent calls to accessor method.
     *
     * @return void
     * @see        addAutorisations()
     */
    public function clearAutorisations()
    {
        $this->collAutorisations = null; // important to set this to NULL since that means it is uninitialized
    }

    /**
     * Reset is the collAutorisations collection loaded partially.
     */
    public function resetPartialAutorisations($v = true)
    {
        $this->collAutorisationsPartial = $v;
    }

    /**
     * Initializes the collAutorisations collection.
     *
     * By default this just sets the collAutorisations collection to an empty array (like clearcollAutorisations());
     * however, you may wish to override this method in your stub class to provide setting appropriate
     * to your application -- for example, setting the initial array to the values stored in database.
     *
     * @param      boolean $overrideExisting If set to true, the method call initializes
     *                                        the collection even if it is not empty
     *
     * @return void
     */
    public function initAutorisations($overrideExisting = true)
    {
        if (null !== $this->collAutorisations && !$overrideExisting) {
            return;
        }

        $collectionClassName = AutorisationTableMap::getTableMap()->getCollectionClassName();

        $this->collAutorisations = new $collectionClassName;
        $this->collAutorisations->setModel('\Autorisation');
    }

    /**
     * Gets an array of ChildAutorisation objects which contain a foreign key that references this object.
     *
     * If the $criteria is not null, it is used to always fetch the results from the database.
     * Otherwise the results are fetched from the database the first time, then cached.
     * Next time the same method is called without $criteria, the cached collection is returned.
     * If this ChildEntite is new, it will return
     * an empty collection or the current collection; the criteria is ignored on a new object.
     *
     * @param      Criteria $criteria optional Criteria object to narrow the query
     * @param      ConnectionInterface $con optional connection object
     * @return ObjectCollection|ChildAutorisation[] List of ChildAutorisation objects
     * @throws PropelException
     */
    public function getAutorisations(Criteria $criteria = null, ConnectionInterface $con = null)
    {
        $partial = $this->collAutorisationsPartial && !$this->isNew();
        if (null === $this->collAutorisations || null !== $criteria  || $partial) {
            if ($this->isNew() && null === $this->collAutorisations) {
                // return empty collection
                $this->initAutorisations();
            } else {
                $collAutorisations = ChildAutorisationQuery::create(null, $criteria)
                    ->filterByEntite($this)
                    ->find($con);

                if (null !== $criteria) {
                    if (false !== $this->collAutorisationsPartial && count($collAutorisations)) {
                        $this->initAutorisations(false);

                        foreach ($collAutorisations as $obj) {
                            if (false == $this->collAutorisations->contains($obj)) {
                                $this->collAutorisations->append($obj);
                            }
                        }

                        $this->collAutorisationsPartial = true;
                    }

                    return $collAutorisations;
                }

                if ($partial && $this->collAutorisations) {
                    foreach ($this->collAutorisations as $obj) {
                        if ($obj->isNew()) {
                            $collAutorisations[] = $obj;
                        }
                    }
                }

                $this->collAutorisations = $collAutorisations;
                $this->collAutorisationsPartial = false;
            }
        }

        return $this->collAutorisations;
    }

    /**
     * Sets a collection of ChildAutorisation objects related by a one-to-many relationship
     * to the current object.
     * It will also schedule objects for deletion based on a diff between old objects (aka persisted)
     * and new objects from the given Propel collection.
     *
     * @param      Collection $autorisations A Propel collection.
     * @param      ConnectionInterface $con Optional connection object
     * @return $this|ChildEntite The current object (for fluent API support)
     */
    public function setAutorisations(Collection $autorisations, ConnectionInterface $con = null)
    {
        /** @var ChildAutorisation[] $autorisationsToDelete */
        $autorisationsToDelete = $this->getAutorisations(new Criteria(), $con)->diff($autorisations);


        $this->autorisationsScheduledForDeletion = $autorisationsToDelete;

        foreach ($autorisationsToDelete as $autorisationRemoved) {
            $autorisationRemoved->setEntite(null);
        }

        $this->collAutorisations = null;
        foreach ($autorisations as $autorisation) {
            $this->addAutorisation($autorisation);
        }

        $this->collAutorisations = $autorisations;
        $this->collAutorisationsPartial = false;

        return $this;
    }

    /**
     * Returns the number of related Autorisation objects.
     *
     * @param      Criteria $criteria
     * @param      boolean $distinct
     * @param      ConnectionInterface $con
     * @return int             Count of related Autorisation objects.
     * @throws PropelException
     */
    public function countAutorisations(Criteria $criteria = null, $distinct = false, ConnectionInterface $con = null)
    {
        $partial = $this->collAutorisationsPartial && !$this->isNew();
        if (null === $this->collAutorisations || null !== $criteria || $partial) {
            if ($this->isNew() && null === $this->collAutorisations) {
                return 0;
            }

            if ($partial && !$criteria) {
                return count($this->getAutorisations());
            }

            $query = ChildAutorisationQuery::create(null, $criteria);
            if ($distinct) {
                $query->distinct();
            }

            return $query
                ->filterByEntite($this)
                ->count($con);
        }

        return count($this->collAutorisations);
    }

    /**
     * Method called to associate a ChildAutorisation object to this object
     * through the ChildAutorisation foreign key attribute.
     *
     * @param  ChildAutorisation $l ChildAutorisation
     * @return $this|\Entite The current object (for fluent API support)
     */
    public function addAutorisation(ChildAutorisation $l)
    {
        if ($this->collAutorisations === null) {
            $this->initAutorisations();
            $this->collAutorisationsPartial = true;
        }

        if (!$this->collAutorisations->contains($l)) {
            $this->doAddAutorisation($l);

            if ($this->autorisationsScheduledForDeletion and $this->autorisationsScheduledForDeletion->contains($l)) {
                $this->autorisationsScheduledForDeletion->remove($this->autorisationsScheduledForDeletion->search($l));
            }
        }

        return $this;
    }

    /**
     * @param ChildAutorisation $autorisation The ChildAutorisation object to add.
     */
    protected function doAddAutorisation(ChildAutorisation $autorisation)
    {
        $this->collAutorisations[]= $autorisation;
        $autorisation->setEntite($this);
    }

    /**
     * @param  ChildAutorisation $autorisation The ChildAutorisation object to remove.
     * @return $this|ChildEntite The current object (for fluent API support)
     */
    public function removeAutorisation(ChildAutorisation $autorisation)
    {
        if ($this->getAutorisations()->contains($autorisation)) {
            $pos = $this->collAutorisations->search($autorisation);
            $this->collAutorisations->remove($pos);
            if (null === $this->autorisationsScheduledForDeletion) {
                $this->autorisationsScheduledForDeletion = clone $this->collAutorisations;
                $this->autorisationsScheduledForDeletion->clear();
            }
            $this->autorisationsScheduledForDeletion[]= clone $autorisation;
            $autorisation->setEntite(null);
        }

        return $this;
    }


    /**
     * If this collection has already been initialized with
     * an identical criteria, it returns the collection.
     * Otherwise if this Entite is new, it will return
     * an empty collection; or if this Entite has previously
     * been saved, it will retrieve related Autorisations from storage.
     *
     * This method is protected by default in order to keep the public
     * api reasonable.  You can provide public methods for those you
     * actually need in Entite.
     *
     * @param      Criteria $criteria optional Criteria object to narrow the query
     * @param      ConnectionInterface $con optional connection object
     * @param      string $joinBehavior optional join type to use (defaults to Criteria::LEFT_JOIN)
     * @return ObjectCollection|ChildAutorisation[] List of ChildAutorisation objects
     */
    public function getAutorisationsJoinIndividu(Criteria $criteria = null, ConnectionInterface $con = null, $joinBehavior = Criteria::LEFT_JOIN)
    {
        $query = ChildAutorisationQuery::create(null, $criteria);
        $query->joinWith('Individu', $joinBehavior);

        return $this->getAutorisations($query, $con);
    }

    /**
     * Clears out the collPaiements collection
     *
     * This does not modify the database; however, it will remove any associated objects, causing
     * them to be refetched by subsequent calls to accessor method.
     *
     * @return void
     * @see        addPaiements()
     */
    public function clearPaiements()
    {
        $this->collPaiements = null; // important to set this to NULL since that means it is uninitialized
    }

    /**
     * Reset is the collPaiements collection loaded partially.
     */
    public function resetPartialPaiements($v = true)
    {
        $this->collPaiementsPartial = $v;
    }

    /**
     * Initializes the collPaiements collection.
     *
     * By default this just sets the collPaiements collection to an empty array (like clearcollPaiements());
     * however, you may wish to override this method in your stub class to provide setting appropriate
     * to your application -- for example, setting the initial array to the values stored in database.
     *
     * @param      boolean $overrideExisting If set to true, the method call initializes
     *                                        the collection even if it is not empty
     *
     * @return void
     */
    public function initPaiements($overrideExisting = true)
    {
        if (null !== $this->collPaiements && !$overrideExisting) {
            return;
        }

        $collectionClassName = PaiementTableMap::getTableMap()->getCollectionClassName();

        $this->collPaiements = new $collectionClassName;
        $this->collPaiements->setModel('\Paiement');
    }

    /**
     * Gets an array of ChildPaiement objects which contain a foreign key that references this object.
     *
     * If the $criteria is not null, it is used to always fetch the results from the database.
     * Otherwise the results are fetched from the database the first time, then cached.
     * Next time the same method is called without $criteria, the cached collection is returned.
     * If this ChildEntite is new, it will return
     * an empty collection or the current collection; the criteria is ignored on a new object.
     *
     * @param      Criteria $criteria optional Criteria object to narrow the query
     * @param      ConnectionInterface $con optional connection object
     * @return ObjectCollection|ChildPaiement[] List of ChildPaiement objects
     * @throws PropelException
     */
    public function getPaiements(Criteria $criteria = null, ConnectionInterface $con = null)
    {
        $partial = $this->collPaiementsPartial && !$this->isNew();
        if (null === $this->collPaiements || null !== $criteria  || $partial) {
            if ($this->isNew() && null === $this->collPaiements) {
                // return empty collection
                $this->initPaiements();
            } else {
                $collPaiements = ChildPaiementQuery::create(null, $criteria)
                    ->filterByEntite($this)
                    ->find($con);

                if (null !== $criteria) {
                    if (false !== $this->collPaiementsPartial && count($collPaiements)) {
                        $this->initPaiements(false);

                        foreach ($collPaiements as $obj) {
                            if (false == $this->collPaiements->contains($obj)) {
                                $this->collPaiements->append($obj);
                            }
                        }

                        $this->collPaiementsPartial = true;
                    }

                    return $collPaiements;
                }

                if ($partial && $this->collPaiements) {
                    foreach ($this->collPaiements as $obj) {
                        if ($obj->isNew()) {
                            $collPaiements[] = $obj;
                        }
                    }
                }

                $this->collPaiements = $collPaiements;
                $this->collPaiementsPartial = false;
            }
        }

        return $this->collPaiements;
    }

    /**
     * Sets a collection of ChildPaiement objects related by a one-to-many relationship
     * to the current object.
     * It will also schedule objects for deletion based on a diff between old objects (aka persisted)
     * and new objects from the given Propel collection.
     *
     * @param      Collection $paiements A Propel collection.
     * @param      ConnectionInterface $con Optional connection object
     * @return $this|ChildEntite The current object (for fluent API support)
     */
    public function setPaiements(Collection $paiements, ConnectionInterface $con = null)
    {
        /** @var ChildPaiement[] $paiementsToDelete */
        $paiementsToDelete = $this->getPaiements(new Criteria(), $con)->diff($paiements);


        $this->paiementsScheduledForDeletion = $paiementsToDelete;

        foreach ($paiementsToDelete as $paiementRemoved) {
            $paiementRemoved->setEntite(null);
        }

        $this->collPaiements = null;
        foreach ($paiements as $paiement) {
            $this->addPaiement($paiement);
        }

        $this->collPaiements = $paiements;
        $this->collPaiementsPartial = false;

        return $this;
    }

    /**
     * Returns the number of related Paiement objects.
     *
     * @param      Criteria $criteria
     * @param      boolean $distinct
     * @param      ConnectionInterface $con
     * @return int             Count of related Paiement objects.
     * @throws PropelException
     */
    public function countPaiements(Criteria $criteria = null, $distinct = false, ConnectionInterface $con = null)
    {
        $partial = $this->collPaiementsPartial && !$this->isNew();
        if (null === $this->collPaiements || null !== $criteria || $partial) {
            if ($this->isNew() && null === $this->collPaiements) {
                return 0;
            }

            if ($partial && !$criteria) {
                return count($this->getPaiements());
            }

            $query = ChildPaiementQuery::create(null, $criteria);
            if ($distinct) {
                $query->distinct();
            }

            return $query
                ->filterByEntite($this)
                ->count($con);
        }

        return count($this->collPaiements);
    }

    /**
     * Method called to associate a ChildPaiement object to this object
     * through the ChildPaiement foreign key attribute.
     *
     * @param  ChildPaiement $l ChildPaiement
     * @return $this|\Entite The current object (for fluent API support)
     */
    public function addPaiement(ChildPaiement $l)
    {
        if ($this->collPaiements === null) {
            $this->initPaiements();
            $this->collPaiementsPartial = true;
        }

        if (!$this->collPaiements->contains($l)) {
            $this->doAddPaiement($l);

            if ($this->paiementsScheduledForDeletion and $this->paiementsScheduledForDeletion->contains($l)) {
                $this->paiementsScheduledForDeletion->remove($this->paiementsScheduledForDeletion->search($l));
            }
        }

        return $this;
    }

    /**
     * @param ChildPaiement $paiement The ChildPaiement object to add.
     */
    protected function doAddPaiement(ChildPaiement $paiement)
    {
        $this->collPaiements[]= $paiement;
        $paiement->setEntite($this);
    }

    /**
     * @param  ChildPaiement $paiement The ChildPaiement object to remove.
     * @return $this|ChildEntite The current object (for fluent API support)
     */
    public function removePaiement(ChildPaiement $paiement)
    {
        if ($this->getPaiements()->contains($paiement)) {
            $pos = $this->collPaiements->search($paiement);
            $this->collPaiements->remove($pos);
            if (null === $this->paiementsScheduledForDeletion) {
                $this->paiementsScheduledForDeletion = clone $this->collPaiements;
                $this->paiementsScheduledForDeletion->clear();
            }
            $this->paiementsScheduledForDeletion[]= clone $paiement;
            $paiement->setEntite(null);
        }

        return $this;
    }


    /**
     * If this collection has already been initialized with
     * an identical criteria, it returns the collection.
     * Otherwise if this Entite is new, it will return
     * an empty collection; or if this Entite has previously
     * been saved, it will retrieve related Paiements from storage.
     *
     * This method is protected by default in order to keep the public
     * api reasonable.  You can provide public methods for those you
     * actually need in Entite.
     *
     * @param      Criteria $criteria optional Criteria object to narrow the query
     * @param      ConnectionInterface $con optional connection object
     * @param      string $joinBehavior optional join type to use (defaults to Criteria::LEFT_JOIN)
     * @return ObjectCollection|ChildPaiement[] List of ChildPaiement objects
     */
    public function getPaiementsJoinTresor(Criteria $criteria = null, ConnectionInterface $con = null, $joinBehavior = Criteria::LEFT_JOIN)
    {
        $query = ChildPaiementQuery::create(null, $criteria);
        $query->joinWith('Tresor', $joinBehavior);

        return $this->getPaiements($query, $con);
    }

    /**
     * Clears out the collPrestations collection
     *
     * This does not modify the database; however, it will remove any associated objects, causing
     * them to be refetched by subsequent calls to accessor method.
     *
     * @return void
     * @see        addPrestations()
     */
    public function clearPrestations()
    {
        $this->collPrestations = null; // important to set this to NULL since that means it is uninitialized
    }

    /**
     * Reset is the collPrestations collection loaded partially.
     */
    public function resetPartialPrestations($v = true)
    {
        $this->collPrestationsPartial = $v;
    }

    /**
     * Initializes the collPrestations collection.
     *
     * By default this just sets the collPrestations collection to an empty array (like clearcollPrestations());
     * however, you may wish to override this method in your stub class to provide setting appropriate
     * to your application -- for example, setting the initial array to the values stored in database.
     *
     * @param      boolean $overrideExisting If set to true, the method call initializes
     *                                        the collection even if it is not empty
     *
     * @return void
     */
    public function initPrestations($overrideExisting = true)
    {
        if (null !== $this->collPrestations && !$overrideExisting) {
            return;
        }

        $collectionClassName = PrestationTableMap::getTableMap()->getCollectionClassName();

        $this->collPrestations = new $collectionClassName;
        $this->collPrestations->setModel('\Prestation');
    }

    /**
     * Gets an array of ChildPrestation objects which contain a foreign key that references this object.
     *
     * If the $criteria is not null, it is used to always fetch the results from the database.
     * Otherwise the results are fetched from the database the first time, then cached.
     * Next time the same method is called without $criteria, the cached collection is returned.
     * If this ChildEntite is new, it will return
     * an empty collection or the current collection; the criteria is ignored on a new object.
     *
     * @param      Criteria $criteria optional Criteria object to narrow the query
     * @param      ConnectionInterface $con optional connection object
     * @return ObjectCollection|ChildPrestation[] List of ChildPrestation objects
     * @throws PropelException
     */
    public function getPrestations(Criteria $criteria = null, ConnectionInterface $con = null)
    {
        $partial = $this->collPrestationsPartial && !$this->isNew();
        if (null === $this->collPrestations || null !== $criteria  || $partial) {
            if ($this->isNew() && null === $this->collPrestations) {
                // return empty collection
                $this->initPrestations();
            } else {
                $collPrestations = ChildPrestationQuery::create(null, $criteria)
                    ->filterByEntite($this)
                    ->find($con);

                if (null !== $criteria) {
                    if (false !== $this->collPrestationsPartial && count($collPrestations)) {
                        $this->initPrestations(false);

                        foreach ($collPrestations as $obj) {
                            if (false == $this->collPrestations->contains($obj)) {
                                $this->collPrestations->append($obj);
                            }
                        }

                        $this->collPrestationsPartial = true;
                    }

                    return $collPrestations;
                }

                if ($partial && $this->collPrestations) {
                    foreach ($this->collPrestations as $obj) {
                        if ($obj->isNew()) {
                            $collPrestations[] = $obj;
                        }
                    }
                }

                $this->collPrestations = $collPrestations;
                $this->collPrestationsPartial = false;
            }
        }

        return $this->collPrestations;
    }

    /**
     * Sets a collection of ChildPrestation objects related by a one-to-many relationship
     * to the current object.
     * It will also schedule objects for deletion based on a diff between old objects (aka persisted)
     * and new objects from the given Propel collection.
     *
     * @param      Collection $prestations A Propel collection.
     * @param      ConnectionInterface $con Optional connection object
     * @return $this|ChildEntite The current object (for fluent API support)
     */
    public function setPrestations(Collection $prestations, ConnectionInterface $con = null)
    {
        /** @var ChildPrestation[] $prestationsToDelete */
        $prestationsToDelete = $this->getPrestations(new Criteria(), $con)->diff($prestations);


        $this->prestationsScheduledForDeletion = $prestationsToDelete;

        foreach ($prestationsToDelete as $prestationRemoved) {
            $prestationRemoved->setEntite(null);
        }

        $this->collPrestations = null;
        foreach ($prestations as $prestation) {
            $this->addPrestation($prestation);
        }

        $this->collPrestations = $prestations;
        $this->collPrestationsPartial = false;

        return $this;
    }

    /**
     * Returns the number of related Prestation objects.
     *
     * @param      Criteria $criteria
     * @param      boolean $distinct
     * @param      ConnectionInterface $con
     * @return int             Count of related Prestation objects.
     * @throws PropelException
     */
    public function countPrestations(Criteria $criteria = null, $distinct = false, ConnectionInterface $con = null)
    {
        $partial = $this->collPrestationsPartial && !$this->isNew();
        if (null === $this->collPrestations || null !== $criteria || $partial) {
            if ($this->isNew() && null === $this->collPrestations) {
                return 0;
            }

            if ($partial && !$criteria) {
                return count($this->getPrestations());
            }

            $query = ChildPrestationQuery::create(null, $criteria);
            if ($distinct) {
                $query->distinct();
            }

            return $query
                ->filterByEntite($this)
                ->count($con);
        }

        return count($this->collPrestations);
    }

    /**
     * Method called to associate a ChildPrestation object to this object
     * through the ChildPrestation foreign key attribute.
     *
     * @param  ChildPrestation $l ChildPrestation
     * @return $this|\Entite The current object (for fluent API support)
     */
    public function addPrestation(ChildPrestation $l)
    {
        if ($this->collPrestations === null) {
            $this->initPrestations();
            $this->collPrestationsPartial = true;
        }

        if (!$this->collPrestations->contains($l)) {
            $this->doAddPrestation($l);

            if ($this->prestationsScheduledForDeletion and $this->prestationsScheduledForDeletion->contains($l)) {
                $this->prestationsScheduledForDeletion->remove($this->prestationsScheduledForDeletion->search($l));
            }
        }

        return $this;
    }

    /**
     * @param ChildPrestation $prestation The ChildPrestation object to add.
     */
    protected function doAddPrestation(ChildPrestation $prestation)
    {
        $this->collPrestations[]= $prestation;
        $prestation->setEntite($this);
    }

    /**
     * @param  ChildPrestation $prestation The ChildPrestation object to remove.
     * @return $this|ChildEntite The current object (for fluent API support)
     */
    public function removePrestation(ChildPrestation $prestation)
    {
        if ($this->getPrestations()->contains($prestation)) {
            $pos = $this->collPrestations->search($prestation);
            $this->collPrestations->remove($pos);
            if (null === $this->prestationsScheduledForDeletion) {
                $this->prestationsScheduledForDeletion = clone $this->collPrestations;
                $this->prestationsScheduledForDeletion->clear();
            }
            $this->prestationsScheduledForDeletion[]= clone $prestation;
            $prestation->setEntite(null);
        }

        return $this;
    }


    /**
     * If this collection has already been initialized with
     * an identical criteria, it returns the collection.
     * Otherwise if this Entite is new, it will return
     * an empty collection; or if this Entite has previously
     * been saved, it will retrieve related Prestations from storage.
     *
     * This method is protected by default in order to keep the public
     * api reasonable.  You can provide public methods for those you
     * actually need in Entite.
     *
     * @param      Criteria $criteria optional Criteria object to narrow the query
     * @param      ConnectionInterface $con optional connection object
     * @param      string $joinBehavior optional join type to use (defaults to Criteria::LEFT_JOIN)
     * @return ObjectCollection|ChildPrestation[] List of ChildPrestation objects
     */
    public function getPrestationsJoinCompte(Criteria $criteria = null, ConnectionInterface $con = null, $joinBehavior = Criteria::LEFT_JOIN)
    {
        $query = ChildPrestationQuery::create(null, $criteria);
        $query->joinWith('Compte', $joinBehavior);

        return $this->getPrestations($query, $con);
    }

    /**
     * Clears out the collPrestationslots collection
     *
     * This does not modify the database; however, it will remove any associated objects, causing
     * them to be refetched by subsequent calls to accessor method.
     *
     * @return void
     * @see        addPrestationslots()
     */
    public function clearPrestationslots()
    {
        $this->collPrestationslots = null; // important to set this to NULL since that means it is uninitialized
    }

    /**
     * Reset is the collPrestationslots collection loaded partially.
     */
    public function resetPartialPrestationslots($v = true)
    {
        $this->collPrestationslotsPartial = $v;
    }

    /**
     * Initializes the collPrestationslots collection.
     *
     * By default this just sets the collPrestationslots collection to an empty array (like clearcollPrestationslots());
     * however, you may wish to override this method in your stub class to provide setting appropriate
     * to your application -- for example, setting the initial array to the values stored in database.
     *
     * @param      boolean $overrideExisting If set to true, the method call initializes
     *                                        the collection even if it is not empty
     *
     * @return void
     */
    public function initPrestationslots($overrideExisting = true)
    {
        if (null !== $this->collPrestationslots && !$overrideExisting) {
            return;
        }

        $collectionClassName = PrestationslotTableMap::getTableMap()->getCollectionClassName();

        $this->collPrestationslots = new $collectionClassName;
        $this->collPrestationslots->setModel('\Prestationslot');
    }

    /**
     * Gets an array of ChildPrestationslot objects which contain a foreign key that references this object.
     *
     * If the $criteria is not null, it is used to always fetch the results from the database.
     * Otherwise the results are fetched from the database the first time, then cached.
     * Next time the same method is called without $criteria, the cached collection is returned.
     * If this ChildEntite is new, it will return
     * an empty collection or the current collection; the criteria is ignored on a new object.
     *
     * @param      Criteria $criteria optional Criteria object to narrow the query
     * @param      ConnectionInterface $con optional connection object
     * @return ObjectCollection|ChildPrestationslot[] List of ChildPrestationslot objects
     * @throws PropelException
     */
    public function getPrestationslots(Criteria $criteria = null, ConnectionInterface $con = null)
    {
        $partial = $this->collPrestationslotsPartial && !$this->isNew();
        if (null === $this->collPrestationslots || null !== $criteria  || $partial) {
            if ($this->isNew() && null === $this->collPrestationslots) {
                // return empty collection
                $this->initPrestationslots();
            } else {
                $collPrestationslots = ChildPrestationslotQuery::create(null, $criteria)
                    ->filterByEntite($this)
                    ->find($con);

                if (null !== $criteria) {
                    if (false !== $this->collPrestationslotsPartial && count($collPrestationslots)) {
                        $this->initPrestationslots(false);

                        foreach ($collPrestationslots as $obj) {
                            if (false == $this->collPrestationslots->contains($obj)) {
                                $this->collPrestationslots->append($obj);
                            }
                        }

                        $this->collPrestationslotsPartial = true;
                    }

                    return $collPrestationslots;
                }

                if ($partial && $this->collPrestationslots) {
                    foreach ($this->collPrestationslots as $obj) {
                        if ($obj->isNew()) {
                            $collPrestationslots[] = $obj;
                        }
                    }
                }

                $this->collPrestationslots = $collPrestationslots;
                $this->collPrestationslotsPartial = false;
            }
        }

        return $this->collPrestationslots;
    }

    /**
     * Sets a collection of ChildPrestationslot objects related by a one-to-many relationship
     * to the current object.
     * It will also schedule objects for deletion based on a diff between old objects (aka persisted)
     * and new objects from the given Propel collection.
     *
     * @param      Collection $prestationslots A Propel collection.
     * @param      ConnectionInterface $con Optional connection object
     * @return $this|ChildEntite The current object (for fluent API support)
     */
    public function setPrestationslots(Collection $prestationslots, ConnectionInterface $con = null)
    {
        /** @var ChildPrestationslot[] $prestationslotsToDelete */
        $prestationslotsToDelete = $this->getPrestationslots(new Criteria(), $con)->diff($prestationslots);


        $this->prestationslotsScheduledForDeletion = $prestationslotsToDelete;

        foreach ($prestationslotsToDelete as $prestationslotRemoved) {
            $prestationslotRemoved->setEntite(null);
        }

        $this->collPrestationslots = null;
        foreach ($prestationslots as $prestationslot) {
            $this->addPrestationslot($prestationslot);
        }

        $this->collPrestationslots = $prestationslots;
        $this->collPrestationslotsPartial = false;

        return $this;
    }

    /**
     * Returns the number of related Prestationslot objects.
     *
     * @param      Criteria $criteria
     * @param      boolean $distinct
     * @param      ConnectionInterface $con
     * @return int             Count of related Prestationslot objects.
     * @throws PropelException
     */
    public function countPrestationslots(Criteria $criteria = null, $distinct = false, ConnectionInterface $con = null)
    {
        $partial = $this->collPrestationslotsPartial && !$this->isNew();
        if (null === $this->collPrestationslots || null !== $criteria || $partial) {
            if ($this->isNew() && null === $this->collPrestationslots) {
                return 0;
            }

            if ($partial && !$criteria) {
                return count($this->getPrestationslots());
            }

            $query = ChildPrestationslotQuery::create(null, $criteria);
            if ($distinct) {
                $query->distinct();
            }

            return $query
                ->filterByEntite($this)
                ->count($con);
        }

        return count($this->collPrestationslots);
    }

    /**
     * Method called to associate a ChildPrestationslot object to this object
     * through the ChildPrestationslot foreign key attribute.
     *
     * @param  ChildPrestationslot $l ChildPrestationslot
     * @return $this|\Entite The current object (for fluent API support)
     */
    public function addPrestationslot(ChildPrestationslot $l)
    {
        if ($this->collPrestationslots === null) {
            $this->initPrestationslots();
            $this->collPrestationslotsPartial = true;
        }

        if (!$this->collPrestationslots->contains($l)) {
            $this->doAddPrestationslot($l);

            if ($this->prestationslotsScheduledForDeletion and $this->prestationslotsScheduledForDeletion->contains($l)) {
                $this->prestationslotsScheduledForDeletion->remove($this->prestationslotsScheduledForDeletion->search($l));
            }
        }

        return $this;
    }

    /**
     * @param ChildPrestationslot $prestationslot The ChildPrestationslot object to add.
     */
    protected function doAddPrestationslot(ChildPrestationslot $prestationslot)
    {
        $this->collPrestationslots[]= $prestationslot;
        $prestationslot->setEntite($this);
    }

    /**
     * @param  ChildPrestationslot $prestationslot The ChildPrestationslot object to remove.
     * @return $this|ChildEntite The current object (for fluent API support)
     */
    public function removePrestationslot(ChildPrestationslot $prestationslot)
    {
        if ($this->getPrestationslots()->contains($prestationslot)) {
            $pos = $this->collPrestationslots->search($prestationslot);
            $this->collPrestationslots->remove($pos);
            if (null === $this->prestationslotsScheduledForDeletion) {
                $this->prestationslotsScheduledForDeletion = clone $this->collPrestationslots;
                $this->prestationslotsScheduledForDeletion->clear();
            }
            $this->prestationslotsScheduledForDeletion[]= clone $prestationslot;
            $prestationslot->setEntite(null);
        }

        return $this;
    }

    /**
     * Clears out the collServicerendus collection
     *
     * This does not modify the database; however, it will remove any associated objects, causing
     * them to be refetched by subsequent calls to accessor method.
     *
     * @return void
     * @see        addServicerendus()
     */
    public function clearServicerendus()
    {
        $this->collServicerendus = null; // important to set this to NULL since that means it is uninitialized
    }

    /**
     * Reset is the collServicerendus collection loaded partially.
     */
    public function resetPartialServicerendus($v = true)
    {
        $this->collServicerendusPartial = $v;
    }

    /**
     * Initializes the collServicerendus collection.
     *
     * By default this just sets the collServicerendus collection to an empty array (like clearcollServicerendus());
     * however, you may wish to override this method in your stub class to provide setting appropriate
     * to your application -- for example, setting the initial array to the values stored in database.
     *
     * @param      boolean $overrideExisting If set to true, the method call initializes
     *                                        the collection even if it is not empty
     *
     * @return void
     */
    public function initServicerendus($overrideExisting = true)
    {
        if (null !== $this->collServicerendus && !$overrideExisting) {
            return;
        }

        $collectionClassName = ServicerenduTableMap::getTableMap()->getCollectionClassName();

        $this->collServicerendus = new $collectionClassName;
        $this->collServicerendus->setModel('\Servicerendu');
    }

    /**
     * Gets an array of ChildServicerendu objects which contain a foreign key that references this object.
     *
     * If the $criteria is not null, it is used to always fetch the results from the database.
     * Otherwise the results are fetched from the database the first time, then cached.
     * Next time the same method is called without $criteria, the cached collection is returned.
     * If this ChildEntite is new, it will return
     * an empty collection or the current collection; the criteria is ignored on a new object.
     *
     * @param      Criteria $criteria optional Criteria object to narrow the query
     * @param      ConnectionInterface $con optional connection object
     * @return ObjectCollection|ChildServicerendu[] List of ChildServicerendu objects
     * @throws PropelException
     */
    public function getServicerendus(Criteria $criteria = null, ConnectionInterface $con = null)
    {
        $partial = $this->collServicerendusPartial && !$this->isNew();
        if (null === $this->collServicerendus || null !== $criteria  || $partial) {
            if ($this->isNew() && null === $this->collServicerendus) {
                // return empty collection
                $this->initServicerendus();
            } else {
                $collServicerendus = ChildServicerenduQuery::create(null, $criteria)
                    ->filterByEntite($this)
                    ->find($con);

                if (null !== $criteria) {
                    if (false !== $this->collServicerendusPartial && count($collServicerendus)) {
                        $this->initServicerendus(false);

                        foreach ($collServicerendus as $obj) {
                            if (false == $this->collServicerendus->contains($obj)) {
                                $this->collServicerendus->append($obj);
                            }
                        }

                        $this->collServicerendusPartial = true;
                    }

                    return $collServicerendus;
                }

                if ($partial && $this->collServicerendus) {
                    foreach ($this->collServicerendus as $obj) {
                        if ($obj->isNew()) {
                            $collServicerendus[] = $obj;
                        }
                    }
                }

                $this->collServicerendus = $collServicerendus;
                $this->collServicerendusPartial = false;
            }
        }

        return $this->collServicerendus;
    }

    /**
     * Sets a collection of ChildServicerendu objects related by a one-to-many relationship
     * to the current object.
     * It will also schedule objects for deletion based on a diff between old objects (aka persisted)
     * and new objects from the given Propel collection.
     *
     * @param      Collection $servicerendus A Propel collection.
     * @param      ConnectionInterface $con Optional connection object
     * @return $this|ChildEntite The current object (for fluent API support)
     */
    public function setServicerendus(Collection $servicerendus, ConnectionInterface $con = null)
    {
        /** @var ChildServicerendu[] $servicerendusToDelete */
        $servicerendusToDelete = $this->getServicerendus(new Criteria(), $con)->diff($servicerendus);


        $this->servicerendusScheduledForDeletion = $servicerendusToDelete;

        foreach ($servicerendusToDelete as $servicerenduRemoved) {
            $servicerenduRemoved->setEntite(null);
        }

        $this->collServicerendus = null;
        foreach ($servicerendus as $servicerendu) {
            $this->addServicerendu($servicerendu);
        }

        $this->collServicerendus = $servicerendus;
        $this->collServicerendusPartial = false;

        return $this;
    }

    /**
     * Returns the number of related Servicerendu objects.
     *
     * @param      Criteria $criteria
     * @param      boolean $distinct
     * @param      ConnectionInterface $con
     * @return int             Count of related Servicerendu objects.
     * @throws PropelException
     */
    public function countServicerendus(Criteria $criteria = null, $distinct = false, ConnectionInterface $con = null)
    {
        $partial = $this->collServicerendusPartial && !$this->isNew();
        if (null === $this->collServicerendus || null !== $criteria || $partial) {
            if ($this->isNew() && null === $this->collServicerendus) {
                return 0;
            }

            if ($partial && !$criteria) {
                return count($this->getServicerendus());
            }

            $query = ChildServicerenduQuery::create(null, $criteria);
            if ($distinct) {
                $query->distinct();
            }

            return $query
                ->filterByEntite($this)
                ->count($con);
        }

        return count($this->collServicerendus);
    }

    /**
     * Method called to associate a ChildServicerendu object to this object
     * through the ChildServicerendu foreign key attribute.
     *
     * @param  ChildServicerendu $l ChildServicerendu
     * @return $this|\Entite The current object (for fluent API support)
     */
    public function addServicerendu(ChildServicerendu $l)
    {
        if ($this->collServicerendus === null) {
            $this->initServicerendus();
            $this->collServicerendusPartial = true;
        }

        if (!$this->collServicerendus->contains($l)) {
            $this->doAddServicerendu($l);

            if ($this->servicerendusScheduledForDeletion and $this->servicerendusScheduledForDeletion->contains($l)) {
                $this->servicerendusScheduledForDeletion->remove($this->servicerendusScheduledForDeletion->search($l));
            }
        }

        return $this;
    }

    /**
     * @param ChildServicerendu $servicerendu The ChildServicerendu object to add.
     */
    protected function doAddServicerendu(ChildServicerendu $servicerendu)
    {
        $this->collServicerendus[]= $servicerendu;
        $servicerendu->setEntite($this);
    }

    /**
     * @param  ChildServicerendu $servicerendu The ChildServicerendu object to remove.
     * @return $this|ChildEntite The current object (for fluent API support)
     */
    public function removeServicerendu(ChildServicerendu $servicerendu)
    {
        if ($this->getServicerendus()->contains($servicerendu)) {
            $pos = $this->collServicerendus->search($servicerendu);
            $this->collServicerendus->remove($pos);
            if (null === $this->servicerendusScheduledForDeletion) {
                $this->servicerendusScheduledForDeletion = clone $this->collServicerendus;
                $this->servicerendusScheduledForDeletion->clear();
            }
            $this->servicerendusScheduledForDeletion[]= clone $servicerendu;
            $servicerendu->setEntite(null);
        }

        return $this;
    }


    /**
     * If this collection has already been initialized with
     * an identical criteria, it returns the collection.
     * Otherwise if this Entite is new, it will return
     * an empty collection; or if this Entite has previously
     * been saved, it will retrieve related Servicerendus from storage.
     *
     * This method is protected by default in order to keep the public
     * api reasonable.  You can provide public methods for those you
     * actually need in Entite.
     *
     * @param      Criteria $criteria optional Criteria object to narrow the query
     * @param      ConnectionInterface $con optional connection object
     * @param      string $joinBehavior optional join type to use (defaults to Criteria::LEFT_JOIN)
     * @return ObjectCollection|ChildServicerendu[] List of ChildServicerendu objects
     */
    public function getServicerendusJoinPrestation(Criteria $criteria = null, ConnectionInterface $con = null, $joinBehavior = Criteria::LEFT_JOIN)
    {
        $query = ChildServicerenduQuery::create(null, $criteria);
        $query->joinWith('Prestation', $joinBehavior);

        return $this->getServicerendus($query, $con);
    }


    /**
     * If this collection has already been initialized with
     * an identical criteria, it returns the collection.
     * Otherwise if this Entite is new, it will return
     * an empty collection; or if this Entite has previously
     * been saved, it will retrieve related Servicerendus from storage.
     *
     * This method is protected by default in order to keep the public
     * api reasonable.  You can provide public methods for those you
     * actually need in Entite.
     *
     * @param      Criteria $criteria optional Criteria object to narrow the query
     * @param      ConnectionInterface $con optional connection object
     * @param      string $joinBehavior optional join type to use (defaults to Criteria::LEFT_JOIN)
     * @return ObjectCollection|ChildServicerendu[] List of ChildServicerendu objects
     */
    public function getServicerendusJoinMembre(Criteria $criteria = null, ConnectionInterface $con = null, $joinBehavior = Criteria::LEFT_JOIN)
    {
        $query = ChildServicerenduQuery::create(null, $criteria);
        $query->joinWith('Membre', $joinBehavior);

        return $this->getServicerendus($query, $con);
    }


    /**
     * If this collection has already been initialized with
     * an identical criteria, it returns the collection.
     * Otherwise if this Entite is new, it will return
     * an empty collection; or if this Entite has previously
     * been saved, it will retrieve related Servicerendus from storage.
     *
     * This method is protected by default in order to keep the public
     * api reasonable.  You can provide public methods for those you
     * actually need in Entite.
     *
     * @param      Criteria $criteria optional Criteria object to narrow the query
     * @param      ConnectionInterface $con optional connection object
     * @param      string $joinBehavior optional join type to use (defaults to Criteria::LEFT_JOIN)
     * @return ObjectCollection|ChildServicerendu[] List of ChildServicerendu objects
     */
    public function getServicerendusJoinServicerenduRelatedByOrigine(Criteria $criteria = null, ConnectionInterface $con = null, $joinBehavior = Criteria::LEFT_JOIN)
    {
        $query = ChildServicerenduQuery::create(null, $criteria);
        $query->joinWith('ServicerenduRelatedByOrigine', $joinBehavior);

        return $this->getServicerendus($query, $con);
    }

    /**
     * Clears out the collTresors collection
     *
     * This does not modify the database; however, it will remove any associated objects, causing
     * them to be refetched by subsequent calls to accessor method.
     *
     * @return void
     * @see        addTresors()
     */
    public function clearTresors()
    {
        $this->collTresors = null; // important to set this to NULL since that means it is uninitialized
    }

    /**
     * Reset is the collTresors collection loaded partially.
     */
    public function resetPartialTresors($v = true)
    {
        $this->collTresorsPartial = $v;
    }

    /**
     * Initializes the collTresors collection.
     *
     * By default this just sets the collTresors collection to an empty array (like clearcollTresors());
     * however, you may wish to override this method in your stub class to provide setting appropriate
     * to your application -- for example, setting the initial array to the values stored in database.
     *
     * @param      boolean $overrideExisting If set to true, the method call initializes
     *                                        the collection even if it is not empty
     *
     * @return void
     */
    public function initTresors($overrideExisting = true)
    {
        if (null !== $this->collTresors && !$overrideExisting) {
            return;
        }

        $collectionClassName = TresorTableMap::getTableMap()->getCollectionClassName();

        $this->collTresors = new $collectionClassName;
        $this->collTresors->setModel('\Tresor');
    }

    /**
     * Gets an array of ChildTresor objects which contain a foreign key that references this object.
     *
     * If the $criteria is not null, it is used to always fetch the results from the database.
     * Otherwise the results are fetched from the database the first time, then cached.
     * Next time the same method is called without $criteria, the cached collection is returned.
     * If this ChildEntite is new, it will return
     * an empty collection or the current collection; the criteria is ignored on a new object.
     *
     * @param      Criteria $criteria optional Criteria object to narrow the query
     * @param      ConnectionInterface $con optional connection object
     * @return ObjectCollection|ChildTresor[] List of ChildTresor objects
     * @throws PropelException
     */
    public function getTresors(Criteria $criteria = null, ConnectionInterface $con = null)
    {
        $partial = $this->collTresorsPartial && !$this->isNew();
        if (null === $this->collTresors || null !== $criteria  || $partial) {
            if ($this->isNew() && null === $this->collTresors) {
                // return empty collection
                $this->initTresors();
            } else {
                $collTresors = ChildTresorQuery::create(null, $criteria)
                    ->filterByEntite($this)
                    ->find($con);

                if (null !== $criteria) {
                    if (false !== $this->collTresorsPartial && count($collTresors)) {
                        $this->initTresors(false);

                        foreach ($collTresors as $obj) {
                            if (false == $this->collTresors->contains($obj)) {
                                $this->collTresors->append($obj);
                            }
                        }

                        $this->collTresorsPartial = true;
                    }

                    return $collTresors;
                }

                if ($partial && $this->collTresors) {
                    foreach ($this->collTresors as $obj) {
                        if ($obj->isNew()) {
                            $collTresors[] = $obj;
                        }
                    }
                }

                $this->collTresors = $collTresors;
                $this->collTresorsPartial = false;
            }
        }

        return $this->collTresors;
    }

    /**
     * Sets a collection of ChildTresor objects related by a one-to-many relationship
     * to the current object.
     * It will also schedule objects for deletion based on a diff between old objects (aka persisted)
     * and new objects from the given Propel collection.
     *
     * @param      Collection $tresors A Propel collection.
     * @param      ConnectionInterface $con Optional connection object
     * @return $this|ChildEntite The current object (for fluent API support)
     */
    public function setTresors(Collection $tresors, ConnectionInterface $con = null)
    {
        /** @var ChildTresor[] $tresorsToDelete */
        $tresorsToDelete = $this->getTresors(new Criteria(), $con)->diff($tresors);


        $this->tresorsScheduledForDeletion = $tresorsToDelete;

        foreach ($tresorsToDelete as $tresorRemoved) {
            $tresorRemoved->setEntite(null);
        }

        $this->collTresors = null;
        foreach ($tresors as $tresor) {
            $this->addTresor($tresor);
        }

        $this->collTresors = $tresors;
        $this->collTresorsPartial = false;

        return $this;
    }

    /**
     * Returns the number of related Tresor objects.
     *
     * @param      Criteria $criteria
     * @param      boolean $distinct
     * @param      ConnectionInterface $con
     * @return int             Count of related Tresor objects.
     * @throws PropelException
     */
    public function countTresors(Criteria $criteria = null, $distinct = false, ConnectionInterface $con = null)
    {
        $partial = $this->collTresorsPartial && !$this->isNew();
        if (null === $this->collTresors || null !== $criteria || $partial) {
            if ($this->isNew() && null === $this->collTresors) {
                return 0;
            }

            if ($partial && !$criteria) {
                return count($this->getTresors());
            }

            $query = ChildTresorQuery::create(null, $criteria);
            if ($distinct) {
                $query->distinct();
            }

            return $query
                ->filterByEntite($this)
                ->count($con);
        }

        return count($this->collTresors);
    }

    /**
     * Method called to associate a ChildTresor object to this object
     * through the ChildTresor foreign key attribute.
     *
     * @param  ChildTresor $l ChildTresor
     * @return $this|\Entite The current object (for fluent API support)
     */
    public function addTresor(ChildTresor $l)
    {
        if ($this->collTresors === null) {
            $this->initTresors();
            $this->collTresorsPartial = true;
        }

        if (!$this->collTresors->contains($l)) {
            $this->doAddTresor($l);

            if ($this->tresorsScheduledForDeletion and $this->tresorsScheduledForDeletion->contains($l)) {
                $this->tresorsScheduledForDeletion->remove($this->tresorsScheduledForDeletion->search($l));
            }
        }

        return $this;
    }

    /**
     * @param ChildTresor $tresor The ChildTresor object to add.
     */
    protected function doAddTresor(ChildTresor $tresor)
    {
        $this->collTresors[]= $tresor;
        $tresor->setEntite($this);
    }

    /**
     * @param  ChildTresor $tresor The ChildTresor object to remove.
     * @return $this|ChildEntite The current object (for fluent API support)
     */
    public function removeTresor(ChildTresor $tresor)
    {
        if ($this->getTresors()->contains($tresor)) {
            $pos = $this->collTresors->search($tresor);
            $this->collTresors->remove($pos);
            if (null === $this->tresorsScheduledForDeletion) {
                $this->tresorsScheduledForDeletion = clone $this->collTresors;
                $this->tresorsScheduledForDeletion->clear();
            }
            $this->tresorsScheduledForDeletion[]= clone $tresor;
            $tresor->setEntite(null);
        }

        return $this;
    }


    /**
     * If this collection has already been initialized with
     * an identical criteria, it returns the collection.
     * Otherwise if this Entite is new, it will return
     * an empty collection; or if this Entite has previously
     * been saved, it will retrieve related Tresors from storage.
     *
     * This method is protected by default in order to keep the public
     * api reasonable.  You can provide public methods for those you
     * actually need in Entite.
     *
     * @param      Criteria $criteria optional Criteria object to narrow the query
     * @param      ConnectionInterface $con optional connection object
     * @param      string $joinBehavior optional join type to use (defaults to Criteria::LEFT_JOIN)
     * @return ObjectCollection|ChildTresor[] List of ChildTresor objects
     */
    public function getTresorsJoinCompte(Criteria $criteria = null, ConnectionInterface $con = null, $joinBehavior = Criteria::LEFT_JOIN)
    {
        $query = ChildTresorQuery::create(null, $criteria);
        $query->joinWith('Compte', $joinBehavior);

        return $this->getTresors($query, $con);
    }

    /**
     * Clears out the collTvas collection
     *
     * This does not modify the database; however, it will remove any associated objects, causing
     * them to be refetched by subsequent calls to accessor method.
     *
     * @return void
     * @see        addTvas()
     */
    public function clearTvas()
    {
        $this->collTvas = null; // important to set this to NULL since that means it is uninitialized
    }

    /**
     * Reset is the collTvas collection loaded partially.
     */
    public function resetPartialTvas($v = true)
    {
        $this->collTvasPartial = $v;
    }

    /**
     * Initializes the collTvas collection.
     *
     * By default this just sets the collTvas collection to an empty array (like clearcollTvas());
     * however, you may wish to override this method in your stub class to provide setting appropriate
     * to your application -- for example, setting the initial array to the values stored in database.
     *
     * @param      boolean $overrideExisting If set to true, the method call initializes
     *                                        the collection even if it is not empty
     *
     * @return void
     */
    public function initTvas($overrideExisting = true)
    {
        if (null !== $this->collTvas && !$overrideExisting) {
            return;
        }

        $collectionClassName = TvaTableMap::getTableMap()->getCollectionClassName();

        $this->collTvas = new $collectionClassName;
        $this->collTvas->setModel('\Tva');
    }

    /**
     * Gets an array of ChildTva objects which contain a foreign key that references this object.
     *
     * If the $criteria is not null, it is used to always fetch the results from the database.
     * Otherwise the results are fetched from the database the first time, then cached.
     * Next time the same method is called without $criteria, the cached collection is returned.
     * If this ChildEntite is new, it will return
     * an empty collection or the current collection; the criteria is ignored on a new object.
     *
     * @param      Criteria $criteria optional Criteria object to narrow the query
     * @param      ConnectionInterface $con optional connection object
     * @return ObjectCollection|ChildTva[] List of ChildTva objects
     * @throws PropelException
     */
    public function getTvas(Criteria $criteria = null, ConnectionInterface $con = null)
    {
        $partial = $this->collTvasPartial && !$this->isNew();
        if (null === $this->collTvas || null !== $criteria  || $partial) {
            if ($this->isNew() && null === $this->collTvas) {
                // return empty collection
                $this->initTvas();
            } else {
                $collTvas = ChildTvaQuery::create(null, $criteria)
                    ->filterByEntite($this)
                    ->find($con);

                if (null !== $criteria) {
                    if (false !== $this->collTvasPartial && count($collTvas)) {
                        $this->initTvas(false);

                        foreach ($collTvas as $obj) {
                            if (false == $this->collTvas->contains($obj)) {
                                $this->collTvas->append($obj);
                            }
                        }

                        $this->collTvasPartial = true;
                    }

                    return $collTvas;
                }

                if ($partial && $this->collTvas) {
                    foreach ($this->collTvas as $obj) {
                        if ($obj->isNew()) {
                            $collTvas[] = $obj;
                        }
                    }
                }

                $this->collTvas = $collTvas;
                $this->collTvasPartial = false;
            }
        }

        return $this->collTvas;
    }

    /**
     * Sets a collection of ChildTva objects related by a one-to-many relationship
     * to the current object.
     * It will also schedule objects for deletion based on a diff between old objects (aka persisted)
     * and new objects from the given Propel collection.
     *
     * @param      Collection $tvas A Propel collection.
     * @param      ConnectionInterface $con Optional connection object
     * @return $this|ChildEntite The current object (for fluent API support)
     */
    public function setTvas(Collection $tvas, ConnectionInterface $con = null)
    {
        /** @var ChildTva[] $tvasToDelete */
        $tvasToDelete = $this->getTvas(new Criteria(), $con)->diff($tvas);


        $this->tvasScheduledForDeletion = $tvasToDelete;

        foreach ($tvasToDelete as $tvaRemoved) {
            $tvaRemoved->setEntite(null);
        }

        $this->collTvas = null;
        foreach ($tvas as $tva) {
            $this->addTva($tva);
        }

        $this->collTvas = $tvas;
        $this->collTvasPartial = false;

        return $this;
    }

    /**
     * Returns the number of related Tva objects.
     *
     * @param      Criteria $criteria
     * @param      boolean $distinct
     * @param      ConnectionInterface $con
     * @return int             Count of related Tva objects.
     * @throws PropelException
     */
    public function countTvas(Criteria $criteria = null, $distinct = false, ConnectionInterface $con = null)
    {
        $partial = $this->collTvasPartial && !$this->isNew();
        if (null === $this->collTvas || null !== $criteria || $partial) {
            if ($this->isNew() && null === $this->collTvas) {
                return 0;
            }

            if ($partial && !$criteria) {
                return count($this->getTvas());
            }

            $query = ChildTvaQuery::create(null, $criteria);
            if ($distinct) {
                $query->distinct();
            }

            return $query
                ->filterByEntite($this)
                ->count($con);
        }

        return count($this->collTvas);
    }

    /**
     * Method called to associate a ChildTva object to this object
     * through the ChildTva foreign key attribute.
     *
     * @param  ChildTva $l ChildTva
     * @return $this|\Entite The current object (for fluent API support)
     */
    public function addTva(ChildTva $l)
    {
        if ($this->collTvas === null) {
            $this->initTvas();
            $this->collTvasPartial = true;
        }

        if (!$this->collTvas->contains($l)) {
            $this->doAddTva($l);

            if ($this->tvasScheduledForDeletion and $this->tvasScheduledForDeletion->contains($l)) {
                $this->tvasScheduledForDeletion->remove($this->tvasScheduledForDeletion->search($l));
            }
        }

        return $this;
    }

    /**
     * @param ChildTva $tva The ChildTva object to add.
     */
    protected function doAddTva(ChildTva $tva)
    {
        $this->collTvas[]= $tva;
        $tva->setEntite($this);
    }

    /**
     * @param  ChildTva $tva The ChildTva object to remove.
     * @return $this|ChildEntite The current object (for fluent API support)
     */
    public function removeTva(ChildTva $tva)
    {
        if ($this->getTvas()->contains($tva)) {
            $pos = $this->collTvas->search($tva);
            $this->collTvas->remove($pos);
            if (null === $this->tvasScheduledForDeletion) {
                $this->tvasScheduledForDeletion = clone $this->collTvas;
                $this->tvasScheduledForDeletion->clear();
            }
            $this->tvasScheduledForDeletion[]= clone $tva;
            $tva->setEntite(null);
        }

        return $this;
    }


    /**
     * If this collection has already been initialized with
     * an identical criteria, it returns the collection.
     * Otherwise if this Entite is new, it will return
     * an empty collection; or if this Entite has previously
     * been saved, it will retrieve related Tvas from storage.
     *
     * This method is protected by default in order to keep the public
     * api reasonable.  You can provide public methods for those you
     * actually need in Entite.
     *
     * @param      Criteria $criteria optional Criteria object to narrow the query
     * @param      ConnectionInterface $con optional connection object
     * @param      string $joinBehavior optional join type to use (defaults to Criteria::LEFT_JOIN)
     * @return ObjectCollection|ChildTva[] List of ChildTva objects
     */
    public function getTvasJoinCompte(Criteria $criteria = null, ConnectionInterface $con = null, $joinBehavior = Criteria::LEFT_JOIN)
    {
        $query = ChildTvaQuery::create(null, $criteria);
        $query->joinWith('Compte', $joinBehavior);

        return $this->getTvas($query, $con);
    }

    /**
     * Clears out the collCourriers collection
     *
     * This does not modify the database; however, it will remove any associated objects, causing
     * them to be refetched by subsequent calls to accessor method.
     *
     * @return void
     * @see        addCourriers()
     */
    public function clearCourriers()
    {
        $this->collCourriers = null; // important to set this to NULL since that means it is uninitialized
    }

    /**
     * Reset is the collCourriers collection loaded partially.
     */
    public function resetPartialCourriers($v = true)
    {
        $this->collCourriersPartial = $v;
    }

    /**
     * Initializes the collCourriers collection.
     *
     * By default this just sets the collCourriers collection to an empty array (like clearcollCourriers());
     * however, you may wish to override this method in your stub class to provide setting appropriate
     * to your application -- for example, setting the initial array to the values stored in database.
     *
     * @param      boolean $overrideExisting If set to true, the method call initializes
     *                                        the collection even if it is not empty
     *
     * @return void
     */
    public function initCourriers($overrideExisting = true)
    {
        if (null !== $this->collCourriers && !$overrideExisting) {
            return;
        }

        $collectionClassName = CourrierTableMap::getTableMap()->getCollectionClassName();

        $this->collCourriers = new $collectionClassName;
        $this->collCourriers->setModel('\Courrier');
    }

    /**
     * Gets an array of ChildCourrier objects which contain a foreign key that references this object.
     *
     * If the $criteria is not null, it is used to always fetch the results from the database.
     * Otherwise the results are fetched from the database the first time, then cached.
     * Next time the same method is called without $criteria, the cached collection is returned.
     * If this ChildEntite is new, it will return
     * an empty collection or the current collection; the criteria is ignored on a new object.
     *
     * @param      Criteria $criteria optional Criteria object to narrow the query
     * @param      ConnectionInterface $con optional connection object
     * @return ObjectCollection|ChildCourrier[] List of ChildCourrier objects
     * @throws PropelException
     */
    public function getCourriers(Criteria $criteria = null, ConnectionInterface $con = null)
    {
        $partial = $this->collCourriersPartial && !$this->isNew();
        if (null === $this->collCourriers || null !== $criteria  || $partial) {
            if ($this->isNew() && null === $this->collCourriers) {
                // return empty collection
                $this->initCourriers();
            } else {
                $collCourriers = ChildCourrierQuery::create(null, $criteria)
                    ->filterByEntite($this)
                    ->find($con);

                if (null !== $criteria) {
                    if (false !== $this->collCourriersPartial && count($collCourriers)) {
                        $this->initCourriers(false);

                        foreach ($collCourriers as $obj) {
                            if (false == $this->collCourriers->contains($obj)) {
                                $this->collCourriers->append($obj);
                            }
                        }

                        $this->collCourriersPartial = true;
                    }

                    return $collCourriers;
                }

                if ($partial && $this->collCourriers) {
                    foreach ($this->collCourriers as $obj) {
                        if ($obj->isNew()) {
                            $collCourriers[] = $obj;
                        }
                    }
                }

                $this->collCourriers = $collCourriers;
                $this->collCourriersPartial = false;
            }
        }

        return $this->collCourriers;
    }

    /**
     * Sets a collection of ChildCourrier objects related by a one-to-many relationship
     * to the current object.
     * It will also schedule objects for deletion based on a diff between old objects (aka persisted)
     * and new objects from the given Propel collection.
     *
     * @param      Collection $courriers A Propel collection.
     * @param      ConnectionInterface $con Optional connection object
     * @return $this|ChildEntite The current object (for fluent API support)
     */
    public function setCourriers(Collection $courriers, ConnectionInterface $con = null)
    {
        /** @var ChildCourrier[] $courriersToDelete */
        $courriersToDelete = $this->getCourriers(new Criteria(), $con)->diff($courriers);


        $this->courriersScheduledForDeletion = $courriersToDelete;

        foreach ($courriersToDelete as $courrierRemoved) {
            $courrierRemoved->setEntite(null);
        }

        $this->collCourriers = null;
        foreach ($courriers as $courrier) {
            $this->addCourrier($courrier);
        }

        $this->collCourriers = $courriers;
        $this->collCourriersPartial = false;

        return $this;
    }

    /**
     * Returns the number of related Courrier objects.
     *
     * @param      Criteria $criteria
     * @param      boolean $distinct
     * @param      ConnectionInterface $con
     * @return int             Count of related Courrier objects.
     * @throws PropelException
     */
    public function countCourriers(Criteria $criteria = null, $distinct = false, ConnectionInterface $con = null)
    {
        $partial = $this->collCourriersPartial && !$this->isNew();
        if (null === $this->collCourriers || null !== $criteria || $partial) {
            if ($this->isNew() && null === $this->collCourriers) {
                return 0;
            }

            if ($partial && !$criteria) {
                return count($this->getCourriers());
            }

            $query = ChildCourrierQuery::create(null, $criteria);
            if ($distinct) {
                $query->distinct();
            }

            return $query
                ->filterByEntite($this)
                ->count($con);
        }

        return count($this->collCourriers);
    }

    /**
     * Method called to associate a ChildCourrier object to this object
     * through the ChildCourrier foreign key attribute.
     *
     * @param  ChildCourrier $l ChildCourrier
     * @return $this|\Entite The current object (for fluent API support)
     */
    public function addCourrier(ChildCourrier $l)
    {
        if ($this->collCourriers === null) {
            $this->initCourriers();
            $this->collCourriersPartial = true;
        }

        if (!$this->collCourriers->contains($l)) {
            $this->doAddCourrier($l);

            if ($this->courriersScheduledForDeletion and $this->courriersScheduledForDeletion->contains($l)) {
                $this->courriersScheduledForDeletion->remove($this->courriersScheduledForDeletion->search($l));
            }
        }

        return $this;
    }

    /**
     * @param ChildCourrier $courrier The ChildCourrier object to add.
     */
    protected function doAddCourrier(ChildCourrier $courrier)
    {
        $this->collCourriers[]= $courrier;
        $courrier->setEntite($this);
    }

    /**
     * @param  ChildCourrier $courrier The ChildCourrier object to remove.
     * @return $this|ChildEntite The current object (for fluent API support)
     */
    public function removeCourrier(ChildCourrier $courrier)
    {
        if ($this->getCourriers()->contains($courrier)) {
            $pos = $this->collCourriers->search($courrier);
            $this->collCourriers->remove($pos);
            if (null === $this->courriersScheduledForDeletion) {
                $this->courriersScheduledForDeletion = clone $this->collCourriers;
                $this->courriersScheduledForDeletion->clear();
            }
            $this->courriersScheduledForDeletion[]= clone $courrier;
            $courrier->setEntite(null);
        }

        return $this;
    }


    /**
     * If this collection has already been initialized with
     * an identical criteria, it returns the collection.
     * Otherwise if this Entite is new, it will return
     * an empty collection; or if this Entite has previously
     * been saved, it will retrieve related Courriers from storage.
     *
     * This method is protected by default in order to keep the public
     * api reasonable.  You can provide public methods for those you
     * actually need in Entite.
     *
     * @param      Criteria $criteria optional Criteria object to narrow the query
     * @param      ConnectionInterface $con optional connection object
     * @param      string $joinBehavior optional join type to use (defaults to Criteria::LEFT_JOIN)
     * @return ObjectCollection|ChildCourrier[] List of ChildCourrier objects
     */
    public function getCourriersJoinComposition(Criteria $criteria = null, ConnectionInterface $con = null, $joinBehavior = Criteria::LEFT_JOIN)
    {
        $query = ChildCourrierQuery::create(null, $criteria);
        $query->joinWith('Composition', $joinBehavior);

        return $this->getCourriers($query, $con);
    }

    /**
     * Clears the current object, sets all attributes to their default values and removes
     * outgoing references as well as back-references (from other objects to this one. Results probably in a database
     * change of those foreign objects when you call `save` there).
     */
    public function clear()
    {
        $this->id_entite = null;
        $this->nom = null;
        $this->nomcourt = null;
        $this->adresse = null;
        $this->codepostal = null;
        $this->ville = null;
        $this->pays = null;
        $this->telephone = null;
        $this->fax = null;
        $this->url = null;
        $this->assujettitva = null;
        $this->email = null;
        $this->created_at = null;
        $this->updated_at = null;
        $this->alreadyInSave = false;
        $this->clearAllReferences();
        $this->applyDefaultValues();
        $this->resetModified();
        $this->setNew(true);
        $this->setDeleted(false);
    }

    /**
     * Resets all references and back-references to other model objects or collections of model objects.
     *
     * This method is used to reset all php object references (not the actual reference in the database).
     * Necessary for object serialisation.
     *
     * @param      boolean $deep Whether to also clear the references on all referrer objects.
     */
    public function clearAllReferences($deep = false)
    {
        if ($deep) {
            if ($this->collActivites) {
                foreach ($this->collActivites as $o) {
                    $o->clearAllReferences($deep);
                }
            }
            if ($this->collComptes) {
                foreach ($this->collComptes as $o) {
                    $o->clearAllReferences($deep);
                }
            }
            if ($this->collEcritures) {
                foreach ($this->collEcritures as $o) {
                    $o->clearAllReferences($deep);
                }
            }
            if ($this->collJournals) {
                foreach ($this->collJournals as $o) {
                    $o->clearAllReferences($deep);
                }
            }
            if ($this->collPieces) {
                foreach ($this->collPieces as $o) {
                    $o->clearAllReferences($deep);
                }
            }
            if ($this->collAutorisations) {
                foreach ($this->collAutorisations as $o) {
                    $o->clearAllReferences($deep);
                }
            }
            if ($this->collPaiements) {
                foreach ($this->collPaiements as $o) {
                    $o->clearAllReferences($deep);
                }
            }
            if ($this->collPrestations) {
                foreach ($this->collPrestations as $o) {
                    $o->clearAllReferences($deep);
                }
            }
            if ($this->collPrestationslots) {
                foreach ($this->collPrestationslots as $o) {
                    $o->clearAllReferences($deep);
                }
            }
            if ($this->collServicerendus) {
                foreach ($this->collServicerendus as $o) {
                    $o->clearAllReferences($deep);
                }
            }
            if ($this->collTresors) {
                foreach ($this->collTresors as $o) {
                    $o->clearAllReferences($deep);
                }
            }
            if ($this->collTvas) {
                foreach ($this->collTvas as $o) {
                    $o->clearAllReferences($deep);
                }
            }
            if ($this->collCourriers) {
                foreach ($this->collCourriers as $o) {
                    $o->clearAllReferences($deep);
                }
            }
        } // if ($deep)

        $this->collActivites = null;
        $this->collComptes = null;
        $this->collEcritures = null;
        $this->collJournals = null;
        $this->collPieces = null;
        $this->collAutorisations = null;
        $this->collPaiements = null;
        $this->collPrestations = null;
        $this->collPrestationslots = null;
        $this->collServicerendus = null;
        $this->collTresors = null;
        $this->collTvas = null;
        $this->collCourriers = null;
    }

    /**
     * Return the string representation of this object
     *
     * @return string
     */
    public function __toString()
    {
        return (string) $this->exportTo(EntiteTableMap::DEFAULT_STRING_FORMAT);
    }

    // timestampable behavior

    /**
     * Mark the current object so that the update date doesn't get updated during next save
     *
     * @return     $this|ChildEntite The current object (for fluent API support)
     */
    public function keepUpdateDateUnchanged()
    {
        $this->modifiedColumns[EntiteTableMap::COL_UPDATED_AT] = true;

        return $this;
    }

    // archivable behavior

    /**
     * Get an archived version of the current object.
     *
     * @param ConnectionInterface $con Optional connection object
     *
     * @return     ChildAssoEntitesArchive An archive object, or null if the current object was never archived
     */
    public function getArchive(ConnectionInterface $con = null)
    {
        if ($this->isNew()) {
            return null;
        }
        $archive = ChildAssoEntitesArchiveQuery::create()
            ->filterByPrimaryKey($this->getPrimaryKey())
            ->findOne($con);

        return $archive;
    }
    /**
     * Copy the data of the current object into a $archiveTablePhpName archive object.
     * The archived object is then saved.
     * If the current object has already been archived, the archived object
     * is updated and not duplicated.
     *
     * @param ConnectionInterface $con Optional connection object
     *
     * @throws PropelException If the object is new
     *
     * @return     ChildAssoEntitesArchive The archive object based on this object
     */
    public function archive(ConnectionInterface $con = null)
    {
        if ($this->isNew()) {
            throw new PropelException('New objects cannot be archived. You must save the current object before calling archive().');
        }
        $archive = $this->getArchive($con);
        if (!$archive) {
            $archive = new ChildAssoEntitesArchive();
            $archive->setPrimaryKey($this->getPrimaryKey());
        }
        $this->copyInto($archive, $deepCopy = false, $makeNew = false);
        $archive->setArchivedAt(time());
        $archive->save($con);

        return $archive;
    }

    /**
     * Revert the the current object to the state it had when it was last archived.
     * The object must be saved afterwards if the changes must persist.
     *
     * @param ConnectionInterface $con Optional connection object
     *
     * @throws PropelException If the object has no corresponding archive.
     *
     * @return $this|ChildEntite The current object (for fluent API support)
     */
    public function restoreFromArchive(ConnectionInterface $con = null)
    {
        $archive = $this->getArchive($con);
        if (!$archive) {
            throw new PropelException('The current object has never been archived and cannot be restored');
        }
        $this->populateFromArchive($archive);

        return $this;
    }

    /**
     * Populates the the current object based on a $archiveTablePhpName archive object.
     *
     * @param      ChildAssoEntitesArchive $archive An archived object based on the same class
      * @param      Boolean $populateAutoIncrementPrimaryKeys
     *               If true, autoincrement columns are copied from the archive object.
     *               If false, autoincrement columns are left intact.
      *
     * @return     ChildEntite The current object (for fluent API support)
     */
    public function populateFromArchive($archive, $populateAutoIncrementPrimaryKeys = false) {
        if ($populateAutoIncrementPrimaryKeys) {
            $this->setIdEntite($archive->getIdEntite());
        }
        $this->setNom($archive->getNom());
        $this->setNomcourt($archive->getNomcourt());
        $this->setAdresse($archive->getAdresse());
        $this->setCodepostal($archive->getCodepostal());
        $this->setVille($archive->getVille());
        $this->setPays($archive->getPays());
        $this->setTelephone($archive->getTelephone());
        $this->setFax($archive->getFax());
        $this->setUrl($archive->getUrl());
        $this->setAssujettitva($archive->getAssujettitva());
        $this->setEmail($archive->getEmail());
        $this->setCreatedAt($archive->getCreatedAt());
        $this->setUpdatedAt($archive->getUpdatedAt());

        return $this;
    }

    /**
     * Removes the object from the database without archiving it.
     *
     * @param ConnectionInterface $con Optional connection object
     *
     * @return $this|ChildEntite The current object (for fluent API support)
     */
    public function deleteWithoutArchive(ConnectionInterface $con = null)
    {
        $this->archiveOnDelete = false;

        return $this->delete($con);
    }

    /**
     * Code to be run before persisting the object
     * @param  ConnectionInterface $con
     * @return boolean
     */
    public function preSave(ConnectionInterface $con = null)
    {
        if (is_callable('parent::preSave')) {
            return parent::preSave($con);
        }
        return true;
    }

    /**
     * Code to be run after persisting the object
     * @param ConnectionInterface $con
     */
    public function postSave(ConnectionInterface $con = null)
    {
        if (is_callable('parent::postSave')) {
            parent::postSave($con);
        }
    }

    /**
     * Code to be run before inserting to database
     * @param  ConnectionInterface $con
     * @return boolean
     */
    public function preInsert(ConnectionInterface $con = null)
    {
        if (is_callable('parent::preInsert')) {
            return parent::preInsert($con);
        }
        return true;
    }

    /**
     * Code to be run after inserting to database
     * @param ConnectionInterface $con
     */
    public function postInsert(ConnectionInterface $con = null)
    {
        if (is_callable('parent::postInsert')) {
            parent::postInsert($con);
        }
    }

    /**
     * Code to be run before updating the object in database
     * @param  ConnectionInterface $con
     * @return boolean
     */
    public function preUpdate(ConnectionInterface $con = null)
    {
        if (is_callable('parent::preUpdate')) {
            return parent::preUpdate($con);
        }
        return true;
    }

    /**
     * Code to be run after updating the object in database
     * @param ConnectionInterface $con
     */
    public function postUpdate(ConnectionInterface $con = null)
    {
        if (is_callable('parent::postUpdate')) {
            parent::postUpdate($con);
        }
    }

    /**
     * Code to be run before deleting the object in database
     * @param  ConnectionInterface $con
     * @return boolean
     */
    public function preDelete(ConnectionInterface $con = null)
    {
        if (is_callable('parent::preDelete')) {
            return parent::preDelete($con);
        }
        return true;
    }

    /**
     * Code to be run after deleting the object in database
     * @param ConnectionInterface $con
     */
    public function postDelete(ConnectionInterface $con = null)
    {
        if (is_callable('parent::postDelete')) {
            parent::postDelete($con);
        }
    }


    /**
     * Derived method to catches calls to undefined methods.
     *
     * Provides magic import/export method support (fromXML()/toXML(), fromYAML()/toYAML(), etc.).
     * Allows to define default __call() behavior if you overwrite __call()
     *
     * @param string $name
     * @param mixed  $params
     *
     * @return array|string
     */
    public function __call($name, $params)
    {
        if (0 === strpos($name, 'get')) {
            $virtualColumn = substr($name, 3);
            if ($this->hasVirtualColumn($virtualColumn)) {
                return $this->getVirtualColumn($virtualColumn);
            }

            $virtualColumn = lcfirst($virtualColumn);
            if ($this->hasVirtualColumn($virtualColumn)) {
                return $this->getVirtualColumn($virtualColumn);
            }
        }

        if (0 === strpos($name, 'from')) {
            $format = substr($name, 4);

            return $this->importFrom($format, reset($params));
        }

        if (0 === strpos($name, 'to')) {
            $format = substr($name, 2);
            $includeLazyLoadColumns = isset($params[0]) ? $params[0] : true;

            return $this->exportTo($format, $includeLazyLoadColumns);
        }

        throw new BadMethodCallException(sprintf('Call to undefined method: %s.', $name));
    }

}
