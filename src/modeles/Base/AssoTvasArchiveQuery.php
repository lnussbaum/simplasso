<?php

namespace Base;

use \AssoTvasArchive as ChildAssoTvasArchive;
use \AssoTvasArchiveQuery as ChildAssoTvasArchiveQuery;
use \Exception;
use \PDO;
use Map\AssoTvasArchiveTableMap;
use Propel\Runtime\Propel;
use Propel\Runtime\ActiveQuery\Criteria;
use Propel\Runtime\ActiveQuery\ModelCriteria;
use Propel\Runtime\Collection\ObjectCollection;
use Propel\Runtime\Connection\ConnectionInterface;
use Propel\Runtime\Exception\PropelException;

/**
 * Base class that represents a query for the 'asso_tvas_archive' table.
 *
 *
 *
 * @method     ChildAssoTvasArchiveQuery orderByIdTva($order = Criteria::ASC) Order by the id_tva column
 * @method     ChildAssoTvasArchiveQuery orderByNom($order = Criteria::ASC) Order by the nom column
 * @method     ChildAssoTvasArchiveQuery orderByNomcourt($order = Criteria::ASC) Order by the nomcourt column
 * @method     ChildAssoTvasArchiveQuery orderByIdCompte($order = Criteria::ASC) Order by the id_compte column
 * @method     ChildAssoTvasArchiveQuery orderByActif($order = Criteria::ASC) Order by the actif column
 * @method     ChildAssoTvasArchiveQuery orderBySigne($order = Criteria::ASC) Order by the signe column
 * @method     ChildAssoTvasArchiveQuery orderByEncaissement($order = Criteria::ASC) Order by the encaissement column
 * @method     ChildAssoTvasArchiveQuery orderByIdEntite($order = Criteria::ASC) Order by the id_entite column
 * @method     ChildAssoTvasArchiveQuery orderByObservation($order = Criteria::ASC) Order by the observation column
 * @method     ChildAssoTvasArchiveQuery orderByCreatedAt($order = Criteria::ASC) Order by the created_at column
 * @method     ChildAssoTvasArchiveQuery orderByUpdatedAt($order = Criteria::ASC) Order by the updated_at column
 * @method     ChildAssoTvasArchiveQuery orderByArchivedAt($order = Criteria::ASC) Order by the archived_at column
 *
 * @method     ChildAssoTvasArchiveQuery groupByIdTva() Group by the id_tva column
 * @method     ChildAssoTvasArchiveQuery groupByNom() Group by the nom column
 * @method     ChildAssoTvasArchiveQuery groupByNomcourt() Group by the nomcourt column
 * @method     ChildAssoTvasArchiveQuery groupByIdCompte() Group by the id_compte column
 * @method     ChildAssoTvasArchiveQuery groupByActif() Group by the actif column
 * @method     ChildAssoTvasArchiveQuery groupBySigne() Group by the signe column
 * @method     ChildAssoTvasArchiveQuery groupByEncaissement() Group by the encaissement column
 * @method     ChildAssoTvasArchiveQuery groupByIdEntite() Group by the id_entite column
 * @method     ChildAssoTvasArchiveQuery groupByObservation() Group by the observation column
 * @method     ChildAssoTvasArchiveQuery groupByCreatedAt() Group by the created_at column
 * @method     ChildAssoTvasArchiveQuery groupByUpdatedAt() Group by the updated_at column
 * @method     ChildAssoTvasArchiveQuery groupByArchivedAt() Group by the archived_at column
 *
 * @method     ChildAssoTvasArchiveQuery leftJoin($relation) Adds a LEFT JOIN clause to the query
 * @method     ChildAssoTvasArchiveQuery rightJoin($relation) Adds a RIGHT JOIN clause to the query
 * @method     ChildAssoTvasArchiveQuery innerJoin($relation) Adds a INNER JOIN clause to the query
 *
 * @method     ChildAssoTvasArchiveQuery leftJoinWith($relation) Adds a LEFT JOIN clause and with to the query
 * @method     ChildAssoTvasArchiveQuery rightJoinWith($relation) Adds a RIGHT JOIN clause and with to the query
 * @method     ChildAssoTvasArchiveQuery innerJoinWith($relation) Adds a INNER JOIN clause and with to the query
 *
 * @method     ChildAssoTvasArchive findOne(ConnectionInterface $con = null) Return the first ChildAssoTvasArchive matching the query
 * @method     ChildAssoTvasArchive findOneOrCreate(ConnectionInterface $con = null) Return the first ChildAssoTvasArchive matching the query, or a new ChildAssoTvasArchive object populated from the query conditions when no match is found
 *
 * @method     ChildAssoTvasArchive findOneByIdTva(string $id_tva) Return the first ChildAssoTvasArchive filtered by the id_tva column
 * @method     ChildAssoTvasArchive findOneByNom(string $nom) Return the first ChildAssoTvasArchive filtered by the nom column
 * @method     ChildAssoTvasArchive findOneByNomcourt(string $nomcourt) Return the first ChildAssoTvasArchive filtered by the nomcourt column
 * @method     ChildAssoTvasArchive findOneByIdCompte(string $id_compte) Return the first ChildAssoTvasArchive filtered by the id_compte column
 * @method     ChildAssoTvasArchive findOneByActif(int $actif) Return the first ChildAssoTvasArchive filtered by the actif column
 * @method     ChildAssoTvasArchive findOneBySigne(string $signe) Return the first ChildAssoTvasArchive filtered by the signe column
 * @method     ChildAssoTvasArchive findOneByEncaissement(int $encaissement) Return the first ChildAssoTvasArchive filtered by the encaissement column
 * @method     ChildAssoTvasArchive findOneByIdEntite(string $id_entite) Return the first ChildAssoTvasArchive filtered by the id_entite column
 * @method     ChildAssoTvasArchive findOneByObservation(string $observation) Return the first ChildAssoTvasArchive filtered by the observation column
 * @method     ChildAssoTvasArchive findOneByCreatedAt(string $created_at) Return the first ChildAssoTvasArchive filtered by the created_at column
 * @method     ChildAssoTvasArchive findOneByUpdatedAt(string $updated_at) Return the first ChildAssoTvasArchive filtered by the updated_at column
 * @method     ChildAssoTvasArchive findOneByArchivedAt(string $archived_at) Return the first ChildAssoTvasArchive filtered by the archived_at column *

 * @method     ChildAssoTvasArchive requirePk($key, ConnectionInterface $con = null) Return the ChildAssoTvasArchive by primary key and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildAssoTvasArchive requireOne(ConnectionInterface $con = null) Return the first ChildAssoTvasArchive matching the query and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 *
 * @method     ChildAssoTvasArchive requireOneByIdTva(string $id_tva) Return the first ChildAssoTvasArchive filtered by the id_tva column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildAssoTvasArchive requireOneByNom(string $nom) Return the first ChildAssoTvasArchive filtered by the nom column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildAssoTvasArchive requireOneByNomcourt(string $nomcourt) Return the first ChildAssoTvasArchive filtered by the nomcourt column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildAssoTvasArchive requireOneByIdCompte(string $id_compte) Return the first ChildAssoTvasArchive filtered by the id_compte column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildAssoTvasArchive requireOneByActif(int $actif) Return the first ChildAssoTvasArchive filtered by the actif column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildAssoTvasArchive requireOneBySigne(string $signe) Return the first ChildAssoTvasArchive filtered by the signe column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildAssoTvasArchive requireOneByEncaissement(int $encaissement) Return the first ChildAssoTvasArchive filtered by the encaissement column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildAssoTvasArchive requireOneByIdEntite(string $id_entite) Return the first ChildAssoTvasArchive filtered by the id_entite column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildAssoTvasArchive requireOneByObservation(string $observation) Return the first ChildAssoTvasArchive filtered by the observation column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildAssoTvasArchive requireOneByCreatedAt(string $created_at) Return the first ChildAssoTvasArchive filtered by the created_at column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildAssoTvasArchive requireOneByUpdatedAt(string $updated_at) Return the first ChildAssoTvasArchive filtered by the updated_at column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildAssoTvasArchive requireOneByArchivedAt(string $archived_at) Return the first ChildAssoTvasArchive filtered by the archived_at column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 *
 * @method     ChildAssoTvasArchive[]|ObjectCollection find(ConnectionInterface $con = null) Return ChildAssoTvasArchive objects based on current ModelCriteria
 * @method     ChildAssoTvasArchive[]|ObjectCollection findByIdTva(string $id_tva) Return ChildAssoTvasArchive objects filtered by the id_tva column
 * @method     ChildAssoTvasArchive[]|ObjectCollection findByNom(string $nom) Return ChildAssoTvasArchive objects filtered by the nom column
 * @method     ChildAssoTvasArchive[]|ObjectCollection findByNomcourt(string $nomcourt) Return ChildAssoTvasArchive objects filtered by the nomcourt column
 * @method     ChildAssoTvasArchive[]|ObjectCollection findByIdCompte(string $id_compte) Return ChildAssoTvasArchive objects filtered by the id_compte column
 * @method     ChildAssoTvasArchive[]|ObjectCollection findByActif(int $actif) Return ChildAssoTvasArchive objects filtered by the actif column
 * @method     ChildAssoTvasArchive[]|ObjectCollection findBySigne(string $signe) Return ChildAssoTvasArchive objects filtered by the signe column
 * @method     ChildAssoTvasArchive[]|ObjectCollection findByEncaissement(int $encaissement) Return ChildAssoTvasArchive objects filtered by the encaissement column
 * @method     ChildAssoTvasArchive[]|ObjectCollection findByIdEntite(string $id_entite) Return ChildAssoTvasArchive objects filtered by the id_entite column
 * @method     ChildAssoTvasArchive[]|ObjectCollection findByObservation(string $observation) Return ChildAssoTvasArchive objects filtered by the observation column
 * @method     ChildAssoTvasArchive[]|ObjectCollection findByCreatedAt(string $created_at) Return ChildAssoTvasArchive objects filtered by the created_at column
 * @method     ChildAssoTvasArchive[]|ObjectCollection findByUpdatedAt(string $updated_at) Return ChildAssoTvasArchive objects filtered by the updated_at column
 * @method     ChildAssoTvasArchive[]|ObjectCollection findByArchivedAt(string $archived_at) Return ChildAssoTvasArchive objects filtered by the archived_at column
 * @method     ChildAssoTvasArchive[]|\Propel\Runtime\Util\PropelModelPager paginate($page = 1, $maxPerPage = 10, ConnectionInterface $con = null) Issue a SELECT query based on the current ModelCriteria and uses a page and a maximum number of results per page to compute an offset and a limit
 *
 */
abstract class AssoTvasArchiveQuery extends ModelCriteria
{
    protected $entityNotFoundExceptionClass = '\\Propel\\Runtime\\Exception\\EntityNotFoundException';

    /**
     * Initializes internal state of \Base\AssoTvasArchiveQuery object.
     *
     * @param     string $dbName The database name
     * @param     string $modelName The phpName of a model, e.g. 'Book'
     * @param     string $modelAlias The alias for the model in this query, e.g. 'b'
     */
    public function __construct($dbName = 'simplasso', $modelName = '\\AssoTvasArchive', $modelAlias = null)
    {
        parent::__construct($dbName, $modelName, $modelAlias);
    }

    /**
     * Returns a new ChildAssoTvasArchiveQuery object.
     *
     * @param     string $modelAlias The alias of a model in the query
     * @param     Criteria $criteria Optional Criteria to build the query from
     *
     * @return ChildAssoTvasArchiveQuery
     */
    public static function create($modelAlias = null, Criteria $criteria = null)
    {
        if ($criteria instanceof ChildAssoTvasArchiveQuery) {
            return $criteria;
        }
        $query = new ChildAssoTvasArchiveQuery();
        if (null !== $modelAlias) {
            $query->setModelAlias($modelAlias);
        }
        if ($criteria instanceof Criteria) {
            $query->mergeWith($criteria);
        }

        return $query;
    }

    /**
     * Find object by primary key.
     * Propel uses the instance pool to skip the database if the object exists.
     * Go fast if the query is untouched.
     *
     * <code>
     * $obj  = $c->findPk(12, $con);
     * </code>
     *
     * @param mixed $key Primary key to use for the query
     * @param ConnectionInterface $con an optional connection object
     *
     * @return ChildAssoTvasArchive|array|mixed the result, formatted by the current formatter
     */
    public function findPk($key, ConnectionInterface $con = null)
    {
        if ($key === null) {
            return null;
        }

        if ($con === null) {
            $con = Propel::getServiceContainer()->getReadConnection(AssoTvasArchiveTableMap::DATABASE_NAME);
        }

        $this->basePreSelect($con);

        if (
            $this->formatter || $this->modelAlias || $this->with || $this->select
            || $this->selectColumns || $this->asColumns || $this->selectModifiers
            || $this->map || $this->having || $this->joins
        ) {
            return $this->findPkComplex($key, $con);
        }

        if ((null !== ($obj = AssoTvasArchiveTableMap::getInstanceFromPool(null === $key || is_scalar($key) || is_callable([$key, '__toString']) ? (string) $key : $key)))) {
            // the object is already in the instance pool
            return $obj;
        }

        return $this->findPkSimple($key, $con);
    }

    /**
     * Find object by primary key using raw SQL to go fast.
     * Bypass doSelect() and the object formatter by using generated code.
     *
     * @param     mixed $key Primary key to use for the query
     * @param     ConnectionInterface $con A connection object
     *
     * @throws \Propel\Runtime\Exception\PropelException
     *
     * @return ChildAssoTvasArchive A model object, or null if the key is not found
     */
    protected function findPkSimple($key, ConnectionInterface $con)
    {
        $sql = 'SELECT `id_tva`, `nom`, `nomcourt`, `id_compte`, `actif`, `signe`, `encaissement`, `id_entite`, `observation`, `created_at`, `updated_at`, `archived_at` FROM `asso_tvas_archive` WHERE `id_tva` = :p0';
        try {
            $stmt = $con->prepare($sql);
            $stmt->bindValue(':p0', $key, PDO::PARAM_INT);
            $stmt->execute();
        } catch (Exception $e) {
            Propel::log($e->getMessage(), Propel::LOG_ERR);
            throw new PropelException(sprintf('Unable to execute SELECT statement [%s]', $sql), 0, $e);
        }
        $obj = null;
        if ($row = $stmt->fetch(\PDO::FETCH_NUM)) {
            /** @var ChildAssoTvasArchive $obj */
            $obj = new ChildAssoTvasArchive();
            $obj->hydrate($row);
            AssoTvasArchiveTableMap::addInstanceToPool($obj, null === $key || is_scalar($key) || is_callable([$key, '__toString']) ? (string) $key : $key);
        }
        $stmt->closeCursor();

        return $obj;
    }

    /**
     * Find object by primary key.
     *
     * @param     mixed $key Primary key to use for the query
     * @param     ConnectionInterface $con A connection object
     *
     * @return ChildAssoTvasArchive|array|mixed the result, formatted by the current formatter
     */
    protected function findPkComplex($key, ConnectionInterface $con)
    {
        // As the query uses a PK condition, no limit(1) is necessary.
        $criteria = $this->isKeepQuery() ? clone $this : $this;
        $dataFetcher = $criteria
            ->filterByPrimaryKey($key)
            ->doSelect($con);

        return $criteria->getFormatter()->init($criteria)->formatOne($dataFetcher);
    }

    /**
     * Find objects by primary key
     * <code>
     * $objs = $c->findPks(array(12, 56, 832), $con);
     * </code>
     * @param     array $keys Primary keys to use for the query
     * @param     ConnectionInterface $con an optional connection object
     *
     * @return ObjectCollection|array|mixed the list of results, formatted by the current formatter
     */
    public function findPks($keys, ConnectionInterface $con = null)
    {
        if (null === $con) {
            $con = Propel::getServiceContainer()->getReadConnection($this->getDbName());
        }
        $this->basePreSelect($con);
        $criteria = $this->isKeepQuery() ? clone $this : $this;
        $dataFetcher = $criteria
            ->filterByPrimaryKeys($keys)
            ->doSelect($con);

        return $criteria->getFormatter()->init($criteria)->format($dataFetcher);
    }

    /**
     * Filter the query by primary key
     *
     * @param     mixed $key Primary key to use for the query
     *
     * @return $this|ChildAssoTvasArchiveQuery The current query, for fluid interface
     */
    public function filterByPrimaryKey($key)
    {

        return $this->addUsingAlias(AssoTvasArchiveTableMap::COL_ID_TVA, $key, Criteria::EQUAL);
    }

    /**
     * Filter the query by a list of primary keys
     *
     * @param     array $keys The list of primary key to use for the query
     *
     * @return $this|ChildAssoTvasArchiveQuery The current query, for fluid interface
     */
    public function filterByPrimaryKeys($keys)
    {

        return $this->addUsingAlias(AssoTvasArchiveTableMap::COL_ID_TVA, $keys, Criteria::IN);
    }

    /**
     * Filter the query on the id_tva column
     *
     * Example usage:
     * <code>
     * $query->filterByIdTva(1234); // WHERE id_tva = 1234
     * $query->filterByIdTva(array(12, 34)); // WHERE id_tva IN (12, 34)
     * $query->filterByIdTva(array('min' => 12)); // WHERE id_tva > 12
     * </code>
     *
     * @param     mixed $idTva The value to use as filter.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildAssoTvasArchiveQuery The current query, for fluid interface
     */
    public function filterByIdTva($idTva = null, $comparison = null)
    {
        if (is_array($idTva)) {
            $useMinMax = false;
            if (isset($idTva['min'])) {
                $this->addUsingAlias(AssoTvasArchiveTableMap::COL_ID_TVA, $idTva['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($idTva['max'])) {
                $this->addUsingAlias(AssoTvasArchiveTableMap::COL_ID_TVA, $idTva['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(AssoTvasArchiveTableMap::COL_ID_TVA, $idTva, $comparison);
    }

    /**
     * Filter the query on the nom column
     *
     * Example usage:
     * <code>
     * $query->filterByNom('fooValue');   // WHERE nom = 'fooValue'
     * $query->filterByNom('%fooValue%', Criteria::LIKE); // WHERE nom LIKE '%fooValue%'
     * </code>
     *
     * @param     string $nom The value to use as filter.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildAssoTvasArchiveQuery The current query, for fluid interface
     */
    public function filterByNom($nom = null, $comparison = null)
    {
        if (null === $comparison) {
            if (is_array($nom)) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(AssoTvasArchiveTableMap::COL_NOM, $nom, $comparison);
    }

    /**
     * Filter the query on the nomcourt column
     *
     * Example usage:
     * <code>
     * $query->filterByNomcourt('fooValue');   // WHERE nomcourt = 'fooValue'
     * $query->filterByNomcourt('%fooValue%', Criteria::LIKE); // WHERE nomcourt LIKE '%fooValue%'
     * </code>
     *
     * @param     string $nomcourt The value to use as filter.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildAssoTvasArchiveQuery The current query, for fluid interface
     */
    public function filterByNomcourt($nomcourt = null, $comparison = null)
    {
        if (null === $comparison) {
            if (is_array($nomcourt)) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(AssoTvasArchiveTableMap::COL_NOMCOURT, $nomcourt, $comparison);
    }

    /**
     * Filter the query on the id_compte column
     *
     * Example usage:
     * <code>
     * $query->filterByIdCompte(1234); // WHERE id_compte = 1234
     * $query->filterByIdCompte(array(12, 34)); // WHERE id_compte IN (12, 34)
     * $query->filterByIdCompte(array('min' => 12)); // WHERE id_compte > 12
     * </code>
     *
     * @param     mixed $idCompte The value to use as filter.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildAssoTvasArchiveQuery The current query, for fluid interface
     */
    public function filterByIdCompte($idCompte = null, $comparison = null)
    {
        if (is_array($idCompte)) {
            $useMinMax = false;
            if (isset($idCompte['min'])) {
                $this->addUsingAlias(AssoTvasArchiveTableMap::COL_ID_COMPTE, $idCompte['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($idCompte['max'])) {
                $this->addUsingAlias(AssoTvasArchiveTableMap::COL_ID_COMPTE, $idCompte['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(AssoTvasArchiveTableMap::COL_ID_COMPTE, $idCompte, $comparison);
    }

    /**
     * Filter the query on the actif column
     *
     * Example usage:
     * <code>
     * $query->filterByActif(1234); // WHERE actif = 1234
     * $query->filterByActif(array(12, 34)); // WHERE actif IN (12, 34)
     * $query->filterByActif(array('min' => 12)); // WHERE actif > 12
     * </code>
     *
     * @param     mixed $actif The value to use as filter.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildAssoTvasArchiveQuery The current query, for fluid interface
     */
    public function filterByActif($actif = null, $comparison = null)
    {
        if (is_array($actif)) {
            $useMinMax = false;
            if (isset($actif['min'])) {
                $this->addUsingAlias(AssoTvasArchiveTableMap::COL_ACTIF, $actif['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($actif['max'])) {
                $this->addUsingAlias(AssoTvasArchiveTableMap::COL_ACTIF, $actif['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(AssoTvasArchiveTableMap::COL_ACTIF, $actif, $comparison);
    }

    /**
     * Filter the query on the signe column
     *
     * Example usage:
     * <code>
     * $query->filterBySigne('fooValue');   // WHERE signe = 'fooValue'
     * $query->filterBySigne('%fooValue%', Criteria::LIKE); // WHERE signe LIKE '%fooValue%'
     * </code>
     *
     * @param     string $signe The value to use as filter.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildAssoTvasArchiveQuery The current query, for fluid interface
     */
    public function filterBySigne($signe = null, $comparison = null)
    {
        if (null === $comparison) {
            if (is_array($signe)) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(AssoTvasArchiveTableMap::COL_SIGNE, $signe, $comparison);
    }

    /**
     * Filter the query on the encaissement column
     *
     * Example usage:
     * <code>
     * $query->filterByEncaissement(1234); // WHERE encaissement = 1234
     * $query->filterByEncaissement(array(12, 34)); // WHERE encaissement IN (12, 34)
     * $query->filterByEncaissement(array('min' => 12)); // WHERE encaissement > 12
     * </code>
     *
     * @param     mixed $encaissement The value to use as filter.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildAssoTvasArchiveQuery The current query, for fluid interface
     */
    public function filterByEncaissement($encaissement = null, $comparison = null)
    {
        if (is_array($encaissement)) {
            $useMinMax = false;
            if (isset($encaissement['min'])) {
                $this->addUsingAlias(AssoTvasArchiveTableMap::COL_ENCAISSEMENT, $encaissement['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($encaissement['max'])) {
                $this->addUsingAlias(AssoTvasArchiveTableMap::COL_ENCAISSEMENT, $encaissement['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(AssoTvasArchiveTableMap::COL_ENCAISSEMENT, $encaissement, $comparison);
    }

    /**
     * Filter the query on the id_entite column
     *
     * Example usage:
     * <code>
     * $query->filterByIdEntite(1234); // WHERE id_entite = 1234
     * $query->filterByIdEntite(array(12, 34)); // WHERE id_entite IN (12, 34)
     * $query->filterByIdEntite(array('min' => 12)); // WHERE id_entite > 12
     * </code>
     *
     * @param     mixed $idEntite The value to use as filter.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildAssoTvasArchiveQuery The current query, for fluid interface
     */
    public function filterByIdEntite($idEntite = null, $comparison = null)
    {
        if (is_array($idEntite)) {
            $useMinMax = false;
            if (isset($idEntite['min'])) {
                $this->addUsingAlias(AssoTvasArchiveTableMap::COL_ID_ENTITE, $idEntite['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($idEntite['max'])) {
                $this->addUsingAlias(AssoTvasArchiveTableMap::COL_ID_ENTITE, $idEntite['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(AssoTvasArchiveTableMap::COL_ID_ENTITE, $idEntite, $comparison);
    }

    /**
     * Filter the query on the observation column
     *
     * Example usage:
     * <code>
     * $query->filterByObservation('fooValue');   // WHERE observation = 'fooValue'
     * $query->filterByObservation('%fooValue%', Criteria::LIKE); // WHERE observation LIKE '%fooValue%'
     * </code>
     *
     * @param     string $observation The value to use as filter.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildAssoTvasArchiveQuery The current query, for fluid interface
     */
    public function filterByObservation($observation = null, $comparison = null)
    {
        if (null === $comparison) {
            if (is_array($observation)) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(AssoTvasArchiveTableMap::COL_OBSERVATION, $observation, $comparison);
    }

    /**
     * Filter the query on the created_at column
     *
     * Example usage:
     * <code>
     * $query->filterByCreatedAt('2011-03-14'); // WHERE created_at = '2011-03-14'
     * $query->filterByCreatedAt('now'); // WHERE created_at = '2011-03-14'
     * $query->filterByCreatedAt(array('max' => 'yesterday')); // WHERE created_at > '2011-03-13'
     * </code>
     *
     * @param     mixed $createdAt The value to use as filter.
     *              Values can be integers (unix timestamps), DateTime objects, or strings.
     *              Empty strings are treated as NULL.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildAssoTvasArchiveQuery The current query, for fluid interface
     */
    public function filterByCreatedAt($createdAt = null, $comparison = null)
    {
        if (is_array($createdAt)) {
            $useMinMax = false;
            if (isset($createdAt['min'])) {
                $this->addUsingAlias(AssoTvasArchiveTableMap::COL_CREATED_AT, $createdAt['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($createdAt['max'])) {
                $this->addUsingAlias(AssoTvasArchiveTableMap::COL_CREATED_AT, $createdAt['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(AssoTvasArchiveTableMap::COL_CREATED_AT, $createdAt, $comparison);
    }

    /**
     * Filter the query on the updated_at column
     *
     * Example usage:
     * <code>
     * $query->filterByUpdatedAt('2011-03-14'); // WHERE updated_at = '2011-03-14'
     * $query->filterByUpdatedAt('now'); // WHERE updated_at = '2011-03-14'
     * $query->filterByUpdatedAt(array('max' => 'yesterday')); // WHERE updated_at > '2011-03-13'
     * </code>
     *
     * @param     mixed $updatedAt The value to use as filter.
     *              Values can be integers (unix timestamps), DateTime objects, or strings.
     *              Empty strings are treated as NULL.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildAssoTvasArchiveQuery The current query, for fluid interface
     */
    public function filterByUpdatedAt($updatedAt = null, $comparison = null)
    {
        if (is_array($updatedAt)) {
            $useMinMax = false;
            if (isset($updatedAt['min'])) {
                $this->addUsingAlias(AssoTvasArchiveTableMap::COL_UPDATED_AT, $updatedAt['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($updatedAt['max'])) {
                $this->addUsingAlias(AssoTvasArchiveTableMap::COL_UPDATED_AT, $updatedAt['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(AssoTvasArchiveTableMap::COL_UPDATED_AT, $updatedAt, $comparison);
    }

    /**
     * Filter the query on the archived_at column
     *
     * Example usage:
     * <code>
     * $query->filterByArchivedAt('2011-03-14'); // WHERE archived_at = '2011-03-14'
     * $query->filterByArchivedAt('now'); // WHERE archived_at = '2011-03-14'
     * $query->filterByArchivedAt(array('max' => 'yesterday')); // WHERE archived_at > '2011-03-13'
     * </code>
     *
     * @param     mixed $archivedAt The value to use as filter.
     *              Values can be integers (unix timestamps), DateTime objects, or strings.
     *              Empty strings are treated as NULL.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildAssoTvasArchiveQuery The current query, for fluid interface
     */
    public function filterByArchivedAt($archivedAt = null, $comparison = null)
    {
        if (is_array($archivedAt)) {
            $useMinMax = false;
            if (isset($archivedAt['min'])) {
                $this->addUsingAlias(AssoTvasArchiveTableMap::COL_ARCHIVED_AT, $archivedAt['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($archivedAt['max'])) {
                $this->addUsingAlias(AssoTvasArchiveTableMap::COL_ARCHIVED_AT, $archivedAt['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(AssoTvasArchiveTableMap::COL_ARCHIVED_AT, $archivedAt, $comparison);
    }

    /**
     * Exclude object from result
     *
     * @param   ChildAssoTvasArchive $assoTvasArchive Object to remove from the list of results
     *
     * @return $this|ChildAssoTvasArchiveQuery The current query, for fluid interface
     */
    public function prune($assoTvasArchive = null)
    {
        if ($assoTvasArchive) {
            $this->addUsingAlias(AssoTvasArchiveTableMap::COL_ID_TVA, $assoTvasArchive->getIdTva(), Criteria::NOT_EQUAL);
        }

        return $this;
    }

    /**
     * Deletes all rows from the asso_tvas_archive table.
     *
     * @param ConnectionInterface $con the connection to use
     * @return int The number of affected rows (if supported by underlying database driver).
     */
    public function doDeleteAll(ConnectionInterface $con = null)
    {
        if (null === $con) {
            $con = Propel::getServiceContainer()->getWriteConnection(AssoTvasArchiveTableMap::DATABASE_NAME);
        }

        // use transaction because $criteria could contain info
        // for more than one table or we could emulating ON DELETE CASCADE, etc.
        return $con->transaction(function () use ($con) {
            $affectedRows = 0; // initialize var to track total num of affected rows
            $affectedRows += parent::doDeleteAll($con);
            // Because this db requires some delete cascade/set null emulation, we have to
            // clear the cached instance *after* the emulation has happened (since
            // instances get re-added by the select statement contained therein).
            AssoTvasArchiveTableMap::clearInstancePool();
            AssoTvasArchiveTableMap::clearRelatedInstancePool();

            return $affectedRows;
        });
    }

    /**
     * Performs a DELETE on the database based on the current ModelCriteria
     *
     * @param ConnectionInterface $con the connection to use
     * @return int             The number of affected rows (if supported by underlying database driver).  This includes CASCADE-related rows
     *                         if supported by native driver or if emulated using Propel.
     * @throws PropelException Any exceptions caught during processing will be
     *                         rethrown wrapped into a PropelException.
     */
    public function delete(ConnectionInterface $con = null)
    {
        if (null === $con) {
            $con = Propel::getServiceContainer()->getWriteConnection(AssoTvasArchiveTableMap::DATABASE_NAME);
        }

        $criteria = $this;

        // Set the correct dbName
        $criteria->setDbName(AssoTvasArchiveTableMap::DATABASE_NAME);

        // use transaction because $criteria could contain info
        // for more than one table or we could emulating ON DELETE CASCADE, etc.
        return $con->transaction(function () use ($con, $criteria) {
            $affectedRows = 0; // initialize var to track total num of affected rows

            AssoTvasArchiveTableMap::removeInstanceFromPool($criteria);

            $affectedRows += ModelCriteria::delete($con);
            AssoTvasArchiveTableMap::clearRelatedInstancePool();

            return $affectedRows;
        });
    }

} // AssoTvasArchiveQuery
