<?php

namespace Base;

use \AsscEcrituresArchive as ChildAsscEcrituresArchive;
use \Ecriture as ChildEcriture;
use \EcritureQuery as ChildEcritureQuery;
use \Exception;
use \PDO;
use Map\EcritureTableMap;
use Propel\Runtime\Propel;
use Propel\Runtime\ActiveQuery\Criteria;
use Propel\Runtime\ActiveQuery\ModelCriteria;
use Propel\Runtime\ActiveQuery\ModelJoin;
use Propel\Runtime\Collection\ObjectCollection;
use Propel\Runtime\Connection\ConnectionInterface;
use Propel\Runtime\Exception\PropelException;

/**
 * Base class that represents a query for the 'assc_ecritures' table.
 *
 *
 *
 * @method     ChildEcritureQuery orderByIdEcriture($order = Criteria::ASC) Order by the id_ecriture column
 * @method     ChildEcritureQuery orderByClassement($order = Criteria::ASC) Order by the classement column
 * @method     ChildEcritureQuery orderByCredit($order = Criteria::ASC) Order by the credit column
 * @method     ChildEcritureQuery orderByDebit($order = Criteria::ASC) Order by the debit column
 * @method     ChildEcritureQuery orderByNom($order = Criteria::ASC) Order by the nom column
 * @method     ChildEcritureQuery orderByIdCompte($order = Criteria::ASC) Order by the id_compte column
 * @method     ChildEcritureQuery orderByIdJournal($order = Criteria::ASC) Order by the id_journal column
 * @method     ChildEcritureQuery orderByIdActivite($order = Criteria::ASC) Order by the id_activite column
 * @method     ChildEcritureQuery orderByIdEntite($order = Criteria::ASC) Order by the id_entite column
 * @method     ChildEcritureQuery orderByLettrage($order = Criteria::ASC) Order by the lettrage column
 * @method     ChildEcritureQuery orderByDateLettrage($order = Criteria::ASC) Order by the date_lettrage column
 * @method     ChildEcritureQuery orderByDateEcriture($order = Criteria::ASC) Order by the date_ecriture column
 * @method     ChildEcritureQuery orderByEtat($order = Criteria::ASC) Order by the etat column
 * @method     ChildEcritureQuery orderByObservation($order = Criteria::ASC) Order by the observation column
 * @method     ChildEcritureQuery orderByIdPiece($order = Criteria::ASC) Order by the id_piece column
 * @method     ChildEcritureQuery orderByEcranSaisie($order = Criteria::ASC) Order by the ecran_saisie column
 * @method     ChildEcritureQuery orderByContrePartie($order = Criteria::ASC) Order by the contre_partie column
 * @method     ChildEcritureQuery orderByQuantite($order = Criteria::ASC) Order by the quantite column
 * @method     ChildEcritureQuery orderByCreatedAt($order = Criteria::ASC) Order by the created_at column
 * @method     ChildEcritureQuery orderByUpdatedAt($order = Criteria::ASC) Order by the updated_at column
 *
 * @method     ChildEcritureQuery groupByIdEcriture() Group by the id_ecriture column
 * @method     ChildEcritureQuery groupByClassement() Group by the classement column
 * @method     ChildEcritureQuery groupByCredit() Group by the credit column
 * @method     ChildEcritureQuery groupByDebit() Group by the debit column
 * @method     ChildEcritureQuery groupByNom() Group by the nom column
 * @method     ChildEcritureQuery groupByIdCompte() Group by the id_compte column
 * @method     ChildEcritureQuery groupByIdJournal() Group by the id_journal column
 * @method     ChildEcritureQuery groupByIdActivite() Group by the id_activite column
 * @method     ChildEcritureQuery groupByIdEntite() Group by the id_entite column
 * @method     ChildEcritureQuery groupByLettrage() Group by the lettrage column
 * @method     ChildEcritureQuery groupByDateLettrage() Group by the date_lettrage column
 * @method     ChildEcritureQuery groupByDateEcriture() Group by the date_ecriture column
 * @method     ChildEcritureQuery groupByEtat() Group by the etat column
 * @method     ChildEcritureQuery groupByObservation() Group by the observation column
 * @method     ChildEcritureQuery groupByIdPiece() Group by the id_piece column
 * @method     ChildEcritureQuery groupByEcranSaisie() Group by the ecran_saisie column
 * @method     ChildEcritureQuery groupByContrePartie() Group by the contre_partie column
 * @method     ChildEcritureQuery groupByQuantite() Group by the quantite column
 * @method     ChildEcritureQuery groupByCreatedAt() Group by the created_at column
 * @method     ChildEcritureQuery groupByUpdatedAt() Group by the updated_at column
 *
 * @method     ChildEcritureQuery leftJoin($relation) Adds a LEFT JOIN clause to the query
 * @method     ChildEcritureQuery rightJoin($relation) Adds a RIGHT JOIN clause to the query
 * @method     ChildEcritureQuery innerJoin($relation) Adds a INNER JOIN clause to the query
 *
 * @method     ChildEcritureQuery leftJoinWith($relation) Adds a LEFT JOIN clause and with to the query
 * @method     ChildEcritureQuery rightJoinWith($relation) Adds a RIGHT JOIN clause and with to the query
 * @method     ChildEcritureQuery innerJoinWith($relation) Adds a INNER JOIN clause and with to the query
 *
 * @method     ChildEcritureQuery leftJoinCompte($relationAlias = null) Adds a LEFT JOIN clause to the query using the Compte relation
 * @method     ChildEcritureQuery rightJoinCompte($relationAlias = null) Adds a RIGHT JOIN clause to the query using the Compte relation
 * @method     ChildEcritureQuery innerJoinCompte($relationAlias = null) Adds a INNER JOIN clause to the query using the Compte relation
 *
 * @method     ChildEcritureQuery joinWithCompte($joinType = Criteria::INNER_JOIN) Adds a join clause and with to the query using the Compte relation
 *
 * @method     ChildEcritureQuery leftJoinWithCompte() Adds a LEFT JOIN clause and with to the query using the Compte relation
 * @method     ChildEcritureQuery rightJoinWithCompte() Adds a RIGHT JOIN clause and with to the query using the Compte relation
 * @method     ChildEcritureQuery innerJoinWithCompte() Adds a INNER JOIN clause and with to the query using the Compte relation
 *
 * @method     ChildEcritureQuery leftJoinJournal($relationAlias = null) Adds a LEFT JOIN clause to the query using the Journal relation
 * @method     ChildEcritureQuery rightJoinJournal($relationAlias = null) Adds a RIGHT JOIN clause to the query using the Journal relation
 * @method     ChildEcritureQuery innerJoinJournal($relationAlias = null) Adds a INNER JOIN clause to the query using the Journal relation
 *
 * @method     ChildEcritureQuery joinWithJournal($joinType = Criteria::INNER_JOIN) Adds a join clause and with to the query using the Journal relation
 *
 * @method     ChildEcritureQuery leftJoinWithJournal() Adds a LEFT JOIN clause and with to the query using the Journal relation
 * @method     ChildEcritureQuery rightJoinWithJournal() Adds a RIGHT JOIN clause and with to the query using the Journal relation
 * @method     ChildEcritureQuery innerJoinWithJournal() Adds a INNER JOIN clause and with to the query using the Journal relation
 *
 * @method     ChildEcritureQuery leftJoinActivite($relationAlias = null) Adds a LEFT JOIN clause to the query using the Activite relation
 * @method     ChildEcritureQuery rightJoinActivite($relationAlias = null) Adds a RIGHT JOIN clause to the query using the Activite relation
 * @method     ChildEcritureQuery innerJoinActivite($relationAlias = null) Adds a INNER JOIN clause to the query using the Activite relation
 *
 * @method     ChildEcritureQuery joinWithActivite($joinType = Criteria::INNER_JOIN) Adds a join clause and with to the query using the Activite relation
 *
 * @method     ChildEcritureQuery leftJoinWithActivite() Adds a LEFT JOIN clause and with to the query using the Activite relation
 * @method     ChildEcritureQuery rightJoinWithActivite() Adds a RIGHT JOIN clause and with to the query using the Activite relation
 * @method     ChildEcritureQuery innerJoinWithActivite() Adds a INNER JOIN clause and with to the query using the Activite relation
 *
 * @method     ChildEcritureQuery leftJoinEntite($relationAlias = null) Adds a LEFT JOIN clause to the query using the Entite relation
 * @method     ChildEcritureQuery rightJoinEntite($relationAlias = null) Adds a RIGHT JOIN clause to the query using the Entite relation
 * @method     ChildEcritureQuery innerJoinEntite($relationAlias = null) Adds a INNER JOIN clause to the query using the Entite relation
 *
 * @method     ChildEcritureQuery joinWithEntite($joinType = Criteria::INNER_JOIN) Adds a join clause and with to the query using the Entite relation
 *
 * @method     ChildEcritureQuery leftJoinWithEntite() Adds a LEFT JOIN clause and with to the query using the Entite relation
 * @method     ChildEcritureQuery rightJoinWithEntite() Adds a RIGHT JOIN clause and with to the query using the Entite relation
 * @method     ChildEcritureQuery innerJoinWithEntite() Adds a INNER JOIN clause and with to the query using the Entite relation
 *
 * @method     \CompteQuery|\JournalQuery|\ActiviteQuery|\EntiteQuery endUse() Finalizes a secondary criteria and merges it with its primary Criteria
 *
 * @method     ChildEcriture findOne(ConnectionInterface $con = null) Return the first ChildEcriture matching the query
 * @method     ChildEcriture findOneOrCreate(ConnectionInterface $con = null) Return the first ChildEcriture matching the query, or a new ChildEcriture object populated from the query conditions when no match is found
 *
 * @method     ChildEcriture findOneByIdEcriture(string $id_ecriture) Return the first ChildEcriture filtered by the id_ecriture column
 * @method     ChildEcriture findOneByClassement(string $classement) Return the first ChildEcriture filtered by the classement column
 * @method     ChildEcriture findOneByCredit(double $credit) Return the first ChildEcriture filtered by the credit column
 * @method     ChildEcriture findOneByDebit(double $debit) Return the first ChildEcriture filtered by the debit column
 * @method     ChildEcriture findOneByNom(string $nom) Return the first ChildEcriture filtered by the nom column
 * @method     ChildEcriture findOneByIdCompte(string $id_compte) Return the first ChildEcriture filtered by the id_compte column
 * @method     ChildEcriture findOneByIdJournal(int $id_journal) Return the first ChildEcriture filtered by the id_journal column
 * @method     ChildEcriture findOneByIdActivite(string $id_activite) Return the first ChildEcriture filtered by the id_activite column
 * @method     ChildEcriture findOneByIdEntite(string $id_entite) Return the first ChildEcriture filtered by the id_entite column
 * @method     ChildEcriture findOneByLettrage(string $lettrage) Return the first ChildEcriture filtered by the lettrage column
 * @method     ChildEcriture findOneByDateLettrage(string $date_lettrage) Return the first ChildEcriture filtered by the date_lettrage column
 * @method     ChildEcriture findOneByDateEcriture(string $date_ecriture) Return the first ChildEcriture filtered by the date_ecriture column
 * @method     ChildEcriture findOneByEtat(int $etat) Return the first ChildEcriture filtered by the etat column
 * @method     ChildEcriture findOneByObservation(string $observation) Return the first ChildEcriture filtered by the observation column
 * @method     ChildEcriture findOneByIdPiece(string $id_piece) Return the first ChildEcriture filtered by the id_piece column
 * @method     ChildEcriture findOneByEcranSaisie(string $ecran_saisie) Return the first ChildEcriture filtered by the ecran_saisie column
 * @method     ChildEcriture findOneByContrePartie(boolean $contre_partie) Return the first ChildEcriture filtered by the contre_partie column
 * @method     ChildEcriture findOneByQuantite(double $quantite) Return the first ChildEcriture filtered by the quantite column
 * @method     ChildEcriture findOneByCreatedAt(string $created_at) Return the first ChildEcriture filtered by the created_at column
 * @method     ChildEcriture findOneByUpdatedAt(string $updated_at) Return the first ChildEcriture filtered by the updated_at column *

 * @method     ChildEcriture requirePk($key, ConnectionInterface $con = null) Return the ChildEcriture by primary key and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildEcriture requireOne(ConnectionInterface $con = null) Return the first ChildEcriture matching the query and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 *
 * @method     ChildEcriture requireOneByIdEcriture(string $id_ecriture) Return the first ChildEcriture filtered by the id_ecriture column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildEcriture requireOneByClassement(string $classement) Return the first ChildEcriture filtered by the classement column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildEcriture requireOneByCredit(double $credit) Return the first ChildEcriture filtered by the credit column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildEcriture requireOneByDebit(double $debit) Return the first ChildEcriture filtered by the debit column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildEcriture requireOneByNom(string $nom) Return the first ChildEcriture filtered by the nom column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildEcriture requireOneByIdCompte(string $id_compte) Return the first ChildEcriture filtered by the id_compte column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildEcriture requireOneByIdJournal(int $id_journal) Return the first ChildEcriture filtered by the id_journal column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildEcriture requireOneByIdActivite(string $id_activite) Return the first ChildEcriture filtered by the id_activite column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildEcriture requireOneByIdEntite(string $id_entite) Return the first ChildEcriture filtered by the id_entite column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildEcriture requireOneByLettrage(string $lettrage) Return the first ChildEcriture filtered by the lettrage column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildEcriture requireOneByDateLettrage(string $date_lettrage) Return the first ChildEcriture filtered by the date_lettrage column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildEcriture requireOneByDateEcriture(string $date_ecriture) Return the first ChildEcriture filtered by the date_ecriture column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildEcriture requireOneByEtat(int $etat) Return the first ChildEcriture filtered by the etat column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildEcriture requireOneByObservation(string $observation) Return the first ChildEcriture filtered by the observation column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildEcriture requireOneByIdPiece(string $id_piece) Return the first ChildEcriture filtered by the id_piece column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildEcriture requireOneByEcranSaisie(string $ecran_saisie) Return the first ChildEcriture filtered by the ecran_saisie column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildEcriture requireOneByContrePartie(boolean $contre_partie) Return the first ChildEcriture filtered by the contre_partie column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildEcriture requireOneByQuantite(double $quantite) Return the first ChildEcriture filtered by the quantite column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildEcriture requireOneByCreatedAt(string $created_at) Return the first ChildEcriture filtered by the created_at column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildEcriture requireOneByUpdatedAt(string $updated_at) Return the first ChildEcriture filtered by the updated_at column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 *
 * @method     ChildEcriture[]|ObjectCollection find(ConnectionInterface $con = null) Return ChildEcriture objects based on current ModelCriteria
 * @method     ChildEcriture[]|ObjectCollection findByIdEcriture(string $id_ecriture) Return ChildEcriture objects filtered by the id_ecriture column
 * @method     ChildEcriture[]|ObjectCollection findByClassement(string $classement) Return ChildEcriture objects filtered by the classement column
 * @method     ChildEcriture[]|ObjectCollection findByCredit(double $credit) Return ChildEcriture objects filtered by the credit column
 * @method     ChildEcriture[]|ObjectCollection findByDebit(double $debit) Return ChildEcriture objects filtered by the debit column
 * @method     ChildEcriture[]|ObjectCollection findByNom(string $nom) Return ChildEcriture objects filtered by the nom column
 * @method     ChildEcriture[]|ObjectCollection findByIdCompte(string $id_compte) Return ChildEcriture objects filtered by the id_compte column
 * @method     ChildEcriture[]|ObjectCollection findByIdJournal(int $id_journal) Return ChildEcriture objects filtered by the id_journal column
 * @method     ChildEcriture[]|ObjectCollection findByIdActivite(string $id_activite) Return ChildEcriture objects filtered by the id_activite column
 * @method     ChildEcriture[]|ObjectCollection findByIdEntite(string $id_entite) Return ChildEcriture objects filtered by the id_entite column
 * @method     ChildEcriture[]|ObjectCollection findByLettrage(string $lettrage) Return ChildEcriture objects filtered by the lettrage column
 * @method     ChildEcriture[]|ObjectCollection findByDateLettrage(string $date_lettrage) Return ChildEcriture objects filtered by the date_lettrage column
 * @method     ChildEcriture[]|ObjectCollection findByDateEcriture(string $date_ecriture) Return ChildEcriture objects filtered by the date_ecriture column
 * @method     ChildEcriture[]|ObjectCollection findByEtat(int $etat) Return ChildEcriture objects filtered by the etat column
 * @method     ChildEcriture[]|ObjectCollection findByObservation(string $observation) Return ChildEcriture objects filtered by the observation column
 * @method     ChildEcriture[]|ObjectCollection findByIdPiece(string $id_piece) Return ChildEcriture objects filtered by the id_piece column
 * @method     ChildEcriture[]|ObjectCollection findByEcranSaisie(string $ecran_saisie) Return ChildEcriture objects filtered by the ecran_saisie column
 * @method     ChildEcriture[]|ObjectCollection findByContrePartie(boolean $contre_partie) Return ChildEcriture objects filtered by the contre_partie column
 * @method     ChildEcriture[]|ObjectCollection findByQuantite(double $quantite) Return ChildEcriture objects filtered by the quantite column
 * @method     ChildEcriture[]|ObjectCollection findByCreatedAt(string $created_at) Return ChildEcriture objects filtered by the created_at column
 * @method     ChildEcriture[]|ObjectCollection findByUpdatedAt(string $updated_at) Return ChildEcriture objects filtered by the updated_at column
 * @method     ChildEcriture[]|\Propel\Runtime\Util\PropelModelPager paginate($page = 1, $maxPerPage = 10, ConnectionInterface $con = null) Issue a SELECT query based on the current ModelCriteria and uses a page and a maximum number of results per page to compute an offset and a limit
 *
 */
abstract class EcritureQuery extends ModelCriteria
{

    // archivable behavior
    protected $archiveOnDelete = true;
protected $entityNotFoundExceptionClass = '\\Propel\\Runtime\\Exception\\EntityNotFoundException';

    /**
     * Initializes internal state of \Base\EcritureQuery object.
     *
     * @param     string $dbName The database name
     * @param     string $modelName The phpName of a model, e.g. 'Book'
     * @param     string $modelAlias The alias for the model in this query, e.g. 'b'
     */
    public function __construct($dbName = 'simplasso', $modelName = '\\Ecriture', $modelAlias = null)
    {
        parent::__construct($dbName, $modelName, $modelAlias);
    }

    /**
     * Returns a new ChildEcritureQuery object.
     *
     * @param     string $modelAlias The alias of a model in the query
     * @param     Criteria $criteria Optional Criteria to build the query from
     *
     * @return ChildEcritureQuery
     */
    public static function create($modelAlias = null, Criteria $criteria = null)
    {
        if ($criteria instanceof ChildEcritureQuery) {
            return $criteria;
        }
        $query = new ChildEcritureQuery();
        if (null !== $modelAlias) {
            $query->setModelAlias($modelAlias);
        }
        if ($criteria instanceof Criteria) {
            $query->mergeWith($criteria);
        }

        return $query;
    }

    /**
     * Find object by primary key.
     * Propel uses the instance pool to skip the database if the object exists.
     * Go fast if the query is untouched.
     *
     * <code>
     * $obj  = $c->findPk(12, $con);
     * </code>
     *
     * @param mixed $key Primary key to use for the query
     * @param ConnectionInterface $con an optional connection object
     *
     * @return ChildEcriture|array|mixed the result, formatted by the current formatter
     */
    public function findPk($key, ConnectionInterface $con = null)
    {
        if ($key === null) {
            return null;
        }

        if ($con === null) {
            $con = Propel::getServiceContainer()->getReadConnection(EcritureTableMap::DATABASE_NAME);
        }

        $this->basePreSelect($con);

        if (
            $this->formatter || $this->modelAlias || $this->with || $this->select
            || $this->selectColumns || $this->asColumns || $this->selectModifiers
            || $this->map || $this->having || $this->joins
        ) {
            return $this->findPkComplex($key, $con);
        }

        if ((null !== ($obj = EcritureTableMap::getInstanceFromPool(null === $key || is_scalar($key) || is_callable([$key, '__toString']) ? (string) $key : $key)))) {
            // the object is already in the instance pool
            return $obj;
        }

        return $this->findPkSimple($key, $con);
    }

    /**
     * Find object by primary key using raw SQL to go fast.
     * Bypass doSelect() and the object formatter by using generated code.
     *
     * @param     mixed $key Primary key to use for the query
     * @param     ConnectionInterface $con A connection object
     *
     * @throws \Propel\Runtime\Exception\PropelException
     *
     * @return ChildEcriture A model object, or null if the key is not found
     */
    protected function findPkSimple($key, ConnectionInterface $con)
    {
        $sql = 'SELECT `id_ecriture`, `classement`, `credit`, `debit`, `nom`, `id_compte`, `id_journal`, `id_activite`, `id_entite`, `lettrage`, `date_lettrage`, `date_ecriture`, `etat`, `observation`, `id_piece`, `ecran_saisie`, `contre_partie`, `quantite`, `created_at`, `updated_at` FROM `assc_ecritures` WHERE `id_ecriture` = :p0';
        try {
            $stmt = $con->prepare($sql);
            $stmt->bindValue(':p0', $key, PDO::PARAM_INT);
            $stmt->execute();
        } catch (Exception $e) {
            Propel::log($e->getMessage(), Propel::LOG_ERR);
            throw new PropelException(sprintf('Unable to execute SELECT statement [%s]', $sql), 0, $e);
        }
        $obj = null;
        if ($row = $stmt->fetch(\PDO::FETCH_NUM)) {
            /** @var ChildEcriture $obj */
            $obj = new ChildEcriture();
            $obj->hydrate($row);
            EcritureTableMap::addInstanceToPool($obj, null === $key || is_scalar($key) || is_callable([$key, '__toString']) ? (string) $key : $key);
        }
        $stmt->closeCursor();

        return $obj;
    }

    /**
     * Find object by primary key.
     *
     * @param     mixed $key Primary key to use for the query
     * @param     ConnectionInterface $con A connection object
     *
     * @return ChildEcriture|array|mixed the result, formatted by the current formatter
     */
    protected function findPkComplex($key, ConnectionInterface $con)
    {
        // As the query uses a PK condition, no limit(1) is necessary.
        $criteria = $this->isKeepQuery() ? clone $this : $this;
        $dataFetcher = $criteria
            ->filterByPrimaryKey($key)
            ->doSelect($con);

        return $criteria->getFormatter()->init($criteria)->formatOne($dataFetcher);
    }

    /**
     * Find objects by primary key
     * <code>
     * $objs = $c->findPks(array(12, 56, 832), $con);
     * </code>
     * @param     array $keys Primary keys to use for the query
     * @param     ConnectionInterface $con an optional connection object
     *
     * @return ObjectCollection|array|mixed the list of results, formatted by the current formatter
     */
    public function findPks($keys, ConnectionInterface $con = null)
    {
        if (null === $con) {
            $con = Propel::getServiceContainer()->getReadConnection($this->getDbName());
        }
        $this->basePreSelect($con);
        $criteria = $this->isKeepQuery() ? clone $this : $this;
        $dataFetcher = $criteria
            ->filterByPrimaryKeys($keys)
            ->doSelect($con);

        return $criteria->getFormatter()->init($criteria)->format($dataFetcher);
    }

    /**
     * Filter the query by primary key
     *
     * @param     mixed $key Primary key to use for the query
     *
     * @return $this|ChildEcritureQuery The current query, for fluid interface
     */
    public function filterByPrimaryKey($key)
    {

        return $this->addUsingAlias(EcritureTableMap::COL_ID_ECRITURE, $key, Criteria::EQUAL);
    }

    /**
     * Filter the query by a list of primary keys
     *
     * @param     array $keys The list of primary key to use for the query
     *
     * @return $this|ChildEcritureQuery The current query, for fluid interface
     */
    public function filterByPrimaryKeys($keys)
    {

        return $this->addUsingAlias(EcritureTableMap::COL_ID_ECRITURE, $keys, Criteria::IN);
    }

    /**
     * Filter the query on the id_ecriture column
     *
     * Example usage:
     * <code>
     * $query->filterByIdEcriture(1234); // WHERE id_ecriture = 1234
     * $query->filterByIdEcriture(array(12, 34)); // WHERE id_ecriture IN (12, 34)
     * $query->filterByIdEcriture(array('min' => 12)); // WHERE id_ecriture > 12
     * </code>
     *
     * @param     mixed $idEcriture The value to use as filter.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildEcritureQuery The current query, for fluid interface
     */
    public function filterByIdEcriture($idEcriture = null, $comparison = null)
    {
        if (is_array($idEcriture)) {
            $useMinMax = false;
            if (isset($idEcriture['min'])) {
                $this->addUsingAlias(EcritureTableMap::COL_ID_ECRITURE, $idEcriture['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($idEcriture['max'])) {
                $this->addUsingAlias(EcritureTableMap::COL_ID_ECRITURE, $idEcriture['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(EcritureTableMap::COL_ID_ECRITURE, $idEcriture, $comparison);
    }

    /**
     * Filter the query on the classement column
     *
     * Example usage:
     * <code>
     * $query->filterByClassement('fooValue');   // WHERE classement = 'fooValue'
     * $query->filterByClassement('%fooValue%', Criteria::LIKE); // WHERE classement LIKE '%fooValue%'
     * </code>
     *
     * @param     string $classement The value to use as filter.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildEcritureQuery The current query, for fluid interface
     */
    public function filterByClassement($classement = null, $comparison = null)
    {
        if (null === $comparison) {
            if (is_array($classement)) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(EcritureTableMap::COL_CLASSEMENT, $classement, $comparison);
    }

    /**
     * Filter the query on the credit column
     *
     * Example usage:
     * <code>
     * $query->filterByCredit(1234); // WHERE credit = 1234
     * $query->filterByCredit(array(12, 34)); // WHERE credit IN (12, 34)
     * $query->filterByCredit(array('min' => 12)); // WHERE credit > 12
     * </code>
     *
     * @param     mixed $credit The value to use as filter.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildEcritureQuery The current query, for fluid interface
     */
    public function filterByCredit($credit = null, $comparison = null)
    {
        if (is_array($credit)) {
            $useMinMax = false;
            if (isset($credit['min'])) {
                $this->addUsingAlias(EcritureTableMap::COL_CREDIT, $credit['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($credit['max'])) {
                $this->addUsingAlias(EcritureTableMap::COL_CREDIT, $credit['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(EcritureTableMap::COL_CREDIT, $credit, $comparison);
    }

    /**
     * Filter the query on the debit column
     *
     * Example usage:
     * <code>
     * $query->filterByDebit(1234); // WHERE debit = 1234
     * $query->filterByDebit(array(12, 34)); // WHERE debit IN (12, 34)
     * $query->filterByDebit(array('min' => 12)); // WHERE debit > 12
     * </code>
     *
     * @param     mixed $debit The value to use as filter.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildEcritureQuery The current query, for fluid interface
     */
    public function filterByDebit($debit = null, $comparison = null)
    {
        if (is_array($debit)) {
            $useMinMax = false;
            if (isset($debit['min'])) {
                $this->addUsingAlias(EcritureTableMap::COL_DEBIT, $debit['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($debit['max'])) {
                $this->addUsingAlias(EcritureTableMap::COL_DEBIT, $debit['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(EcritureTableMap::COL_DEBIT, $debit, $comparison);
    }

    /**
     * Filter the query on the nom column
     *
     * Example usage:
     * <code>
     * $query->filterByNom('fooValue');   // WHERE nom = 'fooValue'
     * $query->filterByNom('%fooValue%', Criteria::LIKE); // WHERE nom LIKE '%fooValue%'
     * </code>
     *
     * @param     string $nom The value to use as filter.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildEcritureQuery The current query, for fluid interface
     */
    public function filterByNom($nom = null, $comparison = null)
    {
        if (null === $comparison) {
            if (is_array($nom)) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(EcritureTableMap::COL_NOM, $nom, $comparison);
    }

    /**
     * Filter the query on the id_compte column
     *
     * Example usage:
     * <code>
     * $query->filterByIdCompte(1234); // WHERE id_compte = 1234
     * $query->filterByIdCompte(array(12, 34)); // WHERE id_compte IN (12, 34)
     * $query->filterByIdCompte(array('min' => 12)); // WHERE id_compte > 12
     * </code>
     *
     * @see       filterByCompte()
     *
     * @param     mixed $idCompte The value to use as filter.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildEcritureQuery The current query, for fluid interface
     */
    public function filterByIdCompte($idCompte = null, $comparison = null)
    {
        if (is_array($idCompte)) {
            $useMinMax = false;
            if (isset($idCompte['min'])) {
                $this->addUsingAlias(EcritureTableMap::COL_ID_COMPTE, $idCompte['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($idCompte['max'])) {
                $this->addUsingAlias(EcritureTableMap::COL_ID_COMPTE, $idCompte['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(EcritureTableMap::COL_ID_COMPTE, $idCompte, $comparison);
    }

    /**
     * Filter the query on the id_journal column
     *
     * Example usage:
     * <code>
     * $query->filterByIdJournal(1234); // WHERE id_journal = 1234
     * $query->filterByIdJournal(array(12, 34)); // WHERE id_journal IN (12, 34)
     * $query->filterByIdJournal(array('min' => 12)); // WHERE id_journal > 12
     * </code>
     *
     * @see       filterByJournal()
     *
     * @param     mixed $idJournal The value to use as filter.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildEcritureQuery The current query, for fluid interface
     */
    public function filterByIdJournal($idJournal = null, $comparison = null)
    {
        if (is_array($idJournal)) {
            $useMinMax = false;
            if (isset($idJournal['min'])) {
                $this->addUsingAlias(EcritureTableMap::COL_ID_JOURNAL, $idJournal['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($idJournal['max'])) {
                $this->addUsingAlias(EcritureTableMap::COL_ID_JOURNAL, $idJournal['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(EcritureTableMap::COL_ID_JOURNAL, $idJournal, $comparison);
    }

    /**
     * Filter the query on the id_activite column
     *
     * Example usage:
     * <code>
     * $query->filterByIdActivite(1234); // WHERE id_activite = 1234
     * $query->filterByIdActivite(array(12, 34)); // WHERE id_activite IN (12, 34)
     * $query->filterByIdActivite(array('min' => 12)); // WHERE id_activite > 12
     * </code>
     *
     * @see       filterByActivite()
     *
     * @param     mixed $idActivite The value to use as filter.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildEcritureQuery The current query, for fluid interface
     */
    public function filterByIdActivite($idActivite = null, $comparison = null)
    {
        if (is_array($idActivite)) {
            $useMinMax = false;
            if (isset($idActivite['min'])) {
                $this->addUsingAlias(EcritureTableMap::COL_ID_ACTIVITE, $idActivite['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($idActivite['max'])) {
                $this->addUsingAlias(EcritureTableMap::COL_ID_ACTIVITE, $idActivite['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(EcritureTableMap::COL_ID_ACTIVITE, $idActivite, $comparison);
    }

    /**
     * Filter the query on the id_entite column
     *
     * Example usage:
     * <code>
     * $query->filterByIdEntite(1234); // WHERE id_entite = 1234
     * $query->filterByIdEntite(array(12, 34)); // WHERE id_entite IN (12, 34)
     * $query->filterByIdEntite(array('min' => 12)); // WHERE id_entite > 12
     * </code>
     *
     * @see       filterByEntite()
     *
     * @param     mixed $idEntite The value to use as filter.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildEcritureQuery The current query, for fluid interface
     */
    public function filterByIdEntite($idEntite = null, $comparison = null)
    {
        if (is_array($idEntite)) {
            $useMinMax = false;
            if (isset($idEntite['min'])) {
                $this->addUsingAlias(EcritureTableMap::COL_ID_ENTITE, $idEntite['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($idEntite['max'])) {
                $this->addUsingAlias(EcritureTableMap::COL_ID_ENTITE, $idEntite['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(EcritureTableMap::COL_ID_ENTITE, $idEntite, $comparison);
    }

    /**
     * Filter the query on the lettrage column
     *
     * Example usage:
     * <code>
     * $query->filterByLettrage('fooValue');   // WHERE lettrage = 'fooValue'
     * $query->filterByLettrage('%fooValue%', Criteria::LIKE); // WHERE lettrage LIKE '%fooValue%'
     * </code>
     *
     * @param     string $lettrage The value to use as filter.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildEcritureQuery The current query, for fluid interface
     */
    public function filterByLettrage($lettrage = null, $comparison = null)
    {
        if (null === $comparison) {
            if (is_array($lettrage)) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(EcritureTableMap::COL_LETTRAGE, $lettrage, $comparison);
    }

    /**
     * Filter the query on the date_lettrage column
     *
     * Example usage:
     * <code>
     * $query->filterByDateLettrage('2011-03-14'); // WHERE date_lettrage = '2011-03-14'
     * $query->filterByDateLettrage('now'); // WHERE date_lettrage = '2011-03-14'
     * $query->filterByDateLettrage(array('max' => 'yesterday')); // WHERE date_lettrage > '2011-03-13'
     * </code>
     *
     * @param     mixed $dateLettrage The value to use as filter.
     *              Values can be integers (unix timestamps), DateTime objects, or strings.
     *              Empty strings are treated as NULL.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildEcritureQuery The current query, for fluid interface
     */
    public function filterByDateLettrage($dateLettrage = null, $comparison = null)
    {
        if (is_array($dateLettrage)) {
            $useMinMax = false;
            if (isset($dateLettrage['min'])) {
                $this->addUsingAlias(EcritureTableMap::COL_DATE_LETTRAGE, $dateLettrage['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($dateLettrage['max'])) {
                $this->addUsingAlias(EcritureTableMap::COL_DATE_LETTRAGE, $dateLettrage['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(EcritureTableMap::COL_DATE_LETTRAGE, $dateLettrage, $comparison);
    }

    /**
     * Filter the query on the date_ecriture column
     *
     * Example usage:
     * <code>
     * $query->filterByDateEcriture('2011-03-14'); // WHERE date_ecriture = '2011-03-14'
     * $query->filterByDateEcriture('now'); // WHERE date_ecriture = '2011-03-14'
     * $query->filterByDateEcriture(array('max' => 'yesterday')); // WHERE date_ecriture > '2011-03-13'
     * </code>
     *
     * @param     mixed $dateEcriture The value to use as filter.
     *              Values can be integers (unix timestamps), DateTime objects, or strings.
     *              Empty strings are treated as NULL.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildEcritureQuery The current query, for fluid interface
     */
    public function filterByDateEcriture($dateEcriture = null, $comparison = null)
    {
        if (is_array($dateEcriture)) {
            $useMinMax = false;
            if (isset($dateEcriture['min'])) {
                $this->addUsingAlias(EcritureTableMap::COL_DATE_ECRITURE, $dateEcriture['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($dateEcriture['max'])) {
                $this->addUsingAlias(EcritureTableMap::COL_DATE_ECRITURE, $dateEcriture['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(EcritureTableMap::COL_DATE_ECRITURE, $dateEcriture, $comparison);
    }

    /**
     * Filter the query on the etat column
     *
     * Example usage:
     * <code>
     * $query->filterByEtat(1234); // WHERE etat = 1234
     * $query->filterByEtat(array(12, 34)); // WHERE etat IN (12, 34)
     * $query->filterByEtat(array('min' => 12)); // WHERE etat > 12
     * </code>
     *
     * @param     mixed $etat The value to use as filter.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildEcritureQuery The current query, for fluid interface
     */
    public function filterByEtat($etat = null, $comparison = null)
    {
        if (is_array($etat)) {
            $useMinMax = false;
            if (isset($etat['min'])) {
                $this->addUsingAlias(EcritureTableMap::COL_ETAT, $etat['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($etat['max'])) {
                $this->addUsingAlias(EcritureTableMap::COL_ETAT, $etat['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(EcritureTableMap::COL_ETAT, $etat, $comparison);
    }

    /**
     * Filter the query on the observation column
     *
     * Example usage:
     * <code>
     * $query->filterByObservation('fooValue');   // WHERE observation = 'fooValue'
     * $query->filterByObservation('%fooValue%', Criteria::LIKE); // WHERE observation LIKE '%fooValue%'
     * </code>
     *
     * @param     string $observation The value to use as filter.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildEcritureQuery The current query, for fluid interface
     */
    public function filterByObservation($observation = null, $comparison = null)
    {
        if (null === $comparison) {
            if (is_array($observation)) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(EcritureTableMap::COL_OBSERVATION, $observation, $comparison);
    }

    /**
     * Filter the query on the id_piece column
     *
     * Example usage:
     * <code>
     * $query->filterByIdPiece(1234); // WHERE id_piece = 1234
     * $query->filterByIdPiece(array(12, 34)); // WHERE id_piece IN (12, 34)
     * $query->filterByIdPiece(array('min' => 12)); // WHERE id_piece > 12
     * </code>
     *
     * @param     mixed $idPiece The value to use as filter.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildEcritureQuery The current query, for fluid interface
     */
    public function filterByIdPiece($idPiece = null, $comparison = null)
    {
        if (is_array($idPiece)) {
            $useMinMax = false;
            if (isset($idPiece['min'])) {
                $this->addUsingAlias(EcritureTableMap::COL_ID_PIECE, $idPiece['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($idPiece['max'])) {
                $this->addUsingAlias(EcritureTableMap::COL_ID_PIECE, $idPiece['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(EcritureTableMap::COL_ID_PIECE, $idPiece, $comparison);
    }

    /**
     * Filter the query on the ecran_saisie column
     *
     * Example usage:
     * <code>
     * $query->filterByEcranSaisie('fooValue');   // WHERE ecran_saisie = 'fooValue'
     * $query->filterByEcranSaisie('%fooValue%', Criteria::LIKE); // WHERE ecran_saisie LIKE '%fooValue%'
     * </code>
     *
     * @param     string $ecranSaisie The value to use as filter.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildEcritureQuery The current query, for fluid interface
     */
    public function filterByEcranSaisie($ecranSaisie = null, $comparison = null)
    {
        if (null === $comparison) {
            if (is_array($ecranSaisie)) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(EcritureTableMap::COL_ECRAN_SAISIE, $ecranSaisie, $comparison);
    }

    /**
     * Filter the query on the contre_partie column
     *
     * Example usage:
     * <code>
     * $query->filterByContrePartie(true); // WHERE contre_partie = true
     * $query->filterByContrePartie('yes'); // WHERE contre_partie = true
     * </code>
     *
     * @param     boolean|string $contrePartie The value to use as filter.
     *              Non-boolean arguments are converted using the following rules:
     *                * 1, '1', 'true',  'on',  and 'yes' are converted to boolean true
     *                * 0, '0', 'false', 'off', and 'no'  are converted to boolean false
     *              Check on string values is case insensitive (so 'FaLsE' is seen as 'false').
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildEcritureQuery The current query, for fluid interface
     */
    public function filterByContrePartie($contrePartie = null, $comparison = null)
    {
        if (is_string($contrePartie)) {
            $contrePartie = in_array(strtolower($contrePartie), array('false', 'off', '-', 'no', 'n', '0', '')) ? false : true;
        }

        return $this->addUsingAlias(EcritureTableMap::COL_CONTRE_PARTIE, $contrePartie, $comparison);
    }

    /**
     * Filter the query on the quantite column
     *
     * Example usage:
     * <code>
     * $query->filterByQuantite(1234); // WHERE quantite = 1234
     * $query->filterByQuantite(array(12, 34)); // WHERE quantite IN (12, 34)
     * $query->filterByQuantite(array('min' => 12)); // WHERE quantite > 12
     * </code>
     *
     * @param     mixed $quantite The value to use as filter.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildEcritureQuery The current query, for fluid interface
     */
    public function filterByQuantite($quantite = null, $comparison = null)
    {
        if (is_array($quantite)) {
            $useMinMax = false;
            if (isset($quantite['min'])) {
                $this->addUsingAlias(EcritureTableMap::COL_QUANTITE, $quantite['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($quantite['max'])) {
                $this->addUsingAlias(EcritureTableMap::COL_QUANTITE, $quantite['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(EcritureTableMap::COL_QUANTITE, $quantite, $comparison);
    }

    /**
     * Filter the query on the created_at column
     *
     * Example usage:
     * <code>
     * $query->filterByCreatedAt('2011-03-14'); // WHERE created_at = '2011-03-14'
     * $query->filterByCreatedAt('now'); // WHERE created_at = '2011-03-14'
     * $query->filterByCreatedAt(array('max' => 'yesterday')); // WHERE created_at > '2011-03-13'
     * </code>
     *
     * @param     mixed $createdAt The value to use as filter.
     *              Values can be integers (unix timestamps), DateTime objects, or strings.
     *              Empty strings are treated as NULL.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildEcritureQuery The current query, for fluid interface
     */
    public function filterByCreatedAt($createdAt = null, $comparison = null)
    {
        if (is_array($createdAt)) {
            $useMinMax = false;
            if (isset($createdAt['min'])) {
                $this->addUsingAlias(EcritureTableMap::COL_CREATED_AT, $createdAt['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($createdAt['max'])) {
                $this->addUsingAlias(EcritureTableMap::COL_CREATED_AT, $createdAt['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(EcritureTableMap::COL_CREATED_AT, $createdAt, $comparison);
    }

    /**
     * Filter the query on the updated_at column
     *
     * Example usage:
     * <code>
     * $query->filterByUpdatedAt('2011-03-14'); // WHERE updated_at = '2011-03-14'
     * $query->filterByUpdatedAt('now'); // WHERE updated_at = '2011-03-14'
     * $query->filterByUpdatedAt(array('max' => 'yesterday')); // WHERE updated_at > '2011-03-13'
     * </code>
     *
     * @param     mixed $updatedAt The value to use as filter.
     *              Values can be integers (unix timestamps), DateTime objects, or strings.
     *              Empty strings are treated as NULL.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildEcritureQuery The current query, for fluid interface
     */
    public function filterByUpdatedAt($updatedAt = null, $comparison = null)
    {
        if (is_array($updatedAt)) {
            $useMinMax = false;
            if (isset($updatedAt['min'])) {
                $this->addUsingAlias(EcritureTableMap::COL_UPDATED_AT, $updatedAt['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($updatedAt['max'])) {
                $this->addUsingAlias(EcritureTableMap::COL_UPDATED_AT, $updatedAt['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(EcritureTableMap::COL_UPDATED_AT, $updatedAt, $comparison);
    }

    /**
     * Filter the query by a related \Compte object
     *
     * @param \Compte|ObjectCollection $compte The related object(s) to use as filter
     * @param string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @throws \Propel\Runtime\Exception\PropelException
     *
     * @return ChildEcritureQuery The current query, for fluid interface
     */
    public function filterByCompte($compte, $comparison = null)
    {
        if ($compte instanceof \Compte) {
            return $this
                ->addUsingAlias(EcritureTableMap::COL_ID_COMPTE, $compte->getIdCompte(), $comparison);
        } elseif ($compte instanceof ObjectCollection) {
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }

            return $this
                ->addUsingAlias(EcritureTableMap::COL_ID_COMPTE, $compte->toKeyValue('PrimaryKey', 'IdCompte'), $comparison);
        } else {
            throw new PropelException('filterByCompte() only accepts arguments of type \Compte or Collection');
        }
    }

    /**
     * Adds a JOIN clause to the query using the Compte relation
     *
     * @param     string $relationAlias optional alias for the relation
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return $this|ChildEcritureQuery The current query, for fluid interface
     */
    public function joinCompte($relationAlias = null, $joinType = Criteria::INNER_JOIN)
    {
        $tableMap = $this->getTableMap();
        $relationMap = $tableMap->getRelation('Compte');

        // create a ModelJoin object for this join
        $join = new ModelJoin();
        $join->setJoinType($joinType);
        $join->setRelationMap($relationMap, $this->useAliasInSQL ? $this->getModelAlias() : null, $relationAlias);
        if ($previousJoin = $this->getPreviousJoin()) {
            $join->setPreviousJoin($previousJoin);
        }

        // add the ModelJoin to the current object
        if ($relationAlias) {
            $this->addAlias($relationAlias, $relationMap->getRightTable()->getName());
            $this->addJoinObject($join, $relationAlias);
        } else {
            $this->addJoinObject($join, 'Compte');
        }

        return $this;
    }

    /**
     * Use the Compte relation Compte object
     *
     * @see useQuery()
     *
     * @param     string $relationAlias optional alias for the relation,
     *                                   to be used as main alias in the secondary query
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return \CompteQuery A secondary query class using the current class as primary query
     */
    public function useCompteQuery($relationAlias = null, $joinType = Criteria::INNER_JOIN)
    {
        return $this
            ->joinCompte($relationAlias, $joinType)
            ->useQuery($relationAlias ? $relationAlias : 'Compte', '\CompteQuery');
    }

    /**
     * Filter the query by a related \Journal object
     *
     * @param \Journal|ObjectCollection $journal The related object(s) to use as filter
     * @param string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @throws \Propel\Runtime\Exception\PropelException
     *
     * @return ChildEcritureQuery The current query, for fluid interface
     */
    public function filterByJournal($journal, $comparison = null)
    {
        if ($journal instanceof \Journal) {
            return $this
                ->addUsingAlias(EcritureTableMap::COL_ID_JOURNAL, $journal->getIdJournal(), $comparison);
        } elseif ($journal instanceof ObjectCollection) {
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }

            return $this
                ->addUsingAlias(EcritureTableMap::COL_ID_JOURNAL, $journal->toKeyValue('PrimaryKey', 'IdJournal'), $comparison);
        } else {
            throw new PropelException('filterByJournal() only accepts arguments of type \Journal or Collection');
        }
    }

    /**
     * Adds a JOIN clause to the query using the Journal relation
     *
     * @param     string $relationAlias optional alias for the relation
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return $this|ChildEcritureQuery The current query, for fluid interface
     */
    public function joinJournal($relationAlias = null, $joinType = Criteria::INNER_JOIN)
    {
        $tableMap = $this->getTableMap();
        $relationMap = $tableMap->getRelation('Journal');

        // create a ModelJoin object for this join
        $join = new ModelJoin();
        $join->setJoinType($joinType);
        $join->setRelationMap($relationMap, $this->useAliasInSQL ? $this->getModelAlias() : null, $relationAlias);
        if ($previousJoin = $this->getPreviousJoin()) {
            $join->setPreviousJoin($previousJoin);
        }

        // add the ModelJoin to the current object
        if ($relationAlias) {
            $this->addAlias($relationAlias, $relationMap->getRightTable()->getName());
            $this->addJoinObject($join, $relationAlias);
        } else {
            $this->addJoinObject($join, 'Journal');
        }

        return $this;
    }

    /**
     * Use the Journal relation Journal object
     *
     * @see useQuery()
     *
     * @param     string $relationAlias optional alias for the relation,
     *                                   to be used as main alias in the secondary query
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return \JournalQuery A secondary query class using the current class as primary query
     */
    public function useJournalQuery($relationAlias = null, $joinType = Criteria::INNER_JOIN)
    {
        return $this
            ->joinJournal($relationAlias, $joinType)
            ->useQuery($relationAlias ? $relationAlias : 'Journal', '\JournalQuery');
    }

    /**
     * Filter the query by a related \Activite object
     *
     * @param \Activite|ObjectCollection $activite The related object(s) to use as filter
     * @param string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @throws \Propel\Runtime\Exception\PropelException
     *
     * @return ChildEcritureQuery The current query, for fluid interface
     */
    public function filterByActivite($activite, $comparison = null)
    {
        if ($activite instanceof \Activite) {
            return $this
                ->addUsingAlias(EcritureTableMap::COL_ID_ACTIVITE, $activite->getIdActivite(), $comparison);
        } elseif ($activite instanceof ObjectCollection) {
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }

            return $this
                ->addUsingAlias(EcritureTableMap::COL_ID_ACTIVITE, $activite->toKeyValue('PrimaryKey', 'IdActivite'), $comparison);
        } else {
            throw new PropelException('filterByActivite() only accepts arguments of type \Activite or Collection');
        }
    }

    /**
     * Adds a JOIN clause to the query using the Activite relation
     *
     * @param     string $relationAlias optional alias for the relation
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return $this|ChildEcritureQuery The current query, for fluid interface
     */
    public function joinActivite($relationAlias = null, $joinType = Criteria::LEFT_JOIN)
    {
        $tableMap = $this->getTableMap();
        $relationMap = $tableMap->getRelation('Activite');

        // create a ModelJoin object for this join
        $join = new ModelJoin();
        $join->setJoinType($joinType);
        $join->setRelationMap($relationMap, $this->useAliasInSQL ? $this->getModelAlias() : null, $relationAlias);
        if ($previousJoin = $this->getPreviousJoin()) {
            $join->setPreviousJoin($previousJoin);
        }

        // add the ModelJoin to the current object
        if ($relationAlias) {
            $this->addAlias($relationAlias, $relationMap->getRightTable()->getName());
            $this->addJoinObject($join, $relationAlias);
        } else {
            $this->addJoinObject($join, 'Activite');
        }

        return $this;
    }

    /**
     * Use the Activite relation Activite object
     *
     * @see useQuery()
     *
     * @param     string $relationAlias optional alias for the relation,
     *                                   to be used as main alias in the secondary query
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return \ActiviteQuery A secondary query class using the current class as primary query
     */
    public function useActiviteQuery($relationAlias = null, $joinType = Criteria::LEFT_JOIN)
    {
        return $this
            ->joinActivite($relationAlias, $joinType)
            ->useQuery($relationAlias ? $relationAlias : 'Activite', '\ActiviteQuery');
    }

    /**
     * Filter the query by a related \Entite object
     *
     * @param \Entite|ObjectCollection $entite The related object(s) to use as filter
     * @param string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @throws \Propel\Runtime\Exception\PropelException
     *
     * @return ChildEcritureQuery The current query, for fluid interface
     */
    public function filterByEntite($entite, $comparison = null)
    {
        if ($entite instanceof \Entite) {
            return $this
                ->addUsingAlias(EcritureTableMap::COL_ID_ENTITE, $entite->getIdEntite(), $comparison);
        } elseif ($entite instanceof ObjectCollection) {
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }

            return $this
                ->addUsingAlias(EcritureTableMap::COL_ID_ENTITE, $entite->toKeyValue('PrimaryKey', 'IdEntite'), $comparison);
        } else {
            throw new PropelException('filterByEntite() only accepts arguments of type \Entite or Collection');
        }
    }

    /**
     * Adds a JOIN clause to the query using the Entite relation
     *
     * @param     string $relationAlias optional alias for the relation
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return $this|ChildEcritureQuery The current query, for fluid interface
     */
    public function joinEntite($relationAlias = null, $joinType = Criteria::INNER_JOIN)
    {
        $tableMap = $this->getTableMap();
        $relationMap = $tableMap->getRelation('Entite');

        // create a ModelJoin object for this join
        $join = new ModelJoin();
        $join->setJoinType($joinType);
        $join->setRelationMap($relationMap, $this->useAliasInSQL ? $this->getModelAlias() : null, $relationAlias);
        if ($previousJoin = $this->getPreviousJoin()) {
            $join->setPreviousJoin($previousJoin);
        }

        // add the ModelJoin to the current object
        if ($relationAlias) {
            $this->addAlias($relationAlias, $relationMap->getRightTable()->getName());
            $this->addJoinObject($join, $relationAlias);
        } else {
            $this->addJoinObject($join, 'Entite');
        }

        return $this;
    }

    /**
     * Use the Entite relation Entite object
     *
     * @see useQuery()
     *
     * @param     string $relationAlias optional alias for the relation,
     *                                   to be used as main alias in the secondary query
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return \EntiteQuery A secondary query class using the current class as primary query
     */
    public function useEntiteQuery($relationAlias = null, $joinType = Criteria::INNER_JOIN)
    {
        return $this
            ->joinEntite($relationAlias, $joinType)
            ->useQuery($relationAlias ? $relationAlias : 'Entite', '\EntiteQuery');
    }

    /**
     * Exclude object from result
     *
     * @param   ChildEcriture $ecriture Object to remove from the list of results
     *
     * @return $this|ChildEcritureQuery The current query, for fluid interface
     */
    public function prune($ecriture = null)
    {
        if ($ecriture) {
            $this->addUsingAlias(EcritureTableMap::COL_ID_ECRITURE, $ecriture->getIdEcriture(), Criteria::NOT_EQUAL);
        }

        return $this;
    }

    /**
     * Code to execute before every DELETE statement
     *
     * @param     ConnectionInterface $con The connection object used by the query
     */
    protected function basePreDelete(ConnectionInterface $con)
    {
        // archivable behavior

        if ($this->archiveOnDelete) {
            $this->archive($con);
        } else {
            $this->archiveOnDelete = true;
        }


        return $this->preDelete($con);
    }

    /**
     * Deletes all rows from the assc_ecritures table.
     *
     * @param ConnectionInterface $con the connection to use
     * @return int The number of affected rows (if supported by underlying database driver).
     */
    public function doDeleteAll(ConnectionInterface $con = null)
    {
        if (null === $con) {
            $con = Propel::getServiceContainer()->getWriteConnection(EcritureTableMap::DATABASE_NAME);
        }

        // use transaction because $criteria could contain info
        // for more than one table or we could emulating ON DELETE CASCADE, etc.
        return $con->transaction(function () use ($con) {
            $affectedRows = 0; // initialize var to track total num of affected rows
            $affectedRows += parent::doDeleteAll($con);
            // Because this db requires some delete cascade/set null emulation, we have to
            // clear the cached instance *after* the emulation has happened (since
            // instances get re-added by the select statement contained therein).
            EcritureTableMap::clearInstancePool();
            EcritureTableMap::clearRelatedInstancePool();

            return $affectedRows;
        });
    }

    /**
     * Performs a DELETE on the database based on the current ModelCriteria
     *
     * @param ConnectionInterface $con the connection to use
     * @return int             The number of affected rows (if supported by underlying database driver).  This includes CASCADE-related rows
     *                         if supported by native driver or if emulated using Propel.
     * @throws PropelException Any exceptions caught during processing will be
     *                         rethrown wrapped into a PropelException.
     */
    public function delete(ConnectionInterface $con = null)
    {
        if (null === $con) {
            $con = Propel::getServiceContainer()->getWriteConnection(EcritureTableMap::DATABASE_NAME);
        }

        $criteria = $this;

        // Set the correct dbName
        $criteria->setDbName(EcritureTableMap::DATABASE_NAME);

        // use transaction because $criteria could contain info
        // for more than one table or we could emulating ON DELETE CASCADE, etc.
        return $con->transaction(function () use ($con, $criteria) {
            $affectedRows = 0; // initialize var to track total num of affected rows

            EcritureTableMap::removeInstanceFromPool($criteria);

            $affectedRows += ModelCriteria::delete($con);
            EcritureTableMap::clearRelatedInstancePool();

            return $affectedRows;
        });
    }

    // timestampable behavior

    /**
     * Filter by the latest updated
     *
     * @param      int $nbDays Maximum age of the latest update in days
     *
     * @return     $this|ChildEcritureQuery The current query, for fluid interface
     */
    public function recentlyUpdated($nbDays = 7)
    {
        return $this->addUsingAlias(EcritureTableMap::COL_UPDATED_AT, time() - $nbDays * 24 * 60 * 60, Criteria::GREATER_EQUAL);
    }

    /**
     * Order by update date desc
     *
     * @return     $this|ChildEcritureQuery The current query, for fluid interface
     */
    public function lastUpdatedFirst()
    {
        return $this->addDescendingOrderByColumn(EcritureTableMap::COL_UPDATED_AT);
    }

    /**
     * Order by update date asc
     *
     * @return     $this|ChildEcritureQuery The current query, for fluid interface
     */
    public function firstUpdatedFirst()
    {
        return $this->addAscendingOrderByColumn(EcritureTableMap::COL_UPDATED_AT);
    }

    /**
     * Order by create date desc
     *
     * @return     $this|ChildEcritureQuery The current query, for fluid interface
     */
    public function lastCreatedFirst()
    {
        return $this->addDescendingOrderByColumn(EcritureTableMap::COL_CREATED_AT);
    }

    /**
     * Filter by the latest created
     *
     * @param      int $nbDays Maximum age of in days
     *
     * @return     $this|ChildEcritureQuery The current query, for fluid interface
     */
    public function recentlyCreated($nbDays = 7)
    {
        return $this->addUsingAlias(EcritureTableMap::COL_CREATED_AT, time() - $nbDays * 24 * 60 * 60, Criteria::GREATER_EQUAL);
    }

    /**
     * Order by create date asc
     *
     * @return     $this|ChildEcritureQuery The current query, for fluid interface
     */
    public function firstCreatedFirst()
    {
        return $this->addAscendingOrderByColumn(EcritureTableMap::COL_CREATED_AT);
    }

    // archivable behavior

    /**
     * Copy the data of the objects satisfying the query into ChildAsscEcrituresArchive archive objects.
     * The archived objects are then saved.
     * If any of the objects has already been archived, the archived object
     * is updated and not duplicated.
     * Warning: This termination methods issues 2n+1 queries.
     *
     * @param      ConnectionInterface $con    Connection to use.
     * @param      Boolean $useLittleMemory    Whether or not to use OnDemandFormatter to retrieve objects.
     *               Set to false if the identity map matters.
     *               Set to true (default) to use less memory.
     *
     * @return     int the number of archived objects
     */
    public function archive($con = null, $useLittleMemory = true)
    {
        $criteria = clone $this;
        // prepare the query
        $criteria->setWith(array());
        if ($useLittleMemory) {
            $criteria->setFormatter(ModelCriteria::FORMAT_ON_DEMAND);
        }
        if ($con === null) {
            $con = Propel::getServiceContainer()->getWriteConnection(EcritureTableMap::DATABASE_NAME);
        }

        return $con->transaction(function () use ($con, $criteria) {
            $totalArchivedObjects = 0;

            // archive all results one by one
            foreach ($criteria->find($con) as $object) {
                $object->archive($con);
                $totalArchivedObjects++;
            }

            return $totalArchivedObjects;
        });
    }

    /**
     * Enable/disable auto-archiving on delete for the next query.
     *
     * @param boolean True if the query must archive deleted objects, false otherwise.
     */
    public function setArchiveOnDelete($archiveOnDelete)
    {
        $this->archiveOnDelete = $archiveOnDelete;
    }

    /**
     * Delete records matching the current query without archiving them.
     *
     * @param      ConnectionInterface $con    Connection to use.
     *
     * @return integer the number of deleted rows
     */
    public function deleteWithoutArchive($con = null)
    {
        $this->archiveOnDelete = false;

        return $this->delete($con);
    }

    /**
     * Delete all records without archiving them.
     *
     * @param      ConnectionInterface $con    Connection to use.
     *
     * @return integer the number of deleted rows
     */
    public function deleteAllWithoutArchive($con = null)
    {
        $this->archiveOnDelete = false;

        return $this->deleteAll($con);
    }

} // EcritureQuery
