<?php

namespace Base;

use \AssoPrixsArchive as ChildAssoPrixsArchive;
use \AssoPrixsArchiveQuery as ChildAssoPrixsArchiveQuery;
use \Exception;
use \PDO;
use Map\AssoPrixsArchiveTableMap;
use Propel\Runtime\Propel;
use Propel\Runtime\ActiveQuery\Criteria;
use Propel\Runtime\ActiveQuery\ModelCriteria;
use Propel\Runtime\Collection\ObjectCollection;
use Propel\Runtime\Connection\ConnectionInterface;
use Propel\Runtime\Exception\PropelException;

/**
 * Base class that represents a query for the 'asso_prixs_archive' table.
 *
 *
 *
 * @method     ChildAssoPrixsArchiveQuery orderByIdPrix($order = Criteria::ASC) Order by the id_prix column
 * @method     ChildAssoPrixsArchiveQuery orderByIdPrestation($order = Criteria::ASC) Order by the id_prestation column
 * @method     ChildAssoPrixsArchiveQuery orderByMontant($order = Criteria::ASC) Order by the montant column
 * @method     ChildAssoPrixsArchiveQuery orderByDateDebut($order = Criteria::ASC) Order by the date_debut column
 * @method     ChildAssoPrixsArchiveQuery orderByDateFin($order = Criteria::ASC) Order by the date_fin column
 * @method     ChildAssoPrixsArchiveQuery orderByObservation($order = Criteria::ASC) Order by the observation column
 * @method     ChildAssoPrixsArchiveQuery orderByCreatedAt($order = Criteria::ASC) Order by the created_at column
 * @method     ChildAssoPrixsArchiveQuery orderByUpdatedAt($order = Criteria::ASC) Order by the updated_at column
 * @method     ChildAssoPrixsArchiveQuery orderByArchivedAt($order = Criteria::ASC) Order by the archived_at column
 *
 * @method     ChildAssoPrixsArchiveQuery groupByIdPrix() Group by the id_prix column
 * @method     ChildAssoPrixsArchiveQuery groupByIdPrestation() Group by the id_prestation column
 * @method     ChildAssoPrixsArchiveQuery groupByMontant() Group by the montant column
 * @method     ChildAssoPrixsArchiveQuery groupByDateDebut() Group by the date_debut column
 * @method     ChildAssoPrixsArchiveQuery groupByDateFin() Group by the date_fin column
 * @method     ChildAssoPrixsArchiveQuery groupByObservation() Group by the observation column
 * @method     ChildAssoPrixsArchiveQuery groupByCreatedAt() Group by the created_at column
 * @method     ChildAssoPrixsArchiveQuery groupByUpdatedAt() Group by the updated_at column
 * @method     ChildAssoPrixsArchiveQuery groupByArchivedAt() Group by the archived_at column
 *
 * @method     ChildAssoPrixsArchiveQuery leftJoin($relation) Adds a LEFT JOIN clause to the query
 * @method     ChildAssoPrixsArchiveQuery rightJoin($relation) Adds a RIGHT JOIN clause to the query
 * @method     ChildAssoPrixsArchiveQuery innerJoin($relation) Adds a INNER JOIN clause to the query
 *
 * @method     ChildAssoPrixsArchiveQuery leftJoinWith($relation) Adds a LEFT JOIN clause and with to the query
 * @method     ChildAssoPrixsArchiveQuery rightJoinWith($relation) Adds a RIGHT JOIN clause and with to the query
 * @method     ChildAssoPrixsArchiveQuery innerJoinWith($relation) Adds a INNER JOIN clause and with to the query
 *
 * @method     ChildAssoPrixsArchive findOne(ConnectionInterface $con = null) Return the first ChildAssoPrixsArchive matching the query
 * @method     ChildAssoPrixsArchive findOneOrCreate(ConnectionInterface $con = null) Return the first ChildAssoPrixsArchive matching the query, or a new ChildAssoPrixsArchive object populated from the query conditions when no match is found
 *
 * @method     ChildAssoPrixsArchive findOneByIdPrix(string $id_prix) Return the first ChildAssoPrixsArchive filtered by the id_prix column
 * @method     ChildAssoPrixsArchive findOneByIdPrestation(string $id_prestation) Return the first ChildAssoPrixsArchive filtered by the id_prestation column
 * @method     ChildAssoPrixsArchive findOneByMontant(double $montant) Return the first ChildAssoPrixsArchive filtered by the montant column
 * @method     ChildAssoPrixsArchive findOneByDateDebut(string $date_debut) Return the first ChildAssoPrixsArchive filtered by the date_debut column
 * @method     ChildAssoPrixsArchive findOneByDateFin(string $date_fin) Return the first ChildAssoPrixsArchive filtered by the date_fin column
 * @method     ChildAssoPrixsArchive findOneByObservation(string $observation) Return the first ChildAssoPrixsArchive filtered by the observation column
 * @method     ChildAssoPrixsArchive findOneByCreatedAt(string $created_at) Return the first ChildAssoPrixsArchive filtered by the created_at column
 * @method     ChildAssoPrixsArchive findOneByUpdatedAt(string $updated_at) Return the first ChildAssoPrixsArchive filtered by the updated_at column
 * @method     ChildAssoPrixsArchive findOneByArchivedAt(string $archived_at) Return the first ChildAssoPrixsArchive filtered by the archived_at column *

 * @method     ChildAssoPrixsArchive requirePk($key, ConnectionInterface $con = null) Return the ChildAssoPrixsArchive by primary key and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildAssoPrixsArchive requireOne(ConnectionInterface $con = null) Return the first ChildAssoPrixsArchive matching the query and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 *
 * @method     ChildAssoPrixsArchive requireOneByIdPrix(string $id_prix) Return the first ChildAssoPrixsArchive filtered by the id_prix column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildAssoPrixsArchive requireOneByIdPrestation(string $id_prestation) Return the first ChildAssoPrixsArchive filtered by the id_prestation column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildAssoPrixsArchive requireOneByMontant(double $montant) Return the first ChildAssoPrixsArchive filtered by the montant column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildAssoPrixsArchive requireOneByDateDebut(string $date_debut) Return the first ChildAssoPrixsArchive filtered by the date_debut column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildAssoPrixsArchive requireOneByDateFin(string $date_fin) Return the first ChildAssoPrixsArchive filtered by the date_fin column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildAssoPrixsArchive requireOneByObservation(string $observation) Return the first ChildAssoPrixsArchive filtered by the observation column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildAssoPrixsArchive requireOneByCreatedAt(string $created_at) Return the first ChildAssoPrixsArchive filtered by the created_at column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildAssoPrixsArchive requireOneByUpdatedAt(string $updated_at) Return the first ChildAssoPrixsArchive filtered by the updated_at column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildAssoPrixsArchive requireOneByArchivedAt(string $archived_at) Return the first ChildAssoPrixsArchive filtered by the archived_at column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 *
 * @method     ChildAssoPrixsArchive[]|ObjectCollection find(ConnectionInterface $con = null) Return ChildAssoPrixsArchive objects based on current ModelCriteria
 * @method     ChildAssoPrixsArchive[]|ObjectCollection findByIdPrix(string $id_prix) Return ChildAssoPrixsArchive objects filtered by the id_prix column
 * @method     ChildAssoPrixsArchive[]|ObjectCollection findByIdPrestation(string $id_prestation) Return ChildAssoPrixsArchive objects filtered by the id_prestation column
 * @method     ChildAssoPrixsArchive[]|ObjectCollection findByMontant(double $montant) Return ChildAssoPrixsArchive objects filtered by the montant column
 * @method     ChildAssoPrixsArchive[]|ObjectCollection findByDateDebut(string $date_debut) Return ChildAssoPrixsArchive objects filtered by the date_debut column
 * @method     ChildAssoPrixsArchive[]|ObjectCollection findByDateFin(string $date_fin) Return ChildAssoPrixsArchive objects filtered by the date_fin column
 * @method     ChildAssoPrixsArchive[]|ObjectCollection findByObservation(string $observation) Return ChildAssoPrixsArchive objects filtered by the observation column
 * @method     ChildAssoPrixsArchive[]|ObjectCollection findByCreatedAt(string $created_at) Return ChildAssoPrixsArchive objects filtered by the created_at column
 * @method     ChildAssoPrixsArchive[]|ObjectCollection findByUpdatedAt(string $updated_at) Return ChildAssoPrixsArchive objects filtered by the updated_at column
 * @method     ChildAssoPrixsArchive[]|ObjectCollection findByArchivedAt(string $archived_at) Return ChildAssoPrixsArchive objects filtered by the archived_at column
 * @method     ChildAssoPrixsArchive[]|\Propel\Runtime\Util\PropelModelPager paginate($page = 1, $maxPerPage = 10, ConnectionInterface $con = null) Issue a SELECT query based on the current ModelCriteria and uses a page and a maximum number of results per page to compute an offset and a limit
 *
 */
abstract class AssoPrixsArchiveQuery extends ModelCriteria
{
    protected $entityNotFoundExceptionClass = '\\Propel\\Runtime\\Exception\\EntityNotFoundException';

    /**
     * Initializes internal state of \Base\AssoPrixsArchiveQuery object.
     *
     * @param     string $dbName The database name
     * @param     string $modelName The phpName of a model, e.g. 'Book'
     * @param     string $modelAlias The alias for the model in this query, e.g. 'b'
     */
    public function __construct($dbName = 'simplasso', $modelName = '\\AssoPrixsArchive', $modelAlias = null)
    {
        parent::__construct($dbName, $modelName, $modelAlias);
    }

    /**
     * Returns a new ChildAssoPrixsArchiveQuery object.
     *
     * @param     string $modelAlias The alias of a model in the query
     * @param     Criteria $criteria Optional Criteria to build the query from
     *
     * @return ChildAssoPrixsArchiveQuery
     */
    public static function create($modelAlias = null, Criteria $criteria = null)
    {
        if ($criteria instanceof ChildAssoPrixsArchiveQuery) {
            return $criteria;
        }
        $query = new ChildAssoPrixsArchiveQuery();
        if (null !== $modelAlias) {
            $query->setModelAlias($modelAlias);
        }
        if ($criteria instanceof Criteria) {
            $query->mergeWith($criteria);
        }

        return $query;
    }

    /**
     * Find object by primary key.
     * Propel uses the instance pool to skip the database if the object exists.
     * Go fast if the query is untouched.
     *
     * <code>
     * $obj  = $c->findPk(12, $con);
     * </code>
     *
     * @param mixed $key Primary key to use for the query
     * @param ConnectionInterface $con an optional connection object
     *
     * @return ChildAssoPrixsArchive|array|mixed the result, formatted by the current formatter
     */
    public function findPk($key, ConnectionInterface $con = null)
    {
        if ($key === null) {
            return null;
        }

        if ($con === null) {
            $con = Propel::getServiceContainer()->getReadConnection(AssoPrixsArchiveTableMap::DATABASE_NAME);
        }

        $this->basePreSelect($con);

        if (
            $this->formatter || $this->modelAlias || $this->with || $this->select
            || $this->selectColumns || $this->asColumns || $this->selectModifiers
            || $this->map || $this->having || $this->joins
        ) {
            return $this->findPkComplex($key, $con);
        }

        if ((null !== ($obj = AssoPrixsArchiveTableMap::getInstanceFromPool(null === $key || is_scalar($key) || is_callable([$key, '__toString']) ? (string) $key : $key)))) {
            // the object is already in the instance pool
            return $obj;
        }

        return $this->findPkSimple($key, $con);
    }

    /**
     * Find object by primary key using raw SQL to go fast.
     * Bypass doSelect() and the object formatter by using generated code.
     *
     * @param     mixed $key Primary key to use for the query
     * @param     ConnectionInterface $con A connection object
     *
     * @throws \Propel\Runtime\Exception\PropelException
     *
     * @return ChildAssoPrixsArchive A model object, or null if the key is not found
     */
    protected function findPkSimple($key, ConnectionInterface $con)
    {
        $sql = 'SELECT `id_prix`, `id_prestation`, `montant`, `date_debut`, `date_fin`, `observation`, `created_at`, `updated_at`, `archived_at` FROM `asso_prixs_archive` WHERE `id_prix` = :p0';
        try {
            $stmt = $con->prepare($sql);
            $stmt->bindValue(':p0', $key, PDO::PARAM_INT);
            $stmt->execute();
        } catch (Exception $e) {
            Propel::log($e->getMessage(), Propel::LOG_ERR);
            throw new PropelException(sprintf('Unable to execute SELECT statement [%s]', $sql), 0, $e);
        }
        $obj = null;
        if ($row = $stmt->fetch(\PDO::FETCH_NUM)) {
            /** @var ChildAssoPrixsArchive $obj */
            $obj = new ChildAssoPrixsArchive();
            $obj->hydrate($row);
            AssoPrixsArchiveTableMap::addInstanceToPool($obj, null === $key || is_scalar($key) || is_callable([$key, '__toString']) ? (string) $key : $key);
        }
        $stmt->closeCursor();

        return $obj;
    }

    /**
     * Find object by primary key.
     *
     * @param     mixed $key Primary key to use for the query
     * @param     ConnectionInterface $con A connection object
     *
     * @return ChildAssoPrixsArchive|array|mixed the result, formatted by the current formatter
     */
    protected function findPkComplex($key, ConnectionInterface $con)
    {
        // As the query uses a PK condition, no limit(1) is necessary.
        $criteria = $this->isKeepQuery() ? clone $this : $this;
        $dataFetcher = $criteria
            ->filterByPrimaryKey($key)
            ->doSelect($con);

        return $criteria->getFormatter()->init($criteria)->formatOne($dataFetcher);
    }

    /**
     * Find objects by primary key
     * <code>
     * $objs = $c->findPks(array(12, 56, 832), $con);
     * </code>
     * @param     array $keys Primary keys to use for the query
     * @param     ConnectionInterface $con an optional connection object
     *
     * @return ObjectCollection|array|mixed the list of results, formatted by the current formatter
     */
    public function findPks($keys, ConnectionInterface $con = null)
    {
        if (null === $con) {
            $con = Propel::getServiceContainer()->getReadConnection($this->getDbName());
        }
        $this->basePreSelect($con);
        $criteria = $this->isKeepQuery() ? clone $this : $this;
        $dataFetcher = $criteria
            ->filterByPrimaryKeys($keys)
            ->doSelect($con);

        return $criteria->getFormatter()->init($criteria)->format($dataFetcher);
    }

    /**
     * Filter the query by primary key
     *
     * @param     mixed $key Primary key to use for the query
     *
     * @return $this|ChildAssoPrixsArchiveQuery The current query, for fluid interface
     */
    public function filterByPrimaryKey($key)
    {

        return $this->addUsingAlias(AssoPrixsArchiveTableMap::COL_ID_PRIX, $key, Criteria::EQUAL);
    }

    /**
     * Filter the query by a list of primary keys
     *
     * @param     array $keys The list of primary key to use for the query
     *
     * @return $this|ChildAssoPrixsArchiveQuery The current query, for fluid interface
     */
    public function filterByPrimaryKeys($keys)
    {

        return $this->addUsingAlias(AssoPrixsArchiveTableMap::COL_ID_PRIX, $keys, Criteria::IN);
    }

    /**
     * Filter the query on the id_prix column
     *
     * Example usage:
     * <code>
     * $query->filterByIdPrix(1234); // WHERE id_prix = 1234
     * $query->filterByIdPrix(array(12, 34)); // WHERE id_prix IN (12, 34)
     * $query->filterByIdPrix(array('min' => 12)); // WHERE id_prix > 12
     * </code>
     *
     * @param     mixed $idPrix The value to use as filter.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildAssoPrixsArchiveQuery The current query, for fluid interface
     */
    public function filterByIdPrix($idPrix = null, $comparison = null)
    {
        if (is_array($idPrix)) {
            $useMinMax = false;
            if (isset($idPrix['min'])) {
                $this->addUsingAlias(AssoPrixsArchiveTableMap::COL_ID_PRIX, $idPrix['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($idPrix['max'])) {
                $this->addUsingAlias(AssoPrixsArchiveTableMap::COL_ID_PRIX, $idPrix['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(AssoPrixsArchiveTableMap::COL_ID_PRIX, $idPrix, $comparison);
    }

    /**
     * Filter the query on the id_prestation column
     *
     * Example usage:
     * <code>
     * $query->filterByIdPrestation(1234); // WHERE id_prestation = 1234
     * $query->filterByIdPrestation(array(12, 34)); // WHERE id_prestation IN (12, 34)
     * $query->filterByIdPrestation(array('min' => 12)); // WHERE id_prestation > 12
     * </code>
     *
     * @param     mixed $idPrestation The value to use as filter.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildAssoPrixsArchiveQuery The current query, for fluid interface
     */
    public function filterByIdPrestation($idPrestation = null, $comparison = null)
    {
        if (is_array($idPrestation)) {
            $useMinMax = false;
            if (isset($idPrestation['min'])) {
                $this->addUsingAlias(AssoPrixsArchiveTableMap::COL_ID_PRESTATION, $idPrestation['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($idPrestation['max'])) {
                $this->addUsingAlias(AssoPrixsArchiveTableMap::COL_ID_PRESTATION, $idPrestation['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(AssoPrixsArchiveTableMap::COL_ID_PRESTATION, $idPrestation, $comparison);
    }

    /**
     * Filter the query on the montant column
     *
     * Example usage:
     * <code>
     * $query->filterByMontant(1234); // WHERE montant = 1234
     * $query->filterByMontant(array(12, 34)); // WHERE montant IN (12, 34)
     * $query->filterByMontant(array('min' => 12)); // WHERE montant > 12
     * </code>
     *
     * @param     mixed $montant The value to use as filter.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildAssoPrixsArchiveQuery The current query, for fluid interface
     */
    public function filterByMontant($montant = null, $comparison = null)
    {
        if (is_array($montant)) {
            $useMinMax = false;
            if (isset($montant['min'])) {
                $this->addUsingAlias(AssoPrixsArchiveTableMap::COL_MONTANT, $montant['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($montant['max'])) {
                $this->addUsingAlias(AssoPrixsArchiveTableMap::COL_MONTANT, $montant['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(AssoPrixsArchiveTableMap::COL_MONTANT, $montant, $comparison);
    }

    /**
     * Filter the query on the date_debut column
     *
     * Example usage:
     * <code>
     * $query->filterByDateDebut('2011-03-14'); // WHERE date_debut = '2011-03-14'
     * $query->filterByDateDebut('now'); // WHERE date_debut = '2011-03-14'
     * $query->filterByDateDebut(array('max' => 'yesterday')); // WHERE date_debut > '2011-03-13'
     * </code>
     *
     * @param     mixed $dateDebut The value to use as filter.
     *              Values can be integers (unix timestamps), DateTime objects, or strings.
     *              Empty strings are treated as NULL.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildAssoPrixsArchiveQuery The current query, for fluid interface
     */
    public function filterByDateDebut($dateDebut = null, $comparison = null)
    {
        if (is_array($dateDebut)) {
            $useMinMax = false;
            if (isset($dateDebut['min'])) {
                $this->addUsingAlias(AssoPrixsArchiveTableMap::COL_DATE_DEBUT, $dateDebut['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($dateDebut['max'])) {
                $this->addUsingAlias(AssoPrixsArchiveTableMap::COL_DATE_DEBUT, $dateDebut['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(AssoPrixsArchiveTableMap::COL_DATE_DEBUT, $dateDebut, $comparison);
    }

    /**
     * Filter the query on the date_fin column
     *
     * Example usage:
     * <code>
     * $query->filterByDateFin('2011-03-14'); // WHERE date_fin = '2011-03-14'
     * $query->filterByDateFin('now'); // WHERE date_fin = '2011-03-14'
     * $query->filterByDateFin(array('max' => 'yesterday')); // WHERE date_fin > '2011-03-13'
     * </code>
     *
     * @param     mixed $dateFin The value to use as filter.
     *              Values can be integers (unix timestamps), DateTime objects, or strings.
     *              Empty strings are treated as NULL.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildAssoPrixsArchiveQuery The current query, for fluid interface
     */
    public function filterByDateFin($dateFin = null, $comparison = null)
    {
        if (is_array($dateFin)) {
            $useMinMax = false;
            if (isset($dateFin['min'])) {
                $this->addUsingAlias(AssoPrixsArchiveTableMap::COL_DATE_FIN, $dateFin['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($dateFin['max'])) {
                $this->addUsingAlias(AssoPrixsArchiveTableMap::COL_DATE_FIN, $dateFin['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(AssoPrixsArchiveTableMap::COL_DATE_FIN, $dateFin, $comparison);
    }

    /**
     * Filter the query on the observation column
     *
     * Example usage:
     * <code>
     * $query->filterByObservation('fooValue');   // WHERE observation = 'fooValue'
     * $query->filterByObservation('%fooValue%', Criteria::LIKE); // WHERE observation LIKE '%fooValue%'
     * </code>
     *
     * @param     string $observation The value to use as filter.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildAssoPrixsArchiveQuery The current query, for fluid interface
     */
    public function filterByObservation($observation = null, $comparison = null)
    {
        if (null === $comparison) {
            if (is_array($observation)) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(AssoPrixsArchiveTableMap::COL_OBSERVATION, $observation, $comparison);
    }

    /**
     * Filter the query on the created_at column
     *
     * Example usage:
     * <code>
     * $query->filterByCreatedAt('2011-03-14'); // WHERE created_at = '2011-03-14'
     * $query->filterByCreatedAt('now'); // WHERE created_at = '2011-03-14'
     * $query->filterByCreatedAt(array('max' => 'yesterday')); // WHERE created_at > '2011-03-13'
     * </code>
     *
     * @param     mixed $createdAt The value to use as filter.
     *              Values can be integers (unix timestamps), DateTime objects, or strings.
     *              Empty strings are treated as NULL.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildAssoPrixsArchiveQuery The current query, for fluid interface
     */
    public function filterByCreatedAt($createdAt = null, $comparison = null)
    {
        if (is_array($createdAt)) {
            $useMinMax = false;
            if (isset($createdAt['min'])) {
                $this->addUsingAlias(AssoPrixsArchiveTableMap::COL_CREATED_AT, $createdAt['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($createdAt['max'])) {
                $this->addUsingAlias(AssoPrixsArchiveTableMap::COL_CREATED_AT, $createdAt['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(AssoPrixsArchiveTableMap::COL_CREATED_AT, $createdAt, $comparison);
    }

    /**
     * Filter the query on the updated_at column
     *
     * Example usage:
     * <code>
     * $query->filterByUpdatedAt('2011-03-14'); // WHERE updated_at = '2011-03-14'
     * $query->filterByUpdatedAt('now'); // WHERE updated_at = '2011-03-14'
     * $query->filterByUpdatedAt(array('max' => 'yesterday')); // WHERE updated_at > '2011-03-13'
     * </code>
     *
     * @param     mixed $updatedAt The value to use as filter.
     *              Values can be integers (unix timestamps), DateTime objects, or strings.
     *              Empty strings are treated as NULL.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildAssoPrixsArchiveQuery The current query, for fluid interface
     */
    public function filterByUpdatedAt($updatedAt = null, $comparison = null)
    {
        if (is_array($updatedAt)) {
            $useMinMax = false;
            if (isset($updatedAt['min'])) {
                $this->addUsingAlias(AssoPrixsArchiveTableMap::COL_UPDATED_AT, $updatedAt['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($updatedAt['max'])) {
                $this->addUsingAlias(AssoPrixsArchiveTableMap::COL_UPDATED_AT, $updatedAt['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(AssoPrixsArchiveTableMap::COL_UPDATED_AT, $updatedAt, $comparison);
    }

    /**
     * Filter the query on the archived_at column
     *
     * Example usage:
     * <code>
     * $query->filterByArchivedAt('2011-03-14'); // WHERE archived_at = '2011-03-14'
     * $query->filterByArchivedAt('now'); // WHERE archived_at = '2011-03-14'
     * $query->filterByArchivedAt(array('max' => 'yesterday')); // WHERE archived_at > '2011-03-13'
     * </code>
     *
     * @param     mixed $archivedAt The value to use as filter.
     *              Values can be integers (unix timestamps), DateTime objects, or strings.
     *              Empty strings are treated as NULL.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildAssoPrixsArchiveQuery The current query, for fluid interface
     */
    public function filterByArchivedAt($archivedAt = null, $comparison = null)
    {
        if (is_array($archivedAt)) {
            $useMinMax = false;
            if (isset($archivedAt['min'])) {
                $this->addUsingAlias(AssoPrixsArchiveTableMap::COL_ARCHIVED_AT, $archivedAt['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($archivedAt['max'])) {
                $this->addUsingAlias(AssoPrixsArchiveTableMap::COL_ARCHIVED_AT, $archivedAt['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(AssoPrixsArchiveTableMap::COL_ARCHIVED_AT, $archivedAt, $comparison);
    }

    /**
     * Exclude object from result
     *
     * @param   ChildAssoPrixsArchive $assoPrixsArchive Object to remove from the list of results
     *
     * @return $this|ChildAssoPrixsArchiveQuery The current query, for fluid interface
     */
    public function prune($assoPrixsArchive = null)
    {
        if ($assoPrixsArchive) {
            $this->addUsingAlias(AssoPrixsArchiveTableMap::COL_ID_PRIX, $assoPrixsArchive->getIdPrix(), Criteria::NOT_EQUAL);
        }

        return $this;
    }

    /**
     * Deletes all rows from the asso_prixs_archive table.
     *
     * @param ConnectionInterface $con the connection to use
     * @return int The number of affected rows (if supported by underlying database driver).
     */
    public function doDeleteAll(ConnectionInterface $con = null)
    {
        if (null === $con) {
            $con = Propel::getServiceContainer()->getWriteConnection(AssoPrixsArchiveTableMap::DATABASE_NAME);
        }

        // use transaction because $criteria could contain info
        // for more than one table or we could emulating ON DELETE CASCADE, etc.
        return $con->transaction(function () use ($con) {
            $affectedRows = 0; // initialize var to track total num of affected rows
            $affectedRows += parent::doDeleteAll($con);
            // Because this db requires some delete cascade/set null emulation, we have to
            // clear the cached instance *after* the emulation has happened (since
            // instances get re-added by the select statement contained therein).
            AssoPrixsArchiveTableMap::clearInstancePool();
            AssoPrixsArchiveTableMap::clearRelatedInstancePool();

            return $affectedRows;
        });
    }

    /**
     * Performs a DELETE on the database based on the current ModelCriteria
     *
     * @param ConnectionInterface $con the connection to use
     * @return int             The number of affected rows (if supported by underlying database driver).  This includes CASCADE-related rows
     *                         if supported by native driver or if emulated using Propel.
     * @throws PropelException Any exceptions caught during processing will be
     *                         rethrown wrapped into a PropelException.
     */
    public function delete(ConnectionInterface $con = null)
    {
        if (null === $con) {
            $con = Propel::getServiceContainer()->getWriteConnection(AssoPrixsArchiveTableMap::DATABASE_NAME);
        }

        $criteria = $this;

        // Set the correct dbName
        $criteria->setDbName(AssoPrixsArchiveTableMap::DATABASE_NAME);

        // use transaction because $criteria could contain info
        // for more than one table or we could emulating ON DELETE CASCADE, etc.
        return $con->transaction(function () use ($con, $criteria) {
            $affectedRows = 0; // initialize var to track total num of affected rows

            AssoPrixsArchiveTableMap::removeInstanceFromPool($criteria);

            $affectedRows += ModelCriteria::delete($con);
            AssoPrixsArchiveTableMap::clearRelatedInstancePool();

            return $affectedRows;
        });
    }

} // AssoPrixsArchiveQuery
