<?php

namespace Base;

use \AssoIndividusArchive as ChildAssoIndividusArchive;
use \AssoIndividusArchiveQuery as ChildAssoIndividusArchiveQuery;
use \Autorisation as ChildAutorisation;
use \AutorisationQuery as ChildAutorisationQuery;
use \CourrierLien as ChildCourrierLien;
use \CourrierLienQuery as ChildCourrierLienQuery;
use \Individu as ChildIndividu;
use \IndividuQuery as ChildIndividuQuery;
use \Log as ChildLog;
use \LogQuery as ChildLogQuery;
use \Membre as ChildMembre;
use \MembreIndividu as ChildMembreIndividu;
use \MembreIndividuQuery as ChildMembreIndividuQuery;
use \MembreQuery as ChildMembreQuery;
use \Notification as ChildNotification;
use \NotificationIndividu as ChildNotificationIndividu;
use \NotificationIndividuQuery as ChildNotificationIndividuQuery;
use \NotificationQuery as ChildNotificationQuery;
use \Servicerendu as ChildServicerendu;
use \ServicerenduIndividu as ChildServicerenduIndividu;
use \ServicerenduIndividuQuery as ChildServicerenduIndividuQuery;
use \ServicerenduQuery as ChildServicerenduQuery;
use \DateTime;
use \Exception;
use \PDO;
use Map\AutorisationTableMap;
use Map\CourrierLienTableMap;
use Map\IndividuTableMap;
use Map\LogTableMap;
use Map\MembreIndividuTableMap;
use Map\MembreTableMap;
use Map\NotificationIndividuTableMap;
use Map\ServicerenduIndividuTableMap;
use Propel\Runtime\Propel;
use Propel\Runtime\ActiveQuery\Criteria;
use Propel\Runtime\ActiveQuery\ModelCriteria;
use Propel\Runtime\ActiveRecord\ActiveRecordInterface;
use Propel\Runtime\Collection\Collection;
use Propel\Runtime\Collection\ObjectCollection;
use Propel\Runtime\Connection\ConnectionInterface;
use Propel\Runtime\Exception\BadMethodCallException;
use Propel\Runtime\Exception\LogicException;
use Propel\Runtime\Exception\PropelException;
use Propel\Runtime\Map\TableMap;
use Propel\Runtime\Parser\AbstractParser;
use Propel\Runtime\Util\PropelDateTime;

/**
 * Base class that represents a row from the 'asso_individus' table.
 *
 *
 *
 * @package    propel.generator..Base
 */
abstract class Individu implements ActiveRecordInterface
{
    /**
     * TableMap class name
     */
    const TABLE_MAP = '\\Map\\IndividuTableMap';


    /**
     * attribute to determine if this object has previously been saved.
     * @var boolean
     */
    protected $new = true;

    /**
     * attribute to determine whether this object has been deleted.
     * @var boolean
     */
    protected $deleted = false;

    /**
     * The columns that have been modified in current object.
     * Tracking modified columns allows us to only update modified columns.
     * @var array
     */
    protected $modifiedColumns = array();

    /**
     * The (virtual) columns that are added at runtime
     * The formatters can add supplementary columns based on a resultset
     * @var array
     */
    protected $virtualColumns = array();

    /**
     * The value for the id_individu field.
     *
     * @var        string
     */
    protected $id_individu;

    /**
     * The value for the nom field.
     *
     * @var        string
     */
    protected $nom;

    /**
     * The value for the bio field.
     *
     * @var        string
     */
    protected $bio;

    /**
     * The value for the email field.
     *
     * @var        string
     */
    protected $email;

    /**
     * The value for the login field.
     *
     * @var        string
     */
    protected $login;

    /**
     * The value for the pass field.
     *
     * @var        string
     */
    protected $pass;

    /**
     * The value for the alea_actuel field.
     *
     * @var        string
     */
    protected $alea_actuel;

    /**
     * The value for the alea_futur field.
     *
     * @var        string
     */
    protected $alea_futur;

    /**
     * The value for the token field.
     *
     * @var        string
     */
    protected $token;

    /**
     * The value for the token_time field.
     *
     * @var        string
     */
    protected $token_time;

    /**
     * The value for the civilite field.
     *
     * @var        string
     */
    protected $civilite;

    /**
     * The value for the sexe field.
     *
     * @var        string
     */
    protected $sexe;

    /**
     * The value for the nom_famille field.
     *
     * @var        string
     */
    protected $nom_famille;

    /**
     * The value for the prenom field.
     *
     * @var        string
     */
    protected $prenom;

    /**
     * The value for the naissance field.
     *
     * @var        DateTime
     */
    protected $naissance;

    /**
     * The value for the adresse field.
     *
     * @var        string
     */
    protected $adresse;

    /**
     * The value for the codepostal field.
     *
     * @var        string
     */
    protected $codepostal;

    /**
     * The value for the ville field.
     *
     * @var        string
     */
    protected $ville;

    /**
     * The value for the pays field.
     *
     * @var        string
     */
    protected $pays;

    /**
     * The value for the telephone field.
     *
     * @var        string
     */
    protected $telephone;

    /**
     * The value for the telephone_pro field.
     *
     * @var        string
     */
    protected $telephone_pro;

    /**
     * The value for the fax field.
     *
     * @var        string
     */
    protected $fax;

    /**
     * The value for the mobile field.
     *
     * @var        string
     */
    protected $mobile;

    /**
     * The value for the url field.
     *
     * @var        string
     */
    protected $url;

    /**
     * The value for the profession field.
     *
     * @var        string
     */
    protected $profession;

    /**
     * The value for the contact_souhait field.
     *
     * Note: this column has a database default value of: true
     * @var        boolean
     */
    protected $contact_souhait;

    /**
     * The value for the observation field.
     *
     * @var        string
     */
    protected $observation;

    /**
     * The value for the created_at field.
     *
     * @var        DateTime
     */
    protected $created_at;

    /**
     * The value for the updated_at field.
     *
     * @var        DateTime
     */
    protected $updated_at;

    /**
     * @var        ObjectCollection|ChildAutorisation[] Collection to store aggregation of ChildAutorisation objects.
     */
    protected $collAutorisations;
    protected $collAutorisationsPartial;

    /**
     * @var        ObjectCollection|ChildLog[] Collection to store aggregation of ChildLog objects.
     */
    protected $collLogs;
    protected $collLogsPartial;

    /**
     * @var        ObjectCollection|ChildMembre[] Collection to store aggregation of ChildMembre objects.
     */
    protected $collMembres0s;
    protected $collMembres0sPartial;

    /**
     * @var        ObjectCollection|ChildMembreIndividu[] Collection to store aggregation of ChildMembreIndividu objects.
     */
    protected $collMembreIndividus;
    protected $collMembreIndividusPartial;

    /**
     * @var        ObjectCollection|ChildServicerenduIndividu[] Collection to store aggregation of ChildServicerenduIndividu objects.
     */
    protected $collServicerenduIndividus;
    protected $collServicerenduIndividusPartial;

    /**
     * @var        ObjectCollection|ChildNotificationIndividu[] Collection to store aggregation of ChildNotificationIndividu objects.
     */
    protected $collNotificationIndividus;
    protected $collNotificationIndividusPartial;

    /**
     * @var        ObjectCollection|ChildCourrierLien[] Collection to store aggregation of ChildCourrierLien objects.
     */
    protected $collCourrierLiens;
    protected $collCourrierLiensPartial;

    /**
     * @var        ObjectCollection|ChildMembre[] Cross Collection to store aggregation of ChildMembre objects.
     */
    protected $collMembres;

    /**
     * @var bool
     */
    protected $collMembresPartial;

    /**
     * @var        ObjectCollection|ChildServicerendu[] Cross Collection to store aggregation of ChildServicerendu objects.
     */
    protected $collServicerendus;

    /**
     * @var bool
     */
    protected $collServicerendusPartial;

    /**
     * @var        ObjectCollection|ChildNotification[] Cross Collection to store aggregation of ChildNotification objects.
     */
    protected $collNotifications;

    /**
     * @var bool
     */
    protected $collNotificationsPartial;

    /**
     * Flag to prevent endless save loop, if this object is referenced
     * by another object which falls in this transaction.
     *
     * @var boolean
     */
    protected $alreadyInSave = false;

    // archivable behavior
    protected $archiveOnDelete = true;

    /**
     * An array of objects scheduled for deletion.
     * @var ObjectCollection|ChildMembre[]
     */
    protected $membresScheduledForDeletion = null;

    /**
     * An array of objects scheduled for deletion.
     * @var ObjectCollection|ChildServicerendu[]
     */
    protected $servicerendusScheduledForDeletion = null;

    /**
     * An array of objects scheduled for deletion.
     * @var ObjectCollection|ChildNotification[]
     */
    protected $notificationsScheduledForDeletion = null;

    /**
     * An array of objects scheduled for deletion.
     * @var ObjectCollection|ChildAutorisation[]
     */
    protected $autorisationsScheduledForDeletion = null;

    /**
     * An array of objects scheduled for deletion.
     * @var ObjectCollection|ChildLog[]
     */
    protected $logsScheduledForDeletion = null;

    /**
     * An array of objects scheduled for deletion.
     * @var ObjectCollection|ChildMembre[]
     */
    protected $membres0sScheduledForDeletion = null;

    /**
     * An array of objects scheduled for deletion.
     * @var ObjectCollection|ChildMembreIndividu[]
     */
    protected $membreIndividusScheduledForDeletion = null;

    /**
     * An array of objects scheduled for deletion.
     * @var ObjectCollection|ChildServicerenduIndividu[]
     */
    protected $servicerenduIndividusScheduledForDeletion = null;

    /**
     * An array of objects scheduled for deletion.
     * @var ObjectCollection|ChildNotificationIndividu[]
     */
    protected $notificationIndividusScheduledForDeletion = null;

    /**
     * An array of objects scheduled for deletion.
     * @var ObjectCollection|ChildCourrierLien[]
     */
    protected $courrierLiensScheduledForDeletion = null;

    /**
     * Applies default values to this object.
     * This method should be called from the object's constructor (or
     * equivalent initialization method).
     * @see __construct()
     */
    public function applyDefaultValues()
    {
        $this->contact_souhait = true;
    }

    /**
     * Initializes internal state of Base\Individu object.
     * @see applyDefaults()
     */
    public function __construct()
    {
        $this->applyDefaultValues();
    }

    /**
     * Returns whether the object has been modified.
     *
     * @return boolean True if the object has been modified.
     */
    public function isModified()
    {
        return !!$this->modifiedColumns;
    }

    /**
     * Has specified column been modified?
     *
     * @param  string  $col column fully qualified name (TableMap::TYPE_COLNAME), e.g. Book::AUTHOR_ID
     * @return boolean True if $col has been modified.
     */
    public function isColumnModified($col)
    {
        return $this->modifiedColumns && isset($this->modifiedColumns[$col]);
    }

    /**
     * Get the columns that have been modified in this object.
     * @return array A unique list of the modified column names for this object.
     */
    public function getModifiedColumns()
    {
        return $this->modifiedColumns ? array_keys($this->modifiedColumns) : [];
    }

    /**
     * Returns whether the object has ever been saved.  This will
     * be false, if the object was retrieved from storage or was created
     * and then saved.
     *
     * @return boolean true, if the object has never been persisted.
     */
    public function isNew()
    {
        return $this->new;
    }

    /**
     * Setter for the isNew attribute.  This method will be called
     * by Propel-generated children and objects.
     *
     * @param boolean $b the state of the object.
     */
    public function setNew($b)
    {
        $this->new = (boolean) $b;
    }

    /**
     * Whether this object has been deleted.
     * @return boolean The deleted state of this object.
     */
    public function isDeleted()
    {
        return $this->deleted;
    }

    /**
     * Specify whether this object has been deleted.
     * @param  boolean $b The deleted state of this object.
     * @return void
     */
    public function setDeleted($b)
    {
        $this->deleted = (boolean) $b;
    }

    /**
     * Sets the modified state for the object to be false.
     * @param  string $col If supplied, only the specified column is reset.
     * @return void
     */
    public function resetModified($col = null)
    {
        if (null !== $col) {
            if (isset($this->modifiedColumns[$col])) {
                unset($this->modifiedColumns[$col]);
            }
        } else {
            $this->modifiedColumns = array();
        }
    }

    /**
     * Compares this with another <code>Individu</code> instance.  If
     * <code>obj</code> is an instance of <code>Individu</code>, delegates to
     * <code>equals(Individu)</code>.  Otherwise, returns <code>false</code>.
     *
     * @param  mixed   $obj The object to compare to.
     * @return boolean Whether equal to the object specified.
     */
    public function equals($obj)
    {
        if (!$obj instanceof static) {
            return false;
        }

        if ($this === $obj) {
            return true;
        }

        if (null === $this->getPrimaryKey() || null === $obj->getPrimaryKey()) {
            return false;
        }

        return $this->getPrimaryKey() === $obj->getPrimaryKey();
    }

    /**
     * Get the associative array of the virtual columns in this object
     *
     * @return array
     */
    public function getVirtualColumns()
    {
        return $this->virtualColumns;
    }

    /**
     * Checks the existence of a virtual column in this object
     *
     * @param  string  $name The virtual column name
     * @return boolean
     */
    public function hasVirtualColumn($name)
    {
        return array_key_exists($name, $this->virtualColumns);
    }

    /**
     * Get the value of a virtual column in this object
     *
     * @param  string $name The virtual column name
     * @return mixed
     *
     * @throws PropelException
     */
    public function getVirtualColumn($name)
    {
        if (!$this->hasVirtualColumn($name)) {
            throw new PropelException(sprintf('Cannot get value of inexistent virtual column %s.', $name));
        }

        return $this->virtualColumns[$name];
    }

    /**
     * Set the value of a virtual column in this object
     *
     * @param string $name  The virtual column name
     * @param mixed  $value The value to give to the virtual column
     *
     * @return $this|Individu The current object, for fluid interface
     */
    public function setVirtualColumn($name, $value)
    {
        $this->virtualColumns[$name] = $value;

        return $this;
    }

    /**
     * Logs a message using Propel::log().
     *
     * @param  string  $msg
     * @param  int     $priority One of the Propel::LOG_* logging levels
     * @return boolean
     */
    protected function log($msg, $priority = Propel::LOG_INFO)
    {
        return Propel::log(get_class($this) . ': ' . $msg, $priority);
    }

    /**
     * Export the current object properties to a string, using a given parser format
     * <code>
     * $book = BookQuery::create()->findPk(9012);
     * echo $book->exportTo('JSON');
     *  => {"Id":9012,"Title":"Don Juan","ISBN":"0140422161","Price":12.99,"PublisherId":1234,"AuthorId":5678}');
     * </code>
     *
     * @param  mixed   $parser                 A AbstractParser instance, or a format name ('XML', 'YAML', 'JSON', 'CSV')
     * @param  boolean $includeLazyLoadColumns (optional) Whether to include lazy load(ed) columns. Defaults to TRUE.
     * @return string  The exported data
     */
    public function exportTo($parser, $includeLazyLoadColumns = true)
    {
        if (!$parser instanceof AbstractParser) {
            $parser = AbstractParser::getParser($parser);
        }

        return $parser->fromArray($this->toArray(TableMap::TYPE_PHPNAME, $includeLazyLoadColumns, array(), true));
    }

    /**
     * Clean up internal collections prior to serializing
     * Avoids recursive loops that turn into segmentation faults when serializing
     */
    public function __sleep()
    {
        $this->clearAllReferences();

        $cls = new \ReflectionClass($this);
        $propertyNames = [];
        $serializableProperties = array_diff($cls->getProperties(), $cls->getProperties(\ReflectionProperty::IS_STATIC));

        foreach($serializableProperties as $property) {
            $propertyNames[] = $property->getName();
        }

        return $propertyNames;
    }

    /**
     * Get the [id_individu] column value.
     *
     * @return string
     */
    public function getIdIndividu()
    {
        return $this->id_individu;
    }

    /**
     * Get the [nom] column value.
     *
     * @return string
     */
    public function getNom()
    {
        return $this->nom;
    }

    /**
     * Get the [bio] column value.
     *
     * @return string
     */
    public function getBio()
    {
        return $this->bio;
    }

    /**
     * Get the [email] column value.
     *
     * @return string
     */
    public function getEmail()
    {
        return $this->email;
    }

    /**
     * Get the [login] column value.
     *
     * @return string
     */
    public function getLogin()
    {
        return $this->login;
    }

    /**
     * Get the [pass] column value.
     *
     * @return string
     */
    public function getPass()
    {
        return $this->pass;
    }

    /**
     * Get the [alea_actuel] column value.
     *
     * @return string
     */
    public function getAleaActuel()
    {
        return $this->alea_actuel;
    }

    /**
     * Get the [alea_futur] column value.
     *
     * @return string
     */
    public function getAleaFutur()
    {
        return $this->alea_futur;
    }

    /**
     * Get the [token] column value.
     *
     * @return string
     */
    public function getToken()
    {
        return $this->token;
    }

    /**
     * Get the [token_time] column value.
     *
     * @return string
     */
    public function getTokenTime()
    {
        return $this->token_time;
    }

    /**
     * Get the [civilite] column value.
     *
     * @return string
     */
    public function getCivilite()
    {
        return $this->civilite;
    }

    /**
     * Get the [sexe] column value.
     *
     * @return string
     */
    public function getSexe()
    {
        return $this->sexe;
    }

    /**
     * Get the [nom_famille] column value.
     *
     * @return string
     */
    public function getNomFamille()
    {
        return $this->nom_famille;
    }

    /**
     * Get the [prenom] column value.
     *
     * @return string
     */
    public function getPrenom()
    {
        return $this->prenom;
    }

    /**
     * Get the [optionally formatted] temporal [naissance] column value.
     *
     *
     * @param      string $format The date/time format string (either date()-style or strftime()-style).
     *                            If format is NULL, then the raw DateTime object will be returned.
     *
     * @return string|DateTime Formatted date/time value as string or DateTime object (if format is NULL), NULL if column is NULL, and 0 if column value is 0000-00-00
     *
     * @throws PropelException - if unable to parse/validate the date/time value.
     */
    public function getNaissance($format = NULL)
    {
        if ($format === null) {
            return $this->naissance;
        } else {
            return $this->naissance instanceof \DateTimeInterface ? $this->naissance->format($format) : null;
        }
    }

    /**
     * Get the [adresse] column value.
     *
     * @return string
     */
    public function getAdresse()
    {
        return $this->adresse;
    }

    /**
     * Get the [codepostal] column value.
     *
     * @return string
     */
    public function getCodepostal()
    {
        return $this->codepostal;
    }

    /**
     * Get the [ville] column value.
     *
     * @return string
     */
    public function getVille()
    {
        return $this->ville;
    }

    /**
     * Get the [pays] column value.
     *
     * @return string
     */
    public function getPays()
    {
        return $this->pays;
    }

    /**
     * Get the [telephone] column value.
     *
     * @return string
     */
    public function getTelephone()
    {
        return $this->telephone;
    }

    /**
     * Get the [telephone_pro] column value.
     *
     * @return string
     */
    public function getTelephonePro()
    {
        return $this->telephone_pro;
    }

    /**
     * Get the [fax] column value.
     *
     * @return string
     */
    public function getFax()
    {
        return $this->fax;
    }

    /**
     * Get the [mobile] column value.
     *
     * @return string
     */
    public function getMobile()
    {
        return $this->mobile;
    }

    /**
     * Get the [url] column value.
     *
     * @return string
     */
    public function getUrl()
    {
        return $this->url;
    }

    /**
     * Get the [profession] column value.
     *
     * @return string
     */
    public function getProfession()
    {
        return $this->profession;
    }

    /**
     * Get the [contact_souhait] column value.
     *
     * @return boolean
     */
    public function getContactSouhait()
    {
        return $this->contact_souhait;
    }

    /**
     * Get the [contact_souhait] column value.
     *
     * @return boolean
     */
    public function isContactSouhait()
    {
        return $this->getContactSouhait();
    }

    /**
     * Get the [observation] column value.
     *
     * @return string
     */
    public function getObservation()
    {
        return $this->observation;
    }

    /**
     * Get the [optionally formatted] temporal [created_at] column value.
     *
     *
     * @param      string $format The date/time format string (either date()-style or strftime()-style).
     *                            If format is NULL, then the raw DateTime object will be returned.
     *
     * @return string|DateTime Formatted date/time value as string or DateTime object (if format is NULL), NULL if column is NULL, and 0 if column value is 0000-00-00 00:00:00
     *
     * @throws PropelException - if unable to parse/validate the date/time value.
     */
    public function getCreatedAt($format = NULL)
    {
        if ($format === null) {
            return $this->created_at;
        } else {
            return $this->created_at instanceof \DateTimeInterface ? $this->created_at->format($format) : null;
        }
    }

    /**
     * Get the [optionally formatted] temporal [updated_at] column value.
     *
     *
     * @param      string $format The date/time format string (either date()-style or strftime()-style).
     *                            If format is NULL, then the raw DateTime object will be returned.
     *
     * @return string|DateTime Formatted date/time value as string or DateTime object (if format is NULL), NULL if column is NULL, and 0 if column value is 0000-00-00 00:00:00
     *
     * @throws PropelException - if unable to parse/validate the date/time value.
     */
    public function getUpdatedAt($format = NULL)
    {
        if ($format === null) {
            return $this->updated_at;
        } else {
            return $this->updated_at instanceof \DateTimeInterface ? $this->updated_at->format($format) : null;
        }
    }

    /**
     * Set the value of [id_individu] column.
     *
     * @param string $v new value
     * @return $this|\Individu The current object (for fluent API support)
     */
    public function setIdIndividu($v)
    {
        if ($v !== null) {
            $v = (string) $v;
        }

        if ($this->id_individu !== $v) {
            $this->id_individu = $v;
            $this->modifiedColumns[IndividuTableMap::COL_ID_INDIVIDU] = true;
        }

        return $this;
    } // setIdIndividu()

    /**
     * Set the value of [nom] column.
     *
     * @param string $v new value
     * @return $this|\Individu The current object (for fluent API support)
     */
    public function setNom($v)
    {
        if ($v !== null) {
            $v = (string) $v;
        }

        if ($this->nom !== $v) {
            $this->nom = $v;
            $this->modifiedColumns[IndividuTableMap::COL_NOM] = true;
        }

        return $this;
    } // setNom()

    /**
     * Set the value of [bio] column.
     *
     * @param string $v new value
     * @return $this|\Individu The current object (for fluent API support)
     */
    public function setBio($v)
    {
        if ($v !== null) {
            $v = (string) $v;
        }

        if ($this->bio !== $v) {
            $this->bio = $v;
            $this->modifiedColumns[IndividuTableMap::COL_BIO] = true;
        }

        return $this;
    } // setBio()

    /**
     * Set the value of [email] column.
     *
     * @param string $v new value
     * @return $this|\Individu The current object (for fluent API support)
     */
    public function setEmail($v)
    {
        if ($v !== null) {
            $v = (string) $v;
        }

        if ($this->email !== $v) {
            $this->email = $v;
            $this->modifiedColumns[IndividuTableMap::COL_EMAIL] = true;
        }

        return $this;
    } // setEmail()

    /**
     * Set the value of [login] column.
     *
     * @param string $v new value
     * @return $this|\Individu The current object (for fluent API support)
     */
    public function setLogin($v)
    {
        if ($v !== null) {
            $v = (string) $v;
        }

        if ($this->login !== $v) {
            $this->login = $v;
            $this->modifiedColumns[IndividuTableMap::COL_LOGIN] = true;
        }

        return $this;
    } // setLogin()

    /**
     * Set the value of [pass] column.
     *
     * @param string $v new value
     * @return $this|\Individu The current object (for fluent API support)
     */
    public function setPass($v)
    {
        if ($v !== null) {
            $v = (string) $v;
        }

        if ($this->pass !== $v) {
            $this->pass = $v;
            $this->modifiedColumns[IndividuTableMap::COL_PASS] = true;
        }

        return $this;
    } // setPass()

    /**
     * Set the value of [alea_actuel] column.
     *
     * @param string $v new value
     * @return $this|\Individu The current object (for fluent API support)
     */
    public function setAleaActuel($v)
    {
        if ($v !== null) {
            $v = (string) $v;
        }

        if ($this->alea_actuel !== $v) {
            $this->alea_actuel = $v;
            $this->modifiedColumns[IndividuTableMap::COL_ALEA_ACTUEL] = true;
        }

        return $this;
    } // setAleaActuel()

    /**
     * Set the value of [alea_futur] column.
     *
     * @param string $v new value
     * @return $this|\Individu The current object (for fluent API support)
     */
    public function setAleaFutur($v)
    {
        if ($v !== null) {
            $v = (string) $v;
        }

        if ($this->alea_futur !== $v) {
            $this->alea_futur = $v;
            $this->modifiedColumns[IndividuTableMap::COL_ALEA_FUTUR] = true;
        }

        return $this;
    } // setAleaFutur()

    /**
     * Set the value of [token] column.
     *
     * @param string $v new value
     * @return $this|\Individu The current object (for fluent API support)
     */
    public function setToken($v)
    {
        if ($v !== null) {
            $v = (string) $v;
        }

        if ($this->token !== $v) {
            $this->token = $v;
            $this->modifiedColumns[IndividuTableMap::COL_TOKEN] = true;
        }

        return $this;
    } // setToken()

    /**
     * Set the value of [token_time] column.
     *
     * @param string $v new value
     * @return $this|\Individu The current object (for fluent API support)
     */
    public function setTokenTime($v)
    {
        if ($v !== null) {
            $v = (string) $v;
        }

        if ($this->token_time !== $v) {
            $this->token_time = $v;
            $this->modifiedColumns[IndividuTableMap::COL_TOKEN_TIME] = true;
        }

        return $this;
    } // setTokenTime()

    /**
     * Set the value of [civilite] column.
     *
     * @param string $v new value
     * @return $this|\Individu The current object (for fluent API support)
     */
    public function setCivilite($v)
    {
        if ($v !== null) {
            $v = (string) $v;
        }

        if ($this->civilite !== $v) {
            $this->civilite = $v;
            $this->modifiedColumns[IndividuTableMap::COL_CIVILITE] = true;
        }

        return $this;
    } // setCivilite()

    /**
     * Set the value of [sexe] column.
     *
     * @param string $v new value
     * @return $this|\Individu The current object (for fluent API support)
     */
    public function setSexe($v)
    {
        if ($v !== null) {
            $v = (string) $v;
        }

        if ($this->sexe !== $v) {
            $this->sexe = $v;
            $this->modifiedColumns[IndividuTableMap::COL_SEXE] = true;
        }

        return $this;
    } // setSexe()

    /**
     * Set the value of [nom_famille] column.
     *
     * @param string $v new value
     * @return $this|\Individu The current object (for fluent API support)
     */
    public function setNomFamille($v)
    {
        if ($v !== null) {
            $v = (string) $v;
        }

        if ($this->nom_famille !== $v) {
            $this->nom_famille = $v;
            $this->modifiedColumns[IndividuTableMap::COL_NOM_FAMILLE] = true;
        }

        return $this;
    } // setNomFamille()

    /**
     * Set the value of [prenom] column.
     *
     * @param string $v new value
     * @return $this|\Individu The current object (for fluent API support)
     */
    public function setPrenom($v)
    {
        if ($v !== null) {
            $v = (string) $v;
        }

        if ($this->prenom !== $v) {
            $this->prenom = $v;
            $this->modifiedColumns[IndividuTableMap::COL_PRENOM] = true;
        }

        return $this;
    } // setPrenom()

    /**
     * Sets the value of [naissance] column to a normalized version of the date/time value specified.
     *
     * @param  mixed $v string, integer (timestamp), or \DateTimeInterface value.
     *               Empty strings are treated as NULL.
     * @return $this|\Individu The current object (for fluent API support)
     */
    public function setNaissance($v)
    {
        $dt = PropelDateTime::newInstance($v, null, 'DateTime');
        if ($this->naissance !== null || $dt !== null) {
            if ($this->naissance === null || $dt === null || $dt->format("Y-m-d") !== $this->naissance->format("Y-m-d")) {
                $this->naissance = $dt === null ? null : clone $dt;
                $this->modifiedColumns[IndividuTableMap::COL_NAISSANCE] = true;
            }
        } // if either are not null

        return $this;
    } // setNaissance()

    /**
     * Set the value of [adresse] column.
     *
     * @param string $v new value
     * @return $this|\Individu The current object (for fluent API support)
     */
    public function setAdresse($v)
    {
        if ($v !== null) {
            $v = (string) $v;
        }

        if ($this->adresse !== $v) {
            $this->adresse = $v;
            $this->modifiedColumns[IndividuTableMap::COL_ADRESSE] = true;
        }

        return $this;
    } // setAdresse()

    /**
     * Set the value of [codepostal] column.
     *
     * @param string $v new value
     * @return $this|\Individu The current object (for fluent API support)
     */
    public function setCodepostal($v)
    {
        if ($v !== null) {
            $v = (string) $v;
        }

        if ($this->codepostal !== $v) {
            $this->codepostal = $v;
            $this->modifiedColumns[IndividuTableMap::COL_CODEPOSTAL] = true;
        }

        return $this;
    } // setCodepostal()

    /**
     * Set the value of [ville] column.
     *
     * @param string $v new value
     * @return $this|\Individu The current object (for fluent API support)
     */
    public function setVille($v)
    {
        if ($v !== null) {
            $v = (string) $v;
        }

        if ($this->ville !== $v) {
            $this->ville = $v;
            $this->modifiedColumns[IndividuTableMap::COL_VILLE] = true;
        }

        return $this;
    } // setVille()

    /**
     * Set the value of [pays] column.
     *
     * @param string $v new value
     * @return $this|\Individu The current object (for fluent API support)
     */
    public function setPays($v)
    {
        if ($v !== null) {
            $v = (string) $v;
        }

        if ($this->pays !== $v) {
            $this->pays = $v;
            $this->modifiedColumns[IndividuTableMap::COL_PAYS] = true;
        }

        return $this;
    } // setPays()

    /**
     * Set the value of [telephone] column.
     *
     * @param string $v new value
     * @return $this|\Individu The current object (for fluent API support)
     */
    public function setTelephone($v)
    {
        if ($v !== null) {
            $v = (string) $v;
        }

        if ($this->telephone !== $v) {
            $this->telephone = $v;
            $this->modifiedColumns[IndividuTableMap::COL_TELEPHONE] = true;
        }

        return $this;
    } // setTelephone()

    /**
     * Set the value of [telephone_pro] column.
     *
     * @param string $v new value
     * @return $this|\Individu The current object (for fluent API support)
     */
    public function setTelephonePro($v)
    {
        if ($v !== null) {
            $v = (string) $v;
        }

        if ($this->telephone_pro !== $v) {
            $this->telephone_pro = $v;
            $this->modifiedColumns[IndividuTableMap::COL_TELEPHONE_PRO] = true;
        }

        return $this;
    } // setTelephonePro()

    /**
     * Set the value of [fax] column.
     *
     * @param string $v new value
     * @return $this|\Individu The current object (for fluent API support)
     */
    public function setFax($v)
    {
        if ($v !== null) {
            $v = (string) $v;
        }

        if ($this->fax !== $v) {
            $this->fax = $v;
            $this->modifiedColumns[IndividuTableMap::COL_FAX] = true;
        }

        return $this;
    } // setFax()

    /**
     * Set the value of [mobile] column.
     *
     * @param string $v new value
     * @return $this|\Individu The current object (for fluent API support)
     */
    public function setMobile($v)
    {
        if ($v !== null) {
            $v = (string) $v;
        }

        if ($this->mobile !== $v) {
            $this->mobile = $v;
            $this->modifiedColumns[IndividuTableMap::COL_MOBILE] = true;
        }

        return $this;
    } // setMobile()

    /**
     * Set the value of [url] column.
     *
     * @param string $v new value
     * @return $this|\Individu The current object (for fluent API support)
     */
    public function setUrl($v)
    {
        if ($v !== null) {
            $v = (string) $v;
        }

        if ($this->url !== $v) {
            $this->url = $v;
            $this->modifiedColumns[IndividuTableMap::COL_URL] = true;
        }

        return $this;
    } // setUrl()

    /**
     * Set the value of [profession] column.
     *
     * @param string $v new value
     * @return $this|\Individu The current object (for fluent API support)
     */
    public function setProfession($v)
    {
        if ($v !== null) {
            $v = (string) $v;
        }

        if ($this->profession !== $v) {
            $this->profession = $v;
            $this->modifiedColumns[IndividuTableMap::COL_PROFESSION] = true;
        }

        return $this;
    } // setProfession()

    /**
     * Sets the value of the [contact_souhait] column.
     * Non-boolean arguments are converted using the following rules:
     *   * 1, '1', 'true',  'on',  and 'yes' are converted to boolean true
     *   * 0, '0', 'false', 'off', and 'no'  are converted to boolean false
     * Check on string values is case insensitive (so 'FaLsE' is seen as 'false').
     *
     * @param  boolean|integer|string $v The new value
     * @return $this|\Individu The current object (for fluent API support)
     */
    public function setContactSouhait($v)
    {
        if ($v !== null) {
            if (is_string($v)) {
                $v = in_array(strtolower($v), array('false', 'off', '-', 'no', 'n', '0', '')) ? false : true;
            } else {
                $v = (boolean) $v;
            }
        }

        if ($this->contact_souhait !== $v) {
            $this->contact_souhait = $v;
            $this->modifiedColumns[IndividuTableMap::COL_CONTACT_SOUHAIT] = true;
        }

        return $this;
    } // setContactSouhait()

    /**
     * Set the value of [observation] column.
     *
     * @param string $v new value
     * @return $this|\Individu The current object (for fluent API support)
     */
    public function setObservation($v)
    {
        if ($v !== null) {
            $v = (string) $v;
        }

        if ($this->observation !== $v) {
            $this->observation = $v;
            $this->modifiedColumns[IndividuTableMap::COL_OBSERVATION] = true;
        }

        return $this;
    } // setObservation()

    /**
     * Sets the value of [created_at] column to a normalized version of the date/time value specified.
     *
     * @param  mixed $v string, integer (timestamp), or \DateTimeInterface value.
     *               Empty strings are treated as NULL.
     * @return $this|\Individu The current object (for fluent API support)
     */
    public function setCreatedAt($v)
    {
        $dt = PropelDateTime::newInstance($v, null, 'DateTime');
        if ($this->created_at !== null || $dt !== null) {
            if ($this->created_at === null || $dt === null || $dt->format("Y-m-d H:i:s.u") !== $this->created_at->format("Y-m-d H:i:s.u")) {
                $this->created_at = $dt === null ? null : clone $dt;
                $this->modifiedColumns[IndividuTableMap::COL_CREATED_AT] = true;
            }
        } // if either are not null

        return $this;
    } // setCreatedAt()

    /**
     * Sets the value of [updated_at] column to a normalized version of the date/time value specified.
     *
     * @param  mixed $v string, integer (timestamp), or \DateTimeInterface value.
     *               Empty strings are treated as NULL.
     * @return $this|\Individu The current object (for fluent API support)
     */
    public function setUpdatedAt($v)
    {
        $dt = PropelDateTime::newInstance($v, null, 'DateTime');
        if ($this->updated_at !== null || $dt !== null) {
            if ($this->updated_at === null || $dt === null || $dt->format("Y-m-d H:i:s.u") !== $this->updated_at->format("Y-m-d H:i:s.u")) {
                $this->updated_at = $dt === null ? null : clone $dt;
                $this->modifiedColumns[IndividuTableMap::COL_UPDATED_AT] = true;
            }
        } // if either are not null

        return $this;
    } // setUpdatedAt()

    /**
     * Indicates whether the columns in this object are only set to default values.
     *
     * This method can be used in conjunction with isModified() to indicate whether an object is both
     * modified _and_ has some values set which are non-default.
     *
     * @return boolean Whether the columns in this object are only been set with default values.
     */
    public function hasOnlyDefaultValues()
    {
            if ($this->contact_souhait !== true) {
                return false;
            }

        // otherwise, everything was equal, so return TRUE
        return true;
    } // hasOnlyDefaultValues()

    /**
     * Hydrates (populates) the object variables with values from the database resultset.
     *
     * An offset (0-based "start column") is specified so that objects can be hydrated
     * with a subset of the columns in the resultset rows.  This is needed, for example,
     * for results of JOIN queries where the resultset row includes columns from two or
     * more tables.
     *
     * @param array   $row       The row returned by DataFetcher->fetch().
     * @param int     $startcol  0-based offset column which indicates which restultset column to start with.
     * @param boolean $rehydrate Whether this object is being re-hydrated from the database.
     * @param string  $indexType The index type of $row. Mostly DataFetcher->getIndexType().
                                  One of the class type constants TableMap::TYPE_PHPNAME, TableMap::TYPE_CAMELNAME
     *                            TableMap::TYPE_COLNAME, TableMap::TYPE_FIELDNAME, TableMap::TYPE_NUM.
     *
     * @return int             next starting column
     * @throws PropelException - Any caught Exception will be rewrapped as a PropelException.
     */
    public function hydrate($row, $startcol = 0, $rehydrate = false, $indexType = TableMap::TYPE_NUM)
    {
        try {

            $col = $row[TableMap::TYPE_NUM == $indexType ? 0 + $startcol : IndividuTableMap::translateFieldName('IdIndividu', TableMap::TYPE_PHPNAME, $indexType)];
            $this->id_individu = (null !== $col) ? (string) $col : null;

            $col = $row[TableMap::TYPE_NUM == $indexType ? 1 + $startcol : IndividuTableMap::translateFieldName('Nom', TableMap::TYPE_PHPNAME, $indexType)];
            $this->nom = (null !== $col) ? (string) $col : null;

            $col = $row[TableMap::TYPE_NUM == $indexType ? 2 + $startcol : IndividuTableMap::translateFieldName('Bio', TableMap::TYPE_PHPNAME, $indexType)];
            $this->bio = (null !== $col) ? (string) $col : null;

            $col = $row[TableMap::TYPE_NUM == $indexType ? 3 + $startcol : IndividuTableMap::translateFieldName('Email', TableMap::TYPE_PHPNAME, $indexType)];
            $this->email = (null !== $col) ? (string) $col : null;

            $col = $row[TableMap::TYPE_NUM == $indexType ? 4 + $startcol : IndividuTableMap::translateFieldName('Login', TableMap::TYPE_PHPNAME, $indexType)];
            $this->login = (null !== $col) ? (string) $col : null;

            $col = $row[TableMap::TYPE_NUM == $indexType ? 5 + $startcol : IndividuTableMap::translateFieldName('Pass', TableMap::TYPE_PHPNAME, $indexType)];
            $this->pass = (null !== $col) ? (string) $col : null;

            $col = $row[TableMap::TYPE_NUM == $indexType ? 6 + $startcol : IndividuTableMap::translateFieldName('AleaActuel', TableMap::TYPE_PHPNAME, $indexType)];
            $this->alea_actuel = (null !== $col) ? (string) $col : null;

            $col = $row[TableMap::TYPE_NUM == $indexType ? 7 + $startcol : IndividuTableMap::translateFieldName('AleaFutur', TableMap::TYPE_PHPNAME, $indexType)];
            $this->alea_futur = (null !== $col) ? (string) $col : null;

            $col = $row[TableMap::TYPE_NUM == $indexType ? 8 + $startcol : IndividuTableMap::translateFieldName('Token', TableMap::TYPE_PHPNAME, $indexType)];
            $this->token = (null !== $col) ? (string) $col : null;

            $col = $row[TableMap::TYPE_NUM == $indexType ? 9 + $startcol : IndividuTableMap::translateFieldName('TokenTime', TableMap::TYPE_PHPNAME, $indexType)];
            $this->token_time = (null !== $col) ? (string) $col : null;

            $col = $row[TableMap::TYPE_NUM == $indexType ? 10 + $startcol : IndividuTableMap::translateFieldName('Civilite', TableMap::TYPE_PHPNAME, $indexType)];
            $this->civilite = (null !== $col) ? (string) $col : null;

            $col = $row[TableMap::TYPE_NUM == $indexType ? 11 + $startcol : IndividuTableMap::translateFieldName('Sexe', TableMap::TYPE_PHPNAME, $indexType)];
            $this->sexe = (null !== $col) ? (string) $col : null;

            $col = $row[TableMap::TYPE_NUM == $indexType ? 12 + $startcol : IndividuTableMap::translateFieldName('NomFamille', TableMap::TYPE_PHPNAME, $indexType)];
            $this->nom_famille = (null !== $col) ? (string) $col : null;

            $col = $row[TableMap::TYPE_NUM == $indexType ? 13 + $startcol : IndividuTableMap::translateFieldName('Prenom', TableMap::TYPE_PHPNAME, $indexType)];
            $this->prenom = (null !== $col) ? (string) $col : null;

            $col = $row[TableMap::TYPE_NUM == $indexType ? 14 + $startcol : IndividuTableMap::translateFieldName('Naissance', TableMap::TYPE_PHPNAME, $indexType)];
            if ($col === '0000-00-00') {
                $col = null;
            }
            $this->naissance = (null !== $col) ? PropelDateTime::newInstance($col, null, 'DateTime') : null;

            $col = $row[TableMap::TYPE_NUM == $indexType ? 15 + $startcol : IndividuTableMap::translateFieldName('Adresse', TableMap::TYPE_PHPNAME, $indexType)];
            $this->adresse = (null !== $col) ? (string) $col : null;

            $col = $row[TableMap::TYPE_NUM == $indexType ? 16 + $startcol : IndividuTableMap::translateFieldName('Codepostal', TableMap::TYPE_PHPNAME, $indexType)];
            $this->codepostal = (null !== $col) ? (string) $col : null;

            $col = $row[TableMap::TYPE_NUM == $indexType ? 17 + $startcol : IndividuTableMap::translateFieldName('Ville', TableMap::TYPE_PHPNAME, $indexType)];
            $this->ville = (null !== $col) ? (string) $col : null;

            $col = $row[TableMap::TYPE_NUM == $indexType ? 18 + $startcol : IndividuTableMap::translateFieldName('Pays', TableMap::TYPE_PHPNAME, $indexType)];
            $this->pays = (null !== $col) ? (string) $col : null;

            $col = $row[TableMap::TYPE_NUM == $indexType ? 19 + $startcol : IndividuTableMap::translateFieldName('Telephone', TableMap::TYPE_PHPNAME, $indexType)];
            $this->telephone = (null !== $col) ? (string) $col : null;

            $col = $row[TableMap::TYPE_NUM == $indexType ? 20 + $startcol : IndividuTableMap::translateFieldName('TelephonePro', TableMap::TYPE_PHPNAME, $indexType)];
            $this->telephone_pro = (null !== $col) ? (string) $col : null;

            $col = $row[TableMap::TYPE_NUM == $indexType ? 21 + $startcol : IndividuTableMap::translateFieldName('Fax', TableMap::TYPE_PHPNAME, $indexType)];
            $this->fax = (null !== $col) ? (string) $col : null;

            $col = $row[TableMap::TYPE_NUM == $indexType ? 22 + $startcol : IndividuTableMap::translateFieldName('Mobile', TableMap::TYPE_PHPNAME, $indexType)];
            $this->mobile = (null !== $col) ? (string) $col : null;

            $col = $row[TableMap::TYPE_NUM == $indexType ? 23 + $startcol : IndividuTableMap::translateFieldName('Url', TableMap::TYPE_PHPNAME, $indexType)];
            $this->url = (null !== $col) ? (string) $col : null;

            $col = $row[TableMap::TYPE_NUM == $indexType ? 24 + $startcol : IndividuTableMap::translateFieldName('Profession', TableMap::TYPE_PHPNAME, $indexType)];
            $this->profession = (null !== $col) ? (string) $col : null;

            $col = $row[TableMap::TYPE_NUM == $indexType ? 25 + $startcol : IndividuTableMap::translateFieldName('ContactSouhait', TableMap::TYPE_PHPNAME, $indexType)];
            $this->contact_souhait = (null !== $col) ? (boolean) $col : null;

            $col = $row[TableMap::TYPE_NUM == $indexType ? 26 + $startcol : IndividuTableMap::translateFieldName('Observation', TableMap::TYPE_PHPNAME, $indexType)];
            $this->observation = (null !== $col) ? (string) $col : null;

            $col = $row[TableMap::TYPE_NUM == $indexType ? 27 + $startcol : IndividuTableMap::translateFieldName('CreatedAt', TableMap::TYPE_PHPNAME, $indexType)];
            if ($col === '0000-00-00 00:00:00') {
                $col = null;
            }
            $this->created_at = (null !== $col) ? PropelDateTime::newInstance($col, null, 'DateTime') : null;

            $col = $row[TableMap::TYPE_NUM == $indexType ? 28 + $startcol : IndividuTableMap::translateFieldName('UpdatedAt', TableMap::TYPE_PHPNAME, $indexType)];
            if ($col === '0000-00-00 00:00:00') {
                $col = null;
            }
            $this->updated_at = (null !== $col) ? PropelDateTime::newInstance($col, null, 'DateTime') : null;
            $this->resetModified();

            $this->setNew(false);

            if ($rehydrate) {
                $this->ensureConsistency();
            }

            return $startcol + 29; // 29 = IndividuTableMap::NUM_HYDRATE_COLUMNS.

        } catch (Exception $e) {
            throw new PropelException(sprintf('Error populating %s object', '\\Individu'), 0, $e);
        }
    }

    /**
     * Checks and repairs the internal consistency of the object.
     *
     * This method is executed after an already-instantiated object is re-hydrated
     * from the database.  It exists to check any foreign keys to make sure that
     * the objects related to the current object are correct based on foreign key.
     *
     * You can override this method in the stub class, but you should always invoke
     * the base method from the overridden method (i.e. parent::ensureConsistency()),
     * in case your model changes.
     *
     * @throws PropelException
     */
    public function ensureConsistency()
    {
    } // ensureConsistency

    /**
     * Reloads this object from datastore based on primary key and (optionally) resets all associated objects.
     *
     * This will only work if the object has been saved and has a valid primary key set.
     *
     * @param      boolean $deep (optional) Whether to also de-associated any related objects.
     * @param      ConnectionInterface $con (optional) The ConnectionInterface connection to use.
     * @return void
     * @throws PropelException - if this object is deleted, unsaved or doesn't have pk match in db
     */
    public function reload($deep = false, ConnectionInterface $con = null)
    {
        if ($this->isDeleted()) {
            throw new PropelException("Cannot reload a deleted object.");
        }

        if ($this->isNew()) {
            throw new PropelException("Cannot reload an unsaved object.");
        }

        if ($con === null) {
            $con = Propel::getServiceContainer()->getReadConnection(IndividuTableMap::DATABASE_NAME);
        }

        // We don't need to alter the object instance pool; we're just modifying this instance
        // already in the pool.

        $dataFetcher = ChildIndividuQuery::create(null, $this->buildPkeyCriteria())->setFormatter(ModelCriteria::FORMAT_STATEMENT)->find($con);
        $row = $dataFetcher->fetch();
        $dataFetcher->close();
        if (!$row) {
            throw new PropelException('Cannot find matching row in the database to reload object values.');
        }
        $this->hydrate($row, 0, true, $dataFetcher->getIndexType()); // rehydrate

        if ($deep) {  // also de-associate any related objects?

            $this->collAutorisations = null;

            $this->collLogs = null;

            $this->collMembres0s = null;

            $this->collMembreIndividus = null;

            $this->collServicerenduIndividus = null;

            $this->collNotificationIndividus = null;

            $this->collCourrierLiens = null;

            $this->collMembres = null;
            $this->collServicerendus = null;
            $this->collNotifications = null;
        } // if (deep)
    }

    /**
     * Removes this object from datastore and sets delete attribute.
     *
     * @param      ConnectionInterface $con
     * @return void
     * @throws PropelException
     * @see Individu::setDeleted()
     * @see Individu::isDeleted()
     */
    public function delete(ConnectionInterface $con = null)
    {
        if ($this->isDeleted()) {
            throw new PropelException("This object has already been deleted.");
        }

        if ($con === null) {
            $con = Propel::getServiceContainer()->getWriteConnection(IndividuTableMap::DATABASE_NAME);
        }

        $con->transaction(function () use ($con) {
            $deleteQuery = ChildIndividuQuery::create()
                ->filterByPrimaryKey($this->getPrimaryKey());
            $ret = $this->preDelete($con);
            // archivable behavior
            if ($ret) {
                if ($this->archiveOnDelete) {
                    // do nothing yet. The object will be archived later when calling ChildIndividuQuery::delete().
                } else {
                    $deleteQuery->setArchiveOnDelete(false);
                    $this->archiveOnDelete = true;
                }
            }

            if ($ret) {
                $deleteQuery->delete($con);
                $this->postDelete($con);
                $this->setDeleted(true);
            }
        });
    }

    /**
     * Persists this object to the database.
     *
     * If the object is new, it inserts it; otherwise an update is performed.
     * All modified related objects will also be persisted in the doSave()
     * method.  This method wraps all precipitate database operations in a
     * single transaction.
     *
     * @param      ConnectionInterface $con
     * @return int             The number of rows affected by this insert/update and any referring fk objects' save() operations.
     * @throws PropelException
     * @see doSave()
     */
    public function save(ConnectionInterface $con = null)
    {
        if ($this->isDeleted()) {
            throw new PropelException("You cannot save an object that has been deleted.");
        }

        if ($this->alreadyInSave) {
            return 0;
        }

        if ($con === null) {
            $con = Propel::getServiceContainer()->getWriteConnection(IndividuTableMap::DATABASE_NAME);
        }

        return $con->transaction(function () use ($con) {
            $ret = $this->preSave($con);
            $isInsert = $this->isNew();
            if ($isInsert) {
                $ret = $ret && $this->preInsert($con);
                // timestampable behavior

                if (!$this->isColumnModified(IndividuTableMap::COL_CREATED_AT)) {
                    $this->setCreatedAt(\Propel\Runtime\Util\PropelDateTime::createHighPrecision());
                }
                if (!$this->isColumnModified(IndividuTableMap::COL_UPDATED_AT)) {
                    $this->setUpdatedAt(\Propel\Runtime\Util\PropelDateTime::createHighPrecision());
                }
            } else {
                $ret = $ret && $this->preUpdate($con);
                // timestampable behavior
                if ($this->isModified() && !$this->isColumnModified(IndividuTableMap::COL_UPDATED_AT)) {
                    $this->setUpdatedAt(\Propel\Runtime\Util\PropelDateTime::createHighPrecision());
                }
            }
            if ($ret) {
                $affectedRows = $this->doSave($con);
                if ($isInsert) {
                    $this->postInsert($con);
                } else {
                    $this->postUpdate($con);
                }
                $this->postSave($con);
                IndividuTableMap::addInstanceToPool($this);
            } else {
                $affectedRows = 0;
            }

            return $affectedRows;
        });
    }

    /**
     * Performs the work of inserting or updating the row in the database.
     *
     * If the object is new, it inserts it; otherwise an update is performed.
     * All related objects are also updated in this method.
     *
     * @param      ConnectionInterface $con
     * @return int             The number of rows affected by this insert/update and any referring fk objects' save() operations.
     * @throws PropelException
     * @see save()
     */
    protected function doSave(ConnectionInterface $con)
    {
        $affectedRows = 0; // initialize var to track total num of affected rows
        if (!$this->alreadyInSave) {
            $this->alreadyInSave = true;

            if ($this->isNew() || $this->isModified()) {
                // persist changes
                if ($this->isNew()) {
                    $this->doInsert($con);
                    $affectedRows += 1;
                } else {
                    $affectedRows += $this->doUpdate($con);
                }
                $this->resetModified();
            }

            if ($this->membresScheduledForDeletion !== null) {
                if (!$this->membresScheduledForDeletion->isEmpty()) {
                    $pks = array();
                    foreach ($this->membresScheduledForDeletion as $entry) {
                        $entryPk = [];

                        $entryPk[0] = $this->getIdIndividu();
                        $entryPk[1] = $entry->getIdMembre();
                        $pks[] = $entryPk;
                    }

                    \MembreIndividuQuery::create()
                        ->filterByPrimaryKeys($pks)
                        ->delete($con);

                    $this->membresScheduledForDeletion = null;
                }

            }

            if ($this->collMembres) {
                foreach ($this->collMembres as $membre) {
                    if (!$membre->isDeleted() && ($membre->isNew() || $membre->isModified())) {
                        $membre->save($con);
                    }
                }
            }


            if ($this->servicerendusScheduledForDeletion !== null) {
                if (!$this->servicerendusScheduledForDeletion->isEmpty()) {
                    $pks = array();
                    foreach ($this->servicerendusScheduledForDeletion as $entry) {
                        $entryPk = [];

                        $entryPk[0] = $this->getIdIndividu();
                        $entryPk[1] = $entry->getIdServicerendu();
                        $pks[] = $entryPk;
                    }

                    \ServicerenduIndividuQuery::create()
                        ->filterByPrimaryKeys($pks)
                        ->delete($con);

                    $this->servicerendusScheduledForDeletion = null;
                }

            }

            if ($this->collServicerendus) {
                foreach ($this->collServicerendus as $servicerendu) {
                    if (!$servicerendu->isDeleted() && ($servicerendu->isNew() || $servicerendu->isModified())) {
                        $servicerendu->save($con);
                    }
                }
            }


            if ($this->notificationsScheduledForDeletion !== null) {
                if (!$this->notificationsScheduledForDeletion->isEmpty()) {
                    $pks = array();
                    foreach ($this->notificationsScheduledForDeletion as $entry) {
                        $entryPk = [];

                        $entryPk[1] = $this->getIdIndividu();
                        $entryPk[0] = $entry->getIdNotification();
                        $pks[] = $entryPk;
                    }

                    \NotificationIndividuQuery::create()
                        ->filterByPrimaryKeys($pks)
                        ->delete($con);

                    $this->notificationsScheduledForDeletion = null;
                }

            }

            if ($this->collNotifications) {
                foreach ($this->collNotifications as $notification) {
                    if (!$notification->isDeleted() && ($notification->isNew() || $notification->isModified())) {
                        $notification->save($con);
                    }
                }
            }


            if ($this->autorisationsScheduledForDeletion !== null) {
                if (!$this->autorisationsScheduledForDeletion->isEmpty()) {
                    \AutorisationQuery::create()
                        ->filterByPrimaryKeys($this->autorisationsScheduledForDeletion->getPrimaryKeys(false))
                        ->delete($con);
                    $this->autorisationsScheduledForDeletion = null;
                }
            }

            if ($this->collAutorisations !== null) {
                foreach ($this->collAutorisations as $referrerFK) {
                    if (!$referrerFK->isDeleted() && ($referrerFK->isNew() || $referrerFK->isModified())) {
                        $affectedRows += $referrerFK->save($con);
                    }
                }
            }

            if ($this->logsScheduledForDeletion !== null) {
                if (!$this->logsScheduledForDeletion->isEmpty()) {
                    \LogQuery::create()
                        ->filterByPrimaryKeys($this->logsScheduledForDeletion->getPrimaryKeys(false))
                        ->delete($con);
                    $this->logsScheduledForDeletion = null;
                }
            }

            if ($this->collLogs !== null) {
                foreach ($this->collLogs as $referrerFK) {
                    if (!$referrerFK->isDeleted() && ($referrerFK->isNew() || $referrerFK->isModified())) {
                        $affectedRows += $referrerFK->save($con);
                    }
                }
            }

            if ($this->membres0sScheduledForDeletion !== null) {
                if (!$this->membres0sScheduledForDeletion->isEmpty()) {
                    \MembreQuery::create()
                        ->filterByPrimaryKeys($this->membres0sScheduledForDeletion->getPrimaryKeys(false))
                        ->delete($con);
                    $this->membres0sScheduledForDeletion = null;
                }
            }

            if ($this->collMembres0s !== null) {
                foreach ($this->collMembres0s as $referrerFK) {
                    if (!$referrerFK->isDeleted() && ($referrerFK->isNew() || $referrerFK->isModified())) {
                        $affectedRows += $referrerFK->save($con);
                    }
                }
            }

            if ($this->membreIndividusScheduledForDeletion !== null) {
                if (!$this->membreIndividusScheduledForDeletion->isEmpty()) {
                    \MembreIndividuQuery::create()
                        ->filterByPrimaryKeys($this->membreIndividusScheduledForDeletion->getPrimaryKeys(false))
                        ->delete($con);
                    $this->membreIndividusScheduledForDeletion = null;
                }
            }

            if ($this->collMembreIndividus !== null) {
                foreach ($this->collMembreIndividus as $referrerFK) {
                    if (!$referrerFK->isDeleted() && ($referrerFK->isNew() || $referrerFK->isModified())) {
                        $affectedRows += $referrerFK->save($con);
                    }
                }
            }

            if ($this->servicerenduIndividusScheduledForDeletion !== null) {
                if (!$this->servicerenduIndividusScheduledForDeletion->isEmpty()) {
                    \ServicerenduIndividuQuery::create()
                        ->filterByPrimaryKeys($this->servicerenduIndividusScheduledForDeletion->getPrimaryKeys(false))
                        ->delete($con);
                    $this->servicerenduIndividusScheduledForDeletion = null;
                }
            }

            if ($this->collServicerenduIndividus !== null) {
                foreach ($this->collServicerenduIndividus as $referrerFK) {
                    if (!$referrerFK->isDeleted() && ($referrerFK->isNew() || $referrerFK->isModified())) {
                        $affectedRows += $referrerFK->save($con);
                    }
                }
            }

            if ($this->notificationIndividusScheduledForDeletion !== null) {
                if (!$this->notificationIndividusScheduledForDeletion->isEmpty()) {
                    \NotificationIndividuQuery::create()
                        ->filterByPrimaryKeys($this->notificationIndividusScheduledForDeletion->getPrimaryKeys(false))
                        ->delete($con);
                    $this->notificationIndividusScheduledForDeletion = null;
                }
            }

            if ($this->collNotificationIndividus !== null) {
                foreach ($this->collNotificationIndividus as $referrerFK) {
                    if (!$referrerFK->isDeleted() && ($referrerFK->isNew() || $referrerFK->isModified())) {
                        $affectedRows += $referrerFK->save($con);
                    }
                }
            }

            if ($this->courrierLiensScheduledForDeletion !== null) {
                if (!$this->courrierLiensScheduledForDeletion->isEmpty()) {
                    \CourrierLienQuery::create()
                        ->filterByPrimaryKeys($this->courrierLiensScheduledForDeletion->getPrimaryKeys(false))
                        ->delete($con);
                    $this->courrierLiensScheduledForDeletion = null;
                }
            }

            if ($this->collCourrierLiens !== null) {
                foreach ($this->collCourrierLiens as $referrerFK) {
                    if (!$referrerFK->isDeleted() && ($referrerFK->isNew() || $referrerFK->isModified())) {
                        $affectedRows += $referrerFK->save($con);
                    }
                }
            }

            $this->alreadyInSave = false;

        }

        return $affectedRows;
    } // doSave()

    /**
     * Insert the row in the database.
     *
     * @param      ConnectionInterface $con
     *
     * @throws PropelException
     * @see doSave()
     */
    protected function doInsert(ConnectionInterface $con)
    {
        $modifiedColumns = array();
        $index = 0;

        $this->modifiedColumns[IndividuTableMap::COL_ID_INDIVIDU] = true;
        if (null !== $this->id_individu) {
            throw new PropelException('Cannot insert a value for auto-increment primary key (' . IndividuTableMap::COL_ID_INDIVIDU . ')');
        }

         // check the columns in natural order for more readable SQL queries
        if ($this->isColumnModified(IndividuTableMap::COL_ID_INDIVIDU)) {
            $modifiedColumns[':p' . $index++]  = '`id_individu`';
        }
        if ($this->isColumnModified(IndividuTableMap::COL_NOM)) {
            $modifiedColumns[':p' . $index++]  = '`nom`';
        }
        if ($this->isColumnModified(IndividuTableMap::COL_BIO)) {
            $modifiedColumns[':p' . $index++]  = '`bio`';
        }
        if ($this->isColumnModified(IndividuTableMap::COL_EMAIL)) {
            $modifiedColumns[':p' . $index++]  = '`email`';
        }
        if ($this->isColumnModified(IndividuTableMap::COL_LOGIN)) {
            $modifiedColumns[':p' . $index++]  = '`login`';
        }
        if ($this->isColumnModified(IndividuTableMap::COL_PASS)) {
            $modifiedColumns[':p' . $index++]  = '`pass`';
        }
        if ($this->isColumnModified(IndividuTableMap::COL_ALEA_ACTUEL)) {
            $modifiedColumns[':p' . $index++]  = '`alea_actuel`';
        }
        if ($this->isColumnModified(IndividuTableMap::COL_ALEA_FUTUR)) {
            $modifiedColumns[':p' . $index++]  = '`alea_futur`';
        }
        if ($this->isColumnModified(IndividuTableMap::COL_TOKEN)) {
            $modifiedColumns[':p' . $index++]  = '`token`';
        }
        if ($this->isColumnModified(IndividuTableMap::COL_TOKEN_TIME)) {
            $modifiedColumns[':p' . $index++]  = '`token_time`';
        }
        if ($this->isColumnModified(IndividuTableMap::COL_CIVILITE)) {
            $modifiedColumns[':p' . $index++]  = '`civilite`';
        }
        if ($this->isColumnModified(IndividuTableMap::COL_SEXE)) {
            $modifiedColumns[':p' . $index++]  = '`sexe`';
        }
        if ($this->isColumnModified(IndividuTableMap::COL_NOM_FAMILLE)) {
            $modifiedColumns[':p' . $index++]  = '`nom_famille`';
        }
        if ($this->isColumnModified(IndividuTableMap::COL_PRENOM)) {
            $modifiedColumns[':p' . $index++]  = '`prenom`';
        }
        if ($this->isColumnModified(IndividuTableMap::COL_NAISSANCE)) {
            $modifiedColumns[':p' . $index++]  = '`naissance`';
        }
        if ($this->isColumnModified(IndividuTableMap::COL_ADRESSE)) {
            $modifiedColumns[':p' . $index++]  = '`adresse`';
        }
        if ($this->isColumnModified(IndividuTableMap::COL_CODEPOSTAL)) {
            $modifiedColumns[':p' . $index++]  = '`codepostal`';
        }
        if ($this->isColumnModified(IndividuTableMap::COL_VILLE)) {
            $modifiedColumns[':p' . $index++]  = '`ville`';
        }
        if ($this->isColumnModified(IndividuTableMap::COL_PAYS)) {
            $modifiedColumns[':p' . $index++]  = '`pays`';
        }
        if ($this->isColumnModified(IndividuTableMap::COL_TELEPHONE)) {
            $modifiedColumns[':p' . $index++]  = '`telephone`';
        }
        if ($this->isColumnModified(IndividuTableMap::COL_TELEPHONE_PRO)) {
            $modifiedColumns[':p' . $index++]  = '`telephone_pro`';
        }
        if ($this->isColumnModified(IndividuTableMap::COL_FAX)) {
            $modifiedColumns[':p' . $index++]  = '`fax`';
        }
        if ($this->isColumnModified(IndividuTableMap::COL_MOBILE)) {
            $modifiedColumns[':p' . $index++]  = '`mobile`';
        }
        if ($this->isColumnModified(IndividuTableMap::COL_URL)) {
            $modifiedColumns[':p' . $index++]  = '`url`';
        }
        if ($this->isColumnModified(IndividuTableMap::COL_PROFESSION)) {
            $modifiedColumns[':p' . $index++]  = '`profession`';
        }
        if ($this->isColumnModified(IndividuTableMap::COL_CONTACT_SOUHAIT)) {
            $modifiedColumns[':p' . $index++]  = '`contact_souhait`';
        }
        if ($this->isColumnModified(IndividuTableMap::COL_OBSERVATION)) {
            $modifiedColumns[':p' . $index++]  = '`observation`';
        }
        if ($this->isColumnModified(IndividuTableMap::COL_CREATED_AT)) {
            $modifiedColumns[':p' . $index++]  = '`created_at`';
        }
        if ($this->isColumnModified(IndividuTableMap::COL_UPDATED_AT)) {
            $modifiedColumns[':p' . $index++]  = '`updated_at`';
        }

        $sql = sprintf(
            'INSERT INTO `asso_individus` (%s) VALUES (%s)',
            implode(', ', $modifiedColumns),
            implode(', ', array_keys($modifiedColumns))
        );

        try {
            $stmt = $con->prepare($sql);
            foreach ($modifiedColumns as $identifier => $columnName) {
                switch ($columnName) {
                    case '`id_individu`':
                        $stmt->bindValue($identifier, $this->id_individu, PDO::PARAM_INT);
                        break;
                    case '`nom`':
                        $stmt->bindValue($identifier, $this->nom, PDO::PARAM_STR);
                        break;
                    case '`bio`':
                        $stmt->bindValue($identifier, $this->bio, PDO::PARAM_STR);
                        break;
                    case '`email`':
                        $stmt->bindValue($identifier, $this->email, PDO::PARAM_STR);
                        break;
                    case '`login`':
                        $stmt->bindValue($identifier, $this->login, PDO::PARAM_STR);
                        break;
                    case '`pass`':
                        $stmt->bindValue($identifier, $this->pass, PDO::PARAM_STR);
                        break;
                    case '`alea_actuel`':
                        $stmt->bindValue($identifier, $this->alea_actuel, PDO::PARAM_STR);
                        break;
                    case '`alea_futur`':
                        $stmt->bindValue($identifier, $this->alea_futur, PDO::PARAM_STR);
                        break;
                    case '`token`':
                        $stmt->bindValue($identifier, $this->token, PDO::PARAM_STR);
                        break;
                    case '`token_time`':
                        $stmt->bindValue($identifier, $this->token_time, PDO::PARAM_INT);
                        break;
                    case '`civilite`':
                        $stmt->bindValue($identifier, $this->civilite, PDO::PARAM_STR);
                        break;
                    case '`sexe`':
                        $stmt->bindValue($identifier, $this->sexe, PDO::PARAM_STR);
                        break;
                    case '`nom_famille`':
                        $stmt->bindValue($identifier, $this->nom_famille, PDO::PARAM_STR);
                        break;
                    case '`prenom`':
                        $stmt->bindValue($identifier, $this->prenom, PDO::PARAM_STR);
                        break;
                    case '`naissance`':
                        $stmt->bindValue($identifier, $this->naissance ? $this->naissance->format("Y-m-d H:i:s.u") : null, PDO::PARAM_STR);
                        break;
                    case '`adresse`':
                        $stmt->bindValue($identifier, $this->adresse, PDO::PARAM_STR);
                        break;
                    case '`codepostal`':
                        $stmt->bindValue($identifier, $this->codepostal, PDO::PARAM_STR);
                        break;
                    case '`ville`':
                        $stmt->bindValue($identifier, $this->ville, PDO::PARAM_STR);
                        break;
                    case '`pays`':
                        $stmt->bindValue($identifier, $this->pays, PDO::PARAM_STR);
                        break;
                    case '`telephone`':
                        $stmt->bindValue($identifier, $this->telephone, PDO::PARAM_STR);
                        break;
                    case '`telephone_pro`':
                        $stmt->bindValue($identifier, $this->telephone_pro, PDO::PARAM_STR);
                        break;
                    case '`fax`':
                        $stmt->bindValue($identifier, $this->fax, PDO::PARAM_STR);
                        break;
                    case '`mobile`':
                        $stmt->bindValue($identifier, $this->mobile, PDO::PARAM_STR);
                        break;
                    case '`url`':
                        $stmt->bindValue($identifier, $this->url, PDO::PARAM_STR);
                        break;
                    case '`profession`':
                        $stmt->bindValue($identifier, $this->profession, PDO::PARAM_STR);
                        break;
                    case '`contact_souhait`':
                        $stmt->bindValue($identifier, (int) $this->contact_souhait, PDO::PARAM_INT);
                        break;
                    case '`observation`':
                        $stmt->bindValue($identifier, $this->observation, PDO::PARAM_STR);
                        break;
                    case '`created_at`':
                        $stmt->bindValue($identifier, $this->created_at ? $this->created_at->format("Y-m-d H:i:s.u") : null, PDO::PARAM_STR);
                        break;
                    case '`updated_at`':
                        $stmt->bindValue($identifier, $this->updated_at ? $this->updated_at->format("Y-m-d H:i:s.u") : null, PDO::PARAM_STR);
                        break;
                }
            }
            $stmt->execute();
        } catch (Exception $e) {
            Propel::log($e->getMessage(), Propel::LOG_ERR);
            throw new PropelException(sprintf('Unable to execute INSERT statement [%s]', $sql), 0, $e);
        }

        try {
            $pk = $con->lastInsertId();
        } catch (Exception $e) {
            throw new PropelException('Unable to get autoincrement id.', 0, $e);
        }
        $this->setIdIndividu($pk);

        $this->setNew(false);
    }

    /**
     * Update the row in the database.
     *
     * @param      ConnectionInterface $con
     *
     * @return Integer Number of updated rows
     * @see doSave()
     */
    protected function doUpdate(ConnectionInterface $con)
    {
        $selectCriteria = $this->buildPkeyCriteria();
        $valuesCriteria = $this->buildCriteria();

        return $selectCriteria->doUpdate($valuesCriteria, $con);
    }

    /**
     * Retrieves a field from the object by name passed in as a string.
     *
     * @param      string $name name
     * @param      string $type The type of fieldname the $name is of:
     *                     one of the class type constants TableMap::TYPE_PHPNAME, TableMap::TYPE_CAMELNAME
     *                     TableMap::TYPE_COLNAME, TableMap::TYPE_FIELDNAME, TableMap::TYPE_NUM.
     *                     Defaults to TableMap::TYPE_FIELDNAME.
     * @return mixed Value of field.
     */
    public function getByName($name, $type = TableMap::TYPE_FIELDNAME)
    {
        $pos = IndividuTableMap::translateFieldName($name, $type, TableMap::TYPE_NUM);
        $field = $this->getByPosition($pos);

        return $field;
    }

    /**
     * Retrieves a field from the object by Position as specified in the xml schema.
     * Zero-based.
     *
     * @param      int $pos position in xml schema
     * @return mixed Value of field at $pos
     */
    public function getByPosition($pos)
    {
        switch ($pos) {
            case 0:
                return $this->getIdIndividu();
                break;
            case 1:
                return $this->getNom();
                break;
            case 2:
                return $this->getBio();
                break;
            case 3:
                return $this->getEmail();
                break;
            case 4:
                return $this->getLogin();
                break;
            case 5:
                return $this->getPass();
                break;
            case 6:
                return $this->getAleaActuel();
                break;
            case 7:
                return $this->getAleaFutur();
                break;
            case 8:
                return $this->getToken();
                break;
            case 9:
                return $this->getTokenTime();
                break;
            case 10:
                return $this->getCivilite();
                break;
            case 11:
                return $this->getSexe();
                break;
            case 12:
                return $this->getNomFamille();
                break;
            case 13:
                return $this->getPrenom();
                break;
            case 14:
                return $this->getNaissance();
                break;
            case 15:
                return $this->getAdresse();
                break;
            case 16:
                return $this->getCodepostal();
                break;
            case 17:
                return $this->getVille();
                break;
            case 18:
                return $this->getPays();
                break;
            case 19:
                return $this->getTelephone();
                break;
            case 20:
                return $this->getTelephonePro();
                break;
            case 21:
                return $this->getFax();
                break;
            case 22:
                return $this->getMobile();
                break;
            case 23:
                return $this->getUrl();
                break;
            case 24:
                return $this->getProfession();
                break;
            case 25:
                return $this->getContactSouhait();
                break;
            case 26:
                return $this->getObservation();
                break;
            case 27:
                return $this->getCreatedAt();
                break;
            case 28:
                return $this->getUpdatedAt();
                break;
            default:
                return null;
                break;
        } // switch()
    }

    /**
     * Exports the object as an array.
     *
     * You can specify the key type of the array by passing one of the class
     * type constants.
     *
     * @param     string  $keyType (optional) One of the class type constants TableMap::TYPE_PHPNAME, TableMap::TYPE_CAMELNAME,
     *                    TableMap::TYPE_COLNAME, TableMap::TYPE_FIELDNAME, TableMap::TYPE_NUM.
     *                    Defaults to TableMap::TYPE_FIELDNAME.
     * @param     boolean $includeLazyLoadColumns (optional) Whether to include lazy loaded columns. Defaults to TRUE.
     * @param     array $alreadyDumpedObjects List of objects to skip to avoid recursion
     * @param     boolean $includeForeignObjects (optional) Whether to include hydrated related objects. Default to FALSE.
     *
     * @return array an associative array containing the field names (as keys) and field values
     */
    public function toArray($keyType = TableMap::TYPE_FIELDNAME, $includeLazyLoadColumns = true, $alreadyDumpedObjects = array(), $includeForeignObjects = false)
    {

        if (isset($alreadyDumpedObjects['Individu'][$this->hashCode()])) {
            return '*RECURSION*';
        }
        $alreadyDumpedObjects['Individu'][$this->hashCode()] = true;
        $keys = IndividuTableMap::getFieldNames($keyType);
        $result = array(
            $keys[0] => $this->getIdIndividu(),
            $keys[1] => $this->getNom(),
            $keys[2] => $this->getBio(),
            $keys[3] => $this->getEmail(),
            $keys[4] => $this->getLogin(),
            $keys[5] => $this->getPass(),
            $keys[6] => $this->getAleaActuel(),
            $keys[7] => $this->getAleaFutur(),
            $keys[8] => $this->getToken(),
            $keys[9] => $this->getTokenTime(),
            $keys[10] => $this->getCivilite(),
            $keys[11] => $this->getSexe(),
            $keys[12] => $this->getNomFamille(),
            $keys[13] => $this->getPrenom(),
            $keys[14] => $this->getNaissance(),
            $keys[15] => $this->getAdresse(),
            $keys[16] => $this->getCodepostal(),
            $keys[17] => $this->getVille(),
            $keys[18] => $this->getPays(),
            $keys[19] => $this->getTelephone(),
            $keys[20] => $this->getTelephonePro(),
            $keys[21] => $this->getFax(),
            $keys[22] => $this->getMobile(),
            $keys[23] => $this->getUrl(),
            $keys[24] => $this->getProfession(),
            $keys[25] => $this->getContactSouhait(),
            $keys[26] => $this->getObservation(),
            $keys[27] => $this->getCreatedAt(),
            $keys[28] => $this->getUpdatedAt(),
        );
        if ($result[$keys[14]] instanceof \DateTimeInterface) {
            $result[$keys[14]] = $result[$keys[14]]->format('c');
        }

        if ($result[$keys[27]] instanceof \DateTimeInterface) {
            $result[$keys[27]] = $result[$keys[27]]->format('c');
        }

        if ($result[$keys[28]] instanceof \DateTimeInterface) {
            $result[$keys[28]] = $result[$keys[28]]->format('c');
        }

        $virtualColumns = $this->virtualColumns;
        foreach ($virtualColumns as $key => $virtualColumn) {
            $result[$key] = $virtualColumn;
        }

        if ($includeForeignObjects) {
            if (null !== $this->collAutorisations) {

                switch ($keyType) {
                    case TableMap::TYPE_CAMELNAME:
                        $key = 'autorisations';
                        break;
                    case TableMap::TYPE_FIELDNAME:
                        $key = 'asso_autorisationss';
                        break;
                    default:
                        $key = 'Autorisations';
                }

                $result[$key] = $this->collAutorisations->toArray(null, false, $keyType, $includeLazyLoadColumns, $alreadyDumpedObjects);
            }
            if (null !== $this->collLogs) {

                switch ($keyType) {
                    case TableMap::TYPE_CAMELNAME:
                        $key = 'logs';
                        break;
                    case TableMap::TYPE_FIELDNAME:
                        $key = 'asso_logss';
                        break;
                    default:
                        $key = 'Logs';
                }

                $result[$key] = $this->collLogs->toArray(null, false, $keyType, $includeLazyLoadColumns, $alreadyDumpedObjects);
            }
            if (null !== $this->collMembres0s) {

                switch ($keyType) {
                    case TableMap::TYPE_CAMELNAME:
                        $key = 'membres';
                        break;
                    case TableMap::TYPE_FIELDNAME:
                        $key = 'asso_membress';
                        break;
                    default:
                        $key = 'Membres0s';
                }

                $result[$key] = $this->collMembres0s->toArray(null, false, $keyType, $includeLazyLoadColumns, $alreadyDumpedObjects);
            }
            if (null !== $this->collMembreIndividus) {

                switch ($keyType) {
                    case TableMap::TYPE_CAMELNAME:
                        $key = 'membreIndividus';
                        break;
                    case TableMap::TYPE_FIELDNAME:
                        $key = 'asso_membres_individuses';
                        break;
                    default:
                        $key = 'MembreIndividus';
                }

                $result[$key] = $this->collMembreIndividus->toArray(null, false, $keyType, $includeLazyLoadColumns, $alreadyDumpedObjects);
            }
            if (null !== $this->collServicerenduIndividus) {

                switch ($keyType) {
                    case TableMap::TYPE_CAMELNAME:
                        $key = 'servicerenduIndividus';
                        break;
                    case TableMap::TYPE_FIELDNAME:
                        $key = 'asso_servicerendus_individuses';
                        break;
                    default:
                        $key = 'ServicerenduIndividus';
                }

                $result[$key] = $this->collServicerenduIndividus->toArray(null, false, $keyType, $includeLazyLoadColumns, $alreadyDumpedObjects);
            }
            if (null !== $this->collNotificationIndividus) {

                switch ($keyType) {
                    case TableMap::TYPE_CAMELNAME:
                        $key = 'notificationIndividus';
                        break;
                    case TableMap::TYPE_FIELDNAME:
                        $key = 'asso_notifications_individuses';
                        break;
                    default:
                        $key = 'NotificationIndividus';
                }

                $result[$key] = $this->collNotificationIndividus->toArray(null, false, $keyType, $includeLazyLoadColumns, $alreadyDumpedObjects);
            }
            if (null !== $this->collCourrierLiens) {

                switch ($keyType) {
                    case TableMap::TYPE_CAMELNAME:
                        $key = 'courrierLiens';
                        break;
                    case TableMap::TYPE_FIELDNAME:
                        $key = 'com_courriers_lienss';
                        break;
                    default:
                        $key = 'CourrierLiens';
                }

                $result[$key] = $this->collCourrierLiens->toArray(null, false, $keyType, $includeLazyLoadColumns, $alreadyDumpedObjects);
            }
        }

        return $result;
    }

    /**
     * Sets a field from the object by name passed in as a string.
     *
     * @param  string $name
     * @param  mixed  $value field value
     * @param  string $type The type of fieldname the $name is of:
     *                one of the class type constants TableMap::TYPE_PHPNAME, TableMap::TYPE_CAMELNAME
     *                TableMap::TYPE_COLNAME, TableMap::TYPE_FIELDNAME, TableMap::TYPE_NUM.
     *                Defaults to TableMap::TYPE_FIELDNAME.
     * @return $this|\Individu
     */
    public function setByName($name, $value, $type = TableMap::TYPE_FIELDNAME)
    {
        $pos = IndividuTableMap::translateFieldName($name, $type, TableMap::TYPE_NUM);

        return $this->setByPosition($pos, $value);
    }

    /**
     * Sets a field from the object by Position as specified in the xml schema.
     * Zero-based.
     *
     * @param  int $pos position in xml schema
     * @param  mixed $value field value
     * @return $this|\Individu
     */
    public function setByPosition($pos, $value)
    {
        switch ($pos) {
            case 0:
                $this->setIdIndividu($value);
                break;
            case 1:
                $this->setNom($value);
                break;
            case 2:
                $this->setBio($value);
                break;
            case 3:
                $this->setEmail($value);
                break;
            case 4:
                $this->setLogin($value);
                break;
            case 5:
                $this->setPass($value);
                break;
            case 6:
                $this->setAleaActuel($value);
                break;
            case 7:
                $this->setAleaFutur($value);
                break;
            case 8:
                $this->setToken($value);
                break;
            case 9:
                $this->setTokenTime($value);
                break;
            case 10:
                $this->setCivilite($value);
                break;
            case 11:
                $this->setSexe($value);
                break;
            case 12:
                $this->setNomFamille($value);
                break;
            case 13:
                $this->setPrenom($value);
                break;
            case 14:
                $this->setNaissance($value);
                break;
            case 15:
                $this->setAdresse($value);
                break;
            case 16:
                $this->setCodepostal($value);
                break;
            case 17:
                $this->setVille($value);
                break;
            case 18:
                $this->setPays($value);
                break;
            case 19:
                $this->setTelephone($value);
                break;
            case 20:
                $this->setTelephonePro($value);
                break;
            case 21:
                $this->setFax($value);
                break;
            case 22:
                $this->setMobile($value);
                break;
            case 23:
                $this->setUrl($value);
                break;
            case 24:
                $this->setProfession($value);
                break;
            case 25:
                $this->setContactSouhait($value);
                break;
            case 26:
                $this->setObservation($value);
                break;
            case 27:
                $this->setCreatedAt($value);
                break;
            case 28:
                $this->setUpdatedAt($value);
                break;
        } // switch()

        return $this;
    }

    /**
     * Populates the object using an array.
     *
     * This is particularly useful when populating an object from one of the
     * request arrays (e.g. $_POST).  This method goes through the column
     * names, checking to see whether a matching key exists in populated
     * array. If so the setByName() method is called for that column.
     *
     * You can specify the key type of the array by additionally passing one
     * of the class type constants TableMap::TYPE_PHPNAME, TableMap::TYPE_CAMELNAME,
     * TableMap::TYPE_COLNAME, TableMap::TYPE_FIELDNAME, TableMap::TYPE_NUM.
     * The default key type is the column's TableMap::TYPE_FIELDNAME.
     *
     * @param      array  $arr     An array to populate the object from.
     * @param      string $keyType The type of keys the array uses.
     * @return void
     */
    public function fromArray($arr, $keyType = TableMap::TYPE_FIELDNAME)
    {
        $keys = IndividuTableMap::getFieldNames($keyType);

        if (array_key_exists($keys[0], $arr)) {
            $this->setIdIndividu($arr[$keys[0]]);
        }
        if (array_key_exists($keys[1], $arr)) {
            $this->setNom($arr[$keys[1]]);
        }
        if (array_key_exists($keys[2], $arr)) {
            $this->setBio($arr[$keys[2]]);
        }
        if (array_key_exists($keys[3], $arr)) {
            $this->setEmail($arr[$keys[3]]);
        }
        if (array_key_exists($keys[4], $arr)) {
            $this->setLogin($arr[$keys[4]]);
        }
        if (array_key_exists($keys[5], $arr)) {
            $this->setPass($arr[$keys[5]]);
        }
        if (array_key_exists($keys[6], $arr)) {
            $this->setAleaActuel($arr[$keys[6]]);
        }
        if (array_key_exists($keys[7], $arr)) {
            $this->setAleaFutur($arr[$keys[7]]);
        }
        if (array_key_exists($keys[8], $arr)) {
            $this->setToken($arr[$keys[8]]);
        }
        if (array_key_exists($keys[9], $arr)) {
            $this->setTokenTime($arr[$keys[9]]);
        }
        if (array_key_exists($keys[10], $arr)) {
            $this->setCivilite($arr[$keys[10]]);
        }
        if (array_key_exists($keys[11], $arr)) {
            $this->setSexe($arr[$keys[11]]);
        }
        if (array_key_exists($keys[12], $arr)) {
            $this->setNomFamille($arr[$keys[12]]);
        }
        if (array_key_exists($keys[13], $arr)) {
            $this->setPrenom($arr[$keys[13]]);
        }
        if (array_key_exists($keys[14], $arr)) {
            $this->setNaissance($arr[$keys[14]]);
        }
        if (array_key_exists($keys[15], $arr)) {
            $this->setAdresse($arr[$keys[15]]);
        }
        if (array_key_exists($keys[16], $arr)) {
            $this->setCodepostal($arr[$keys[16]]);
        }
        if (array_key_exists($keys[17], $arr)) {
            $this->setVille($arr[$keys[17]]);
        }
        if (array_key_exists($keys[18], $arr)) {
            $this->setPays($arr[$keys[18]]);
        }
        if (array_key_exists($keys[19], $arr)) {
            $this->setTelephone($arr[$keys[19]]);
        }
        if (array_key_exists($keys[20], $arr)) {
            $this->setTelephonePro($arr[$keys[20]]);
        }
        if (array_key_exists($keys[21], $arr)) {
            $this->setFax($arr[$keys[21]]);
        }
        if (array_key_exists($keys[22], $arr)) {
            $this->setMobile($arr[$keys[22]]);
        }
        if (array_key_exists($keys[23], $arr)) {
            $this->setUrl($arr[$keys[23]]);
        }
        if (array_key_exists($keys[24], $arr)) {
            $this->setProfession($arr[$keys[24]]);
        }
        if (array_key_exists($keys[25], $arr)) {
            $this->setContactSouhait($arr[$keys[25]]);
        }
        if (array_key_exists($keys[26], $arr)) {
            $this->setObservation($arr[$keys[26]]);
        }
        if (array_key_exists($keys[27], $arr)) {
            $this->setCreatedAt($arr[$keys[27]]);
        }
        if (array_key_exists($keys[28], $arr)) {
            $this->setUpdatedAt($arr[$keys[28]]);
        }
    }

     /**
     * Populate the current object from a string, using a given parser format
     * <code>
     * $book = new Book();
     * $book->importFrom('JSON', '{"Id":9012,"Title":"Don Juan","ISBN":"0140422161","Price":12.99,"PublisherId":1234,"AuthorId":5678}');
     * </code>
     *
     * You can specify the key type of the array by additionally passing one
     * of the class type constants TableMap::TYPE_PHPNAME, TableMap::TYPE_CAMELNAME,
     * TableMap::TYPE_COLNAME, TableMap::TYPE_FIELDNAME, TableMap::TYPE_NUM.
     * The default key type is the column's TableMap::TYPE_FIELDNAME.
     *
     * @param mixed $parser A AbstractParser instance,
     *                       or a format name ('XML', 'YAML', 'JSON', 'CSV')
     * @param string $data The source data to import from
     * @param string $keyType The type of keys the array uses.
     *
     * @return $this|\Individu The current object, for fluid interface
     */
    public function importFrom($parser, $data, $keyType = TableMap::TYPE_FIELDNAME)
    {
        if (!$parser instanceof AbstractParser) {
            $parser = AbstractParser::getParser($parser);
        }

        $this->fromArray($parser->toArray($data), $keyType);

        return $this;
    }

    /**
     * Build a Criteria object containing the values of all modified columns in this object.
     *
     * @return Criteria The Criteria object containing all modified values.
     */
    public function buildCriteria()
    {
        $criteria = new Criteria(IndividuTableMap::DATABASE_NAME);

        if ($this->isColumnModified(IndividuTableMap::COL_ID_INDIVIDU)) {
            $criteria->add(IndividuTableMap::COL_ID_INDIVIDU, $this->id_individu);
        }
        if ($this->isColumnModified(IndividuTableMap::COL_NOM)) {
            $criteria->add(IndividuTableMap::COL_NOM, $this->nom);
        }
        if ($this->isColumnModified(IndividuTableMap::COL_BIO)) {
            $criteria->add(IndividuTableMap::COL_BIO, $this->bio);
        }
        if ($this->isColumnModified(IndividuTableMap::COL_EMAIL)) {
            $criteria->add(IndividuTableMap::COL_EMAIL, $this->email);
        }
        if ($this->isColumnModified(IndividuTableMap::COL_LOGIN)) {
            $criteria->add(IndividuTableMap::COL_LOGIN, $this->login);
        }
        if ($this->isColumnModified(IndividuTableMap::COL_PASS)) {
            $criteria->add(IndividuTableMap::COL_PASS, $this->pass);
        }
        if ($this->isColumnModified(IndividuTableMap::COL_ALEA_ACTUEL)) {
            $criteria->add(IndividuTableMap::COL_ALEA_ACTUEL, $this->alea_actuel);
        }
        if ($this->isColumnModified(IndividuTableMap::COL_ALEA_FUTUR)) {
            $criteria->add(IndividuTableMap::COL_ALEA_FUTUR, $this->alea_futur);
        }
        if ($this->isColumnModified(IndividuTableMap::COL_TOKEN)) {
            $criteria->add(IndividuTableMap::COL_TOKEN, $this->token);
        }
        if ($this->isColumnModified(IndividuTableMap::COL_TOKEN_TIME)) {
            $criteria->add(IndividuTableMap::COL_TOKEN_TIME, $this->token_time);
        }
        if ($this->isColumnModified(IndividuTableMap::COL_CIVILITE)) {
            $criteria->add(IndividuTableMap::COL_CIVILITE, $this->civilite);
        }
        if ($this->isColumnModified(IndividuTableMap::COL_SEXE)) {
            $criteria->add(IndividuTableMap::COL_SEXE, $this->sexe);
        }
        if ($this->isColumnModified(IndividuTableMap::COL_NOM_FAMILLE)) {
            $criteria->add(IndividuTableMap::COL_NOM_FAMILLE, $this->nom_famille);
        }
        if ($this->isColumnModified(IndividuTableMap::COL_PRENOM)) {
            $criteria->add(IndividuTableMap::COL_PRENOM, $this->prenom);
        }
        if ($this->isColumnModified(IndividuTableMap::COL_NAISSANCE)) {
            $criteria->add(IndividuTableMap::COL_NAISSANCE, $this->naissance);
        }
        if ($this->isColumnModified(IndividuTableMap::COL_ADRESSE)) {
            $criteria->add(IndividuTableMap::COL_ADRESSE, $this->adresse);
        }
        if ($this->isColumnModified(IndividuTableMap::COL_CODEPOSTAL)) {
            $criteria->add(IndividuTableMap::COL_CODEPOSTAL, $this->codepostal);
        }
        if ($this->isColumnModified(IndividuTableMap::COL_VILLE)) {
            $criteria->add(IndividuTableMap::COL_VILLE, $this->ville);
        }
        if ($this->isColumnModified(IndividuTableMap::COL_PAYS)) {
            $criteria->add(IndividuTableMap::COL_PAYS, $this->pays);
        }
        if ($this->isColumnModified(IndividuTableMap::COL_TELEPHONE)) {
            $criteria->add(IndividuTableMap::COL_TELEPHONE, $this->telephone);
        }
        if ($this->isColumnModified(IndividuTableMap::COL_TELEPHONE_PRO)) {
            $criteria->add(IndividuTableMap::COL_TELEPHONE_PRO, $this->telephone_pro);
        }
        if ($this->isColumnModified(IndividuTableMap::COL_FAX)) {
            $criteria->add(IndividuTableMap::COL_FAX, $this->fax);
        }
        if ($this->isColumnModified(IndividuTableMap::COL_MOBILE)) {
            $criteria->add(IndividuTableMap::COL_MOBILE, $this->mobile);
        }
        if ($this->isColumnModified(IndividuTableMap::COL_URL)) {
            $criteria->add(IndividuTableMap::COL_URL, $this->url);
        }
        if ($this->isColumnModified(IndividuTableMap::COL_PROFESSION)) {
            $criteria->add(IndividuTableMap::COL_PROFESSION, $this->profession);
        }
        if ($this->isColumnModified(IndividuTableMap::COL_CONTACT_SOUHAIT)) {
            $criteria->add(IndividuTableMap::COL_CONTACT_SOUHAIT, $this->contact_souhait);
        }
        if ($this->isColumnModified(IndividuTableMap::COL_OBSERVATION)) {
            $criteria->add(IndividuTableMap::COL_OBSERVATION, $this->observation);
        }
        if ($this->isColumnModified(IndividuTableMap::COL_CREATED_AT)) {
            $criteria->add(IndividuTableMap::COL_CREATED_AT, $this->created_at);
        }
        if ($this->isColumnModified(IndividuTableMap::COL_UPDATED_AT)) {
            $criteria->add(IndividuTableMap::COL_UPDATED_AT, $this->updated_at);
        }

        return $criteria;
    }

    /**
     * Builds a Criteria object containing the primary key for this object.
     *
     * Unlike buildCriteria() this method includes the primary key values regardless
     * of whether or not they have been modified.
     *
     * @throws LogicException if no primary key is defined
     *
     * @return Criteria The Criteria object containing value(s) for primary key(s).
     */
    public function buildPkeyCriteria()
    {
        $criteria = ChildIndividuQuery::create();
        $criteria->add(IndividuTableMap::COL_ID_INDIVIDU, $this->id_individu);

        return $criteria;
    }

    /**
     * If the primary key is not null, return the hashcode of the
     * primary key. Otherwise, return the hash code of the object.
     *
     * @return int Hashcode
     */
    public function hashCode()
    {
        $validPk = null !== $this->getIdIndividu();

        $validPrimaryKeyFKs = 0;
        $primaryKeyFKs = [];

        if ($validPk) {
            return crc32(json_encode($this->getPrimaryKey(), JSON_UNESCAPED_UNICODE));
        } elseif ($validPrimaryKeyFKs) {
            return crc32(json_encode($primaryKeyFKs, JSON_UNESCAPED_UNICODE));
        }

        return spl_object_hash($this);
    }

    /**
     * Returns the primary key for this object (row).
     * @return string
     */
    public function getPrimaryKey()
    {
        return $this->getIdIndividu();
    }

    /**
     * Generic method to set the primary key (id_individu column).
     *
     * @param       string $key Primary key.
     * @return void
     */
    public function setPrimaryKey($key)
    {
        $this->setIdIndividu($key);
    }

    /**
     * Returns true if the primary key for this object is null.
     * @return boolean
     */
    public function isPrimaryKeyNull()
    {
        return null === $this->getIdIndividu();
    }

    /**
     * Sets contents of passed object to values from current object.
     *
     * If desired, this method can also make copies of all associated (fkey referrers)
     * objects.
     *
     * @param      object $copyObj An object of \Individu (or compatible) type.
     * @param      boolean $deepCopy Whether to also copy all rows that refer (by fkey) to the current row.
     * @param      boolean $makeNew Whether to reset autoincrement PKs and make the object new.
     * @throws PropelException
     */
    public function copyInto($copyObj, $deepCopy = false, $makeNew = true)
    {
        $copyObj->setNom($this->getNom());
        $copyObj->setBio($this->getBio());
        $copyObj->setEmail($this->getEmail());
        $copyObj->setLogin($this->getLogin());
        $copyObj->setPass($this->getPass());
        $copyObj->setAleaActuel($this->getAleaActuel());
        $copyObj->setAleaFutur($this->getAleaFutur());
        $copyObj->setToken($this->getToken());
        $copyObj->setTokenTime($this->getTokenTime());
        $copyObj->setCivilite($this->getCivilite());
        $copyObj->setSexe($this->getSexe());
        $copyObj->setNomFamille($this->getNomFamille());
        $copyObj->setPrenom($this->getPrenom());
        $copyObj->setNaissance($this->getNaissance());
        $copyObj->setAdresse($this->getAdresse());
        $copyObj->setCodepostal($this->getCodepostal());
        $copyObj->setVille($this->getVille());
        $copyObj->setPays($this->getPays());
        $copyObj->setTelephone($this->getTelephone());
        $copyObj->setTelephonePro($this->getTelephonePro());
        $copyObj->setFax($this->getFax());
        $copyObj->setMobile($this->getMobile());
        $copyObj->setUrl($this->getUrl());
        $copyObj->setProfession($this->getProfession());
        $copyObj->setContactSouhait($this->getContactSouhait());
        $copyObj->setObservation($this->getObservation());
        $copyObj->setCreatedAt($this->getCreatedAt());
        $copyObj->setUpdatedAt($this->getUpdatedAt());

        if ($deepCopy) {
            // important: temporarily setNew(false) because this affects the behavior of
            // the getter/setter methods for fkey referrer objects.
            $copyObj->setNew(false);

            foreach ($this->getAutorisations() as $relObj) {
                if ($relObj !== $this) {  // ensure that we don't try to copy a reference to ourselves
                    $copyObj->addAutorisation($relObj->copy($deepCopy));
                }
            }

            foreach ($this->getLogs() as $relObj) {
                if ($relObj !== $this) {  // ensure that we don't try to copy a reference to ourselves
                    $copyObj->addLog($relObj->copy($deepCopy));
                }
            }

            foreach ($this->getMembres0s() as $relObj) {
                if ($relObj !== $this) {  // ensure that we don't try to copy a reference to ourselves
                    $copyObj->addMembres0($relObj->copy($deepCopy));
                }
            }

            foreach ($this->getMembreIndividus() as $relObj) {
                if ($relObj !== $this) {  // ensure that we don't try to copy a reference to ourselves
                    $copyObj->addMembreIndividu($relObj->copy($deepCopy));
                }
            }

            foreach ($this->getServicerenduIndividus() as $relObj) {
                if ($relObj !== $this) {  // ensure that we don't try to copy a reference to ourselves
                    $copyObj->addServicerenduIndividu($relObj->copy($deepCopy));
                }
            }

            foreach ($this->getNotificationIndividus() as $relObj) {
                if ($relObj !== $this) {  // ensure that we don't try to copy a reference to ourselves
                    $copyObj->addNotificationIndividu($relObj->copy($deepCopy));
                }
            }

            foreach ($this->getCourrierLiens() as $relObj) {
                if ($relObj !== $this) {  // ensure that we don't try to copy a reference to ourselves
                    $copyObj->addCourrierLien($relObj->copy($deepCopy));
                }
            }

        } // if ($deepCopy)

        if ($makeNew) {
            $copyObj->setNew(true);
            $copyObj->setIdIndividu(NULL); // this is a auto-increment column, so set to default value
        }
    }

    /**
     * Makes a copy of this object that will be inserted as a new row in table when saved.
     * It creates a new object filling in the simple attributes, but skipping any primary
     * keys that are defined for the table.
     *
     * If desired, this method can also make copies of all associated (fkey referrers)
     * objects.
     *
     * @param  boolean $deepCopy Whether to also copy all rows that refer (by fkey) to the current row.
     * @return \Individu Clone of current object.
     * @throws PropelException
     */
    public function copy($deepCopy = false)
    {
        // we use get_class(), because this might be a subclass
        $clazz = get_class($this);
        $copyObj = new $clazz();
        $this->copyInto($copyObj, $deepCopy);

        return $copyObj;
    }


    /**
     * Initializes a collection based on the name of a relation.
     * Avoids crafting an 'init[$relationName]s' method name
     * that wouldn't work when StandardEnglishPluralizer is used.
     *
     * @param      string $relationName The name of the relation to initialize
     * @return void
     */
    public function initRelation($relationName)
    {
        if ('Autorisation' == $relationName) {
            $this->initAutorisations();
            return;
        }
        if ('Log' == $relationName) {
            $this->initLogs();
            return;
        }
        if ('Membres0' == $relationName) {
            $this->initMembres0s();
            return;
        }
        if ('MembreIndividu' == $relationName) {
            $this->initMembreIndividus();
            return;
        }
        if ('ServicerenduIndividu' == $relationName) {
            $this->initServicerenduIndividus();
            return;
        }
        if ('NotificationIndividu' == $relationName) {
            $this->initNotificationIndividus();
            return;
        }
        if ('CourrierLien' == $relationName) {
            $this->initCourrierLiens();
            return;
        }
    }

    /**
     * Clears out the collAutorisations collection
     *
     * This does not modify the database; however, it will remove any associated objects, causing
     * them to be refetched by subsequent calls to accessor method.
     *
     * @return void
     * @see        addAutorisations()
     */
    public function clearAutorisations()
    {
        $this->collAutorisations = null; // important to set this to NULL since that means it is uninitialized
    }

    /**
     * Reset is the collAutorisations collection loaded partially.
     */
    public function resetPartialAutorisations($v = true)
    {
        $this->collAutorisationsPartial = $v;
    }

    /**
     * Initializes the collAutorisations collection.
     *
     * By default this just sets the collAutorisations collection to an empty array (like clearcollAutorisations());
     * however, you may wish to override this method in your stub class to provide setting appropriate
     * to your application -- for example, setting the initial array to the values stored in database.
     *
     * @param      boolean $overrideExisting If set to true, the method call initializes
     *                                        the collection even if it is not empty
     *
     * @return void
     */
    public function initAutorisations($overrideExisting = true)
    {
        if (null !== $this->collAutorisations && !$overrideExisting) {
            return;
        }

        $collectionClassName = AutorisationTableMap::getTableMap()->getCollectionClassName();

        $this->collAutorisations = new $collectionClassName;
        $this->collAutorisations->setModel('\Autorisation');
    }

    /**
     * Gets an array of ChildAutorisation objects which contain a foreign key that references this object.
     *
     * If the $criteria is not null, it is used to always fetch the results from the database.
     * Otherwise the results are fetched from the database the first time, then cached.
     * Next time the same method is called without $criteria, the cached collection is returned.
     * If this ChildIndividu is new, it will return
     * an empty collection or the current collection; the criteria is ignored on a new object.
     *
     * @param      Criteria $criteria optional Criteria object to narrow the query
     * @param      ConnectionInterface $con optional connection object
     * @return ObjectCollection|ChildAutorisation[] List of ChildAutorisation objects
     * @throws PropelException
     */
    public function getAutorisations(Criteria $criteria = null, ConnectionInterface $con = null)
    {
        $partial = $this->collAutorisationsPartial && !$this->isNew();
        if (null === $this->collAutorisations || null !== $criteria  || $partial) {
            if ($this->isNew() && null === $this->collAutorisations) {
                // return empty collection
                $this->initAutorisations();
            } else {
                $collAutorisations = ChildAutorisationQuery::create(null, $criteria)
                    ->filterByIndividu($this)
                    ->find($con);

                if (null !== $criteria) {
                    if (false !== $this->collAutorisationsPartial && count($collAutorisations)) {
                        $this->initAutorisations(false);

                        foreach ($collAutorisations as $obj) {
                            if (false == $this->collAutorisations->contains($obj)) {
                                $this->collAutorisations->append($obj);
                            }
                        }

                        $this->collAutorisationsPartial = true;
                    }

                    return $collAutorisations;
                }

                if ($partial && $this->collAutorisations) {
                    foreach ($this->collAutorisations as $obj) {
                        if ($obj->isNew()) {
                            $collAutorisations[] = $obj;
                        }
                    }
                }

                $this->collAutorisations = $collAutorisations;
                $this->collAutorisationsPartial = false;
            }
        }

        return $this->collAutorisations;
    }

    /**
     * Sets a collection of ChildAutorisation objects related by a one-to-many relationship
     * to the current object.
     * It will also schedule objects for deletion based on a diff between old objects (aka persisted)
     * and new objects from the given Propel collection.
     *
     * @param      Collection $autorisations A Propel collection.
     * @param      ConnectionInterface $con Optional connection object
     * @return $this|ChildIndividu The current object (for fluent API support)
     */
    public function setAutorisations(Collection $autorisations, ConnectionInterface $con = null)
    {
        /** @var ChildAutorisation[] $autorisationsToDelete */
        $autorisationsToDelete = $this->getAutorisations(new Criteria(), $con)->diff($autorisations);


        $this->autorisationsScheduledForDeletion = $autorisationsToDelete;

        foreach ($autorisationsToDelete as $autorisationRemoved) {
            $autorisationRemoved->setIndividu(null);
        }

        $this->collAutorisations = null;
        foreach ($autorisations as $autorisation) {
            $this->addAutorisation($autorisation);
        }

        $this->collAutorisations = $autorisations;
        $this->collAutorisationsPartial = false;

        return $this;
    }

    /**
     * Returns the number of related Autorisation objects.
     *
     * @param      Criteria $criteria
     * @param      boolean $distinct
     * @param      ConnectionInterface $con
     * @return int             Count of related Autorisation objects.
     * @throws PropelException
     */
    public function countAutorisations(Criteria $criteria = null, $distinct = false, ConnectionInterface $con = null)
    {
        $partial = $this->collAutorisationsPartial && !$this->isNew();
        if (null === $this->collAutorisations || null !== $criteria || $partial) {
            if ($this->isNew() && null === $this->collAutorisations) {
                return 0;
            }

            if ($partial && !$criteria) {
                return count($this->getAutorisations());
            }

            $query = ChildAutorisationQuery::create(null, $criteria);
            if ($distinct) {
                $query->distinct();
            }

            return $query
                ->filterByIndividu($this)
                ->count($con);
        }

        return count($this->collAutorisations);
    }

    /**
     * Method called to associate a ChildAutorisation object to this object
     * through the ChildAutorisation foreign key attribute.
     *
     * @param  ChildAutorisation $l ChildAutorisation
     * @return $this|\Individu The current object (for fluent API support)
     */
    public function addAutorisation(ChildAutorisation $l)
    {
        if ($this->collAutorisations === null) {
            $this->initAutorisations();
            $this->collAutorisationsPartial = true;
        }

        if (!$this->collAutorisations->contains($l)) {
            $this->doAddAutorisation($l);

            if ($this->autorisationsScheduledForDeletion and $this->autorisationsScheduledForDeletion->contains($l)) {
                $this->autorisationsScheduledForDeletion->remove($this->autorisationsScheduledForDeletion->search($l));
            }
        }

        return $this;
    }

    /**
     * @param ChildAutorisation $autorisation The ChildAutorisation object to add.
     */
    protected function doAddAutorisation(ChildAutorisation $autorisation)
    {
        $this->collAutorisations[]= $autorisation;
        $autorisation->setIndividu($this);
    }

    /**
     * @param  ChildAutorisation $autorisation The ChildAutorisation object to remove.
     * @return $this|ChildIndividu The current object (for fluent API support)
     */
    public function removeAutorisation(ChildAutorisation $autorisation)
    {
        if ($this->getAutorisations()->contains($autorisation)) {
            $pos = $this->collAutorisations->search($autorisation);
            $this->collAutorisations->remove($pos);
            if (null === $this->autorisationsScheduledForDeletion) {
                $this->autorisationsScheduledForDeletion = clone $this->collAutorisations;
                $this->autorisationsScheduledForDeletion->clear();
            }
            $this->autorisationsScheduledForDeletion[]= clone $autorisation;
            $autorisation->setIndividu(null);
        }

        return $this;
    }


    /**
     * If this collection has already been initialized with
     * an identical criteria, it returns the collection.
     * Otherwise if this Individu is new, it will return
     * an empty collection; or if this Individu has previously
     * been saved, it will retrieve related Autorisations from storage.
     *
     * This method is protected by default in order to keep the public
     * api reasonable.  You can provide public methods for those you
     * actually need in Individu.
     *
     * @param      Criteria $criteria optional Criteria object to narrow the query
     * @param      ConnectionInterface $con optional connection object
     * @param      string $joinBehavior optional join type to use (defaults to Criteria::LEFT_JOIN)
     * @return ObjectCollection|ChildAutorisation[] List of ChildAutorisation objects
     */
    public function getAutorisationsJoinEntite(Criteria $criteria = null, ConnectionInterface $con = null, $joinBehavior = Criteria::LEFT_JOIN)
    {
        $query = ChildAutorisationQuery::create(null, $criteria);
        $query->joinWith('Entite', $joinBehavior);

        return $this->getAutorisations($query, $con);
    }

    /**
     * Clears out the collLogs collection
     *
     * This does not modify the database; however, it will remove any associated objects, causing
     * them to be refetched by subsequent calls to accessor method.
     *
     * @return void
     * @see        addLogs()
     */
    public function clearLogs()
    {
        $this->collLogs = null; // important to set this to NULL since that means it is uninitialized
    }

    /**
     * Reset is the collLogs collection loaded partially.
     */
    public function resetPartialLogs($v = true)
    {
        $this->collLogsPartial = $v;
    }

    /**
     * Initializes the collLogs collection.
     *
     * By default this just sets the collLogs collection to an empty array (like clearcollLogs());
     * however, you may wish to override this method in your stub class to provide setting appropriate
     * to your application -- for example, setting the initial array to the values stored in database.
     *
     * @param      boolean $overrideExisting If set to true, the method call initializes
     *                                        the collection even if it is not empty
     *
     * @return void
     */
    public function initLogs($overrideExisting = true)
    {
        if (null !== $this->collLogs && !$overrideExisting) {
            return;
        }

        $collectionClassName = LogTableMap::getTableMap()->getCollectionClassName();

        $this->collLogs = new $collectionClassName;
        $this->collLogs->setModel('\Log');
    }

    /**
     * Gets an array of ChildLog objects which contain a foreign key that references this object.
     *
     * If the $criteria is not null, it is used to always fetch the results from the database.
     * Otherwise the results are fetched from the database the first time, then cached.
     * Next time the same method is called without $criteria, the cached collection is returned.
     * If this ChildIndividu is new, it will return
     * an empty collection or the current collection; the criteria is ignored on a new object.
     *
     * @param      Criteria $criteria optional Criteria object to narrow the query
     * @param      ConnectionInterface $con optional connection object
     * @return ObjectCollection|ChildLog[] List of ChildLog objects
     * @throws PropelException
     */
    public function getLogs(Criteria $criteria = null, ConnectionInterface $con = null)
    {
        $partial = $this->collLogsPartial && !$this->isNew();
        if (null === $this->collLogs || null !== $criteria  || $partial) {
            if ($this->isNew() && null === $this->collLogs) {
                // return empty collection
                $this->initLogs();
            } else {
                $collLogs = ChildLogQuery::create(null, $criteria)
                    ->filterByIndividu($this)
                    ->find($con);

                if (null !== $criteria) {
                    if (false !== $this->collLogsPartial && count($collLogs)) {
                        $this->initLogs(false);

                        foreach ($collLogs as $obj) {
                            if (false == $this->collLogs->contains($obj)) {
                                $this->collLogs->append($obj);
                            }
                        }

                        $this->collLogsPartial = true;
                    }

                    return $collLogs;
                }

                if ($partial && $this->collLogs) {
                    foreach ($this->collLogs as $obj) {
                        if ($obj->isNew()) {
                            $collLogs[] = $obj;
                        }
                    }
                }

                $this->collLogs = $collLogs;
                $this->collLogsPartial = false;
            }
        }

        return $this->collLogs;
    }

    /**
     * Sets a collection of ChildLog objects related by a one-to-many relationship
     * to the current object.
     * It will also schedule objects for deletion based on a diff between old objects (aka persisted)
     * and new objects from the given Propel collection.
     *
     * @param      Collection $logs A Propel collection.
     * @param      ConnectionInterface $con Optional connection object
     * @return $this|ChildIndividu The current object (for fluent API support)
     */
    public function setLogs(Collection $logs, ConnectionInterface $con = null)
    {
        /** @var ChildLog[] $logsToDelete */
        $logsToDelete = $this->getLogs(new Criteria(), $con)->diff($logs);


        $this->logsScheduledForDeletion = $logsToDelete;

        foreach ($logsToDelete as $logRemoved) {
            $logRemoved->setIndividu(null);
        }

        $this->collLogs = null;
        foreach ($logs as $log) {
            $this->addLog($log);
        }

        $this->collLogs = $logs;
        $this->collLogsPartial = false;

        return $this;
    }

    /**
     * Returns the number of related Log objects.
     *
     * @param      Criteria $criteria
     * @param      boolean $distinct
     * @param      ConnectionInterface $con
     * @return int             Count of related Log objects.
     * @throws PropelException
     */
    public function countLogs(Criteria $criteria = null, $distinct = false, ConnectionInterface $con = null)
    {
        $partial = $this->collLogsPartial && !$this->isNew();
        if (null === $this->collLogs || null !== $criteria || $partial) {
            if ($this->isNew() && null === $this->collLogs) {
                return 0;
            }

            if ($partial && !$criteria) {
                return count($this->getLogs());
            }

            $query = ChildLogQuery::create(null, $criteria);
            if ($distinct) {
                $query->distinct();
            }

            return $query
                ->filterByIndividu($this)
                ->count($con);
        }

        return count($this->collLogs);
    }

    /**
     * Method called to associate a ChildLog object to this object
     * through the ChildLog foreign key attribute.
     *
     * @param  ChildLog $l ChildLog
     * @return $this|\Individu The current object (for fluent API support)
     */
    public function addLog(ChildLog $l)
    {
        if ($this->collLogs === null) {
            $this->initLogs();
            $this->collLogsPartial = true;
        }

        if (!$this->collLogs->contains($l)) {
            $this->doAddLog($l);

            if ($this->logsScheduledForDeletion and $this->logsScheduledForDeletion->contains($l)) {
                $this->logsScheduledForDeletion->remove($this->logsScheduledForDeletion->search($l));
            }
        }

        return $this;
    }

    /**
     * @param ChildLog $log The ChildLog object to add.
     */
    protected function doAddLog(ChildLog $log)
    {
        $this->collLogs[]= $log;
        $log->setIndividu($this);
    }

    /**
     * @param  ChildLog $log The ChildLog object to remove.
     * @return $this|ChildIndividu The current object (for fluent API support)
     */
    public function removeLog(ChildLog $log)
    {
        if ($this->getLogs()->contains($log)) {
            $pos = $this->collLogs->search($log);
            $this->collLogs->remove($pos);
            if (null === $this->logsScheduledForDeletion) {
                $this->logsScheduledForDeletion = clone $this->collLogs;
                $this->logsScheduledForDeletion->clear();
            }
            $this->logsScheduledForDeletion[]= clone $log;
            $log->setIndividu(null);
        }

        return $this;
    }

    /**
     * Clears out the collMembres0s collection
     *
     * This does not modify the database; however, it will remove any associated objects, causing
     * them to be refetched by subsequent calls to accessor method.
     *
     * @return void
     * @see        addMembres0s()
     */
    public function clearMembres0s()
    {
        $this->collMembres0s = null; // important to set this to NULL since that means it is uninitialized
    }

    /**
     * Reset is the collMembres0s collection loaded partially.
     */
    public function resetPartialMembres0s($v = true)
    {
        $this->collMembres0sPartial = $v;
    }

    /**
     * Initializes the collMembres0s collection.
     *
     * By default this just sets the collMembres0s collection to an empty array (like clearcollMembres0s());
     * however, you may wish to override this method in your stub class to provide setting appropriate
     * to your application -- for example, setting the initial array to the values stored in database.
     *
     * @param      boolean $overrideExisting If set to true, the method call initializes
     *                                        the collection even if it is not empty
     *
     * @return void
     */
    public function initMembres0s($overrideExisting = true)
    {
        if (null !== $this->collMembres0s && !$overrideExisting) {
            return;
        }

        $collectionClassName = MembreTableMap::getTableMap()->getCollectionClassName();

        $this->collMembres0s = new $collectionClassName;
        $this->collMembres0s->setModel('\Membre');
    }

    /**
     * Gets an array of ChildMembre objects which contain a foreign key that references this object.
     *
     * If the $criteria is not null, it is used to always fetch the results from the database.
     * Otherwise the results are fetched from the database the first time, then cached.
     * Next time the same method is called without $criteria, the cached collection is returned.
     * If this ChildIndividu is new, it will return
     * an empty collection or the current collection; the criteria is ignored on a new object.
     *
     * @param      Criteria $criteria optional Criteria object to narrow the query
     * @param      ConnectionInterface $con optional connection object
     * @return ObjectCollection|ChildMembre[] List of ChildMembre objects
     * @throws PropelException
     */
    public function getMembres0s(Criteria $criteria = null, ConnectionInterface $con = null)
    {
        $partial = $this->collMembres0sPartial && !$this->isNew();
        if (null === $this->collMembres0s || null !== $criteria  || $partial) {
            if ($this->isNew() && null === $this->collMembres0s) {
                // return empty collection
                $this->initMembres0s();
            } else {
                $collMembres0s = ChildMembreQuery::create(null, $criteria)
                    ->filterByIndividuTitulaire($this)
                    ->find($con);

                if (null !== $criteria) {
                    if (false !== $this->collMembres0sPartial && count($collMembres0s)) {
                        $this->initMembres0s(false);

                        foreach ($collMembres0s as $obj) {
                            if (false == $this->collMembres0s->contains($obj)) {
                                $this->collMembres0s->append($obj);
                            }
                        }

                        $this->collMembres0sPartial = true;
                    }

                    return $collMembres0s;
                }

                if ($partial && $this->collMembres0s) {
                    foreach ($this->collMembres0s as $obj) {
                        if ($obj->isNew()) {
                            $collMembres0s[] = $obj;
                        }
                    }
                }

                $this->collMembres0s = $collMembres0s;
                $this->collMembres0sPartial = false;
            }
        }

        return $this->collMembres0s;
    }

    /**
     * Sets a collection of ChildMembre objects related by a one-to-many relationship
     * to the current object.
     * It will also schedule objects for deletion based on a diff between old objects (aka persisted)
     * and new objects from the given Propel collection.
     *
     * @param      Collection $membres0s A Propel collection.
     * @param      ConnectionInterface $con Optional connection object
     * @return $this|ChildIndividu The current object (for fluent API support)
     */
    public function setMembres0s(Collection $membres0s, ConnectionInterface $con = null)
    {
        /** @var ChildMembre[] $membres0sToDelete */
        $membres0sToDelete = $this->getMembres0s(new Criteria(), $con)->diff($membres0s);


        $this->membres0sScheduledForDeletion = $membres0sToDelete;

        foreach ($membres0sToDelete as $membres0Removed) {
            $membres0Removed->setIndividuTitulaire(null);
        }

        $this->collMembres0s = null;
        foreach ($membres0s as $membres0) {
            $this->addMembres0($membres0);
        }

        $this->collMembres0s = $membres0s;
        $this->collMembres0sPartial = false;

        return $this;
    }

    /**
     * Returns the number of related Membre objects.
     *
     * @param      Criteria $criteria
     * @param      boolean $distinct
     * @param      ConnectionInterface $con
     * @return int             Count of related Membre objects.
     * @throws PropelException
     */
    public function countMembres0s(Criteria $criteria = null, $distinct = false, ConnectionInterface $con = null)
    {
        $partial = $this->collMembres0sPartial && !$this->isNew();
        if (null === $this->collMembres0s || null !== $criteria || $partial) {
            if ($this->isNew() && null === $this->collMembres0s) {
                return 0;
            }

            if ($partial && !$criteria) {
                return count($this->getMembres0s());
            }

            $query = ChildMembreQuery::create(null, $criteria);
            if ($distinct) {
                $query->distinct();
            }

            return $query
                ->filterByIndividuTitulaire($this)
                ->count($con);
        }

        return count($this->collMembres0s);
    }

    /**
     * Method called to associate a ChildMembre object to this object
     * through the ChildMembre foreign key attribute.
     *
     * @param  ChildMembre $l ChildMembre
     * @return $this|\Individu The current object (for fluent API support)
     */
    public function addMembres0(ChildMembre $l)
    {
        if ($this->collMembres0s === null) {
            $this->initMembres0s();
            $this->collMembres0sPartial = true;
        }

        if (!$this->collMembres0s->contains($l)) {
            $this->doAddMembres0($l);

            if ($this->membres0sScheduledForDeletion and $this->membres0sScheduledForDeletion->contains($l)) {
                $this->membres0sScheduledForDeletion->remove($this->membres0sScheduledForDeletion->search($l));
            }
        }

        return $this;
    }

    /**
     * @param ChildMembre $membres0 The ChildMembre object to add.
     */
    protected function doAddMembres0(ChildMembre $membres0)
    {
        $this->collMembres0s[]= $membres0;
        $membres0->setIndividuTitulaire($this);
    }

    /**
     * @param  ChildMembre $membres0 The ChildMembre object to remove.
     * @return $this|ChildIndividu The current object (for fluent API support)
     */
    public function removeMembres0(ChildMembre $membres0)
    {
        if ($this->getMembres0s()->contains($membres0)) {
            $pos = $this->collMembres0s->search($membres0);
            $this->collMembres0s->remove($pos);
            if (null === $this->membres0sScheduledForDeletion) {
                $this->membres0sScheduledForDeletion = clone $this->collMembres0s;
                $this->membres0sScheduledForDeletion->clear();
            }
            $this->membres0sScheduledForDeletion[]= clone $membres0;
            $membres0->setIndividuTitulaire(null);
        }

        return $this;
    }

    /**
     * Clears out the collMembreIndividus collection
     *
     * This does not modify the database; however, it will remove any associated objects, causing
     * them to be refetched by subsequent calls to accessor method.
     *
     * @return void
     * @see        addMembreIndividus()
     */
    public function clearMembreIndividus()
    {
        $this->collMembreIndividus = null; // important to set this to NULL since that means it is uninitialized
    }

    /**
     * Reset is the collMembreIndividus collection loaded partially.
     */
    public function resetPartialMembreIndividus($v = true)
    {
        $this->collMembreIndividusPartial = $v;
    }

    /**
     * Initializes the collMembreIndividus collection.
     *
     * By default this just sets the collMembreIndividus collection to an empty array (like clearcollMembreIndividus());
     * however, you may wish to override this method in your stub class to provide setting appropriate
     * to your application -- for example, setting the initial array to the values stored in database.
     *
     * @param      boolean $overrideExisting If set to true, the method call initializes
     *                                        the collection even if it is not empty
     *
     * @return void
     */
    public function initMembreIndividus($overrideExisting = true)
    {
        if (null !== $this->collMembreIndividus && !$overrideExisting) {
            return;
        }

        $collectionClassName = MembreIndividuTableMap::getTableMap()->getCollectionClassName();

        $this->collMembreIndividus = new $collectionClassName;
        $this->collMembreIndividus->setModel('\MembreIndividu');
    }

    /**
     * Gets an array of ChildMembreIndividu objects which contain a foreign key that references this object.
     *
     * If the $criteria is not null, it is used to always fetch the results from the database.
     * Otherwise the results are fetched from the database the first time, then cached.
     * Next time the same method is called without $criteria, the cached collection is returned.
     * If this ChildIndividu is new, it will return
     * an empty collection or the current collection; the criteria is ignored on a new object.
     *
     * @param      Criteria $criteria optional Criteria object to narrow the query
     * @param      ConnectionInterface $con optional connection object
     * @return ObjectCollection|ChildMembreIndividu[] List of ChildMembreIndividu objects
     * @throws PropelException
     */
    public function getMembreIndividus(Criteria $criteria = null, ConnectionInterface $con = null)
    {
        $partial = $this->collMembreIndividusPartial && !$this->isNew();
        if (null === $this->collMembreIndividus || null !== $criteria  || $partial) {
            if ($this->isNew() && null === $this->collMembreIndividus) {
                // return empty collection
                $this->initMembreIndividus();
            } else {
                $collMembreIndividus = ChildMembreIndividuQuery::create(null, $criteria)
                    ->filterByIndividu($this)
                    ->find($con);

                if (null !== $criteria) {
                    if (false !== $this->collMembreIndividusPartial && count($collMembreIndividus)) {
                        $this->initMembreIndividus(false);

                        foreach ($collMembreIndividus as $obj) {
                            if (false == $this->collMembreIndividus->contains($obj)) {
                                $this->collMembreIndividus->append($obj);
                            }
                        }

                        $this->collMembreIndividusPartial = true;
                    }

                    return $collMembreIndividus;
                }

                if ($partial && $this->collMembreIndividus) {
                    foreach ($this->collMembreIndividus as $obj) {
                        if ($obj->isNew()) {
                            $collMembreIndividus[] = $obj;
                        }
                    }
                }

                $this->collMembreIndividus = $collMembreIndividus;
                $this->collMembreIndividusPartial = false;
            }
        }

        return $this->collMembreIndividus;
    }

    /**
     * Sets a collection of ChildMembreIndividu objects related by a one-to-many relationship
     * to the current object.
     * It will also schedule objects for deletion based on a diff between old objects (aka persisted)
     * and new objects from the given Propel collection.
     *
     * @param      Collection $membreIndividus A Propel collection.
     * @param      ConnectionInterface $con Optional connection object
     * @return $this|ChildIndividu The current object (for fluent API support)
     */
    public function setMembreIndividus(Collection $membreIndividus, ConnectionInterface $con = null)
    {
        /** @var ChildMembreIndividu[] $membreIndividusToDelete */
        $membreIndividusToDelete = $this->getMembreIndividus(new Criteria(), $con)->diff($membreIndividus);


        //since at least one column in the foreign key is at the same time a PK
        //we can not just set a PK to NULL in the lines below. We have to store
        //a backup of all values, so we are able to manipulate these items based on the onDelete value later.
        $this->membreIndividusScheduledForDeletion = clone $membreIndividusToDelete;

        foreach ($membreIndividusToDelete as $membreIndividuRemoved) {
            $membreIndividuRemoved->setIndividu(null);
        }

        $this->collMembreIndividus = null;
        foreach ($membreIndividus as $membreIndividu) {
            $this->addMembreIndividu($membreIndividu);
        }

        $this->collMembreIndividus = $membreIndividus;
        $this->collMembreIndividusPartial = false;

        return $this;
    }

    /**
     * Returns the number of related MembreIndividu objects.
     *
     * @param      Criteria $criteria
     * @param      boolean $distinct
     * @param      ConnectionInterface $con
     * @return int             Count of related MembreIndividu objects.
     * @throws PropelException
     */
    public function countMembreIndividus(Criteria $criteria = null, $distinct = false, ConnectionInterface $con = null)
    {
        $partial = $this->collMembreIndividusPartial && !$this->isNew();
        if (null === $this->collMembreIndividus || null !== $criteria || $partial) {
            if ($this->isNew() && null === $this->collMembreIndividus) {
                return 0;
            }

            if ($partial && !$criteria) {
                return count($this->getMembreIndividus());
            }

            $query = ChildMembreIndividuQuery::create(null, $criteria);
            if ($distinct) {
                $query->distinct();
            }

            return $query
                ->filterByIndividu($this)
                ->count($con);
        }

        return count($this->collMembreIndividus);
    }

    /**
     * Method called to associate a ChildMembreIndividu object to this object
     * through the ChildMembreIndividu foreign key attribute.
     *
     * @param  ChildMembreIndividu $l ChildMembreIndividu
     * @return $this|\Individu The current object (for fluent API support)
     */
    public function addMembreIndividu(ChildMembreIndividu $l)
    {
        if ($this->collMembreIndividus === null) {
            $this->initMembreIndividus();
            $this->collMembreIndividusPartial = true;
        }

        if (!$this->collMembreIndividus->contains($l)) {
            $this->doAddMembreIndividu($l);

            if ($this->membreIndividusScheduledForDeletion and $this->membreIndividusScheduledForDeletion->contains($l)) {
                $this->membreIndividusScheduledForDeletion->remove($this->membreIndividusScheduledForDeletion->search($l));
            }
        }

        return $this;
    }

    /**
     * @param ChildMembreIndividu $membreIndividu The ChildMembreIndividu object to add.
     */
    protected function doAddMembreIndividu(ChildMembreIndividu $membreIndividu)
    {
        $this->collMembreIndividus[]= $membreIndividu;
        $membreIndividu->setIndividu($this);
    }

    /**
     * @param  ChildMembreIndividu $membreIndividu The ChildMembreIndividu object to remove.
     * @return $this|ChildIndividu The current object (for fluent API support)
     */
    public function removeMembreIndividu(ChildMembreIndividu $membreIndividu)
    {
        if ($this->getMembreIndividus()->contains($membreIndividu)) {
            $pos = $this->collMembreIndividus->search($membreIndividu);
            $this->collMembreIndividus->remove($pos);
            if (null === $this->membreIndividusScheduledForDeletion) {
                $this->membreIndividusScheduledForDeletion = clone $this->collMembreIndividus;
                $this->membreIndividusScheduledForDeletion->clear();
            }
            $this->membreIndividusScheduledForDeletion[]= clone $membreIndividu;
            $membreIndividu->setIndividu(null);
        }

        return $this;
    }


    /**
     * If this collection has already been initialized with
     * an identical criteria, it returns the collection.
     * Otherwise if this Individu is new, it will return
     * an empty collection; or if this Individu has previously
     * been saved, it will retrieve related MembreIndividus from storage.
     *
     * This method is protected by default in order to keep the public
     * api reasonable.  You can provide public methods for those you
     * actually need in Individu.
     *
     * @param      Criteria $criteria optional Criteria object to narrow the query
     * @param      ConnectionInterface $con optional connection object
     * @param      string $joinBehavior optional join type to use (defaults to Criteria::LEFT_JOIN)
     * @return ObjectCollection|ChildMembreIndividu[] List of ChildMembreIndividu objects
     */
    public function getMembreIndividusJoinMembre(Criteria $criteria = null, ConnectionInterface $con = null, $joinBehavior = Criteria::LEFT_JOIN)
    {
        $query = ChildMembreIndividuQuery::create(null, $criteria);
        $query->joinWith('Membre', $joinBehavior);

        return $this->getMembreIndividus($query, $con);
    }

    /**
     * Clears out the collServicerenduIndividus collection
     *
     * This does not modify the database; however, it will remove any associated objects, causing
     * them to be refetched by subsequent calls to accessor method.
     *
     * @return void
     * @see        addServicerenduIndividus()
     */
    public function clearServicerenduIndividus()
    {
        $this->collServicerenduIndividus = null; // important to set this to NULL since that means it is uninitialized
    }

    /**
     * Reset is the collServicerenduIndividus collection loaded partially.
     */
    public function resetPartialServicerenduIndividus($v = true)
    {
        $this->collServicerenduIndividusPartial = $v;
    }

    /**
     * Initializes the collServicerenduIndividus collection.
     *
     * By default this just sets the collServicerenduIndividus collection to an empty array (like clearcollServicerenduIndividus());
     * however, you may wish to override this method in your stub class to provide setting appropriate
     * to your application -- for example, setting the initial array to the values stored in database.
     *
     * @param      boolean $overrideExisting If set to true, the method call initializes
     *                                        the collection even if it is not empty
     *
     * @return void
     */
    public function initServicerenduIndividus($overrideExisting = true)
    {
        if (null !== $this->collServicerenduIndividus && !$overrideExisting) {
            return;
        }

        $collectionClassName = ServicerenduIndividuTableMap::getTableMap()->getCollectionClassName();

        $this->collServicerenduIndividus = new $collectionClassName;
        $this->collServicerenduIndividus->setModel('\ServicerenduIndividu');
    }

    /**
     * Gets an array of ChildServicerenduIndividu objects which contain a foreign key that references this object.
     *
     * If the $criteria is not null, it is used to always fetch the results from the database.
     * Otherwise the results are fetched from the database the first time, then cached.
     * Next time the same method is called without $criteria, the cached collection is returned.
     * If this ChildIndividu is new, it will return
     * an empty collection or the current collection; the criteria is ignored on a new object.
     *
     * @param      Criteria $criteria optional Criteria object to narrow the query
     * @param      ConnectionInterface $con optional connection object
     * @return ObjectCollection|ChildServicerenduIndividu[] List of ChildServicerenduIndividu objects
     * @throws PropelException
     */
    public function getServicerenduIndividus(Criteria $criteria = null, ConnectionInterface $con = null)
    {
        $partial = $this->collServicerenduIndividusPartial && !$this->isNew();
        if (null === $this->collServicerenduIndividus || null !== $criteria  || $partial) {
            if ($this->isNew() && null === $this->collServicerenduIndividus) {
                // return empty collection
                $this->initServicerenduIndividus();
            } else {
                $collServicerenduIndividus = ChildServicerenduIndividuQuery::create(null, $criteria)
                    ->filterByIndividu($this)
                    ->find($con);

                if (null !== $criteria) {
                    if (false !== $this->collServicerenduIndividusPartial && count($collServicerenduIndividus)) {
                        $this->initServicerenduIndividus(false);

                        foreach ($collServicerenduIndividus as $obj) {
                            if (false == $this->collServicerenduIndividus->contains($obj)) {
                                $this->collServicerenduIndividus->append($obj);
                            }
                        }

                        $this->collServicerenduIndividusPartial = true;
                    }

                    return $collServicerenduIndividus;
                }

                if ($partial && $this->collServicerenduIndividus) {
                    foreach ($this->collServicerenduIndividus as $obj) {
                        if ($obj->isNew()) {
                            $collServicerenduIndividus[] = $obj;
                        }
                    }
                }

                $this->collServicerenduIndividus = $collServicerenduIndividus;
                $this->collServicerenduIndividusPartial = false;
            }
        }

        return $this->collServicerenduIndividus;
    }

    /**
     * Sets a collection of ChildServicerenduIndividu objects related by a one-to-many relationship
     * to the current object.
     * It will also schedule objects for deletion based on a diff between old objects (aka persisted)
     * and new objects from the given Propel collection.
     *
     * @param      Collection $servicerenduIndividus A Propel collection.
     * @param      ConnectionInterface $con Optional connection object
     * @return $this|ChildIndividu The current object (for fluent API support)
     */
    public function setServicerenduIndividus(Collection $servicerenduIndividus, ConnectionInterface $con = null)
    {
        /** @var ChildServicerenduIndividu[] $servicerenduIndividusToDelete */
        $servicerenduIndividusToDelete = $this->getServicerenduIndividus(new Criteria(), $con)->diff($servicerenduIndividus);


        //since at least one column in the foreign key is at the same time a PK
        //we can not just set a PK to NULL in the lines below. We have to store
        //a backup of all values, so we are able to manipulate these items based on the onDelete value later.
        $this->servicerenduIndividusScheduledForDeletion = clone $servicerenduIndividusToDelete;

        foreach ($servicerenduIndividusToDelete as $servicerenduIndividuRemoved) {
            $servicerenduIndividuRemoved->setIndividu(null);
        }

        $this->collServicerenduIndividus = null;
        foreach ($servicerenduIndividus as $servicerenduIndividu) {
            $this->addServicerenduIndividu($servicerenduIndividu);
        }

        $this->collServicerenduIndividus = $servicerenduIndividus;
        $this->collServicerenduIndividusPartial = false;

        return $this;
    }

    /**
     * Returns the number of related ServicerenduIndividu objects.
     *
     * @param      Criteria $criteria
     * @param      boolean $distinct
     * @param      ConnectionInterface $con
     * @return int             Count of related ServicerenduIndividu objects.
     * @throws PropelException
     */
    public function countServicerenduIndividus(Criteria $criteria = null, $distinct = false, ConnectionInterface $con = null)
    {
        $partial = $this->collServicerenduIndividusPartial && !$this->isNew();
        if (null === $this->collServicerenduIndividus || null !== $criteria || $partial) {
            if ($this->isNew() && null === $this->collServicerenduIndividus) {
                return 0;
            }

            if ($partial && !$criteria) {
                return count($this->getServicerenduIndividus());
            }

            $query = ChildServicerenduIndividuQuery::create(null, $criteria);
            if ($distinct) {
                $query->distinct();
            }

            return $query
                ->filterByIndividu($this)
                ->count($con);
        }

        return count($this->collServicerenduIndividus);
    }

    /**
     * Method called to associate a ChildServicerenduIndividu object to this object
     * through the ChildServicerenduIndividu foreign key attribute.
     *
     * @param  ChildServicerenduIndividu $l ChildServicerenduIndividu
     * @return $this|\Individu The current object (for fluent API support)
     */
    public function addServicerenduIndividu(ChildServicerenduIndividu $l)
    {
        if ($this->collServicerenduIndividus === null) {
            $this->initServicerenduIndividus();
            $this->collServicerenduIndividusPartial = true;
        }

        if (!$this->collServicerenduIndividus->contains($l)) {
            $this->doAddServicerenduIndividu($l);

            if ($this->servicerenduIndividusScheduledForDeletion and $this->servicerenduIndividusScheduledForDeletion->contains($l)) {
                $this->servicerenduIndividusScheduledForDeletion->remove($this->servicerenduIndividusScheduledForDeletion->search($l));
            }
        }

        return $this;
    }

    /**
     * @param ChildServicerenduIndividu $servicerenduIndividu The ChildServicerenduIndividu object to add.
     */
    protected function doAddServicerenduIndividu(ChildServicerenduIndividu $servicerenduIndividu)
    {
        $this->collServicerenduIndividus[]= $servicerenduIndividu;
        $servicerenduIndividu->setIndividu($this);
    }

    /**
     * @param  ChildServicerenduIndividu $servicerenduIndividu The ChildServicerenduIndividu object to remove.
     * @return $this|ChildIndividu The current object (for fluent API support)
     */
    public function removeServicerenduIndividu(ChildServicerenduIndividu $servicerenduIndividu)
    {
        if ($this->getServicerenduIndividus()->contains($servicerenduIndividu)) {
            $pos = $this->collServicerenduIndividus->search($servicerenduIndividu);
            $this->collServicerenduIndividus->remove($pos);
            if (null === $this->servicerenduIndividusScheduledForDeletion) {
                $this->servicerenduIndividusScheduledForDeletion = clone $this->collServicerenduIndividus;
                $this->servicerenduIndividusScheduledForDeletion->clear();
            }
            $this->servicerenduIndividusScheduledForDeletion[]= clone $servicerenduIndividu;
            $servicerenduIndividu->setIndividu(null);
        }

        return $this;
    }


    /**
     * If this collection has already been initialized with
     * an identical criteria, it returns the collection.
     * Otherwise if this Individu is new, it will return
     * an empty collection; or if this Individu has previously
     * been saved, it will retrieve related ServicerenduIndividus from storage.
     *
     * This method is protected by default in order to keep the public
     * api reasonable.  You can provide public methods for those you
     * actually need in Individu.
     *
     * @param      Criteria $criteria optional Criteria object to narrow the query
     * @param      ConnectionInterface $con optional connection object
     * @param      string $joinBehavior optional join type to use (defaults to Criteria::LEFT_JOIN)
     * @return ObjectCollection|ChildServicerenduIndividu[] List of ChildServicerenduIndividu objects
     */
    public function getServicerenduIndividusJoinServicerendu(Criteria $criteria = null, ConnectionInterface $con = null, $joinBehavior = Criteria::LEFT_JOIN)
    {
        $query = ChildServicerenduIndividuQuery::create(null, $criteria);
        $query->joinWith('Servicerendu', $joinBehavior);

        return $this->getServicerenduIndividus($query, $con);
    }

    /**
     * Clears out the collNotificationIndividus collection
     *
     * This does not modify the database; however, it will remove any associated objects, causing
     * them to be refetched by subsequent calls to accessor method.
     *
     * @return void
     * @see        addNotificationIndividus()
     */
    public function clearNotificationIndividus()
    {
        $this->collNotificationIndividus = null; // important to set this to NULL since that means it is uninitialized
    }

    /**
     * Reset is the collNotificationIndividus collection loaded partially.
     */
    public function resetPartialNotificationIndividus($v = true)
    {
        $this->collNotificationIndividusPartial = $v;
    }

    /**
     * Initializes the collNotificationIndividus collection.
     *
     * By default this just sets the collNotificationIndividus collection to an empty array (like clearcollNotificationIndividus());
     * however, you may wish to override this method in your stub class to provide setting appropriate
     * to your application -- for example, setting the initial array to the values stored in database.
     *
     * @param      boolean $overrideExisting If set to true, the method call initializes
     *                                        the collection even if it is not empty
     *
     * @return void
     */
    public function initNotificationIndividus($overrideExisting = true)
    {
        if (null !== $this->collNotificationIndividus && !$overrideExisting) {
            return;
        }

        $collectionClassName = NotificationIndividuTableMap::getTableMap()->getCollectionClassName();

        $this->collNotificationIndividus = new $collectionClassName;
        $this->collNotificationIndividus->setModel('\NotificationIndividu');
    }

    /**
     * Gets an array of ChildNotificationIndividu objects which contain a foreign key that references this object.
     *
     * If the $criteria is not null, it is used to always fetch the results from the database.
     * Otherwise the results are fetched from the database the first time, then cached.
     * Next time the same method is called without $criteria, the cached collection is returned.
     * If this ChildIndividu is new, it will return
     * an empty collection or the current collection; the criteria is ignored on a new object.
     *
     * @param      Criteria $criteria optional Criteria object to narrow the query
     * @param      ConnectionInterface $con optional connection object
     * @return ObjectCollection|ChildNotificationIndividu[] List of ChildNotificationIndividu objects
     * @throws PropelException
     */
    public function getNotificationIndividus(Criteria $criteria = null, ConnectionInterface $con = null)
    {
        $partial = $this->collNotificationIndividusPartial && !$this->isNew();
        if (null === $this->collNotificationIndividus || null !== $criteria  || $partial) {
            if ($this->isNew() && null === $this->collNotificationIndividus) {
                // return empty collection
                $this->initNotificationIndividus();
            } else {
                $collNotificationIndividus = ChildNotificationIndividuQuery::create(null, $criteria)
                    ->filterByIndividu($this)
                    ->find($con);

                if (null !== $criteria) {
                    if (false !== $this->collNotificationIndividusPartial && count($collNotificationIndividus)) {
                        $this->initNotificationIndividus(false);

                        foreach ($collNotificationIndividus as $obj) {
                            if (false == $this->collNotificationIndividus->contains($obj)) {
                                $this->collNotificationIndividus->append($obj);
                            }
                        }

                        $this->collNotificationIndividusPartial = true;
                    }

                    return $collNotificationIndividus;
                }

                if ($partial && $this->collNotificationIndividus) {
                    foreach ($this->collNotificationIndividus as $obj) {
                        if ($obj->isNew()) {
                            $collNotificationIndividus[] = $obj;
                        }
                    }
                }

                $this->collNotificationIndividus = $collNotificationIndividus;
                $this->collNotificationIndividusPartial = false;
            }
        }

        return $this->collNotificationIndividus;
    }

    /**
     * Sets a collection of ChildNotificationIndividu objects related by a one-to-many relationship
     * to the current object.
     * It will also schedule objects for deletion based on a diff between old objects (aka persisted)
     * and new objects from the given Propel collection.
     *
     * @param      Collection $notificationIndividus A Propel collection.
     * @param      ConnectionInterface $con Optional connection object
     * @return $this|ChildIndividu The current object (for fluent API support)
     */
    public function setNotificationIndividus(Collection $notificationIndividus, ConnectionInterface $con = null)
    {
        /** @var ChildNotificationIndividu[] $notificationIndividusToDelete */
        $notificationIndividusToDelete = $this->getNotificationIndividus(new Criteria(), $con)->diff($notificationIndividus);


        //since at least one column in the foreign key is at the same time a PK
        //we can not just set a PK to NULL in the lines below. We have to store
        //a backup of all values, so we are able to manipulate these items based on the onDelete value later.
        $this->notificationIndividusScheduledForDeletion = clone $notificationIndividusToDelete;

        foreach ($notificationIndividusToDelete as $notificationIndividuRemoved) {
            $notificationIndividuRemoved->setIndividu(null);
        }

        $this->collNotificationIndividus = null;
        foreach ($notificationIndividus as $notificationIndividu) {
            $this->addNotificationIndividu($notificationIndividu);
        }

        $this->collNotificationIndividus = $notificationIndividus;
        $this->collNotificationIndividusPartial = false;

        return $this;
    }

    /**
     * Returns the number of related NotificationIndividu objects.
     *
     * @param      Criteria $criteria
     * @param      boolean $distinct
     * @param      ConnectionInterface $con
     * @return int             Count of related NotificationIndividu objects.
     * @throws PropelException
     */
    public function countNotificationIndividus(Criteria $criteria = null, $distinct = false, ConnectionInterface $con = null)
    {
        $partial = $this->collNotificationIndividusPartial && !$this->isNew();
        if (null === $this->collNotificationIndividus || null !== $criteria || $partial) {
            if ($this->isNew() && null === $this->collNotificationIndividus) {
                return 0;
            }

            if ($partial && !$criteria) {
                return count($this->getNotificationIndividus());
            }

            $query = ChildNotificationIndividuQuery::create(null, $criteria);
            if ($distinct) {
                $query->distinct();
            }

            return $query
                ->filterByIndividu($this)
                ->count($con);
        }

        return count($this->collNotificationIndividus);
    }

    /**
     * Method called to associate a ChildNotificationIndividu object to this object
     * through the ChildNotificationIndividu foreign key attribute.
     *
     * @param  ChildNotificationIndividu $l ChildNotificationIndividu
     * @return $this|\Individu The current object (for fluent API support)
     */
    public function addNotificationIndividu(ChildNotificationIndividu $l)
    {
        if ($this->collNotificationIndividus === null) {
            $this->initNotificationIndividus();
            $this->collNotificationIndividusPartial = true;
        }

        if (!$this->collNotificationIndividus->contains($l)) {
            $this->doAddNotificationIndividu($l);

            if ($this->notificationIndividusScheduledForDeletion and $this->notificationIndividusScheduledForDeletion->contains($l)) {
                $this->notificationIndividusScheduledForDeletion->remove($this->notificationIndividusScheduledForDeletion->search($l));
            }
        }

        return $this;
    }

    /**
     * @param ChildNotificationIndividu $notificationIndividu The ChildNotificationIndividu object to add.
     */
    protected function doAddNotificationIndividu(ChildNotificationIndividu $notificationIndividu)
    {
        $this->collNotificationIndividus[]= $notificationIndividu;
        $notificationIndividu->setIndividu($this);
    }

    /**
     * @param  ChildNotificationIndividu $notificationIndividu The ChildNotificationIndividu object to remove.
     * @return $this|ChildIndividu The current object (for fluent API support)
     */
    public function removeNotificationIndividu(ChildNotificationIndividu $notificationIndividu)
    {
        if ($this->getNotificationIndividus()->contains($notificationIndividu)) {
            $pos = $this->collNotificationIndividus->search($notificationIndividu);
            $this->collNotificationIndividus->remove($pos);
            if (null === $this->notificationIndividusScheduledForDeletion) {
                $this->notificationIndividusScheduledForDeletion = clone $this->collNotificationIndividus;
                $this->notificationIndividusScheduledForDeletion->clear();
            }
            $this->notificationIndividusScheduledForDeletion[]= clone $notificationIndividu;
            $notificationIndividu->setIndividu(null);
        }

        return $this;
    }


    /**
     * If this collection has already been initialized with
     * an identical criteria, it returns the collection.
     * Otherwise if this Individu is new, it will return
     * an empty collection; or if this Individu has previously
     * been saved, it will retrieve related NotificationIndividus from storage.
     *
     * This method is protected by default in order to keep the public
     * api reasonable.  You can provide public methods for those you
     * actually need in Individu.
     *
     * @param      Criteria $criteria optional Criteria object to narrow the query
     * @param      ConnectionInterface $con optional connection object
     * @param      string $joinBehavior optional join type to use (defaults to Criteria::LEFT_JOIN)
     * @return ObjectCollection|ChildNotificationIndividu[] List of ChildNotificationIndividu objects
     */
    public function getNotificationIndividusJoinNotification(Criteria $criteria = null, ConnectionInterface $con = null, $joinBehavior = Criteria::LEFT_JOIN)
    {
        $query = ChildNotificationIndividuQuery::create(null, $criteria);
        $query->joinWith('Notification', $joinBehavior);

        return $this->getNotificationIndividus($query, $con);
    }

    /**
     * Clears out the collCourrierLiens collection
     *
     * This does not modify the database; however, it will remove any associated objects, causing
     * them to be refetched by subsequent calls to accessor method.
     *
     * @return void
     * @see        addCourrierLiens()
     */
    public function clearCourrierLiens()
    {
        $this->collCourrierLiens = null; // important to set this to NULL since that means it is uninitialized
    }

    /**
     * Reset is the collCourrierLiens collection loaded partially.
     */
    public function resetPartialCourrierLiens($v = true)
    {
        $this->collCourrierLiensPartial = $v;
    }

    /**
     * Initializes the collCourrierLiens collection.
     *
     * By default this just sets the collCourrierLiens collection to an empty array (like clearcollCourrierLiens());
     * however, you may wish to override this method in your stub class to provide setting appropriate
     * to your application -- for example, setting the initial array to the values stored in database.
     *
     * @param      boolean $overrideExisting If set to true, the method call initializes
     *                                        the collection even if it is not empty
     *
     * @return void
     */
    public function initCourrierLiens($overrideExisting = true)
    {
        if (null !== $this->collCourrierLiens && !$overrideExisting) {
            return;
        }

        $collectionClassName = CourrierLienTableMap::getTableMap()->getCollectionClassName();

        $this->collCourrierLiens = new $collectionClassName;
        $this->collCourrierLiens->setModel('\CourrierLien');
    }

    /**
     * Gets an array of ChildCourrierLien objects which contain a foreign key that references this object.
     *
     * If the $criteria is not null, it is used to always fetch the results from the database.
     * Otherwise the results are fetched from the database the first time, then cached.
     * Next time the same method is called without $criteria, the cached collection is returned.
     * If this ChildIndividu is new, it will return
     * an empty collection or the current collection; the criteria is ignored on a new object.
     *
     * @param      Criteria $criteria optional Criteria object to narrow the query
     * @param      ConnectionInterface $con optional connection object
     * @return ObjectCollection|ChildCourrierLien[] List of ChildCourrierLien objects
     * @throws PropelException
     */
    public function getCourrierLiens(Criteria $criteria = null, ConnectionInterface $con = null)
    {
        $partial = $this->collCourrierLiensPartial && !$this->isNew();
        if (null === $this->collCourrierLiens || null !== $criteria  || $partial) {
            if ($this->isNew() && null === $this->collCourrierLiens) {
                // return empty collection
                $this->initCourrierLiens();
            } else {
                $collCourrierLiens = ChildCourrierLienQuery::create(null, $criteria)
                    ->filterByIndividu($this)
                    ->find($con);

                if (null !== $criteria) {
                    if (false !== $this->collCourrierLiensPartial && count($collCourrierLiens)) {
                        $this->initCourrierLiens(false);

                        foreach ($collCourrierLiens as $obj) {
                            if (false == $this->collCourrierLiens->contains($obj)) {
                                $this->collCourrierLiens->append($obj);
                            }
                        }

                        $this->collCourrierLiensPartial = true;
                    }

                    return $collCourrierLiens;
                }

                if ($partial && $this->collCourrierLiens) {
                    foreach ($this->collCourrierLiens as $obj) {
                        if ($obj->isNew()) {
                            $collCourrierLiens[] = $obj;
                        }
                    }
                }

                $this->collCourrierLiens = $collCourrierLiens;
                $this->collCourrierLiensPartial = false;
            }
        }

        return $this->collCourrierLiens;
    }

    /**
     * Sets a collection of ChildCourrierLien objects related by a one-to-many relationship
     * to the current object.
     * It will also schedule objects for deletion based on a diff between old objects (aka persisted)
     * and new objects from the given Propel collection.
     *
     * @param      Collection $courrierLiens A Propel collection.
     * @param      ConnectionInterface $con Optional connection object
     * @return $this|ChildIndividu The current object (for fluent API support)
     */
    public function setCourrierLiens(Collection $courrierLiens, ConnectionInterface $con = null)
    {
        /** @var ChildCourrierLien[] $courrierLiensToDelete */
        $courrierLiensToDelete = $this->getCourrierLiens(new Criteria(), $con)->diff($courrierLiens);


        $this->courrierLiensScheduledForDeletion = $courrierLiensToDelete;

        foreach ($courrierLiensToDelete as $courrierLienRemoved) {
            $courrierLienRemoved->setIndividu(null);
        }

        $this->collCourrierLiens = null;
        foreach ($courrierLiens as $courrierLien) {
            $this->addCourrierLien($courrierLien);
        }

        $this->collCourrierLiens = $courrierLiens;
        $this->collCourrierLiensPartial = false;

        return $this;
    }

    /**
     * Returns the number of related CourrierLien objects.
     *
     * @param      Criteria $criteria
     * @param      boolean $distinct
     * @param      ConnectionInterface $con
     * @return int             Count of related CourrierLien objects.
     * @throws PropelException
     */
    public function countCourrierLiens(Criteria $criteria = null, $distinct = false, ConnectionInterface $con = null)
    {
        $partial = $this->collCourrierLiensPartial && !$this->isNew();
        if (null === $this->collCourrierLiens || null !== $criteria || $partial) {
            if ($this->isNew() && null === $this->collCourrierLiens) {
                return 0;
            }

            if ($partial && !$criteria) {
                return count($this->getCourrierLiens());
            }

            $query = ChildCourrierLienQuery::create(null, $criteria);
            if ($distinct) {
                $query->distinct();
            }

            return $query
                ->filterByIndividu($this)
                ->count($con);
        }

        return count($this->collCourrierLiens);
    }

    /**
     * Method called to associate a ChildCourrierLien object to this object
     * through the ChildCourrierLien foreign key attribute.
     *
     * @param  ChildCourrierLien $l ChildCourrierLien
     * @return $this|\Individu The current object (for fluent API support)
     */
    public function addCourrierLien(ChildCourrierLien $l)
    {
        if ($this->collCourrierLiens === null) {
            $this->initCourrierLiens();
            $this->collCourrierLiensPartial = true;
        }

        if (!$this->collCourrierLiens->contains($l)) {
            $this->doAddCourrierLien($l);

            if ($this->courrierLiensScheduledForDeletion and $this->courrierLiensScheduledForDeletion->contains($l)) {
                $this->courrierLiensScheduledForDeletion->remove($this->courrierLiensScheduledForDeletion->search($l));
            }
        }

        return $this;
    }

    /**
     * @param ChildCourrierLien $courrierLien The ChildCourrierLien object to add.
     */
    protected function doAddCourrierLien(ChildCourrierLien $courrierLien)
    {
        $this->collCourrierLiens[]= $courrierLien;
        $courrierLien->setIndividu($this);
    }

    /**
     * @param  ChildCourrierLien $courrierLien The ChildCourrierLien object to remove.
     * @return $this|ChildIndividu The current object (for fluent API support)
     */
    public function removeCourrierLien(ChildCourrierLien $courrierLien)
    {
        if ($this->getCourrierLiens()->contains($courrierLien)) {
            $pos = $this->collCourrierLiens->search($courrierLien);
            $this->collCourrierLiens->remove($pos);
            if (null === $this->courrierLiensScheduledForDeletion) {
                $this->courrierLiensScheduledForDeletion = clone $this->collCourrierLiens;
                $this->courrierLiensScheduledForDeletion->clear();
            }
            $this->courrierLiensScheduledForDeletion[]= clone $courrierLien;
            $courrierLien->setIndividu(null);
        }

        return $this;
    }


    /**
     * If this collection has already been initialized with
     * an identical criteria, it returns the collection.
     * Otherwise if this Individu is new, it will return
     * an empty collection; or if this Individu has previously
     * been saved, it will retrieve related CourrierLiens from storage.
     *
     * This method is protected by default in order to keep the public
     * api reasonable.  You can provide public methods for those you
     * actually need in Individu.
     *
     * @param      Criteria $criteria optional Criteria object to narrow the query
     * @param      ConnectionInterface $con optional connection object
     * @param      string $joinBehavior optional join type to use (defaults to Criteria::LEFT_JOIN)
     * @return ObjectCollection|ChildCourrierLien[] List of ChildCourrierLien objects
     */
    public function getCourrierLiensJoinMembre(Criteria $criteria = null, ConnectionInterface $con = null, $joinBehavior = Criteria::LEFT_JOIN)
    {
        $query = ChildCourrierLienQuery::create(null, $criteria);
        $query->joinWith('Membre', $joinBehavior);

        return $this->getCourrierLiens($query, $con);
    }


    /**
     * If this collection has already been initialized with
     * an identical criteria, it returns the collection.
     * Otherwise if this Individu is new, it will return
     * an empty collection; or if this Individu has previously
     * been saved, it will retrieve related CourrierLiens from storage.
     *
     * This method is protected by default in order to keep the public
     * api reasonable.  You can provide public methods for those you
     * actually need in Individu.
     *
     * @param      Criteria $criteria optional Criteria object to narrow the query
     * @param      ConnectionInterface $con optional connection object
     * @param      string $joinBehavior optional join type to use (defaults to Criteria::LEFT_JOIN)
     * @return ObjectCollection|ChildCourrierLien[] List of ChildCourrierLien objects
     */
    public function getCourrierLiensJoinCourrier(Criteria $criteria = null, ConnectionInterface $con = null, $joinBehavior = Criteria::LEFT_JOIN)
    {
        $query = ChildCourrierLienQuery::create(null, $criteria);
        $query->joinWith('Courrier', $joinBehavior);

        return $this->getCourrierLiens($query, $con);
    }

    /**
     * Clears out the collMembres collection
     *
     * This does not modify the database; however, it will remove any associated objects, causing
     * them to be refetched by subsequent calls to accessor method.
     *
     * @return void
     * @see        addMembres()
     */
    public function clearMembres()
    {
        $this->collMembres = null; // important to set this to NULL since that means it is uninitialized
    }

    /**
     * Initializes the collMembres crossRef collection.
     *
     * By default this just sets the collMembres collection to an empty collection (like clearMembres());
     * however, you may wish to override this method in your stub class to provide setting appropriate
     * to your application -- for example, setting the initial array to the values stored in database.
     *
     * @return void
     */
    public function initMembres()
    {
        $collectionClassName = MembreIndividuTableMap::getTableMap()->getCollectionClassName();

        $this->collMembres = new $collectionClassName;
        $this->collMembresPartial = true;
        $this->collMembres->setModel('\Membre');
    }

    /**
     * Checks if the collMembres collection is loaded.
     *
     * @return bool
     */
    public function isMembresLoaded()
    {
        return null !== $this->collMembres;
    }

    /**
     * Gets a collection of ChildMembre objects related by a many-to-many relationship
     * to the current object by way of the asso_membres_individus cross-reference table.
     *
     * If the $criteria is not null, it is used to always fetch the results from the database.
     * Otherwise the results are fetched from the database the first time, then cached.
     * Next time the same method is called without $criteria, the cached collection is returned.
     * If this ChildIndividu is new, it will return
     * an empty collection or the current collection; the criteria is ignored on a new object.
     *
     * @param      Criteria $criteria Optional query object to filter the query
     * @param      ConnectionInterface $con Optional connection object
     *
     * @return ObjectCollection|ChildMembre[] List of ChildMembre objects
     */
    public function getMembres(Criteria $criteria = null, ConnectionInterface $con = null)
    {
        $partial = $this->collMembresPartial && !$this->isNew();
        if (null === $this->collMembres || null !== $criteria || $partial) {
            if ($this->isNew()) {
                // return empty collection
                if (null === $this->collMembres) {
                    $this->initMembres();
                }
            } else {

                $query = ChildMembreQuery::create(null, $criteria)
                    ->filterByIndividu($this);
                $collMembres = $query->find($con);
                if (null !== $criteria) {
                    return $collMembres;
                }

                if ($partial && $this->collMembres) {
                    //make sure that already added objects gets added to the list of the database.
                    foreach ($this->collMembres as $obj) {
                        if (!$collMembres->contains($obj)) {
                            $collMembres[] = $obj;
                        }
                    }
                }

                $this->collMembres = $collMembres;
                $this->collMembresPartial = false;
            }
        }

        return $this->collMembres;
    }

    /**
     * Sets a collection of Membre objects related by a many-to-many relationship
     * to the current object by way of the asso_membres_individus cross-reference table.
     * It will also schedule objects for deletion based on a diff between old objects (aka persisted)
     * and new objects from the given Propel collection.
     *
     * @param  Collection $membres A Propel collection.
     * @param  ConnectionInterface $con Optional connection object
     * @return $this|ChildIndividu The current object (for fluent API support)
     */
    public function setMembres(Collection $membres, ConnectionInterface $con = null)
    {
        $this->clearMembres();
        $currentMembres = $this->getMembres();

        $membresScheduledForDeletion = $currentMembres->diff($membres);

        foreach ($membresScheduledForDeletion as $toDelete) {
            $this->removeMembre($toDelete);
        }

        foreach ($membres as $membre) {
            if (!$currentMembres->contains($membre)) {
                $this->doAddMembre($membre);
            }
        }

        $this->collMembresPartial = false;
        $this->collMembres = $membres;

        return $this;
    }

    /**
     * Gets the number of Membre objects related by a many-to-many relationship
     * to the current object by way of the asso_membres_individus cross-reference table.
     *
     * @param      Criteria $criteria Optional query object to filter the query
     * @param      boolean $distinct Set to true to force count distinct
     * @param      ConnectionInterface $con Optional connection object
     *
     * @return int the number of related Membre objects
     */
    public function countMembres(Criteria $criteria = null, $distinct = false, ConnectionInterface $con = null)
    {
        $partial = $this->collMembresPartial && !$this->isNew();
        if (null === $this->collMembres || null !== $criteria || $partial) {
            if ($this->isNew() && null === $this->collMembres) {
                return 0;
            } else {

                if ($partial && !$criteria) {
                    return count($this->getMembres());
                }

                $query = ChildMembreQuery::create(null, $criteria);
                if ($distinct) {
                    $query->distinct();
                }

                return $query
                    ->filterByIndividu($this)
                    ->count($con);
            }
        } else {
            return count($this->collMembres);
        }
    }

    /**
     * Associate a ChildMembre to this object
     * through the asso_membres_individus cross reference table.
     *
     * @param ChildMembre $membre
     * @return ChildIndividu The current object (for fluent API support)
     */
    public function addMembre(ChildMembre $membre)
    {
        if ($this->collMembres === null) {
            $this->initMembres();
        }

        if (!$this->getMembres()->contains($membre)) {
            // only add it if the **same** object is not already associated
            $this->collMembres->push($membre);
            $this->doAddMembre($membre);
        }

        return $this;
    }

    /**
     *
     * @param ChildMembre $membre
     */
    protected function doAddMembre(ChildMembre $membre)
    {
        $membreIndividu = new ChildMembreIndividu();

        $membreIndividu->setMembre($membre);

        $membreIndividu->setIndividu($this);

        $this->addMembreIndividu($membreIndividu);

        // set the back reference to this object directly as using provided method either results
        // in endless loop or in multiple relations
        if (!$membre->isIndividusLoaded()) {
            $membre->initIndividus();
            $membre->getIndividus()->push($this);
        } elseif (!$membre->getIndividus()->contains($this)) {
            $membre->getIndividus()->push($this);
        }

    }

    /**
     * Remove membre of this object
     * through the asso_membres_individus cross reference table.
     *
     * @param ChildMembre $membre
     * @return ChildIndividu The current object (for fluent API support)
     */
    public function removeMembre(ChildMembre $membre)
    {
        if ($this->getMembres()->contains($membre)) {
            $membreIndividu = new ChildMembreIndividu();
            $membreIndividu->setMembre($membre);
            if ($membre->isIndividusLoaded()) {
                //remove the back reference if available
                $membre->getIndividus()->removeObject($this);
            }

            $membreIndividu->setIndividu($this);
            $this->removeMembreIndividu(clone $membreIndividu);
            $membreIndividu->clear();

            $this->collMembres->remove($this->collMembres->search($membre));

            if (null === $this->membresScheduledForDeletion) {
                $this->membresScheduledForDeletion = clone $this->collMembres;
                $this->membresScheduledForDeletion->clear();
            }

            $this->membresScheduledForDeletion->push($membre);
        }


        return $this;
    }

    /**
     * Clears out the collServicerendus collection
     *
     * This does not modify the database; however, it will remove any associated objects, causing
     * them to be refetched by subsequent calls to accessor method.
     *
     * @return void
     * @see        addServicerendus()
     */
    public function clearServicerendus()
    {
        $this->collServicerendus = null; // important to set this to NULL since that means it is uninitialized
    }

    /**
     * Initializes the collServicerendus crossRef collection.
     *
     * By default this just sets the collServicerendus collection to an empty collection (like clearServicerendus());
     * however, you may wish to override this method in your stub class to provide setting appropriate
     * to your application -- for example, setting the initial array to the values stored in database.
     *
     * @return void
     */
    public function initServicerendus()
    {
        $collectionClassName = ServicerenduIndividuTableMap::getTableMap()->getCollectionClassName();

        $this->collServicerendus = new $collectionClassName;
        $this->collServicerendusPartial = true;
        $this->collServicerendus->setModel('\Servicerendu');
    }

    /**
     * Checks if the collServicerendus collection is loaded.
     *
     * @return bool
     */
    public function isServicerendusLoaded()
    {
        return null !== $this->collServicerendus;
    }

    /**
     * Gets a collection of ChildServicerendu objects related by a many-to-many relationship
     * to the current object by way of the asso_servicerendus_individus cross-reference table.
     *
     * If the $criteria is not null, it is used to always fetch the results from the database.
     * Otherwise the results are fetched from the database the first time, then cached.
     * Next time the same method is called without $criteria, the cached collection is returned.
     * If this ChildIndividu is new, it will return
     * an empty collection or the current collection; the criteria is ignored on a new object.
     *
     * @param      Criteria $criteria Optional query object to filter the query
     * @param      ConnectionInterface $con Optional connection object
     *
     * @return ObjectCollection|ChildServicerendu[] List of ChildServicerendu objects
     */
    public function getServicerendus(Criteria $criteria = null, ConnectionInterface $con = null)
    {
        $partial = $this->collServicerendusPartial && !$this->isNew();
        if (null === $this->collServicerendus || null !== $criteria || $partial) {
            if ($this->isNew()) {
                // return empty collection
                if (null === $this->collServicerendus) {
                    $this->initServicerendus();
                }
            } else {

                $query = ChildServicerenduQuery::create(null, $criteria)
                    ->filterByIndividu($this);
                $collServicerendus = $query->find($con);
                if (null !== $criteria) {
                    return $collServicerendus;
                }

                if ($partial && $this->collServicerendus) {
                    //make sure that already added objects gets added to the list of the database.
                    foreach ($this->collServicerendus as $obj) {
                        if (!$collServicerendus->contains($obj)) {
                            $collServicerendus[] = $obj;
                        }
                    }
                }

                $this->collServicerendus = $collServicerendus;
                $this->collServicerendusPartial = false;
            }
        }

        return $this->collServicerendus;
    }

    /**
     * Sets a collection of Servicerendu objects related by a many-to-many relationship
     * to the current object by way of the asso_servicerendus_individus cross-reference table.
     * It will also schedule objects for deletion based on a diff between old objects (aka persisted)
     * and new objects from the given Propel collection.
     *
     * @param  Collection $servicerendus A Propel collection.
     * @param  ConnectionInterface $con Optional connection object
     * @return $this|ChildIndividu The current object (for fluent API support)
     */
    public function setServicerendus(Collection $servicerendus, ConnectionInterface $con = null)
    {
        $this->clearServicerendus();
        $currentServicerendus = $this->getServicerendus();

        $servicerendusScheduledForDeletion = $currentServicerendus->diff($servicerendus);

        foreach ($servicerendusScheduledForDeletion as $toDelete) {
            $this->removeServicerendu($toDelete);
        }

        foreach ($servicerendus as $servicerendu) {
            if (!$currentServicerendus->contains($servicerendu)) {
                $this->doAddServicerendu($servicerendu);
            }
        }

        $this->collServicerendusPartial = false;
        $this->collServicerendus = $servicerendus;

        return $this;
    }

    /**
     * Gets the number of Servicerendu objects related by a many-to-many relationship
     * to the current object by way of the asso_servicerendus_individus cross-reference table.
     *
     * @param      Criteria $criteria Optional query object to filter the query
     * @param      boolean $distinct Set to true to force count distinct
     * @param      ConnectionInterface $con Optional connection object
     *
     * @return int the number of related Servicerendu objects
     */
    public function countServicerendus(Criteria $criteria = null, $distinct = false, ConnectionInterface $con = null)
    {
        $partial = $this->collServicerendusPartial && !$this->isNew();
        if (null === $this->collServicerendus || null !== $criteria || $partial) {
            if ($this->isNew() && null === $this->collServicerendus) {
                return 0;
            } else {

                if ($partial && !$criteria) {
                    return count($this->getServicerendus());
                }

                $query = ChildServicerenduQuery::create(null, $criteria);
                if ($distinct) {
                    $query->distinct();
                }

                return $query
                    ->filterByIndividu($this)
                    ->count($con);
            }
        } else {
            return count($this->collServicerendus);
        }
    }

    /**
     * Associate a ChildServicerendu to this object
     * through the asso_servicerendus_individus cross reference table.
     *
     * @param ChildServicerendu $servicerendu
     * @return ChildIndividu The current object (for fluent API support)
     */
    public function addServicerendu(ChildServicerendu $servicerendu)
    {
        if ($this->collServicerendus === null) {
            $this->initServicerendus();
        }

        if (!$this->getServicerendus()->contains($servicerendu)) {
            // only add it if the **same** object is not already associated
            $this->collServicerendus->push($servicerendu);
            $this->doAddServicerendu($servicerendu);
        }

        return $this;
    }

    /**
     *
     * @param ChildServicerendu $servicerendu
     */
    protected function doAddServicerendu(ChildServicerendu $servicerendu)
    {
        $servicerenduIndividu = new ChildServicerenduIndividu();

        $servicerenduIndividu->setServicerendu($servicerendu);

        $servicerenduIndividu->setIndividu($this);

        $this->addServicerenduIndividu($servicerenduIndividu);

        // set the back reference to this object directly as using provided method either results
        // in endless loop or in multiple relations
        if (!$servicerendu->isIndividusLoaded()) {
            $servicerendu->initIndividus();
            $servicerendu->getIndividus()->push($this);
        } elseif (!$servicerendu->getIndividus()->contains($this)) {
            $servicerendu->getIndividus()->push($this);
        }

    }

    /**
     * Remove servicerendu of this object
     * through the asso_servicerendus_individus cross reference table.
     *
     * @param ChildServicerendu $servicerendu
     * @return ChildIndividu The current object (for fluent API support)
     */
    public function removeServicerendu(ChildServicerendu $servicerendu)
    {
        if ($this->getServicerendus()->contains($servicerendu)) {
            $servicerenduIndividu = new ChildServicerenduIndividu();
            $servicerenduIndividu->setServicerendu($servicerendu);
            if ($servicerendu->isIndividusLoaded()) {
                //remove the back reference if available
                $servicerendu->getIndividus()->removeObject($this);
            }

            $servicerenduIndividu->setIndividu($this);
            $this->removeServicerenduIndividu(clone $servicerenduIndividu);
            $servicerenduIndividu->clear();

            $this->collServicerendus->remove($this->collServicerendus->search($servicerendu));

            if (null === $this->servicerendusScheduledForDeletion) {
                $this->servicerendusScheduledForDeletion = clone $this->collServicerendus;
                $this->servicerendusScheduledForDeletion->clear();
            }

            $this->servicerendusScheduledForDeletion->push($servicerendu);
        }


        return $this;
    }

    /**
     * Clears out the collNotifications collection
     *
     * This does not modify the database; however, it will remove any associated objects, causing
     * them to be refetched by subsequent calls to accessor method.
     *
     * @return void
     * @see        addNotifications()
     */
    public function clearNotifications()
    {
        $this->collNotifications = null; // important to set this to NULL since that means it is uninitialized
    }

    /**
     * Initializes the collNotifications crossRef collection.
     *
     * By default this just sets the collNotifications collection to an empty collection (like clearNotifications());
     * however, you may wish to override this method in your stub class to provide setting appropriate
     * to your application -- for example, setting the initial array to the values stored in database.
     *
     * @return void
     */
    public function initNotifications()
    {
        $collectionClassName = NotificationIndividuTableMap::getTableMap()->getCollectionClassName();

        $this->collNotifications = new $collectionClassName;
        $this->collNotificationsPartial = true;
        $this->collNotifications->setModel('\Notification');
    }

    /**
     * Checks if the collNotifications collection is loaded.
     *
     * @return bool
     */
    public function isNotificationsLoaded()
    {
        return null !== $this->collNotifications;
    }

    /**
     * Gets a collection of ChildNotification objects related by a many-to-many relationship
     * to the current object by way of the asso_notifications_individus cross-reference table.
     *
     * If the $criteria is not null, it is used to always fetch the results from the database.
     * Otherwise the results are fetched from the database the first time, then cached.
     * Next time the same method is called without $criteria, the cached collection is returned.
     * If this ChildIndividu is new, it will return
     * an empty collection or the current collection; the criteria is ignored on a new object.
     *
     * @param      Criteria $criteria Optional query object to filter the query
     * @param      ConnectionInterface $con Optional connection object
     *
     * @return ObjectCollection|ChildNotification[] List of ChildNotification objects
     */
    public function getNotifications(Criteria $criteria = null, ConnectionInterface $con = null)
    {
        $partial = $this->collNotificationsPartial && !$this->isNew();
        if (null === $this->collNotifications || null !== $criteria || $partial) {
            if ($this->isNew()) {
                // return empty collection
                if (null === $this->collNotifications) {
                    $this->initNotifications();
                }
            } else {

                $query = ChildNotificationQuery::create(null, $criteria)
                    ->filterByIndividu($this);
                $collNotifications = $query->find($con);
                if (null !== $criteria) {
                    return $collNotifications;
                }

                if ($partial && $this->collNotifications) {
                    //make sure that already added objects gets added to the list of the database.
                    foreach ($this->collNotifications as $obj) {
                        if (!$collNotifications->contains($obj)) {
                            $collNotifications[] = $obj;
                        }
                    }
                }

                $this->collNotifications = $collNotifications;
                $this->collNotificationsPartial = false;
            }
        }

        return $this->collNotifications;
    }

    /**
     * Sets a collection of Notification objects related by a many-to-many relationship
     * to the current object by way of the asso_notifications_individus cross-reference table.
     * It will also schedule objects for deletion based on a diff between old objects (aka persisted)
     * and new objects from the given Propel collection.
     *
     * @param  Collection $notifications A Propel collection.
     * @param  ConnectionInterface $con Optional connection object
     * @return $this|ChildIndividu The current object (for fluent API support)
     */
    public function setNotifications(Collection $notifications, ConnectionInterface $con = null)
    {
        $this->clearNotifications();
        $currentNotifications = $this->getNotifications();

        $notificationsScheduledForDeletion = $currentNotifications->diff($notifications);

        foreach ($notificationsScheduledForDeletion as $toDelete) {
            $this->removeNotification($toDelete);
        }

        foreach ($notifications as $notification) {
            if (!$currentNotifications->contains($notification)) {
                $this->doAddNotification($notification);
            }
        }

        $this->collNotificationsPartial = false;
        $this->collNotifications = $notifications;

        return $this;
    }

    /**
     * Gets the number of Notification objects related by a many-to-many relationship
     * to the current object by way of the asso_notifications_individus cross-reference table.
     *
     * @param      Criteria $criteria Optional query object to filter the query
     * @param      boolean $distinct Set to true to force count distinct
     * @param      ConnectionInterface $con Optional connection object
     *
     * @return int the number of related Notification objects
     */
    public function countNotifications(Criteria $criteria = null, $distinct = false, ConnectionInterface $con = null)
    {
        $partial = $this->collNotificationsPartial && !$this->isNew();
        if (null === $this->collNotifications || null !== $criteria || $partial) {
            if ($this->isNew() && null === $this->collNotifications) {
                return 0;
            } else {

                if ($partial && !$criteria) {
                    return count($this->getNotifications());
                }

                $query = ChildNotificationQuery::create(null, $criteria);
                if ($distinct) {
                    $query->distinct();
                }

                return $query
                    ->filterByIndividu($this)
                    ->count($con);
            }
        } else {
            return count($this->collNotifications);
        }
    }

    /**
     * Associate a ChildNotification to this object
     * through the asso_notifications_individus cross reference table.
     *
     * @param ChildNotification $notification
     * @return ChildIndividu The current object (for fluent API support)
     */
    public function addNotification(ChildNotification $notification)
    {
        if ($this->collNotifications === null) {
            $this->initNotifications();
        }

        if (!$this->getNotifications()->contains($notification)) {
            // only add it if the **same** object is not already associated
            $this->collNotifications->push($notification);
            $this->doAddNotification($notification);
        }

        return $this;
    }

    /**
     *
     * @param ChildNotification $notification
     */
    protected function doAddNotification(ChildNotification $notification)
    {
        $notificationIndividu = new ChildNotificationIndividu();

        $notificationIndividu->setNotification($notification);

        $notificationIndividu->setIndividu($this);

        $this->addNotificationIndividu($notificationIndividu);

        // set the back reference to this object directly as using provided method either results
        // in endless loop or in multiple relations
        if (!$notification->isIndividusLoaded()) {
            $notification->initIndividus();
            $notification->getIndividus()->push($this);
        } elseif (!$notification->getIndividus()->contains($this)) {
            $notification->getIndividus()->push($this);
        }

    }

    /**
     * Remove notification of this object
     * through the asso_notifications_individus cross reference table.
     *
     * @param ChildNotification $notification
     * @return ChildIndividu The current object (for fluent API support)
     */
    public function removeNotification(ChildNotification $notification)
    {
        if ($this->getNotifications()->contains($notification)) {
            $notificationIndividu = new ChildNotificationIndividu();
            $notificationIndividu->setNotification($notification);
            if ($notification->isIndividusLoaded()) {
                //remove the back reference if available
                $notification->getIndividus()->removeObject($this);
            }

            $notificationIndividu->setIndividu($this);
            $this->removeNotificationIndividu(clone $notificationIndividu);
            $notificationIndividu->clear();

            $this->collNotifications->remove($this->collNotifications->search($notification));

            if (null === $this->notificationsScheduledForDeletion) {
                $this->notificationsScheduledForDeletion = clone $this->collNotifications;
                $this->notificationsScheduledForDeletion->clear();
            }

            $this->notificationsScheduledForDeletion->push($notification);
        }


        return $this;
    }

    /**
     * Clears the current object, sets all attributes to their default values and removes
     * outgoing references as well as back-references (from other objects to this one. Results probably in a database
     * change of those foreign objects when you call `save` there).
     */
    public function clear()
    {
        $this->id_individu = null;
        $this->nom = null;
        $this->bio = null;
        $this->email = null;
        $this->login = null;
        $this->pass = null;
        $this->alea_actuel = null;
        $this->alea_futur = null;
        $this->token = null;
        $this->token_time = null;
        $this->civilite = null;
        $this->sexe = null;
        $this->nom_famille = null;
        $this->prenom = null;
        $this->naissance = null;
        $this->adresse = null;
        $this->codepostal = null;
        $this->ville = null;
        $this->pays = null;
        $this->telephone = null;
        $this->telephone_pro = null;
        $this->fax = null;
        $this->mobile = null;
        $this->url = null;
        $this->profession = null;
        $this->contact_souhait = null;
        $this->observation = null;
        $this->created_at = null;
        $this->updated_at = null;
        $this->alreadyInSave = false;
        $this->clearAllReferences();
        $this->applyDefaultValues();
        $this->resetModified();
        $this->setNew(true);
        $this->setDeleted(false);
    }

    /**
     * Resets all references and back-references to other model objects or collections of model objects.
     *
     * This method is used to reset all php object references (not the actual reference in the database).
     * Necessary for object serialisation.
     *
     * @param      boolean $deep Whether to also clear the references on all referrer objects.
     */
    public function clearAllReferences($deep = false)
    {
        if ($deep) {
            if ($this->collAutorisations) {
                foreach ($this->collAutorisations as $o) {
                    $o->clearAllReferences($deep);
                }
            }
            if ($this->collLogs) {
                foreach ($this->collLogs as $o) {
                    $o->clearAllReferences($deep);
                }
            }
            if ($this->collMembres0s) {
                foreach ($this->collMembres0s as $o) {
                    $o->clearAllReferences($deep);
                }
            }
            if ($this->collMembreIndividus) {
                foreach ($this->collMembreIndividus as $o) {
                    $o->clearAllReferences($deep);
                }
            }
            if ($this->collServicerenduIndividus) {
                foreach ($this->collServicerenduIndividus as $o) {
                    $o->clearAllReferences($deep);
                }
            }
            if ($this->collNotificationIndividus) {
                foreach ($this->collNotificationIndividus as $o) {
                    $o->clearAllReferences($deep);
                }
            }
            if ($this->collCourrierLiens) {
                foreach ($this->collCourrierLiens as $o) {
                    $o->clearAllReferences($deep);
                }
            }
            if ($this->collMembres) {
                foreach ($this->collMembres as $o) {
                    $o->clearAllReferences($deep);
                }
            }
            if ($this->collServicerendus) {
                foreach ($this->collServicerendus as $o) {
                    $o->clearAllReferences($deep);
                }
            }
            if ($this->collNotifications) {
                foreach ($this->collNotifications as $o) {
                    $o->clearAllReferences($deep);
                }
            }
        } // if ($deep)

        $this->collAutorisations = null;
        $this->collLogs = null;
        $this->collMembres0s = null;
        $this->collMembreIndividus = null;
        $this->collServicerenduIndividus = null;
        $this->collNotificationIndividus = null;
        $this->collCourrierLiens = null;
        $this->collMembres = null;
        $this->collServicerendus = null;
        $this->collNotifications = null;
    }

    /**
     * Return the string representation of this object
     *
     * @return string
     */
    public function __toString()
    {
        return (string) $this->exportTo(IndividuTableMap::DEFAULT_STRING_FORMAT);
    }

    // timestampable behavior

    /**
     * Mark the current object so that the update date doesn't get updated during next save
     *
     * @return     $this|ChildIndividu The current object (for fluent API support)
     */
    public function keepUpdateDateUnchanged()
    {
        $this->modifiedColumns[IndividuTableMap::COL_UPDATED_AT] = true;

        return $this;
    }

    // archivable behavior

    /**
     * Get an archived version of the current object.
     *
     * @param ConnectionInterface $con Optional connection object
     *
     * @return     ChildAssoIndividusArchive An archive object, or null if the current object was never archived
     */
    public function getArchive(ConnectionInterface $con = null)
    {
        if ($this->isNew()) {
            return null;
        }
        $archive = ChildAssoIndividusArchiveQuery::create()
            ->filterByPrimaryKey($this->getPrimaryKey())
            ->findOne($con);

        return $archive;
    }
    /**
     * Copy the data of the current object into a $archiveTablePhpName archive object.
     * The archived object is then saved.
     * If the current object has already been archived, the archived object
     * is updated and not duplicated.
     *
     * @param ConnectionInterface $con Optional connection object
     *
     * @throws PropelException If the object is new
     *
     * @return     ChildAssoIndividusArchive The archive object based on this object
     */
    public function archive(ConnectionInterface $con = null)
    {
        if ($this->isNew()) {
            throw new PropelException('New objects cannot be archived. You must save the current object before calling archive().');
        }
        $archive = $this->getArchive($con);
        if (!$archive) {
            $archive = new ChildAssoIndividusArchive();
            $archive->setPrimaryKey($this->getPrimaryKey());
        }
        $this->copyInto($archive, $deepCopy = false, $makeNew = false);
        $archive->setArchivedAt(time());
        $archive->save($con);

        return $archive;
    }

    /**
     * Revert the the current object to the state it had when it was last archived.
     * The object must be saved afterwards if the changes must persist.
     *
     * @param ConnectionInterface $con Optional connection object
     *
     * @throws PropelException If the object has no corresponding archive.
     *
     * @return $this|ChildIndividu The current object (for fluent API support)
     */
    public function restoreFromArchive(ConnectionInterface $con = null)
    {
        $archive = $this->getArchive($con);
        if (!$archive) {
            throw new PropelException('The current object has never been archived and cannot be restored');
        }
        $this->populateFromArchive($archive);

        return $this;
    }

    /**
     * Populates the the current object based on a $archiveTablePhpName archive object.
     *
     * @param      ChildAssoIndividusArchive $archive An archived object based on the same class
      * @param      Boolean $populateAutoIncrementPrimaryKeys
     *               If true, autoincrement columns are copied from the archive object.
     *               If false, autoincrement columns are left intact.
      *
     * @return     ChildIndividu The current object (for fluent API support)
     */
    public function populateFromArchive($archive, $populateAutoIncrementPrimaryKeys = false) {
        if ($populateAutoIncrementPrimaryKeys) {
            $this->setIdIndividu($archive->getIdIndividu());
        }
        $this->setNom($archive->getNom());
        $this->setBio($archive->getBio());
        $this->setEmail($archive->getEmail());
        $this->setLogin($archive->getLogin());
        $this->setPass($archive->getPass());
        $this->setAleaActuel($archive->getAleaActuel());
        $this->setAleaFutur($archive->getAleaFutur());
        $this->setToken($archive->getToken());
        $this->setTokenTime($archive->getTokenTime());
        $this->setCivilite($archive->getCivilite());
        $this->setSexe($archive->getSexe());
        $this->setNomFamille($archive->getNomFamille());
        $this->setPrenom($archive->getPrenom());
        $this->setNaissance($archive->getNaissance());
        $this->setAdresse($archive->getAdresse());
        $this->setCodepostal($archive->getCodepostal());
        $this->setVille($archive->getVille());
        $this->setPays($archive->getPays());
        $this->setTelephone($archive->getTelephone());
        $this->setTelephonePro($archive->getTelephonePro());
        $this->setFax($archive->getFax());
        $this->setMobile($archive->getMobile());
        $this->setUrl($archive->getUrl());
        $this->setProfession($archive->getProfession());
        $this->setContactSouhait($archive->getContactSouhait());
        $this->setObservation($archive->getObservation());
        $this->setCreatedAt($archive->getCreatedAt());
        $this->setUpdatedAt($archive->getUpdatedAt());

        return $this;
    }

    /**
     * Removes the object from the database without archiving it.
     *
     * @param ConnectionInterface $con Optional connection object
     *
     * @return $this|ChildIndividu The current object (for fluent API support)
     */
    public function deleteWithoutArchive(ConnectionInterface $con = null)
    {
        $this->archiveOnDelete = false;

        return $this->delete($con);
    }

    /**
     * Code to be run before persisting the object
     * @param  ConnectionInterface $con
     * @return boolean
     */
    public function preSave(ConnectionInterface $con = null)
    {
        if (is_callable('parent::preSave')) {
            return parent::preSave($con);
        }
        return true;
    }

    /**
     * Code to be run after persisting the object
     * @param ConnectionInterface $con
     */
    public function postSave(ConnectionInterface $con = null)
    {
        if (is_callable('parent::postSave')) {
            parent::postSave($con);
        }
    }

    /**
     * Code to be run before inserting to database
     * @param  ConnectionInterface $con
     * @return boolean
     */
    public function preInsert(ConnectionInterface $con = null)
    {
        if (is_callable('parent::preInsert')) {
            return parent::preInsert($con);
        }
        return true;
    }

    /**
     * Code to be run after inserting to database
     * @param ConnectionInterface $con
     */
    public function postInsert(ConnectionInterface $con = null)
    {
        if (is_callable('parent::postInsert')) {
            parent::postInsert($con);
        }
    }

    /**
     * Code to be run before updating the object in database
     * @param  ConnectionInterface $con
     * @return boolean
     */
    public function preUpdate(ConnectionInterface $con = null)
    {
        if (is_callable('parent::preUpdate')) {
            return parent::preUpdate($con);
        }
        return true;
    }

    /**
     * Code to be run after updating the object in database
     * @param ConnectionInterface $con
     */
    public function postUpdate(ConnectionInterface $con = null)
    {
        if (is_callable('parent::postUpdate')) {
            parent::postUpdate($con);
        }
    }

    /**
     * Code to be run before deleting the object in database
     * @param  ConnectionInterface $con
     * @return boolean
     */
    public function preDelete(ConnectionInterface $con = null)
    {
        if (is_callable('parent::preDelete')) {
            return parent::preDelete($con);
        }
        return true;
    }

    /**
     * Code to be run after deleting the object in database
     * @param ConnectionInterface $con
     */
    public function postDelete(ConnectionInterface $con = null)
    {
        if (is_callable('parent::postDelete')) {
            parent::postDelete($con);
        }
    }


    /**
     * Derived method to catches calls to undefined methods.
     *
     * Provides magic import/export method support (fromXML()/toXML(), fromYAML()/toYAML(), etc.).
     * Allows to define default __call() behavior if you overwrite __call()
     *
     * @param string $name
     * @param mixed  $params
     *
     * @return array|string
     */
    public function __call($name, $params)
    {
        if (0 === strpos($name, 'get')) {
            $virtualColumn = substr($name, 3);
            if ($this->hasVirtualColumn($virtualColumn)) {
                return $this->getVirtualColumn($virtualColumn);
            }

            $virtualColumn = lcfirst($virtualColumn);
            if ($this->hasVirtualColumn($virtualColumn)) {
                return $this->getVirtualColumn($virtualColumn);
            }
        }

        if (0 === strpos($name, 'from')) {
            $format = substr($name, 4);

            return $this->importFrom($format, reset($params));
        }

        if (0 === strpos($name, 'to')) {
            $format = substr($name, 2);
            $includeLazyLoadColumns = isset($params[0]) ? $params[0] : true;

            return $this->exportTo($format, $includeLazyLoadColumns);
        }

        throw new BadMethodCallException(sprintf('Call to undefined method: %s.', $name));
    }

}
