<?php

namespace Base;

use \AssoTresorsArchive as ChildAssoTresorsArchive;
use \Tresor as ChildTresor;
use \TresorQuery as ChildTresorQuery;
use \Exception;
use \PDO;
use Map\TresorTableMap;
use Propel\Runtime\Propel;
use Propel\Runtime\ActiveQuery\Criteria;
use Propel\Runtime\ActiveQuery\ModelCriteria;
use Propel\Runtime\ActiveQuery\ModelJoin;
use Propel\Runtime\Collection\ObjectCollection;
use Propel\Runtime\Connection\ConnectionInterface;
use Propel\Runtime\Exception\PropelException;

/**
 * Base class that represents a query for the 'asso_tresors' table.
 *
 *
 *
 * @method     ChildTresorQuery orderByIdTresor($order = Criteria::ASC) Order by the id_tresor column
 * @method     ChildTresorQuery orderByIdEntite($order = Criteria::ASC) Order by the id_entite column
 * @method     ChildTresorQuery orderByNom($order = Criteria::ASC) Order by the nom column
 * @method     ChildTresorQuery orderByNomcourt($order = Criteria::ASC) Order by the nomcourt column
 * @method     ChildTresorQuery orderByIban($order = Criteria::ASC) Order by the iban column
 * @method     ChildTresorQuery orderByBic($order = Criteria::ASC) Order by the bic column
 * @method     ChildTresorQuery orderByIdCompterb($order = Criteria::ASC) Order by the id_compterb column
 * @method     ChildTresorQuery orderByIdCompte($order = Criteria::ASC) Order by the id_compte column
 * @method     ChildTresorQuery orderByRemise($order = Criteria::ASC) Order by the remise column
 * @method     ChildTresorQuery orderByObservation($order = Criteria::ASC) Order by the observation column
 * @method     ChildTresorQuery orderByCreatedAt($order = Criteria::ASC) Order by the created_at column
 * @method     ChildTresorQuery orderByUpdatedAt($order = Criteria::ASC) Order by the updated_at column
 *
 * @method     ChildTresorQuery groupByIdTresor() Group by the id_tresor column
 * @method     ChildTresorQuery groupByIdEntite() Group by the id_entite column
 * @method     ChildTresorQuery groupByNom() Group by the nom column
 * @method     ChildTresorQuery groupByNomcourt() Group by the nomcourt column
 * @method     ChildTresorQuery groupByIban() Group by the iban column
 * @method     ChildTresorQuery groupByBic() Group by the bic column
 * @method     ChildTresorQuery groupByIdCompterb() Group by the id_compterb column
 * @method     ChildTresorQuery groupByIdCompte() Group by the id_compte column
 * @method     ChildTresorQuery groupByRemise() Group by the remise column
 * @method     ChildTresorQuery groupByObservation() Group by the observation column
 * @method     ChildTresorQuery groupByCreatedAt() Group by the created_at column
 * @method     ChildTresorQuery groupByUpdatedAt() Group by the updated_at column
 *
 * @method     ChildTresorQuery leftJoin($relation) Adds a LEFT JOIN clause to the query
 * @method     ChildTresorQuery rightJoin($relation) Adds a RIGHT JOIN clause to the query
 * @method     ChildTresorQuery innerJoin($relation) Adds a INNER JOIN clause to the query
 *
 * @method     ChildTresorQuery leftJoinWith($relation) Adds a LEFT JOIN clause and with to the query
 * @method     ChildTresorQuery rightJoinWith($relation) Adds a RIGHT JOIN clause and with to the query
 * @method     ChildTresorQuery innerJoinWith($relation) Adds a INNER JOIN clause and with to the query
 *
 * @method     ChildTresorQuery leftJoinEntite($relationAlias = null) Adds a LEFT JOIN clause to the query using the Entite relation
 * @method     ChildTresorQuery rightJoinEntite($relationAlias = null) Adds a RIGHT JOIN clause to the query using the Entite relation
 * @method     ChildTresorQuery innerJoinEntite($relationAlias = null) Adds a INNER JOIN clause to the query using the Entite relation
 *
 * @method     ChildTresorQuery joinWithEntite($joinType = Criteria::INNER_JOIN) Adds a join clause and with to the query using the Entite relation
 *
 * @method     ChildTresorQuery leftJoinWithEntite() Adds a LEFT JOIN clause and with to the query using the Entite relation
 * @method     ChildTresorQuery rightJoinWithEntite() Adds a RIGHT JOIN clause and with to the query using the Entite relation
 * @method     ChildTresorQuery innerJoinWithEntite() Adds a INNER JOIN clause and with to the query using the Entite relation
 *
 * @method     ChildTresorQuery leftJoinCompte($relationAlias = null) Adds a LEFT JOIN clause to the query using the Compte relation
 * @method     ChildTresorQuery rightJoinCompte($relationAlias = null) Adds a RIGHT JOIN clause to the query using the Compte relation
 * @method     ChildTresorQuery innerJoinCompte($relationAlias = null) Adds a INNER JOIN clause to the query using the Compte relation
 *
 * @method     ChildTresorQuery joinWithCompte($joinType = Criteria::INNER_JOIN) Adds a join clause and with to the query using the Compte relation
 *
 * @method     ChildTresorQuery leftJoinWithCompte() Adds a LEFT JOIN clause and with to the query using the Compte relation
 * @method     ChildTresorQuery rightJoinWithCompte() Adds a RIGHT JOIN clause and with to the query using the Compte relation
 * @method     ChildTresorQuery innerJoinWithCompte() Adds a INNER JOIN clause and with to the query using the Compte relation
 *
 * @method     ChildTresorQuery leftJoinPaiement($relationAlias = null) Adds a LEFT JOIN clause to the query using the Paiement relation
 * @method     ChildTresorQuery rightJoinPaiement($relationAlias = null) Adds a RIGHT JOIN clause to the query using the Paiement relation
 * @method     ChildTresorQuery innerJoinPaiement($relationAlias = null) Adds a INNER JOIN clause to the query using the Paiement relation
 *
 * @method     ChildTresorQuery joinWithPaiement($joinType = Criteria::INNER_JOIN) Adds a join clause and with to the query using the Paiement relation
 *
 * @method     ChildTresorQuery leftJoinWithPaiement() Adds a LEFT JOIN clause and with to the query using the Paiement relation
 * @method     ChildTresorQuery rightJoinWithPaiement() Adds a RIGHT JOIN clause and with to the query using the Paiement relation
 * @method     ChildTresorQuery innerJoinWithPaiement() Adds a INNER JOIN clause and with to the query using the Paiement relation
 *
 * @method     \EntiteQuery|\CompteQuery|\PaiementQuery endUse() Finalizes a secondary criteria and merges it with its primary Criteria
 *
 * @method     ChildTresor findOne(ConnectionInterface $con = null) Return the first ChildTresor matching the query
 * @method     ChildTresor findOneOrCreate(ConnectionInterface $con = null) Return the first ChildTresor matching the query, or a new ChildTresor object populated from the query conditions when no match is found
 *
 * @method     ChildTresor findOneByIdTresor(string $id_tresor) Return the first ChildTresor filtered by the id_tresor column
 * @method     ChildTresor findOneByIdEntite(string $id_entite) Return the first ChildTresor filtered by the id_entite column
 * @method     ChildTresor findOneByNom(string $nom) Return the first ChildTresor filtered by the nom column
 * @method     ChildTresor findOneByNomcourt(string $nomcourt) Return the first ChildTresor filtered by the nomcourt column
 * @method     ChildTresor findOneByIban(string $iban) Return the first ChildTresor filtered by the iban column
 * @method     ChildTresor findOneByBic(string $bic) Return the first ChildTresor filtered by the bic column
 * @method     ChildTresor findOneByIdCompterb(string $id_compterb) Return the first ChildTresor filtered by the id_compterb column
 * @method     ChildTresor findOneByIdCompte(string $id_compte) Return the first ChildTresor filtered by the id_compte column
 * @method     ChildTresor findOneByRemise(string $remise) Return the first ChildTresor filtered by the remise column
 * @method     ChildTresor findOneByObservation(string $observation) Return the first ChildTresor filtered by the observation column
 * @method     ChildTresor findOneByCreatedAt(string $created_at) Return the first ChildTresor filtered by the created_at column
 * @method     ChildTresor findOneByUpdatedAt(string $updated_at) Return the first ChildTresor filtered by the updated_at column *

 * @method     ChildTresor requirePk($key, ConnectionInterface $con = null) Return the ChildTresor by primary key and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildTresor requireOne(ConnectionInterface $con = null) Return the first ChildTresor matching the query and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 *
 * @method     ChildTresor requireOneByIdTresor(string $id_tresor) Return the first ChildTresor filtered by the id_tresor column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildTresor requireOneByIdEntite(string $id_entite) Return the first ChildTresor filtered by the id_entite column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildTresor requireOneByNom(string $nom) Return the first ChildTresor filtered by the nom column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildTresor requireOneByNomcourt(string $nomcourt) Return the first ChildTresor filtered by the nomcourt column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildTresor requireOneByIban(string $iban) Return the first ChildTresor filtered by the iban column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildTresor requireOneByBic(string $bic) Return the first ChildTresor filtered by the bic column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildTresor requireOneByIdCompterb(string $id_compterb) Return the first ChildTresor filtered by the id_compterb column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildTresor requireOneByIdCompte(string $id_compte) Return the first ChildTresor filtered by the id_compte column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildTresor requireOneByRemise(string $remise) Return the first ChildTresor filtered by the remise column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildTresor requireOneByObservation(string $observation) Return the first ChildTresor filtered by the observation column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildTresor requireOneByCreatedAt(string $created_at) Return the first ChildTresor filtered by the created_at column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildTresor requireOneByUpdatedAt(string $updated_at) Return the first ChildTresor filtered by the updated_at column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 *
 * @method     ChildTresor[]|ObjectCollection find(ConnectionInterface $con = null) Return ChildTresor objects based on current ModelCriteria
 * @method     ChildTresor[]|ObjectCollection findByIdTresor(string $id_tresor) Return ChildTresor objects filtered by the id_tresor column
 * @method     ChildTresor[]|ObjectCollection findByIdEntite(string $id_entite) Return ChildTresor objects filtered by the id_entite column
 * @method     ChildTresor[]|ObjectCollection findByNom(string $nom) Return ChildTresor objects filtered by the nom column
 * @method     ChildTresor[]|ObjectCollection findByNomcourt(string $nomcourt) Return ChildTresor objects filtered by the nomcourt column
 * @method     ChildTresor[]|ObjectCollection findByIban(string $iban) Return ChildTresor objects filtered by the iban column
 * @method     ChildTresor[]|ObjectCollection findByBic(string $bic) Return ChildTresor objects filtered by the bic column
 * @method     ChildTresor[]|ObjectCollection findByIdCompterb(string $id_compterb) Return ChildTresor objects filtered by the id_compterb column
 * @method     ChildTresor[]|ObjectCollection findByIdCompte(string $id_compte) Return ChildTresor objects filtered by the id_compte column
 * @method     ChildTresor[]|ObjectCollection findByRemise(string $remise) Return ChildTresor objects filtered by the remise column
 * @method     ChildTresor[]|ObjectCollection findByObservation(string $observation) Return ChildTresor objects filtered by the observation column
 * @method     ChildTresor[]|ObjectCollection findByCreatedAt(string $created_at) Return ChildTresor objects filtered by the created_at column
 * @method     ChildTresor[]|ObjectCollection findByUpdatedAt(string $updated_at) Return ChildTresor objects filtered by the updated_at column
 * @method     ChildTresor[]|\Propel\Runtime\Util\PropelModelPager paginate($page = 1, $maxPerPage = 10, ConnectionInterface $con = null) Issue a SELECT query based on the current ModelCriteria and uses a page and a maximum number of results per page to compute an offset and a limit
 *
 */
abstract class TresorQuery extends ModelCriteria
{

    // archivable behavior
    protected $archiveOnDelete = true;
protected $entityNotFoundExceptionClass = '\\Propel\\Runtime\\Exception\\EntityNotFoundException';

    /**
     * Initializes internal state of \Base\TresorQuery object.
     *
     * @param     string $dbName The database name
     * @param     string $modelName The phpName of a model, e.g. 'Book'
     * @param     string $modelAlias The alias for the model in this query, e.g. 'b'
     */
    public function __construct($dbName = 'simplasso', $modelName = '\\Tresor', $modelAlias = null)
    {
        parent::__construct($dbName, $modelName, $modelAlias);
    }

    /**
     * Returns a new ChildTresorQuery object.
     *
     * @param     string $modelAlias The alias of a model in the query
     * @param     Criteria $criteria Optional Criteria to build the query from
     *
     * @return ChildTresorQuery
     */
    public static function create($modelAlias = null, Criteria $criteria = null)
    {
        if ($criteria instanceof ChildTresorQuery) {
            return $criteria;
        }
        $query = new ChildTresorQuery();
        if (null !== $modelAlias) {
            $query->setModelAlias($modelAlias);
        }
        if ($criteria instanceof Criteria) {
            $query->mergeWith($criteria);
        }

        return $query;
    }

    /**
     * Find object by primary key.
     * Propel uses the instance pool to skip the database if the object exists.
     * Go fast if the query is untouched.
     *
     * <code>
     * $obj  = $c->findPk(12, $con);
     * </code>
     *
     * @param mixed $key Primary key to use for the query
     * @param ConnectionInterface $con an optional connection object
     *
     * @return ChildTresor|array|mixed the result, formatted by the current formatter
     */
    public function findPk($key, ConnectionInterface $con = null)
    {
        if ($key === null) {
            return null;
        }

        if ($con === null) {
            $con = Propel::getServiceContainer()->getReadConnection(TresorTableMap::DATABASE_NAME);
        }

        $this->basePreSelect($con);

        if (
            $this->formatter || $this->modelAlias || $this->with || $this->select
            || $this->selectColumns || $this->asColumns || $this->selectModifiers
            || $this->map || $this->having || $this->joins
        ) {
            return $this->findPkComplex($key, $con);
        }

        if ((null !== ($obj = TresorTableMap::getInstanceFromPool(null === $key || is_scalar($key) || is_callable([$key, '__toString']) ? (string) $key : $key)))) {
            // the object is already in the instance pool
            return $obj;
        }

        return $this->findPkSimple($key, $con);
    }

    /**
     * Find object by primary key using raw SQL to go fast.
     * Bypass doSelect() and the object formatter by using generated code.
     *
     * @param     mixed $key Primary key to use for the query
     * @param     ConnectionInterface $con A connection object
     *
     * @throws \Propel\Runtime\Exception\PropelException
     *
     * @return ChildTresor A model object, or null if the key is not found
     */
    protected function findPkSimple($key, ConnectionInterface $con)
    {
        $sql = 'SELECT `id_tresor`, `id_entite`, `nom`, `nomcourt`, `iban`, `bic`, `id_compterb`, `id_compte`, `remise`, `observation`, `created_at`, `updated_at` FROM `asso_tresors` WHERE `id_tresor` = :p0';
        try {
            $stmt = $con->prepare($sql);
            $stmt->bindValue(':p0', $key, PDO::PARAM_INT);
            $stmt->execute();
        } catch (Exception $e) {
            Propel::log($e->getMessage(), Propel::LOG_ERR);
            throw new PropelException(sprintf('Unable to execute SELECT statement [%s]', $sql), 0, $e);
        }
        $obj = null;
        if ($row = $stmt->fetch(\PDO::FETCH_NUM)) {
            /** @var ChildTresor $obj */
            $obj = new ChildTresor();
            $obj->hydrate($row);
            TresorTableMap::addInstanceToPool($obj, null === $key || is_scalar($key) || is_callable([$key, '__toString']) ? (string) $key : $key);
        }
        $stmt->closeCursor();

        return $obj;
    }

    /**
     * Find object by primary key.
     *
     * @param     mixed $key Primary key to use for the query
     * @param     ConnectionInterface $con A connection object
     *
     * @return ChildTresor|array|mixed the result, formatted by the current formatter
     */
    protected function findPkComplex($key, ConnectionInterface $con)
    {
        // As the query uses a PK condition, no limit(1) is necessary.
        $criteria = $this->isKeepQuery() ? clone $this : $this;
        $dataFetcher = $criteria
            ->filterByPrimaryKey($key)
            ->doSelect($con);

        return $criteria->getFormatter()->init($criteria)->formatOne($dataFetcher);
    }

    /**
     * Find objects by primary key
     * <code>
     * $objs = $c->findPks(array(12, 56, 832), $con);
     * </code>
     * @param     array $keys Primary keys to use for the query
     * @param     ConnectionInterface $con an optional connection object
     *
     * @return ObjectCollection|array|mixed the list of results, formatted by the current formatter
     */
    public function findPks($keys, ConnectionInterface $con = null)
    {
        if (null === $con) {
            $con = Propel::getServiceContainer()->getReadConnection($this->getDbName());
        }
        $this->basePreSelect($con);
        $criteria = $this->isKeepQuery() ? clone $this : $this;
        $dataFetcher = $criteria
            ->filterByPrimaryKeys($keys)
            ->doSelect($con);

        return $criteria->getFormatter()->init($criteria)->format($dataFetcher);
    }

    /**
     * Filter the query by primary key
     *
     * @param     mixed $key Primary key to use for the query
     *
     * @return $this|ChildTresorQuery The current query, for fluid interface
     */
    public function filterByPrimaryKey($key)
    {

        return $this->addUsingAlias(TresorTableMap::COL_ID_TRESOR, $key, Criteria::EQUAL);
    }

    /**
     * Filter the query by a list of primary keys
     *
     * @param     array $keys The list of primary key to use for the query
     *
     * @return $this|ChildTresorQuery The current query, for fluid interface
     */
    public function filterByPrimaryKeys($keys)
    {

        return $this->addUsingAlias(TresorTableMap::COL_ID_TRESOR, $keys, Criteria::IN);
    }

    /**
     * Filter the query on the id_tresor column
     *
     * Example usage:
     * <code>
     * $query->filterByIdTresor(1234); // WHERE id_tresor = 1234
     * $query->filterByIdTresor(array(12, 34)); // WHERE id_tresor IN (12, 34)
     * $query->filterByIdTresor(array('min' => 12)); // WHERE id_tresor > 12
     * </code>
     *
     * @param     mixed $idTresor The value to use as filter.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildTresorQuery The current query, for fluid interface
     */
    public function filterByIdTresor($idTresor = null, $comparison = null)
    {
        if (is_array($idTresor)) {
            $useMinMax = false;
            if (isset($idTresor['min'])) {
                $this->addUsingAlias(TresorTableMap::COL_ID_TRESOR, $idTresor['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($idTresor['max'])) {
                $this->addUsingAlias(TresorTableMap::COL_ID_TRESOR, $idTresor['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(TresorTableMap::COL_ID_TRESOR, $idTresor, $comparison);
    }

    /**
     * Filter the query on the id_entite column
     *
     * Example usage:
     * <code>
     * $query->filterByIdEntite(1234); // WHERE id_entite = 1234
     * $query->filterByIdEntite(array(12, 34)); // WHERE id_entite IN (12, 34)
     * $query->filterByIdEntite(array('min' => 12)); // WHERE id_entite > 12
     * </code>
     *
     * @see       filterByEntite()
     *
     * @param     mixed $idEntite The value to use as filter.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildTresorQuery The current query, for fluid interface
     */
    public function filterByIdEntite($idEntite = null, $comparison = null)
    {
        if (is_array($idEntite)) {
            $useMinMax = false;
            if (isset($idEntite['min'])) {
                $this->addUsingAlias(TresorTableMap::COL_ID_ENTITE, $idEntite['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($idEntite['max'])) {
                $this->addUsingAlias(TresorTableMap::COL_ID_ENTITE, $idEntite['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(TresorTableMap::COL_ID_ENTITE, $idEntite, $comparison);
    }

    /**
     * Filter the query on the nom column
     *
     * Example usage:
     * <code>
     * $query->filterByNom('fooValue');   // WHERE nom = 'fooValue'
     * $query->filterByNom('%fooValue%', Criteria::LIKE); // WHERE nom LIKE '%fooValue%'
     * </code>
     *
     * @param     string $nom The value to use as filter.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildTresorQuery The current query, for fluid interface
     */
    public function filterByNom($nom = null, $comparison = null)
    {
        if (null === $comparison) {
            if (is_array($nom)) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(TresorTableMap::COL_NOM, $nom, $comparison);
    }

    /**
     * Filter the query on the nomcourt column
     *
     * Example usage:
     * <code>
     * $query->filterByNomcourt('fooValue');   // WHERE nomcourt = 'fooValue'
     * $query->filterByNomcourt('%fooValue%', Criteria::LIKE); // WHERE nomcourt LIKE '%fooValue%'
     * </code>
     *
     * @param     string $nomcourt The value to use as filter.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildTresorQuery The current query, for fluid interface
     */
    public function filterByNomcourt($nomcourt = null, $comparison = null)
    {
        if (null === $comparison) {
            if (is_array($nomcourt)) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(TresorTableMap::COL_NOMCOURT, $nomcourt, $comparison);
    }

    /**
     * Filter the query on the iban column
     *
     * Example usage:
     * <code>
     * $query->filterByIban('fooValue');   // WHERE iban = 'fooValue'
     * $query->filterByIban('%fooValue%', Criteria::LIKE); // WHERE iban LIKE '%fooValue%'
     * </code>
     *
     * @param     string $iban The value to use as filter.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildTresorQuery The current query, for fluid interface
     */
    public function filterByIban($iban = null, $comparison = null)
    {
        if (null === $comparison) {
            if (is_array($iban)) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(TresorTableMap::COL_IBAN, $iban, $comparison);
    }

    /**
     * Filter the query on the bic column
     *
     * Example usage:
     * <code>
     * $query->filterByBic('fooValue');   // WHERE bic = 'fooValue'
     * $query->filterByBic('%fooValue%', Criteria::LIKE); // WHERE bic LIKE '%fooValue%'
     * </code>
     *
     * @param     string $bic The value to use as filter.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildTresorQuery The current query, for fluid interface
     */
    public function filterByBic($bic = null, $comparison = null)
    {
        if (null === $comparison) {
            if (is_array($bic)) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(TresorTableMap::COL_BIC, $bic, $comparison);
    }

    /**
     * Filter the query on the id_compterb column
     *
     * Example usage:
     * <code>
     * $query->filterByIdCompterb(1234); // WHERE id_compterb = 1234
     * $query->filterByIdCompterb(array(12, 34)); // WHERE id_compterb IN (12, 34)
     * $query->filterByIdCompterb(array('min' => 12)); // WHERE id_compterb > 12
     * </code>
     *
     * @param     mixed $idCompterb The value to use as filter.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildTresorQuery The current query, for fluid interface
     */
    public function filterByIdCompterb($idCompterb = null, $comparison = null)
    {
        if (is_array($idCompterb)) {
            $useMinMax = false;
            if (isset($idCompterb['min'])) {
                $this->addUsingAlias(TresorTableMap::COL_ID_COMPTERB, $idCompterb['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($idCompterb['max'])) {
                $this->addUsingAlias(TresorTableMap::COL_ID_COMPTERB, $idCompterb['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(TresorTableMap::COL_ID_COMPTERB, $idCompterb, $comparison);
    }

    /**
     * Filter the query on the id_compte column
     *
     * Example usage:
     * <code>
     * $query->filterByIdCompte(1234); // WHERE id_compte = 1234
     * $query->filterByIdCompte(array(12, 34)); // WHERE id_compte IN (12, 34)
     * $query->filterByIdCompte(array('min' => 12)); // WHERE id_compte > 12
     * </code>
     *
     * @see       filterByCompte()
     *
     * @param     mixed $idCompte The value to use as filter.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildTresorQuery The current query, for fluid interface
     */
    public function filterByIdCompte($idCompte = null, $comparison = null)
    {
        if (is_array($idCompte)) {
            $useMinMax = false;
            if (isset($idCompte['min'])) {
                $this->addUsingAlias(TresorTableMap::COL_ID_COMPTE, $idCompte['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($idCompte['max'])) {
                $this->addUsingAlias(TresorTableMap::COL_ID_COMPTE, $idCompte['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(TresorTableMap::COL_ID_COMPTE, $idCompte, $comparison);
    }

    /**
     * Filter the query on the remise column
     *
     * Example usage:
     * <code>
     * $query->filterByRemise(1234); // WHERE remise = 1234
     * $query->filterByRemise(array(12, 34)); // WHERE remise IN (12, 34)
     * $query->filterByRemise(array('min' => 12)); // WHERE remise > 12
     * </code>
     *
     * @param     mixed $remise The value to use as filter.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildTresorQuery The current query, for fluid interface
     */
    public function filterByRemise($remise = null, $comparison = null)
    {
        if (is_array($remise)) {
            $useMinMax = false;
            if (isset($remise['min'])) {
                $this->addUsingAlias(TresorTableMap::COL_REMISE, $remise['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($remise['max'])) {
                $this->addUsingAlias(TresorTableMap::COL_REMISE, $remise['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(TresorTableMap::COL_REMISE, $remise, $comparison);
    }

    /**
     * Filter the query on the observation column
     *
     * Example usage:
     * <code>
     * $query->filterByObservation('fooValue');   // WHERE observation = 'fooValue'
     * $query->filterByObservation('%fooValue%', Criteria::LIKE); // WHERE observation LIKE '%fooValue%'
     * </code>
     *
     * @param     string $observation The value to use as filter.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildTresorQuery The current query, for fluid interface
     */
    public function filterByObservation($observation = null, $comparison = null)
    {
        if (null === $comparison) {
            if (is_array($observation)) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(TresorTableMap::COL_OBSERVATION, $observation, $comparison);
    }

    /**
     * Filter the query on the created_at column
     *
     * Example usage:
     * <code>
     * $query->filterByCreatedAt('2011-03-14'); // WHERE created_at = '2011-03-14'
     * $query->filterByCreatedAt('now'); // WHERE created_at = '2011-03-14'
     * $query->filterByCreatedAt(array('max' => 'yesterday')); // WHERE created_at > '2011-03-13'
     * </code>
     *
     * @param     mixed $createdAt The value to use as filter.
     *              Values can be integers (unix timestamps), DateTime objects, or strings.
     *              Empty strings are treated as NULL.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildTresorQuery The current query, for fluid interface
     */
    public function filterByCreatedAt($createdAt = null, $comparison = null)
    {
        if (is_array($createdAt)) {
            $useMinMax = false;
            if (isset($createdAt['min'])) {
                $this->addUsingAlias(TresorTableMap::COL_CREATED_AT, $createdAt['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($createdAt['max'])) {
                $this->addUsingAlias(TresorTableMap::COL_CREATED_AT, $createdAt['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(TresorTableMap::COL_CREATED_AT, $createdAt, $comparison);
    }

    /**
     * Filter the query on the updated_at column
     *
     * Example usage:
     * <code>
     * $query->filterByUpdatedAt('2011-03-14'); // WHERE updated_at = '2011-03-14'
     * $query->filterByUpdatedAt('now'); // WHERE updated_at = '2011-03-14'
     * $query->filterByUpdatedAt(array('max' => 'yesterday')); // WHERE updated_at > '2011-03-13'
     * </code>
     *
     * @param     mixed $updatedAt The value to use as filter.
     *              Values can be integers (unix timestamps), DateTime objects, or strings.
     *              Empty strings are treated as NULL.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildTresorQuery The current query, for fluid interface
     */
    public function filterByUpdatedAt($updatedAt = null, $comparison = null)
    {
        if (is_array($updatedAt)) {
            $useMinMax = false;
            if (isset($updatedAt['min'])) {
                $this->addUsingAlias(TresorTableMap::COL_UPDATED_AT, $updatedAt['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($updatedAt['max'])) {
                $this->addUsingAlias(TresorTableMap::COL_UPDATED_AT, $updatedAt['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(TresorTableMap::COL_UPDATED_AT, $updatedAt, $comparison);
    }

    /**
     * Filter the query by a related \Entite object
     *
     * @param \Entite|ObjectCollection $entite The related object(s) to use as filter
     * @param string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @throws \Propel\Runtime\Exception\PropelException
     *
     * @return ChildTresorQuery The current query, for fluid interface
     */
    public function filterByEntite($entite, $comparison = null)
    {
        if ($entite instanceof \Entite) {
            return $this
                ->addUsingAlias(TresorTableMap::COL_ID_ENTITE, $entite->getIdEntite(), $comparison);
        } elseif ($entite instanceof ObjectCollection) {
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }

            return $this
                ->addUsingAlias(TresorTableMap::COL_ID_ENTITE, $entite->toKeyValue('PrimaryKey', 'IdEntite'), $comparison);
        } else {
            throw new PropelException('filterByEntite() only accepts arguments of type \Entite or Collection');
        }
    }

    /**
     * Adds a JOIN clause to the query using the Entite relation
     *
     * @param     string $relationAlias optional alias for the relation
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return $this|ChildTresorQuery The current query, for fluid interface
     */
    public function joinEntite($relationAlias = null, $joinType = Criteria::INNER_JOIN)
    {
        $tableMap = $this->getTableMap();
        $relationMap = $tableMap->getRelation('Entite');

        // create a ModelJoin object for this join
        $join = new ModelJoin();
        $join->setJoinType($joinType);
        $join->setRelationMap($relationMap, $this->useAliasInSQL ? $this->getModelAlias() : null, $relationAlias);
        if ($previousJoin = $this->getPreviousJoin()) {
            $join->setPreviousJoin($previousJoin);
        }

        // add the ModelJoin to the current object
        if ($relationAlias) {
            $this->addAlias($relationAlias, $relationMap->getRightTable()->getName());
            $this->addJoinObject($join, $relationAlias);
        } else {
            $this->addJoinObject($join, 'Entite');
        }

        return $this;
    }

    /**
     * Use the Entite relation Entite object
     *
     * @see useQuery()
     *
     * @param     string $relationAlias optional alias for the relation,
     *                                   to be used as main alias in the secondary query
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return \EntiteQuery A secondary query class using the current class as primary query
     */
    public function useEntiteQuery($relationAlias = null, $joinType = Criteria::INNER_JOIN)
    {
        return $this
            ->joinEntite($relationAlias, $joinType)
            ->useQuery($relationAlias ? $relationAlias : 'Entite', '\EntiteQuery');
    }

    /**
     * Filter the query by a related \Compte object
     *
     * @param \Compte|ObjectCollection $compte The related object(s) to use as filter
     * @param string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @throws \Propel\Runtime\Exception\PropelException
     *
     * @return ChildTresorQuery The current query, for fluid interface
     */
    public function filterByCompte($compte, $comparison = null)
    {
        if ($compte instanceof \Compte) {
            return $this
                ->addUsingAlias(TresorTableMap::COL_ID_COMPTE, $compte->getIdCompte(), $comparison);
        } elseif ($compte instanceof ObjectCollection) {
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }

            return $this
                ->addUsingAlias(TresorTableMap::COL_ID_COMPTE, $compte->toKeyValue('PrimaryKey', 'IdCompte'), $comparison);
        } else {
            throw new PropelException('filterByCompte() only accepts arguments of type \Compte or Collection');
        }
    }

    /**
     * Adds a JOIN clause to the query using the Compte relation
     *
     * @param     string $relationAlias optional alias for the relation
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return $this|ChildTresorQuery The current query, for fluid interface
     */
    public function joinCompte($relationAlias = null, $joinType = Criteria::LEFT_JOIN)
    {
        $tableMap = $this->getTableMap();
        $relationMap = $tableMap->getRelation('Compte');

        // create a ModelJoin object for this join
        $join = new ModelJoin();
        $join->setJoinType($joinType);
        $join->setRelationMap($relationMap, $this->useAliasInSQL ? $this->getModelAlias() : null, $relationAlias);
        if ($previousJoin = $this->getPreviousJoin()) {
            $join->setPreviousJoin($previousJoin);
        }

        // add the ModelJoin to the current object
        if ($relationAlias) {
            $this->addAlias($relationAlias, $relationMap->getRightTable()->getName());
            $this->addJoinObject($join, $relationAlias);
        } else {
            $this->addJoinObject($join, 'Compte');
        }

        return $this;
    }

    /**
     * Use the Compte relation Compte object
     *
     * @see useQuery()
     *
     * @param     string $relationAlias optional alias for the relation,
     *                                   to be used as main alias in the secondary query
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return \CompteQuery A secondary query class using the current class as primary query
     */
    public function useCompteQuery($relationAlias = null, $joinType = Criteria::LEFT_JOIN)
    {
        return $this
            ->joinCompte($relationAlias, $joinType)
            ->useQuery($relationAlias ? $relationAlias : 'Compte', '\CompteQuery');
    }

    /**
     * Filter the query by a related \Paiement object
     *
     * @param \Paiement|ObjectCollection $paiement the related object to use as filter
     * @param string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return ChildTresorQuery The current query, for fluid interface
     */
    public function filterByPaiement($paiement, $comparison = null)
    {
        if ($paiement instanceof \Paiement) {
            return $this
                ->addUsingAlias(TresorTableMap::COL_ID_TRESOR, $paiement->getIdTresor(), $comparison);
        } elseif ($paiement instanceof ObjectCollection) {
            return $this
                ->usePaiementQuery()
                ->filterByPrimaryKeys($paiement->getPrimaryKeys())
                ->endUse();
        } else {
            throw new PropelException('filterByPaiement() only accepts arguments of type \Paiement or Collection');
        }
    }

    /**
     * Adds a JOIN clause to the query using the Paiement relation
     *
     * @param     string $relationAlias optional alias for the relation
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return $this|ChildTresorQuery The current query, for fluid interface
     */
    public function joinPaiement($relationAlias = null, $joinType = Criteria::INNER_JOIN)
    {
        $tableMap = $this->getTableMap();
        $relationMap = $tableMap->getRelation('Paiement');

        // create a ModelJoin object for this join
        $join = new ModelJoin();
        $join->setJoinType($joinType);
        $join->setRelationMap($relationMap, $this->useAliasInSQL ? $this->getModelAlias() : null, $relationAlias);
        if ($previousJoin = $this->getPreviousJoin()) {
            $join->setPreviousJoin($previousJoin);
        }

        // add the ModelJoin to the current object
        if ($relationAlias) {
            $this->addAlias($relationAlias, $relationMap->getRightTable()->getName());
            $this->addJoinObject($join, $relationAlias);
        } else {
            $this->addJoinObject($join, 'Paiement');
        }

        return $this;
    }

    /**
     * Use the Paiement relation Paiement object
     *
     * @see useQuery()
     *
     * @param     string $relationAlias optional alias for the relation,
     *                                   to be used as main alias in the secondary query
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return \PaiementQuery A secondary query class using the current class as primary query
     */
    public function usePaiementQuery($relationAlias = null, $joinType = Criteria::INNER_JOIN)
    {
        return $this
            ->joinPaiement($relationAlias, $joinType)
            ->useQuery($relationAlias ? $relationAlias : 'Paiement', '\PaiementQuery');
    }

    /**
     * Exclude object from result
     *
     * @param   ChildTresor $tresor Object to remove from the list of results
     *
     * @return $this|ChildTresorQuery The current query, for fluid interface
     */
    public function prune($tresor = null)
    {
        if ($tresor) {
            $this->addUsingAlias(TresorTableMap::COL_ID_TRESOR, $tresor->getIdTresor(), Criteria::NOT_EQUAL);
        }

        return $this;
    }

    /**
     * Code to execute before every DELETE statement
     *
     * @param     ConnectionInterface $con The connection object used by the query
     */
    protected function basePreDelete(ConnectionInterface $con)
    {
        // archivable behavior

        if ($this->archiveOnDelete) {
            $this->archive($con);
        } else {
            $this->archiveOnDelete = true;
        }


        return $this->preDelete($con);
    }

    /**
     * Deletes all rows from the asso_tresors table.
     *
     * @param ConnectionInterface $con the connection to use
     * @return int The number of affected rows (if supported by underlying database driver).
     */
    public function doDeleteAll(ConnectionInterface $con = null)
    {
        if (null === $con) {
            $con = Propel::getServiceContainer()->getWriteConnection(TresorTableMap::DATABASE_NAME);
        }

        // use transaction because $criteria could contain info
        // for more than one table or we could emulating ON DELETE CASCADE, etc.
        return $con->transaction(function () use ($con) {
            $affectedRows = 0; // initialize var to track total num of affected rows
            $affectedRows += parent::doDeleteAll($con);
            // Because this db requires some delete cascade/set null emulation, we have to
            // clear the cached instance *after* the emulation has happened (since
            // instances get re-added by the select statement contained therein).
            TresorTableMap::clearInstancePool();
            TresorTableMap::clearRelatedInstancePool();

            return $affectedRows;
        });
    }

    /**
     * Performs a DELETE on the database based on the current ModelCriteria
     *
     * @param ConnectionInterface $con the connection to use
     * @return int             The number of affected rows (if supported by underlying database driver).  This includes CASCADE-related rows
     *                         if supported by native driver or if emulated using Propel.
     * @throws PropelException Any exceptions caught during processing will be
     *                         rethrown wrapped into a PropelException.
     */
    public function delete(ConnectionInterface $con = null)
    {
        if (null === $con) {
            $con = Propel::getServiceContainer()->getWriteConnection(TresorTableMap::DATABASE_NAME);
        }

        $criteria = $this;

        // Set the correct dbName
        $criteria->setDbName(TresorTableMap::DATABASE_NAME);

        // use transaction because $criteria could contain info
        // for more than one table or we could emulating ON DELETE CASCADE, etc.
        return $con->transaction(function () use ($con, $criteria) {
            $affectedRows = 0; // initialize var to track total num of affected rows

            TresorTableMap::removeInstanceFromPool($criteria);

            $affectedRows += ModelCriteria::delete($con);
            TresorTableMap::clearRelatedInstancePool();

            return $affectedRows;
        });
    }

    // timestampable behavior

    /**
     * Filter by the latest updated
     *
     * @param      int $nbDays Maximum age of the latest update in days
     *
     * @return     $this|ChildTresorQuery The current query, for fluid interface
     */
    public function recentlyUpdated($nbDays = 7)
    {
        return $this->addUsingAlias(TresorTableMap::COL_UPDATED_AT, time() - $nbDays * 24 * 60 * 60, Criteria::GREATER_EQUAL);
    }

    /**
     * Order by update date desc
     *
     * @return     $this|ChildTresorQuery The current query, for fluid interface
     */
    public function lastUpdatedFirst()
    {
        return $this->addDescendingOrderByColumn(TresorTableMap::COL_UPDATED_AT);
    }

    /**
     * Order by update date asc
     *
     * @return     $this|ChildTresorQuery The current query, for fluid interface
     */
    public function firstUpdatedFirst()
    {
        return $this->addAscendingOrderByColumn(TresorTableMap::COL_UPDATED_AT);
    }

    /**
     * Order by create date desc
     *
     * @return     $this|ChildTresorQuery The current query, for fluid interface
     */
    public function lastCreatedFirst()
    {
        return $this->addDescendingOrderByColumn(TresorTableMap::COL_CREATED_AT);
    }

    /**
     * Filter by the latest created
     *
     * @param      int $nbDays Maximum age of in days
     *
     * @return     $this|ChildTresorQuery The current query, for fluid interface
     */
    public function recentlyCreated($nbDays = 7)
    {
        return $this->addUsingAlias(TresorTableMap::COL_CREATED_AT, time() - $nbDays * 24 * 60 * 60, Criteria::GREATER_EQUAL);
    }

    /**
     * Order by create date asc
     *
     * @return     $this|ChildTresorQuery The current query, for fluid interface
     */
    public function firstCreatedFirst()
    {
        return $this->addAscendingOrderByColumn(TresorTableMap::COL_CREATED_AT);
    }

    // archivable behavior

    /**
     * Copy the data of the objects satisfying the query into ChildAssoTresorsArchive archive objects.
     * The archived objects are then saved.
     * If any of the objects has already been archived, the archived object
     * is updated and not duplicated.
     * Warning: This termination methods issues 2n+1 queries.
     *
     * @param      ConnectionInterface $con    Connection to use.
     * @param      Boolean $useLittleMemory    Whether or not to use OnDemandFormatter to retrieve objects.
     *               Set to false if the identity map matters.
     *               Set to true (default) to use less memory.
     *
     * @return     int the number of archived objects
     */
    public function archive($con = null, $useLittleMemory = true)
    {
        $criteria = clone $this;
        // prepare the query
        $criteria->setWith(array());
        if ($useLittleMemory) {
            $criteria->setFormatter(ModelCriteria::FORMAT_ON_DEMAND);
        }
        if ($con === null) {
            $con = Propel::getServiceContainer()->getWriteConnection(TresorTableMap::DATABASE_NAME);
        }

        return $con->transaction(function () use ($con, $criteria) {
            $totalArchivedObjects = 0;

            // archive all results one by one
            foreach ($criteria->find($con) as $object) {
                $object->archive($con);
                $totalArchivedObjects++;
            }

            return $totalArchivedObjects;
        });
    }

    /**
     * Enable/disable auto-archiving on delete for the next query.
     *
     * @param boolean True if the query must archive deleted objects, false otherwise.
     */
    public function setArchiveOnDelete($archiveOnDelete)
    {
        $this->archiveOnDelete = $archiveOnDelete;
    }

    /**
     * Delete records matching the current query without archiving them.
     *
     * @param      ConnectionInterface $con    Connection to use.
     *
     * @return integer the number of deleted rows
     */
    public function deleteWithoutArchive($con = null)
    {
        $this->archiveOnDelete = false;

        return $this->delete($con);
    }

    /**
     * Delete all records without archiving them.
     *
     * @param      ConnectionInterface $con    Connection to use.
     *
     * @return integer the number of deleted rows
     */
    public function deleteAllWithoutArchive($con = null)
    {
        $this->archiveOnDelete = false;

        return $this->deleteAll($con);
    }

} // TresorQuery
