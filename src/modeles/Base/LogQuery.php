<?php

namespace Base;

use \Log as ChildLog;
use \LogQuery as ChildLogQuery;
use \Exception;
use \PDO;
use Map\LogTableMap;
use Propel\Runtime\Propel;
use Propel\Runtime\ActiveQuery\Criteria;
use Propel\Runtime\ActiveQuery\ModelCriteria;
use Propel\Runtime\ActiveQuery\ModelJoin;
use Propel\Runtime\Collection\ObjectCollection;
use Propel\Runtime\Connection\ConnectionInterface;
use Propel\Runtime\Exception\PropelException;

/**
 * Base class that represents a query for the 'asso_logs' table.
 *
 *
 *
 * @method     ChildLogQuery orderByIdLog($order = Criteria::ASC) Order by the id_log column
 * @method     ChildLogQuery orderByIdIndividu($order = Criteria::ASC) Order by the id_individu column
 * @method     ChildLogQuery orderByIdEntite($order = Criteria::ASC) Order by the id_entite column
 * @method     ChildLogQuery orderByCode($order = Criteria::ASC) Order by the code column
 * @method     ChildLogQuery orderByVariables($order = Criteria::ASC) Order by the variables column
 * @method     ChildLogQuery orderByDateOperation($order = Criteria::ASC) Order by the date_operation column
 * @method     ChildLogQuery orderByObservation($order = Criteria::ASC) Order by the observation column
 *
 * @method     ChildLogQuery groupByIdLog() Group by the id_log column
 * @method     ChildLogQuery groupByIdIndividu() Group by the id_individu column
 * @method     ChildLogQuery groupByIdEntite() Group by the id_entite column
 * @method     ChildLogQuery groupByCode() Group by the code column
 * @method     ChildLogQuery groupByVariables() Group by the variables column
 * @method     ChildLogQuery groupByDateOperation() Group by the date_operation column
 * @method     ChildLogQuery groupByObservation() Group by the observation column
 *
 * @method     ChildLogQuery leftJoin($relation) Adds a LEFT JOIN clause to the query
 * @method     ChildLogQuery rightJoin($relation) Adds a RIGHT JOIN clause to the query
 * @method     ChildLogQuery innerJoin($relation) Adds a INNER JOIN clause to the query
 *
 * @method     ChildLogQuery leftJoinWith($relation) Adds a LEFT JOIN clause and with to the query
 * @method     ChildLogQuery rightJoinWith($relation) Adds a RIGHT JOIN clause and with to the query
 * @method     ChildLogQuery innerJoinWith($relation) Adds a INNER JOIN clause and with to the query
 *
 * @method     ChildLogQuery leftJoinIndividu($relationAlias = null) Adds a LEFT JOIN clause to the query using the Individu relation
 * @method     ChildLogQuery rightJoinIndividu($relationAlias = null) Adds a RIGHT JOIN clause to the query using the Individu relation
 * @method     ChildLogQuery innerJoinIndividu($relationAlias = null) Adds a INNER JOIN clause to the query using the Individu relation
 *
 * @method     ChildLogQuery joinWithIndividu($joinType = Criteria::INNER_JOIN) Adds a join clause and with to the query using the Individu relation
 *
 * @method     ChildLogQuery leftJoinWithIndividu() Adds a LEFT JOIN clause and with to the query using the Individu relation
 * @method     ChildLogQuery rightJoinWithIndividu() Adds a RIGHT JOIN clause and with to the query using the Individu relation
 * @method     ChildLogQuery innerJoinWithIndividu() Adds a INNER JOIN clause and with to the query using the Individu relation
 *
 * @method     ChildLogQuery leftJoinLogLien($relationAlias = null) Adds a LEFT JOIN clause to the query using the LogLien relation
 * @method     ChildLogQuery rightJoinLogLien($relationAlias = null) Adds a RIGHT JOIN clause to the query using the LogLien relation
 * @method     ChildLogQuery innerJoinLogLien($relationAlias = null) Adds a INNER JOIN clause to the query using the LogLien relation
 *
 * @method     ChildLogQuery joinWithLogLien($joinType = Criteria::INNER_JOIN) Adds a join clause and with to the query using the LogLien relation
 *
 * @method     ChildLogQuery leftJoinWithLogLien() Adds a LEFT JOIN clause and with to the query using the LogLien relation
 * @method     ChildLogQuery rightJoinWithLogLien() Adds a RIGHT JOIN clause and with to the query using the LogLien relation
 * @method     ChildLogQuery innerJoinWithLogLien() Adds a INNER JOIN clause and with to the query using the LogLien relation
 *
 * @method     \IndividuQuery|\LogLienQuery endUse() Finalizes a secondary criteria and merges it with its primary Criteria
 *
 * @method     ChildLog findOne(ConnectionInterface $con = null) Return the first ChildLog matching the query
 * @method     ChildLog findOneOrCreate(ConnectionInterface $con = null) Return the first ChildLog matching the query, or a new ChildLog object populated from the query conditions when no match is found
 *
 * @method     ChildLog findOneByIdLog(string $id_log) Return the first ChildLog filtered by the id_log column
 * @method     ChildLog findOneByIdIndividu(string $id_individu) Return the first ChildLog filtered by the id_individu column
 * @method     ChildLog findOneByIdEntite(string $id_entite) Return the first ChildLog filtered by the id_entite column
 * @method     ChildLog findOneByCode(string $code) Return the first ChildLog filtered by the code column
 * @method     ChildLog findOneByVariables(string $variables) Return the first ChildLog filtered by the variables column
 * @method     ChildLog findOneByDateOperation(string $date_operation) Return the first ChildLog filtered by the date_operation column
 * @method     ChildLog findOneByObservation(string $observation) Return the first ChildLog filtered by the observation column *

 * @method     ChildLog requirePk($key, ConnectionInterface $con = null) Return the ChildLog by primary key and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildLog requireOne(ConnectionInterface $con = null) Return the first ChildLog matching the query and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 *
 * @method     ChildLog requireOneByIdLog(string $id_log) Return the first ChildLog filtered by the id_log column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildLog requireOneByIdIndividu(string $id_individu) Return the first ChildLog filtered by the id_individu column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildLog requireOneByIdEntite(string $id_entite) Return the first ChildLog filtered by the id_entite column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildLog requireOneByCode(string $code) Return the first ChildLog filtered by the code column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildLog requireOneByVariables(string $variables) Return the first ChildLog filtered by the variables column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildLog requireOneByDateOperation(string $date_operation) Return the first ChildLog filtered by the date_operation column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildLog requireOneByObservation(string $observation) Return the first ChildLog filtered by the observation column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 *
 * @method     ChildLog[]|ObjectCollection find(ConnectionInterface $con = null) Return ChildLog objects based on current ModelCriteria
 * @method     ChildLog[]|ObjectCollection findByIdLog(string $id_log) Return ChildLog objects filtered by the id_log column
 * @method     ChildLog[]|ObjectCollection findByIdIndividu(string $id_individu) Return ChildLog objects filtered by the id_individu column
 * @method     ChildLog[]|ObjectCollection findByIdEntite(string $id_entite) Return ChildLog objects filtered by the id_entite column
 * @method     ChildLog[]|ObjectCollection findByCode(string $code) Return ChildLog objects filtered by the code column
 * @method     ChildLog[]|ObjectCollection findByVariables(string $variables) Return ChildLog objects filtered by the variables column
 * @method     ChildLog[]|ObjectCollection findByDateOperation(string $date_operation) Return ChildLog objects filtered by the date_operation column
 * @method     ChildLog[]|ObjectCollection findByObservation(string $observation) Return ChildLog objects filtered by the observation column
 * @method     ChildLog[]|\Propel\Runtime\Util\PropelModelPager paginate($page = 1, $maxPerPage = 10, ConnectionInterface $con = null) Issue a SELECT query based on the current ModelCriteria and uses a page and a maximum number of results per page to compute an offset and a limit
 *
 */
abstract class LogQuery extends ModelCriteria
{
    protected $entityNotFoundExceptionClass = '\\Propel\\Runtime\\Exception\\EntityNotFoundException';

    /**
     * Initializes internal state of \Base\LogQuery object.
     *
     * @param     string $dbName The database name
     * @param     string $modelName The phpName of a model, e.g. 'Book'
     * @param     string $modelAlias The alias for the model in this query, e.g. 'b'
     */
    public function __construct($dbName = 'simplasso', $modelName = '\\Log', $modelAlias = null)
    {
        parent::__construct($dbName, $modelName, $modelAlias);
    }

    /**
     * Returns a new ChildLogQuery object.
     *
     * @param     string $modelAlias The alias of a model in the query
     * @param     Criteria $criteria Optional Criteria to build the query from
     *
     * @return ChildLogQuery
     */
    public static function create($modelAlias = null, Criteria $criteria = null)
    {
        if ($criteria instanceof ChildLogQuery) {
            return $criteria;
        }
        $query = new ChildLogQuery();
        if (null !== $modelAlias) {
            $query->setModelAlias($modelAlias);
        }
        if ($criteria instanceof Criteria) {
            $query->mergeWith($criteria);
        }

        return $query;
    }

    /**
     * Find object by primary key.
     * Propel uses the instance pool to skip the database if the object exists.
     * Go fast if the query is untouched.
     *
     * <code>
     * $obj  = $c->findPk(12, $con);
     * </code>
     *
     * @param mixed $key Primary key to use for the query
     * @param ConnectionInterface $con an optional connection object
     *
     * @return ChildLog|array|mixed the result, formatted by the current formatter
     */
    public function findPk($key, ConnectionInterface $con = null)
    {
        if ($key === null) {
            return null;
        }

        if ($con === null) {
            $con = Propel::getServiceContainer()->getReadConnection(LogTableMap::DATABASE_NAME);
        }

        $this->basePreSelect($con);

        if (
            $this->formatter || $this->modelAlias || $this->with || $this->select
            || $this->selectColumns || $this->asColumns || $this->selectModifiers
            || $this->map || $this->having || $this->joins
        ) {
            return $this->findPkComplex($key, $con);
        }

        if ((null !== ($obj = LogTableMap::getInstanceFromPool(null === $key || is_scalar($key) || is_callable([$key, '__toString']) ? (string) $key : $key)))) {
            // the object is already in the instance pool
            return $obj;
        }

        return $this->findPkSimple($key, $con);
    }

    /**
     * Find object by primary key using raw SQL to go fast.
     * Bypass doSelect() and the object formatter by using generated code.
     *
     * @param     mixed $key Primary key to use for the query
     * @param     ConnectionInterface $con A connection object
     *
     * @throws \Propel\Runtime\Exception\PropelException
     *
     * @return ChildLog A model object, or null if the key is not found
     */
    protected function findPkSimple($key, ConnectionInterface $con)
    {
        $sql = 'SELECT `id_log`, `id_individu`, `id_entite`, `code`, `variables`, `date_operation`, `observation` FROM `asso_logs` WHERE `id_log` = :p0';
        try {
            $stmt = $con->prepare($sql);
            $stmt->bindValue(':p0', $key, PDO::PARAM_INT);
            $stmt->execute();
        } catch (Exception $e) {
            Propel::log($e->getMessage(), Propel::LOG_ERR);
            throw new PropelException(sprintf('Unable to execute SELECT statement [%s]', $sql), 0, $e);
        }
        $obj = null;
        if ($row = $stmt->fetch(\PDO::FETCH_NUM)) {
            /** @var ChildLog $obj */
            $obj = new ChildLog();
            $obj->hydrate($row);
            LogTableMap::addInstanceToPool($obj, null === $key || is_scalar($key) || is_callable([$key, '__toString']) ? (string) $key : $key);
        }
        $stmt->closeCursor();

        return $obj;
    }

    /**
     * Find object by primary key.
     *
     * @param     mixed $key Primary key to use for the query
     * @param     ConnectionInterface $con A connection object
     *
     * @return ChildLog|array|mixed the result, formatted by the current formatter
     */
    protected function findPkComplex($key, ConnectionInterface $con)
    {
        // As the query uses a PK condition, no limit(1) is necessary.
        $criteria = $this->isKeepQuery() ? clone $this : $this;
        $dataFetcher = $criteria
            ->filterByPrimaryKey($key)
            ->doSelect($con);

        return $criteria->getFormatter()->init($criteria)->formatOne($dataFetcher);
    }

    /**
     * Find objects by primary key
     * <code>
     * $objs = $c->findPks(array(12, 56, 832), $con);
     * </code>
     * @param     array $keys Primary keys to use for the query
     * @param     ConnectionInterface $con an optional connection object
     *
     * @return ObjectCollection|array|mixed the list of results, formatted by the current formatter
     */
    public function findPks($keys, ConnectionInterface $con = null)
    {
        if (null === $con) {
            $con = Propel::getServiceContainer()->getReadConnection($this->getDbName());
        }
        $this->basePreSelect($con);
        $criteria = $this->isKeepQuery() ? clone $this : $this;
        $dataFetcher = $criteria
            ->filterByPrimaryKeys($keys)
            ->doSelect($con);

        return $criteria->getFormatter()->init($criteria)->format($dataFetcher);
    }

    /**
     * Filter the query by primary key
     *
     * @param     mixed $key Primary key to use for the query
     *
     * @return $this|ChildLogQuery The current query, for fluid interface
     */
    public function filterByPrimaryKey($key)
    {

        return $this->addUsingAlias(LogTableMap::COL_ID_LOG, $key, Criteria::EQUAL);
    }

    /**
     * Filter the query by a list of primary keys
     *
     * @param     array $keys The list of primary key to use for the query
     *
     * @return $this|ChildLogQuery The current query, for fluid interface
     */
    public function filterByPrimaryKeys($keys)
    {

        return $this->addUsingAlias(LogTableMap::COL_ID_LOG, $keys, Criteria::IN);
    }

    /**
     * Filter the query on the id_log column
     *
     * Example usage:
     * <code>
     * $query->filterByIdLog(1234); // WHERE id_log = 1234
     * $query->filterByIdLog(array(12, 34)); // WHERE id_log IN (12, 34)
     * $query->filterByIdLog(array('min' => 12)); // WHERE id_log > 12
     * </code>
     *
     * @param     mixed $idLog The value to use as filter.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildLogQuery The current query, for fluid interface
     */
    public function filterByIdLog($idLog = null, $comparison = null)
    {
        if (is_array($idLog)) {
            $useMinMax = false;
            if (isset($idLog['min'])) {
                $this->addUsingAlias(LogTableMap::COL_ID_LOG, $idLog['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($idLog['max'])) {
                $this->addUsingAlias(LogTableMap::COL_ID_LOG, $idLog['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(LogTableMap::COL_ID_LOG, $idLog, $comparison);
    }

    /**
     * Filter the query on the id_individu column
     *
     * Example usage:
     * <code>
     * $query->filterByIdIndividu(1234); // WHERE id_individu = 1234
     * $query->filterByIdIndividu(array(12, 34)); // WHERE id_individu IN (12, 34)
     * $query->filterByIdIndividu(array('min' => 12)); // WHERE id_individu > 12
     * </code>
     *
     * @see       filterByIndividu()
     *
     * @param     mixed $idIndividu The value to use as filter.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildLogQuery The current query, for fluid interface
     */
    public function filterByIdIndividu($idIndividu = null, $comparison = null)
    {
        if (is_array($idIndividu)) {
            $useMinMax = false;
            if (isset($idIndividu['min'])) {
                $this->addUsingAlias(LogTableMap::COL_ID_INDIVIDU, $idIndividu['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($idIndividu['max'])) {
                $this->addUsingAlias(LogTableMap::COL_ID_INDIVIDU, $idIndividu['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(LogTableMap::COL_ID_INDIVIDU, $idIndividu, $comparison);
    }

    /**
     * Filter the query on the id_entite column
     *
     * Example usage:
     * <code>
     * $query->filterByIdEntite(1234); // WHERE id_entite = 1234
     * $query->filterByIdEntite(array(12, 34)); // WHERE id_entite IN (12, 34)
     * $query->filterByIdEntite(array('min' => 12)); // WHERE id_entite > 12
     * </code>
     *
     * @param     mixed $idEntite The value to use as filter.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildLogQuery The current query, for fluid interface
     */
    public function filterByIdEntite($idEntite = null, $comparison = null)
    {
        if (is_array($idEntite)) {
            $useMinMax = false;
            if (isset($idEntite['min'])) {
                $this->addUsingAlias(LogTableMap::COL_ID_ENTITE, $idEntite['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($idEntite['max'])) {
                $this->addUsingAlias(LogTableMap::COL_ID_ENTITE, $idEntite['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(LogTableMap::COL_ID_ENTITE, $idEntite, $comparison);
    }

    /**
     * Filter the query on the code column
     *
     * Example usage:
     * <code>
     * $query->filterByCode('fooValue');   // WHERE code = 'fooValue'
     * $query->filterByCode('%fooValue%', Criteria::LIKE); // WHERE code LIKE '%fooValue%'
     * </code>
     *
     * @param     string $code The value to use as filter.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildLogQuery The current query, for fluid interface
     */
    public function filterByCode($code = null, $comparison = null)
    {
        if (null === $comparison) {
            if (is_array($code)) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(LogTableMap::COL_CODE, $code, $comparison);
    }

    /**
     * Filter the query on the variables column
     *
     * Example usage:
     * <code>
     * $query->filterByVariables('fooValue');   // WHERE variables = 'fooValue'
     * $query->filterByVariables('%fooValue%', Criteria::LIKE); // WHERE variables LIKE '%fooValue%'
     * </code>
     *
     * @param     string $variables The value to use as filter.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildLogQuery The current query, for fluid interface
     */
    public function filterByVariables($variables = null, $comparison = null)
    {
        if (null === $comparison) {
            if (is_array($variables)) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(LogTableMap::COL_VARIABLES, $variables, $comparison);
    }

    /**
     * Filter the query on the date_operation column
     *
     * Example usage:
     * <code>
     * $query->filterByDateOperation('2011-03-14'); // WHERE date_operation = '2011-03-14'
     * $query->filterByDateOperation('now'); // WHERE date_operation = '2011-03-14'
     * $query->filterByDateOperation(array('max' => 'yesterday')); // WHERE date_operation > '2011-03-13'
     * </code>
     *
     * @param     mixed $dateOperation The value to use as filter.
     *              Values can be integers (unix timestamps), DateTime objects, or strings.
     *              Empty strings are treated as NULL.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildLogQuery The current query, for fluid interface
     */
    public function filterByDateOperation($dateOperation = null, $comparison = null)
    {
        if (is_array($dateOperation)) {
            $useMinMax = false;
            if (isset($dateOperation['min'])) {
                $this->addUsingAlias(LogTableMap::COL_DATE_OPERATION, $dateOperation['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($dateOperation['max'])) {
                $this->addUsingAlias(LogTableMap::COL_DATE_OPERATION, $dateOperation['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(LogTableMap::COL_DATE_OPERATION, $dateOperation, $comparison);
    }

    /**
     * Filter the query on the observation column
     *
     * Example usage:
     * <code>
     * $query->filterByObservation('fooValue');   // WHERE observation = 'fooValue'
     * $query->filterByObservation('%fooValue%', Criteria::LIKE); // WHERE observation LIKE '%fooValue%'
     * </code>
     *
     * @param     string $observation The value to use as filter.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildLogQuery The current query, for fluid interface
     */
    public function filterByObservation($observation = null, $comparison = null)
    {
        if (null === $comparison) {
            if (is_array($observation)) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(LogTableMap::COL_OBSERVATION, $observation, $comparison);
    }

    /**
     * Filter the query by a related \Individu object
     *
     * @param \Individu|ObjectCollection $individu The related object(s) to use as filter
     * @param string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @throws \Propel\Runtime\Exception\PropelException
     *
     * @return ChildLogQuery The current query, for fluid interface
     */
    public function filterByIndividu($individu, $comparison = null)
    {
        if ($individu instanceof \Individu) {
            return $this
                ->addUsingAlias(LogTableMap::COL_ID_INDIVIDU, $individu->getIdIndividu(), $comparison);
        } elseif ($individu instanceof ObjectCollection) {
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }

            return $this
                ->addUsingAlias(LogTableMap::COL_ID_INDIVIDU, $individu->toKeyValue('PrimaryKey', 'IdIndividu'), $comparison);
        } else {
            throw new PropelException('filterByIndividu() only accepts arguments of type \Individu or Collection');
        }
    }

    /**
     * Adds a JOIN clause to the query using the Individu relation
     *
     * @param     string $relationAlias optional alias for the relation
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return $this|ChildLogQuery The current query, for fluid interface
     */
    public function joinIndividu($relationAlias = null, $joinType = Criteria::INNER_JOIN)
    {
        $tableMap = $this->getTableMap();
        $relationMap = $tableMap->getRelation('Individu');

        // create a ModelJoin object for this join
        $join = new ModelJoin();
        $join->setJoinType($joinType);
        $join->setRelationMap($relationMap, $this->useAliasInSQL ? $this->getModelAlias() : null, $relationAlias);
        if ($previousJoin = $this->getPreviousJoin()) {
            $join->setPreviousJoin($previousJoin);
        }

        // add the ModelJoin to the current object
        if ($relationAlias) {
            $this->addAlias($relationAlias, $relationMap->getRightTable()->getName());
            $this->addJoinObject($join, $relationAlias);
        } else {
            $this->addJoinObject($join, 'Individu');
        }

        return $this;
    }

    /**
     * Use the Individu relation Individu object
     *
     * @see useQuery()
     *
     * @param     string $relationAlias optional alias for the relation,
     *                                   to be used as main alias in the secondary query
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return \IndividuQuery A secondary query class using the current class as primary query
     */
    public function useIndividuQuery($relationAlias = null, $joinType = Criteria::INNER_JOIN)
    {
        return $this
            ->joinIndividu($relationAlias, $joinType)
            ->useQuery($relationAlias ? $relationAlias : 'Individu', '\IndividuQuery');
    }

    /**
     * Filter the query by a related \LogLien object
     *
     * @param \LogLien|ObjectCollection $logLien the related object to use as filter
     * @param string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return ChildLogQuery The current query, for fluid interface
     */
    public function filterByLogLien($logLien, $comparison = null)
    {
        if ($logLien instanceof \LogLien) {
            return $this
                ->addUsingAlias(LogTableMap::COL_ID_LOG, $logLien->getIdLog(), $comparison);
        } elseif ($logLien instanceof ObjectCollection) {
            return $this
                ->useLogLienQuery()
                ->filterByPrimaryKeys($logLien->getPrimaryKeys())
                ->endUse();
        } else {
            throw new PropelException('filterByLogLien() only accepts arguments of type \LogLien or Collection');
        }
    }

    /**
     * Adds a JOIN clause to the query using the LogLien relation
     *
     * @param     string $relationAlias optional alias for the relation
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return $this|ChildLogQuery The current query, for fluid interface
     */
    public function joinLogLien($relationAlias = null, $joinType = Criteria::INNER_JOIN)
    {
        $tableMap = $this->getTableMap();
        $relationMap = $tableMap->getRelation('LogLien');

        // create a ModelJoin object for this join
        $join = new ModelJoin();
        $join->setJoinType($joinType);
        $join->setRelationMap($relationMap, $this->useAliasInSQL ? $this->getModelAlias() : null, $relationAlias);
        if ($previousJoin = $this->getPreviousJoin()) {
            $join->setPreviousJoin($previousJoin);
        }

        // add the ModelJoin to the current object
        if ($relationAlias) {
            $this->addAlias($relationAlias, $relationMap->getRightTable()->getName());
            $this->addJoinObject($join, $relationAlias);
        } else {
            $this->addJoinObject($join, 'LogLien');
        }

        return $this;
    }

    /**
     * Use the LogLien relation LogLien object
     *
     * @see useQuery()
     *
     * @param     string $relationAlias optional alias for the relation,
     *                                   to be used as main alias in the secondary query
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return \LogLienQuery A secondary query class using the current class as primary query
     */
    public function useLogLienQuery($relationAlias = null, $joinType = Criteria::INNER_JOIN)
    {
        return $this
            ->joinLogLien($relationAlias, $joinType)
            ->useQuery($relationAlias ? $relationAlias : 'LogLien', '\LogLienQuery');
    }

    /**
     * Exclude object from result
     *
     * @param   ChildLog $log Object to remove from the list of results
     *
     * @return $this|ChildLogQuery The current query, for fluid interface
     */
    public function prune($log = null)
    {
        if ($log) {
            $this->addUsingAlias(LogTableMap::COL_ID_LOG, $log->getIdLog(), Criteria::NOT_EQUAL);
        }

        return $this;
    }

    /**
     * Deletes all rows from the asso_logs table.
     *
     * @param ConnectionInterface $con the connection to use
     * @return int The number of affected rows (if supported by underlying database driver).
     */
    public function doDeleteAll(ConnectionInterface $con = null)
    {
        if (null === $con) {
            $con = Propel::getServiceContainer()->getWriteConnection(LogTableMap::DATABASE_NAME);
        }

        // use transaction because $criteria could contain info
        // for more than one table or we could emulating ON DELETE CASCADE, etc.
        return $con->transaction(function () use ($con) {
            $affectedRows = 0; // initialize var to track total num of affected rows
            $affectedRows += parent::doDeleteAll($con);
            // Because this db requires some delete cascade/set null emulation, we have to
            // clear the cached instance *after* the emulation has happened (since
            // instances get re-added by the select statement contained therein).
            LogTableMap::clearInstancePool();
            LogTableMap::clearRelatedInstancePool();

            return $affectedRows;
        });
    }

    /**
     * Performs a DELETE on the database based on the current ModelCriteria
     *
     * @param ConnectionInterface $con the connection to use
     * @return int             The number of affected rows (if supported by underlying database driver).  This includes CASCADE-related rows
     *                         if supported by native driver or if emulated using Propel.
     * @throws PropelException Any exceptions caught during processing will be
     *                         rethrown wrapped into a PropelException.
     */
    public function delete(ConnectionInterface $con = null)
    {
        if (null === $con) {
            $con = Propel::getServiceContainer()->getWriteConnection(LogTableMap::DATABASE_NAME);
        }

        $criteria = $this;

        // Set the correct dbName
        $criteria->setDbName(LogTableMap::DATABASE_NAME);

        // use transaction because $criteria could contain info
        // for more than one table or we could emulating ON DELETE CASCADE, etc.
        return $con->transaction(function () use ($con, $criteria) {
            $affectedRows = 0; // initialize var to track total num of affected rows

            LogTableMap::removeInstanceFromPool($criteria);

            $affectedRows += ModelCriteria::delete($con);
            LogTableMap::clearRelatedInstancePool();

            return $affectedRows;
        });
    }

} // LogQuery
