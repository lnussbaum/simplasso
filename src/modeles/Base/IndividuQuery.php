<?php

namespace Base;

use \AssoIndividusArchive as ChildAssoIndividusArchive;
use \Individu as ChildIndividu;
use \IndividuQuery as ChildIndividuQuery;
use \Exception;
use \PDO;
use Map\IndividuTableMap;
use Propel\Runtime\Propel;
use Propel\Runtime\ActiveQuery\Criteria;
use Propel\Runtime\ActiveQuery\ModelCriteria;
use Propel\Runtime\ActiveQuery\ModelJoin;
use Propel\Runtime\Collection\ObjectCollection;
use Propel\Runtime\Connection\ConnectionInterface;
use Propel\Runtime\Exception\PropelException;

/**
 * Base class that represents a query for the 'asso_individus' table.
 *
 *
 *
 * @method     ChildIndividuQuery orderByIdIndividu($order = Criteria::ASC) Order by the id_individu column
 * @method     ChildIndividuQuery orderByNom($order = Criteria::ASC) Order by the nom column
 * @method     ChildIndividuQuery orderByBio($order = Criteria::ASC) Order by the bio column
 * @method     ChildIndividuQuery orderByEmail($order = Criteria::ASC) Order by the email column
 * @method     ChildIndividuQuery orderByLogin($order = Criteria::ASC) Order by the login column
 * @method     ChildIndividuQuery orderByPass($order = Criteria::ASC) Order by the pass column
 * @method     ChildIndividuQuery orderByAleaActuel($order = Criteria::ASC) Order by the alea_actuel column
 * @method     ChildIndividuQuery orderByAleaFutur($order = Criteria::ASC) Order by the alea_futur column
 * @method     ChildIndividuQuery orderByToken($order = Criteria::ASC) Order by the token column
 * @method     ChildIndividuQuery orderByTokenTime($order = Criteria::ASC) Order by the token_time column
 * @method     ChildIndividuQuery orderByCivilite($order = Criteria::ASC) Order by the civilite column
 * @method     ChildIndividuQuery orderBySexe($order = Criteria::ASC) Order by the sexe column
 * @method     ChildIndividuQuery orderByNomFamille($order = Criteria::ASC) Order by the nom_famille column
 * @method     ChildIndividuQuery orderByPrenom($order = Criteria::ASC) Order by the prenom column
 * @method     ChildIndividuQuery orderByNaissance($order = Criteria::ASC) Order by the naissance column
 * @method     ChildIndividuQuery orderByAdresse($order = Criteria::ASC) Order by the adresse column
 * @method     ChildIndividuQuery orderByCodepostal($order = Criteria::ASC) Order by the codepostal column
 * @method     ChildIndividuQuery orderByVille($order = Criteria::ASC) Order by the ville column
 * @method     ChildIndividuQuery orderByPays($order = Criteria::ASC) Order by the pays column
 * @method     ChildIndividuQuery orderByTelephone($order = Criteria::ASC) Order by the telephone column
 * @method     ChildIndividuQuery orderByTelephonePro($order = Criteria::ASC) Order by the telephone_pro column
 * @method     ChildIndividuQuery orderByFax($order = Criteria::ASC) Order by the fax column
 * @method     ChildIndividuQuery orderByMobile($order = Criteria::ASC) Order by the mobile column
 * @method     ChildIndividuQuery orderByUrl($order = Criteria::ASC) Order by the url column
 * @method     ChildIndividuQuery orderByProfession($order = Criteria::ASC) Order by the profession column
 * @method     ChildIndividuQuery orderByContactSouhait($order = Criteria::ASC) Order by the contact_souhait column
 * @method     ChildIndividuQuery orderByObservation($order = Criteria::ASC) Order by the observation column
 * @method     ChildIndividuQuery orderByCreatedAt($order = Criteria::ASC) Order by the created_at column
 * @method     ChildIndividuQuery orderByUpdatedAt($order = Criteria::ASC) Order by the updated_at column
 *
 * @method     ChildIndividuQuery groupByIdIndividu() Group by the id_individu column
 * @method     ChildIndividuQuery groupByNom() Group by the nom column
 * @method     ChildIndividuQuery groupByBio() Group by the bio column
 * @method     ChildIndividuQuery groupByEmail() Group by the email column
 * @method     ChildIndividuQuery groupByLogin() Group by the login column
 * @method     ChildIndividuQuery groupByPass() Group by the pass column
 * @method     ChildIndividuQuery groupByAleaActuel() Group by the alea_actuel column
 * @method     ChildIndividuQuery groupByAleaFutur() Group by the alea_futur column
 * @method     ChildIndividuQuery groupByToken() Group by the token column
 * @method     ChildIndividuQuery groupByTokenTime() Group by the token_time column
 * @method     ChildIndividuQuery groupByCivilite() Group by the civilite column
 * @method     ChildIndividuQuery groupBySexe() Group by the sexe column
 * @method     ChildIndividuQuery groupByNomFamille() Group by the nom_famille column
 * @method     ChildIndividuQuery groupByPrenom() Group by the prenom column
 * @method     ChildIndividuQuery groupByNaissance() Group by the naissance column
 * @method     ChildIndividuQuery groupByAdresse() Group by the adresse column
 * @method     ChildIndividuQuery groupByCodepostal() Group by the codepostal column
 * @method     ChildIndividuQuery groupByVille() Group by the ville column
 * @method     ChildIndividuQuery groupByPays() Group by the pays column
 * @method     ChildIndividuQuery groupByTelephone() Group by the telephone column
 * @method     ChildIndividuQuery groupByTelephonePro() Group by the telephone_pro column
 * @method     ChildIndividuQuery groupByFax() Group by the fax column
 * @method     ChildIndividuQuery groupByMobile() Group by the mobile column
 * @method     ChildIndividuQuery groupByUrl() Group by the url column
 * @method     ChildIndividuQuery groupByProfession() Group by the profession column
 * @method     ChildIndividuQuery groupByContactSouhait() Group by the contact_souhait column
 * @method     ChildIndividuQuery groupByObservation() Group by the observation column
 * @method     ChildIndividuQuery groupByCreatedAt() Group by the created_at column
 * @method     ChildIndividuQuery groupByUpdatedAt() Group by the updated_at column
 *
 * @method     ChildIndividuQuery leftJoin($relation) Adds a LEFT JOIN clause to the query
 * @method     ChildIndividuQuery rightJoin($relation) Adds a RIGHT JOIN clause to the query
 * @method     ChildIndividuQuery innerJoin($relation) Adds a INNER JOIN clause to the query
 *
 * @method     ChildIndividuQuery leftJoinWith($relation) Adds a LEFT JOIN clause and with to the query
 * @method     ChildIndividuQuery rightJoinWith($relation) Adds a RIGHT JOIN clause and with to the query
 * @method     ChildIndividuQuery innerJoinWith($relation) Adds a INNER JOIN clause and with to the query
 *
 * @method     ChildIndividuQuery leftJoinAutorisation($relationAlias = null) Adds a LEFT JOIN clause to the query using the Autorisation relation
 * @method     ChildIndividuQuery rightJoinAutorisation($relationAlias = null) Adds a RIGHT JOIN clause to the query using the Autorisation relation
 * @method     ChildIndividuQuery innerJoinAutorisation($relationAlias = null) Adds a INNER JOIN clause to the query using the Autorisation relation
 *
 * @method     ChildIndividuQuery joinWithAutorisation($joinType = Criteria::INNER_JOIN) Adds a join clause and with to the query using the Autorisation relation
 *
 * @method     ChildIndividuQuery leftJoinWithAutorisation() Adds a LEFT JOIN clause and with to the query using the Autorisation relation
 * @method     ChildIndividuQuery rightJoinWithAutorisation() Adds a RIGHT JOIN clause and with to the query using the Autorisation relation
 * @method     ChildIndividuQuery innerJoinWithAutorisation() Adds a INNER JOIN clause and with to the query using the Autorisation relation
 *
 * @method     ChildIndividuQuery leftJoinLog($relationAlias = null) Adds a LEFT JOIN clause to the query using the Log relation
 * @method     ChildIndividuQuery rightJoinLog($relationAlias = null) Adds a RIGHT JOIN clause to the query using the Log relation
 * @method     ChildIndividuQuery innerJoinLog($relationAlias = null) Adds a INNER JOIN clause to the query using the Log relation
 *
 * @method     ChildIndividuQuery joinWithLog($joinType = Criteria::INNER_JOIN) Adds a join clause and with to the query using the Log relation
 *
 * @method     ChildIndividuQuery leftJoinWithLog() Adds a LEFT JOIN clause and with to the query using the Log relation
 * @method     ChildIndividuQuery rightJoinWithLog() Adds a RIGHT JOIN clause and with to the query using the Log relation
 * @method     ChildIndividuQuery innerJoinWithLog() Adds a INNER JOIN clause and with to the query using the Log relation
 *
 * @method     ChildIndividuQuery leftJoinMembres0($relationAlias = null) Adds a LEFT JOIN clause to the query using the Membres0 relation
 * @method     ChildIndividuQuery rightJoinMembres0($relationAlias = null) Adds a RIGHT JOIN clause to the query using the Membres0 relation
 * @method     ChildIndividuQuery innerJoinMembres0($relationAlias = null) Adds a INNER JOIN clause to the query using the Membres0 relation
 *
 * @method     ChildIndividuQuery joinWithMembres0($joinType = Criteria::INNER_JOIN) Adds a join clause and with to the query using the Membres0 relation
 *
 * @method     ChildIndividuQuery leftJoinWithMembres0() Adds a LEFT JOIN clause and with to the query using the Membres0 relation
 * @method     ChildIndividuQuery rightJoinWithMembres0() Adds a RIGHT JOIN clause and with to the query using the Membres0 relation
 * @method     ChildIndividuQuery innerJoinWithMembres0() Adds a INNER JOIN clause and with to the query using the Membres0 relation
 *
 * @method     ChildIndividuQuery leftJoinMembreIndividu($relationAlias = null) Adds a LEFT JOIN clause to the query using the MembreIndividu relation
 * @method     ChildIndividuQuery rightJoinMembreIndividu($relationAlias = null) Adds a RIGHT JOIN clause to the query using the MembreIndividu relation
 * @method     ChildIndividuQuery innerJoinMembreIndividu($relationAlias = null) Adds a INNER JOIN clause to the query using the MembreIndividu relation
 *
 * @method     ChildIndividuQuery joinWithMembreIndividu($joinType = Criteria::INNER_JOIN) Adds a join clause and with to the query using the MembreIndividu relation
 *
 * @method     ChildIndividuQuery leftJoinWithMembreIndividu() Adds a LEFT JOIN clause and with to the query using the MembreIndividu relation
 * @method     ChildIndividuQuery rightJoinWithMembreIndividu() Adds a RIGHT JOIN clause and with to the query using the MembreIndividu relation
 * @method     ChildIndividuQuery innerJoinWithMembreIndividu() Adds a INNER JOIN clause and with to the query using the MembreIndividu relation
 *
 * @method     ChildIndividuQuery leftJoinServicerenduIndividu($relationAlias = null) Adds a LEFT JOIN clause to the query using the ServicerenduIndividu relation
 * @method     ChildIndividuQuery rightJoinServicerenduIndividu($relationAlias = null) Adds a RIGHT JOIN clause to the query using the ServicerenduIndividu relation
 * @method     ChildIndividuQuery innerJoinServicerenduIndividu($relationAlias = null) Adds a INNER JOIN clause to the query using the ServicerenduIndividu relation
 *
 * @method     ChildIndividuQuery joinWithServicerenduIndividu($joinType = Criteria::INNER_JOIN) Adds a join clause and with to the query using the ServicerenduIndividu relation
 *
 * @method     ChildIndividuQuery leftJoinWithServicerenduIndividu() Adds a LEFT JOIN clause and with to the query using the ServicerenduIndividu relation
 * @method     ChildIndividuQuery rightJoinWithServicerenduIndividu() Adds a RIGHT JOIN clause and with to the query using the ServicerenduIndividu relation
 * @method     ChildIndividuQuery innerJoinWithServicerenduIndividu() Adds a INNER JOIN clause and with to the query using the ServicerenduIndividu relation
 *
 * @method     ChildIndividuQuery leftJoinNotificationIndividu($relationAlias = null) Adds a LEFT JOIN clause to the query using the NotificationIndividu relation
 * @method     ChildIndividuQuery rightJoinNotificationIndividu($relationAlias = null) Adds a RIGHT JOIN clause to the query using the NotificationIndividu relation
 * @method     ChildIndividuQuery innerJoinNotificationIndividu($relationAlias = null) Adds a INNER JOIN clause to the query using the NotificationIndividu relation
 *
 * @method     ChildIndividuQuery joinWithNotificationIndividu($joinType = Criteria::INNER_JOIN) Adds a join clause and with to the query using the NotificationIndividu relation
 *
 * @method     ChildIndividuQuery leftJoinWithNotificationIndividu() Adds a LEFT JOIN clause and with to the query using the NotificationIndividu relation
 * @method     ChildIndividuQuery rightJoinWithNotificationIndividu() Adds a RIGHT JOIN clause and with to the query using the NotificationIndividu relation
 * @method     ChildIndividuQuery innerJoinWithNotificationIndividu() Adds a INNER JOIN clause and with to the query using the NotificationIndividu relation
 *
 * @method     ChildIndividuQuery leftJoinCourrierLien($relationAlias = null) Adds a LEFT JOIN clause to the query using the CourrierLien relation
 * @method     ChildIndividuQuery rightJoinCourrierLien($relationAlias = null) Adds a RIGHT JOIN clause to the query using the CourrierLien relation
 * @method     ChildIndividuQuery innerJoinCourrierLien($relationAlias = null) Adds a INNER JOIN clause to the query using the CourrierLien relation
 *
 * @method     ChildIndividuQuery joinWithCourrierLien($joinType = Criteria::INNER_JOIN) Adds a join clause and with to the query using the CourrierLien relation
 *
 * @method     ChildIndividuQuery leftJoinWithCourrierLien() Adds a LEFT JOIN clause and with to the query using the CourrierLien relation
 * @method     ChildIndividuQuery rightJoinWithCourrierLien() Adds a RIGHT JOIN clause and with to the query using the CourrierLien relation
 * @method     ChildIndividuQuery innerJoinWithCourrierLien() Adds a INNER JOIN clause and with to the query using the CourrierLien relation
 *
 * @method     \AutorisationQuery|\LogQuery|\MembreQuery|\MembreIndividuQuery|\ServicerenduIndividuQuery|\NotificationIndividuQuery|\CourrierLienQuery endUse() Finalizes a secondary criteria and merges it with its primary Criteria
 *
 * @method     ChildIndividu findOne(ConnectionInterface $con = null) Return the first ChildIndividu matching the query
 * @method     ChildIndividu findOneOrCreate(ConnectionInterface $con = null) Return the first ChildIndividu matching the query, or a new ChildIndividu object populated from the query conditions when no match is found
 *
 * @method     ChildIndividu findOneByIdIndividu(string $id_individu) Return the first ChildIndividu filtered by the id_individu column
 * @method     ChildIndividu findOneByNom(string $nom) Return the first ChildIndividu filtered by the nom column
 * @method     ChildIndividu findOneByBio(string $bio) Return the first ChildIndividu filtered by the bio column
 * @method     ChildIndividu findOneByEmail(string $email) Return the first ChildIndividu filtered by the email column
 * @method     ChildIndividu findOneByLogin(string $login) Return the first ChildIndividu filtered by the login column
 * @method     ChildIndividu findOneByPass(string $pass) Return the first ChildIndividu filtered by the pass column
 * @method     ChildIndividu findOneByAleaActuel(string $alea_actuel) Return the first ChildIndividu filtered by the alea_actuel column
 * @method     ChildIndividu findOneByAleaFutur(string $alea_futur) Return the first ChildIndividu filtered by the alea_futur column
 * @method     ChildIndividu findOneByToken(string $token) Return the first ChildIndividu filtered by the token column
 * @method     ChildIndividu findOneByTokenTime(string $token_time) Return the first ChildIndividu filtered by the token_time column
 * @method     ChildIndividu findOneByCivilite(string $civilite) Return the first ChildIndividu filtered by the civilite column
 * @method     ChildIndividu findOneBySexe(string $sexe) Return the first ChildIndividu filtered by the sexe column
 * @method     ChildIndividu findOneByNomFamille(string $nom_famille) Return the first ChildIndividu filtered by the nom_famille column
 * @method     ChildIndividu findOneByPrenom(string $prenom) Return the first ChildIndividu filtered by the prenom column
 * @method     ChildIndividu findOneByNaissance(string $naissance) Return the first ChildIndividu filtered by the naissance column
 * @method     ChildIndividu findOneByAdresse(string $adresse) Return the first ChildIndividu filtered by the adresse column
 * @method     ChildIndividu findOneByCodepostal(string $codepostal) Return the first ChildIndividu filtered by the codepostal column
 * @method     ChildIndividu findOneByVille(string $ville) Return the first ChildIndividu filtered by the ville column
 * @method     ChildIndividu findOneByPays(string $pays) Return the first ChildIndividu filtered by the pays column
 * @method     ChildIndividu findOneByTelephone(string $telephone) Return the first ChildIndividu filtered by the telephone column
 * @method     ChildIndividu findOneByTelephonePro(string $telephone_pro) Return the first ChildIndividu filtered by the telephone_pro column
 * @method     ChildIndividu findOneByFax(string $fax) Return the first ChildIndividu filtered by the fax column
 * @method     ChildIndividu findOneByMobile(string $mobile) Return the first ChildIndividu filtered by the mobile column
 * @method     ChildIndividu findOneByUrl(string $url) Return the first ChildIndividu filtered by the url column
 * @method     ChildIndividu findOneByProfession(string $profession) Return the first ChildIndividu filtered by the profession column
 * @method     ChildIndividu findOneByContactSouhait(boolean $contact_souhait) Return the first ChildIndividu filtered by the contact_souhait column
 * @method     ChildIndividu findOneByObservation(string $observation) Return the first ChildIndividu filtered by the observation column
 * @method     ChildIndividu findOneByCreatedAt(string $created_at) Return the first ChildIndividu filtered by the created_at column
 * @method     ChildIndividu findOneByUpdatedAt(string $updated_at) Return the first ChildIndividu filtered by the updated_at column *

 * @method     ChildIndividu requirePk($key, ConnectionInterface $con = null) Return the ChildIndividu by primary key and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildIndividu requireOne(ConnectionInterface $con = null) Return the first ChildIndividu matching the query and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 *
 * @method     ChildIndividu requireOneByIdIndividu(string $id_individu) Return the first ChildIndividu filtered by the id_individu column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildIndividu requireOneByNom(string $nom) Return the first ChildIndividu filtered by the nom column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildIndividu requireOneByBio(string $bio) Return the first ChildIndividu filtered by the bio column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildIndividu requireOneByEmail(string $email) Return the first ChildIndividu filtered by the email column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildIndividu requireOneByLogin(string $login) Return the first ChildIndividu filtered by the login column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildIndividu requireOneByPass(string $pass) Return the first ChildIndividu filtered by the pass column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildIndividu requireOneByAleaActuel(string $alea_actuel) Return the first ChildIndividu filtered by the alea_actuel column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildIndividu requireOneByAleaFutur(string $alea_futur) Return the first ChildIndividu filtered by the alea_futur column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildIndividu requireOneByToken(string $token) Return the first ChildIndividu filtered by the token column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildIndividu requireOneByTokenTime(string $token_time) Return the first ChildIndividu filtered by the token_time column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildIndividu requireOneByCivilite(string $civilite) Return the first ChildIndividu filtered by the civilite column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildIndividu requireOneBySexe(string $sexe) Return the first ChildIndividu filtered by the sexe column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildIndividu requireOneByNomFamille(string $nom_famille) Return the first ChildIndividu filtered by the nom_famille column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildIndividu requireOneByPrenom(string $prenom) Return the first ChildIndividu filtered by the prenom column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildIndividu requireOneByNaissance(string $naissance) Return the first ChildIndividu filtered by the naissance column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildIndividu requireOneByAdresse(string $adresse) Return the first ChildIndividu filtered by the adresse column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildIndividu requireOneByCodepostal(string $codepostal) Return the first ChildIndividu filtered by the codepostal column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildIndividu requireOneByVille(string $ville) Return the first ChildIndividu filtered by the ville column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildIndividu requireOneByPays(string $pays) Return the first ChildIndividu filtered by the pays column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildIndividu requireOneByTelephone(string $telephone) Return the first ChildIndividu filtered by the telephone column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildIndividu requireOneByTelephonePro(string $telephone_pro) Return the first ChildIndividu filtered by the telephone_pro column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildIndividu requireOneByFax(string $fax) Return the first ChildIndividu filtered by the fax column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildIndividu requireOneByMobile(string $mobile) Return the first ChildIndividu filtered by the mobile column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildIndividu requireOneByUrl(string $url) Return the first ChildIndividu filtered by the url column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildIndividu requireOneByProfession(string $profession) Return the first ChildIndividu filtered by the profession column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildIndividu requireOneByContactSouhait(boolean $contact_souhait) Return the first ChildIndividu filtered by the contact_souhait column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildIndividu requireOneByObservation(string $observation) Return the first ChildIndividu filtered by the observation column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildIndividu requireOneByCreatedAt(string $created_at) Return the first ChildIndividu filtered by the created_at column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildIndividu requireOneByUpdatedAt(string $updated_at) Return the first ChildIndividu filtered by the updated_at column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 *
 * @method     ChildIndividu[]|ObjectCollection find(ConnectionInterface $con = null) Return ChildIndividu objects based on current ModelCriteria
 * @method     ChildIndividu[]|ObjectCollection findByIdIndividu(string $id_individu) Return ChildIndividu objects filtered by the id_individu column
 * @method     ChildIndividu[]|ObjectCollection findByNom(string $nom) Return ChildIndividu objects filtered by the nom column
 * @method     ChildIndividu[]|ObjectCollection findByBio(string $bio) Return ChildIndividu objects filtered by the bio column
 * @method     ChildIndividu[]|ObjectCollection findByEmail(string $email) Return ChildIndividu objects filtered by the email column
 * @method     ChildIndividu[]|ObjectCollection findByLogin(string $login) Return ChildIndividu objects filtered by the login column
 * @method     ChildIndividu[]|ObjectCollection findByPass(string $pass) Return ChildIndividu objects filtered by the pass column
 * @method     ChildIndividu[]|ObjectCollection findByAleaActuel(string $alea_actuel) Return ChildIndividu objects filtered by the alea_actuel column
 * @method     ChildIndividu[]|ObjectCollection findByAleaFutur(string $alea_futur) Return ChildIndividu objects filtered by the alea_futur column
 * @method     ChildIndividu[]|ObjectCollection findByToken(string $token) Return ChildIndividu objects filtered by the token column
 * @method     ChildIndividu[]|ObjectCollection findByTokenTime(string $token_time) Return ChildIndividu objects filtered by the token_time column
 * @method     ChildIndividu[]|ObjectCollection findByCivilite(string $civilite) Return ChildIndividu objects filtered by the civilite column
 * @method     ChildIndividu[]|ObjectCollection findBySexe(string $sexe) Return ChildIndividu objects filtered by the sexe column
 * @method     ChildIndividu[]|ObjectCollection findByNomFamille(string $nom_famille) Return ChildIndividu objects filtered by the nom_famille column
 * @method     ChildIndividu[]|ObjectCollection findByPrenom(string $prenom) Return ChildIndividu objects filtered by the prenom column
 * @method     ChildIndividu[]|ObjectCollection findByNaissance(string $naissance) Return ChildIndividu objects filtered by the naissance column
 * @method     ChildIndividu[]|ObjectCollection findByAdresse(string $adresse) Return ChildIndividu objects filtered by the adresse column
 * @method     ChildIndividu[]|ObjectCollection findByCodepostal(string $codepostal) Return ChildIndividu objects filtered by the codepostal column
 * @method     ChildIndividu[]|ObjectCollection findByVille(string $ville) Return ChildIndividu objects filtered by the ville column
 * @method     ChildIndividu[]|ObjectCollection findByPays(string $pays) Return ChildIndividu objects filtered by the pays column
 * @method     ChildIndividu[]|ObjectCollection findByTelephone(string $telephone) Return ChildIndividu objects filtered by the telephone column
 * @method     ChildIndividu[]|ObjectCollection findByTelephonePro(string $telephone_pro) Return ChildIndividu objects filtered by the telephone_pro column
 * @method     ChildIndividu[]|ObjectCollection findByFax(string $fax) Return ChildIndividu objects filtered by the fax column
 * @method     ChildIndividu[]|ObjectCollection findByMobile(string $mobile) Return ChildIndividu objects filtered by the mobile column
 * @method     ChildIndividu[]|ObjectCollection findByUrl(string $url) Return ChildIndividu objects filtered by the url column
 * @method     ChildIndividu[]|ObjectCollection findByProfession(string $profession) Return ChildIndividu objects filtered by the profession column
 * @method     ChildIndividu[]|ObjectCollection findByContactSouhait(boolean $contact_souhait) Return ChildIndividu objects filtered by the contact_souhait column
 * @method     ChildIndividu[]|ObjectCollection findByObservation(string $observation) Return ChildIndividu objects filtered by the observation column
 * @method     ChildIndividu[]|ObjectCollection findByCreatedAt(string $created_at) Return ChildIndividu objects filtered by the created_at column
 * @method     ChildIndividu[]|ObjectCollection findByUpdatedAt(string $updated_at) Return ChildIndividu objects filtered by the updated_at column
 * @method     ChildIndividu[]|\Propel\Runtime\Util\PropelModelPager paginate($page = 1, $maxPerPage = 10, ConnectionInterface $con = null) Issue a SELECT query based on the current ModelCriteria and uses a page and a maximum number of results per page to compute an offset and a limit
 *
 */
abstract class IndividuQuery extends ModelCriteria
{

    // archivable behavior
    protected $archiveOnDelete = true;
protected $entityNotFoundExceptionClass = '\\Propel\\Runtime\\Exception\\EntityNotFoundException';

    /**
     * Initializes internal state of \Base\IndividuQuery object.
     *
     * @param     string $dbName The database name
     * @param     string $modelName The phpName of a model, e.g. 'Book'
     * @param     string $modelAlias The alias for the model in this query, e.g. 'b'
     */
    public function __construct($dbName = 'simplasso', $modelName = '\\Individu', $modelAlias = null)
    {
        parent::__construct($dbName, $modelName, $modelAlias);
    }

    /**
     * Returns a new ChildIndividuQuery object.
     *
     * @param     string $modelAlias The alias of a model in the query
     * @param     Criteria $criteria Optional Criteria to build the query from
     *
     * @return ChildIndividuQuery
     */
    public static function create($modelAlias = null, Criteria $criteria = null)
    {
        if ($criteria instanceof ChildIndividuQuery) {
            return $criteria;
        }
        $query = new ChildIndividuQuery();
        if (null !== $modelAlias) {
            $query->setModelAlias($modelAlias);
        }
        if ($criteria instanceof Criteria) {
            $query->mergeWith($criteria);
        }

        return $query;
    }

    /**
     * Find object by primary key.
     * Propel uses the instance pool to skip the database if the object exists.
     * Go fast if the query is untouched.
     *
     * <code>
     * $obj  = $c->findPk(12, $con);
     * </code>
     *
     * @param mixed $key Primary key to use for the query
     * @param ConnectionInterface $con an optional connection object
     *
     * @return ChildIndividu|array|mixed the result, formatted by the current formatter
     */
    public function findPk($key, ConnectionInterface $con = null)
    {
        if ($key === null) {
            return null;
        }

        if ($con === null) {
            $con = Propel::getServiceContainer()->getReadConnection(IndividuTableMap::DATABASE_NAME);
        }

        $this->basePreSelect($con);

        if (
            $this->formatter || $this->modelAlias || $this->with || $this->select
            || $this->selectColumns || $this->asColumns || $this->selectModifiers
            || $this->map || $this->having || $this->joins
        ) {
            return $this->findPkComplex($key, $con);
        }

        if ((null !== ($obj = IndividuTableMap::getInstanceFromPool(null === $key || is_scalar($key) || is_callable([$key, '__toString']) ? (string) $key : $key)))) {
            // the object is already in the instance pool
            return $obj;
        }

        return $this->findPkSimple($key, $con);
    }

    /**
     * Find object by primary key using raw SQL to go fast.
     * Bypass doSelect() and the object formatter by using generated code.
     *
     * @param     mixed $key Primary key to use for the query
     * @param     ConnectionInterface $con A connection object
     *
     * @throws \Propel\Runtime\Exception\PropelException
     *
     * @return ChildIndividu A model object, or null if the key is not found
     */
    protected function findPkSimple($key, ConnectionInterface $con)
    {
        $sql = 'SELECT `id_individu`, `nom`, `bio`, `email`, `login`, `pass`, `alea_actuel`, `alea_futur`, `token`, `token_time`, `civilite`, `sexe`, `nom_famille`, `prenom`, `naissance`, `adresse`, `codepostal`, `ville`, `pays`, `telephone`, `telephone_pro`, `fax`, `mobile`, `url`, `profession`, `contact_souhait`, `observation`, `created_at`, `updated_at` FROM `asso_individus` WHERE `id_individu` = :p0';
        try {
            $stmt = $con->prepare($sql);
            $stmt->bindValue(':p0', $key, PDO::PARAM_INT);
            $stmt->execute();
        } catch (Exception $e) {
            Propel::log($e->getMessage(), Propel::LOG_ERR);
            throw new PropelException(sprintf('Unable to execute SELECT statement [%s]', $sql), 0, $e);
        }
        $obj = null;
        if ($row = $stmt->fetch(\PDO::FETCH_NUM)) {
            /** @var ChildIndividu $obj */
            $obj = new ChildIndividu();
            $obj->hydrate($row);
            IndividuTableMap::addInstanceToPool($obj, null === $key || is_scalar($key) || is_callable([$key, '__toString']) ? (string) $key : $key);
        }
        $stmt->closeCursor();

        return $obj;
    }

    /**
     * Find object by primary key.
     *
     * @param     mixed $key Primary key to use for the query
     * @param     ConnectionInterface $con A connection object
     *
     * @return ChildIndividu|array|mixed the result, formatted by the current formatter
     */
    protected function findPkComplex($key, ConnectionInterface $con)
    {
        // As the query uses a PK condition, no limit(1) is necessary.
        $criteria = $this->isKeepQuery() ? clone $this : $this;
        $dataFetcher = $criteria
            ->filterByPrimaryKey($key)
            ->doSelect($con);

        return $criteria->getFormatter()->init($criteria)->formatOne($dataFetcher);
    }

    /**
     * Find objects by primary key
     * <code>
     * $objs = $c->findPks(array(12, 56, 832), $con);
     * </code>
     * @param     array $keys Primary keys to use for the query
     * @param     ConnectionInterface $con an optional connection object
     *
     * @return ObjectCollection|array|mixed the list of results, formatted by the current formatter
     */
    public function findPks($keys, ConnectionInterface $con = null)
    {
        if (null === $con) {
            $con = Propel::getServiceContainer()->getReadConnection($this->getDbName());
        }
        $this->basePreSelect($con);
        $criteria = $this->isKeepQuery() ? clone $this : $this;
        $dataFetcher = $criteria
            ->filterByPrimaryKeys($keys)
            ->doSelect($con);

        return $criteria->getFormatter()->init($criteria)->format($dataFetcher);
    }

    /**
     * Filter the query by primary key
     *
     * @param     mixed $key Primary key to use for the query
     *
     * @return $this|ChildIndividuQuery The current query, for fluid interface
     */
    public function filterByPrimaryKey($key)
    {

        return $this->addUsingAlias(IndividuTableMap::COL_ID_INDIVIDU, $key, Criteria::EQUAL);
    }

    /**
     * Filter the query by a list of primary keys
     *
     * @param     array $keys The list of primary key to use for the query
     *
     * @return $this|ChildIndividuQuery The current query, for fluid interface
     */
    public function filterByPrimaryKeys($keys)
    {

        return $this->addUsingAlias(IndividuTableMap::COL_ID_INDIVIDU, $keys, Criteria::IN);
    }

    /**
     * Filter the query on the id_individu column
     *
     * Example usage:
     * <code>
     * $query->filterByIdIndividu(1234); // WHERE id_individu = 1234
     * $query->filterByIdIndividu(array(12, 34)); // WHERE id_individu IN (12, 34)
     * $query->filterByIdIndividu(array('min' => 12)); // WHERE id_individu > 12
     * </code>
     *
     * @param     mixed $idIndividu The value to use as filter.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildIndividuQuery The current query, for fluid interface
     */
    public function filterByIdIndividu($idIndividu = null, $comparison = null)
    {
        if (is_array($idIndividu)) {
            $useMinMax = false;
            if (isset($idIndividu['min'])) {
                $this->addUsingAlias(IndividuTableMap::COL_ID_INDIVIDU, $idIndividu['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($idIndividu['max'])) {
                $this->addUsingAlias(IndividuTableMap::COL_ID_INDIVIDU, $idIndividu['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(IndividuTableMap::COL_ID_INDIVIDU, $idIndividu, $comparison);
    }

    /**
     * Filter the query on the nom column
     *
     * Example usage:
     * <code>
     * $query->filterByNom('fooValue');   // WHERE nom = 'fooValue'
     * $query->filterByNom('%fooValue%', Criteria::LIKE); // WHERE nom LIKE '%fooValue%'
     * </code>
     *
     * @param     string $nom The value to use as filter.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildIndividuQuery The current query, for fluid interface
     */
    public function filterByNom($nom = null, $comparison = null)
    {
        if (null === $comparison) {
            if (is_array($nom)) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(IndividuTableMap::COL_NOM, $nom, $comparison);
    }

    /**
     * Filter the query on the bio column
     *
     * Example usage:
     * <code>
     * $query->filterByBio('fooValue');   // WHERE bio = 'fooValue'
     * $query->filterByBio('%fooValue%', Criteria::LIKE); // WHERE bio LIKE '%fooValue%'
     * </code>
     *
     * @param     string $bio The value to use as filter.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildIndividuQuery The current query, for fluid interface
     */
    public function filterByBio($bio = null, $comparison = null)
    {
        if (null === $comparison) {
            if (is_array($bio)) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(IndividuTableMap::COL_BIO, $bio, $comparison);
    }

    /**
     * Filter the query on the email column
     *
     * Example usage:
     * <code>
     * $query->filterByEmail('fooValue');   // WHERE email = 'fooValue'
     * $query->filterByEmail('%fooValue%', Criteria::LIKE); // WHERE email LIKE '%fooValue%'
     * </code>
     *
     * @param     string $email The value to use as filter.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildIndividuQuery The current query, for fluid interface
     */
    public function filterByEmail($email = null, $comparison = null)
    {
        if (null === $comparison) {
            if (is_array($email)) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(IndividuTableMap::COL_EMAIL, $email, $comparison);
    }

    /**
     * Filter the query on the login column
     *
     * Example usage:
     * <code>
     * $query->filterByLogin('fooValue');   // WHERE login = 'fooValue'
     * $query->filterByLogin('%fooValue%', Criteria::LIKE); // WHERE login LIKE '%fooValue%'
     * </code>
     *
     * @param     string $login The value to use as filter.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildIndividuQuery The current query, for fluid interface
     */
    public function filterByLogin($login = null, $comparison = null)
    {
        if (null === $comparison) {
            if (is_array($login)) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(IndividuTableMap::COL_LOGIN, $login, $comparison);
    }

    /**
     * Filter the query on the pass column
     *
     * Example usage:
     * <code>
     * $query->filterByPass('fooValue');   // WHERE pass = 'fooValue'
     * $query->filterByPass('%fooValue%', Criteria::LIKE); // WHERE pass LIKE '%fooValue%'
     * </code>
     *
     * @param     string $pass The value to use as filter.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildIndividuQuery The current query, for fluid interface
     */
    public function filterByPass($pass = null, $comparison = null)
    {
        if (null === $comparison) {
            if (is_array($pass)) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(IndividuTableMap::COL_PASS, $pass, $comparison);
    }

    /**
     * Filter the query on the alea_actuel column
     *
     * Example usage:
     * <code>
     * $query->filterByAleaActuel('fooValue');   // WHERE alea_actuel = 'fooValue'
     * $query->filterByAleaActuel('%fooValue%', Criteria::LIKE); // WHERE alea_actuel LIKE '%fooValue%'
     * </code>
     *
     * @param     string $aleaActuel The value to use as filter.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildIndividuQuery The current query, for fluid interface
     */
    public function filterByAleaActuel($aleaActuel = null, $comparison = null)
    {
        if (null === $comparison) {
            if (is_array($aleaActuel)) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(IndividuTableMap::COL_ALEA_ACTUEL, $aleaActuel, $comparison);
    }

    /**
     * Filter the query on the alea_futur column
     *
     * Example usage:
     * <code>
     * $query->filterByAleaFutur('fooValue');   // WHERE alea_futur = 'fooValue'
     * $query->filterByAleaFutur('%fooValue%', Criteria::LIKE); // WHERE alea_futur LIKE '%fooValue%'
     * </code>
     *
     * @param     string $aleaFutur The value to use as filter.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildIndividuQuery The current query, for fluid interface
     */
    public function filterByAleaFutur($aleaFutur = null, $comparison = null)
    {
        if (null === $comparison) {
            if (is_array($aleaFutur)) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(IndividuTableMap::COL_ALEA_FUTUR, $aleaFutur, $comparison);
    }

    /**
     * Filter the query on the token column
     *
     * Example usage:
     * <code>
     * $query->filterByToken('fooValue');   // WHERE token = 'fooValue'
     * $query->filterByToken('%fooValue%', Criteria::LIKE); // WHERE token LIKE '%fooValue%'
     * </code>
     *
     * @param     string $token The value to use as filter.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildIndividuQuery The current query, for fluid interface
     */
    public function filterByToken($token = null, $comparison = null)
    {
        if (null === $comparison) {
            if (is_array($token)) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(IndividuTableMap::COL_TOKEN, $token, $comparison);
    }

    /**
     * Filter the query on the token_time column
     *
     * Example usage:
     * <code>
     * $query->filterByTokenTime(1234); // WHERE token_time = 1234
     * $query->filterByTokenTime(array(12, 34)); // WHERE token_time IN (12, 34)
     * $query->filterByTokenTime(array('min' => 12)); // WHERE token_time > 12
     * </code>
     *
     * @param     mixed $tokenTime The value to use as filter.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildIndividuQuery The current query, for fluid interface
     */
    public function filterByTokenTime($tokenTime = null, $comparison = null)
    {
        if (is_array($tokenTime)) {
            $useMinMax = false;
            if (isset($tokenTime['min'])) {
                $this->addUsingAlias(IndividuTableMap::COL_TOKEN_TIME, $tokenTime['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($tokenTime['max'])) {
                $this->addUsingAlias(IndividuTableMap::COL_TOKEN_TIME, $tokenTime['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(IndividuTableMap::COL_TOKEN_TIME, $tokenTime, $comparison);
    }

    /**
     * Filter the query on the civilite column
     *
     * Example usage:
     * <code>
     * $query->filterByCivilite('fooValue');   // WHERE civilite = 'fooValue'
     * $query->filterByCivilite('%fooValue%', Criteria::LIKE); // WHERE civilite LIKE '%fooValue%'
     * </code>
     *
     * @param     string $civilite The value to use as filter.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildIndividuQuery The current query, for fluid interface
     */
    public function filterByCivilite($civilite = null, $comparison = null)
    {
        if (null === $comparison) {
            if (is_array($civilite)) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(IndividuTableMap::COL_CIVILITE, $civilite, $comparison);
    }

    /**
     * Filter the query on the sexe column
     *
     * Example usage:
     * <code>
     * $query->filterBySexe('fooValue');   // WHERE sexe = 'fooValue'
     * $query->filterBySexe('%fooValue%', Criteria::LIKE); // WHERE sexe LIKE '%fooValue%'
     * </code>
     *
     * @param     string $sexe The value to use as filter.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildIndividuQuery The current query, for fluid interface
     */
    public function filterBySexe($sexe = null, $comparison = null)
    {
        if (null === $comparison) {
            if (is_array($sexe)) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(IndividuTableMap::COL_SEXE, $sexe, $comparison);
    }

    /**
     * Filter the query on the nom_famille column
     *
     * Example usage:
     * <code>
     * $query->filterByNomFamille('fooValue');   // WHERE nom_famille = 'fooValue'
     * $query->filterByNomFamille('%fooValue%', Criteria::LIKE); // WHERE nom_famille LIKE '%fooValue%'
     * </code>
     *
     * @param     string $nomFamille The value to use as filter.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildIndividuQuery The current query, for fluid interface
     */
    public function filterByNomFamille($nomFamille = null, $comparison = null)
    {
        if (null === $comparison) {
            if (is_array($nomFamille)) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(IndividuTableMap::COL_NOM_FAMILLE, $nomFamille, $comparison);
    }

    /**
     * Filter the query on the prenom column
     *
     * Example usage:
     * <code>
     * $query->filterByPrenom('fooValue');   // WHERE prenom = 'fooValue'
     * $query->filterByPrenom('%fooValue%', Criteria::LIKE); // WHERE prenom LIKE '%fooValue%'
     * </code>
     *
     * @param     string $prenom The value to use as filter.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildIndividuQuery The current query, for fluid interface
     */
    public function filterByPrenom($prenom = null, $comparison = null)
    {
        if (null === $comparison) {
            if (is_array($prenom)) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(IndividuTableMap::COL_PRENOM, $prenom, $comparison);
    }

    /**
     * Filter the query on the naissance column
     *
     * Example usage:
     * <code>
     * $query->filterByNaissance('2011-03-14'); // WHERE naissance = '2011-03-14'
     * $query->filterByNaissance('now'); // WHERE naissance = '2011-03-14'
     * $query->filterByNaissance(array('max' => 'yesterday')); // WHERE naissance > '2011-03-13'
     * </code>
     *
     * @param     mixed $naissance The value to use as filter.
     *              Values can be integers (unix timestamps), DateTime objects, or strings.
     *              Empty strings are treated as NULL.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildIndividuQuery The current query, for fluid interface
     */
    public function filterByNaissance($naissance = null, $comparison = null)
    {
        if (is_array($naissance)) {
            $useMinMax = false;
            if (isset($naissance['min'])) {
                $this->addUsingAlias(IndividuTableMap::COL_NAISSANCE, $naissance['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($naissance['max'])) {
                $this->addUsingAlias(IndividuTableMap::COL_NAISSANCE, $naissance['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(IndividuTableMap::COL_NAISSANCE, $naissance, $comparison);
    }

    /**
     * Filter the query on the adresse column
     *
     * Example usage:
     * <code>
     * $query->filterByAdresse('fooValue');   // WHERE adresse = 'fooValue'
     * $query->filterByAdresse('%fooValue%', Criteria::LIKE); // WHERE adresse LIKE '%fooValue%'
     * </code>
     *
     * @param     string $adresse The value to use as filter.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildIndividuQuery The current query, for fluid interface
     */
    public function filterByAdresse($adresse = null, $comparison = null)
    {
        if (null === $comparison) {
            if (is_array($adresse)) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(IndividuTableMap::COL_ADRESSE, $adresse, $comparison);
    }

    /**
     * Filter the query on the codepostal column
     *
     * Example usage:
     * <code>
     * $query->filterByCodepostal('fooValue');   // WHERE codepostal = 'fooValue'
     * $query->filterByCodepostal('%fooValue%', Criteria::LIKE); // WHERE codepostal LIKE '%fooValue%'
     * </code>
     *
     * @param     string $codepostal The value to use as filter.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildIndividuQuery The current query, for fluid interface
     */
    public function filterByCodepostal($codepostal = null, $comparison = null)
    {
        if (null === $comparison) {
            if (is_array($codepostal)) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(IndividuTableMap::COL_CODEPOSTAL, $codepostal, $comparison);
    }

    /**
     * Filter the query on the ville column
     *
     * Example usage:
     * <code>
     * $query->filterByVille('fooValue');   // WHERE ville = 'fooValue'
     * $query->filterByVille('%fooValue%', Criteria::LIKE); // WHERE ville LIKE '%fooValue%'
     * </code>
     *
     * @param     string $ville The value to use as filter.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildIndividuQuery The current query, for fluid interface
     */
    public function filterByVille($ville = null, $comparison = null)
    {
        if (null === $comparison) {
            if (is_array($ville)) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(IndividuTableMap::COL_VILLE, $ville, $comparison);
    }

    /**
     * Filter the query on the pays column
     *
     * Example usage:
     * <code>
     * $query->filterByPays('fooValue');   // WHERE pays = 'fooValue'
     * $query->filterByPays('%fooValue%', Criteria::LIKE); // WHERE pays LIKE '%fooValue%'
     * </code>
     *
     * @param     string $pays The value to use as filter.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildIndividuQuery The current query, for fluid interface
     */
    public function filterByPays($pays = null, $comparison = null)
    {
        if (null === $comparison) {
            if (is_array($pays)) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(IndividuTableMap::COL_PAYS, $pays, $comparison);
    }

    /**
     * Filter the query on the telephone column
     *
     * Example usage:
     * <code>
     * $query->filterByTelephone('fooValue');   // WHERE telephone = 'fooValue'
     * $query->filterByTelephone('%fooValue%', Criteria::LIKE); // WHERE telephone LIKE '%fooValue%'
     * </code>
     *
     * @param     string $telephone The value to use as filter.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildIndividuQuery The current query, for fluid interface
     */
    public function filterByTelephone($telephone = null, $comparison = null)
    {
        if (null === $comparison) {
            if (is_array($telephone)) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(IndividuTableMap::COL_TELEPHONE, $telephone, $comparison);
    }

    /**
     * Filter the query on the telephone_pro column
     *
     * Example usage:
     * <code>
     * $query->filterByTelephonePro('fooValue');   // WHERE telephone_pro = 'fooValue'
     * $query->filterByTelephonePro('%fooValue%', Criteria::LIKE); // WHERE telephone_pro LIKE '%fooValue%'
     * </code>
     *
     * @param     string $telephonePro The value to use as filter.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildIndividuQuery The current query, for fluid interface
     */
    public function filterByTelephonePro($telephonePro = null, $comparison = null)
    {
        if (null === $comparison) {
            if (is_array($telephonePro)) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(IndividuTableMap::COL_TELEPHONE_PRO, $telephonePro, $comparison);
    }

    /**
     * Filter the query on the fax column
     *
     * Example usage:
     * <code>
     * $query->filterByFax('fooValue');   // WHERE fax = 'fooValue'
     * $query->filterByFax('%fooValue%', Criteria::LIKE); // WHERE fax LIKE '%fooValue%'
     * </code>
     *
     * @param     string $fax The value to use as filter.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildIndividuQuery The current query, for fluid interface
     */
    public function filterByFax($fax = null, $comparison = null)
    {
        if (null === $comparison) {
            if (is_array($fax)) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(IndividuTableMap::COL_FAX, $fax, $comparison);
    }

    /**
     * Filter the query on the mobile column
     *
     * Example usage:
     * <code>
     * $query->filterByMobile('fooValue');   // WHERE mobile = 'fooValue'
     * $query->filterByMobile('%fooValue%', Criteria::LIKE); // WHERE mobile LIKE '%fooValue%'
     * </code>
     *
     * @param     string $mobile The value to use as filter.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildIndividuQuery The current query, for fluid interface
     */
    public function filterByMobile($mobile = null, $comparison = null)
    {
        if (null === $comparison) {
            if (is_array($mobile)) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(IndividuTableMap::COL_MOBILE, $mobile, $comparison);
    }

    /**
     * Filter the query on the url column
     *
     * Example usage:
     * <code>
     * $query->filterByUrl('fooValue');   // WHERE url = 'fooValue'
     * $query->filterByUrl('%fooValue%', Criteria::LIKE); // WHERE url LIKE '%fooValue%'
     * </code>
     *
     * @param     string $url The value to use as filter.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildIndividuQuery The current query, for fluid interface
     */
    public function filterByUrl($url = null, $comparison = null)
    {
        if (null === $comparison) {
            if (is_array($url)) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(IndividuTableMap::COL_URL, $url, $comparison);
    }

    /**
     * Filter the query on the profession column
     *
     * Example usage:
     * <code>
     * $query->filterByProfession('fooValue');   // WHERE profession = 'fooValue'
     * $query->filterByProfession('%fooValue%', Criteria::LIKE); // WHERE profession LIKE '%fooValue%'
     * </code>
     *
     * @param     string $profession The value to use as filter.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildIndividuQuery The current query, for fluid interface
     */
    public function filterByProfession($profession = null, $comparison = null)
    {
        if (null === $comparison) {
            if (is_array($profession)) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(IndividuTableMap::COL_PROFESSION, $profession, $comparison);
    }

    /**
     * Filter the query on the contact_souhait column
     *
     * Example usage:
     * <code>
     * $query->filterByContactSouhait(true); // WHERE contact_souhait = true
     * $query->filterByContactSouhait('yes'); // WHERE contact_souhait = true
     * </code>
     *
     * @param     boolean|string $contactSouhait The value to use as filter.
     *              Non-boolean arguments are converted using the following rules:
     *                * 1, '1', 'true',  'on',  and 'yes' are converted to boolean true
     *                * 0, '0', 'false', 'off', and 'no'  are converted to boolean false
     *              Check on string values is case insensitive (so 'FaLsE' is seen as 'false').
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildIndividuQuery The current query, for fluid interface
     */
    public function filterByContactSouhait($contactSouhait = null, $comparison = null)
    {
        if (is_string($contactSouhait)) {
            $contactSouhait = in_array(strtolower($contactSouhait), array('false', 'off', '-', 'no', 'n', '0', '')) ? false : true;
        }

        return $this->addUsingAlias(IndividuTableMap::COL_CONTACT_SOUHAIT, $contactSouhait, $comparison);
    }

    /**
     * Filter the query on the observation column
     *
     * Example usage:
     * <code>
     * $query->filterByObservation('fooValue');   // WHERE observation = 'fooValue'
     * $query->filterByObservation('%fooValue%', Criteria::LIKE); // WHERE observation LIKE '%fooValue%'
     * </code>
     *
     * @param     string $observation The value to use as filter.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildIndividuQuery The current query, for fluid interface
     */
    public function filterByObservation($observation = null, $comparison = null)
    {
        if (null === $comparison) {
            if (is_array($observation)) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(IndividuTableMap::COL_OBSERVATION, $observation, $comparison);
    }

    /**
     * Filter the query on the created_at column
     *
     * Example usage:
     * <code>
     * $query->filterByCreatedAt('2011-03-14'); // WHERE created_at = '2011-03-14'
     * $query->filterByCreatedAt('now'); // WHERE created_at = '2011-03-14'
     * $query->filterByCreatedAt(array('max' => 'yesterday')); // WHERE created_at > '2011-03-13'
     * </code>
     *
     * @param     mixed $createdAt The value to use as filter.
     *              Values can be integers (unix timestamps), DateTime objects, or strings.
     *              Empty strings are treated as NULL.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildIndividuQuery The current query, for fluid interface
     */
    public function filterByCreatedAt($createdAt = null, $comparison = null)
    {
        if (is_array($createdAt)) {
            $useMinMax = false;
            if (isset($createdAt['min'])) {
                $this->addUsingAlias(IndividuTableMap::COL_CREATED_AT, $createdAt['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($createdAt['max'])) {
                $this->addUsingAlias(IndividuTableMap::COL_CREATED_AT, $createdAt['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(IndividuTableMap::COL_CREATED_AT, $createdAt, $comparison);
    }

    /**
     * Filter the query on the updated_at column
     *
     * Example usage:
     * <code>
     * $query->filterByUpdatedAt('2011-03-14'); // WHERE updated_at = '2011-03-14'
     * $query->filterByUpdatedAt('now'); // WHERE updated_at = '2011-03-14'
     * $query->filterByUpdatedAt(array('max' => 'yesterday')); // WHERE updated_at > '2011-03-13'
     * </code>
     *
     * @param     mixed $updatedAt The value to use as filter.
     *              Values can be integers (unix timestamps), DateTime objects, or strings.
     *              Empty strings are treated as NULL.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildIndividuQuery The current query, for fluid interface
     */
    public function filterByUpdatedAt($updatedAt = null, $comparison = null)
    {
        if (is_array($updatedAt)) {
            $useMinMax = false;
            if (isset($updatedAt['min'])) {
                $this->addUsingAlias(IndividuTableMap::COL_UPDATED_AT, $updatedAt['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($updatedAt['max'])) {
                $this->addUsingAlias(IndividuTableMap::COL_UPDATED_AT, $updatedAt['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(IndividuTableMap::COL_UPDATED_AT, $updatedAt, $comparison);
    }

    /**
     * Filter the query by a related \Autorisation object
     *
     * @param \Autorisation|ObjectCollection $autorisation the related object to use as filter
     * @param string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return ChildIndividuQuery The current query, for fluid interface
     */
    public function filterByAutorisation($autorisation, $comparison = null)
    {
        if ($autorisation instanceof \Autorisation) {
            return $this
                ->addUsingAlias(IndividuTableMap::COL_ID_INDIVIDU, $autorisation->getIdIndividu(), $comparison);
        } elseif ($autorisation instanceof ObjectCollection) {
            return $this
                ->useAutorisationQuery()
                ->filterByPrimaryKeys($autorisation->getPrimaryKeys())
                ->endUse();
        } else {
            throw new PropelException('filterByAutorisation() only accepts arguments of type \Autorisation or Collection');
        }
    }

    /**
     * Adds a JOIN clause to the query using the Autorisation relation
     *
     * @param     string $relationAlias optional alias for the relation
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return $this|ChildIndividuQuery The current query, for fluid interface
     */
    public function joinAutorisation($relationAlias = null, $joinType = Criteria::INNER_JOIN)
    {
        $tableMap = $this->getTableMap();
        $relationMap = $tableMap->getRelation('Autorisation');

        // create a ModelJoin object for this join
        $join = new ModelJoin();
        $join->setJoinType($joinType);
        $join->setRelationMap($relationMap, $this->useAliasInSQL ? $this->getModelAlias() : null, $relationAlias);
        if ($previousJoin = $this->getPreviousJoin()) {
            $join->setPreviousJoin($previousJoin);
        }

        // add the ModelJoin to the current object
        if ($relationAlias) {
            $this->addAlias($relationAlias, $relationMap->getRightTable()->getName());
            $this->addJoinObject($join, $relationAlias);
        } else {
            $this->addJoinObject($join, 'Autorisation');
        }

        return $this;
    }

    /**
     * Use the Autorisation relation Autorisation object
     *
     * @see useQuery()
     *
     * @param     string $relationAlias optional alias for the relation,
     *                                   to be used as main alias in the secondary query
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return \AutorisationQuery A secondary query class using the current class as primary query
     */
    public function useAutorisationQuery($relationAlias = null, $joinType = Criteria::INNER_JOIN)
    {
        return $this
            ->joinAutorisation($relationAlias, $joinType)
            ->useQuery($relationAlias ? $relationAlias : 'Autorisation', '\AutorisationQuery');
    }

    /**
     * Filter the query by a related \Log object
     *
     * @param \Log|ObjectCollection $log the related object to use as filter
     * @param string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return ChildIndividuQuery The current query, for fluid interface
     */
    public function filterByLog($log, $comparison = null)
    {
        if ($log instanceof \Log) {
            return $this
                ->addUsingAlias(IndividuTableMap::COL_ID_INDIVIDU, $log->getIdIndividu(), $comparison);
        } elseif ($log instanceof ObjectCollection) {
            return $this
                ->useLogQuery()
                ->filterByPrimaryKeys($log->getPrimaryKeys())
                ->endUse();
        } else {
            throw new PropelException('filterByLog() only accepts arguments of type \Log or Collection');
        }
    }

    /**
     * Adds a JOIN clause to the query using the Log relation
     *
     * @param     string $relationAlias optional alias for the relation
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return $this|ChildIndividuQuery The current query, for fluid interface
     */
    public function joinLog($relationAlias = null, $joinType = Criteria::INNER_JOIN)
    {
        $tableMap = $this->getTableMap();
        $relationMap = $tableMap->getRelation('Log');

        // create a ModelJoin object for this join
        $join = new ModelJoin();
        $join->setJoinType($joinType);
        $join->setRelationMap($relationMap, $this->useAliasInSQL ? $this->getModelAlias() : null, $relationAlias);
        if ($previousJoin = $this->getPreviousJoin()) {
            $join->setPreviousJoin($previousJoin);
        }

        // add the ModelJoin to the current object
        if ($relationAlias) {
            $this->addAlias($relationAlias, $relationMap->getRightTable()->getName());
            $this->addJoinObject($join, $relationAlias);
        } else {
            $this->addJoinObject($join, 'Log');
        }

        return $this;
    }

    /**
     * Use the Log relation Log object
     *
     * @see useQuery()
     *
     * @param     string $relationAlias optional alias for the relation,
     *                                   to be used as main alias in the secondary query
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return \LogQuery A secondary query class using the current class as primary query
     */
    public function useLogQuery($relationAlias = null, $joinType = Criteria::INNER_JOIN)
    {
        return $this
            ->joinLog($relationAlias, $joinType)
            ->useQuery($relationAlias ? $relationAlias : 'Log', '\LogQuery');
    }

    /**
     * Filter the query by a related \Membre object
     *
     * @param \Membre|ObjectCollection $membre the related object to use as filter
     * @param string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return ChildIndividuQuery The current query, for fluid interface
     */
    public function filterByMembres0($membre, $comparison = null)
    {
        if ($membre instanceof \Membre) {
            return $this
                ->addUsingAlias(IndividuTableMap::COL_ID_INDIVIDU, $membre->getIdIndividuTitulaire(), $comparison);
        } elseif ($membre instanceof ObjectCollection) {
            return $this
                ->useMembres0Query()
                ->filterByPrimaryKeys($membre->getPrimaryKeys())
                ->endUse();
        } else {
            throw new PropelException('filterByMembres0() only accepts arguments of type \Membre or Collection');
        }
    }

    /**
     * Adds a JOIN clause to the query using the Membres0 relation
     *
     * @param     string $relationAlias optional alias for the relation
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return $this|ChildIndividuQuery The current query, for fluid interface
     */
    public function joinMembres0($relationAlias = null, $joinType = Criteria::INNER_JOIN)
    {
        $tableMap = $this->getTableMap();
        $relationMap = $tableMap->getRelation('Membres0');

        // create a ModelJoin object for this join
        $join = new ModelJoin();
        $join->setJoinType($joinType);
        $join->setRelationMap($relationMap, $this->useAliasInSQL ? $this->getModelAlias() : null, $relationAlias);
        if ($previousJoin = $this->getPreviousJoin()) {
            $join->setPreviousJoin($previousJoin);
        }

        // add the ModelJoin to the current object
        if ($relationAlias) {
            $this->addAlias($relationAlias, $relationMap->getRightTable()->getName());
            $this->addJoinObject($join, $relationAlias);
        } else {
            $this->addJoinObject($join, 'Membres0');
        }

        return $this;
    }

    /**
     * Use the Membres0 relation Membre object
     *
     * @see useQuery()
     *
     * @param     string $relationAlias optional alias for the relation,
     *                                   to be used as main alias in the secondary query
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return \MembreQuery A secondary query class using the current class as primary query
     */
    public function useMembres0Query($relationAlias = null, $joinType = Criteria::INNER_JOIN)
    {
        return $this
            ->joinMembres0($relationAlias, $joinType)
            ->useQuery($relationAlias ? $relationAlias : 'Membres0', '\MembreQuery');
    }

    /**
     * Filter the query by a related \MembreIndividu object
     *
     * @param \MembreIndividu|ObjectCollection $membreIndividu the related object to use as filter
     * @param string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return ChildIndividuQuery The current query, for fluid interface
     */
    public function filterByMembreIndividu($membreIndividu, $comparison = null)
    {
        if ($membreIndividu instanceof \MembreIndividu) {
            return $this
                ->addUsingAlias(IndividuTableMap::COL_ID_INDIVIDU, $membreIndividu->getIdIndividu(), $comparison);
        } elseif ($membreIndividu instanceof ObjectCollection) {
            return $this
                ->useMembreIndividuQuery()
                ->filterByPrimaryKeys($membreIndividu->getPrimaryKeys())
                ->endUse();
        } else {
            throw new PropelException('filterByMembreIndividu() only accepts arguments of type \MembreIndividu or Collection');
        }
    }

    /**
     * Adds a JOIN clause to the query using the MembreIndividu relation
     *
     * @param     string $relationAlias optional alias for the relation
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return $this|ChildIndividuQuery The current query, for fluid interface
     */
    public function joinMembreIndividu($relationAlias = null, $joinType = Criteria::INNER_JOIN)
    {
        $tableMap = $this->getTableMap();
        $relationMap = $tableMap->getRelation('MembreIndividu');

        // create a ModelJoin object for this join
        $join = new ModelJoin();
        $join->setJoinType($joinType);
        $join->setRelationMap($relationMap, $this->useAliasInSQL ? $this->getModelAlias() : null, $relationAlias);
        if ($previousJoin = $this->getPreviousJoin()) {
            $join->setPreviousJoin($previousJoin);
        }

        // add the ModelJoin to the current object
        if ($relationAlias) {
            $this->addAlias($relationAlias, $relationMap->getRightTable()->getName());
            $this->addJoinObject($join, $relationAlias);
        } else {
            $this->addJoinObject($join, 'MembreIndividu');
        }

        return $this;
    }

    /**
     * Use the MembreIndividu relation MembreIndividu object
     *
     * @see useQuery()
     *
     * @param     string $relationAlias optional alias for the relation,
     *                                   to be used as main alias in the secondary query
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return \MembreIndividuQuery A secondary query class using the current class as primary query
     */
    public function useMembreIndividuQuery($relationAlias = null, $joinType = Criteria::INNER_JOIN)
    {
        return $this
            ->joinMembreIndividu($relationAlias, $joinType)
            ->useQuery($relationAlias ? $relationAlias : 'MembreIndividu', '\MembreIndividuQuery');
    }

    /**
     * Filter the query by a related \ServicerenduIndividu object
     *
     * @param \ServicerenduIndividu|ObjectCollection $servicerenduIndividu the related object to use as filter
     * @param string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return ChildIndividuQuery The current query, for fluid interface
     */
    public function filterByServicerenduIndividu($servicerenduIndividu, $comparison = null)
    {
        if ($servicerenduIndividu instanceof \ServicerenduIndividu) {
            return $this
                ->addUsingAlias(IndividuTableMap::COL_ID_INDIVIDU, $servicerenduIndividu->getIdIndividu(), $comparison);
        } elseif ($servicerenduIndividu instanceof ObjectCollection) {
            return $this
                ->useServicerenduIndividuQuery()
                ->filterByPrimaryKeys($servicerenduIndividu->getPrimaryKeys())
                ->endUse();
        } else {
            throw new PropelException('filterByServicerenduIndividu() only accepts arguments of type \ServicerenduIndividu or Collection');
        }
    }

    /**
     * Adds a JOIN clause to the query using the ServicerenduIndividu relation
     *
     * @param     string $relationAlias optional alias for the relation
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return $this|ChildIndividuQuery The current query, for fluid interface
     */
    public function joinServicerenduIndividu($relationAlias = null, $joinType = Criteria::INNER_JOIN)
    {
        $tableMap = $this->getTableMap();
        $relationMap = $tableMap->getRelation('ServicerenduIndividu');

        // create a ModelJoin object for this join
        $join = new ModelJoin();
        $join->setJoinType($joinType);
        $join->setRelationMap($relationMap, $this->useAliasInSQL ? $this->getModelAlias() : null, $relationAlias);
        if ($previousJoin = $this->getPreviousJoin()) {
            $join->setPreviousJoin($previousJoin);
        }

        // add the ModelJoin to the current object
        if ($relationAlias) {
            $this->addAlias($relationAlias, $relationMap->getRightTable()->getName());
            $this->addJoinObject($join, $relationAlias);
        } else {
            $this->addJoinObject($join, 'ServicerenduIndividu');
        }

        return $this;
    }

    /**
     * Use the ServicerenduIndividu relation ServicerenduIndividu object
     *
     * @see useQuery()
     *
     * @param     string $relationAlias optional alias for the relation,
     *                                   to be used as main alias in the secondary query
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return \ServicerenduIndividuQuery A secondary query class using the current class as primary query
     */
    public function useServicerenduIndividuQuery($relationAlias = null, $joinType = Criteria::INNER_JOIN)
    {
        return $this
            ->joinServicerenduIndividu($relationAlias, $joinType)
            ->useQuery($relationAlias ? $relationAlias : 'ServicerenduIndividu', '\ServicerenduIndividuQuery');
    }

    /**
     * Filter the query by a related \NotificationIndividu object
     *
     * @param \NotificationIndividu|ObjectCollection $notificationIndividu the related object to use as filter
     * @param string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return ChildIndividuQuery The current query, for fluid interface
     */
    public function filterByNotificationIndividu($notificationIndividu, $comparison = null)
    {
        if ($notificationIndividu instanceof \NotificationIndividu) {
            return $this
                ->addUsingAlias(IndividuTableMap::COL_ID_INDIVIDU, $notificationIndividu->getIdIndividu(), $comparison);
        } elseif ($notificationIndividu instanceof ObjectCollection) {
            return $this
                ->useNotificationIndividuQuery()
                ->filterByPrimaryKeys($notificationIndividu->getPrimaryKeys())
                ->endUse();
        } else {
            throw new PropelException('filterByNotificationIndividu() only accepts arguments of type \NotificationIndividu or Collection');
        }
    }

    /**
     * Adds a JOIN clause to the query using the NotificationIndividu relation
     *
     * @param     string $relationAlias optional alias for the relation
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return $this|ChildIndividuQuery The current query, for fluid interface
     */
    public function joinNotificationIndividu($relationAlias = null, $joinType = Criteria::INNER_JOIN)
    {
        $tableMap = $this->getTableMap();
        $relationMap = $tableMap->getRelation('NotificationIndividu');

        // create a ModelJoin object for this join
        $join = new ModelJoin();
        $join->setJoinType($joinType);
        $join->setRelationMap($relationMap, $this->useAliasInSQL ? $this->getModelAlias() : null, $relationAlias);
        if ($previousJoin = $this->getPreviousJoin()) {
            $join->setPreviousJoin($previousJoin);
        }

        // add the ModelJoin to the current object
        if ($relationAlias) {
            $this->addAlias($relationAlias, $relationMap->getRightTable()->getName());
            $this->addJoinObject($join, $relationAlias);
        } else {
            $this->addJoinObject($join, 'NotificationIndividu');
        }

        return $this;
    }

    /**
     * Use the NotificationIndividu relation NotificationIndividu object
     *
     * @see useQuery()
     *
     * @param     string $relationAlias optional alias for the relation,
     *                                   to be used as main alias in the secondary query
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return \NotificationIndividuQuery A secondary query class using the current class as primary query
     */
    public function useNotificationIndividuQuery($relationAlias = null, $joinType = Criteria::INNER_JOIN)
    {
        return $this
            ->joinNotificationIndividu($relationAlias, $joinType)
            ->useQuery($relationAlias ? $relationAlias : 'NotificationIndividu', '\NotificationIndividuQuery');
    }

    /**
     * Filter the query by a related \CourrierLien object
     *
     * @param \CourrierLien|ObjectCollection $courrierLien the related object to use as filter
     * @param string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return ChildIndividuQuery The current query, for fluid interface
     */
    public function filterByCourrierLien($courrierLien, $comparison = null)
    {
        if ($courrierLien instanceof \CourrierLien) {
            return $this
                ->addUsingAlias(IndividuTableMap::COL_ID_INDIVIDU, $courrierLien->getIdIndividu(), $comparison);
        } elseif ($courrierLien instanceof ObjectCollection) {
            return $this
                ->useCourrierLienQuery()
                ->filterByPrimaryKeys($courrierLien->getPrimaryKeys())
                ->endUse();
        } else {
            throw new PropelException('filterByCourrierLien() only accepts arguments of type \CourrierLien or Collection');
        }
    }

    /**
     * Adds a JOIN clause to the query using the CourrierLien relation
     *
     * @param     string $relationAlias optional alias for the relation
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return $this|ChildIndividuQuery The current query, for fluid interface
     */
    public function joinCourrierLien($relationAlias = null, $joinType = Criteria::INNER_JOIN)
    {
        $tableMap = $this->getTableMap();
        $relationMap = $tableMap->getRelation('CourrierLien');

        // create a ModelJoin object for this join
        $join = new ModelJoin();
        $join->setJoinType($joinType);
        $join->setRelationMap($relationMap, $this->useAliasInSQL ? $this->getModelAlias() : null, $relationAlias);
        if ($previousJoin = $this->getPreviousJoin()) {
            $join->setPreviousJoin($previousJoin);
        }

        // add the ModelJoin to the current object
        if ($relationAlias) {
            $this->addAlias($relationAlias, $relationMap->getRightTable()->getName());
            $this->addJoinObject($join, $relationAlias);
        } else {
            $this->addJoinObject($join, 'CourrierLien');
        }

        return $this;
    }

    /**
     * Use the CourrierLien relation CourrierLien object
     *
     * @see useQuery()
     *
     * @param     string $relationAlias optional alias for the relation,
     *                                   to be used as main alias in the secondary query
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return \CourrierLienQuery A secondary query class using the current class as primary query
     */
    public function useCourrierLienQuery($relationAlias = null, $joinType = Criteria::INNER_JOIN)
    {
        return $this
            ->joinCourrierLien($relationAlias, $joinType)
            ->useQuery($relationAlias ? $relationAlias : 'CourrierLien', '\CourrierLienQuery');
    }

    /**
     * Filter the query by a related Membre object
     * using the asso_membres_individus table as cross reference
     *
     * @param Membre $membre the related object to use as filter
     * @param string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return ChildIndividuQuery The current query, for fluid interface
     */
    public function filterByMembre($membre, $comparison = Criteria::EQUAL)
    {
        return $this
            ->useMembreIndividuQuery()
            ->filterByMembre($membre, $comparison)
            ->endUse();
    }

    /**
     * Filter the query by a related Servicerendu object
     * using the asso_servicerendus_individus table as cross reference
     *
     * @param Servicerendu $servicerendu the related object to use as filter
     * @param string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return ChildIndividuQuery The current query, for fluid interface
     */
    public function filterByServicerendu($servicerendu, $comparison = Criteria::EQUAL)
    {
        return $this
            ->useServicerenduIndividuQuery()
            ->filterByServicerendu($servicerendu, $comparison)
            ->endUse();
    }

    /**
     * Filter the query by a related Notification object
     * using the asso_notifications_individus table as cross reference
     *
     * @param Notification $notification the related object to use as filter
     * @param string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return ChildIndividuQuery The current query, for fluid interface
     */
    public function filterByNotification($notification, $comparison = Criteria::EQUAL)
    {
        return $this
            ->useNotificationIndividuQuery()
            ->filterByNotification($notification, $comparison)
            ->endUse();
    }

    /**
     * Exclude object from result
     *
     * @param   ChildIndividu $individu Object to remove from the list of results
     *
     * @return $this|ChildIndividuQuery The current query, for fluid interface
     */
    public function prune($individu = null)
    {
        if ($individu) {
            $this->addUsingAlias(IndividuTableMap::COL_ID_INDIVIDU, $individu->getIdIndividu(), Criteria::NOT_EQUAL);
        }

        return $this;
    }

    /**
     * Code to execute before every DELETE statement
     *
     * @param     ConnectionInterface $con The connection object used by the query
     */
    protected function basePreDelete(ConnectionInterface $con)
    {
        // archivable behavior

        if ($this->archiveOnDelete) {
            $this->archive($con);
        } else {
            $this->archiveOnDelete = true;
        }


        return $this->preDelete($con);
    }

    /**
     * Deletes all rows from the asso_individus table.
     *
     * @param ConnectionInterface $con the connection to use
     * @return int The number of affected rows (if supported by underlying database driver).
     */
    public function doDeleteAll(ConnectionInterface $con = null)
    {
        if (null === $con) {
            $con = Propel::getServiceContainer()->getWriteConnection(IndividuTableMap::DATABASE_NAME);
        }

        // use transaction because $criteria could contain info
        // for more than one table or we could emulating ON DELETE CASCADE, etc.
        return $con->transaction(function () use ($con) {
            $affectedRows = 0; // initialize var to track total num of affected rows
            $affectedRows += parent::doDeleteAll($con);
            // Because this db requires some delete cascade/set null emulation, we have to
            // clear the cached instance *after* the emulation has happened (since
            // instances get re-added by the select statement contained therein).
            IndividuTableMap::clearInstancePool();
            IndividuTableMap::clearRelatedInstancePool();

            return $affectedRows;
        });
    }

    /**
     * Performs a DELETE on the database based on the current ModelCriteria
     *
     * @param ConnectionInterface $con the connection to use
     * @return int             The number of affected rows (if supported by underlying database driver).  This includes CASCADE-related rows
     *                         if supported by native driver or if emulated using Propel.
     * @throws PropelException Any exceptions caught during processing will be
     *                         rethrown wrapped into a PropelException.
     */
    public function delete(ConnectionInterface $con = null)
    {
        if (null === $con) {
            $con = Propel::getServiceContainer()->getWriteConnection(IndividuTableMap::DATABASE_NAME);
        }

        $criteria = $this;

        // Set the correct dbName
        $criteria->setDbName(IndividuTableMap::DATABASE_NAME);

        // use transaction because $criteria could contain info
        // for more than one table or we could emulating ON DELETE CASCADE, etc.
        return $con->transaction(function () use ($con, $criteria) {
            $affectedRows = 0; // initialize var to track total num of affected rows

            IndividuTableMap::removeInstanceFromPool($criteria);

            $affectedRows += ModelCriteria::delete($con);
            IndividuTableMap::clearRelatedInstancePool();

            return $affectedRows;
        });
    }

    // timestampable behavior

    /**
     * Filter by the latest updated
     *
     * @param      int $nbDays Maximum age of the latest update in days
     *
     * @return     $this|ChildIndividuQuery The current query, for fluid interface
     */
    public function recentlyUpdated($nbDays = 7)
    {
        return $this->addUsingAlias(IndividuTableMap::COL_UPDATED_AT, time() - $nbDays * 24 * 60 * 60, Criteria::GREATER_EQUAL);
    }

    /**
     * Order by update date desc
     *
     * @return     $this|ChildIndividuQuery The current query, for fluid interface
     */
    public function lastUpdatedFirst()
    {
        return $this->addDescendingOrderByColumn(IndividuTableMap::COL_UPDATED_AT);
    }

    /**
     * Order by update date asc
     *
     * @return     $this|ChildIndividuQuery The current query, for fluid interface
     */
    public function firstUpdatedFirst()
    {
        return $this->addAscendingOrderByColumn(IndividuTableMap::COL_UPDATED_AT);
    }

    /**
     * Order by create date desc
     *
     * @return     $this|ChildIndividuQuery The current query, for fluid interface
     */
    public function lastCreatedFirst()
    {
        return $this->addDescendingOrderByColumn(IndividuTableMap::COL_CREATED_AT);
    }

    /**
     * Filter by the latest created
     *
     * @param      int $nbDays Maximum age of in days
     *
     * @return     $this|ChildIndividuQuery The current query, for fluid interface
     */
    public function recentlyCreated($nbDays = 7)
    {
        return $this->addUsingAlias(IndividuTableMap::COL_CREATED_AT, time() - $nbDays * 24 * 60 * 60, Criteria::GREATER_EQUAL);
    }

    /**
     * Order by create date asc
     *
     * @return     $this|ChildIndividuQuery The current query, for fluid interface
     */
    public function firstCreatedFirst()
    {
        return $this->addAscendingOrderByColumn(IndividuTableMap::COL_CREATED_AT);
    }

    // archivable behavior

    /**
     * Copy the data of the objects satisfying the query into ChildAssoIndividusArchive archive objects.
     * The archived objects are then saved.
     * If any of the objects has already been archived, the archived object
     * is updated and not duplicated.
     * Warning: This termination methods issues 2n+1 queries.
     *
     * @param      ConnectionInterface $con    Connection to use.
     * @param      Boolean $useLittleMemory    Whether or not to use OnDemandFormatter to retrieve objects.
     *               Set to false if the identity map matters.
     *               Set to true (default) to use less memory.
     *
     * @return     int the number of archived objects
     */
    public function archive($con = null, $useLittleMemory = true)
    {
        $criteria = clone $this;
        // prepare the query
        $criteria->setWith(array());
        if ($useLittleMemory) {
            $criteria->setFormatter(ModelCriteria::FORMAT_ON_DEMAND);
        }
        if ($con === null) {
            $con = Propel::getServiceContainer()->getWriteConnection(IndividuTableMap::DATABASE_NAME);
        }

        return $con->transaction(function () use ($con, $criteria) {
            $totalArchivedObjects = 0;

            // archive all results one by one
            foreach ($criteria->find($con) as $object) {
                $object->archive($con);
                $totalArchivedObjects++;
            }

            return $totalArchivedObjects;
        });
    }

    /**
     * Enable/disable auto-archiving on delete for the next query.
     *
     * @param boolean True if the query must archive deleted objects, false otherwise.
     */
    public function setArchiveOnDelete($archiveOnDelete)
    {
        $this->archiveOnDelete = $archiveOnDelete;
    }

    /**
     * Delete records matching the current query without archiving them.
     *
     * @param      ConnectionInterface $con    Connection to use.
     *
     * @return integer the number of deleted rows
     */
    public function deleteWithoutArchive($con = null)
    {
        $this->archiveOnDelete = false;

        return $this->delete($con);
    }

    /**
     * Delete all records without archiving them.
     *
     * @param      ConnectionInterface $con    Connection to use.
     *
     * @return integer the number of deleted rows
     */
    public function deleteAllWithoutArchive($con = null)
    {
        $this->archiveOnDelete = false;

        return $this->deleteAll($con);
    }

} // IndividuQuery
