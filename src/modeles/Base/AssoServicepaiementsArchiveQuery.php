<?php

namespace Base;

use \AssoServicepaiementsArchive as ChildAssoServicepaiementsArchive;
use \AssoServicepaiementsArchiveQuery as ChildAssoServicepaiementsArchiveQuery;
use \Exception;
use \PDO;
use Map\AssoServicepaiementsArchiveTableMap;
use Propel\Runtime\Propel;
use Propel\Runtime\ActiveQuery\Criteria;
use Propel\Runtime\ActiveQuery\ModelCriteria;
use Propel\Runtime\Collection\ObjectCollection;
use Propel\Runtime\Connection\ConnectionInterface;
use Propel\Runtime\Exception\PropelException;

/**
 * Base class that represents a query for the 'asso_servicepaiements_archive' table.
 *
 *
 *
 * @method     ChildAssoServicepaiementsArchiveQuery orderByIdServicepaiement($order = Criteria::ASC) Order by the id_servicepaiement column
 * @method     ChildAssoServicepaiementsArchiveQuery orderByIdServicerendu($order = Criteria::ASC) Order by the id_servicerendu column
 * @method     ChildAssoServicepaiementsArchiveQuery orderByIdPaiement($order = Criteria::ASC) Order by the id_paiement column
 * @method     ChildAssoServicepaiementsArchiveQuery orderByMontant($order = Criteria::ASC) Order by the montant column
 * @method     ChildAssoServicepaiementsArchiveQuery orderByCreatedAt($order = Criteria::ASC) Order by the created_at column
 * @method     ChildAssoServicepaiementsArchiveQuery orderByUpdatedAt($order = Criteria::ASC) Order by the updated_at column
 * @method     ChildAssoServicepaiementsArchiveQuery orderByArchivedAt($order = Criteria::ASC) Order by the archived_at column
 *
 * @method     ChildAssoServicepaiementsArchiveQuery groupByIdServicepaiement() Group by the id_servicepaiement column
 * @method     ChildAssoServicepaiementsArchiveQuery groupByIdServicerendu() Group by the id_servicerendu column
 * @method     ChildAssoServicepaiementsArchiveQuery groupByIdPaiement() Group by the id_paiement column
 * @method     ChildAssoServicepaiementsArchiveQuery groupByMontant() Group by the montant column
 * @method     ChildAssoServicepaiementsArchiveQuery groupByCreatedAt() Group by the created_at column
 * @method     ChildAssoServicepaiementsArchiveQuery groupByUpdatedAt() Group by the updated_at column
 * @method     ChildAssoServicepaiementsArchiveQuery groupByArchivedAt() Group by the archived_at column
 *
 * @method     ChildAssoServicepaiementsArchiveQuery leftJoin($relation) Adds a LEFT JOIN clause to the query
 * @method     ChildAssoServicepaiementsArchiveQuery rightJoin($relation) Adds a RIGHT JOIN clause to the query
 * @method     ChildAssoServicepaiementsArchiveQuery innerJoin($relation) Adds a INNER JOIN clause to the query
 *
 * @method     ChildAssoServicepaiementsArchiveQuery leftJoinWith($relation) Adds a LEFT JOIN clause and with to the query
 * @method     ChildAssoServicepaiementsArchiveQuery rightJoinWith($relation) Adds a RIGHT JOIN clause and with to the query
 * @method     ChildAssoServicepaiementsArchiveQuery innerJoinWith($relation) Adds a INNER JOIN clause and with to the query
 *
 * @method     ChildAssoServicepaiementsArchive findOne(ConnectionInterface $con = null) Return the first ChildAssoServicepaiementsArchive matching the query
 * @method     ChildAssoServicepaiementsArchive findOneOrCreate(ConnectionInterface $con = null) Return the first ChildAssoServicepaiementsArchive matching the query, or a new ChildAssoServicepaiementsArchive object populated from the query conditions when no match is found
 *
 * @method     ChildAssoServicepaiementsArchive findOneByIdServicepaiement(string $id_servicepaiement) Return the first ChildAssoServicepaiementsArchive filtered by the id_servicepaiement column
 * @method     ChildAssoServicepaiementsArchive findOneByIdServicerendu(string $id_servicerendu) Return the first ChildAssoServicepaiementsArchive filtered by the id_servicerendu column
 * @method     ChildAssoServicepaiementsArchive findOneByIdPaiement(string $id_paiement) Return the first ChildAssoServicepaiementsArchive filtered by the id_paiement column
 * @method     ChildAssoServicepaiementsArchive findOneByMontant(double $montant) Return the first ChildAssoServicepaiementsArchive filtered by the montant column
 * @method     ChildAssoServicepaiementsArchive findOneByCreatedAt(string $created_at) Return the first ChildAssoServicepaiementsArchive filtered by the created_at column
 * @method     ChildAssoServicepaiementsArchive findOneByUpdatedAt(string $updated_at) Return the first ChildAssoServicepaiementsArchive filtered by the updated_at column
 * @method     ChildAssoServicepaiementsArchive findOneByArchivedAt(string $archived_at) Return the first ChildAssoServicepaiementsArchive filtered by the archived_at column *

 * @method     ChildAssoServicepaiementsArchive requirePk($key, ConnectionInterface $con = null) Return the ChildAssoServicepaiementsArchive by primary key and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildAssoServicepaiementsArchive requireOne(ConnectionInterface $con = null) Return the first ChildAssoServicepaiementsArchive matching the query and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 *
 * @method     ChildAssoServicepaiementsArchive requireOneByIdServicepaiement(string $id_servicepaiement) Return the first ChildAssoServicepaiementsArchive filtered by the id_servicepaiement column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildAssoServicepaiementsArchive requireOneByIdServicerendu(string $id_servicerendu) Return the first ChildAssoServicepaiementsArchive filtered by the id_servicerendu column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildAssoServicepaiementsArchive requireOneByIdPaiement(string $id_paiement) Return the first ChildAssoServicepaiementsArchive filtered by the id_paiement column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildAssoServicepaiementsArchive requireOneByMontant(double $montant) Return the first ChildAssoServicepaiementsArchive filtered by the montant column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildAssoServicepaiementsArchive requireOneByCreatedAt(string $created_at) Return the first ChildAssoServicepaiementsArchive filtered by the created_at column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildAssoServicepaiementsArchive requireOneByUpdatedAt(string $updated_at) Return the first ChildAssoServicepaiementsArchive filtered by the updated_at column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildAssoServicepaiementsArchive requireOneByArchivedAt(string $archived_at) Return the first ChildAssoServicepaiementsArchive filtered by the archived_at column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 *
 * @method     ChildAssoServicepaiementsArchive[]|ObjectCollection find(ConnectionInterface $con = null) Return ChildAssoServicepaiementsArchive objects based on current ModelCriteria
 * @method     ChildAssoServicepaiementsArchive[]|ObjectCollection findByIdServicepaiement(string $id_servicepaiement) Return ChildAssoServicepaiementsArchive objects filtered by the id_servicepaiement column
 * @method     ChildAssoServicepaiementsArchive[]|ObjectCollection findByIdServicerendu(string $id_servicerendu) Return ChildAssoServicepaiementsArchive objects filtered by the id_servicerendu column
 * @method     ChildAssoServicepaiementsArchive[]|ObjectCollection findByIdPaiement(string $id_paiement) Return ChildAssoServicepaiementsArchive objects filtered by the id_paiement column
 * @method     ChildAssoServicepaiementsArchive[]|ObjectCollection findByMontant(double $montant) Return ChildAssoServicepaiementsArchive objects filtered by the montant column
 * @method     ChildAssoServicepaiementsArchive[]|ObjectCollection findByCreatedAt(string $created_at) Return ChildAssoServicepaiementsArchive objects filtered by the created_at column
 * @method     ChildAssoServicepaiementsArchive[]|ObjectCollection findByUpdatedAt(string $updated_at) Return ChildAssoServicepaiementsArchive objects filtered by the updated_at column
 * @method     ChildAssoServicepaiementsArchive[]|ObjectCollection findByArchivedAt(string $archived_at) Return ChildAssoServicepaiementsArchive objects filtered by the archived_at column
 * @method     ChildAssoServicepaiementsArchive[]|\Propel\Runtime\Util\PropelModelPager paginate($page = 1, $maxPerPage = 10, ConnectionInterface $con = null) Issue a SELECT query based on the current ModelCriteria and uses a page and a maximum number of results per page to compute an offset and a limit
 *
 */
abstract class AssoServicepaiementsArchiveQuery extends ModelCriteria
{
    protected $entityNotFoundExceptionClass = '\\Propel\\Runtime\\Exception\\EntityNotFoundException';

    /**
     * Initializes internal state of \Base\AssoServicepaiementsArchiveQuery object.
     *
     * @param     string $dbName The database name
     * @param     string $modelName The phpName of a model, e.g. 'Book'
     * @param     string $modelAlias The alias for the model in this query, e.g. 'b'
     */
    public function __construct($dbName = 'simplasso', $modelName = '\\AssoServicepaiementsArchive', $modelAlias = null)
    {
        parent::__construct($dbName, $modelName, $modelAlias);
    }

    /**
     * Returns a new ChildAssoServicepaiementsArchiveQuery object.
     *
     * @param     string $modelAlias The alias of a model in the query
     * @param     Criteria $criteria Optional Criteria to build the query from
     *
     * @return ChildAssoServicepaiementsArchiveQuery
     */
    public static function create($modelAlias = null, Criteria $criteria = null)
    {
        if ($criteria instanceof ChildAssoServicepaiementsArchiveQuery) {
            return $criteria;
        }
        $query = new ChildAssoServicepaiementsArchiveQuery();
        if (null !== $modelAlias) {
            $query->setModelAlias($modelAlias);
        }
        if ($criteria instanceof Criteria) {
            $query->mergeWith($criteria);
        }

        return $query;
    }

    /**
     * Find object by primary key.
     * Propel uses the instance pool to skip the database if the object exists.
     * Go fast if the query is untouched.
     *
     * <code>
     * $obj  = $c->findPk(12, $con);
     * </code>
     *
     * @param mixed $key Primary key to use for the query
     * @param ConnectionInterface $con an optional connection object
     *
     * @return ChildAssoServicepaiementsArchive|array|mixed the result, formatted by the current formatter
     */
    public function findPk($key, ConnectionInterface $con = null)
    {
        if ($key === null) {
            return null;
        }

        if ($con === null) {
            $con = Propel::getServiceContainer()->getReadConnection(AssoServicepaiementsArchiveTableMap::DATABASE_NAME);
        }

        $this->basePreSelect($con);

        if (
            $this->formatter || $this->modelAlias || $this->with || $this->select
            || $this->selectColumns || $this->asColumns || $this->selectModifiers
            || $this->map || $this->having || $this->joins
        ) {
            return $this->findPkComplex($key, $con);
        }

        if ((null !== ($obj = AssoServicepaiementsArchiveTableMap::getInstanceFromPool(null === $key || is_scalar($key) || is_callable([$key, '__toString']) ? (string) $key : $key)))) {
            // the object is already in the instance pool
            return $obj;
        }

        return $this->findPkSimple($key, $con);
    }

    /**
     * Find object by primary key using raw SQL to go fast.
     * Bypass doSelect() and the object formatter by using generated code.
     *
     * @param     mixed $key Primary key to use for the query
     * @param     ConnectionInterface $con A connection object
     *
     * @throws \Propel\Runtime\Exception\PropelException
     *
     * @return ChildAssoServicepaiementsArchive A model object, or null if the key is not found
     */
    protected function findPkSimple($key, ConnectionInterface $con)
    {
        $sql = 'SELECT `id_servicepaiement`, `id_servicerendu`, `id_paiement`, `montant`, `created_at`, `updated_at`, `archived_at` FROM `asso_servicepaiements_archive` WHERE `id_servicepaiement` = :p0';
        try {
            $stmt = $con->prepare($sql);
            $stmt->bindValue(':p0', $key, PDO::PARAM_INT);
            $stmt->execute();
        } catch (Exception $e) {
            Propel::log($e->getMessage(), Propel::LOG_ERR);
            throw new PropelException(sprintf('Unable to execute SELECT statement [%s]', $sql), 0, $e);
        }
        $obj = null;
        if ($row = $stmt->fetch(\PDO::FETCH_NUM)) {
            /** @var ChildAssoServicepaiementsArchive $obj */
            $obj = new ChildAssoServicepaiementsArchive();
            $obj->hydrate($row);
            AssoServicepaiementsArchiveTableMap::addInstanceToPool($obj, null === $key || is_scalar($key) || is_callable([$key, '__toString']) ? (string) $key : $key);
        }
        $stmt->closeCursor();

        return $obj;
    }

    /**
     * Find object by primary key.
     *
     * @param     mixed $key Primary key to use for the query
     * @param     ConnectionInterface $con A connection object
     *
     * @return ChildAssoServicepaiementsArchive|array|mixed the result, formatted by the current formatter
     */
    protected function findPkComplex($key, ConnectionInterface $con)
    {
        // As the query uses a PK condition, no limit(1) is necessary.
        $criteria = $this->isKeepQuery() ? clone $this : $this;
        $dataFetcher = $criteria
            ->filterByPrimaryKey($key)
            ->doSelect($con);

        return $criteria->getFormatter()->init($criteria)->formatOne($dataFetcher);
    }

    /**
     * Find objects by primary key
     * <code>
     * $objs = $c->findPks(array(12, 56, 832), $con);
     * </code>
     * @param     array $keys Primary keys to use for the query
     * @param     ConnectionInterface $con an optional connection object
     *
     * @return ObjectCollection|array|mixed the list of results, formatted by the current formatter
     */
    public function findPks($keys, ConnectionInterface $con = null)
    {
        if (null === $con) {
            $con = Propel::getServiceContainer()->getReadConnection($this->getDbName());
        }
        $this->basePreSelect($con);
        $criteria = $this->isKeepQuery() ? clone $this : $this;
        $dataFetcher = $criteria
            ->filterByPrimaryKeys($keys)
            ->doSelect($con);

        return $criteria->getFormatter()->init($criteria)->format($dataFetcher);
    }

    /**
     * Filter the query by primary key
     *
     * @param     mixed $key Primary key to use for the query
     *
     * @return $this|ChildAssoServicepaiementsArchiveQuery The current query, for fluid interface
     */
    public function filterByPrimaryKey($key)
    {

        return $this->addUsingAlias(AssoServicepaiementsArchiveTableMap::COL_ID_SERVICEPAIEMENT, $key, Criteria::EQUAL);
    }

    /**
     * Filter the query by a list of primary keys
     *
     * @param     array $keys The list of primary key to use for the query
     *
     * @return $this|ChildAssoServicepaiementsArchiveQuery The current query, for fluid interface
     */
    public function filterByPrimaryKeys($keys)
    {

        return $this->addUsingAlias(AssoServicepaiementsArchiveTableMap::COL_ID_SERVICEPAIEMENT, $keys, Criteria::IN);
    }

    /**
     * Filter the query on the id_servicepaiement column
     *
     * Example usage:
     * <code>
     * $query->filterByIdServicepaiement(1234); // WHERE id_servicepaiement = 1234
     * $query->filterByIdServicepaiement(array(12, 34)); // WHERE id_servicepaiement IN (12, 34)
     * $query->filterByIdServicepaiement(array('min' => 12)); // WHERE id_servicepaiement > 12
     * </code>
     *
     * @param     mixed $idServicepaiement The value to use as filter.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildAssoServicepaiementsArchiveQuery The current query, for fluid interface
     */
    public function filterByIdServicepaiement($idServicepaiement = null, $comparison = null)
    {
        if (is_array($idServicepaiement)) {
            $useMinMax = false;
            if (isset($idServicepaiement['min'])) {
                $this->addUsingAlias(AssoServicepaiementsArchiveTableMap::COL_ID_SERVICEPAIEMENT, $idServicepaiement['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($idServicepaiement['max'])) {
                $this->addUsingAlias(AssoServicepaiementsArchiveTableMap::COL_ID_SERVICEPAIEMENT, $idServicepaiement['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(AssoServicepaiementsArchiveTableMap::COL_ID_SERVICEPAIEMENT, $idServicepaiement, $comparison);
    }

    /**
     * Filter the query on the id_servicerendu column
     *
     * Example usage:
     * <code>
     * $query->filterByIdServicerendu(1234); // WHERE id_servicerendu = 1234
     * $query->filterByIdServicerendu(array(12, 34)); // WHERE id_servicerendu IN (12, 34)
     * $query->filterByIdServicerendu(array('min' => 12)); // WHERE id_servicerendu > 12
     * </code>
     *
     * @param     mixed $idServicerendu The value to use as filter.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildAssoServicepaiementsArchiveQuery The current query, for fluid interface
     */
    public function filterByIdServicerendu($idServicerendu = null, $comparison = null)
    {
        if (is_array($idServicerendu)) {
            $useMinMax = false;
            if (isset($idServicerendu['min'])) {
                $this->addUsingAlias(AssoServicepaiementsArchiveTableMap::COL_ID_SERVICERENDU, $idServicerendu['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($idServicerendu['max'])) {
                $this->addUsingAlias(AssoServicepaiementsArchiveTableMap::COL_ID_SERVICERENDU, $idServicerendu['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(AssoServicepaiementsArchiveTableMap::COL_ID_SERVICERENDU, $idServicerendu, $comparison);
    }

    /**
     * Filter the query on the id_paiement column
     *
     * Example usage:
     * <code>
     * $query->filterByIdPaiement(1234); // WHERE id_paiement = 1234
     * $query->filterByIdPaiement(array(12, 34)); // WHERE id_paiement IN (12, 34)
     * $query->filterByIdPaiement(array('min' => 12)); // WHERE id_paiement > 12
     * </code>
     *
     * @param     mixed $idPaiement The value to use as filter.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildAssoServicepaiementsArchiveQuery The current query, for fluid interface
     */
    public function filterByIdPaiement($idPaiement = null, $comparison = null)
    {
        if (is_array($idPaiement)) {
            $useMinMax = false;
            if (isset($idPaiement['min'])) {
                $this->addUsingAlias(AssoServicepaiementsArchiveTableMap::COL_ID_PAIEMENT, $idPaiement['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($idPaiement['max'])) {
                $this->addUsingAlias(AssoServicepaiementsArchiveTableMap::COL_ID_PAIEMENT, $idPaiement['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(AssoServicepaiementsArchiveTableMap::COL_ID_PAIEMENT, $idPaiement, $comparison);
    }

    /**
     * Filter the query on the montant column
     *
     * Example usage:
     * <code>
     * $query->filterByMontant(1234); // WHERE montant = 1234
     * $query->filterByMontant(array(12, 34)); // WHERE montant IN (12, 34)
     * $query->filterByMontant(array('min' => 12)); // WHERE montant > 12
     * </code>
     *
     * @param     mixed $montant The value to use as filter.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildAssoServicepaiementsArchiveQuery The current query, for fluid interface
     */
    public function filterByMontant($montant = null, $comparison = null)
    {
        if (is_array($montant)) {
            $useMinMax = false;
            if (isset($montant['min'])) {
                $this->addUsingAlias(AssoServicepaiementsArchiveTableMap::COL_MONTANT, $montant['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($montant['max'])) {
                $this->addUsingAlias(AssoServicepaiementsArchiveTableMap::COL_MONTANT, $montant['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(AssoServicepaiementsArchiveTableMap::COL_MONTANT, $montant, $comparison);
    }

    /**
     * Filter the query on the created_at column
     *
     * Example usage:
     * <code>
     * $query->filterByCreatedAt('2011-03-14'); // WHERE created_at = '2011-03-14'
     * $query->filterByCreatedAt('now'); // WHERE created_at = '2011-03-14'
     * $query->filterByCreatedAt(array('max' => 'yesterday')); // WHERE created_at > '2011-03-13'
     * </code>
     *
     * @param     mixed $createdAt The value to use as filter.
     *              Values can be integers (unix timestamps), DateTime objects, or strings.
     *              Empty strings are treated as NULL.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildAssoServicepaiementsArchiveQuery The current query, for fluid interface
     */
    public function filterByCreatedAt($createdAt = null, $comparison = null)
    {
        if (is_array($createdAt)) {
            $useMinMax = false;
            if (isset($createdAt['min'])) {
                $this->addUsingAlias(AssoServicepaiementsArchiveTableMap::COL_CREATED_AT, $createdAt['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($createdAt['max'])) {
                $this->addUsingAlias(AssoServicepaiementsArchiveTableMap::COL_CREATED_AT, $createdAt['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(AssoServicepaiementsArchiveTableMap::COL_CREATED_AT, $createdAt, $comparison);
    }

    /**
     * Filter the query on the updated_at column
     *
     * Example usage:
     * <code>
     * $query->filterByUpdatedAt('2011-03-14'); // WHERE updated_at = '2011-03-14'
     * $query->filterByUpdatedAt('now'); // WHERE updated_at = '2011-03-14'
     * $query->filterByUpdatedAt(array('max' => 'yesterday')); // WHERE updated_at > '2011-03-13'
     * </code>
     *
     * @param     mixed $updatedAt The value to use as filter.
     *              Values can be integers (unix timestamps), DateTime objects, or strings.
     *              Empty strings are treated as NULL.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildAssoServicepaiementsArchiveQuery The current query, for fluid interface
     */
    public function filterByUpdatedAt($updatedAt = null, $comparison = null)
    {
        if (is_array($updatedAt)) {
            $useMinMax = false;
            if (isset($updatedAt['min'])) {
                $this->addUsingAlias(AssoServicepaiementsArchiveTableMap::COL_UPDATED_AT, $updatedAt['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($updatedAt['max'])) {
                $this->addUsingAlias(AssoServicepaiementsArchiveTableMap::COL_UPDATED_AT, $updatedAt['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(AssoServicepaiementsArchiveTableMap::COL_UPDATED_AT, $updatedAt, $comparison);
    }

    /**
     * Filter the query on the archived_at column
     *
     * Example usage:
     * <code>
     * $query->filterByArchivedAt('2011-03-14'); // WHERE archived_at = '2011-03-14'
     * $query->filterByArchivedAt('now'); // WHERE archived_at = '2011-03-14'
     * $query->filterByArchivedAt(array('max' => 'yesterday')); // WHERE archived_at > '2011-03-13'
     * </code>
     *
     * @param     mixed $archivedAt The value to use as filter.
     *              Values can be integers (unix timestamps), DateTime objects, or strings.
     *              Empty strings are treated as NULL.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildAssoServicepaiementsArchiveQuery The current query, for fluid interface
     */
    public function filterByArchivedAt($archivedAt = null, $comparison = null)
    {
        if (is_array($archivedAt)) {
            $useMinMax = false;
            if (isset($archivedAt['min'])) {
                $this->addUsingAlias(AssoServicepaiementsArchiveTableMap::COL_ARCHIVED_AT, $archivedAt['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($archivedAt['max'])) {
                $this->addUsingAlias(AssoServicepaiementsArchiveTableMap::COL_ARCHIVED_AT, $archivedAt['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(AssoServicepaiementsArchiveTableMap::COL_ARCHIVED_AT, $archivedAt, $comparison);
    }

    /**
     * Exclude object from result
     *
     * @param   ChildAssoServicepaiementsArchive $assoServicepaiementsArchive Object to remove from the list of results
     *
     * @return $this|ChildAssoServicepaiementsArchiveQuery The current query, for fluid interface
     */
    public function prune($assoServicepaiementsArchive = null)
    {
        if ($assoServicepaiementsArchive) {
            $this->addUsingAlias(AssoServicepaiementsArchiveTableMap::COL_ID_SERVICEPAIEMENT, $assoServicepaiementsArchive->getIdServicepaiement(), Criteria::NOT_EQUAL);
        }

        return $this;
    }

    /**
     * Deletes all rows from the asso_servicepaiements_archive table.
     *
     * @param ConnectionInterface $con the connection to use
     * @return int The number of affected rows (if supported by underlying database driver).
     */
    public function doDeleteAll(ConnectionInterface $con = null)
    {
        if (null === $con) {
            $con = Propel::getServiceContainer()->getWriteConnection(AssoServicepaiementsArchiveTableMap::DATABASE_NAME);
        }

        // use transaction because $criteria could contain info
        // for more than one table or we could emulating ON DELETE CASCADE, etc.
        return $con->transaction(function () use ($con) {
            $affectedRows = 0; // initialize var to track total num of affected rows
            $affectedRows += parent::doDeleteAll($con);
            // Because this db requires some delete cascade/set null emulation, we have to
            // clear the cached instance *after* the emulation has happened (since
            // instances get re-added by the select statement contained therein).
            AssoServicepaiementsArchiveTableMap::clearInstancePool();
            AssoServicepaiementsArchiveTableMap::clearRelatedInstancePool();

            return $affectedRows;
        });
    }

    /**
     * Performs a DELETE on the database based on the current ModelCriteria
     *
     * @param ConnectionInterface $con the connection to use
     * @return int             The number of affected rows (if supported by underlying database driver).  This includes CASCADE-related rows
     *                         if supported by native driver or if emulated using Propel.
     * @throws PropelException Any exceptions caught during processing will be
     *                         rethrown wrapped into a PropelException.
     */
    public function delete(ConnectionInterface $con = null)
    {
        if (null === $con) {
            $con = Propel::getServiceContainer()->getWriteConnection(AssoServicepaiementsArchiveTableMap::DATABASE_NAME);
        }

        $criteria = $this;

        // Set the correct dbName
        $criteria->setDbName(AssoServicepaiementsArchiveTableMap::DATABASE_NAME);

        // use transaction because $criteria could contain info
        // for more than one table or we could emulating ON DELETE CASCADE, etc.
        return $con->transaction(function () use ($con, $criteria) {
            $affectedRows = 0; // initialize var to track total num of affected rows

            AssoServicepaiementsArchiveTableMap::removeInstanceFromPool($criteria);

            $affectedRows += ModelCriteria::delete($con);
            AssoServicepaiementsArchiveTableMap::clearRelatedInstancePool();

            return $affectedRows;
        });
    }

} // AssoServicepaiementsArchiveQuery
