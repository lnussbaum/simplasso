<?php

namespace Base;

use \AssoImportsArchive as ChildAssoImportsArchive;
use \AssoImportsArchiveQuery as ChildAssoImportsArchiveQuery;
use \Exception;
use \PDO;
use Map\AssoImportsArchiveTableMap;
use Propel\Runtime\Propel;
use Propel\Runtime\ActiveQuery\Criteria;
use Propel\Runtime\ActiveQuery\ModelCriteria;
use Propel\Runtime\Collection\ObjectCollection;
use Propel\Runtime\Connection\ConnectionInterface;
use Propel\Runtime\Exception\PropelException;

/**
 * Base class that represents a query for the 'asso_imports_archive' table.
 *
 *
 *
 * @method     ChildAssoImportsArchiveQuery orderByIdImport($order = Criteria::ASC) Order by the id_import column
 * @method     ChildAssoImportsArchiveQuery orderByModele($order = Criteria::ASC) Order by the modele column
 * @method     ChildAssoImportsArchiveQuery orderByAvancement($order = Criteria::ASC) Order by the avancement column
 * @method     ChildAssoImportsArchiveQuery orderByNom($order = Criteria::ASC) Order by the nom column
 * @method     ChildAssoImportsArchiveQuery orderByNomprovisoire($order = Criteria::ASC) Order by the nomprovisoire column
 * @method     ChildAssoImportsArchiveQuery orderByTaille($order = Criteria::ASC) Order by the taille column
 * @method     ChildAssoImportsArchiveQuery orderByControlemd5($order = Criteria::ASC) Order by the controlemd5 column
 * @method     ChildAssoImportsArchiveQuery orderByOriginals($order = Criteria::ASC) Order by the originals column
 * @method     ChildAssoImportsArchiveQuery orderByInformations($order = Criteria::ASC) Order by the informations column
 * @method     ChildAssoImportsArchiveQuery orderByModifications($order = Criteria::ASC) Order by the modifications column
 * @method     ChildAssoImportsArchiveQuery orderByNbLigne($order = Criteria::ASC) Order by the nb_ligne column
 * @method     ChildAssoImportsArchiveQuery orderByLigneEnCours($order = Criteria::ASC) Order by the ligne_en_cours column
 * @method     ChildAssoImportsArchiveQuery orderByCreatedAt($order = Criteria::ASC) Order by the created_at column
 * @method     ChildAssoImportsArchiveQuery orderByUpdatedAt($order = Criteria::ASC) Order by the updated_at column
 * @method     ChildAssoImportsArchiveQuery orderByArchivedAt($order = Criteria::ASC) Order by the archived_at column
 *
 * @method     ChildAssoImportsArchiveQuery groupByIdImport() Group by the id_import column
 * @method     ChildAssoImportsArchiveQuery groupByModele() Group by the modele column
 * @method     ChildAssoImportsArchiveQuery groupByAvancement() Group by the avancement column
 * @method     ChildAssoImportsArchiveQuery groupByNom() Group by the nom column
 * @method     ChildAssoImportsArchiveQuery groupByNomprovisoire() Group by the nomprovisoire column
 * @method     ChildAssoImportsArchiveQuery groupByTaille() Group by the taille column
 * @method     ChildAssoImportsArchiveQuery groupByControlemd5() Group by the controlemd5 column
 * @method     ChildAssoImportsArchiveQuery groupByOriginals() Group by the originals column
 * @method     ChildAssoImportsArchiveQuery groupByInformations() Group by the informations column
 * @method     ChildAssoImportsArchiveQuery groupByModifications() Group by the modifications column
 * @method     ChildAssoImportsArchiveQuery groupByNbLigne() Group by the nb_ligne column
 * @method     ChildAssoImportsArchiveQuery groupByLigneEnCours() Group by the ligne_en_cours column
 * @method     ChildAssoImportsArchiveQuery groupByCreatedAt() Group by the created_at column
 * @method     ChildAssoImportsArchiveQuery groupByUpdatedAt() Group by the updated_at column
 * @method     ChildAssoImportsArchiveQuery groupByArchivedAt() Group by the archived_at column
 *
 * @method     ChildAssoImportsArchiveQuery leftJoin($relation) Adds a LEFT JOIN clause to the query
 * @method     ChildAssoImportsArchiveQuery rightJoin($relation) Adds a RIGHT JOIN clause to the query
 * @method     ChildAssoImportsArchiveQuery innerJoin($relation) Adds a INNER JOIN clause to the query
 *
 * @method     ChildAssoImportsArchiveQuery leftJoinWith($relation) Adds a LEFT JOIN clause and with to the query
 * @method     ChildAssoImportsArchiveQuery rightJoinWith($relation) Adds a RIGHT JOIN clause and with to the query
 * @method     ChildAssoImportsArchiveQuery innerJoinWith($relation) Adds a INNER JOIN clause and with to the query
 *
 * @method     ChildAssoImportsArchive findOne(ConnectionInterface $con = null) Return the first ChildAssoImportsArchive matching the query
 * @method     ChildAssoImportsArchive findOneOrCreate(ConnectionInterface $con = null) Return the first ChildAssoImportsArchive matching the query, or a new ChildAssoImportsArchive object populated from the query conditions when no match is found
 *
 * @method     ChildAssoImportsArchive findOneByIdImport(string $id_import) Return the first ChildAssoImportsArchive filtered by the id_import column
 * @method     ChildAssoImportsArchive findOneByModele(string $modele) Return the first ChildAssoImportsArchive filtered by the modele column
 * @method     ChildAssoImportsArchive findOneByAvancement(int $avancement) Return the first ChildAssoImportsArchive filtered by the avancement column
 * @method     ChildAssoImportsArchive findOneByNom(string $nom) Return the first ChildAssoImportsArchive filtered by the nom column
 * @method     ChildAssoImportsArchive findOneByNomprovisoire(string $nomprovisoire) Return the first ChildAssoImportsArchive filtered by the nomprovisoire column
 * @method     ChildAssoImportsArchive findOneByTaille(string $taille) Return the first ChildAssoImportsArchive filtered by the taille column
 * @method     ChildAssoImportsArchive findOneByControlemd5(string $controlemd5) Return the first ChildAssoImportsArchive filtered by the controlemd5 column
 * @method     ChildAssoImportsArchive findOneByOriginals(string $originals) Return the first ChildAssoImportsArchive filtered by the originals column
 * @method     ChildAssoImportsArchive findOneByInformations(string $informations) Return the first ChildAssoImportsArchive filtered by the informations column
 * @method     ChildAssoImportsArchive findOneByModifications(string $modifications) Return the first ChildAssoImportsArchive filtered by the modifications column
 * @method     ChildAssoImportsArchive findOneByNbLigne(string $nb_ligne) Return the first ChildAssoImportsArchive filtered by the nb_ligne column
 * @method     ChildAssoImportsArchive findOneByLigneEnCours(string $ligne_en_cours) Return the first ChildAssoImportsArchive filtered by the ligne_en_cours column
 * @method     ChildAssoImportsArchive findOneByCreatedAt(string $created_at) Return the first ChildAssoImportsArchive filtered by the created_at column
 * @method     ChildAssoImportsArchive findOneByUpdatedAt(string $updated_at) Return the first ChildAssoImportsArchive filtered by the updated_at column
 * @method     ChildAssoImportsArchive findOneByArchivedAt(string $archived_at) Return the first ChildAssoImportsArchive filtered by the archived_at column *

 * @method     ChildAssoImportsArchive requirePk($key, ConnectionInterface $con = null) Return the ChildAssoImportsArchive by primary key and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildAssoImportsArchive requireOne(ConnectionInterface $con = null) Return the first ChildAssoImportsArchive matching the query and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 *
 * @method     ChildAssoImportsArchive requireOneByIdImport(string $id_import) Return the first ChildAssoImportsArchive filtered by the id_import column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildAssoImportsArchive requireOneByModele(string $modele) Return the first ChildAssoImportsArchive filtered by the modele column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildAssoImportsArchive requireOneByAvancement(int $avancement) Return the first ChildAssoImportsArchive filtered by the avancement column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildAssoImportsArchive requireOneByNom(string $nom) Return the first ChildAssoImportsArchive filtered by the nom column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildAssoImportsArchive requireOneByNomprovisoire(string $nomprovisoire) Return the first ChildAssoImportsArchive filtered by the nomprovisoire column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildAssoImportsArchive requireOneByTaille(string $taille) Return the first ChildAssoImportsArchive filtered by the taille column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildAssoImportsArchive requireOneByControlemd5(string $controlemd5) Return the first ChildAssoImportsArchive filtered by the controlemd5 column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildAssoImportsArchive requireOneByOriginals(string $originals) Return the first ChildAssoImportsArchive filtered by the originals column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildAssoImportsArchive requireOneByInformations(string $informations) Return the first ChildAssoImportsArchive filtered by the informations column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildAssoImportsArchive requireOneByModifications(string $modifications) Return the first ChildAssoImportsArchive filtered by the modifications column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildAssoImportsArchive requireOneByNbLigne(string $nb_ligne) Return the first ChildAssoImportsArchive filtered by the nb_ligne column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildAssoImportsArchive requireOneByLigneEnCours(string $ligne_en_cours) Return the first ChildAssoImportsArchive filtered by the ligne_en_cours column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildAssoImportsArchive requireOneByCreatedAt(string $created_at) Return the first ChildAssoImportsArchive filtered by the created_at column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildAssoImportsArchive requireOneByUpdatedAt(string $updated_at) Return the first ChildAssoImportsArchive filtered by the updated_at column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildAssoImportsArchive requireOneByArchivedAt(string $archived_at) Return the first ChildAssoImportsArchive filtered by the archived_at column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 *
 * @method     ChildAssoImportsArchive[]|ObjectCollection find(ConnectionInterface $con = null) Return ChildAssoImportsArchive objects based on current ModelCriteria
 * @method     ChildAssoImportsArchive[]|ObjectCollection findByIdImport(string $id_import) Return ChildAssoImportsArchive objects filtered by the id_import column
 * @method     ChildAssoImportsArchive[]|ObjectCollection findByModele(string $modele) Return ChildAssoImportsArchive objects filtered by the modele column
 * @method     ChildAssoImportsArchive[]|ObjectCollection findByAvancement(int $avancement) Return ChildAssoImportsArchive objects filtered by the avancement column
 * @method     ChildAssoImportsArchive[]|ObjectCollection findByNom(string $nom) Return ChildAssoImportsArchive objects filtered by the nom column
 * @method     ChildAssoImportsArchive[]|ObjectCollection findByNomprovisoire(string $nomprovisoire) Return ChildAssoImportsArchive objects filtered by the nomprovisoire column
 * @method     ChildAssoImportsArchive[]|ObjectCollection findByTaille(string $taille) Return ChildAssoImportsArchive objects filtered by the taille column
 * @method     ChildAssoImportsArchive[]|ObjectCollection findByControlemd5(string $controlemd5) Return ChildAssoImportsArchive objects filtered by the controlemd5 column
 * @method     ChildAssoImportsArchive[]|ObjectCollection findByOriginals(string $originals) Return ChildAssoImportsArchive objects filtered by the originals column
 * @method     ChildAssoImportsArchive[]|ObjectCollection findByInformations(string $informations) Return ChildAssoImportsArchive objects filtered by the informations column
 * @method     ChildAssoImportsArchive[]|ObjectCollection findByModifications(string $modifications) Return ChildAssoImportsArchive objects filtered by the modifications column
 * @method     ChildAssoImportsArchive[]|ObjectCollection findByNbLigne(string $nb_ligne) Return ChildAssoImportsArchive objects filtered by the nb_ligne column
 * @method     ChildAssoImportsArchive[]|ObjectCollection findByLigneEnCours(string $ligne_en_cours) Return ChildAssoImportsArchive objects filtered by the ligne_en_cours column
 * @method     ChildAssoImportsArchive[]|ObjectCollection findByCreatedAt(string $created_at) Return ChildAssoImportsArchive objects filtered by the created_at column
 * @method     ChildAssoImportsArchive[]|ObjectCollection findByUpdatedAt(string $updated_at) Return ChildAssoImportsArchive objects filtered by the updated_at column
 * @method     ChildAssoImportsArchive[]|ObjectCollection findByArchivedAt(string $archived_at) Return ChildAssoImportsArchive objects filtered by the archived_at column
 * @method     ChildAssoImportsArchive[]|\Propel\Runtime\Util\PropelModelPager paginate($page = 1, $maxPerPage = 10, ConnectionInterface $con = null) Issue a SELECT query based on the current ModelCriteria and uses a page and a maximum number of results per page to compute an offset and a limit
 *
 */
abstract class AssoImportsArchiveQuery extends ModelCriteria
{
    protected $entityNotFoundExceptionClass = '\\Propel\\Runtime\\Exception\\EntityNotFoundException';

    /**
     * Initializes internal state of \Base\AssoImportsArchiveQuery object.
     *
     * @param     string $dbName The database name
     * @param     string $modelName The phpName of a model, e.g. 'Book'
     * @param     string $modelAlias The alias for the model in this query, e.g. 'b'
     */
    public function __construct($dbName = 'simplasso', $modelName = '\\AssoImportsArchive', $modelAlias = null)
    {
        parent::__construct($dbName, $modelName, $modelAlias);
    }

    /**
     * Returns a new ChildAssoImportsArchiveQuery object.
     *
     * @param     string $modelAlias The alias of a model in the query
     * @param     Criteria $criteria Optional Criteria to build the query from
     *
     * @return ChildAssoImportsArchiveQuery
     */
    public static function create($modelAlias = null, Criteria $criteria = null)
    {
        if ($criteria instanceof ChildAssoImportsArchiveQuery) {
            return $criteria;
        }
        $query = new ChildAssoImportsArchiveQuery();
        if (null !== $modelAlias) {
            $query->setModelAlias($modelAlias);
        }
        if ($criteria instanceof Criteria) {
            $query->mergeWith($criteria);
        }

        return $query;
    }

    /**
     * Find object by primary key.
     * Propel uses the instance pool to skip the database if the object exists.
     * Go fast if the query is untouched.
     *
     * <code>
     * $obj  = $c->findPk(12, $con);
     * </code>
     *
     * @param mixed $key Primary key to use for the query
     * @param ConnectionInterface $con an optional connection object
     *
     * @return ChildAssoImportsArchive|array|mixed the result, formatted by the current formatter
     */
    public function findPk($key, ConnectionInterface $con = null)
    {
        if ($key === null) {
            return null;
        }

        if ($con === null) {
            $con = Propel::getServiceContainer()->getReadConnection(AssoImportsArchiveTableMap::DATABASE_NAME);
        }

        $this->basePreSelect($con);

        if (
            $this->formatter || $this->modelAlias || $this->with || $this->select
            || $this->selectColumns || $this->asColumns || $this->selectModifiers
            || $this->map || $this->having || $this->joins
        ) {
            return $this->findPkComplex($key, $con);
        }

        if ((null !== ($obj = AssoImportsArchiveTableMap::getInstanceFromPool(null === $key || is_scalar($key) || is_callable([$key, '__toString']) ? (string) $key : $key)))) {
            // the object is already in the instance pool
            return $obj;
        }

        return $this->findPkSimple($key, $con);
    }

    /**
     * Find object by primary key using raw SQL to go fast.
     * Bypass doSelect() and the object formatter by using generated code.
     *
     * @param     mixed $key Primary key to use for the query
     * @param     ConnectionInterface $con A connection object
     *
     * @throws \Propel\Runtime\Exception\PropelException
     *
     * @return ChildAssoImportsArchive A model object, or null if the key is not found
     */
    protected function findPkSimple($key, ConnectionInterface $con)
    {
        $sql = 'SELECT `id_import`, `modele`, `avancement`, `nom`, `nomprovisoire`, `taille`, `controlemd5`, `nb_ligne`, `ligne_en_cours`, `created_at`, `updated_at`, `archived_at` FROM `asso_imports_archive` WHERE `id_import` = :p0';
        try {
            $stmt = $con->prepare($sql);
            $stmt->bindValue(':p0', $key, PDO::PARAM_INT);
            $stmt->execute();
        } catch (Exception $e) {
            Propel::log($e->getMessage(), Propel::LOG_ERR);
            throw new PropelException(sprintf('Unable to execute SELECT statement [%s]', $sql), 0, $e);
        }
        $obj = null;
        if ($row = $stmt->fetch(\PDO::FETCH_NUM)) {
            /** @var ChildAssoImportsArchive $obj */
            $obj = new ChildAssoImportsArchive();
            $obj->hydrate($row);
            AssoImportsArchiveTableMap::addInstanceToPool($obj, null === $key || is_scalar($key) || is_callable([$key, '__toString']) ? (string) $key : $key);
        }
        $stmt->closeCursor();

        return $obj;
    }

    /**
     * Find object by primary key.
     *
     * @param     mixed $key Primary key to use for the query
     * @param     ConnectionInterface $con A connection object
     *
     * @return ChildAssoImportsArchive|array|mixed the result, formatted by the current formatter
     */
    protected function findPkComplex($key, ConnectionInterface $con)
    {
        // As the query uses a PK condition, no limit(1) is necessary.
        $criteria = $this->isKeepQuery() ? clone $this : $this;
        $dataFetcher = $criteria
            ->filterByPrimaryKey($key)
            ->doSelect($con);

        return $criteria->getFormatter()->init($criteria)->formatOne($dataFetcher);
    }

    /**
     * Find objects by primary key
     * <code>
     * $objs = $c->findPks(array(12, 56, 832), $con);
     * </code>
     * @param     array $keys Primary keys to use for the query
     * @param     ConnectionInterface $con an optional connection object
     *
     * @return ObjectCollection|array|mixed the list of results, formatted by the current formatter
     */
    public function findPks($keys, ConnectionInterface $con = null)
    {
        if (null === $con) {
            $con = Propel::getServiceContainer()->getReadConnection($this->getDbName());
        }
        $this->basePreSelect($con);
        $criteria = $this->isKeepQuery() ? clone $this : $this;
        $dataFetcher = $criteria
            ->filterByPrimaryKeys($keys)
            ->doSelect($con);

        return $criteria->getFormatter()->init($criteria)->format($dataFetcher);
    }

    /**
     * Filter the query by primary key
     *
     * @param     mixed $key Primary key to use for the query
     *
     * @return $this|ChildAssoImportsArchiveQuery The current query, for fluid interface
     */
    public function filterByPrimaryKey($key)
    {

        return $this->addUsingAlias(AssoImportsArchiveTableMap::COL_ID_IMPORT, $key, Criteria::EQUAL);
    }

    /**
     * Filter the query by a list of primary keys
     *
     * @param     array $keys The list of primary key to use for the query
     *
     * @return $this|ChildAssoImportsArchiveQuery The current query, for fluid interface
     */
    public function filterByPrimaryKeys($keys)
    {

        return $this->addUsingAlias(AssoImportsArchiveTableMap::COL_ID_IMPORT, $keys, Criteria::IN);
    }

    /**
     * Filter the query on the id_import column
     *
     * Example usage:
     * <code>
     * $query->filterByIdImport(1234); // WHERE id_import = 1234
     * $query->filterByIdImport(array(12, 34)); // WHERE id_import IN (12, 34)
     * $query->filterByIdImport(array('min' => 12)); // WHERE id_import > 12
     * </code>
     *
     * @param     mixed $idImport The value to use as filter.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildAssoImportsArchiveQuery The current query, for fluid interface
     */
    public function filterByIdImport($idImport = null, $comparison = null)
    {
        if (is_array($idImport)) {
            $useMinMax = false;
            if (isset($idImport['min'])) {
                $this->addUsingAlias(AssoImportsArchiveTableMap::COL_ID_IMPORT, $idImport['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($idImport['max'])) {
                $this->addUsingAlias(AssoImportsArchiveTableMap::COL_ID_IMPORT, $idImport['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(AssoImportsArchiveTableMap::COL_ID_IMPORT, $idImport, $comparison);
    }

    /**
     * Filter the query on the modele column
     *
     * Example usage:
     * <code>
     * $query->filterByModele('fooValue');   // WHERE modele = 'fooValue'
     * $query->filterByModele('%fooValue%', Criteria::LIKE); // WHERE modele LIKE '%fooValue%'
     * </code>
     *
     * @param     string $modele The value to use as filter.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildAssoImportsArchiveQuery The current query, for fluid interface
     */
    public function filterByModele($modele = null, $comparison = null)
    {
        if (null === $comparison) {
            if (is_array($modele)) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(AssoImportsArchiveTableMap::COL_MODELE, $modele, $comparison);
    }

    /**
     * Filter the query on the avancement column
     *
     * Example usage:
     * <code>
     * $query->filterByAvancement(1234); // WHERE avancement = 1234
     * $query->filterByAvancement(array(12, 34)); // WHERE avancement IN (12, 34)
     * $query->filterByAvancement(array('min' => 12)); // WHERE avancement > 12
     * </code>
     *
     * @param     mixed $avancement The value to use as filter.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildAssoImportsArchiveQuery The current query, for fluid interface
     */
    public function filterByAvancement($avancement = null, $comparison = null)
    {
        if (is_array($avancement)) {
            $useMinMax = false;
            if (isset($avancement['min'])) {
                $this->addUsingAlias(AssoImportsArchiveTableMap::COL_AVANCEMENT, $avancement['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($avancement['max'])) {
                $this->addUsingAlias(AssoImportsArchiveTableMap::COL_AVANCEMENT, $avancement['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(AssoImportsArchiveTableMap::COL_AVANCEMENT, $avancement, $comparison);
    }

    /**
     * Filter the query on the nom column
     *
     * Example usage:
     * <code>
     * $query->filterByNom('fooValue');   // WHERE nom = 'fooValue'
     * $query->filterByNom('%fooValue%', Criteria::LIKE); // WHERE nom LIKE '%fooValue%'
     * </code>
     *
     * @param     string $nom The value to use as filter.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildAssoImportsArchiveQuery The current query, for fluid interface
     */
    public function filterByNom($nom = null, $comparison = null)
    {
        if (null === $comparison) {
            if (is_array($nom)) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(AssoImportsArchiveTableMap::COL_NOM, $nom, $comparison);
    }

    /**
     * Filter the query on the nomprovisoire column
     *
     * Example usage:
     * <code>
     * $query->filterByNomprovisoire('fooValue');   // WHERE nomprovisoire = 'fooValue'
     * $query->filterByNomprovisoire('%fooValue%', Criteria::LIKE); // WHERE nomprovisoire LIKE '%fooValue%'
     * </code>
     *
     * @param     string $nomprovisoire The value to use as filter.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildAssoImportsArchiveQuery The current query, for fluid interface
     */
    public function filterByNomprovisoire($nomprovisoire = null, $comparison = null)
    {
        if (null === $comparison) {
            if (is_array($nomprovisoire)) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(AssoImportsArchiveTableMap::COL_NOMPROVISOIRE, $nomprovisoire, $comparison);
    }

    /**
     * Filter the query on the taille column
     *
     * Example usage:
     * <code>
     * $query->filterByTaille(1234); // WHERE taille = 1234
     * $query->filterByTaille(array(12, 34)); // WHERE taille IN (12, 34)
     * $query->filterByTaille(array('min' => 12)); // WHERE taille > 12
     * </code>
     *
     * @param     mixed $taille The value to use as filter.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildAssoImportsArchiveQuery The current query, for fluid interface
     */
    public function filterByTaille($taille = null, $comparison = null)
    {
        if (is_array($taille)) {
            $useMinMax = false;
            if (isset($taille['min'])) {
                $this->addUsingAlias(AssoImportsArchiveTableMap::COL_TAILLE, $taille['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($taille['max'])) {
                $this->addUsingAlias(AssoImportsArchiveTableMap::COL_TAILLE, $taille['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(AssoImportsArchiveTableMap::COL_TAILLE, $taille, $comparison);
    }

    /**
     * Filter the query on the controlemd5 column
     *
     * Example usage:
     * <code>
     * $query->filterByControlemd5('fooValue');   // WHERE controlemd5 = 'fooValue'
     * $query->filterByControlemd5('%fooValue%', Criteria::LIKE); // WHERE controlemd5 LIKE '%fooValue%'
     * </code>
     *
     * @param     string $controlemd5 The value to use as filter.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildAssoImportsArchiveQuery The current query, for fluid interface
     */
    public function filterByControlemd5($controlemd5 = null, $comparison = null)
    {
        if (null === $comparison) {
            if (is_array($controlemd5)) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(AssoImportsArchiveTableMap::COL_CONTROLEMD5, $controlemd5, $comparison);
    }

    /**
     * Filter the query on the originals column
     *
     * Example usage:
     * <code>
     * $query->filterByOriginals('fooValue');   // WHERE originals = 'fooValue'
     * $query->filterByOriginals('%fooValue%', Criteria::LIKE); // WHERE originals LIKE '%fooValue%'
     * </code>
     *
     * @param     string $originals The value to use as filter.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildAssoImportsArchiveQuery The current query, for fluid interface
     */
    public function filterByOriginals($originals = null, $comparison = null)
    {
        if (null === $comparison) {
            if (is_array($originals)) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(AssoImportsArchiveTableMap::COL_ORIGINALS, $originals, $comparison);
    }

    /**
     * Filter the query on the informations column
     *
     * Example usage:
     * <code>
     * $query->filterByInformations('fooValue');   // WHERE informations = 'fooValue'
     * $query->filterByInformations('%fooValue%', Criteria::LIKE); // WHERE informations LIKE '%fooValue%'
     * </code>
     *
     * @param     string $informations The value to use as filter.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildAssoImportsArchiveQuery The current query, for fluid interface
     */
    public function filterByInformations($informations = null, $comparison = null)
    {
        if (null === $comparison) {
            if (is_array($informations)) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(AssoImportsArchiveTableMap::COL_INFORMATIONS, $informations, $comparison);
    }

    /**
     * Filter the query on the modifications column
     *
     * Example usage:
     * <code>
     * $query->filterByModifications('fooValue');   // WHERE modifications = 'fooValue'
     * $query->filterByModifications('%fooValue%', Criteria::LIKE); // WHERE modifications LIKE '%fooValue%'
     * </code>
     *
     * @param     string $modifications The value to use as filter.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildAssoImportsArchiveQuery The current query, for fluid interface
     */
    public function filterByModifications($modifications = null, $comparison = null)
    {
        if (null === $comparison) {
            if (is_array($modifications)) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(AssoImportsArchiveTableMap::COL_MODIFICATIONS, $modifications, $comparison);
    }

    /**
     * Filter the query on the nb_ligne column
     *
     * Example usage:
     * <code>
     * $query->filterByNbLigne(1234); // WHERE nb_ligne = 1234
     * $query->filterByNbLigne(array(12, 34)); // WHERE nb_ligne IN (12, 34)
     * $query->filterByNbLigne(array('min' => 12)); // WHERE nb_ligne > 12
     * </code>
     *
     * @param     mixed $nbLigne The value to use as filter.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildAssoImportsArchiveQuery The current query, for fluid interface
     */
    public function filterByNbLigne($nbLigne = null, $comparison = null)
    {
        if (is_array($nbLigne)) {
            $useMinMax = false;
            if (isset($nbLigne['min'])) {
                $this->addUsingAlias(AssoImportsArchiveTableMap::COL_NB_LIGNE, $nbLigne['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($nbLigne['max'])) {
                $this->addUsingAlias(AssoImportsArchiveTableMap::COL_NB_LIGNE, $nbLigne['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(AssoImportsArchiveTableMap::COL_NB_LIGNE, $nbLigne, $comparison);
    }

    /**
     * Filter the query on the ligne_en_cours column
     *
     * Example usage:
     * <code>
     * $query->filterByLigneEnCours(1234); // WHERE ligne_en_cours = 1234
     * $query->filterByLigneEnCours(array(12, 34)); // WHERE ligne_en_cours IN (12, 34)
     * $query->filterByLigneEnCours(array('min' => 12)); // WHERE ligne_en_cours > 12
     * </code>
     *
     * @param     mixed $ligneEnCours The value to use as filter.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildAssoImportsArchiveQuery The current query, for fluid interface
     */
    public function filterByLigneEnCours($ligneEnCours = null, $comparison = null)
    {
        if (is_array($ligneEnCours)) {
            $useMinMax = false;
            if (isset($ligneEnCours['min'])) {
                $this->addUsingAlias(AssoImportsArchiveTableMap::COL_LIGNE_EN_COURS, $ligneEnCours['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($ligneEnCours['max'])) {
                $this->addUsingAlias(AssoImportsArchiveTableMap::COL_LIGNE_EN_COURS, $ligneEnCours['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(AssoImportsArchiveTableMap::COL_LIGNE_EN_COURS, $ligneEnCours, $comparison);
    }

    /**
     * Filter the query on the created_at column
     *
     * Example usage:
     * <code>
     * $query->filterByCreatedAt('2011-03-14'); // WHERE created_at = '2011-03-14'
     * $query->filterByCreatedAt('now'); // WHERE created_at = '2011-03-14'
     * $query->filterByCreatedAt(array('max' => 'yesterday')); // WHERE created_at > '2011-03-13'
     * </code>
     *
     * @param     mixed $createdAt The value to use as filter.
     *              Values can be integers (unix timestamps), DateTime objects, or strings.
     *              Empty strings are treated as NULL.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildAssoImportsArchiveQuery The current query, for fluid interface
     */
    public function filterByCreatedAt($createdAt = null, $comparison = null)
    {
        if (is_array($createdAt)) {
            $useMinMax = false;
            if (isset($createdAt['min'])) {
                $this->addUsingAlias(AssoImportsArchiveTableMap::COL_CREATED_AT, $createdAt['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($createdAt['max'])) {
                $this->addUsingAlias(AssoImportsArchiveTableMap::COL_CREATED_AT, $createdAt['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(AssoImportsArchiveTableMap::COL_CREATED_AT, $createdAt, $comparison);
    }

    /**
     * Filter the query on the updated_at column
     *
     * Example usage:
     * <code>
     * $query->filterByUpdatedAt('2011-03-14'); // WHERE updated_at = '2011-03-14'
     * $query->filterByUpdatedAt('now'); // WHERE updated_at = '2011-03-14'
     * $query->filterByUpdatedAt(array('max' => 'yesterday')); // WHERE updated_at > '2011-03-13'
     * </code>
     *
     * @param     mixed $updatedAt The value to use as filter.
     *              Values can be integers (unix timestamps), DateTime objects, or strings.
     *              Empty strings are treated as NULL.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildAssoImportsArchiveQuery The current query, for fluid interface
     */
    public function filterByUpdatedAt($updatedAt = null, $comparison = null)
    {
        if (is_array($updatedAt)) {
            $useMinMax = false;
            if (isset($updatedAt['min'])) {
                $this->addUsingAlias(AssoImportsArchiveTableMap::COL_UPDATED_AT, $updatedAt['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($updatedAt['max'])) {
                $this->addUsingAlias(AssoImportsArchiveTableMap::COL_UPDATED_AT, $updatedAt['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(AssoImportsArchiveTableMap::COL_UPDATED_AT, $updatedAt, $comparison);
    }

    /**
     * Filter the query on the archived_at column
     *
     * Example usage:
     * <code>
     * $query->filterByArchivedAt('2011-03-14'); // WHERE archived_at = '2011-03-14'
     * $query->filterByArchivedAt('now'); // WHERE archived_at = '2011-03-14'
     * $query->filterByArchivedAt(array('max' => 'yesterday')); // WHERE archived_at > '2011-03-13'
     * </code>
     *
     * @param     mixed $archivedAt The value to use as filter.
     *              Values can be integers (unix timestamps), DateTime objects, or strings.
     *              Empty strings are treated as NULL.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildAssoImportsArchiveQuery The current query, for fluid interface
     */
    public function filterByArchivedAt($archivedAt = null, $comparison = null)
    {
        if (is_array($archivedAt)) {
            $useMinMax = false;
            if (isset($archivedAt['min'])) {
                $this->addUsingAlias(AssoImportsArchiveTableMap::COL_ARCHIVED_AT, $archivedAt['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($archivedAt['max'])) {
                $this->addUsingAlias(AssoImportsArchiveTableMap::COL_ARCHIVED_AT, $archivedAt['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(AssoImportsArchiveTableMap::COL_ARCHIVED_AT, $archivedAt, $comparison);
    }

    /**
     * Exclude object from result
     *
     * @param   ChildAssoImportsArchive $assoImportsArchive Object to remove from the list of results
     *
     * @return $this|ChildAssoImportsArchiveQuery The current query, for fluid interface
     */
    public function prune($assoImportsArchive = null)
    {
        if ($assoImportsArchive) {
            $this->addUsingAlias(AssoImportsArchiveTableMap::COL_ID_IMPORT, $assoImportsArchive->getIdImport(), Criteria::NOT_EQUAL);
        }

        return $this;
    }

    /**
     * Deletes all rows from the asso_imports_archive table.
     *
     * @param ConnectionInterface $con the connection to use
     * @return int The number of affected rows (if supported by underlying database driver).
     */
    public function doDeleteAll(ConnectionInterface $con = null)
    {
        if (null === $con) {
            $con = Propel::getServiceContainer()->getWriteConnection(AssoImportsArchiveTableMap::DATABASE_NAME);
        }

        // use transaction because $criteria could contain info
        // for more than one table or we could emulating ON DELETE CASCADE, etc.
        return $con->transaction(function () use ($con) {
            $affectedRows = 0; // initialize var to track total num of affected rows
            $affectedRows += parent::doDeleteAll($con);
            // Because this db requires some delete cascade/set null emulation, we have to
            // clear the cached instance *after* the emulation has happened (since
            // instances get re-added by the select statement contained therein).
            AssoImportsArchiveTableMap::clearInstancePool();
            AssoImportsArchiveTableMap::clearRelatedInstancePool();

            return $affectedRows;
        });
    }

    /**
     * Performs a DELETE on the database based on the current ModelCriteria
     *
     * @param ConnectionInterface $con the connection to use
     * @return int             The number of affected rows (if supported by underlying database driver).  This includes CASCADE-related rows
     *                         if supported by native driver or if emulated using Propel.
     * @throws PropelException Any exceptions caught during processing will be
     *                         rethrown wrapped into a PropelException.
     */
    public function delete(ConnectionInterface $con = null)
    {
        if (null === $con) {
            $con = Propel::getServiceContainer()->getWriteConnection(AssoImportsArchiveTableMap::DATABASE_NAME);
        }

        $criteria = $this;

        // Set the correct dbName
        $criteria->setDbName(AssoImportsArchiveTableMap::DATABASE_NAME);

        // use transaction because $criteria could contain info
        // for more than one table or we could emulating ON DELETE CASCADE, etc.
        return $con->transaction(function () use ($con, $criteria) {
            $affectedRows = 0; // initialize var to track total num of affected rows

            AssoImportsArchiveTableMap::removeInstanceFromPool($criteria);

            $affectedRows += ModelCriteria::delete($con);
            AssoImportsArchiveTableMap::clearRelatedInstancePool();

            return $affectedRows;
        });
    }

} // AssoImportsArchiveQuery
