<?php

namespace Base;

use \AssoTvatauxsArchive as ChildAssoTvatauxsArchive;
use \AssoTvatauxsArchiveQuery as ChildAssoTvatauxsArchiveQuery;
use \Exception;
use \PDO;
use Map\AssoTvatauxsArchiveTableMap;
use Propel\Runtime\Propel;
use Propel\Runtime\ActiveQuery\Criteria;
use Propel\Runtime\ActiveQuery\ModelCriteria;
use Propel\Runtime\Collection\ObjectCollection;
use Propel\Runtime\Connection\ConnectionInterface;
use Propel\Runtime\Exception\PropelException;

/**
 * Base class that represents a query for the 'asso_tvatauxs_archive' table.
 *
 *
 *
 * @method     ChildAssoTvatauxsArchiveQuery orderByIdTvataux($order = Criteria::ASC) Order by the id_tvataux column
 * @method     ChildAssoTvatauxsArchiveQuery orderByIdTva($order = Criteria::ASC) Order by the id_tva column
 * @method     ChildAssoTvatauxsArchiveQuery orderByTaux($order = Criteria::ASC) Order by the taux column
 * @method     ChildAssoTvatauxsArchiveQuery orderByDateDebut($order = Criteria::ASC) Order by the date_debut column
 * @method     ChildAssoTvatauxsArchiveQuery orderByDateFin($order = Criteria::ASC) Order by the date_fin column
 * @method     ChildAssoTvatauxsArchiveQuery orderByObservation($order = Criteria::ASC) Order by the observation column
 * @method     ChildAssoTvatauxsArchiveQuery orderByCreatedAt($order = Criteria::ASC) Order by the created_at column
 * @method     ChildAssoTvatauxsArchiveQuery orderByUpdatedAt($order = Criteria::ASC) Order by the updated_at column
 * @method     ChildAssoTvatauxsArchiveQuery orderByArchivedAt($order = Criteria::ASC) Order by the archived_at column
 *
 * @method     ChildAssoTvatauxsArchiveQuery groupByIdTvataux() Group by the id_tvataux column
 * @method     ChildAssoTvatauxsArchiveQuery groupByIdTva() Group by the id_tva column
 * @method     ChildAssoTvatauxsArchiveQuery groupByTaux() Group by the taux column
 * @method     ChildAssoTvatauxsArchiveQuery groupByDateDebut() Group by the date_debut column
 * @method     ChildAssoTvatauxsArchiveQuery groupByDateFin() Group by the date_fin column
 * @method     ChildAssoTvatauxsArchiveQuery groupByObservation() Group by the observation column
 * @method     ChildAssoTvatauxsArchiveQuery groupByCreatedAt() Group by the created_at column
 * @method     ChildAssoTvatauxsArchiveQuery groupByUpdatedAt() Group by the updated_at column
 * @method     ChildAssoTvatauxsArchiveQuery groupByArchivedAt() Group by the archived_at column
 *
 * @method     ChildAssoTvatauxsArchiveQuery leftJoin($relation) Adds a LEFT JOIN clause to the query
 * @method     ChildAssoTvatauxsArchiveQuery rightJoin($relation) Adds a RIGHT JOIN clause to the query
 * @method     ChildAssoTvatauxsArchiveQuery innerJoin($relation) Adds a INNER JOIN clause to the query
 *
 * @method     ChildAssoTvatauxsArchiveQuery leftJoinWith($relation) Adds a LEFT JOIN clause and with to the query
 * @method     ChildAssoTvatauxsArchiveQuery rightJoinWith($relation) Adds a RIGHT JOIN clause and with to the query
 * @method     ChildAssoTvatauxsArchiveQuery innerJoinWith($relation) Adds a INNER JOIN clause and with to the query
 *
 * @method     ChildAssoTvatauxsArchive findOne(ConnectionInterface $con = null) Return the first ChildAssoTvatauxsArchive matching the query
 * @method     ChildAssoTvatauxsArchive findOneOrCreate(ConnectionInterface $con = null) Return the first ChildAssoTvatauxsArchive matching the query, or a new ChildAssoTvatauxsArchive object populated from the query conditions when no match is found
 *
 * @method     ChildAssoTvatauxsArchive findOneByIdTvataux(string $id_tvataux) Return the first ChildAssoTvatauxsArchive filtered by the id_tvataux column
 * @method     ChildAssoTvatauxsArchive findOneByIdTva(string $id_tva) Return the first ChildAssoTvatauxsArchive filtered by the id_tva column
 * @method     ChildAssoTvatauxsArchive findOneByTaux(double $taux) Return the first ChildAssoTvatauxsArchive filtered by the taux column
 * @method     ChildAssoTvatauxsArchive findOneByDateDebut(string $date_debut) Return the first ChildAssoTvatauxsArchive filtered by the date_debut column
 * @method     ChildAssoTvatauxsArchive findOneByDateFin(string $date_fin) Return the first ChildAssoTvatauxsArchive filtered by the date_fin column
 * @method     ChildAssoTvatauxsArchive findOneByObservation(string $observation) Return the first ChildAssoTvatauxsArchive filtered by the observation column
 * @method     ChildAssoTvatauxsArchive findOneByCreatedAt(string $created_at) Return the first ChildAssoTvatauxsArchive filtered by the created_at column
 * @method     ChildAssoTvatauxsArchive findOneByUpdatedAt(string $updated_at) Return the first ChildAssoTvatauxsArchive filtered by the updated_at column
 * @method     ChildAssoTvatauxsArchive findOneByArchivedAt(string $archived_at) Return the first ChildAssoTvatauxsArchive filtered by the archived_at column *

 * @method     ChildAssoTvatauxsArchive requirePk($key, ConnectionInterface $con = null) Return the ChildAssoTvatauxsArchive by primary key and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildAssoTvatauxsArchive requireOne(ConnectionInterface $con = null) Return the first ChildAssoTvatauxsArchive matching the query and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 *
 * @method     ChildAssoTvatauxsArchive requireOneByIdTvataux(string $id_tvataux) Return the first ChildAssoTvatauxsArchive filtered by the id_tvataux column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildAssoTvatauxsArchive requireOneByIdTva(string $id_tva) Return the first ChildAssoTvatauxsArchive filtered by the id_tva column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildAssoTvatauxsArchive requireOneByTaux(double $taux) Return the first ChildAssoTvatauxsArchive filtered by the taux column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildAssoTvatauxsArchive requireOneByDateDebut(string $date_debut) Return the first ChildAssoTvatauxsArchive filtered by the date_debut column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildAssoTvatauxsArchive requireOneByDateFin(string $date_fin) Return the first ChildAssoTvatauxsArchive filtered by the date_fin column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildAssoTvatauxsArchive requireOneByObservation(string $observation) Return the first ChildAssoTvatauxsArchive filtered by the observation column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildAssoTvatauxsArchive requireOneByCreatedAt(string $created_at) Return the first ChildAssoTvatauxsArchive filtered by the created_at column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildAssoTvatauxsArchive requireOneByUpdatedAt(string $updated_at) Return the first ChildAssoTvatauxsArchive filtered by the updated_at column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildAssoTvatauxsArchive requireOneByArchivedAt(string $archived_at) Return the first ChildAssoTvatauxsArchive filtered by the archived_at column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 *
 * @method     ChildAssoTvatauxsArchive[]|ObjectCollection find(ConnectionInterface $con = null) Return ChildAssoTvatauxsArchive objects based on current ModelCriteria
 * @method     ChildAssoTvatauxsArchive[]|ObjectCollection findByIdTvataux(string $id_tvataux) Return ChildAssoTvatauxsArchive objects filtered by the id_tvataux column
 * @method     ChildAssoTvatauxsArchive[]|ObjectCollection findByIdTva(string $id_tva) Return ChildAssoTvatauxsArchive objects filtered by the id_tva column
 * @method     ChildAssoTvatauxsArchive[]|ObjectCollection findByTaux(double $taux) Return ChildAssoTvatauxsArchive objects filtered by the taux column
 * @method     ChildAssoTvatauxsArchive[]|ObjectCollection findByDateDebut(string $date_debut) Return ChildAssoTvatauxsArchive objects filtered by the date_debut column
 * @method     ChildAssoTvatauxsArchive[]|ObjectCollection findByDateFin(string $date_fin) Return ChildAssoTvatauxsArchive objects filtered by the date_fin column
 * @method     ChildAssoTvatauxsArchive[]|ObjectCollection findByObservation(string $observation) Return ChildAssoTvatauxsArchive objects filtered by the observation column
 * @method     ChildAssoTvatauxsArchive[]|ObjectCollection findByCreatedAt(string $created_at) Return ChildAssoTvatauxsArchive objects filtered by the created_at column
 * @method     ChildAssoTvatauxsArchive[]|ObjectCollection findByUpdatedAt(string $updated_at) Return ChildAssoTvatauxsArchive objects filtered by the updated_at column
 * @method     ChildAssoTvatauxsArchive[]|ObjectCollection findByArchivedAt(string $archived_at) Return ChildAssoTvatauxsArchive objects filtered by the archived_at column
 * @method     ChildAssoTvatauxsArchive[]|\Propel\Runtime\Util\PropelModelPager paginate($page = 1, $maxPerPage = 10, ConnectionInterface $con = null) Issue a SELECT query based on the current ModelCriteria and uses a page and a maximum number of results per page to compute an offset and a limit
 *
 */
abstract class AssoTvatauxsArchiveQuery extends ModelCriteria
{
    protected $entityNotFoundExceptionClass = '\\Propel\\Runtime\\Exception\\EntityNotFoundException';

    /**
     * Initializes internal state of \Base\AssoTvatauxsArchiveQuery object.
     *
     * @param     string $dbName The database name
     * @param     string $modelName The phpName of a model, e.g. 'Book'
     * @param     string $modelAlias The alias for the model in this query, e.g. 'b'
     */
    public function __construct($dbName = 'simplasso', $modelName = '\\AssoTvatauxsArchive', $modelAlias = null)
    {
        parent::__construct($dbName, $modelName, $modelAlias);
    }

    /**
     * Returns a new ChildAssoTvatauxsArchiveQuery object.
     *
     * @param     string $modelAlias The alias of a model in the query
     * @param     Criteria $criteria Optional Criteria to build the query from
     *
     * @return ChildAssoTvatauxsArchiveQuery
     */
    public static function create($modelAlias = null, Criteria $criteria = null)
    {
        if ($criteria instanceof ChildAssoTvatauxsArchiveQuery) {
            return $criteria;
        }
        $query = new ChildAssoTvatauxsArchiveQuery();
        if (null !== $modelAlias) {
            $query->setModelAlias($modelAlias);
        }
        if ($criteria instanceof Criteria) {
            $query->mergeWith($criteria);
        }

        return $query;
    }

    /**
     * Find object by primary key.
     * Propel uses the instance pool to skip the database if the object exists.
     * Go fast if the query is untouched.
     *
     * <code>
     * $obj  = $c->findPk(12, $con);
     * </code>
     *
     * @param mixed $key Primary key to use for the query
     * @param ConnectionInterface $con an optional connection object
     *
     * @return ChildAssoTvatauxsArchive|array|mixed the result, formatted by the current formatter
     */
    public function findPk($key, ConnectionInterface $con = null)
    {
        if ($key === null) {
            return null;
        }

        if ($con === null) {
            $con = Propel::getServiceContainer()->getReadConnection(AssoTvatauxsArchiveTableMap::DATABASE_NAME);
        }

        $this->basePreSelect($con);

        if (
            $this->formatter || $this->modelAlias || $this->with || $this->select
            || $this->selectColumns || $this->asColumns || $this->selectModifiers
            || $this->map || $this->having || $this->joins
        ) {
            return $this->findPkComplex($key, $con);
        }

        if ((null !== ($obj = AssoTvatauxsArchiveTableMap::getInstanceFromPool(null === $key || is_scalar($key) || is_callable([$key, '__toString']) ? (string) $key : $key)))) {
            // the object is already in the instance pool
            return $obj;
        }

        return $this->findPkSimple($key, $con);
    }

    /**
     * Find object by primary key using raw SQL to go fast.
     * Bypass doSelect() and the object formatter by using generated code.
     *
     * @param     mixed $key Primary key to use for the query
     * @param     ConnectionInterface $con A connection object
     *
     * @throws \Propel\Runtime\Exception\PropelException
     *
     * @return ChildAssoTvatauxsArchive A model object, or null if the key is not found
     */
    protected function findPkSimple($key, ConnectionInterface $con)
    {
        $sql = 'SELECT `id_tvataux`, `id_tva`, `taux`, `date_debut`, `date_fin`, `observation`, `created_at`, `updated_at`, `archived_at` FROM `asso_tvatauxs_archive` WHERE `id_tvataux` = :p0';
        try {
            $stmt = $con->prepare($sql);
            $stmt->bindValue(':p0', $key, PDO::PARAM_INT);
            $stmt->execute();
        } catch (Exception $e) {
            Propel::log($e->getMessage(), Propel::LOG_ERR);
            throw new PropelException(sprintf('Unable to execute SELECT statement [%s]', $sql), 0, $e);
        }
        $obj = null;
        if ($row = $stmt->fetch(\PDO::FETCH_NUM)) {
            /** @var ChildAssoTvatauxsArchive $obj */
            $obj = new ChildAssoTvatauxsArchive();
            $obj->hydrate($row);
            AssoTvatauxsArchiveTableMap::addInstanceToPool($obj, null === $key || is_scalar($key) || is_callable([$key, '__toString']) ? (string) $key : $key);
        }
        $stmt->closeCursor();

        return $obj;
    }

    /**
     * Find object by primary key.
     *
     * @param     mixed $key Primary key to use for the query
     * @param     ConnectionInterface $con A connection object
     *
     * @return ChildAssoTvatauxsArchive|array|mixed the result, formatted by the current formatter
     */
    protected function findPkComplex($key, ConnectionInterface $con)
    {
        // As the query uses a PK condition, no limit(1) is necessary.
        $criteria = $this->isKeepQuery() ? clone $this : $this;
        $dataFetcher = $criteria
            ->filterByPrimaryKey($key)
            ->doSelect($con);

        return $criteria->getFormatter()->init($criteria)->formatOne($dataFetcher);
    }

    /**
     * Find objects by primary key
     * <code>
     * $objs = $c->findPks(array(12, 56, 832), $con);
     * </code>
     * @param     array $keys Primary keys to use for the query
     * @param     ConnectionInterface $con an optional connection object
     *
     * @return ObjectCollection|array|mixed the list of results, formatted by the current formatter
     */
    public function findPks($keys, ConnectionInterface $con = null)
    {
        if (null === $con) {
            $con = Propel::getServiceContainer()->getReadConnection($this->getDbName());
        }
        $this->basePreSelect($con);
        $criteria = $this->isKeepQuery() ? clone $this : $this;
        $dataFetcher = $criteria
            ->filterByPrimaryKeys($keys)
            ->doSelect($con);

        return $criteria->getFormatter()->init($criteria)->format($dataFetcher);
    }

    /**
     * Filter the query by primary key
     *
     * @param     mixed $key Primary key to use for the query
     *
     * @return $this|ChildAssoTvatauxsArchiveQuery The current query, for fluid interface
     */
    public function filterByPrimaryKey($key)
    {

        return $this->addUsingAlias(AssoTvatauxsArchiveTableMap::COL_ID_TVATAUX, $key, Criteria::EQUAL);
    }

    /**
     * Filter the query by a list of primary keys
     *
     * @param     array $keys The list of primary key to use for the query
     *
     * @return $this|ChildAssoTvatauxsArchiveQuery The current query, for fluid interface
     */
    public function filterByPrimaryKeys($keys)
    {

        return $this->addUsingAlias(AssoTvatauxsArchiveTableMap::COL_ID_TVATAUX, $keys, Criteria::IN);
    }

    /**
     * Filter the query on the id_tvataux column
     *
     * Example usage:
     * <code>
     * $query->filterByIdTvataux(1234); // WHERE id_tvataux = 1234
     * $query->filterByIdTvataux(array(12, 34)); // WHERE id_tvataux IN (12, 34)
     * $query->filterByIdTvataux(array('min' => 12)); // WHERE id_tvataux > 12
     * </code>
     *
     * @param     mixed $idTvataux The value to use as filter.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildAssoTvatauxsArchiveQuery The current query, for fluid interface
     */
    public function filterByIdTvataux($idTvataux = null, $comparison = null)
    {
        if (is_array($idTvataux)) {
            $useMinMax = false;
            if (isset($idTvataux['min'])) {
                $this->addUsingAlias(AssoTvatauxsArchiveTableMap::COL_ID_TVATAUX, $idTvataux['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($idTvataux['max'])) {
                $this->addUsingAlias(AssoTvatauxsArchiveTableMap::COL_ID_TVATAUX, $idTvataux['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(AssoTvatauxsArchiveTableMap::COL_ID_TVATAUX, $idTvataux, $comparison);
    }

    /**
     * Filter the query on the id_tva column
     *
     * Example usage:
     * <code>
     * $query->filterByIdTva(1234); // WHERE id_tva = 1234
     * $query->filterByIdTva(array(12, 34)); // WHERE id_tva IN (12, 34)
     * $query->filterByIdTva(array('min' => 12)); // WHERE id_tva > 12
     * </code>
     *
     * @param     mixed $idTva The value to use as filter.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildAssoTvatauxsArchiveQuery The current query, for fluid interface
     */
    public function filterByIdTva($idTva = null, $comparison = null)
    {
        if (is_array($idTva)) {
            $useMinMax = false;
            if (isset($idTva['min'])) {
                $this->addUsingAlias(AssoTvatauxsArchiveTableMap::COL_ID_TVA, $idTva['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($idTva['max'])) {
                $this->addUsingAlias(AssoTvatauxsArchiveTableMap::COL_ID_TVA, $idTva['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(AssoTvatauxsArchiveTableMap::COL_ID_TVA, $idTva, $comparison);
    }

    /**
     * Filter the query on the taux column
     *
     * Example usage:
     * <code>
     * $query->filterByTaux(1234); // WHERE taux = 1234
     * $query->filterByTaux(array(12, 34)); // WHERE taux IN (12, 34)
     * $query->filterByTaux(array('min' => 12)); // WHERE taux > 12
     * </code>
     *
     * @param     mixed $taux The value to use as filter.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildAssoTvatauxsArchiveQuery The current query, for fluid interface
     */
    public function filterByTaux($taux = null, $comparison = null)
    {
        if (is_array($taux)) {
            $useMinMax = false;
            if (isset($taux['min'])) {
                $this->addUsingAlias(AssoTvatauxsArchiveTableMap::COL_TAUX, $taux['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($taux['max'])) {
                $this->addUsingAlias(AssoTvatauxsArchiveTableMap::COL_TAUX, $taux['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(AssoTvatauxsArchiveTableMap::COL_TAUX, $taux, $comparison);
    }

    /**
     * Filter the query on the date_debut column
     *
     * Example usage:
     * <code>
     * $query->filterByDateDebut('2011-03-14'); // WHERE date_debut = '2011-03-14'
     * $query->filterByDateDebut('now'); // WHERE date_debut = '2011-03-14'
     * $query->filterByDateDebut(array('max' => 'yesterday')); // WHERE date_debut > '2011-03-13'
     * </code>
     *
     * @param     mixed $dateDebut The value to use as filter.
     *              Values can be integers (unix timestamps), DateTime objects, or strings.
     *              Empty strings are treated as NULL.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildAssoTvatauxsArchiveQuery The current query, for fluid interface
     */
    public function filterByDateDebut($dateDebut = null, $comparison = null)
    {
        if (is_array($dateDebut)) {
            $useMinMax = false;
            if (isset($dateDebut['min'])) {
                $this->addUsingAlias(AssoTvatauxsArchiveTableMap::COL_DATE_DEBUT, $dateDebut['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($dateDebut['max'])) {
                $this->addUsingAlias(AssoTvatauxsArchiveTableMap::COL_DATE_DEBUT, $dateDebut['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(AssoTvatauxsArchiveTableMap::COL_DATE_DEBUT, $dateDebut, $comparison);
    }

    /**
     * Filter the query on the date_fin column
     *
     * Example usage:
     * <code>
     * $query->filterByDateFin('2011-03-14'); // WHERE date_fin = '2011-03-14'
     * $query->filterByDateFin('now'); // WHERE date_fin = '2011-03-14'
     * $query->filterByDateFin(array('max' => 'yesterday')); // WHERE date_fin > '2011-03-13'
     * </code>
     *
     * @param     mixed $dateFin The value to use as filter.
     *              Values can be integers (unix timestamps), DateTime objects, or strings.
     *              Empty strings are treated as NULL.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildAssoTvatauxsArchiveQuery The current query, for fluid interface
     */
    public function filterByDateFin($dateFin = null, $comparison = null)
    {
        if (is_array($dateFin)) {
            $useMinMax = false;
            if (isset($dateFin['min'])) {
                $this->addUsingAlias(AssoTvatauxsArchiveTableMap::COL_DATE_FIN, $dateFin['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($dateFin['max'])) {
                $this->addUsingAlias(AssoTvatauxsArchiveTableMap::COL_DATE_FIN, $dateFin['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(AssoTvatauxsArchiveTableMap::COL_DATE_FIN, $dateFin, $comparison);
    }

    /**
     * Filter the query on the observation column
     *
     * Example usage:
     * <code>
     * $query->filterByObservation('fooValue');   // WHERE observation = 'fooValue'
     * $query->filterByObservation('%fooValue%', Criteria::LIKE); // WHERE observation LIKE '%fooValue%'
     * </code>
     *
     * @param     string $observation The value to use as filter.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildAssoTvatauxsArchiveQuery The current query, for fluid interface
     */
    public function filterByObservation($observation = null, $comparison = null)
    {
        if (null === $comparison) {
            if (is_array($observation)) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(AssoTvatauxsArchiveTableMap::COL_OBSERVATION, $observation, $comparison);
    }

    /**
     * Filter the query on the created_at column
     *
     * Example usage:
     * <code>
     * $query->filterByCreatedAt('2011-03-14'); // WHERE created_at = '2011-03-14'
     * $query->filterByCreatedAt('now'); // WHERE created_at = '2011-03-14'
     * $query->filterByCreatedAt(array('max' => 'yesterday')); // WHERE created_at > '2011-03-13'
     * </code>
     *
     * @param     mixed $createdAt The value to use as filter.
     *              Values can be integers (unix timestamps), DateTime objects, or strings.
     *              Empty strings are treated as NULL.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildAssoTvatauxsArchiveQuery The current query, for fluid interface
     */
    public function filterByCreatedAt($createdAt = null, $comparison = null)
    {
        if (is_array($createdAt)) {
            $useMinMax = false;
            if (isset($createdAt['min'])) {
                $this->addUsingAlias(AssoTvatauxsArchiveTableMap::COL_CREATED_AT, $createdAt['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($createdAt['max'])) {
                $this->addUsingAlias(AssoTvatauxsArchiveTableMap::COL_CREATED_AT, $createdAt['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(AssoTvatauxsArchiveTableMap::COL_CREATED_AT, $createdAt, $comparison);
    }

    /**
     * Filter the query on the updated_at column
     *
     * Example usage:
     * <code>
     * $query->filterByUpdatedAt('2011-03-14'); // WHERE updated_at = '2011-03-14'
     * $query->filterByUpdatedAt('now'); // WHERE updated_at = '2011-03-14'
     * $query->filterByUpdatedAt(array('max' => 'yesterday')); // WHERE updated_at > '2011-03-13'
     * </code>
     *
     * @param     mixed $updatedAt The value to use as filter.
     *              Values can be integers (unix timestamps), DateTime objects, or strings.
     *              Empty strings are treated as NULL.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildAssoTvatauxsArchiveQuery The current query, for fluid interface
     */
    public function filterByUpdatedAt($updatedAt = null, $comparison = null)
    {
        if (is_array($updatedAt)) {
            $useMinMax = false;
            if (isset($updatedAt['min'])) {
                $this->addUsingAlias(AssoTvatauxsArchiveTableMap::COL_UPDATED_AT, $updatedAt['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($updatedAt['max'])) {
                $this->addUsingAlias(AssoTvatauxsArchiveTableMap::COL_UPDATED_AT, $updatedAt['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(AssoTvatauxsArchiveTableMap::COL_UPDATED_AT, $updatedAt, $comparison);
    }

    /**
     * Filter the query on the archived_at column
     *
     * Example usage:
     * <code>
     * $query->filterByArchivedAt('2011-03-14'); // WHERE archived_at = '2011-03-14'
     * $query->filterByArchivedAt('now'); // WHERE archived_at = '2011-03-14'
     * $query->filterByArchivedAt(array('max' => 'yesterday')); // WHERE archived_at > '2011-03-13'
     * </code>
     *
     * @param     mixed $archivedAt The value to use as filter.
     *              Values can be integers (unix timestamps), DateTime objects, or strings.
     *              Empty strings are treated as NULL.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildAssoTvatauxsArchiveQuery The current query, for fluid interface
     */
    public function filterByArchivedAt($archivedAt = null, $comparison = null)
    {
        if (is_array($archivedAt)) {
            $useMinMax = false;
            if (isset($archivedAt['min'])) {
                $this->addUsingAlias(AssoTvatauxsArchiveTableMap::COL_ARCHIVED_AT, $archivedAt['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($archivedAt['max'])) {
                $this->addUsingAlias(AssoTvatauxsArchiveTableMap::COL_ARCHIVED_AT, $archivedAt['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(AssoTvatauxsArchiveTableMap::COL_ARCHIVED_AT, $archivedAt, $comparison);
    }

    /**
     * Exclude object from result
     *
     * @param   ChildAssoTvatauxsArchive $assoTvatauxsArchive Object to remove from the list of results
     *
     * @return $this|ChildAssoTvatauxsArchiveQuery The current query, for fluid interface
     */
    public function prune($assoTvatauxsArchive = null)
    {
        if ($assoTvatauxsArchive) {
            $this->addUsingAlias(AssoTvatauxsArchiveTableMap::COL_ID_TVATAUX, $assoTvatauxsArchive->getIdTvataux(), Criteria::NOT_EQUAL);
        }

        return $this;
    }

    /**
     * Deletes all rows from the asso_tvatauxs_archive table.
     *
     * @param ConnectionInterface $con the connection to use
     * @return int The number of affected rows (if supported by underlying database driver).
     */
    public function doDeleteAll(ConnectionInterface $con = null)
    {
        if (null === $con) {
            $con = Propel::getServiceContainer()->getWriteConnection(AssoTvatauxsArchiveTableMap::DATABASE_NAME);
        }

        // use transaction because $criteria could contain info
        // for more than one table or we could emulating ON DELETE CASCADE, etc.
        return $con->transaction(function () use ($con) {
            $affectedRows = 0; // initialize var to track total num of affected rows
            $affectedRows += parent::doDeleteAll($con);
            // Because this db requires some delete cascade/set null emulation, we have to
            // clear the cached instance *after* the emulation has happened (since
            // instances get re-added by the select statement contained therein).
            AssoTvatauxsArchiveTableMap::clearInstancePool();
            AssoTvatauxsArchiveTableMap::clearRelatedInstancePool();

            return $affectedRows;
        });
    }

    /**
     * Performs a DELETE on the database based on the current ModelCriteria
     *
     * @param ConnectionInterface $con the connection to use
     * @return int             The number of affected rows (if supported by underlying database driver).  This includes CASCADE-related rows
     *                         if supported by native driver or if emulated using Propel.
     * @throws PropelException Any exceptions caught during processing will be
     *                         rethrown wrapped into a PropelException.
     */
    public function delete(ConnectionInterface $con = null)
    {
        if (null === $con) {
            $con = Propel::getServiceContainer()->getWriteConnection(AssoTvatauxsArchiveTableMap::DATABASE_NAME);
        }

        $criteria = $this;

        // Set the correct dbName
        $criteria->setDbName(AssoTvatauxsArchiveTableMap::DATABASE_NAME);

        // use transaction because $criteria could contain info
        // for more than one table or we could emulating ON DELETE CASCADE, etc.
        return $con->transaction(function () use ($con, $criteria) {
            $affectedRows = 0; // initialize var to track total num of affected rows

            AssoTvatauxsArchiveTableMap::removeInstanceFromPool($criteria);

            $affectedRows += ModelCriteria::delete($con);
            AssoTvatauxsArchiveTableMap::clearRelatedInstancePool();

            return $affectedRows;
        });
    }

} // AssoTvatauxsArchiveQuery
