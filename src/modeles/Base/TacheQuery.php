<?php

namespace Base;

use \Tache as ChildTache;
use \TacheQuery as ChildTacheQuery;
use \Exception;
use \PDO;
use Map\TacheTableMap;
use Propel\Runtime\Propel;
use Propel\Runtime\ActiveQuery\Criteria;
use Propel\Runtime\ActiveQuery\ModelCriteria;
use Propel\Runtime\Collection\ObjectCollection;
use Propel\Runtime\Connection\ConnectionInterface;
use Propel\Runtime\Exception\PropelException;

/**
 * Base class that represents a query for the 'asso_taches' table.
 *
 *
 *
 * @method     ChildTacheQuery orderByIdTache($order = Criteria::ASC) Order by the id_tache column
 * @method     ChildTacheQuery orderByDescriptif($order = Criteria::ASC) Order by the descriptif column
 * @method     ChildTacheQuery orderByFonction($order = Criteria::ASC) Order by the fonction column
 * @method     ChildTacheQuery orderByArgs($order = Criteria::ASC) Order by the args column
 * @method     ChildTacheQuery orderByStatut($order = Criteria::ASC) Order by the statut column
 * @method     ChildTacheQuery orderByPriorite($order = Criteria::ASC) Order by the priorite column
 * @method     ChildTacheQuery orderByDateExecution($order = Criteria::ASC) Order by the date_execution column
 * @method     ChildTacheQuery orderByCreatedAt($order = Criteria::ASC) Order by the created_at column
 * @method     ChildTacheQuery orderByUpdatedAt($order = Criteria::ASC) Order by the updated_at column
 *
 * @method     ChildTacheQuery groupByIdTache() Group by the id_tache column
 * @method     ChildTacheQuery groupByDescriptif() Group by the descriptif column
 * @method     ChildTacheQuery groupByFonction() Group by the fonction column
 * @method     ChildTacheQuery groupByArgs() Group by the args column
 * @method     ChildTacheQuery groupByStatut() Group by the statut column
 * @method     ChildTacheQuery groupByPriorite() Group by the priorite column
 * @method     ChildTacheQuery groupByDateExecution() Group by the date_execution column
 * @method     ChildTacheQuery groupByCreatedAt() Group by the created_at column
 * @method     ChildTacheQuery groupByUpdatedAt() Group by the updated_at column
 *
 * @method     ChildTacheQuery leftJoin($relation) Adds a LEFT JOIN clause to the query
 * @method     ChildTacheQuery rightJoin($relation) Adds a RIGHT JOIN clause to the query
 * @method     ChildTacheQuery innerJoin($relation) Adds a INNER JOIN clause to the query
 *
 * @method     ChildTacheQuery leftJoinWith($relation) Adds a LEFT JOIN clause and with to the query
 * @method     ChildTacheQuery rightJoinWith($relation) Adds a RIGHT JOIN clause and with to the query
 * @method     ChildTacheQuery innerJoinWith($relation) Adds a INNER JOIN clause and with to the query
 *
 * @method     ChildTache findOne(ConnectionInterface $con = null) Return the first ChildTache matching the query
 * @method     ChildTache findOneOrCreate(ConnectionInterface $con = null) Return the first ChildTache matching the query, or a new ChildTache object populated from the query conditions when no match is found
 *
 * @method     ChildTache findOneByIdTache(string $id_tache) Return the first ChildTache filtered by the id_tache column
 * @method     ChildTache findOneByDescriptif(string $descriptif) Return the first ChildTache filtered by the descriptif column
 * @method     ChildTache findOneByFonction(string $fonction) Return the first ChildTache filtered by the fonction column
 * @method     ChildTache findOneByArgs(string $args) Return the first ChildTache filtered by the args column
 * @method     ChildTache findOneByStatut(int $statut) Return the first ChildTache filtered by the statut column
 * @method     ChildTache findOneByPriorite(int $priorite) Return the first ChildTache filtered by the priorite column
 * @method     ChildTache findOneByDateExecution(string $date_execution) Return the first ChildTache filtered by the date_execution column
 * @method     ChildTache findOneByCreatedAt(string $created_at) Return the first ChildTache filtered by the created_at column
 * @method     ChildTache findOneByUpdatedAt(string $updated_at) Return the first ChildTache filtered by the updated_at column *

 * @method     ChildTache requirePk($key, ConnectionInterface $con = null) Return the ChildTache by primary key and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildTache requireOne(ConnectionInterface $con = null) Return the first ChildTache matching the query and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 *
 * @method     ChildTache requireOneByIdTache(string $id_tache) Return the first ChildTache filtered by the id_tache column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildTache requireOneByDescriptif(string $descriptif) Return the first ChildTache filtered by the descriptif column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildTache requireOneByFonction(string $fonction) Return the first ChildTache filtered by the fonction column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildTache requireOneByArgs(string $args) Return the first ChildTache filtered by the args column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildTache requireOneByStatut(int $statut) Return the first ChildTache filtered by the statut column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildTache requireOneByPriorite(int $priorite) Return the first ChildTache filtered by the priorite column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildTache requireOneByDateExecution(string $date_execution) Return the first ChildTache filtered by the date_execution column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildTache requireOneByCreatedAt(string $created_at) Return the first ChildTache filtered by the created_at column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildTache requireOneByUpdatedAt(string $updated_at) Return the first ChildTache filtered by the updated_at column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 *
 * @method     ChildTache[]|ObjectCollection find(ConnectionInterface $con = null) Return ChildTache objects based on current ModelCriteria
 * @method     ChildTache[]|ObjectCollection findByIdTache(string $id_tache) Return ChildTache objects filtered by the id_tache column
 * @method     ChildTache[]|ObjectCollection findByDescriptif(string $descriptif) Return ChildTache objects filtered by the descriptif column
 * @method     ChildTache[]|ObjectCollection findByFonction(string $fonction) Return ChildTache objects filtered by the fonction column
 * @method     ChildTache[]|ObjectCollection findByArgs(string $args) Return ChildTache objects filtered by the args column
 * @method     ChildTache[]|ObjectCollection findByStatut(int $statut) Return ChildTache objects filtered by the statut column
 * @method     ChildTache[]|ObjectCollection findByPriorite(int $priorite) Return ChildTache objects filtered by the priorite column
 * @method     ChildTache[]|ObjectCollection findByDateExecution(string $date_execution) Return ChildTache objects filtered by the date_execution column
 * @method     ChildTache[]|ObjectCollection findByCreatedAt(string $created_at) Return ChildTache objects filtered by the created_at column
 * @method     ChildTache[]|ObjectCollection findByUpdatedAt(string $updated_at) Return ChildTache objects filtered by the updated_at column
 * @method     ChildTache[]|\Propel\Runtime\Util\PropelModelPager paginate($page = 1, $maxPerPage = 10, ConnectionInterface $con = null) Issue a SELECT query based on the current ModelCriteria and uses a page and a maximum number of results per page to compute an offset and a limit
 *
 */
abstract class TacheQuery extends ModelCriteria
{
    protected $entityNotFoundExceptionClass = '\\Propel\\Runtime\\Exception\\EntityNotFoundException';

    /**
     * Initializes internal state of \Base\TacheQuery object.
     *
     * @param     string $dbName The database name
     * @param     string $modelName The phpName of a model, e.g. 'Book'
     * @param     string $modelAlias The alias for the model in this query, e.g. 'b'
     */
    public function __construct($dbName = 'simplasso', $modelName = '\\Tache', $modelAlias = null)
    {
        parent::__construct($dbName, $modelName, $modelAlias);
    }

    /**
     * Returns a new ChildTacheQuery object.
     *
     * @param     string $modelAlias The alias of a model in the query
     * @param     Criteria $criteria Optional Criteria to build the query from
     *
     * @return ChildTacheQuery
     */
    public static function create($modelAlias = null, Criteria $criteria = null)
    {
        if ($criteria instanceof ChildTacheQuery) {
            return $criteria;
        }
        $query = new ChildTacheQuery();
        if (null !== $modelAlias) {
            $query->setModelAlias($modelAlias);
        }
        if ($criteria instanceof Criteria) {
            $query->mergeWith($criteria);
        }

        return $query;
    }

    /**
     * Find object by primary key.
     * Propel uses the instance pool to skip the database if the object exists.
     * Go fast if the query is untouched.
     *
     * <code>
     * $obj  = $c->findPk(12, $con);
     * </code>
     *
     * @param mixed $key Primary key to use for the query
     * @param ConnectionInterface $con an optional connection object
     *
     * @return ChildTache|array|mixed the result, formatted by the current formatter
     */
    public function findPk($key, ConnectionInterface $con = null)
    {
        if ($key === null) {
            return null;
        }

        if ($con === null) {
            $con = Propel::getServiceContainer()->getReadConnection(TacheTableMap::DATABASE_NAME);
        }

        $this->basePreSelect($con);

        if (
            $this->formatter || $this->modelAlias || $this->with || $this->select
            || $this->selectColumns || $this->asColumns || $this->selectModifiers
            || $this->map || $this->having || $this->joins
        ) {
            return $this->findPkComplex($key, $con);
        }

        if ((null !== ($obj = TacheTableMap::getInstanceFromPool(null === $key || is_scalar($key) || is_callable([$key, '__toString']) ? (string) $key : $key)))) {
            // the object is already in the instance pool
            return $obj;
        }

        return $this->findPkSimple($key, $con);
    }

    /**
     * Find object by primary key using raw SQL to go fast.
     * Bypass doSelect() and the object formatter by using generated code.
     *
     * @param     mixed $key Primary key to use for the query
     * @param     ConnectionInterface $con A connection object
     *
     * @throws \Propel\Runtime\Exception\PropelException
     *
     * @return ChildTache A model object, or null if the key is not found
     */
    protected function findPkSimple($key, ConnectionInterface $con)
    {
        $sql = 'SELECT `id_tache`, `descriptif`, `fonction`, `args`, `statut`, `priorite`, `date_execution`, `created_at`, `updated_at` FROM `asso_taches` WHERE `id_tache` = :p0';
        try {
            $stmt = $con->prepare($sql);
            $stmt->bindValue(':p0', $key, PDO::PARAM_INT);
            $stmt->execute();
        } catch (Exception $e) {
            Propel::log($e->getMessage(), Propel::LOG_ERR);
            throw new PropelException(sprintf('Unable to execute SELECT statement [%s]', $sql), 0, $e);
        }
        $obj = null;
        if ($row = $stmt->fetch(\PDO::FETCH_NUM)) {
            /** @var ChildTache $obj */
            $obj = new ChildTache();
            $obj->hydrate($row);
            TacheTableMap::addInstanceToPool($obj, null === $key || is_scalar($key) || is_callable([$key, '__toString']) ? (string) $key : $key);
        }
        $stmt->closeCursor();

        return $obj;
    }

    /**
     * Find object by primary key.
     *
     * @param     mixed $key Primary key to use for the query
     * @param     ConnectionInterface $con A connection object
     *
     * @return ChildTache|array|mixed the result, formatted by the current formatter
     */
    protected function findPkComplex($key, ConnectionInterface $con)
    {
        // As the query uses a PK condition, no limit(1) is necessary.
        $criteria = $this->isKeepQuery() ? clone $this : $this;
        $dataFetcher = $criteria
            ->filterByPrimaryKey($key)
            ->doSelect($con);

        return $criteria->getFormatter()->init($criteria)->formatOne($dataFetcher);
    }

    /**
     * Find objects by primary key
     * <code>
     * $objs = $c->findPks(array(12, 56, 832), $con);
     * </code>
     * @param     array $keys Primary keys to use for the query
     * @param     ConnectionInterface $con an optional connection object
     *
     * @return ObjectCollection|array|mixed the list of results, formatted by the current formatter
     */
    public function findPks($keys, ConnectionInterface $con = null)
    {
        if (null === $con) {
            $con = Propel::getServiceContainer()->getReadConnection($this->getDbName());
        }
        $this->basePreSelect($con);
        $criteria = $this->isKeepQuery() ? clone $this : $this;
        $dataFetcher = $criteria
            ->filterByPrimaryKeys($keys)
            ->doSelect($con);

        return $criteria->getFormatter()->init($criteria)->format($dataFetcher);
    }

    /**
     * Filter the query by primary key
     *
     * @param     mixed $key Primary key to use for the query
     *
     * @return $this|ChildTacheQuery The current query, for fluid interface
     */
    public function filterByPrimaryKey($key)
    {

        return $this->addUsingAlias(TacheTableMap::COL_ID_TACHE, $key, Criteria::EQUAL);
    }

    /**
     * Filter the query by a list of primary keys
     *
     * @param     array $keys The list of primary key to use for the query
     *
     * @return $this|ChildTacheQuery The current query, for fluid interface
     */
    public function filterByPrimaryKeys($keys)
    {

        return $this->addUsingAlias(TacheTableMap::COL_ID_TACHE, $keys, Criteria::IN);
    }

    /**
     * Filter the query on the id_tache column
     *
     * Example usage:
     * <code>
     * $query->filterByIdTache(1234); // WHERE id_tache = 1234
     * $query->filterByIdTache(array(12, 34)); // WHERE id_tache IN (12, 34)
     * $query->filterByIdTache(array('min' => 12)); // WHERE id_tache > 12
     * </code>
     *
     * @param     mixed $idTache The value to use as filter.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildTacheQuery The current query, for fluid interface
     */
    public function filterByIdTache($idTache = null, $comparison = null)
    {
        if (is_array($idTache)) {
            $useMinMax = false;
            if (isset($idTache['min'])) {
                $this->addUsingAlias(TacheTableMap::COL_ID_TACHE, $idTache['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($idTache['max'])) {
                $this->addUsingAlias(TacheTableMap::COL_ID_TACHE, $idTache['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(TacheTableMap::COL_ID_TACHE, $idTache, $comparison);
    }

    /**
     * Filter the query on the descriptif column
     *
     * Example usage:
     * <code>
     * $query->filterByDescriptif('fooValue');   // WHERE descriptif = 'fooValue'
     * $query->filterByDescriptif('%fooValue%', Criteria::LIKE); // WHERE descriptif LIKE '%fooValue%'
     * </code>
     *
     * @param     string $descriptif The value to use as filter.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildTacheQuery The current query, for fluid interface
     */
    public function filterByDescriptif($descriptif = null, $comparison = null)
    {
        if (null === $comparison) {
            if (is_array($descriptif)) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(TacheTableMap::COL_DESCRIPTIF, $descriptif, $comparison);
    }

    /**
     * Filter the query on the fonction column
     *
     * Example usage:
     * <code>
     * $query->filterByFonction('fooValue');   // WHERE fonction = 'fooValue'
     * $query->filterByFonction('%fooValue%', Criteria::LIKE); // WHERE fonction LIKE '%fooValue%'
     * </code>
     *
     * @param     string $fonction The value to use as filter.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildTacheQuery The current query, for fluid interface
     */
    public function filterByFonction($fonction = null, $comparison = null)
    {
        if (null === $comparison) {
            if (is_array($fonction)) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(TacheTableMap::COL_FONCTION, $fonction, $comparison);
    }

    /**
     * Filter the query on the args column
     *
     * Example usage:
     * <code>
     * $query->filterByArgs('fooValue');   // WHERE args = 'fooValue'
     * $query->filterByArgs('%fooValue%', Criteria::LIKE); // WHERE args LIKE '%fooValue%'
     * </code>
     *
     * @param     string $args The value to use as filter.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildTacheQuery The current query, for fluid interface
     */
    public function filterByArgs($args = null, $comparison = null)
    {
        if (null === $comparison) {
            if (is_array($args)) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(TacheTableMap::COL_ARGS, $args, $comparison);
    }

    /**
     * Filter the query on the statut column
     *
     * Example usage:
     * <code>
     * $query->filterByStatut(1234); // WHERE statut = 1234
     * $query->filterByStatut(array(12, 34)); // WHERE statut IN (12, 34)
     * $query->filterByStatut(array('min' => 12)); // WHERE statut > 12
     * </code>
     *
     * @param     mixed $statut The value to use as filter.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildTacheQuery The current query, for fluid interface
     */
    public function filterByStatut($statut = null, $comparison = null)
    {
        if (is_array($statut)) {
            $useMinMax = false;
            if (isset($statut['min'])) {
                $this->addUsingAlias(TacheTableMap::COL_STATUT, $statut['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($statut['max'])) {
                $this->addUsingAlias(TacheTableMap::COL_STATUT, $statut['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(TacheTableMap::COL_STATUT, $statut, $comparison);
    }

    /**
     * Filter the query on the priorite column
     *
     * Example usage:
     * <code>
     * $query->filterByPriorite(1234); // WHERE priorite = 1234
     * $query->filterByPriorite(array(12, 34)); // WHERE priorite IN (12, 34)
     * $query->filterByPriorite(array('min' => 12)); // WHERE priorite > 12
     * </code>
     *
     * @param     mixed $priorite The value to use as filter.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildTacheQuery The current query, for fluid interface
     */
    public function filterByPriorite($priorite = null, $comparison = null)
    {
        if (is_array($priorite)) {
            $useMinMax = false;
            if (isset($priorite['min'])) {
                $this->addUsingAlias(TacheTableMap::COL_PRIORITE, $priorite['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($priorite['max'])) {
                $this->addUsingAlias(TacheTableMap::COL_PRIORITE, $priorite['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(TacheTableMap::COL_PRIORITE, $priorite, $comparison);
    }

    /**
     * Filter the query on the date_execution column
     *
     * Example usage:
     * <code>
     * $query->filterByDateExecution('2011-03-14'); // WHERE date_execution = '2011-03-14'
     * $query->filterByDateExecution('now'); // WHERE date_execution = '2011-03-14'
     * $query->filterByDateExecution(array('max' => 'yesterday')); // WHERE date_execution > '2011-03-13'
     * </code>
     *
     * @param     mixed $dateExecution The value to use as filter.
     *              Values can be integers (unix timestamps), DateTime objects, or strings.
     *              Empty strings are treated as NULL.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildTacheQuery The current query, for fluid interface
     */
    public function filterByDateExecution($dateExecution = null, $comparison = null)
    {
        if (is_array($dateExecution)) {
            $useMinMax = false;
            if (isset($dateExecution['min'])) {
                $this->addUsingAlias(TacheTableMap::COL_DATE_EXECUTION, $dateExecution['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($dateExecution['max'])) {
                $this->addUsingAlias(TacheTableMap::COL_DATE_EXECUTION, $dateExecution['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(TacheTableMap::COL_DATE_EXECUTION, $dateExecution, $comparison);
    }

    /**
     * Filter the query on the created_at column
     *
     * Example usage:
     * <code>
     * $query->filterByCreatedAt('2011-03-14'); // WHERE created_at = '2011-03-14'
     * $query->filterByCreatedAt('now'); // WHERE created_at = '2011-03-14'
     * $query->filterByCreatedAt(array('max' => 'yesterday')); // WHERE created_at > '2011-03-13'
     * </code>
     *
     * @param     mixed $createdAt The value to use as filter.
     *              Values can be integers (unix timestamps), DateTime objects, or strings.
     *              Empty strings are treated as NULL.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildTacheQuery The current query, for fluid interface
     */
    public function filterByCreatedAt($createdAt = null, $comparison = null)
    {
        if (is_array($createdAt)) {
            $useMinMax = false;
            if (isset($createdAt['min'])) {
                $this->addUsingAlias(TacheTableMap::COL_CREATED_AT, $createdAt['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($createdAt['max'])) {
                $this->addUsingAlias(TacheTableMap::COL_CREATED_AT, $createdAt['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(TacheTableMap::COL_CREATED_AT, $createdAt, $comparison);
    }

    /**
     * Filter the query on the updated_at column
     *
     * Example usage:
     * <code>
     * $query->filterByUpdatedAt('2011-03-14'); // WHERE updated_at = '2011-03-14'
     * $query->filterByUpdatedAt('now'); // WHERE updated_at = '2011-03-14'
     * $query->filterByUpdatedAt(array('max' => 'yesterday')); // WHERE updated_at > '2011-03-13'
     * </code>
     *
     * @param     mixed $updatedAt The value to use as filter.
     *              Values can be integers (unix timestamps), DateTime objects, or strings.
     *              Empty strings are treated as NULL.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildTacheQuery The current query, for fluid interface
     */
    public function filterByUpdatedAt($updatedAt = null, $comparison = null)
    {
        if (is_array($updatedAt)) {
            $useMinMax = false;
            if (isset($updatedAt['min'])) {
                $this->addUsingAlias(TacheTableMap::COL_UPDATED_AT, $updatedAt['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($updatedAt['max'])) {
                $this->addUsingAlias(TacheTableMap::COL_UPDATED_AT, $updatedAt['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(TacheTableMap::COL_UPDATED_AT, $updatedAt, $comparison);
    }

    /**
     * Exclude object from result
     *
     * @param   ChildTache $tache Object to remove from the list of results
     *
     * @return $this|ChildTacheQuery The current query, for fluid interface
     */
    public function prune($tache = null)
    {
        if ($tache) {
            $this->addUsingAlias(TacheTableMap::COL_ID_TACHE, $tache->getIdTache(), Criteria::NOT_EQUAL);
        }

        return $this;
    }

    /**
     * Deletes all rows from the asso_taches table.
     *
     * @param ConnectionInterface $con the connection to use
     * @return int The number of affected rows (if supported by underlying database driver).
     */
    public function doDeleteAll(ConnectionInterface $con = null)
    {
        if (null === $con) {
            $con = Propel::getServiceContainer()->getWriteConnection(TacheTableMap::DATABASE_NAME);
        }

        // use transaction because $criteria could contain info
        // for more than one table or we could emulating ON DELETE CASCADE, etc.
        return $con->transaction(function () use ($con) {
            $affectedRows = 0; // initialize var to track total num of affected rows
            $affectedRows += parent::doDeleteAll($con);
            // Because this db requires some delete cascade/set null emulation, we have to
            // clear the cached instance *after* the emulation has happened (since
            // instances get re-added by the select statement contained therein).
            TacheTableMap::clearInstancePool();
            TacheTableMap::clearRelatedInstancePool();

            return $affectedRows;
        });
    }

    /**
     * Performs a DELETE on the database based on the current ModelCriteria
     *
     * @param ConnectionInterface $con the connection to use
     * @return int             The number of affected rows (if supported by underlying database driver).  This includes CASCADE-related rows
     *                         if supported by native driver or if emulated using Propel.
     * @throws PropelException Any exceptions caught during processing will be
     *                         rethrown wrapped into a PropelException.
     */
    public function delete(ConnectionInterface $con = null)
    {
        if (null === $con) {
            $con = Propel::getServiceContainer()->getWriteConnection(TacheTableMap::DATABASE_NAME);
        }

        $criteria = $this;

        // Set the correct dbName
        $criteria->setDbName(TacheTableMap::DATABASE_NAME);

        // use transaction because $criteria could contain info
        // for more than one table or we could emulating ON DELETE CASCADE, etc.
        return $con->transaction(function () use ($con, $criteria) {
            $affectedRows = 0; // initialize var to track total num of affected rows

            TacheTableMap::removeInstanceFromPool($criteria);

            $affectedRows += ModelCriteria::delete($con);
            TacheTableMap::clearRelatedInstancePool();

            return $affectedRows;
        });
    }

    // timestampable behavior

    /**
     * Filter by the latest updated
     *
     * @param      int $nbDays Maximum age of the latest update in days
     *
     * @return     $this|ChildTacheQuery The current query, for fluid interface
     */
    public function recentlyUpdated($nbDays = 7)
    {
        return $this->addUsingAlias(TacheTableMap::COL_UPDATED_AT, time() - $nbDays * 24 * 60 * 60, Criteria::GREATER_EQUAL);
    }

    /**
     * Order by update date desc
     *
     * @return     $this|ChildTacheQuery The current query, for fluid interface
     */
    public function lastUpdatedFirst()
    {
        return $this->addDescendingOrderByColumn(TacheTableMap::COL_UPDATED_AT);
    }

    /**
     * Order by update date asc
     *
     * @return     $this|ChildTacheQuery The current query, for fluid interface
     */
    public function firstUpdatedFirst()
    {
        return $this->addAscendingOrderByColumn(TacheTableMap::COL_UPDATED_AT);
    }

    /**
     * Order by create date desc
     *
     * @return     $this|ChildTacheQuery The current query, for fluid interface
     */
    public function lastCreatedFirst()
    {
        return $this->addDescendingOrderByColumn(TacheTableMap::COL_CREATED_AT);
    }

    /**
     * Filter by the latest created
     *
     * @param      int $nbDays Maximum age of in days
     *
     * @return     $this|ChildTacheQuery The current query, for fluid interface
     */
    public function recentlyCreated($nbDays = 7)
    {
        return $this->addUsingAlias(TacheTableMap::COL_CREATED_AT, time() - $nbDays * 24 * 60 * 60, Criteria::GREATER_EQUAL);
    }

    /**
     * Order by create date asc
     *
     * @return     $this|ChildTacheQuery The current query, for fluid interface
     */
    public function firstCreatedFirst()
    {
        return $this->addAscendingOrderByColumn(TacheTableMap::COL_CREATED_AT);
    }

} // TacheQuery
