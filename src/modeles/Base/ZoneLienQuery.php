<?php

namespace Base;

use \ZoneLien as ChildZoneLien;
use \ZoneLienQuery as ChildZoneLienQuery;
use \Exception;
use \PDO;
use Map\ZoneLienTableMap;
use Propel\Runtime\Propel;
use Propel\Runtime\ActiveQuery\Criteria;
use Propel\Runtime\ActiveQuery\ModelCriteria;
use Propel\Runtime\ActiveQuery\ModelJoin;
use Propel\Runtime\Collection\ObjectCollection;
use Propel\Runtime\Connection\ConnectionInterface;
use Propel\Runtime\Exception\PropelException;

/**
 * Base class that represents a query for the 'geo_zones_liens' table.
 *
 *
 *
 * @method     ChildZoneLienQuery orderByIdZone($order = Criteria::ASC) Order by the id_zone column
 * @method     ChildZoneLienQuery orderByObjet($order = Criteria::ASC) Order by the objet column
 * @method     ChildZoneLienQuery orderByIdObjet($order = Criteria::ASC) Order by the id_objet column
 *
 * @method     ChildZoneLienQuery groupByIdZone() Group by the id_zone column
 * @method     ChildZoneLienQuery groupByObjet() Group by the objet column
 * @method     ChildZoneLienQuery groupByIdObjet() Group by the id_objet column
 *
 * @method     ChildZoneLienQuery leftJoin($relation) Adds a LEFT JOIN clause to the query
 * @method     ChildZoneLienQuery rightJoin($relation) Adds a RIGHT JOIN clause to the query
 * @method     ChildZoneLienQuery innerJoin($relation) Adds a INNER JOIN clause to the query
 *
 * @method     ChildZoneLienQuery leftJoinWith($relation) Adds a LEFT JOIN clause and with to the query
 * @method     ChildZoneLienQuery rightJoinWith($relation) Adds a RIGHT JOIN clause and with to the query
 * @method     ChildZoneLienQuery innerJoinWith($relation) Adds a INNER JOIN clause and with to the query
 *
 * @method     ChildZoneLienQuery leftJoinZone($relationAlias = null) Adds a LEFT JOIN clause to the query using the Zone relation
 * @method     ChildZoneLienQuery rightJoinZone($relationAlias = null) Adds a RIGHT JOIN clause to the query using the Zone relation
 * @method     ChildZoneLienQuery innerJoinZone($relationAlias = null) Adds a INNER JOIN clause to the query using the Zone relation
 *
 * @method     ChildZoneLienQuery joinWithZone($joinType = Criteria::INNER_JOIN) Adds a join clause and with to the query using the Zone relation
 *
 * @method     ChildZoneLienQuery leftJoinWithZone() Adds a LEFT JOIN clause and with to the query using the Zone relation
 * @method     ChildZoneLienQuery rightJoinWithZone() Adds a RIGHT JOIN clause and with to the query using the Zone relation
 * @method     ChildZoneLienQuery innerJoinWithZone() Adds a INNER JOIN clause and with to the query using the Zone relation
 *
 * @method     \ZoneQuery endUse() Finalizes a secondary criteria and merges it with its primary Criteria
 *
 * @method     ChildZoneLien findOne(ConnectionInterface $con = null) Return the first ChildZoneLien matching the query
 * @method     ChildZoneLien findOneOrCreate(ConnectionInterface $con = null) Return the first ChildZoneLien matching the query, or a new ChildZoneLien object populated from the query conditions when no match is found
 *
 * @method     ChildZoneLien findOneByIdZone(string $id_zone) Return the first ChildZoneLien filtered by the id_zone column
 * @method     ChildZoneLien findOneByObjet(string $objet) Return the first ChildZoneLien filtered by the objet column
 * @method     ChildZoneLien findOneByIdObjet(string $id_objet) Return the first ChildZoneLien filtered by the id_objet column *

 * @method     ChildZoneLien requirePk($key, ConnectionInterface $con = null) Return the ChildZoneLien by primary key and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildZoneLien requireOne(ConnectionInterface $con = null) Return the first ChildZoneLien matching the query and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 *
 * @method     ChildZoneLien requireOneByIdZone(string $id_zone) Return the first ChildZoneLien filtered by the id_zone column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildZoneLien requireOneByObjet(string $objet) Return the first ChildZoneLien filtered by the objet column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildZoneLien requireOneByIdObjet(string $id_objet) Return the first ChildZoneLien filtered by the id_objet column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 *
 * @method     ChildZoneLien[]|ObjectCollection find(ConnectionInterface $con = null) Return ChildZoneLien objects based on current ModelCriteria
 * @method     ChildZoneLien[]|ObjectCollection findByIdZone(string $id_zone) Return ChildZoneLien objects filtered by the id_zone column
 * @method     ChildZoneLien[]|ObjectCollection findByObjet(string $objet) Return ChildZoneLien objects filtered by the objet column
 * @method     ChildZoneLien[]|ObjectCollection findByIdObjet(string $id_objet) Return ChildZoneLien objects filtered by the id_objet column
 * @method     ChildZoneLien[]|\Propel\Runtime\Util\PropelModelPager paginate($page = 1, $maxPerPage = 10, ConnectionInterface $con = null) Issue a SELECT query based on the current ModelCriteria and uses a page and a maximum number of results per page to compute an offset and a limit
 *
 */
abstract class ZoneLienQuery extends ModelCriteria
{
    protected $entityNotFoundExceptionClass = '\\Propel\\Runtime\\Exception\\EntityNotFoundException';

    /**
     * Initializes internal state of \Base\ZoneLienQuery object.
     *
     * @param     string $dbName The database name
     * @param     string $modelName The phpName of a model, e.g. 'Book'
     * @param     string $modelAlias The alias for the model in this query, e.g. 'b'
     */
    public function __construct($dbName = 'simplasso', $modelName = '\\ZoneLien', $modelAlias = null)
    {
        parent::__construct($dbName, $modelName, $modelAlias);
    }

    /**
     * Returns a new ChildZoneLienQuery object.
     *
     * @param     string $modelAlias The alias of a model in the query
     * @param     Criteria $criteria Optional Criteria to build the query from
     *
     * @return ChildZoneLienQuery
     */
    public static function create($modelAlias = null, Criteria $criteria = null)
    {
        if ($criteria instanceof ChildZoneLienQuery) {
            return $criteria;
        }
        $query = new ChildZoneLienQuery();
        if (null !== $modelAlias) {
            $query->setModelAlias($modelAlias);
        }
        if ($criteria instanceof Criteria) {
            $query->mergeWith($criteria);
        }

        return $query;
    }

    /**
     * Find object by primary key.
     * Propel uses the instance pool to skip the database if the object exists.
     * Go fast if the query is untouched.
     *
     * <code>
     * $obj = $c->findPk(array(12, 34, 56), $con);
     * </code>
     *
     * @param array[$id_zone, $objet, $id_objet] $key Primary key to use for the query
     * @param ConnectionInterface $con an optional connection object
     *
     * @return ChildZoneLien|array|mixed the result, formatted by the current formatter
     */
    public function findPk($key, ConnectionInterface $con = null)
    {
        if ($key === null) {
            return null;
        }

        if ($con === null) {
            $con = Propel::getServiceContainer()->getReadConnection(ZoneLienTableMap::DATABASE_NAME);
        }

        $this->basePreSelect($con);

        if (
            $this->formatter || $this->modelAlias || $this->with || $this->select
            || $this->selectColumns || $this->asColumns || $this->selectModifiers
            || $this->map || $this->having || $this->joins
        ) {
            return $this->findPkComplex($key, $con);
        }

        if ((null !== ($obj = ZoneLienTableMap::getInstanceFromPool(serialize([(null === $key[0] || is_scalar($key[0]) || is_callable([$key[0], '__toString']) ? (string) $key[0] : $key[0]), (null === $key[1] || is_scalar($key[1]) || is_callable([$key[1], '__toString']) ? (string) $key[1] : $key[1]), (null === $key[2] || is_scalar($key[2]) || is_callable([$key[2], '__toString']) ? (string) $key[2] : $key[2])]))))) {
            // the object is already in the instance pool
            return $obj;
        }

        return $this->findPkSimple($key, $con);
    }

    /**
     * Find object by primary key using raw SQL to go fast.
     * Bypass doSelect() and the object formatter by using generated code.
     *
     * @param     mixed $key Primary key to use for the query
     * @param     ConnectionInterface $con A connection object
     *
     * @throws \Propel\Runtime\Exception\PropelException
     *
     * @return ChildZoneLien A model object, or null if the key is not found
     */
    protected function findPkSimple($key, ConnectionInterface $con)
    {
        $sql = 'SELECT `id_zone`, `objet`, `id_objet` FROM `geo_zones_liens` WHERE `id_zone` = :p0 AND `objet` = :p1 AND `id_objet` = :p2';
        try {
            $stmt = $con->prepare($sql);
            $stmt->bindValue(':p0', $key[0], PDO::PARAM_INT);
            $stmt->bindValue(':p1', $key[1], PDO::PARAM_STR);
            $stmt->bindValue(':p2', $key[2], PDO::PARAM_INT);
            $stmt->execute();
        } catch (Exception $e) {
            Propel::log($e->getMessage(), Propel::LOG_ERR);
            throw new PropelException(sprintf('Unable to execute SELECT statement [%s]', $sql), 0, $e);
        }
        $obj = null;
        if ($row = $stmt->fetch(\PDO::FETCH_NUM)) {
            /** @var ChildZoneLien $obj */
            $obj = new ChildZoneLien();
            $obj->hydrate($row);
            ZoneLienTableMap::addInstanceToPool($obj, serialize([(null === $key[0] || is_scalar($key[0]) || is_callable([$key[0], '__toString']) ? (string) $key[0] : $key[0]), (null === $key[1] || is_scalar($key[1]) || is_callable([$key[1], '__toString']) ? (string) $key[1] : $key[1]), (null === $key[2] || is_scalar($key[2]) || is_callable([$key[2], '__toString']) ? (string) $key[2] : $key[2])]));
        }
        $stmt->closeCursor();

        return $obj;
    }

    /**
     * Find object by primary key.
     *
     * @param     mixed $key Primary key to use for the query
     * @param     ConnectionInterface $con A connection object
     *
     * @return ChildZoneLien|array|mixed the result, formatted by the current formatter
     */
    protected function findPkComplex($key, ConnectionInterface $con)
    {
        // As the query uses a PK condition, no limit(1) is necessary.
        $criteria = $this->isKeepQuery() ? clone $this : $this;
        $dataFetcher = $criteria
            ->filterByPrimaryKey($key)
            ->doSelect($con);

        return $criteria->getFormatter()->init($criteria)->formatOne($dataFetcher);
    }

    /**
     * Find objects by primary key
     * <code>
     * $objs = $c->findPks(array(array(12, 56), array(832, 123), array(123, 456)), $con);
     * </code>
     * @param     array $keys Primary keys to use for the query
     * @param     ConnectionInterface $con an optional connection object
     *
     * @return ObjectCollection|array|mixed the list of results, formatted by the current formatter
     */
    public function findPks($keys, ConnectionInterface $con = null)
    {
        if (null === $con) {
            $con = Propel::getServiceContainer()->getReadConnection($this->getDbName());
        }
        $this->basePreSelect($con);
        $criteria = $this->isKeepQuery() ? clone $this : $this;
        $dataFetcher = $criteria
            ->filterByPrimaryKeys($keys)
            ->doSelect($con);

        return $criteria->getFormatter()->init($criteria)->format($dataFetcher);
    }

    /**
     * Filter the query by primary key
     *
     * @param     mixed $key Primary key to use for the query
     *
     * @return $this|ChildZoneLienQuery The current query, for fluid interface
     */
    public function filterByPrimaryKey($key)
    {
        $this->addUsingAlias(ZoneLienTableMap::COL_ID_ZONE, $key[0], Criteria::EQUAL);
        $this->addUsingAlias(ZoneLienTableMap::COL_OBJET, $key[1], Criteria::EQUAL);
        $this->addUsingAlias(ZoneLienTableMap::COL_ID_OBJET, $key[2], Criteria::EQUAL);

        return $this;
    }

    /**
     * Filter the query by a list of primary keys
     *
     * @param     array $keys The list of primary key to use for the query
     *
     * @return $this|ChildZoneLienQuery The current query, for fluid interface
     */
    public function filterByPrimaryKeys($keys)
    {
        if (empty($keys)) {
            return $this->add(null, '1<>1', Criteria::CUSTOM);
        }
        foreach ($keys as $key) {
            $cton0 = $this->getNewCriterion(ZoneLienTableMap::COL_ID_ZONE, $key[0], Criteria::EQUAL);
            $cton1 = $this->getNewCriterion(ZoneLienTableMap::COL_OBJET, $key[1], Criteria::EQUAL);
            $cton0->addAnd($cton1);
            $cton2 = $this->getNewCriterion(ZoneLienTableMap::COL_ID_OBJET, $key[2], Criteria::EQUAL);
            $cton0->addAnd($cton2);
            $this->addOr($cton0);
        }

        return $this;
    }

    /**
     * Filter the query on the id_zone column
     *
     * Example usage:
     * <code>
     * $query->filterByIdZone(1234); // WHERE id_zone = 1234
     * $query->filterByIdZone(array(12, 34)); // WHERE id_zone IN (12, 34)
     * $query->filterByIdZone(array('min' => 12)); // WHERE id_zone > 12
     * </code>
     *
     * @see       filterByZone()
     *
     * @param     mixed $idZone The value to use as filter.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildZoneLienQuery The current query, for fluid interface
     */
    public function filterByIdZone($idZone = null, $comparison = null)
    {
        if (is_array($idZone)) {
            $useMinMax = false;
            if (isset($idZone['min'])) {
                $this->addUsingAlias(ZoneLienTableMap::COL_ID_ZONE, $idZone['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($idZone['max'])) {
                $this->addUsingAlias(ZoneLienTableMap::COL_ID_ZONE, $idZone['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(ZoneLienTableMap::COL_ID_ZONE, $idZone, $comparison);
    }

    /**
     * Filter the query on the objet column
     *
     * Example usage:
     * <code>
     * $query->filterByObjet('fooValue');   // WHERE objet = 'fooValue'
     * $query->filterByObjet('%fooValue%', Criteria::LIKE); // WHERE objet LIKE '%fooValue%'
     * </code>
     *
     * @param     string $objet The value to use as filter.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildZoneLienQuery The current query, for fluid interface
     */
    public function filterByObjet($objet = null, $comparison = null)
    {
        if (null === $comparison) {
            if (is_array($objet)) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(ZoneLienTableMap::COL_OBJET, $objet, $comparison);
    }

    /**
     * Filter the query on the id_objet column
     *
     * Example usage:
     * <code>
     * $query->filterByIdObjet(1234); // WHERE id_objet = 1234
     * $query->filterByIdObjet(array(12, 34)); // WHERE id_objet IN (12, 34)
     * $query->filterByIdObjet(array('min' => 12)); // WHERE id_objet > 12
     * </code>
     *
     * @param     mixed $idObjet The value to use as filter.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildZoneLienQuery The current query, for fluid interface
     */
    public function filterByIdObjet($idObjet = null, $comparison = null)
    {
        if (is_array($idObjet)) {
            $useMinMax = false;
            if (isset($idObjet['min'])) {
                $this->addUsingAlias(ZoneLienTableMap::COL_ID_OBJET, $idObjet['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($idObjet['max'])) {
                $this->addUsingAlias(ZoneLienTableMap::COL_ID_OBJET, $idObjet['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(ZoneLienTableMap::COL_ID_OBJET, $idObjet, $comparison);
    }

    /**
     * Filter the query by a related \Zone object
     *
     * @param \Zone|ObjectCollection $zone The related object(s) to use as filter
     * @param string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @throws \Propel\Runtime\Exception\PropelException
     *
     * @return ChildZoneLienQuery The current query, for fluid interface
     */
    public function filterByZone($zone, $comparison = null)
    {
        if ($zone instanceof \Zone) {
            return $this
                ->addUsingAlias(ZoneLienTableMap::COL_ID_ZONE, $zone->getIdZone(), $comparison);
        } elseif ($zone instanceof ObjectCollection) {
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }

            return $this
                ->addUsingAlias(ZoneLienTableMap::COL_ID_ZONE, $zone->toKeyValue('PrimaryKey', 'IdZone'), $comparison);
        } else {
            throw new PropelException('filterByZone() only accepts arguments of type \Zone or Collection');
        }
    }

    /**
     * Adds a JOIN clause to the query using the Zone relation
     *
     * @param     string $relationAlias optional alias for the relation
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return $this|ChildZoneLienQuery The current query, for fluid interface
     */
    public function joinZone($relationAlias = null, $joinType = Criteria::INNER_JOIN)
    {
        $tableMap = $this->getTableMap();
        $relationMap = $tableMap->getRelation('Zone');

        // create a ModelJoin object for this join
        $join = new ModelJoin();
        $join->setJoinType($joinType);
        $join->setRelationMap($relationMap, $this->useAliasInSQL ? $this->getModelAlias() : null, $relationAlias);
        if ($previousJoin = $this->getPreviousJoin()) {
            $join->setPreviousJoin($previousJoin);
        }

        // add the ModelJoin to the current object
        if ($relationAlias) {
            $this->addAlias($relationAlias, $relationMap->getRightTable()->getName());
            $this->addJoinObject($join, $relationAlias);
        } else {
            $this->addJoinObject($join, 'Zone');
        }

        return $this;
    }

    /**
     * Use the Zone relation Zone object
     *
     * @see useQuery()
     *
     * @param     string $relationAlias optional alias for the relation,
     *                                   to be used as main alias in the secondary query
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return \ZoneQuery A secondary query class using the current class as primary query
     */
    public function useZoneQuery($relationAlias = null, $joinType = Criteria::INNER_JOIN)
    {
        return $this
            ->joinZone($relationAlias, $joinType)
            ->useQuery($relationAlias ? $relationAlias : 'Zone', '\ZoneQuery');
    }

    /**
     * Exclude object from result
     *
     * @param   ChildZoneLien $zoneLien Object to remove from the list of results
     *
     * @return $this|ChildZoneLienQuery The current query, for fluid interface
     */
    public function prune($zoneLien = null)
    {
        if ($zoneLien) {
            $this->addCond('pruneCond0', $this->getAliasedColName(ZoneLienTableMap::COL_ID_ZONE), $zoneLien->getIdZone(), Criteria::NOT_EQUAL);
            $this->addCond('pruneCond1', $this->getAliasedColName(ZoneLienTableMap::COL_OBJET), $zoneLien->getObjet(), Criteria::NOT_EQUAL);
            $this->addCond('pruneCond2', $this->getAliasedColName(ZoneLienTableMap::COL_ID_OBJET), $zoneLien->getIdObjet(), Criteria::NOT_EQUAL);
            $this->combine(array('pruneCond0', 'pruneCond1', 'pruneCond2'), Criteria::LOGICAL_OR);
        }

        return $this;
    }

    /**
     * Deletes all rows from the geo_zones_liens table.
     *
     * @param ConnectionInterface $con the connection to use
     * @return int The number of affected rows (if supported by underlying database driver).
     */
    public function doDeleteAll(ConnectionInterface $con = null)
    {
        if (null === $con) {
            $con = Propel::getServiceContainer()->getWriteConnection(ZoneLienTableMap::DATABASE_NAME);
        }

        // use transaction because $criteria could contain info
        // for more than one table or we could emulating ON DELETE CASCADE, etc.
        return $con->transaction(function () use ($con) {
            $affectedRows = 0; // initialize var to track total num of affected rows
            $affectedRows += parent::doDeleteAll($con);
            // Because this db requires some delete cascade/set null emulation, we have to
            // clear the cached instance *after* the emulation has happened (since
            // instances get re-added by the select statement contained therein).
            ZoneLienTableMap::clearInstancePool();
            ZoneLienTableMap::clearRelatedInstancePool();

            return $affectedRows;
        });
    }

    /**
     * Performs a DELETE on the database based on the current ModelCriteria
     *
     * @param ConnectionInterface $con the connection to use
     * @return int             The number of affected rows (if supported by underlying database driver).  This includes CASCADE-related rows
     *                         if supported by native driver or if emulated using Propel.
     * @throws PropelException Any exceptions caught during processing will be
     *                         rethrown wrapped into a PropelException.
     */
    public function delete(ConnectionInterface $con = null)
    {
        if (null === $con) {
            $con = Propel::getServiceContainer()->getWriteConnection(ZoneLienTableMap::DATABASE_NAME);
        }

        $criteria = $this;

        // Set the correct dbName
        $criteria->setDbName(ZoneLienTableMap::DATABASE_NAME);

        // use transaction because $criteria could contain info
        // for more than one table or we could emulating ON DELETE CASCADE, etc.
        return $con->transaction(function () use ($con, $criteria) {
            $affectedRows = 0; // initialize var to track total num of affected rows

            ZoneLienTableMap::removeInstanceFromPool($criteria);

            $affectedRows += ModelCriteria::delete($con);
            ZoneLienTableMap::clearRelatedInstancePool();

            return $affectedRows;
        });
    }

} // ZoneLienQuery
