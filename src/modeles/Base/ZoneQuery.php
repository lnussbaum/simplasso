<?php

namespace Base;

use \Zone as ChildZone;
use \ZoneQuery as ChildZoneQuery;
use \Exception;
use \PDO;
use Map\ZoneTableMap;
use Propel\Runtime\Propel;
use Propel\Runtime\ActiveQuery\Criteria;
use Propel\Runtime\ActiveQuery\ModelCriteria;
use Propel\Runtime\ActiveQuery\ModelJoin;
use Propel\Runtime\Collection\ObjectCollection;
use Propel\Runtime\Connection\ConnectionInterface;
use Propel\Runtime\Exception\PropelException;

/**
 * Base class that represents a query for the 'geo_zones' table.
 *
 *
 *
 * @method     ChildZoneQuery orderByIdZone($order = Criteria::ASC) Order by the id_zone column
 * @method     ChildZoneQuery orderByIdZonegroupe($order = Criteria::ASC) Order by the id_zonegroupe column
 * @method     ChildZoneQuery orderByNom($order = Criteria::ASC) Order by the nom column
 * @method     ChildZoneQuery orderByDescriptif($order = Criteria::ASC) Order by the descriptif column
 * @method     ChildZoneQuery orderByZoneGeo($order = Criteria::ASC) Order by the zone_geo column
 * @method     ChildZoneQuery orderByCreatedAt($order = Criteria::ASC) Order by the created_at column
 * @method     ChildZoneQuery orderByUpdatedAt($order = Criteria::ASC) Order by the updated_at column
 *
 * @method     ChildZoneQuery groupByIdZone() Group by the id_zone column
 * @method     ChildZoneQuery groupByIdZonegroupe() Group by the id_zonegroupe column
 * @method     ChildZoneQuery groupByNom() Group by the nom column
 * @method     ChildZoneQuery groupByDescriptif() Group by the descriptif column
 * @method     ChildZoneQuery groupByZoneGeo() Group by the zone_geo column
 * @method     ChildZoneQuery groupByCreatedAt() Group by the created_at column
 * @method     ChildZoneQuery groupByUpdatedAt() Group by the updated_at column
 *
 * @method     ChildZoneQuery leftJoin($relation) Adds a LEFT JOIN clause to the query
 * @method     ChildZoneQuery rightJoin($relation) Adds a RIGHT JOIN clause to the query
 * @method     ChildZoneQuery innerJoin($relation) Adds a INNER JOIN clause to the query
 *
 * @method     ChildZoneQuery leftJoinWith($relation) Adds a LEFT JOIN clause and with to the query
 * @method     ChildZoneQuery rightJoinWith($relation) Adds a RIGHT JOIN clause and with to the query
 * @method     ChildZoneQuery innerJoinWith($relation) Adds a INNER JOIN clause and with to the query
 *
 * @method     ChildZoneQuery leftJoinZonegroupe($relationAlias = null) Adds a LEFT JOIN clause to the query using the Zonegroupe relation
 * @method     ChildZoneQuery rightJoinZonegroupe($relationAlias = null) Adds a RIGHT JOIN clause to the query using the Zonegroupe relation
 * @method     ChildZoneQuery innerJoinZonegroupe($relationAlias = null) Adds a INNER JOIN clause to the query using the Zonegroupe relation
 *
 * @method     ChildZoneQuery joinWithZonegroupe($joinType = Criteria::INNER_JOIN) Adds a join clause and with to the query using the Zonegroupe relation
 *
 * @method     ChildZoneQuery leftJoinWithZonegroupe() Adds a LEFT JOIN clause and with to the query using the Zonegroupe relation
 * @method     ChildZoneQuery rightJoinWithZonegroupe() Adds a RIGHT JOIN clause and with to the query using the Zonegroupe relation
 * @method     ChildZoneQuery innerJoinWithZonegroupe() Adds a INNER JOIN clause and with to the query using the Zonegroupe relation
 *
 * @method     ChildZoneQuery leftJoinZoneLien($relationAlias = null) Adds a LEFT JOIN clause to the query using the ZoneLien relation
 * @method     ChildZoneQuery rightJoinZoneLien($relationAlias = null) Adds a RIGHT JOIN clause to the query using the ZoneLien relation
 * @method     ChildZoneQuery innerJoinZoneLien($relationAlias = null) Adds a INNER JOIN clause to the query using the ZoneLien relation
 *
 * @method     ChildZoneQuery joinWithZoneLien($joinType = Criteria::INNER_JOIN) Adds a join clause and with to the query using the ZoneLien relation
 *
 * @method     ChildZoneQuery leftJoinWithZoneLien() Adds a LEFT JOIN clause and with to the query using the ZoneLien relation
 * @method     ChildZoneQuery rightJoinWithZoneLien() Adds a RIGHT JOIN clause and with to the query using the ZoneLien relation
 * @method     ChildZoneQuery innerJoinWithZoneLien() Adds a INNER JOIN clause and with to the query using the ZoneLien relation
 *
 * @method     \ZonegroupeQuery|\ZoneLienQuery endUse() Finalizes a secondary criteria and merges it with its primary Criteria
 *
 * @method     ChildZone findOne(ConnectionInterface $con = null) Return the first ChildZone matching the query
 * @method     ChildZone findOneOrCreate(ConnectionInterface $con = null) Return the first ChildZone matching the query, or a new ChildZone object populated from the query conditions when no match is found
 *
 * @method     ChildZone findOneByIdZone(string $id_zone) Return the first ChildZone filtered by the id_zone column
 * @method     ChildZone findOneByIdZonegroupe(int $id_zonegroupe) Return the first ChildZone filtered by the id_zonegroupe column
 * @method     ChildZone findOneByNom(string $nom) Return the first ChildZone filtered by the nom column
 * @method     ChildZone findOneByDescriptif(string $descriptif) Return the first ChildZone filtered by the descriptif column
 * @method     ChildZone findOneByZoneGeo(string $zone_geo) Return the first ChildZone filtered by the zone_geo column
 * @method     ChildZone findOneByCreatedAt(string $created_at) Return the first ChildZone filtered by the created_at column
 * @method     ChildZone findOneByUpdatedAt(string $updated_at) Return the first ChildZone filtered by the updated_at column *

 * @method     ChildZone requirePk($key, ConnectionInterface $con = null) Return the ChildZone by primary key and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildZone requireOne(ConnectionInterface $con = null) Return the first ChildZone matching the query and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 *
 * @method     ChildZone requireOneByIdZone(string $id_zone) Return the first ChildZone filtered by the id_zone column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildZone requireOneByIdZonegroupe(int $id_zonegroupe) Return the first ChildZone filtered by the id_zonegroupe column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildZone requireOneByNom(string $nom) Return the first ChildZone filtered by the nom column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildZone requireOneByDescriptif(string $descriptif) Return the first ChildZone filtered by the descriptif column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildZone requireOneByZoneGeo(string $zone_geo) Return the first ChildZone filtered by the zone_geo column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildZone requireOneByCreatedAt(string $created_at) Return the first ChildZone filtered by the created_at column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildZone requireOneByUpdatedAt(string $updated_at) Return the first ChildZone filtered by the updated_at column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 *
 * @method     ChildZone[]|ObjectCollection find(ConnectionInterface $con = null) Return ChildZone objects based on current ModelCriteria
 * @method     ChildZone[]|ObjectCollection findByIdZone(string $id_zone) Return ChildZone objects filtered by the id_zone column
 * @method     ChildZone[]|ObjectCollection findByIdZonegroupe(int $id_zonegroupe) Return ChildZone objects filtered by the id_zonegroupe column
 * @method     ChildZone[]|ObjectCollection findByNom(string $nom) Return ChildZone objects filtered by the nom column
 * @method     ChildZone[]|ObjectCollection findByDescriptif(string $descriptif) Return ChildZone objects filtered by the descriptif column
 * @method     ChildZone[]|ObjectCollection findByZoneGeo(string $zone_geo) Return ChildZone objects filtered by the zone_geo column
 * @method     ChildZone[]|ObjectCollection findByCreatedAt(string $created_at) Return ChildZone objects filtered by the created_at column
 * @method     ChildZone[]|ObjectCollection findByUpdatedAt(string $updated_at) Return ChildZone objects filtered by the updated_at column
 * @method     ChildZone[]|\Propel\Runtime\Util\PropelModelPager paginate($page = 1, $maxPerPage = 10, ConnectionInterface $con = null) Issue a SELECT query based on the current ModelCriteria and uses a page and a maximum number of results per page to compute an offset and a limit
 *
 */
abstract class ZoneQuery extends ModelCriteria
{
    protected $entityNotFoundExceptionClass = '\\Propel\\Runtime\\Exception\\EntityNotFoundException';

    /**
     * Initializes internal state of \Base\ZoneQuery object.
     *
     * @param     string $dbName The database name
     * @param     string $modelName The phpName of a model, e.g. 'Book'
     * @param     string $modelAlias The alias for the model in this query, e.g. 'b'
     */
    public function __construct($dbName = 'simplasso', $modelName = '\\Zone', $modelAlias = null)
    {
        parent::__construct($dbName, $modelName, $modelAlias);
    }

    /**
     * Returns a new ChildZoneQuery object.
     *
     * @param     string $modelAlias The alias of a model in the query
     * @param     Criteria $criteria Optional Criteria to build the query from
     *
     * @return ChildZoneQuery
     */
    public static function create($modelAlias = null, Criteria $criteria = null)
    {
        if ($criteria instanceof ChildZoneQuery) {
            return $criteria;
        }
        $query = new ChildZoneQuery();
        if (null !== $modelAlias) {
            $query->setModelAlias($modelAlias);
        }
        if ($criteria instanceof Criteria) {
            $query->mergeWith($criteria);
        }

        return $query;
    }

    /**
     * Find object by primary key.
     * Propel uses the instance pool to skip the database if the object exists.
     * Go fast if the query is untouched.
     *
     * <code>
     * $obj  = $c->findPk(12, $con);
     * </code>
     *
     * @param mixed $key Primary key to use for the query
     * @param ConnectionInterface $con an optional connection object
     *
     * @return ChildZone|array|mixed the result, formatted by the current formatter
     */
    public function findPk($key, ConnectionInterface $con = null)
    {
        if ($key === null) {
            return null;
        }

        if ($con === null) {
            $con = Propel::getServiceContainer()->getReadConnection(ZoneTableMap::DATABASE_NAME);
        }

        $this->basePreSelect($con);

        if (
            $this->formatter || $this->modelAlias || $this->with || $this->select
            || $this->selectColumns || $this->asColumns || $this->selectModifiers
            || $this->map || $this->having || $this->joins
        ) {
            return $this->findPkComplex($key, $con);
        }

        if ((null !== ($obj = ZoneTableMap::getInstanceFromPool(null === $key || is_scalar($key) || is_callable([$key, '__toString']) ? (string) $key : $key)))) {
            // the object is already in the instance pool
            return $obj;
        }

        return $this->findPkSimple($key, $con);
    }

    /**
     * Find object by primary key using raw SQL to go fast.
     * Bypass doSelect() and the object formatter by using generated code.
     *
     * @param     mixed $key Primary key to use for the query
     * @param     ConnectionInterface $con A connection object
     *
     * @throws \Propel\Runtime\Exception\PropelException
     *
     * @return ChildZone A model object, or null if the key is not found
     */
    protected function findPkSimple($key, ConnectionInterface $con)
    {
        $sql = 'SELECT `id_zone`, `id_zonegroupe`, `nom`, `descriptif`, `zone_geo`, `created_at`, `updated_at` FROM `geo_zones` WHERE `id_zone` = :p0';
        try {
            $stmt = $con->prepare($sql);
            $stmt->bindValue(':p0', $key, PDO::PARAM_INT);
            $stmt->execute();
        } catch (Exception $e) {
            Propel::log($e->getMessage(), Propel::LOG_ERR);
            throw new PropelException(sprintf('Unable to execute SELECT statement [%s]', $sql), 0, $e);
        }
        $obj = null;
        if ($row = $stmt->fetch(\PDO::FETCH_NUM)) {
            /** @var ChildZone $obj */
            $obj = new ChildZone();
            $obj->hydrate($row);
            ZoneTableMap::addInstanceToPool($obj, null === $key || is_scalar($key) || is_callable([$key, '__toString']) ? (string) $key : $key);
        }
        $stmt->closeCursor();

        return $obj;
    }

    /**
     * Find object by primary key.
     *
     * @param     mixed $key Primary key to use for the query
     * @param     ConnectionInterface $con A connection object
     *
     * @return ChildZone|array|mixed the result, formatted by the current formatter
     */
    protected function findPkComplex($key, ConnectionInterface $con)
    {
        // As the query uses a PK condition, no limit(1) is necessary.
        $criteria = $this->isKeepQuery() ? clone $this : $this;
        $dataFetcher = $criteria
            ->filterByPrimaryKey($key)
            ->doSelect($con);

        return $criteria->getFormatter()->init($criteria)->formatOne($dataFetcher);
    }

    /**
     * Find objects by primary key
     * <code>
     * $objs = $c->findPks(array(12, 56, 832), $con);
     * </code>
     * @param     array $keys Primary keys to use for the query
     * @param     ConnectionInterface $con an optional connection object
     *
     * @return ObjectCollection|array|mixed the list of results, formatted by the current formatter
     */
    public function findPks($keys, ConnectionInterface $con = null)
    {
        if (null === $con) {
            $con = Propel::getServiceContainer()->getReadConnection($this->getDbName());
        }
        $this->basePreSelect($con);
        $criteria = $this->isKeepQuery() ? clone $this : $this;
        $dataFetcher = $criteria
            ->filterByPrimaryKeys($keys)
            ->doSelect($con);

        return $criteria->getFormatter()->init($criteria)->format($dataFetcher);
    }

    /**
     * Filter the query by primary key
     *
     * @param     mixed $key Primary key to use for the query
     *
     * @return $this|ChildZoneQuery The current query, for fluid interface
     */
    public function filterByPrimaryKey($key)
    {

        return $this->addUsingAlias(ZoneTableMap::COL_ID_ZONE, $key, Criteria::EQUAL);
    }

    /**
     * Filter the query by a list of primary keys
     *
     * @param     array $keys The list of primary key to use for the query
     *
     * @return $this|ChildZoneQuery The current query, for fluid interface
     */
    public function filterByPrimaryKeys($keys)
    {

        return $this->addUsingAlias(ZoneTableMap::COL_ID_ZONE, $keys, Criteria::IN);
    }

    /**
     * Filter the query on the id_zone column
     *
     * Example usage:
     * <code>
     * $query->filterByIdZone(1234); // WHERE id_zone = 1234
     * $query->filterByIdZone(array(12, 34)); // WHERE id_zone IN (12, 34)
     * $query->filterByIdZone(array('min' => 12)); // WHERE id_zone > 12
     * </code>
     *
     * @param     mixed $idZone The value to use as filter.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildZoneQuery The current query, for fluid interface
     */
    public function filterByIdZone($idZone = null, $comparison = null)
    {
        if (is_array($idZone)) {
            $useMinMax = false;
            if (isset($idZone['min'])) {
                $this->addUsingAlias(ZoneTableMap::COL_ID_ZONE, $idZone['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($idZone['max'])) {
                $this->addUsingAlias(ZoneTableMap::COL_ID_ZONE, $idZone['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(ZoneTableMap::COL_ID_ZONE, $idZone, $comparison);
    }

    /**
     * Filter the query on the id_zonegroupe column
     *
     * Example usage:
     * <code>
     * $query->filterByIdZonegroupe(1234); // WHERE id_zonegroupe = 1234
     * $query->filterByIdZonegroupe(array(12, 34)); // WHERE id_zonegroupe IN (12, 34)
     * $query->filterByIdZonegroupe(array('min' => 12)); // WHERE id_zonegroupe > 12
     * </code>
     *
     * @see       filterByZonegroupe()
     *
     * @param     mixed $idZonegroupe The value to use as filter.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildZoneQuery The current query, for fluid interface
     */
    public function filterByIdZonegroupe($idZonegroupe = null, $comparison = null)
    {
        if (is_array($idZonegroupe)) {
            $useMinMax = false;
            if (isset($idZonegroupe['min'])) {
                $this->addUsingAlias(ZoneTableMap::COL_ID_ZONEGROUPE, $idZonegroupe['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($idZonegroupe['max'])) {
                $this->addUsingAlias(ZoneTableMap::COL_ID_ZONEGROUPE, $idZonegroupe['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(ZoneTableMap::COL_ID_ZONEGROUPE, $idZonegroupe, $comparison);
    }

    /**
     * Filter the query on the nom column
     *
     * Example usage:
     * <code>
     * $query->filterByNom('fooValue');   // WHERE nom = 'fooValue'
     * $query->filterByNom('%fooValue%', Criteria::LIKE); // WHERE nom LIKE '%fooValue%'
     * </code>
     *
     * @param     string $nom The value to use as filter.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildZoneQuery The current query, for fluid interface
     */
    public function filterByNom($nom = null, $comparison = null)
    {
        if (null === $comparison) {
            if (is_array($nom)) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(ZoneTableMap::COL_NOM, $nom, $comparison);
    }

    /**
     * Filter the query on the descriptif column
     *
     * Example usage:
     * <code>
     * $query->filterByDescriptif('fooValue');   // WHERE descriptif = 'fooValue'
     * $query->filterByDescriptif('%fooValue%', Criteria::LIKE); // WHERE descriptif LIKE '%fooValue%'
     * </code>
     *
     * @param     string $descriptif The value to use as filter.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildZoneQuery The current query, for fluid interface
     */
    public function filterByDescriptif($descriptif = null, $comparison = null)
    {
        if (null === $comparison) {
            if (is_array($descriptif)) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(ZoneTableMap::COL_DESCRIPTIF, $descriptif, $comparison);
    }

    /**
     * Filter the query on the zone_geo column
     *
     * Example usage:
     * <code>
     * $query->filterByZoneGeo('fooValue');   // WHERE zone_geo = 'fooValue'
     * $query->filterByZoneGeo('%fooValue%', Criteria::LIKE); // WHERE zone_geo LIKE '%fooValue%'
     * </code>
     *
     * @param     string $zoneGeo The value to use as filter.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildZoneQuery The current query, for fluid interface
     */
    public function filterByZoneGeo($zoneGeo = null, $comparison = null)
    {
        if (null === $comparison) {
            if (is_array($zoneGeo)) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(ZoneTableMap::COL_ZONE_GEO, $zoneGeo, $comparison);
    }

    /**
     * Filter the query on the created_at column
     *
     * Example usage:
     * <code>
     * $query->filterByCreatedAt('2011-03-14'); // WHERE created_at = '2011-03-14'
     * $query->filterByCreatedAt('now'); // WHERE created_at = '2011-03-14'
     * $query->filterByCreatedAt(array('max' => 'yesterday')); // WHERE created_at > '2011-03-13'
     * </code>
     *
     * @param     mixed $createdAt The value to use as filter.
     *              Values can be integers (unix timestamps), DateTime objects, or strings.
     *              Empty strings are treated as NULL.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildZoneQuery The current query, for fluid interface
     */
    public function filterByCreatedAt($createdAt = null, $comparison = null)
    {
        if (is_array($createdAt)) {
            $useMinMax = false;
            if (isset($createdAt['min'])) {
                $this->addUsingAlias(ZoneTableMap::COL_CREATED_AT, $createdAt['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($createdAt['max'])) {
                $this->addUsingAlias(ZoneTableMap::COL_CREATED_AT, $createdAt['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(ZoneTableMap::COL_CREATED_AT, $createdAt, $comparison);
    }

    /**
     * Filter the query on the updated_at column
     *
     * Example usage:
     * <code>
     * $query->filterByUpdatedAt('2011-03-14'); // WHERE updated_at = '2011-03-14'
     * $query->filterByUpdatedAt('now'); // WHERE updated_at = '2011-03-14'
     * $query->filterByUpdatedAt(array('max' => 'yesterday')); // WHERE updated_at > '2011-03-13'
     * </code>
     *
     * @param     mixed $updatedAt The value to use as filter.
     *              Values can be integers (unix timestamps), DateTime objects, or strings.
     *              Empty strings are treated as NULL.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildZoneQuery The current query, for fluid interface
     */
    public function filterByUpdatedAt($updatedAt = null, $comparison = null)
    {
        if (is_array($updatedAt)) {
            $useMinMax = false;
            if (isset($updatedAt['min'])) {
                $this->addUsingAlias(ZoneTableMap::COL_UPDATED_AT, $updatedAt['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($updatedAt['max'])) {
                $this->addUsingAlias(ZoneTableMap::COL_UPDATED_AT, $updatedAt['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(ZoneTableMap::COL_UPDATED_AT, $updatedAt, $comparison);
    }

    /**
     * Filter the query by a related \Zonegroupe object
     *
     * @param \Zonegroupe|ObjectCollection $zonegroupe The related object(s) to use as filter
     * @param string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @throws \Propel\Runtime\Exception\PropelException
     *
     * @return ChildZoneQuery The current query, for fluid interface
     */
    public function filterByZonegroupe($zonegroupe, $comparison = null)
    {
        if ($zonegroupe instanceof \Zonegroupe) {
            return $this
                ->addUsingAlias(ZoneTableMap::COL_ID_ZONEGROUPE, $zonegroupe->getIdZonegroupe(), $comparison);
        } elseif ($zonegroupe instanceof ObjectCollection) {
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }

            return $this
                ->addUsingAlias(ZoneTableMap::COL_ID_ZONEGROUPE, $zonegroupe->toKeyValue('PrimaryKey', 'IdZonegroupe'), $comparison);
        } else {
            throw new PropelException('filterByZonegroupe() only accepts arguments of type \Zonegroupe or Collection');
        }
    }

    /**
     * Adds a JOIN clause to the query using the Zonegroupe relation
     *
     * @param     string $relationAlias optional alias for the relation
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return $this|ChildZoneQuery The current query, for fluid interface
     */
    public function joinZonegroupe($relationAlias = null, $joinType = Criteria::INNER_JOIN)
    {
        $tableMap = $this->getTableMap();
        $relationMap = $tableMap->getRelation('Zonegroupe');

        // create a ModelJoin object for this join
        $join = new ModelJoin();
        $join->setJoinType($joinType);
        $join->setRelationMap($relationMap, $this->useAliasInSQL ? $this->getModelAlias() : null, $relationAlias);
        if ($previousJoin = $this->getPreviousJoin()) {
            $join->setPreviousJoin($previousJoin);
        }

        // add the ModelJoin to the current object
        if ($relationAlias) {
            $this->addAlias($relationAlias, $relationMap->getRightTable()->getName());
            $this->addJoinObject($join, $relationAlias);
        } else {
            $this->addJoinObject($join, 'Zonegroupe');
        }

        return $this;
    }

    /**
     * Use the Zonegroupe relation Zonegroupe object
     *
     * @see useQuery()
     *
     * @param     string $relationAlias optional alias for the relation,
     *                                   to be used as main alias in the secondary query
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return \ZonegroupeQuery A secondary query class using the current class as primary query
     */
    public function useZonegroupeQuery($relationAlias = null, $joinType = Criteria::INNER_JOIN)
    {
        return $this
            ->joinZonegroupe($relationAlias, $joinType)
            ->useQuery($relationAlias ? $relationAlias : 'Zonegroupe', '\ZonegroupeQuery');
    }

    /**
     * Filter the query by a related \ZoneLien object
     *
     * @param \ZoneLien|ObjectCollection $zoneLien the related object to use as filter
     * @param string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return ChildZoneQuery The current query, for fluid interface
     */
    public function filterByZoneLien($zoneLien, $comparison = null)
    {
        if ($zoneLien instanceof \ZoneLien) {
            return $this
                ->addUsingAlias(ZoneTableMap::COL_ID_ZONE, $zoneLien->getIdZone(), $comparison);
        } elseif ($zoneLien instanceof ObjectCollection) {
            return $this
                ->useZoneLienQuery()
                ->filterByPrimaryKeys($zoneLien->getPrimaryKeys())
                ->endUse();
        } else {
            throw new PropelException('filterByZoneLien() only accepts arguments of type \ZoneLien or Collection');
        }
    }

    /**
     * Adds a JOIN clause to the query using the ZoneLien relation
     *
     * @param     string $relationAlias optional alias for the relation
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return $this|ChildZoneQuery The current query, for fluid interface
     */
    public function joinZoneLien($relationAlias = null, $joinType = Criteria::INNER_JOIN)
    {
        $tableMap = $this->getTableMap();
        $relationMap = $tableMap->getRelation('ZoneLien');

        // create a ModelJoin object for this join
        $join = new ModelJoin();
        $join->setJoinType($joinType);
        $join->setRelationMap($relationMap, $this->useAliasInSQL ? $this->getModelAlias() : null, $relationAlias);
        if ($previousJoin = $this->getPreviousJoin()) {
            $join->setPreviousJoin($previousJoin);
        }

        // add the ModelJoin to the current object
        if ($relationAlias) {
            $this->addAlias($relationAlias, $relationMap->getRightTable()->getName());
            $this->addJoinObject($join, $relationAlias);
        } else {
            $this->addJoinObject($join, 'ZoneLien');
        }

        return $this;
    }

    /**
     * Use the ZoneLien relation ZoneLien object
     *
     * @see useQuery()
     *
     * @param     string $relationAlias optional alias for the relation,
     *                                   to be used as main alias in the secondary query
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return \ZoneLienQuery A secondary query class using the current class as primary query
     */
    public function useZoneLienQuery($relationAlias = null, $joinType = Criteria::INNER_JOIN)
    {
        return $this
            ->joinZoneLien($relationAlias, $joinType)
            ->useQuery($relationAlias ? $relationAlias : 'ZoneLien', '\ZoneLienQuery');
    }

    /**
     * Exclude object from result
     *
     * @param   ChildZone $zone Object to remove from the list of results
     *
     * @return $this|ChildZoneQuery The current query, for fluid interface
     */
    public function prune($zone = null)
    {
        if ($zone) {
            $this->addUsingAlias(ZoneTableMap::COL_ID_ZONE, $zone->getIdZone(), Criteria::NOT_EQUAL);
        }

        return $this;
    }

    /**
     * Deletes all rows from the geo_zones table.
     *
     * @param ConnectionInterface $con the connection to use
     * @return int The number of affected rows (if supported by underlying database driver).
     */
    public function doDeleteAll(ConnectionInterface $con = null)
    {
        if (null === $con) {
            $con = Propel::getServiceContainer()->getWriteConnection(ZoneTableMap::DATABASE_NAME);
        }

        // use transaction because $criteria could contain info
        // for more than one table or we could emulating ON DELETE CASCADE, etc.
        return $con->transaction(function () use ($con) {
            $affectedRows = 0; // initialize var to track total num of affected rows
            $affectedRows += parent::doDeleteAll($con);
            // Because this db requires some delete cascade/set null emulation, we have to
            // clear the cached instance *after* the emulation has happened (since
            // instances get re-added by the select statement contained therein).
            ZoneTableMap::clearInstancePool();
            ZoneTableMap::clearRelatedInstancePool();

            return $affectedRows;
        });
    }

    /**
     * Performs a DELETE on the database based on the current ModelCriteria
     *
     * @param ConnectionInterface $con the connection to use
     * @return int             The number of affected rows (if supported by underlying database driver).  This includes CASCADE-related rows
     *                         if supported by native driver or if emulated using Propel.
     * @throws PropelException Any exceptions caught during processing will be
     *                         rethrown wrapped into a PropelException.
     */
    public function delete(ConnectionInterface $con = null)
    {
        if (null === $con) {
            $con = Propel::getServiceContainer()->getWriteConnection(ZoneTableMap::DATABASE_NAME);
        }

        $criteria = $this;

        // Set the correct dbName
        $criteria->setDbName(ZoneTableMap::DATABASE_NAME);

        // use transaction because $criteria could contain info
        // for more than one table or we could emulating ON DELETE CASCADE, etc.
        return $con->transaction(function () use ($con, $criteria) {
            $affectedRows = 0; // initialize var to track total num of affected rows

            ZoneTableMap::removeInstanceFromPool($criteria);

            $affectedRows += ModelCriteria::delete($con);
            ZoneTableMap::clearRelatedInstancePool();

            return $affectedRows;
        });
    }

    // timestampable behavior

    /**
     * Filter by the latest updated
     *
     * @param      int $nbDays Maximum age of the latest update in days
     *
     * @return     $this|ChildZoneQuery The current query, for fluid interface
     */
    public function recentlyUpdated($nbDays = 7)
    {
        return $this->addUsingAlias(ZoneTableMap::COL_UPDATED_AT, time() - $nbDays * 24 * 60 * 60, Criteria::GREATER_EQUAL);
    }

    /**
     * Order by update date desc
     *
     * @return     $this|ChildZoneQuery The current query, for fluid interface
     */
    public function lastUpdatedFirst()
    {
        return $this->addDescendingOrderByColumn(ZoneTableMap::COL_UPDATED_AT);
    }

    /**
     * Order by update date asc
     *
     * @return     $this|ChildZoneQuery The current query, for fluid interface
     */
    public function firstUpdatedFirst()
    {
        return $this->addAscendingOrderByColumn(ZoneTableMap::COL_UPDATED_AT);
    }

    /**
     * Order by create date desc
     *
     * @return     $this|ChildZoneQuery The current query, for fluid interface
     */
    public function lastCreatedFirst()
    {
        return $this->addDescendingOrderByColumn(ZoneTableMap::COL_CREATED_AT);
    }

    /**
     * Filter by the latest created
     *
     * @param      int $nbDays Maximum age of in days
     *
     * @return     $this|ChildZoneQuery The current query, for fluid interface
     */
    public function recentlyCreated($nbDays = 7)
    {
        return $this->addUsingAlias(ZoneTableMap::COL_CREATED_AT, time() - $nbDays * 24 * 60 * 60, Criteria::GREATER_EQUAL);
    }

    /**
     * Order by create date asc
     *
     * @return     $this|ChildZoneQuery The current query, for fluid interface
     */
    public function firstCreatedFirst()
    {
        return $this->addAscendingOrderByColumn(ZoneTableMap::COL_CREATED_AT);
    }

} // ZoneQuery
