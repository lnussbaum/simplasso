<?php

namespace Base;

use \AssoMotsArchive as ChildAssoMotsArchive;
use \AssoMotsArchiveQuery as ChildAssoMotsArchiveQuery;
use \Mot as ChildMot;
use \MotLien as ChildMotLien;
use \MotLienQuery as ChildMotLienQuery;
use \MotQuery as ChildMotQuery;
use \Motgroupe as ChildMotgroupe;
use \MotgroupeQuery as ChildMotgroupeQuery;
use \DateTime;
use \Exception;
use \PDO;
use Map\MotLienTableMap;
use Map\MotTableMap;
use Propel\Runtime\Propel;
use Propel\Runtime\ActiveQuery\Criteria;
use Propel\Runtime\ActiveQuery\ModelCriteria;
use Propel\Runtime\ActiveRecord\ActiveRecordInterface;
use Propel\Runtime\Collection\Collection;
use Propel\Runtime\Collection\ObjectCollection;
use Propel\Runtime\Connection\ConnectionInterface;
use Propel\Runtime\Exception\BadMethodCallException;
use Propel\Runtime\Exception\LogicException;
use Propel\Runtime\Exception\PropelException;
use Propel\Runtime\Map\TableMap;
use Propel\Runtime\Parser\AbstractParser;
use Propel\Runtime\Util\PropelDateTime;

/**
 * Base class that represents a row from the 'asso_mots' table.
 *
 *
 *
 * @package    propel.generator..Base
 */
abstract class Mot implements ActiveRecordInterface
{
    /**
     * TableMap class name
     */
    const TABLE_MAP = '\\Map\\MotTableMap';


    /**
     * attribute to determine if this object has previously been saved.
     * @var boolean
     */
    protected $new = true;

    /**
     * attribute to determine whether this object has been deleted.
     * @var boolean
     */
    protected $deleted = false;

    /**
     * The columns that have been modified in current object.
     * Tracking modified columns allows us to only update modified columns.
     * @var array
     */
    protected $modifiedColumns = array();

    /**
     * The (virtual) columns that are added at runtime
     * The formatters can add supplementary columns based on a resultset
     * @var array
     */
    protected $virtualColumns = array();

    /**
     * The value for the id_mot field.
     *
     * @var        string
     */
    protected $id_mot;

    /**
     * The value for the nom field.
     *
     * @var        string
     */
    protected $nom;

    /**
     * The value for the nomcourt field.
     *
     * @var        string
     */
    protected $nomcourt;

    /**
     * The value for the descriptif field.
     *
     * @var        string
     */
    protected $descriptif;

    /**
     * The value for the texte field.
     *
     * @var        string
     */
    protected $texte;

    /**
     * The value for the importance field.
     *
     * Note: this column has a database default value of: 1
     * @var        int
     */
    protected $importance;

    /**
     * The value for the actif field.
     *
     * Note: this column has a database default value of: 1
     * @var        int
     */
    protected $actif;

    /**
     * The value for the id_motgroupe field.
     *
     * Note: this column has a database default value of: '0'
     * @var        string
     */
    protected $id_motgroupe;

    /**
     * The value for the created_at field.
     *
     * @var        DateTime
     */
    protected $created_at;

    /**
     * The value for the updated_at field.
     *
     * @var        DateTime
     */
    protected $updated_at;

    /**
     * @var        ChildMotgroupe
     */
    protected $aMotgroupe;

    /**
     * @var        ObjectCollection|ChildMotLien[] Collection to store aggregation of ChildMotLien objects.
     */
    protected $collMotLiens;
    protected $collMotLiensPartial;

    /**
     * Flag to prevent endless save loop, if this object is referenced
     * by another object which falls in this transaction.
     *
     * @var boolean
     */
    protected $alreadyInSave = false;

    // archivable behavior
    protected $archiveOnDelete = true;

    /**
     * An array of objects scheduled for deletion.
     * @var ObjectCollection|ChildMotLien[]
     */
    protected $motLiensScheduledForDeletion = null;

    /**
     * Applies default values to this object.
     * This method should be called from the object's constructor (or
     * equivalent initialization method).
     * @see __construct()
     */
    public function applyDefaultValues()
    {
        $this->importance = 1;
        $this->actif = 1;
        $this->id_motgroupe = '0';
    }

    /**
     * Initializes internal state of Base\Mot object.
     * @see applyDefaults()
     */
    public function __construct()
    {
        $this->applyDefaultValues();
    }

    /**
     * Returns whether the object has been modified.
     *
     * @return boolean True if the object has been modified.
     */
    public function isModified()
    {
        return !!$this->modifiedColumns;
    }

    /**
     * Has specified column been modified?
     *
     * @param  string  $col column fully qualified name (TableMap::TYPE_COLNAME), e.g. Book::AUTHOR_ID
     * @return boolean True if $col has been modified.
     */
    public function isColumnModified($col)
    {
        return $this->modifiedColumns && isset($this->modifiedColumns[$col]);
    }

    /**
     * Get the columns that have been modified in this object.
     * @return array A unique list of the modified column names for this object.
     */
    public function getModifiedColumns()
    {
        return $this->modifiedColumns ? array_keys($this->modifiedColumns) : [];
    }

    /**
     * Returns whether the object has ever been saved.  This will
     * be false, if the object was retrieved from storage or was created
     * and then saved.
     *
     * @return boolean true, if the object has never been persisted.
     */
    public function isNew()
    {
        return $this->new;
    }

    /**
     * Setter for the isNew attribute.  This method will be called
     * by Propel-generated children and objects.
     *
     * @param boolean $b the state of the object.
     */
    public function setNew($b)
    {
        $this->new = (boolean) $b;
    }

    /**
     * Whether this object has been deleted.
     * @return boolean The deleted state of this object.
     */
    public function isDeleted()
    {
        return $this->deleted;
    }

    /**
     * Specify whether this object has been deleted.
     * @param  boolean $b The deleted state of this object.
     * @return void
     */
    public function setDeleted($b)
    {
        $this->deleted = (boolean) $b;
    }

    /**
     * Sets the modified state for the object to be false.
     * @param  string $col If supplied, only the specified column is reset.
     * @return void
     */
    public function resetModified($col = null)
    {
        if (null !== $col) {
            if (isset($this->modifiedColumns[$col])) {
                unset($this->modifiedColumns[$col]);
            }
        } else {
            $this->modifiedColumns = array();
        }
    }

    /**
     * Compares this with another <code>Mot</code> instance.  If
     * <code>obj</code> is an instance of <code>Mot</code>, delegates to
     * <code>equals(Mot)</code>.  Otherwise, returns <code>false</code>.
     *
     * @param  mixed   $obj The object to compare to.
     * @return boolean Whether equal to the object specified.
     */
    public function equals($obj)
    {
        if (!$obj instanceof static) {
            return false;
        }

        if ($this === $obj) {
            return true;
        }

        if (null === $this->getPrimaryKey() || null === $obj->getPrimaryKey()) {
            return false;
        }

        return $this->getPrimaryKey() === $obj->getPrimaryKey();
    }

    /**
     * Get the associative array of the virtual columns in this object
     *
     * @return array
     */
    public function getVirtualColumns()
    {
        return $this->virtualColumns;
    }

    /**
     * Checks the existence of a virtual column in this object
     *
     * @param  string  $name The virtual column name
     * @return boolean
     */
    public function hasVirtualColumn($name)
    {
        return array_key_exists($name, $this->virtualColumns);
    }

    /**
     * Get the value of a virtual column in this object
     *
     * @param  string $name The virtual column name
     * @return mixed
     *
     * @throws PropelException
     */
    public function getVirtualColumn($name)
    {
        if (!$this->hasVirtualColumn($name)) {
            throw new PropelException(sprintf('Cannot get value of inexistent virtual column %s.', $name));
        }

        return $this->virtualColumns[$name];
    }

    /**
     * Set the value of a virtual column in this object
     *
     * @param string $name  The virtual column name
     * @param mixed  $value The value to give to the virtual column
     *
     * @return $this|Mot The current object, for fluid interface
     */
    public function setVirtualColumn($name, $value)
    {
        $this->virtualColumns[$name] = $value;

        return $this;
    }

    /**
     * Logs a message using Propel::log().
     *
     * @param  string  $msg
     * @param  int     $priority One of the Propel::LOG_* logging levels
     * @return boolean
     */
    protected function log($msg, $priority = Propel::LOG_INFO)
    {
        return Propel::log(get_class($this) . ': ' . $msg, $priority);
    }

    /**
     * Export the current object properties to a string, using a given parser format
     * <code>
     * $book = BookQuery::create()->findPk(9012);
     * echo $book->exportTo('JSON');
     *  => {"Id":9012,"Title":"Don Juan","ISBN":"0140422161","Price":12.99,"PublisherId":1234,"AuthorId":5678}');
     * </code>
     *
     * @param  mixed   $parser                 A AbstractParser instance, or a format name ('XML', 'YAML', 'JSON', 'CSV')
     * @param  boolean $includeLazyLoadColumns (optional) Whether to include lazy load(ed) columns. Defaults to TRUE.
     * @return string  The exported data
     */
    public function exportTo($parser, $includeLazyLoadColumns = true)
    {
        if (!$parser instanceof AbstractParser) {
            $parser = AbstractParser::getParser($parser);
        }

        return $parser->fromArray($this->toArray(TableMap::TYPE_PHPNAME, $includeLazyLoadColumns, array(), true));
    }

    /**
     * Clean up internal collections prior to serializing
     * Avoids recursive loops that turn into segmentation faults when serializing
     */
    public function __sleep()
    {
        $this->clearAllReferences();

        $cls = new \ReflectionClass($this);
        $propertyNames = [];
        $serializableProperties = array_diff($cls->getProperties(), $cls->getProperties(\ReflectionProperty::IS_STATIC));

        foreach($serializableProperties as $property) {
            $propertyNames[] = $property->getName();
        }

        return $propertyNames;
    }

    /**
     * Get the [id_mot] column value.
     *
     * @return string
     */
    public function getIdMot()
    {
        return $this->id_mot;
    }

    /**
     * Get the [nom] column value.
     *
     * @return string
     */
    public function getNom()
    {
        return $this->nom;
    }

    /**
     * Get the [nomcourt] column value.
     *
     * @return string
     */
    public function getNomcourt()
    {
        return $this->nomcourt;
    }

    /**
     * Get the [descriptif] column value.
     *
     * @return string
     */
    public function getDescriptif()
    {
        return $this->descriptif;
    }

    /**
     * Get the [texte] column value.
     *
     * @return string
     */
    public function getTexte()
    {
        return $this->texte;
    }

    /**
     * Get the [importance] column value.
     *
     * @return int
     */
    public function getImportance()
    {
        return $this->importance;
    }

    /**
     * Get the [actif] column value.
     *
     * @return int
     */
    public function getActif()
    {
        return $this->actif;
    }

    /**
     * Get the [id_motgroupe] column value.
     *
     * @return string
     */
    public function getIdMotgroupe()
    {
        return $this->id_motgroupe;
    }

    /**
     * Get the [optionally formatted] temporal [created_at] column value.
     *
     *
     * @param      string $format The date/time format string (either date()-style or strftime()-style).
     *                            If format is NULL, then the raw DateTime object will be returned.
     *
     * @return string|DateTime Formatted date/time value as string or DateTime object (if format is NULL), NULL if column is NULL, and 0 if column value is 0000-00-00 00:00:00
     *
     * @throws PropelException - if unable to parse/validate the date/time value.
     */
    public function getCreatedAt($format = NULL)
    {
        if ($format === null) {
            return $this->created_at;
        } else {
            return $this->created_at instanceof \DateTimeInterface ? $this->created_at->format($format) : null;
        }
    }

    /**
     * Get the [optionally formatted] temporal [updated_at] column value.
     *
     *
     * @param      string $format The date/time format string (either date()-style or strftime()-style).
     *                            If format is NULL, then the raw DateTime object will be returned.
     *
     * @return string|DateTime Formatted date/time value as string or DateTime object (if format is NULL), NULL if column is NULL, and 0 if column value is 0000-00-00 00:00:00
     *
     * @throws PropelException - if unable to parse/validate the date/time value.
     */
    public function getUpdatedAt($format = NULL)
    {
        if ($format === null) {
            return $this->updated_at;
        } else {
            return $this->updated_at instanceof \DateTimeInterface ? $this->updated_at->format($format) : null;
        }
    }

    /**
     * Set the value of [id_mot] column.
     *
     * @param string $v new value
     * @return $this|\Mot The current object (for fluent API support)
     */
    public function setIdMot($v)
    {
        if ($v !== null) {
            $v = (string) $v;
        }

        if ($this->id_mot !== $v) {
            $this->id_mot = $v;
            $this->modifiedColumns[MotTableMap::COL_ID_MOT] = true;
        }

        return $this;
    } // setIdMot()

    /**
     * Set the value of [nom] column.
     *
     * @param string $v new value
     * @return $this|\Mot The current object (for fluent API support)
     */
    public function setNom($v)
    {
        if ($v !== null) {
            $v = (string) $v;
        }

        if ($this->nom !== $v) {
            $this->nom = $v;
            $this->modifiedColumns[MotTableMap::COL_NOM] = true;
        }

        return $this;
    } // setNom()

    /**
     * Set the value of [nomcourt] column.
     *
     * @param string $v new value
     * @return $this|\Mot The current object (for fluent API support)
     */
    public function setNomcourt($v)
    {
        if ($v !== null) {
            $v = (string) $v;
        }

        if ($this->nomcourt !== $v) {
            $this->nomcourt = $v;
            $this->modifiedColumns[MotTableMap::COL_NOMCOURT] = true;
        }

        return $this;
    } // setNomcourt()

    /**
     * Set the value of [descriptif] column.
     *
     * @param string $v new value
     * @return $this|\Mot The current object (for fluent API support)
     */
    public function setDescriptif($v)
    {
        if ($v !== null) {
            $v = (string) $v;
        }

        if ($this->descriptif !== $v) {
            $this->descriptif = $v;
            $this->modifiedColumns[MotTableMap::COL_DESCRIPTIF] = true;
        }

        return $this;
    } // setDescriptif()

    /**
     * Set the value of [texte] column.
     *
     * @param string $v new value
     * @return $this|\Mot The current object (for fluent API support)
     */
    public function setTexte($v)
    {
        if ($v !== null) {
            $v = (string) $v;
        }

        if ($this->texte !== $v) {
            $this->texte = $v;
            $this->modifiedColumns[MotTableMap::COL_TEXTE] = true;
        }

        return $this;
    } // setTexte()

    /**
     * Set the value of [importance] column.
     *
     * @param int $v new value
     * @return $this|\Mot The current object (for fluent API support)
     */
    public function setImportance($v)
    {
        if ($v !== null) {
            $v = (int) $v;
        }

        if ($this->importance !== $v) {
            $this->importance = $v;
            $this->modifiedColumns[MotTableMap::COL_IMPORTANCE] = true;
        }

        return $this;
    } // setImportance()

    /**
     * Set the value of [actif] column.
     *
     * @param int $v new value
     * @return $this|\Mot The current object (for fluent API support)
     */
    public function setActif($v)
    {
        if ($v !== null) {
            $v = (int) $v;
        }

        if ($this->actif !== $v) {
            $this->actif = $v;
            $this->modifiedColumns[MotTableMap::COL_ACTIF] = true;
        }

        return $this;
    } // setActif()

    /**
     * Set the value of [id_motgroupe] column.
     *
     * @param string $v new value
     * @return $this|\Mot The current object (for fluent API support)
     */
    public function setIdMotgroupe($v)
    {
        if ($v !== null) {
            $v = (string) $v;
        }

        if ($this->id_motgroupe !== $v) {
            $this->id_motgroupe = $v;
            $this->modifiedColumns[MotTableMap::COL_ID_MOTGROUPE] = true;
        }

        if ($this->aMotgroupe !== null && $this->aMotgroupe->getIdMotgroupe() !== $v) {
            $this->aMotgroupe = null;
        }

        return $this;
    } // setIdMotgroupe()

    /**
     * Sets the value of [created_at] column to a normalized version of the date/time value specified.
     *
     * @param  mixed $v string, integer (timestamp), or \DateTimeInterface value.
     *               Empty strings are treated as NULL.
     * @return $this|\Mot The current object (for fluent API support)
     */
    public function setCreatedAt($v)
    {
        $dt = PropelDateTime::newInstance($v, null, 'DateTime');
        if ($this->created_at !== null || $dt !== null) {
            if ($this->created_at === null || $dt === null || $dt->format("Y-m-d H:i:s.u") !== $this->created_at->format("Y-m-d H:i:s.u")) {
                $this->created_at = $dt === null ? null : clone $dt;
                $this->modifiedColumns[MotTableMap::COL_CREATED_AT] = true;
            }
        } // if either are not null

        return $this;
    } // setCreatedAt()

    /**
     * Sets the value of [updated_at] column to a normalized version of the date/time value specified.
     *
     * @param  mixed $v string, integer (timestamp), or \DateTimeInterface value.
     *               Empty strings are treated as NULL.
     * @return $this|\Mot The current object (for fluent API support)
     */
    public function setUpdatedAt($v)
    {
        $dt = PropelDateTime::newInstance($v, null, 'DateTime');
        if ($this->updated_at !== null || $dt !== null) {
            if ($this->updated_at === null || $dt === null || $dt->format("Y-m-d H:i:s.u") !== $this->updated_at->format("Y-m-d H:i:s.u")) {
                $this->updated_at = $dt === null ? null : clone $dt;
                $this->modifiedColumns[MotTableMap::COL_UPDATED_AT] = true;
            }
        } // if either are not null

        return $this;
    } // setUpdatedAt()

    /**
     * Indicates whether the columns in this object are only set to default values.
     *
     * This method can be used in conjunction with isModified() to indicate whether an object is both
     * modified _and_ has some values set which are non-default.
     *
     * @return boolean Whether the columns in this object are only been set with default values.
     */
    public function hasOnlyDefaultValues()
    {
            if ($this->importance !== 1) {
                return false;
            }

            if ($this->actif !== 1) {
                return false;
            }

            if ($this->id_motgroupe !== '0') {
                return false;
            }

        // otherwise, everything was equal, so return TRUE
        return true;
    } // hasOnlyDefaultValues()

    /**
     * Hydrates (populates) the object variables with values from the database resultset.
     *
     * An offset (0-based "start column") is specified so that objects can be hydrated
     * with a subset of the columns in the resultset rows.  This is needed, for example,
     * for results of JOIN queries where the resultset row includes columns from two or
     * more tables.
     *
     * @param array   $row       The row returned by DataFetcher->fetch().
     * @param int     $startcol  0-based offset column which indicates which restultset column to start with.
     * @param boolean $rehydrate Whether this object is being re-hydrated from the database.
     * @param string  $indexType The index type of $row. Mostly DataFetcher->getIndexType().
                                  One of the class type constants TableMap::TYPE_PHPNAME, TableMap::TYPE_CAMELNAME
     *                            TableMap::TYPE_COLNAME, TableMap::TYPE_FIELDNAME, TableMap::TYPE_NUM.
     *
     * @return int             next starting column
     * @throws PropelException - Any caught Exception will be rewrapped as a PropelException.
     */
    public function hydrate($row, $startcol = 0, $rehydrate = false, $indexType = TableMap::TYPE_NUM)
    {
        try {

            $col = $row[TableMap::TYPE_NUM == $indexType ? 0 + $startcol : MotTableMap::translateFieldName('IdMot', TableMap::TYPE_PHPNAME, $indexType)];
            $this->id_mot = (null !== $col) ? (string) $col : null;

            $col = $row[TableMap::TYPE_NUM == $indexType ? 1 + $startcol : MotTableMap::translateFieldName('Nom', TableMap::TYPE_PHPNAME, $indexType)];
            $this->nom = (null !== $col) ? (string) $col : null;

            $col = $row[TableMap::TYPE_NUM == $indexType ? 2 + $startcol : MotTableMap::translateFieldName('Nomcourt', TableMap::TYPE_PHPNAME, $indexType)];
            $this->nomcourt = (null !== $col) ? (string) $col : null;

            $col = $row[TableMap::TYPE_NUM == $indexType ? 3 + $startcol : MotTableMap::translateFieldName('Descriptif', TableMap::TYPE_PHPNAME, $indexType)];
            $this->descriptif = (null !== $col) ? (string) $col : null;

            $col = $row[TableMap::TYPE_NUM == $indexType ? 4 + $startcol : MotTableMap::translateFieldName('Texte', TableMap::TYPE_PHPNAME, $indexType)];
            $this->texte = (null !== $col) ? (string) $col : null;

            $col = $row[TableMap::TYPE_NUM == $indexType ? 5 + $startcol : MotTableMap::translateFieldName('Importance', TableMap::TYPE_PHPNAME, $indexType)];
            $this->importance = (null !== $col) ? (int) $col : null;

            $col = $row[TableMap::TYPE_NUM == $indexType ? 6 + $startcol : MotTableMap::translateFieldName('Actif', TableMap::TYPE_PHPNAME, $indexType)];
            $this->actif = (null !== $col) ? (int) $col : null;

            $col = $row[TableMap::TYPE_NUM == $indexType ? 7 + $startcol : MotTableMap::translateFieldName('IdMotgroupe', TableMap::TYPE_PHPNAME, $indexType)];
            $this->id_motgroupe = (null !== $col) ? (string) $col : null;

            $col = $row[TableMap::TYPE_NUM == $indexType ? 8 + $startcol : MotTableMap::translateFieldName('CreatedAt', TableMap::TYPE_PHPNAME, $indexType)];
            if ($col === '0000-00-00 00:00:00') {
                $col = null;
            }
            $this->created_at = (null !== $col) ? PropelDateTime::newInstance($col, null, 'DateTime') : null;

            $col = $row[TableMap::TYPE_NUM == $indexType ? 9 + $startcol : MotTableMap::translateFieldName('UpdatedAt', TableMap::TYPE_PHPNAME, $indexType)];
            if ($col === '0000-00-00 00:00:00') {
                $col = null;
            }
            $this->updated_at = (null !== $col) ? PropelDateTime::newInstance($col, null, 'DateTime') : null;
            $this->resetModified();

            $this->setNew(false);

            if ($rehydrate) {
                $this->ensureConsistency();
            }

            return $startcol + 10; // 10 = MotTableMap::NUM_HYDRATE_COLUMNS.

        } catch (Exception $e) {
            throw new PropelException(sprintf('Error populating %s object', '\\Mot'), 0, $e);
        }
    }

    /**
     * Checks and repairs the internal consistency of the object.
     *
     * This method is executed after an already-instantiated object is re-hydrated
     * from the database.  It exists to check any foreign keys to make sure that
     * the objects related to the current object are correct based on foreign key.
     *
     * You can override this method in the stub class, but you should always invoke
     * the base method from the overridden method (i.e. parent::ensureConsistency()),
     * in case your model changes.
     *
     * @throws PropelException
     */
    public function ensureConsistency()
    {
        if ($this->aMotgroupe !== null && $this->id_motgroupe !== $this->aMotgroupe->getIdMotgroupe()) {
            $this->aMotgroupe = null;
        }
    } // ensureConsistency

    /**
     * Reloads this object from datastore based on primary key and (optionally) resets all associated objects.
     *
     * This will only work if the object has been saved and has a valid primary key set.
     *
     * @param      boolean $deep (optional) Whether to also de-associated any related objects.
     * @param      ConnectionInterface $con (optional) The ConnectionInterface connection to use.
     * @return void
     * @throws PropelException - if this object is deleted, unsaved or doesn't have pk match in db
     */
    public function reload($deep = false, ConnectionInterface $con = null)
    {
        if ($this->isDeleted()) {
            throw new PropelException("Cannot reload a deleted object.");
        }

        if ($this->isNew()) {
            throw new PropelException("Cannot reload an unsaved object.");
        }

        if ($con === null) {
            $con = Propel::getServiceContainer()->getReadConnection(MotTableMap::DATABASE_NAME);
        }

        // We don't need to alter the object instance pool; we're just modifying this instance
        // already in the pool.

        $dataFetcher = ChildMotQuery::create(null, $this->buildPkeyCriteria())->setFormatter(ModelCriteria::FORMAT_STATEMENT)->find($con);
        $row = $dataFetcher->fetch();
        $dataFetcher->close();
        if (!$row) {
            throw new PropelException('Cannot find matching row in the database to reload object values.');
        }
        $this->hydrate($row, 0, true, $dataFetcher->getIndexType()); // rehydrate

        if ($deep) {  // also de-associate any related objects?

            $this->aMotgroupe = null;
            $this->collMotLiens = null;

        } // if (deep)
    }

    /**
     * Removes this object from datastore and sets delete attribute.
     *
     * @param      ConnectionInterface $con
     * @return void
     * @throws PropelException
     * @see Mot::setDeleted()
     * @see Mot::isDeleted()
     */
    public function delete(ConnectionInterface $con = null)
    {
        if ($this->isDeleted()) {
            throw new PropelException("This object has already been deleted.");
        }

        if ($con === null) {
            $con = Propel::getServiceContainer()->getWriteConnection(MotTableMap::DATABASE_NAME);
        }

        $con->transaction(function () use ($con) {
            $deleteQuery = ChildMotQuery::create()
                ->filterByPrimaryKey($this->getPrimaryKey());
            $ret = $this->preDelete($con);
            // archivable behavior
            if ($ret) {
                if ($this->archiveOnDelete) {
                    // do nothing yet. The object will be archived later when calling ChildMotQuery::delete().
                } else {
                    $deleteQuery->setArchiveOnDelete(false);
                    $this->archiveOnDelete = true;
                }
            }

            if ($ret) {
                $deleteQuery->delete($con);
                $this->postDelete($con);
                $this->setDeleted(true);
            }
        });
    }

    /**
     * Persists this object to the database.
     *
     * If the object is new, it inserts it; otherwise an update is performed.
     * All modified related objects will also be persisted in the doSave()
     * method.  This method wraps all precipitate database operations in a
     * single transaction.
     *
     * @param      ConnectionInterface $con
     * @return int             The number of rows affected by this insert/update and any referring fk objects' save() operations.
     * @throws PropelException
     * @see doSave()
     */
    public function save(ConnectionInterface $con = null)
    {
        if ($this->isDeleted()) {
            throw new PropelException("You cannot save an object that has been deleted.");
        }

        if ($this->alreadyInSave) {
            return 0;
        }

        if ($con === null) {
            $con = Propel::getServiceContainer()->getWriteConnection(MotTableMap::DATABASE_NAME);
        }

        return $con->transaction(function () use ($con) {
            $ret = $this->preSave($con);
            $isInsert = $this->isNew();
            if ($isInsert) {
                $ret = $ret && $this->preInsert($con);
                // timestampable behavior

                if (!$this->isColumnModified(MotTableMap::COL_CREATED_AT)) {
                    $this->setCreatedAt(\Propel\Runtime\Util\PropelDateTime::createHighPrecision());
                }
                if (!$this->isColumnModified(MotTableMap::COL_UPDATED_AT)) {
                    $this->setUpdatedAt(\Propel\Runtime\Util\PropelDateTime::createHighPrecision());
                }
            } else {
                $ret = $ret && $this->preUpdate($con);
                // timestampable behavior
                if ($this->isModified() && !$this->isColumnModified(MotTableMap::COL_UPDATED_AT)) {
                    $this->setUpdatedAt(\Propel\Runtime\Util\PropelDateTime::createHighPrecision());
                }
            }
            if ($ret) {
                $affectedRows = $this->doSave($con);
                if ($isInsert) {
                    $this->postInsert($con);
                } else {
                    $this->postUpdate($con);
                }
                $this->postSave($con);
                MotTableMap::addInstanceToPool($this);
            } else {
                $affectedRows = 0;
            }

            return $affectedRows;
        });
    }

    /**
     * Performs the work of inserting or updating the row in the database.
     *
     * If the object is new, it inserts it; otherwise an update is performed.
     * All related objects are also updated in this method.
     *
     * @param      ConnectionInterface $con
     * @return int             The number of rows affected by this insert/update and any referring fk objects' save() operations.
     * @throws PropelException
     * @see save()
     */
    protected function doSave(ConnectionInterface $con)
    {
        $affectedRows = 0; // initialize var to track total num of affected rows
        if (!$this->alreadyInSave) {
            $this->alreadyInSave = true;

            // We call the save method on the following object(s) if they
            // were passed to this object by their corresponding set
            // method.  This object relates to these object(s) by a
            // foreign key reference.

            if ($this->aMotgroupe !== null) {
                if ($this->aMotgroupe->isModified() || $this->aMotgroupe->isNew()) {
                    $affectedRows += $this->aMotgroupe->save($con);
                }
                $this->setMotgroupe($this->aMotgroupe);
            }

            if ($this->isNew() || $this->isModified()) {
                // persist changes
                if ($this->isNew()) {
                    $this->doInsert($con);
                    $affectedRows += 1;
                } else {
                    $affectedRows += $this->doUpdate($con);
                }
                $this->resetModified();
            }

            if ($this->motLiensScheduledForDeletion !== null) {
                if (!$this->motLiensScheduledForDeletion->isEmpty()) {
                    \MotLienQuery::create()
                        ->filterByPrimaryKeys($this->motLiensScheduledForDeletion->getPrimaryKeys(false))
                        ->delete($con);
                    $this->motLiensScheduledForDeletion = null;
                }
            }

            if ($this->collMotLiens !== null) {
                foreach ($this->collMotLiens as $referrerFK) {
                    if (!$referrerFK->isDeleted() && ($referrerFK->isNew() || $referrerFK->isModified())) {
                        $affectedRows += $referrerFK->save($con);
                    }
                }
            }

            $this->alreadyInSave = false;

        }

        return $affectedRows;
    } // doSave()

    /**
     * Insert the row in the database.
     *
     * @param      ConnectionInterface $con
     *
     * @throws PropelException
     * @see doSave()
     */
    protected function doInsert(ConnectionInterface $con)
    {
        $modifiedColumns = array();
        $index = 0;

        $this->modifiedColumns[MotTableMap::COL_ID_MOT] = true;
        if (null !== $this->id_mot) {
            throw new PropelException('Cannot insert a value for auto-increment primary key (' . MotTableMap::COL_ID_MOT . ')');
        }

         // check the columns in natural order for more readable SQL queries
        if ($this->isColumnModified(MotTableMap::COL_ID_MOT)) {
            $modifiedColumns[':p' . $index++]  = '`id_mot`';
        }
        if ($this->isColumnModified(MotTableMap::COL_NOM)) {
            $modifiedColumns[':p' . $index++]  = '`nom`';
        }
        if ($this->isColumnModified(MotTableMap::COL_NOMCOURT)) {
            $modifiedColumns[':p' . $index++]  = '`nomcourt`';
        }
        if ($this->isColumnModified(MotTableMap::COL_DESCRIPTIF)) {
            $modifiedColumns[':p' . $index++]  = '`descriptif`';
        }
        if ($this->isColumnModified(MotTableMap::COL_TEXTE)) {
            $modifiedColumns[':p' . $index++]  = '`texte`';
        }
        if ($this->isColumnModified(MotTableMap::COL_IMPORTANCE)) {
            $modifiedColumns[':p' . $index++]  = '`importance`';
        }
        if ($this->isColumnModified(MotTableMap::COL_ACTIF)) {
            $modifiedColumns[':p' . $index++]  = '`actif`';
        }
        if ($this->isColumnModified(MotTableMap::COL_ID_MOTGROUPE)) {
            $modifiedColumns[':p' . $index++]  = '`id_motgroupe`';
        }
        if ($this->isColumnModified(MotTableMap::COL_CREATED_AT)) {
            $modifiedColumns[':p' . $index++]  = '`created_at`';
        }
        if ($this->isColumnModified(MotTableMap::COL_UPDATED_AT)) {
            $modifiedColumns[':p' . $index++]  = '`updated_at`';
        }

        $sql = sprintf(
            'INSERT INTO `asso_mots` (%s) VALUES (%s)',
            implode(', ', $modifiedColumns),
            implode(', ', array_keys($modifiedColumns))
        );

        try {
            $stmt = $con->prepare($sql);
            foreach ($modifiedColumns as $identifier => $columnName) {
                switch ($columnName) {
                    case '`id_mot`':
                        $stmt->bindValue($identifier, $this->id_mot, PDO::PARAM_INT);
                        break;
                    case '`nom`':
                        $stmt->bindValue($identifier, $this->nom, PDO::PARAM_STR);
                        break;
                    case '`nomcourt`':
                        $stmt->bindValue($identifier, $this->nomcourt, PDO::PARAM_STR);
                        break;
                    case '`descriptif`':
                        $stmt->bindValue($identifier, $this->descriptif, PDO::PARAM_STR);
                        break;
                    case '`texte`':
                        $stmt->bindValue($identifier, $this->texte, PDO::PARAM_STR);
                        break;
                    case '`importance`':
                        $stmt->bindValue($identifier, $this->importance, PDO::PARAM_INT);
                        break;
                    case '`actif`':
                        $stmt->bindValue($identifier, $this->actif, PDO::PARAM_INT);
                        break;
                    case '`id_motgroupe`':
                        $stmt->bindValue($identifier, $this->id_motgroupe, PDO::PARAM_INT);
                        break;
                    case '`created_at`':
                        $stmt->bindValue($identifier, $this->created_at ? $this->created_at->format("Y-m-d H:i:s.u") : null, PDO::PARAM_STR);
                        break;
                    case '`updated_at`':
                        $stmt->bindValue($identifier, $this->updated_at ? $this->updated_at->format("Y-m-d H:i:s.u") : null, PDO::PARAM_STR);
                        break;
                }
            }
            $stmt->execute();
        } catch (Exception $e) {
            Propel::log($e->getMessage(), Propel::LOG_ERR);
            throw new PropelException(sprintf('Unable to execute INSERT statement [%s]', $sql), 0, $e);
        }

        try {
            $pk = $con->lastInsertId();
        } catch (Exception $e) {
            throw new PropelException('Unable to get autoincrement id.', 0, $e);
        }
        $this->setIdMot($pk);

        $this->setNew(false);
    }

    /**
     * Update the row in the database.
     *
     * @param      ConnectionInterface $con
     *
     * @return Integer Number of updated rows
     * @see doSave()
     */
    protected function doUpdate(ConnectionInterface $con)
    {
        $selectCriteria = $this->buildPkeyCriteria();
        $valuesCriteria = $this->buildCriteria();

        return $selectCriteria->doUpdate($valuesCriteria, $con);
    }

    /**
     * Retrieves a field from the object by name passed in as a string.
     *
     * @param      string $name name
     * @param      string $type The type of fieldname the $name is of:
     *                     one of the class type constants TableMap::TYPE_PHPNAME, TableMap::TYPE_CAMELNAME
     *                     TableMap::TYPE_COLNAME, TableMap::TYPE_FIELDNAME, TableMap::TYPE_NUM.
     *                     Defaults to TableMap::TYPE_FIELDNAME.
     * @return mixed Value of field.
     */
    public function getByName($name, $type = TableMap::TYPE_FIELDNAME)
    {
        $pos = MotTableMap::translateFieldName($name, $type, TableMap::TYPE_NUM);
        $field = $this->getByPosition($pos);

        return $field;
    }

    /**
     * Retrieves a field from the object by Position as specified in the xml schema.
     * Zero-based.
     *
     * @param      int $pos position in xml schema
     * @return mixed Value of field at $pos
     */
    public function getByPosition($pos)
    {
        switch ($pos) {
            case 0:
                return $this->getIdMot();
                break;
            case 1:
                return $this->getNom();
                break;
            case 2:
                return $this->getNomcourt();
                break;
            case 3:
                return $this->getDescriptif();
                break;
            case 4:
                return $this->getTexte();
                break;
            case 5:
                return $this->getImportance();
                break;
            case 6:
                return $this->getActif();
                break;
            case 7:
                return $this->getIdMotgroupe();
                break;
            case 8:
                return $this->getCreatedAt();
                break;
            case 9:
                return $this->getUpdatedAt();
                break;
            default:
                return null;
                break;
        } // switch()
    }

    /**
     * Exports the object as an array.
     *
     * You can specify the key type of the array by passing one of the class
     * type constants.
     *
     * @param     string  $keyType (optional) One of the class type constants TableMap::TYPE_PHPNAME, TableMap::TYPE_CAMELNAME,
     *                    TableMap::TYPE_COLNAME, TableMap::TYPE_FIELDNAME, TableMap::TYPE_NUM.
     *                    Defaults to TableMap::TYPE_FIELDNAME.
     * @param     boolean $includeLazyLoadColumns (optional) Whether to include lazy loaded columns. Defaults to TRUE.
     * @param     array $alreadyDumpedObjects List of objects to skip to avoid recursion
     * @param     boolean $includeForeignObjects (optional) Whether to include hydrated related objects. Default to FALSE.
     *
     * @return array an associative array containing the field names (as keys) and field values
     */
    public function toArray($keyType = TableMap::TYPE_FIELDNAME, $includeLazyLoadColumns = true, $alreadyDumpedObjects = array(), $includeForeignObjects = false)
    {

        if (isset($alreadyDumpedObjects['Mot'][$this->hashCode()])) {
            return '*RECURSION*';
        }
        $alreadyDumpedObjects['Mot'][$this->hashCode()] = true;
        $keys = MotTableMap::getFieldNames($keyType);
        $result = array(
            $keys[0] => $this->getIdMot(),
            $keys[1] => $this->getNom(),
            $keys[2] => $this->getNomcourt(),
            $keys[3] => $this->getDescriptif(),
            $keys[4] => $this->getTexte(),
            $keys[5] => $this->getImportance(),
            $keys[6] => $this->getActif(),
            $keys[7] => $this->getIdMotgroupe(),
            $keys[8] => $this->getCreatedAt(),
            $keys[9] => $this->getUpdatedAt(),
        );
        if ($result[$keys[8]] instanceof \DateTimeInterface) {
            $result[$keys[8]] = $result[$keys[8]]->format('c');
        }

        if ($result[$keys[9]] instanceof \DateTimeInterface) {
            $result[$keys[9]] = $result[$keys[9]]->format('c');
        }

        $virtualColumns = $this->virtualColumns;
        foreach ($virtualColumns as $key => $virtualColumn) {
            $result[$key] = $virtualColumn;
        }

        if ($includeForeignObjects) {
            if (null !== $this->aMotgroupe) {

                switch ($keyType) {
                    case TableMap::TYPE_CAMELNAME:
                        $key = 'motgroupe';
                        break;
                    case TableMap::TYPE_FIELDNAME:
                        $key = 'asso_motgroupes';
                        break;
                    default:
                        $key = 'Motgroupe';
                }

                $result[$key] = $this->aMotgroupe->toArray($keyType, $includeLazyLoadColumns,  $alreadyDumpedObjects, true);
            }
            if (null !== $this->collMotLiens) {

                switch ($keyType) {
                    case TableMap::TYPE_CAMELNAME:
                        $key = 'motLiens';
                        break;
                    case TableMap::TYPE_FIELDNAME:
                        $key = 'asso_mots_lienss';
                        break;
                    default:
                        $key = 'MotLiens';
                }

                $result[$key] = $this->collMotLiens->toArray(null, false, $keyType, $includeLazyLoadColumns, $alreadyDumpedObjects);
            }
        }

        return $result;
    }

    /**
     * Sets a field from the object by name passed in as a string.
     *
     * @param  string $name
     * @param  mixed  $value field value
     * @param  string $type The type of fieldname the $name is of:
     *                one of the class type constants TableMap::TYPE_PHPNAME, TableMap::TYPE_CAMELNAME
     *                TableMap::TYPE_COLNAME, TableMap::TYPE_FIELDNAME, TableMap::TYPE_NUM.
     *                Defaults to TableMap::TYPE_FIELDNAME.
     * @return $this|\Mot
     */
    public function setByName($name, $value, $type = TableMap::TYPE_FIELDNAME)
    {
        $pos = MotTableMap::translateFieldName($name, $type, TableMap::TYPE_NUM);

        return $this->setByPosition($pos, $value);
    }

    /**
     * Sets a field from the object by Position as specified in the xml schema.
     * Zero-based.
     *
     * @param  int $pos position in xml schema
     * @param  mixed $value field value
     * @return $this|\Mot
     */
    public function setByPosition($pos, $value)
    {
        switch ($pos) {
            case 0:
                $this->setIdMot($value);
                break;
            case 1:
                $this->setNom($value);
                break;
            case 2:
                $this->setNomcourt($value);
                break;
            case 3:
                $this->setDescriptif($value);
                break;
            case 4:
                $this->setTexte($value);
                break;
            case 5:
                $this->setImportance($value);
                break;
            case 6:
                $this->setActif($value);
                break;
            case 7:
                $this->setIdMotgroupe($value);
                break;
            case 8:
                $this->setCreatedAt($value);
                break;
            case 9:
                $this->setUpdatedAt($value);
                break;
        } // switch()

        return $this;
    }

    /**
     * Populates the object using an array.
     *
     * This is particularly useful when populating an object from one of the
     * request arrays (e.g. $_POST).  This method goes through the column
     * names, checking to see whether a matching key exists in populated
     * array. If so the setByName() method is called for that column.
     *
     * You can specify the key type of the array by additionally passing one
     * of the class type constants TableMap::TYPE_PHPNAME, TableMap::TYPE_CAMELNAME,
     * TableMap::TYPE_COLNAME, TableMap::TYPE_FIELDNAME, TableMap::TYPE_NUM.
     * The default key type is the column's TableMap::TYPE_FIELDNAME.
     *
     * @param      array  $arr     An array to populate the object from.
     * @param      string $keyType The type of keys the array uses.
     * @return void
     */
    public function fromArray($arr, $keyType = TableMap::TYPE_FIELDNAME)
    {
        $keys = MotTableMap::getFieldNames($keyType);

        if (array_key_exists($keys[0], $arr)) {
            $this->setIdMot($arr[$keys[0]]);
        }
        if (array_key_exists($keys[1], $arr)) {
            $this->setNom($arr[$keys[1]]);
        }
        if (array_key_exists($keys[2], $arr)) {
            $this->setNomcourt($arr[$keys[2]]);
        }
        if (array_key_exists($keys[3], $arr)) {
            $this->setDescriptif($arr[$keys[3]]);
        }
        if (array_key_exists($keys[4], $arr)) {
            $this->setTexte($arr[$keys[4]]);
        }
        if (array_key_exists($keys[5], $arr)) {
            $this->setImportance($arr[$keys[5]]);
        }
        if (array_key_exists($keys[6], $arr)) {
            $this->setActif($arr[$keys[6]]);
        }
        if (array_key_exists($keys[7], $arr)) {
            $this->setIdMotgroupe($arr[$keys[7]]);
        }
        if (array_key_exists($keys[8], $arr)) {
            $this->setCreatedAt($arr[$keys[8]]);
        }
        if (array_key_exists($keys[9], $arr)) {
            $this->setUpdatedAt($arr[$keys[9]]);
        }
    }

     /**
     * Populate the current object from a string, using a given parser format
     * <code>
     * $book = new Book();
     * $book->importFrom('JSON', '{"Id":9012,"Title":"Don Juan","ISBN":"0140422161","Price":12.99,"PublisherId":1234,"AuthorId":5678}');
     * </code>
     *
     * You can specify the key type of the array by additionally passing one
     * of the class type constants TableMap::TYPE_PHPNAME, TableMap::TYPE_CAMELNAME,
     * TableMap::TYPE_COLNAME, TableMap::TYPE_FIELDNAME, TableMap::TYPE_NUM.
     * The default key type is the column's TableMap::TYPE_FIELDNAME.
     *
     * @param mixed $parser A AbstractParser instance,
     *                       or a format name ('XML', 'YAML', 'JSON', 'CSV')
     * @param string $data The source data to import from
     * @param string $keyType The type of keys the array uses.
     *
     * @return $this|\Mot The current object, for fluid interface
     */
    public function importFrom($parser, $data, $keyType = TableMap::TYPE_FIELDNAME)
    {
        if (!$parser instanceof AbstractParser) {
            $parser = AbstractParser::getParser($parser);
        }

        $this->fromArray($parser->toArray($data), $keyType);

        return $this;
    }

    /**
     * Build a Criteria object containing the values of all modified columns in this object.
     *
     * @return Criteria The Criteria object containing all modified values.
     */
    public function buildCriteria()
    {
        $criteria = new Criteria(MotTableMap::DATABASE_NAME);

        if ($this->isColumnModified(MotTableMap::COL_ID_MOT)) {
            $criteria->add(MotTableMap::COL_ID_MOT, $this->id_mot);
        }
        if ($this->isColumnModified(MotTableMap::COL_NOM)) {
            $criteria->add(MotTableMap::COL_NOM, $this->nom);
        }
        if ($this->isColumnModified(MotTableMap::COL_NOMCOURT)) {
            $criteria->add(MotTableMap::COL_NOMCOURT, $this->nomcourt);
        }
        if ($this->isColumnModified(MotTableMap::COL_DESCRIPTIF)) {
            $criteria->add(MotTableMap::COL_DESCRIPTIF, $this->descriptif);
        }
        if ($this->isColumnModified(MotTableMap::COL_TEXTE)) {
            $criteria->add(MotTableMap::COL_TEXTE, $this->texte);
        }
        if ($this->isColumnModified(MotTableMap::COL_IMPORTANCE)) {
            $criteria->add(MotTableMap::COL_IMPORTANCE, $this->importance);
        }
        if ($this->isColumnModified(MotTableMap::COL_ACTIF)) {
            $criteria->add(MotTableMap::COL_ACTIF, $this->actif);
        }
        if ($this->isColumnModified(MotTableMap::COL_ID_MOTGROUPE)) {
            $criteria->add(MotTableMap::COL_ID_MOTGROUPE, $this->id_motgroupe);
        }
        if ($this->isColumnModified(MotTableMap::COL_CREATED_AT)) {
            $criteria->add(MotTableMap::COL_CREATED_AT, $this->created_at);
        }
        if ($this->isColumnModified(MotTableMap::COL_UPDATED_AT)) {
            $criteria->add(MotTableMap::COL_UPDATED_AT, $this->updated_at);
        }

        return $criteria;
    }

    /**
     * Builds a Criteria object containing the primary key for this object.
     *
     * Unlike buildCriteria() this method includes the primary key values regardless
     * of whether or not they have been modified.
     *
     * @throws LogicException if no primary key is defined
     *
     * @return Criteria The Criteria object containing value(s) for primary key(s).
     */
    public function buildPkeyCriteria()
    {
        $criteria = ChildMotQuery::create();
        $criteria->add(MotTableMap::COL_ID_MOT, $this->id_mot);

        return $criteria;
    }

    /**
     * If the primary key is not null, return the hashcode of the
     * primary key. Otherwise, return the hash code of the object.
     *
     * @return int Hashcode
     */
    public function hashCode()
    {
        $validPk = null !== $this->getIdMot();

        $validPrimaryKeyFKs = 0;
        $primaryKeyFKs = [];

        if ($validPk) {
            return crc32(json_encode($this->getPrimaryKey(), JSON_UNESCAPED_UNICODE));
        } elseif ($validPrimaryKeyFKs) {
            return crc32(json_encode($primaryKeyFKs, JSON_UNESCAPED_UNICODE));
        }

        return spl_object_hash($this);
    }

    /**
     * Returns the primary key for this object (row).
     * @return string
     */
    public function getPrimaryKey()
    {
        return $this->getIdMot();
    }

    /**
     * Generic method to set the primary key (id_mot column).
     *
     * @param       string $key Primary key.
     * @return void
     */
    public function setPrimaryKey($key)
    {
        $this->setIdMot($key);
    }

    /**
     * Returns true if the primary key for this object is null.
     * @return boolean
     */
    public function isPrimaryKeyNull()
    {
        return null === $this->getIdMot();
    }

    /**
     * Sets contents of passed object to values from current object.
     *
     * If desired, this method can also make copies of all associated (fkey referrers)
     * objects.
     *
     * @param      object $copyObj An object of \Mot (or compatible) type.
     * @param      boolean $deepCopy Whether to also copy all rows that refer (by fkey) to the current row.
     * @param      boolean $makeNew Whether to reset autoincrement PKs and make the object new.
     * @throws PropelException
     */
    public function copyInto($copyObj, $deepCopy = false, $makeNew = true)
    {
        $copyObj->setNom($this->getNom());
        $copyObj->setNomcourt($this->getNomcourt());
        $copyObj->setDescriptif($this->getDescriptif());
        $copyObj->setTexte($this->getTexte());
        $copyObj->setImportance($this->getImportance());
        $copyObj->setActif($this->getActif());
        $copyObj->setIdMotgroupe($this->getIdMotgroupe());
        $copyObj->setCreatedAt($this->getCreatedAt());
        $copyObj->setUpdatedAt($this->getUpdatedAt());

        if ($deepCopy) {
            // important: temporarily setNew(false) because this affects the behavior of
            // the getter/setter methods for fkey referrer objects.
            $copyObj->setNew(false);

            foreach ($this->getMotLiens() as $relObj) {
                if ($relObj !== $this) {  // ensure that we don't try to copy a reference to ourselves
                    $copyObj->addMotLien($relObj->copy($deepCopy));
                }
            }

        } // if ($deepCopy)

        if ($makeNew) {
            $copyObj->setNew(true);
            $copyObj->setIdMot(NULL); // this is a auto-increment column, so set to default value
        }
    }

    /**
     * Makes a copy of this object that will be inserted as a new row in table when saved.
     * It creates a new object filling in the simple attributes, but skipping any primary
     * keys that are defined for the table.
     *
     * If desired, this method can also make copies of all associated (fkey referrers)
     * objects.
     *
     * @param  boolean $deepCopy Whether to also copy all rows that refer (by fkey) to the current row.
     * @return \Mot Clone of current object.
     * @throws PropelException
     */
    public function copy($deepCopy = false)
    {
        // we use get_class(), because this might be a subclass
        $clazz = get_class($this);
        $copyObj = new $clazz();
        $this->copyInto($copyObj, $deepCopy);

        return $copyObj;
    }

    /**
     * Declares an association between this object and a ChildMotgroupe object.
     *
     * @param  ChildMotgroupe $v
     * @return $this|\Mot The current object (for fluent API support)
     * @throws PropelException
     */
    public function setMotgroupe(ChildMotgroupe $v = null)
    {
        if ($v === null) {
            $this->setIdMotgroupe('0');
        } else {
            $this->setIdMotgroupe($v->getIdMotgroupe());
        }

        $this->aMotgroupe = $v;

        // Add binding for other direction of this n:n relationship.
        // If this object has already been added to the ChildMotgroupe object, it will not be re-added.
        if ($v !== null) {
            $v->addMot($this);
        }


        return $this;
    }


    /**
     * Get the associated ChildMotgroupe object
     *
     * @param  ConnectionInterface $con Optional Connection object.
     * @return ChildMotgroupe The associated ChildMotgroupe object.
     * @throws PropelException
     */
    public function getMotgroupe(ConnectionInterface $con = null)
    {
        if ($this->aMotgroupe === null && (($this->id_motgroupe !== "" && $this->id_motgroupe !== null))) {
            $this->aMotgroupe = ChildMotgroupeQuery::create()->findPk($this->id_motgroupe, $con);
            /* The following can be used additionally to
                guarantee the related object contains a reference
                to this object.  This level of coupling may, however, be
                undesirable since it could result in an only partially populated collection
                in the referenced object.
                $this->aMotgroupe->addMots($this);
             */
        }

        return $this->aMotgroupe;
    }


    /**
     * Initializes a collection based on the name of a relation.
     * Avoids crafting an 'init[$relationName]s' method name
     * that wouldn't work when StandardEnglishPluralizer is used.
     *
     * @param      string $relationName The name of the relation to initialize
     * @return void
     */
    public function initRelation($relationName)
    {
        if ('MotLien' == $relationName) {
            $this->initMotLiens();
            return;
        }
    }

    /**
     * Clears out the collMotLiens collection
     *
     * This does not modify the database; however, it will remove any associated objects, causing
     * them to be refetched by subsequent calls to accessor method.
     *
     * @return void
     * @see        addMotLiens()
     */
    public function clearMotLiens()
    {
        $this->collMotLiens = null; // important to set this to NULL since that means it is uninitialized
    }

    /**
     * Reset is the collMotLiens collection loaded partially.
     */
    public function resetPartialMotLiens($v = true)
    {
        $this->collMotLiensPartial = $v;
    }

    /**
     * Initializes the collMotLiens collection.
     *
     * By default this just sets the collMotLiens collection to an empty array (like clearcollMotLiens());
     * however, you may wish to override this method in your stub class to provide setting appropriate
     * to your application -- for example, setting the initial array to the values stored in database.
     *
     * @param      boolean $overrideExisting If set to true, the method call initializes
     *                                        the collection even if it is not empty
     *
     * @return void
     */
    public function initMotLiens($overrideExisting = true)
    {
        if (null !== $this->collMotLiens && !$overrideExisting) {
            return;
        }

        $collectionClassName = MotLienTableMap::getTableMap()->getCollectionClassName();

        $this->collMotLiens = new $collectionClassName;
        $this->collMotLiens->setModel('\MotLien');
    }

    /**
     * Gets an array of ChildMotLien objects which contain a foreign key that references this object.
     *
     * If the $criteria is not null, it is used to always fetch the results from the database.
     * Otherwise the results are fetched from the database the first time, then cached.
     * Next time the same method is called without $criteria, the cached collection is returned.
     * If this ChildMot is new, it will return
     * an empty collection or the current collection; the criteria is ignored on a new object.
     *
     * @param      Criteria $criteria optional Criteria object to narrow the query
     * @param      ConnectionInterface $con optional connection object
     * @return ObjectCollection|ChildMotLien[] List of ChildMotLien objects
     * @throws PropelException
     */
    public function getMotLiens(Criteria $criteria = null, ConnectionInterface $con = null)
    {
        $partial = $this->collMotLiensPartial && !$this->isNew();
        if (null === $this->collMotLiens || null !== $criteria  || $partial) {
            if ($this->isNew() && null === $this->collMotLiens) {
                // return empty collection
                $this->initMotLiens();
            } else {
                $collMotLiens = ChildMotLienQuery::create(null, $criteria)
                    ->filterByMot($this)
                    ->find($con);

                if (null !== $criteria) {
                    if (false !== $this->collMotLiensPartial && count($collMotLiens)) {
                        $this->initMotLiens(false);

                        foreach ($collMotLiens as $obj) {
                            if (false == $this->collMotLiens->contains($obj)) {
                                $this->collMotLiens->append($obj);
                            }
                        }

                        $this->collMotLiensPartial = true;
                    }

                    return $collMotLiens;
                }

                if ($partial && $this->collMotLiens) {
                    foreach ($this->collMotLiens as $obj) {
                        if ($obj->isNew()) {
                            $collMotLiens[] = $obj;
                        }
                    }
                }

                $this->collMotLiens = $collMotLiens;
                $this->collMotLiensPartial = false;
            }
        }

        return $this->collMotLiens;
    }

    /**
     * Sets a collection of ChildMotLien objects related by a one-to-many relationship
     * to the current object.
     * It will also schedule objects for deletion based on a diff between old objects (aka persisted)
     * and new objects from the given Propel collection.
     *
     * @param      Collection $motLiens A Propel collection.
     * @param      ConnectionInterface $con Optional connection object
     * @return $this|ChildMot The current object (for fluent API support)
     */
    public function setMotLiens(Collection $motLiens, ConnectionInterface $con = null)
    {
        /** @var ChildMotLien[] $motLiensToDelete */
        $motLiensToDelete = $this->getMotLiens(new Criteria(), $con)->diff($motLiens);


        //since at least one column in the foreign key is at the same time a PK
        //we can not just set a PK to NULL in the lines below. We have to store
        //a backup of all values, so we are able to manipulate these items based on the onDelete value later.
        $this->motLiensScheduledForDeletion = clone $motLiensToDelete;

        foreach ($motLiensToDelete as $motLienRemoved) {
            $motLienRemoved->setMot(null);
        }

        $this->collMotLiens = null;
        foreach ($motLiens as $motLien) {
            $this->addMotLien($motLien);
        }

        $this->collMotLiens = $motLiens;
        $this->collMotLiensPartial = false;

        return $this;
    }

    /**
     * Returns the number of related MotLien objects.
     *
     * @param      Criteria $criteria
     * @param      boolean $distinct
     * @param      ConnectionInterface $con
     * @return int             Count of related MotLien objects.
     * @throws PropelException
     */
    public function countMotLiens(Criteria $criteria = null, $distinct = false, ConnectionInterface $con = null)
    {
        $partial = $this->collMotLiensPartial && !$this->isNew();
        if (null === $this->collMotLiens || null !== $criteria || $partial) {
            if ($this->isNew() && null === $this->collMotLiens) {
                return 0;
            }

            if ($partial && !$criteria) {
                return count($this->getMotLiens());
            }

            $query = ChildMotLienQuery::create(null, $criteria);
            if ($distinct) {
                $query->distinct();
            }

            return $query
                ->filterByMot($this)
                ->count($con);
        }

        return count($this->collMotLiens);
    }

    /**
     * Method called to associate a ChildMotLien object to this object
     * through the ChildMotLien foreign key attribute.
     *
     * @param  ChildMotLien $l ChildMotLien
     * @return $this|\Mot The current object (for fluent API support)
     */
    public function addMotLien(ChildMotLien $l)
    {
        if ($this->collMotLiens === null) {
            $this->initMotLiens();
            $this->collMotLiensPartial = true;
        }

        if (!$this->collMotLiens->contains($l)) {
            $this->doAddMotLien($l);

            if ($this->motLiensScheduledForDeletion and $this->motLiensScheduledForDeletion->contains($l)) {
                $this->motLiensScheduledForDeletion->remove($this->motLiensScheduledForDeletion->search($l));
            }
        }

        return $this;
    }

    /**
     * @param ChildMotLien $motLien The ChildMotLien object to add.
     */
    protected function doAddMotLien(ChildMotLien $motLien)
    {
        $this->collMotLiens[]= $motLien;
        $motLien->setMot($this);
    }

    /**
     * @param  ChildMotLien $motLien The ChildMotLien object to remove.
     * @return $this|ChildMot The current object (for fluent API support)
     */
    public function removeMotLien(ChildMotLien $motLien)
    {
        if ($this->getMotLiens()->contains($motLien)) {
            $pos = $this->collMotLiens->search($motLien);
            $this->collMotLiens->remove($pos);
            if (null === $this->motLiensScheduledForDeletion) {
                $this->motLiensScheduledForDeletion = clone $this->collMotLiens;
                $this->motLiensScheduledForDeletion->clear();
            }
            $this->motLiensScheduledForDeletion[]= clone $motLien;
            $motLien->setMot(null);
        }

        return $this;
    }

    /**
     * Clears the current object, sets all attributes to their default values and removes
     * outgoing references as well as back-references (from other objects to this one. Results probably in a database
     * change of those foreign objects when you call `save` there).
     */
    public function clear()
    {
        if (null !== $this->aMotgroupe) {
            $this->aMotgroupe->removeMot($this);
        }
        $this->id_mot = null;
        $this->nom = null;
        $this->nomcourt = null;
        $this->descriptif = null;
        $this->texte = null;
        $this->importance = null;
        $this->actif = null;
        $this->id_motgroupe = null;
        $this->created_at = null;
        $this->updated_at = null;
        $this->alreadyInSave = false;
        $this->clearAllReferences();
        $this->applyDefaultValues();
        $this->resetModified();
        $this->setNew(true);
        $this->setDeleted(false);
    }

    /**
     * Resets all references and back-references to other model objects or collections of model objects.
     *
     * This method is used to reset all php object references (not the actual reference in the database).
     * Necessary for object serialisation.
     *
     * @param      boolean $deep Whether to also clear the references on all referrer objects.
     */
    public function clearAllReferences($deep = false)
    {
        if ($deep) {
            if ($this->collMotLiens) {
                foreach ($this->collMotLiens as $o) {
                    $o->clearAllReferences($deep);
                }
            }
        } // if ($deep)

        $this->collMotLiens = null;
        $this->aMotgroupe = null;
    }

    /**
     * Return the string representation of this object
     *
     * @return string
     */
    public function __toString()
    {
        return (string) $this->exportTo(MotTableMap::DEFAULT_STRING_FORMAT);
    }

    // timestampable behavior

    /**
     * Mark the current object so that the update date doesn't get updated during next save
     *
     * @return     $this|ChildMot The current object (for fluent API support)
     */
    public function keepUpdateDateUnchanged()
    {
        $this->modifiedColumns[MotTableMap::COL_UPDATED_AT] = true;

        return $this;
    }

    // archivable behavior

    /**
     * Get an archived version of the current object.
     *
     * @param ConnectionInterface $con Optional connection object
     *
     * @return     ChildAssoMotsArchive An archive object, or null if the current object was never archived
     */
    public function getArchive(ConnectionInterface $con = null)
    {
        if ($this->isNew()) {
            return null;
        }
        $archive = ChildAssoMotsArchiveQuery::create()
            ->filterByPrimaryKey($this->getPrimaryKey())
            ->findOne($con);

        return $archive;
    }
    /**
     * Copy the data of the current object into a $archiveTablePhpName archive object.
     * The archived object is then saved.
     * If the current object has already been archived, the archived object
     * is updated and not duplicated.
     *
     * @param ConnectionInterface $con Optional connection object
     *
     * @throws PropelException If the object is new
     *
     * @return     ChildAssoMotsArchive The archive object based on this object
     */
    public function archive(ConnectionInterface $con = null)
    {
        if ($this->isNew()) {
            throw new PropelException('New objects cannot be archived. You must save the current object before calling archive().');
        }
        $archive = $this->getArchive($con);
        if (!$archive) {
            $archive = new ChildAssoMotsArchive();
            $archive->setPrimaryKey($this->getPrimaryKey());
        }
        $this->copyInto($archive, $deepCopy = false, $makeNew = false);
        $archive->setArchivedAt(time());
        $archive->save($con);

        return $archive;
    }

    /**
     * Revert the the current object to the state it had when it was last archived.
     * The object must be saved afterwards if the changes must persist.
     *
     * @param ConnectionInterface $con Optional connection object
     *
     * @throws PropelException If the object has no corresponding archive.
     *
     * @return $this|ChildMot The current object (for fluent API support)
     */
    public function restoreFromArchive(ConnectionInterface $con = null)
    {
        $archive = $this->getArchive($con);
        if (!$archive) {
            throw new PropelException('The current object has never been archived and cannot be restored');
        }
        $this->populateFromArchive($archive);

        return $this;
    }

    /**
     * Populates the the current object based on a $archiveTablePhpName archive object.
     *
     * @param      ChildAssoMotsArchive $archive An archived object based on the same class
      * @param      Boolean $populateAutoIncrementPrimaryKeys
     *               If true, autoincrement columns are copied from the archive object.
     *               If false, autoincrement columns are left intact.
      *
     * @return     ChildMot The current object (for fluent API support)
     */
    public function populateFromArchive($archive, $populateAutoIncrementPrimaryKeys = false) {
        if ($populateAutoIncrementPrimaryKeys) {
            $this->setIdMot($archive->getIdMot());
        }
        $this->setNom($archive->getNom());
        $this->setNomcourt($archive->getNomcourt());
        $this->setDescriptif($archive->getDescriptif());
        $this->setTexte($archive->getTexte());
        $this->setImportance($archive->getImportance());
        $this->setActif($archive->getActif());
        $this->setIdMotgroupe($archive->getIdMotgroupe());
        $this->setCreatedAt($archive->getCreatedAt());
        $this->setUpdatedAt($archive->getUpdatedAt());

        return $this;
    }

    /**
     * Removes the object from the database without archiving it.
     *
     * @param ConnectionInterface $con Optional connection object
     *
     * @return $this|ChildMot The current object (for fluent API support)
     */
    public function deleteWithoutArchive(ConnectionInterface $con = null)
    {
        $this->archiveOnDelete = false;

        return $this->delete($con);
    }

    /**
     * Code to be run before persisting the object
     * @param  ConnectionInterface $con
     * @return boolean
     */
    public function preSave(ConnectionInterface $con = null)
    {
        if (is_callable('parent::preSave')) {
            return parent::preSave($con);
        }
        return true;
    }

    /**
     * Code to be run after persisting the object
     * @param ConnectionInterface $con
     */
    public function postSave(ConnectionInterface $con = null)
    {
        if (is_callable('parent::postSave')) {
            parent::postSave($con);
        }
    }

    /**
     * Code to be run before inserting to database
     * @param  ConnectionInterface $con
     * @return boolean
     */
    public function preInsert(ConnectionInterface $con = null)
    {
        if (is_callable('parent::preInsert')) {
            return parent::preInsert($con);
        }
        return true;
    }

    /**
     * Code to be run after inserting to database
     * @param ConnectionInterface $con
     */
    public function postInsert(ConnectionInterface $con = null)
    {
        if (is_callable('parent::postInsert')) {
            parent::postInsert($con);
        }
    }

    /**
     * Code to be run before updating the object in database
     * @param  ConnectionInterface $con
     * @return boolean
     */
    public function preUpdate(ConnectionInterface $con = null)
    {
        if (is_callable('parent::preUpdate')) {
            return parent::preUpdate($con);
        }
        return true;
    }

    /**
     * Code to be run after updating the object in database
     * @param ConnectionInterface $con
     */
    public function postUpdate(ConnectionInterface $con = null)
    {
        if (is_callable('parent::postUpdate')) {
            parent::postUpdate($con);
        }
    }

    /**
     * Code to be run before deleting the object in database
     * @param  ConnectionInterface $con
     * @return boolean
     */
    public function preDelete(ConnectionInterface $con = null)
    {
        if (is_callable('parent::preDelete')) {
            return parent::preDelete($con);
        }
        return true;
    }

    /**
     * Code to be run after deleting the object in database
     * @param ConnectionInterface $con
     */
    public function postDelete(ConnectionInterface $con = null)
    {
        if (is_callable('parent::postDelete')) {
            parent::postDelete($con);
        }
    }


    /**
     * Derived method to catches calls to undefined methods.
     *
     * Provides magic import/export method support (fromXML()/toXML(), fromYAML()/toYAML(), etc.).
     * Allows to define default __call() behavior if you overwrite __call()
     *
     * @param string $name
     * @param mixed  $params
     *
     * @return array|string
     */
    public function __call($name, $params)
    {
        if (0 === strpos($name, 'get')) {
            $virtualColumn = substr($name, 3);
            if ($this->hasVirtualColumn($virtualColumn)) {
                return $this->getVirtualColumn($virtualColumn);
            }

            $virtualColumn = lcfirst($virtualColumn);
            if ($this->hasVirtualColumn($virtualColumn)) {
                return $this->getVirtualColumn($virtualColumn);
            }
        }

        if (0 === strpos($name, 'from')) {
            $format = substr($name, 4);

            return $this->importFrom($format, reset($params));
        }

        if (0 === strpos($name, 'to')) {
            $format = substr($name, 2);
            $includeLazyLoadColumns = isset($params[0]) ? $params[0] : true;

            return $this->exportTo($format, $includeLazyLoadColumns);
        }

        throw new BadMethodCallException(sprintf('Call to undefined method: %s.', $name));
    }

}
