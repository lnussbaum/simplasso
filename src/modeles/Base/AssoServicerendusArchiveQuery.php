<?php

namespace Base;

use \AssoServicerendusArchive as ChildAssoServicerendusArchive;
use \AssoServicerendusArchiveQuery as ChildAssoServicerendusArchiveQuery;
use \Exception;
use \PDO;
use Map\AssoServicerendusArchiveTableMap;
use Propel\Runtime\Propel;
use Propel\Runtime\ActiveQuery\Criteria;
use Propel\Runtime\ActiveQuery\ModelCriteria;
use Propel\Runtime\Collection\ObjectCollection;
use Propel\Runtime\Connection\ConnectionInterface;
use Propel\Runtime\Exception\PropelException;

/**
 * Base class that represents a query for the 'asso_servicerendus_archive' table.
 *
 *
 *
 * @method     ChildAssoServicerendusArchiveQuery orderByIdServicerendu($order = Criteria::ASC) Order by the id_servicerendu column
 * @method     ChildAssoServicerendusArchiveQuery orderByIdMembre($order = Criteria::ASC) Order by the id_membre column
 * @method     ChildAssoServicerendusArchiveQuery orderByMontant($order = Criteria::ASC) Order by the montant column
 * @method     ChildAssoServicerendusArchiveQuery orderByDateEnregistrement($order = Criteria::ASC) Order by the date_enregistrement column
 * @method     ChildAssoServicerendusArchiveQuery orderByDateDebut($order = Criteria::ASC) Order by the date_debut column
 * @method     ChildAssoServicerendusArchiveQuery orderByDateFin($order = Criteria::ASC) Order by the date_fin column
 * @method     ChildAssoServicerendusArchiveQuery orderByOrigine($order = Criteria::ASC) Order by the origine column
 * @method     ChildAssoServicerendusArchiveQuery orderByPremier($order = Criteria::ASC) Order by the premier column
 * @method     ChildAssoServicerendusArchiveQuery orderByDernier($order = Criteria::ASC) Order by the dernier column
 * @method     ChildAssoServicerendusArchiveQuery orderByIdEntite($order = Criteria::ASC) Order by the id_entite column
 * @method     ChildAssoServicerendusArchiveQuery orderByIdPrestation($order = Criteria::ASC) Order by the id_prestation column
 * @method     ChildAssoServicerendusArchiveQuery orderByRegle($order = Criteria::ASC) Order by the regle column
 * @method     ChildAssoServicerendusArchiveQuery orderByComptabilise($order = Criteria::ASC) Order by the comptabilise column
 * @method     ChildAssoServicerendusArchiveQuery orderByObservation($order = Criteria::ASC) Order by the observation column
 * @method     ChildAssoServicerendusArchiveQuery orderByIdTva($order = Criteria::ASC) Order by the id_tva column
 * @method     ChildAssoServicerendusArchiveQuery orderByQuantite($order = Criteria::ASC) Order by the quantite column
 * @method     ChildAssoServicerendusArchiveQuery orderByCreatedAt($order = Criteria::ASC) Order by the created_at column
 * @method     ChildAssoServicerendusArchiveQuery orderByUpdatedAt($order = Criteria::ASC) Order by the updated_at column
 * @method     ChildAssoServicerendusArchiveQuery orderByArchivedAt($order = Criteria::ASC) Order by the archived_at column
 *
 * @method     ChildAssoServicerendusArchiveQuery groupByIdServicerendu() Group by the id_servicerendu column
 * @method     ChildAssoServicerendusArchiveQuery groupByIdMembre() Group by the id_membre column
 * @method     ChildAssoServicerendusArchiveQuery groupByMontant() Group by the montant column
 * @method     ChildAssoServicerendusArchiveQuery groupByDateEnregistrement() Group by the date_enregistrement column
 * @method     ChildAssoServicerendusArchiveQuery groupByDateDebut() Group by the date_debut column
 * @method     ChildAssoServicerendusArchiveQuery groupByDateFin() Group by the date_fin column
 * @method     ChildAssoServicerendusArchiveQuery groupByOrigine() Group by the origine column
 * @method     ChildAssoServicerendusArchiveQuery groupByPremier() Group by the premier column
 * @method     ChildAssoServicerendusArchiveQuery groupByDernier() Group by the dernier column
 * @method     ChildAssoServicerendusArchiveQuery groupByIdEntite() Group by the id_entite column
 * @method     ChildAssoServicerendusArchiveQuery groupByIdPrestation() Group by the id_prestation column
 * @method     ChildAssoServicerendusArchiveQuery groupByRegle() Group by the regle column
 * @method     ChildAssoServicerendusArchiveQuery groupByComptabilise() Group by the comptabilise column
 * @method     ChildAssoServicerendusArchiveQuery groupByObservation() Group by the observation column
 * @method     ChildAssoServicerendusArchiveQuery groupByIdTva() Group by the id_tva column
 * @method     ChildAssoServicerendusArchiveQuery groupByQuantite() Group by the quantite column
 * @method     ChildAssoServicerendusArchiveQuery groupByCreatedAt() Group by the created_at column
 * @method     ChildAssoServicerendusArchiveQuery groupByUpdatedAt() Group by the updated_at column
 * @method     ChildAssoServicerendusArchiveQuery groupByArchivedAt() Group by the archived_at column
 *
 * @method     ChildAssoServicerendusArchiveQuery leftJoin($relation) Adds a LEFT JOIN clause to the query
 * @method     ChildAssoServicerendusArchiveQuery rightJoin($relation) Adds a RIGHT JOIN clause to the query
 * @method     ChildAssoServicerendusArchiveQuery innerJoin($relation) Adds a INNER JOIN clause to the query
 *
 * @method     ChildAssoServicerendusArchiveQuery leftJoinWith($relation) Adds a LEFT JOIN clause and with to the query
 * @method     ChildAssoServicerendusArchiveQuery rightJoinWith($relation) Adds a RIGHT JOIN clause and with to the query
 * @method     ChildAssoServicerendusArchiveQuery innerJoinWith($relation) Adds a INNER JOIN clause and with to the query
 *
 * @method     ChildAssoServicerendusArchive findOne(ConnectionInterface $con = null) Return the first ChildAssoServicerendusArchive matching the query
 * @method     ChildAssoServicerendusArchive findOneOrCreate(ConnectionInterface $con = null) Return the first ChildAssoServicerendusArchive matching the query, or a new ChildAssoServicerendusArchive object populated from the query conditions when no match is found
 *
 * @method     ChildAssoServicerendusArchive findOneByIdServicerendu(string $id_servicerendu) Return the first ChildAssoServicerendusArchive filtered by the id_servicerendu column
 * @method     ChildAssoServicerendusArchive findOneByIdMembre(string $id_membre) Return the first ChildAssoServicerendusArchive filtered by the id_membre column
 * @method     ChildAssoServicerendusArchive findOneByMontant(double $montant) Return the first ChildAssoServicerendusArchive filtered by the montant column
 * @method     ChildAssoServicerendusArchive findOneByDateEnregistrement(string $date_enregistrement) Return the first ChildAssoServicerendusArchive filtered by the date_enregistrement column
 * @method     ChildAssoServicerendusArchive findOneByDateDebut(string $date_debut) Return the first ChildAssoServicerendusArchive filtered by the date_debut column
 * @method     ChildAssoServicerendusArchive findOneByDateFin(string $date_fin) Return the first ChildAssoServicerendusArchive filtered by the date_fin column
 * @method     ChildAssoServicerendusArchive findOneByOrigine(int $origine) Return the first ChildAssoServicerendusArchive filtered by the origine column
 * @method     ChildAssoServicerendusArchive findOneByPremier(int $premier) Return the first ChildAssoServicerendusArchive filtered by the premier column
 * @method     ChildAssoServicerendusArchive findOneByDernier(int $dernier) Return the first ChildAssoServicerendusArchive filtered by the dernier column
 * @method     ChildAssoServicerendusArchive findOneByIdEntite(int $id_entite) Return the first ChildAssoServicerendusArchive filtered by the id_entite column
 * @method     ChildAssoServicerendusArchive findOneByIdPrestation(string $id_prestation) Return the first ChildAssoServicerendusArchive filtered by the id_prestation column
 * @method     ChildAssoServicerendusArchive findOneByRegle(int $regle) Return the first ChildAssoServicerendusArchive filtered by the regle column
 * @method     ChildAssoServicerendusArchive findOneByComptabilise(boolean $comptabilise) Return the first ChildAssoServicerendusArchive filtered by the comptabilise column
 * @method     ChildAssoServicerendusArchive findOneByObservation(string $observation) Return the first ChildAssoServicerendusArchive filtered by the observation column
 * @method     ChildAssoServicerendusArchive findOneByIdTva(string $id_tva) Return the first ChildAssoServicerendusArchive filtered by the id_tva column
 * @method     ChildAssoServicerendusArchive findOneByQuantite(double $quantite) Return the first ChildAssoServicerendusArchive filtered by the quantite column
 * @method     ChildAssoServicerendusArchive findOneByCreatedAt(string $created_at) Return the first ChildAssoServicerendusArchive filtered by the created_at column
 * @method     ChildAssoServicerendusArchive findOneByUpdatedAt(string $updated_at) Return the first ChildAssoServicerendusArchive filtered by the updated_at column
 * @method     ChildAssoServicerendusArchive findOneByArchivedAt(string $archived_at) Return the first ChildAssoServicerendusArchive filtered by the archived_at column *

 * @method     ChildAssoServicerendusArchive requirePk($key, ConnectionInterface $con = null) Return the ChildAssoServicerendusArchive by primary key and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildAssoServicerendusArchive requireOne(ConnectionInterface $con = null) Return the first ChildAssoServicerendusArchive matching the query and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 *
 * @method     ChildAssoServicerendusArchive requireOneByIdServicerendu(string $id_servicerendu) Return the first ChildAssoServicerendusArchive filtered by the id_servicerendu column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildAssoServicerendusArchive requireOneByIdMembre(string $id_membre) Return the first ChildAssoServicerendusArchive filtered by the id_membre column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildAssoServicerendusArchive requireOneByMontant(double $montant) Return the first ChildAssoServicerendusArchive filtered by the montant column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildAssoServicerendusArchive requireOneByDateEnregistrement(string $date_enregistrement) Return the first ChildAssoServicerendusArchive filtered by the date_enregistrement column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildAssoServicerendusArchive requireOneByDateDebut(string $date_debut) Return the first ChildAssoServicerendusArchive filtered by the date_debut column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildAssoServicerendusArchive requireOneByDateFin(string $date_fin) Return the first ChildAssoServicerendusArchive filtered by the date_fin column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildAssoServicerendusArchive requireOneByOrigine(int $origine) Return the first ChildAssoServicerendusArchive filtered by the origine column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildAssoServicerendusArchive requireOneByPremier(int $premier) Return the first ChildAssoServicerendusArchive filtered by the premier column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildAssoServicerendusArchive requireOneByDernier(int $dernier) Return the first ChildAssoServicerendusArchive filtered by the dernier column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildAssoServicerendusArchive requireOneByIdEntite(int $id_entite) Return the first ChildAssoServicerendusArchive filtered by the id_entite column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildAssoServicerendusArchive requireOneByIdPrestation(string $id_prestation) Return the first ChildAssoServicerendusArchive filtered by the id_prestation column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildAssoServicerendusArchive requireOneByRegle(int $regle) Return the first ChildAssoServicerendusArchive filtered by the regle column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildAssoServicerendusArchive requireOneByComptabilise(boolean $comptabilise) Return the first ChildAssoServicerendusArchive filtered by the comptabilise column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildAssoServicerendusArchive requireOneByObservation(string $observation) Return the first ChildAssoServicerendusArchive filtered by the observation column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildAssoServicerendusArchive requireOneByIdTva(string $id_tva) Return the first ChildAssoServicerendusArchive filtered by the id_tva column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildAssoServicerendusArchive requireOneByQuantite(double $quantite) Return the first ChildAssoServicerendusArchive filtered by the quantite column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildAssoServicerendusArchive requireOneByCreatedAt(string $created_at) Return the first ChildAssoServicerendusArchive filtered by the created_at column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildAssoServicerendusArchive requireOneByUpdatedAt(string $updated_at) Return the first ChildAssoServicerendusArchive filtered by the updated_at column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildAssoServicerendusArchive requireOneByArchivedAt(string $archived_at) Return the first ChildAssoServicerendusArchive filtered by the archived_at column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 *
 * @method     ChildAssoServicerendusArchive[]|ObjectCollection find(ConnectionInterface $con = null) Return ChildAssoServicerendusArchive objects based on current ModelCriteria
 * @method     ChildAssoServicerendusArchive[]|ObjectCollection findByIdServicerendu(string $id_servicerendu) Return ChildAssoServicerendusArchive objects filtered by the id_servicerendu column
 * @method     ChildAssoServicerendusArchive[]|ObjectCollection findByIdMembre(string $id_membre) Return ChildAssoServicerendusArchive objects filtered by the id_membre column
 * @method     ChildAssoServicerendusArchive[]|ObjectCollection findByMontant(double $montant) Return ChildAssoServicerendusArchive objects filtered by the montant column
 * @method     ChildAssoServicerendusArchive[]|ObjectCollection findByDateEnregistrement(string $date_enregistrement) Return ChildAssoServicerendusArchive objects filtered by the date_enregistrement column
 * @method     ChildAssoServicerendusArchive[]|ObjectCollection findByDateDebut(string $date_debut) Return ChildAssoServicerendusArchive objects filtered by the date_debut column
 * @method     ChildAssoServicerendusArchive[]|ObjectCollection findByDateFin(string $date_fin) Return ChildAssoServicerendusArchive objects filtered by the date_fin column
 * @method     ChildAssoServicerendusArchive[]|ObjectCollection findByOrigine(int $origine) Return ChildAssoServicerendusArchive objects filtered by the origine column
 * @method     ChildAssoServicerendusArchive[]|ObjectCollection findByPremier(int $premier) Return ChildAssoServicerendusArchive objects filtered by the premier column
 * @method     ChildAssoServicerendusArchive[]|ObjectCollection findByDernier(int $dernier) Return ChildAssoServicerendusArchive objects filtered by the dernier column
 * @method     ChildAssoServicerendusArchive[]|ObjectCollection findByIdEntite(int $id_entite) Return ChildAssoServicerendusArchive objects filtered by the id_entite column
 * @method     ChildAssoServicerendusArchive[]|ObjectCollection findByIdPrestation(string $id_prestation) Return ChildAssoServicerendusArchive objects filtered by the id_prestation column
 * @method     ChildAssoServicerendusArchive[]|ObjectCollection findByRegle(int $regle) Return ChildAssoServicerendusArchive objects filtered by the regle column
 * @method     ChildAssoServicerendusArchive[]|ObjectCollection findByComptabilise(boolean $comptabilise) Return ChildAssoServicerendusArchive objects filtered by the comptabilise column
 * @method     ChildAssoServicerendusArchive[]|ObjectCollection findByObservation(string $observation) Return ChildAssoServicerendusArchive objects filtered by the observation column
 * @method     ChildAssoServicerendusArchive[]|ObjectCollection findByIdTva(string $id_tva) Return ChildAssoServicerendusArchive objects filtered by the id_tva column
 * @method     ChildAssoServicerendusArchive[]|ObjectCollection findByQuantite(double $quantite) Return ChildAssoServicerendusArchive objects filtered by the quantite column
 * @method     ChildAssoServicerendusArchive[]|ObjectCollection findByCreatedAt(string $created_at) Return ChildAssoServicerendusArchive objects filtered by the created_at column
 * @method     ChildAssoServicerendusArchive[]|ObjectCollection findByUpdatedAt(string $updated_at) Return ChildAssoServicerendusArchive objects filtered by the updated_at column
 * @method     ChildAssoServicerendusArchive[]|ObjectCollection findByArchivedAt(string $archived_at) Return ChildAssoServicerendusArchive objects filtered by the archived_at column
 * @method     ChildAssoServicerendusArchive[]|\Propel\Runtime\Util\PropelModelPager paginate($page = 1, $maxPerPage = 10, ConnectionInterface $con = null) Issue a SELECT query based on the current ModelCriteria and uses a page and a maximum number of results per page to compute an offset and a limit
 *
 */
abstract class AssoServicerendusArchiveQuery extends ModelCriteria
{
    protected $entityNotFoundExceptionClass = '\\Propel\\Runtime\\Exception\\EntityNotFoundException';

    /**
     * Initializes internal state of \Base\AssoServicerendusArchiveQuery object.
     *
     * @param     string $dbName The database name
     * @param     string $modelName The phpName of a model, e.g. 'Book'
     * @param     string $modelAlias The alias for the model in this query, e.g. 'b'
     */
    public function __construct($dbName = 'simplasso', $modelName = '\\AssoServicerendusArchive', $modelAlias = null)
    {
        parent::__construct($dbName, $modelName, $modelAlias);
    }

    /**
     * Returns a new ChildAssoServicerendusArchiveQuery object.
     *
     * @param     string $modelAlias The alias of a model in the query
     * @param     Criteria $criteria Optional Criteria to build the query from
     *
     * @return ChildAssoServicerendusArchiveQuery
     */
    public static function create($modelAlias = null, Criteria $criteria = null)
    {
        if ($criteria instanceof ChildAssoServicerendusArchiveQuery) {
            return $criteria;
        }
        $query = new ChildAssoServicerendusArchiveQuery();
        if (null !== $modelAlias) {
            $query->setModelAlias($modelAlias);
        }
        if ($criteria instanceof Criteria) {
            $query->mergeWith($criteria);
        }

        return $query;
    }

    /**
     * Find object by primary key.
     * Propel uses the instance pool to skip the database if the object exists.
     * Go fast if the query is untouched.
     *
     * <code>
     * $obj  = $c->findPk(12, $con);
     * </code>
     *
     * @param mixed $key Primary key to use for the query
     * @param ConnectionInterface $con an optional connection object
     *
     * @return ChildAssoServicerendusArchive|array|mixed the result, formatted by the current formatter
     */
    public function findPk($key, ConnectionInterface $con = null)
    {
        if ($key === null) {
            return null;
        }

        if ($con === null) {
            $con = Propel::getServiceContainer()->getReadConnection(AssoServicerendusArchiveTableMap::DATABASE_NAME);
        }

        $this->basePreSelect($con);

        if (
            $this->formatter || $this->modelAlias || $this->with || $this->select
            || $this->selectColumns || $this->asColumns || $this->selectModifiers
            || $this->map || $this->having || $this->joins
        ) {
            return $this->findPkComplex($key, $con);
        }

        if ((null !== ($obj = AssoServicerendusArchiveTableMap::getInstanceFromPool(null === $key || is_scalar($key) || is_callable([$key, '__toString']) ? (string) $key : $key)))) {
            // the object is already in the instance pool
            return $obj;
        }

        return $this->findPkSimple($key, $con);
    }

    /**
     * Find object by primary key using raw SQL to go fast.
     * Bypass doSelect() and the object formatter by using generated code.
     *
     * @param     mixed $key Primary key to use for the query
     * @param     ConnectionInterface $con A connection object
     *
     * @throws \Propel\Runtime\Exception\PropelException
     *
     * @return ChildAssoServicerendusArchive A model object, or null if the key is not found
     */
    protected function findPkSimple($key, ConnectionInterface $con)
    {
        $sql = 'SELECT `id_servicerendu`, `id_membre`, `montant`, `date_enregistrement`, `date_debut`, `date_fin`, `origine`, `premier`, `dernier`, `id_entite`, `id_prestation`, `regle`, `comptabilise`, `observation`, `id_tva`, `quantite`, `created_at`, `updated_at`, `archived_at` FROM `asso_servicerendus_archive` WHERE `id_servicerendu` = :p0';
        try {
            $stmt = $con->prepare($sql);
            $stmt->bindValue(':p0', $key, PDO::PARAM_INT);
            $stmt->execute();
        } catch (Exception $e) {
            Propel::log($e->getMessage(), Propel::LOG_ERR);
            throw new PropelException(sprintf('Unable to execute SELECT statement [%s]', $sql), 0, $e);
        }
        $obj = null;
        if ($row = $stmt->fetch(\PDO::FETCH_NUM)) {
            /** @var ChildAssoServicerendusArchive $obj */
            $obj = new ChildAssoServicerendusArchive();
            $obj->hydrate($row);
            AssoServicerendusArchiveTableMap::addInstanceToPool($obj, null === $key || is_scalar($key) || is_callable([$key, '__toString']) ? (string) $key : $key);
        }
        $stmt->closeCursor();

        return $obj;
    }

    /**
     * Find object by primary key.
     *
     * @param     mixed $key Primary key to use for the query
     * @param     ConnectionInterface $con A connection object
     *
     * @return ChildAssoServicerendusArchive|array|mixed the result, formatted by the current formatter
     */
    protected function findPkComplex($key, ConnectionInterface $con)
    {
        // As the query uses a PK condition, no limit(1) is necessary.
        $criteria = $this->isKeepQuery() ? clone $this : $this;
        $dataFetcher = $criteria
            ->filterByPrimaryKey($key)
            ->doSelect($con);

        return $criteria->getFormatter()->init($criteria)->formatOne($dataFetcher);
    }

    /**
     * Find objects by primary key
     * <code>
     * $objs = $c->findPks(array(12, 56, 832), $con);
     * </code>
     * @param     array $keys Primary keys to use for the query
     * @param     ConnectionInterface $con an optional connection object
     *
     * @return ObjectCollection|array|mixed the list of results, formatted by the current formatter
     */
    public function findPks($keys, ConnectionInterface $con = null)
    {
        if (null === $con) {
            $con = Propel::getServiceContainer()->getReadConnection($this->getDbName());
        }
        $this->basePreSelect($con);
        $criteria = $this->isKeepQuery() ? clone $this : $this;
        $dataFetcher = $criteria
            ->filterByPrimaryKeys($keys)
            ->doSelect($con);

        return $criteria->getFormatter()->init($criteria)->format($dataFetcher);
    }

    /**
     * Filter the query by primary key
     *
     * @param     mixed $key Primary key to use for the query
     *
     * @return $this|ChildAssoServicerendusArchiveQuery The current query, for fluid interface
     */
    public function filterByPrimaryKey($key)
    {

        return $this->addUsingAlias(AssoServicerendusArchiveTableMap::COL_ID_SERVICERENDU, $key, Criteria::EQUAL);
    }

    /**
     * Filter the query by a list of primary keys
     *
     * @param     array $keys The list of primary key to use for the query
     *
     * @return $this|ChildAssoServicerendusArchiveQuery The current query, for fluid interface
     */
    public function filterByPrimaryKeys($keys)
    {

        return $this->addUsingAlias(AssoServicerendusArchiveTableMap::COL_ID_SERVICERENDU, $keys, Criteria::IN);
    }

    /**
     * Filter the query on the id_servicerendu column
     *
     * Example usage:
     * <code>
     * $query->filterByIdServicerendu(1234); // WHERE id_servicerendu = 1234
     * $query->filterByIdServicerendu(array(12, 34)); // WHERE id_servicerendu IN (12, 34)
     * $query->filterByIdServicerendu(array('min' => 12)); // WHERE id_servicerendu > 12
     * </code>
     *
     * @param     mixed $idServicerendu The value to use as filter.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildAssoServicerendusArchiveQuery The current query, for fluid interface
     */
    public function filterByIdServicerendu($idServicerendu = null, $comparison = null)
    {
        if (is_array($idServicerendu)) {
            $useMinMax = false;
            if (isset($idServicerendu['min'])) {
                $this->addUsingAlias(AssoServicerendusArchiveTableMap::COL_ID_SERVICERENDU, $idServicerendu['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($idServicerendu['max'])) {
                $this->addUsingAlias(AssoServicerendusArchiveTableMap::COL_ID_SERVICERENDU, $idServicerendu['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(AssoServicerendusArchiveTableMap::COL_ID_SERVICERENDU, $idServicerendu, $comparison);
    }

    /**
     * Filter the query on the id_membre column
     *
     * Example usage:
     * <code>
     * $query->filterByIdMembre(1234); // WHERE id_membre = 1234
     * $query->filterByIdMembre(array(12, 34)); // WHERE id_membre IN (12, 34)
     * $query->filterByIdMembre(array('min' => 12)); // WHERE id_membre > 12
     * </code>
     *
     * @param     mixed $idMembre The value to use as filter.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildAssoServicerendusArchiveQuery The current query, for fluid interface
     */
    public function filterByIdMembre($idMembre = null, $comparison = null)
    {
        if (is_array($idMembre)) {
            $useMinMax = false;
            if (isset($idMembre['min'])) {
                $this->addUsingAlias(AssoServicerendusArchiveTableMap::COL_ID_MEMBRE, $idMembre['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($idMembre['max'])) {
                $this->addUsingAlias(AssoServicerendusArchiveTableMap::COL_ID_MEMBRE, $idMembre['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(AssoServicerendusArchiveTableMap::COL_ID_MEMBRE, $idMembre, $comparison);
    }

    /**
     * Filter the query on the montant column
     *
     * Example usage:
     * <code>
     * $query->filterByMontant(1234); // WHERE montant = 1234
     * $query->filterByMontant(array(12, 34)); // WHERE montant IN (12, 34)
     * $query->filterByMontant(array('min' => 12)); // WHERE montant > 12
     * </code>
     *
     * @param     mixed $montant The value to use as filter.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildAssoServicerendusArchiveQuery The current query, for fluid interface
     */
    public function filterByMontant($montant = null, $comparison = null)
    {
        if (is_array($montant)) {
            $useMinMax = false;
            if (isset($montant['min'])) {
                $this->addUsingAlias(AssoServicerendusArchiveTableMap::COL_MONTANT, $montant['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($montant['max'])) {
                $this->addUsingAlias(AssoServicerendusArchiveTableMap::COL_MONTANT, $montant['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(AssoServicerendusArchiveTableMap::COL_MONTANT, $montant, $comparison);
    }

    /**
     * Filter the query on the date_enregistrement column
     *
     * Example usage:
     * <code>
     * $query->filterByDateEnregistrement('2011-03-14'); // WHERE date_enregistrement = '2011-03-14'
     * $query->filterByDateEnregistrement('now'); // WHERE date_enregistrement = '2011-03-14'
     * $query->filterByDateEnregistrement(array('max' => 'yesterday')); // WHERE date_enregistrement > '2011-03-13'
     * </code>
     *
     * @param     mixed $dateEnregistrement The value to use as filter.
     *              Values can be integers (unix timestamps), DateTime objects, or strings.
     *              Empty strings are treated as NULL.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildAssoServicerendusArchiveQuery The current query, for fluid interface
     */
    public function filterByDateEnregistrement($dateEnregistrement = null, $comparison = null)
    {
        if (is_array($dateEnregistrement)) {
            $useMinMax = false;
            if (isset($dateEnregistrement['min'])) {
                $this->addUsingAlias(AssoServicerendusArchiveTableMap::COL_DATE_ENREGISTREMENT, $dateEnregistrement['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($dateEnregistrement['max'])) {
                $this->addUsingAlias(AssoServicerendusArchiveTableMap::COL_DATE_ENREGISTREMENT, $dateEnregistrement['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(AssoServicerendusArchiveTableMap::COL_DATE_ENREGISTREMENT, $dateEnregistrement, $comparison);
    }

    /**
     * Filter the query on the date_debut column
     *
     * Example usage:
     * <code>
     * $query->filterByDateDebut('2011-03-14'); // WHERE date_debut = '2011-03-14'
     * $query->filterByDateDebut('now'); // WHERE date_debut = '2011-03-14'
     * $query->filterByDateDebut(array('max' => 'yesterday')); // WHERE date_debut > '2011-03-13'
     * </code>
     *
     * @param     mixed $dateDebut The value to use as filter.
     *              Values can be integers (unix timestamps), DateTime objects, or strings.
     *              Empty strings are treated as NULL.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildAssoServicerendusArchiveQuery The current query, for fluid interface
     */
    public function filterByDateDebut($dateDebut = null, $comparison = null)
    {
        if (is_array($dateDebut)) {
            $useMinMax = false;
            if (isset($dateDebut['min'])) {
                $this->addUsingAlias(AssoServicerendusArchiveTableMap::COL_DATE_DEBUT, $dateDebut['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($dateDebut['max'])) {
                $this->addUsingAlias(AssoServicerendusArchiveTableMap::COL_DATE_DEBUT, $dateDebut['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(AssoServicerendusArchiveTableMap::COL_DATE_DEBUT, $dateDebut, $comparison);
    }

    /**
     * Filter the query on the date_fin column
     *
     * Example usage:
     * <code>
     * $query->filterByDateFin('2011-03-14'); // WHERE date_fin = '2011-03-14'
     * $query->filterByDateFin('now'); // WHERE date_fin = '2011-03-14'
     * $query->filterByDateFin(array('max' => 'yesterday')); // WHERE date_fin > '2011-03-13'
     * </code>
     *
     * @param     mixed $dateFin The value to use as filter.
     *              Values can be integers (unix timestamps), DateTime objects, or strings.
     *              Empty strings are treated as NULL.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildAssoServicerendusArchiveQuery The current query, for fluid interface
     */
    public function filterByDateFin($dateFin = null, $comparison = null)
    {
        if (is_array($dateFin)) {
            $useMinMax = false;
            if (isset($dateFin['min'])) {
                $this->addUsingAlias(AssoServicerendusArchiveTableMap::COL_DATE_FIN, $dateFin['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($dateFin['max'])) {
                $this->addUsingAlias(AssoServicerendusArchiveTableMap::COL_DATE_FIN, $dateFin['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(AssoServicerendusArchiveTableMap::COL_DATE_FIN, $dateFin, $comparison);
    }

    /**
     * Filter the query on the origine column
     *
     * Example usage:
     * <code>
     * $query->filterByOrigine(1234); // WHERE origine = 1234
     * $query->filterByOrigine(array(12, 34)); // WHERE origine IN (12, 34)
     * $query->filterByOrigine(array('min' => 12)); // WHERE origine > 12
     * </code>
     *
     * @param     mixed $origine The value to use as filter.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildAssoServicerendusArchiveQuery The current query, for fluid interface
     */
    public function filterByOrigine($origine = null, $comparison = null)
    {
        if (is_array($origine)) {
            $useMinMax = false;
            if (isset($origine['min'])) {
                $this->addUsingAlias(AssoServicerendusArchiveTableMap::COL_ORIGINE, $origine['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($origine['max'])) {
                $this->addUsingAlias(AssoServicerendusArchiveTableMap::COL_ORIGINE, $origine['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(AssoServicerendusArchiveTableMap::COL_ORIGINE, $origine, $comparison);
    }

    /**
     * Filter the query on the premier column
     *
     * Example usage:
     * <code>
     * $query->filterByPremier(1234); // WHERE premier = 1234
     * $query->filterByPremier(array(12, 34)); // WHERE premier IN (12, 34)
     * $query->filterByPremier(array('min' => 12)); // WHERE premier > 12
     * </code>
     *
     * @param     mixed $premier The value to use as filter.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildAssoServicerendusArchiveQuery The current query, for fluid interface
     */
    public function filterByPremier($premier = null, $comparison = null)
    {
        if (is_array($premier)) {
            $useMinMax = false;
            if (isset($premier['min'])) {
                $this->addUsingAlias(AssoServicerendusArchiveTableMap::COL_PREMIER, $premier['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($premier['max'])) {
                $this->addUsingAlias(AssoServicerendusArchiveTableMap::COL_PREMIER, $premier['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(AssoServicerendusArchiveTableMap::COL_PREMIER, $premier, $comparison);
    }

    /**
     * Filter the query on the dernier column
     *
     * Example usage:
     * <code>
     * $query->filterByDernier(1234); // WHERE dernier = 1234
     * $query->filterByDernier(array(12, 34)); // WHERE dernier IN (12, 34)
     * $query->filterByDernier(array('min' => 12)); // WHERE dernier > 12
     * </code>
     *
     * @param     mixed $dernier The value to use as filter.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildAssoServicerendusArchiveQuery The current query, for fluid interface
     */
    public function filterByDernier($dernier = null, $comparison = null)
    {
        if (is_array($dernier)) {
            $useMinMax = false;
            if (isset($dernier['min'])) {
                $this->addUsingAlias(AssoServicerendusArchiveTableMap::COL_DERNIER, $dernier['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($dernier['max'])) {
                $this->addUsingAlias(AssoServicerendusArchiveTableMap::COL_DERNIER, $dernier['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(AssoServicerendusArchiveTableMap::COL_DERNIER, $dernier, $comparison);
    }

    /**
     * Filter the query on the id_entite column
     *
     * Example usage:
     * <code>
     * $query->filterByIdEntite(1234); // WHERE id_entite = 1234
     * $query->filterByIdEntite(array(12, 34)); // WHERE id_entite IN (12, 34)
     * $query->filterByIdEntite(array('min' => 12)); // WHERE id_entite > 12
     * </code>
     *
     * @param     mixed $idEntite The value to use as filter.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildAssoServicerendusArchiveQuery The current query, for fluid interface
     */
    public function filterByIdEntite($idEntite = null, $comparison = null)
    {
        if (is_array($idEntite)) {
            $useMinMax = false;
            if (isset($idEntite['min'])) {
                $this->addUsingAlias(AssoServicerendusArchiveTableMap::COL_ID_ENTITE, $idEntite['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($idEntite['max'])) {
                $this->addUsingAlias(AssoServicerendusArchiveTableMap::COL_ID_ENTITE, $idEntite['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(AssoServicerendusArchiveTableMap::COL_ID_ENTITE, $idEntite, $comparison);
    }

    /**
     * Filter the query on the id_prestation column
     *
     * Example usage:
     * <code>
     * $query->filterByIdPrestation(1234); // WHERE id_prestation = 1234
     * $query->filterByIdPrestation(array(12, 34)); // WHERE id_prestation IN (12, 34)
     * $query->filterByIdPrestation(array('min' => 12)); // WHERE id_prestation > 12
     * </code>
     *
     * @param     mixed $idPrestation The value to use as filter.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildAssoServicerendusArchiveQuery The current query, for fluid interface
     */
    public function filterByIdPrestation($idPrestation = null, $comparison = null)
    {
        if (is_array($idPrestation)) {
            $useMinMax = false;
            if (isset($idPrestation['min'])) {
                $this->addUsingAlias(AssoServicerendusArchiveTableMap::COL_ID_PRESTATION, $idPrestation['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($idPrestation['max'])) {
                $this->addUsingAlias(AssoServicerendusArchiveTableMap::COL_ID_PRESTATION, $idPrestation['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(AssoServicerendusArchiveTableMap::COL_ID_PRESTATION, $idPrestation, $comparison);
    }

    /**
     * Filter the query on the regle column
     *
     * Example usage:
     * <code>
     * $query->filterByRegle(1234); // WHERE regle = 1234
     * $query->filterByRegle(array(12, 34)); // WHERE regle IN (12, 34)
     * $query->filterByRegle(array('min' => 12)); // WHERE regle > 12
     * </code>
     *
     * @param     mixed $regle The value to use as filter.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildAssoServicerendusArchiveQuery The current query, for fluid interface
     */
    public function filterByRegle($regle = null, $comparison = null)
    {
        if (is_array($regle)) {
            $useMinMax = false;
            if (isset($regle['min'])) {
                $this->addUsingAlias(AssoServicerendusArchiveTableMap::COL_REGLE, $regle['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($regle['max'])) {
                $this->addUsingAlias(AssoServicerendusArchiveTableMap::COL_REGLE, $regle['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(AssoServicerendusArchiveTableMap::COL_REGLE, $regle, $comparison);
    }

    /**
     * Filter the query on the comptabilise column
     *
     * Example usage:
     * <code>
     * $query->filterByComptabilise(true); // WHERE comptabilise = true
     * $query->filterByComptabilise('yes'); // WHERE comptabilise = true
     * </code>
     *
     * @param     boolean|string $comptabilise The value to use as filter.
     *              Non-boolean arguments are converted using the following rules:
     *                * 1, '1', 'true',  'on',  and 'yes' are converted to boolean true
     *                * 0, '0', 'false', 'off', and 'no'  are converted to boolean false
     *              Check on string values is case insensitive (so 'FaLsE' is seen as 'false').
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildAssoServicerendusArchiveQuery The current query, for fluid interface
     */
    public function filterByComptabilise($comptabilise = null, $comparison = null)
    {
        if (is_string($comptabilise)) {
            $comptabilise = in_array(strtolower($comptabilise), array('false', 'off', '-', 'no', 'n', '0', '')) ? false : true;
        }

        return $this->addUsingAlias(AssoServicerendusArchiveTableMap::COL_COMPTABILISE, $comptabilise, $comparison);
    }

    /**
     * Filter the query on the observation column
     *
     * Example usage:
     * <code>
     * $query->filterByObservation('fooValue');   // WHERE observation = 'fooValue'
     * $query->filterByObservation('%fooValue%', Criteria::LIKE); // WHERE observation LIKE '%fooValue%'
     * </code>
     *
     * @param     string $observation The value to use as filter.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildAssoServicerendusArchiveQuery The current query, for fluid interface
     */
    public function filterByObservation($observation = null, $comparison = null)
    {
        if (null === $comparison) {
            if (is_array($observation)) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(AssoServicerendusArchiveTableMap::COL_OBSERVATION, $observation, $comparison);
    }

    /**
     * Filter the query on the id_tva column
     *
     * Example usage:
     * <code>
     * $query->filterByIdTva(1234); // WHERE id_tva = 1234
     * $query->filterByIdTva(array(12, 34)); // WHERE id_tva IN (12, 34)
     * $query->filterByIdTva(array('min' => 12)); // WHERE id_tva > 12
     * </code>
     *
     * @param     mixed $idTva The value to use as filter.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildAssoServicerendusArchiveQuery The current query, for fluid interface
     */
    public function filterByIdTva($idTva = null, $comparison = null)
    {
        if (is_array($idTva)) {
            $useMinMax = false;
            if (isset($idTva['min'])) {
                $this->addUsingAlias(AssoServicerendusArchiveTableMap::COL_ID_TVA, $idTva['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($idTva['max'])) {
                $this->addUsingAlias(AssoServicerendusArchiveTableMap::COL_ID_TVA, $idTva['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(AssoServicerendusArchiveTableMap::COL_ID_TVA, $idTva, $comparison);
    }

    /**
     * Filter the query on the quantite column
     *
     * Example usage:
     * <code>
     * $query->filterByQuantite(1234); // WHERE quantite = 1234
     * $query->filterByQuantite(array(12, 34)); // WHERE quantite IN (12, 34)
     * $query->filterByQuantite(array('min' => 12)); // WHERE quantite > 12
     * </code>
     *
     * @param     mixed $quantite The value to use as filter.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildAssoServicerendusArchiveQuery The current query, for fluid interface
     */
    public function filterByQuantite($quantite = null, $comparison = null)
    {
        if (is_array($quantite)) {
            $useMinMax = false;
            if (isset($quantite['min'])) {
                $this->addUsingAlias(AssoServicerendusArchiveTableMap::COL_QUANTITE, $quantite['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($quantite['max'])) {
                $this->addUsingAlias(AssoServicerendusArchiveTableMap::COL_QUANTITE, $quantite['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(AssoServicerendusArchiveTableMap::COL_QUANTITE, $quantite, $comparison);
    }

    /**
     * Filter the query on the created_at column
     *
     * Example usage:
     * <code>
     * $query->filterByCreatedAt('2011-03-14'); // WHERE created_at = '2011-03-14'
     * $query->filterByCreatedAt('now'); // WHERE created_at = '2011-03-14'
     * $query->filterByCreatedAt(array('max' => 'yesterday')); // WHERE created_at > '2011-03-13'
     * </code>
     *
     * @param     mixed $createdAt The value to use as filter.
     *              Values can be integers (unix timestamps), DateTime objects, or strings.
     *              Empty strings are treated as NULL.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildAssoServicerendusArchiveQuery The current query, for fluid interface
     */
    public function filterByCreatedAt($createdAt = null, $comparison = null)
    {
        if (is_array($createdAt)) {
            $useMinMax = false;
            if (isset($createdAt['min'])) {
                $this->addUsingAlias(AssoServicerendusArchiveTableMap::COL_CREATED_AT, $createdAt['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($createdAt['max'])) {
                $this->addUsingAlias(AssoServicerendusArchiveTableMap::COL_CREATED_AT, $createdAt['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(AssoServicerendusArchiveTableMap::COL_CREATED_AT, $createdAt, $comparison);
    }

    /**
     * Filter the query on the updated_at column
     *
     * Example usage:
     * <code>
     * $query->filterByUpdatedAt('2011-03-14'); // WHERE updated_at = '2011-03-14'
     * $query->filterByUpdatedAt('now'); // WHERE updated_at = '2011-03-14'
     * $query->filterByUpdatedAt(array('max' => 'yesterday')); // WHERE updated_at > '2011-03-13'
     * </code>
     *
     * @param     mixed $updatedAt The value to use as filter.
     *              Values can be integers (unix timestamps), DateTime objects, or strings.
     *              Empty strings are treated as NULL.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildAssoServicerendusArchiveQuery The current query, for fluid interface
     */
    public function filterByUpdatedAt($updatedAt = null, $comparison = null)
    {
        if (is_array($updatedAt)) {
            $useMinMax = false;
            if (isset($updatedAt['min'])) {
                $this->addUsingAlias(AssoServicerendusArchiveTableMap::COL_UPDATED_AT, $updatedAt['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($updatedAt['max'])) {
                $this->addUsingAlias(AssoServicerendusArchiveTableMap::COL_UPDATED_AT, $updatedAt['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(AssoServicerendusArchiveTableMap::COL_UPDATED_AT, $updatedAt, $comparison);
    }

    /**
     * Filter the query on the archived_at column
     *
     * Example usage:
     * <code>
     * $query->filterByArchivedAt('2011-03-14'); // WHERE archived_at = '2011-03-14'
     * $query->filterByArchivedAt('now'); // WHERE archived_at = '2011-03-14'
     * $query->filterByArchivedAt(array('max' => 'yesterday')); // WHERE archived_at > '2011-03-13'
     * </code>
     *
     * @param     mixed $archivedAt The value to use as filter.
     *              Values can be integers (unix timestamps), DateTime objects, or strings.
     *              Empty strings are treated as NULL.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildAssoServicerendusArchiveQuery The current query, for fluid interface
     */
    public function filterByArchivedAt($archivedAt = null, $comparison = null)
    {
        if (is_array($archivedAt)) {
            $useMinMax = false;
            if (isset($archivedAt['min'])) {
                $this->addUsingAlias(AssoServicerendusArchiveTableMap::COL_ARCHIVED_AT, $archivedAt['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($archivedAt['max'])) {
                $this->addUsingAlias(AssoServicerendusArchiveTableMap::COL_ARCHIVED_AT, $archivedAt['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(AssoServicerendusArchiveTableMap::COL_ARCHIVED_AT, $archivedAt, $comparison);
    }

    /**
     * Exclude object from result
     *
     * @param   ChildAssoServicerendusArchive $assoServicerendusArchive Object to remove from the list of results
     *
     * @return $this|ChildAssoServicerendusArchiveQuery The current query, for fluid interface
     */
    public function prune($assoServicerendusArchive = null)
    {
        if ($assoServicerendusArchive) {
            $this->addUsingAlias(AssoServicerendusArchiveTableMap::COL_ID_SERVICERENDU, $assoServicerendusArchive->getIdServicerendu(), Criteria::NOT_EQUAL);
        }

        return $this;
    }

    /**
     * Deletes all rows from the asso_servicerendus_archive table.
     *
     * @param ConnectionInterface $con the connection to use
     * @return int The number of affected rows (if supported by underlying database driver).
     */
    public function doDeleteAll(ConnectionInterface $con = null)
    {
        if (null === $con) {
            $con = Propel::getServiceContainer()->getWriteConnection(AssoServicerendusArchiveTableMap::DATABASE_NAME);
        }

        // use transaction because $criteria could contain info
        // for more than one table or we could emulating ON DELETE CASCADE, etc.
        return $con->transaction(function () use ($con) {
            $affectedRows = 0; // initialize var to track total num of affected rows
            $affectedRows += parent::doDeleteAll($con);
            // Because this db requires some delete cascade/set null emulation, we have to
            // clear the cached instance *after* the emulation has happened (since
            // instances get re-added by the select statement contained therein).
            AssoServicerendusArchiveTableMap::clearInstancePool();
            AssoServicerendusArchiveTableMap::clearRelatedInstancePool();

            return $affectedRows;
        });
    }

    /**
     * Performs a DELETE on the database based on the current ModelCriteria
     *
     * @param ConnectionInterface $con the connection to use
     * @return int             The number of affected rows (if supported by underlying database driver).  This includes CASCADE-related rows
     *                         if supported by native driver or if emulated using Propel.
     * @throws PropelException Any exceptions caught during processing will be
     *                         rethrown wrapped into a PropelException.
     */
    public function delete(ConnectionInterface $con = null)
    {
        if (null === $con) {
            $con = Propel::getServiceContainer()->getWriteConnection(AssoServicerendusArchiveTableMap::DATABASE_NAME);
        }

        $criteria = $this;

        // Set the correct dbName
        $criteria->setDbName(AssoServicerendusArchiveTableMap::DATABASE_NAME);

        // use transaction because $criteria could contain info
        // for more than one table or we could emulating ON DELETE CASCADE, etc.
        return $con->transaction(function () use ($con, $criteria) {
            $affectedRows = 0; // initialize var to track total num of affected rows

            AssoServicerendusArchiveTableMap::removeInstanceFromPool($criteria);

            $affectedRows += ModelCriteria::delete($con);
            AssoServicerendusArchiveTableMap::clearRelatedInstancePool();

            return $affectedRows;
        });
    }

} // AssoServicerendusArchiveQuery
