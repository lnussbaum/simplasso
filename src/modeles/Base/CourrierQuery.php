<?php

namespace Base;

use \Courrier as ChildCourrier;
use \CourrierQuery as ChildCourrierQuery;
use \Exception;
use \PDO;
use Map\CourrierTableMap;
use Propel\Runtime\Propel;
use Propel\Runtime\ActiveQuery\Criteria;
use Propel\Runtime\ActiveQuery\ModelCriteria;
use Propel\Runtime\ActiveQuery\ModelJoin;
use Propel\Runtime\Collection\ObjectCollection;
use Propel\Runtime\Connection\ConnectionInterface;
use Propel\Runtime\Exception\PropelException;

/**
 * Base class that represents a query for the 'com_courriers' table.
 *
 *
 *
 * @method     ChildCourrierQuery orderByIdCourrier($order = Criteria::ASC) Order by the id_courrier column
 * @method     ChildCourrierQuery orderByType($order = Criteria::ASC) Order by the type column
 * @method     ChildCourrierQuery orderByCanal($order = Criteria::ASC) Order by the canal column
 * @method     ChildCourrierQuery orderByIdEntite($order = Criteria::ASC) Order by the id_entite column
 * @method     ChildCourrierQuery orderByNom($order = Criteria::ASC) Order by the nom column
 * @method     ChildCourrierQuery orderByNomcourt($order = Criteria::ASC) Order by the nomcourt column
 * @method     ChildCourrierQuery orderByIdComposition($order = Criteria::ASC) Order by the id_composition column
 * @method     ChildCourrierQuery orderByVariables($order = Criteria::ASC) Order by the variables column
 * @method     ChildCourrierQuery orderByObservation($order = Criteria::ASC) Order by the observation column
 * @method     ChildCourrierQuery orderByDateEnvoi($order = Criteria::ASC) Order by the date_envoi column
 * @method     ChildCourrierQuery orderByStatut($order = Criteria::ASC) Order by the statut column
 * @method     ChildCourrierQuery orderByCreatedAt($order = Criteria::ASC) Order by the created_at column
 * @method     ChildCourrierQuery orderByUpdatedAt($order = Criteria::ASC) Order by the updated_at column
 *
 * @method     ChildCourrierQuery groupByIdCourrier() Group by the id_courrier column
 * @method     ChildCourrierQuery groupByType() Group by the type column
 * @method     ChildCourrierQuery groupByCanal() Group by the canal column
 * @method     ChildCourrierQuery groupByIdEntite() Group by the id_entite column
 * @method     ChildCourrierQuery groupByNom() Group by the nom column
 * @method     ChildCourrierQuery groupByNomcourt() Group by the nomcourt column
 * @method     ChildCourrierQuery groupByIdComposition() Group by the id_composition column
 * @method     ChildCourrierQuery groupByVariables() Group by the variables column
 * @method     ChildCourrierQuery groupByObservation() Group by the observation column
 * @method     ChildCourrierQuery groupByDateEnvoi() Group by the date_envoi column
 * @method     ChildCourrierQuery groupByStatut() Group by the statut column
 * @method     ChildCourrierQuery groupByCreatedAt() Group by the created_at column
 * @method     ChildCourrierQuery groupByUpdatedAt() Group by the updated_at column
 *
 * @method     ChildCourrierQuery leftJoin($relation) Adds a LEFT JOIN clause to the query
 * @method     ChildCourrierQuery rightJoin($relation) Adds a RIGHT JOIN clause to the query
 * @method     ChildCourrierQuery innerJoin($relation) Adds a INNER JOIN clause to the query
 *
 * @method     ChildCourrierQuery leftJoinWith($relation) Adds a LEFT JOIN clause and with to the query
 * @method     ChildCourrierQuery rightJoinWith($relation) Adds a RIGHT JOIN clause and with to the query
 * @method     ChildCourrierQuery innerJoinWith($relation) Adds a INNER JOIN clause and with to the query
 *
 * @method     ChildCourrierQuery leftJoinEntite($relationAlias = null) Adds a LEFT JOIN clause to the query using the Entite relation
 * @method     ChildCourrierQuery rightJoinEntite($relationAlias = null) Adds a RIGHT JOIN clause to the query using the Entite relation
 * @method     ChildCourrierQuery innerJoinEntite($relationAlias = null) Adds a INNER JOIN clause to the query using the Entite relation
 *
 * @method     ChildCourrierQuery joinWithEntite($joinType = Criteria::INNER_JOIN) Adds a join clause and with to the query using the Entite relation
 *
 * @method     ChildCourrierQuery leftJoinWithEntite() Adds a LEFT JOIN clause and with to the query using the Entite relation
 * @method     ChildCourrierQuery rightJoinWithEntite() Adds a RIGHT JOIN clause and with to the query using the Entite relation
 * @method     ChildCourrierQuery innerJoinWithEntite() Adds a INNER JOIN clause and with to the query using the Entite relation
 *
 * @method     ChildCourrierQuery leftJoinComposition($relationAlias = null) Adds a LEFT JOIN clause to the query using the Composition relation
 * @method     ChildCourrierQuery rightJoinComposition($relationAlias = null) Adds a RIGHT JOIN clause to the query using the Composition relation
 * @method     ChildCourrierQuery innerJoinComposition($relationAlias = null) Adds a INNER JOIN clause to the query using the Composition relation
 *
 * @method     ChildCourrierQuery joinWithComposition($joinType = Criteria::INNER_JOIN) Adds a join clause and with to the query using the Composition relation
 *
 * @method     ChildCourrierQuery leftJoinWithComposition() Adds a LEFT JOIN clause and with to the query using the Composition relation
 * @method     ChildCourrierQuery rightJoinWithComposition() Adds a RIGHT JOIN clause and with to the query using the Composition relation
 * @method     ChildCourrierQuery innerJoinWithComposition() Adds a INNER JOIN clause and with to the query using the Composition relation
 *
 * @method     ChildCourrierQuery leftJoinCourrierLien($relationAlias = null) Adds a LEFT JOIN clause to the query using the CourrierLien relation
 * @method     ChildCourrierQuery rightJoinCourrierLien($relationAlias = null) Adds a RIGHT JOIN clause to the query using the CourrierLien relation
 * @method     ChildCourrierQuery innerJoinCourrierLien($relationAlias = null) Adds a INNER JOIN clause to the query using the CourrierLien relation
 *
 * @method     ChildCourrierQuery joinWithCourrierLien($joinType = Criteria::INNER_JOIN) Adds a join clause and with to the query using the CourrierLien relation
 *
 * @method     ChildCourrierQuery leftJoinWithCourrierLien() Adds a LEFT JOIN clause and with to the query using the CourrierLien relation
 * @method     ChildCourrierQuery rightJoinWithCourrierLien() Adds a RIGHT JOIN clause and with to the query using the CourrierLien relation
 * @method     ChildCourrierQuery innerJoinWithCourrierLien() Adds a INNER JOIN clause and with to the query using the CourrierLien relation
 *
 * @method     \EntiteQuery|\CompositionQuery|\CourrierLienQuery endUse() Finalizes a secondary criteria and merges it with its primary Criteria
 *
 * @method     ChildCourrier findOne(ConnectionInterface $con = null) Return the first ChildCourrier matching the query
 * @method     ChildCourrier findOneOrCreate(ConnectionInterface $con = null) Return the first ChildCourrier matching the query, or a new ChildCourrier object populated from the query conditions when no match is found
 *
 * @method     ChildCourrier findOneByIdCourrier(string $id_courrier) Return the first ChildCourrier filtered by the id_courrier column
 * @method     ChildCourrier findOneByType(int $type) Return the first ChildCourrier filtered by the type column
 * @method     ChildCourrier findOneByCanal(string $canal) Return the first ChildCourrier filtered by the canal column
 * @method     ChildCourrier findOneByIdEntite(string $id_entite) Return the first ChildCourrier filtered by the id_entite column
 * @method     ChildCourrier findOneByNom(string $nom) Return the first ChildCourrier filtered by the nom column
 * @method     ChildCourrier findOneByNomcourt(string $nomcourt) Return the first ChildCourrier filtered by the nomcourt column
 * @method     ChildCourrier findOneByIdComposition(int $id_composition) Return the first ChildCourrier filtered by the id_composition column
 * @method     ChildCourrier findOneByVariables(string $variables) Return the first ChildCourrier filtered by the variables column
 * @method     ChildCourrier findOneByObservation(string $observation) Return the first ChildCourrier filtered by the observation column
 * @method     ChildCourrier findOneByDateEnvoi(string $date_envoi) Return the first ChildCourrier filtered by the date_envoi column
 * @method     ChildCourrier findOneByStatut(string $statut) Return the first ChildCourrier filtered by the statut column
 * @method     ChildCourrier findOneByCreatedAt(string $created_at) Return the first ChildCourrier filtered by the created_at column
 * @method     ChildCourrier findOneByUpdatedAt(string $updated_at) Return the first ChildCourrier filtered by the updated_at column *

 * @method     ChildCourrier requirePk($key, ConnectionInterface $con = null) Return the ChildCourrier by primary key and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildCourrier requireOne(ConnectionInterface $con = null) Return the first ChildCourrier matching the query and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 *
 * @method     ChildCourrier requireOneByIdCourrier(string $id_courrier) Return the first ChildCourrier filtered by the id_courrier column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildCourrier requireOneByType(int $type) Return the first ChildCourrier filtered by the type column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildCourrier requireOneByCanal(string $canal) Return the first ChildCourrier filtered by the canal column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildCourrier requireOneByIdEntite(string $id_entite) Return the first ChildCourrier filtered by the id_entite column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildCourrier requireOneByNom(string $nom) Return the first ChildCourrier filtered by the nom column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildCourrier requireOneByNomcourt(string $nomcourt) Return the first ChildCourrier filtered by the nomcourt column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildCourrier requireOneByIdComposition(int $id_composition) Return the first ChildCourrier filtered by the id_composition column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildCourrier requireOneByVariables(string $variables) Return the first ChildCourrier filtered by the variables column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildCourrier requireOneByObservation(string $observation) Return the first ChildCourrier filtered by the observation column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildCourrier requireOneByDateEnvoi(string $date_envoi) Return the first ChildCourrier filtered by the date_envoi column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildCourrier requireOneByStatut(string $statut) Return the first ChildCourrier filtered by the statut column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildCourrier requireOneByCreatedAt(string $created_at) Return the first ChildCourrier filtered by the created_at column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildCourrier requireOneByUpdatedAt(string $updated_at) Return the first ChildCourrier filtered by the updated_at column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 *
 * @method     ChildCourrier[]|ObjectCollection find(ConnectionInterface $con = null) Return ChildCourrier objects based on current ModelCriteria
 * @method     ChildCourrier[]|ObjectCollection findByIdCourrier(string $id_courrier) Return ChildCourrier objects filtered by the id_courrier column
 * @method     ChildCourrier[]|ObjectCollection findByType(int $type) Return ChildCourrier objects filtered by the type column
 * @method     ChildCourrier[]|ObjectCollection findByCanal(string $canal) Return ChildCourrier objects filtered by the canal column
 * @method     ChildCourrier[]|ObjectCollection findByIdEntite(string $id_entite) Return ChildCourrier objects filtered by the id_entite column
 * @method     ChildCourrier[]|ObjectCollection findByNom(string $nom) Return ChildCourrier objects filtered by the nom column
 * @method     ChildCourrier[]|ObjectCollection findByNomcourt(string $nomcourt) Return ChildCourrier objects filtered by the nomcourt column
 * @method     ChildCourrier[]|ObjectCollection findByIdComposition(int $id_composition) Return ChildCourrier objects filtered by the id_composition column
 * @method     ChildCourrier[]|ObjectCollection findByVariables(string $variables) Return ChildCourrier objects filtered by the variables column
 * @method     ChildCourrier[]|ObjectCollection findByObservation(string $observation) Return ChildCourrier objects filtered by the observation column
 * @method     ChildCourrier[]|ObjectCollection findByDateEnvoi(string $date_envoi) Return ChildCourrier objects filtered by the date_envoi column
 * @method     ChildCourrier[]|ObjectCollection findByStatut(string $statut) Return ChildCourrier objects filtered by the statut column
 * @method     ChildCourrier[]|ObjectCollection findByCreatedAt(string $created_at) Return ChildCourrier objects filtered by the created_at column
 * @method     ChildCourrier[]|ObjectCollection findByUpdatedAt(string $updated_at) Return ChildCourrier objects filtered by the updated_at column
 * @method     ChildCourrier[]|\Propel\Runtime\Util\PropelModelPager paginate($page = 1, $maxPerPage = 10, ConnectionInterface $con = null) Issue a SELECT query based on the current ModelCriteria and uses a page and a maximum number of results per page to compute an offset and a limit
 *
 */
abstract class CourrierQuery extends ModelCriteria
{
    protected $entityNotFoundExceptionClass = '\\Propel\\Runtime\\Exception\\EntityNotFoundException';

    /**
     * Initializes internal state of \Base\CourrierQuery object.
     *
     * @param     string $dbName The database name
     * @param     string $modelName The phpName of a model, e.g. 'Book'
     * @param     string $modelAlias The alias for the model in this query, e.g. 'b'
     */
    public function __construct($dbName = 'simplasso', $modelName = '\\Courrier', $modelAlias = null)
    {
        parent::__construct($dbName, $modelName, $modelAlias);
    }

    /**
     * Returns a new ChildCourrierQuery object.
     *
     * @param     string $modelAlias The alias of a model in the query
     * @param     Criteria $criteria Optional Criteria to build the query from
     *
     * @return ChildCourrierQuery
     */
    public static function create($modelAlias = null, Criteria $criteria = null)
    {
        if ($criteria instanceof ChildCourrierQuery) {
            return $criteria;
        }
        $query = new ChildCourrierQuery();
        if (null !== $modelAlias) {
            $query->setModelAlias($modelAlias);
        }
        if ($criteria instanceof Criteria) {
            $query->mergeWith($criteria);
        }

        return $query;
    }

    /**
     * Find object by primary key.
     * Propel uses the instance pool to skip the database if the object exists.
     * Go fast if the query is untouched.
     *
     * <code>
     * $obj  = $c->findPk(12, $con);
     * </code>
     *
     * @param mixed $key Primary key to use for the query
     * @param ConnectionInterface $con an optional connection object
     *
     * @return ChildCourrier|array|mixed the result, formatted by the current formatter
     */
    public function findPk($key, ConnectionInterface $con = null)
    {
        if ($key === null) {
            return null;
        }

        if ($con === null) {
            $con = Propel::getServiceContainer()->getReadConnection(CourrierTableMap::DATABASE_NAME);
        }

        $this->basePreSelect($con);

        if (
            $this->formatter || $this->modelAlias || $this->with || $this->select
            || $this->selectColumns || $this->asColumns || $this->selectModifiers
            || $this->map || $this->having || $this->joins
        ) {
            return $this->findPkComplex($key, $con);
        }

        if ((null !== ($obj = CourrierTableMap::getInstanceFromPool(null === $key || is_scalar($key) || is_callable([$key, '__toString']) ? (string) $key : $key)))) {
            // the object is already in the instance pool
            return $obj;
        }

        return $this->findPkSimple($key, $con);
    }

    /**
     * Find object by primary key using raw SQL to go fast.
     * Bypass doSelect() and the object formatter by using generated code.
     *
     * @param     mixed $key Primary key to use for the query
     * @param     ConnectionInterface $con A connection object
     *
     * @throws \Propel\Runtime\Exception\PropelException
     *
     * @return ChildCourrier A model object, or null if the key is not found
     */
    protected function findPkSimple($key, ConnectionInterface $con)
    {
        $sql = 'SELECT `id_courrier`, `type`, `canal`, `id_entite`, `nom`, `nomcourt`, `id_composition`, `variables`, `observation`, `date_envoi`, `statut`, `created_at`, `updated_at` FROM `com_courriers` WHERE `id_courrier` = :p0';
        try {
            $stmt = $con->prepare($sql);
            $stmt->bindValue(':p0', $key, PDO::PARAM_INT);
            $stmt->execute();
        } catch (Exception $e) {
            Propel::log($e->getMessage(), Propel::LOG_ERR);
            throw new PropelException(sprintf('Unable to execute SELECT statement [%s]', $sql), 0, $e);
        }
        $obj = null;
        if ($row = $stmt->fetch(\PDO::FETCH_NUM)) {
            /** @var ChildCourrier $obj */
            $obj = new ChildCourrier();
            $obj->hydrate($row);
            CourrierTableMap::addInstanceToPool($obj, null === $key || is_scalar($key) || is_callable([$key, '__toString']) ? (string) $key : $key);
        }
        $stmt->closeCursor();

        return $obj;
    }

    /**
     * Find object by primary key.
     *
     * @param     mixed $key Primary key to use for the query
     * @param     ConnectionInterface $con A connection object
     *
     * @return ChildCourrier|array|mixed the result, formatted by the current formatter
     */
    protected function findPkComplex($key, ConnectionInterface $con)
    {
        // As the query uses a PK condition, no limit(1) is necessary.
        $criteria = $this->isKeepQuery() ? clone $this : $this;
        $dataFetcher = $criteria
            ->filterByPrimaryKey($key)
            ->doSelect($con);

        return $criteria->getFormatter()->init($criteria)->formatOne($dataFetcher);
    }

    /**
     * Find objects by primary key
     * <code>
     * $objs = $c->findPks(array(12, 56, 832), $con);
     * </code>
     * @param     array $keys Primary keys to use for the query
     * @param     ConnectionInterface $con an optional connection object
     *
     * @return ObjectCollection|array|mixed the list of results, formatted by the current formatter
     */
    public function findPks($keys, ConnectionInterface $con = null)
    {
        if (null === $con) {
            $con = Propel::getServiceContainer()->getReadConnection($this->getDbName());
        }
        $this->basePreSelect($con);
        $criteria = $this->isKeepQuery() ? clone $this : $this;
        $dataFetcher = $criteria
            ->filterByPrimaryKeys($keys)
            ->doSelect($con);

        return $criteria->getFormatter()->init($criteria)->format($dataFetcher);
    }

    /**
     * Filter the query by primary key
     *
     * @param     mixed $key Primary key to use for the query
     *
     * @return $this|ChildCourrierQuery The current query, for fluid interface
     */
    public function filterByPrimaryKey($key)
    {

        return $this->addUsingAlias(CourrierTableMap::COL_ID_COURRIER, $key, Criteria::EQUAL);
    }

    /**
     * Filter the query by a list of primary keys
     *
     * @param     array $keys The list of primary key to use for the query
     *
     * @return $this|ChildCourrierQuery The current query, for fluid interface
     */
    public function filterByPrimaryKeys($keys)
    {

        return $this->addUsingAlias(CourrierTableMap::COL_ID_COURRIER, $keys, Criteria::IN);
    }

    /**
     * Filter the query on the id_courrier column
     *
     * Example usage:
     * <code>
     * $query->filterByIdCourrier(1234); // WHERE id_courrier = 1234
     * $query->filterByIdCourrier(array(12, 34)); // WHERE id_courrier IN (12, 34)
     * $query->filterByIdCourrier(array('min' => 12)); // WHERE id_courrier > 12
     * </code>
     *
     * @param     mixed $idCourrier The value to use as filter.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildCourrierQuery The current query, for fluid interface
     */
    public function filterByIdCourrier($idCourrier = null, $comparison = null)
    {
        if (is_array($idCourrier)) {
            $useMinMax = false;
            if (isset($idCourrier['min'])) {
                $this->addUsingAlias(CourrierTableMap::COL_ID_COURRIER, $idCourrier['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($idCourrier['max'])) {
                $this->addUsingAlias(CourrierTableMap::COL_ID_COURRIER, $idCourrier['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(CourrierTableMap::COL_ID_COURRIER, $idCourrier, $comparison);
    }

    /**
     * Filter the query on the type column
     *
     * Example usage:
     * <code>
     * $query->filterByType(1234); // WHERE type = 1234
     * $query->filterByType(array(12, 34)); // WHERE type IN (12, 34)
     * $query->filterByType(array('min' => 12)); // WHERE type > 12
     * </code>
     *
     * @param     mixed $type The value to use as filter.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildCourrierQuery The current query, for fluid interface
     */
    public function filterByType($type = null, $comparison = null)
    {
        if (is_array($type)) {
            $useMinMax = false;
            if (isset($type['min'])) {
                $this->addUsingAlias(CourrierTableMap::COL_TYPE, $type['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($type['max'])) {
                $this->addUsingAlias(CourrierTableMap::COL_TYPE, $type['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(CourrierTableMap::COL_TYPE, $type, $comparison);
    }

    /**
     * Filter the query on the canal column
     *
     * Example usage:
     * <code>
     * $query->filterByCanal('fooValue');   // WHERE canal = 'fooValue'
     * $query->filterByCanal('%fooValue%', Criteria::LIKE); // WHERE canal LIKE '%fooValue%'
     * </code>
     *
     * @param     string $canal The value to use as filter.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildCourrierQuery The current query, for fluid interface
     */
    public function filterByCanal($canal = null, $comparison = null)
    {
        if (null === $comparison) {
            if (is_array($canal)) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(CourrierTableMap::COL_CANAL, $canal, $comparison);
    }

    /**
     * Filter the query on the id_entite column
     *
     * Example usage:
     * <code>
     * $query->filterByIdEntite(1234); // WHERE id_entite = 1234
     * $query->filterByIdEntite(array(12, 34)); // WHERE id_entite IN (12, 34)
     * $query->filterByIdEntite(array('min' => 12)); // WHERE id_entite > 12
     * </code>
     *
     * @see       filterByEntite()
     *
     * @param     mixed $idEntite The value to use as filter.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildCourrierQuery The current query, for fluid interface
     */
    public function filterByIdEntite($idEntite = null, $comparison = null)
    {
        if (is_array($idEntite)) {
            $useMinMax = false;
            if (isset($idEntite['min'])) {
                $this->addUsingAlias(CourrierTableMap::COL_ID_ENTITE, $idEntite['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($idEntite['max'])) {
                $this->addUsingAlias(CourrierTableMap::COL_ID_ENTITE, $idEntite['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(CourrierTableMap::COL_ID_ENTITE, $idEntite, $comparison);
    }

    /**
     * Filter the query on the nom column
     *
     * Example usage:
     * <code>
     * $query->filterByNom('fooValue');   // WHERE nom = 'fooValue'
     * $query->filterByNom('%fooValue%', Criteria::LIKE); // WHERE nom LIKE '%fooValue%'
     * </code>
     *
     * @param     string $nom The value to use as filter.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildCourrierQuery The current query, for fluid interface
     */
    public function filterByNom($nom = null, $comparison = null)
    {
        if (null === $comparison) {
            if (is_array($nom)) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(CourrierTableMap::COL_NOM, $nom, $comparison);
    }

    /**
     * Filter the query on the nomcourt column
     *
     * Example usage:
     * <code>
     * $query->filterByNomcourt('fooValue');   // WHERE nomcourt = 'fooValue'
     * $query->filterByNomcourt('%fooValue%', Criteria::LIKE); // WHERE nomcourt LIKE '%fooValue%'
     * </code>
     *
     * @param     string $nomcourt The value to use as filter.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildCourrierQuery The current query, for fluid interface
     */
    public function filterByNomcourt($nomcourt = null, $comparison = null)
    {
        if (null === $comparison) {
            if (is_array($nomcourt)) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(CourrierTableMap::COL_NOMCOURT, $nomcourt, $comparison);
    }

    /**
     * Filter the query on the id_composition column
     *
     * Example usage:
     * <code>
     * $query->filterByIdComposition(1234); // WHERE id_composition = 1234
     * $query->filterByIdComposition(array(12, 34)); // WHERE id_composition IN (12, 34)
     * $query->filterByIdComposition(array('min' => 12)); // WHERE id_composition > 12
     * </code>
     *
     * @see       filterByComposition()
     *
     * @param     mixed $idComposition The value to use as filter.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildCourrierQuery The current query, for fluid interface
     */
    public function filterByIdComposition($idComposition = null, $comparison = null)
    {
        if (is_array($idComposition)) {
            $useMinMax = false;
            if (isset($idComposition['min'])) {
                $this->addUsingAlias(CourrierTableMap::COL_ID_COMPOSITION, $idComposition['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($idComposition['max'])) {
                $this->addUsingAlias(CourrierTableMap::COL_ID_COMPOSITION, $idComposition['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(CourrierTableMap::COL_ID_COMPOSITION, $idComposition, $comparison);
    }

    /**
     * Filter the query on the variables column
     *
     * Example usage:
     * <code>
     * $query->filterByVariables('fooValue');   // WHERE variables = 'fooValue'
     * $query->filterByVariables('%fooValue%', Criteria::LIKE); // WHERE variables LIKE '%fooValue%'
     * </code>
     *
     * @param     string $variables The value to use as filter.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildCourrierQuery The current query, for fluid interface
     */
    public function filterByVariables($variables = null, $comparison = null)
    {
        if (null === $comparison) {
            if (is_array($variables)) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(CourrierTableMap::COL_VARIABLES, $variables, $comparison);
    }

    /**
     * Filter the query on the observation column
     *
     * Example usage:
     * <code>
     * $query->filterByObservation('fooValue');   // WHERE observation = 'fooValue'
     * $query->filterByObservation('%fooValue%', Criteria::LIKE); // WHERE observation LIKE '%fooValue%'
     * </code>
     *
     * @param     string $observation The value to use as filter.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildCourrierQuery The current query, for fluid interface
     */
    public function filterByObservation($observation = null, $comparison = null)
    {
        if (null === $comparison) {
            if (is_array($observation)) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(CourrierTableMap::COL_OBSERVATION, $observation, $comparison);
    }

    /**
     * Filter the query on the date_envoi column
     *
     * Example usage:
     * <code>
     * $query->filterByDateEnvoi('2011-03-14'); // WHERE date_envoi = '2011-03-14'
     * $query->filterByDateEnvoi('now'); // WHERE date_envoi = '2011-03-14'
     * $query->filterByDateEnvoi(array('max' => 'yesterday')); // WHERE date_envoi > '2011-03-13'
     * </code>
     *
     * @param     mixed $dateEnvoi The value to use as filter.
     *              Values can be integers (unix timestamps), DateTime objects, or strings.
     *              Empty strings are treated as NULL.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildCourrierQuery The current query, for fluid interface
     */
    public function filterByDateEnvoi($dateEnvoi = null, $comparison = null)
    {
        if (is_array($dateEnvoi)) {
            $useMinMax = false;
            if (isset($dateEnvoi['min'])) {
                $this->addUsingAlias(CourrierTableMap::COL_DATE_ENVOI, $dateEnvoi['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($dateEnvoi['max'])) {
                $this->addUsingAlias(CourrierTableMap::COL_DATE_ENVOI, $dateEnvoi['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(CourrierTableMap::COL_DATE_ENVOI, $dateEnvoi, $comparison);
    }

    /**
     * Filter the query on the statut column
     *
     * Example usage:
     * <code>
     * $query->filterByStatut('fooValue');   // WHERE statut = 'fooValue'
     * $query->filterByStatut('%fooValue%', Criteria::LIKE); // WHERE statut LIKE '%fooValue%'
     * </code>
     *
     * @param     string $statut The value to use as filter.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildCourrierQuery The current query, for fluid interface
     */
    public function filterByStatut($statut = null, $comparison = null)
    {
        if (null === $comparison) {
            if (is_array($statut)) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(CourrierTableMap::COL_STATUT, $statut, $comparison);
    }

    /**
     * Filter the query on the created_at column
     *
     * Example usage:
     * <code>
     * $query->filterByCreatedAt('2011-03-14'); // WHERE created_at = '2011-03-14'
     * $query->filterByCreatedAt('now'); // WHERE created_at = '2011-03-14'
     * $query->filterByCreatedAt(array('max' => 'yesterday')); // WHERE created_at > '2011-03-13'
     * </code>
     *
     * @param     mixed $createdAt The value to use as filter.
     *              Values can be integers (unix timestamps), DateTime objects, or strings.
     *              Empty strings are treated as NULL.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildCourrierQuery The current query, for fluid interface
     */
    public function filterByCreatedAt($createdAt = null, $comparison = null)
    {
        if (is_array($createdAt)) {
            $useMinMax = false;
            if (isset($createdAt['min'])) {
                $this->addUsingAlias(CourrierTableMap::COL_CREATED_AT, $createdAt['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($createdAt['max'])) {
                $this->addUsingAlias(CourrierTableMap::COL_CREATED_AT, $createdAt['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(CourrierTableMap::COL_CREATED_AT, $createdAt, $comparison);
    }

    /**
     * Filter the query on the updated_at column
     *
     * Example usage:
     * <code>
     * $query->filterByUpdatedAt('2011-03-14'); // WHERE updated_at = '2011-03-14'
     * $query->filterByUpdatedAt('now'); // WHERE updated_at = '2011-03-14'
     * $query->filterByUpdatedAt(array('max' => 'yesterday')); // WHERE updated_at > '2011-03-13'
     * </code>
     *
     * @param     mixed $updatedAt The value to use as filter.
     *              Values can be integers (unix timestamps), DateTime objects, or strings.
     *              Empty strings are treated as NULL.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildCourrierQuery The current query, for fluid interface
     */
    public function filterByUpdatedAt($updatedAt = null, $comparison = null)
    {
        if (is_array($updatedAt)) {
            $useMinMax = false;
            if (isset($updatedAt['min'])) {
                $this->addUsingAlias(CourrierTableMap::COL_UPDATED_AT, $updatedAt['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($updatedAt['max'])) {
                $this->addUsingAlias(CourrierTableMap::COL_UPDATED_AT, $updatedAt['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(CourrierTableMap::COL_UPDATED_AT, $updatedAt, $comparison);
    }

    /**
     * Filter the query by a related \Entite object
     *
     * @param \Entite|ObjectCollection $entite The related object(s) to use as filter
     * @param string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @throws \Propel\Runtime\Exception\PropelException
     *
     * @return ChildCourrierQuery The current query, for fluid interface
     */
    public function filterByEntite($entite, $comparison = null)
    {
        if ($entite instanceof \Entite) {
            return $this
                ->addUsingAlias(CourrierTableMap::COL_ID_ENTITE, $entite->getIdEntite(), $comparison);
        } elseif ($entite instanceof ObjectCollection) {
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }

            return $this
                ->addUsingAlias(CourrierTableMap::COL_ID_ENTITE, $entite->toKeyValue('PrimaryKey', 'IdEntite'), $comparison);
        } else {
            throw new PropelException('filterByEntite() only accepts arguments of type \Entite or Collection');
        }
    }

    /**
     * Adds a JOIN clause to the query using the Entite relation
     *
     * @param     string $relationAlias optional alias for the relation
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return $this|ChildCourrierQuery The current query, for fluid interface
     */
    public function joinEntite($relationAlias = null, $joinType = Criteria::INNER_JOIN)
    {
        $tableMap = $this->getTableMap();
        $relationMap = $tableMap->getRelation('Entite');

        // create a ModelJoin object for this join
        $join = new ModelJoin();
        $join->setJoinType($joinType);
        $join->setRelationMap($relationMap, $this->useAliasInSQL ? $this->getModelAlias() : null, $relationAlias);
        if ($previousJoin = $this->getPreviousJoin()) {
            $join->setPreviousJoin($previousJoin);
        }

        // add the ModelJoin to the current object
        if ($relationAlias) {
            $this->addAlias($relationAlias, $relationMap->getRightTable()->getName());
            $this->addJoinObject($join, $relationAlias);
        } else {
            $this->addJoinObject($join, 'Entite');
        }

        return $this;
    }

    /**
     * Use the Entite relation Entite object
     *
     * @see useQuery()
     *
     * @param     string $relationAlias optional alias for the relation,
     *                                   to be used as main alias in the secondary query
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return \EntiteQuery A secondary query class using the current class as primary query
     */
    public function useEntiteQuery($relationAlias = null, $joinType = Criteria::INNER_JOIN)
    {
        return $this
            ->joinEntite($relationAlias, $joinType)
            ->useQuery($relationAlias ? $relationAlias : 'Entite', '\EntiteQuery');
    }

    /**
     * Filter the query by a related \Composition object
     *
     * @param \Composition|ObjectCollection $composition The related object(s) to use as filter
     * @param string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @throws \Propel\Runtime\Exception\PropelException
     *
     * @return ChildCourrierQuery The current query, for fluid interface
     */
    public function filterByComposition($composition, $comparison = null)
    {
        if ($composition instanceof \Composition) {
            return $this
                ->addUsingAlias(CourrierTableMap::COL_ID_COMPOSITION, $composition->getIdComposition(), $comparison);
        } elseif ($composition instanceof ObjectCollection) {
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }

            return $this
                ->addUsingAlias(CourrierTableMap::COL_ID_COMPOSITION, $composition->toKeyValue('PrimaryKey', 'IdComposition'), $comparison);
        } else {
            throw new PropelException('filterByComposition() only accepts arguments of type \Composition or Collection');
        }
    }

    /**
     * Adds a JOIN clause to the query using the Composition relation
     *
     * @param     string $relationAlias optional alias for the relation
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return $this|ChildCourrierQuery The current query, for fluid interface
     */
    public function joinComposition($relationAlias = null, $joinType = Criteria::LEFT_JOIN)
    {
        $tableMap = $this->getTableMap();
        $relationMap = $tableMap->getRelation('Composition');

        // create a ModelJoin object for this join
        $join = new ModelJoin();
        $join->setJoinType($joinType);
        $join->setRelationMap($relationMap, $this->useAliasInSQL ? $this->getModelAlias() : null, $relationAlias);
        if ($previousJoin = $this->getPreviousJoin()) {
            $join->setPreviousJoin($previousJoin);
        }

        // add the ModelJoin to the current object
        if ($relationAlias) {
            $this->addAlias($relationAlias, $relationMap->getRightTable()->getName());
            $this->addJoinObject($join, $relationAlias);
        } else {
            $this->addJoinObject($join, 'Composition');
        }

        return $this;
    }

    /**
     * Use the Composition relation Composition object
     *
     * @see useQuery()
     *
     * @param     string $relationAlias optional alias for the relation,
     *                                   to be used as main alias in the secondary query
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return \CompositionQuery A secondary query class using the current class as primary query
     */
    public function useCompositionQuery($relationAlias = null, $joinType = Criteria::LEFT_JOIN)
    {
        return $this
            ->joinComposition($relationAlias, $joinType)
            ->useQuery($relationAlias ? $relationAlias : 'Composition', '\CompositionQuery');
    }

    /**
     * Filter the query by a related \CourrierLien object
     *
     * @param \CourrierLien|ObjectCollection $courrierLien the related object to use as filter
     * @param string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return ChildCourrierQuery The current query, for fluid interface
     */
    public function filterByCourrierLien($courrierLien, $comparison = null)
    {
        if ($courrierLien instanceof \CourrierLien) {
            return $this
                ->addUsingAlias(CourrierTableMap::COL_ID_COURRIER, $courrierLien->getIdCourrier(), $comparison);
        } elseif ($courrierLien instanceof ObjectCollection) {
            return $this
                ->useCourrierLienQuery()
                ->filterByPrimaryKeys($courrierLien->getPrimaryKeys())
                ->endUse();
        } else {
            throw new PropelException('filterByCourrierLien() only accepts arguments of type \CourrierLien or Collection');
        }
    }

    /**
     * Adds a JOIN clause to the query using the CourrierLien relation
     *
     * @param     string $relationAlias optional alias for the relation
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return $this|ChildCourrierQuery The current query, for fluid interface
     */
    public function joinCourrierLien($relationAlias = null, $joinType = Criteria::INNER_JOIN)
    {
        $tableMap = $this->getTableMap();
        $relationMap = $tableMap->getRelation('CourrierLien');

        // create a ModelJoin object for this join
        $join = new ModelJoin();
        $join->setJoinType($joinType);
        $join->setRelationMap($relationMap, $this->useAliasInSQL ? $this->getModelAlias() : null, $relationAlias);
        if ($previousJoin = $this->getPreviousJoin()) {
            $join->setPreviousJoin($previousJoin);
        }

        // add the ModelJoin to the current object
        if ($relationAlias) {
            $this->addAlias($relationAlias, $relationMap->getRightTable()->getName());
            $this->addJoinObject($join, $relationAlias);
        } else {
            $this->addJoinObject($join, 'CourrierLien');
        }

        return $this;
    }

    /**
     * Use the CourrierLien relation CourrierLien object
     *
     * @see useQuery()
     *
     * @param     string $relationAlias optional alias for the relation,
     *                                   to be used as main alias in the secondary query
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return \CourrierLienQuery A secondary query class using the current class as primary query
     */
    public function useCourrierLienQuery($relationAlias = null, $joinType = Criteria::INNER_JOIN)
    {
        return $this
            ->joinCourrierLien($relationAlias, $joinType)
            ->useQuery($relationAlias ? $relationAlias : 'CourrierLien', '\CourrierLienQuery');
    }

    /**
     * Exclude object from result
     *
     * @param   ChildCourrier $courrier Object to remove from the list of results
     *
     * @return $this|ChildCourrierQuery The current query, for fluid interface
     */
    public function prune($courrier = null)
    {
        if ($courrier) {
            $this->addUsingAlias(CourrierTableMap::COL_ID_COURRIER, $courrier->getIdCourrier(), Criteria::NOT_EQUAL);
        }

        return $this;
    }

    /**
     * Deletes all rows from the com_courriers table.
     *
     * @param ConnectionInterface $con the connection to use
     * @return int The number of affected rows (if supported by underlying database driver).
     */
    public function doDeleteAll(ConnectionInterface $con = null)
    {
        if (null === $con) {
            $con = Propel::getServiceContainer()->getWriteConnection(CourrierTableMap::DATABASE_NAME);
        }

        // use transaction because $criteria could contain info
        // for more than one table or we could emulating ON DELETE CASCADE, etc.
        return $con->transaction(function () use ($con) {
            $affectedRows = 0; // initialize var to track total num of affected rows
            $affectedRows += parent::doDeleteAll($con);
            // Because this db requires some delete cascade/set null emulation, we have to
            // clear the cached instance *after* the emulation has happened (since
            // instances get re-added by the select statement contained therein).
            CourrierTableMap::clearInstancePool();
            CourrierTableMap::clearRelatedInstancePool();

            return $affectedRows;
        });
    }

    /**
     * Performs a DELETE on the database based on the current ModelCriteria
     *
     * @param ConnectionInterface $con the connection to use
     * @return int             The number of affected rows (if supported by underlying database driver).  This includes CASCADE-related rows
     *                         if supported by native driver or if emulated using Propel.
     * @throws PropelException Any exceptions caught during processing will be
     *                         rethrown wrapped into a PropelException.
     */
    public function delete(ConnectionInterface $con = null)
    {
        if (null === $con) {
            $con = Propel::getServiceContainer()->getWriteConnection(CourrierTableMap::DATABASE_NAME);
        }

        $criteria = $this;

        // Set the correct dbName
        $criteria->setDbName(CourrierTableMap::DATABASE_NAME);

        // use transaction because $criteria could contain info
        // for more than one table or we could emulating ON DELETE CASCADE, etc.
        return $con->transaction(function () use ($con, $criteria) {
            $affectedRows = 0; // initialize var to track total num of affected rows

            CourrierTableMap::removeInstanceFromPool($criteria);

            $affectedRows += ModelCriteria::delete($con);
            CourrierTableMap::clearRelatedInstancePool();

            return $affectedRows;
        });
    }

    // timestampable behavior

    /**
     * Filter by the latest updated
     *
     * @param      int $nbDays Maximum age of the latest update in days
     *
     * @return     $this|ChildCourrierQuery The current query, for fluid interface
     */
    public function recentlyUpdated($nbDays = 7)
    {
        return $this->addUsingAlias(CourrierTableMap::COL_UPDATED_AT, time() - $nbDays * 24 * 60 * 60, Criteria::GREATER_EQUAL);
    }

    /**
     * Order by update date desc
     *
     * @return     $this|ChildCourrierQuery The current query, for fluid interface
     */
    public function lastUpdatedFirst()
    {
        return $this->addDescendingOrderByColumn(CourrierTableMap::COL_UPDATED_AT);
    }

    /**
     * Order by update date asc
     *
     * @return     $this|ChildCourrierQuery The current query, for fluid interface
     */
    public function firstUpdatedFirst()
    {
        return $this->addAscendingOrderByColumn(CourrierTableMap::COL_UPDATED_AT);
    }

    /**
     * Order by create date desc
     *
     * @return     $this|ChildCourrierQuery The current query, for fluid interface
     */
    public function lastCreatedFirst()
    {
        return $this->addDescendingOrderByColumn(CourrierTableMap::COL_CREATED_AT);
    }

    /**
     * Filter by the latest created
     *
     * @param      int $nbDays Maximum age of in days
     *
     * @return     $this|ChildCourrierQuery The current query, for fluid interface
     */
    public function recentlyCreated($nbDays = 7)
    {
        return $this->addUsingAlias(CourrierTableMap::COL_CREATED_AT, time() - $nbDays * 24 * 60 * 60, Criteria::GREATER_EQUAL);
    }

    /**
     * Order by create date asc
     *
     * @return     $this|ChildCourrierQuery The current query, for fluid interface
     */
    public function firstCreatedFirst()
    {
        return $this->addAscendingOrderByColumn(CourrierTableMap::COL_CREATED_AT);
    }

} // CourrierQuery
