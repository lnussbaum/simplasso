<?php

namespace Base;

use \Commune as ChildCommune;
use \CommuneQuery as ChildCommuneQuery;
use \DateTime;
use \Exception;
use \PDO;
use Map\CommuneTableMap;
use Propel\Runtime\Propel;
use Propel\Runtime\ActiveQuery\Criteria;
use Propel\Runtime\ActiveQuery\ModelCriteria;
use Propel\Runtime\ActiveRecord\ActiveRecordInterface;
use Propel\Runtime\Collection\Collection;
use Propel\Runtime\Connection\ConnectionInterface;
use Propel\Runtime\Exception\BadMethodCallException;
use Propel\Runtime\Exception\LogicException;
use Propel\Runtime\Exception\PropelException;
use Propel\Runtime\Map\TableMap;
use Propel\Runtime\Parser\AbstractParser;
use Propel\Runtime\Util\PropelDateTime;

/**
 * Base class that represents a row from the 'geo_communes' table.
 *
 *
 *
 * @package    propel.generator..Base
 */
abstract class Commune implements ActiveRecordInterface
{
    /**
     * TableMap class name
     */
    const TABLE_MAP = '\\Map\\CommuneTableMap';


    /**
     * attribute to determine if this object has previously been saved.
     * @var boolean
     */
    protected $new = true;

    /**
     * attribute to determine whether this object has been deleted.
     * @var boolean
     */
    protected $deleted = false;

    /**
     * The columns that have been modified in current object.
     * Tracking modified columns allows us to only update modified columns.
     * @var array
     */
    protected $modifiedColumns = array();

    /**
     * The (virtual) columns that are added at runtime
     * The formatters can add supplementary columns based on a resultset
     * @var array
     */
    protected $virtualColumns = array();

    /**
     * The value for the id_commune field.
     * identifiant de la commune
     * @var        int
     */
    protected $id_commune;

    /**
     * The value for the pays field.
     * code du pays
     * @var        string
     */
    protected $pays;

    /**
     * The value for the region field.
     * Code région
     * @var        int
     */
    protected $region;

    /**
     * The value for the departement field.
     * Code département
     * @var        string
     */
    protected $departement;

    /**
     * The value for the code field.
     * Code commune insse
     * @var        int
     */
    protected $code;

    /**
     * The value for the arrondissement field.
     * Code arrondissement
     * @var        boolean
     */
    protected $arrondissement;

    /**
     * The value for the canton field.
     * Code canton
     * @var        int
     */
    protected $canton;

    /**
     * The value for the type_charniere field.
     * Type de nom en clair
     * @var        boolean
     */
    protected $type_charniere;

    /**
     * The value for the article field.
     * Article (typographie riche)
     * @var        string
     */
    protected $article;

    /**
     * The value for the nom field.
     * Nom en clair (typographie riche)
     * @var        string
     */
    protected $nom;

    /**
     * The value for the lon field.
     *
     * @var        string
     */
    protected $lon;

    /**
     * The value for the lat field.
     *
     * @var        string
     */
    protected $lat;

    /**
     * The value for the zoom field.
     * Zoom
     * @var        int
     */
    protected $zoom;

    /**
     * The value for the elevation field.
     * elevation de la commune
     * @var        int
     */
    protected $elevation;

    /**
     * The value for the elevation_moyenne field.
     * elevation moyenne de la commune
     * @var        int
     */
    protected $elevation_moyenne;

    /**
     * The value for the population field.
     * elevation moyenne de la commune
     * @var        string
     */
    protected $population;

    /**
     * The value for the autre_nom field.
     * Nom dans d'autre langue
     * @var        string
     */
    protected $autre_nom;

    /**
     * The value for the url field.
     * url du site de la commune
     * @var        string
     */
    protected $url;

    /**
     * The value for the zonage field.
     * zonage
     * @var        string
     */
    protected $zonage;

    /**
     * The value for the created_at field.
     *
     * @var        DateTime
     */
    protected $created_at;

    /**
     * The value for the updated_at field.
     *
     * @var        DateTime
     */
    protected $updated_at;

    /**
     * Flag to prevent endless save loop, if this object is referenced
     * by another object which falls in this transaction.
     *
     * @var boolean
     */
    protected $alreadyInSave = false;

    /**
     * Initializes internal state of Base\Commune object.
     */
    public function __construct()
    {
    }

    /**
     * Returns whether the object has been modified.
     *
     * @return boolean True if the object has been modified.
     */
    public function isModified()
    {
        return !!$this->modifiedColumns;
    }

    /**
     * Has specified column been modified?
     *
     * @param  string  $col column fully qualified name (TableMap::TYPE_COLNAME), e.g. Book::AUTHOR_ID
     * @return boolean True if $col has been modified.
     */
    public function isColumnModified($col)
    {
        return $this->modifiedColumns && isset($this->modifiedColumns[$col]);
    }

    /**
     * Get the columns that have been modified in this object.
     * @return array A unique list of the modified column names for this object.
     */
    public function getModifiedColumns()
    {
        return $this->modifiedColumns ? array_keys($this->modifiedColumns) : [];
    }

    /**
     * Returns whether the object has ever been saved.  This will
     * be false, if the object was retrieved from storage or was created
     * and then saved.
     *
     * @return boolean true, if the object has never been persisted.
     */
    public function isNew()
    {
        return $this->new;
    }

    /**
     * Setter for the isNew attribute.  This method will be called
     * by Propel-generated children and objects.
     *
     * @param boolean $b the state of the object.
     */
    public function setNew($b)
    {
        $this->new = (boolean) $b;
    }

    /**
     * Whether this object has been deleted.
     * @return boolean The deleted state of this object.
     */
    public function isDeleted()
    {
        return $this->deleted;
    }

    /**
     * Specify whether this object has been deleted.
     * @param  boolean $b The deleted state of this object.
     * @return void
     */
    public function setDeleted($b)
    {
        $this->deleted = (boolean) $b;
    }

    /**
     * Sets the modified state for the object to be false.
     * @param  string $col If supplied, only the specified column is reset.
     * @return void
     */
    public function resetModified($col = null)
    {
        if (null !== $col) {
            if (isset($this->modifiedColumns[$col])) {
                unset($this->modifiedColumns[$col]);
            }
        } else {
            $this->modifiedColumns = array();
        }
    }

    /**
     * Compares this with another <code>Commune</code> instance.  If
     * <code>obj</code> is an instance of <code>Commune</code>, delegates to
     * <code>equals(Commune)</code>.  Otherwise, returns <code>false</code>.
     *
     * @param  mixed   $obj The object to compare to.
     * @return boolean Whether equal to the object specified.
     */
    public function equals($obj)
    {
        if (!$obj instanceof static) {
            return false;
        }

        if ($this === $obj) {
            return true;
        }

        if (null === $this->getPrimaryKey() || null === $obj->getPrimaryKey()) {
            return false;
        }

        return $this->getPrimaryKey() === $obj->getPrimaryKey();
    }

    /**
     * Get the associative array of the virtual columns in this object
     *
     * @return array
     */
    public function getVirtualColumns()
    {
        return $this->virtualColumns;
    }

    /**
     * Checks the existence of a virtual column in this object
     *
     * @param  string  $name The virtual column name
     * @return boolean
     */
    public function hasVirtualColumn($name)
    {
        return array_key_exists($name, $this->virtualColumns);
    }

    /**
     * Get the value of a virtual column in this object
     *
     * @param  string $name The virtual column name
     * @return mixed
     *
     * @throws PropelException
     */
    public function getVirtualColumn($name)
    {
        if (!$this->hasVirtualColumn($name)) {
            throw new PropelException(sprintf('Cannot get value of inexistent virtual column %s.', $name));
        }

        return $this->virtualColumns[$name];
    }

    /**
     * Set the value of a virtual column in this object
     *
     * @param string $name  The virtual column name
     * @param mixed  $value The value to give to the virtual column
     *
     * @return $this|Commune The current object, for fluid interface
     */
    public function setVirtualColumn($name, $value)
    {
        $this->virtualColumns[$name] = $value;

        return $this;
    }

    /**
     * Logs a message using Propel::log().
     *
     * @param  string  $msg
     * @param  int     $priority One of the Propel::LOG_* logging levels
     * @return boolean
     */
    protected function log($msg, $priority = Propel::LOG_INFO)
    {
        return Propel::log(get_class($this) . ': ' . $msg, $priority);
    }

    /**
     * Export the current object properties to a string, using a given parser format
     * <code>
     * $book = BookQuery::create()->findPk(9012);
     * echo $book->exportTo('JSON');
     *  => {"Id":9012,"Title":"Don Juan","ISBN":"0140422161","Price":12.99,"PublisherId":1234,"AuthorId":5678}');
     * </code>
     *
     * @param  mixed   $parser                 A AbstractParser instance, or a format name ('XML', 'YAML', 'JSON', 'CSV')
     * @param  boolean $includeLazyLoadColumns (optional) Whether to include lazy load(ed) columns. Defaults to TRUE.
     * @return string  The exported data
     */
    public function exportTo($parser, $includeLazyLoadColumns = true)
    {
        if (!$parser instanceof AbstractParser) {
            $parser = AbstractParser::getParser($parser);
        }

        return $parser->fromArray($this->toArray(TableMap::TYPE_PHPNAME, $includeLazyLoadColumns, array(), true));
    }

    /**
     * Clean up internal collections prior to serializing
     * Avoids recursive loops that turn into segmentation faults when serializing
     */
    public function __sleep()
    {
        $this->clearAllReferences();

        $cls = new \ReflectionClass($this);
        $propertyNames = [];
        $serializableProperties = array_diff($cls->getProperties(), $cls->getProperties(\ReflectionProperty::IS_STATIC));

        foreach($serializableProperties as $property) {
            $propertyNames[] = $property->getName();
        }

        return $propertyNames;
    }

    /**
     * Get the [id_commune] column value.
     * identifiant de la commune
     * @return int
     */
    public function getIdCommune()
    {
        return $this->id_commune;
    }

    /**
     * Get the [pays] column value.
     * code du pays
     * @return string
     */
    public function getPays()
    {
        return $this->pays;
    }

    /**
     * Get the [region] column value.
     * Code région
     * @return int
     */
    public function getRegion()
    {
        return $this->region;
    }

    /**
     * Get the [departement] column value.
     * Code département
     * @return string
     */
    public function getDepartement()
    {
        return $this->departement;
    }

    /**
     * Get the [code] column value.
     * Code commune insse
     * @return int
     */
    public function getCode()
    {
        return $this->code;
    }

    /**
     * Get the [arrondissement] column value.
     * Code arrondissement
     * @return boolean
     */
    public function getArrondissement()
    {
        return $this->arrondissement;
    }

    /**
     * Get the [arrondissement] column value.
     * Code arrondissement
     * @return boolean
     */
    public function isArrondissement()
    {
        return $this->getArrondissement();
    }

    /**
     * Get the [canton] column value.
     * Code canton
     * @return int
     */
    public function getCanton()
    {
        return $this->canton;
    }

    /**
     * Get the [type_charniere] column value.
     * Type de nom en clair
     * @return boolean
     */
    public function getTypeCharniere()
    {
        return $this->type_charniere;
    }

    /**
     * Get the [type_charniere] column value.
     * Type de nom en clair
     * @return boolean
     */
    public function isTypeCharniere()
    {
        return $this->getTypeCharniere();
    }

    /**
     * Get the [article] column value.
     * Article (typographie riche)
     * @return string
     */
    public function getArticle()
    {
        return $this->article;
    }

    /**
     * Get the [nom] column value.
     * Nom en clair (typographie riche)
     * @return string
     */
    public function getNom()
    {
        return $this->nom;
    }

    /**
     * Get the [lon] column value.
     *
     * @return string
     */
    public function getLon()
    {
        return $this->lon;
    }

    /**
     * Get the [lat] column value.
     *
     * @return string
     */
    public function getLat()
    {
        return $this->lat;
    }

    /**
     * Get the [zoom] column value.
     * Zoom
     * @return int
     */
    public function getZoom()
    {
        return $this->zoom;
    }

    /**
     * Get the [elevation] column value.
     * elevation de la commune
     * @return int
     */
    public function getElevation()
    {
        return $this->elevation;
    }

    /**
     * Get the [elevation_moyenne] column value.
     * elevation moyenne de la commune
     * @return int
     */
    public function getElevationMoyenne()
    {
        return $this->elevation_moyenne;
    }

    /**
     * Get the [population] column value.
     * elevation moyenne de la commune
     * @return string
     */
    public function getPopulation()
    {
        return $this->population;
    }

    /**
     * Get the [autre_nom] column value.
     * Nom dans d'autre langue
     * @return string
     */
    public function getAutreNom()
    {
        return $this->autre_nom;
    }

    /**
     * Get the [url] column value.
     * url du site de la commune
     * @return string
     */
    public function getUrl()
    {
        return $this->url;
    }

    /**
     * Get the [zonage] column value.
     * zonage
     * @return string
     */
    public function getZonage()
    {
        return $this->zonage;
    }

    /**
     * Get the [optionally formatted] temporal [created_at] column value.
     *
     *
     * @param      string $format The date/time format string (either date()-style or strftime()-style).
     *                            If format is NULL, then the raw DateTime object will be returned.
     *
     * @return string|DateTime Formatted date/time value as string or DateTime object (if format is NULL), NULL if column is NULL, and 0 if column value is 0000-00-00 00:00:00
     *
     * @throws PropelException - if unable to parse/validate the date/time value.
     */
    public function getCreatedAt($format = NULL)
    {
        if ($format === null) {
            return $this->created_at;
        } else {
            return $this->created_at instanceof \DateTimeInterface ? $this->created_at->format($format) : null;
        }
    }

    /**
     * Get the [optionally formatted] temporal [updated_at] column value.
     *
     *
     * @param      string $format The date/time format string (either date()-style or strftime()-style).
     *                            If format is NULL, then the raw DateTime object will be returned.
     *
     * @return string|DateTime Formatted date/time value as string or DateTime object (if format is NULL), NULL if column is NULL, and 0 if column value is 0000-00-00 00:00:00
     *
     * @throws PropelException - if unable to parse/validate the date/time value.
     */
    public function getUpdatedAt($format = NULL)
    {
        if ($format === null) {
            return $this->updated_at;
        } else {
            return $this->updated_at instanceof \DateTimeInterface ? $this->updated_at->format($format) : null;
        }
    }

    /**
     * Set the value of [id_commune] column.
     * identifiant de la commune
     * @param int $v new value
     * @return $this|\Commune The current object (for fluent API support)
     */
    public function setIdCommune($v)
    {
        if ($v !== null) {
            $v = (int) $v;
        }

        if ($this->id_commune !== $v) {
            $this->id_commune = $v;
            $this->modifiedColumns[CommuneTableMap::COL_ID_COMMUNE] = true;
        }

        return $this;
    } // setIdCommune()

    /**
     * Set the value of [pays] column.
     * code du pays
     * @param string $v new value
     * @return $this|\Commune The current object (for fluent API support)
     */
    public function setPays($v)
    {
        if ($v !== null) {
            $v = (string) $v;
        }

        if ($this->pays !== $v) {
            $this->pays = $v;
            $this->modifiedColumns[CommuneTableMap::COL_PAYS] = true;
        }

        return $this;
    } // setPays()

    /**
     * Set the value of [region] column.
     * Code région
     * @param int $v new value
     * @return $this|\Commune The current object (for fluent API support)
     */
    public function setRegion($v)
    {
        if ($v !== null) {
            $v = (int) $v;
        }

        if ($this->region !== $v) {
            $this->region = $v;
            $this->modifiedColumns[CommuneTableMap::COL_REGION] = true;
        }

        return $this;
    } // setRegion()

    /**
     * Set the value of [departement] column.
     * Code département
     * @param string $v new value
     * @return $this|\Commune The current object (for fluent API support)
     */
    public function setDepartement($v)
    {
        if ($v !== null) {
            $v = (string) $v;
        }

        if ($this->departement !== $v) {
            $this->departement = $v;
            $this->modifiedColumns[CommuneTableMap::COL_DEPARTEMENT] = true;
        }

        return $this;
    } // setDepartement()

    /**
     * Set the value of [code] column.
     * Code commune insse
     * @param int $v new value
     * @return $this|\Commune The current object (for fluent API support)
     */
    public function setCode($v)
    {
        if ($v !== null) {
            $v = (int) $v;
        }

        if ($this->code !== $v) {
            $this->code = $v;
            $this->modifiedColumns[CommuneTableMap::COL_CODE] = true;
        }

        return $this;
    } // setCode()

    /**
     * Sets the value of the [arrondissement] column.
     * Non-boolean arguments are converted using the following rules:
     *   * 1, '1', 'true',  'on',  and 'yes' are converted to boolean true
     *   * 0, '0', 'false', 'off', and 'no'  are converted to boolean false
     * Check on string values is case insensitive (so 'FaLsE' is seen as 'false').
     * Code arrondissement
     * @param  boolean|integer|string $v The new value
     * @return $this|\Commune The current object (for fluent API support)
     */
    public function setArrondissement($v)
    {
        if ($v !== null) {
            if (is_string($v)) {
                $v = in_array(strtolower($v), array('false', 'off', '-', 'no', 'n', '0', '')) ? false : true;
            } else {
                $v = (boolean) $v;
            }
        }

        if ($this->arrondissement !== $v) {
            $this->arrondissement = $v;
            $this->modifiedColumns[CommuneTableMap::COL_ARRONDISSEMENT] = true;
        }

        return $this;
    } // setArrondissement()

    /**
     * Set the value of [canton] column.
     * Code canton
     * @param int $v new value
     * @return $this|\Commune The current object (for fluent API support)
     */
    public function setCanton($v)
    {
        if ($v !== null) {
            $v = (int) $v;
        }

        if ($this->canton !== $v) {
            $this->canton = $v;
            $this->modifiedColumns[CommuneTableMap::COL_CANTON] = true;
        }

        return $this;
    } // setCanton()

    /**
     * Sets the value of the [type_charniere] column.
     * Non-boolean arguments are converted using the following rules:
     *   * 1, '1', 'true',  'on',  and 'yes' are converted to boolean true
     *   * 0, '0', 'false', 'off', and 'no'  are converted to boolean false
     * Check on string values is case insensitive (so 'FaLsE' is seen as 'false').
     * Type de nom en clair
     * @param  boolean|integer|string $v The new value
     * @return $this|\Commune The current object (for fluent API support)
     */
    public function setTypeCharniere($v)
    {
        if ($v !== null) {
            if (is_string($v)) {
                $v = in_array(strtolower($v), array('false', 'off', '-', 'no', 'n', '0', '')) ? false : true;
            } else {
                $v = (boolean) $v;
            }
        }

        if ($this->type_charniere !== $v) {
            $this->type_charniere = $v;
            $this->modifiedColumns[CommuneTableMap::COL_TYPE_CHARNIERE] = true;
        }

        return $this;
    } // setTypeCharniere()

    /**
     * Set the value of [article] column.
     * Article (typographie riche)
     * @param string $v new value
     * @return $this|\Commune The current object (for fluent API support)
     */
    public function setArticle($v)
    {
        if ($v !== null) {
            $v = (string) $v;
        }

        if ($this->article !== $v) {
            $this->article = $v;
            $this->modifiedColumns[CommuneTableMap::COL_ARTICLE] = true;
        }

        return $this;
    } // setArticle()

    /**
     * Set the value of [nom] column.
     * Nom en clair (typographie riche)
     * @param string $v new value
     * @return $this|\Commune The current object (for fluent API support)
     */
    public function setNom($v)
    {
        if ($v !== null) {
            $v = (string) $v;
        }

        if ($this->nom !== $v) {
            $this->nom = $v;
            $this->modifiedColumns[CommuneTableMap::COL_NOM] = true;
        }

        return $this;
    } // setNom()

    /**
     * Set the value of [lon] column.
     *
     * @param string $v new value
     * @return $this|\Commune The current object (for fluent API support)
     */
    public function setLon($v)
    {
        if ($v !== null) {
            $v = (string) $v;
        }

        if ($this->lon !== $v) {
            $this->lon = $v;
            $this->modifiedColumns[CommuneTableMap::COL_LON] = true;
        }

        return $this;
    } // setLon()

    /**
     * Set the value of [lat] column.
     *
     * @param string $v new value
     * @return $this|\Commune The current object (for fluent API support)
     */
    public function setLat($v)
    {
        if ($v !== null) {
            $v = (string) $v;
        }

        if ($this->lat !== $v) {
            $this->lat = $v;
            $this->modifiedColumns[CommuneTableMap::COL_LAT] = true;
        }

        return $this;
    } // setLat()

    /**
     * Set the value of [zoom] column.
     * Zoom
     * @param int $v new value
     * @return $this|\Commune The current object (for fluent API support)
     */
    public function setZoom($v)
    {
        if ($v !== null) {
            $v = (int) $v;
        }

        if ($this->zoom !== $v) {
            $this->zoom = $v;
            $this->modifiedColumns[CommuneTableMap::COL_ZOOM] = true;
        }

        return $this;
    } // setZoom()

    /**
     * Set the value of [elevation] column.
     * elevation de la commune
     * @param int $v new value
     * @return $this|\Commune The current object (for fluent API support)
     */
    public function setElevation($v)
    {
        if ($v !== null) {
            $v = (int) $v;
        }

        if ($this->elevation !== $v) {
            $this->elevation = $v;
            $this->modifiedColumns[CommuneTableMap::COL_ELEVATION] = true;
        }

        return $this;
    } // setElevation()

    /**
     * Set the value of [elevation_moyenne] column.
     * elevation moyenne de la commune
     * @param int $v new value
     * @return $this|\Commune The current object (for fluent API support)
     */
    public function setElevationMoyenne($v)
    {
        if ($v !== null) {
            $v = (int) $v;
        }

        if ($this->elevation_moyenne !== $v) {
            $this->elevation_moyenne = $v;
            $this->modifiedColumns[CommuneTableMap::COL_ELEVATION_MOYENNE] = true;
        }

        return $this;
    } // setElevationMoyenne()

    /**
     * Set the value of [population] column.
     * elevation moyenne de la commune
     * @param string $v new value
     * @return $this|\Commune The current object (for fluent API support)
     */
    public function setPopulation($v)
    {
        if ($v !== null) {
            $v = (string) $v;
        }

        if ($this->population !== $v) {
            $this->population = $v;
            $this->modifiedColumns[CommuneTableMap::COL_POPULATION] = true;
        }

        return $this;
    } // setPopulation()

    /**
     * Set the value of [autre_nom] column.
     * Nom dans d'autre langue
     * @param string $v new value
     * @return $this|\Commune The current object (for fluent API support)
     */
    public function setAutreNom($v)
    {
        if ($v !== null) {
            $v = (string) $v;
        }

        if ($this->autre_nom !== $v) {
            $this->autre_nom = $v;
            $this->modifiedColumns[CommuneTableMap::COL_AUTRE_NOM] = true;
        }

        return $this;
    } // setAutreNom()

    /**
     * Set the value of [url] column.
     * url du site de la commune
     * @param string $v new value
     * @return $this|\Commune The current object (for fluent API support)
     */
    public function setUrl($v)
    {
        if ($v !== null) {
            $v = (string) $v;
        }

        if ($this->url !== $v) {
            $this->url = $v;
            $this->modifiedColumns[CommuneTableMap::COL_URL] = true;
        }

        return $this;
    } // setUrl()

    /**
     * Set the value of [zonage] column.
     * zonage
     * @param string $v new value
     * @return $this|\Commune The current object (for fluent API support)
     */
    public function setZonage($v)
    {
        if ($v !== null) {
            $v = (string) $v;
        }

        if ($this->zonage !== $v) {
            $this->zonage = $v;
            $this->modifiedColumns[CommuneTableMap::COL_ZONAGE] = true;
        }

        return $this;
    } // setZonage()

    /**
     * Sets the value of [created_at] column to a normalized version of the date/time value specified.
     *
     * @param  mixed $v string, integer (timestamp), or \DateTimeInterface value.
     *               Empty strings are treated as NULL.
     * @return $this|\Commune The current object (for fluent API support)
     */
    public function setCreatedAt($v)
    {
        $dt = PropelDateTime::newInstance($v, null, 'DateTime');
        if ($this->created_at !== null || $dt !== null) {
            if ($this->created_at === null || $dt === null || $dt->format("Y-m-d H:i:s.u") !== $this->created_at->format("Y-m-d H:i:s.u")) {
                $this->created_at = $dt === null ? null : clone $dt;
                $this->modifiedColumns[CommuneTableMap::COL_CREATED_AT] = true;
            }
        } // if either are not null

        return $this;
    } // setCreatedAt()

    /**
     * Sets the value of [updated_at] column to a normalized version of the date/time value specified.
     *
     * @param  mixed $v string, integer (timestamp), or \DateTimeInterface value.
     *               Empty strings are treated as NULL.
     * @return $this|\Commune The current object (for fluent API support)
     */
    public function setUpdatedAt($v)
    {
        $dt = PropelDateTime::newInstance($v, null, 'DateTime');
        if ($this->updated_at !== null || $dt !== null) {
            if ($this->updated_at === null || $dt === null || $dt->format("Y-m-d H:i:s.u") !== $this->updated_at->format("Y-m-d H:i:s.u")) {
                $this->updated_at = $dt === null ? null : clone $dt;
                $this->modifiedColumns[CommuneTableMap::COL_UPDATED_AT] = true;
            }
        } // if either are not null

        return $this;
    } // setUpdatedAt()

    /**
     * Indicates whether the columns in this object are only set to default values.
     *
     * This method can be used in conjunction with isModified() to indicate whether an object is both
     * modified _and_ has some values set which are non-default.
     *
     * @return boolean Whether the columns in this object are only been set with default values.
     */
    public function hasOnlyDefaultValues()
    {
        // otherwise, everything was equal, so return TRUE
        return true;
    } // hasOnlyDefaultValues()

    /**
     * Hydrates (populates) the object variables with values from the database resultset.
     *
     * An offset (0-based "start column") is specified so that objects can be hydrated
     * with a subset of the columns in the resultset rows.  This is needed, for example,
     * for results of JOIN queries where the resultset row includes columns from two or
     * more tables.
     *
     * @param array   $row       The row returned by DataFetcher->fetch().
     * @param int     $startcol  0-based offset column which indicates which restultset column to start with.
     * @param boolean $rehydrate Whether this object is being re-hydrated from the database.
     * @param string  $indexType The index type of $row. Mostly DataFetcher->getIndexType().
                                  One of the class type constants TableMap::TYPE_PHPNAME, TableMap::TYPE_CAMELNAME
     *                            TableMap::TYPE_COLNAME, TableMap::TYPE_FIELDNAME, TableMap::TYPE_NUM.
     *
     * @return int             next starting column
     * @throws PropelException - Any caught Exception will be rewrapped as a PropelException.
     */
    public function hydrate($row, $startcol = 0, $rehydrate = false, $indexType = TableMap::TYPE_NUM)
    {
        try {

            $col = $row[TableMap::TYPE_NUM == $indexType ? 0 + $startcol : CommuneTableMap::translateFieldName('IdCommune', TableMap::TYPE_PHPNAME, $indexType)];
            $this->id_commune = (null !== $col) ? (int) $col : null;

            $col = $row[TableMap::TYPE_NUM == $indexType ? 1 + $startcol : CommuneTableMap::translateFieldName('Pays', TableMap::TYPE_PHPNAME, $indexType)];
            $this->pays = (null !== $col) ? (string) $col : null;

            $col = $row[TableMap::TYPE_NUM == $indexType ? 2 + $startcol : CommuneTableMap::translateFieldName('Region', TableMap::TYPE_PHPNAME, $indexType)];
            $this->region = (null !== $col) ? (int) $col : null;

            $col = $row[TableMap::TYPE_NUM == $indexType ? 3 + $startcol : CommuneTableMap::translateFieldName('Departement', TableMap::TYPE_PHPNAME, $indexType)];
            $this->departement = (null !== $col) ? (string) $col : null;

            $col = $row[TableMap::TYPE_NUM == $indexType ? 4 + $startcol : CommuneTableMap::translateFieldName('Code', TableMap::TYPE_PHPNAME, $indexType)];
            $this->code = (null !== $col) ? (int) $col : null;

            $col = $row[TableMap::TYPE_NUM == $indexType ? 5 + $startcol : CommuneTableMap::translateFieldName('Arrondissement', TableMap::TYPE_PHPNAME, $indexType)];
            $this->arrondissement = (null !== $col) ? (boolean) $col : null;

            $col = $row[TableMap::TYPE_NUM == $indexType ? 6 + $startcol : CommuneTableMap::translateFieldName('Canton', TableMap::TYPE_PHPNAME, $indexType)];
            $this->canton = (null !== $col) ? (int) $col : null;

            $col = $row[TableMap::TYPE_NUM == $indexType ? 7 + $startcol : CommuneTableMap::translateFieldName('TypeCharniere', TableMap::TYPE_PHPNAME, $indexType)];
            $this->type_charniere = (null !== $col) ? (boolean) $col : null;

            $col = $row[TableMap::TYPE_NUM == $indexType ? 8 + $startcol : CommuneTableMap::translateFieldName('Article', TableMap::TYPE_PHPNAME, $indexType)];
            $this->article = (null !== $col) ? (string) $col : null;

            $col = $row[TableMap::TYPE_NUM == $indexType ? 9 + $startcol : CommuneTableMap::translateFieldName('Nom', TableMap::TYPE_PHPNAME, $indexType)];
            $this->nom = (null !== $col) ? (string) $col : null;

            $col = $row[TableMap::TYPE_NUM == $indexType ? 10 + $startcol : CommuneTableMap::translateFieldName('Lon', TableMap::TYPE_PHPNAME, $indexType)];
            $this->lon = (null !== $col) ? (string) $col : null;

            $col = $row[TableMap::TYPE_NUM == $indexType ? 11 + $startcol : CommuneTableMap::translateFieldName('Lat', TableMap::TYPE_PHPNAME, $indexType)];
            $this->lat = (null !== $col) ? (string) $col : null;

            $col = $row[TableMap::TYPE_NUM == $indexType ? 12 + $startcol : CommuneTableMap::translateFieldName('Zoom', TableMap::TYPE_PHPNAME, $indexType)];
            $this->zoom = (null !== $col) ? (int) $col : null;

            $col = $row[TableMap::TYPE_NUM == $indexType ? 13 + $startcol : CommuneTableMap::translateFieldName('Elevation', TableMap::TYPE_PHPNAME, $indexType)];
            $this->elevation = (null !== $col) ? (int) $col : null;

            $col = $row[TableMap::TYPE_NUM == $indexType ? 14 + $startcol : CommuneTableMap::translateFieldName('ElevationMoyenne', TableMap::TYPE_PHPNAME, $indexType)];
            $this->elevation_moyenne = (null !== $col) ? (int) $col : null;

            $col = $row[TableMap::TYPE_NUM == $indexType ? 15 + $startcol : CommuneTableMap::translateFieldName('Population', TableMap::TYPE_PHPNAME, $indexType)];
            $this->population = (null !== $col) ? (string) $col : null;

            $col = $row[TableMap::TYPE_NUM == $indexType ? 16 + $startcol : CommuneTableMap::translateFieldName('AutreNom', TableMap::TYPE_PHPNAME, $indexType)];
            $this->autre_nom = (null !== $col) ? (string) $col : null;

            $col = $row[TableMap::TYPE_NUM == $indexType ? 17 + $startcol : CommuneTableMap::translateFieldName('Url', TableMap::TYPE_PHPNAME, $indexType)];
            $this->url = (null !== $col) ? (string) $col : null;

            $col = $row[TableMap::TYPE_NUM == $indexType ? 18 + $startcol : CommuneTableMap::translateFieldName('Zonage', TableMap::TYPE_PHPNAME, $indexType)];
            $this->zonage = (null !== $col) ? (string) $col : null;

            $col = $row[TableMap::TYPE_NUM == $indexType ? 19 + $startcol : CommuneTableMap::translateFieldName('CreatedAt', TableMap::TYPE_PHPNAME, $indexType)];
            if ($col === '0000-00-00 00:00:00') {
                $col = null;
            }
            $this->created_at = (null !== $col) ? PropelDateTime::newInstance($col, null, 'DateTime') : null;

            $col = $row[TableMap::TYPE_NUM == $indexType ? 20 + $startcol : CommuneTableMap::translateFieldName('UpdatedAt', TableMap::TYPE_PHPNAME, $indexType)];
            if ($col === '0000-00-00 00:00:00') {
                $col = null;
            }
            $this->updated_at = (null !== $col) ? PropelDateTime::newInstance($col, null, 'DateTime') : null;
            $this->resetModified();

            $this->setNew(false);

            if ($rehydrate) {
                $this->ensureConsistency();
            }

            return $startcol + 21; // 21 = CommuneTableMap::NUM_HYDRATE_COLUMNS.

        } catch (Exception $e) {
            throw new PropelException(sprintf('Error populating %s object', '\\Commune'), 0, $e);
        }
    }

    /**
     * Checks and repairs the internal consistency of the object.
     *
     * This method is executed after an already-instantiated object is re-hydrated
     * from the database.  It exists to check any foreign keys to make sure that
     * the objects related to the current object are correct based on foreign key.
     *
     * You can override this method in the stub class, but you should always invoke
     * the base method from the overridden method (i.e. parent::ensureConsistency()),
     * in case your model changes.
     *
     * @throws PropelException
     */
    public function ensureConsistency()
    {
    } // ensureConsistency

    /**
     * Reloads this object from datastore based on primary key and (optionally) resets all associated objects.
     *
     * This will only work if the object has been saved and has a valid primary key set.
     *
     * @param      boolean $deep (optional) Whether to also de-associated any related objects.
     * @param      ConnectionInterface $con (optional) The ConnectionInterface connection to use.
     * @return void
     * @throws PropelException - if this object is deleted, unsaved or doesn't have pk match in db
     */
    public function reload($deep = false, ConnectionInterface $con = null)
    {
        if ($this->isDeleted()) {
            throw new PropelException("Cannot reload a deleted object.");
        }

        if ($this->isNew()) {
            throw new PropelException("Cannot reload an unsaved object.");
        }

        if ($con === null) {
            $con = Propel::getServiceContainer()->getReadConnection(CommuneTableMap::DATABASE_NAME);
        }

        // We don't need to alter the object instance pool; we're just modifying this instance
        // already in the pool.

        $dataFetcher = ChildCommuneQuery::create(null, $this->buildPkeyCriteria())->setFormatter(ModelCriteria::FORMAT_STATEMENT)->find($con);
        $row = $dataFetcher->fetch();
        $dataFetcher->close();
        if (!$row) {
            throw new PropelException('Cannot find matching row in the database to reload object values.');
        }
        $this->hydrate($row, 0, true, $dataFetcher->getIndexType()); // rehydrate

        if ($deep) {  // also de-associate any related objects?

        } // if (deep)
    }

    /**
     * Removes this object from datastore and sets delete attribute.
     *
     * @param      ConnectionInterface $con
     * @return void
     * @throws PropelException
     * @see Commune::setDeleted()
     * @see Commune::isDeleted()
     */
    public function delete(ConnectionInterface $con = null)
    {
        if ($this->isDeleted()) {
            throw new PropelException("This object has already been deleted.");
        }

        if ($con === null) {
            $con = Propel::getServiceContainer()->getWriteConnection(CommuneTableMap::DATABASE_NAME);
        }

        $con->transaction(function () use ($con) {
            $deleteQuery = ChildCommuneQuery::create()
                ->filterByPrimaryKey($this->getPrimaryKey());
            $ret = $this->preDelete($con);
            if ($ret) {
                $deleteQuery->delete($con);
                $this->postDelete($con);
                $this->setDeleted(true);
            }
        });
    }

    /**
     * Persists this object to the database.
     *
     * If the object is new, it inserts it; otherwise an update is performed.
     * All modified related objects will also be persisted in the doSave()
     * method.  This method wraps all precipitate database operations in a
     * single transaction.
     *
     * @param      ConnectionInterface $con
     * @return int             The number of rows affected by this insert/update and any referring fk objects' save() operations.
     * @throws PropelException
     * @see doSave()
     */
    public function save(ConnectionInterface $con = null)
    {
        if ($this->isDeleted()) {
            throw new PropelException("You cannot save an object that has been deleted.");
        }

        if ($this->alreadyInSave) {
            return 0;
        }

        if ($con === null) {
            $con = Propel::getServiceContainer()->getWriteConnection(CommuneTableMap::DATABASE_NAME);
        }

        return $con->transaction(function () use ($con) {
            $ret = $this->preSave($con);
            $isInsert = $this->isNew();
            if ($isInsert) {
                $ret = $ret && $this->preInsert($con);
                // timestampable behavior

                if (!$this->isColumnModified(CommuneTableMap::COL_CREATED_AT)) {
                    $this->setCreatedAt(\Propel\Runtime\Util\PropelDateTime::createHighPrecision());
                }
                if (!$this->isColumnModified(CommuneTableMap::COL_UPDATED_AT)) {
                    $this->setUpdatedAt(\Propel\Runtime\Util\PropelDateTime::createHighPrecision());
                }
            } else {
                $ret = $ret && $this->preUpdate($con);
                // timestampable behavior
                if ($this->isModified() && !$this->isColumnModified(CommuneTableMap::COL_UPDATED_AT)) {
                    $this->setUpdatedAt(\Propel\Runtime\Util\PropelDateTime::createHighPrecision());
                }
            }
            if ($ret) {
                $affectedRows = $this->doSave($con);
                if ($isInsert) {
                    $this->postInsert($con);
                } else {
                    $this->postUpdate($con);
                }
                $this->postSave($con);
                CommuneTableMap::addInstanceToPool($this);
            } else {
                $affectedRows = 0;
            }

            return $affectedRows;
        });
    }

    /**
     * Performs the work of inserting or updating the row in the database.
     *
     * If the object is new, it inserts it; otherwise an update is performed.
     * All related objects are also updated in this method.
     *
     * @param      ConnectionInterface $con
     * @return int             The number of rows affected by this insert/update and any referring fk objects' save() operations.
     * @throws PropelException
     * @see save()
     */
    protected function doSave(ConnectionInterface $con)
    {
        $affectedRows = 0; // initialize var to track total num of affected rows
        if (!$this->alreadyInSave) {
            $this->alreadyInSave = true;

            if ($this->isNew() || $this->isModified()) {
                // persist changes
                if ($this->isNew()) {
                    $this->doInsert($con);
                    $affectedRows += 1;
                } else {
                    $affectedRows += $this->doUpdate($con);
                }
                $this->resetModified();
            }

            $this->alreadyInSave = false;

        }

        return $affectedRows;
    } // doSave()

    /**
     * Insert the row in the database.
     *
     * @param      ConnectionInterface $con
     *
     * @throws PropelException
     * @see doSave()
     */
    protected function doInsert(ConnectionInterface $con)
    {
        $modifiedColumns = array();
        $index = 0;

        $this->modifiedColumns[CommuneTableMap::COL_ID_COMMUNE] = true;
        if (null !== $this->id_commune) {
            throw new PropelException('Cannot insert a value for auto-increment primary key (' . CommuneTableMap::COL_ID_COMMUNE . ')');
        }

         // check the columns in natural order for more readable SQL queries
        if ($this->isColumnModified(CommuneTableMap::COL_ID_COMMUNE)) {
            $modifiedColumns[':p' . $index++]  = '`id_commune`';
        }
        if ($this->isColumnModified(CommuneTableMap::COL_PAYS)) {
            $modifiedColumns[':p' . $index++]  = '`pays`';
        }
        if ($this->isColumnModified(CommuneTableMap::COL_REGION)) {
            $modifiedColumns[':p' . $index++]  = '`region`';
        }
        if ($this->isColumnModified(CommuneTableMap::COL_DEPARTEMENT)) {
            $modifiedColumns[':p' . $index++]  = '`departement`';
        }
        if ($this->isColumnModified(CommuneTableMap::COL_CODE)) {
            $modifiedColumns[':p' . $index++]  = '`code`';
        }
        if ($this->isColumnModified(CommuneTableMap::COL_ARRONDISSEMENT)) {
            $modifiedColumns[':p' . $index++]  = '`arrondissement`';
        }
        if ($this->isColumnModified(CommuneTableMap::COL_CANTON)) {
            $modifiedColumns[':p' . $index++]  = '`canton`';
        }
        if ($this->isColumnModified(CommuneTableMap::COL_TYPE_CHARNIERE)) {
            $modifiedColumns[':p' . $index++]  = '`type_charniere`';
        }
        if ($this->isColumnModified(CommuneTableMap::COL_ARTICLE)) {
            $modifiedColumns[':p' . $index++]  = '`article`';
        }
        if ($this->isColumnModified(CommuneTableMap::COL_NOM)) {
            $modifiedColumns[':p' . $index++]  = '`nom`';
        }
        if ($this->isColumnModified(CommuneTableMap::COL_LON)) {
            $modifiedColumns[':p' . $index++]  = '`lon`';
        }
        if ($this->isColumnModified(CommuneTableMap::COL_LAT)) {
            $modifiedColumns[':p' . $index++]  = '`lat`';
        }
        if ($this->isColumnModified(CommuneTableMap::COL_ZOOM)) {
            $modifiedColumns[':p' . $index++]  = '`zoom`';
        }
        if ($this->isColumnModified(CommuneTableMap::COL_ELEVATION)) {
            $modifiedColumns[':p' . $index++]  = '`elevation`';
        }
        if ($this->isColumnModified(CommuneTableMap::COL_ELEVATION_MOYENNE)) {
            $modifiedColumns[':p' . $index++]  = '`elevation_moyenne`';
        }
        if ($this->isColumnModified(CommuneTableMap::COL_POPULATION)) {
            $modifiedColumns[':p' . $index++]  = '`population`';
        }
        if ($this->isColumnModified(CommuneTableMap::COL_AUTRE_NOM)) {
            $modifiedColumns[':p' . $index++]  = '`autre_nom`';
        }
        if ($this->isColumnModified(CommuneTableMap::COL_URL)) {
            $modifiedColumns[':p' . $index++]  = '`url`';
        }
        if ($this->isColumnModified(CommuneTableMap::COL_ZONAGE)) {
            $modifiedColumns[':p' . $index++]  = '`zonage`';
        }
        if ($this->isColumnModified(CommuneTableMap::COL_CREATED_AT)) {
            $modifiedColumns[':p' . $index++]  = '`created_at`';
        }
        if ($this->isColumnModified(CommuneTableMap::COL_UPDATED_AT)) {
            $modifiedColumns[':p' . $index++]  = '`updated_at`';
        }

        $sql = sprintf(
            'INSERT INTO `geo_communes` (%s) VALUES (%s)',
            implode(', ', $modifiedColumns),
            implode(', ', array_keys($modifiedColumns))
        );

        try {
            $stmt = $con->prepare($sql);
            foreach ($modifiedColumns as $identifier => $columnName) {
                switch ($columnName) {
                    case '`id_commune`':
                        $stmt->bindValue($identifier, $this->id_commune, PDO::PARAM_INT);
                        break;
                    case '`pays`':
                        $stmt->bindValue($identifier, $this->pays, PDO::PARAM_STR);
                        break;
                    case '`region`':
                        $stmt->bindValue($identifier, $this->region, PDO::PARAM_INT);
                        break;
                    case '`departement`':
                        $stmt->bindValue($identifier, $this->departement, PDO::PARAM_STR);
                        break;
                    case '`code`':
                        $stmt->bindValue($identifier, $this->code, PDO::PARAM_INT);
                        break;
                    case '`arrondissement`':
                        $stmt->bindValue($identifier, (int) $this->arrondissement, PDO::PARAM_INT);
                        break;
                    case '`canton`':
                        $stmt->bindValue($identifier, $this->canton, PDO::PARAM_INT);
                        break;
                    case '`type_charniere`':
                        $stmt->bindValue($identifier, (int) $this->type_charniere, PDO::PARAM_INT);
                        break;
                    case '`article`':
                        $stmt->bindValue($identifier, $this->article, PDO::PARAM_STR);
                        break;
                    case '`nom`':
                        $stmt->bindValue($identifier, $this->nom, PDO::PARAM_STR);
                        break;
                    case '`lon`':
                        $stmt->bindValue($identifier, $this->lon, PDO::PARAM_STR);
                        break;
                    case '`lat`':
                        $stmt->bindValue($identifier, $this->lat, PDO::PARAM_STR);
                        break;
                    case '`zoom`':
                        $stmt->bindValue($identifier, $this->zoom, PDO::PARAM_INT);
                        break;
                    case '`elevation`':
                        $stmt->bindValue($identifier, $this->elevation, PDO::PARAM_INT);
                        break;
                    case '`elevation_moyenne`':
                        $stmt->bindValue($identifier, $this->elevation_moyenne, PDO::PARAM_INT);
                        break;
                    case '`population`':
                        $stmt->bindValue($identifier, $this->population, PDO::PARAM_INT);
                        break;
                    case '`autre_nom`':
                        $stmt->bindValue($identifier, $this->autre_nom, PDO::PARAM_STR);
                        break;
                    case '`url`':
                        $stmt->bindValue($identifier, $this->url, PDO::PARAM_STR);
                        break;
                    case '`zonage`':
                        $stmt->bindValue($identifier, $this->zonage, PDO::PARAM_STR);
                        break;
                    case '`created_at`':
                        $stmt->bindValue($identifier, $this->created_at ? $this->created_at->format("Y-m-d H:i:s.u") : null, PDO::PARAM_STR);
                        break;
                    case '`updated_at`':
                        $stmt->bindValue($identifier, $this->updated_at ? $this->updated_at->format("Y-m-d H:i:s.u") : null, PDO::PARAM_STR);
                        break;
                }
            }
            $stmt->execute();
        } catch (Exception $e) {
            Propel::log($e->getMessage(), Propel::LOG_ERR);
            throw new PropelException(sprintf('Unable to execute INSERT statement [%s]', $sql), 0, $e);
        }

        try {
            $pk = $con->lastInsertId();
        } catch (Exception $e) {
            throw new PropelException('Unable to get autoincrement id.', 0, $e);
        }
        $this->setIdCommune($pk);

        $this->setNew(false);
    }

    /**
     * Update the row in the database.
     *
     * @param      ConnectionInterface $con
     *
     * @return Integer Number of updated rows
     * @see doSave()
     */
    protected function doUpdate(ConnectionInterface $con)
    {
        $selectCriteria = $this->buildPkeyCriteria();
        $valuesCriteria = $this->buildCriteria();

        return $selectCriteria->doUpdate($valuesCriteria, $con);
    }

    /**
     * Retrieves a field from the object by name passed in as a string.
     *
     * @param      string $name name
     * @param      string $type The type of fieldname the $name is of:
     *                     one of the class type constants TableMap::TYPE_PHPNAME, TableMap::TYPE_CAMELNAME
     *                     TableMap::TYPE_COLNAME, TableMap::TYPE_FIELDNAME, TableMap::TYPE_NUM.
     *                     Defaults to TableMap::TYPE_FIELDNAME.
     * @return mixed Value of field.
     */
    public function getByName($name, $type = TableMap::TYPE_FIELDNAME)
    {
        $pos = CommuneTableMap::translateFieldName($name, $type, TableMap::TYPE_NUM);
        $field = $this->getByPosition($pos);

        return $field;
    }

    /**
     * Retrieves a field from the object by Position as specified in the xml schema.
     * Zero-based.
     *
     * @param      int $pos position in xml schema
     * @return mixed Value of field at $pos
     */
    public function getByPosition($pos)
    {
        switch ($pos) {
            case 0:
                return $this->getIdCommune();
                break;
            case 1:
                return $this->getPays();
                break;
            case 2:
                return $this->getRegion();
                break;
            case 3:
                return $this->getDepartement();
                break;
            case 4:
                return $this->getCode();
                break;
            case 5:
                return $this->getArrondissement();
                break;
            case 6:
                return $this->getCanton();
                break;
            case 7:
                return $this->getTypeCharniere();
                break;
            case 8:
                return $this->getArticle();
                break;
            case 9:
                return $this->getNom();
                break;
            case 10:
                return $this->getLon();
                break;
            case 11:
                return $this->getLat();
                break;
            case 12:
                return $this->getZoom();
                break;
            case 13:
                return $this->getElevation();
                break;
            case 14:
                return $this->getElevationMoyenne();
                break;
            case 15:
                return $this->getPopulation();
                break;
            case 16:
                return $this->getAutreNom();
                break;
            case 17:
                return $this->getUrl();
                break;
            case 18:
                return $this->getZonage();
                break;
            case 19:
                return $this->getCreatedAt();
                break;
            case 20:
                return $this->getUpdatedAt();
                break;
            default:
                return null;
                break;
        } // switch()
    }

    /**
     * Exports the object as an array.
     *
     * You can specify the key type of the array by passing one of the class
     * type constants.
     *
     * @param     string  $keyType (optional) One of the class type constants TableMap::TYPE_PHPNAME, TableMap::TYPE_CAMELNAME,
     *                    TableMap::TYPE_COLNAME, TableMap::TYPE_FIELDNAME, TableMap::TYPE_NUM.
     *                    Defaults to TableMap::TYPE_FIELDNAME.
     * @param     boolean $includeLazyLoadColumns (optional) Whether to include lazy loaded columns. Defaults to TRUE.
     * @param     array $alreadyDumpedObjects List of objects to skip to avoid recursion
     *
     * @return array an associative array containing the field names (as keys) and field values
     */
    public function toArray($keyType = TableMap::TYPE_FIELDNAME, $includeLazyLoadColumns = true, $alreadyDumpedObjects = array())
    {

        if (isset($alreadyDumpedObjects['Commune'][$this->hashCode()])) {
            return '*RECURSION*';
        }
        $alreadyDumpedObjects['Commune'][$this->hashCode()] = true;
        $keys = CommuneTableMap::getFieldNames($keyType);
        $result = array(
            $keys[0] => $this->getIdCommune(),
            $keys[1] => $this->getPays(),
            $keys[2] => $this->getRegion(),
            $keys[3] => $this->getDepartement(),
            $keys[4] => $this->getCode(),
            $keys[5] => $this->getArrondissement(),
            $keys[6] => $this->getCanton(),
            $keys[7] => $this->getTypeCharniere(),
            $keys[8] => $this->getArticle(),
            $keys[9] => $this->getNom(),
            $keys[10] => $this->getLon(),
            $keys[11] => $this->getLat(),
            $keys[12] => $this->getZoom(),
            $keys[13] => $this->getElevation(),
            $keys[14] => $this->getElevationMoyenne(),
            $keys[15] => $this->getPopulation(),
            $keys[16] => $this->getAutreNom(),
            $keys[17] => $this->getUrl(),
            $keys[18] => $this->getZonage(),
            $keys[19] => $this->getCreatedAt(),
            $keys[20] => $this->getUpdatedAt(),
        );
        if ($result[$keys[19]] instanceof \DateTimeInterface) {
            $result[$keys[19]] = $result[$keys[19]]->format('c');
        }

        if ($result[$keys[20]] instanceof \DateTimeInterface) {
            $result[$keys[20]] = $result[$keys[20]]->format('c');
        }

        $virtualColumns = $this->virtualColumns;
        foreach ($virtualColumns as $key => $virtualColumn) {
            $result[$key] = $virtualColumn;
        }


        return $result;
    }

    /**
     * Sets a field from the object by name passed in as a string.
     *
     * @param  string $name
     * @param  mixed  $value field value
     * @param  string $type The type of fieldname the $name is of:
     *                one of the class type constants TableMap::TYPE_PHPNAME, TableMap::TYPE_CAMELNAME
     *                TableMap::TYPE_COLNAME, TableMap::TYPE_FIELDNAME, TableMap::TYPE_NUM.
     *                Defaults to TableMap::TYPE_FIELDNAME.
     * @return $this|\Commune
     */
    public function setByName($name, $value, $type = TableMap::TYPE_FIELDNAME)
    {
        $pos = CommuneTableMap::translateFieldName($name, $type, TableMap::TYPE_NUM);

        return $this->setByPosition($pos, $value);
    }

    /**
     * Sets a field from the object by Position as specified in the xml schema.
     * Zero-based.
     *
     * @param  int $pos position in xml schema
     * @param  mixed $value field value
     * @return $this|\Commune
     */
    public function setByPosition($pos, $value)
    {
        switch ($pos) {
            case 0:
                $this->setIdCommune($value);
                break;
            case 1:
                $this->setPays($value);
                break;
            case 2:
                $this->setRegion($value);
                break;
            case 3:
                $this->setDepartement($value);
                break;
            case 4:
                $this->setCode($value);
                break;
            case 5:
                $this->setArrondissement($value);
                break;
            case 6:
                $this->setCanton($value);
                break;
            case 7:
                $this->setTypeCharniere($value);
                break;
            case 8:
                $this->setArticle($value);
                break;
            case 9:
                $this->setNom($value);
                break;
            case 10:
                $this->setLon($value);
                break;
            case 11:
                $this->setLat($value);
                break;
            case 12:
                $this->setZoom($value);
                break;
            case 13:
                $this->setElevation($value);
                break;
            case 14:
                $this->setElevationMoyenne($value);
                break;
            case 15:
                $this->setPopulation($value);
                break;
            case 16:
                $this->setAutreNom($value);
                break;
            case 17:
                $this->setUrl($value);
                break;
            case 18:
                $this->setZonage($value);
                break;
            case 19:
                $this->setCreatedAt($value);
                break;
            case 20:
                $this->setUpdatedAt($value);
                break;
        } // switch()

        return $this;
    }

    /**
     * Populates the object using an array.
     *
     * This is particularly useful when populating an object from one of the
     * request arrays (e.g. $_POST).  This method goes through the column
     * names, checking to see whether a matching key exists in populated
     * array. If so the setByName() method is called for that column.
     *
     * You can specify the key type of the array by additionally passing one
     * of the class type constants TableMap::TYPE_PHPNAME, TableMap::TYPE_CAMELNAME,
     * TableMap::TYPE_COLNAME, TableMap::TYPE_FIELDNAME, TableMap::TYPE_NUM.
     * The default key type is the column's TableMap::TYPE_FIELDNAME.
     *
     * @param      array  $arr     An array to populate the object from.
     * @param      string $keyType The type of keys the array uses.
     * @return void
     */
    public function fromArray($arr, $keyType = TableMap::TYPE_FIELDNAME)
    {
        $keys = CommuneTableMap::getFieldNames($keyType);

        if (array_key_exists($keys[0], $arr)) {
            $this->setIdCommune($arr[$keys[0]]);
        }
        if (array_key_exists($keys[1], $arr)) {
            $this->setPays($arr[$keys[1]]);
        }
        if (array_key_exists($keys[2], $arr)) {
            $this->setRegion($arr[$keys[2]]);
        }
        if (array_key_exists($keys[3], $arr)) {
            $this->setDepartement($arr[$keys[3]]);
        }
        if (array_key_exists($keys[4], $arr)) {
            $this->setCode($arr[$keys[4]]);
        }
        if (array_key_exists($keys[5], $arr)) {
            $this->setArrondissement($arr[$keys[5]]);
        }
        if (array_key_exists($keys[6], $arr)) {
            $this->setCanton($arr[$keys[6]]);
        }
        if (array_key_exists($keys[7], $arr)) {
            $this->setTypeCharniere($arr[$keys[7]]);
        }
        if (array_key_exists($keys[8], $arr)) {
            $this->setArticle($arr[$keys[8]]);
        }
        if (array_key_exists($keys[9], $arr)) {
            $this->setNom($arr[$keys[9]]);
        }
        if (array_key_exists($keys[10], $arr)) {
            $this->setLon($arr[$keys[10]]);
        }
        if (array_key_exists($keys[11], $arr)) {
            $this->setLat($arr[$keys[11]]);
        }
        if (array_key_exists($keys[12], $arr)) {
            $this->setZoom($arr[$keys[12]]);
        }
        if (array_key_exists($keys[13], $arr)) {
            $this->setElevation($arr[$keys[13]]);
        }
        if (array_key_exists($keys[14], $arr)) {
            $this->setElevationMoyenne($arr[$keys[14]]);
        }
        if (array_key_exists($keys[15], $arr)) {
            $this->setPopulation($arr[$keys[15]]);
        }
        if (array_key_exists($keys[16], $arr)) {
            $this->setAutreNom($arr[$keys[16]]);
        }
        if (array_key_exists($keys[17], $arr)) {
            $this->setUrl($arr[$keys[17]]);
        }
        if (array_key_exists($keys[18], $arr)) {
            $this->setZonage($arr[$keys[18]]);
        }
        if (array_key_exists($keys[19], $arr)) {
            $this->setCreatedAt($arr[$keys[19]]);
        }
        if (array_key_exists($keys[20], $arr)) {
            $this->setUpdatedAt($arr[$keys[20]]);
        }
    }

     /**
     * Populate the current object from a string, using a given parser format
     * <code>
     * $book = new Book();
     * $book->importFrom('JSON', '{"Id":9012,"Title":"Don Juan","ISBN":"0140422161","Price":12.99,"PublisherId":1234,"AuthorId":5678}');
     * </code>
     *
     * You can specify the key type of the array by additionally passing one
     * of the class type constants TableMap::TYPE_PHPNAME, TableMap::TYPE_CAMELNAME,
     * TableMap::TYPE_COLNAME, TableMap::TYPE_FIELDNAME, TableMap::TYPE_NUM.
     * The default key type is the column's TableMap::TYPE_FIELDNAME.
     *
     * @param mixed $parser A AbstractParser instance,
     *                       or a format name ('XML', 'YAML', 'JSON', 'CSV')
     * @param string $data The source data to import from
     * @param string $keyType The type of keys the array uses.
     *
     * @return $this|\Commune The current object, for fluid interface
     */
    public function importFrom($parser, $data, $keyType = TableMap::TYPE_FIELDNAME)
    {
        if (!$parser instanceof AbstractParser) {
            $parser = AbstractParser::getParser($parser);
        }

        $this->fromArray($parser->toArray($data), $keyType);

        return $this;
    }

    /**
     * Build a Criteria object containing the values of all modified columns in this object.
     *
     * @return Criteria The Criteria object containing all modified values.
     */
    public function buildCriteria()
    {
        $criteria = new Criteria(CommuneTableMap::DATABASE_NAME);

        if ($this->isColumnModified(CommuneTableMap::COL_ID_COMMUNE)) {
            $criteria->add(CommuneTableMap::COL_ID_COMMUNE, $this->id_commune);
        }
        if ($this->isColumnModified(CommuneTableMap::COL_PAYS)) {
            $criteria->add(CommuneTableMap::COL_PAYS, $this->pays);
        }
        if ($this->isColumnModified(CommuneTableMap::COL_REGION)) {
            $criteria->add(CommuneTableMap::COL_REGION, $this->region);
        }
        if ($this->isColumnModified(CommuneTableMap::COL_DEPARTEMENT)) {
            $criteria->add(CommuneTableMap::COL_DEPARTEMENT, $this->departement);
        }
        if ($this->isColumnModified(CommuneTableMap::COL_CODE)) {
            $criteria->add(CommuneTableMap::COL_CODE, $this->code);
        }
        if ($this->isColumnModified(CommuneTableMap::COL_ARRONDISSEMENT)) {
            $criteria->add(CommuneTableMap::COL_ARRONDISSEMENT, $this->arrondissement);
        }
        if ($this->isColumnModified(CommuneTableMap::COL_CANTON)) {
            $criteria->add(CommuneTableMap::COL_CANTON, $this->canton);
        }
        if ($this->isColumnModified(CommuneTableMap::COL_TYPE_CHARNIERE)) {
            $criteria->add(CommuneTableMap::COL_TYPE_CHARNIERE, $this->type_charniere);
        }
        if ($this->isColumnModified(CommuneTableMap::COL_ARTICLE)) {
            $criteria->add(CommuneTableMap::COL_ARTICLE, $this->article);
        }
        if ($this->isColumnModified(CommuneTableMap::COL_NOM)) {
            $criteria->add(CommuneTableMap::COL_NOM, $this->nom);
        }
        if ($this->isColumnModified(CommuneTableMap::COL_LON)) {
            $criteria->add(CommuneTableMap::COL_LON, $this->lon);
        }
        if ($this->isColumnModified(CommuneTableMap::COL_LAT)) {
            $criteria->add(CommuneTableMap::COL_LAT, $this->lat);
        }
        if ($this->isColumnModified(CommuneTableMap::COL_ZOOM)) {
            $criteria->add(CommuneTableMap::COL_ZOOM, $this->zoom);
        }
        if ($this->isColumnModified(CommuneTableMap::COL_ELEVATION)) {
            $criteria->add(CommuneTableMap::COL_ELEVATION, $this->elevation);
        }
        if ($this->isColumnModified(CommuneTableMap::COL_ELEVATION_MOYENNE)) {
            $criteria->add(CommuneTableMap::COL_ELEVATION_MOYENNE, $this->elevation_moyenne);
        }
        if ($this->isColumnModified(CommuneTableMap::COL_POPULATION)) {
            $criteria->add(CommuneTableMap::COL_POPULATION, $this->population);
        }
        if ($this->isColumnModified(CommuneTableMap::COL_AUTRE_NOM)) {
            $criteria->add(CommuneTableMap::COL_AUTRE_NOM, $this->autre_nom);
        }
        if ($this->isColumnModified(CommuneTableMap::COL_URL)) {
            $criteria->add(CommuneTableMap::COL_URL, $this->url);
        }
        if ($this->isColumnModified(CommuneTableMap::COL_ZONAGE)) {
            $criteria->add(CommuneTableMap::COL_ZONAGE, $this->zonage);
        }
        if ($this->isColumnModified(CommuneTableMap::COL_CREATED_AT)) {
            $criteria->add(CommuneTableMap::COL_CREATED_AT, $this->created_at);
        }
        if ($this->isColumnModified(CommuneTableMap::COL_UPDATED_AT)) {
            $criteria->add(CommuneTableMap::COL_UPDATED_AT, $this->updated_at);
        }

        return $criteria;
    }

    /**
     * Builds a Criteria object containing the primary key for this object.
     *
     * Unlike buildCriteria() this method includes the primary key values regardless
     * of whether or not they have been modified.
     *
     * @throws LogicException if no primary key is defined
     *
     * @return Criteria The Criteria object containing value(s) for primary key(s).
     */
    public function buildPkeyCriteria()
    {
        $criteria = ChildCommuneQuery::create();
        $criteria->add(CommuneTableMap::COL_ID_COMMUNE, $this->id_commune);

        return $criteria;
    }

    /**
     * If the primary key is not null, return the hashcode of the
     * primary key. Otherwise, return the hash code of the object.
     *
     * @return int Hashcode
     */
    public function hashCode()
    {
        $validPk = null !== $this->getIdCommune();

        $validPrimaryKeyFKs = 0;
        $primaryKeyFKs = [];

        if ($validPk) {
            return crc32(json_encode($this->getPrimaryKey(), JSON_UNESCAPED_UNICODE));
        } elseif ($validPrimaryKeyFKs) {
            return crc32(json_encode($primaryKeyFKs, JSON_UNESCAPED_UNICODE));
        }

        return spl_object_hash($this);
    }

    /**
     * Returns the primary key for this object (row).
     * @return int
     */
    public function getPrimaryKey()
    {
        return $this->getIdCommune();
    }

    /**
     * Generic method to set the primary key (id_commune column).
     *
     * @param       int $key Primary key.
     * @return void
     */
    public function setPrimaryKey($key)
    {
        $this->setIdCommune($key);
    }

    /**
     * Returns true if the primary key for this object is null.
     * @return boolean
     */
    public function isPrimaryKeyNull()
    {
        return null === $this->getIdCommune();
    }

    /**
     * Sets contents of passed object to values from current object.
     *
     * If desired, this method can also make copies of all associated (fkey referrers)
     * objects.
     *
     * @param      object $copyObj An object of \Commune (or compatible) type.
     * @param      boolean $deepCopy Whether to also copy all rows that refer (by fkey) to the current row.
     * @param      boolean $makeNew Whether to reset autoincrement PKs and make the object new.
     * @throws PropelException
     */
    public function copyInto($copyObj, $deepCopy = false, $makeNew = true)
    {
        $copyObj->setPays($this->getPays());
        $copyObj->setRegion($this->getRegion());
        $copyObj->setDepartement($this->getDepartement());
        $copyObj->setCode($this->getCode());
        $copyObj->setArrondissement($this->getArrondissement());
        $copyObj->setCanton($this->getCanton());
        $copyObj->setTypeCharniere($this->getTypeCharniere());
        $copyObj->setArticle($this->getArticle());
        $copyObj->setNom($this->getNom());
        $copyObj->setLon($this->getLon());
        $copyObj->setLat($this->getLat());
        $copyObj->setZoom($this->getZoom());
        $copyObj->setElevation($this->getElevation());
        $copyObj->setElevationMoyenne($this->getElevationMoyenne());
        $copyObj->setPopulation($this->getPopulation());
        $copyObj->setAutreNom($this->getAutreNom());
        $copyObj->setUrl($this->getUrl());
        $copyObj->setZonage($this->getZonage());
        $copyObj->setCreatedAt($this->getCreatedAt());
        $copyObj->setUpdatedAt($this->getUpdatedAt());
        if ($makeNew) {
            $copyObj->setNew(true);
            $copyObj->setIdCommune(NULL); // this is a auto-increment column, so set to default value
        }
    }

    /**
     * Makes a copy of this object that will be inserted as a new row in table when saved.
     * It creates a new object filling in the simple attributes, but skipping any primary
     * keys that are defined for the table.
     *
     * If desired, this method can also make copies of all associated (fkey referrers)
     * objects.
     *
     * @param  boolean $deepCopy Whether to also copy all rows that refer (by fkey) to the current row.
     * @return \Commune Clone of current object.
     * @throws PropelException
     */
    public function copy($deepCopy = false)
    {
        // we use get_class(), because this might be a subclass
        $clazz = get_class($this);
        $copyObj = new $clazz();
        $this->copyInto($copyObj, $deepCopy);

        return $copyObj;
    }

    /**
     * Clears the current object, sets all attributes to their default values and removes
     * outgoing references as well as back-references (from other objects to this one. Results probably in a database
     * change of those foreign objects when you call `save` there).
     */
    public function clear()
    {
        $this->id_commune = null;
        $this->pays = null;
        $this->region = null;
        $this->departement = null;
        $this->code = null;
        $this->arrondissement = null;
        $this->canton = null;
        $this->type_charniere = null;
        $this->article = null;
        $this->nom = null;
        $this->lon = null;
        $this->lat = null;
        $this->zoom = null;
        $this->elevation = null;
        $this->elevation_moyenne = null;
        $this->population = null;
        $this->autre_nom = null;
        $this->url = null;
        $this->zonage = null;
        $this->created_at = null;
        $this->updated_at = null;
        $this->alreadyInSave = false;
        $this->clearAllReferences();
        $this->resetModified();
        $this->setNew(true);
        $this->setDeleted(false);
    }

    /**
     * Resets all references and back-references to other model objects or collections of model objects.
     *
     * This method is used to reset all php object references (not the actual reference in the database).
     * Necessary for object serialisation.
     *
     * @param      boolean $deep Whether to also clear the references on all referrer objects.
     */
    public function clearAllReferences($deep = false)
    {
        if ($deep) {
        } // if ($deep)

    }

    /**
     * Return the string representation of this object
     *
     * @return string
     */
    public function __toString()
    {
        return (string) $this->exportTo(CommuneTableMap::DEFAULT_STRING_FORMAT);
    }

    // timestampable behavior

    /**
     * Mark the current object so that the update date doesn't get updated during next save
     *
     * @return     $this|ChildCommune The current object (for fluent API support)
     */
    public function keepUpdateDateUnchanged()
    {
        $this->modifiedColumns[CommuneTableMap::COL_UPDATED_AT] = true;

        return $this;
    }

    /**
     * Code to be run before persisting the object
     * @param  ConnectionInterface $con
     * @return boolean
     */
    public function preSave(ConnectionInterface $con = null)
    {
        if (is_callable('parent::preSave')) {
            return parent::preSave($con);
        }
        return true;
    }

    /**
     * Code to be run after persisting the object
     * @param ConnectionInterface $con
     */
    public function postSave(ConnectionInterface $con = null)
    {
        if (is_callable('parent::postSave')) {
            parent::postSave($con);
        }
    }

    /**
     * Code to be run before inserting to database
     * @param  ConnectionInterface $con
     * @return boolean
     */
    public function preInsert(ConnectionInterface $con = null)
    {
        if (is_callable('parent::preInsert')) {
            return parent::preInsert($con);
        }
        return true;
    }

    /**
     * Code to be run after inserting to database
     * @param ConnectionInterface $con
     */
    public function postInsert(ConnectionInterface $con = null)
    {
        if (is_callable('parent::postInsert')) {
            parent::postInsert($con);
        }
    }

    /**
     * Code to be run before updating the object in database
     * @param  ConnectionInterface $con
     * @return boolean
     */
    public function preUpdate(ConnectionInterface $con = null)
    {
        if (is_callable('parent::preUpdate')) {
            return parent::preUpdate($con);
        }
        return true;
    }

    /**
     * Code to be run after updating the object in database
     * @param ConnectionInterface $con
     */
    public function postUpdate(ConnectionInterface $con = null)
    {
        if (is_callable('parent::postUpdate')) {
            parent::postUpdate($con);
        }
    }

    /**
     * Code to be run before deleting the object in database
     * @param  ConnectionInterface $con
     * @return boolean
     */
    public function preDelete(ConnectionInterface $con = null)
    {
        if (is_callable('parent::preDelete')) {
            return parent::preDelete($con);
        }
        return true;
    }

    /**
     * Code to be run after deleting the object in database
     * @param ConnectionInterface $con
     */
    public function postDelete(ConnectionInterface $con = null)
    {
        if (is_callable('parent::postDelete')) {
            parent::postDelete($con);
        }
    }


    /**
     * Derived method to catches calls to undefined methods.
     *
     * Provides magic import/export method support (fromXML()/toXML(), fromYAML()/toYAML(), etc.).
     * Allows to define default __call() behavior if you overwrite __call()
     *
     * @param string $name
     * @param mixed  $params
     *
     * @return array|string
     */
    public function __call($name, $params)
    {
        if (0 === strpos($name, 'get')) {
            $virtualColumn = substr($name, 3);
            if ($this->hasVirtualColumn($virtualColumn)) {
                return $this->getVirtualColumn($virtualColumn);
            }

            $virtualColumn = lcfirst($virtualColumn);
            if ($this->hasVirtualColumn($virtualColumn)) {
                return $this->getVirtualColumn($virtualColumn);
            }
        }

        if (0 === strpos($name, 'from')) {
            $format = substr($name, 4);

            return $this->importFrom($format, reset($params));
        }

        if (0 === strpos($name, 'to')) {
            $format = substr($name, 2);
            $includeLazyLoadColumns = isset($params[0]) ? $params[0] : true;

            return $this->exportTo($format, $includeLazyLoadColumns);
        }

        throw new BadMethodCallException(sprintf('Call to undefined method: %s.', $name));
    }

}
