<?php

namespace Base;

use \AssoMotsArchive as ChildAssoMotsArchive;
use \Mot as ChildMot;
use \MotQuery as ChildMotQuery;
use \Exception;
use \PDO;
use Map\MotTableMap;
use Propel\Runtime\Propel;
use Propel\Runtime\ActiveQuery\Criteria;
use Propel\Runtime\ActiveQuery\ModelCriteria;
use Propel\Runtime\ActiveQuery\ModelJoin;
use Propel\Runtime\Collection\ObjectCollection;
use Propel\Runtime\Connection\ConnectionInterface;
use Propel\Runtime\Exception\PropelException;

/**
 * Base class that represents a query for the 'asso_mots' table.
 *
 *
 *
 * @method     ChildMotQuery orderByIdMot($order = Criteria::ASC) Order by the id_mot column
 * @method     ChildMotQuery orderByNom($order = Criteria::ASC) Order by the nom column
 * @method     ChildMotQuery orderByNomcourt($order = Criteria::ASC) Order by the nomcourt column
 * @method     ChildMotQuery orderByDescriptif($order = Criteria::ASC) Order by the descriptif column
 * @method     ChildMotQuery orderByTexte($order = Criteria::ASC) Order by the texte column
 * @method     ChildMotQuery orderByImportance($order = Criteria::ASC) Order by the importance column
 * @method     ChildMotQuery orderByActif($order = Criteria::ASC) Order by the actif column
 * @method     ChildMotQuery orderByIdMotgroupe($order = Criteria::ASC) Order by the id_motgroupe column
 * @method     ChildMotQuery orderByCreatedAt($order = Criteria::ASC) Order by the created_at column
 * @method     ChildMotQuery orderByUpdatedAt($order = Criteria::ASC) Order by the updated_at column
 *
 * @method     ChildMotQuery groupByIdMot() Group by the id_mot column
 * @method     ChildMotQuery groupByNom() Group by the nom column
 * @method     ChildMotQuery groupByNomcourt() Group by the nomcourt column
 * @method     ChildMotQuery groupByDescriptif() Group by the descriptif column
 * @method     ChildMotQuery groupByTexte() Group by the texte column
 * @method     ChildMotQuery groupByImportance() Group by the importance column
 * @method     ChildMotQuery groupByActif() Group by the actif column
 * @method     ChildMotQuery groupByIdMotgroupe() Group by the id_motgroupe column
 * @method     ChildMotQuery groupByCreatedAt() Group by the created_at column
 * @method     ChildMotQuery groupByUpdatedAt() Group by the updated_at column
 *
 * @method     ChildMotQuery leftJoin($relation) Adds a LEFT JOIN clause to the query
 * @method     ChildMotQuery rightJoin($relation) Adds a RIGHT JOIN clause to the query
 * @method     ChildMotQuery innerJoin($relation) Adds a INNER JOIN clause to the query
 *
 * @method     ChildMotQuery leftJoinWith($relation) Adds a LEFT JOIN clause and with to the query
 * @method     ChildMotQuery rightJoinWith($relation) Adds a RIGHT JOIN clause and with to the query
 * @method     ChildMotQuery innerJoinWith($relation) Adds a INNER JOIN clause and with to the query
 *
 * @method     ChildMotQuery leftJoinMotgroupe($relationAlias = null) Adds a LEFT JOIN clause to the query using the Motgroupe relation
 * @method     ChildMotQuery rightJoinMotgroupe($relationAlias = null) Adds a RIGHT JOIN clause to the query using the Motgroupe relation
 * @method     ChildMotQuery innerJoinMotgroupe($relationAlias = null) Adds a INNER JOIN clause to the query using the Motgroupe relation
 *
 * @method     ChildMotQuery joinWithMotgroupe($joinType = Criteria::INNER_JOIN) Adds a join clause and with to the query using the Motgroupe relation
 *
 * @method     ChildMotQuery leftJoinWithMotgroupe() Adds a LEFT JOIN clause and with to the query using the Motgroupe relation
 * @method     ChildMotQuery rightJoinWithMotgroupe() Adds a RIGHT JOIN clause and with to the query using the Motgroupe relation
 * @method     ChildMotQuery innerJoinWithMotgroupe() Adds a INNER JOIN clause and with to the query using the Motgroupe relation
 *
 * @method     ChildMotQuery leftJoinMotLien($relationAlias = null) Adds a LEFT JOIN clause to the query using the MotLien relation
 * @method     ChildMotQuery rightJoinMotLien($relationAlias = null) Adds a RIGHT JOIN clause to the query using the MotLien relation
 * @method     ChildMotQuery innerJoinMotLien($relationAlias = null) Adds a INNER JOIN clause to the query using the MotLien relation
 *
 * @method     ChildMotQuery joinWithMotLien($joinType = Criteria::INNER_JOIN) Adds a join clause and with to the query using the MotLien relation
 *
 * @method     ChildMotQuery leftJoinWithMotLien() Adds a LEFT JOIN clause and with to the query using the MotLien relation
 * @method     ChildMotQuery rightJoinWithMotLien() Adds a RIGHT JOIN clause and with to the query using the MotLien relation
 * @method     ChildMotQuery innerJoinWithMotLien() Adds a INNER JOIN clause and with to the query using the MotLien relation
 *
 * @method     \MotgroupeQuery|\MotLienQuery endUse() Finalizes a secondary criteria and merges it with its primary Criteria
 *
 * @method     ChildMot findOne(ConnectionInterface $con = null) Return the first ChildMot matching the query
 * @method     ChildMot findOneOrCreate(ConnectionInterface $con = null) Return the first ChildMot matching the query, or a new ChildMot object populated from the query conditions when no match is found
 *
 * @method     ChildMot findOneByIdMot(string $id_mot) Return the first ChildMot filtered by the id_mot column
 * @method     ChildMot findOneByNom(string $nom) Return the first ChildMot filtered by the nom column
 * @method     ChildMot findOneByNomcourt(string $nomcourt) Return the first ChildMot filtered by the nomcourt column
 * @method     ChildMot findOneByDescriptif(string $descriptif) Return the first ChildMot filtered by the descriptif column
 * @method     ChildMot findOneByTexte(string $texte) Return the first ChildMot filtered by the texte column
 * @method     ChildMot findOneByImportance(int $importance) Return the first ChildMot filtered by the importance column
 * @method     ChildMot findOneByActif(int $actif) Return the first ChildMot filtered by the actif column
 * @method     ChildMot findOneByIdMotgroupe(string $id_motgroupe) Return the first ChildMot filtered by the id_motgroupe column
 * @method     ChildMot findOneByCreatedAt(string $created_at) Return the first ChildMot filtered by the created_at column
 * @method     ChildMot findOneByUpdatedAt(string $updated_at) Return the first ChildMot filtered by the updated_at column *

 * @method     ChildMot requirePk($key, ConnectionInterface $con = null) Return the ChildMot by primary key and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildMot requireOne(ConnectionInterface $con = null) Return the first ChildMot matching the query and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 *
 * @method     ChildMot requireOneByIdMot(string $id_mot) Return the first ChildMot filtered by the id_mot column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildMot requireOneByNom(string $nom) Return the first ChildMot filtered by the nom column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildMot requireOneByNomcourt(string $nomcourt) Return the first ChildMot filtered by the nomcourt column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildMot requireOneByDescriptif(string $descriptif) Return the first ChildMot filtered by the descriptif column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildMot requireOneByTexte(string $texte) Return the first ChildMot filtered by the texte column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildMot requireOneByImportance(int $importance) Return the first ChildMot filtered by the importance column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildMot requireOneByActif(int $actif) Return the first ChildMot filtered by the actif column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildMot requireOneByIdMotgroupe(string $id_motgroupe) Return the first ChildMot filtered by the id_motgroupe column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildMot requireOneByCreatedAt(string $created_at) Return the first ChildMot filtered by the created_at column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildMot requireOneByUpdatedAt(string $updated_at) Return the first ChildMot filtered by the updated_at column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 *
 * @method     ChildMot[]|ObjectCollection find(ConnectionInterface $con = null) Return ChildMot objects based on current ModelCriteria
 * @method     ChildMot[]|ObjectCollection findByIdMot(string $id_mot) Return ChildMot objects filtered by the id_mot column
 * @method     ChildMot[]|ObjectCollection findByNom(string $nom) Return ChildMot objects filtered by the nom column
 * @method     ChildMot[]|ObjectCollection findByNomcourt(string $nomcourt) Return ChildMot objects filtered by the nomcourt column
 * @method     ChildMot[]|ObjectCollection findByDescriptif(string $descriptif) Return ChildMot objects filtered by the descriptif column
 * @method     ChildMot[]|ObjectCollection findByTexte(string $texte) Return ChildMot objects filtered by the texte column
 * @method     ChildMot[]|ObjectCollection findByImportance(int $importance) Return ChildMot objects filtered by the importance column
 * @method     ChildMot[]|ObjectCollection findByActif(int $actif) Return ChildMot objects filtered by the actif column
 * @method     ChildMot[]|ObjectCollection findByIdMotgroupe(string $id_motgroupe) Return ChildMot objects filtered by the id_motgroupe column
 * @method     ChildMot[]|ObjectCollection findByCreatedAt(string $created_at) Return ChildMot objects filtered by the created_at column
 * @method     ChildMot[]|ObjectCollection findByUpdatedAt(string $updated_at) Return ChildMot objects filtered by the updated_at column
 * @method     ChildMot[]|\Propel\Runtime\Util\PropelModelPager paginate($page = 1, $maxPerPage = 10, ConnectionInterface $con = null) Issue a SELECT query based on the current ModelCriteria and uses a page and a maximum number of results per page to compute an offset and a limit
 *
 */
abstract class MotQuery extends ModelCriteria
{

    // archivable behavior
    protected $archiveOnDelete = true;
protected $entityNotFoundExceptionClass = '\\Propel\\Runtime\\Exception\\EntityNotFoundException';

    /**
     * Initializes internal state of \Base\MotQuery object.
     *
     * @param     string $dbName The database name
     * @param     string $modelName The phpName of a model, e.g. 'Book'
     * @param     string $modelAlias The alias for the model in this query, e.g. 'b'
     */
    public function __construct($dbName = 'simplasso', $modelName = '\\Mot', $modelAlias = null)
    {
        parent::__construct($dbName, $modelName, $modelAlias);
    }

    /**
     * Returns a new ChildMotQuery object.
     *
     * @param     string $modelAlias The alias of a model in the query
     * @param     Criteria $criteria Optional Criteria to build the query from
     *
     * @return ChildMotQuery
     */
    public static function create($modelAlias = null, Criteria $criteria = null)
    {
        if ($criteria instanceof ChildMotQuery) {
            return $criteria;
        }
        $query = new ChildMotQuery();
        if (null !== $modelAlias) {
            $query->setModelAlias($modelAlias);
        }
        if ($criteria instanceof Criteria) {
            $query->mergeWith($criteria);
        }

        return $query;
    }

    /**
     * Find object by primary key.
     * Propel uses the instance pool to skip the database if the object exists.
     * Go fast if the query is untouched.
     *
     * <code>
     * $obj  = $c->findPk(12, $con);
     * </code>
     *
     * @param mixed $key Primary key to use for the query
     * @param ConnectionInterface $con an optional connection object
     *
     * @return ChildMot|array|mixed the result, formatted by the current formatter
     */
    public function findPk($key, ConnectionInterface $con = null)
    {
        if ($key === null) {
            return null;
        }

        if ($con === null) {
            $con = Propel::getServiceContainer()->getReadConnection(MotTableMap::DATABASE_NAME);
        }

        $this->basePreSelect($con);

        if (
            $this->formatter || $this->modelAlias || $this->with || $this->select
            || $this->selectColumns || $this->asColumns || $this->selectModifiers
            || $this->map || $this->having || $this->joins
        ) {
            return $this->findPkComplex($key, $con);
        }

        if ((null !== ($obj = MotTableMap::getInstanceFromPool(null === $key || is_scalar($key) || is_callable([$key, '__toString']) ? (string) $key : $key)))) {
            // the object is already in the instance pool
            return $obj;
        }

        return $this->findPkSimple($key, $con);
    }

    /**
     * Find object by primary key using raw SQL to go fast.
     * Bypass doSelect() and the object formatter by using generated code.
     *
     * @param     mixed $key Primary key to use for the query
     * @param     ConnectionInterface $con A connection object
     *
     * @throws \Propel\Runtime\Exception\PropelException
     *
     * @return ChildMot A model object, or null if the key is not found
     */
    protected function findPkSimple($key, ConnectionInterface $con)
    {
        $sql = 'SELECT `id_mot`, `nom`, `nomcourt`, `descriptif`, `texte`, `importance`, `actif`, `id_motgroupe`, `created_at`, `updated_at` FROM `asso_mots` WHERE `id_mot` = :p0';
        try {
            $stmt = $con->prepare($sql);
            $stmt->bindValue(':p0', $key, PDO::PARAM_INT);
            $stmt->execute();
        } catch (Exception $e) {
            Propel::log($e->getMessage(), Propel::LOG_ERR);
            throw new PropelException(sprintf('Unable to execute SELECT statement [%s]', $sql), 0, $e);
        }
        $obj = null;
        if ($row = $stmt->fetch(\PDO::FETCH_NUM)) {
            /** @var ChildMot $obj */
            $obj = new ChildMot();
            $obj->hydrate($row);
            MotTableMap::addInstanceToPool($obj, null === $key || is_scalar($key) || is_callable([$key, '__toString']) ? (string) $key : $key);
        }
        $stmt->closeCursor();

        return $obj;
    }

    /**
     * Find object by primary key.
     *
     * @param     mixed $key Primary key to use for the query
     * @param     ConnectionInterface $con A connection object
     *
     * @return ChildMot|array|mixed the result, formatted by the current formatter
     */
    protected function findPkComplex($key, ConnectionInterface $con)
    {
        // As the query uses a PK condition, no limit(1) is necessary.
        $criteria = $this->isKeepQuery() ? clone $this : $this;
        $dataFetcher = $criteria
            ->filterByPrimaryKey($key)
            ->doSelect($con);

        return $criteria->getFormatter()->init($criteria)->formatOne($dataFetcher);
    }

    /**
     * Find objects by primary key
     * <code>
     * $objs = $c->findPks(array(12, 56, 832), $con);
     * </code>
     * @param     array $keys Primary keys to use for the query
     * @param     ConnectionInterface $con an optional connection object
     *
     * @return ObjectCollection|array|mixed the list of results, formatted by the current formatter
     */
    public function findPks($keys, ConnectionInterface $con = null)
    {
        if (null === $con) {
            $con = Propel::getServiceContainer()->getReadConnection($this->getDbName());
        }
        $this->basePreSelect($con);
        $criteria = $this->isKeepQuery() ? clone $this : $this;
        $dataFetcher = $criteria
            ->filterByPrimaryKeys($keys)
            ->doSelect($con);

        return $criteria->getFormatter()->init($criteria)->format($dataFetcher);
    }

    /**
     * Filter the query by primary key
     *
     * @param     mixed $key Primary key to use for the query
     *
     * @return $this|ChildMotQuery The current query, for fluid interface
     */
    public function filterByPrimaryKey($key)
    {

        return $this->addUsingAlias(MotTableMap::COL_ID_MOT, $key, Criteria::EQUAL);
    }

    /**
     * Filter the query by a list of primary keys
     *
     * @param     array $keys The list of primary key to use for the query
     *
     * @return $this|ChildMotQuery The current query, for fluid interface
     */
    public function filterByPrimaryKeys($keys)
    {

        return $this->addUsingAlias(MotTableMap::COL_ID_MOT, $keys, Criteria::IN);
    }

    /**
     * Filter the query on the id_mot column
     *
     * Example usage:
     * <code>
     * $query->filterByIdMot(1234); // WHERE id_mot = 1234
     * $query->filterByIdMot(array(12, 34)); // WHERE id_mot IN (12, 34)
     * $query->filterByIdMot(array('min' => 12)); // WHERE id_mot > 12
     * </code>
     *
     * @param     mixed $idMot The value to use as filter.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildMotQuery The current query, for fluid interface
     */
    public function filterByIdMot($idMot = null, $comparison = null)
    {
        if (is_array($idMot)) {
            $useMinMax = false;
            if (isset($idMot['min'])) {
                $this->addUsingAlias(MotTableMap::COL_ID_MOT, $idMot['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($idMot['max'])) {
                $this->addUsingAlias(MotTableMap::COL_ID_MOT, $idMot['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(MotTableMap::COL_ID_MOT, $idMot, $comparison);
    }

    /**
     * Filter the query on the nom column
     *
     * Example usage:
     * <code>
     * $query->filterByNom('fooValue');   // WHERE nom = 'fooValue'
     * $query->filterByNom('%fooValue%', Criteria::LIKE); // WHERE nom LIKE '%fooValue%'
     * </code>
     *
     * @param     string $nom The value to use as filter.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildMotQuery The current query, for fluid interface
     */
    public function filterByNom($nom = null, $comparison = null)
    {
        if (null === $comparison) {
            if (is_array($nom)) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(MotTableMap::COL_NOM, $nom, $comparison);
    }

    /**
     * Filter the query on the nomcourt column
     *
     * Example usage:
     * <code>
     * $query->filterByNomcourt('fooValue');   // WHERE nomcourt = 'fooValue'
     * $query->filterByNomcourt('%fooValue%', Criteria::LIKE); // WHERE nomcourt LIKE '%fooValue%'
     * </code>
     *
     * @param     string $nomcourt The value to use as filter.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildMotQuery The current query, for fluid interface
     */
    public function filterByNomcourt($nomcourt = null, $comparison = null)
    {
        if (null === $comparison) {
            if (is_array($nomcourt)) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(MotTableMap::COL_NOMCOURT, $nomcourt, $comparison);
    }

    /**
     * Filter the query on the descriptif column
     *
     * Example usage:
     * <code>
     * $query->filterByDescriptif('fooValue');   // WHERE descriptif = 'fooValue'
     * $query->filterByDescriptif('%fooValue%', Criteria::LIKE); // WHERE descriptif LIKE '%fooValue%'
     * </code>
     *
     * @param     string $descriptif The value to use as filter.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildMotQuery The current query, for fluid interface
     */
    public function filterByDescriptif($descriptif = null, $comparison = null)
    {
        if (null === $comparison) {
            if (is_array($descriptif)) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(MotTableMap::COL_DESCRIPTIF, $descriptif, $comparison);
    }

    /**
     * Filter the query on the texte column
     *
     * Example usage:
     * <code>
     * $query->filterByTexte('fooValue');   // WHERE texte = 'fooValue'
     * $query->filterByTexte('%fooValue%', Criteria::LIKE); // WHERE texte LIKE '%fooValue%'
     * </code>
     *
     * @param     string $texte The value to use as filter.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildMotQuery The current query, for fluid interface
     */
    public function filterByTexte($texte = null, $comparison = null)
    {
        if (null === $comparison) {
            if (is_array($texte)) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(MotTableMap::COL_TEXTE, $texte, $comparison);
    }

    /**
     * Filter the query on the importance column
     *
     * Example usage:
     * <code>
     * $query->filterByImportance(1234); // WHERE importance = 1234
     * $query->filterByImportance(array(12, 34)); // WHERE importance IN (12, 34)
     * $query->filterByImportance(array('min' => 12)); // WHERE importance > 12
     * </code>
     *
     * @param     mixed $importance The value to use as filter.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildMotQuery The current query, for fluid interface
     */
    public function filterByImportance($importance = null, $comparison = null)
    {
        if (is_array($importance)) {
            $useMinMax = false;
            if (isset($importance['min'])) {
                $this->addUsingAlias(MotTableMap::COL_IMPORTANCE, $importance['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($importance['max'])) {
                $this->addUsingAlias(MotTableMap::COL_IMPORTANCE, $importance['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(MotTableMap::COL_IMPORTANCE, $importance, $comparison);
    }

    /**
     * Filter the query on the actif column
     *
     * Example usage:
     * <code>
     * $query->filterByActif(1234); // WHERE actif = 1234
     * $query->filterByActif(array(12, 34)); // WHERE actif IN (12, 34)
     * $query->filterByActif(array('min' => 12)); // WHERE actif > 12
     * </code>
     *
     * @param     mixed $actif The value to use as filter.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildMotQuery The current query, for fluid interface
     */
    public function filterByActif($actif = null, $comparison = null)
    {
        if (is_array($actif)) {
            $useMinMax = false;
            if (isset($actif['min'])) {
                $this->addUsingAlias(MotTableMap::COL_ACTIF, $actif['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($actif['max'])) {
                $this->addUsingAlias(MotTableMap::COL_ACTIF, $actif['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(MotTableMap::COL_ACTIF, $actif, $comparison);
    }

    /**
     * Filter the query on the id_motgroupe column
     *
     * Example usage:
     * <code>
     * $query->filterByIdMotgroupe(1234); // WHERE id_motgroupe = 1234
     * $query->filterByIdMotgroupe(array(12, 34)); // WHERE id_motgroupe IN (12, 34)
     * $query->filterByIdMotgroupe(array('min' => 12)); // WHERE id_motgroupe > 12
     * </code>
     *
     * @see       filterByMotgroupe()
     *
     * @param     mixed $idMotgroupe The value to use as filter.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildMotQuery The current query, for fluid interface
     */
    public function filterByIdMotgroupe($idMotgroupe = null, $comparison = null)
    {
        if (is_array($idMotgroupe)) {
            $useMinMax = false;
            if (isset($idMotgroupe['min'])) {
                $this->addUsingAlias(MotTableMap::COL_ID_MOTGROUPE, $idMotgroupe['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($idMotgroupe['max'])) {
                $this->addUsingAlias(MotTableMap::COL_ID_MOTGROUPE, $idMotgroupe['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(MotTableMap::COL_ID_MOTGROUPE, $idMotgroupe, $comparison);
    }

    /**
     * Filter the query on the created_at column
     *
     * Example usage:
     * <code>
     * $query->filterByCreatedAt('2011-03-14'); // WHERE created_at = '2011-03-14'
     * $query->filterByCreatedAt('now'); // WHERE created_at = '2011-03-14'
     * $query->filterByCreatedAt(array('max' => 'yesterday')); // WHERE created_at > '2011-03-13'
     * </code>
     *
     * @param     mixed $createdAt The value to use as filter.
     *              Values can be integers (unix timestamps), DateTime objects, or strings.
     *              Empty strings are treated as NULL.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildMotQuery The current query, for fluid interface
     */
    public function filterByCreatedAt($createdAt = null, $comparison = null)
    {
        if (is_array($createdAt)) {
            $useMinMax = false;
            if (isset($createdAt['min'])) {
                $this->addUsingAlias(MotTableMap::COL_CREATED_AT, $createdAt['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($createdAt['max'])) {
                $this->addUsingAlias(MotTableMap::COL_CREATED_AT, $createdAt['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(MotTableMap::COL_CREATED_AT, $createdAt, $comparison);
    }

    /**
     * Filter the query on the updated_at column
     *
     * Example usage:
     * <code>
     * $query->filterByUpdatedAt('2011-03-14'); // WHERE updated_at = '2011-03-14'
     * $query->filterByUpdatedAt('now'); // WHERE updated_at = '2011-03-14'
     * $query->filterByUpdatedAt(array('max' => 'yesterday')); // WHERE updated_at > '2011-03-13'
     * </code>
     *
     * @param     mixed $updatedAt The value to use as filter.
     *              Values can be integers (unix timestamps), DateTime objects, or strings.
     *              Empty strings are treated as NULL.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildMotQuery The current query, for fluid interface
     */
    public function filterByUpdatedAt($updatedAt = null, $comparison = null)
    {
        if (is_array($updatedAt)) {
            $useMinMax = false;
            if (isset($updatedAt['min'])) {
                $this->addUsingAlias(MotTableMap::COL_UPDATED_AT, $updatedAt['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($updatedAt['max'])) {
                $this->addUsingAlias(MotTableMap::COL_UPDATED_AT, $updatedAt['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(MotTableMap::COL_UPDATED_AT, $updatedAt, $comparison);
    }

    /**
     * Filter the query by a related \Motgroupe object
     *
     * @param \Motgroupe|ObjectCollection $motgroupe The related object(s) to use as filter
     * @param string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @throws \Propel\Runtime\Exception\PropelException
     *
     * @return ChildMotQuery The current query, for fluid interface
     */
    public function filterByMotgroupe($motgroupe, $comparison = null)
    {
        if ($motgroupe instanceof \Motgroupe) {
            return $this
                ->addUsingAlias(MotTableMap::COL_ID_MOTGROUPE, $motgroupe->getIdMotgroupe(), $comparison);
        } elseif ($motgroupe instanceof ObjectCollection) {
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }

            return $this
                ->addUsingAlias(MotTableMap::COL_ID_MOTGROUPE, $motgroupe->toKeyValue('PrimaryKey', 'IdMotgroupe'), $comparison);
        } else {
            throw new PropelException('filterByMotgroupe() only accepts arguments of type \Motgroupe or Collection');
        }
    }

    /**
     * Adds a JOIN clause to the query using the Motgroupe relation
     *
     * @param     string $relationAlias optional alias for the relation
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return $this|ChildMotQuery The current query, for fluid interface
     */
    public function joinMotgroupe($relationAlias = null, $joinType = Criteria::INNER_JOIN)
    {
        $tableMap = $this->getTableMap();
        $relationMap = $tableMap->getRelation('Motgroupe');

        // create a ModelJoin object for this join
        $join = new ModelJoin();
        $join->setJoinType($joinType);
        $join->setRelationMap($relationMap, $this->useAliasInSQL ? $this->getModelAlias() : null, $relationAlias);
        if ($previousJoin = $this->getPreviousJoin()) {
            $join->setPreviousJoin($previousJoin);
        }

        // add the ModelJoin to the current object
        if ($relationAlias) {
            $this->addAlias($relationAlias, $relationMap->getRightTable()->getName());
            $this->addJoinObject($join, $relationAlias);
        } else {
            $this->addJoinObject($join, 'Motgroupe');
        }

        return $this;
    }

    /**
     * Use the Motgroupe relation Motgroupe object
     *
     * @see useQuery()
     *
     * @param     string $relationAlias optional alias for the relation,
     *                                   to be used as main alias in the secondary query
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return \MotgroupeQuery A secondary query class using the current class as primary query
     */
    public function useMotgroupeQuery($relationAlias = null, $joinType = Criteria::INNER_JOIN)
    {
        return $this
            ->joinMotgroupe($relationAlias, $joinType)
            ->useQuery($relationAlias ? $relationAlias : 'Motgroupe', '\MotgroupeQuery');
    }

    /**
     * Filter the query by a related \MotLien object
     *
     * @param \MotLien|ObjectCollection $motLien the related object to use as filter
     * @param string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return ChildMotQuery The current query, for fluid interface
     */
    public function filterByMotLien($motLien, $comparison = null)
    {
        if ($motLien instanceof \MotLien) {
            return $this
                ->addUsingAlias(MotTableMap::COL_ID_MOT, $motLien->getIdMot(), $comparison);
        } elseif ($motLien instanceof ObjectCollection) {
            return $this
                ->useMotLienQuery()
                ->filterByPrimaryKeys($motLien->getPrimaryKeys())
                ->endUse();
        } else {
            throw new PropelException('filterByMotLien() only accepts arguments of type \MotLien or Collection');
        }
    }

    /**
     * Adds a JOIN clause to the query using the MotLien relation
     *
     * @param     string $relationAlias optional alias for the relation
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return $this|ChildMotQuery The current query, for fluid interface
     */
    public function joinMotLien($relationAlias = null, $joinType = Criteria::INNER_JOIN)
    {
        $tableMap = $this->getTableMap();
        $relationMap = $tableMap->getRelation('MotLien');

        // create a ModelJoin object for this join
        $join = new ModelJoin();
        $join->setJoinType($joinType);
        $join->setRelationMap($relationMap, $this->useAliasInSQL ? $this->getModelAlias() : null, $relationAlias);
        if ($previousJoin = $this->getPreviousJoin()) {
            $join->setPreviousJoin($previousJoin);
        }

        // add the ModelJoin to the current object
        if ($relationAlias) {
            $this->addAlias($relationAlias, $relationMap->getRightTable()->getName());
            $this->addJoinObject($join, $relationAlias);
        } else {
            $this->addJoinObject($join, 'MotLien');
        }

        return $this;
    }

    /**
     * Use the MotLien relation MotLien object
     *
     * @see useQuery()
     *
     * @param     string $relationAlias optional alias for the relation,
     *                                   to be used as main alias in the secondary query
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return \MotLienQuery A secondary query class using the current class as primary query
     */
    public function useMotLienQuery($relationAlias = null, $joinType = Criteria::INNER_JOIN)
    {
        return $this
            ->joinMotLien($relationAlias, $joinType)
            ->useQuery($relationAlias ? $relationAlias : 'MotLien', '\MotLienQuery');
    }

    /**
     * Exclude object from result
     *
     * @param   ChildMot $mot Object to remove from the list of results
     *
     * @return $this|ChildMotQuery The current query, for fluid interface
     */
    public function prune($mot = null)
    {
        if ($mot) {
            $this->addUsingAlias(MotTableMap::COL_ID_MOT, $mot->getIdMot(), Criteria::NOT_EQUAL);
        }

        return $this;
    }

    /**
     * Code to execute before every DELETE statement
     *
     * @param     ConnectionInterface $con The connection object used by the query
     */
    protected function basePreDelete(ConnectionInterface $con)
    {
        // archivable behavior

        if ($this->archiveOnDelete) {
            $this->archive($con);
        } else {
            $this->archiveOnDelete = true;
        }


        return $this->preDelete($con);
    }

    /**
     * Deletes all rows from the asso_mots table.
     *
     * @param ConnectionInterface $con the connection to use
     * @return int The number of affected rows (if supported by underlying database driver).
     */
    public function doDeleteAll(ConnectionInterface $con = null)
    {
        if (null === $con) {
            $con = Propel::getServiceContainer()->getWriteConnection(MotTableMap::DATABASE_NAME);
        }

        // use transaction because $criteria could contain info
        // for more than one table or we could emulating ON DELETE CASCADE, etc.
        return $con->transaction(function () use ($con) {
            $affectedRows = 0; // initialize var to track total num of affected rows
            $affectedRows += parent::doDeleteAll($con);
            // Because this db requires some delete cascade/set null emulation, we have to
            // clear the cached instance *after* the emulation has happened (since
            // instances get re-added by the select statement contained therein).
            MotTableMap::clearInstancePool();
            MotTableMap::clearRelatedInstancePool();

            return $affectedRows;
        });
    }

    /**
     * Performs a DELETE on the database based on the current ModelCriteria
     *
     * @param ConnectionInterface $con the connection to use
     * @return int             The number of affected rows (if supported by underlying database driver).  This includes CASCADE-related rows
     *                         if supported by native driver or if emulated using Propel.
     * @throws PropelException Any exceptions caught during processing will be
     *                         rethrown wrapped into a PropelException.
     */
    public function delete(ConnectionInterface $con = null)
    {
        if (null === $con) {
            $con = Propel::getServiceContainer()->getWriteConnection(MotTableMap::DATABASE_NAME);
        }

        $criteria = $this;

        // Set the correct dbName
        $criteria->setDbName(MotTableMap::DATABASE_NAME);

        // use transaction because $criteria could contain info
        // for more than one table or we could emulating ON DELETE CASCADE, etc.
        return $con->transaction(function () use ($con, $criteria) {
            $affectedRows = 0; // initialize var to track total num of affected rows

            MotTableMap::removeInstanceFromPool($criteria);

            $affectedRows += ModelCriteria::delete($con);
            MotTableMap::clearRelatedInstancePool();

            return $affectedRows;
        });
    }

    // timestampable behavior

    /**
     * Filter by the latest updated
     *
     * @param      int $nbDays Maximum age of the latest update in days
     *
     * @return     $this|ChildMotQuery The current query, for fluid interface
     */
    public function recentlyUpdated($nbDays = 7)
    {
        return $this->addUsingAlias(MotTableMap::COL_UPDATED_AT, time() - $nbDays * 24 * 60 * 60, Criteria::GREATER_EQUAL);
    }

    /**
     * Order by update date desc
     *
     * @return     $this|ChildMotQuery The current query, for fluid interface
     */
    public function lastUpdatedFirst()
    {
        return $this->addDescendingOrderByColumn(MotTableMap::COL_UPDATED_AT);
    }

    /**
     * Order by update date asc
     *
     * @return     $this|ChildMotQuery The current query, for fluid interface
     */
    public function firstUpdatedFirst()
    {
        return $this->addAscendingOrderByColumn(MotTableMap::COL_UPDATED_AT);
    }

    /**
     * Order by create date desc
     *
     * @return     $this|ChildMotQuery The current query, for fluid interface
     */
    public function lastCreatedFirst()
    {
        return $this->addDescendingOrderByColumn(MotTableMap::COL_CREATED_AT);
    }

    /**
     * Filter by the latest created
     *
     * @param      int $nbDays Maximum age of in days
     *
     * @return     $this|ChildMotQuery The current query, for fluid interface
     */
    public function recentlyCreated($nbDays = 7)
    {
        return $this->addUsingAlias(MotTableMap::COL_CREATED_AT, time() - $nbDays * 24 * 60 * 60, Criteria::GREATER_EQUAL);
    }

    /**
     * Order by create date asc
     *
     * @return     $this|ChildMotQuery The current query, for fluid interface
     */
    public function firstCreatedFirst()
    {
        return $this->addAscendingOrderByColumn(MotTableMap::COL_CREATED_AT);
    }

    // archivable behavior

    /**
     * Copy the data of the objects satisfying the query into ChildAssoMotsArchive archive objects.
     * The archived objects are then saved.
     * If any of the objects has already been archived, the archived object
     * is updated and not duplicated.
     * Warning: This termination methods issues 2n+1 queries.
     *
     * @param      ConnectionInterface $con    Connection to use.
     * @param      Boolean $useLittleMemory    Whether or not to use OnDemandFormatter to retrieve objects.
     *               Set to false if the identity map matters.
     *               Set to true (default) to use less memory.
     *
     * @return     int the number of archived objects
     */
    public function archive($con = null, $useLittleMemory = true)
    {
        $criteria = clone $this;
        // prepare the query
        $criteria->setWith(array());
        if ($useLittleMemory) {
            $criteria->setFormatter(ModelCriteria::FORMAT_ON_DEMAND);
        }
        if ($con === null) {
            $con = Propel::getServiceContainer()->getWriteConnection(MotTableMap::DATABASE_NAME);
        }

        return $con->transaction(function () use ($con, $criteria) {
            $totalArchivedObjects = 0;

            // archive all results one by one
            foreach ($criteria->find($con) as $object) {
                $object->archive($con);
                $totalArchivedObjects++;
            }

            return $totalArchivedObjects;
        });
    }

    /**
     * Enable/disable auto-archiving on delete for the next query.
     *
     * @param boolean True if the query must archive deleted objects, false otherwise.
     */
    public function setArchiveOnDelete($archiveOnDelete)
    {
        $this->archiveOnDelete = $archiveOnDelete;
    }

    /**
     * Delete records matching the current query without archiving them.
     *
     * @param      ConnectionInterface $con    Connection to use.
     *
     * @return integer the number of deleted rows
     */
    public function deleteWithoutArchive($con = null)
    {
        $this->archiveOnDelete = false;

        return $this->delete($con);
    }

    /**
     * Delete all records without archiving them.
     *
     * @param      ConnectionInterface $con    Connection to use.
     *
     * @return integer the number of deleted rows
     */
    public function deleteAllWithoutArchive($con = null)
    {
        $this->archiveOnDelete = false;

        return $this->deleteAll($con);
    }

} // MotQuery
