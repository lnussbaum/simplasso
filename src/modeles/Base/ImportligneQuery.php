<?php

namespace Base;

use \AssoImportlignesArchive as ChildAssoImportlignesArchive;
use \Importligne as ChildImportligne;
use \ImportligneQuery as ChildImportligneQuery;
use \Exception;
use \PDO;
use Map\ImportligneTableMap;
use Propel\Runtime\Propel;
use Propel\Runtime\ActiveQuery\Criteria;
use Propel\Runtime\ActiveQuery\ModelCriteria;
use Propel\Runtime\ActiveQuery\ModelJoin;
use Propel\Runtime\Collection\ObjectCollection;
use Propel\Runtime\Connection\ConnectionInterface;
use Propel\Runtime\Exception\PropelException;

/**
 * Base class that represents a query for the 'asso_importlignes' table.
 *
 *
 *
 * @method     ChildImportligneQuery orderByIdImportligne($order = Criteria::ASC) Order by the id_importligne column
 * @method     ChildImportligneQuery orderByIdImport($order = Criteria::ASC) Order by the id_import column
 * @method     ChildImportligneQuery orderByLigne($order = Criteria::ASC) Order by the ligne column
 * @method     ChildImportligneQuery orderByStatut($order = Criteria::ASC) Order by the statut column
 * @method     ChildImportligneQuery orderByVariables($order = Criteria::ASC) Order by the variables column
 * @method     ChildImportligneQuery orderByPropositions($order = Criteria::ASC) Order by the propositions column
 * @method     ChildImportligneQuery orderByObservation($order = Criteria::ASC) Order by the observation column
 * @method     ChildImportligneQuery orderByCreatedAt($order = Criteria::ASC) Order by the created_at column
 * @method     ChildImportligneQuery orderByUpdatedAt($order = Criteria::ASC) Order by the updated_at column
 *
 * @method     ChildImportligneQuery groupByIdImportligne() Group by the id_importligne column
 * @method     ChildImportligneQuery groupByIdImport() Group by the id_import column
 * @method     ChildImportligneQuery groupByLigne() Group by the ligne column
 * @method     ChildImportligneQuery groupByStatut() Group by the statut column
 * @method     ChildImportligneQuery groupByVariables() Group by the variables column
 * @method     ChildImportligneQuery groupByPropositions() Group by the propositions column
 * @method     ChildImportligneQuery groupByObservation() Group by the observation column
 * @method     ChildImportligneQuery groupByCreatedAt() Group by the created_at column
 * @method     ChildImportligneQuery groupByUpdatedAt() Group by the updated_at column
 *
 * @method     ChildImportligneQuery leftJoin($relation) Adds a LEFT JOIN clause to the query
 * @method     ChildImportligneQuery rightJoin($relation) Adds a RIGHT JOIN clause to the query
 * @method     ChildImportligneQuery innerJoin($relation) Adds a INNER JOIN clause to the query
 *
 * @method     ChildImportligneQuery leftJoinWith($relation) Adds a LEFT JOIN clause and with to the query
 * @method     ChildImportligneQuery rightJoinWith($relation) Adds a RIGHT JOIN clause and with to the query
 * @method     ChildImportligneQuery innerJoinWith($relation) Adds a INNER JOIN clause and with to the query
 *
 * @method     ChildImportligneQuery leftJoinImport($relationAlias = null) Adds a LEFT JOIN clause to the query using the Import relation
 * @method     ChildImportligneQuery rightJoinImport($relationAlias = null) Adds a RIGHT JOIN clause to the query using the Import relation
 * @method     ChildImportligneQuery innerJoinImport($relationAlias = null) Adds a INNER JOIN clause to the query using the Import relation
 *
 * @method     ChildImportligneQuery joinWithImport($joinType = Criteria::INNER_JOIN) Adds a join clause and with to the query using the Import relation
 *
 * @method     ChildImportligneQuery leftJoinWithImport() Adds a LEFT JOIN clause and with to the query using the Import relation
 * @method     ChildImportligneQuery rightJoinWithImport() Adds a RIGHT JOIN clause and with to the query using the Import relation
 * @method     ChildImportligneQuery innerJoinWithImport() Adds a INNER JOIN clause and with to the query using the Import relation
 *
 * @method     \ImportQuery endUse() Finalizes a secondary criteria and merges it with its primary Criteria
 *
 * @method     ChildImportligne findOne(ConnectionInterface $con = null) Return the first ChildImportligne matching the query
 * @method     ChildImportligne findOneOrCreate(ConnectionInterface $con = null) Return the first ChildImportligne matching the query, or a new ChildImportligne object populated from the query conditions when no match is found
 *
 * @method     ChildImportligne findOneByIdImportligne(string $id_importligne) Return the first ChildImportligne filtered by the id_importligne column
 * @method     ChildImportligne findOneByIdImport(string $id_import) Return the first ChildImportligne filtered by the id_import column
 * @method     ChildImportligne findOneByLigne(string $ligne) Return the first ChildImportligne filtered by the ligne column
 * @method     ChildImportligne findOneByStatut(string $statut) Return the first ChildImportligne filtered by the statut column
 * @method     ChildImportligne findOneByVariables(string $variables) Return the first ChildImportligne filtered by the variables column
 * @method     ChildImportligne findOneByPropositions(string $propositions) Return the first ChildImportligne filtered by the propositions column
 * @method     ChildImportligne findOneByObservation(string $observation) Return the first ChildImportligne filtered by the observation column
 * @method     ChildImportligne findOneByCreatedAt(string $created_at) Return the first ChildImportligne filtered by the created_at column
 * @method     ChildImportligne findOneByUpdatedAt(string $updated_at) Return the first ChildImportligne filtered by the updated_at column *

 * @method     ChildImportligne requirePk($key, ConnectionInterface $con = null) Return the ChildImportligne by primary key and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildImportligne requireOne(ConnectionInterface $con = null) Return the first ChildImportligne matching the query and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 *
 * @method     ChildImportligne requireOneByIdImportligne(string $id_importligne) Return the first ChildImportligne filtered by the id_importligne column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildImportligne requireOneByIdImport(string $id_import) Return the first ChildImportligne filtered by the id_import column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildImportligne requireOneByLigne(string $ligne) Return the first ChildImportligne filtered by the ligne column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildImportligne requireOneByStatut(string $statut) Return the first ChildImportligne filtered by the statut column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildImportligne requireOneByVariables(string $variables) Return the first ChildImportligne filtered by the variables column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildImportligne requireOneByPropositions(string $propositions) Return the first ChildImportligne filtered by the propositions column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildImportligne requireOneByObservation(string $observation) Return the first ChildImportligne filtered by the observation column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildImportligne requireOneByCreatedAt(string $created_at) Return the first ChildImportligne filtered by the created_at column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildImportligne requireOneByUpdatedAt(string $updated_at) Return the first ChildImportligne filtered by the updated_at column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 *
 * @method     ChildImportligne[]|ObjectCollection find(ConnectionInterface $con = null) Return ChildImportligne objects based on current ModelCriteria
 * @method     ChildImportligne[]|ObjectCollection findByIdImportligne(string $id_importligne) Return ChildImportligne objects filtered by the id_importligne column
 * @method     ChildImportligne[]|ObjectCollection findByIdImport(string $id_import) Return ChildImportligne objects filtered by the id_import column
 * @method     ChildImportligne[]|ObjectCollection findByLigne(string $ligne) Return ChildImportligne objects filtered by the ligne column
 * @method     ChildImportligne[]|ObjectCollection findByStatut(string $statut) Return ChildImportligne objects filtered by the statut column
 * @method     ChildImportligne[]|ObjectCollection findByVariables(string $variables) Return ChildImportligne objects filtered by the variables column
 * @method     ChildImportligne[]|ObjectCollection findByPropositions(string $propositions) Return ChildImportligne objects filtered by the propositions column
 * @method     ChildImportligne[]|ObjectCollection findByObservation(string $observation) Return ChildImportligne objects filtered by the observation column
 * @method     ChildImportligne[]|ObjectCollection findByCreatedAt(string $created_at) Return ChildImportligne objects filtered by the created_at column
 * @method     ChildImportligne[]|ObjectCollection findByUpdatedAt(string $updated_at) Return ChildImportligne objects filtered by the updated_at column
 * @method     ChildImportligne[]|\Propel\Runtime\Util\PropelModelPager paginate($page = 1, $maxPerPage = 10, ConnectionInterface $con = null) Issue a SELECT query based on the current ModelCriteria and uses a page and a maximum number of results per page to compute an offset and a limit
 *
 */
abstract class ImportligneQuery extends ModelCriteria
{

    // archivable behavior
    protected $archiveOnDelete = true;
protected $entityNotFoundExceptionClass = '\\Propel\\Runtime\\Exception\\EntityNotFoundException';

    /**
     * Initializes internal state of \Base\ImportligneQuery object.
     *
     * @param     string $dbName The database name
     * @param     string $modelName The phpName of a model, e.g. 'Book'
     * @param     string $modelAlias The alias for the model in this query, e.g. 'b'
     */
    public function __construct($dbName = 'simplasso', $modelName = '\\Importligne', $modelAlias = null)
    {
        parent::__construct($dbName, $modelName, $modelAlias);
    }

    /**
     * Returns a new ChildImportligneQuery object.
     *
     * @param     string $modelAlias The alias of a model in the query
     * @param     Criteria $criteria Optional Criteria to build the query from
     *
     * @return ChildImportligneQuery
     */
    public static function create($modelAlias = null, Criteria $criteria = null)
    {
        if ($criteria instanceof ChildImportligneQuery) {
            return $criteria;
        }
        $query = new ChildImportligneQuery();
        if (null !== $modelAlias) {
            $query->setModelAlias($modelAlias);
        }
        if ($criteria instanceof Criteria) {
            $query->mergeWith($criteria);
        }

        return $query;
    }

    /**
     * Find object by primary key.
     * Propel uses the instance pool to skip the database if the object exists.
     * Go fast if the query is untouched.
     *
     * <code>
     * $obj  = $c->findPk(12, $con);
     * </code>
     *
     * @param mixed $key Primary key to use for the query
     * @param ConnectionInterface $con an optional connection object
     *
     * @return ChildImportligne|array|mixed the result, formatted by the current formatter
     */
    public function findPk($key, ConnectionInterface $con = null)
    {
        if ($key === null) {
            return null;
        }

        if ($con === null) {
            $con = Propel::getServiceContainer()->getReadConnection(ImportligneTableMap::DATABASE_NAME);
        }

        $this->basePreSelect($con);

        if (
            $this->formatter || $this->modelAlias || $this->with || $this->select
            || $this->selectColumns || $this->asColumns || $this->selectModifiers
            || $this->map || $this->having || $this->joins
        ) {
            return $this->findPkComplex($key, $con);
        }

        if ((null !== ($obj = ImportligneTableMap::getInstanceFromPool(null === $key || is_scalar($key) || is_callable([$key, '__toString']) ? (string) $key : $key)))) {
            // the object is already in the instance pool
            return $obj;
        }

        return $this->findPkSimple($key, $con);
    }

    /**
     * Find object by primary key using raw SQL to go fast.
     * Bypass doSelect() and the object formatter by using generated code.
     *
     * @param     mixed $key Primary key to use for the query
     * @param     ConnectionInterface $con A connection object
     *
     * @throws \Propel\Runtime\Exception\PropelException
     *
     * @return ChildImportligne A model object, or null if the key is not found
     */
    protected function findPkSimple($key, ConnectionInterface $con)
    {
        $sql = 'SELECT `id_importligne`, `id_import`, `ligne`, `statut`, `variables`, `propositions`, `observation`, `created_at`, `updated_at` FROM `asso_importlignes` WHERE `id_importligne` = :p0';
        try {
            $stmt = $con->prepare($sql);
            $stmt->bindValue(':p0', $key, PDO::PARAM_INT);
            $stmt->execute();
        } catch (Exception $e) {
            Propel::log($e->getMessage(), Propel::LOG_ERR);
            throw new PropelException(sprintf('Unable to execute SELECT statement [%s]', $sql), 0, $e);
        }
        $obj = null;
        if ($row = $stmt->fetch(\PDO::FETCH_NUM)) {
            /** @var ChildImportligne $obj */
            $obj = new ChildImportligne();
            $obj->hydrate($row);
            ImportligneTableMap::addInstanceToPool($obj, null === $key || is_scalar($key) || is_callable([$key, '__toString']) ? (string) $key : $key);
        }
        $stmt->closeCursor();

        return $obj;
    }

    /**
     * Find object by primary key.
     *
     * @param     mixed $key Primary key to use for the query
     * @param     ConnectionInterface $con A connection object
     *
     * @return ChildImportligne|array|mixed the result, formatted by the current formatter
     */
    protected function findPkComplex($key, ConnectionInterface $con)
    {
        // As the query uses a PK condition, no limit(1) is necessary.
        $criteria = $this->isKeepQuery() ? clone $this : $this;
        $dataFetcher = $criteria
            ->filterByPrimaryKey($key)
            ->doSelect($con);

        return $criteria->getFormatter()->init($criteria)->formatOne($dataFetcher);
    }

    /**
     * Find objects by primary key
     * <code>
     * $objs = $c->findPks(array(12, 56, 832), $con);
     * </code>
     * @param     array $keys Primary keys to use for the query
     * @param     ConnectionInterface $con an optional connection object
     *
     * @return ObjectCollection|array|mixed the list of results, formatted by the current formatter
     */
    public function findPks($keys, ConnectionInterface $con = null)
    {
        if (null === $con) {
            $con = Propel::getServiceContainer()->getReadConnection($this->getDbName());
        }
        $this->basePreSelect($con);
        $criteria = $this->isKeepQuery() ? clone $this : $this;
        $dataFetcher = $criteria
            ->filterByPrimaryKeys($keys)
            ->doSelect($con);

        return $criteria->getFormatter()->init($criteria)->format($dataFetcher);
    }

    /**
     * Filter the query by primary key
     *
     * @param     mixed $key Primary key to use for the query
     *
     * @return $this|ChildImportligneQuery The current query, for fluid interface
     */
    public function filterByPrimaryKey($key)
    {

        return $this->addUsingAlias(ImportligneTableMap::COL_ID_IMPORTLIGNE, $key, Criteria::EQUAL);
    }

    /**
     * Filter the query by a list of primary keys
     *
     * @param     array $keys The list of primary key to use for the query
     *
     * @return $this|ChildImportligneQuery The current query, for fluid interface
     */
    public function filterByPrimaryKeys($keys)
    {

        return $this->addUsingAlias(ImportligneTableMap::COL_ID_IMPORTLIGNE, $keys, Criteria::IN);
    }

    /**
     * Filter the query on the id_importligne column
     *
     * Example usage:
     * <code>
     * $query->filterByIdImportligne(1234); // WHERE id_importligne = 1234
     * $query->filterByIdImportligne(array(12, 34)); // WHERE id_importligne IN (12, 34)
     * $query->filterByIdImportligne(array('min' => 12)); // WHERE id_importligne > 12
     * </code>
     *
     * @param     mixed $idImportligne The value to use as filter.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildImportligneQuery The current query, for fluid interface
     */
    public function filterByIdImportligne($idImportligne = null, $comparison = null)
    {
        if (is_array($idImportligne)) {
            $useMinMax = false;
            if (isset($idImportligne['min'])) {
                $this->addUsingAlias(ImportligneTableMap::COL_ID_IMPORTLIGNE, $idImportligne['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($idImportligne['max'])) {
                $this->addUsingAlias(ImportligneTableMap::COL_ID_IMPORTLIGNE, $idImportligne['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(ImportligneTableMap::COL_ID_IMPORTLIGNE, $idImportligne, $comparison);
    }

    /**
     * Filter the query on the id_import column
     *
     * Example usage:
     * <code>
     * $query->filterByIdImport(1234); // WHERE id_import = 1234
     * $query->filterByIdImport(array(12, 34)); // WHERE id_import IN (12, 34)
     * $query->filterByIdImport(array('min' => 12)); // WHERE id_import > 12
     * </code>
     *
     * @see       filterByImport()
     *
     * @param     mixed $idImport The value to use as filter.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildImportligneQuery The current query, for fluid interface
     */
    public function filterByIdImport($idImport = null, $comparison = null)
    {
        if (is_array($idImport)) {
            $useMinMax = false;
            if (isset($idImport['min'])) {
                $this->addUsingAlias(ImportligneTableMap::COL_ID_IMPORT, $idImport['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($idImport['max'])) {
                $this->addUsingAlias(ImportligneTableMap::COL_ID_IMPORT, $idImport['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(ImportligneTableMap::COL_ID_IMPORT, $idImport, $comparison);
    }

    /**
     * Filter the query on the ligne column
     *
     * Example usage:
     * <code>
     * $query->filterByLigne(1234); // WHERE ligne = 1234
     * $query->filterByLigne(array(12, 34)); // WHERE ligne IN (12, 34)
     * $query->filterByLigne(array('min' => 12)); // WHERE ligne > 12
     * </code>
     *
     * @param     mixed $ligne The value to use as filter.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildImportligneQuery The current query, for fluid interface
     */
    public function filterByLigne($ligne = null, $comparison = null)
    {
        if (is_array($ligne)) {
            $useMinMax = false;
            if (isset($ligne['min'])) {
                $this->addUsingAlias(ImportligneTableMap::COL_LIGNE, $ligne['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($ligne['max'])) {
                $this->addUsingAlias(ImportligneTableMap::COL_LIGNE, $ligne['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(ImportligneTableMap::COL_LIGNE, $ligne, $comparison);
    }

    /**
     * Filter the query on the statut column
     *
     * Example usage:
     * <code>
     * $query->filterByStatut('fooValue');   // WHERE statut = 'fooValue'
     * $query->filterByStatut('%fooValue%', Criteria::LIKE); // WHERE statut LIKE '%fooValue%'
     * </code>
     *
     * @param     string $statut The value to use as filter.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildImportligneQuery The current query, for fluid interface
     */
    public function filterByStatut($statut = null, $comparison = null)
    {
        if (null === $comparison) {
            if (is_array($statut)) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(ImportligneTableMap::COL_STATUT, $statut, $comparison);
    }

    /**
     * Filter the query on the variables column
     *
     * Example usage:
     * <code>
     * $query->filterByVariables('fooValue');   // WHERE variables = 'fooValue'
     * $query->filterByVariables('%fooValue%', Criteria::LIKE); // WHERE variables LIKE '%fooValue%'
     * </code>
     *
     * @param     string $variables The value to use as filter.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildImportligneQuery The current query, for fluid interface
     */
    public function filterByVariables($variables = null, $comparison = null)
    {
        if (null === $comparison) {
            if (is_array($variables)) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(ImportligneTableMap::COL_VARIABLES, $variables, $comparison);
    }

    /**
     * Filter the query on the propositions column
     *
     * Example usage:
     * <code>
     * $query->filterByPropositions('fooValue');   // WHERE propositions = 'fooValue'
     * $query->filterByPropositions('%fooValue%', Criteria::LIKE); // WHERE propositions LIKE '%fooValue%'
     * </code>
     *
     * @param     string $propositions The value to use as filter.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildImportligneQuery The current query, for fluid interface
     */
    public function filterByPropositions($propositions = null, $comparison = null)
    {
        if (null === $comparison) {
            if (is_array($propositions)) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(ImportligneTableMap::COL_PROPOSITIONS, $propositions, $comparison);
    }

    /**
     * Filter the query on the observation column
     *
     * Example usage:
     * <code>
     * $query->filterByObservation('fooValue');   // WHERE observation = 'fooValue'
     * $query->filterByObservation('%fooValue%', Criteria::LIKE); // WHERE observation LIKE '%fooValue%'
     * </code>
     *
     * @param     string $observation The value to use as filter.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildImportligneQuery The current query, for fluid interface
     */
    public function filterByObservation($observation = null, $comparison = null)
    {
        if (null === $comparison) {
            if (is_array($observation)) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(ImportligneTableMap::COL_OBSERVATION, $observation, $comparison);
    }

    /**
     * Filter the query on the created_at column
     *
     * Example usage:
     * <code>
     * $query->filterByCreatedAt('2011-03-14'); // WHERE created_at = '2011-03-14'
     * $query->filterByCreatedAt('now'); // WHERE created_at = '2011-03-14'
     * $query->filterByCreatedAt(array('max' => 'yesterday')); // WHERE created_at > '2011-03-13'
     * </code>
     *
     * @param     mixed $createdAt The value to use as filter.
     *              Values can be integers (unix timestamps), DateTime objects, or strings.
     *              Empty strings are treated as NULL.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildImportligneQuery The current query, for fluid interface
     */
    public function filterByCreatedAt($createdAt = null, $comparison = null)
    {
        if (is_array($createdAt)) {
            $useMinMax = false;
            if (isset($createdAt['min'])) {
                $this->addUsingAlias(ImportligneTableMap::COL_CREATED_AT, $createdAt['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($createdAt['max'])) {
                $this->addUsingAlias(ImportligneTableMap::COL_CREATED_AT, $createdAt['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(ImportligneTableMap::COL_CREATED_AT, $createdAt, $comparison);
    }

    /**
     * Filter the query on the updated_at column
     *
     * Example usage:
     * <code>
     * $query->filterByUpdatedAt('2011-03-14'); // WHERE updated_at = '2011-03-14'
     * $query->filterByUpdatedAt('now'); // WHERE updated_at = '2011-03-14'
     * $query->filterByUpdatedAt(array('max' => 'yesterday')); // WHERE updated_at > '2011-03-13'
     * </code>
     *
     * @param     mixed $updatedAt The value to use as filter.
     *              Values can be integers (unix timestamps), DateTime objects, or strings.
     *              Empty strings are treated as NULL.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildImportligneQuery The current query, for fluid interface
     */
    public function filterByUpdatedAt($updatedAt = null, $comparison = null)
    {
        if (is_array($updatedAt)) {
            $useMinMax = false;
            if (isset($updatedAt['min'])) {
                $this->addUsingAlias(ImportligneTableMap::COL_UPDATED_AT, $updatedAt['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($updatedAt['max'])) {
                $this->addUsingAlias(ImportligneTableMap::COL_UPDATED_AT, $updatedAt['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(ImportligneTableMap::COL_UPDATED_AT, $updatedAt, $comparison);
    }

    /**
     * Filter the query by a related \Import object
     *
     * @param \Import|ObjectCollection $import The related object(s) to use as filter
     * @param string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @throws \Propel\Runtime\Exception\PropelException
     *
     * @return ChildImportligneQuery The current query, for fluid interface
     */
    public function filterByImport($import, $comparison = null)
    {
        if ($import instanceof \Import) {
            return $this
                ->addUsingAlias(ImportligneTableMap::COL_ID_IMPORT, $import->getIdImport(), $comparison);
        } elseif ($import instanceof ObjectCollection) {
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }

            return $this
                ->addUsingAlias(ImportligneTableMap::COL_ID_IMPORT, $import->toKeyValue('PrimaryKey', 'IdImport'), $comparison);
        } else {
            throw new PropelException('filterByImport() only accepts arguments of type \Import or Collection');
        }
    }

    /**
     * Adds a JOIN clause to the query using the Import relation
     *
     * @param     string $relationAlias optional alias for the relation
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return $this|ChildImportligneQuery The current query, for fluid interface
     */
    public function joinImport($relationAlias = null, $joinType = Criteria::INNER_JOIN)
    {
        $tableMap = $this->getTableMap();
        $relationMap = $tableMap->getRelation('Import');

        // create a ModelJoin object for this join
        $join = new ModelJoin();
        $join->setJoinType($joinType);
        $join->setRelationMap($relationMap, $this->useAliasInSQL ? $this->getModelAlias() : null, $relationAlias);
        if ($previousJoin = $this->getPreviousJoin()) {
            $join->setPreviousJoin($previousJoin);
        }

        // add the ModelJoin to the current object
        if ($relationAlias) {
            $this->addAlias($relationAlias, $relationMap->getRightTable()->getName());
            $this->addJoinObject($join, $relationAlias);
        } else {
            $this->addJoinObject($join, 'Import');
        }

        return $this;
    }

    /**
     * Use the Import relation Import object
     *
     * @see useQuery()
     *
     * @param     string $relationAlias optional alias for the relation,
     *                                   to be used as main alias in the secondary query
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return \ImportQuery A secondary query class using the current class as primary query
     */
    public function useImportQuery($relationAlias = null, $joinType = Criteria::INNER_JOIN)
    {
        return $this
            ->joinImport($relationAlias, $joinType)
            ->useQuery($relationAlias ? $relationAlias : 'Import', '\ImportQuery');
    }

    /**
     * Exclude object from result
     *
     * @param   ChildImportligne $importligne Object to remove from the list of results
     *
     * @return $this|ChildImportligneQuery The current query, for fluid interface
     */
    public function prune($importligne = null)
    {
        if ($importligne) {
            $this->addUsingAlias(ImportligneTableMap::COL_ID_IMPORTLIGNE, $importligne->getIdImportligne(), Criteria::NOT_EQUAL);
        }

        return $this;
    }

    /**
     * Code to execute before every DELETE statement
     *
     * @param     ConnectionInterface $con The connection object used by the query
     */
    protected function basePreDelete(ConnectionInterface $con)
    {
        // archivable behavior

        if ($this->archiveOnDelete) {
            $this->archive($con);
        } else {
            $this->archiveOnDelete = true;
        }


        return $this->preDelete($con);
    }

    /**
     * Deletes all rows from the asso_importlignes table.
     *
     * @param ConnectionInterface $con the connection to use
     * @return int The number of affected rows (if supported by underlying database driver).
     */
    public function doDeleteAll(ConnectionInterface $con = null)
    {
        if (null === $con) {
            $con = Propel::getServiceContainer()->getWriteConnection(ImportligneTableMap::DATABASE_NAME);
        }

        // use transaction because $criteria could contain info
        // for more than one table or we could emulating ON DELETE CASCADE, etc.
        return $con->transaction(function () use ($con) {
            $affectedRows = 0; // initialize var to track total num of affected rows
            $affectedRows += parent::doDeleteAll($con);
            // Because this db requires some delete cascade/set null emulation, we have to
            // clear the cached instance *after* the emulation has happened (since
            // instances get re-added by the select statement contained therein).
            ImportligneTableMap::clearInstancePool();
            ImportligneTableMap::clearRelatedInstancePool();

            return $affectedRows;
        });
    }

    /**
     * Performs a DELETE on the database based on the current ModelCriteria
     *
     * @param ConnectionInterface $con the connection to use
     * @return int             The number of affected rows (if supported by underlying database driver).  This includes CASCADE-related rows
     *                         if supported by native driver or if emulated using Propel.
     * @throws PropelException Any exceptions caught during processing will be
     *                         rethrown wrapped into a PropelException.
     */
    public function delete(ConnectionInterface $con = null)
    {
        if (null === $con) {
            $con = Propel::getServiceContainer()->getWriteConnection(ImportligneTableMap::DATABASE_NAME);
        }

        $criteria = $this;

        // Set the correct dbName
        $criteria->setDbName(ImportligneTableMap::DATABASE_NAME);

        // use transaction because $criteria could contain info
        // for more than one table or we could emulating ON DELETE CASCADE, etc.
        return $con->transaction(function () use ($con, $criteria) {
            $affectedRows = 0; // initialize var to track total num of affected rows

            ImportligneTableMap::removeInstanceFromPool($criteria);

            $affectedRows += ModelCriteria::delete($con);
            ImportligneTableMap::clearRelatedInstancePool();

            return $affectedRows;
        });
    }

    // timestampable behavior

    /**
     * Filter by the latest updated
     *
     * @param      int $nbDays Maximum age of the latest update in days
     *
     * @return     $this|ChildImportligneQuery The current query, for fluid interface
     */
    public function recentlyUpdated($nbDays = 7)
    {
        return $this->addUsingAlias(ImportligneTableMap::COL_UPDATED_AT, time() - $nbDays * 24 * 60 * 60, Criteria::GREATER_EQUAL);
    }

    /**
     * Order by update date desc
     *
     * @return     $this|ChildImportligneQuery The current query, for fluid interface
     */
    public function lastUpdatedFirst()
    {
        return $this->addDescendingOrderByColumn(ImportligneTableMap::COL_UPDATED_AT);
    }

    /**
     * Order by update date asc
     *
     * @return     $this|ChildImportligneQuery The current query, for fluid interface
     */
    public function firstUpdatedFirst()
    {
        return $this->addAscendingOrderByColumn(ImportligneTableMap::COL_UPDATED_AT);
    }

    /**
     * Order by create date desc
     *
     * @return     $this|ChildImportligneQuery The current query, for fluid interface
     */
    public function lastCreatedFirst()
    {
        return $this->addDescendingOrderByColumn(ImportligneTableMap::COL_CREATED_AT);
    }

    /**
     * Filter by the latest created
     *
     * @param      int $nbDays Maximum age of in days
     *
     * @return     $this|ChildImportligneQuery The current query, for fluid interface
     */
    public function recentlyCreated($nbDays = 7)
    {
        return $this->addUsingAlias(ImportligneTableMap::COL_CREATED_AT, time() - $nbDays * 24 * 60 * 60, Criteria::GREATER_EQUAL);
    }

    /**
     * Order by create date asc
     *
     * @return     $this|ChildImportligneQuery The current query, for fluid interface
     */
    public function firstCreatedFirst()
    {
        return $this->addAscendingOrderByColumn(ImportligneTableMap::COL_CREATED_AT);
    }

    // archivable behavior

    /**
     * Copy the data of the objects satisfying the query into ChildAssoImportlignesArchive archive objects.
     * The archived objects are then saved.
     * If any of the objects has already been archived, the archived object
     * is updated and not duplicated.
     * Warning: This termination methods issues 2n+1 queries.
     *
     * @param      ConnectionInterface $con    Connection to use.
     * @param      Boolean $useLittleMemory    Whether or not to use OnDemandFormatter to retrieve objects.
     *               Set to false if the identity map matters.
     *               Set to true (default) to use less memory.
     *
     * @return     int the number of archived objects
     */
    public function archive($con = null, $useLittleMemory = true)
    {
        $criteria = clone $this;
        // prepare the query
        $criteria->setWith(array());
        if ($useLittleMemory) {
            $criteria->setFormatter(ModelCriteria::FORMAT_ON_DEMAND);
        }
        if ($con === null) {
            $con = Propel::getServiceContainer()->getWriteConnection(ImportligneTableMap::DATABASE_NAME);
        }

        return $con->transaction(function () use ($con, $criteria) {
            $totalArchivedObjects = 0;

            // archive all results one by one
            foreach ($criteria->find($con) as $object) {
                $object->archive($con);
                $totalArchivedObjects++;
            }

            return $totalArchivedObjects;
        });
    }

    /**
     * Enable/disable auto-archiving on delete for the next query.
     *
     * @param boolean True if the query must archive deleted objects, false otherwise.
     */
    public function setArchiveOnDelete($archiveOnDelete)
    {
        $this->archiveOnDelete = $archiveOnDelete;
    }

    /**
     * Delete records matching the current query without archiving them.
     *
     * @param      ConnectionInterface $con    Connection to use.
     *
     * @return integer the number of deleted rows
     */
    public function deleteWithoutArchive($con = null)
    {
        $this->archiveOnDelete = false;

        return $this->delete($con);
    }

    /**
     * Delete all records without archiving them.
     *
     * @param      ConnectionInterface $con    Connection to use.
     *
     * @return integer the number of deleted rows
     */
    public function deleteAllWithoutArchive($con = null)
    {
        $this->archiveOnDelete = false;

        return $this->deleteAll($con);
    }

} // ImportligneQuery
