<?php

namespace Base;

use \Codespostaux as ChildCodespostaux;
use \CodespostauxQuery as ChildCodespostauxQuery;
use \Exception;
use \PDO;
use Map\CodespostauxTableMap;
use Propel\Runtime\Propel;
use Propel\Runtime\ActiveQuery\Criteria;
use Propel\Runtime\ActiveQuery\ModelCriteria;
use Propel\Runtime\Collection\ObjectCollection;
use Propel\Runtime\Connection\ConnectionInterface;
use Propel\Runtime\Exception\PropelException;

/**
 * Base class that represents a query for the 'geo_codespostaux' table.
 *
 *
 *
 * @method     ChildCodespostauxQuery orderByIdCodepostal($order = Criteria::ASC) Order by the id_codepostal column
 * @method     ChildCodespostauxQuery orderByPays($order = Criteria::ASC) Order by the pays column
 * @method     ChildCodespostauxQuery orderByCodepostal($order = Criteria::ASC) Order by the codepostal column
 * @method     ChildCodespostauxQuery orderByNom($order = Criteria::ASC) Order by the nom column
 * @method     ChildCodespostauxQuery orderByNomSuite($order = Criteria::ASC) Order by the nom_suite column
 * @method     ChildCodespostauxQuery orderByCreatedAt($order = Criteria::ASC) Order by the created_at column
 * @method     ChildCodespostauxQuery orderByUpdatedAt($order = Criteria::ASC) Order by the updated_at column
 *
 * @method     ChildCodespostauxQuery groupByIdCodepostal() Group by the id_codepostal column
 * @method     ChildCodespostauxQuery groupByPays() Group by the pays column
 * @method     ChildCodespostauxQuery groupByCodepostal() Group by the codepostal column
 * @method     ChildCodespostauxQuery groupByNom() Group by the nom column
 * @method     ChildCodespostauxQuery groupByNomSuite() Group by the nom_suite column
 * @method     ChildCodespostauxQuery groupByCreatedAt() Group by the created_at column
 * @method     ChildCodespostauxQuery groupByUpdatedAt() Group by the updated_at column
 *
 * @method     ChildCodespostauxQuery leftJoin($relation) Adds a LEFT JOIN clause to the query
 * @method     ChildCodespostauxQuery rightJoin($relation) Adds a RIGHT JOIN clause to the query
 * @method     ChildCodespostauxQuery innerJoin($relation) Adds a INNER JOIN clause to the query
 *
 * @method     ChildCodespostauxQuery leftJoinWith($relation) Adds a LEFT JOIN clause and with to the query
 * @method     ChildCodespostauxQuery rightJoinWith($relation) Adds a RIGHT JOIN clause and with to the query
 * @method     ChildCodespostauxQuery innerJoinWith($relation) Adds a INNER JOIN clause and with to the query
 *
 * @method     ChildCodespostaux findOne(ConnectionInterface $con = null) Return the first ChildCodespostaux matching the query
 * @method     ChildCodespostaux findOneOrCreate(ConnectionInterface $con = null) Return the first ChildCodespostaux matching the query, or a new ChildCodespostaux object populated from the query conditions when no match is found
 *
 * @method     ChildCodespostaux findOneByIdCodepostal(int $id_codepostal) Return the first ChildCodespostaux filtered by the id_codepostal column
 * @method     ChildCodespostaux findOneByPays(string $pays) Return the first ChildCodespostaux filtered by the pays column
 * @method     ChildCodespostaux findOneByCodepostal(string $codepostal) Return the first ChildCodespostaux filtered by the codepostal column
 * @method     ChildCodespostaux findOneByNom(string $nom) Return the first ChildCodespostaux filtered by the nom column
 * @method     ChildCodespostaux findOneByNomSuite(string $nom_suite) Return the first ChildCodespostaux filtered by the nom_suite column
 * @method     ChildCodespostaux findOneByCreatedAt(string $created_at) Return the first ChildCodespostaux filtered by the created_at column
 * @method     ChildCodespostaux findOneByUpdatedAt(string $updated_at) Return the first ChildCodespostaux filtered by the updated_at column *

 * @method     ChildCodespostaux requirePk($key, ConnectionInterface $con = null) Return the ChildCodespostaux by primary key and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildCodespostaux requireOne(ConnectionInterface $con = null) Return the first ChildCodespostaux matching the query and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 *
 * @method     ChildCodespostaux requireOneByIdCodepostal(int $id_codepostal) Return the first ChildCodespostaux filtered by the id_codepostal column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildCodespostaux requireOneByPays(string $pays) Return the first ChildCodespostaux filtered by the pays column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildCodespostaux requireOneByCodepostal(string $codepostal) Return the first ChildCodespostaux filtered by the codepostal column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildCodespostaux requireOneByNom(string $nom) Return the first ChildCodespostaux filtered by the nom column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildCodespostaux requireOneByNomSuite(string $nom_suite) Return the first ChildCodespostaux filtered by the nom_suite column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildCodespostaux requireOneByCreatedAt(string $created_at) Return the first ChildCodespostaux filtered by the created_at column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildCodespostaux requireOneByUpdatedAt(string $updated_at) Return the first ChildCodespostaux filtered by the updated_at column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 *
 * @method     ChildCodespostaux[]|ObjectCollection find(ConnectionInterface $con = null) Return ChildCodespostaux objects based on current ModelCriteria
 * @method     ChildCodespostaux[]|ObjectCollection findByIdCodepostal(int $id_codepostal) Return ChildCodespostaux objects filtered by the id_codepostal column
 * @method     ChildCodespostaux[]|ObjectCollection findByPays(string $pays) Return ChildCodespostaux objects filtered by the pays column
 * @method     ChildCodespostaux[]|ObjectCollection findByCodepostal(string $codepostal) Return ChildCodespostaux objects filtered by the codepostal column
 * @method     ChildCodespostaux[]|ObjectCollection findByNom(string $nom) Return ChildCodespostaux objects filtered by the nom column
 * @method     ChildCodespostaux[]|ObjectCollection findByNomSuite(string $nom_suite) Return ChildCodespostaux objects filtered by the nom_suite column
 * @method     ChildCodespostaux[]|ObjectCollection findByCreatedAt(string $created_at) Return ChildCodespostaux objects filtered by the created_at column
 * @method     ChildCodespostaux[]|ObjectCollection findByUpdatedAt(string $updated_at) Return ChildCodespostaux objects filtered by the updated_at column
 * @method     ChildCodespostaux[]|\Propel\Runtime\Util\PropelModelPager paginate($page = 1, $maxPerPage = 10, ConnectionInterface $con = null) Issue a SELECT query based on the current ModelCriteria and uses a page and a maximum number of results per page to compute an offset and a limit
 *
 */
abstract class CodespostauxQuery extends ModelCriteria
{
    protected $entityNotFoundExceptionClass = '\\Propel\\Runtime\\Exception\\EntityNotFoundException';

    /**
     * Initializes internal state of \Base\CodespostauxQuery object.
     *
     * @param     string $dbName The database name
     * @param     string $modelName The phpName of a model, e.g. 'Book'
     * @param     string $modelAlias The alias for the model in this query, e.g. 'b'
     */
    public function __construct($dbName = 'simplasso', $modelName = '\\Codespostaux', $modelAlias = null)
    {
        parent::__construct($dbName, $modelName, $modelAlias);
    }

    /**
     * Returns a new ChildCodespostauxQuery object.
     *
     * @param     string $modelAlias The alias of a model in the query
     * @param     Criteria $criteria Optional Criteria to build the query from
     *
     * @return ChildCodespostauxQuery
     */
    public static function create($modelAlias = null, Criteria $criteria = null)
    {
        if ($criteria instanceof ChildCodespostauxQuery) {
            return $criteria;
        }
        $query = new ChildCodespostauxQuery();
        if (null !== $modelAlias) {
            $query->setModelAlias($modelAlias);
        }
        if ($criteria instanceof Criteria) {
            $query->mergeWith($criteria);
        }

        return $query;
    }

    /**
     * Find object by primary key.
     * Propel uses the instance pool to skip the database if the object exists.
     * Go fast if the query is untouched.
     *
     * <code>
     * $obj  = $c->findPk(12, $con);
     * </code>
     *
     * @param mixed $key Primary key to use for the query
     * @param ConnectionInterface $con an optional connection object
     *
     * @return ChildCodespostaux|array|mixed the result, formatted by the current formatter
     */
    public function findPk($key, ConnectionInterface $con = null)
    {
        if ($key === null) {
            return null;
        }

        if ($con === null) {
            $con = Propel::getServiceContainer()->getReadConnection(CodespostauxTableMap::DATABASE_NAME);
        }

        $this->basePreSelect($con);

        if (
            $this->formatter || $this->modelAlias || $this->with || $this->select
            || $this->selectColumns || $this->asColumns || $this->selectModifiers
            || $this->map || $this->having || $this->joins
        ) {
            return $this->findPkComplex($key, $con);
        }

        if ((null !== ($obj = CodespostauxTableMap::getInstanceFromPool(null === $key || is_scalar($key) || is_callable([$key, '__toString']) ? (string) $key : $key)))) {
            // the object is already in the instance pool
            return $obj;
        }

        return $this->findPkSimple($key, $con);
    }

    /**
     * Find object by primary key using raw SQL to go fast.
     * Bypass doSelect() and the object formatter by using generated code.
     *
     * @param     mixed $key Primary key to use for the query
     * @param     ConnectionInterface $con A connection object
     *
     * @throws \Propel\Runtime\Exception\PropelException
     *
     * @return ChildCodespostaux A model object, or null if the key is not found
     */
    protected function findPkSimple($key, ConnectionInterface $con)
    {
        $sql = 'SELECT `id_codepostal`, `pays`, `codepostal`, `nom`, `nom_suite`, `created_at`, `updated_at` FROM `geo_codespostaux` WHERE `id_codepostal` = :p0';
        try {
            $stmt = $con->prepare($sql);
            $stmt->bindValue(':p0', $key, PDO::PARAM_INT);
            $stmt->execute();
        } catch (Exception $e) {
            Propel::log($e->getMessage(), Propel::LOG_ERR);
            throw new PropelException(sprintf('Unable to execute SELECT statement [%s]', $sql), 0, $e);
        }
        $obj = null;
        if ($row = $stmt->fetch(\PDO::FETCH_NUM)) {
            /** @var ChildCodespostaux $obj */
            $obj = new ChildCodespostaux();
            $obj->hydrate($row);
            CodespostauxTableMap::addInstanceToPool($obj, null === $key || is_scalar($key) || is_callable([$key, '__toString']) ? (string) $key : $key);
        }
        $stmt->closeCursor();

        return $obj;
    }

    /**
     * Find object by primary key.
     *
     * @param     mixed $key Primary key to use for the query
     * @param     ConnectionInterface $con A connection object
     *
     * @return ChildCodespostaux|array|mixed the result, formatted by the current formatter
     */
    protected function findPkComplex($key, ConnectionInterface $con)
    {
        // As the query uses a PK condition, no limit(1) is necessary.
        $criteria = $this->isKeepQuery() ? clone $this : $this;
        $dataFetcher = $criteria
            ->filterByPrimaryKey($key)
            ->doSelect($con);

        return $criteria->getFormatter()->init($criteria)->formatOne($dataFetcher);
    }

    /**
     * Find objects by primary key
     * <code>
     * $objs = $c->findPks(array(12, 56, 832), $con);
     * </code>
     * @param     array $keys Primary keys to use for the query
     * @param     ConnectionInterface $con an optional connection object
     *
     * @return ObjectCollection|array|mixed the list of results, formatted by the current formatter
     */
    public function findPks($keys, ConnectionInterface $con = null)
    {
        if (null === $con) {
            $con = Propel::getServiceContainer()->getReadConnection($this->getDbName());
        }
        $this->basePreSelect($con);
        $criteria = $this->isKeepQuery() ? clone $this : $this;
        $dataFetcher = $criteria
            ->filterByPrimaryKeys($keys)
            ->doSelect($con);

        return $criteria->getFormatter()->init($criteria)->format($dataFetcher);
    }

    /**
     * Filter the query by primary key
     *
     * @param     mixed $key Primary key to use for the query
     *
     * @return $this|ChildCodespostauxQuery The current query, for fluid interface
     */
    public function filterByPrimaryKey($key)
    {

        return $this->addUsingAlias(CodespostauxTableMap::COL_ID_CODEPOSTAL, $key, Criteria::EQUAL);
    }

    /**
     * Filter the query by a list of primary keys
     *
     * @param     array $keys The list of primary key to use for the query
     *
     * @return $this|ChildCodespostauxQuery The current query, for fluid interface
     */
    public function filterByPrimaryKeys($keys)
    {

        return $this->addUsingAlias(CodespostauxTableMap::COL_ID_CODEPOSTAL, $keys, Criteria::IN);
    }

    /**
     * Filter the query on the id_codepostal column
     *
     * Example usage:
     * <code>
     * $query->filterByIdCodepostal(1234); // WHERE id_codepostal = 1234
     * $query->filterByIdCodepostal(array(12, 34)); // WHERE id_codepostal IN (12, 34)
     * $query->filterByIdCodepostal(array('min' => 12)); // WHERE id_codepostal > 12
     * </code>
     *
     * @param     mixed $idCodepostal The value to use as filter.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildCodespostauxQuery The current query, for fluid interface
     */
    public function filterByIdCodepostal($idCodepostal = null, $comparison = null)
    {
        if (is_array($idCodepostal)) {
            $useMinMax = false;
            if (isset($idCodepostal['min'])) {
                $this->addUsingAlias(CodespostauxTableMap::COL_ID_CODEPOSTAL, $idCodepostal['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($idCodepostal['max'])) {
                $this->addUsingAlias(CodespostauxTableMap::COL_ID_CODEPOSTAL, $idCodepostal['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(CodespostauxTableMap::COL_ID_CODEPOSTAL, $idCodepostal, $comparison);
    }

    /**
     * Filter the query on the pays column
     *
     * Example usage:
     * <code>
     * $query->filterByPays('fooValue');   // WHERE pays = 'fooValue'
     * $query->filterByPays('%fooValue%', Criteria::LIKE); // WHERE pays LIKE '%fooValue%'
     * </code>
     *
     * @param     string $pays The value to use as filter.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildCodespostauxQuery The current query, for fluid interface
     */
    public function filterByPays($pays = null, $comparison = null)
    {
        if (null === $comparison) {
            if (is_array($pays)) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(CodespostauxTableMap::COL_PAYS, $pays, $comparison);
    }

    /**
     * Filter the query on the codepostal column
     *
     * Example usage:
     * <code>
     * $query->filterByCodepostal('fooValue');   // WHERE codepostal = 'fooValue'
     * $query->filterByCodepostal('%fooValue%', Criteria::LIKE); // WHERE codepostal LIKE '%fooValue%'
     * </code>
     *
     * @param     string $codepostal The value to use as filter.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildCodespostauxQuery The current query, for fluid interface
     */
    public function filterByCodepostal($codepostal = null, $comparison = null)
    {
        if (null === $comparison) {
            if (is_array($codepostal)) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(CodespostauxTableMap::COL_CODEPOSTAL, $codepostal, $comparison);
    }

    /**
     * Filter the query on the nom column
     *
     * Example usage:
     * <code>
     * $query->filterByNom('fooValue');   // WHERE nom = 'fooValue'
     * $query->filterByNom('%fooValue%', Criteria::LIKE); // WHERE nom LIKE '%fooValue%'
     * </code>
     *
     * @param     string $nom The value to use as filter.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildCodespostauxQuery The current query, for fluid interface
     */
    public function filterByNom($nom = null, $comparison = null)
    {
        if (null === $comparison) {
            if (is_array($nom)) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(CodespostauxTableMap::COL_NOM, $nom, $comparison);
    }

    /**
     * Filter the query on the nom_suite column
     *
     * Example usage:
     * <code>
     * $query->filterByNomSuite('fooValue');   // WHERE nom_suite = 'fooValue'
     * $query->filterByNomSuite('%fooValue%', Criteria::LIKE); // WHERE nom_suite LIKE '%fooValue%'
     * </code>
     *
     * @param     string $nomSuite The value to use as filter.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildCodespostauxQuery The current query, for fluid interface
     */
    public function filterByNomSuite($nomSuite = null, $comparison = null)
    {
        if (null === $comparison) {
            if (is_array($nomSuite)) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(CodespostauxTableMap::COL_NOM_SUITE, $nomSuite, $comparison);
    }

    /**
     * Filter the query on the created_at column
     *
     * Example usage:
     * <code>
     * $query->filterByCreatedAt('2011-03-14'); // WHERE created_at = '2011-03-14'
     * $query->filterByCreatedAt('now'); // WHERE created_at = '2011-03-14'
     * $query->filterByCreatedAt(array('max' => 'yesterday')); // WHERE created_at > '2011-03-13'
     * </code>
     *
     * @param     mixed $createdAt The value to use as filter.
     *              Values can be integers (unix timestamps), DateTime objects, or strings.
     *              Empty strings are treated as NULL.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildCodespostauxQuery The current query, for fluid interface
     */
    public function filterByCreatedAt($createdAt = null, $comparison = null)
    {
        if (is_array($createdAt)) {
            $useMinMax = false;
            if (isset($createdAt['min'])) {
                $this->addUsingAlias(CodespostauxTableMap::COL_CREATED_AT, $createdAt['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($createdAt['max'])) {
                $this->addUsingAlias(CodespostauxTableMap::COL_CREATED_AT, $createdAt['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(CodespostauxTableMap::COL_CREATED_AT, $createdAt, $comparison);
    }

    /**
     * Filter the query on the updated_at column
     *
     * Example usage:
     * <code>
     * $query->filterByUpdatedAt('2011-03-14'); // WHERE updated_at = '2011-03-14'
     * $query->filterByUpdatedAt('now'); // WHERE updated_at = '2011-03-14'
     * $query->filterByUpdatedAt(array('max' => 'yesterday')); // WHERE updated_at > '2011-03-13'
     * </code>
     *
     * @param     mixed $updatedAt The value to use as filter.
     *              Values can be integers (unix timestamps), DateTime objects, or strings.
     *              Empty strings are treated as NULL.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildCodespostauxQuery The current query, for fluid interface
     */
    public function filterByUpdatedAt($updatedAt = null, $comparison = null)
    {
        if (is_array($updatedAt)) {
            $useMinMax = false;
            if (isset($updatedAt['min'])) {
                $this->addUsingAlias(CodespostauxTableMap::COL_UPDATED_AT, $updatedAt['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($updatedAt['max'])) {
                $this->addUsingAlias(CodespostauxTableMap::COL_UPDATED_AT, $updatedAt['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(CodespostauxTableMap::COL_UPDATED_AT, $updatedAt, $comparison);
    }

    /**
     * Exclude object from result
     *
     * @param   ChildCodespostaux $codespostaux Object to remove from the list of results
     *
     * @return $this|ChildCodespostauxQuery The current query, for fluid interface
     */
    public function prune($codespostaux = null)
    {
        if ($codespostaux) {
            $this->addUsingAlias(CodespostauxTableMap::COL_ID_CODEPOSTAL, $codespostaux->getIdCodepostal(), Criteria::NOT_EQUAL);
        }

        return $this;
    }

    /**
     * Deletes all rows from the geo_codespostaux table.
     *
     * @param ConnectionInterface $con the connection to use
     * @return int The number of affected rows (if supported by underlying database driver).
     */
    public function doDeleteAll(ConnectionInterface $con = null)
    {
        if (null === $con) {
            $con = Propel::getServiceContainer()->getWriteConnection(CodespostauxTableMap::DATABASE_NAME);
        }

        // use transaction because $criteria could contain info
        // for more than one table or we could emulating ON DELETE CASCADE, etc.
        return $con->transaction(function () use ($con) {
            $affectedRows = 0; // initialize var to track total num of affected rows
            $affectedRows += parent::doDeleteAll($con);
            // Because this db requires some delete cascade/set null emulation, we have to
            // clear the cached instance *after* the emulation has happened (since
            // instances get re-added by the select statement contained therein).
            CodespostauxTableMap::clearInstancePool();
            CodespostauxTableMap::clearRelatedInstancePool();

            return $affectedRows;
        });
    }

    /**
     * Performs a DELETE on the database based on the current ModelCriteria
     *
     * @param ConnectionInterface $con the connection to use
     * @return int             The number of affected rows (if supported by underlying database driver).  This includes CASCADE-related rows
     *                         if supported by native driver or if emulated using Propel.
     * @throws PropelException Any exceptions caught during processing will be
     *                         rethrown wrapped into a PropelException.
     */
    public function delete(ConnectionInterface $con = null)
    {
        if (null === $con) {
            $con = Propel::getServiceContainer()->getWriteConnection(CodespostauxTableMap::DATABASE_NAME);
        }

        $criteria = $this;

        // Set the correct dbName
        $criteria->setDbName(CodespostauxTableMap::DATABASE_NAME);

        // use transaction because $criteria could contain info
        // for more than one table or we could emulating ON DELETE CASCADE, etc.
        return $con->transaction(function () use ($con, $criteria) {
            $affectedRows = 0; // initialize var to track total num of affected rows

            CodespostauxTableMap::removeInstanceFromPool($criteria);

            $affectedRows += ModelCriteria::delete($con);
            CodespostauxTableMap::clearRelatedInstancePool();

            return $affectedRows;
        });
    }

    // timestampable behavior

    /**
     * Filter by the latest updated
     *
     * @param      int $nbDays Maximum age of the latest update in days
     *
     * @return     $this|ChildCodespostauxQuery The current query, for fluid interface
     */
    public function recentlyUpdated($nbDays = 7)
    {
        return $this->addUsingAlias(CodespostauxTableMap::COL_UPDATED_AT, time() - $nbDays * 24 * 60 * 60, Criteria::GREATER_EQUAL);
    }

    /**
     * Order by update date desc
     *
     * @return     $this|ChildCodespostauxQuery The current query, for fluid interface
     */
    public function lastUpdatedFirst()
    {
        return $this->addDescendingOrderByColumn(CodespostauxTableMap::COL_UPDATED_AT);
    }

    /**
     * Order by update date asc
     *
     * @return     $this|ChildCodespostauxQuery The current query, for fluid interface
     */
    public function firstUpdatedFirst()
    {
        return $this->addAscendingOrderByColumn(CodespostauxTableMap::COL_UPDATED_AT);
    }

    /**
     * Order by create date desc
     *
     * @return     $this|ChildCodespostauxQuery The current query, for fluid interface
     */
    public function lastCreatedFirst()
    {
        return $this->addDescendingOrderByColumn(CodespostauxTableMap::COL_CREATED_AT);
    }

    /**
     * Filter by the latest created
     *
     * @param      int $nbDays Maximum age of in days
     *
     * @return     $this|ChildCodespostauxQuery The current query, for fluid interface
     */
    public function recentlyCreated($nbDays = 7)
    {
        return $this->addUsingAlias(CodespostauxTableMap::COL_CREATED_AT, time() - $nbDays * 24 * 60 * 60, Criteria::GREATER_EQUAL);
    }

    /**
     * Order by create date asc
     *
     * @return     $this|ChildCodespostauxQuery The current query, for fluid interface
     */
    public function firstCreatedFirst()
    {
        return $this->addAscendingOrderByColumn(CodespostauxTableMap::COL_CREATED_AT);
    }

} // CodespostauxQuery
