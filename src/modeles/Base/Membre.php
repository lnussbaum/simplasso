<?php

namespace Base;

use \AssoMembresArchive as ChildAssoMembresArchive;
use \AssoMembresArchiveQuery as ChildAssoMembresArchiveQuery;
use \CourrierLien as ChildCourrierLien;
use \CourrierLienQuery as ChildCourrierLienQuery;
use \Individu as ChildIndividu;
use \IndividuQuery as ChildIndividuQuery;
use \Membre as ChildMembre;
use \MembreIndividu as ChildMembreIndividu;
use \MembreIndividuQuery as ChildMembreIndividuQuery;
use \MembreQuery as ChildMembreQuery;
use \Servicerendu as ChildServicerendu;
use \ServicerenduQuery as ChildServicerenduQuery;
use \DateTime;
use \Exception;
use \PDO;
use Map\CourrierLienTableMap;
use Map\MembreIndividuTableMap;
use Map\MembreTableMap;
use Map\ServicerenduTableMap;
use Propel\Runtime\Propel;
use Propel\Runtime\ActiveQuery\Criteria;
use Propel\Runtime\ActiveQuery\ModelCriteria;
use Propel\Runtime\ActiveRecord\ActiveRecordInterface;
use Propel\Runtime\Collection\Collection;
use Propel\Runtime\Collection\ObjectCollection;
use Propel\Runtime\Connection\ConnectionInterface;
use Propel\Runtime\Exception\BadMethodCallException;
use Propel\Runtime\Exception\LogicException;
use Propel\Runtime\Exception\PropelException;
use Propel\Runtime\Map\TableMap;
use Propel\Runtime\Parser\AbstractParser;
use Propel\Runtime\Util\PropelDateTime;

/**
 * Base class that represents a row from the 'asso_membres' table.
 *
 *
 *
 * @package    propel.generator..Base
 */
abstract class Membre implements ActiveRecordInterface
{
    /**
     * TableMap class name
     */
    const TABLE_MAP = '\\Map\\MembreTableMap';


    /**
     * attribute to determine if this object has previously been saved.
     * @var boolean
     */
    protected $new = true;

    /**
     * attribute to determine whether this object has been deleted.
     * @var boolean
     */
    protected $deleted = false;

    /**
     * The columns that have been modified in current object.
     * Tracking modified columns allows us to only update modified columns.
     * @var array
     */
    protected $modifiedColumns = array();

    /**
     * The (virtual) columns that are added at runtime
     * The formatters can add supplementary columns based on a resultset
     * @var array
     */
    protected $virtualColumns = array();

    /**
     * The value for the id_membre field.
     *
     * @var        string
     */
    protected $id_membre;

    /**
     * The value for the id_individu_titulaire field.
     *
     * @var        string
     */
    protected $id_individu_titulaire;

    /**
     * The value for the identifiant_interne field.
     *
     * @var        string
     */
    protected $identifiant_interne;

    /**
     * The value for the civilite field.
     *
     * @var        string
     */
    protected $civilite;

    /**
     * The value for the nom field.
     *
     * @var        string
     */
    protected $nom;

    /**
     * The value for the nomcourt field.
     * nomcourt
     * @var        string
     */
    protected $nomcourt;

    /**
     * The value for the date_sortie field.
     *
     * @var        DateTime
     */
    protected $date_sortie;

    /**
     * The value for the observation field.
     *
     * @var        string
     */
    protected $observation;

    /**
     * The value for the created_at field.
     *
     * @var        DateTime
     */
    protected $created_at;

    /**
     * The value for the updated_at field.
     *
     * @var        DateTime
     */
    protected $updated_at;

    /**
     * @var        ChildIndividu
     */
    protected $aIndividuTitulaire;

    /**
     * @var        ObjectCollection|ChildMembreIndividu[] Collection to store aggregation of ChildMembreIndividu objects.
     */
    protected $collMembreIndividus;
    protected $collMembreIndividusPartial;

    /**
     * @var        ObjectCollection|ChildServicerendu[] Collection to store aggregation of ChildServicerendu objects.
     */
    protected $collServicerendus;
    protected $collServicerendusPartial;

    /**
     * @var        ObjectCollection|ChildCourrierLien[] Collection to store aggregation of ChildCourrierLien objects.
     */
    protected $collCourrierLiens;
    protected $collCourrierLiensPartial;

    /**
     * @var        ObjectCollection|ChildIndividu[] Cross Collection to store aggregation of ChildIndividu objects.
     */
    protected $collIndividus;

    /**
     * @var bool
     */
    protected $collIndividusPartial;

    /**
     * Flag to prevent endless save loop, if this object is referenced
     * by another object which falls in this transaction.
     *
     * @var boolean
     */
    protected $alreadyInSave = false;

    // archivable behavior
    protected $archiveOnDelete = true;

    /**
     * An array of objects scheduled for deletion.
     * @var ObjectCollection|ChildIndividu[]
     */
    protected $individusScheduledForDeletion = null;

    /**
     * An array of objects scheduled for deletion.
     * @var ObjectCollection|ChildMembreIndividu[]
     */
    protected $membreIndividusScheduledForDeletion = null;

    /**
     * An array of objects scheduled for deletion.
     * @var ObjectCollection|ChildServicerendu[]
     */
    protected $servicerendusScheduledForDeletion = null;

    /**
     * An array of objects scheduled for deletion.
     * @var ObjectCollection|ChildCourrierLien[]
     */
    protected $courrierLiensScheduledForDeletion = null;

    /**
     * Initializes internal state of Base\Membre object.
     */
    public function __construct()
    {
    }

    /**
     * Returns whether the object has been modified.
     *
     * @return boolean True if the object has been modified.
     */
    public function isModified()
    {
        return !!$this->modifiedColumns;
    }

    /**
     * Has specified column been modified?
     *
     * @param  string  $col column fully qualified name (TableMap::TYPE_COLNAME), e.g. Book::AUTHOR_ID
     * @return boolean True if $col has been modified.
     */
    public function isColumnModified($col)
    {
        return $this->modifiedColumns && isset($this->modifiedColumns[$col]);
    }

    /**
     * Get the columns that have been modified in this object.
     * @return array A unique list of the modified column names for this object.
     */
    public function getModifiedColumns()
    {
        return $this->modifiedColumns ? array_keys($this->modifiedColumns) : [];
    }

    /**
     * Returns whether the object has ever been saved.  This will
     * be false, if the object was retrieved from storage or was created
     * and then saved.
     *
     * @return boolean true, if the object has never been persisted.
     */
    public function isNew()
    {
        return $this->new;
    }

    /**
     * Setter for the isNew attribute.  This method will be called
     * by Propel-generated children and objects.
     *
     * @param boolean $b the state of the object.
     */
    public function setNew($b)
    {
        $this->new = (boolean) $b;
    }

    /**
     * Whether this object has been deleted.
     * @return boolean The deleted state of this object.
     */
    public function isDeleted()
    {
        return $this->deleted;
    }

    /**
     * Specify whether this object has been deleted.
     * @param  boolean $b The deleted state of this object.
     * @return void
     */
    public function setDeleted($b)
    {
        $this->deleted = (boolean) $b;
    }

    /**
     * Sets the modified state for the object to be false.
     * @param  string $col If supplied, only the specified column is reset.
     * @return void
     */
    public function resetModified($col = null)
    {
        if (null !== $col) {
            if (isset($this->modifiedColumns[$col])) {
                unset($this->modifiedColumns[$col]);
            }
        } else {
            $this->modifiedColumns = array();
        }
    }

    /**
     * Compares this with another <code>Membre</code> instance.  If
     * <code>obj</code> is an instance of <code>Membre</code>, delegates to
     * <code>equals(Membre)</code>.  Otherwise, returns <code>false</code>.
     *
     * @param  mixed   $obj The object to compare to.
     * @return boolean Whether equal to the object specified.
     */
    public function equals($obj)
    {
        if (!$obj instanceof static) {
            return false;
        }

        if ($this === $obj) {
            return true;
        }

        if (null === $this->getPrimaryKey() || null === $obj->getPrimaryKey()) {
            return false;
        }

        return $this->getPrimaryKey() === $obj->getPrimaryKey();
    }

    /**
     * Get the associative array of the virtual columns in this object
     *
     * @return array
     */
    public function getVirtualColumns()
    {
        return $this->virtualColumns;
    }

    /**
     * Checks the existence of a virtual column in this object
     *
     * @param  string  $name The virtual column name
     * @return boolean
     */
    public function hasVirtualColumn($name)
    {
        return array_key_exists($name, $this->virtualColumns);
    }

    /**
     * Get the value of a virtual column in this object
     *
     * @param  string $name The virtual column name
     * @return mixed
     *
     * @throws PropelException
     */
    public function getVirtualColumn($name)
    {
        if (!$this->hasVirtualColumn($name)) {
            throw new PropelException(sprintf('Cannot get value of inexistent virtual column %s.', $name));
        }

        return $this->virtualColumns[$name];
    }

    /**
     * Set the value of a virtual column in this object
     *
     * @param string $name  The virtual column name
     * @param mixed  $value The value to give to the virtual column
     *
     * @return $this|Membre The current object, for fluid interface
     */
    public function setVirtualColumn($name, $value)
    {
        $this->virtualColumns[$name] = $value;

        return $this;
    }

    /**
     * Logs a message using Propel::log().
     *
     * @param  string  $msg
     * @param  int     $priority One of the Propel::LOG_* logging levels
     * @return boolean
     */
    protected function log($msg, $priority = Propel::LOG_INFO)
    {
        return Propel::log(get_class($this) . ': ' . $msg, $priority);
    }

    /**
     * Export the current object properties to a string, using a given parser format
     * <code>
     * $book = BookQuery::create()->findPk(9012);
     * echo $book->exportTo('JSON');
     *  => {"Id":9012,"Title":"Don Juan","ISBN":"0140422161","Price":12.99,"PublisherId":1234,"AuthorId":5678}');
     * </code>
     *
     * @param  mixed   $parser                 A AbstractParser instance, or a format name ('XML', 'YAML', 'JSON', 'CSV')
     * @param  boolean $includeLazyLoadColumns (optional) Whether to include lazy load(ed) columns. Defaults to TRUE.
     * @return string  The exported data
     */
    public function exportTo($parser, $includeLazyLoadColumns = true)
    {
        if (!$parser instanceof AbstractParser) {
            $parser = AbstractParser::getParser($parser);
        }

        return $parser->fromArray($this->toArray(TableMap::TYPE_PHPNAME, $includeLazyLoadColumns, array(), true));
    }

    /**
     * Clean up internal collections prior to serializing
     * Avoids recursive loops that turn into segmentation faults when serializing
     */
    public function __sleep()
    {
        $this->clearAllReferences();

        $cls = new \ReflectionClass($this);
        $propertyNames = [];
        $serializableProperties = array_diff($cls->getProperties(), $cls->getProperties(\ReflectionProperty::IS_STATIC));

        foreach($serializableProperties as $property) {
            $propertyNames[] = $property->getName();
        }

        return $propertyNames;
    }

    /**
     * Get the [id_membre] column value.
     *
     * @return string
     */
    public function getIdMembre()
    {
        return $this->id_membre;
    }

    /**
     * Get the [id_individu_titulaire] column value.
     *
     * @return string
     */
    public function getIdIndividuTitulaire()
    {
        return $this->id_individu_titulaire;
    }

    /**
     * Get the [identifiant_interne] column value.
     *
     * @return string
     */
    public function getIdentifiantInterne()
    {
        return $this->identifiant_interne;
    }

    /**
     * Get the [civilite] column value.
     *
     * @return string
     */
    public function getCivilite()
    {
        return $this->civilite;
    }

    /**
     * Get the [nom] column value.
     *
     * @return string
     */
    public function getNom()
    {
        return $this->nom;
    }

    /**
     * Get the [nomcourt] column value.
     * nomcourt
     * @return string
     */
    public function getNomcourt()
    {
        return $this->nomcourt;
    }

    /**
     * Get the [optionally formatted] temporal [date_sortie] column value.
     *
     *
     * @param      string $format The date/time format string (either date()-style or strftime()-style).
     *                            If format is NULL, then the raw DateTime object will be returned.
     *
     * @return string|DateTime Formatted date/time value as string or DateTime object (if format is NULL), NULL if column is NULL, and 0 if column value is 0000-00-00
     *
     * @throws PropelException - if unable to parse/validate the date/time value.
     */
    public function getDateSortie($format = NULL)
    {
        if ($format === null) {
            return $this->date_sortie;
        } else {
            return $this->date_sortie instanceof \DateTimeInterface ? $this->date_sortie->format($format) : null;
        }
    }

    /**
     * Get the [observation] column value.
     *
     * @return string
     */
    public function getObservation()
    {
        return $this->observation;
    }

    /**
     * Get the [optionally formatted] temporal [created_at] column value.
     *
     *
     * @param      string $format The date/time format string (either date()-style or strftime()-style).
     *                            If format is NULL, then the raw DateTime object will be returned.
     *
     * @return string|DateTime Formatted date/time value as string or DateTime object (if format is NULL), NULL if column is NULL, and 0 if column value is 0000-00-00 00:00:00
     *
     * @throws PropelException - if unable to parse/validate the date/time value.
     */
    public function getCreatedAt($format = NULL)
    {
        if ($format === null) {
            return $this->created_at;
        } else {
            return $this->created_at instanceof \DateTimeInterface ? $this->created_at->format($format) : null;
        }
    }

    /**
     * Get the [optionally formatted] temporal [updated_at] column value.
     *
     *
     * @param      string $format The date/time format string (either date()-style or strftime()-style).
     *                            If format is NULL, then the raw DateTime object will be returned.
     *
     * @return string|DateTime Formatted date/time value as string or DateTime object (if format is NULL), NULL if column is NULL, and 0 if column value is 0000-00-00 00:00:00
     *
     * @throws PropelException - if unable to parse/validate the date/time value.
     */
    public function getUpdatedAt($format = NULL)
    {
        if ($format === null) {
            return $this->updated_at;
        } else {
            return $this->updated_at instanceof \DateTimeInterface ? $this->updated_at->format($format) : null;
        }
    }

    /**
     * Set the value of [id_membre] column.
     *
     * @param string $v new value
     * @return $this|\Membre The current object (for fluent API support)
     */
    public function setIdMembre($v)
    {
        if ($v !== null) {
            $v = (string) $v;
        }

        if ($this->id_membre !== $v) {
            $this->id_membre = $v;
            $this->modifiedColumns[MembreTableMap::COL_ID_MEMBRE] = true;
        }

        return $this;
    } // setIdMembre()

    /**
     * Set the value of [id_individu_titulaire] column.
     *
     * @param string $v new value
     * @return $this|\Membre The current object (for fluent API support)
     */
    public function setIdIndividuTitulaire($v)
    {
        if ($v !== null) {
            $v = (string) $v;
        }

        if ($this->id_individu_titulaire !== $v) {
            $this->id_individu_titulaire = $v;
            $this->modifiedColumns[MembreTableMap::COL_ID_INDIVIDU_TITULAIRE] = true;
        }

        if ($this->aIndividuTitulaire !== null && $this->aIndividuTitulaire->getIdIndividu() !== $v) {
            $this->aIndividuTitulaire = null;
        }

        return $this;
    } // setIdIndividuTitulaire()

    /**
     * Set the value of [identifiant_interne] column.
     *
     * @param string $v new value
     * @return $this|\Membre The current object (for fluent API support)
     */
    public function setIdentifiantInterne($v)
    {
        if ($v !== null) {
            $v = (string) $v;
        }

        if ($this->identifiant_interne !== $v) {
            $this->identifiant_interne = $v;
            $this->modifiedColumns[MembreTableMap::COL_IDENTIFIANT_INTERNE] = true;
        }

        return $this;
    } // setIdentifiantInterne()

    /**
     * Set the value of [civilite] column.
     *
     * @param string $v new value
     * @return $this|\Membre The current object (for fluent API support)
     */
    public function setCivilite($v)
    {
        if ($v !== null) {
            $v = (string) $v;
        }

        if ($this->civilite !== $v) {
            $this->civilite = $v;
            $this->modifiedColumns[MembreTableMap::COL_CIVILITE] = true;
        }

        return $this;
    } // setCivilite()

    /**
     * Set the value of [nom] column.
     *
     * @param string $v new value
     * @return $this|\Membre The current object (for fluent API support)
     */
    public function setNom($v)
    {
        if ($v !== null) {
            $v = (string) $v;
        }

        if ($this->nom !== $v) {
            $this->nom = $v;
            $this->modifiedColumns[MembreTableMap::COL_NOM] = true;
        }

        return $this;
    } // setNom()

    /**
     * Set the value of [nomcourt] column.
     * nomcourt
     * @param string $v new value
     * @return $this|\Membre The current object (for fluent API support)
     */
    public function setNomcourt($v)
    {
        if ($v !== null) {
            $v = (string) $v;
        }

        if ($this->nomcourt !== $v) {
            $this->nomcourt = $v;
            $this->modifiedColumns[MembreTableMap::COL_NOMCOURT] = true;
        }

        return $this;
    } // setNomcourt()

    /**
     * Sets the value of [date_sortie] column to a normalized version of the date/time value specified.
     *
     * @param  mixed $v string, integer (timestamp), or \DateTimeInterface value.
     *               Empty strings are treated as NULL.
     * @return $this|\Membre The current object (for fluent API support)
     */
    public function setDateSortie($v)
    {
        $dt = PropelDateTime::newInstance($v, null, 'DateTime');
        if ($this->date_sortie !== null || $dt !== null) {
            if ($this->date_sortie === null || $dt === null || $dt->format("Y-m-d") !== $this->date_sortie->format("Y-m-d")) {
                $this->date_sortie = $dt === null ? null : clone $dt;
                $this->modifiedColumns[MembreTableMap::COL_DATE_SORTIE] = true;
            }
        } // if either are not null

        return $this;
    } // setDateSortie()

    /**
     * Set the value of [observation] column.
     *
     * @param string $v new value
     * @return $this|\Membre The current object (for fluent API support)
     */
    public function setObservation($v)
    {
        if ($v !== null) {
            $v = (string) $v;
        }

        if ($this->observation !== $v) {
            $this->observation = $v;
            $this->modifiedColumns[MembreTableMap::COL_OBSERVATION] = true;
        }

        return $this;
    } // setObservation()

    /**
     * Sets the value of [created_at] column to a normalized version of the date/time value specified.
     *
     * @param  mixed $v string, integer (timestamp), or \DateTimeInterface value.
     *               Empty strings are treated as NULL.
     * @return $this|\Membre The current object (for fluent API support)
     */
    public function setCreatedAt($v)
    {
        $dt = PropelDateTime::newInstance($v, null, 'DateTime');
        if ($this->created_at !== null || $dt !== null) {
            if ($this->created_at === null || $dt === null || $dt->format("Y-m-d H:i:s.u") !== $this->created_at->format("Y-m-d H:i:s.u")) {
                $this->created_at = $dt === null ? null : clone $dt;
                $this->modifiedColumns[MembreTableMap::COL_CREATED_AT] = true;
            }
        } // if either are not null

        return $this;
    } // setCreatedAt()

    /**
     * Sets the value of [updated_at] column to a normalized version of the date/time value specified.
     *
     * @param  mixed $v string, integer (timestamp), or \DateTimeInterface value.
     *               Empty strings are treated as NULL.
     * @return $this|\Membre The current object (for fluent API support)
     */
    public function setUpdatedAt($v)
    {
        $dt = PropelDateTime::newInstance($v, null, 'DateTime');
        if ($this->updated_at !== null || $dt !== null) {
            if ($this->updated_at === null || $dt === null || $dt->format("Y-m-d H:i:s.u") !== $this->updated_at->format("Y-m-d H:i:s.u")) {
                $this->updated_at = $dt === null ? null : clone $dt;
                $this->modifiedColumns[MembreTableMap::COL_UPDATED_AT] = true;
            }
        } // if either are not null

        return $this;
    } // setUpdatedAt()

    /**
     * Indicates whether the columns in this object are only set to default values.
     *
     * This method can be used in conjunction with isModified() to indicate whether an object is both
     * modified _and_ has some values set which are non-default.
     *
     * @return boolean Whether the columns in this object are only been set with default values.
     */
    public function hasOnlyDefaultValues()
    {
        // otherwise, everything was equal, so return TRUE
        return true;
    } // hasOnlyDefaultValues()

    /**
     * Hydrates (populates) the object variables with values from the database resultset.
     *
     * An offset (0-based "start column") is specified so that objects can be hydrated
     * with a subset of the columns in the resultset rows.  This is needed, for example,
     * for results of JOIN queries where the resultset row includes columns from two or
     * more tables.
     *
     * @param array   $row       The row returned by DataFetcher->fetch().
     * @param int     $startcol  0-based offset column which indicates which restultset column to start with.
     * @param boolean $rehydrate Whether this object is being re-hydrated from the database.
     * @param string  $indexType The index type of $row. Mostly DataFetcher->getIndexType().
                                  One of the class type constants TableMap::TYPE_PHPNAME, TableMap::TYPE_CAMELNAME
     *                            TableMap::TYPE_COLNAME, TableMap::TYPE_FIELDNAME, TableMap::TYPE_NUM.
     *
     * @return int             next starting column
     * @throws PropelException - Any caught Exception will be rewrapped as a PropelException.
     */
    public function hydrate($row, $startcol = 0, $rehydrate = false, $indexType = TableMap::TYPE_NUM)
    {
        try {

            $col = $row[TableMap::TYPE_NUM == $indexType ? 0 + $startcol : MembreTableMap::translateFieldName('IdMembre', TableMap::TYPE_PHPNAME, $indexType)];
            $this->id_membre = (null !== $col) ? (string) $col : null;

            $col = $row[TableMap::TYPE_NUM == $indexType ? 1 + $startcol : MembreTableMap::translateFieldName('IdIndividuTitulaire', TableMap::TYPE_PHPNAME, $indexType)];
            $this->id_individu_titulaire = (null !== $col) ? (string) $col : null;

            $col = $row[TableMap::TYPE_NUM == $indexType ? 2 + $startcol : MembreTableMap::translateFieldName('IdentifiantInterne', TableMap::TYPE_PHPNAME, $indexType)];
            $this->identifiant_interne = (null !== $col) ? (string) $col : null;

            $col = $row[TableMap::TYPE_NUM == $indexType ? 3 + $startcol : MembreTableMap::translateFieldName('Civilite', TableMap::TYPE_PHPNAME, $indexType)];
            $this->civilite = (null !== $col) ? (string) $col : null;

            $col = $row[TableMap::TYPE_NUM == $indexType ? 4 + $startcol : MembreTableMap::translateFieldName('Nom', TableMap::TYPE_PHPNAME, $indexType)];
            $this->nom = (null !== $col) ? (string) $col : null;

            $col = $row[TableMap::TYPE_NUM == $indexType ? 5 + $startcol : MembreTableMap::translateFieldName('Nomcourt', TableMap::TYPE_PHPNAME, $indexType)];
            $this->nomcourt = (null !== $col) ? (string) $col : null;

            $col = $row[TableMap::TYPE_NUM == $indexType ? 6 + $startcol : MembreTableMap::translateFieldName('DateSortie', TableMap::TYPE_PHPNAME, $indexType)];
            if ($col === '0000-00-00') {
                $col = null;
            }
            $this->date_sortie = (null !== $col) ? PropelDateTime::newInstance($col, null, 'DateTime') : null;

            $col = $row[TableMap::TYPE_NUM == $indexType ? 7 + $startcol : MembreTableMap::translateFieldName('Observation', TableMap::TYPE_PHPNAME, $indexType)];
            $this->observation = (null !== $col) ? (string) $col : null;

            $col = $row[TableMap::TYPE_NUM == $indexType ? 8 + $startcol : MembreTableMap::translateFieldName('CreatedAt', TableMap::TYPE_PHPNAME, $indexType)];
            if ($col === '0000-00-00 00:00:00') {
                $col = null;
            }
            $this->created_at = (null !== $col) ? PropelDateTime::newInstance($col, null, 'DateTime') : null;

            $col = $row[TableMap::TYPE_NUM == $indexType ? 9 + $startcol : MembreTableMap::translateFieldName('UpdatedAt', TableMap::TYPE_PHPNAME, $indexType)];
            if ($col === '0000-00-00 00:00:00') {
                $col = null;
            }
            $this->updated_at = (null !== $col) ? PropelDateTime::newInstance($col, null, 'DateTime') : null;
            $this->resetModified();

            $this->setNew(false);

            if ($rehydrate) {
                $this->ensureConsistency();
            }

            return $startcol + 10; // 10 = MembreTableMap::NUM_HYDRATE_COLUMNS.

        } catch (Exception $e) {
            throw new PropelException(sprintf('Error populating %s object', '\\Membre'), 0, $e);
        }
    }

    /**
     * Checks and repairs the internal consistency of the object.
     *
     * This method is executed after an already-instantiated object is re-hydrated
     * from the database.  It exists to check any foreign keys to make sure that
     * the objects related to the current object are correct based on foreign key.
     *
     * You can override this method in the stub class, but you should always invoke
     * the base method from the overridden method (i.e. parent::ensureConsistency()),
     * in case your model changes.
     *
     * @throws PropelException
     */
    public function ensureConsistency()
    {
        if ($this->aIndividuTitulaire !== null && $this->id_individu_titulaire !== $this->aIndividuTitulaire->getIdIndividu()) {
            $this->aIndividuTitulaire = null;
        }
    } // ensureConsistency

    /**
     * Reloads this object from datastore based on primary key and (optionally) resets all associated objects.
     *
     * This will only work if the object has been saved and has a valid primary key set.
     *
     * @param      boolean $deep (optional) Whether to also de-associated any related objects.
     * @param      ConnectionInterface $con (optional) The ConnectionInterface connection to use.
     * @return void
     * @throws PropelException - if this object is deleted, unsaved or doesn't have pk match in db
     */
    public function reload($deep = false, ConnectionInterface $con = null)
    {
        if ($this->isDeleted()) {
            throw new PropelException("Cannot reload a deleted object.");
        }

        if ($this->isNew()) {
            throw new PropelException("Cannot reload an unsaved object.");
        }

        if ($con === null) {
            $con = Propel::getServiceContainer()->getReadConnection(MembreTableMap::DATABASE_NAME);
        }

        // We don't need to alter the object instance pool; we're just modifying this instance
        // already in the pool.

        $dataFetcher = ChildMembreQuery::create(null, $this->buildPkeyCriteria())->setFormatter(ModelCriteria::FORMAT_STATEMENT)->find($con);
        $row = $dataFetcher->fetch();
        $dataFetcher->close();
        if (!$row) {
            throw new PropelException('Cannot find matching row in the database to reload object values.');
        }
        $this->hydrate($row, 0, true, $dataFetcher->getIndexType()); // rehydrate

        if ($deep) {  // also de-associate any related objects?

            $this->aIndividuTitulaire = null;
            $this->collMembreIndividus = null;

            $this->collServicerendus = null;

            $this->collCourrierLiens = null;

            $this->collIndividus = null;
        } // if (deep)
    }

    /**
     * Removes this object from datastore and sets delete attribute.
     *
     * @param      ConnectionInterface $con
     * @return void
     * @throws PropelException
     * @see Membre::setDeleted()
     * @see Membre::isDeleted()
     */
    public function delete(ConnectionInterface $con = null)
    {
        if ($this->isDeleted()) {
            throw new PropelException("This object has already been deleted.");
        }

        if ($con === null) {
            $con = Propel::getServiceContainer()->getWriteConnection(MembreTableMap::DATABASE_NAME);
        }

        $con->transaction(function () use ($con) {
            $deleteQuery = ChildMembreQuery::create()
                ->filterByPrimaryKey($this->getPrimaryKey());
            $ret = $this->preDelete($con);
            // archivable behavior
            if ($ret) {
                if ($this->archiveOnDelete) {
                    // do nothing yet. The object will be archived later when calling ChildMembreQuery::delete().
                } else {
                    $deleteQuery->setArchiveOnDelete(false);
                    $this->archiveOnDelete = true;
                }
            }

            if ($ret) {
                $deleteQuery->delete($con);
                $this->postDelete($con);
                $this->setDeleted(true);
            }
        });
    }

    /**
     * Persists this object to the database.
     *
     * If the object is new, it inserts it; otherwise an update is performed.
     * All modified related objects will also be persisted in the doSave()
     * method.  This method wraps all precipitate database operations in a
     * single transaction.
     *
     * @param      ConnectionInterface $con
     * @return int             The number of rows affected by this insert/update and any referring fk objects' save() operations.
     * @throws PropelException
     * @see doSave()
     */
    public function save(ConnectionInterface $con = null)
    {
        if ($this->isDeleted()) {
            throw new PropelException("You cannot save an object that has been deleted.");
        }

        if ($this->alreadyInSave) {
            return 0;
        }

        if ($con === null) {
            $con = Propel::getServiceContainer()->getWriteConnection(MembreTableMap::DATABASE_NAME);
        }

        return $con->transaction(function () use ($con) {
            $ret = $this->preSave($con);
            $isInsert = $this->isNew();
            if ($isInsert) {
                $ret = $ret && $this->preInsert($con);
                // timestampable behavior

                if (!$this->isColumnModified(MembreTableMap::COL_CREATED_AT)) {
                    $this->setCreatedAt(\Propel\Runtime\Util\PropelDateTime::createHighPrecision());
                }
                if (!$this->isColumnModified(MembreTableMap::COL_UPDATED_AT)) {
                    $this->setUpdatedAt(\Propel\Runtime\Util\PropelDateTime::createHighPrecision());
                }
            } else {
                $ret = $ret && $this->preUpdate($con);
                // timestampable behavior
                if ($this->isModified() && !$this->isColumnModified(MembreTableMap::COL_UPDATED_AT)) {
                    $this->setUpdatedAt(\Propel\Runtime\Util\PropelDateTime::createHighPrecision());
                }
            }
            if ($ret) {
                $affectedRows = $this->doSave($con);
                if ($isInsert) {
                    $this->postInsert($con);
                } else {
                    $this->postUpdate($con);
                }
                $this->postSave($con);
                MembreTableMap::addInstanceToPool($this);
            } else {
                $affectedRows = 0;
            }

            return $affectedRows;
        });
    }

    /**
     * Performs the work of inserting or updating the row in the database.
     *
     * If the object is new, it inserts it; otherwise an update is performed.
     * All related objects are also updated in this method.
     *
     * @param      ConnectionInterface $con
     * @return int             The number of rows affected by this insert/update and any referring fk objects' save() operations.
     * @throws PropelException
     * @see save()
     */
    protected function doSave(ConnectionInterface $con)
    {
        $affectedRows = 0; // initialize var to track total num of affected rows
        if (!$this->alreadyInSave) {
            $this->alreadyInSave = true;

            // We call the save method on the following object(s) if they
            // were passed to this object by their corresponding set
            // method.  This object relates to these object(s) by a
            // foreign key reference.

            if ($this->aIndividuTitulaire !== null) {
                if ($this->aIndividuTitulaire->isModified() || $this->aIndividuTitulaire->isNew()) {
                    $affectedRows += $this->aIndividuTitulaire->save($con);
                }
                $this->setIndividuTitulaire($this->aIndividuTitulaire);
            }

            if ($this->isNew() || $this->isModified()) {
                // persist changes
                if ($this->isNew()) {
                    $this->doInsert($con);
                    $affectedRows += 1;
                } else {
                    $affectedRows += $this->doUpdate($con);
                }
                $this->resetModified();
            }

            if ($this->individusScheduledForDeletion !== null) {
                if (!$this->individusScheduledForDeletion->isEmpty()) {
                    $pks = array();
                    foreach ($this->individusScheduledForDeletion as $entry) {
                        $entryPk = [];

                        $entryPk[1] = $this->getIdMembre();
                        $entryPk[0] = $entry->getIdIndividu();
                        $pks[] = $entryPk;
                    }

                    \MembreIndividuQuery::create()
                        ->filterByPrimaryKeys($pks)
                        ->delete($con);

                    $this->individusScheduledForDeletion = null;
                }

            }

            if ($this->collIndividus) {
                foreach ($this->collIndividus as $individu) {
                    if (!$individu->isDeleted() && ($individu->isNew() || $individu->isModified())) {
                        $individu->save($con);
                    }
                }
            }


            if ($this->membreIndividusScheduledForDeletion !== null) {
                if (!$this->membreIndividusScheduledForDeletion->isEmpty()) {
                    \MembreIndividuQuery::create()
                        ->filterByPrimaryKeys($this->membreIndividusScheduledForDeletion->getPrimaryKeys(false))
                        ->delete($con);
                    $this->membreIndividusScheduledForDeletion = null;
                }
            }

            if ($this->collMembreIndividus !== null) {
                foreach ($this->collMembreIndividus as $referrerFK) {
                    if (!$referrerFK->isDeleted() && ($referrerFK->isNew() || $referrerFK->isModified())) {
                        $affectedRows += $referrerFK->save($con);
                    }
                }
            }

            if ($this->servicerendusScheduledForDeletion !== null) {
                if (!$this->servicerendusScheduledForDeletion->isEmpty()) {
                    \ServicerenduQuery::create()
                        ->filterByPrimaryKeys($this->servicerendusScheduledForDeletion->getPrimaryKeys(false))
                        ->delete($con);
                    $this->servicerendusScheduledForDeletion = null;
                }
            }

            if ($this->collServicerendus !== null) {
                foreach ($this->collServicerendus as $referrerFK) {
                    if (!$referrerFK->isDeleted() && ($referrerFK->isNew() || $referrerFK->isModified())) {
                        $affectedRows += $referrerFK->save($con);
                    }
                }
            }

            if ($this->courrierLiensScheduledForDeletion !== null) {
                if (!$this->courrierLiensScheduledForDeletion->isEmpty()) {
                    \CourrierLienQuery::create()
                        ->filterByPrimaryKeys($this->courrierLiensScheduledForDeletion->getPrimaryKeys(false))
                        ->delete($con);
                    $this->courrierLiensScheduledForDeletion = null;
                }
            }

            if ($this->collCourrierLiens !== null) {
                foreach ($this->collCourrierLiens as $referrerFK) {
                    if (!$referrerFK->isDeleted() && ($referrerFK->isNew() || $referrerFK->isModified())) {
                        $affectedRows += $referrerFK->save($con);
                    }
                }
            }

            $this->alreadyInSave = false;

        }

        return $affectedRows;
    } // doSave()

    /**
     * Insert the row in the database.
     *
     * @param      ConnectionInterface $con
     *
     * @throws PropelException
     * @see doSave()
     */
    protected function doInsert(ConnectionInterface $con)
    {
        $modifiedColumns = array();
        $index = 0;

        $this->modifiedColumns[MembreTableMap::COL_ID_MEMBRE] = true;
        if (null !== $this->id_membre) {
            throw new PropelException('Cannot insert a value for auto-increment primary key (' . MembreTableMap::COL_ID_MEMBRE . ')');
        }

         // check the columns in natural order for more readable SQL queries
        if ($this->isColumnModified(MembreTableMap::COL_ID_MEMBRE)) {
            $modifiedColumns[':p' . $index++]  = '`id_membre`';
        }
        if ($this->isColumnModified(MembreTableMap::COL_ID_INDIVIDU_TITULAIRE)) {
            $modifiedColumns[':p' . $index++]  = '`id_individu_titulaire`';
        }
        if ($this->isColumnModified(MembreTableMap::COL_IDENTIFIANT_INTERNE)) {
            $modifiedColumns[':p' . $index++]  = '`identifiant_interne`';
        }
        if ($this->isColumnModified(MembreTableMap::COL_CIVILITE)) {
            $modifiedColumns[':p' . $index++]  = '`civilite`';
        }
        if ($this->isColumnModified(MembreTableMap::COL_NOM)) {
            $modifiedColumns[':p' . $index++]  = '`nom`';
        }
        if ($this->isColumnModified(MembreTableMap::COL_NOMCOURT)) {
            $modifiedColumns[':p' . $index++]  = '`nomcourt`';
        }
        if ($this->isColumnModified(MembreTableMap::COL_DATE_SORTIE)) {
            $modifiedColumns[':p' . $index++]  = '`date_sortie`';
        }
        if ($this->isColumnModified(MembreTableMap::COL_OBSERVATION)) {
            $modifiedColumns[':p' . $index++]  = '`observation`';
        }
        if ($this->isColumnModified(MembreTableMap::COL_CREATED_AT)) {
            $modifiedColumns[':p' . $index++]  = '`created_at`';
        }
        if ($this->isColumnModified(MembreTableMap::COL_UPDATED_AT)) {
            $modifiedColumns[':p' . $index++]  = '`updated_at`';
        }

        $sql = sprintf(
            'INSERT INTO `asso_membres` (%s) VALUES (%s)',
            implode(', ', $modifiedColumns),
            implode(', ', array_keys($modifiedColumns))
        );

        try {
            $stmt = $con->prepare($sql);
            foreach ($modifiedColumns as $identifier => $columnName) {
                switch ($columnName) {
                    case '`id_membre`':
                        $stmt->bindValue($identifier, $this->id_membre, PDO::PARAM_INT);
                        break;
                    case '`id_individu_titulaire`':
                        $stmt->bindValue($identifier, $this->id_individu_titulaire, PDO::PARAM_INT);
                        break;
                    case '`identifiant_interne`':
                        $stmt->bindValue($identifier, $this->identifiant_interne, PDO::PARAM_STR);
                        break;
                    case '`civilite`':
                        $stmt->bindValue($identifier, $this->civilite, PDO::PARAM_STR);
                        break;
                    case '`nom`':
                        $stmt->bindValue($identifier, $this->nom, PDO::PARAM_STR);
                        break;
                    case '`nomcourt`':
                        $stmt->bindValue($identifier, $this->nomcourt, PDO::PARAM_STR);
                        break;
                    case '`date_sortie`':
                        $stmt->bindValue($identifier, $this->date_sortie ? $this->date_sortie->format("Y-m-d H:i:s.u") : null, PDO::PARAM_STR);
                        break;
                    case '`observation`':
                        $stmt->bindValue($identifier, $this->observation, PDO::PARAM_STR);
                        break;
                    case '`created_at`':
                        $stmt->bindValue($identifier, $this->created_at ? $this->created_at->format("Y-m-d H:i:s.u") : null, PDO::PARAM_STR);
                        break;
                    case '`updated_at`':
                        $stmt->bindValue($identifier, $this->updated_at ? $this->updated_at->format("Y-m-d H:i:s.u") : null, PDO::PARAM_STR);
                        break;
                }
            }
            $stmt->execute();
        } catch (Exception $e) {
            Propel::log($e->getMessage(), Propel::LOG_ERR);
            throw new PropelException(sprintf('Unable to execute INSERT statement [%s]', $sql), 0, $e);
        }

        try {
            $pk = $con->lastInsertId();
        } catch (Exception $e) {
            throw new PropelException('Unable to get autoincrement id.', 0, $e);
        }
        $this->setIdMembre($pk);

        $this->setNew(false);
    }

    /**
     * Update the row in the database.
     *
     * @param      ConnectionInterface $con
     *
     * @return Integer Number of updated rows
     * @see doSave()
     */
    protected function doUpdate(ConnectionInterface $con)
    {
        $selectCriteria = $this->buildPkeyCriteria();
        $valuesCriteria = $this->buildCriteria();

        return $selectCriteria->doUpdate($valuesCriteria, $con);
    }

    /**
     * Retrieves a field from the object by name passed in as a string.
     *
     * @param      string $name name
     * @param      string $type The type of fieldname the $name is of:
     *                     one of the class type constants TableMap::TYPE_PHPNAME, TableMap::TYPE_CAMELNAME
     *                     TableMap::TYPE_COLNAME, TableMap::TYPE_FIELDNAME, TableMap::TYPE_NUM.
     *                     Defaults to TableMap::TYPE_FIELDNAME.
     * @return mixed Value of field.
     */
    public function getByName($name, $type = TableMap::TYPE_FIELDNAME)
    {
        $pos = MembreTableMap::translateFieldName($name, $type, TableMap::TYPE_NUM);
        $field = $this->getByPosition($pos);

        return $field;
    }

    /**
     * Retrieves a field from the object by Position as specified in the xml schema.
     * Zero-based.
     *
     * @param      int $pos position in xml schema
     * @return mixed Value of field at $pos
     */
    public function getByPosition($pos)
    {
        switch ($pos) {
            case 0:
                return $this->getIdMembre();
                break;
            case 1:
                return $this->getIdIndividuTitulaire();
                break;
            case 2:
                return $this->getIdentifiantInterne();
                break;
            case 3:
                return $this->getCivilite();
                break;
            case 4:
                return $this->getNom();
                break;
            case 5:
                return $this->getNomcourt();
                break;
            case 6:
                return $this->getDateSortie();
                break;
            case 7:
                return $this->getObservation();
                break;
            case 8:
                return $this->getCreatedAt();
                break;
            case 9:
                return $this->getUpdatedAt();
                break;
            default:
                return null;
                break;
        } // switch()
    }

    /**
     * Exports the object as an array.
     *
     * You can specify the key type of the array by passing one of the class
     * type constants.
     *
     * @param     string  $keyType (optional) One of the class type constants TableMap::TYPE_PHPNAME, TableMap::TYPE_CAMELNAME,
     *                    TableMap::TYPE_COLNAME, TableMap::TYPE_FIELDNAME, TableMap::TYPE_NUM.
     *                    Defaults to TableMap::TYPE_FIELDNAME.
     * @param     boolean $includeLazyLoadColumns (optional) Whether to include lazy loaded columns. Defaults to TRUE.
     * @param     array $alreadyDumpedObjects List of objects to skip to avoid recursion
     * @param     boolean $includeForeignObjects (optional) Whether to include hydrated related objects. Default to FALSE.
     *
     * @return array an associative array containing the field names (as keys) and field values
     */
    public function toArray($keyType = TableMap::TYPE_FIELDNAME, $includeLazyLoadColumns = true, $alreadyDumpedObjects = array(), $includeForeignObjects = false)
    {

        if (isset($alreadyDumpedObjects['Membre'][$this->hashCode()])) {
            return '*RECURSION*';
        }
        $alreadyDumpedObjects['Membre'][$this->hashCode()] = true;
        $keys = MembreTableMap::getFieldNames($keyType);
        $result = array(
            $keys[0] => $this->getIdMembre(),
            $keys[1] => $this->getIdIndividuTitulaire(),
            $keys[2] => $this->getIdentifiantInterne(),
            $keys[3] => $this->getCivilite(),
            $keys[4] => $this->getNom(),
            $keys[5] => $this->getNomcourt(),
            $keys[6] => $this->getDateSortie(),
            $keys[7] => $this->getObservation(),
            $keys[8] => $this->getCreatedAt(),
            $keys[9] => $this->getUpdatedAt(),
        );
        if ($result[$keys[6]] instanceof \DateTimeInterface) {
            $result[$keys[6]] = $result[$keys[6]]->format('c');
        }

        if ($result[$keys[8]] instanceof \DateTimeInterface) {
            $result[$keys[8]] = $result[$keys[8]]->format('c');
        }

        if ($result[$keys[9]] instanceof \DateTimeInterface) {
            $result[$keys[9]] = $result[$keys[9]]->format('c');
        }

        $virtualColumns = $this->virtualColumns;
        foreach ($virtualColumns as $key => $virtualColumn) {
            $result[$key] = $virtualColumn;
        }

        if ($includeForeignObjects) {
            if (null !== $this->aIndividuTitulaire) {

                switch ($keyType) {
                    case TableMap::TYPE_CAMELNAME:
                        $key = 'individu';
                        break;
                    case TableMap::TYPE_FIELDNAME:
                        $key = 'asso_individus';
                        break;
                    default:
                        $key = 'IndividuTitulaire';
                }

                $result[$key] = $this->aIndividuTitulaire->toArray($keyType, $includeLazyLoadColumns,  $alreadyDumpedObjects, true);
            }
            if (null !== $this->collMembreIndividus) {

                switch ($keyType) {
                    case TableMap::TYPE_CAMELNAME:
                        $key = 'membreIndividus';
                        break;
                    case TableMap::TYPE_FIELDNAME:
                        $key = 'asso_membres_individuses';
                        break;
                    default:
                        $key = 'MembreIndividus';
                }

                $result[$key] = $this->collMembreIndividus->toArray(null, false, $keyType, $includeLazyLoadColumns, $alreadyDumpedObjects);
            }
            if (null !== $this->collServicerendus) {

                switch ($keyType) {
                    case TableMap::TYPE_CAMELNAME:
                        $key = 'servicerendus';
                        break;
                    case TableMap::TYPE_FIELDNAME:
                        $key = 'asso_servicerenduses';
                        break;
                    default:
                        $key = 'Servicerendus';
                }

                $result[$key] = $this->collServicerendus->toArray(null, false, $keyType, $includeLazyLoadColumns, $alreadyDumpedObjects);
            }
            if (null !== $this->collCourrierLiens) {

                switch ($keyType) {
                    case TableMap::TYPE_CAMELNAME:
                        $key = 'courrierLiens';
                        break;
                    case TableMap::TYPE_FIELDNAME:
                        $key = 'com_courriers_lienss';
                        break;
                    default:
                        $key = 'CourrierLiens';
                }

                $result[$key] = $this->collCourrierLiens->toArray(null, false, $keyType, $includeLazyLoadColumns, $alreadyDumpedObjects);
            }
        }

        return $result;
    }

    /**
     * Sets a field from the object by name passed in as a string.
     *
     * @param  string $name
     * @param  mixed  $value field value
     * @param  string $type The type of fieldname the $name is of:
     *                one of the class type constants TableMap::TYPE_PHPNAME, TableMap::TYPE_CAMELNAME
     *                TableMap::TYPE_COLNAME, TableMap::TYPE_FIELDNAME, TableMap::TYPE_NUM.
     *                Defaults to TableMap::TYPE_FIELDNAME.
     * @return $this|\Membre
     */
    public function setByName($name, $value, $type = TableMap::TYPE_FIELDNAME)
    {
        $pos = MembreTableMap::translateFieldName($name, $type, TableMap::TYPE_NUM);

        return $this->setByPosition($pos, $value);
    }

    /**
     * Sets a field from the object by Position as specified in the xml schema.
     * Zero-based.
     *
     * @param  int $pos position in xml schema
     * @param  mixed $value field value
     * @return $this|\Membre
     */
    public function setByPosition($pos, $value)
    {
        switch ($pos) {
            case 0:
                $this->setIdMembre($value);
                break;
            case 1:
                $this->setIdIndividuTitulaire($value);
                break;
            case 2:
                $this->setIdentifiantInterne($value);
                break;
            case 3:
                $this->setCivilite($value);
                break;
            case 4:
                $this->setNom($value);
                break;
            case 5:
                $this->setNomcourt($value);
                break;
            case 6:
                $this->setDateSortie($value);
                break;
            case 7:
                $this->setObservation($value);
                break;
            case 8:
                $this->setCreatedAt($value);
                break;
            case 9:
                $this->setUpdatedAt($value);
                break;
        } // switch()

        return $this;
    }

    /**
     * Populates the object using an array.
     *
     * This is particularly useful when populating an object from one of the
     * request arrays (e.g. $_POST).  This method goes through the column
     * names, checking to see whether a matching key exists in populated
     * array. If so the setByName() method is called for that column.
     *
     * You can specify the key type of the array by additionally passing one
     * of the class type constants TableMap::TYPE_PHPNAME, TableMap::TYPE_CAMELNAME,
     * TableMap::TYPE_COLNAME, TableMap::TYPE_FIELDNAME, TableMap::TYPE_NUM.
     * The default key type is the column's TableMap::TYPE_FIELDNAME.
     *
     * @param      array  $arr     An array to populate the object from.
     * @param      string $keyType The type of keys the array uses.
     * @return void
     */
    public function fromArray($arr, $keyType = TableMap::TYPE_FIELDNAME)
    {
        $keys = MembreTableMap::getFieldNames($keyType);

        if (array_key_exists($keys[0], $arr)) {
            $this->setIdMembre($arr[$keys[0]]);
        }
        if (array_key_exists($keys[1], $arr)) {
            $this->setIdIndividuTitulaire($arr[$keys[1]]);
        }
        if (array_key_exists($keys[2], $arr)) {
            $this->setIdentifiantInterne($arr[$keys[2]]);
        }
        if (array_key_exists($keys[3], $arr)) {
            $this->setCivilite($arr[$keys[3]]);
        }
        if (array_key_exists($keys[4], $arr)) {
            $this->setNom($arr[$keys[4]]);
        }
        if (array_key_exists($keys[5], $arr)) {
            $this->setNomcourt($arr[$keys[5]]);
        }
        if (array_key_exists($keys[6], $arr)) {
            $this->setDateSortie($arr[$keys[6]]);
        }
        if (array_key_exists($keys[7], $arr)) {
            $this->setObservation($arr[$keys[7]]);
        }
        if (array_key_exists($keys[8], $arr)) {
            $this->setCreatedAt($arr[$keys[8]]);
        }
        if (array_key_exists($keys[9], $arr)) {
            $this->setUpdatedAt($arr[$keys[9]]);
        }
    }

     /**
     * Populate the current object from a string, using a given parser format
     * <code>
     * $book = new Book();
     * $book->importFrom('JSON', '{"Id":9012,"Title":"Don Juan","ISBN":"0140422161","Price":12.99,"PublisherId":1234,"AuthorId":5678}');
     * </code>
     *
     * You can specify the key type of the array by additionally passing one
     * of the class type constants TableMap::TYPE_PHPNAME, TableMap::TYPE_CAMELNAME,
     * TableMap::TYPE_COLNAME, TableMap::TYPE_FIELDNAME, TableMap::TYPE_NUM.
     * The default key type is the column's TableMap::TYPE_FIELDNAME.
     *
     * @param mixed $parser A AbstractParser instance,
     *                       or a format name ('XML', 'YAML', 'JSON', 'CSV')
     * @param string $data The source data to import from
     * @param string $keyType The type of keys the array uses.
     *
     * @return $this|\Membre The current object, for fluid interface
     */
    public function importFrom($parser, $data, $keyType = TableMap::TYPE_FIELDNAME)
    {
        if (!$parser instanceof AbstractParser) {
            $parser = AbstractParser::getParser($parser);
        }

        $this->fromArray($parser->toArray($data), $keyType);

        return $this;
    }

    /**
     * Build a Criteria object containing the values of all modified columns in this object.
     *
     * @return Criteria The Criteria object containing all modified values.
     */
    public function buildCriteria()
    {
        $criteria = new Criteria(MembreTableMap::DATABASE_NAME);

        if ($this->isColumnModified(MembreTableMap::COL_ID_MEMBRE)) {
            $criteria->add(MembreTableMap::COL_ID_MEMBRE, $this->id_membre);
        }
        if ($this->isColumnModified(MembreTableMap::COL_ID_INDIVIDU_TITULAIRE)) {
            $criteria->add(MembreTableMap::COL_ID_INDIVIDU_TITULAIRE, $this->id_individu_titulaire);
        }
        if ($this->isColumnModified(MembreTableMap::COL_IDENTIFIANT_INTERNE)) {
            $criteria->add(MembreTableMap::COL_IDENTIFIANT_INTERNE, $this->identifiant_interne);
        }
        if ($this->isColumnModified(MembreTableMap::COL_CIVILITE)) {
            $criteria->add(MembreTableMap::COL_CIVILITE, $this->civilite);
        }
        if ($this->isColumnModified(MembreTableMap::COL_NOM)) {
            $criteria->add(MembreTableMap::COL_NOM, $this->nom);
        }
        if ($this->isColumnModified(MembreTableMap::COL_NOMCOURT)) {
            $criteria->add(MembreTableMap::COL_NOMCOURT, $this->nomcourt);
        }
        if ($this->isColumnModified(MembreTableMap::COL_DATE_SORTIE)) {
            $criteria->add(MembreTableMap::COL_DATE_SORTIE, $this->date_sortie);
        }
        if ($this->isColumnModified(MembreTableMap::COL_OBSERVATION)) {
            $criteria->add(MembreTableMap::COL_OBSERVATION, $this->observation);
        }
        if ($this->isColumnModified(MembreTableMap::COL_CREATED_AT)) {
            $criteria->add(MembreTableMap::COL_CREATED_AT, $this->created_at);
        }
        if ($this->isColumnModified(MembreTableMap::COL_UPDATED_AT)) {
            $criteria->add(MembreTableMap::COL_UPDATED_AT, $this->updated_at);
        }

        return $criteria;
    }

    /**
     * Builds a Criteria object containing the primary key for this object.
     *
     * Unlike buildCriteria() this method includes the primary key values regardless
     * of whether or not they have been modified.
     *
     * @throws LogicException if no primary key is defined
     *
     * @return Criteria The Criteria object containing value(s) for primary key(s).
     */
    public function buildPkeyCriteria()
    {
        $criteria = ChildMembreQuery::create();
        $criteria->add(MembreTableMap::COL_ID_MEMBRE, $this->id_membre);

        return $criteria;
    }

    /**
     * If the primary key is not null, return the hashcode of the
     * primary key. Otherwise, return the hash code of the object.
     *
     * @return int Hashcode
     */
    public function hashCode()
    {
        $validPk = null !== $this->getIdMembre();

        $validPrimaryKeyFKs = 0;
        $primaryKeyFKs = [];

        if ($validPk) {
            return crc32(json_encode($this->getPrimaryKey(), JSON_UNESCAPED_UNICODE));
        } elseif ($validPrimaryKeyFKs) {
            return crc32(json_encode($primaryKeyFKs, JSON_UNESCAPED_UNICODE));
        }

        return spl_object_hash($this);
    }

    /**
     * Returns the primary key for this object (row).
     * @return string
     */
    public function getPrimaryKey()
    {
        return $this->getIdMembre();
    }

    /**
     * Generic method to set the primary key (id_membre column).
     *
     * @param       string $key Primary key.
     * @return void
     */
    public function setPrimaryKey($key)
    {
        $this->setIdMembre($key);
    }

    /**
     * Returns true if the primary key for this object is null.
     * @return boolean
     */
    public function isPrimaryKeyNull()
    {
        return null === $this->getIdMembre();
    }

    /**
     * Sets contents of passed object to values from current object.
     *
     * If desired, this method can also make copies of all associated (fkey referrers)
     * objects.
     *
     * @param      object $copyObj An object of \Membre (or compatible) type.
     * @param      boolean $deepCopy Whether to also copy all rows that refer (by fkey) to the current row.
     * @param      boolean $makeNew Whether to reset autoincrement PKs and make the object new.
     * @throws PropelException
     */
    public function copyInto($copyObj, $deepCopy = false, $makeNew = true)
    {
        $copyObj->setIdIndividuTitulaire($this->getIdIndividuTitulaire());
        $copyObj->setIdentifiantInterne($this->getIdentifiantInterne());
        $copyObj->setCivilite($this->getCivilite());
        $copyObj->setNom($this->getNom());
        $copyObj->setNomcourt($this->getNomcourt());
        $copyObj->setDateSortie($this->getDateSortie());
        $copyObj->setObservation($this->getObservation());
        $copyObj->setCreatedAt($this->getCreatedAt());
        $copyObj->setUpdatedAt($this->getUpdatedAt());

        if ($deepCopy) {
            // important: temporarily setNew(false) because this affects the behavior of
            // the getter/setter methods for fkey referrer objects.
            $copyObj->setNew(false);

            foreach ($this->getMembreIndividus() as $relObj) {
                if ($relObj !== $this) {  // ensure that we don't try to copy a reference to ourselves
                    $copyObj->addMembreIndividu($relObj->copy($deepCopy));
                }
            }

            foreach ($this->getServicerendus() as $relObj) {
                if ($relObj !== $this) {  // ensure that we don't try to copy a reference to ourselves
                    $copyObj->addServicerendu($relObj->copy($deepCopy));
                }
            }

            foreach ($this->getCourrierLiens() as $relObj) {
                if ($relObj !== $this) {  // ensure that we don't try to copy a reference to ourselves
                    $copyObj->addCourrierLien($relObj->copy($deepCopy));
                }
            }

        } // if ($deepCopy)

        if ($makeNew) {
            $copyObj->setNew(true);
            $copyObj->setIdMembre(NULL); // this is a auto-increment column, so set to default value
        }
    }

    /**
     * Makes a copy of this object that will be inserted as a new row in table when saved.
     * It creates a new object filling in the simple attributes, but skipping any primary
     * keys that are defined for the table.
     *
     * If desired, this method can also make copies of all associated (fkey referrers)
     * objects.
     *
     * @param  boolean $deepCopy Whether to also copy all rows that refer (by fkey) to the current row.
     * @return \Membre Clone of current object.
     * @throws PropelException
     */
    public function copy($deepCopy = false)
    {
        // we use get_class(), because this might be a subclass
        $clazz = get_class($this);
        $copyObj = new $clazz();
        $this->copyInto($copyObj, $deepCopy);

        return $copyObj;
    }

    /**
     * Declares an association between this object and a ChildIndividu object.
     *
     * @param  ChildIndividu $v
     * @return $this|\Membre The current object (for fluent API support)
     * @throws PropelException
     */
    public function setIndividuTitulaire(ChildIndividu $v = null)
    {
        if ($v === null) {
            $this->setIdIndividuTitulaire(NULL);
        } else {
            $this->setIdIndividuTitulaire($v->getIdIndividu());
        }

        $this->aIndividuTitulaire = $v;

        // Add binding for other direction of this n:n relationship.
        // If this object has already been added to the ChildIndividu object, it will not be re-added.
        if ($v !== null) {
            $v->addMembres0($this);
        }


        return $this;
    }


    /**
     * Get the associated ChildIndividu object
     *
     * @param  ConnectionInterface $con Optional Connection object.
     * @return ChildIndividu The associated ChildIndividu object.
     * @throws PropelException
     */
    public function getIndividuTitulaire(ConnectionInterface $con = null)
    {
        if ($this->aIndividuTitulaire === null && (($this->id_individu_titulaire !== "" && $this->id_individu_titulaire !== null))) {
            $this->aIndividuTitulaire = ChildIndividuQuery::create()->findPk($this->id_individu_titulaire, $con);
            /* The following can be used additionally to
                guarantee the related object contains a reference
                to this object.  This level of coupling may, however, be
                undesirable since it could result in an only partially populated collection
                in the referenced object.
                $this->aIndividuTitulaire->addMembres0s($this);
             */
        }

        return $this->aIndividuTitulaire;
    }


    /**
     * Initializes a collection based on the name of a relation.
     * Avoids crafting an 'init[$relationName]s' method name
     * that wouldn't work when StandardEnglishPluralizer is used.
     *
     * @param      string $relationName The name of the relation to initialize
     * @return void
     */
    public function initRelation($relationName)
    {
        if ('MembreIndividu' == $relationName) {
            $this->initMembreIndividus();
            return;
        }
        if ('Servicerendu' == $relationName) {
            $this->initServicerendus();
            return;
        }
        if ('CourrierLien' == $relationName) {
            $this->initCourrierLiens();
            return;
        }
    }

    /**
     * Clears out the collMembreIndividus collection
     *
     * This does not modify the database; however, it will remove any associated objects, causing
     * them to be refetched by subsequent calls to accessor method.
     *
     * @return void
     * @see        addMembreIndividus()
     */
    public function clearMembreIndividus()
    {
        $this->collMembreIndividus = null; // important to set this to NULL since that means it is uninitialized
    }

    /**
     * Reset is the collMembreIndividus collection loaded partially.
     */
    public function resetPartialMembreIndividus($v = true)
    {
        $this->collMembreIndividusPartial = $v;
    }

    /**
     * Initializes the collMembreIndividus collection.
     *
     * By default this just sets the collMembreIndividus collection to an empty array (like clearcollMembreIndividus());
     * however, you may wish to override this method in your stub class to provide setting appropriate
     * to your application -- for example, setting the initial array to the values stored in database.
     *
     * @param      boolean $overrideExisting If set to true, the method call initializes
     *                                        the collection even if it is not empty
     *
     * @return void
     */
    public function initMembreIndividus($overrideExisting = true)
    {
        if (null !== $this->collMembreIndividus && !$overrideExisting) {
            return;
        }

        $collectionClassName = MembreIndividuTableMap::getTableMap()->getCollectionClassName();

        $this->collMembreIndividus = new $collectionClassName;
        $this->collMembreIndividus->setModel('\MembreIndividu');
    }

    /**
     * Gets an array of ChildMembreIndividu objects which contain a foreign key that references this object.
     *
     * If the $criteria is not null, it is used to always fetch the results from the database.
     * Otherwise the results are fetched from the database the first time, then cached.
     * Next time the same method is called without $criteria, the cached collection is returned.
     * If this ChildMembre is new, it will return
     * an empty collection or the current collection; the criteria is ignored on a new object.
     *
     * @param      Criteria $criteria optional Criteria object to narrow the query
     * @param      ConnectionInterface $con optional connection object
     * @return ObjectCollection|ChildMembreIndividu[] List of ChildMembreIndividu objects
     * @throws PropelException
     */
    public function getMembreIndividus(Criteria $criteria = null, ConnectionInterface $con = null)
    {
        $partial = $this->collMembreIndividusPartial && !$this->isNew();
        if (null === $this->collMembreIndividus || null !== $criteria  || $partial) {
            if ($this->isNew() && null === $this->collMembreIndividus) {
                // return empty collection
                $this->initMembreIndividus();
            } else {
                $collMembreIndividus = ChildMembreIndividuQuery::create(null, $criteria)
                    ->filterByMembre($this)
                    ->find($con);

                if (null !== $criteria) {
                    if (false !== $this->collMembreIndividusPartial && count($collMembreIndividus)) {
                        $this->initMembreIndividus(false);

                        foreach ($collMembreIndividus as $obj) {
                            if (false == $this->collMembreIndividus->contains($obj)) {
                                $this->collMembreIndividus->append($obj);
                            }
                        }

                        $this->collMembreIndividusPartial = true;
                    }

                    return $collMembreIndividus;
                }

                if ($partial && $this->collMembreIndividus) {
                    foreach ($this->collMembreIndividus as $obj) {
                        if ($obj->isNew()) {
                            $collMembreIndividus[] = $obj;
                        }
                    }
                }

                $this->collMembreIndividus = $collMembreIndividus;
                $this->collMembreIndividusPartial = false;
            }
        }

        return $this->collMembreIndividus;
    }

    /**
     * Sets a collection of ChildMembreIndividu objects related by a one-to-many relationship
     * to the current object.
     * It will also schedule objects for deletion based on a diff between old objects (aka persisted)
     * and new objects from the given Propel collection.
     *
     * @param      Collection $membreIndividus A Propel collection.
     * @param      ConnectionInterface $con Optional connection object
     * @return $this|ChildMembre The current object (for fluent API support)
     */
    public function setMembreIndividus(Collection $membreIndividus, ConnectionInterface $con = null)
    {
        /** @var ChildMembreIndividu[] $membreIndividusToDelete */
        $membreIndividusToDelete = $this->getMembreIndividus(new Criteria(), $con)->diff($membreIndividus);


        //since at least one column in the foreign key is at the same time a PK
        //we can not just set a PK to NULL in the lines below. We have to store
        //a backup of all values, so we are able to manipulate these items based on the onDelete value later.
        $this->membreIndividusScheduledForDeletion = clone $membreIndividusToDelete;

        foreach ($membreIndividusToDelete as $membreIndividuRemoved) {
            $membreIndividuRemoved->setMembre(null);
        }

        $this->collMembreIndividus = null;
        foreach ($membreIndividus as $membreIndividu) {
            $this->addMembreIndividu($membreIndividu);
        }

        $this->collMembreIndividus = $membreIndividus;
        $this->collMembreIndividusPartial = false;

        return $this;
    }

    /**
     * Returns the number of related MembreIndividu objects.
     *
     * @param      Criteria $criteria
     * @param      boolean $distinct
     * @param      ConnectionInterface $con
     * @return int             Count of related MembreIndividu objects.
     * @throws PropelException
     */
    public function countMembreIndividus(Criteria $criteria = null, $distinct = false, ConnectionInterface $con = null)
    {
        $partial = $this->collMembreIndividusPartial && !$this->isNew();
        if (null === $this->collMembreIndividus || null !== $criteria || $partial) {
            if ($this->isNew() && null === $this->collMembreIndividus) {
                return 0;
            }

            if ($partial && !$criteria) {
                return count($this->getMembreIndividus());
            }

            $query = ChildMembreIndividuQuery::create(null, $criteria);
            if ($distinct) {
                $query->distinct();
            }

            return $query
                ->filterByMembre($this)
                ->count($con);
        }

        return count($this->collMembreIndividus);
    }

    /**
     * Method called to associate a ChildMembreIndividu object to this object
     * through the ChildMembreIndividu foreign key attribute.
     *
     * @param  ChildMembreIndividu $l ChildMembreIndividu
     * @return $this|\Membre The current object (for fluent API support)
     */
    public function addMembreIndividu(ChildMembreIndividu $l)
    {
        if ($this->collMembreIndividus === null) {
            $this->initMembreIndividus();
            $this->collMembreIndividusPartial = true;
        }

        if (!$this->collMembreIndividus->contains($l)) {
            $this->doAddMembreIndividu($l);

            if ($this->membreIndividusScheduledForDeletion and $this->membreIndividusScheduledForDeletion->contains($l)) {
                $this->membreIndividusScheduledForDeletion->remove($this->membreIndividusScheduledForDeletion->search($l));
            }
        }

        return $this;
    }

    /**
     * @param ChildMembreIndividu $membreIndividu The ChildMembreIndividu object to add.
     */
    protected function doAddMembreIndividu(ChildMembreIndividu $membreIndividu)
    {
        $this->collMembreIndividus[]= $membreIndividu;
        $membreIndividu->setMembre($this);
    }

    /**
     * @param  ChildMembreIndividu $membreIndividu The ChildMembreIndividu object to remove.
     * @return $this|ChildMembre The current object (for fluent API support)
     */
    public function removeMembreIndividu(ChildMembreIndividu $membreIndividu)
    {
        if ($this->getMembreIndividus()->contains($membreIndividu)) {
            $pos = $this->collMembreIndividus->search($membreIndividu);
            $this->collMembreIndividus->remove($pos);
            if (null === $this->membreIndividusScheduledForDeletion) {
                $this->membreIndividusScheduledForDeletion = clone $this->collMembreIndividus;
                $this->membreIndividusScheduledForDeletion->clear();
            }
            $this->membreIndividusScheduledForDeletion[]= clone $membreIndividu;
            $membreIndividu->setMembre(null);
        }

        return $this;
    }


    /**
     * If this collection has already been initialized with
     * an identical criteria, it returns the collection.
     * Otherwise if this Membre is new, it will return
     * an empty collection; or if this Membre has previously
     * been saved, it will retrieve related MembreIndividus from storage.
     *
     * This method is protected by default in order to keep the public
     * api reasonable.  You can provide public methods for those you
     * actually need in Membre.
     *
     * @param      Criteria $criteria optional Criteria object to narrow the query
     * @param      ConnectionInterface $con optional connection object
     * @param      string $joinBehavior optional join type to use (defaults to Criteria::LEFT_JOIN)
     * @return ObjectCollection|ChildMembreIndividu[] List of ChildMembreIndividu objects
     */
    public function getMembreIndividusJoinIndividu(Criteria $criteria = null, ConnectionInterface $con = null, $joinBehavior = Criteria::LEFT_JOIN)
    {
        $query = ChildMembreIndividuQuery::create(null, $criteria);
        $query->joinWith('Individu', $joinBehavior);

        return $this->getMembreIndividus($query, $con);
    }

    /**
     * Clears out the collServicerendus collection
     *
     * This does not modify the database; however, it will remove any associated objects, causing
     * them to be refetched by subsequent calls to accessor method.
     *
     * @return void
     * @see        addServicerendus()
     */
    public function clearServicerendus()
    {
        $this->collServicerendus = null; // important to set this to NULL since that means it is uninitialized
    }

    /**
     * Reset is the collServicerendus collection loaded partially.
     */
    public function resetPartialServicerendus($v = true)
    {
        $this->collServicerendusPartial = $v;
    }

    /**
     * Initializes the collServicerendus collection.
     *
     * By default this just sets the collServicerendus collection to an empty array (like clearcollServicerendus());
     * however, you may wish to override this method in your stub class to provide setting appropriate
     * to your application -- for example, setting the initial array to the values stored in database.
     *
     * @param      boolean $overrideExisting If set to true, the method call initializes
     *                                        the collection even if it is not empty
     *
     * @return void
     */
    public function initServicerendus($overrideExisting = true)
    {
        if (null !== $this->collServicerendus && !$overrideExisting) {
            return;
        }

        $collectionClassName = ServicerenduTableMap::getTableMap()->getCollectionClassName();

        $this->collServicerendus = new $collectionClassName;
        $this->collServicerendus->setModel('\Servicerendu');
    }

    /**
     * Gets an array of ChildServicerendu objects which contain a foreign key that references this object.
     *
     * If the $criteria is not null, it is used to always fetch the results from the database.
     * Otherwise the results are fetched from the database the first time, then cached.
     * Next time the same method is called without $criteria, the cached collection is returned.
     * If this ChildMembre is new, it will return
     * an empty collection or the current collection; the criteria is ignored on a new object.
     *
     * @param      Criteria $criteria optional Criteria object to narrow the query
     * @param      ConnectionInterface $con optional connection object
     * @return ObjectCollection|ChildServicerendu[] List of ChildServicerendu objects
     * @throws PropelException
     */
    public function getServicerendus(Criteria $criteria = null, ConnectionInterface $con = null)
    {
        $partial = $this->collServicerendusPartial && !$this->isNew();
        if (null === $this->collServicerendus || null !== $criteria  || $partial) {
            if ($this->isNew() && null === $this->collServicerendus) {
                // return empty collection
                $this->initServicerendus();
            } else {
                $collServicerendus = ChildServicerenduQuery::create(null, $criteria)
                    ->filterByMembre($this)
                    ->find($con);

                if (null !== $criteria) {
                    if (false !== $this->collServicerendusPartial && count($collServicerendus)) {
                        $this->initServicerendus(false);

                        foreach ($collServicerendus as $obj) {
                            if (false == $this->collServicerendus->contains($obj)) {
                                $this->collServicerendus->append($obj);
                            }
                        }

                        $this->collServicerendusPartial = true;
                    }

                    return $collServicerendus;
                }

                if ($partial && $this->collServicerendus) {
                    foreach ($this->collServicerendus as $obj) {
                        if ($obj->isNew()) {
                            $collServicerendus[] = $obj;
                        }
                    }
                }

                $this->collServicerendus = $collServicerendus;
                $this->collServicerendusPartial = false;
            }
        }

        return $this->collServicerendus;
    }

    /**
     * Sets a collection of ChildServicerendu objects related by a one-to-many relationship
     * to the current object.
     * It will also schedule objects for deletion based on a diff between old objects (aka persisted)
     * and new objects from the given Propel collection.
     *
     * @param      Collection $servicerendus A Propel collection.
     * @param      ConnectionInterface $con Optional connection object
     * @return $this|ChildMembre The current object (for fluent API support)
     */
    public function setServicerendus(Collection $servicerendus, ConnectionInterface $con = null)
    {
        /** @var ChildServicerendu[] $servicerendusToDelete */
        $servicerendusToDelete = $this->getServicerendus(new Criteria(), $con)->diff($servicerendus);


        $this->servicerendusScheduledForDeletion = $servicerendusToDelete;

        foreach ($servicerendusToDelete as $servicerenduRemoved) {
            $servicerenduRemoved->setMembre(null);
        }

        $this->collServicerendus = null;
        foreach ($servicerendus as $servicerendu) {
            $this->addServicerendu($servicerendu);
        }

        $this->collServicerendus = $servicerendus;
        $this->collServicerendusPartial = false;

        return $this;
    }

    /**
     * Returns the number of related Servicerendu objects.
     *
     * @param      Criteria $criteria
     * @param      boolean $distinct
     * @param      ConnectionInterface $con
     * @return int             Count of related Servicerendu objects.
     * @throws PropelException
     */
    public function countServicerendus(Criteria $criteria = null, $distinct = false, ConnectionInterface $con = null)
    {
        $partial = $this->collServicerendusPartial && !$this->isNew();
        if (null === $this->collServicerendus || null !== $criteria || $partial) {
            if ($this->isNew() && null === $this->collServicerendus) {
                return 0;
            }

            if ($partial && !$criteria) {
                return count($this->getServicerendus());
            }

            $query = ChildServicerenduQuery::create(null, $criteria);
            if ($distinct) {
                $query->distinct();
            }

            return $query
                ->filterByMembre($this)
                ->count($con);
        }

        return count($this->collServicerendus);
    }

    /**
     * Method called to associate a ChildServicerendu object to this object
     * through the ChildServicerendu foreign key attribute.
     *
     * @param  ChildServicerendu $l ChildServicerendu
     * @return $this|\Membre The current object (for fluent API support)
     */
    public function addServicerendu(ChildServicerendu $l)
    {
        if ($this->collServicerendus === null) {
            $this->initServicerendus();
            $this->collServicerendusPartial = true;
        }

        if (!$this->collServicerendus->contains($l)) {
            $this->doAddServicerendu($l);

            if ($this->servicerendusScheduledForDeletion and $this->servicerendusScheduledForDeletion->contains($l)) {
                $this->servicerendusScheduledForDeletion->remove($this->servicerendusScheduledForDeletion->search($l));
            }
        }

        return $this;
    }

    /**
     * @param ChildServicerendu $servicerendu The ChildServicerendu object to add.
     */
    protected function doAddServicerendu(ChildServicerendu $servicerendu)
    {
        $this->collServicerendus[]= $servicerendu;
        $servicerendu->setMembre($this);
    }

    /**
     * @param  ChildServicerendu $servicerendu The ChildServicerendu object to remove.
     * @return $this|ChildMembre The current object (for fluent API support)
     */
    public function removeServicerendu(ChildServicerendu $servicerendu)
    {
        if ($this->getServicerendus()->contains($servicerendu)) {
            $pos = $this->collServicerendus->search($servicerendu);
            $this->collServicerendus->remove($pos);
            if (null === $this->servicerendusScheduledForDeletion) {
                $this->servicerendusScheduledForDeletion = clone $this->collServicerendus;
                $this->servicerendusScheduledForDeletion->clear();
            }
            $this->servicerendusScheduledForDeletion[]= $servicerendu;
            $servicerendu->setMembre(null);
        }

        return $this;
    }


    /**
     * If this collection has already been initialized with
     * an identical criteria, it returns the collection.
     * Otherwise if this Membre is new, it will return
     * an empty collection; or if this Membre has previously
     * been saved, it will retrieve related Servicerendus from storage.
     *
     * This method is protected by default in order to keep the public
     * api reasonable.  You can provide public methods for those you
     * actually need in Membre.
     *
     * @param      Criteria $criteria optional Criteria object to narrow the query
     * @param      ConnectionInterface $con optional connection object
     * @param      string $joinBehavior optional join type to use (defaults to Criteria::LEFT_JOIN)
     * @return ObjectCollection|ChildServicerendu[] List of ChildServicerendu objects
     */
    public function getServicerendusJoinPrestation(Criteria $criteria = null, ConnectionInterface $con = null, $joinBehavior = Criteria::LEFT_JOIN)
    {
        $query = ChildServicerenduQuery::create(null, $criteria);
        $query->joinWith('Prestation', $joinBehavior);

        return $this->getServicerendus($query, $con);
    }


    /**
     * If this collection has already been initialized with
     * an identical criteria, it returns the collection.
     * Otherwise if this Membre is new, it will return
     * an empty collection; or if this Membre has previously
     * been saved, it will retrieve related Servicerendus from storage.
     *
     * This method is protected by default in order to keep the public
     * api reasonable.  You can provide public methods for those you
     * actually need in Membre.
     *
     * @param      Criteria $criteria optional Criteria object to narrow the query
     * @param      ConnectionInterface $con optional connection object
     * @param      string $joinBehavior optional join type to use (defaults to Criteria::LEFT_JOIN)
     * @return ObjectCollection|ChildServicerendu[] List of ChildServicerendu objects
     */
    public function getServicerendusJoinEntite(Criteria $criteria = null, ConnectionInterface $con = null, $joinBehavior = Criteria::LEFT_JOIN)
    {
        $query = ChildServicerenduQuery::create(null, $criteria);
        $query->joinWith('Entite', $joinBehavior);

        return $this->getServicerendus($query, $con);
    }


    /**
     * If this collection has already been initialized with
     * an identical criteria, it returns the collection.
     * Otherwise if this Membre is new, it will return
     * an empty collection; or if this Membre has previously
     * been saved, it will retrieve related Servicerendus from storage.
     *
     * This method is protected by default in order to keep the public
     * api reasonable.  You can provide public methods for those you
     * actually need in Membre.
     *
     * @param      Criteria $criteria optional Criteria object to narrow the query
     * @param      ConnectionInterface $con optional connection object
     * @param      string $joinBehavior optional join type to use (defaults to Criteria::LEFT_JOIN)
     * @return ObjectCollection|ChildServicerendu[] List of ChildServicerendu objects
     */
    public function getServicerendusJoinServicerenduRelatedByOrigine(Criteria $criteria = null, ConnectionInterface $con = null, $joinBehavior = Criteria::LEFT_JOIN)
    {
        $query = ChildServicerenduQuery::create(null, $criteria);
        $query->joinWith('ServicerenduRelatedByOrigine', $joinBehavior);

        return $this->getServicerendus($query, $con);
    }

    /**
     * Clears out the collCourrierLiens collection
     *
     * This does not modify the database; however, it will remove any associated objects, causing
     * them to be refetched by subsequent calls to accessor method.
     *
     * @return void
     * @see        addCourrierLiens()
     */
    public function clearCourrierLiens()
    {
        $this->collCourrierLiens = null; // important to set this to NULL since that means it is uninitialized
    }

    /**
     * Reset is the collCourrierLiens collection loaded partially.
     */
    public function resetPartialCourrierLiens($v = true)
    {
        $this->collCourrierLiensPartial = $v;
    }

    /**
     * Initializes the collCourrierLiens collection.
     *
     * By default this just sets the collCourrierLiens collection to an empty array (like clearcollCourrierLiens());
     * however, you may wish to override this method in your stub class to provide setting appropriate
     * to your application -- for example, setting the initial array to the values stored in database.
     *
     * @param      boolean $overrideExisting If set to true, the method call initializes
     *                                        the collection even if it is not empty
     *
     * @return void
     */
    public function initCourrierLiens($overrideExisting = true)
    {
        if (null !== $this->collCourrierLiens && !$overrideExisting) {
            return;
        }

        $collectionClassName = CourrierLienTableMap::getTableMap()->getCollectionClassName();

        $this->collCourrierLiens = new $collectionClassName;
        $this->collCourrierLiens->setModel('\CourrierLien');
    }

    /**
     * Gets an array of ChildCourrierLien objects which contain a foreign key that references this object.
     *
     * If the $criteria is not null, it is used to always fetch the results from the database.
     * Otherwise the results are fetched from the database the first time, then cached.
     * Next time the same method is called without $criteria, the cached collection is returned.
     * If this ChildMembre is new, it will return
     * an empty collection or the current collection; the criteria is ignored on a new object.
     *
     * @param      Criteria $criteria optional Criteria object to narrow the query
     * @param      ConnectionInterface $con optional connection object
     * @return ObjectCollection|ChildCourrierLien[] List of ChildCourrierLien objects
     * @throws PropelException
     */
    public function getCourrierLiens(Criteria $criteria = null, ConnectionInterface $con = null)
    {
        $partial = $this->collCourrierLiensPartial && !$this->isNew();
        if (null === $this->collCourrierLiens || null !== $criteria  || $partial) {
            if ($this->isNew() && null === $this->collCourrierLiens) {
                // return empty collection
                $this->initCourrierLiens();
            } else {
                $collCourrierLiens = ChildCourrierLienQuery::create(null, $criteria)
                    ->filterByMembre($this)
                    ->find($con);

                if (null !== $criteria) {
                    if (false !== $this->collCourrierLiensPartial && count($collCourrierLiens)) {
                        $this->initCourrierLiens(false);

                        foreach ($collCourrierLiens as $obj) {
                            if (false == $this->collCourrierLiens->contains($obj)) {
                                $this->collCourrierLiens->append($obj);
                            }
                        }

                        $this->collCourrierLiensPartial = true;
                    }

                    return $collCourrierLiens;
                }

                if ($partial && $this->collCourrierLiens) {
                    foreach ($this->collCourrierLiens as $obj) {
                        if ($obj->isNew()) {
                            $collCourrierLiens[] = $obj;
                        }
                    }
                }

                $this->collCourrierLiens = $collCourrierLiens;
                $this->collCourrierLiensPartial = false;
            }
        }

        return $this->collCourrierLiens;
    }

    /**
     * Sets a collection of ChildCourrierLien objects related by a one-to-many relationship
     * to the current object.
     * It will also schedule objects for deletion based on a diff between old objects (aka persisted)
     * and new objects from the given Propel collection.
     *
     * @param      Collection $courrierLiens A Propel collection.
     * @param      ConnectionInterface $con Optional connection object
     * @return $this|ChildMembre The current object (for fluent API support)
     */
    public function setCourrierLiens(Collection $courrierLiens, ConnectionInterface $con = null)
    {
        /** @var ChildCourrierLien[] $courrierLiensToDelete */
        $courrierLiensToDelete = $this->getCourrierLiens(new Criteria(), $con)->diff($courrierLiens);


        $this->courrierLiensScheduledForDeletion = $courrierLiensToDelete;

        foreach ($courrierLiensToDelete as $courrierLienRemoved) {
            $courrierLienRemoved->setMembre(null);
        }

        $this->collCourrierLiens = null;
        foreach ($courrierLiens as $courrierLien) {
            $this->addCourrierLien($courrierLien);
        }

        $this->collCourrierLiens = $courrierLiens;
        $this->collCourrierLiensPartial = false;

        return $this;
    }

    /**
     * Returns the number of related CourrierLien objects.
     *
     * @param      Criteria $criteria
     * @param      boolean $distinct
     * @param      ConnectionInterface $con
     * @return int             Count of related CourrierLien objects.
     * @throws PropelException
     */
    public function countCourrierLiens(Criteria $criteria = null, $distinct = false, ConnectionInterface $con = null)
    {
        $partial = $this->collCourrierLiensPartial && !$this->isNew();
        if (null === $this->collCourrierLiens || null !== $criteria || $partial) {
            if ($this->isNew() && null === $this->collCourrierLiens) {
                return 0;
            }

            if ($partial && !$criteria) {
                return count($this->getCourrierLiens());
            }

            $query = ChildCourrierLienQuery::create(null, $criteria);
            if ($distinct) {
                $query->distinct();
            }

            return $query
                ->filterByMembre($this)
                ->count($con);
        }

        return count($this->collCourrierLiens);
    }

    /**
     * Method called to associate a ChildCourrierLien object to this object
     * through the ChildCourrierLien foreign key attribute.
     *
     * @param  ChildCourrierLien $l ChildCourrierLien
     * @return $this|\Membre The current object (for fluent API support)
     */
    public function addCourrierLien(ChildCourrierLien $l)
    {
        if ($this->collCourrierLiens === null) {
            $this->initCourrierLiens();
            $this->collCourrierLiensPartial = true;
        }

        if (!$this->collCourrierLiens->contains($l)) {
            $this->doAddCourrierLien($l);

            if ($this->courrierLiensScheduledForDeletion and $this->courrierLiensScheduledForDeletion->contains($l)) {
                $this->courrierLiensScheduledForDeletion->remove($this->courrierLiensScheduledForDeletion->search($l));
            }
        }

        return $this;
    }

    /**
     * @param ChildCourrierLien $courrierLien The ChildCourrierLien object to add.
     */
    protected function doAddCourrierLien(ChildCourrierLien $courrierLien)
    {
        $this->collCourrierLiens[]= $courrierLien;
        $courrierLien->setMembre($this);
    }

    /**
     * @param  ChildCourrierLien $courrierLien The ChildCourrierLien object to remove.
     * @return $this|ChildMembre The current object (for fluent API support)
     */
    public function removeCourrierLien(ChildCourrierLien $courrierLien)
    {
        if ($this->getCourrierLiens()->contains($courrierLien)) {
            $pos = $this->collCourrierLiens->search($courrierLien);
            $this->collCourrierLiens->remove($pos);
            if (null === $this->courrierLiensScheduledForDeletion) {
                $this->courrierLiensScheduledForDeletion = clone $this->collCourrierLiens;
                $this->courrierLiensScheduledForDeletion->clear();
            }
            $this->courrierLiensScheduledForDeletion[]= $courrierLien;
            $courrierLien->setMembre(null);
        }

        return $this;
    }


    /**
     * If this collection has already been initialized with
     * an identical criteria, it returns the collection.
     * Otherwise if this Membre is new, it will return
     * an empty collection; or if this Membre has previously
     * been saved, it will retrieve related CourrierLiens from storage.
     *
     * This method is protected by default in order to keep the public
     * api reasonable.  You can provide public methods for those you
     * actually need in Membre.
     *
     * @param      Criteria $criteria optional Criteria object to narrow the query
     * @param      ConnectionInterface $con optional connection object
     * @param      string $joinBehavior optional join type to use (defaults to Criteria::LEFT_JOIN)
     * @return ObjectCollection|ChildCourrierLien[] List of ChildCourrierLien objects
     */
    public function getCourrierLiensJoinIndividu(Criteria $criteria = null, ConnectionInterface $con = null, $joinBehavior = Criteria::LEFT_JOIN)
    {
        $query = ChildCourrierLienQuery::create(null, $criteria);
        $query->joinWith('Individu', $joinBehavior);

        return $this->getCourrierLiens($query, $con);
    }


    /**
     * If this collection has already been initialized with
     * an identical criteria, it returns the collection.
     * Otherwise if this Membre is new, it will return
     * an empty collection; or if this Membre has previously
     * been saved, it will retrieve related CourrierLiens from storage.
     *
     * This method is protected by default in order to keep the public
     * api reasonable.  You can provide public methods for those you
     * actually need in Membre.
     *
     * @param      Criteria $criteria optional Criteria object to narrow the query
     * @param      ConnectionInterface $con optional connection object
     * @param      string $joinBehavior optional join type to use (defaults to Criteria::LEFT_JOIN)
     * @return ObjectCollection|ChildCourrierLien[] List of ChildCourrierLien objects
     */
    public function getCourrierLiensJoinCourrier(Criteria $criteria = null, ConnectionInterface $con = null, $joinBehavior = Criteria::LEFT_JOIN)
    {
        $query = ChildCourrierLienQuery::create(null, $criteria);
        $query->joinWith('Courrier', $joinBehavior);

        return $this->getCourrierLiens($query, $con);
    }

    /**
     * Clears out the collIndividus collection
     *
     * This does not modify the database; however, it will remove any associated objects, causing
     * them to be refetched by subsequent calls to accessor method.
     *
     * @return void
     * @see        addIndividus()
     */
    public function clearIndividus()
    {
        $this->collIndividus = null; // important to set this to NULL since that means it is uninitialized
    }

    /**
     * Initializes the collIndividus crossRef collection.
     *
     * By default this just sets the collIndividus collection to an empty collection (like clearIndividus());
     * however, you may wish to override this method in your stub class to provide setting appropriate
     * to your application -- for example, setting the initial array to the values stored in database.
     *
     * @return void
     */
    public function initIndividus()
    {
        $collectionClassName = MembreIndividuTableMap::getTableMap()->getCollectionClassName();

        $this->collIndividus = new $collectionClassName;
        $this->collIndividusPartial = true;
        $this->collIndividus->setModel('\Individu');
    }

    /**
     * Checks if the collIndividus collection is loaded.
     *
     * @return bool
     */
    public function isIndividusLoaded()
    {
        return null !== $this->collIndividus;
    }

    /**
     * Gets a collection of ChildIndividu objects related by a many-to-many relationship
     * to the current object by way of the asso_membres_individus cross-reference table.
     *
     * If the $criteria is not null, it is used to always fetch the results from the database.
     * Otherwise the results are fetched from the database the first time, then cached.
     * Next time the same method is called without $criteria, the cached collection is returned.
     * If this ChildMembre is new, it will return
     * an empty collection or the current collection; the criteria is ignored on a new object.
     *
     * @param      Criteria $criteria Optional query object to filter the query
     * @param      ConnectionInterface $con Optional connection object
     *
     * @return ObjectCollection|ChildIndividu[] List of ChildIndividu objects
     */
    public function getIndividus(Criteria $criteria = null, ConnectionInterface $con = null)
    {
        $partial = $this->collIndividusPartial && !$this->isNew();
        if (null === $this->collIndividus || null !== $criteria || $partial) {
            if ($this->isNew()) {
                // return empty collection
                if (null === $this->collIndividus) {
                    $this->initIndividus();
                }
            } else {

                $query = ChildIndividuQuery::create(null, $criteria)
                    ->filterByMembre($this);
                $collIndividus = $query->find($con);
                if (null !== $criteria) {
                    return $collIndividus;
                }

                if ($partial && $this->collIndividus) {
                    //make sure that already added objects gets added to the list of the database.
                    foreach ($this->collIndividus as $obj) {
                        if (!$collIndividus->contains($obj)) {
                            $collIndividus[] = $obj;
                        }
                    }
                }

                $this->collIndividus = $collIndividus;
                $this->collIndividusPartial = false;
            }
        }

        return $this->collIndividus;
    }

    /**
     * Sets a collection of Individu objects related by a many-to-many relationship
     * to the current object by way of the asso_membres_individus cross-reference table.
     * It will also schedule objects for deletion based on a diff between old objects (aka persisted)
     * and new objects from the given Propel collection.
     *
     * @param  Collection $individus A Propel collection.
     * @param  ConnectionInterface $con Optional connection object
     * @return $this|ChildMembre The current object (for fluent API support)
     */
    public function setIndividus(Collection $individus, ConnectionInterface $con = null)
    {
        $this->clearIndividus();
        $currentIndividus = $this->getIndividus();

        $individusScheduledForDeletion = $currentIndividus->diff($individus);

        foreach ($individusScheduledForDeletion as $toDelete) {
            $this->removeIndividu($toDelete);
        }

        foreach ($individus as $individu) {
            if (!$currentIndividus->contains($individu)) {
                $this->doAddIndividu($individu);
            }
        }

        $this->collIndividusPartial = false;
        $this->collIndividus = $individus;

        return $this;
    }

    /**
     * Gets the number of Individu objects related by a many-to-many relationship
     * to the current object by way of the asso_membres_individus cross-reference table.
     *
     * @param      Criteria $criteria Optional query object to filter the query
     * @param      boolean $distinct Set to true to force count distinct
     * @param      ConnectionInterface $con Optional connection object
     *
     * @return int the number of related Individu objects
     */
    public function countIndividus(Criteria $criteria = null, $distinct = false, ConnectionInterface $con = null)
    {
        $partial = $this->collIndividusPartial && !$this->isNew();
        if (null === $this->collIndividus || null !== $criteria || $partial) {
            if ($this->isNew() && null === $this->collIndividus) {
                return 0;
            } else {

                if ($partial && !$criteria) {
                    return count($this->getIndividus());
                }

                $query = ChildIndividuQuery::create(null, $criteria);
                if ($distinct) {
                    $query->distinct();
                }

                return $query
                    ->filterByMembre($this)
                    ->count($con);
            }
        } else {
            return count($this->collIndividus);
        }
    }

    /**
     * Associate a ChildIndividu to this object
     * through the asso_membres_individus cross reference table.
     *
     * @param ChildIndividu $individu
     * @return ChildMembre The current object (for fluent API support)
     */
    public function addIndividu(ChildIndividu $individu)
    {
        if ($this->collIndividus === null) {
            $this->initIndividus();
        }

        if (!$this->getIndividus()->contains($individu)) {
            // only add it if the **same** object is not already associated
            $this->collIndividus->push($individu);
            $this->doAddIndividu($individu);
        }

        return $this;
    }

    /**
     *
     * @param ChildIndividu $individu
     */
    protected function doAddIndividu(ChildIndividu $individu)
    {
        $membreIndividu = new ChildMembreIndividu();

        $membreIndividu->setIndividu($individu);

        $membreIndividu->setMembre($this);

        $this->addMembreIndividu($membreIndividu);

        // set the back reference to this object directly as using provided method either results
        // in endless loop or in multiple relations
        if (!$individu->isMembresLoaded()) {
            $individu->initMembres();
            $individu->getMembres()->push($this);
        } elseif (!$individu->getMembres()->contains($this)) {
            $individu->getMembres()->push($this);
        }

    }

    /**
     * Remove individu of this object
     * through the asso_membres_individus cross reference table.
     *
     * @param ChildIndividu $individu
     * @return ChildMembre The current object (for fluent API support)
     */
    public function removeIndividu(ChildIndividu $individu)
    {
        if ($this->getIndividus()->contains($individu)) {
            $membreIndividu = new ChildMembreIndividu();
            $membreIndividu->setIndividu($individu);
            if ($individu->isMembresLoaded()) {
                //remove the back reference if available
                $individu->getMembres()->removeObject($this);
            }

            $membreIndividu->setMembre($this);
            $this->removeMembreIndividu(clone $membreIndividu);
            $membreIndividu->clear();

            $this->collIndividus->remove($this->collIndividus->search($individu));

            if (null === $this->individusScheduledForDeletion) {
                $this->individusScheduledForDeletion = clone $this->collIndividus;
                $this->individusScheduledForDeletion->clear();
            }

            $this->individusScheduledForDeletion->push($individu);
        }


        return $this;
    }

    /**
     * Clears the current object, sets all attributes to their default values and removes
     * outgoing references as well as back-references (from other objects to this one. Results probably in a database
     * change of those foreign objects when you call `save` there).
     */
    public function clear()
    {
        if (null !== $this->aIndividuTitulaire) {
            $this->aIndividuTitulaire->removeMembres0($this);
        }
        $this->id_membre = null;
        $this->id_individu_titulaire = null;
        $this->identifiant_interne = null;
        $this->civilite = null;
        $this->nom = null;
        $this->nomcourt = null;
        $this->date_sortie = null;
        $this->observation = null;
        $this->created_at = null;
        $this->updated_at = null;
        $this->alreadyInSave = false;
        $this->clearAllReferences();
        $this->resetModified();
        $this->setNew(true);
        $this->setDeleted(false);
    }

    /**
     * Resets all references and back-references to other model objects or collections of model objects.
     *
     * This method is used to reset all php object references (not the actual reference in the database).
     * Necessary for object serialisation.
     *
     * @param      boolean $deep Whether to also clear the references on all referrer objects.
     */
    public function clearAllReferences($deep = false)
    {
        if ($deep) {
            if ($this->collMembreIndividus) {
                foreach ($this->collMembreIndividus as $o) {
                    $o->clearAllReferences($deep);
                }
            }
            if ($this->collServicerendus) {
                foreach ($this->collServicerendus as $o) {
                    $o->clearAllReferences($deep);
                }
            }
            if ($this->collCourrierLiens) {
                foreach ($this->collCourrierLiens as $o) {
                    $o->clearAllReferences($deep);
                }
            }
            if ($this->collIndividus) {
                foreach ($this->collIndividus as $o) {
                    $o->clearAllReferences($deep);
                }
            }
        } // if ($deep)

        $this->collMembreIndividus = null;
        $this->collServicerendus = null;
        $this->collCourrierLiens = null;
        $this->collIndividus = null;
        $this->aIndividuTitulaire = null;
    }

    /**
     * Return the string representation of this object
     *
     * @return string
     */
    public function __toString()
    {
        return (string) $this->exportTo(MembreTableMap::DEFAULT_STRING_FORMAT);
    }

    // timestampable behavior

    /**
     * Mark the current object so that the update date doesn't get updated during next save
     *
     * @return     $this|ChildMembre The current object (for fluent API support)
     */
    public function keepUpdateDateUnchanged()
    {
        $this->modifiedColumns[MembreTableMap::COL_UPDATED_AT] = true;

        return $this;
    }

    // archivable behavior

    /**
     * Get an archived version of the current object.
     *
     * @param ConnectionInterface $con Optional connection object
     *
     * @return     ChildAssoMembresArchive An archive object, or null if the current object was never archived
     */
    public function getArchive(ConnectionInterface $con = null)
    {
        if ($this->isNew()) {
            return null;
        }
        $archive = ChildAssoMembresArchiveQuery::create()
            ->filterByPrimaryKey($this->getPrimaryKey())
            ->findOne($con);

        return $archive;
    }
    /**
     * Copy the data of the current object into a $archiveTablePhpName archive object.
     * The archived object is then saved.
     * If the current object has already been archived, the archived object
     * is updated and not duplicated.
     *
     * @param ConnectionInterface $con Optional connection object
     *
     * @throws PropelException If the object is new
     *
     * @return     ChildAssoMembresArchive The archive object based on this object
     */
    public function archive(ConnectionInterface $con = null)
    {
        if ($this->isNew()) {
            throw new PropelException('New objects cannot be archived. You must save the current object before calling archive().');
        }
        $archive = $this->getArchive($con);
        if (!$archive) {
            $archive = new ChildAssoMembresArchive();
            $archive->setPrimaryKey($this->getPrimaryKey());
        }
        $this->copyInto($archive, $deepCopy = false, $makeNew = false);
        $archive->setArchivedAt(time());
        $archive->save($con);

        return $archive;
    }

    /**
     * Revert the the current object to the state it had when it was last archived.
     * The object must be saved afterwards if the changes must persist.
     *
     * @param ConnectionInterface $con Optional connection object
     *
     * @throws PropelException If the object has no corresponding archive.
     *
     * @return $this|ChildMembre The current object (for fluent API support)
     */
    public function restoreFromArchive(ConnectionInterface $con = null)
    {
        $archive = $this->getArchive($con);
        if (!$archive) {
            throw new PropelException('The current object has never been archived and cannot be restored');
        }
        $this->populateFromArchive($archive);

        return $this;
    }

    /**
     * Populates the the current object based on a $archiveTablePhpName archive object.
     *
     * @param      ChildAssoMembresArchive $archive An archived object based on the same class
      * @param      Boolean $populateAutoIncrementPrimaryKeys
     *               If true, autoincrement columns are copied from the archive object.
     *               If false, autoincrement columns are left intact.
      *
     * @return     ChildMembre The current object (for fluent API support)
     */
    public function populateFromArchive($archive, $populateAutoIncrementPrimaryKeys = false) {
        if ($populateAutoIncrementPrimaryKeys) {
            $this->setIdMembre($archive->getIdMembre());
        }
        $this->setIdIndividuTitulaire($archive->getIdIndividuTitulaire());
        $this->setIdentifiantInterne($archive->getIdentifiantInterne());
        $this->setCivilite($archive->getCivilite());
        $this->setNom($archive->getNom());
        $this->setNomcourt($archive->getNomcourt());
        $this->setDateSortie($archive->getDateSortie());
        $this->setObservation($archive->getObservation());
        $this->setCreatedAt($archive->getCreatedAt());
        $this->setUpdatedAt($archive->getUpdatedAt());

        return $this;
    }

    /**
     * Removes the object from the database without archiving it.
     *
     * @param ConnectionInterface $con Optional connection object
     *
     * @return $this|ChildMembre The current object (for fluent API support)
     */
    public function deleteWithoutArchive(ConnectionInterface $con = null)
    {
        $this->archiveOnDelete = false;

        return $this->delete($con);
    }

    /**
     * Code to be run before persisting the object
     * @param  ConnectionInterface $con
     * @return boolean
     */
    public function preSave(ConnectionInterface $con = null)
    {
        if (is_callable('parent::preSave')) {
            return parent::preSave($con);
        }
        return true;
    }

    /**
     * Code to be run after persisting the object
     * @param ConnectionInterface $con
     */
    public function postSave(ConnectionInterface $con = null)
    {
        if (is_callable('parent::postSave')) {
            parent::postSave($con);
        }
    }

    /**
     * Code to be run before inserting to database
     * @param  ConnectionInterface $con
     * @return boolean
     */
    public function preInsert(ConnectionInterface $con = null)
    {
        if (is_callable('parent::preInsert')) {
            return parent::preInsert($con);
        }
        return true;
    }

    /**
     * Code to be run after inserting to database
     * @param ConnectionInterface $con
     */
    public function postInsert(ConnectionInterface $con = null)
    {
        if (is_callable('parent::postInsert')) {
            parent::postInsert($con);
        }
    }

    /**
     * Code to be run before updating the object in database
     * @param  ConnectionInterface $con
     * @return boolean
     */
    public function preUpdate(ConnectionInterface $con = null)
    {
        if (is_callable('parent::preUpdate')) {
            return parent::preUpdate($con);
        }
        return true;
    }

    /**
     * Code to be run after updating the object in database
     * @param ConnectionInterface $con
     */
    public function postUpdate(ConnectionInterface $con = null)
    {
        if (is_callable('parent::postUpdate')) {
            parent::postUpdate($con);
        }
    }

    /**
     * Code to be run before deleting the object in database
     * @param  ConnectionInterface $con
     * @return boolean
     */
    public function preDelete(ConnectionInterface $con = null)
    {
        if (is_callable('parent::preDelete')) {
            return parent::preDelete($con);
        }
        return true;
    }

    /**
     * Code to be run after deleting the object in database
     * @param ConnectionInterface $con
     */
    public function postDelete(ConnectionInterface $con = null)
    {
        if (is_callable('parent::postDelete')) {
            parent::postDelete($con);
        }
    }


    /**
     * Derived method to catches calls to undefined methods.
     *
     * Provides magic import/export method support (fromXML()/toXML(), fromYAML()/toYAML(), etc.).
     * Allows to define default __call() behavior if you overwrite __call()
     *
     * @param string $name
     * @param mixed  $params
     *
     * @return array|string
     */
    public function __call($name, $params)
    {
        if (0 === strpos($name, 'get')) {
            $virtualColumn = substr($name, 3);
            if ($this->hasVirtualColumn($virtualColumn)) {
                return $this->getVirtualColumn($virtualColumn);
            }

            $virtualColumn = lcfirst($virtualColumn);
            if ($this->hasVirtualColumn($virtualColumn)) {
                return $this->getVirtualColumn($virtualColumn);
            }
        }

        if (0 === strpos($name, 'from')) {
            $format = substr($name, 4);

            return $this->importFrom($format, reset($params));
        }

        if (0 === strpos($name, 'to')) {
            $format = substr($name, 2);
            $includeLazyLoadColumns = isset($params[0]) ? $params[0] : true;

            return $this->exportTo($format, $includeLazyLoadColumns);
        }

        throw new BadMethodCallException(sprintf('Call to undefined method: %s.', $name));
    }

}
