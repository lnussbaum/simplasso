<?php

namespace Base;

use \AssoPaiementsArchive as ChildAssoPaiementsArchive;
use \Paiement as ChildPaiement;
use \PaiementQuery as ChildPaiementQuery;
use \Exception;
use \PDO;
use Map\PaiementTableMap;
use Propel\Runtime\Propel;
use Propel\Runtime\ActiveQuery\Criteria;
use Propel\Runtime\ActiveQuery\ModelCriteria;
use Propel\Runtime\ActiveQuery\ModelJoin;
use Propel\Runtime\Collection\ObjectCollection;
use Propel\Runtime\Connection\ConnectionInterface;
use Propel\Runtime\Exception\PropelException;

/**
 * Base class that represents a query for the 'asso_paiements' table.
 *
 *
 *
 * @method     ChildPaiementQuery orderByIdPaiement($order = Criteria::ASC) Order by the id_paiement column
 * @method     ChildPaiementQuery orderByObjet($order = Criteria::ASC) Order by the objet column
 * @method     ChildPaiementQuery orderByIdObjet($order = Criteria::ASC) Order by the id_objet column
 * @method     ChildPaiementQuery orderByIdEntite($order = Criteria::ASC) Order by the id_entite column
 * @method     ChildPaiementQuery orderByIdTresor($order = Criteria::ASC) Order by the id_tresor column
 * @method     ChildPaiementQuery orderByMontant($order = Criteria::ASC) Order by the montant column
 * @method     ChildPaiementQuery orderByNumero($order = Criteria::ASC) Order by the numero column
 * @method     ChildPaiementQuery orderByDateCheque($order = Criteria::ASC) Order by the date_cheque column
 * @method     ChildPaiementQuery orderByDateEnregistrement($order = Criteria::ASC) Order by the date_enregistrement column
 * @method     ChildPaiementQuery orderByRemise($order = Criteria::ASC) Order by the remise column
 * @method     ChildPaiementQuery orderByComptabilise($order = Criteria::ASC) Order by the comptabilise column
 * @method     ChildPaiementQuery orderByObservation($order = Criteria::ASC) Order by the observation column
 * @method     ChildPaiementQuery orderByCreatedAt($order = Criteria::ASC) Order by the created_at column
 * @method     ChildPaiementQuery orderByUpdatedAt($order = Criteria::ASC) Order by the updated_at column
 *
 * @method     ChildPaiementQuery groupByIdPaiement() Group by the id_paiement column
 * @method     ChildPaiementQuery groupByObjet() Group by the objet column
 * @method     ChildPaiementQuery groupByIdObjet() Group by the id_objet column
 * @method     ChildPaiementQuery groupByIdEntite() Group by the id_entite column
 * @method     ChildPaiementQuery groupByIdTresor() Group by the id_tresor column
 * @method     ChildPaiementQuery groupByMontant() Group by the montant column
 * @method     ChildPaiementQuery groupByNumero() Group by the numero column
 * @method     ChildPaiementQuery groupByDateCheque() Group by the date_cheque column
 * @method     ChildPaiementQuery groupByDateEnregistrement() Group by the date_enregistrement column
 * @method     ChildPaiementQuery groupByRemise() Group by the remise column
 * @method     ChildPaiementQuery groupByComptabilise() Group by the comptabilise column
 * @method     ChildPaiementQuery groupByObservation() Group by the observation column
 * @method     ChildPaiementQuery groupByCreatedAt() Group by the created_at column
 * @method     ChildPaiementQuery groupByUpdatedAt() Group by the updated_at column
 *
 * @method     ChildPaiementQuery leftJoin($relation) Adds a LEFT JOIN clause to the query
 * @method     ChildPaiementQuery rightJoin($relation) Adds a RIGHT JOIN clause to the query
 * @method     ChildPaiementQuery innerJoin($relation) Adds a INNER JOIN clause to the query
 *
 * @method     ChildPaiementQuery leftJoinWith($relation) Adds a LEFT JOIN clause and with to the query
 * @method     ChildPaiementQuery rightJoinWith($relation) Adds a RIGHT JOIN clause and with to the query
 * @method     ChildPaiementQuery innerJoinWith($relation) Adds a INNER JOIN clause and with to the query
 *
 * @method     ChildPaiementQuery leftJoinEntite($relationAlias = null) Adds a LEFT JOIN clause to the query using the Entite relation
 * @method     ChildPaiementQuery rightJoinEntite($relationAlias = null) Adds a RIGHT JOIN clause to the query using the Entite relation
 * @method     ChildPaiementQuery innerJoinEntite($relationAlias = null) Adds a INNER JOIN clause to the query using the Entite relation
 *
 * @method     ChildPaiementQuery joinWithEntite($joinType = Criteria::INNER_JOIN) Adds a join clause and with to the query using the Entite relation
 *
 * @method     ChildPaiementQuery leftJoinWithEntite() Adds a LEFT JOIN clause and with to the query using the Entite relation
 * @method     ChildPaiementQuery rightJoinWithEntite() Adds a RIGHT JOIN clause and with to the query using the Entite relation
 * @method     ChildPaiementQuery innerJoinWithEntite() Adds a INNER JOIN clause and with to the query using the Entite relation
 *
 * @method     ChildPaiementQuery leftJoinTresor($relationAlias = null) Adds a LEFT JOIN clause to the query using the Tresor relation
 * @method     ChildPaiementQuery rightJoinTresor($relationAlias = null) Adds a RIGHT JOIN clause to the query using the Tresor relation
 * @method     ChildPaiementQuery innerJoinTresor($relationAlias = null) Adds a INNER JOIN clause to the query using the Tresor relation
 *
 * @method     ChildPaiementQuery joinWithTresor($joinType = Criteria::INNER_JOIN) Adds a join clause and with to the query using the Tresor relation
 *
 * @method     ChildPaiementQuery leftJoinWithTresor() Adds a LEFT JOIN clause and with to the query using the Tresor relation
 * @method     ChildPaiementQuery rightJoinWithTresor() Adds a RIGHT JOIN clause and with to the query using the Tresor relation
 * @method     ChildPaiementQuery innerJoinWithTresor() Adds a INNER JOIN clause and with to the query using the Tresor relation
 *
 * @method     ChildPaiementQuery leftJoinServicepaiement($relationAlias = null) Adds a LEFT JOIN clause to the query using the Servicepaiement relation
 * @method     ChildPaiementQuery rightJoinServicepaiement($relationAlias = null) Adds a RIGHT JOIN clause to the query using the Servicepaiement relation
 * @method     ChildPaiementQuery innerJoinServicepaiement($relationAlias = null) Adds a INNER JOIN clause to the query using the Servicepaiement relation
 *
 * @method     ChildPaiementQuery joinWithServicepaiement($joinType = Criteria::INNER_JOIN) Adds a join clause and with to the query using the Servicepaiement relation
 *
 * @method     ChildPaiementQuery leftJoinWithServicepaiement() Adds a LEFT JOIN clause and with to the query using the Servicepaiement relation
 * @method     ChildPaiementQuery rightJoinWithServicepaiement() Adds a RIGHT JOIN clause and with to the query using the Servicepaiement relation
 * @method     ChildPaiementQuery innerJoinWithServicepaiement() Adds a INNER JOIN clause and with to the query using the Servicepaiement relation
 *
 * @method     \EntiteQuery|\TresorQuery|\ServicepaiementQuery endUse() Finalizes a secondary criteria and merges it with its primary Criteria
 *
 * @method     ChildPaiement findOne(ConnectionInterface $con = null) Return the first ChildPaiement matching the query
 * @method     ChildPaiement findOneOrCreate(ConnectionInterface $con = null) Return the first ChildPaiement matching the query, or a new ChildPaiement object populated from the query conditions when no match is found
 *
 * @method     ChildPaiement findOneByIdPaiement(string $id_paiement) Return the first ChildPaiement filtered by the id_paiement column
 * @method     ChildPaiement findOneByObjet(string $objet) Return the first ChildPaiement filtered by the objet column
 * @method     ChildPaiement findOneByIdObjet(string $id_objet) Return the first ChildPaiement filtered by the id_objet column
 * @method     ChildPaiement findOneByIdEntite(string $id_entite) Return the first ChildPaiement filtered by the id_entite column
 * @method     ChildPaiement findOneByIdTresor(string $id_tresor) Return the first ChildPaiement filtered by the id_tresor column
 * @method     ChildPaiement findOneByMontant(double $montant) Return the first ChildPaiement filtered by the montant column
 * @method     ChildPaiement findOneByNumero(string $numero) Return the first ChildPaiement filtered by the numero column
 * @method     ChildPaiement findOneByDateCheque(string $date_cheque) Return the first ChildPaiement filtered by the date_cheque column
 * @method     ChildPaiement findOneByDateEnregistrement(string $date_enregistrement) Return the first ChildPaiement filtered by the date_enregistrement column
 * @method     ChildPaiement findOneByRemise(string $remise) Return the first ChildPaiement filtered by the remise column
 * @method     ChildPaiement findOneByComptabilise(boolean $comptabilise) Return the first ChildPaiement filtered by the comptabilise column
 * @method     ChildPaiement findOneByObservation(string $observation) Return the first ChildPaiement filtered by the observation column
 * @method     ChildPaiement findOneByCreatedAt(string $created_at) Return the first ChildPaiement filtered by the created_at column
 * @method     ChildPaiement findOneByUpdatedAt(string $updated_at) Return the first ChildPaiement filtered by the updated_at column *

 * @method     ChildPaiement requirePk($key, ConnectionInterface $con = null) Return the ChildPaiement by primary key and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildPaiement requireOne(ConnectionInterface $con = null) Return the first ChildPaiement matching the query and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 *
 * @method     ChildPaiement requireOneByIdPaiement(string $id_paiement) Return the first ChildPaiement filtered by the id_paiement column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildPaiement requireOneByObjet(string $objet) Return the first ChildPaiement filtered by the objet column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildPaiement requireOneByIdObjet(string $id_objet) Return the first ChildPaiement filtered by the id_objet column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildPaiement requireOneByIdEntite(string $id_entite) Return the first ChildPaiement filtered by the id_entite column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildPaiement requireOneByIdTresor(string $id_tresor) Return the first ChildPaiement filtered by the id_tresor column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildPaiement requireOneByMontant(double $montant) Return the first ChildPaiement filtered by the montant column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildPaiement requireOneByNumero(string $numero) Return the first ChildPaiement filtered by the numero column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildPaiement requireOneByDateCheque(string $date_cheque) Return the first ChildPaiement filtered by the date_cheque column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildPaiement requireOneByDateEnregistrement(string $date_enregistrement) Return the first ChildPaiement filtered by the date_enregistrement column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildPaiement requireOneByRemise(string $remise) Return the first ChildPaiement filtered by the remise column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildPaiement requireOneByComptabilise(boolean $comptabilise) Return the first ChildPaiement filtered by the comptabilise column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildPaiement requireOneByObservation(string $observation) Return the first ChildPaiement filtered by the observation column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildPaiement requireOneByCreatedAt(string $created_at) Return the first ChildPaiement filtered by the created_at column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildPaiement requireOneByUpdatedAt(string $updated_at) Return the first ChildPaiement filtered by the updated_at column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 *
 * @method     ChildPaiement[]|ObjectCollection find(ConnectionInterface $con = null) Return ChildPaiement objects based on current ModelCriteria
 * @method     ChildPaiement[]|ObjectCollection findByIdPaiement(string $id_paiement) Return ChildPaiement objects filtered by the id_paiement column
 * @method     ChildPaiement[]|ObjectCollection findByObjet(string $objet) Return ChildPaiement objects filtered by the objet column
 * @method     ChildPaiement[]|ObjectCollection findByIdObjet(string $id_objet) Return ChildPaiement objects filtered by the id_objet column
 * @method     ChildPaiement[]|ObjectCollection findByIdEntite(string $id_entite) Return ChildPaiement objects filtered by the id_entite column
 * @method     ChildPaiement[]|ObjectCollection findByIdTresor(string $id_tresor) Return ChildPaiement objects filtered by the id_tresor column
 * @method     ChildPaiement[]|ObjectCollection findByMontant(double $montant) Return ChildPaiement objects filtered by the montant column
 * @method     ChildPaiement[]|ObjectCollection findByNumero(string $numero) Return ChildPaiement objects filtered by the numero column
 * @method     ChildPaiement[]|ObjectCollection findByDateCheque(string $date_cheque) Return ChildPaiement objects filtered by the date_cheque column
 * @method     ChildPaiement[]|ObjectCollection findByDateEnregistrement(string $date_enregistrement) Return ChildPaiement objects filtered by the date_enregistrement column
 * @method     ChildPaiement[]|ObjectCollection findByRemise(string $remise) Return ChildPaiement objects filtered by the remise column
 * @method     ChildPaiement[]|ObjectCollection findByComptabilise(boolean $comptabilise) Return ChildPaiement objects filtered by the comptabilise column
 * @method     ChildPaiement[]|ObjectCollection findByObservation(string $observation) Return ChildPaiement objects filtered by the observation column
 * @method     ChildPaiement[]|ObjectCollection findByCreatedAt(string $created_at) Return ChildPaiement objects filtered by the created_at column
 * @method     ChildPaiement[]|ObjectCollection findByUpdatedAt(string $updated_at) Return ChildPaiement objects filtered by the updated_at column
 * @method     ChildPaiement[]|\Propel\Runtime\Util\PropelModelPager paginate($page = 1, $maxPerPage = 10, ConnectionInterface $con = null) Issue a SELECT query based on the current ModelCriteria and uses a page and a maximum number of results per page to compute an offset and a limit
 *
 */
abstract class PaiementQuery extends ModelCriteria
{

    // archivable behavior
    protected $archiveOnDelete = true;
protected $entityNotFoundExceptionClass = '\\Propel\\Runtime\\Exception\\EntityNotFoundException';

    /**
     * Initializes internal state of \Base\PaiementQuery object.
     *
     * @param     string $dbName The database name
     * @param     string $modelName The phpName of a model, e.g. 'Book'
     * @param     string $modelAlias The alias for the model in this query, e.g. 'b'
     */
    public function __construct($dbName = 'simplasso', $modelName = '\\Paiement', $modelAlias = null)
    {
        parent::__construct($dbName, $modelName, $modelAlias);
    }

    /**
     * Returns a new ChildPaiementQuery object.
     *
     * @param     string $modelAlias The alias of a model in the query
     * @param     Criteria $criteria Optional Criteria to build the query from
     *
     * @return ChildPaiementQuery
     */
    public static function create($modelAlias = null, Criteria $criteria = null)
    {
        if ($criteria instanceof ChildPaiementQuery) {
            return $criteria;
        }
        $query = new ChildPaiementQuery();
        if (null !== $modelAlias) {
            $query->setModelAlias($modelAlias);
        }
        if ($criteria instanceof Criteria) {
            $query->mergeWith($criteria);
        }

        return $query;
    }

    /**
     * Find object by primary key.
     * Propel uses the instance pool to skip the database if the object exists.
     * Go fast if the query is untouched.
     *
     * <code>
     * $obj  = $c->findPk(12, $con);
     * </code>
     *
     * @param mixed $key Primary key to use for the query
     * @param ConnectionInterface $con an optional connection object
     *
     * @return ChildPaiement|array|mixed the result, formatted by the current formatter
     */
    public function findPk($key, ConnectionInterface $con = null)
    {
        if ($key === null) {
            return null;
        }

        if ($con === null) {
            $con = Propel::getServiceContainer()->getReadConnection(PaiementTableMap::DATABASE_NAME);
        }

        $this->basePreSelect($con);

        if (
            $this->formatter || $this->modelAlias || $this->with || $this->select
            || $this->selectColumns || $this->asColumns || $this->selectModifiers
            || $this->map || $this->having || $this->joins
        ) {
            return $this->findPkComplex($key, $con);
        }

        if ((null !== ($obj = PaiementTableMap::getInstanceFromPool(null === $key || is_scalar($key) || is_callable([$key, '__toString']) ? (string) $key : $key)))) {
            // the object is already in the instance pool
            return $obj;
        }

        return $this->findPkSimple($key, $con);
    }

    /**
     * Find object by primary key using raw SQL to go fast.
     * Bypass doSelect() and the object formatter by using generated code.
     *
     * @param     mixed $key Primary key to use for the query
     * @param     ConnectionInterface $con A connection object
     *
     * @throws \Propel\Runtime\Exception\PropelException
     *
     * @return ChildPaiement A model object, or null if the key is not found
     */
    protected function findPkSimple($key, ConnectionInterface $con)
    {
        $sql = 'SELECT `id_paiement`, `objet`, `id_objet`, `id_entite`, `id_tresor`, `montant`, `numero`, `date_cheque`, `date_enregistrement`, `remise`, `comptabilise`, `observation`, `created_at`, `updated_at` FROM `asso_paiements` WHERE `id_paiement` = :p0';
        try {
            $stmt = $con->prepare($sql);
            $stmt->bindValue(':p0', $key, PDO::PARAM_INT);
            $stmt->execute();
        } catch (Exception $e) {
            Propel::log($e->getMessage(), Propel::LOG_ERR);
            throw new PropelException(sprintf('Unable to execute SELECT statement [%s]', $sql), 0, $e);
        }
        $obj = null;
        if ($row = $stmt->fetch(\PDO::FETCH_NUM)) {
            /** @var ChildPaiement $obj */
            $obj = new ChildPaiement();
            $obj->hydrate($row);
            PaiementTableMap::addInstanceToPool($obj, null === $key || is_scalar($key) || is_callable([$key, '__toString']) ? (string) $key : $key);
        }
        $stmt->closeCursor();

        return $obj;
    }

    /**
     * Find object by primary key.
     *
     * @param     mixed $key Primary key to use for the query
     * @param     ConnectionInterface $con A connection object
     *
     * @return ChildPaiement|array|mixed the result, formatted by the current formatter
     */
    protected function findPkComplex($key, ConnectionInterface $con)
    {
        // As the query uses a PK condition, no limit(1) is necessary.
        $criteria = $this->isKeepQuery() ? clone $this : $this;
        $dataFetcher = $criteria
            ->filterByPrimaryKey($key)
            ->doSelect($con);

        return $criteria->getFormatter()->init($criteria)->formatOne($dataFetcher);
    }

    /**
     * Find objects by primary key
     * <code>
     * $objs = $c->findPks(array(12, 56, 832), $con);
     * </code>
     * @param     array $keys Primary keys to use for the query
     * @param     ConnectionInterface $con an optional connection object
     *
     * @return ObjectCollection|array|mixed the list of results, formatted by the current formatter
     */
    public function findPks($keys, ConnectionInterface $con = null)
    {
        if (null === $con) {
            $con = Propel::getServiceContainer()->getReadConnection($this->getDbName());
        }
        $this->basePreSelect($con);
        $criteria = $this->isKeepQuery() ? clone $this : $this;
        $dataFetcher = $criteria
            ->filterByPrimaryKeys($keys)
            ->doSelect($con);

        return $criteria->getFormatter()->init($criteria)->format($dataFetcher);
    }

    /**
     * Filter the query by primary key
     *
     * @param     mixed $key Primary key to use for the query
     *
     * @return $this|ChildPaiementQuery The current query, for fluid interface
     */
    public function filterByPrimaryKey($key)
    {

        return $this->addUsingAlias(PaiementTableMap::COL_ID_PAIEMENT, $key, Criteria::EQUAL);
    }

    /**
     * Filter the query by a list of primary keys
     *
     * @param     array $keys The list of primary key to use for the query
     *
     * @return $this|ChildPaiementQuery The current query, for fluid interface
     */
    public function filterByPrimaryKeys($keys)
    {

        return $this->addUsingAlias(PaiementTableMap::COL_ID_PAIEMENT, $keys, Criteria::IN);
    }

    /**
     * Filter the query on the id_paiement column
     *
     * Example usage:
     * <code>
     * $query->filterByIdPaiement(1234); // WHERE id_paiement = 1234
     * $query->filterByIdPaiement(array(12, 34)); // WHERE id_paiement IN (12, 34)
     * $query->filterByIdPaiement(array('min' => 12)); // WHERE id_paiement > 12
     * </code>
     *
     * @param     mixed $idPaiement The value to use as filter.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildPaiementQuery The current query, for fluid interface
     */
    public function filterByIdPaiement($idPaiement = null, $comparison = null)
    {
        if (is_array($idPaiement)) {
            $useMinMax = false;
            if (isset($idPaiement['min'])) {
                $this->addUsingAlias(PaiementTableMap::COL_ID_PAIEMENT, $idPaiement['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($idPaiement['max'])) {
                $this->addUsingAlias(PaiementTableMap::COL_ID_PAIEMENT, $idPaiement['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(PaiementTableMap::COL_ID_PAIEMENT, $idPaiement, $comparison);
    }

    /**
     * Filter the query on the objet column
     *
     * Example usage:
     * <code>
     * $query->filterByObjet('fooValue');   // WHERE objet = 'fooValue'
     * $query->filterByObjet('%fooValue%', Criteria::LIKE); // WHERE objet LIKE '%fooValue%'
     * </code>
     *
     * @param     string $objet The value to use as filter.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildPaiementQuery The current query, for fluid interface
     */
    public function filterByObjet($objet = null, $comparison = null)
    {
        if (null === $comparison) {
            if (is_array($objet)) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(PaiementTableMap::COL_OBJET, $objet, $comparison);
    }

    /**
     * Filter the query on the id_objet column
     *
     * Example usage:
     * <code>
     * $query->filterByIdObjet(1234); // WHERE id_objet = 1234
     * $query->filterByIdObjet(array(12, 34)); // WHERE id_objet IN (12, 34)
     * $query->filterByIdObjet(array('min' => 12)); // WHERE id_objet > 12
     * </code>
     *
     * @param     mixed $idObjet The value to use as filter.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildPaiementQuery The current query, for fluid interface
     */
    public function filterByIdObjet($idObjet = null, $comparison = null)
    {
        if (is_array($idObjet)) {
            $useMinMax = false;
            if (isset($idObjet['min'])) {
                $this->addUsingAlias(PaiementTableMap::COL_ID_OBJET, $idObjet['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($idObjet['max'])) {
                $this->addUsingAlias(PaiementTableMap::COL_ID_OBJET, $idObjet['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(PaiementTableMap::COL_ID_OBJET, $idObjet, $comparison);
    }

    /**
     * Filter the query on the id_entite column
     *
     * Example usage:
     * <code>
     * $query->filterByIdEntite(1234); // WHERE id_entite = 1234
     * $query->filterByIdEntite(array(12, 34)); // WHERE id_entite IN (12, 34)
     * $query->filterByIdEntite(array('min' => 12)); // WHERE id_entite > 12
     * </code>
     *
     * @see       filterByEntite()
     *
     * @param     mixed $idEntite The value to use as filter.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildPaiementQuery The current query, for fluid interface
     */
    public function filterByIdEntite($idEntite = null, $comparison = null)
    {
        if (is_array($idEntite)) {
            $useMinMax = false;
            if (isset($idEntite['min'])) {
                $this->addUsingAlias(PaiementTableMap::COL_ID_ENTITE, $idEntite['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($idEntite['max'])) {
                $this->addUsingAlias(PaiementTableMap::COL_ID_ENTITE, $idEntite['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(PaiementTableMap::COL_ID_ENTITE, $idEntite, $comparison);
    }

    /**
     * Filter the query on the id_tresor column
     *
     * Example usage:
     * <code>
     * $query->filterByIdTresor(1234); // WHERE id_tresor = 1234
     * $query->filterByIdTresor(array(12, 34)); // WHERE id_tresor IN (12, 34)
     * $query->filterByIdTresor(array('min' => 12)); // WHERE id_tresor > 12
     * </code>
     *
     * @see       filterByTresor()
     *
     * @param     mixed $idTresor The value to use as filter.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildPaiementQuery The current query, for fluid interface
     */
    public function filterByIdTresor($idTresor = null, $comparison = null)
    {
        if (is_array($idTresor)) {
            $useMinMax = false;
            if (isset($idTresor['min'])) {
                $this->addUsingAlias(PaiementTableMap::COL_ID_TRESOR, $idTresor['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($idTresor['max'])) {
                $this->addUsingAlias(PaiementTableMap::COL_ID_TRESOR, $idTresor['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(PaiementTableMap::COL_ID_TRESOR, $idTresor, $comparison);
    }

    /**
     * Filter the query on the montant column
     *
     * Example usage:
     * <code>
     * $query->filterByMontant(1234); // WHERE montant = 1234
     * $query->filterByMontant(array(12, 34)); // WHERE montant IN (12, 34)
     * $query->filterByMontant(array('min' => 12)); // WHERE montant > 12
     * </code>
     *
     * @param     mixed $montant The value to use as filter.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildPaiementQuery The current query, for fluid interface
     */
    public function filterByMontant($montant = null, $comparison = null)
    {
        if (is_array($montant)) {
            $useMinMax = false;
            if (isset($montant['min'])) {
                $this->addUsingAlias(PaiementTableMap::COL_MONTANT, $montant['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($montant['max'])) {
                $this->addUsingAlias(PaiementTableMap::COL_MONTANT, $montant['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(PaiementTableMap::COL_MONTANT, $montant, $comparison);
    }

    /**
     * Filter the query on the numero column
     *
     * Example usage:
     * <code>
     * $query->filterByNumero('fooValue');   // WHERE numero = 'fooValue'
     * $query->filterByNumero('%fooValue%', Criteria::LIKE); // WHERE numero LIKE '%fooValue%'
     * </code>
     *
     * @param     string $numero The value to use as filter.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildPaiementQuery The current query, for fluid interface
     */
    public function filterByNumero($numero = null, $comparison = null)
    {
        if (null === $comparison) {
            if (is_array($numero)) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(PaiementTableMap::COL_NUMERO, $numero, $comparison);
    }

    /**
     * Filter the query on the date_cheque column
     *
     * Example usage:
     * <code>
     * $query->filterByDateCheque('2011-03-14'); // WHERE date_cheque = '2011-03-14'
     * $query->filterByDateCheque('now'); // WHERE date_cheque = '2011-03-14'
     * $query->filterByDateCheque(array('max' => 'yesterday')); // WHERE date_cheque > '2011-03-13'
     * </code>
     *
     * @param     mixed $dateCheque The value to use as filter.
     *              Values can be integers (unix timestamps), DateTime objects, or strings.
     *              Empty strings are treated as NULL.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildPaiementQuery The current query, for fluid interface
     */
    public function filterByDateCheque($dateCheque = null, $comparison = null)
    {
        if (is_array($dateCheque)) {
            $useMinMax = false;
            if (isset($dateCheque['min'])) {
                $this->addUsingAlias(PaiementTableMap::COL_DATE_CHEQUE, $dateCheque['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($dateCheque['max'])) {
                $this->addUsingAlias(PaiementTableMap::COL_DATE_CHEQUE, $dateCheque['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(PaiementTableMap::COL_DATE_CHEQUE, $dateCheque, $comparison);
    }

    /**
     * Filter the query on the date_enregistrement column
     *
     * Example usage:
     * <code>
     * $query->filterByDateEnregistrement('2011-03-14'); // WHERE date_enregistrement = '2011-03-14'
     * $query->filterByDateEnregistrement('now'); // WHERE date_enregistrement = '2011-03-14'
     * $query->filterByDateEnregistrement(array('max' => 'yesterday')); // WHERE date_enregistrement > '2011-03-13'
     * </code>
     *
     * @param     mixed $dateEnregistrement The value to use as filter.
     *              Values can be integers (unix timestamps), DateTime objects, or strings.
     *              Empty strings are treated as NULL.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildPaiementQuery The current query, for fluid interface
     */
    public function filterByDateEnregistrement($dateEnregistrement = null, $comparison = null)
    {
        if (is_array($dateEnregistrement)) {
            $useMinMax = false;
            if (isset($dateEnregistrement['min'])) {
                $this->addUsingAlias(PaiementTableMap::COL_DATE_ENREGISTREMENT, $dateEnregistrement['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($dateEnregistrement['max'])) {
                $this->addUsingAlias(PaiementTableMap::COL_DATE_ENREGISTREMENT, $dateEnregistrement['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(PaiementTableMap::COL_DATE_ENREGISTREMENT, $dateEnregistrement, $comparison);
    }

    /**
     * Filter the query on the remise column
     *
     * Example usage:
     * <code>
     * $query->filterByRemise(1234); // WHERE remise = 1234
     * $query->filterByRemise(array(12, 34)); // WHERE remise IN (12, 34)
     * $query->filterByRemise(array('min' => 12)); // WHERE remise > 12
     * </code>
     *
     * @param     mixed $remise The value to use as filter.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildPaiementQuery The current query, for fluid interface
     */
    public function filterByRemise($remise = null, $comparison = null)
    {
        if (is_array($remise)) {
            $useMinMax = false;
            if (isset($remise['min'])) {
                $this->addUsingAlias(PaiementTableMap::COL_REMISE, $remise['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($remise['max'])) {
                $this->addUsingAlias(PaiementTableMap::COL_REMISE, $remise['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(PaiementTableMap::COL_REMISE, $remise, $comparison);
    }

    /**
     * Filter the query on the comptabilise column
     *
     * Example usage:
     * <code>
     * $query->filterByComptabilise(true); // WHERE comptabilise = true
     * $query->filterByComptabilise('yes'); // WHERE comptabilise = true
     * </code>
     *
     * @param     boolean|string $comptabilise The value to use as filter.
     *              Non-boolean arguments are converted using the following rules:
     *                * 1, '1', 'true',  'on',  and 'yes' are converted to boolean true
     *                * 0, '0', 'false', 'off', and 'no'  are converted to boolean false
     *              Check on string values is case insensitive (so 'FaLsE' is seen as 'false').
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildPaiementQuery The current query, for fluid interface
     */
    public function filterByComptabilise($comptabilise = null, $comparison = null)
    {
        if (is_string($comptabilise)) {
            $comptabilise = in_array(strtolower($comptabilise), array('false', 'off', '-', 'no', 'n', '0', '')) ? false : true;
        }

        return $this->addUsingAlias(PaiementTableMap::COL_COMPTABILISE, $comptabilise, $comparison);
    }

    /**
     * Filter the query on the observation column
     *
     * Example usage:
     * <code>
     * $query->filterByObservation('fooValue');   // WHERE observation = 'fooValue'
     * $query->filterByObservation('%fooValue%', Criteria::LIKE); // WHERE observation LIKE '%fooValue%'
     * </code>
     *
     * @param     string $observation The value to use as filter.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildPaiementQuery The current query, for fluid interface
     */
    public function filterByObservation($observation = null, $comparison = null)
    {
        if (null === $comparison) {
            if (is_array($observation)) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(PaiementTableMap::COL_OBSERVATION, $observation, $comparison);
    }

    /**
     * Filter the query on the created_at column
     *
     * Example usage:
     * <code>
     * $query->filterByCreatedAt('2011-03-14'); // WHERE created_at = '2011-03-14'
     * $query->filterByCreatedAt('now'); // WHERE created_at = '2011-03-14'
     * $query->filterByCreatedAt(array('max' => 'yesterday')); // WHERE created_at > '2011-03-13'
     * </code>
     *
     * @param     mixed $createdAt The value to use as filter.
     *              Values can be integers (unix timestamps), DateTime objects, or strings.
     *              Empty strings are treated as NULL.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildPaiementQuery The current query, for fluid interface
     */
    public function filterByCreatedAt($createdAt = null, $comparison = null)
    {
        if (is_array($createdAt)) {
            $useMinMax = false;
            if (isset($createdAt['min'])) {
                $this->addUsingAlias(PaiementTableMap::COL_CREATED_AT, $createdAt['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($createdAt['max'])) {
                $this->addUsingAlias(PaiementTableMap::COL_CREATED_AT, $createdAt['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(PaiementTableMap::COL_CREATED_AT, $createdAt, $comparison);
    }

    /**
     * Filter the query on the updated_at column
     *
     * Example usage:
     * <code>
     * $query->filterByUpdatedAt('2011-03-14'); // WHERE updated_at = '2011-03-14'
     * $query->filterByUpdatedAt('now'); // WHERE updated_at = '2011-03-14'
     * $query->filterByUpdatedAt(array('max' => 'yesterday')); // WHERE updated_at > '2011-03-13'
     * </code>
     *
     * @param     mixed $updatedAt The value to use as filter.
     *              Values can be integers (unix timestamps), DateTime objects, or strings.
     *              Empty strings are treated as NULL.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildPaiementQuery The current query, for fluid interface
     */
    public function filterByUpdatedAt($updatedAt = null, $comparison = null)
    {
        if (is_array($updatedAt)) {
            $useMinMax = false;
            if (isset($updatedAt['min'])) {
                $this->addUsingAlias(PaiementTableMap::COL_UPDATED_AT, $updatedAt['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($updatedAt['max'])) {
                $this->addUsingAlias(PaiementTableMap::COL_UPDATED_AT, $updatedAt['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(PaiementTableMap::COL_UPDATED_AT, $updatedAt, $comparison);
    }

    /**
     * Filter the query by a related \Entite object
     *
     * @param \Entite|ObjectCollection $entite The related object(s) to use as filter
     * @param string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @throws \Propel\Runtime\Exception\PropelException
     *
     * @return ChildPaiementQuery The current query, for fluid interface
     */
    public function filterByEntite($entite, $comparison = null)
    {
        if ($entite instanceof \Entite) {
            return $this
                ->addUsingAlias(PaiementTableMap::COL_ID_ENTITE, $entite->getIdEntite(), $comparison);
        } elseif ($entite instanceof ObjectCollection) {
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }

            return $this
                ->addUsingAlias(PaiementTableMap::COL_ID_ENTITE, $entite->toKeyValue('PrimaryKey', 'IdEntite'), $comparison);
        } else {
            throw new PropelException('filterByEntite() only accepts arguments of type \Entite or Collection');
        }
    }

    /**
     * Adds a JOIN clause to the query using the Entite relation
     *
     * @param     string $relationAlias optional alias for the relation
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return $this|ChildPaiementQuery The current query, for fluid interface
     */
    public function joinEntite($relationAlias = null, $joinType = Criteria::INNER_JOIN)
    {
        $tableMap = $this->getTableMap();
        $relationMap = $tableMap->getRelation('Entite');

        // create a ModelJoin object for this join
        $join = new ModelJoin();
        $join->setJoinType($joinType);
        $join->setRelationMap($relationMap, $this->useAliasInSQL ? $this->getModelAlias() : null, $relationAlias);
        if ($previousJoin = $this->getPreviousJoin()) {
            $join->setPreviousJoin($previousJoin);
        }

        // add the ModelJoin to the current object
        if ($relationAlias) {
            $this->addAlias($relationAlias, $relationMap->getRightTable()->getName());
            $this->addJoinObject($join, $relationAlias);
        } else {
            $this->addJoinObject($join, 'Entite');
        }

        return $this;
    }

    /**
     * Use the Entite relation Entite object
     *
     * @see useQuery()
     *
     * @param     string $relationAlias optional alias for the relation,
     *                                   to be used as main alias in the secondary query
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return \EntiteQuery A secondary query class using the current class as primary query
     */
    public function useEntiteQuery($relationAlias = null, $joinType = Criteria::INNER_JOIN)
    {
        return $this
            ->joinEntite($relationAlias, $joinType)
            ->useQuery($relationAlias ? $relationAlias : 'Entite', '\EntiteQuery');
    }

    /**
     * Filter the query by a related \Tresor object
     *
     * @param \Tresor|ObjectCollection $tresor The related object(s) to use as filter
     * @param string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @throws \Propel\Runtime\Exception\PropelException
     *
     * @return ChildPaiementQuery The current query, for fluid interface
     */
    public function filterByTresor($tresor, $comparison = null)
    {
        if ($tresor instanceof \Tresor) {
            return $this
                ->addUsingAlias(PaiementTableMap::COL_ID_TRESOR, $tresor->getIdTresor(), $comparison);
        } elseif ($tresor instanceof ObjectCollection) {
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }

            return $this
                ->addUsingAlias(PaiementTableMap::COL_ID_TRESOR, $tresor->toKeyValue('PrimaryKey', 'IdTresor'), $comparison);
        } else {
            throw new PropelException('filterByTresor() only accepts arguments of type \Tresor or Collection');
        }
    }

    /**
     * Adds a JOIN clause to the query using the Tresor relation
     *
     * @param     string $relationAlias optional alias for the relation
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return $this|ChildPaiementQuery The current query, for fluid interface
     */
    public function joinTresor($relationAlias = null, $joinType = Criteria::INNER_JOIN)
    {
        $tableMap = $this->getTableMap();
        $relationMap = $tableMap->getRelation('Tresor');

        // create a ModelJoin object for this join
        $join = new ModelJoin();
        $join->setJoinType($joinType);
        $join->setRelationMap($relationMap, $this->useAliasInSQL ? $this->getModelAlias() : null, $relationAlias);
        if ($previousJoin = $this->getPreviousJoin()) {
            $join->setPreviousJoin($previousJoin);
        }

        // add the ModelJoin to the current object
        if ($relationAlias) {
            $this->addAlias($relationAlias, $relationMap->getRightTable()->getName());
            $this->addJoinObject($join, $relationAlias);
        } else {
            $this->addJoinObject($join, 'Tresor');
        }

        return $this;
    }

    /**
     * Use the Tresor relation Tresor object
     *
     * @see useQuery()
     *
     * @param     string $relationAlias optional alias for the relation,
     *                                   to be used as main alias in the secondary query
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return \TresorQuery A secondary query class using the current class as primary query
     */
    public function useTresorQuery($relationAlias = null, $joinType = Criteria::INNER_JOIN)
    {
        return $this
            ->joinTresor($relationAlias, $joinType)
            ->useQuery($relationAlias ? $relationAlias : 'Tresor', '\TresorQuery');
    }

    /**
     * Filter the query by a related \Servicepaiement object
     *
     * @param \Servicepaiement|ObjectCollection $servicepaiement the related object to use as filter
     * @param string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return ChildPaiementQuery The current query, for fluid interface
     */
    public function filterByServicepaiement($servicepaiement, $comparison = null)
    {
        if ($servicepaiement instanceof \Servicepaiement) {
            return $this
                ->addUsingAlias(PaiementTableMap::COL_ID_PAIEMENT, $servicepaiement->getIdPaiement(), $comparison);
        } elseif ($servicepaiement instanceof ObjectCollection) {
            return $this
                ->useServicepaiementQuery()
                ->filterByPrimaryKeys($servicepaiement->getPrimaryKeys())
                ->endUse();
        } else {
            throw new PropelException('filterByServicepaiement() only accepts arguments of type \Servicepaiement or Collection');
        }
    }

    /**
     * Adds a JOIN clause to the query using the Servicepaiement relation
     *
     * @param     string $relationAlias optional alias for the relation
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return $this|ChildPaiementQuery The current query, for fluid interface
     */
    public function joinServicepaiement($relationAlias = null, $joinType = Criteria::INNER_JOIN)
    {
        $tableMap = $this->getTableMap();
        $relationMap = $tableMap->getRelation('Servicepaiement');

        // create a ModelJoin object for this join
        $join = new ModelJoin();
        $join->setJoinType($joinType);
        $join->setRelationMap($relationMap, $this->useAliasInSQL ? $this->getModelAlias() : null, $relationAlias);
        if ($previousJoin = $this->getPreviousJoin()) {
            $join->setPreviousJoin($previousJoin);
        }

        // add the ModelJoin to the current object
        if ($relationAlias) {
            $this->addAlias($relationAlias, $relationMap->getRightTable()->getName());
            $this->addJoinObject($join, $relationAlias);
        } else {
            $this->addJoinObject($join, 'Servicepaiement');
        }

        return $this;
    }

    /**
     * Use the Servicepaiement relation Servicepaiement object
     *
     * @see useQuery()
     *
     * @param     string $relationAlias optional alias for the relation,
     *                                   to be used as main alias in the secondary query
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return \ServicepaiementQuery A secondary query class using the current class as primary query
     */
    public function useServicepaiementQuery($relationAlias = null, $joinType = Criteria::INNER_JOIN)
    {
        return $this
            ->joinServicepaiement($relationAlias, $joinType)
            ->useQuery($relationAlias ? $relationAlias : 'Servicepaiement', '\ServicepaiementQuery');
    }

    /**
     * Exclude object from result
     *
     * @param   ChildPaiement $paiement Object to remove from the list of results
     *
     * @return $this|ChildPaiementQuery The current query, for fluid interface
     */
    public function prune($paiement = null)
    {
        if ($paiement) {
            $this->addUsingAlias(PaiementTableMap::COL_ID_PAIEMENT, $paiement->getIdPaiement(), Criteria::NOT_EQUAL);
        }

        return $this;
    }

    /**
     * Code to execute before every DELETE statement
     *
     * @param     ConnectionInterface $con The connection object used by the query
     */
    protected function basePreDelete(ConnectionInterface $con)
    {
        // archivable behavior

        if ($this->archiveOnDelete) {
            $this->archive($con);
        } else {
            $this->archiveOnDelete = true;
        }


        return $this->preDelete($con);
    }

    /**
     * Deletes all rows from the asso_paiements table.
     *
     * @param ConnectionInterface $con the connection to use
     * @return int The number of affected rows (if supported by underlying database driver).
     */
    public function doDeleteAll(ConnectionInterface $con = null)
    {
        if (null === $con) {
            $con = Propel::getServiceContainer()->getWriteConnection(PaiementTableMap::DATABASE_NAME);
        }

        // use transaction because $criteria could contain info
        // for more than one table or we could emulating ON DELETE CASCADE, etc.
        return $con->transaction(function () use ($con) {
            $affectedRows = 0; // initialize var to track total num of affected rows
            $affectedRows += parent::doDeleteAll($con);
            // Because this db requires some delete cascade/set null emulation, we have to
            // clear the cached instance *after* the emulation has happened (since
            // instances get re-added by the select statement contained therein).
            PaiementTableMap::clearInstancePool();
            PaiementTableMap::clearRelatedInstancePool();

            return $affectedRows;
        });
    }

    /**
     * Performs a DELETE on the database based on the current ModelCriteria
     *
     * @param ConnectionInterface $con the connection to use
     * @return int             The number of affected rows (if supported by underlying database driver).  This includes CASCADE-related rows
     *                         if supported by native driver or if emulated using Propel.
     * @throws PropelException Any exceptions caught during processing will be
     *                         rethrown wrapped into a PropelException.
     */
    public function delete(ConnectionInterface $con = null)
    {
        if (null === $con) {
            $con = Propel::getServiceContainer()->getWriteConnection(PaiementTableMap::DATABASE_NAME);
        }

        $criteria = $this;

        // Set the correct dbName
        $criteria->setDbName(PaiementTableMap::DATABASE_NAME);

        // use transaction because $criteria could contain info
        // for more than one table or we could emulating ON DELETE CASCADE, etc.
        return $con->transaction(function () use ($con, $criteria) {
            $affectedRows = 0; // initialize var to track total num of affected rows

            PaiementTableMap::removeInstanceFromPool($criteria);

            $affectedRows += ModelCriteria::delete($con);
            PaiementTableMap::clearRelatedInstancePool();

            return $affectedRows;
        });
    }

    // timestampable behavior

    /**
     * Filter by the latest updated
     *
     * @param      int $nbDays Maximum age of the latest update in days
     *
     * @return     $this|ChildPaiementQuery The current query, for fluid interface
     */
    public function recentlyUpdated($nbDays = 7)
    {
        return $this->addUsingAlias(PaiementTableMap::COL_UPDATED_AT, time() - $nbDays * 24 * 60 * 60, Criteria::GREATER_EQUAL);
    }

    /**
     * Order by update date desc
     *
     * @return     $this|ChildPaiementQuery The current query, for fluid interface
     */
    public function lastUpdatedFirst()
    {
        return $this->addDescendingOrderByColumn(PaiementTableMap::COL_UPDATED_AT);
    }

    /**
     * Order by update date asc
     *
     * @return     $this|ChildPaiementQuery The current query, for fluid interface
     */
    public function firstUpdatedFirst()
    {
        return $this->addAscendingOrderByColumn(PaiementTableMap::COL_UPDATED_AT);
    }

    /**
     * Order by create date desc
     *
     * @return     $this|ChildPaiementQuery The current query, for fluid interface
     */
    public function lastCreatedFirst()
    {
        return $this->addDescendingOrderByColumn(PaiementTableMap::COL_CREATED_AT);
    }

    /**
     * Filter by the latest created
     *
     * @param      int $nbDays Maximum age of in days
     *
     * @return     $this|ChildPaiementQuery The current query, for fluid interface
     */
    public function recentlyCreated($nbDays = 7)
    {
        return $this->addUsingAlias(PaiementTableMap::COL_CREATED_AT, time() - $nbDays * 24 * 60 * 60, Criteria::GREATER_EQUAL);
    }

    /**
     * Order by create date asc
     *
     * @return     $this|ChildPaiementQuery The current query, for fluid interface
     */
    public function firstCreatedFirst()
    {
        return $this->addAscendingOrderByColumn(PaiementTableMap::COL_CREATED_AT);
    }

    // archivable behavior

    /**
     * Copy the data of the objects satisfying the query into ChildAssoPaiementsArchive archive objects.
     * The archived objects are then saved.
     * If any of the objects has already been archived, the archived object
     * is updated and not duplicated.
     * Warning: This termination methods issues 2n+1 queries.
     *
     * @param      ConnectionInterface $con    Connection to use.
     * @param      Boolean $useLittleMemory    Whether or not to use OnDemandFormatter to retrieve objects.
     *               Set to false if the identity map matters.
     *               Set to true (default) to use less memory.
     *
     * @return     int the number of archived objects
     */
    public function archive($con = null, $useLittleMemory = true)
    {
        $criteria = clone $this;
        // prepare the query
        $criteria->setWith(array());
        if ($useLittleMemory) {
            $criteria->setFormatter(ModelCriteria::FORMAT_ON_DEMAND);
        }
        if ($con === null) {
            $con = Propel::getServiceContainer()->getWriteConnection(PaiementTableMap::DATABASE_NAME);
        }

        return $con->transaction(function () use ($con, $criteria) {
            $totalArchivedObjects = 0;

            // archive all results one by one
            foreach ($criteria->find($con) as $object) {
                $object->archive($con);
                $totalArchivedObjects++;
            }

            return $totalArchivedObjects;
        });
    }

    /**
     * Enable/disable auto-archiving on delete for the next query.
     *
     * @param boolean True if the query must archive deleted objects, false otherwise.
     */
    public function setArchiveOnDelete($archiveOnDelete)
    {
        $this->archiveOnDelete = $archiveOnDelete;
    }

    /**
     * Delete records matching the current query without archiving them.
     *
     * @param      ConnectionInterface $con    Connection to use.
     *
     * @return integer the number of deleted rows
     */
    public function deleteWithoutArchive($con = null)
    {
        $this->archiveOnDelete = false;

        return $this->delete($con);
    }

    /**
     * Delete all records without archiving them.
     *
     * @param      ConnectionInterface $con    Connection to use.
     *
     * @return integer the number of deleted rows
     */
    public function deleteAllWithoutArchive($con = null)
    {
        $this->archiveOnDelete = false;

        return $this->deleteAll($con);
    }

} // PaiementQuery
