<?php

namespace Base;

use \Infolettre as ChildInfolettre;
use \InfolettreQuery as ChildInfolettreQuery;
use \Exception;
use \PDO;
use Map\InfolettreTableMap;
use Propel\Runtime\Propel;
use Propel\Runtime\ActiveQuery\Criteria;
use Propel\Runtime\ActiveQuery\ModelCriteria;
use Propel\Runtime\ActiveQuery\ModelJoin;
use Propel\Runtime\Collection\ObjectCollection;
use Propel\Runtime\Connection\ConnectionInterface;
use Propel\Runtime\Exception\PropelException;

/**
 * Base class that represents a query for the 'com_infolettres' table.
 *
 *
 *
 * @method     ChildInfolettreQuery orderByIdInfolettre($order = Criteria::ASC) Order by the id_infolettre column
 * @method     ChildInfolettreQuery orderByIdentifiant($order = Criteria::ASC) Order by the identifiant column
 * @method     ChildInfolettreQuery orderByNom($order = Criteria::ASC) Order by the nom column
 * @method     ChildInfolettreQuery orderByDescriptif($order = Criteria::ASC) Order by the descriptif column
 * @method     ChildInfolettreQuery orderByAutomatique($order = Criteria::ASC) Order by the automatique column
 * @method     ChildInfolettreQuery orderByDateSynchro($order = Criteria::ASC) Order by the date_synchro column
 * @method     ChildInfolettreQuery orderByActive($order = Criteria::ASC) Order by the active column
 * @method     ChildInfolettreQuery orderByLiaisonAuto($order = Criteria::ASC) Order by the liaison_auto column
 * @method     ChildInfolettreQuery orderByOptions($order = Criteria::ASC) Order by the options column
 * @method     ChildInfolettreQuery orderByPosition($order = Criteria::ASC) Order by the position column
 * @method     ChildInfolettreQuery orderByCreatedAt($order = Criteria::ASC) Order by the created_at column
 * @method     ChildInfolettreQuery orderByUpdatedAt($order = Criteria::ASC) Order by the updated_at column
 *
 * @method     ChildInfolettreQuery groupByIdInfolettre() Group by the id_infolettre column
 * @method     ChildInfolettreQuery groupByIdentifiant() Group by the identifiant column
 * @method     ChildInfolettreQuery groupByNom() Group by the nom column
 * @method     ChildInfolettreQuery groupByDescriptif() Group by the descriptif column
 * @method     ChildInfolettreQuery groupByAutomatique() Group by the automatique column
 * @method     ChildInfolettreQuery groupByDateSynchro() Group by the date_synchro column
 * @method     ChildInfolettreQuery groupByActive() Group by the active column
 * @method     ChildInfolettreQuery groupByLiaisonAuto() Group by the liaison_auto column
 * @method     ChildInfolettreQuery groupByOptions() Group by the options column
 * @method     ChildInfolettreQuery groupByPosition() Group by the position column
 * @method     ChildInfolettreQuery groupByCreatedAt() Group by the created_at column
 * @method     ChildInfolettreQuery groupByUpdatedAt() Group by the updated_at column
 *
 * @method     ChildInfolettreQuery leftJoin($relation) Adds a LEFT JOIN clause to the query
 * @method     ChildInfolettreQuery rightJoin($relation) Adds a RIGHT JOIN clause to the query
 * @method     ChildInfolettreQuery innerJoin($relation) Adds a INNER JOIN clause to the query
 *
 * @method     ChildInfolettreQuery leftJoinWith($relation) Adds a LEFT JOIN clause and with to the query
 * @method     ChildInfolettreQuery rightJoinWith($relation) Adds a RIGHT JOIN clause and with to the query
 * @method     ChildInfolettreQuery innerJoinWith($relation) Adds a INNER JOIN clause and with to the query
 *
 * @method     ChildInfolettreQuery leftJoinInfolettreInscrit($relationAlias = null) Adds a LEFT JOIN clause to the query using the InfolettreInscrit relation
 * @method     ChildInfolettreQuery rightJoinInfolettreInscrit($relationAlias = null) Adds a RIGHT JOIN clause to the query using the InfolettreInscrit relation
 * @method     ChildInfolettreQuery innerJoinInfolettreInscrit($relationAlias = null) Adds a INNER JOIN clause to the query using the InfolettreInscrit relation
 *
 * @method     ChildInfolettreQuery joinWithInfolettreInscrit($joinType = Criteria::INNER_JOIN) Adds a join clause and with to the query using the InfolettreInscrit relation
 *
 * @method     ChildInfolettreQuery leftJoinWithInfolettreInscrit() Adds a LEFT JOIN clause and with to the query using the InfolettreInscrit relation
 * @method     ChildInfolettreQuery rightJoinWithInfolettreInscrit() Adds a RIGHT JOIN clause and with to the query using the InfolettreInscrit relation
 * @method     ChildInfolettreQuery innerJoinWithInfolettreInscrit() Adds a INNER JOIN clause and with to the query using the InfolettreInscrit relation
 *
 * @method     \InfolettreInscritQuery endUse() Finalizes a secondary criteria and merges it with its primary Criteria
 *
 * @method     ChildInfolettre findOne(ConnectionInterface $con = null) Return the first ChildInfolettre matching the query
 * @method     ChildInfolettre findOneOrCreate(ConnectionInterface $con = null) Return the first ChildInfolettre matching the query, or a new ChildInfolettre object populated from the query conditions when no match is found
 *
 * @method     ChildInfolettre findOneByIdInfolettre(string $id_infolettre) Return the first ChildInfolettre filtered by the id_infolettre column
 * @method     ChildInfolettre findOneByIdentifiant(string $identifiant) Return the first ChildInfolettre filtered by the identifiant column
 * @method     ChildInfolettre findOneByNom(string $nom) Return the first ChildInfolettre filtered by the nom column
 * @method     ChildInfolettre findOneByDescriptif(string $descriptif) Return the first ChildInfolettre filtered by the descriptif column
 * @method     ChildInfolettre findOneByAutomatique(boolean $automatique) Return the first ChildInfolettre filtered by the automatique column
 * @method     ChildInfolettre findOneByDateSynchro(string $date_synchro) Return the first ChildInfolettre filtered by the date_synchro column
 * @method     ChildInfolettre findOneByActive(boolean $active) Return the first ChildInfolettre filtered by the active column
 * @method     ChildInfolettre findOneByLiaisonAuto(boolean $liaison_auto) Return the first ChildInfolettre filtered by the liaison_auto column
 * @method     ChildInfolettre findOneByOptions(string $options) Return the first ChildInfolettre filtered by the options column
 * @method     ChildInfolettre findOneByPosition(int $position) Return the first ChildInfolettre filtered by the position column
 * @method     ChildInfolettre findOneByCreatedAt(string $created_at) Return the first ChildInfolettre filtered by the created_at column
 * @method     ChildInfolettre findOneByUpdatedAt(string $updated_at) Return the first ChildInfolettre filtered by the updated_at column *

 * @method     ChildInfolettre requirePk($key, ConnectionInterface $con = null) Return the ChildInfolettre by primary key and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildInfolettre requireOne(ConnectionInterface $con = null) Return the first ChildInfolettre matching the query and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 *
 * @method     ChildInfolettre requireOneByIdInfolettre(string $id_infolettre) Return the first ChildInfolettre filtered by the id_infolettre column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildInfolettre requireOneByIdentifiant(string $identifiant) Return the first ChildInfolettre filtered by the identifiant column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildInfolettre requireOneByNom(string $nom) Return the first ChildInfolettre filtered by the nom column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildInfolettre requireOneByDescriptif(string $descriptif) Return the first ChildInfolettre filtered by the descriptif column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildInfolettre requireOneByAutomatique(boolean $automatique) Return the first ChildInfolettre filtered by the automatique column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildInfolettre requireOneByDateSynchro(string $date_synchro) Return the first ChildInfolettre filtered by the date_synchro column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildInfolettre requireOneByActive(boolean $active) Return the first ChildInfolettre filtered by the active column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildInfolettre requireOneByLiaisonAuto(boolean $liaison_auto) Return the first ChildInfolettre filtered by the liaison_auto column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildInfolettre requireOneByOptions(string $options) Return the first ChildInfolettre filtered by the options column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildInfolettre requireOneByPosition(int $position) Return the first ChildInfolettre filtered by the position column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildInfolettre requireOneByCreatedAt(string $created_at) Return the first ChildInfolettre filtered by the created_at column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildInfolettre requireOneByUpdatedAt(string $updated_at) Return the first ChildInfolettre filtered by the updated_at column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 *
 * @method     ChildInfolettre[]|ObjectCollection find(ConnectionInterface $con = null) Return ChildInfolettre objects based on current ModelCriteria
 * @method     ChildInfolettre[]|ObjectCollection findByIdInfolettre(string $id_infolettre) Return ChildInfolettre objects filtered by the id_infolettre column
 * @method     ChildInfolettre[]|ObjectCollection findByIdentifiant(string $identifiant) Return ChildInfolettre objects filtered by the identifiant column
 * @method     ChildInfolettre[]|ObjectCollection findByNom(string $nom) Return ChildInfolettre objects filtered by the nom column
 * @method     ChildInfolettre[]|ObjectCollection findByDescriptif(string $descriptif) Return ChildInfolettre objects filtered by the descriptif column
 * @method     ChildInfolettre[]|ObjectCollection findByAutomatique(boolean $automatique) Return ChildInfolettre objects filtered by the automatique column
 * @method     ChildInfolettre[]|ObjectCollection findByDateSynchro(string $date_synchro) Return ChildInfolettre objects filtered by the date_synchro column
 * @method     ChildInfolettre[]|ObjectCollection findByActive(boolean $active) Return ChildInfolettre objects filtered by the active column
 * @method     ChildInfolettre[]|ObjectCollection findByLiaisonAuto(boolean $liaison_auto) Return ChildInfolettre objects filtered by the liaison_auto column
 * @method     ChildInfolettre[]|ObjectCollection findByOptions(string $options) Return ChildInfolettre objects filtered by the options column
 * @method     ChildInfolettre[]|ObjectCollection findByPosition(int $position) Return ChildInfolettre objects filtered by the position column
 * @method     ChildInfolettre[]|ObjectCollection findByCreatedAt(string $created_at) Return ChildInfolettre objects filtered by the created_at column
 * @method     ChildInfolettre[]|ObjectCollection findByUpdatedAt(string $updated_at) Return ChildInfolettre objects filtered by the updated_at column
 * @method     ChildInfolettre[]|\Propel\Runtime\Util\PropelModelPager paginate($page = 1, $maxPerPage = 10, ConnectionInterface $con = null) Issue a SELECT query based on the current ModelCriteria and uses a page and a maximum number of results per page to compute an offset and a limit
 *
 */
abstract class InfolettreQuery extends ModelCriteria
{
    protected $entityNotFoundExceptionClass = '\\Propel\\Runtime\\Exception\\EntityNotFoundException';

    /**
     * Initializes internal state of \Base\InfolettreQuery object.
     *
     * @param     string $dbName The database name
     * @param     string $modelName The phpName of a model, e.g. 'Book'
     * @param     string $modelAlias The alias for the model in this query, e.g. 'b'
     */
    public function __construct($dbName = 'simplasso', $modelName = '\\Infolettre', $modelAlias = null)
    {
        parent::__construct($dbName, $modelName, $modelAlias);
    }

    /**
     * Returns a new ChildInfolettreQuery object.
     *
     * @param     string $modelAlias The alias of a model in the query
     * @param     Criteria $criteria Optional Criteria to build the query from
     *
     * @return ChildInfolettreQuery
     */
    public static function create($modelAlias = null, Criteria $criteria = null)
    {
        if ($criteria instanceof ChildInfolettreQuery) {
            return $criteria;
        }
        $query = new ChildInfolettreQuery();
        if (null !== $modelAlias) {
            $query->setModelAlias($modelAlias);
        }
        if ($criteria instanceof Criteria) {
            $query->mergeWith($criteria);
        }

        return $query;
    }

    /**
     * Find object by primary key.
     * Propel uses the instance pool to skip the database if the object exists.
     * Go fast if the query is untouched.
     *
     * <code>
     * $obj  = $c->findPk(12, $con);
     * </code>
     *
     * @param mixed $key Primary key to use for the query
     * @param ConnectionInterface $con an optional connection object
     *
     * @return ChildInfolettre|array|mixed the result, formatted by the current formatter
     */
    public function findPk($key, ConnectionInterface $con = null)
    {
        if ($key === null) {
            return null;
        }

        if ($con === null) {
            $con = Propel::getServiceContainer()->getReadConnection(InfolettreTableMap::DATABASE_NAME);
        }

        $this->basePreSelect($con);

        if (
            $this->formatter || $this->modelAlias || $this->with || $this->select
            || $this->selectColumns || $this->asColumns || $this->selectModifiers
            || $this->map || $this->having || $this->joins
        ) {
            return $this->findPkComplex($key, $con);
        }

        if ((null !== ($obj = InfolettreTableMap::getInstanceFromPool(null === $key || is_scalar($key) || is_callable([$key, '__toString']) ? (string) $key : $key)))) {
            // the object is already in the instance pool
            return $obj;
        }

        return $this->findPkSimple($key, $con);
    }

    /**
     * Find object by primary key using raw SQL to go fast.
     * Bypass doSelect() and the object formatter by using generated code.
     *
     * @param     mixed $key Primary key to use for the query
     * @param     ConnectionInterface $con A connection object
     *
     * @throws \Propel\Runtime\Exception\PropelException
     *
     * @return ChildInfolettre A model object, or null if the key is not found
     */
    protected function findPkSimple($key, ConnectionInterface $con)
    {
        $sql = 'SELECT `id_infolettre`, `identifiant`, `nom`, `descriptif`, `automatique`, `date_synchro`, `active`, `liaison_auto`, `options`, `position`, `created_at`, `updated_at` FROM `com_infolettres` WHERE `id_infolettre` = :p0';
        try {
            $stmt = $con->prepare($sql);
            $stmt->bindValue(':p0', $key, PDO::PARAM_INT);
            $stmt->execute();
        } catch (Exception $e) {
            Propel::log($e->getMessage(), Propel::LOG_ERR);
            throw new PropelException(sprintf('Unable to execute SELECT statement [%s]', $sql), 0, $e);
        }
        $obj = null;
        if ($row = $stmt->fetch(\PDO::FETCH_NUM)) {
            /** @var ChildInfolettre $obj */
            $obj = new ChildInfolettre();
            $obj->hydrate($row);
            InfolettreTableMap::addInstanceToPool($obj, null === $key || is_scalar($key) || is_callable([$key, '__toString']) ? (string) $key : $key);
        }
        $stmt->closeCursor();

        return $obj;
    }

    /**
     * Find object by primary key.
     *
     * @param     mixed $key Primary key to use for the query
     * @param     ConnectionInterface $con A connection object
     *
     * @return ChildInfolettre|array|mixed the result, formatted by the current formatter
     */
    protected function findPkComplex($key, ConnectionInterface $con)
    {
        // As the query uses a PK condition, no limit(1) is necessary.
        $criteria = $this->isKeepQuery() ? clone $this : $this;
        $dataFetcher = $criteria
            ->filterByPrimaryKey($key)
            ->doSelect($con);

        return $criteria->getFormatter()->init($criteria)->formatOne($dataFetcher);
    }

    /**
     * Find objects by primary key
     * <code>
     * $objs = $c->findPks(array(12, 56, 832), $con);
     * </code>
     * @param     array $keys Primary keys to use for the query
     * @param     ConnectionInterface $con an optional connection object
     *
     * @return ObjectCollection|array|mixed the list of results, formatted by the current formatter
     */
    public function findPks($keys, ConnectionInterface $con = null)
    {
        if (null === $con) {
            $con = Propel::getServiceContainer()->getReadConnection($this->getDbName());
        }
        $this->basePreSelect($con);
        $criteria = $this->isKeepQuery() ? clone $this : $this;
        $dataFetcher = $criteria
            ->filterByPrimaryKeys($keys)
            ->doSelect($con);

        return $criteria->getFormatter()->init($criteria)->format($dataFetcher);
    }

    /**
     * Filter the query by primary key
     *
     * @param     mixed $key Primary key to use for the query
     *
     * @return $this|ChildInfolettreQuery The current query, for fluid interface
     */
    public function filterByPrimaryKey($key)
    {

        return $this->addUsingAlias(InfolettreTableMap::COL_ID_INFOLETTRE, $key, Criteria::EQUAL);
    }

    /**
     * Filter the query by a list of primary keys
     *
     * @param     array $keys The list of primary key to use for the query
     *
     * @return $this|ChildInfolettreQuery The current query, for fluid interface
     */
    public function filterByPrimaryKeys($keys)
    {

        return $this->addUsingAlias(InfolettreTableMap::COL_ID_INFOLETTRE, $keys, Criteria::IN);
    }

    /**
     * Filter the query on the id_infolettre column
     *
     * Example usage:
     * <code>
     * $query->filterByIdInfolettre(1234); // WHERE id_infolettre = 1234
     * $query->filterByIdInfolettre(array(12, 34)); // WHERE id_infolettre IN (12, 34)
     * $query->filterByIdInfolettre(array('min' => 12)); // WHERE id_infolettre > 12
     * </code>
     *
     * @param     mixed $idInfolettre The value to use as filter.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildInfolettreQuery The current query, for fluid interface
     */
    public function filterByIdInfolettre($idInfolettre = null, $comparison = null)
    {
        if (is_array($idInfolettre)) {
            $useMinMax = false;
            if (isset($idInfolettre['min'])) {
                $this->addUsingAlias(InfolettreTableMap::COL_ID_INFOLETTRE, $idInfolettre['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($idInfolettre['max'])) {
                $this->addUsingAlias(InfolettreTableMap::COL_ID_INFOLETTRE, $idInfolettre['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(InfolettreTableMap::COL_ID_INFOLETTRE, $idInfolettre, $comparison);
    }

    /**
     * Filter the query on the identifiant column
     *
     * Example usage:
     * <code>
     * $query->filterByIdentifiant('fooValue');   // WHERE identifiant = 'fooValue'
     * $query->filterByIdentifiant('%fooValue%', Criteria::LIKE); // WHERE identifiant LIKE '%fooValue%'
     * </code>
     *
     * @param     string $identifiant The value to use as filter.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildInfolettreQuery The current query, for fluid interface
     */
    public function filterByIdentifiant($identifiant = null, $comparison = null)
    {
        if (null === $comparison) {
            if (is_array($identifiant)) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(InfolettreTableMap::COL_IDENTIFIANT, $identifiant, $comparison);
    }

    /**
     * Filter the query on the nom column
     *
     * Example usage:
     * <code>
     * $query->filterByNom('fooValue');   // WHERE nom = 'fooValue'
     * $query->filterByNom('%fooValue%', Criteria::LIKE); // WHERE nom LIKE '%fooValue%'
     * </code>
     *
     * @param     string $nom The value to use as filter.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildInfolettreQuery The current query, for fluid interface
     */
    public function filterByNom($nom = null, $comparison = null)
    {
        if (null === $comparison) {
            if (is_array($nom)) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(InfolettreTableMap::COL_NOM, $nom, $comparison);
    }

    /**
     * Filter the query on the descriptif column
     *
     * Example usage:
     * <code>
     * $query->filterByDescriptif('fooValue');   // WHERE descriptif = 'fooValue'
     * $query->filterByDescriptif('%fooValue%', Criteria::LIKE); // WHERE descriptif LIKE '%fooValue%'
     * </code>
     *
     * @param     string $descriptif The value to use as filter.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildInfolettreQuery The current query, for fluid interface
     */
    public function filterByDescriptif($descriptif = null, $comparison = null)
    {
        if (null === $comparison) {
            if (is_array($descriptif)) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(InfolettreTableMap::COL_DESCRIPTIF, $descriptif, $comparison);
    }

    /**
     * Filter the query on the automatique column
     *
     * Example usage:
     * <code>
     * $query->filterByAutomatique(true); // WHERE automatique = true
     * $query->filterByAutomatique('yes'); // WHERE automatique = true
     * </code>
     *
     * @param     boolean|string $automatique The value to use as filter.
     *              Non-boolean arguments are converted using the following rules:
     *                * 1, '1', 'true',  'on',  and 'yes' are converted to boolean true
     *                * 0, '0', 'false', 'off', and 'no'  are converted to boolean false
     *              Check on string values is case insensitive (so 'FaLsE' is seen as 'false').
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildInfolettreQuery The current query, for fluid interface
     */
    public function filterByAutomatique($automatique = null, $comparison = null)
    {
        if (is_string($automatique)) {
            $automatique = in_array(strtolower($automatique), array('false', 'off', '-', 'no', 'n', '0', '')) ? false : true;
        }

        return $this->addUsingAlias(InfolettreTableMap::COL_AUTOMATIQUE, $automatique, $comparison);
    }

    /**
     * Filter the query on the date_synchro column
     *
     * Example usage:
     * <code>
     * $query->filterByDateSynchro('2011-03-14'); // WHERE date_synchro = '2011-03-14'
     * $query->filterByDateSynchro('now'); // WHERE date_synchro = '2011-03-14'
     * $query->filterByDateSynchro(array('max' => 'yesterday')); // WHERE date_synchro > '2011-03-13'
     * </code>
     *
     * @param     mixed $dateSynchro The value to use as filter.
     *              Values can be integers (unix timestamps), DateTime objects, or strings.
     *              Empty strings are treated as NULL.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildInfolettreQuery The current query, for fluid interface
     */
    public function filterByDateSynchro($dateSynchro = null, $comparison = null)
    {
        if (is_array($dateSynchro)) {
            $useMinMax = false;
            if (isset($dateSynchro['min'])) {
                $this->addUsingAlias(InfolettreTableMap::COL_DATE_SYNCHRO, $dateSynchro['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($dateSynchro['max'])) {
                $this->addUsingAlias(InfolettreTableMap::COL_DATE_SYNCHRO, $dateSynchro['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(InfolettreTableMap::COL_DATE_SYNCHRO, $dateSynchro, $comparison);
    }

    /**
     * Filter the query on the active column
     *
     * Example usage:
     * <code>
     * $query->filterByActive(true); // WHERE active = true
     * $query->filterByActive('yes'); // WHERE active = true
     * </code>
     *
     * @param     boolean|string $active The value to use as filter.
     *              Non-boolean arguments are converted using the following rules:
     *                * 1, '1', 'true',  'on',  and 'yes' are converted to boolean true
     *                * 0, '0', 'false', 'off', and 'no'  are converted to boolean false
     *              Check on string values is case insensitive (so 'FaLsE' is seen as 'false').
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildInfolettreQuery The current query, for fluid interface
     */
    public function filterByActive($active = null, $comparison = null)
    {
        if (is_string($active)) {
            $active = in_array(strtolower($active), array('false', 'off', '-', 'no', 'n', '0', '')) ? false : true;
        }

        return $this->addUsingAlias(InfolettreTableMap::COL_ACTIVE, $active, $comparison);
    }

    /**
     * Filter the query on the liaison_auto column
     *
     * Example usage:
     * <code>
     * $query->filterByLiaisonAuto(true); // WHERE liaison_auto = true
     * $query->filterByLiaisonAuto('yes'); // WHERE liaison_auto = true
     * </code>
     *
     * @param     boolean|string $liaisonAuto The value to use as filter.
     *              Non-boolean arguments are converted using the following rules:
     *                * 1, '1', 'true',  'on',  and 'yes' are converted to boolean true
     *                * 0, '0', 'false', 'off', and 'no'  are converted to boolean false
     *              Check on string values is case insensitive (so 'FaLsE' is seen as 'false').
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildInfolettreQuery The current query, for fluid interface
     */
    public function filterByLiaisonAuto($liaisonAuto = null, $comparison = null)
    {
        if (is_string($liaisonAuto)) {
            $liaisonAuto = in_array(strtolower($liaisonAuto), array('false', 'off', '-', 'no', 'n', '0', '')) ? false : true;
        }

        return $this->addUsingAlias(InfolettreTableMap::COL_LIAISON_AUTO, $liaisonAuto, $comparison);
    }

    /**
     * Filter the query on the options column
     *
     * Example usage:
     * <code>
     * $query->filterByOptions('fooValue');   // WHERE options = 'fooValue'
     * $query->filterByOptions('%fooValue%', Criteria::LIKE); // WHERE options LIKE '%fooValue%'
     * </code>
     *
     * @param     string $options The value to use as filter.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildInfolettreQuery The current query, for fluid interface
     */
    public function filterByOptions($options = null, $comparison = null)
    {
        if (null === $comparison) {
            if (is_array($options)) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(InfolettreTableMap::COL_OPTIONS, $options, $comparison);
    }

    /**
     * Filter the query on the position column
     *
     * Example usage:
     * <code>
     * $query->filterByPosition(1234); // WHERE position = 1234
     * $query->filterByPosition(array(12, 34)); // WHERE position IN (12, 34)
     * $query->filterByPosition(array('min' => 12)); // WHERE position > 12
     * </code>
     *
     * @param     mixed $position The value to use as filter.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildInfolettreQuery The current query, for fluid interface
     */
    public function filterByPosition($position = null, $comparison = null)
    {
        if (is_array($position)) {
            $useMinMax = false;
            if (isset($position['min'])) {
                $this->addUsingAlias(InfolettreTableMap::COL_POSITION, $position['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($position['max'])) {
                $this->addUsingAlias(InfolettreTableMap::COL_POSITION, $position['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(InfolettreTableMap::COL_POSITION, $position, $comparison);
    }

    /**
     * Filter the query on the created_at column
     *
     * Example usage:
     * <code>
     * $query->filterByCreatedAt('2011-03-14'); // WHERE created_at = '2011-03-14'
     * $query->filterByCreatedAt('now'); // WHERE created_at = '2011-03-14'
     * $query->filterByCreatedAt(array('max' => 'yesterday')); // WHERE created_at > '2011-03-13'
     * </code>
     *
     * @param     mixed $createdAt The value to use as filter.
     *              Values can be integers (unix timestamps), DateTime objects, or strings.
     *              Empty strings are treated as NULL.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildInfolettreQuery The current query, for fluid interface
     */
    public function filterByCreatedAt($createdAt = null, $comparison = null)
    {
        if (is_array($createdAt)) {
            $useMinMax = false;
            if (isset($createdAt['min'])) {
                $this->addUsingAlias(InfolettreTableMap::COL_CREATED_AT, $createdAt['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($createdAt['max'])) {
                $this->addUsingAlias(InfolettreTableMap::COL_CREATED_AT, $createdAt['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(InfolettreTableMap::COL_CREATED_AT, $createdAt, $comparison);
    }

    /**
     * Filter the query on the updated_at column
     *
     * Example usage:
     * <code>
     * $query->filterByUpdatedAt('2011-03-14'); // WHERE updated_at = '2011-03-14'
     * $query->filterByUpdatedAt('now'); // WHERE updated_at = '2011-03-14'
     * $query->filterByUpdatedAt(array('max' => 'yesterday')); // WHERE updated_at > '2011-03-13'
     * </code>
     *
     * @param     mixed $updatedAt The value to use as filter.
     *              Values can be integers (unix timestamps), DateTime objects, or strings.
     *              Empty strings are treated as NULL.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildInfolettreQuery The current query, for fluid interface
     */
    public function filterByUpdatedAt($updatedAt = null, $comparison = null)
    {
        if (is_array($updatedAt)) {
            $useMinMax = false;
            if (isset($updatedAt['min'])) {
                $this->addUsingAlias(InfolettreTableMap::COL_UPDATED_AT, $updatedAt['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($updatedAt['max'])) {
                $this->addUsingAlias(InfolettreTableMap::COL_UPDATED_AT, $updatedAt['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(InfolettreTableMap::COL_UPDATED_AT, $updatedAt, $comparison);
    }

    /**
     * Filter the query by a related \InfolettreInscrit object
     *
     * @param \InfolettreInscrit|ObjectCollection $infolettreInscrit the related object to use as filter
     * @param string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return ChildInfolettreQuery The current query, for fluid interface
     */
    public function filterByInfolettreInscrit($infolettreInscrit, $comparison = null)
    {
        if ($infolettreInscrit instanceof \InfolettreInscrit) {
            return $this
                ->addUsingAlias(InfolettreTableMap::COL_ID_INFOLETTRE, $infolettreInscrit->getIdInfolettre(), $comparison);
        } elseif ($infolettreInscrit instanceof ObjectCollection) {
            return $this
                ->useInfolettreInscritQuery()
                ->filterByPrimaryKeys($infolettreInscrit->getPrimaryKeys())
                ->endUse();
        } else {
            throw new PropelException('filterByInfolettreInscrit() only accepts arguments of type \InfolettreInscrit or Collection');
        }
    }

    /**
     * Adds a JOIN clause to the query using the InfolettreInscrit relation
     *
     * @param     string $relationAlias optional alias for the relation
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return $this|ChildInfolettreQuery The current query, for fluid interface
     */
    public function joinInfolettreInscrit($relationAlias = null, $joinType = Criteria::INNER_JOIN)
    {
        $tableMap = $this->getTableMap();
        $relationMap = $tableMap->getRelation('InfolettreInscrit');

        // create a ModelJoin object for this join
        $join = new ModelJoin();
        $join->setJoinType($joinType);
        $join->setRelationMap($relationMap, $this->useAliasInSQL ? $this->getModelAlias() : null, $relationAlias);
        if ($previousJoin = $this->getPreviousJoin()) {
            $join->setPreviousJoin($previousJoin);
        }

        // add the ModelJoin to the current object
        if ($relationAlias) {
            $this->addAlias($relationAlias, $relationMap->getRightTable()->getName());
            $this->addJoinObject($join, $relationAlias);
        } else {
            $this->addJoinObject($join, 'InfolettreInscrit');
        }

        return $this;
    }

    /**
     * Use the InfolettreInscrit relation InfolettreInscrit object
     *
     * @see useQuery()
     *
     * @param     string $relationAlias optional alias for the relation,
     *                                   to be used as main alias in the secondary query
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return \InfolettreInscritQuery A secondary query class using the current class as primary query
     */
    public function useInfolettreInscritQuery($relationAlias = null, $joinType = Criteria::INNER_JOIN)
    {
        return $this
            ->joinInfolettreInscrit($relationAlias, $joinType)
            ->useQuery($relationAlias ? $relationAlias : 'InfolettreInscrit', '\InfolettreInscritQuery');
    }

    /**
     * Exclude object from result
     *
     * @param   ChildInfolettre $infolettre Object to remove from the list of results
     *
     * @return $this|ChildInfolettreQuery The current query, for fluid interface
     */
    public function prune($infolettre = null)
    {
        if ($infolettre) {
            $this->addUsingAlias(InfolettreTableMap::COL_ID_INFOLETTRE, $infolettre->getIdInfolettre(), Criteria::NOT_EQUAL);
        }

        return $this;
    }

    /**
     * Deletes all rows from the com_infolettres table.
     *
     * @param ConnectionInterface $con the connection to use
     * @return int The number of affected rows (if supported by underlying database driver).
     */
    public function doDeleteAll(ConnectionInterface $con = null)
    {
        if (null === $con) {
            $con = Propel::getServiceContainer()->getWriteConnection(InfolettreTableMap::DATABASE_NAME);
        }

        // use transaction because $criteria could contain info
        // for more than one table or we could emulating ON DELETE CASCADE, etc.
        return $con->transaction(function () use ($con) {
            $affectedRows = 0; // initialize var to track total num of affected rows
            $affectedRows += parent::doDeleteAll($con);
            // Because this db requires some delete cascade/set null emulation, we have to
            // clear the cached instance *after* the emulation has happened (since
            // instances get re-added by the select statement contained therein).
            InfolettreTableMap::clearInstancePool();
            InfolettreTableMap::clearRelatedInstancePool();

            return $affectedRows;
        });
    }

    /**
     * Performs a DELETE on the database based on the current ModelCriteria
     *
     * @param ConnectionInterface $con the connection to use
     * @return int             The number of affected rows (if supported by underlying database driver).  This includes CASCADE-related rows
     *                         if supported by native driver or if emulated using Propel.
     * @throws PropelException Any exceptions caught during processing will be
     *                         rethrown wrapped into a PropelException.
     */
    public function delete(ConnectionInterface $con = null)
    {
        if (null === $con) {
            $con = Propel::getServiceContainer()->getWriteConnection(InfolettreTableMap::DATABASE_NAME);
        }

        $criteria = $this;

        // Set the correct dbName
        $criteria->setDbName(InfolettreTableMap::DATABASE_NAME);

        // use transaction because $criteria could contain info
        // for more than one table or we could emulating ON DELETE CASCADE, etc.
        return $con->transaction(function () use ($con, $criteria) {
            $affectedRows = 0; // initialize var to track total num of affected rows

            InfolettreTableMap::removeInstanceFromPool($criteria);

            $affectedRows += ModelCriteria::delete($con);
            InfolettreTableMap::clearRelatedInstancePool();

            return $affectedRows;
        });
    }

    // timestampable behavior

    /**
     * Filter by the latest updated
     *
     * @param      int $nbDays Maximum age of the latest update in days
     *
     * @return     $this|ChildInfolettreQuery The current query, for fluid interface
     */
    public function recentlyUpdated($nbDays = 7)
    {
        return $this->addUsingAlias(InfolettreTableMap::COL_UPDATED_AT, time() - $nbDays * 24 * 60 * 60, Criteria::GREATER_EQUAL);
    }

    /**
     * Order by update date desc
     *
     * @return     $this|ChildInfolettreQuery The current query, for fluid interface
     */
    public function lastUpdatedFirst()
    {
        return $this->addDescendingOrderByColumn(InfolettreTableMap::COL_UPDATED_AT);
    }

    /**
     * Order by update date asc
     *
     * @return     $this|ChildInfolettreQuery The current query, for fluid interface
     */
    public function firstUpdatedFirst()
    {
        return $this->addAscendingOrderByColumn(InfolettreTableMap::COL_UPDATED_AT);
    }

    /**
     * Order by create date desc
     *
     * @return     $this|ChildInfolettreQuery The current query, for fluid interface
     */
    public function lastCreatedFirst()
    {
        return $this->addDescendingOrderByColumn(InfolettreTableMap::COL_CREATED_AT);
    }

    /**
     * Filter by the latest created
     *
     * @param      int $nbDays Maximum age of in days
     *
     * @return     $this|ChildInfolettreQuery The current query, for fluid interface
     */
    public function recentlyCreated($nbDays = 7)
    {
        return $this->addUsingAlias(InfolettreTableMap::COL_CREATED_AT, time() - $nbDays * 24 * 60 * 60, Criteria::GREATER_EQUAL);
    }

    /**
     * Order by create date asc
     *
     * @return     $this|ChildInfolettreQuery The current query, for fluid interface
     */
    public function firstCreatedFirst()
    {
        return $this->addAscendingOrderByColumn(InfolettreTableMap::COL_CREATED_AT);
    }

} // InfolettreQuery
