<?php

namespace Base;

use \AssoServicerendusArchive as ChildAssoServicerendusArchive;
use \Servicerendu as ChildServicerendu;
use \ServicerenduQuery as ChildServicerenduQuery;
use \Exception;
use \PDO;
use Map\ServicerenduTableMap;
use Propel\Runtime\Propel;
use Propel\Runtime\ActiveQuery\Criteria;
use Propel\Runtime\ActiveQuery\ModelCriteria;
use Propel\Runtime\ActiveQuery\ModelJoin;
use Propel\Runtime\Collection\ObjectCollection;
use Propel\Runtime\Connection\ConnectionInterface;
use Propel\Runtime\Exception\PropelException;

/**
 * Base class that represents a query for the 'asso_servicerendus' table.
 *
 *
 *
 * @method     ChildServicerenduQuery orderByIdServicerendu($order = Criteria::ASC) Order by the id_servicerendu column
 * @method     ChildServicerenduQuery orderByIdMembre($order = Criteria::ASC) Order by the id_membre column
 * @method     ChildServicerenduQuery orderByMontant($order = Criteria::ASC) Order by the montant column
 * @method     ChildServicerenduQuery orderByDateEnregistrement($order = Criteria::ASC) Order by the date_enregistrement column
 * @method     ChildServicerenduQuery orderByDateDebut($order = Criteria::ASC) Order by the date_debut column
 * @method     ChildServicerenduQuery orderByDateFin($order = Criteria::ASC) Order by the date_fin column
 * @method     ChildServicerenduQuery orderByOrigine($order = Criteria::ASC) Order by the origine column
 * @method     ChildServicerenduQuery orderByPremier($order = Criteria::ASC) Order by the premier column
 * @method     ChildServicerenduQuery orderByDernier($order = Criteria::ASC) Order by the dernier column
 * @method     ChildServicerenduQuery orderByIdEntite($order = Criteria::ASC) Order by the id_entite column
 * @method     ChildServicerenduQuery orderByIdPrestation($order = Criteria::ASC) Order by the id_prestation column
 * @method     ChildServicerenduQuery orderByRegle($order = Criteria::ASC) Order by the regle column
 * @method     ChildServicerenduQuery orderByComptabilise($order = Criteria::ASC) Order by the comptabilise column
 * @method     ChildServicerenduQuery orderByObservation($order = Criteria::ASC) Order by the observation column
 * @method     ChildServicerenduQuery orderByIdTva($order = Criteria::ASC) Order by the id_tva column
 * @method     ChildServicerenduQuery orderByQuantite($order = Criteria::ASC) Order by the quantite column
 * @method     ChildServicerenduQuery orderByCreatedAt($order = Criteria::ASC) Order by the created_at column
 * @method     ChildServicerenduQuery orderByUpdatedAt($order = Criteria::ASC) Order by the updated_at column
 *
 * @method     ChildServicerenduQuery groupByIdServicerendu() Group by the id_servicerendu column
 * @method     ChildServicerenduQuery groupByIdMembre() Group by the id_membre column
 * @method     ChildServicerenduQuery groupByMontant() Group by the montant column
 * @method     ChildServicerenduQuery groupByDateEnregistrement() Group by the date_enregistrement column
 * @method     ChildServicerenduQuery groupByDateDebut() Group by the date_debut column
 * @method     ChildServicerenduQuery groupByDateFin() Group by the date_fin column
 * @method     ChildServicerenduQuery groupByOrigine() Group by the origine column
 * @method     ChildServicerenduQuery groupByPremier() Group by the premier column
 * @method     ChildServicerenduQuery groupByDernier() Group by the dernier column
 * @method     ChildServicerenduQuery groupByIdEntite() Group by the id_entite column
 * @method     ChildServicerenduQuery groupByIdPrestation() Group by the id_prestation column
 * @method     ChildServicerenduQuery groupByRegle() Group by the regle column
 * @method     ChildServicerenduQuery groupByComptabilise() Group by the comptabilise column
 * @method     ChildServicerenduQuery groupByObservation() Group by the observation column
 * @method     ChildServicerenduQuery groupByIdTva() Group by the id_tva column
 * @method     ChildServicerenduQuery groupByQuantite() Group by the quantite column
 * @method     ChildServicerenduQuery groupByCreatedAt() Group by the created_at column
 * @method     ChildServicerenduQuery groupByUpdatedAt() Group by the updated_at column
 *
 * @method     ChildServicerenduQuery leftJoin($relation) Adds a LEFT JOIN clause to the query
 * @method     ChildServicerenduQuery rightJoin($relation) Adds a RIGHT JOIN clause to the query
 * @method     ChildServicerenduQuery innerJoin($relation) Adds a INNER JOIN clause to the query
 *
 * @method     ChildServicerenduQuery leftJoinWith($relation) Adds a LEFT JOIN clause and with to the query
 * @method     ChildServicerenduQuery rightJoinWith($relation) Adds a RIGHT JOIN clause and with to the query
 * @method     ChildServicerenduQuery innerJoinWith($relation) Adds a INNER JOIN clause and with to the query
 *
 * @method     ChildServicerenduQuery leftJoinPrestation($relationAlias = null) Adds a LEFT JOIN clause to the query using the Prestation relation
 * @method     ChildServicerenduQuery rightJoinPrestation($relationAlias = null) Adds a RIGHT JOIN clause to the query using the Prestation relation
 * @method     ChildServicerenduQuery innerJoinPrestation($relationAlias = null) Adds a INNER JOIN clause to the query using the Prestation relation
 *
 * @method     ChildServicerenduQuery joinWithPrestation($joinType = Criteria::INNER_JOIN) Adds a join clause and with to the query using the Prestation relation
 *
 * @method     ChildServicerenduQuery leftJoinWithPrestation() Adds a LEFT JOIN clause and with to the query using the Prestation relation
 * @method     ChildServicerenduQuery rightJoinWithPrestation() Adds a RIGHT JOIN clause and with to the query using the Prestation relation
 * @method     ChildServicerenduQuery innerJoinWithPrestation() Adds a INNER JOIN clause and with to the query using the Prestation relation
 *
 * @method     ChildServicerenduQuery leftJoinEntite($relationAlias = null) Adds a LEFT JOIN clause to the query using the Entite relation
 * @method     ChildServicerenduQuery rightJoinEntite($relationAlias = null) Adds a RIGHT JOIN clause to the query using the Entite relation
 * @method     ChildServicerenduQuery innerJoinEntite($relationAlias = null) Adds a INNER JOIN clause to the query using the Entite relation
 *
 * @method     ChildServicerenduQuery joinWithEntite($joinType = Criteria::INNER_JOIN) Adds a join clause and with to the query using the Entite relation
 *
 * @method     ChildServicerenduQuery leftJoinWithEntite() Adds a LEFT JOIN clause and with to the query using the Entite relation
 * @method     ChildServicerenduQuery rightJoinWithEntite() Adds a RIGHT JOIN clause and with to the query using the Entite relation
 * @method     ChildServicerenduQuery innerJoinWithEntite() Adds a INNER JOIN clause and with to the query using the Entite relation
 *
 * @method     ChildServicerenduQuery leftJoinMembre($relationAlias = null) Adds a LEFT JOIN clause to the query using the Membre relation
 * @method     ChildServicerenduQuery rightJoinMembre($relationAlias = null) Adds a RIGHT JOIN clause to the query using the Membre relation
 * @method     ChildServicerenduQuery innerJoinMembre($relationAlias = null) Adds a INNER JOIN clause to the query using the Membre relation
 *
 * @method     ChildServicerenduQuery joinWithMembre($joinType = Criteria::INNER_JOIN) Adds a join clause and with to the query using the Membre relation
 *
 * @method     ChildServicerenduQuery leftJoinWithMembre() Adds a LEFT JOIN clause and with to the query using the Membre relation
 * @method     ChildServicerenduQuery rightJoinWithMembre() Adds a RIGHT JOIN clause and with to the query using the Membre relation
 * @method     ChildServicerenduQuery innerJoinWithMembre() Adds a INNER JOIN clause and with to the query using the Membre relation
 *
 * @method     ChildServicerenduQuery leftJoinServicerenduRelatedByOrigine($relationAlias = null) Adds a LEFT JOIN clause to the query using the ServicerenduRelatedByOrigine relation
 * @method     ChildServicerenduQuery rightJoinServicerenduRelatedByOrigine($relationAlias = null) Adds a RIGHT JOIN clause to the query using the ServicerenduRelatedByOrigine relation
 * @method     ChildServicerenduQuery innerJoinServicerenduRelatedByOrigine($relationAlias = null) Adds a INNER JOIN clause to the query using the ServicerenduRelatedByOrigine relation
 *
 * @method     ChildServicerenduQuery joinWithServicerenduRelatedByOrigine($joinType = Criteria::INNER_JOIN) Adds a join clause and with to the query using the ServicerenduRelatedByOrigine relation
 *
 * @method     ChildServicerenduQuery leftJoinWithServicerenduRelatedByOrigine() Adds a LEFT JOIN clause and with to the query using the ServicerenduRelatedByOrigine relation
 * @method     ChildServicerenduQuery rightJoinWithServicerenduRelatedByOrigine() Adds a RIGHT JOIN clause and with to the query using the ServicerenduRelatedByOrigine relation
 * @method     ChildServicerenduQuery innerJoinWithServicerenduRelatedByOrigine() Adds a INNER JOIN clause and with to the query using the ServicerenduRelatedByOrigine relation
 *
 * @method     ChildServicerenduQuery leftJoinServicepaiement($relationAlias = null) Adds a LEFT JOIN clause to the query using the Servicepaiement relation
 * @method     ChildServicerenduQuery rightJoinServicepaiement($relationAlias = null) Adds a RIGHT JOIN clause to the query using the Servicepaiement relation
 * @method     ChildServicerenduQuery innerJoinServicepaiement($relationAlias = null) Adds a INNER JOIN clause to the query using the Servicepaiement relation
 *
 * @method     ChildServicerenduQuery joinWithServicepaiement($joinType = Criteria::INNER_JOIN) Adds a join clause and with to the query using the Servicepaiement relation
 *
 * @method     ChildServicerenduQuery leftJoinWithServicepaiement() Adds a LEFT JOIN clause and with to the query using the Servicepaiement relation
 * @method     ChildServicerenduQuery rightJoinWithServicepaiement() Adds a RIGHT JOIN clause and with to the query using the Servicepaiement relation
 * @method     ChildServicerenduQuery innerJoinWithServicepaiement() Adds a INNER JOIN clause and with to the query using the Servicepaiement relation
 *
 * @method     ChildServicerenduQuery leftJoinServicerenduRelatedByIdServicerendu($relationAlias = null) Adds a LEFT JOIN clause to the query using the ServicerenduRelatedByIdServicerendu relation
 * @method     ChildServicerenduQuery rightJoinServicerenduRelatedByIdServicerendu($relationAlias = null) Adds a RIGHT JOIN clause to the query using the ServicerenduRelatedByIdServicerendu relation
 * @method     ChildServicerenduQuery innerJoinServicerenduRelatedByIdServicerendu($relationAlias = null) Adds a INNER JOIN clause to the query using the ServicerenduRelatedByIdServicerendu relation
 *
 * @method     ChildServicerenduQuery joinWithServicerenduRelatedByIdServicerendu($joinType = Criteria::INNER_JOIN) Adds a join clause and with to the query using the ServicerenduRelatedByIdServicerendu relation
 *
 * @method     ChildServicerenduQuery leftJoinWithServicerenduRelatedByIdServicerendu() Adds a LEFT JOIN clause and with to the query using the ServicerenduRelatedByIdServicerendu relation
 * @method     ChildServicerenduQuery rightJoinWithServicerenduRelatedByIdServicerendu() Adds a RIGHT JOIN clause and with to the query using the ServicerenduRelatedByIdServicerendu relation
 * @method     ChildServicerenduQuery innerJoinWithServicerenduRelatedByIdServicerendu() Adds a INNER JOIN clause and with to the query using the ServicerenduRelatedByIdServicerendu relation
 *
 * @method     ChildServicerenduQuery leftJoinServicerenduIndividu($relationAlias = null) Adds a LEFT JOIN clause to the query using the ServicerenduIndividu relation
 * @method     ChildServicerenduQuery rightJoinServicerenduIndividu($relationAlias = null) Adds a RIGHT JOIN clause to the query using the ServicerenduIndividu relation
 * @method     ChildServicerenduQuery innerJoinServicerenduIndividu($relationAlias = null) Adds a INNER JOIN clause to the query using the ServicerenduIndividu relation
 *
 * @method     ChildServicerenduQuery joinWithServicerenduIndividu($joinType = Criteria::INNER_JOIN) Adds a join clause and with to the query using the ServicerenduIndividu relation
 *
 * @method     ChildServicerenduQuery leftJoinWithServicerenduIndividu() Adds a LEFT JOIN clause and with to the query using the ServicerenduIndividu relation
 * @method     ChildServicerenduQuery rightJoinWithServicerenduIndividu() Adds a RIGHT JOIN clause and with to the query using the ServicerenduIndividu relation
 * @method     ChildServicerenduQuery innerJoinWithServicerenduIndividu() Adds a INNER JOIN clause and with to the query using the ServicerenduIndividu relation
 *
 * @method     \PrestationQuery|\EntiteQuery|\MembreQuery|\ServicerenduQuery|\ServicepaiementQuery|\ServicerenduIndividuQuery endUse() Finalizes a secondary criteria and merges it with its primary Criteria
 *
 * @method     ChildServicerendu findOne(ConnectionInterface $con = null) Return the first ChildServicerendu matching the query
 * @method     ChildServicerendu findOneOrCreate(ConnectionInterface $con = null) Return the first ChildServicerendu matching the query, or a new ChildServicerendu object populated from the query conditions when no match is found
 *
 * @method     ChildServicerendu findOneByIdServicerendu(string $id_servicerendu) Return the first ChildServicerendu filtered by the id_servicerendu column
 * @method     ChildServicerendu findOneByIdMembre(string $id_membre) Return the first ChildServicerendu filtered by the id_membre column
 * @method     ChildServicerendu findOneByMontant(double $montant) Return the first ChildServicerendu filtered by the montant column
 * @method     ChildServicerendu findOneByDateEnregistrement(string $date_enregistrement) Return the first ChildServicerendu filtered by the date_enregistrement column
 * @method     ChildServicerendu findOneByDateDebut(string $date_debut) Return the first ChildServicerendu filtered by the date_debut column
 * @method     ChildServicerendu findOneByDateFin(string $date_fin) Return the first ChildServicerendu filtered by the date_fin column
 * @method     ChildServicerendu findOneByOrigine(int $origine) Return the first ChildServicerendu filtered by the origine column
 * @method     ChildServicerendu findOneByPremier(int $premier) Return the first ChildServicerendu filtered by the premier column
 * @method     ChildServicerendu findOneByDernier(int $dernier) Return the first ChildServicerendu filtered by the dernier column
 * @method     ChildServicerendu findOneByIdEntite(int $id_entite) Return the first ChildServicerendu filtered by the id_entite column
 * @method     ChildServicerendu findOneByIdPrestation(string $id_prestation) Return the first ChildServicerendu filtered by the id_prestation column
 * @method     ChildServicerendu findOneByRegle(int $regle) Return the first ChildServicerendu filtered by the regle column
 * @method     ChildServicerendu findOneByComptabilise(boolean $comptabilise) Return the first ChildServicerendu filtered by the comptabilise column
 * @method     ChildServicerendu findOneByObservation(string $observation) Return the first ChildServicerendu filtered by the observation column
 * @method     ChildServicerendu findOneByIdTva(string $id_tva) Return the first ChildServicerendu filtered by the id_tva column
 * @method     ChildServicerendu findOneByQuantite(double $quantite) Return the first ChildServicerendu filtered by the quantite column
 * @method     ChildServicerendu findOneByCreatedAt(string $created_at) Return the first ChildServicerendu filtered by the created_at column
 * @method     ChildServicerendu findOneByUpdatedAt(string $updated_at) Return the first ChildServicerendu filtered by the updated_at column *

 * @method     ChildServicerendu requirePk($key, ConnectionInterface $con = null) Return the ChildServicerendu by primary key and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildServicerendu requireOne(ConnectionInterface $con = null) Return the first ChildServicerendu matching the query and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 *
 * @method     ChildServicerendu requireOneByIdServicerendu(string $id_servicerendu) Return the first ChildServicerendu filtered by the id_servicerendu column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildServicerendu requireOneByIdMembre(string $id_membre) Return the first ChildServicerendu filtered by the id_membre column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildServicerendu requireOneByMontant(double $montant) Return the first ChildServicerendu filtered by the montant column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildServicerendu requireOneByDateEnregistrement(string $date_enregistrement) Return the first ChildServicerendu filtered by the date_enregistrement column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildServicerendu requireOneByDateDebut(string $date_debut) Return the first ChildServicerendu filtered by the date_debut column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildServicerendu requireOneByDateFin(string $date_fin) Return the first ChildServicerendu filtered by the date_fin column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildServicerendu requireOneByOrigine(int $origine) Return the first ChildServicerendu filtered by the origine column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildServicerendu requireOneByPremier(int $premier) Return the first ChildServicerendu filtered by the premier column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildServicerendu requireOneByDernier(int $dernier) Return the first ChildServicerendu filtered by the dernier column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildServicerendu requireOneByIdEntite(int $id_entite) Return the first ChildServicerendu filtered by the id_entite column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildServicerendu requireOneByIdPrestation(string $id_prestation) Return the first ChildServicerendu filtered by the id_prestation column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildServicerendu requireOneByRegle(int $regle) Return the first ChildServicerendu filtered by the regle column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildServicerendu requireOneByComptabilise(boolean $comptabilise) Return the first ChildServicerendu filtered by the comptabilise column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildServicerendu requireOneByObservation(string $observation) Return the first ChildServicerendu filtered by the observation column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildServicerendu requireOneByIdTva(string $id_tva) Return the first ChildServicerendu filtered by the id_tva column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildServicerendu requireOneByQuantite(double $quantite) Return the first ChildServicerendu filtered by the quantite column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildServicerendu requireOneByCreatedAt(string $created_at) Return the first ChildServicerendu filtered by the created_at column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildServicerendu requireOneByUpdatedAt(string $updated_at) Return the first ChildServicerendu filtered by the updated_at column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 *
 * @method     ChildServicerendu[]|ObjectCollection find(ConnectionInterface $con = null) Return ChildServicerendu objects based on current ModelCriteria
 * @method     ChildServicerendu[]|ObjectCollection findByIdServicerendu(string $id_servicerendu) Return ChildServicerendu objects filtered by the id_servicerendu column
 * @method     ChildServicerendu[]|ObjectCollection findByIdMembre(string $id_membre) Return ChildServicerendu objects filtered by the id_membre column
 * @method     ChildServicerendu[]|ObjectCollection findByMontant(double $montant) Return ChildServicerendu objects filtered by the montant column
 * @method     ChildServicerendu[]|ObjectCollection findByDateEnregistrement(string $date_enregistrement) Return ChildServicerendu objects filtered by the date_enregistrement column
 * @method     ChildServicerendu[]|ObjectCollection findByDateDebut(string $date_debut) Return ChildServicerendu objects filtered by the date_debut column
 * @method     ChildServicerendu[]|ObjectCollection findByDateFin(string $date_fin) Return ChildServicerendu objects filtered by the date_fin column
 * @method     ChildServicerendu[]|ObjectCollection findByOrigine(int $origine) Return ChildServicerendu objects filtered by the origine column
 * @method     ChildServicerendu[]|ObjectCollection findByPremier(int $premier) Return ChildServicerendu objects filtered by the premier column
 * @method     ChildServicerendu[]|ObjectCollection findByDernier(int $dernier) Return ChildServicerendu objects filtered by the dernier column
 * @method     ChildServicerendu[]|ObjectCollection findByIdEntite(int $id_entite) Return ChildServicerendu objects filtered by the id_entite column
 * @method     ChildServicerendu[]|ObjectCollection findByIdPrestation(string $id_prestation) Return ChildServicerendu objects filtered by the id_prestation column
 * @method     ChildServicerendu[]|ObjectCollection findByRegle(int $regle) Return ChildServicerendu objects filtered by the regle column
 * @method     ChildServicerendu[]|ObjectCollection findByComptabilise(boolean $comptabilise) Return ChildServicerendu objects filtered by the comptabilise column
 * @method     ChildServicerendu[]|ObjectCollection findByObservation(string $observation) Return ChildServicerendu objects filtered by the observation column
 * @method     ChildServicerendu[]|ObjectCollection findByIdTva(string $id_tva) Return ChildServicerendu objects filtered by the id_tva column
 * @method     ChildServicerendu[]|ObjectCollection findByQuantite(double $quantite) Return ChildServicerendu objects filtered by the quantite column
 * @method     ChildServicerendu[]|ObjectCollection findByCreatedAt(string $created_at) Return ChildServicerendu objects filtered by the created_at column
 * @method     ChildServicerendu[]|ObjectCollection findByUpdatedAt(string $updated_at) Return ChildServicerendu objects filtered by the updated_at column
 * @method     ChildServicerendu[]|\Propel\Runtime\Util\PropelModelPager paginate($page = 1, $maxPerPage = 10, ConnectionInterface $con = null) Issue a SELECT query based on the current ModelCriteria and uses a page and a maximum number of results per page to compute an offset and a limit
 *
 */
abstract class ServicerenduQuery extends ModelCriteria
{

    // archivable behavior
    protected $archiveOnDelete = true;
protected $entityNotFoundExceptionClass = '\\Propel\\Runtime\\Exception\\EntityNotFoundException';

    /**
     * Initializes internal state of \Base\ServicerenduQuery object.
     *
     * @param     string $dbName The database name
     * @param     string $modelName The phpName of a model, e.g. 'Book'
     * @param     string $modelAlias The alias for the model in this query, e.g. 'b'
     */
    public function __construct($dbName = 'simplasso', $modelName = '\\Servicerendu', $modelAlias = null)
    {
        parent::__construct($dbName, $modelName, $modelAlias);
    }

    /**
     * Returns a new ChildServicerenduQuery object.
     *
     * @param     string $modelAlias The alias of a model in the query
     * @param     Criteria $criteria Optional Criteria to build the query from
     *
     * @return ChildServicerenduQuery
     */
    public static function create($modelAlias = null, Criteria $criteria = null)
    {
        if ($criteria instanceof ChildServicerenduQuery) {
            return $criteria;
        }
        $query = new ChildServicerenduQuery();
        if (null !== $modelAlias) {
            $query->setModelAlias($modelAlias);
        }
        if ($criteria instanceof Criteria) {
            $query->mergeWith($criteria);
        }

        return $query;
    }

    /**
     * Find object by primary key.
     * Propel uses the instance pool to skip the database if the object exists.
     * Go fast if the query is untouched.
     *
     * <code>
     * $obj  = $c->findPk(12, $con);
     * </code>
     *
     * @param mixed $key Primary key to use for the query
     * @param ConnectionInterface $con an optional connection object
     *
     * @return ChildServicerendu|array|mixed the result, formatted by the current formatter
     */
    public function findPk($key, ConnectionInterface $con = null)
    {
        if ($key === null) {
            return null;
        }

        if ($con === null) {
            $con = Propel::getServiceContainer()->getReadConnection(ServicerenduTableMap::DATABASE_NAME);
        }

        $this->basePreSelect($con);

        if (
            $this->formatter || $this->modelAlias || $this->with || $this->select
            || $this->selectColumns || $this->asColumns || $this->selectModifiers
            || $this->map || $this->having || $this->joins
        ) {
            return $this->findPkComplex($key, $con);
        }

        if ((null !== ($obj = ServicerenduTableMap::getInstanceFromPool(null === $key || is_scalar($key) || is_callable([$key, '__toString']) ? (string) $key : $key)))) {
            // the object is already in the instance pool
            return $obj;
        }

        return $this->findPkSimple($key, $con);
    }

    /**
     * Find object by primary key using raw SQL to go fast.
     * Bypass doSelect() and the object formatter by using generated code.
     *
     * @param     mixed $key Primary key to use for the query
     * @param     ConnectionInterface $con A connection object
     *
     * @throws \Propel\Runtime\Exception\PropelException
     *
     * @return ChildServicerendu A model object, or null if the key is not found
     */
    protected function findPkSimple($key, ConnectionInterface $con)
    {
        $sql = 'SELECT `id_servicerendu`, `id_membre`, `montant`, `date_enregistrement`, `date_debut`, `date_fin`, `origine`, `premier`, `dernier`, `id_entite`, `id_prestation`, `regle`, `comptabilise`, `observation`, `id_tva`, `quantite`, `created_at`, `updated_at` FROM `asso_servicerendus` WHERE `id_servicerendu` = :p0';
        try {
            $stmt = $con->prepare($sql);
            $stmt->bindValue(':p0', $key, PDO::PARAM_INT);
            $stmt->execute();
        } catch (Exception $e) {
            Propel::log($e->getMessage(), Propel::LOG_ERR);
            throw new PropelException(sprintf('Unable to execute SELECT statement [%s]', $sql), 0, $e);
        }
        $obj = null;
        if ($row = $stmt->fetch(\PDO::FETCH_NUM)) {
            /** @var ChildServicerendu $obj */
            $obj = new ChildServicerendu();
            $obj->hydrate($row);
            ServicerenduTableMap::addInstanceToPool($obj, null === $key || is_scalar($key) || is_callable([$key, '__toString']) ? (string) $key : $key);
        }
        $stmt->closeCursor();

        return $obj;
    }

    /**
     * Find object by primary key.
     *
     * @param     mixed $key Primary key to use for the query
     * @param     ConnectionInterface $con A connection object
     *
     * @return ChildServicerendu|array|mixed the result, formatted by the current formatter
     */
    protected function findPkComplex($key, ConnectionInterface $con)
    {
        // As the query uses a PK condition, no limit(1) is necessary.
        $criteria = $this->isKeepQuery() ? clone $this : $this;
        $dataFetcher = $criteria
            ->filterByPrimaryKey($key)
            ->doSelect($con);

        return $criteria->getFormatter()->init($criteria)->formatOne($dataFetcher);
    }

    /**
     * Find objects by primary key
     * <code>
     * $objs = $c->findPks(array(12, 56, 832), $con);
     * </code>
     * @param     array $keys Primary keys to use for the query
     * @param     ConnectionInterface $con an optional connection object
     *
     * @return ObjectCollection|array|mixed the list of results, formatted by the current formatter
     */
    public function findPks($keys, ConnectionInterface $con = null)
    {
        if (null === $con) {
            $con = Propel::getServiceContainer()->getReadConnection($this->getDbName());
        }
        $this->basePreSelect($con);
        $criteria = $this->isKeepQuery() ? clone $this : $this;
        $dataFetcher = $criteria
            ->filterByPrimaryKeys($keys)
            ->doSelect($con);

        return $criteria->getFormatter()->init($criteria)->format($dataFetcher);
    }

    /**
     * Filter the query by primary key
     *
     * @param     mixed $key Primary key to use for the query
     *
     * @return $this|ChildServicerenduQuery The current query, for fluid interface
     */
    public function filterByPrimaryKey($key)
    {

        return $this->addUsingAlias(ServicerenduTableMap::COL_ID_SERVICERENDU, $key, Criteria::EQUAL);
    }

    /**
     * Filter the query by a list of primary keys
     *
     * @param     array $keys The list of primary key to use for the query
     *
     * @return $this|ChildServicerenduQuery The current query, for fluid interface
     */
    public function filterByPrimaryKeys($keys)
    {

        return $this->addUsingAlias(ServicerenduTableMap::COL_ID_SERVICERENDU, $keys, Criteria::IN);
    }

    /**
     * Filter the query on the id_servicerendu column
     *
     * Example usage:
     * <code>
     * $query->filterByIdServicerendu(1234); // WHERE id_servicerendu = 1234
     * $query->filterByIdServicerendu(array(12, 34)); // WHERE id_servicerendu IN (12, 34)
     * $query->filterByIdServicerendu(array('min' => 12)); // WHERE id_servicerendu > 12
     * </code>
     *
     * @param     mixed $idServicerendu The value to use as filter.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildServicerenduQuery The current query, for fluid interface
     */
    public function filterByIdServicerendu($idServicerendu = null, $comparison = null)
    {
        if (is_array($idServicerendu)) {
            $useMinMax = false;
            if (isset($idServicerendu['min'])) {
                $this->addUsingAlias(ServicerenduTableMap::COL_ID_SERVICERENDU, $idServicerendu['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($idServicerendu['max'])) {
                $this->addUsingAlias(ServicerenduTableMap::COL_ID_SERVICERENDU, $idServicerendu['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(ServicerenduTableMap::COL_ID_SERVICERENDU, $idServicerendu, $comparison);
    }

    /**
     * Filter the query on the id_membre column
     *
     * Example usage:
     * <code>
     * $query->filterByIdMembre(1234); // WHERE id_membre = 1234
     * $query->filterByIdMembre(array(12, 34)); // WHERE id_membre IN (12, 34)
     * $query->filterByIdMembre(array('min' => 12)); // WHERE id_membre > 12
     * </code>
     *
     * @see       filterByMembre()
     *
     * @param     mixed $idMembre The value to use as filter.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildServicerenduQuery The current query, for fluid interface
     */
    public function filterByIdMembre($idMembre = null, $comparison = null)
    {
        if (is_array($idMembre)) {
            $useMinMax = false;
            if (isset($idMembre['min'])) {
                $this->addUsingAlias(ServicerenduTableMap::COL_ID_MEMBRE, $idMembre['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($idMembre['max'])) {
                $this->addUsingAlias(ServicerenduTableMap::COL_ID_MEMBRE, $idMembre['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(ServicerenduTableMap::COL_ID_MEMBRE, $idMembre, $comparison);
    }

    /**
     * Filter the query on the montant column
     *
     * Example usage:
     * <code>
     * $query->filterByMontant(1234); // WHERE montant = 1234
     * $query->filterByMontant(array(12, 34)); // WHERE montant IN (12, 34)
     * $query->filterByMontant(array('min' => 12)); // WHERE montant > 12
     * </code>
     *
     * @param     mixed $montant The value to use as filter.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildServicerenduQuery The current query, for fluid interface
     */
    public function filterByMontant($montant = null, $comparison = null)
    {
        if (is_array($montant)) {
            $useMinMax = false;
            if (isset($montant['min'])) {
                $this->addUsingAlias(ServicerenduTableMap::COL_MONTANT, $montant['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($montant['max'])) {
                $this->addUsingAlias(ServicerenduTableMap::COL_MONTANT, $montant['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(ServicerenduTableMap::COL_MONTANT, $montant, $comparison);
    }

    /**
     * Filter the query on the date_enregistrement column
     *
     * Example usage:
     * <code>
     * $query->filterByDateEnregistrement('2011-03-14'); // WHERE date_enregistrement = '2011-03-14'
     * $query->filterByDateEnregistrement('now'); // WHERE date_enregistrement = '2011-03-14'
     * $query->filterByDateEnregistrement(array('max' => 'yesterday')); // WHERE date_enregistrement > '2011-03-13'
     * </code>
     *
     * @param     mixed $dateEnregistrement The value to use as filter.
     *              Values can be integers (unix timestamps), DateTime objects, or strings.
     *              Empty strings are treated as NULL.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildServicerenduQuery The current query, for fluid interface
     */
    public function filterByDateEnregistrement($dateEnregistrement = null, $comparison = null)
    {
        if (is_array($dateEnregistrement)) {
            $useMinMax = false;
            if (isset($dateEnregistrement['min'])) {
                $this->addUsingAlias(ServicerenduTableMap::COL_DATE_ENREGISTREMENT, $dateEnregistrement['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($dateEnregistrement['max'])) {
                $this->addUsingAlias(ServicerenduTableMap::COL_DATE_ENREGISTREMENT, $dateEnregistrement['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(ServicerenduTableMap::COL_DATE_ENREGISTREMENT, $dateEnregistrement, $comparison);
    }

    /**
     * Filter the query on the date_debut column
     *
     * Example usage:
     * <code>
     * $query->filterByDateDebut('2011-03-14'); // WHERE date_debut = '2011-03-14'
     * $query->filterByDateDebut('now'); // WHERE date_debut = '2011-03-14'
     * $query->filterByDateDebut(array('max' => 'yesterday')); // WHERE date_debut > '2011-03-13'
     * </code>
     *
     * @param     mixed $dateDebut The value to use as filter.
     *              Values can be integers (unix timestamps), DateTime objects, or strings.
     *              Empty strings are treated as NULL.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildServicerenduQuery The current query, for fluid interface
     */
    public function filterByDateDebut($dateDebut = null, $comparison = null)
    {
        if (is_array($dateDebut)) {
            $useMinMax = false;
            if (isset($dateDebut['min'])) {
                $this->addUsingAlias(ServicerenduTableMap::COL_DATE_DEBUT, $dateDebut['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($dateDebut['max'])) {
                $this->addUsingAlias(ServicerenduTableMap::COL_DATE_DEBUT, $dateDebut['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(ServicerenduTableMap::COL_DATE_DEBUT, $dateDebut, $comparison);
    }

    /**
     * Filter the query on the date_fin column
     *
     * Example usage:
     * <code>
     * $query->filterByDateFin('2011-03-14'); // WHERE date_fin = '2011-03-14'
     * $query->filterByDateFin('now'); // WHERE date_fin = '2011-03-14'
     * $query->filterByDateFin(array('max' => 'yesterday')); // WHERE date_fin > '2011-03-13'
     * </code>
     *
     * @param     mixed $dateFin The value to use as filter.
     *              Values can be integers (unix timestamps), DateTime objects, or strings.
     *              Empty strings are treated as NULL.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildServicerenduQuery The current query, for fluid interface
     */
    public function filterByDateFin($dateFin = null, $comparison = null)
    {
        if (is_array($dateFin)) {
            $useMinMax = false;
            if (isset($dateFin['min'])) {
                $this->addUsingAlias(ServicerenduTableMap::COL_DATE_FIN, $dateFin['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($dateFin['max'])) {
                $this->addUsingAlias(ServicerenduTableMap::COL_DATE_FIN, $dateFin['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(ServicerenduTableMap::COL_DATE_FIN, $dateFin, $comparison);
    }

    /**
     * Filter the query on the origine column
     *
     * Example usage:
     * <code>
     * $query->filterByOrigine(1234); // WHERE origine = 1234
     * $query->filterByOrigine(array(12, 34)); // WHERE origine IN (12, 34)
     * $query->filterByOrigine(array('min' => 12)); // WHERE origine > 12
     * </code>
     *
     * @see       filterByServicerenduRelatedByOrigine()
     *
     * @param     mixed $origine The value to use as filter.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildServicerenduQuery The current query, for fluid interface
     */
    public function filterByOrigine($origine = null, $comparison = null)
    {
        if (is_array($origine)) {
            $useMinMax = false;
            if (isset($origine['min'])) {
                $this->addUsingAlias(ServicerenduTableMap::COL_ORIGINE, $origine['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($origine['max'])) {
                $this->addUsingAlias(ServicerenduTableMap::COL_ORIGINE, $origine['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(ServicerenduTableMap::COL_ORIGINE, $origine, $comparison);
    }

    /**
     * Filter the query on the premier column
     *
     * Example usage:
     * <code>
     * $query->filterByPremier(1234); // WHERE premier = 1234
     * $query->filterByPremier(array(12, 34)); // WHERE premier IN (12, 34)
     * $query->filterByPremier(array('min' => 12)); // WHERE premier > 12
     * </code>
     *
     * @param     mixed $premier The value to use as filter.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildServicerenduQuery The current query, for fluid interface
     */
    public function filterByPremier($premier = null, $comparison = null)
    {
        if (is_array($premier)) {
            $useMinMax = false;
            if (isset($premier['min'])) {
                $this->addUsingAlias(ServicerenduTableMap::COL_PREMIER, $premier['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($premier['max'])) {
                $this->addUsingAlias(ServicerenduTableMap::COL_PREMIER, $premier['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(ServicerenduTableMap::COL_PREMIER, $premier, $comparison);
    }

    /**
     * Filter the query on the dernier column
     *
     * Example usage:
     * <code>
     * $query->filterByDernier(1234); // WHERE dernier = 1234
     * $query->filterByDernier(array(12, 34)); // WHERE dernier IN (12, 34)
     * $query->filterByDernier(array('min' => 12)); // WHERE dernier > 12
     * </code>
     *
     * @param     mixed $dernier The value to use as filter.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildServicerenduQuery The current query, for fluid interface
     */
    public function filterByDernier($dernier = null, $comparison = null)
    {
        if (is_array($dernier)) {
            $useMinMax = false;
            if (isset($dernier['min'])) {
                $this->addUsingAlias(ServicerenduTableMap::COL_DERNIER, $dernier['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($dernier['max'])) {
                $this->addUsingAlias(ServicerenduTableMap::COL_DERNIER, $dernier['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(ServicerenduTableMap::COL_DERNIER, $dernier, $comparison);
    }

    /**
     * Filter the query on the id_entite column
     *
     * Example usage:
     * <code>
     * $query->filterByIdEntite(1234); // WHERE id_entite = 1234
     * $query->filterByIdEntite(array(12, 34)); // WHERE id_entite IN (12, 34)
     * $query->filterByIdEntite(array('min' => 12)); // WHERE id_entite > 12
     * </code>
     *
     * @see       filterByEntite()
     *
     * @param     mixed $idEntite The value to use as filter.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildServicerenduQuery The current query, for fluid interface
     */
    public function filterByIdEntite($idEntite = null, $comparison = null)
    {
        if (is_array($idEntite)) {
            $useMinMax = false;
            if (isset($idEntite['min'])) {
                $this->addUsingAlias(ServicerenduTableMap::COL_ID_ENTITE, $idEntite['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($idEntite['max'])) {
                $this->addUsingAlias(ServicerenduTableMap::COL_ID_ENTITE, $idEntite['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(ServicerenduTableMap::COL_ID_ENTITE, $idEntite, $comparison);
    }

    /**
     * Filter the query on the id_prestation column
     *
     * Example usage:
     * <code>
     * $query->filterByIdPrestation(1234); // WHERE id_prestation = 1234
     * $query->filterByIdPrestation(array(12, 34)); // WHERE id_prestation IN (12, 34)
     * $query->filterByIdPrestation(array('min' => 12)); // WHERE id_prestation > 12
     * </code>
     *
     * @see       filterByPrestation()
     *
     * @param     mixed $idPrestation The value to use as filter.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildServicerenduQuery The current query, for fluid interface
     */
    public function filterByIdPrestation($idPrestation = null, $comparison = null)
    {
        if (is_array($idPrestation)) {
            $useMinMax = false;
            if (isset($idPrestation['min'])) {
                $this->addUsingAlias(ServicerenduTableMap::COL_ID_PRESTATION, $idPrestation['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($idPrestation['max'])) {
                $this->addUsingAlias(ServicerenduTableMap::COL_ID_PRESTATION, $idPrestation['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(ServicerenduTableMap::COL_ID_PRESTATION, $idPrestation, $comparison);
    }

    /**
     * Filter the query on the regle column
     *
     * Example usage:
     * <code>
     * $query->filterByRegle(1234); // WHERE regle = 1234
     * $query->filterByRegle(array(12, 34)); // WHERE regle IN (12, 34)
     * $query->filterByRegle(array('min' => 12)); // WHERE regle > 12
     * </code>
     *
     * @param     mixed $regle The value to use as filter.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildServicerenduQuery The current query, for fluid interface
     */
    public function filterByRegle($regle = null, $comparison = null)
    {
        if (is_array($regle)) {
            $useMinMax = false;
            if (isset($regle['min'])) {
                $this->addUsingAlias(ServicerenduTableMap::COL_REGLE, $regle['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($regle['max'])) {
                $this->addUsingAlias(ServicerenduTableMap::COL_REGLE, $regle['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(ServicerenduTableMap::COL_REGLE, $regle, $comparison);
    }

    /**
     * Filter the query on the comptabilise column
     *
     * Example usage:
     * <code>
     * $query->filterByComptabilise(true); // WHERE comptabilise = true
     * $query->filterByComptabilise('yes'); // WHERE comptabilise = true
     * </code>
     *
     * @param     boolean|string $comptabilise The value to use as filter.
     *              Non-boolean arguments are converted using the following rules:
     *                * 1, '1', 'true',  'on',  and 'yes' are converted to boolean true
     *                * 0, '0', 'false', 'off', and 'no'  are converted to boolean false
     *              Check on string values is case insensitive (so 'FaLsE' is seen as 'false').
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildServicerenduQuery The current query, for fluid interface
     */
    public function filterByComptabilise($comptabilise = null, $comparison = null)
    {
        if (is_string($comptabilise)) {
            $comptabilise = in_array(strtolower($comptabilise), array('false', 'off', '-', 'no', 'n', '0', '')) ? false : true;
        }

        return $this->addUsingAlias(ServicerenduTableMap::COL_COMPTABILISE, $comptabilise, $comparison);
    }

    /**
     * Filter the query on the observation column
     *
     * Example usage:
     * <code>
     * $query->filterByObservation('fooValue');   // WHERE observation = 'fooValue'
     * $query->filterByObservation('%fooValue%', Criteria::LIKE); // WHERE observation LIKE '%fooValue%'
     * </code>
     *
     * @param     string $observation The value to use as filter.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildServicerenduQuery The current query, for fluid interface
     */
    public function filterByObservation($observation = null, $comparison = null)
    {
        if (null === $comparison) {
            if (is_array($observation)) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(ServicerenduTableMap::COL_OBSERVATION, $observation, $comparison);
    }

    /**
     * Filter the query on the id_tva column
     *
     * Example usage:
     * <code>
     * $query->filterByIdTva(1234); // WHERE id_tva = 1234
     * $query->filterByIdTva(array(12, 34)); // WHERE id_tva IN (12, 34)
     * $query->filterByIdTva(array('min' => 12)); // WHERE id_tva > 12
     * </code>
     *
     * @param     mixed $idTva The value to use as filter.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildServicerenduQuery The current query, for fluid interface
     */
    public function filterByIdTva($idTva = null, $comparison = null)
    {
        if (is_array($idTva)) {
            $useMinMax = false;
            if (isset($idTva['min'])) {
                $this->addUsingAlias(ServicerenduTableMap::COL_ID_TVA, $idTva['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($idTva['max'])) {
                $this->addUsingAlias(ServicerenduTableMap::COL_ID_TVA, $idTva['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(ServicerenduTableMap::COL_ID_TVA, $idTva, $comparison);
    }

    /**
     * Filter the query on the quantite column
     *
     * Example usage:
     * <code>
     * $query->filterByQuantite(1234); // WHERE quantite = 1234
     * $query->filterByQuantite(array(12, 34)); // WHERE quantite IN (12, 34)
     * $query->filterByQuantite(array('min' => 12)); // WHERE quantite > 12
     * </code>
     *
     * @param     mixed $quantite The value to use as filter.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildServicerenduQuery The current query, for fluid interface
     */
    public function filterByQuantite($quantite = null, $comparison = null)
    {
        if (is_array($quantite)) {
            $useMinMax = false;
            if (isset($quantite['min'])) {
                $this->addUsingAlias(ServicerenduTableMap::COL_QUANTITE, $quantite['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($quantite['max'])) {
                $this->addUsingAlias(ServicerenduTableMap::COL_QUANTITE, $quantite['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(ServicerenduTableMap::COL_QUANTITE, $quantite, $comparison);
    }

    /**
     * Filter the query on the created_at column
     *
     * Example usage:
     * <code>
     * $query->filterByCreatedAt('2011-03-14'); // WHERE created_at = '2011-03-14'
     * $query->filterByCreatedAt('now'); // WHERE created_at = '2011-03-14'
     * $query->filterByCreatedAt(array('max' => 'yesterday')); // WHERE created_at > '2011-03-13'
     * </code>
     *
     * @param     mixed $createdAt The value to use as filter.
     *              Values can be integers (unix timestamps), DateTime objects, or strings.
     *              Empty strings are treated as NULL.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildServicerenduQuery The current query, for fluid interface
     */
    public function filterByCreatedAt($createdAt = null, $comparison = null)
    {
        if (is_array($createdAt)) {
            $useMinMax = false;
            if (isset($createdAt['min'])) {
                $this->addUsingAlias(ServicerenduTableMap::COL_CREATED_AT, $createdAt['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($createdAt['max'])) {
                $this->addUsingAlias(ServicerenduTableMap::COL_CREATED_AT, $createdAt['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(ServicerenduTableMap::COL_CREATED_AT, $createdAt, $comparison);
    }

    /**
     * Filter the query on the updated_at column
     *
     * Example usage:
     * <code>
     * $query->filterByUpdatedAt('2011-03-14'); // WHERE updated_at = '2011-03-14'
     * $query->filterByUpdatedAt('now'); // WHERE updated_at = '2011-03-14'
     * $query->filterByUpdatedAt(array('max' => 'yesterday')); // WHERE updated_at > '2011-03-13'
     * </code>
     *
     * @param     mixed $updatedAt The value to use as filter.
     *              Values can be integers (unix timestamps), DateTime objects, or strings.
     *              Empty strings are treated as NULL.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildServicerenduQuery The current query, for fluid interface
     */
    public function filterByUpdatedAt($updatedAt = null, $comparison = null)
    {
        if (is_array($updatedAt)) {
            $useMinMax = false;
            if (isset($updatedAt['min'])) {
                $this->addUsingAlias(ServicerenduTableMap::COL_UPDATED_AT, $updatedAt['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($updatedAt['max'])) {
                $this->addUsingAlias(ServicerenduTableMap::COL_UPDATED_AT, $updatedAt['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(ServicerenduTableMap::COL_UPDATED_AT, $updatedAt, $comparison);
    }

    /**
     * Filter the query by a related \Prestation object
     *
     * @param \Prestation|ObjectCollection $prestation The related object(s) to use as filter
     * @param string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @throws \Propel\Runtime\Exception\PropelException
     *
     * @return ChildServicerenduQuery The current query, for fluid interface
     */
    public function filterByPrestation($prestation, $comparison = null)
    {
        if ($prestation instanceof \Prestation) {
            return $this
                ->addUsingAlias(ServicerenduTableMap::COL_ID_PRESTATION, $prestation->getIdPrestation(), $comparison);
        } elseif ($prestation instanceof ObjectCollection) {
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }

            return $this
                ->addUsingAlias(ServicerenduTableMap::COL_ID_PRESTATION, $prestation->toKeyValue('PrimaryKey', 'IdPrestation'), $comparison);
        } else {
            throw new PropelException('filterByPrestation() only accepts arguments of type \Prestation or Collection');
        }
    }

    /**
     * Adds a JOIN clause to the query using the Prestation relation
     *
     * @param     string $relationAlias optional alias for the relation
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return $this|ChildServicerenduQuery The current query, for fluid interface
     */
    public function joinPrestation($relationAlias = null, $joinType = Criteria::INNER_JOIN)
    {
        $tableMap = $this->getTableMap();
        $relationMap = $tableMap->getRelation('Prestation');

        // create a ModelJoin object for this join
        $join = new ModelJoin();
        $join->setJoinType($joinType);
        $join->setRelationMap($relationMap, $this->useAliasInSQL ? $this->getModelAlias() : null, $relationAlias);
        if ($previousJoin = $this->getPreviousJoin()) {
            $join->setPreviousJoin($previousJoin);
        }

        // add the ModelJoin to the current object
        if ($relationAlias) {
            $this->addAlias($relationAlias, $relationMap->getRightTable()->getName());
            $this->addJoinObject($join, $relationAlias);
        } else {
            $this->addJoinObject($join, 'Prestation');
        }

        return $this;
    }

    /**
     * Use the Prestation relation Prestation object
     *
     * @see useQuery()
     *
     * @param     string $relationAlias optional alias for the relation,
     *                                   to be used as main alias in the secondary query
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return \PrestationQuery A secondary query class using the current class as primary query
     */
    public function usePrestationQuery($relationAlias = null, $joinType = Criteria::INNER_JOIN)
    {
        return $this
            ->joinPrestation($relationAlias, $joinType)
            ->useQuery($relationAlias ? $relationAlias : 'Prestation', '\PrestationQuery');
    }

    /**
     * Filter the query by a related \Entite object
     *
     * @param \Entite|ObjectCollection $entite The related object(s) to use as filter
     * @param string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @throws \Propel\Runtime\Exception\PropelException
     *
     * @return ChildServicerenduQuery The current query, for fluid interface
     */
    public function filterByEntite($entite, $comparison = null)
    {
        if ($entite instanceof \Entite) {
            return $this
                ->addUsingAlias(ServicerenduTableMap::COL_ID_ENTITE, $entite->getIdEntite(), $comparison);
        } elseif ($entite instanceof ObjectCollection) {
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }

            return $this
                ->addUsingAlias(ServicerenduTableMap::COL_ID_ENTITE, $entite->toKeyValue('PrimaryKey', 'IdEntite'), $comparison);
        } else {
            throw new PropelException('filterByEntite() only accepts arguments of type \Entite or Collection');
        }
    }

    /**
     * Adds a JOIN clause to the query using the Entite relation
     *
     * @param     string $relationAlias optional alias for the relation
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return $this|ChildServicerenduQuery The current query, for fluid interface
     */
    public function joinEntite($relationAlias = null, $joinType = Criteria::INNER_JOIN)
    {
        $tableMap = $this->getTableMap();
        $relationMap = $tableMap->getRelation('Entite');

        // create a ModelJoin object for this join
        $join = new ModelJoin();
        $join->setJoinType($joinType);
        $join->setRelationMap($relationMap, $this->useAliasInSQL ? $this->getModelAlias() : null, $relationAlias);
        if ($previousJoin = $this->getPreviousJoin()) {
            $join->setPreviousJoin($previousJoin);
        }

        // add the ModelJoin to the current object
        if ($relationAlias) {
            $this->addAlias($relationAlias, $relationMap->getRightTable()->getName());
            $this->addJoinObject($join, $relationAlias);
        } else {
            $this->addJoinObject($join, 'Entite');
        }

        return $this;
    }

    /**
     * Use the Entite relation Entite object
     *
     * @see useQuery()
     *
     * @param     string $relationAlias optional alias for the relation,
     *                                   to be used as main alias in the secondary query
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return \EntiteQuery A secondary query class using the current class as primary query
     */
    public function useEntiteQuery($relationAlias = null, $joinType = Criteria::INNER_JOIN)
    {
        return $this
            ->joinEntite($relationAlias, $joinType)
            ->useQuery($relationAlias ? $relationAlias : 'Entite', '\EntiteQuery');
    }

    /**
     * Filter the query by a related \Membre object
     *
     * @param \Membre|ObjectCollection $membre The related object(s) to use as filter
     * @param string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @throws \Propel\Runtime\Exception\PropelException
     *
     * @return ChildServicerenduQuery The current query, for fluid interface
     */
    public function filterByMembre($membre, $comparison = null)
    {
        if ($membre instanceof \Membre) {
            return $this
                ->addUsingAlias(ServicerenduTableMap::COL_ID_MEMBRE, $membre->getIdMembre(), $comparison);
        } elseif ($membre instanceof ObjectCollection) {
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }

            return $this
                ->addUsingAlias(ServicerenduTableMap::COL_ID_MEMBRE, $membre->toKeyValue('PrimaryKey', 'IdMembre'), $comparison);
        } else {
            throw new PropelException('filterByMembre() only accepts arguments of type \Membre or Collection');
        }
    }

    /**
     * Adds a JOIN clause to the query using the Membre relation
     *
     * @param     string $relationAlias optional alias for the relation
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return $this|ChildServicerenduQuery The current query, for fluid interface
     */
    public function joinMembre($relationAlias = null, $joinType = Criteria::LEFT_JOIN)
    {
        $tableMap = $this->getTableMap();
        $relationMap = $tableMap->getRelation('Membre');

        // create a ModelJoin object for this join
        $join = new ModelJoin();
        $join->setJoinType($joinType);
        $join->setRelationMap($relationMap, $this->useAliasInSQL ? $this->getModelAlias() : null, $relationAlias);
        if ($previousJoin = $this->getPreviousJoin()) {
            $join->setPreviousJoin($previousJoin);
        }

        // add the ModelJoin to the current object
        if ($relationAlias) {
            $this->addAlias($relationAlias, $relationMap->getRightTable()->getName());
            $this->addJoinObject($join, $relationAlias);
        } else {
            $this->addJoinObject($join, 'Membre');
        }

        return $this;
    }

    /**
     * Use the Membre relation Membre object
     *
     * @see useQuery()
     *
     * @param     string $relationAlias optional alias for the relation,
     *                                   to be used as main alias in the secondary query
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return \MembreQuery A secondary query class using the current class as primary query
     */
    public function useMembreQuery($relationAlias = null, $joinType = Criteria::LEFT_JOIN)
    {
        return $this
            ->joinMembre($relationAlias, $joinType)
            ->useQuery($relationAlias ? $relationAlias : 'Membre', '\MembreQuery');
    }

    /**
     * Filter the query by a related \Servicerendu object
     *
     * @param \Servicerendu|ObjectCollection $servicerendu The related object(s) to use as filter
     * @param string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @throws \Propel\Runtime\Exception\PropelException
     *
     * @return ChildServicerenduQuery The current query, for fluid interface
     */
    public function filterByServicerenduRelatedByOrigine($servicerendu, $comparison = null)
    {
        if ($servicerendu instanceof \Servicerendu) {
            return $this
                ->addUsingAlias(ServicerenduTableMap::COL_ORIGINE, $servicerendu->getIdServicerendu(), $comparison);
        } elseif ($servicerendu instanceof ObjectCollection) {
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }

            return $this
                ->addUsingAlias(ServicerenduTableMap::COL_ORIGINE, $servicerendu->toKeyValue('PrimaryKey', 'IdServicerendu'), $comparison);
        } else {
            throw new PropelException('filterByServicerenduRelatedByOrigine() only accepts arguments of type \Servicerendu or Collection');
        }
    }

    /**
     * Adds a JOIN clause to the query using the ServicerenduRelatedByOrigine relation
     *
     * @param     string $relationAlias optional alias for the relation
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return $this|ChildServicerenduQuery The current query, for fluid interface
     */
    public function joinServicerenduRelatedByOrigine($relationAlias = null, $joinType = Criteria::LEFT_JOIN)
    {
        $tableMap = $this->getTableMap();
        $relationMap = $tableMap->getRelation('ServicerenduRelatedByOrigine');

        // create a ModelJoin object for this join
        $join = new ModelJoin();
        $join->setJoinType($joinType);
        $join->setRelationMap($relationMap, $this->useAliasInSQL ? $this->getModelAlias() : null, $relationAlias);
        if ($previousJoin = $this->getPreviousJoin()) {
            $join->setPreviousJoin($previousJoin);
        }

        // add the ModelJoin to the current object
        if ($relationAlias) {
            $this->addAlias($relationAlias, $relationMap->getRightTable()->getName());
            $this->addJoinObject($join, $relationAlias);
        } else {
            $this->addJoinObject($join, 'ServicerenduRelatedByOrigine');
        }

        return $this;
    }

    /**
     * Use the ServicerenduRelatedByOrigine relation Servicerendu object
     *
     * @see useQuery()
     *
     * @param     string $relationAlias optional alias for the relation,
     *                                   to be used as main alias in the secondary query
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return \ServicerenduQuery A secondary query class using the current class as primary query
     */
    public function useServicerenduRelatedByOrigineQuery($relationAlias = null, $joinType = Criteria::LEFT_JOIN)
    {
        return $this
            ->joinServicerenduRelatedByOrigine($relationAlias, $joinType)
            ->useQuery($relationAlias ? $relationAlias : 'ServicerenduRelatedByOrigine', '\ServicerenduQuery');
    }

    /**
     * Filter the query by a related \Servicepaiement object
     *
     * @param \Servicepaiement|ObjectCollection $servicepaiement the related object to use as filter
     * @param string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return ChildServicerenduQuery The current query, for fluid interface
     */
    public function filterByServicepaiement($servicepaiement, $comparison = null)
    {
        if ($servicepaiement instanceof \Servicepaiement) {
            return $this
                ->addUsingAlias(ServicerenduTableMap::COL_ID_SERVICERENDU, $servicepaiement->getIdServicerendu(), $comparison);
        } elseif ($servicepaiement instanceof ObjectCollection) {
            return $this
                ->useServicepaiementQuery()
                ->filterByPrimaryKeys($servicepaiement->getPrimaryKeys())
                ->endUse();
        } else {
            throw new PropelException('filterByServicepaiement() only accepts arguments of type \Servicepaiement or Collection');
        }
    }

    /**
     * Adds a JOIN clause to the query using the Servicepaiement relation
     *
     * @param     string $relationAlias optional alias for the relation
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return $this|ChildServicerenduQuery The current query, for fluid interface
     */
    public function joinServicepaiement($relationAlias = null, $joinType = Criteria::INNER_JOIN)
    {
        $tableMap = $this->getTableMap();
        $relationMap = $tableMap->getRelation('Servicepaiement');

        // create a ModelJoin object for this join
        $join = new ModelJoin();
        $join->setJoinType($joinType);
        $join->setRelationMap($relationMap, $this->useAliasInSQL ? $this->getModelAlias() : null, $relationAlias);
        if ($previousJoin = $this->getPreviousJoin()) {
            $join->setPreviousJoin($previousJoin);
        }

        // add the ModelJoin to the current object
        if ($relationAlias) {
            $this->addAlias($relationAlias, $relationMap->getRightTable()->getName());
            $this->addJoinObject($join, $relationAlias);
        } else {
            $this->addJoinObject($join, 'Servicepaiement');
        }

        return $this;
    }

    /**
     * Use the Servicepaiement relation Servicepaiement object
     *
     * @see useQuery()
     *
     * @param     string $relationAlias optional alias for the relation,
     *                                   to be used as main alias in the secondary query
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return \ServicepaiementQuery A secondary query class using the current class as primary query
     */
    public function useServicepaiementQuery($relationAlias = null, $joinType = Criteria::INNER_JOIN)
    {
        return $this
            ->joinServicepaiement($relationAlias, $joinType)
            ->useQuery($relationAlias ? $relationAlias : 'Servicepaiement', '\ServicepaiementQuery');
    }

    /**
     * Filter the query by a related \Servicerendu object
     *
     * @param \Servicerendu|ObjectCollection $servicerendu the related object to use as filter
     * @param string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return ChildServicerenduQuery The current query, for fluid interface
     */
    public function filterByServicerenduRelatedByIdServicerendu($servicerendu, $comparison = null)
    {
        if ($servicerendu instanceof \Servicerendu) {
            return $this
                ->addUsingAlias(ServicerenduTableMap::COL_ID_SERVICERENDU, $servicerendu->getOrigine(), $comparison);
        } elseif ($servicerendu instanceof ObjectCollection) {
            return $this
                ->useServicerenduRelatedByIdServicerenduQuery()
                ->filterByPrimaryKeys($servicerendu->getPrimaryKeys())
                ->endUse();
        } else {
            throw new PropelException('filterByServicerenduRelatedByIdServicerendu() only accepts arguments of type \Servicerendu or Collection');
        }
    }

    /**
     * Adds a JOIN clause to the query using the ServicerenduRelatedByIdServicerendu relation
     *
     * @param     string $relationAlias optional alias for the relation
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return $this|ChildServicerenduQuery The current query, for fluid interface
     */
    public function joinServicerenduRelatedByIdServicerendu($relationAlias = null, $joinType = Criteria::LEFT_JOIN)
    {
        $tableMap = $this->getTableMap();
        $relationMap = $tableMap->getRelation('ServicerenduRelatedByIdServicerendu');

        // create a ModelJoin object for this join
        $join = new ModelJoin();
        $join->setJoinType($joinType);
        $join->setRelationMap($relationMap, $this->useAliasInSQL ? $this->getModelAlias() : null, $relationAlias);
        if ($previousJoin = $this->getPreviousJoin()) {
            $join->setPreviousJoin($previousJoin);
        }

        // add the ModelJoin to the current object
        if ($relationAlias) {
            $this->addAlias($relationAlias, $relationMap->getRightTable()->getName());
            $this->addJoinObject($join, $relationAlias);
        } else {
            $this->addJoinObject($join, 'ServicerenduRelatedByIdServicerendu');
        }

        return $this;
    }

    /**
     * Use the ServicerenduRelatedByIdServicerendu relation Servicerendu object
     *
     * @see useQuery()
     *
     * @param     string $relationAlias optional alias for the relation,
     *                                   to be used as main alias in the secondary query
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return \ServicerenduQuery A secondary query class using the current class as primary query
     */
    public function useServicerenduRelatedByIdServicerenduQuery($relationAlias = null, $joinType = Criteria::LEFT_JOIN)
    {
        return $this
            ->joinServicerenduRelatedByIdServicerendu($relationAlias, $joinType)
            ->useQuery($relationAlias ? $relationAlias : 'ServicerenduRelatedByIdServicerendu', '\ServicerenduQuery');
    }

    /**
     * Filter the query by a related \ServicerenduIndividu object
     *
     * @param \ServicerenduIndividu|ObjectCollection $servicerenduIndividu the related object to use as filter
     * @param string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return ChildServicerenduQuery The current query, for fluid interface
     */
    public function filterByServicerenduIndividu($servicerenduIndividu, $comparison = null)
    {
        if ($servicerenduIndividu instanceof \ServicerenduIndividu) {
            return $this
                ->addUsingAlias(ServicerenduTableMap::COL_ID_SERVICERENDU, $servicerenduIndividu->getIdServicerendu(), $comparison);
        } elseif ($servicerenduIndividu instanceof ObjectCollection) {
            return $this
                ->useServicerenduIndividuQuery()
                ->filterByPrimaryKeys($servicerenduIndividu->getPrimaryKeys())
                ->endUse();
        } else {
            throw new PropelException('filterByServicerenduIndividu() only accepts arguments of type \ServicerenduIndividu or Collection');
        }
    }

    /**
     * Adds a JOIN clause to the query using the ServicerenduIndividu relation
     *
     * @param     string $relationAlias optional alias for the relation
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return $this|ChildServicerenduQuery The current query, for fluid interface
     */
    public function joinServicerenduIndividu($relationAlias = null, $joinType = Criteria::INNER_JOIN)
    {
        $tableMap = $this->getTableMap();
        $relationMap = $tableMap->getRelation('ServicerenduIndividu');

        // create a ModelJoin object for this join
        $join = new ModelJoin();
        $join->setJoinType($joinType);
        $join->setRelationMap($relationMap, $this->useAliasInSQL ? $this->getModelAlias() : null, $relationAlias);
        if ($previousJoin = $this->getPreviousJoin()) {
            $join->setPreviousJoin($previousJoin);
        }

        // add the ModelJoin to the current object
        if ($relationAlias) {
            $this->addAlias($relationAlias, $relationMap->getRightTable()->getName());
            $this->addJoinObject($join, $relationAlias);
        } else {
            $this->addJoinObject($join, 'ServicerenduIndividu');
        }

        return $this;
    }

    /**
     * Use the ServicerenduIndividu relation ServicerenduIndividu object
     *
     * @see useQuery()
     *
     * @param     string $relationAlias optional alias for the relation,
     *                                   to be used as main alias in the secondary query
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return \ServicerenduIndividuQuery A secondary query class using the current class as primary query
     */
    public function useServicerenduIndividuQuery($relationAlias = null, $joinType = Criteria::INNER_JOIN)
    {
        return $this
            ->joinServicerenduIndividu($relationAlias, $joinType)
            ->useQuery($relationAlias ? $relationAlias : 'ServicerenduIndividu', '\ServicerenduIndividuQuery');
    }

    /**
     * Filter the query by a related Individu object
     * using the asso_servicerendus_individus table as cross reference
     *
     * @param Individu $individu the related object to use as filter
     * @param string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return ChildServicerenduQuery The current query, for fluid interface
     */
    public function filterByIndividu($individu, $comparison = Criteria::EQUAL)
    {
        return $this
            ->useServicerenduIndividuQuery()
            ->filterByIndividu($individu, $comparison)
            ->endUse();
    }

    /**
     * Exclude object from result
     *
     * @param   ChildServicerendu $servicerendu Object to remove from the list of results
     *
     * @return $this|ChildServicerenduQuery The current query, for fluid interface
     */
    public function prune($servicerendu = null)
    {
        if ($servicerendu) {
            $this->addUsingAlias(ServicerenduTableMap::COL_ID_SERVICERENDU, $servicerendu->getIdServicerendu(), Criteria::NOT_EQUAL);
        }

        return $this;
    }

    /**
     * Code to execute before every DELETE statement
     *
     * @param     ConnectionInterface $con The connection object used by the query
     */
    protected function basePreDelete(ConnectionInterface $con)
    {
        // archivable behavior

        if ($this->archiveOnDelete) {
            $this->archive($con);
        } else {
            $this->archiveOnDelete = true;
        }


        return $this->preDelete($con);
    }

    /**
     * Deletes all rows from the asso_servicerendus table.
     *
     * @param ConnectionInterface $con the connection to use
     * @return int The number of affected rows (if supported by underlying database driver).
     */
    public function doDeleteAll(ConnectionInterface $con = null)
    {
        if (null === $con) {
            $con = Propel::getServiceContainer()->getWriteConnection(ServicerenduTableMap::DATABASE_NAME);
        }

        // use transaction because $criteria could contain info
        // for more than one table or we could emulating ON DELETE CASCADE, etc.
        return $con->transaction(function () use ($con) {
            $affectedRows = 0; // initialize var to track total num of affected rows
            $affectedRows += parent::doDeleteAll($con);
            // Because this db requires some delete cascade/set null emulation, we have to
            // clear the cached instance *after* the emulation has happened (since
            // instances get re-added by the select statement contained therein).
            ServicerenduTableMap::clearInstancePool();
            ServicerenduTableMap::clearRelatedInstancePool();

            return $affectedRows;
        });
    }

    /**
     * Performs a DELETE on the database based on the current ModelCriteria
     *
     * @param ConnectionInterface $con the connection to use
     * @return int             The number of affected rows (if supported by underlying database driver).  This includes CASCADE-related rows
     *                         if supported by native driver or if emulated using Propel.
     * @throws PropelException Any exceptions caught during processing will be
     *                         rethrown wrapped into a PropelException.
     */
    public function delete(ConnectionInterface $con = null)
    {
        if (null === $con) {
            $con = Propel::getServiceContainer()->getWriteConnection(ServicerenduTableMap::DATABASE_NAME);
        }

        $criteria = $this;

        // Set the correct dbName
        $criteria->setDbName(ServicerenduTableMap::DATABASE_NAME);

        // use transaction because $criteria could contain info
        // for more than one table or we could emulating ON DELETE CASCADE, etc.
        return $con->transaction(function () use ($con, $criteria) {
            $affectedRows = 0; // initialize var to track total num of affected rows

            ServicerenduTableMap::removeInstanceFromPool($criteria);

            $affectedRows += ModelCriteria::delete($con);
            ServicerenduTableMap::clearRelatedInstancePool();

            return $affectedRows;
        });
    }

    // timestampable behavior

    /**
     * Filter by the latest updated
     *
     * @param      int $nbDays Maximum age of the latest update in days
     *
     * @return     $this|ChildServicerenduQuery The current query, for fluid interface
     */
    public function recentlyUpdated($nbDays = 7)
    {
        return $this->addUsingAlias(ServicerenduTableMap::COL_UPDATED_AT, time() - $nbDays * 24 * 60 * 60, Criteria::GREATER_EQUAL);
    }

    /**
     * Order by update date desc
     *
     * @return     $this|ChildServicerenduQuery The current query, for fluid interface
     */
    public function lastUpdatedFirst()
    {
        return $this->addDescendingOrderByColumn(ServicerenduTableMap::COL_UPDATED_AT);
    }

    /**
     * Order by update date asc
     *
     * @return     $this|ChildServicerenduQuery The current query, for fluid interface
     */
    public function firstUpdatedFirst()
    {
        return $this->addAscendingOrderByColumn(ServicerenduTableMap::COL_UPDATED_AT);
    }

    /**
     * Order by create date desc
     *
     * @return     $this|ChildServicerenduQuery The current query, for fluid interface
     */
    public function lastCreatedFirst()
    {
        return $this->addDescendingOrderByColumn(ServicerenduTableMap::COL_CREATED_AT);
    }

    /**
     * Filter by the latest created
     *
     * @param      int $nbDays Maximum age of in days
     *
     * @return     $this|ChildServicerenduQuery The current query, for fluid interface
     */
    public function recentlyCreated($nbDays = 7)
    {
        return $this->addUsingAlias(ServicerenduTableMap::COL_CREATED_AT, time() - $nbDays * 24 * 60 * 60, Criteria::GREATER_EQUAL);
    }

    /**
     * Order by create date asc
     *
     * @return     $this|ChildServicerenduQuery The current query, for fluid interface
     */
    public function firstCreatedFirst()
    {
        return $this->addAscendingOrderByColumn(ServicerenduTableMap::COL_CREATED_AT);
    }

    // archivable behavior

    /**
     * Copy the data of the objects satisfying the query into ChildAssoServicerendusArchive archive objects.
     * The archived objects are then saved.
     * If any of the objects has already been archived, the archived object
     * is updated and not duplicated.
     * Warning: This termination methods issues 2n+1 queries.
     *
     * @param      ConnectionInterface $con    Connection to use.
     * @param      Boolean $useLittleMemory    Whether or not to use OnDemandFormatter to retrieve objects.
     *               Set to false if the identity map matters.
     *               Set to true (default) to use less memory.
     *
     * @return     int the number of archived objects
     */
    public function archive($con = null, $useLittleMemory = true)
    {
        $criteria = clone $this;
        // prepare the query
        $criteria->setWith(array());
        if ($useLittleMemory) {
            $criteria->setFormatter(ModelCriteria::FORMAT_ON_DEMAND);
        }
        if ($con === null) {
            $con = Propel::getServiceContainer()->getWriteConnection(ServicerenduTableMap::DATABASE_NAME);
        }

        return $con->transaction(function () use ($con, $criteria) {
            $totalArchivedObjects = 0;

            // archive all results one by one
            foreach ($criteria->find($con) as $object) {
                $object->archive($con);
                $totalArchivedObjects++;
            }

            return $totalArchivedObjects;
        });
    }

    /**
     * Enable/disable auto-archiving on delete for the next query.
     *
     * @param boolean True if the query must archive deleted objects, false otherwise.
     */
    public function setArchiveOnDelete($archiveOnDelete)
    {
        $this->archiveOnDelete = $archiveOnDelete;
    }

    /**
     * Delete records matching the current query without archiving them.
     *
     * @param      ConnectionInterface $con    Connection to use.
     *
     * @return integer the number of deleted rows
     */
    public function deleteWithoutArchive($con = null)
    {
        $this->archiveOnDelete = false;

        return $this->delete($con);
    }

    /**
     * Delete all records without archiving them.
     *
     * @param      ConnectionInterface $con    Connection to use.
     *
     * @return integer the number of deleted rows
     */
    public function deleteAllWithoutArchive($con = null)
    {
        $this->archiveOnDelete = false;

        return $this->deleteAll($con);
    }

} // ServicerenduQuery
