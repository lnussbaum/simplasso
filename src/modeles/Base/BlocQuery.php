<?php

namespace Base;

use \Bloc as ChildBloc;
use \BlocQuery as ChildBlocQuery;
use \Exception;
use \PDO;
use Map\BlocTableMap;
use Propel\Runtime\Propel;
use Propel\Runtime\ActiveQuery\Criteria;
use Propel\Runtime\ActiveQuery\ModelCriteria;
use Propel\Runtime\ActiveQuery\ModelJoin;
use Propel\Runtime\Collection\ObjectCollection;
use Propel\Runtime\Connection\ConnectionInterface;
use Propel\Runtime\Exception\PropelException;

/**
 * Base class that represents a query for the 'com_blocs' table.
 *
 *
 *
 * @method     ChildBlocQuery orderByIdBloc($order = Criteria::ASC) Order by the id_bloc column
 * @method     ChildBlocQuery orderByNom($order = Criteria::ASC) Order by the nom column
 * @method     ChildBlocQuery orderByValeurs($order = Criteria::ASC) Order by the valeurs column
 * @method     ChildBlocQuery orderByTexte($order = Criteria::ASC) Order by the texte column
 * @method     ChildBlocQuery orderByCss($order = Criteria::ASC) Order by the css column
 * @method     ChildBlocQuery orderByCanal($order = Criteria::ASC) Order by the canal column
 *
 * @method     ChildBlocQuery groupByIdBloc() Group by the id_bloc column
 * @method     ChildBlocQuery groupByNom() Group by the nom column
 * @method     ChildBlocQuery groupByValeurs() Group by the valeurs column
 * @method     ChildBlocQuery groupByTexte() Group by the texte column
 * @method     ChildBlocQuery groupByCss() Group by the css column
 * @method     ChildBlocQuery groupByCanal() Group by the canal column
 *
 * @method     ChildBlocQuery leftJoin($relation) Adds a LEFT JOIN clause to the query
 * @method     ChildBlocQuery rightJoin($relation) Adds a RIGHT JOIN clause to the query
 * @method     ChildBlocQuery innerJoin($relation) Adds a INNER JOIN clause to the query
 *
 * @method     ChildBlocQuery leftJoinWith($relation) Adds a LEFT JOIN clause and with to the query
 * @method     ChildBlocQuery rightJoinWith($relation) Adds a RIGHT JOIN clause and with to the query
 * @method     ChildBlocQuery innerJoinWith($relation) Adds a INNER JOIN clause and with to the query
 *
 * @method     ChildBlocQuery leftJoinCompositionsBloc($relationAlias = null) Adds a LEFT JOIN clause to the query using the CompositionsBloc relation
 * @method     ChildBlocQuery rightJoinCompositionsBloc($relationAlias = null) Adds a RIGHT JOIN clause to the query using the CompositionsBloc relation
 * @method     ChildBlocQuery innerJoinCompositionsBloc($relationAlias = null) Adds a INNER JOIN clause to the query using the CompositionsBloc relation
 *
 * @method     ChildBlocQuery joinWithCompositionsBloc($joinType = Criteria::INNER_JOIN) Adds a join clause and with to the query using the CompositionsBloc relation
 *
 * @method     ChildBlocQuery leftJoinWithCompositionsBloc() Adds a LEFT JOIN clause and with to the query using the CompositionsBloc relation
 * @method     ChildBlocQuery rightJoinWithCompositionsBloc() Adds a RIGHT JOIN clause and with to the query using the CompositionsBloc relation
 * @method     ChildBlocQuery innerJoinWithCompositionsBloc() Adds a INNER JOIN clause and with to the query using the CompositionsBloc relation
 *
 * @method     \CompositionsBlocQuery endUse() Finalizes a secondary criteria and merges it with its primary Criteria
 *
 * @method     ChildBloc findOne(ConnectionInterface $con = null) Return the first ChildBloc matching the query
 * @method     ChildBloc findOneOrCreate(ConnectionInterface $con = null) Return the first ChildBloc matching the query, or a new ChildBloc object populated from the query conditions when no match is found
 *
 * @method     ChildBloc findOneByIdBloc(int $id_bloc) Return the first ChildBloc filtered by the id_bloc column
 * @method     ChildBloc findOneByNom(string $nom) Return the first ChildBloc filtered by the nom column
 * @method     ChildBloc findOneByValeurs(string $valeurs) Return the first ChildBloc filtered by the valeurs column
 * @method     ChildBloc findOneByTexte(string $texte) Return the first ChildBloc filtered by the texte column
 * @method     ChildBloc findOneByCss(string $css) Return the first ChildBloc filtered by the css column
 * @method     ChildBloc findOneByCanal(string $canal) Return the first ChildBloc filtered by the canal column *

 * @method     ChildBloc requirePk($key, ConnectionInterface $con = null) Return the ChildBloc by primary key and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildBloc requireOne(ConnectionInterface $con = null) Return the first ChildBloc matching the query and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 *
 * @method     ChildBloc requireOneByIdBloc(int $id_bloc) Return the first ChildBloc filtered by the id_bloc column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildBloc requireOneByNom(string $nom) Return the first ChildBloc filtered by the nom column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildBloc requireOneByValeurs(string $valeurs) Return the first ChildBloc filtered by the valeurs column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildBloc requireOneByTexte(string $texte) Return the first ChildBloc filtered by the texte column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildBloc requireOneByCss(string $css) Return the first ChildBloc filtered by the css column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildBloc requireOneByCanal(string $canal) Return the first ChildBloc filtered by the canal column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 *
 * @method     ChildBloc[]|ObjectCollection find(ConnectionInterface $con = null) Return ChildBloc objects based on current ModelCriteria
 * @method     ChildBloc[]|ObjectCollection findByIdBloc(int $id_bloc) Return ChildBloc objects filtered by the id_bloc column
 * @method     ChildBloc[]|ObjectCollection findByNom(string $nom) Return ChildBloc objects filtered by the nom column
 * @method     ChildBloc[]|ObjectCollection findByValeurs(string $valeurs) Return ChildBloc objects filtered by the valeurs column
 * @method     ChildBloc[]|ObjectCollection findByTexte(string $texte) Return ChildBloc objects filtered by the texte column
 * @method     ChildBloc[]|ObjectCollection findByCss(string $css) Return ChildBloc objects filtered by the css column
 * @method     ChildBloc[]|ObjectCollection findByCanal(string $canal) Return ChildBloc objects filtered by the canal column
 * @method     ChildBloc[]|\Propel\Runtime\Util\PropelModelPager paginate($page = 1, $maxPerPage = 10, ConnectionInterface $con = null) Issue a SELECT query based on the current ModelCriteria and uses a page and a maximum number of results per page to compute an offset and a limit
 *
 */
abstract class BlocQuery extends ModelCriteria
{
    protected $entityNotFoundExceptionClass = '\\Propel\\Runtime\\Exception\\EntityNotFoundException';

    /**
     * Initializes internal state of \Base\BlocQuery object.
     *
     * @param     string $dbName The database name
     * @param     string $modelName The phpName of a model, e.g. 'Book'
     * @param     string $modelAlias The alias for the model in this query, e.g. 'b'
     */
    public function __construct($dbName = 'simplasso', $modelName = '\\Bloc', $modelAlias = null)
    {
        parent::__construct($dbName, $modelName, $modelAlias);
    }

    /**
     * Returns a new ChildBlocQuery object.
     *
     * @param     string $modelAlias The alias of a model in the query
     * @param     Criteria $criteria Optional Criteria to build the query from
     *
     * @return ChildBlocQuery
     */
    public static function create($modelAlias = null, Criteria $criteria = null)
    {
        if ($criteria instanceof ChildBlocQuery) {
            return $criteria;
        }
        $query = new ChildBlocQuery();
        if (null !== $modelAlias) {
            $query->setModelAlias($modelAlias);
        }
        if ($criteria instanceof Criteria) {
            $query->mergeWith($criteria);
        }

        return $query;
    }

    /**
     * Find object by primary key.
     * Propel uses the instance pool to skip the database if the object exists.
     * Go fast if the query is untouched.
     *
     * <code>
     * $obj  = $c->findPk(12, $con);
     * </code>
     *
     * @param mixed $key Primary key to use for the query
     * @param ConnectionInterface $con an optional connection object
     *
     * @return ChildBloc|array|mixed the result, formatted by the current formatter
     */
    public function findPk($key, ConnectionInterface $con = null)
    {
        if ($key === null) {
            return null;
        }

        if ($con === null) {
            $con = Propel::getServiceContainer()->getReadConnection(BlocTableMap::DATABASE_NAME);
        }

        $this->basePreSelect($con);

        if (
            $this->formatter || $this->modelAlias || $this->with || $this->select
            || $this->selectColumns || $this->asColumns || $this->selectModifiers
            || $this->map || $this->having || $this->joins
        ) {
            return $this->findPkComplex($key, $con);
        }

        if ((null !== ($obj = BlocTableMap::getInstanceFromPool(null === $key || is_scalar($key) || is_callable([$key, '__toString']) ? (string) $key : $key)))) {
            // the object is already in the instance pool
            return $obj;
        }

        return $this->findPkSimple($key, $con);
    }

    /**
     * Find object by primary key using raw SQL to go fast.
     * Bypass doSelect() and the object formatter by using generated code.
     *
     * @param     mixed $key Primary key to use for the query
     * @param     ConnectionInterface $con A connection object
     *
     * @throws \Propel\Runtime\Exception\PropelException
     *
     * @return ChildBloc A model object, or null if the key is not found
     */
    protected function findPkSimple($key, ConnectionInterface $con)
    {
        $sql = 'SELECT `id_bloc`, `nom`, `valeurs`, `texte`, `css`, `canal` FROM `com_blocs` WHERE `id_bloc` = :p0';
        try {
            $stmt = $con->prepare($sql);
            $stmt->bindValue(':p0', $key, PDO::PARAM_INT);
            $stmt->execute();
        } catch (Exception $e) {
            Propel::log($e->getMessage(), Propel::LOG_ERR);
            throw new PropelException(sprintf('Unable to execute SELECT statement [%s]', $sql), 0, $e);
        }
        $obj = null;
        if ($row = $stmt->fetch(\PDO::FETCH_NUM)) {
            /** @var ChildBloc $obj */
            $obj = new ChildBloc();
            $obj->hydrate($row);
            BlocTableMap::addInstanceToPool($obj, null === $key || is_scalar($key) || is_callable([$key, '__toString']) ? (string) $key : $key);
        }
        $stmt->closeCursor();

        return $obj;
    }

    /**
     * Find object by primary key.
     *
     * @param     mixed $key Primary key to use for the query
     * @param     ConnectionInterface $con A connection object
     *
     * @return ChildBloc|array|mixed the result, formatted by the current formatter
     */
    protected function findPkComplex($key, ConnectionInterface $con)
    {
        // As the query uses a PK condition, no limit(1) is necessary.
        $criteria = $this->isKeepQuery() ? clone $this : $this;
        $dataFetcher = $criteria
            ->filterByPrimaryKey($key)
            ->doSelect($con);

        return $criteria->getFormatter()->init($criteria)->formatOne($dataFetcher);
    }

    /**
     * Find objects by primary key
     * <code>
     * $objs = $c->findPks(array(12, 56, 832), $con);
     * </code>
     * @param     array $keys Primary keys to use for the query
     * @param     ConnectionInterface $con an optional connection object
     *
     * @return ObjectCollection|array|mixed the list of results, formatted by the current formatter
     */
    public function findPks($keys, ConnectionInterface $con = null)
    {
        if (null === $con) {
            $con = Propel::getServiceContainer()->getReadConnection($this->getDbName());
        }
        $this->basePreSelect($con);
        $criteria = $this->isKeepQuery() ? clone $this : $this;
        $dataFetcher = $criteria
            ->filterByPrimaryKeys($keys)
            ->doSelect($con);

        return $criteria->getFormatter()->init($criteria)->format($dataFetcher);
    }

    /**
     * Filter the query by primary key
     *
     * @param     mixed $key Primary key to use for the query
     *
     * @return $this|ChildBlocQuery The current query, for fluid interface
     */
    public function filterByPrimaryKey($key)
    {

        return $this->addUsingAlias(BlocTableMap::COL_ID_BLOC, $key, Criteria::EQUAL);
    }

    /**
     * Filter the query by a list of primary keys
     *
     * @param     array $keys The list of primary key to use for the query
     *
     * @return $this|ChildBlocQuery The current query, for fluid interface
     */
    public function filterByPrimaryKeys($keys)
    {

        return $this->addUsingAlias(BlocTableMap::COL_ID_BLOC, $keys, Criteria::IN);
    }

    /**
     * Filter the query on the id_bloc column
     *
     * Example usage:
     * <code>
     * $query->filterByIdBloc(1234); // WHERE id_bloc = 1234
     * $query->filterByIdBloc(array(12, 34)); // WHERE id_bloc IN (12, 34)
     * $query->filterByIdBloc(array('min' => 12)); // WHERE id_bloc > 12
     * </code>
     *
     * @param     mixed $idBloc The value to use as filter.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildBlocQuery The current query, for fluid interface
     */
    public function filterByIdBloc($idBloc = null, $comparison = null)
    {
        if (is_array($idBloc)) {
            $useMinMax = false;
            if (isset($idBloc['min'])) {
                $this->addUsingAlias(BlocTableMap::COL_ID_BLOC, $idBloc['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($idBloc['max'])) {
                $this->addUsingAlias(BlocTableMap::COL_ID_BLOC, $idBloc['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(BlocTableMap::COL_ID_BLOC, $idBloc, $comparison);
    }

    /**
     * Filter the query on the nom column
     *
     * Example usage:
     * <code>
     * $query->filterByNom('fooValue');   // WHERE nom = 'fooValue'
     * $query->filterByNom('%fooValue%', Criteria::LIKE); // WHERE nom LIKE '%fooValue%'
     * </code>
     *
     * @param     string $nom The value to use as filter.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildBlocQuery The current query, for fluid interface
     */
    public function filterByNom($nom = null, $comparison = null)
    {
        if (null === $comparison) {
            if (is_array($nom)) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(BlocTableMap::COL_NOM, $nom, $comparison);
    }

    /**
     * Filter the query on the valeurs column
     *
     * Example usage:
     * <code>
     * $query->filterByValeurs('fooValue');   // WHERE valeurs = 'fooValue'
     * $query->filterByValeurs('%fooValue%', Criteria::LIKE); // WHERE valeurs LIKE '%fooValue%'
     * </code>
     *
     * @param     string $valeurs The value to use as filter.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildBlocQuery The current query, for fluid interface
     */
    public function filterByValeurs($valeurs = null, $comparison = null)
    {
        if (null === $comparison) {
            if (is_array($valeurs)) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(BlocTableMap::COL_VALEURS, $valeurs, $comparison);
    }

    /**
     * Filter the query on the texte column
     *
     * Example usage:
     * <code>
     * $query->filterByTexte('fooValue');   // WHERE texte = 'fooValue'
     * $query->filterByTexte('%fooValue%', Criteria::LIKE); // WHERE texte LIKE '%fooValue%'
     * </code>
     *
     * @param     string $texte The value to use as filter.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildBlocQuery The current query, for fluid interface
     */
    public function filterByTexte($texte = null, $comparison = null)
    {
        if (null === $comparison) {
            if (is_array($texte)) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(BlocTableMap::COL_TEXTE, $texte, $comparison);
    }

    /**
     * Filter the query on the css column
     *
     * Example usage:
     * <code>
     * $query->filterByCss('fooValue');   // WHERE css = 'fooValue'
     * $query->filterByCss('%fooValue%', Criteria::LIKE); // WHERE css LIKE '%fooValue%'
     * </code>
     *
     * @param     string $css The value to use as filter.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildBlocQuery The current query, for fluid interface
     */
    public function filterByCss($css = null, $comparison = null)
    {
        if (null === $comparison) {
            if (is_array($css)) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(BlocTableMap::COL_CSS, $css, $comparison);
    }

    /**
     * Filter the query on the canal column
     *
     * Example usage:
     * <code>
     * $query->filterByCanal('fooValue');   // WHERE canal = 'fooValue'
     * $query->filterByCanal('%fooValue%', Criteria::LIKE); // WHERE canal LIKE '%fooValue%'
     * </code>
     *
     * @param     string $canal The value to use as filter.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildBlocQuery The current query, for fluid interface
     */
    public function filterByCanal($canal = null, $comparison = null)
    {
        if (null === $comparison) {
            if (is_array($canal)) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(BlocTableMap::COL_CANAL, $canal, $comparison);
    }

    /**
     * Filter the query by a related \CompositionsBloc object
     *
     * @param \CompositionsBloc|ObjectCollection $compositionsBloc the related object to use as filter
     * @param string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return ChildBlocQuery The current query, for fluid interface
     */
    public function filterByCompositionsBloc($compositionsBloc, $comparison = null)
    {
        if ($compositionsBloc instanceof \CompositionsBloc) {
            return $this
                ->addUsingAlias(BlocTableMap::COL_ID_BLOC, $compositionsBloc->getIdBloc(), $comparison);
        } elseif ($compositionsBloc instanceof ObjectCollection) {
            return $this
                ->useCompositionsBlocQuery()
                ->filterByPrimaryKeys($compositionsBloc->getPrimaryKeys())
                ->endUse();
        } else {
            throw new PropelException('filterByCompositionsBloc() only accepts arguments of type \CompositionsBloc or Collection');
        }
    }

    /**
     * Adds a JOIN clause to the query using the CompositionsBloc relation
     *
     * @param     string $relationAlias optional alias for the relation
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return $this|ChildBlocQuery The current query, for fluid interface
     */
    public function joinCompositionsBloc($relationAlias = null, $joinType = Criteria::INNER_JOIN)
    {
        $tableMap = $this->getTableMap();
        $relationMap = $tableMap->getRelation('CompositionsBloc');

        // create a ModelJoin object for this join
        $join = new ModelJoin();
        $join->setJoinType($joinType);
        $join->setRelationMap($relationMap, $this->useAliasInSQL ? $this->getModelAlias() : null, $relationAlias);
        if ($previousJoin = $this->getPreviousJoin()) {
            $join->setPreviousJoin($previousJoin);
        }

        // add the ModelJoin to the current object
        if ($relationAlias) {
            $this->addAlias($relationAlias, $relationMap->getRightTable()->getName());
            $this->addJoinObject($join, $relationAlias);
        } else {
            $this->addJoinObject($join, 'CompositionsBloc');
        }

        return $this;
    }

    /**
     * Use the CompositionsBloc relation CompositionsBloc object
     *
     * @see useQuery()
     *
     * @param     string $relationAlias optional alias for the relation,
     *                                   to be used as main alias in the secondary query
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return \CompositionsBlocQuery A secondary query class using the current class as primary query
     */
    public function useCompositionsBlocQuery($relationAlias = null, $joinType = Criteria::INNER_JOIN)
    {
        return $this
            ->joinCompositionsBloc($relationAlias, $joinType)
            ->useQuery($relationAlias ? $relationAlias : 'CompositionsBloc', '\CompositionsBlocQuery');
    }

    /**
     * Filter the query by a related Composition object
     * using the com_compositions_blocs table as cross reference
     *
     * @param Composition $composition the related object to use as filter
     * @param string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return ChildBlocQuery The current query, for fluid interface
     */
    public function filterByComposition($composition, $comparison = Criteria::EQUAL)
    {
        return $this
            ->useCompositionsBlocQuery()
            ->filterByComposition($composition, $comparison)
            ->endUse();
    }

    /**
     * Exclude object from result
     *
     * @param   ChildBloc $bloc Object to remove from the list of results
     *
     * @return $this|ChildBlocQuery The current query, for fluid interface
     */
    public function prune($bloc = null)
    {
        if ($bloc) {
            $this->addUsingAlias(BlocTableMap::COL_ID_BLOC, $bloc->getIdBloc(), Criteria::NOT_EQUAL);
        }

        return $this;
    }

    /**
     * Deletes all rows from the com_blocs table.
     *
     * @param ConnectionInterface $con the connection to use
     * @return int The number of affected rows (if supported by underlying database driver).
     */
    public function doDeleteAll(ConnectionInterface $con = null)
    {
        if (null === $con) {
            $con = Propel::getServiceContainer()->getWriteConnection(BlocTableMap::DATABASE_NAME);
        }

        // use transaction because $criteria could contain info
        // for more than one table or we could emulating ON DELETE CASCADE, etc.
        return $con->transaction(function () use ($con) {
            $affectedRows = 0; // initialize var to track total num of affected rows
            $affectedRows += parent::doDeleteAll($con);
            // Because this db requires some delete cascade/set null emulation, we have to
            // clear the cached instance *after* the emulation has happened (since
            // instances get re-added by the select statement contained therein).
            BlocTableMap::clearInstancePool();
            BlocTableMap::clearRelatedInstancePool();

            return $affectedRows;
        });
    }

    /**
     * Performs a DELETE on the database based on the current ModelCriteria
     *
     * @param ConnectionInterface $con the connection to use
     * @return int             The number of affected rows (if supported by underlying database driver).  This includes CASCADE-related rows
     *                         if supported by native driver or if emulated using Propel.
     * @throws PropelException Any exceptions caught during processing will be
     *                         rethrown wrapped into a PropelException.
     */
    public function delete(ConnectionInterface $con = null)
    {
        if (null === $con) {
            $con = Propel::getServiceContainer()->getWriteConnection(BlocTableMap::DATABASE_NAME);
        }

        $criteria = $this;

        // Set the correct dbName
        $criteria->setDbName(BlocTableMap::DATABASE_NAME);

        // use transaction because $criteria could contain info
        // for more than one table or we could emulating ON DELETE CASCADE, etc.
        return $con->transaction(function () use ($con, $criteria) {
            $affectedRows = 0; // initialize var to track total num of affected rows

            BlocTableMap::removeInstanceFromPool($criteria);

            $affectedRows += ModelCriteria::delete($con);
            BlocTableMap::clearRelatedInstancePool();

            return $affectedRows;
        });
    }

} // BlocQuery
