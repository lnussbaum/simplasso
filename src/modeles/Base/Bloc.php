<?php

namespace Base;

use \Bloc as ChildBloc;
use \BlocQuery as ChildBlocQuery;
use \Composition as ChildComposition;
use \CompositionQuery as ChildCompositionQuery;
use \CompositionsBloc as ChildCompositionsBloc;
use \CompositionsBlocQuery as ChildCompositionsBlocQuery;
use \Exception;
use \PDO;
use Map\BlocTableMap;
use Map\CompositionsBlocTableMap;
use Propel\Runtime\Propel;
use Propel\Runtime\ActiveQuery\Criteria;
use Propel\Runtime\ActiveQuery\ModelCriteria;
use Propel\Runtime\ActiveRecord\ActiveRecordInterface;
use Propel\Runtime\Collection\Collection;
use Propel\Runtime\Collection\ObjectCollection;
use Propel\Runtime\Connection\ConnectionInterface;
use Propel\Runtime\Exception\BadMethodCallException;
use Propel\Runtime\Exception\LogicException;
use Propel\Runtime\Exception\PropelException;
use Propel\Runtime\Map\TableMap;
use Propel\Runtime\Parser\AbstractParser;

/**
 * Base class that represents a row from the 'com_blocs' table.
 *
 *
 *
 * @package    propel.generator..Base
 */
abstract class Bloc implements ActiveRecordInterface
{
    /**
     * TableMap class name
     */
    const TABLE_MAP = '\\Map\\BlocTableMap';


    /**
     * attribute to determine if this object has previously been saved.
     * @var boolean
     */
    protected $new = true;

    /**
     * attribute to determine whether this object has been deleted.
     * @var boolean
     */
    protected $deleted = false;

    /**
     * The columns that have been modified in current object.
     * Tracking modified columns allows us to only update modified columns.
     * @var array
     */
    protected $modifiedColumns = array();

    /**
     * The (virtual) columns that are added at runtime
     * The formatters can add supplementary columns based on a resultset
     * @var array
     */
    protected $virtualColumns = array();

    /**
     * The value for the id_bloc field.
     *
     * @var        int
     */
    protected $id_bloc;

    /**
     * The value for the nom field.
     *
     * @var        string
     */
    protected $nom;

    /**
     * The value for the valeurs field.
     *
     * @var        string
     */
    protected $valeurs;

    /**
     * The value for the texte field.
     *
     * @var        string
     */
    protected $texte;

    /**
     * The value for the css field.
     *
     * @var        string
     */
    protected $css;

    /**
     * The value for the canal field.
     *
     * @var        string
     */
    protected $canal;

    /**
     * @var        ObjectCollection|ChildCompositionsBloc[] Collection to store aggregation of ChildCompositionsBloc objects.
     */
    protected $collCompositionsBlocs;
    protected $collCompositionsBlocsPartial;

    /**
     * @var        ObjectCollection|ChildComposition[] Cross Collection to store aggregation of ChildComposition objects.
     */
    protected $collCompositions;

    /**
     * @var bool
     */
    protected $collCompositionsPartial;

    /**
     * Flag to prevent endless save loop, if this object is referenced
     * by another object which falls in this transaction.
     *
     * @var boolean
     */
    protected $alreadyInSave = false;

    /**
     * An array of objects scheduled for deletion.
     * @var ObjectCollection|ChildComposition[]
     */
    protected $compositionsScheduledForDeletion = null;

    /**
     * An array of objects scheduled for deletion.
     * @var ObjectCollection|ChildCompositionsBloc[]
     */
    protected $compositionsBlocsScheduledForDeletion = null;

    /**
     * Initializes internal state of Base\Bloc object.
     */
    public function __construct()
    {
    }

    /**
     * Returns whether the object has been modified.
     *
     * @return boolean True if the object has been modified.
     */
    public function isModified()
    {
        return !!$this->modifiedColumns;
    }

    /**
     * Has specified column been modified?
     *
     * @param  string  $col column fully qualified name (TableMap::TYPE_COLNAME), e.g. Book::AUTHOR_ID
     * @return boolean True if $col has been modified.
     */
    public function isColumnModified($col)
    {
        return $this->modifiedColumns && isset($this->modifiedColumns[$col]);
    }

    /**
     * Get the columns that have been modified in this object.
     * @return array A unique list of the modified column names for this object.
     */
    public function getModifiedColumns()
    {
        return $this->modifiedColumns ? array_keys($this->modifiedColumns) : [];
    }

    /**
     * Returns whether the object has ever been saved.  This will
     * be false, if the object was retrieved from storage or was created
     * and then saved.
     *
     * @return boolean true, if the object has never been persisted.
     */
    public function isNew()
    {
        return $this->new;
    }

    /**
     * Setter for the isNew attribute.  This method will be called
     * by Propel-generated children and objects.
     *
     * @param boolean $b the state of the object.
     */
    public function setNew($b)
    {
        $this->new = (boolean) $b;
    }

    /**
     * Whether this object has been deleted.
     * @return boolean The deleted state of this object.
     */
    public function isDeleted()
    {
        return $this->deleted;
    }

    /**
     * Specify whether this object has been deleted.
     * @param  boolean $b The deleted state of this object.
     * @return void
     */
    public function setDeleted($b)
    {
        $this->deleted = (boolean) $b;
    }

    /**
     * Sets the modified state for the object to be false.
     * @param  string $col If supplied, only the specified column is reset.
     * @return void
     */
    public function resetModified($col = null)
    {
        if (null !== $col) {
            if (isset($this->modifiedColumns[$col])) {
                unset($this->modifiedColumns[$col]);
            }
        } else {
            $this->modifiedColumns = array();
        }
    }

    /**
     * Compares this with another <code>Bloc</code> instance.  If
     * <code>obj</code> is an instance of <code>Bloc</code>, delegates to
     * <code>equals(Bloc)</code>.  Otherwise, returns <code>false</code>.
     *
     * @param  mixed   $obj The object to compare to.
     * @return boolean Whether equal to the object specified.
     */
    public function equals($obj)
    {
        if (!$obj instanceof static) {
            return false;
        }

        if ($this === $obj) {
            return true;
        }

        if (null === $this->getPrimaryKey() || null === $obj->getPrimaryKey()) {
            return false;
        }

        return $this->getPrimaryKey() === $obj->getPrimaryKey();
    }

    /**
     * Get the associative array of the virtual columns in this object
     *
     * @return array
     */
    public function getVirtualColumns()
    {
        return $this->virtualColumns;
    }

    /**
     * Checks the existence of a virtual column in this object
     *
     * @param  string  $name The virtual column name
     * @return boolean
     */
    public function hasVirtualColumn($name)
    {
        return array_key_exists($name, $this->virtualColumns);
    }

    /**
     * Get the value of a virtual column in this object
     *
     * @param  string $name The virtual column name
     * @return mixed
     *
     * @throws PropelException
     */
    public function getVirtualColumn($name)
    {
        if (!$this->hasVirtualColumn($name)) {
            throw new PropelException(sprintf('Cannot get value of inexistent virtual column %s.', $name));
        }

        return $this->virtualColumns[$name];
    }

    /**
     * Set the value of a virtual column in this object
     *
     * @param string $name  The virtual column name
     * @param mixed  $value The value to give to the virtual column
     *
     * @return $this|Bloc The current object, for fluid interface
     */
    public function setVirtualColumn($name, $value)
    {
        $this->virtualColumns[$name] = $value;

        return $this;
    }

    /**
     * Logs a message using Propel::log().
     *
     * @param  string  $msg
     * @param  int     $priority One of the Propel::LOG_* logging levels
     * @return boolean
     */
    protected function log($msg, $priority = Propel::LOG_INFO)
    {
        return Propel::log(get_class($this) . ': ' . $msg, $priority);
    }

    /**
     * Export the current object properties to a string, using a given parser format
     * <code>
     * $book = BookQuery::create()->findPk(9012);
     * echo $book->exportTo('JSON');
     *  => {"Id":9012,"Title":"Don Juan","ISBN":"0140422161","Price":12.99,"PublisherId":1234,"AuthorId":5678}');
     * </code>
     *
     * @param  mixed   $parser                 A AbstractParser instance, or a format name ('XML', 'YAML', 'JSON', 'CSV')
     * @param  boolean $includeLazyLoadColumns (optional) Whether to include lazy load(ed) columns. Defaults to TRUE.
     * @return string  The exported data
     */
    public function exportTo($parser, $includeLazyLoadColumns = true)
    {
        if (!$parser instanceof AbstractParser) {
            $parser = AbstractParser::getParser($parser);
        }

        return $parser->fromArray($this->toArray(TableMap::TYPE_PHPNAME, $includeLazyLoadColumns, array(), true));
    }

    /**
     * Clean up internal collections prior to serializing
     * Avoids recursive loops that turn into segmentation faults when serializing
     */
    public function __sleep()
    {
        $this->clearAllReferences();

        $cls = new \ReflectionClass($this);
        $propertyNames = [];
        $serializableProperties = array_diff($cls->getProperties(), $cls->getProperties(\ReflectionProperty::IS_STATIC));

        foreach($serializableProperties as $property) {
            $propertyNames[] = $property->getName();
        }

        return $propertyNames;
    }

    /**
     * Get the [id_bloc] column value.
     *
     * @return int
     */
    public function getIdBloc()
    {
        return $this->id_bloc;
    }

    /**
     * Get the [nom] column value.
     *
     * @return string
     */
    public function getNom()
    {
        return $this->nom;
    }

    /**
     * Get the [valeurs] column value.
     *
     * @return string
     */
    public function getValeurs()
    {
        return $this->valeurs;
    }

    /**
     * Get the [texte] column value.
     *
     * @return string
     */
    public function getTexte()
    {
        return $this->texte;
    }

    /**
     * Get the [css] column value.
     *
     * @return string
     */
    public function getCss()
    {
        return $this->css;
    }

    /**
     * Get the [canal] column value.
     *
     * @return string
     */
    public function getCanal()
    {
        return $this->canal;
    }

    /**
     * Set the value of [id_bloc] column.
     *
     * @param int $v new value
     * @return $this|\Bloc The current object (for fluent API support)
     */
    public function setIdBloc($v)
    {
        if ($v !== null) {
            $v = (int) $v;
        }

        if ($this->id_bloc !== $v) {
            $this->id_bloc = $v;
            $this->modifiedColumns[BlocTableMap::COL_ID_BLOC] = true;
        }

        return $this;
    } // setIdBloc()

    /**
     * Set the value of [nom] column.
     *
     * @param string $v new value
     * @return $this|\Bloc The current object (for fluent API support)
     */
    public function setNom($v)
    {
        if ($v !== null) {
            $v = (string) $v;
        }

        if ($this->nom !== $v) {
            $this->nom = $v;
            $this->modifiedColumns[BlocTableMap::COL_NOM] = true;
        }

        return $this;
    } // setNom()

    /**
     * Set the value of [valeurs] column.
     *
     * @param string $v new value
     * @return $this|\Bloc The current object (for fluent API support)
     */
    public function setValeurs($v)
    {
        if ($v !== null) {
            $v = (string) $v;
        }

        if ($this->valeurs !== $v) {
            $this->valeurs = $v;
            $this->modifiedColumns[BlocTableMap::COL_VALEURS] = true;
        }

        return $this;
    } // setValeurs()

    /**
     * Set the value of [texte] column.
     *
     * @param string $v new value
     * @return $this|\Bloc The current object (for fluent API support)
     */
    public function setTexte($v)
    {
        if ($v !== null) {
            $v = (string) $v;
        }

        if ($this->texte !== $v) {
            $this->texte = $v;
            $this->modifiedColumns[BlocTableMap::COL_TEXTE] = true;
        }

        return $this;
    } // setTexte()

    /**
     * Set the value of [css] column.
     *
     * @param string $v new value
     * @return $this|\Bloc The current object (for fluent API support)
     */
    public function setCss($v)
    {
        if ($v !== null) {
            $v = (string) $v;
        }

        if ($this->css !== $v) {
            $this->css = $v;
            $this->modifiedColumns[BlocTableMap::COL_CSS] = true;
        }

        return $this;
    } // setCss()

    /**
     * Set the value of [canal] column.
     *
     * @param string $v new value
     * @return $this|\Bloc The current object (for fluent API support)
     */
    public function setCanal($v)
    {
        if ($v !== null) {
            $v = (string) $v;
        }

        if ($this->canal !== $v) {
            $this->canal = $v;
            $this->modifiedColumns[BlocTableMap::COL_CANAL] = true;
        }

        return $this;
    } // setCanal()

    /**
     * Indicates whether the columns in this object are only set to default values.
     *
     * This method can be used in conjunction with isModified() to indicate whether an object is both
     * modified _and_ has some values set which are non-default.
     *
     * @return boolean Whether the columns in this object are only been set with default values.
     */
    public function hasOnlyDefaultValues()
    {
        // otherwise, everything was equal, so return TRUE
        return true;
    } // hasOnlyDefaultValues()

    /**
     * Hydrates (populates) the object variables with values from the database resultset.
     *
     * An offset (0-based "start column") is specified so that objects can be hydrated
     * with a subset of the columns in the resultset rows.  This is needed, for example,
     * for results of JOIN queries where the resultset row includes columns from two or
     * more tables.
     *
     * @param array   $row       The row returned by DataFetcher->fetch().
     * @param int     $startcol  0-based offset column which indicates which restultset column to start with.
     * @param boolean $rehydrate Whether this object is being re-hydrated from the database.
     * @param string  $indexType The index type of $row. Mostly DataFetcher->getIndexType().
                                  One of the class type constants TableMap::TYPE_PHPNAME, TableMap::TYPE_CAMELNAME
     *                            TableMap::TYPE_COLNAME, TableMap::TYPE_FIELDNAME, TableMap::TYPE_NUM.
     *
     * @return int             next starting column
     * @throws PropelException - Any caught Exception will be rewrapped as a PropelException.
     */
    public function hydrate($row, $startcol = 0, $rehydrate = false, $indexType = TableMap::TYPE_NUM)
    {
        try {

            $col = $row[TableMap::TYPE_NUM == $indexType ? 0 + $startcol : BlocTableMap::translateFieldName('IdBloc', TableMap::TYPE_PHPNAME, $indexType)];
            $this->id_bloc = (null !== $col) ? (int) $col : null;

            $col = $row[TableMap::TYPE_NUM == $indexType ? 1 + $startcol : BlocTableMap::translateFieldName('Nom', TableMap::TYPE_PHPNAME, $indexType)];
            $this->nom = (null !== $col) ? (string) $col : null;

            $col = $row[TableMap::TYPE_NUM == $indexType ? 2 + $startcol : BlocTableMap::translateFieldName('Valeurs', TableMap::TYPE_PHPNAME, $indexType)];
            $this->valeurs = (null !== $col) ? (string) $col : null;

            $col = $row[TableMap::TYPE_NUM == $indexType ? 3 + $startcol : BlocTableMap::translateFieldName('Texte', TableMap::TYPE_PHPNAME, $indexType)];
            $this->texte = (null !== $col) ? (string) $col : null;

            $col = $row[TableMap::TYPE_NUM == $indexType ? 4 + $startcol : BlocTableMap::translateFieldName('Css', TableMap::TYPE_PHPNAME, $indexType)];
            $this->css = (null !== $col) ? (string) $col : null;

            $col = $row[TableMap::TYPE_NUM == $indexType ? 5 + $startcol : BlocTableMap::translateFieldName('Canal', TableMap::TYPE_PHPNAME, $indexType)];
            $this->canal = (null !== $col) ? (string) $col : null;
            $this->resetModified();

            $this->setNew(false);

            if ($rehydrate) {
                $this->ensureConsistency();
            }

            return $startcol + 6; // 6 = BlocTableMap::NUM_HYDRATE_COLUMNS.

        } catch (Exception $e) {
            throw new PropelException(sprintf('Error populating %s object', '\\Bloc'), 0, $e);
        }
    }

    /**
     * Checks and repairs the internal consistency of the object.
     *
     * This method is executed after an already-instantiated object is re-hydrated
     * from the database.  It exists to check any foreign keys to make sure that
     * the objects related to the current object are correct based on foreign key.
     *
     * You can override this method in the stub class, but you should always invoke
     * the base method from the overridden method (i.e. parent::ensureConsistency()),
     * in case your model changes.
     *
     * @throws PropelException
     */
    public function ensureConsistency()
    {
    } // ensureConsistency

    /**
     * Reloads this object from datastore based on primary key and (optionally) resets all associated objects.
     *
     * This will only work if the object has been saved and has a valid primary key set.
     *
     * @param      boolean $deep (optional) Whether to also de-associated any related objects.
     * @param      ConnectionInterface $con (optional) The ConnectionInterface connection to use.
     * @return void
     * @throws PropelException - if this object is deleted, unsaved or doesn't have pk match in db
     */
    public function reload($deep = false, ConnectionInterface $con = null)
    {
        if ($this->isDeleted()) {
            throw new PropelException("Cannot reload a deleted object.");
        }

        if ($this->isNew()) {
            throw new PropelException("Cannot reload an unsaved object.");
        }

        if ($con === null) {
            $con = Propel::getServiceContainer()->getReadConnection(BlocTableMap::DATABASE_NAME);
        }

        // We don't need to alter the object instance pool; we're just modifying this instance
        // already in the pool.

        $dataFetcher = ChildBlocQuery::create(null, $this->buildPkeyCriteria())->setFormatter(ModelCriteria::FORMAT_STATEMENT)->find($con);
        $row = $dataFetcher->fetch();
        $dataFetcher->close();
        if (!$row) {
            throw new PropelException('Cannot find matching row in the database to reload object values.');
        }
        $this->hydrate($row, 0, true, $dataFetcher->getIndexType()); // rehydrate

        if ($deep) {  // also de-associate any related objects?

            $this->collCompositionsBlocs = null;

            $this->collCompositions = null;
        } // if (deep)
    }

    /**
     * Removes this object from datastore and sets delete attribute.
     *
     * @param      ConnectionInterface $con
     * @return void
     * @throws PropelException
     * @see Bloc::setDeleted()
     * @see Bloc::isDeleted()
     */
    public function delete(ConnectionInterface $con = null)
    {
        if ($this->isDeleted()) {
            throw new PropelException("This object has already been deleted.");
        }

        if ($con === null) {
            $con = Propel::getServiceContainer()->getWriteConnection(BlocTableMap::DATABASE_NAME);
        }

        $con->transaction(function () use ($con) {
            $deleteQuery = ChildBlocQuery::create()
                ->filterByPrimaryKey($this->getPrimaryKey());
            $ret = $this->preDelete($con);
            if ($ret) {
                $deleteQuery->delete($con);
                $this->postDelete($con);
                $this->setDeleted(true);
            }
        });
    }

    /**
     * Persists this object to the database.
     *
     * If the object is new, it inserts it; otherwise an update is performed.
     * All modified related objects will also be persisted in the doSave()
     * method.  This method wraps all precipitate database operations in a
     * single transaction.
     *
     * @param      ConnectionInterface $con
     * @return int             The number of rows affected by this insert/update and any referring fk objects' save() operations.
     * @throws PropelException
     * @see doSave()
     */
    public function save(ConnectionInterface $con = null)
    {
        if ($this->isDeleted()) {
            throw new PropelException("You cannot save an object that has been deleted.");
        }

        if ($this->alreadyInSave) {
            return 0;
        }

        if ($con === null) {
            $con = Propel::getServiceContainer()->getWriteConnection(BlocTableMap::DATABASE_NAME);
        }

        return $con->transaction(function () use ($con) {
            $ret = $this->preSave($con);
            $isInsert = $this->isNew();
            if ($isInsert) {
                $ret = $ret && $this->preInsert($con);
            } else {
                $ret = $ret && $this->preUpdate($con);
            }
            if ($ret) {
                $affectedRows = $this->doSave($con);
                if ($isInsert) {
                    $this->postInsert($con);
                } else {
                    $this->postUpdate($con);
                }
                $this->postSave($con);
                BlocTableMap::addInstanceToPool($this);
            } else {
                $affectedRows = 0;
            }

            return $affectedRows;
        });
    }

    /**
     * Performs the work of inserting or updating the row in the database.
     *
     * If the object is new, it inserts it; otherwise an update is performed.
     * All related objects are also updated in this method.
     *
     * @param      ConnectionInterface $con
     * @return int             The number of rows affected by this insert/update and any referring fk objects' save() operations.
     * @throws PropelException
     * @see save()
     */
    protected function doSave(ConnectionInterface $con)
    {
        $affectedRows = 0; // initialize var to track total num of affected rows
        if (!$this->alreadyInSave) {
            $this->alreadyInSave = true;

            if ($this->isNew() || $this->isModified()) {
                // persist changes
                if ($this->isNew()) {
                    $this->doInsert($con);
                    $affectedRows += 1;
                } else {
                    $affectedRows += $this->doUpdate($con);
                }
                $this->resetModified();
            }

            if ($this->compositionsScheduledForDeletion !== null) {
                if (!$this->compositionsScheduledForDeletion->isEmpty()) {
                    $pks = array();
                    foreach ($this->compositionsScheduledForDeletion as $entry) {
                        $entryPk = [];

                        $entryPk[1] = $this->getIdBloc();
                        $entryPk[0] = $entry->getIdComposition();
                        $pks[] = $entryPk;
                    }

                    \CompositionsBlocQuery::create()
                        ->filterByPrimaryKeys($pks)
                        ->delete($con);

                    $this->compositionsScheduledForDeletion = null;
                }

            }

            if ($this->collCompositions) {
                foreach ($this->collCompositions as $composition) {
                    if (!$composition->isDeleted() && ($composition->isNew() || $composition->isModified())) {
                        $composition->save($con);
                    }
                }
            }


            if ($this->compositionsBlocsScheduledForDeletion !== null) {
                if (!$this->compositionsBlocsScheduledForDeletion->isEmpty()) {
                    \CompositionsBlocQuery::create()
                        ->filterByPrimaryKeys($this->compositionsBlocsScheduledForDeletion->getPrimaryKeys(false))
                        ->delete($con);
                    $this->compositionsBlocsScheduledForDeletion = null;
                }
            }

            if ($this->collCompositionsBlocs !== null) {
                foreach ($this->collCompositionsBlocs as $referrerFK) {
                    if (!$referrerFK->isDeleted() && ($referrerFK->isNew() || $referrerFK->isModified())) {
                        $affectedRows += $referrerFK->save($con);
                    }
                }
            }

            $this->alreadyInSave = false;

        }

        return $affectedRows;
    } // doSave()

    /**
     * Insert the row in the database.
     *
     * @param      ConnectionInterface $con
     *
     * @throws PropelException
     * @see doSave()
     */
    protected function doInsert(ConnectionInterface $con)
    {
        $modifiedColumns = array();
        $index = 0;

        $this->modifiedColumns[BlocTableMap::COL_ID_BLOC] = true;
        if (null !== $this->id_bloc) {
            throw new PropelException('Cannot insert a value for auto-increment primary key (' . BlocTableMap::COL_ID_BLOC . ')');
        }

         // check the columns in natural order for more readable SQL queries
        if ($this->isColumnModified(BlocTableMap::COL_ID_BLOC)) {
            $modifiedColumns[':p' . $index++]  = '`id_bloc`';
        }
        if ($this->isColumnModified(BlocTableMap::COL_NOM)) {
            $modifiedColumns[':p' . $index++]  = '`nom`';
        }
        if ($this->isColumnModified(BlocTableMap::COL_VALEURS)) {
            $modifiedColumns[':p' . $index++]  = '`valeurs`';
        }
        if ($this->isColumnModified(BlocTableMap::COL_TEXTE)) {
            $modifiedColumns[':p' . $index++]  = '`texte`';
        }
        if ($this->isColumnModified(BlocTableMap::COL_CSS)) {
            $modifiedColumns[':p' . $index++]  = '`css`';
        }
        if ($this->isColumnModified(BlocTableMap::COL_CANAL)) {
            $modifiedColumns[':p' . $index++]  = '`canal`';
        }

        $sql = sprintf(
            'INSERT INTO `com_blocs` (%s) VALUES (%s)',
            implode(', ', $modifiedColumns),
            implode(', ', array_keys($modifiedColumns))
        );

        try {
            $stmt = $con->prepare($sql);
            foreach ($modifiedColumns as $identifier => $columnName) {
                switch ($columnName) {
                    case '`id_bloc`':
                        $stmt->bindValue($identifier, $this->id_bloc, PDO::PARAM_INT);
                        break;
                    case '`nom`':
                        $stmt->bindValue($identifier, $this->nom, PDO::PARAM_STR);
                        break;
                    case '`valeurs`':
                        $stmt->bindValue($identifier, $this->valeurs, PDO::PARAM_STR);
                        break;
                    case '`texte`':
                        $stmt->bindValue($identifier, $this->texte, PDO::PARAM_STR);
                        break;
                    case '`css`':
                        $stmt->bindValue($identifier, $this->css, PDO::PARAM_STR);
                        break;
                    case '`canal`':
                        $stmt->bindValue($identifier, $this->canal, PDO::PARAM_STR);
                        break;
                }
            }
            $stmt->execute();
        } catch (Exception $e) {
            Propel::log($e->getMessage(), Propel::LOG_ERR);
            throw new PropelException(sprintf('Unable to execute INSERT statement [%s]', $sql), 0, $e);
        }

        try {
            $pk = $con->lastInsertId();
        } catch (Exception $e) {
            throw new PropelException('Unable to get autoincrement id.', 0, $e);
        }
        $this->setIdBloc($pk);

        $this->setNew(false);
    }

    /**
     * Update the row in the database.
     *
     * @param      ConnectionInterface $con
     *
     * @return Integer Number of updated rows
     * @see doSave()
     */
    protected function doUpdate(ConnectionInterface $con)
    {
        $selectCriteria = $this->buildPkeyCriteria();
        $valuesCriteria = $this->buildCriteria();

        return $selectCriteria->doUpdate($valuesCriteria, $con);
    }

    /**
     * Retrieves a field from the object by name passed in as a string.
     *
     * @param      string $name name
     * @param      string $type The type of fieldname the $name is of:
     *                     one of the class type constants TableMap::TYPE_PHPNAME, TableMap::TYPE_CAMELNAME
     *                     TableMap::TYPE_COLNAME, TableMap::TYPE_FIELDNAME, TableMap::TYPE_NUM.
     *                     Defaults to TableMap::TYPE_FIELDNAME.
     * @return mixed Value of field.
     */
    public function getByName($name, $type = TableMap::TYPE_FIELDNAME)
    {
        $pos = BlocTableMap::translateFieldName($name, $type, TableMap::TYPE_NUM);
        $field = $this->getByPosition($pos);

        return $field;
    }

    /**
     * Retrieves a field from the object by Position as specified in the xml schema.
     * Zero-based.
     *
     * @param      int $pos position in xml schema
     * @return mixed Value of field at $pos
     */
    public function getByPosition($pos)
    {
        switch ($pos) {
            case 0:
                return $this->getIdBloc();
                break;
            case 1:
                return $this->getNom();
                break;
            case 2:
                return $this->getValeurs();
                break;
            case 3:
                return $this->getTexte();
                break;
            case 4:
                return $this->getCss();
                break;
            case 5:
                return $this->getCanal();
                break;
            default:
                return null;
                break;
        } // switch()
    }

    /**
     * Exports the object as an array.
     *
     * You can specify the key type of the array by passing one of the class
     * type constants.
     *
     * @param     string  $keyType (optional) One of the class type constants TableMap::TYPE_PHPNAME, TableMap::TYPE_CAMELNAME,
     *                    TableMap::TYPE_COLNAME, TableMap::TYPE_FIELDNAME, TableMap::TYPE_NUM.
     *                    Defaults to TableMap::TYPE_FIELDNAME.
     * @param     boolean $includeLazyLoadColumns (optional) Whether to include lazy loaded columns. Defaults to TRUE.
     * @param     array $alreadyDumpedObjects List of objects to skip to avoid recursion
     * @param     boolean $includeForeignObjects (optional) Whether to include hydrated related objects. Default to FALSE.
     *
     * @return array an associative array containing the field names (as keys) and field values
     */
    public function toArray($keyType = TableMap::TYPE_FIELDNAME, $includeLazyLoadColumns = true, $alreadyDumpedObjects = array(), $includeForeignObjects = false)
    {

        if (isset($alreadyDumpedObjects['Bloc'][$this->hashCode()])) {
            return '*RECURSION*';
        }
        $alreadyDumpedObjects['Bloc'][$this->hashCode()] = true;
        $keys = BlocTableMap::getFieldNames($keyType);
        $result = array(
            $keys[0] => $this->getIdBloc(),
            $keys[1] => $this->getNom(),
            $keys[2] => $this->getValeurs(),
            $keys[3] => $this->getTexte(),
            $keys[4] => $this->getCss(),
            $keys[5] => $this->getCanal(),
        );
        $virtualColumns = $this->virtualColumns;
        foreach ($virtualColumns as $key => $virtualColumn) {
            $result[$key] = $virtualColumn;
        }

        if ($includeForeignObjects) {
            if (null !== $this->collCompositionsBlocs) {

                switch ($keyType) {
                    case TableMap::TYPE_CAMELNAME:
                        $key = 'compositionsBlocs';
                        break;
                    case TableMap::TYPE_FIELDNAME:
                        $key = 'com_compositions_blocss';
                        break;
                    default:
                        $key = 'CompositionsBlocs';
                }

                $result[$key] = $this->collCompositionsBlocs->toArray(null, false, $keyType, $includeLazyLoadColumns, $alreadyDumpedObjects);
            }
        }

        return $result;
    }

    /**
     * Sets a field from the object by name passed in as a string.
     *
     * @param  string $name
     * @param  mixed  $value field value
     * @param  string $type The type of fieldname the $name is of:
     *                one of the class type constants TableMap::TYPE_PHPNAME, TableMap::TYPE_CAMELNAME
     *                TableMap::TYPE_COLNAME, TableMap::TYPE_FIELDNAME, TableMap::TYPE_NUM.
     *                Defaults to TableMap::TYPE_FIELDNAME.
     * @return $this|\Bloc
     */
    public function setByName($name, $value, $type = TableMap::TYPE_FIELDNAME)
    {
        $pos = BlocTableMap::translateFieldName($name, $type, TableMap::TYPE_NUM);

        return $this->setByPosition($pos, $value);
    }

    /**
     * Sets a field from the object by Position as specified in the xml schema.
     * Zero-based.
     *
     * @param  int $pos position in xml schema
     * @param  mixed $value field value
     * @return $this|\Bloc
     */
    public function setByPosition($pos, $value)
    {
        switch ($pos) {
            case 0:
                $this->setIdBloc($value);
                break;
            case 1:
                $this->setNom($value);
                break;
            case 2:
                $this->setValeurs($value);
                break;
            case 3:
                $this->setTexte($value);
                break;
            case 4:
                $this->setCss($value);
                break;
            case 5:
                $this->setCanal($value);
                break;
        } // switch()

        return $this;
    }

    /**
     * Populates the object using an array.
     *
     * This is particularly useful when populating an object from one of the
     * request arrays (e.g. $_POST).  This method goes through the column
     * names, checking to see whether a matching key exists in populated
     * array. If so the setByName() method is called for that column.
     *
     * You can specify the key type of the array by additionally passing one
     * of the class type constants TableMap::TYPE_PHPNAME, TableMap::TYPE_CAMELNAME,
     * TableMap::TYPE_COLNAME, TableMap::TYPE_FIELDNAME, TableMap::TYPE_NUM.
     * The default key type is the column's TableMap::TYPE_FIELDNAME.
     *
     * @param      array  $arr     An array to populate the object from.
     * @param      string $keyType The type of keys the array uses.
     * @return void
     */
    public function fromArray($arr, $keyType = TableMap::TYPE_FIELDNAME)
    {
        $keys = BlocTableMap::getFieldNames($keyType);

        if (array_key_exists($keys[0], $arr)) {
            $this->setIdBloc($arr[$keys[0]]);
        }
        if (array_key_exists($keys[1], $arr)) {
            $this->setNom($arr[$keys[1]]);
        }
        if (array_key_exists($keys[2], $arr)) {
            $this->setValeurs($arr[$keys[2]]);
        }
        if (array_key_exists($keys[3], $arr)) {
            $this->setTexte($arr[$keys[3]]);
        }
        if (array_key_exists($keys[4], $arr)) {
            $this->setCss($arr[$keys[4]]);
        }
        if (array_key_exists($keys[5], $arr)) {
            $this->setCanal($arr[$keys[5]]);
        }
    }

     /**
     * Populate the current object from a string, using a given parser format
     * <code>
     * $book = new Book();
     * $book->importFrom('JSON', '{"Id":9012,"Title":"Don Juan","ISBN":"0140422161","Price":12.99,"PublisherId":1234,"AuthorId":5678}');
     * </code>
     *
     * You can specify the key type of the array by additionally passing one
     * of the class type constants TableMap::TYPE_PHPNAME, TableMap::TYPE_CAMELNAME,
     * TableMap::TYPE_COLNAME, TableMap::TYPE_FIELDNAME, TableMap::TYPE_NUM.
     * The default key type is the column's TableMap::TYPE_FIELDNAME.
     *
     * @param mixed $parser A AbstractParser instance,
     *                       or a format name ('XML', 'YAML', 'JSON', 'CSV')
     * @param string $data The source data to import from
     * @param string $keyType The type of keys the array uses.
     *
     * @return $this|\Bloc The current object, for fluid interface
     */
    public function importFrom($parser, $data, $keyType = TableMap::TYPE_FIELDNAME)
    {
        if (!$parser instanceof AbstractParser) {
            $parser = AbstractParser::getParser($parser);
        }

        $this->fromArray($parser->toArray($data), $keyType);

        return $this;
    }

    /**
     * Build a Criteria object containing the values of all modified columns in this object.
     *
     * @return Criteria The Criteria object containing all modified values.
     */
    public function buildCriteria()
    {
        $criteria = new Criteria(BlocTableMap::DATABASE_NAME);

        if ($this->isColumnModified(BlocTableMap::COL_ID_BLOC)) {
            $criteria->add(BlocTableMap::COL_ID_BLOC, $this->id_bloc);
        }
        if ($this->isColumnModified(BlocTableMap::COL_NOM)) {
            $criteria->add(BlocTableMap::COL_NOM, $this->nom);
        }
        if ($this->isColumnModified(BlocTableMap::COL_VALEURS)) {
            $criteria->add(BlocTableMap::COL_VALEURS, $this->valeurs);
        }
        if ($this->isColumnModified(BlocTableMap::COL_TEXTE)) {
            $criteria->add(BlocTableMap::COL_TEXTE, $this->texte);
        }
        if ($this->isColumnModified(BlocTableMap::COL_CSS)) {
            $criteria->add(BlocTableMap::COL_CSS, $this->css);
        }
        if ($this->isColumnModified(BlocTableMap::COL_CANAL)) {
            $criteria->add(BlocTableMap::COL_CANAL, $this->canal);
        }

        return $criteria;
    }

    /**
     * Builds a Criteria object containing the primary key for this object.
     *
     * Unlike buildCriteria() this method includes the primary key values regardless
     * of whether or not they have been modified.
     *
     * @throws LogicException if no primary key is defined
     *
     * @return Criteria The Criteria object containing value(s) for primary key(s).
     */
    public function buildPkeyCriteria()
    {
        $criteria = ChildBlocQuery::create();
        $criteria->add(BlocTableMap::COL_ID_BLOC, $this->id_bloc);

        return $criteria;
    }

    /**
     * If the primary key is not null, return the hashcode of the
     * primary key. Otherwise, return the hash code of the object.
     *
     * @return int Hashcode
     */
    public function hashCode()
    {
        $validPk = null !== $this->getIdBloc();

        $validPrimaryKeyFKs = 0;
        $primaryKeyFKs = [];

        if ($validPk) {
            return crc32(json_encode($this->getPrimaryKey(), JSON_UNESCAPED_UNICODE));
        } elseif ($validPrimaryKeyFKs) {
            return crc32(json_encode($primaryKeyFKs, JSON_UNESCAPED_UNICODE));
        }

        return spl_object_hash($this);
    }

    /**
     * Returns the primary key for this object (row).
     * @return int
     */
    public function getPrimaryKey()
    {
        return $this->getIdBloc();
    }

    /**
     * Generic method to set the primary key (id_bloc column).
     *
     * @param       int $key Primary key.
     * @return void
     */
    public function setPrimaryKey($key)
    {
        $this->setIdBloc($key);
    }

    /**
     * Returns true if the primary key for this object is null.
     * @return boolean
     */
    public function isPrimaryKeyNull()
    {
        return null === $this->getIdBloc();
    }

    /**
     * Sets contents of passed object to values from current object.
     *
     * If desired, this method can also make copies of all associated (fkey referrers)
     * objects.
     *
     * @param      object $copyObj An object of \Bloc (or compatible) type.
     * @param      boolean $deepCopy Whether to also copy all rows that refer (by fkey) to the current row.
     * @param      boolean $makeNew Whether to reset autoincrement PKs and make the object new.
     * @throws PropelException
     */
    public function copyInto($copyObj, $deepCopy = false, $makeNew = true)
    {
        $copyObj->setNom($this->getNom());
        $copyObj->setValeurs($this->getValeurs());
        $copyObj->setTexte($this->getTexte());
        $copyObj->setCss($this->getCss());
        $copyObj->setCanal($this->getCanal());

        if ($deepCopy) {
            // important: temporarily setNew(false) because this affects the behavior of
            // the getter/setter methods for fkey referrer objects.
            $copyObj->setNew(false);

            foreach ($this->getCompositionsBlocs() as $relObj) {
                if ($relObj !== $this) {  // ensure that we don't try to copy a reference to ourselves
                    $copyObj->addCompositionsBloc($relObj->copy($deepCopy));
                }
            }

        } // if ($deepCopy)

        if ($makeNew) {
            $copyObj->setNew(true);
            $copyObj->setIdBloc(NULL); // this is a auto-increment column, so set to default value
        }
    }

    /**
     * Makes a copy of this object that will be inserted as a new row in table when saved.
     * It creates a new object filling in the simple attributes, but skipping any primary
     * keys that are defined for the table.
     *
     * If desired, this method can also make copies of all associated (fkey referrers)
     * objects.
     *
     * @param  boolean $deepCopy Whether to also copy all rows that refer (by fkey) to the current row.
     * @return \Bloc Clone of current object.
     * @throws PropelException
     */
    public function copy($deepCopy = false)
    {
        // we use get_class(), because this might be a subclass
        $clazz = get_class($this);
        $copyObj = new $clazz();
        $this->copyInto($copyObj, $deepCopy);

        return $copyObj;
    }


    /**
     * Initializes a collection based on the name of a relation.
     * Avoids crafting an 'init[$relationName]s' method name
     * that wouldn't work when StandardEnglishPluralizer is used.
     *
     * @param      string $relationName The name of the relation to initialize
     * @return void
     */
    public function initRelation($relationName)
    {
        if ('CompositionsBloc' == $relationName) {
            $this->initCompositionsBlocs();
            return;
        }
    }

    /**
     * Clears out the collCompositionsBlocs collection
     *
     * This does not modify the database; however, it will remove any associated objects, causing
     * them to be refetched by subsequent calls to accessor method.
     *
     * @return void
     * @see        addCompositionsBlocs()
     */
    public function clearCompositionsBlocs()
    {
        $this->collCompositionsBlocs = null; // important to set this to NULL since that means it is uninitialized
    }

    /**
     * Reset is the collCompositionsBlocs collection loaded partially.
     */
    public function resetPartialCompositionsBlocs($v = true)
    {
        $this->collCompositionsBlocsPartial = $v;
    }

    /**
     * Initializes the collCompositionsBlocs collection.
     *
     * By default this just sets the collCompositionsBlocs collection to an empty array (like clearcollCompositionsBlocs());
     * however, you may wish to override this method in your stub class to provide setting appropriate
     * to your application -- for example, setting the initial array to the values stored in database.
     *
     * @param      boolean $overrideExisting If set to true, the method call initializes
     *                                        the collection even if it is not empty
     *
     * @return void
     */
    public function initCompositionsBlocs($overrideExisting = true)
    {
        if (null !== $this->collCompositionsBlocs && !$overrideExisting) {
            return;
        }

        $collectionClassName = CompositionsBlocTableMap::getTableMap()->getCollectionClassName();

        $this->collCompositionsBlocs = new $collectionClassName;
        $this->collCompositionsBlocs->setModel('\CompositionsBloc');
    }

    /**
     * Gets an array of ChildCompositionsBloc objects which contain a foreign key that references this object.
     *
     * If the $criteria is not null, it is used to always fetch the results from the database.
     * Otherwise the results are fetched from the database the first time, then cached.
     * Next time the same method is called without $criteria, the cached collection is returned.
     * If this ChildBloc is new, it will return
     * an empty collection or the current collection; the criteria is ignored on a new object.
     *
     * @param      Criteria $criteria optional Criteria object to narrow the query
     * @param      ConnectionInterface $con optional connection object
     * @return ObjectCollection|ChildCompositionsBloc[] List of ChildCompositionsBloc objects
     * @throws PropelException
     */
    public function getCompositionsBlocs(Criteria $criteria = null, ConnectionInterface $con = null)
    {
        $partial = $this->collCompositionsBlocsPartial && !$this->isNew();
        if (null === $this->collCompositionsBlocs || null !== $criteria  || $partial) {
            if ($this->isNew() && null === $this->collCompositionsBlocs) {
                // return empty collection
                $this->initCompositionsBlocs();
            } else {
                $collCompositionsBlocs = ChildCompositionsBlocQuery::create(null, $criteria)
                    ->filterByBloc($this)
                    ->find($con);

                if (null !== $criteria) {
                    if (false !== $this->collCompositionsBlocsPartial && count($collCompositionsBlocs)) {
                        $this->initCompositionsBlocs(false);

                        foreach ($collCompositionsBlocs as $obj) {
                            if (false == $this->collCompositionsBlocs->contains($obj)) {
                                $this->collCompositionsBlocs->append($obj);
                            }
                        }

                        $this->collCompositionsBlocsPartial = true;
                    }

                    return $collCompositionsBlocs;
                }

                if ($partial && $this->collCompositionsBlocs) {
                    foreach ($this->collCompositionsBlocs as $obj) {
                        if ($obj->isNew()) {
                            $collCompositionsBlocs[] = $obj;
                        }
                    }
                }

                $this->collCompositionsBlocs = $collCompositionsBlocs;
                $this->collCompositionsBlocsPartial = false;
            }
        }

        return $this->collCompositionsBlocs;
    }

    /**
     * Sets a collection of ChildCompositionsBloc objects related by a one-to-many relationship
     * to the current object.
     * It will also schedule objects for deletion based on a diff between old objects (aka persisted)
     * and new objects from the given Propel collection.
     *
     * @param      Collection $compositionsBlocs A Propel collection.
     * @param      ConnectionInterface $con Optional connection object
     * @return $this|ChildBloc The current object (for fluent API support)
     */
    public function setCompositionsBlocs(Collection $compositionsBlocs, ConnectionInterface $con = null)
    {
        /** @var ChildCompositionsBloc[] $compositionsBlocsToDelete */
        $compositionsBlocsToDelete = $this->getCompositionsBlocs(new Criteria(), $con)->diff($compositionsBlocs);


        //since at least one column in the foreign key is at the same time a PK
        //we can not just set a PK to NULL in the lines below. We have to store
        //a backup of all values, so we are able to manipulate these items based on the onDelete value later.
        $this->compositionsBlocsScheduledForDeletion = clone $compositionsBlocsToDelete;

        foreach ($compositionsBlocsToDelete as $compositionsBlocRemoved) {
            $compositionsBlocRemoved->setBloc(null);
        }

        $this->collCompositionsBlocs = null;
        foreach ($compositionsBlocs as $compositionsBloc) {
            $this->addCompositionsBloc($compositionsBloc);
        }

        $this->collCompositionsBlocs = $compositionsBlocs;
        $this->collCompositionsBlocsPartial = false;

        return $this;
    }

    /**
     * Returns the number of related CompositionsBloc objects.
     *
     * @param      Criteria $criteria
     * @param      boolean $distinct
     * @param      ConnectionInterface $con
     * @return int             Count of related CompositionsBloc objects.
     * @throws PropelException
     */
    public function countCompositionsBlocs(Criteria $criteria = null, $distinct = false, ConnectionInterface $con = null)
    {
        $partial = $this->collCompositionsBlocsPartial && !$this->isNew();
        if (null === $this->collCompositionsBlocs || null !== $criteria || $partial) {
            if ($this->isNew() && null === $this->collCompositionsBlocs) {
                return 0;
            }

            if ($partial && !$criteria) {
                return count($this->getCompositionsBlocs());
            }

            $query = ChildCompositionsBlocQuery::create(null, $criteria);
            if ($distinct) {
                $query->distinct();
            }

            return $query
                ->filterByBloc($this)
                ->count($con);
        }

        return count($this->collCompositionsBlocs);
    }

    /**
     * Method called to associate a ChildCompositionsBloc object to this object
     * through the ChildCompositionsBloc foreign key attribute.
     *
     * @param  ChildCompositionsBloc $l ChildCompositionsBloc
     * @return $this|\Bloc The current object (for fluent API support)
     */
    public function addCompositionsBloc(ChildCompositionsBloc $l)
    {
        if ($this->collCompositionsBlocs === null) {
            $this->initCompositionsBlocs();
            $this->collCompositionsBlocsPartial = true;
        }

        if (!$this->collCompositionsBlocs->contains($l)) {
            $this->doAddCompositionsBloc($l);

            if ($this->compositionsBlocsScheduledForDeletion and $this->compositionsBlocsScheduledForDeletion->contains($l)) {
                $this->compositionsBlocsScheduledForDeletion->remove($this->compositionsBlocsScheduledForDeletion->search($l));
            }
        }

        return $this;
    }

    /**
     * @param ChildCompositionsBloc $compositionsBloc The ChildCompositionsBloc object to add.
     */
    protected function doAddCompositionsBloc(ChildCompositionsBloc $compositionsBloc)
    {
        $this->collCompositionsBlocs[]= $compositionsBloc;
        $compositionsBloc->setBloc($this);
    }

    /**
     * @param  ChildCompositionsBloc $compositionsBloc The ChildCompositionsBloc object to remove.
     * @return $this|ChildBloc The current object (for fluent API support)
     */
    public function removeCompositionsBloc(ChildCompositionsBloc $compositionsBloc)
    {
        if ($this->getCompositionsBlocs()->contains($compositionsBloc)) {
            $pos = $this->collCompositionsBlocs->search($compositionsBloc);
            $this->collCompositionsBlocs->remove($pos);
            if (null === $this->compositionsBlocsScheduledForDeletion) {
                $this->compositionsBlocsScheduledForDeletion = clone $this->collCompositionsBlocs;
                $this->compositionsBlocsScheduledForDeletion->clear();
            }
            $this->compositionsBlocsScheduledForDeletion[]= clone $compositionsBloc;
            $compositionsBloc->setBloc(null);
        }

        return $this;
    }


    /**
     * If this collection has already been initialized with
     * an identical criteria, it returns the collection.
     * Otherwise if this Bloc is new, it will return
     * an empty collection; or if this Bloc has previously
     * been saved, it will retrieve related CompositionsBlocs from storage.
     *
     * This method is protected by default in order to keep the public
     * api reasonable.  You can provide public methods for those you
     * actually need in Bloc.
     *
     * @param      Criteria $criteria optional Criteria object to narrow the query
     * @param      ConnectionInterface $con optional connection object
     * @param      string $joinBehavior optional join type to use (defaults to Criteria::LEFT_JOIN)
     * @return ObjectCollection|ChildCompositionsBloc[] List of ChildCompositionsBloc objects
     */
    public function getCompositionsBlocsJoinComposition(Criteria $criteria = null, ConnectionInterface $con = null, $joinBehavior = Criteria::LEFT_JOIN)
    {
        $query = ChildCompositionsBlocQuery::create(null, $criteria);
        $query->joinWith('Composition', $joinBehavior);

        return $this->getCompositionsBlocs($query, $con);
    }

    /**
     * Clears out the collCompositions collection
     *
     * This does not modify the database; however, it will remove any associated objects, causing
     * them to be refetched by subsequent calls to accessor method.
     *
     * @return void
     * @see        addCompositions()
     */
    public function clearCompositions()
    {
        $this->collCompositions = null; // important to set this to NULL since that means it is uninitialized
    }

    /**
     * Initializes the collCompositions crossRef collection.
     *
     * By default this just sets the collCompositions collection to an empty collection (like clearCompositions());
     * however, you may wish to override this method in your stub class to provide setting appropriate
     * to your application -- for example, setting the initial array to the values stored in database.
     *
     * @return void
     */
    public function initCompositions()
    {
        $collectionClassName = CompositionsBlocTableMap::getTableMap()->getCollectionClassName();

        $this->collCompositions = new $collectionClassName;
        $this->collCompositionsPartial = true;
        $this->collCompositions->setModel('\Composition');
    }

    /**
     * Checks if the collCompositions collection is loaded.
     *
     * @return bool
     */
    public function isCompositionsLoaded()
    {
        return null !== $this->collCompositions;
    }

    /**
     * Gets a collection of ChildComposition objects related by a many-to-many relationship
     * to the current object by way of the com_compositions_blocs cross-reference table.
     *
     * If the $criteria is not null, it is used to always fetch the results from the database.
     * Otherwise the results are fetched from the database the first time, then cached.
     * Next time the same method is called without $criteria, the cached collection is returned.
     * If this ChildBloc is new, it will return
     * an empty collection or the current collection; the criteria is ignored on a new object.
     *
     * @param      Criteria $criteria Optional query object to filter the query
     * @param      ConnectionInterface $con Optional connection object
     *
     * @return ObjectCollection|ChildComposition[] List of ChildComposition objects
     */
    public function getCompositions(Criteria $criteria = null, ConnectionInterface $con = null)
    {
        $partial = $this->collCompositionsPartial && !$this->isNew();
        if (null === $this->collCompositions || null !== $criteria || $partial) {
            if ($this->isNew()) {
                // return empty collection
                if (null === $this->collCompositions) {
                    $this->initCompositions();
                }
            } else {

                $query = ChildCompositionQuery::create(null, $criteria)
                    ->filterByBloc($this);
                $collCompositions = $query->find($con);
                if (null !== $criteria) {
                    return $collCompositions;
                }

                if ($partial && $this->collCompositions) {
                    //make sure that already added objects gets added to the list of the database.
                    foreach ($this->collCompositions as $obj) {
                        if (!$collCompositions->contains($obj)) {
                            $collCompositions[] = $obj;
                        }
                    }
                }

                $this->collCompositions = $collCompositions;
                $this->collCompositionsPartial = false;
            }
        }

        return $this->collCompositions;
    }

    /**
     * Sets a collection of Composition objects related by a many-to-many relationship
     * to the current object by way of the com_compositions_blocs cross-reference table.
     * It will also schedule objects for deletion based on a diff between old objects (aka persisted)
     * and new objects from the given Propel collection.
     *
     * @param  Collection $compositions A Propel collection.
     * @param  ConnectionInterface $con Optional connection object
     * @return $this|ChildBloc The current object (for fluent API support)
     */
    public function setCompositions(Collection $compositions, ConnectionInterface $con = null)
    {
        $this->clearCompositions();
        $currentCompositions = $this->getCompositions();

        $compositionsScheduledForDeletion = $currentCompositions->diff($compositions);

        foreach ($compositionsScheduledForDeletion as $toDelete) {
            $this->removeComposition($toDelete);
        }

        foreach ($compositions as $composition) {
            if (!$currentCompositions->contains($composition)) {
                $this->doAddComposition($composition);
            }
        }

        $this->collCompositionsPartial = false;
        $this->collCompositions = $compositions;

        return $this;
    }

    /**
     * Gets the number of Composition objects related by a many-to-many relationship
     * to the current object by way of the com_compositions_blocs cross-reference table.
     *
     * @param      Criteria $criteria Optional query object to filter the query
     * @param      boolean $distinct Set to true to force count distinct
     * @param      ConnectionInterface $con Optional connection object
     *
     * @return int the number of related Composition objects
     */
    public function countCompositions(Criteria $criteria = null, $distinct = false, ConnectionInterface $con = null)
    {
        $partial = $this->collCompositionsPartial && !$this->isNew();
        if (null === $this->collCompositions || null !== $criteria || $partial) {
            if ($this->isNew() && null === $this->collCompositions) {
                return 0;
            } else {

                if ($partial && !$criteria) {
                    return count($this->getCompositions());
                }

                $query = ChildCompositionQuery::create(null, $criteria);
                if ($distinct) {
                    $query->distinct();
                }

                return $query
                    ->filterByBloc($this)
                    ->count($con);
            }
        } else {
            return count($this->collCompositions);
        }
    }

    /**
     * Associate a ChildComposition to this object
     * through the com_compositions_blocs cross reference table.
     *
     * @param ChildComposition $composition
     * @return ChildBloc The current object (for fluent API support)
     */
    public function addComposition(ChildComposition $composition)
    {
        if ($this->collCompositions === null) {
            $this->initCompositions();
        }

        if (!$this->getCompositions()->contains($composition)) {
            // only add it if the **same** object is not already associated
            $this->collCompositions->push($composition);
            $this->doAddComposition($composition);
        }

        return $this;
    }

    /**
     *
     * @param ChildComposition $composition
     */
    protected function doAddComposition(ChildComposition $composition)
    {
        $compositionsBloc = new ChildCompositionsBloc();

        $compositionsBloc->setComposition($composition);

        $compositionsBloc->setBloc($this);

        $this->addCompositionsBloc($compositionsBloc);

        // set the back reference to this object directly as using provided method either results
        // in endless loop or in multiple relations
        if (!$composition->isBlocsLoaded()) {
            $composition->initBlocs();
            $composition->getBlocs()->push($this);
        } elseif (!$composition->getBlocs()->contains($this)) {
            $composition->getBlocs()->push($this);
        }

    }

    /**
     * Remove composition of this object
     * through the com_compositions_blocs cross reference table.
     *
     * @param ChildComposition $composition
     * @return ChildBloc The current object (for fluent API support)
     */
    public function removeComposition(ChildComposition $composition)
    {
        if ($this->getCompositions()->contains($composition)) {
            $compositionsBloc = new ChildCompositionsBloc();
            $compositionsBloc->setComposition($composition);
            if ($composition->isBlocsLoaded()) {
                //remove the back reference if available
                $composition->getBlocs()->removeObject($this);
            }

            $compositionsBloc->setBloc($this);
            $this->removeCompositionsBloc(clone $compositionsBloc);
            $compositionsBloc->clear();

            $this->collCompositions->remove($this->collCompositions->search($composition));

            if (null === $this->compositionsScheduledForDeletion) {
                $this->compositionsScheduledForDeletion = clone $this->collCompositions;
                $this->compositionsScheduledForDeletion->clear();
            }

            $this->compositionsScheduledForDeletion->push($composition);
        }


        return $this;
    }

    /**
     * Clears the current object, sets all attributes to their default values and removes
     * outgoing references as well as back-references (from other objects to this one. Results probably in a database
     * change of those foreign objects when you call `save` there).
     */
    public function clear()
    {
        $this->id_bloc = null;
        $this->nom = null;
        $this->valeurs = null;
        $this->texte = null;
        $this->css = null;
        $this->canal = null;
        $this->alreadyInSave = false;
        $this->clearAllReferences();
        $this->resetModified();
        $this->setNew(true);
        $this->setDeleted(false);
    }

    /**
     * Resets all references and back-references to other model objects or collections of model objects.
     *
     * This method is used to reset all php object references (not the actual reference in the database).
     * Necessary for object serialisation.
     *
     * @param      boolean $deep Whether to also clear the references on all referrer objects.
     */
    public function clearAllReferences($deep = false)
    {
        if ($deep) {
            if ($this->collCompositionsBlocs) {
                foreach ($this->collCompositionsBlocs as $o) {
                    $o->clearAllReferences($deep);
                }
            }
            if ($this->collCompositions) {
                foreach ($this->collCompositions as $o) {
                    $o->clearAllReferences($deep);
                }
            }
        } // if ($deep)

        $this->collCompositionsBlocs = null;
        $this->collCompositions = null;
    }

    /**
     * Return the string representation of this object
     *
     * @return string
     */
    public function __toString()
    {
        return (string) $this->exportTo(BlocTableMap::DEFAULT_STRING_FORMAT);
    }

    /**
     * Code to be run before persisting the object
     * @param  ConnectionInterface $con
     * @return boolean
     */
    public function preSave(ConnectionInterface $con = null)
    {
        if (is_callable('parent::preSave')) {
            return parent::preSave($con);
        }
        return true;
    }

    /**
     * Code to be run after persisting the object
     * @param ConnectionInterface $con
     */
    public function postSave(ConnectionInterface $con = null)
    {
        if (is_callable('parent::postSave')) {
            parent::postSave($con);
        }
    }

    /**
     * Code to be run before inserting to database
     * @param  ConnectionInterface $con
     * @return boolean
     */
    public function preInsert(ConnectionInterface $con = null)
    {
        if (is_callable('parent::preInsert')) {
            return parent::preInsert($con);
        }
        return true;
    }

    /**
     * Code to be run after inserting to database
     * @param ConnectionInterface $con
     */
    public function postInsert(ConnectionInterface $con = null)
    {
        if (is_callable('parent::postInsert')) {
            parent::postInsert($con);
        }
    }

    /**
     * Code to be run before updating the object in database
     * @param  ConnectionInterface $con
     * @return boolean
     */
    public function preUpdate(ConnectionInterface $con = null)
    {
        if (is_callable('parent::preUpdate')) {
            return parent::preUpdate($con);
        }
        return true;
    }

    /**
     * Code to be run after updating the object in database
     * @param ConnectionInterface $con
     */
    public function postUpdate(ConnectionInterface $con = null)
    {
        if (is_callable('parent::postUpdate')) {
            parent::postUpdate($con);
        }
    }

    /**
     * Code to be run before deleting the object in database
     * @param  ConnectionInterface $con
     * @return boolean
     */
    public function preDelete(ConnectionInterface $con = null)
    {
        if (is_callable('parent::preDelete')) {
            return parent::preDelete($con);
        }
        return true;
    }

    /**
     * Code to be run after deleting the object in database
     * @param ConnectionInterface $con
     */
    public function postDelete(ConnectionInterface $con = null)
    {
        if (is_callable('parent::postDelete')) {
            parent::postDelete($con);
        }
    }


    /**
     * Derived method to catches calls to undefined methods.
     *
     * Provides magic import/export method support (fromXML()/toXML(), fromYAML()/toYAML(), etc.).
     * Allows to define default __call() behavior if you overwrite __call()
     *
     * @param string $name
     * @param mixed  $params
     *
     * @return array|string
     */
    public function __call($name, $params)
    {
        if (0 === strpos($name, 'get')) {
            $virtualColumn = substr($name, 3);
            if ($this->hasVirtualColumn($virtualColumn)) {
                return $this->getVirtualColumn($virtualColumn);
            }

            $virtualColumn = lcfirst($virtualColumn);
            if ($this->hasVirtualColumn($virtualColumn)) {
                return $this->getVirtualColumn($virtualColumn);
            }
        }

        if (0 === strpos($name, 'from')) {
            $format = substr($name, 4);

            return $this->importFrom($format, reset($params));
        }

        if (0 === strpos($name, 'to')) {
            $format = substr($name, 2);
            $includeLazyLoadColumns = isset($params[0]) ? $params[0] : true;

            return $this->exportTo($format, $includeLazyLoadColumns);
        }

        throw new BadMethodCallException(sprintf('Call to undefined method: %s.', $name));
    }

}
