<?php

namespace Base;

use \Zonegroupe as ChildZonegroupe;
use \ZonegroupeQuery as ChildZonegroupeQuery;
use \Exception;
use \PDO;
use Map\ZonegroupeTableMap;
use Propel\Runtime\Propel;
use Propel\Runtime\ActiveQuery\Criteria;
use Propel\Runtime\ActiveQuery\ModelCriteria;
use Propel\Runtime\ActiveQuery\ModelJoin;
use Propel\Runtime\Collection\ObjectCollection;
use Propel\Runtime\Connection\ConnectionInterface;
use Propel\Runtime\Exception\PropelException;

/**
 * Base class that represents a query for the 'geo_zonegroupes' table.
 *
 *
 *
 * @method     ChildZonegroupeQuery orderByIdZonegroupe($order = Criteria::ASC) Order by the id_zonegroupe column
 * @method     ChildZonegroupeQuery orderByNom($order = Criteria::ASC) Order by the nom column
 * @method     ChildZonegroupeQuery orderByCreatedAt($order = Criteria::ASC) Order by the created_at column
 * @method     ChildZonegroupeQuery orderByUpdatedAt($order = Criteria::ASC) Order by the updated_at column
 *
 * @method     ChildZonegroupeQuery groupByIdZonegroupe() Group by the id_zonegroupe column
 * @method     ChildZonegroupeQuery groupByNom() Group by the nom column
 * @method     ChildZonegroupeQuery groupByCreatedAt() Group by the created_at column
 * @method     ChildZonegroupeQuery groupByUpdatedAt() Group by the updated_at column
 *
 * @method     ChildZonegroupeQuery leftJoin($relation) Adds a LEFT JOIN clause to the query
 * @method     ChildZonegroupeQuery rightJoin($relation) Adds a RIGHT JOIN clause to the query
 * @method     ChildZonegroupeQuery innerJoin($relation) Adds a INNER JOIN clause to the query
 *
 * @method     ChildZonegroupeQuery leftJoinWith($relation) Adds a LEFT JOIN clause and with to the query
 * @method     ChildZonegroupeQuery rightJoinWith($relation) Adds a RIGHT JOIN clause and with to the query
 * @method     ChildZonegroupeQuery innerJoinWith($relation) Adds a INNER JOIN clause and with to the query
 *
 * @method     ChildZonegroupeQuery leftJoinZone($relationAlias = null) Adds a LEFT JOIN clause to the query using the Zone relation
 * @method     ChildZonegroupeQuery rightJoinZone($relationAlias = null) Adds a RIGHT JOIN clause to the query using the Zone relation
 * @method     ChildZonegroupeQuery innerJoinZone($relationAlias = null) Adds a INNER JOIN clause to the query using the Zone relation
 *
 * @method     ChildZonegroupeQuery joinWithZone($joinType = Criteria::INNER_JOIN) Adds a join clause and with to the query using the Zone relation
 *
 * @method     ChildZonegroupeQuery leftJoinWithZone() Adds a LEFT JOIN clause and with to the query using the Zone relation
 * @method     ChildZonegroupeQuery rightJoinWithZone() Adds a RIGHT JOIN clause and with to the query using the Zone relation
 * @method     ChildZonegroupeQuery innerJoinWithZone() Adds a INNER JOIN clause and with to the query using the Zone relation
 *
 * @method     \ZoneQuery endUse() Finalizes a secondary criteria and merges it with its primary Criteria
 *
 * @method     ChildZonegroupe findOne(ConnectionInterface $con = null) Return the first ChildZonegroupe matching the query
 * @method     ChildZonegroupe findOneOrCreate(ConnectionInterface $con = null) Return the first ChildZonegroupe matching the query, or a new ChildZonegroupe object populated from the query conditions when no match is found
 *
 * @method     ChildZonegroupe findOneByIdZonegroupe(int $id_zonegroupe) Return the first ChildZonegroupe filtered by the id_zonegroupe column
 * @method     ChildZonegroupe findOneByNom(string $nom) Return the first ChildZonegroupe filtered by the nom column
 * @method     ChildZonegroupe findOneByCreatedAt(string $created_at) Return the first ChildZonegroupe filtered by the created_at column
 * @method     ChildZonegroupe findOneByUpdatedAt(string $updated_at) Return the first ChildZonegroupe filtered by the updated_at column *

 * @method     ChildZonegroupe requirePk($key, ConnectionInterface $con = null) Return the ChildZonegroupe by primary key and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildZonegroupe requireOne(ConnectionInterface $con = null) Return the first ChildZonegroupe matching the query and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 *
 * @method     ChildZonegroupe requireOneByIdZonegroupe(int $id_zonegroupe) Return the first ChildZonegroupe filtered by the id_zonegroupe column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildZonegroupe requireOneByNom(string $nom) Return the first ChildZonegroupe filtered by the nom column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildZonegroupe requireOneByCreatedAt(string $created_at) Return the first ChildZonegroupe filtered by the created_at column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildZonegroupe requireOneByUpdatedAt(string $updated_at) Return the first ChildZonegroupe filtered by the updated_at column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 *
 * @method     ChildZonegroupe[]|ObjectCollection find(ConnectionInterface $con = null) Return ChildZonegroupe objects based on current ModelCriteria
 * @method     ChildZonegroupe[]|ObjectCollection findByIdZonegroupe(int $id_zonegroupe) Return ChildZonegroupe objects filtered by the id_zonegroupe column
 * @method     ChildZonegroupe[]|ObjectCollection findByNom(string $nom) Return ChildZonegroupe objects filtered by the nom column
 * @method     ChildZonegroupe[]|ObjectCollection findByCreatedAt(string $created_at) Return ChildZonegroupe objects filtered by the created_at column
 * @method     ChildZonegroupe[]|ObjectCollection findByUpdatedAt(string $updated_at) Return ChildZonegroupe objects filtered by the updated_at column
 * @method     ChildZonegroupe[]|\Propel\Runtime\Util\PropelModelPager paginate($page = 1, $maxPerPage = 10, ConnectionInterface $con = null) Issue a SELECT query based on the current ModelCriteria and uses a page and a maximum number of results per page to compute an offset and a limit
 *
 */
abstract class ZonegroupeQuery extends ModelCriteria
{
    protected $entityNotFoundExceptionClass = '\\Propel\\Runtime\\Exception\\EntityNotFoundException';

    /**
     * Initializes internal state of \Base\ZonegroupeQuery object.
     *
     * @param     string $dbName The database name
     * @param     string $modelName The phpName of a model, e.g. 'Book'
     * @param     string $modelAlias The alias for the model in this query, e.g. 'b'
     */
    public function __construct($dbName = 'simplasso', $modelName = '\\Zonegroupe', $modelAlias = null)
    {
        parent::__construct($dbName, $modelName, $modelAlias);
    }

    /**
     * Returns a new ChildZonegroupeQuery object.
     *
     * @param     string $modelAlias The alias of a model in the query
     * @param     Criteria $criteria Optional Criteria to build the query from
     *
     * @return ChildZonegroupeQuery
     */
    public static function create($modelAlias = null, Criteria $criteria = null)
    {
        if ($criteria instanceof ChildZonegroupeQuery) {
            return $criteria;
        }
        $query = new ChildZonegroupeQuery();
        if (null !== $modelAlias) {
            $query->setModelAlias($modelAlias);
        }
        if ($criteria instanceof Criteria) {
            $query->mergeWith($criteria);
        }

        return $query;
    }

    /**
     * Find object by primary key.
     * Propel uses the instance pool to skip the database if the object exists.
     * Go fast if the query is untouched.
     *
     * <code>
     * $obj  = $c->findPk(12, $con);
     * </code>
     *
     * @param mixed $key Primary key to use for the query
     * @param ConnectionInterface $con an optional connection object
     *
     * @return ChildZonegroupe|array|mixed the result, formatted by the current formatter
     */
    public function findPk($key, ConnectionInterface $con = null)
    {
        if ($key === null) {
            return null;
        }

        if ($con === null) {
            $con = Propel::getServiceContainer()->getReadConnection(ZonegroupeTableMap::DATABASE_NAME);
        }

        $this->basePreSelect($con);

        if (
            $this->formatter || $this->modelAlias || $this->with || $this->select
            || $this->selectColumns || $this->asColumns || $this->selectModifiers
            || $this->map || $this->having || $this->joins
        ) {
            return $this->findPkComplex($key, $con);
        }

        if ((null !== ($obj = ZonegroupeTableMap::getInstanceFromPool(null === $key || is_scalar($key) || is_callable([$key, '__toString']) ? (string) $key : $key)))) {
            // the object is already in the instance pool
            return $obj;
        }

        return $this->findPkSimple($key, $con);
    }

    /**
     * Find object by primary key using raw SQL to go fast.
     * Bypass doSelect() and the object formatter by using generated code.
     *
     * @param     mixed $key Primary key to use for the query
     * @param     ConnectionInterface $con A connection object
     *
     * @throws \Propel\Runtime\Exception\PropelException
     *
     * @return ChildZonegroupe A model object, or null if the key is not found
     */
    protected function findPkSimple($key, ConnectionInterface $con)
    {
        $sql = 'SELECT `id_zonegroupe`, `nom`, `created_at`, `updated_at` FROM `geo_zonegroupes` WHERE `id_zonegroupe` = :p0';
        try {
            $stmt = $con->prepare($sql);
            $stmt->bindValue(':p0', $key, PDO::PARAM_INT);
            $stmt->execute();
        } catch (Exception $e) {
            Propel::log($e->getMessage(), Propel::LOG_ERR);
            throw new PropelException(sprintf('Unable to execute SELECT statement [%s]', $sql), 0, $e);
        }
        $obj = null;
        if ($row = $stmt->fetch(\PDO::FETCH_NUM)) {
            /** @var ChildZonegroupe $obj */
            $obj = new ChildZonegroupe();
            $obj->hydrate($row);
            ZonegroupeTableMap::addInstanceToPool($obj, null === $key || is_scalar($key) || is_callable([$key, '__toString']) ? (string) $key : $key);
        }
        $stmt->closeCursor();

        return $obj;
    }

    /**
     * Find object by primary key.
     *
     * @param     mixed $key Primary key to use for the query
     * @param     ConnectionInterface $con A connection object
     *
     * @return ChildZonegroupe|array|mixed the result, formatted by the current formatter
     */
    protected function findPkComplex($key, ConnectionInterface $con)
    {
        // As the query uses a PK condition, no limit(1) is necessary.
        $criteria = $this->isKeepQuery() ? clone $this : $this;
        $dataFetcher = $criteria
            ->filterByPrimaryKey($key)
            ->doSelect($con);

        return $criteria->getFormatter()->init($criteria)->formatOne($dataFetcher);
    }

    /**
     * Find objects by primary key
     * <code>
     * $objs = $c->findPks(array(12, 56, 832), $con);
     * </code>
     * @param     array $keys Primary keys to use for the query
     * @param     ConnectionInterface $con an optional connection object
     *
     * @return ObjectCollection|array|mixed the list of results, formatted by the current formatter
     */
    public function findPks($keys, ConnectionInterface $con = null)
    {
        if (null === $con) {
            $con = Propel::getServiceContainer()->getReadConnection($this->getDbName());
        }
        $this->basePreSelect($con);
        $criteria = $this->isKeepQuery() ? clone $this : $this;
        $dataFetcher = $criteria
            ->filterByPrimaryKeys($keys)
            ->doSelect($con);

        return $criteria->getFormatter()->init($criteria)->format($dataFetcher);
    }

    /**
     * Filter the query by primary key
     *
     * @param     mixed $key Primary key to use for the query
     *
     * @return $this|ChildZonegroupeQuery The current query, for fluid interface
     */
    public function filterByPrimaryKey($key)
    {

        return $this->addUsingAlias(ZonegroupeTableMap::COL_ID_ZONEGROUPE, $key, Criteria::EQUAL);
    }

    /**
     * Filter the query by a list of primary keys
     *
     * @param     array $keys The list of primary key to use for the query
     *
     * @return $this|ChildZonegroupeQuery The current query, for fluid interface
     */
    public function filterByPrimaryKeys($keys)
    {

        return $this->addUsingAlias(ZonegroupeTableMap::COL_ID_ZONEGROUPE, $keys, Criteria::IN);
    }

    /**
     * Filter the query on the id_zonegroupe column
     *
     * Example usage:
     * <code>
     * $query->filterByIdZonegroupe(1234); // WHERE id_zonegroupe = 1234
     * $query->filterByIdZonegroupe(array(12, 34)); // WHERE id_zonegroupe IN (12, 34)
     * $query->filterByIdZonegroupe(array('min' => 12)); // WHERE id_zonegroupe > 12
     * </code>
     *
     * @param     mixed $idZonegroupe The value to use as filter.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildZonegroupeQuery The current query, for fluid interface
     */
    public function filterByIdZonegroupe($idZonegroupe = null, $comparison = null)
    {
        if (is_array($idZonegroupe)) {
            $useMinMax = false;
            if (isset($idZonegroupe['min'])) {
                $this->addUsingAlias(ZonegroupeTableMap::COL_ID_ZONEGROUPE, $idZonegroupe['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($idZonegroupe['max'])) {
                $this->addUsingAlias(ZonegroupeTableMap::COL_ID_ZONEGROUPE, $idZonegroupe['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(ZonegroupeTableMap::COL_ID_ZONEGROUPE, $idZonegroupe, $comparison);
    }

    /**
     * Filter the query on the nom column
     *
     * Example usage:
     * <code>
     * $query->filterByNom('fooValue');   // WHERE nom = 'fooValue'
     * $query->filterByNom('%fooValue%', Criteria::LIKE); // WHERE nom LIKE '%fooValue%'
     * </code>
     *
     * @param     string $nom The value to use as filter.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildZonegroupeQuery The current query, for fluid interface
     */
    public function filterByNom($nom = null, $comparison = null)
    {
        if (null === $comparison) {
            if (is_array($nom)) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(ZonegroupeTableMap::COL_NOM, $nom, $comparison);
    }

    /**
     * Filter the query on the created_at column
     *
     * Example usage:
     * <code>
     * $query->filterByCreatedAt('2011-03-14'); // WHERE created_at = '2011-03-14'
     * $query->filterByCreatedAt('now'); // WHERE created_at = '2011-03-14'
     * $query->filterByCreatedAt(array('max' => 'yesterday')); // WHERE created_at > '2011-03-13'
     * </code>
     *
     * @param     mixed $createdAt The value to use as filter.
     *              Values can be integers (unix timestamps), DateTime objects, or strings.
     *              Empty strings are treated as NULL.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildZonegroupeQuery The current query, for fluid interface
     */
    public function filterByCreatedAt($createdAt = null, $comparison = null)
    {
        if (is_array($createdAt)) {
            $useMinMax = false;
            if (isset($createdAt['min'])) {
                $this->addUsingAlias(ZonegroupeTableMap::COL_CREATED_AT, $createdAt['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($createdAt['max'])) {
                $this->addUsingAlias(ZonegroupeTableMap::COL_CREATED_AT, $createdAt['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(ZonegroupeTableMap::COL_CREATED_AT, $createdAt, $comparison);
    }

    /**
     * Filter the query on the updated_at column
     *
     * Example usage:
     * <code>
     * $query->filterByUpdatedAt('2011-03-14'); // WHERE updated_at = '2011-03-14'
     * $query->filterByUpdatedAt('now'); // WHERE updated_at = '2011-03-14'
     * $query->filterByUpdatedAt(array('max' => 'yesterday')); // WHERE updated_at > '2011-03-13'
     * </code>
     *
     * @param     mixed $updatedAt The value to use as filter.
     *              Values can be integers (unix timestamps), DateTime objects, or strings.
     *              Empty strings are treated as NULL.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildZonegroupeQuery The current query, for fluid interface
     */
    public function filterByUpdatedAt($updatedAt = null, $comparison = null)
    {
        if (is_array($updatedAt)) {
            $useMinMax = false;
            if (isset($updatedAt['min'])) {
                $this->addUsingAlias(ZonegroupeTableMap::COL_UPDATED_AT, $updatedAt['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($updatedAt['max'])) {
                $this->addUsingAlias(ZonegroupeTableMap::COL_UPDATED_AT, $updatedAt['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(ZonegroupeTableMap::COL_UPDATED_AT, $updatedAt, $comparison);
    }

    /**
     * Filter the query by a related \Zone object
     *
     * @param \Zone|ObjectCollection $zone the related object to use as filter
     * @param string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return ChildZonegroupeQuery The current query, for fluid interface
     */
    public function filterByZone($zone, $comparison = null)
    {
        if ($zone instanceof \Zone) {
            return $this
                ->addUsingAlias(ZonegroupeTableMap::COL_ID_ZONEGROUPE, $zone->getIdZonegroupe(), $comparison);
        } elseif ($zone instanceof ObjectCollection) {
            return $this
                ->useZoneQuery()
                ->filterByPrimaryKeys($zone->getPrimaryKeys())
                ->endUse();
        } else {
            throw new PropelException('filterByZone() only accepts arguments of type \Zone or Collection');
        }
    }

    /**
     * Adds a JOIN clause to the query using the Zone relation
     *
     * @param     string $relationAlias optional alias for the relation
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return $this|ChildZonegroupeQuery The current query, for fluid interface
     */
    public function joinZone($relationAlias = null, $joinType = Criteria::INNER_JOIN)
    {
        $tableMap = $this->getTableMap();
        $relationMap = $tableMap->getRelation('Zone');

        // create a ModelJoin object for this join
        $join = new ModelJoin();
        $join->setJoinType($joinType);
        $join->setRelationMap($relationMap, $this->useAliasInSQL ? $this->getModelAlias() : null, $relationAlias);
        if ($previousJoin = $this->getPreviousJoin()) {
            $join->setPreviousJoin($previousJoin);
        }

        // add the ModelJoin to the current object
        if ($relationAlias) {
            $this->addAlias($relationAlias, $relationMap->getRightTable()->getName());
            $this->addJoinObject($join, $relationAlias);
        } else {
            $this->addJoinObject($join, 'Zone');
        }

        return $this;
    }

    /**
     * Use the Zone relation Zone object
     *
     * @see useQuery()
     *
     * @param     string $relationAlias optional alias for the relation,
     *                                   to be used as main alias in the secondary query
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return \ZoneQuery A secondary query class using the current class as primary query
     */
    public function useZoneQuery($relationAlias = null, $joinType = Criteria::INNER_JOIN)
    {
        return $this
            ->joinZone($relationAlias, $joinType)
            ->useQuery($relationAlias ? $relationAlias : 'Zone', '\ZoneQuery');
    }

    /**
     * Exclude object from result
     *
     * @param   ChildZonegroupe $zonegroupe Object to remove from the list of results
     *
     * @return $this|ChildZonegroupeQuery The current query, for fluid interface
     */
    public function prune($zonegroupe = null)
    {
        if ($zonegroupe) {
            $this->addUsingAlias(ZonegroupeTableMap::COL_ID_ZONEGROUPE, $zonegroupe->getIdZonegroupe(), Criteria::NOT_EQUAL);
        }

        return $this;
    }

    /**
     * Deletes all rows from the geo_zonegroupes table.
     *
     * @param ConnectionInterface $con the connection to use
     * @return int The number of affected rows (if supported by underlying database driver).
     */
    public function doDeleteAll(ConnectionInterface $con = null)
    {
        if (null === $con) {
            $con = Propel::getServiceContainer()->getWriteConnection(ZonegroupeTableMap::DATABASE_NAME);
        }

        // use transaction because $criteria could contain info
        // for more than one table or we could emulating ON DELETE CASCADE, etc.
        return $con->transaction(function () use ($con) {
            $affectedRows = 0; // initialize var to track total num of affected rows
            $affectedRows += parent::doDeleteAll($con);
            // Because this db requires some delete cascade/set null emulation, we have to
            // clear the cached instance *after* the emulation has happened (since
            // instances get re-added by the select statement contained therein).
            ZonegroupeTableMap::clearInstancePool();
            ZonegroupeTableMap::clearRelatedInstancePool();

            return $affectedRows;
        });
    }

    /**
     * Performs a DELETE on the database based on the current ModelCriteria
     *
     * @param ConnectionInterface $con the connection to use
     * @return int             The number of affected rows (if supported by underlying database driver).  This includes CASCADE-related rows
     *                         if supported by native driver or if emulated using Propel.
     * @throws PropelException Any exceptions caught during processing will be
     *                         rethrown wrapped into a PropelException.
     */
    public function delete(ConnectionInterface $con = null)
    {
        if (null === $con) {
            $con = Propel::getServiceContainer()->getWriteConnection(ZonegroupeTableMap::DATABASE_NAME);
        }

        $criteria = $this;

        // Set the correct dbName
        $criteria->setDbName(ZonegroupeTableMap::DATABASE_NAME);

        // use transaction because $criteria could contain info
        // for more than one table or we could emulating ON DELETE CASCADE, etc.
        return $con->transaction(function () use ($con, $criteria) {
            $affectedRows = 0; // initialize var to track total num of affected rows

            ZonegroupeTableMap::removeInstanceFromPool($criteria);

            $affectedRows += ModelCriteria::delete($con);
            ZonegroupeTableMap::clearRelatedInstancePool();

            return $affectedRows;
        });
    }

    // timestampable behavior

    /**
     * Filter by the latest updated
     *
     * @param      int $nbDays Maximum age of the latest update in days
     *
     * @return     $this|ChildZonegroupeQuery The current query, for fluid interface
     */
    public function recentlyUpdated($nbDays = 7)
    {
        return $this->addUsingAlias(ZonegroupeTableMap::COL_UPDATED_AT, time() - $nbDays * 24 * 60 * 60, Criteria::GREATER_EQUAL);
    }

    /**
     * Order by update date desc
     *
     * @return     $this|ChildZonegroupeQuery The current query, for fluid interface
     */
    public function lastUpdatedFirst()
    {
        return $this->addDescendingOrderByColumn(ZonegroupeTableMap::COL_UPDATED_AT);
    }

    /**
     * Order by update date asc
     *
     * @return     $this|ChildZonegroupeQuery The current query, for fluid interface
     */
    public function firstUpdatedFirst()
    {
        return $this->addAscendingOrderByColumn(ZonegroupeTableMap::COL_UPDATED_AT);
    }

    /**
     * Order by create date desc
     *
     * @return     $this|ChildZonegroupeQuery The current query, for fluid interface
     */
    public function lastCreatedFirst()
    {
        return $this->addDescendingOrderByColumn(ZonegroupeTableMap::COL_CREATED_AT);
    }

    /**
     * Filter by the latest created
     *
     * @param      int $nbDays Maximum age of in days
     *
     * @return     $this|ChildZonegroupeQuery The current query, for fluid interface
     */
    public function recentlyCreated($nbDays = 7)
    {
        return $this->addUsingAlias(ZonegroupeTableMap::COL_CREATED_AT, time() - $nbDays * 24 * 60 * 60, Criteria::GREATER_EQUAL);
    }

    /**
     * Order by create date asc
     *
     * @return     $this|ChildZonegroupeQuery The current query, for fluid interface
     */
    public function firstCreatedFirst()
    {
        return $this->addAscendingOrderByColumn(ZonegroupeTableMap::COL_CREATED_AT);
    }

} // ZonegroupeQuery
