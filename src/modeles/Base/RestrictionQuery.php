<?php

namespace Base;

use \AssoRestrictionsArchive as ChildAssoRestrictionsArchive;
use \Restriction as ChildRestriction;
use \RestrictionQuery as ChildRestrictionQuery;
use \Exception;
use \PDO;
use Map\RestrictionTableMap;
use Propel\Runtime\Propel;
use Propel\Runtime\ActiveQuery\Criteria;
use Propel\Runtime\ActiveQuery\ModelCriteria;
use Propel\Runtime\Collection\ObjectCollection;
use Propel\Runtime\Connection\ConnectionInterface;
use Propel\Runtime\Exception\PropelException;

/**
 * Base class that represents a query for the 'asso_restrictions' table.
 *
 *
 *
 * @method     ChildRestrictionQuery orderByIdRestriction($order = Criteria::ASC) Order by the id_restriction column
 * @method     ChildRestrictionQuery orderByNom($order = Criteria::ASC) Order by the nom column
 * @method     ChildRestrictionQuery orderByNomcourt($order = Criteria::ASC) Order by the nomcourt column
 * @method     ChildRestrictionQuery orderByActif($order = Criteria::ASC) Order by the actif column
 * @method     ChildRestrictionQuery orderByVariables($order = Criteria::ASC) Order by the variables column
 * @method     ChildRestrictionQuery orderByCreatedAt($order = Criteria::ASC) Order by the created_at column
 * @method     ChildRestrictionQuery orderByUpdatedAt($order = Criteria::ASC) Order by the updated_at column
 *
 * @method     ChildRestrictionQuery groupByIdRestriction() Group by the id_restriction column
 * @method     ChildRestrictionQuery groupByNom() Group by the nom column
 * @method     ChildRestrictionQuery groupByNomcourt() Group by the nomcourt column
 * @method     ChildRestrictionQuery groupByActif() Group by the actif column
 * @method     ChildRestrictionQuery groupByVariables() Group by the variables column
 * @method     ChildRestrictionQuery groupByCreatedAt() Group by the created_at column
 * @method     ChildRestrictionQuery groupByUpdatedAt() Group by the updated_at column
 *
 * @method     ChildRestrictionQuery leftJoin($relation) Adds a LEFT JOIN clause to the query
 * @method     ChildRestrictionQuery rightJoin($relation) Adds a RIGHT JOIN clause to the query
 * @method     ChildRestrictionQuery innerJoin($relation) Adds a INNER JOIN clause to the query
 *
 * @method     ChildRestrictionQuery leftJoinWith($relation) Adds a LEFT JOIN clause and with to the query
 * @method     ChildRestrictionQuery rightJoinWith($relation) Adds a RIGHT JOIN clause and with to the query
 * @method     ChildRestrictionQuery innerJoinWith($relation) Adds a INNER JOIN clause and with to the query
 *
 * @method     ChildRestriction findOne(ConnectionInterface $con = null) Return the first ChildRestriction matching the query
 * @method     ChildRestriction findOneOrCreate(ConnectionInterface $con = null) Return the first ChildRestriction matching the query, or a new ChildRestriction object populated from the query conditions when no match is found
 *
 * @method     ChildRestriction findOneByIdRestriction(string $id_restriction) Return the first ChildRestriction filtered by the id_restriction column
 * @method     ChildRestriction findOneByNom(string $nom) Return the first ChildRestriction filtered by the nom column
 * @method     ChildRestriction findOneByNomcourt(string $nomcourt) Return the first ChildRestriction filtered by the nomcourt column
 * @method     ChildRestriction findOneByActif(int $actif) Return the first ChildRestriction filtered by the actif column
 * @method     ChildRestriction findOneByVariables(string $variables) Return the first ChildRestriction filtered by the variables column
 * @method     ChildRestriction findOneByCreatedAt(string $created_at) Return the first ChildRestriction filtered by the created_at column
 * @method     ChildRestriction findOneByUpdatedAt(string $updated_at) Return the first ChildRestriction filtered by the updated_at column *

 * @method     ChildRestriction requirePk($key, ConnectionInterface $con = null) Return the ChildRestriction by primary key and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildRestriction requireOne(ConnectionInterface $con = null) Return the first ChildRestriction matching the query and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 *
 * @method     ChildRestriction requireOneByIdRestriction(string $id_restriction) Return the first ChildRestriction filtered by the id_restriction column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildRestriction requireOneByNom(string $nom) Return the first ChildRestriction filtered by the nom column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildRestriction requireOneByNomcourt(string $nomcourt) Return the first ChildRestriction filtered by the nomcourt column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildRestriction requireOneByActif(int $actif) Return the first ChildRestriction filtered by the actif column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildRestriction requireOneByVariables(string $variables) Return the first ChildRestriction filtered by the variables column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildRestriction requireOneByCreatedAt(string $created_at) Return the first ChildRestriction filtered by the created_at column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildRestriction requireOneByUpdatedAt(string $updated_at) Return the first ChildRestriction filtered by the updated_at column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 *
 * @method     ChildRestriction[]|ObjectCollection find(ConnectionInterface $con = null) Return ChildRestriction objects based on current ModelCriteria
 * @method     ChildRestriction[]|ObjectCollection findByIdRestriction(string $id_restriction) Return ChildRestriction objects filtered by the id_restriction column
 * @method     ChildRestriction[]|ObjectCollection findByNom(string $nom) Return ChildRestriction objects filtered by the nom column
 * @method     ChildRestriction[]|ObjectCollection findByNomcourt(string $nomcourt) Return ChildRestriction objects filtered by the nomcourt column
 * @method     ChildRestriction[]|ObjectCollection findByActif(int $actif) Return ChildRestriction objects filtered by the actif column
 * @method     ChildRestriction[]|ObjectCollection findByVariables(string $variables) Return ChildRestriction objects filtered by the variables column
 * @method     ChildRestriction[]|ObjectCollection findByCreatedAt(string $created_at) Return ChildRestriction objects filtered by the created_at column
 * @method     ChildRestriction[]|ObjectCollection findByUpdatedAt(string $updated_at) Return ChildRestriction objects filtered by the updated_at column
 * @method     ChildRestriction[]|\Propel\Runtime\Util\PropelModelPager paginate($page = 1, $maxPerPage = 10, ConnectionInterface $con = null) Issue a SELECT query based on the current ModelCriteria and uses a page and a maximum number of results per page to compute an offset and a limit
 *
 */
abstract class RestrictionQuery extends ModelCriteria
{

    // archivable behavior
    protected $archiveOnDelete = true;
protected $entityNotFoundExceptionClass = '\\Propel\\Runtime\\Exception\\EntityNotFoundException';

    /**
     * Initializes internal state of \Base\RestrictionQuery object.
     *
     * @param     string $dbName The database name
     * @param     string $modelName The phpName of a model, e.g. 'Book'
     * @param     string $modelAlias The alias for the model in this query, e.g. 'b'
     */
    public function __construct($dbName = 'simplasso', $modelName = '\\Restriction', $modelAlias = null)
    {
        parent::__construct($dbName, $modelName, $modelAlias);
    }

    /**
     * Returns a new ChildRestrictionQuery object.
     *
     * @param     string $modelAlias The alias of a model in the query
     * @param     Criteria $criteria Optional Criteria to build the query from
     *
     * @return ChildRestrictionQuery
     */
    public static function create($modelAlias = null, Criteria $criteria = null)
    {
        if ($criteria instanceof ChildRestrictionQuery) {
            return $criteria;
        }
        $query = new ChildRestrictionQuery();
        if (null !== $modelAlias) {
            $query->setModelAlias($modelAlias);
        }
        if ($criteria instanceof Criteria) {
            $query->mergeWith($criteria);
        }

        return $query;
    }

    /**
     * Find object by primary key.
     * Propel uses the instance pool to skip the database if the object exists.
     * Go fast if the query is untouched.
     *
     * <code>
     * $obj  = $c->findPk(12, $con);
     * </code>
     *
     * @param mixed $key Primary key to use for the query
     * @param ConnectionInterface $con an optional connection object
     *
     * @return ChildRestriction|array|mixed the result, formatted by the current formatter
     */
    public function findPk($key, ConnectionInterface $con = null)
    {
        if ($key === null) {
            return null;
        }

        if ($con === null) {
            $con = Propel::getServiceContainer()->getReadConnection(RestrictionTableMap::DATABASE_NAME);
        }

        $this->basePreSelect($con);

        if (
            $this->formatter || $this->modelAlias || $this->with || $this->select
            || $this->selectColumns || $this->asColumns || $this->selectModifiers
            || $this->map || $this->having || $this->joins
        ) {
            return $this->findPkComplex($key, $con);
        }

        if ((null !== ($obj = RestrictionTableMap::getInstanceFromPool(null === $key || is_scalar($key) || is_callable([$key, '__toString']) ? (string) $key : $key)))) {
            // the object is already in the instance pool
            return $obj;
        }

        return $this->findPkSimple($key, $con);
    }

    /**
     * Find object by primary key using raw SQL to go fast.
     * Bypass doSelect() and the object formatter by using generated code.
     *
     * @param     mixed $key Primary key to use for the query
     * @param     ConnectionInterface $con A connection object
     *
     * @throws \Propel\Runtime\Exception\PropelException
     *
     * @return ChildRestriction A model object, or null if the key is not found
     */
    protected function findPkSimple($key, ConnectionInterface $con)
    {
        $sql = 'SELECT `id_restriction`, `nom`, `nomcourt`, `actif`, `variables`, `created_at`, `updated_at` FROM `asso_restrictions` WHERE `id_restriction` = :p0';
        try {
            $stmt = $con->prepare($sql);
            $stmt->bindValue(':p0', $key, PDO::PARAM_INT);
            $stmt->execute();
        } catch (Exception $e) {
            Propel::log($e->getMessage(), Propel::LOG_ERR);
            throw new PropelException(sprintf('Unable to execute SELECT statement [%s]', $sql), 0, $e);
        }
        $obj = null;
        if ($row = $stmt->fetch(\PDO::FETCH_NUM)) {
            /** @var ChildRestriction $obj */
            $obj = new ChildRestriction();
            $obj->hydrate($row);
            RestrictionTableMap::addInstanceToPool($obj, null === $key || is_scalar($key) || is_callable([$key, '__toString']) ? (string) $key : $key);
        }
        $stmt->closeCursor();

        return $obj;
    }

    /**
     * Find object by primary key.
     *
     * @param     mixed $key Primary key to use for the query
     * @param     ConnectionInterface $con A connection object
     *
     * @return ChildRestriction|array|mixed the result, formatted by the current formatter
     */
    protected function findPkComplex($key, ConnectionInterface $con)
    {
        // As the query uses a PK condition, no limit(1) is necessary.
        $criteria = $this->isKeepQuery() ? clone $this : $this;
        $dataFetcher = $criteria
            ->filterByPrimaryKey($key)
            ->doSelect($con);

        return $criteria->getFormatter()->init($criteria)->formatOne($dataFetcher);
    }

    /**
     * Find objects by primary key
     * <code>
     * $objs = $c->findPks(array(12, 56, 832), $con);
     * </code>
     * @param     array $keys Primary keys to use for the query
     * @param     ConnectionInterface $con an optional connection object
     *
     * @return ObjectCollection|array|mixed the list of results, formatted by the current formatter
     */
    public function findPks($keys, ConnectionInterface $con = null)
    {
        if (null === $con) {
            $con = Propel::getServiceContainer()->getReadConnection($this->getDbName());
        }
        $this->basePreSelect($con);
        $criteria = $this->isKeepQuery() ? clone $this : $this;
        $dataFetcher = $criteria
            ->filterByPrimaryKeys($keys)
            ->doSelect($con);

        return $criteria->getFormatter()->init($criteria)->format($dataFetcher);
    }

    /**
     * Filter the query by primary key
     *
     * @param     mixed $key Primary key to use for the query
     *
     * @return $this|ChildRestrictionQuery The current query, for fluid interface
     */
    public function filterByPrimaryKey($key)
    {

        return $this->addUsingAlias(RestrictionTableMap::COL_ID_RESTRICTION, $key, Criteria::EQUAL);
    }

    /**
     * Filter the query by a list of primary keys
     *
     * @param     array $keys The list of primary key to use for the query
     *
     * @return $this|ChildRestrictionQuery The current query, for fluid interface
     */
    public function filterByPrimaryKeys($keys)
    {

        return $this->addUsingAlias(RestrictionTableMap::COL_ID_RESTRICTION, $keys, Criteria::IN);
    }

    /**
     * Filter the query on the id_restriction column
     *
     * Example usage:
     * <code>
     * $query->filterByIdRestriction(1234); // WHERE id_restriction = 1234
     * $query->filterByIdRestriction(array(12, 34)); // WHERE id_restriction IN (12, 34)
     * $query->filterByIdRestriction(array('min' => 12)); // WHERE id_restriction > 12
     * </code>
     *
     * @param     mixed $idRestriction The value to use as filter.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildRestrictionQuery The current query, for fluid interface
     */
    public function filterByIdRestriction($idRestriction = null, $comparison = null)
    {
        if (is_array($idRestriction)) {
            $useMinMax = false;
            if (isset($idRestriction['min'])) {
                $this->addUsingAlias(RestrictionTableMap::COL_ID_RESTRICTION, $idRestriction['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($idRestriction['max'])) {
                $this->addUsingAlias(RestrictionTableMap::COL_ID_RESTRICTION, $idRestriction['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(RestrictionTableMap::COL_ID_RESTRICTION, $idRestriction, $comparison);
    }

    /**
     * Filter the query on the nom column
     *
     * Example usage:
     * <code>
     * $query->filterByNom('fooValue');   // WHERE nom = 'fooValue'
     * $query->filterByNom('%fooValue%', Criteria::LIKE); // WHERE nom LIKE '%fooValue%'
     * </code>
     *
     * @param     string $nom The value to use as filter.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildRestrictionQuery The current query, for fluid interface
     */
    public function filterByNom($nom = null, $comparison = null)
    {
        if (null === $comparison) {
            if (is_array($nom)) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(RestrictionTableMap::COL_NOM, $nom, $comparison);
    }

    /**
     * Filter the query on the nomcourt column
     *
     * Example usage:
     * <code>
     * $query->filterByNomcourt('fooValue');   // WHERE nomcourt = 'fooValue'
     * $query->filterByNomcourt('%fooValue%', Criteria::LIKE); // WHERE nomcourt LIKE '%fooValue%'
     * </code>
     *
     * @param     string $nomcourt The value to use as filter.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildRestrictionQuery The current query, for fluid interface
     */
    public function filterByNomcourt($nomcourt = null, $comparison = null)
    {
        if (null === $comparison) {
            if (is_array($nomcourt)) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(RestrictionTableMap::COL_NOMCOURT, $nomcourt, $comparison);
    }

    /**
     * Filter the query on the actif column
     *
     * Example usage:
     * <code>
     * $query->filterByActif(1234); // WHERE actif = 1234
     * $query->filterByActif(array(12, 34)); // WHERE actif IN (12, 34)
     * $query->filterByActif(array('min' => 12)); // WHERE actif > 12
     * </code>
     *
     * @param     mixed $actif The value to use as filter.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildRestrictionQuery The current query, for fluid interface
     */
    public function filterByActif($actif = null, $comparison = null)
    {
        if (is_array($actif)) {
            $useMinMax = false;
            if (isset($actif['min'])) {
                $this->addUsingAlias(RestrictionTableMap::COL_ACTIF, $actif['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($actif['max'])) {
                $this->addUsingAlias(RestrictionTableMap::COL_ACTIF, $actif['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(RestrictionTableMap::COL_ACTIF, $actif, $comparison);
    }

    /**
     * Filter the query on the variables column
     *
     * Example usage:
     * <code>
     * $query->filterByVariables('fooValue');   // WHERE variables = 'fooValue'
     * $query->filterByVariables('%fooValue%', Criteria::LIKE); // WHERE variables LIKE '%fooValue%'
     * </code>
     *
     * @param     string $variables The value to use as filter.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildRestrictionQuery The current query, for fluid interface
     */
    public function filterByVariables($variables = null, $comparison = null)
    {
        if (null === $comparison) {
            if (is_array($variables)) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(RestrictionTableMap::COL_VARIABLES, $variables, $comparison);
    }

    /**
     * Filter the query on the created_at column
     *
     * Example usage:
     * <code>
     * $query->filterByCreatedAt('2011-03-14'); // WHERE created_at = '2011-03-14'
     * $query->filterByCreatedAt('now'); // WHERE created_at = '2011-03-14'
     * $query->filterByCreatedAt(array('max' => 'yesterday')); // WHERE created_at > '2011-03-13'
     * </code>
     *
     * @param     mixed $createdAt The value to use as filter.
     *              Values can be integers (unix timestamps), DateTime objects, or strings.
     *              Empty strings are treated as NULL.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildRestrictionQuery The current query, for fluid interface
     */
    public function filterByCreatedAt($createdAt = null, $comparison = null)
    {
        if (is_array($createdAt)) {
            $useMinMax = false;
            if (isset($createdAt['min'])) {
                $this->addUsingAlias(RestrictionTableMap::COL_CREATED_AT, $createdAt['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($createdAt['max'])) {
                $this->addUsingAlias(RestrictionTableMap::COL_CREATED_AT, $createdAt['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(RestrictionTableMap::COL_CREATED_AT, $createdAt, $comparison);
    }

    /**
     * Filter the query on the updated_at column
     *
     * Example usage:
     * <code>
     * $query->filterByUpdatedAt('2011-03-14'); // WHERE updated_at = '2011-03-14'
     * $query->filterByUpdatedAt('now'); // WHERE updated_at = '2011-03-14'
     * $query->filterByUpdatedAt(array('max' => 'yesterday')); // WHERE updated_at > '2011-03-13'
     * </code>
     *
     * @param     mixed $updatedAt The value to use as filter.
     *              Values can be integers (unix timestamps), DateTime objects, or strings.
     *              Empty strings are treated as NULL.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildRestrictionQuery The current query, for fluid interface
     */
    public function filterByUpdatedAt($updatedAt = null, $comparison = null)
    {
        if (is_array($updatedAt)) {
            $useMinMax = false;
            if (isset($updatedAt['min'])) {
                $this->addUsingAlias(RestrictionTableMap::COL_UPDATED_AT, $updatedAt['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($updatedAt['max'])) {
                $this->addUsingAlias(RestrictionTableMap::COL_UPDATED_AT, $updatedAt['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(RestrictionTableMap::COL_UPDATED_AT, $updatedAt, $comparison);
    }

    /**
     * Exclude object from result
     *
     * @param   ChildRestriction $restriction Object to remove from the list of results
     *
     * @return $this|ChildRestrictionQuery The current query, for fluid interface
     */
    public function prune($restriction = null)
    {
        if ($restriction) {
            $this->addUsingAlias(RestrictionTableMap::COL_ID_RESTRICTION, $restriction->getIdRestriction(), Criteria::NOT_EQUAL);
        }

        return $this;
    }

    /**
     * Code to execute before every DELETE statement
     *
     * @param     ConnectionInterface $con The connection object used by the query
     */
    protected function basePreDelete(ConnectionInterface $con)
    {
        // archivable behavior

        if ($this->archiveOnDelete) {
            $this->archive($con);
        } else {
            $this->archiveOnDelete = true;
        }


        return $this->preDelete($con);
    }

    /**
     * Deletes all rows from the asso_restrictions table.
     *
     * @param ConnectionInterface $con the connection to use
     * @return int The number of affected rows (if supported by underlying database driver).
     */
    public function doDeleteAll(ConnectionInterface $con = null)
    {
        if (null === $con) {
            $con = Propel::getServiceContainer()->getWriteConnection(RestrictionTableMap::DATABASE_NAME);
        }

        // use transaction because $criteria could contain info
        // for more than one table or we could emulating ON DELETE CASCADE, etc.
        return $con->transaction(function () use ($con) {
            $affectedRows = 0; // initialize var to track total num of affected rows
            $affectedRows += parent::doDeleteAll($con);
            // Because this db requires some delete cascade/set null emulation, we have to
            // clear the cached instance *after* the emulation has happened (since
            // instances get re-added by the select statement contained therein).
            RestrictionTableMap::clearInstancePool();
            RestrictionTableMap::clearRelatedInstancePool();

            return $affectedRows;
        });
    }

    /**
     * Performs a DELETE on the database based on the current ModelCriteria
     *
     * @param ConnectionInterface $con the connection to use
     * @return int             The number of affected rows (if supported by underlying database driver).  This includes CASCADE-related rows
     *                         if supported by native driver or if emulated using Propel.
     * @throws PropelException Any exceptions caught during processing will be
     *                         rethrown wrapped into a PropelException.
     */
    public function delete(ConnectionInterface $con = null)
    {
        if (null === $con) {
            $con = Propel::getServiceContainer()->getWriteConnection(RestrictionTableMap::DATABASE_NAME);
        }

        $criteria = $this;

        // Set the correct dbName
        $criteria->setDbName(RestrictionTableMap::DATABASE_NAME);

        // use transaction because $criteria could contain info
        // for more than one table or we could emulating ON DELETE CASCADE, etc.
        return $con->transaction(function () use ($con, $criteria) {
            $affectedRows = 0; // initialize var to track total num of affected rows

            RestrictionTableMap::removeInstanceFromPool($criteria);

            $affectedRows += ModelCriteria::delete($con);
            RestrictionTableMap::clearRelatedInstancePool();

            return $affectedRows;
        });
    }

    // timestampable behavior

    /**
     * Filter by the latest updated
     *
     * @param      int $nbDays Maximum age of the latest update in days
     *
     * @return     $this|ChildRestrictionQuery The current query, for fluid interface
     */
    public function recentlyUpdated($nbDays = 7)
    {
        return $this->addUsingAlias(RestrictionTableMap::COL_UPDATED_AT, time() - $nbDays * 24 * 60 * 60, Criteria::GREATER_EQUAL);
    }

    /**
     * Order by update date desc
     *
     * @return     $this|ChildRestrictionQuery The current query, for fluid interface
     */
    public function lastUpdatedFirst()
    {
        return $this->addDescendingOrderByColumn(RestrictionTableMap::COL_UPDATED_AT);
    }

    /**
     * Order by update date asc
     *
     * @return     $this|ChildRestrictionQuery The current query, for fluid interface
     */
    public function firstUpdatedFirst()
    {
        return $this->addAscendingOrderByColumn(RestrictionTableMap::COL_UPDATED_AT);
    }

    /**
     * Order by create date desc
     *
     * @return     $this|ChildRestrictionQuery The current query, for fluid interface
     */
    public function lastCreatedFirst()
    {
        return $this->addDescendingOrderByColumn(RestrictionTableMap::COL_CREATED_AT);
    }

    /**
     * Filter by the latest created
     *
     * @param      int $nbDays Maximum age of in days
     *
     * @return     $this|ChildRestrictionQuery The current query, for fluid interface
     */
    public function recentlyCreated($nbDays = 7)
    {
        return $this->addUsingAlias(RestrictionTableMap::COL_CREATED_AT, time() - $nbDays * 24 * 60 * 60, Criteria::GREATER_EQUAL);
    }

    /**
     * Order by create date asc
     *
     * @return     $this|ChildRestrictionQuery The current query, for fluid interface
     */
    public function firstCreatedFirst()
    {
        return $this->addAscendingOrderByColumn(RestrictionTableMap::COL_CREATED_AT);
    }

    // archivable behavior

    /**
     * Copy the data of the objects satisfying the query into ChildAssoRestrictionsArchive archive objects.
     * The archived objects are then saved.
     * If any of the objects has already been archived, the archived object
     * is updated and not duplicated.
     * Warning: This termination methods issues 2n+1 queries.
     *
     * @param      ConnectionInterface $con    Connection to use.
     * @param      Boolean $useLittleMemory    Whether or not to use OnDemandFormatter to retrieve objects.
     *               Set to false if the identity map matters.
     *               Set to true (default) to use less memory.
     *
     * @return     int the number of archived objects
     */
    public function archive($con = null, $useLittleMemory = true)
    {
        $criteria = clone $this;
        // prepare the query
        $criteria->setWith(array());
        if ($useLittleMemory) {
            $criteria->setFormatter(ModelCriteria::FORMAT_ON_DEMAND);
        }
        if ($con === null) {
            $con = Propel::getServiceContainer()->getWriteConnection(RestrictionTableMap::DATABASE_NAME);
        }

        return $con->transaction(function () use ($con, $criteria) {
            $totalArchivedObjects = 0;

            // archive all results one by one
            foreach ($criteria->find($con) as $object) {
                $object->archive($con);
                $totalArchivedObjects++;
            }

            return $totalArchivedObjects;
        });
    }

    /**
     * Enable/disable auto-archiving on delete for the next query.
     *
     * @param boolean True if the query must archive deleted objects, false otherwise.
     */
    public function setArchiveOnDelete($archiveOnDelete)
    {
        $this->archiveOnDelete = $archiveOnDelete;
    }

    /**
     * Delete records matching the current query without archiving them.
     *
     * @param      ConnectionInterface $con    Connection to use.
     *
     * @return integer the number of deleted rows
     */
    public function deleteWithoutArchive($con = null)
    {
        $this->archiveOnDelete = false;

        return $this->delete($con);
    }

    /**
     * Delete all records without archiving them.
     *
     * @param      ConnectionInterface $con    Connection to use.
     *
     * @return integer the number of deleted rows
     */
    public function deleteAllWithoutArchive($con = null)
    {
        $this->archiveOnDelete = false;

        return $this->deleteAll($con);
    }

} // RestrictionQuery
