<?php

namespace Base;

use \ServicerenduIndividu as ChildServicerenduIndividu;
use \ServicerenduIndividuQuery as ChildServicerenduIndividuQuery;
use \Exception;
use \PDO;
use Map\ServicerenduIndividuTableMap;
use Propel\Runtime\Propel;
use Propel\Runtime\ActiveQuery\Criteria;
use Propel\Runtime\ActiveQuery\ModelCriteria;
use Propel\Runtime\ActiveQuery\ModelJoin;
use Propel\Runtime\Collection\ObjectCollection;
use Propel\Runtime\Connection\ConnectionInterface;
use Propel\Runtime\Exception\PropelException;

/**
 * Base class that represents a query for the 'asso_servicerendus_individus' table.
 *
 *
 *
 * @method     ChildServicerenduIndividuQuery orderByIdIndividu($order = Criteria::ASC) Order by the id_individu column
 * @method     ChildServicerenduIndividuQuery orderByIdServicerendu($order = Criteria::ASC) Order by the id_servicerendu column
 *
 * @method     ChildServicerenduIndividuQuery groupByIdIndividu() Group by the id_individu column
 * @method     ChildServicerenduIndividuQuery groupByIdServicerendu() Group by the id_servicerendu column
 *
 * @method     ChildServicerenduIndividuQuery leftJoin($relation) Adds a LEFT JOIN clause to the query
 * @method     ChildServicerenduIndividuQuery rightJoin($relation) Adds a RIGHT JOIN clause to the query
 * @method     ChildServicerenduIndividuQuery innerJoin($relation) Adds a INNER JOIN clause to the query
 *
 * @method     ChildServicerenduIndividuQuery leftJoinWith($relation) Adds a LEFT JOIN clause and with to the query
 * @method     ChildServicerenduIndividuQuery rightJoinWith($relation) Adds a RIGHT JOIN clause and with to the query
 * @method     ChildServicerenduIndividuQuery innerJoinWith($relation) Adds a INNER JOIN clause and with to the query
 *
 * @method     ChildServicerenduIndividuQuery leftJoinServicerendu($relationAlias = null) Adds a LEFT JOIN clause to the query using the Servicerendu relation
 * @method     ChildServicerenduIndividuQuery rightJoinServicerendu($relationAlias = null) Adds a RIGHT JOIN clause to the query using the Servicerendu relation
 * @method     ChildServicerenduIndividuQuery innerJoinServicerendu($relationAlias = null) Adds a INNER JOIN clause to the query using the Servicerendu relation
 *
 * @method     ChildServicerenduIndividuQuery joinWithServicerendu($joinType = Criteria::INNER_JOIN) Adds a join clause and with to the query using the Servicerendu relation
 *
 * @method     ChildServicerenduIndividuQuery leftJoinWithServicerendu() Adds a LEFT JOIN clause and with to the query using the Servicerendu relation
 * @method     ChildServicerenduIndividuQuery rightJoinWithServicerendu() Adds a RIGHT JOIN clause and with to the query using the Servicerendu relation
 * @method     ChildServicerenduIndividuQuery innerJoinWithServicerendu() Adds a INNER JOIN clause and with to the query using the Servicerendu relation
 *
 * @method     ChildServicerenduIndividuQuery leftJoinIndividu($relationAlias = null) Adds a LEFT JOIN clause to the query using the Individu relation
 * @method     ChildServicerenduIndividuQuery rightJoinIndividu($relationAlias = null) Adds a RIGHT JOIN clause to the query using the Individu relation
 * @method     ChildServicerenduIndividuQuery innerJoinIndividu($relationAlias = null) Adds a INNER JOIN clause to the query using the Individu relation
 *
 * @method     ChildServicerenduIndividuQuery joinWithIndividu($joinType = Criteria::INNER_JOIN) Adds a join clause and with to the query using the Individu relation
 *
 * @method     ChildServicerenduIndividuQuery leftJoinWithIndividu() Adds a LEFT JOIN clause and with to the query using the Individu relation
 * @method     ChildServicerenduIndividuQuery rightJoinWithIndividu() Adds a RIGHT JOIN clause and with to the query using the Individu relation
 * @method     ChildServicerenduIndividuQuery innerJoinWithIndividu() Adds a INNER JOIN clause and with to the query using the Individu relation
 *
 * @method     \ServicerenduQuery|\IndividuQuery endUse() Finalizes a secondary criteria and merges it with its primary Criteria
 *
 * @method     ChildServicerenduIndividu findOne(ConnectionInterface $con = null) Return the first ChildServicerenduIndividu matching the query
 * @method     ChildServicerenduIndividu findOneOrCreate(ConnectionInterface $con = null) Return the first ChildServicerenduIndividu matching the query, or a new ChildServicerenduIndividu object populated from the query conditions when no match is found
 *
 * @method     ChildServicerenduIndividu findOneByIdIndividu(string $id_individu) Return the first ChildServicerenduIndividu filtered by the id_individu column
 * @method     ChildServicerenduIndividu findOneByIdServicerendu(string $id_servicerendu) Return the first ChildServicerenduIndividu filtered by the id_servicerendu column *

 * @method     ChildServicerenduIndividu requirePk($key, ConnectionInterface $con = null) Return the ChildServicerenduIndividu by primary key and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildServicerenduIndividu requireOne(ConnectionInterface $con = null) Return the first ChildServicerenduIndividu matching the query and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 *
 * @method     ChildServicerenduIndividu requireOneByIdIndividu(string $id_individu) Return the first ChildServicerenduIndividu filtered by the id_individu column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildServicerenduIndividu requireOneByIdServicerendu(string $id_servicerendu) Return the first ChildServicerenduIndividu filtered by the id_servicerendu column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 *
 * @method     ChildServicerenduIndividu[]|ObjectCollection find(ConnectionInterface $con = null) Return ChildServicerenduIndividu objects based on current ModelCriteria
 * @method     ChildServicerenduIndividu[]|ObjectCollection findByIdIndividu(string $id_individu) Return ChildServicerenduIndividu objects filtered by the id_individu column
 * @method     ChildServicerenduIndividu[]|ObjectCollection findByIdServicerendu(string $id_servicerendu) Return ChildServicerenduIndividu objects filtered by the id_servicerendu column
 * @method     ChildServicerenduIndividu[]|\Propel\Runtime\Util\PropelModelPager paginate($page = 1, $maxPerPage = 10, ConnectionInterface $con = null) Issue a SELECT query based on the current ModelCriteria and uses a page and a maximum number of results per page to compute an offset and a limit
 *
 */
abstract class ServicerenduIndividuQuery extends ModelCriteria
{
    protected $entityNotFoundExceptionClass = '\\Propel\\Runtime\\Exception\\EntityNotFoundException';

    /**
     * Initializes internal state of \Base\ServicerenduIndividuQuery object.
     *
     * @param     string $dbName The database name
     * @param     string $modelName The phpName of a model, e.g. 'Book'
     * @param     string $modelAlias The alias for the model in this query, e.g. 'b'
     */
    public function __construct($dbName = 'simplasso', $modelName = '\\ServicerenduIndividu', $modelAlias = null)
    {
        parent::__construct($dbName, $modelName, $modelAlias);
    }

    /**
     * Returns a new ChildServicerenduIndividuQuery object.
     *
     * @param     string $modelAlias The alias of a model in the query
     * @param     Criteria $criteria Optional Criteria to build the query from
     *
     * @return ChildServicerenduIndividuQuery
     */
    public static function create($modelAlias = null, Criteria $criteria = null)
    {
        if ($criteria instanceof ChildServicerenduIndividuQuery) {
            return $criteria;
        }
        $query = new ChildServicerenduIndividuQuery();
        if (null !== $modelAlias) {
            $query->setModelAlias($modelAlias);
        }
        if ($criteria instanceof Criteria) {
            $query->mergeWith($criteria);
        }

        return $query;
    }

    /**
     * Find object by primary key.
     * Propel uses the instance pool to skip the database if the object exists.
     * Go fast if the query is untouched.
     *
     * <code>
     * $obj = $c->findPk(array(12, 34), $con);
     * </code>
     *
     * @param array[$id_individu, $id_servicerendu] $key Primary key to use for the query
     * @param ConnectionInterface $con an optional connection object
     *
     * @return ChildServicerenduIndividu|array|mixed the result, formatted by the current formatter
     */
    public function findPk($key, ConnectionInterface $con = null)
    {
        if ($key === null) {
            return null;
        }

        if ($con === null) {
            $con = Propel::getServiceContainer()->getReadConnection(ServicerenduIndividuTableMap::DATABASE_NAME);
        }

        $this->basePreSelect($con);

        if (
            $this->formatter || $this->modelAlias || $this->with || $this->select
            || $this->selectColumns || $this->asColumns || $this->selectModifiers
            || $this->map || $this->having || $this->joins
        ) {
            return $this->findPkComplex($key, $con);
        }

        if ((null !== ($obj = ServicerenduIndividuTableMap::getInstanceFromPool(serialize([(null === $key[0] || is_scalar($key[0]) || is_callable([$key[0], '__toString']) ? (string) $key[0] : $key[0]), (null === $key[1] || is_scalar($key[1]) || is_callable([$key[1], '__toString']) ? (string) $key[1] : $key[1])]))))) {
            // the object is already in the instance pool
            return $obj;
        }

        return $this->findPkSimple($key, $con);
    }

    /**
     * Find object by primary key using raw SQL to go fast.
     * Bypass doSelect() and the object formatter by using generated code.
     *
     * @param     mixed $key Primary key to use for the query
     * @param     ConnectionInterface $con A connection object
     *
     * @throws \Propel\Runtime\Exception\PropelException
     *
     * @return ChildServicerenduIndividu A model object, or null if the key is not found
     */
    protected function findPkSimple($key, ConnectionInterface $con)
    {
        $sql = 'SELECT `id_individu`, `id_servicerendu` FROM `asso_servicerendus_individus` WHERE `id_individu` = :p0 AND `id_servicerendu` = :p1';
        try {
            $stmt = $con->prepare($sql);
            $stmt->bindValue(':p0', $key[0], PDO::PARAM_INT);
            $stmt->bindValue(':p1', $key[1], PDO::PARAM_INT);
            $stmt->execute();
        } catch (Exception $e) {
            Propel::log($e->getMessage(), Propel::LOG_ERR);
            throw new PropelException(sprintf('Unable to execute SELECT statement [%s]', $sql), 0, $e);
        }
        $obj = null;
        if ($row = $stmt->fetch(\PDO::FETCH_NUM)) {
            /** @var ChildServicerenduIndividu $obj */
            $obj = new ChildServicerenduIndividu();
            $obj->hydrate($row);
            ServicerenduIndividuTableMap::addInstanceToPool($obj, serialize([(null === $key[0] || is_scalar($key[0]) || is_callable([$key[0], '__toString']) ? (string) $key[0] : $key[0]), (null === $key[1] || is_scalar($key[1]) || is_callable([$key[1], '__toString']) ? (string) $key[1] : $key[1])]));
        }
        $stmt->closeCursor();

        return $obj;
    }

    /**
     * Find object by primary key.
     *
     * @param     mixed $key Primary key to use for the query
     * @param     ConnectionInterface $con A connection object
     *
     * @return ChildServicerenduIndividu|array|mixed the result, formatted by the current formatter
     */
    protected function findPkComplex($key, ConnectionInterface $con)
    {
        // As the query uses a PK condition, no limit(1) is necessary.
        $criteria = $this->isKeepQuery() ? clone $this : $this;
        $dataFetcher = $criteria
            ->filterByPrimaryKey($key)
            ->doSelect($con);

        return $criteria->getFormatter()->init($criteria)->formatOne($dataFetcher);
    }

    /**
     * Find objects by primary key
     * <code>
     * $objs = $c->findPks(array(array(12, 56), array(832, 123), array(123, 456)), $con);
     * </code>
     * @param     array $keys Primary keys to use for the query
     * @param     ConnectionInterface $con an optional connection object
     *
     * @return ObjectCollection|array|mixed the list of results, formatted by the current formatter
     */
    public function findPks($keys, ConnectionInterface $con = null)
    {
        if (null === $con) {
            $con = Propel::getServiceContainer()->getReadConnection($this->getDbName());
        }
        $this->basePreSelect($con);
        $criteria = $this->isKeepQuery() ? clone $this : $this;
        $dataFetcher = $criteria
            ->filterByPrimaryKeys($keys)
            ->doSelect($con);

        return $criteria->getFormatter()->init($criteria)->format($dataFetcher);
    }

    /**
     * Filter the query by primary key
     *
     * @param     mixed $key Primary key to use for the query
     *
     * @return $this|ChildServicerenduIndividuQuery The current query, for fluid interface
     */
    public function filterByPrimaryKey($key)
    {
        $this->addUsingAlias(ServicerenduIndividuTableMap::COL_ID_INDIVIDU, $key[0], Criteria::EQUAL);
        $this->addUsingAlias(ServicerenduIndividuTableMap::COL_ID_SERVICERENDU, $key[1], Criteria::EQUAL);

        return $this;
    }

    /**
     * Filter the query by a list of primary keys
     *
     * @param     array $keys The list of primary key to use for the query
     *
     * @return $this|ChildServicerenduIndividuQuery The current query, for fluid interface
     */
    public function filterByPrimaryKeys($keys)
    {
        if (empty($keys)) {
            return $this->add(null, '1<>1', Criteria::CUSTOM);
        }
        foreach ($keys as $key) {
            $cton0 = $this->getNewCriterion(ServicerenduIndividuTableMap::COL_ID_INDIVIDU, $key[0], Criteria::EQUAL);
            $cton1 = $this->getNewCriterion(ServicerenduIndividuTableMap::COL_ID_SERVICERENDU, $key[1], Criteria::EQUAL);
            $cton0->addAnd($cton1);
            $this->addOr($cton0);
        }

        return $this;
    }

    /**
     * Filter the query on the id_individu column
     *
     * Example usage:
     * <code>
     * $query->filterByIdIndividu(1234); // WHERE id_individu = 1234
     * $query->filterByIdIndividu(array(12, 34)); // WHERE id_individu IN (12, 34)
     * $query->filterByIdIndividu(array('min' => 12)); // WHERE id_individu > 12
     * </code>
     *
     * @see       filterByIndividu()
     *
     * @param     mixed $idIndividu The value to use as filter.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildServicerenduIndividuQuery The current query, for fluid interface
     */
    public function filterByIdIndividu($idIndividu = null, $comparison = null)
    {
        if (is_array($idIndividu)) {
            $useMinMax = false;
            if (isset($idIndividu['min'])) {
                $this->addUsingAlias(ServicerenduIndividuTableMap::COL_ID_INDIVIDU, $idIndividu['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($idIndividu['max'])) {
                $this->addUsingAlias(ServicerenduIndividuTableMap::COL_ID_INDIVIDU, $idIndividu['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(ServicerenduIndividuTableMap::COL_ID_INDIVIDU, $idIndividu, $comparison);
    }

    /**
     * Filter the query on the id_servicerendu column
     *
     * Example usage:
     * <code>
     * $query->filterByIdServicerendu(1234); // WHERE id_servicerendu = 1234
     * $query->filterByIdServicerendu(array(12, 34)); // WHERE id_servicerendu IN (12, 34)
     * $query->filterByIdServicerendu(array('min' => 12)); // WHERE id_servicerendu > 12
     * </code>
     *
     * @see       filterByServicerendu()
     *
     * @param     mixed $idServicerendu The value to use as filter.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildServicerenduIndividuQuery The current query, for fluid interface
     */
    public function filterByIdServicerendu($idServicerendu = null, $comparison = null)
    {
        if (is_array($idServicerendu)) {
            $useMinMax = false;
            if (isset($idServicerendu['min'])) {
                $this->addUsingAlias(ServicerenduIndividuTableMap::COL_ID_SERVICERENDU, $idServicerendu['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($idServicerendu['max'])) {
                $this->addUsingAlias(ServicerenduIndividuTableMap::COL_ID_SERVICERENDU, $idServicerendu['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(ServicerenduIndividuTableMap::COL_ID_SERVICERENDU, $idServicerendu, $comparison);
    }

    /**
     * Filter the query by a related \Servicerendu object
     *
     * @param \Servicerendu|ObjectCollection $servicerendu The related object(s) to use as filter
     * @param string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @throws \Propel\Runtime\Exception\PropelException
     *
     * @return ChildServicerenduIndividuQuery The current query, for fluid interface
     */
    public function filterByServicerendu($servicerendu, $comparison = null)
    {
        if ($servicerendu instanceof \Servicerendu) {
            return $this
                ->addUsingAlias(ServicerenduIndividuTableMap::COL_ID_SERVICERENDU, $servicerendu->getIdServicerendu(), $comparison);
        } elseif ($servicerendu instanceof ObjectCollection) {
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }

            return $this
                ->addUsingAlias(ServicerenduIndividuTableMap::COL_ID_SERVICERENDU, $servicerendu->toKeyValue('PrimaryKey', 'IdServicerendu'), $comparison);
        } else {
            throw new PropelException('filterByServicerendu() only accepts arguments of type \Servicerendu or Collection');
        }
    }

    /**
     * Adds a JOIN clause to the query using the Servicerendu relation
     *
     * @param     string $relationAlias optional alias for the relation
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return $this|ChildServicerenduIndividuQuery The current query, for fluid interface
     */
    public function joinServicerendu($relationAlias = null, $joinType = Criteria::INNER_JOIN)
    {
        $tableMap = $this->getTableMap();
        $relationMap = $tableMap->getRelation('Servicerendu');

        // create a ModelJoin object for this join
        $join = new ModelJoin();
        $join->setJoinType($joinType);
        $join->setRelationMap($relationMap, $this->useAliasInSQL ? $this->getModelAlias() : null, $relationAlias);
        if ($previousJoin = $this->getPreviousJoin()) {
            $join->setPreviousJoin($previousJoin);
        }

        // add the ModelJoin to the current object
        if ($relationAlias) {
            $this->addAlias($relationAlias, $relationMap->getRightTable()->getName());
            $this->addJoinObject($join, $relationAlias);
        } else {
            $this->addJoinObject($join, 'Servicerendu');
        }

        return $this;
    }

    /**
     * Use the Servicerendu relation Servicerendu object
     *
     * @see useQuery()
     *
     * @param     string $relationAlias optional alias for the relation,
     *                                   to be used as main alias in the secondary query
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return \ServicerenduQuery A secondary query class using the current class as primary query
     */
    public function useServicerenduQuery($relationAlias = null, $joinType = Criteria::INNER_JOIN)
    {
        return $this
            ->joinServicerendu($relationAlias, $joinType)
            ->useQuery($relationAlias ? $relationAlias : 'Servicerendu', '\ServicerenduQuery');
    }

    /**
     * Filter the query by a related \Individu object
     *
     * @param \Individu|ObjectCollection $individu The related object(s) to use as filter
     * @param string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @throws \Propel\Runtime\Exception\PropelException
     *
     * @return ChildServicerenduIndividuQuery The current query, for fluid interface
     */
    public function filterByIndividu($individu, $comparison = null)
    {
        if ($individu instanceof \Individu) {
            return $this
                ->addUsingAlias(ServicerenduIndividuTableMap::COL_ID_INDIVIDU, $individu->getIdIndividu(), $comparison);
        } elseif ($individu instanceof ObjectCollection) {
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }

            return $this
                ->addUsingAlias(ServicerenduIndividuTableMap::COL_ID_INDIVIDU, $individu->toKeyValue('PrimaryKey', 'IdIndividu'), $comparison);
        } else {
            throw new PropelException('filterByIndividu() only accepts arguments of type \Individu or Collection');
        }
    }

    /**
     * Adds a JOIN clause to the query using the Individu relation
     *
     * @param     string $relationAlias optional alias for the relation
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return $this|ChildServicerenduIndividuQuery The current query, for fluid interface
     */
    public function joinIndividu($relationAlias = null, $joinType = Criteria::INNER_JOIN)
    {
        $tableMap = $this->getTableMap();
        $relationMap = $tableMap->getRelation('Individu');

        // create a ModelJoin object for this join
        $join = new ModelJoin();
        $join->setJoinType($joinType);
        $join->setRelationMap($relationMap, $this->useAliasInSQL ? $this->getModelAlias() : null, $relationAlias);
        if ($previousJoin = $this->getPreviousJoin()) {
            $join->setPreviousJoin($previousJoin);
        }

        // add the ModelJoin to the current object
        if ($relationAlias) {
            $this->addAlias($relationAlias, $relationMap->getRightTable()->getName());
            $this->addJoinObject($join, $relationAlias);
        } else {
            $this->addJoinObject($join, 'Individu');
        }

        return $this;
    }

    /**
     * Use the Individu relation Individu object
     *
     * @see useQuery()
     *
     * @param     string $relationAlias optional alias for the relation,
     *                                   to be used as main alias in the secondary query
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return \IndividuQuery A secondary query class using the current class as primary query
     */
    public function useIndividuQuery($relationAlias = null, $joinType = Criteria::INNER_JOIN)
    {
        return $this
            ->joinIndividu($relationAlias, $joinType)
            ->useQuery($relationAlias ? $relationAlias : 'Individu', '\IndividuQuery');
    }

    /**
     * Exclude object from result
     *
     * @param   ChildServicerenduIndividu $servicerenduIndividu Object to remove from the list of results
     *
     * @return $this|ChildServicerenduIndividuQuery The current query, for fluid interface
     */
    public function prune($servicerenduIndividu = null)
    {
        if ($servicerenduIndividu) {
            $this->addCond('pruneCond0', $this->getAliasedColName(ServicerenduIndividuTableMap::COL_ID_INDIVIDU), $servicerenduIndividu->getIdIndividu(), Criteria::NOT_EQUAL);
            $this->addCond('pruneCond1', $this->getAliasedColName(ServicerenduIndividuTableMap::COL_ID_SERVICERENDU), $servicerenduIndividu->getIdServicerendu(), Criteria::NOT_EQUAL);
            $this->combine(array('pruneCond0', 'pruneCond1'), Criteria::LOGICAL_OR);
        }

        return $this;
    }

    /**
     * Deletes all rows from the asso_servicerendus_individus table.
     *
     * @param ConnectionInterface $con the connection to use
     * @return int The number of affected rows (if supported by underlying database driver).
     */
    public function doDeleteAll(ConnectionInterface $con = null)
    {
        if (null === $con) {
            $con = Propel::getServiceContainer()->getWriteConnection(ServicerenduIndividuTableMap::DATABASE_NAME);
        }

        // use transaction because $criteria could contain info
        // for more than one table or we could emulating ON DELETE CASCADE, etc.
        return $con->transaction(function () use ($con) {
            $affectedRows = 0; // initialize var to track total num of affected rows
            $affectedRows += parent::doDeleteAll($con);
            // Because this db requires some delete cascade/set null emulation, we have to
            // clear the cached instance *after* the emulation has happened (since
            // instances get re-added by the select statement contained therein).
            ServicerenduIndividuTableMap::clearInstancePool();
            ServicerenduIndividuTableMap::clearRelatedInstancePool();

            return $affectedRows;
        });
    }

    /**
     * Performs a DELETE on the database based on the current ModelCriteria
     *
     * @param ConnectionInterface $con the connection to use
     * @return int             The number of affected rows (if supported by underlying database driver).  This includes CASCADE-related rows
     *                         if supported by native driver or if emulated using Propel.
     * @throws PropelException Any exceptions caught during processing will be
     *                         rethrown wrapped into a PropelException.
     */
    public function delete(ConnectionInterface $con = null)
    {
        if (null === $con) {
            $con = Propel::getServiceContainer()->getWriteConnection(ServicerenduIndividuTableMap::DATABASE_NAME);
        }

        $criteria = $this;

        // Set the correct dbName
        $criteria->setDbName(ServicerenduIndividuTableMap::DATABASE_NAME);

        // use transaction because $criteria could contain info
        // for more than one table or we could emulating ON DELETE CASCADE, etc.
        return $con->transaction(function () use ($con, $criteria) {
            $affectedRows = 0; // initialize var to track total num of affected rows

            ServicerenduIndividuTableMap::removeInstanceFromPool($criteria);

            $affectedRows += ModelCriteria::delete($con);
            ServicerenduIndividuTableMap::clearRelatedInstancePool();

            return $affectedRows;
        });
    }

} // ServicerenduIndividuQuery
