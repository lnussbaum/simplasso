<?php

namespace Base;

use \AssoMotgroupesArchive as ChildAssoMotgroupesArchive;
use \AssoMotgroupesArchiveQuery as ChildAssoMotgroupesArchiveQuery;
use \Exception;
use \PDO;
use Map\AssoMotgroupesArchiveTableMap;
use Propel\Runtime\Propel;
use Propel\Runtime\ActiveQuery\Criteria;
use Propel\Runtime\ActiveQuery\ModelCriteria;
use Propel\Runtime\Collection\ObjectCollection;
use Propel\Runtime\Connection\ConnectionInterface;
use Propel\Runtime\Exception\PropelException;

/**
 * Base class that represents a query for the 'asso_motgroupes_archive' table.
 *
 *
 *
 * @method     ChildAssoMotgroupesArchiveQuery orderByIdMotgroupe($order = Criteria::ASC) Order by the id_motgroupe column
 * @method     ChildAssoMotgroupesArchiveQuery orderByIdParent($order = Criteria::ASC) Order by the id_parent column
 * @method     ChildAssoMotgroupesArchiveQuery orderByNom($order = Criteria::ASC) Order by the nom column
 * @method     ChildAssoMotgroupesArchiveQuery orderByNomcourt($order = Criteria::ASC) Order by the nomcourt column
 * @method     ChildAssoMotgroupesArchiveQuery orderByDescriptif($order = Criteria::ASC) Order by the descriptif column
 * @method     ChildAssoMotgroupesArchiveQuery orderByTexte($order = Criteria::ASC) Order by the texte column
 * @method     ChildAssoMotgroupesArchiveQuery orderByImportance($order = Criteria::ASC) Order by the importance column
 * @method     ChildAssoMotgroupesArchiveQuery orderByObjetsEnLien($order = Criteria::ASC) Order by the objets_en_lien column
 * @method     ChildAssoMotgroupesArchiveQuery orderBySysteme($order = Criteria::ASC) Order by the systeme column
 * @method     ChildAssoMotgroupesArchiveQuery orderByActif($order = Criteria::ASC) Order by the actif column
 * @method     ChildAssoMotgroupesArchiveQuery orderByOptions($order = Criteria::ASC) Order by the options column
 * @method     ChildAssoMotgroupesArchiveQuery orderByCreatedAt($order = Criteria::ASC) Order by the created_at column
 * @method     ChildAssoMotgroupesArchiveQuery orderByUpdatedAt($order = Criteria::ASC) Order by the updated_at column
 * @method     ChildAssoMotgroupesArchiveQuery orderByArchivedAt($order = Criteria::ASC) Order by the archived_at column
 *
 * @method     ChildAssoMotgroupesArchiveQuery groupByIdMotgroupe() Group by the id_motgroupe column
 * @method     ChildAssoMotgroupesArchiveQuery groupByIdParent() Group by the id_parent column
 * @method     ChildAssoMotgroupesArchiveQuery groupByNom() Group by the nom column
 * @method     ChildAssoMotgroupesArchiveQuery groupByNomcourt() Group by the nomcourt column
 * @method     ChildAssoMotgroupesArchiveQuery groupByDescriptif() Group by the descriptif column
 * @method     ChildAssoMotgroupesArchiveQuery groupByTexte() Group by the texte column
 * @method     ChildAssoMotgroupesArchiveQuery groupByImportance() Group by the importance column
 * @method     ChildAssoMotgroupesArchiveQuery groupByObjetsEnLien() Group by the objets_en_lien column
 * @method     ChildAssoMotgroupesArchiveQuery groupBySysteme() Group by the systeme column
 * @method     ChildAssoMotgroupesArchiveQuery groupByActif() Group by the actif column
 * @method     ChildAssoMotgroupesArchiveQuery groupByOptions() Group by the options column
 * @method     ChildAssoMotgroupesArchiveQuery groupByCreatedAt() Group by the created_at column
 * @method     ChildAssoMotgroupesArchiveQuery groupByUpdatedAt() Group by the updated_at column
 * @method     ChildAssoMotgroupesArchiveQuery groupByArchivedAt() Group by the archived_at column
 *
 * @method     ChildAssoMotgroupesArchiveQuery leftJoin($relation) Adds a LEFT JOIN clause to the query
 * @method     ChildAssoMotgroupesArchiveQuery rightJoin($relation) Adds a RIGHT JOIN clause to the query
 * @method     ChildAssoMotgroupesArchiveQuery innerJoin($relation) Adds a INNER JOIN clause to the query
 *
 * @method     ChildAssoMotgroupesArchiveQuery leftJoinWith($relation) Adds a LEFT JOIN clause and with to the query
 * @method     ChildAssoMotgroupesArchiveQuery rightJoinWith($relation) Adds a RIGHT JOIN clause and with to the query
 * @method     ChildAssoMotgroupesArchiveQuery innerJoinWith($relation) Adds a INNER JOIN clause and with to the query
 *
 * @method     ChildAssoMotgroupesArchive findOne(ConnectionInterface $con = null) Return the first ChildAssoMotgroupesArchive matching the query
 * @method     ChildAssoMotgroupesArchive findOneOrCreate(ConnectionInterface $con = null) Return the first ChildAssoMotgroupesArchive matching the query, or a new ChildAssoMotgroupesArchive object populated from the query conditions when no match is found
 *
 * @method     ChildAssoMotgroupesArchive findOneByIdMotgroupe(string $id_motgroupe) Return the first ChildAssoMotgroupesArchive filtered by the id_motgroupe column
 * @method     ChildAssoMotgroupesArchive findOneByIdParent(string $id_parent) Return the first ChildAssoMotgroupesArchive filtered by the id_parent column
 * @method     ChildAssoMotgroupesArchive findOneByNom(string $nom) Return the first ChildAssoMotgroupesArchive filtered by the nom column
 * @method     ChildAssoMotgroupesArchive findOneByNomcourt(string $nomcourt) Return the first ChildAssoMotgroupesArchive filtered by the nomcourt column
 * @method     ChildAssoMotgroupesArchive findOneByDescriptif(string $descriptif) Return the first ChildAssoMotgroupesArchive filtered by the descriptif column
 * @method     ChildAssoMotgroupesArchive findOneByTexte(string $texte) Return the first ChildAssoMotgroupesArchive filtered by the texte column
 * @method     ChildAssoMotgroupesArchive findOneByImportance(int $importance) Return the first ChildAssoMotgroupesArchive filtered by the importance column
 * @method     ChildAssoMotgroupesArchive findOneByObjetsEnLien(string $objets_en_lien) Return the first ChildAssoMotgroupesArchive filtered by the objets_en_lien column
 * @method     ChildAssoMotgroupesArchive findOneBySysteme(boolean $systeme) Return the first ChildAssoMotgroupesArchive filtered by the systeme column
 * @method     ChildAssoMotgroupesArchive findOneByActif(int $actif) Return the first ChildAssoMotgroupesArchive filtered by the actif column
 * @method     ChildAssoMotgroupesArchive findOneByOptions(string $options) Return the first ChildAssoMotgroupesArchive filtered by the options column
 * @method     ChildAssoMotgroupesArchive findOneByCreatedAt(string $created_at) Return the first ChildAssoMotgroupesArchive filtered by the created_at column
 * @method     ChildAssoMotgroupesArchive findOneByUpdatedAt(string $updated_at) Return the first ChildAssoMotgroupesArchive filtered by the updated_at column
 * @method     ChildAssoMotgroupesArchive findOneByArchivedAt(string $archived_at) Return the first ChildAssoMotgroupesArchive filtered by the archived_at column *

 * @method     ChildAssoMotgroupesArchive requirePk($key, ConnectionInterface $con = null) Return the ChildAssoMotgroupesArchive by primary key and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildAssoMotgroupesArchive requireOne(ConnectionInterface $con = null) Return the first ChildAssoMotgroupesArchive matching the query and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 *
 * @method     ChildAssoMotgroupesArchive requireOneByIdMotgroupe(string $id_motgroupe) Return the first ChildAssoMotgroupesArchive filtered by the id_motgroupe column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildAssoMotgroupesArchive requireOneByIdParent(string $id_parent) Return the first ChildAssoMotgroupesArchive filtered by the id_parent column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildAssoMotgroupesArchive requireOneByNom(string $nom) Return the first ChildAssoMotgroupesArchive filtered by the nom column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildAssoMotgroupesArchive requireOneByNomcourt(string $nomcourt) Return the first ChildAssoMotgroupesArchive filtered by the nomcourt column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildAssoMotgroupesArchive requireOneByDescriptif(string $descriptif) Return the first ChildAssoMotgroupesArchive filtered by the descriptif column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildAssoMotgroupesArchive requireOneByTexte(string $texte) Return the first ChildAssoMotgroupesArchive filtered by the texte column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildAssoMotgroupesArchive requireOneByImportance(int $importance) Return the first ChildAssoMotgroupesArchive filtered by the importance column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildAssoMotgroupesArchive requireOneByObjetsEnLien(string $objets_en_lien) Return the first ChildAssoMotgroupesArchive filtered by the objets_en_lien column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildAssoMotgroupesArchive requireOneBySysteme(boolean $systeme) Return the first ChildAssoMotgroupesArchive filtered by the systeme column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildAssoMotgroupesArchive requireOneByActif(int $actif) Return the first ChildAssoMotgroupesArchive filtered by the actif column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildAssoMotgroupesArchive requireOneByOptions(string $options) Return the first ChildAssoMotgroupesArchive filtered by the options column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildAssoMotgroupesArchive requireOneByCreatedAt(string $created_at) Return the first ChildAssoMotgroupesArchive filtered by the created_at column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildAssoMotgroupesArchive requireOneByUpdatedAt(string $updated_at) Return the first ChildAssoMotgroupesArchive filtered by the updated_at column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildAssoMotgroupesArchive requireOneByArchivedAt(string $archived_at) Return the first ChildAssoMotgroupesArchive filtered by the archived_at column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 *
 * @method     ChildAssoMotgroupesArchive[]|ObjectCollection find(ConnectionInterface $con = null) Return ChildAssoMotgroupesArchive objects based on current ModelCriteria
 * @method     ChildAssoMotgroupesArchive[]|ObjectCollection findByIdMotgroupe(string $id_motgroupe) Return ChildAssoMotgroupesArchive objects filtered by the id_motgroupe column
 * @method     ChildAssoMotgroupesArchive[]|ObjectCollection findByIdParent(string $id_parent) Return ChildAssoMotgroupesArchive objects filtered by the id_parent column
 * @method     ChildAssoMotgroupesArchive[]|ObjectCollection findByNom(string $nom) Return ChildAssoMotgroupesArchive objects filtered by the nom column
 * @method     ChildAssoMotgroupesArchive[]|ObjectCollection findByNomcourt(string $nomcourt) Return ChildAssoMotgroupesArchive objects filtered by the nomcourt column
 * @method     ChildAssoMotgroupesArchive[]|ObjectCollection findByDescriptif(string $descriptif) Return ChildAssoMotgroupesArchive objects filtered by the descriptif column
 * @method     ChildAssoMotgroupesArchive[]|ObjectCollection findByTexte(string $texte) Return ChildAssoMotgroupesArchive objects filtered by the texte column
 * @method     ChildAssoMotgroupesArchive[]|ObjectCollection findByImportance(int $importance) Return ChildAssoMotgroupesArchive objects filtered by the importance column
 * @method     ChildAssoMotgroupesArchive[]|ObjectCollection findByObjetsEnLien(string $objets_en_lien) Return ChildAssoMotgroupesArchive objects filtered by the objets_en_lien column
 * @method     ChildAssoMotgroupesArchive[]|ObjectCollection findBySysteme(boolean $systeme) Return ChildAssoMotgroupesArchive objects filtered by the systeme column
 * @method     ChildAssoMotgroupesArchive[]|ObjectCollection findByActif(int $actif) Return ChildAssoMotgroupesArchive objects filtered by the actif column
 * @method     ChildAssoMotgroupesArchive[]|ObjectCollection findByOptions(string $options) Return ChildAssoMotgroupesArchive objects filtered by the options column
 * @method     ChildAssoMotgroupesArchive[]|ObjectCollection findByCreatedAt(string $created_at) Return ChildAssoMotgroupesArchive objects filtered by the created_at column
 * @method     ChildAssoMotgroupesArchive[]|ObjectCollection findByUpdatedAt(string $updated_at) Return ChildAssoMotgroupesArchive objects filtered by the updated_at column
 * @method     ChildAssoMotgroupesArchive[]|ObjectCollection findByArchivedAt(string $archived_at) Return ChildAssoMotgroupesArchive objects filtered by the archived_at column
 * @method     ChildAssoMotgroupesArchive[]|\Propel\Runtime\Util\PropelModelPager paginate($page = 1, $maxPerPage = 10, ConnectionInterface $con = null) Issue a SELECT query based on the current ModelCriteria and uses a page and a maximum number of results per page to compute an offset and a limit
 *
 */
abstract class AssoMotgroupesArchiveQuery extends ModelCriteria
{
    protected $entityNotFoundExceptionClass = '\\Propel\\Runtime\\Exception\\EntityNotFoundException';

    /**
     * Initializes internal state of \Base\AssoMotgroupesArchiveQuery object.
     *
     * @param     string $dbName The database name
     * @param     string $modelName The phpName of a model, e.g. 'Book'
     * @param     string $modelAlias The alias for the model in this query, e.g. 'b'
     */
    public function __construct($dbName = 'simplasso', $modelName = '\\AssoMotgroupesArchive', $modelAlias = null)
    {
        parent::__construct($dbName, $modelName, $modelAlias);
    }

    /**
     * Returns a new ChildAssoMotgroupesArchiveQuery object.
     *
     * @param     string $modelAlias The alias of a model in the query
     * @param     Criteria $criteria Optional Criteria to build the query from
     *
     * @return ChildAssoMotgroupesArchiveQuery
     */
    public static function create($modelAlias = null, Criteria $criteria = null)
    {
        if ($criteria instanceof ChildAssoMotgroupesArchiveQuery) {
            return $criteria;
        }
        $query = new ChildAssoMotgroupesArchiveQuery();
        if (null !== $modelAlias) {
            $query->setModelAlias($modelAlias);
        }
        if ($criteria instanceof Criteria) {
            $query->mergeWith($criteria);
        }

        return $query;
    }

    /**
     * Find object by primary key.
     * Propel uses the instance pool to skip the database if the object exists.
     * Go fast if the query is untouched.
     *
     * <code>
     * $obj  = $c->findPk(12, $con);
     * </code>
     *
     * @param mixed $key Primary key to use for the query
     * @param ConnectionInterface $con an optional connection object
     *
     * @return ChildAssoMotgroupesArchive|array|mixed the result, formatted by the current formatter
     */
    public function findPk($key, ConnectionInterface $con = null)
    {
        if ($key === null) {
            return null;
        }

        if ($con === null) {
            $con = Propel::getServiceContainer()->getReadConnection(AssoMotgroupesArchiveTableMap::DATABASE_NAME);
        }

        $this->basePreSelect($con);

        if (
            $this->formatter || $this->modelAlias || $this->with || $this->select
            || $this->selectColumns || $this->asColumns || $this->selectModifiers
            || $this->map || $this->having || $this->joins
        ) {
            return $this->findPkComplex($key, $con);
        }

        if ((null !== ($obj = AssoMotgroupesArchiveTableMap::getInstanceFromPool(null === $key || is_scalar($key) || is_callable([$key, '__toString']) ? (string) $key : $key)))) {
            // the object is already in the instance pool
            return $obj;
        }

        return $this->findPkSimple($key, $con);
    }

    /**
     * Find object by primary key using raw SQL to go fast.
     * Bypass doSelect() and the object formatter by using generated code.
     *
     * @param     mixed $key Primary key to use for the query
     * @param     ConnectionInterface $con A connection object
     *
     * @throws \Propel\Runtime\Exception\PropelException
     *
     * @return ChildAssoMotgroupesArchive A model object, or null if the key is not found
     */
    protected function findPkSimple($key, ConnectionInterface $con)
    {
        $sql = 'SELECT `id_motgroupe`, `id_parent`, `nom`, `nomcourt`, `descriptif`, `texte`, `importance`, `objets_en_lien`, `systeme`, `actif`, `options`, `created_at`, `updated_at`, `archived_at` FROM `asso_motgroupes_archive` WHERE `id_motgroupe` = :p0';
        try {
            $stmt = $con->prepare($sql);
            $stmt->bindValue(':p0', $key, PDO::PARAM_INT);
            $stmt->execute();
        } catch (Exception $e) {
            Propel::log($e->getMessage(), Propel::LOG_ERR);
            throw new PropelException(sprintf('Unable to execute SELECT statement [%s]', $sql), 0, $e);
        }
        $obj = null;
        if ($row = $stmt->fetch(\PDO::FETCH_NUM)) {
            /** @var ChildAssoMotgroupesArchive $obj */
            $obj = new ChildAssoMotgroupesArchive();
            $obj->hydrate($row);
            AssoMotgroupesArchiveTableMap::addInstanceToPool($obj, null === $key || is_scalar($key) || is_callable([$key, '__toString']) ? (string) $key : $key);
        }
        $stmt->closeCursor();

        return $obj;
    }

    /**
     * Find object by primary key.
     *
     * @param     mixed $key Primary key to use for the query
     * @param     ConnectionInterface $con A connection object
     *
     * @return ChildAssoMotgroupesArchive|array|mixed the result, formatted by the current formatter
     */
    protected function findPkComplex($key, ConnectionInterface $con)
    {
        // As the query uses a PK condition, no limit(1) is necessary.
        $criteria = $this->isKeepQuery() ? clone $this : $this;
        $dataFetcher = $criteria
            ->filterByPrimaryKey($key)
            ->doSelect($con);

        return $criteria->getFormatter()->init($criteria)->formatOne($dataFetcher);
    }

    /**
     * Find objects by primary key
     * <code>
     * $objs = $c->findPks(array(12, 56, 832), $con);
     * </code>
     * @param     array $keys Primary keys to use for the query
     * @param     ConnectionInterface $con an optional connection object
     *
     * @return ObjectCollection|array|mixed the list of results, formatted by the current formatter
     */
    public function findPks($keys, ConnectionInterface $con = null)
    {
        if (null === $con) {
            $con = Propel::getServiceContainer()->getReadConnection($this->getDbName());
        }
        $this->basePreSelect($con);
        $criteria = $this->isKeepQuery() ? clone $this : $this;
        $dataFetcher = $criteria
            ->filterByPrimaryKeys($keys)
            ->doSelect($con);

        return $criteria->getFormatter()->init($criteria)->format($dataFetcher);
    }

    /**
     * Filter the query by primary key
     *
     * @param     mixed $key Primary key to use for the query
     *
     * @return $this|ChildAssoMotgroupesArchiveQuery The current query, for fluid interface
     */
    public function filterByPrimaryKey($key)
    {

        return $this->addUsingAlias(AssoMotgroupesArchiveTableMap::COL_ID_MOTGROUPE, $key, Criteria::EQUAL);
    }

    /**
     * Filter the query by a list of primary keys
     *
     * @param     array $keys The list of primary key to use for the query
     *
     * @return $this|ChildAssoMotgroupesArchiveQuery The current query, for fluid interface
     */
    public function filterByPrimaryKeys($keys)
    {

        return $this->addUsingAlias(AssoMotgroupesArchiveTableMap::COL_ID_MOTGROUPE, $keys, Criteria::IN);
    }

    /**
     * Filter the query on the id_motgroupe column
     *
     * Example usage:
     * <code>
     * $query->filterByIdMotgroupe(1234); // WHERE id_motgroupe = 1234
     * $query->filterByIdMotgroupe(array(12, 34)); // WHERE id_motgroupe IN (12, 34)
     * $query->filterByIdMotgroupe(array('min' => 12)); // WHERE id_motgroupe > 12
     * </code>
     *
     * @param     mixed $idMotgroupe The value to use as filter.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildAssoMotgroupesArchiveQuery The current query, for fluid interface
     */
    public function filterByIdMotgroupe($idMotgroupe = null, $comparison = null)
    {
        if (is_array($idMotgroupe)) {
            $useMinMax = false;
            if (isset($idMotgroupe['min'])) {
                $this->addUsingAlias(AssoMotgroupesArchiveTableMap::COL_ID_MOTGROUPE, $idMotgroupe['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($idMotgroupe['max'])) {
                $this->addUsingAlias(AssoMotgroupesArchiveTableMap::COL_ID_MOTGROUPE, $idMotgroupe['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(AssoMotgroupesArchiveTableMap::COL_ID_MOTGROUPE, $idMotgroupe, $comparison);
    }

    /**
     * Filter the query on the id_parent column
     *
     * Example usage:
     * <code>
     * $query->filterByIdParent(1234); // WHERE id_parent = 1234
     * $query->filterByIdParent(array(12, 34)); // WHERE id_parent IN (12, 34)
     * $query->filterByIdParent(array('min' => 12)); // WHERE id_parent > 12
     * </code>
     *
     * @param     mixed $idParent The value to use as filter.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildAssoMotgroupesArchiveQuery The current query, for fluid interface
     */
    public function filterByIdParent($idParent = null, $comparison = null)
    {
        if (is_array($idParent)) {
            $useMinMax = false;
            if (isset($idParent['min'])) {
                $this->addUsingAlias(AssoMotgroupesArchiveTableMap::COL_ID_PARENT, $idParent['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($idParent['max'])) {
                $this->addUsingAlias(AssoMotgroupesArchiveTableMap::COL_ID_PARENT, $idParent['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(AssoMotgroupesArchiveTableMap::COL_ID_PARENT, $idParent, $comparison);
    }

    /**
     * Filter the query on the nom column
     *
     * Example usage:
     * <code>
     * $query->filterByNom('fooValue');   // WHERE nom = 'fooValue'
     * $query->filterByNom('%fooValue%', Criteria::LIKE); // WHERE nom LIKE '%fooValue%'
     * </code>
     *
     * @param     string $nom The value to use as filter.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildAssoMotgroupesArchiveQuery The current query, for fluid interface
     */
    public function filterByNom($nom = null, $comparison = null)
    {
        if (null === $comparison) {
            if (is_array($nom)) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(AssoMotgroupesArchiveTableMap::COL_NOM, $nom, $comparison);
    }

    /**
     * Filter the query on the nomcourt column
     *
     * Example usage:
     * <code>
     * $query->filterByNomcourt('fooValue');   // WHERE nomcourt = 'fooValue'
     * $query->filterByNomcourt('%fooValue%', Criteria::LIKE); // WHERE nomcourt LIKE '%fooValue%'
     * </code>
     *
     * @param     string $nomcourt The value to use as filter.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildAssoMotgroupesArchiveQuery The current query, for fluid interface
     */
    public function filterByNomcourt($nomcourt = null, $comparison = null)
    {
        if (null === $comparison) {
            if (is_array($nomcourt)) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(AssoMotgroupesArchiveTableMap::COL_NOMCOURT, $nomcourt, $comparison);
    }

    /**
     * Filter the query on the descriptif column
     *
     * Example usage:
     * <code>
     * $query->filterByDescriptif('fooValue');   // WHERE descriptif = 'fooValue'
     * $query->filterByDescriptif('%fooValue%', Criteria::LIKE); // WHERE descriptif LIKE '%fooValue%'
     * </code>
     *
     * @param     string $descriptif The value to use as filter.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildAssoMotgroupesArchiveQuery The current query, for fluid interface
     */
    public function filterByDescriptif($descriptif = null, $comparison = null)
    {
        if (null === $comparison) {
            if (is_array($descriptif)) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(AssoMotgroupesArchiveTableMap::COL_DESCRIPTIF, $descriptif, $comparison);
    }

    /**
     * Filter the query on the texte column
     *
     * Example usage:
     * <code>
     * $query->filterByTexte('fooValue');   // WHERE texte = 'fooValue'
     * $query->filterByTexte('%fooValue%', Criteria::LIKE); // WHERE texte LIKE '%fooValue%'
     * </code>
     *
     * @param     string $texte The value to use as filter.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildAssoMotgroupesArchiveQuery The current query, for fluid interface
     */
    public function filterByTexte($texte = null, $comparison = null)
    {
        if (null === $comparison) {
            if (is_array($texte)) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(AssoMotgroupesArchiveTableMap::COL_TEXTE, $texte, $comparison);
    }

    /**
     * Filter the query on the importance column
     *
     * Example usage:
     * <code>
     * $query->filterByImportance(1234); // WHERE importance = 1234
     * $query->filterByImportance(array(12, 34)); // WHERE importance IN (12, 34)
     * $query->filterByImportance(array('min' => 12)); // WHERE importance > 12
     * </code>
     *
     * @param     mixed $importance The value to use as filter.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildAssoMotgroupesArchiveQuery The current query, for fluid interface
     */
    public function filterByImportance($importance = null, $comparison = null)
    {
        if (is_array($importance)) {
            $useMinMax = false;
            if (isset($importance['min'])) {
                $this->addUsingAlias(AssoMotgroupesArchiveTableMap::COL_IMPORTANCE, $importance['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($importance['max'])) {
                $this->addUsingAlias(AssoMotgroupesArchiveTableMap::COL_IMPORTANCE, $importance['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(AssoMotgroupesArchiveTableMap::COL_IMPORTANCE, $importance, $comparison);
    }

    /**
     * Filter the query on the objets_en_lien column
     *
     * Example usage:
     * <code>
     * $query->filterByObjetsEnLien('fooValue');   // WHERE objets_en_lien = 'fooValue'
     * $query->filterByObjetsEnLien('%fooValue%', Criteria::LIKE); // WHERE objets_en_lien LIKE '%fooValue%'
     * </code>
     *
     * @param     string $objetsEnLien The value to use as filter.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildAssoMotgroupesArchiveQuery The current query, for fluid interface
     */
    public function filterByObjetsEnLien($objetsEnLien = null, $comparison = null)
    {
        if (null === $comparison) {
            if (is_array($objetsEnLien)) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(AssoMotgroupesArchiveTableMap::COL_OBJETS_EN_LIEN, $objetsEnLien, $comparison);
    }

    /**
     * Filter the query on the systeme column
     *
     * Example usage:
     * <code>
     * $query->filterBySysteme(true); // WHERE systeme = true
     * $query->filterBySysteme('yes'); // WHERE systeme = true
     * </code>
     *
     * @param     boolean|string $systeme The value to use as filter.
     *              Non-boolean arguments are converted using the following rules:
     *                * 1, '1', 'true',  'on',  and 'yes' are converted to boolean true
     *                * 0, '0', 'false', 'off', and 'no'  are converted to boolean false
     *              Check on string values is case insensitive (so 'FaLsE' is seen as 'false').
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildAssoMotgroupesArchiveQuery The current query, for fluid interface
     */
    public function filterBySysteme($systeme = null, $comparison = null)
    {
        if (is_string($systeme)) {
            $systeme = in_array(strtolower($systeme), array('false', 'off', '-', 'no', 'n', '0', '')) ? false : true;
        }

        return $this->addUsingAlias(AssoMotgroupesArchiveTableMap::COL_SYSTEME, $systeme, $comparison);
    }

    /**
     * Filter the query on the actif column
     *
     * Example usage:
     * <code>
     * $query->filterByActif(1234); // WHERE actif = 1234
     * $query->filterByActif(array(12, 34)); // WHERE actif IN (12, 34)
     * $query->filterByActif(array('min' => 12)); // WHERE actif > 12
     * </code>
     *
     * @param     mixed $actif The value to use as filter.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildAssoMotgroupesArchiveQuery The current query, for fluid interface
     */
    public function filterByActif($actif = null, $comparison = null)
    {
        if (is_array($actif)) {
            $useMinMax = false;
            if (isset($actif['min'])) {
                $this->addUsingAlias(AssoMotgroupesArchiveTableMap::COL_ACTIF, $actif['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($actif['max'])) {
                $this->addUsingAlias(AssoMotgroupesArchiveTableMap::COL_ACTIF, $actif['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(AssoMotgroupesArchiveTableMap::COL_ACTIF, $actif, $comparison);
    }

    /**
     * Filter the query on the options column
     *
     * Example usage:
     * <code>
     * $query->filterByOptions('fooValue');   // WHERE options = 'fooValue'
     * $query->filterByOptions('%fooValue%', Criteria::LIKE); // WHERE options LIKE '%fooValue%'
     * </code>
     *
     * @param     string $options The value to use as filter.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildAssoMotgroupesArchiveQuery The current query, for fluid interface
     */
    public function filterByOptions($options = null, $comparison = null)
    {
        if (null === $comparison) {
            if (is_array($options)) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(AssoMotgroupesArchiveTableMap::COL_OPTIONS, $options, $comparison);
    }

    /**
     * Filter the query on the created_at column
     *
     * Example usage:
     * <code>
     * $query->filterByCreatedAt('2011-03-14'); // WHERE created_at = '2011-03-14'
     * $query->filterByCreatedAt('now'); // WHERE created_at = '2011-03-14'
     * $query->filterByCreatedAt(array('max' => 'yesterday')); // WHERE created_at > '2011-03-13'
     * </code>
     *
     * @param     mixed $createdAt The value to use as filter.
     *              Values can be integers (unix timestamps), DateTime objects, or strings.
     *              Empty strings are treated as NULL.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildAssoMotgroupesArchiveQuery The current query, for fluid interface
     */
    public function filterByCreatedAt($createdAt = null, $comparison = null)
    {
        if (is_array($createdAt)) {
            $useMinMax = false;
            if (isset($createdAt['min'])) {
                $this->addUsingAlias(AssoMotgroupesArchiveTableMap::COL_CREATED_AT, $createdAt['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($createdAt['max'])) {
                $this->addUsingAlias(AssoMotgroupesArchiveTableMap::COL_CREATED_AT, $createdAt['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(AssoMotgroupesArchiveTableMap::COL_CREATED_AT, $createdAt, $comparison);
    }

    /**
     * Filter the query on the updated_at column
     *
     * Example usage:
     * <code>
     * $query->filterByUpdatedAt('2011-03-14'); // WHERE updated_at = '2011-03-14'
     * $query->filterByUpdatedAt('now'); // WHERE updated_at = '2011-03-14'
     * $query->filterByUpdatedAt(array('max' => 'yesterday')); // WHERE updated_at > '2011-03-13'
     * </code>
     *
     * @param     mixed $updatedAt The value to use as filter.
     *              Values can be integers (unix timestamps), DateTime objects, or strings.
     *              Empty strings are treated as NULL.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildAssoMotgroupesArchiveQuery The current query, for fluid interface
     */
    public function filterByUpdatedAt($updatedAt = null, $comparison = null)
    {
        if (is_array($updatedAt)) {
            $useMinMax = false;
            if (isset($updatedAt['min'])) {
                $this->addUsingAlias(AssoMotgroupesArchiveTableMap::COL_UPDATED_AT, $updatedAt['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($updatedAt['max'])) {
                $this->addUsingAlias(AssoMotgroupesArchiveTableMap::COL_UPDATED_AT, $updatedAt['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(AssoMotgroupesArchiveTableMap::COL_UPDATED_AT, $updatedAt, $comparison);
    }

    /**
     * Filter the query on the archived_at column
     *
     * Example usage:
     * <code>
     * $query->filterByArchivedAt('2011-03-14'); // WHERE archived_at = '2011-03-14'
     * $query->filterByArchivedAt('now'); // WHERE archived_at = '2011-03-14'
     * $query->filterByArchivedAt(array('max' => 'yesterday')); // WHERE archived_at > '2011-03-13'
     * </code>
     *
     * @param     mixed $archivedAt The value to use as filter.
     *              Values can be integers (unix timestamps), DateTime objects, or strings.
     *              Empty strings are treated as NULL.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildAssoMotgroupesArchiveQuery The current query, for fluid interface
     */
    public function filterByArchivedAt($archivedAt = null, $comparison = null)
    {
        if (is_array($archivedAt)) {
            $useMinMax = false;
            if (isset($archivedAt['min'])) {
                $this->addUsingAlias(AssoMotgroupesArchiveTableMap::COL_ARCHIVED_AT, $archivedAt['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($archivedAt['max'])) {
                $this->addUsingAlias(AssoMotgroupesArchiveTableMap::COL_ARCHIVED_AT, $archivedAt['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(AssoMotgroupesArchiveTableMap::COL_ARCHIVED_AT, $archivedAt, $comparison);
    }

    /**
     * Exclude object from result
     *
     * @param   ChildAssoMotgroupesArchive $assoMotgroupesArchive Object to remove from the list of results
     *
     * @return $this|ChildAssoMotgroupesArchiveQuery The current query, for fluid interface
     */
    public function prune($assoMotgroupesArchive = null)
    {
        if ($assoMotgroupesArchive) {
            $this->addUsingAlias(AssoMotgroupesArchiveTableMap::COL_ID_MOTGROUPE, $assoMotgroupesArchive->getIdMotgroupe(), Criteria::NOT_EQUAL);
        }

        return $this;
    }

    /**
     * Deletes all rows from the asso_motgroupes_archive table.
     *
     * @param ConnectionInterface $con the connection to use
     * @return int The number of affected rows (if supported by underlying database driver).
     */
    public function doDeleteAll(ConnectionInterface $con = null)
    {
        if (null === $con) {
            $con = Propel::getServiceContainer()->getWriteConnection(AssoMotgroupesArchiveTableMap::DATABASE_NAME);
        }

        // use transaction because $criteria could contain info
        // for more than one table or we could emulating ON DELETE CASCADE, etc.
        return $con->transaction(function () use ($con) {
            $affectedRows = 0; // initialize var to track total num of affected rows
            $affectedRows += parent::doDeleteAll($con);
            // Because this db requires some delete cascade/set null emulation, we have to
            // clear the cached instance *after* the emulation has happened (since
            // instances get re-added by the select statement contained therein).
            AssoMotgroupesArchiveTableMap::clearInstancePool();
            AssoMotgroupesArchiveTableMap::clearRelatedInstancePool();

            return $affectedRows;
        });
    }

    /**
     * Performs a DELETE on the database based on the current ModelCriteria
     *
     * @param ConnectionInterface $con the connection to use
     * @return int             The number of affected rows (if supported by underlying database driver).  This includes CASCADE-related rows
     *                         if supported by native driver or if emulated using Propel.
     * @throws PropelException Any exceptions caught during processing will be
     *                         rethrown wrapped into a PropelException.
     */
    public function delete(ConnectionInterface $con = null)
    {
        if (null === $con) {
            $con = Propel::getServiceContainer()->getWriteConnection(AssoMotgroupesArchiveTableMap::DATABASE_NAME);
        }

        $criteria = $this;

        // Set the correct dbName
        $criteria->setDbName(AssoMotgroupesArchiveTableMap::DATABASE_NAME);

        // use transaction because $criteria could contain info
        // for more than one table or we could emulating ON DELETE CASCADE, etc.
        return $con->transaction(function () use ($con, $criteria) {
            $affectedRows = 0; // initialize var to track total num of affected rows

            AssoMotgroupesArchiveTableMap::removeInstanceFromPool($criteria);

            $affectedRows += ModelCriteria::delete($con);
            AssoMotgroupesArchiveTableMap::clearRelatedInstancePool();

            return $affectedRows;
        });
    }

} // AssoMotgroupesArchiveQuery
