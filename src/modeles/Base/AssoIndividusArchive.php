<?php

namespace Base;

use \AssoIndividusArchiveQuery as ChildAssoIndividusArchiveQuery;
use \DateTime;
use \Exception;
use \PDO;
use Map\AssoIndividusArchiveTableMap;
use Propel\Runtime\Propel;
use Propel\Runtime\ActiveQuery\Criteria;
use Propel\Runtime\ActiveQuery\ModelCriteria;
use Propel\Runtime\ActiveRecord\ActiveRecordInterface;
use Propel\Runtime\Collection\Collection;
use Propel\Runtime\Connection\ConnectionInterface;
use Propel\Runtime\Exception\BadMethodCallException;
use Propel\Runtime\Exception\LogicException;
use Propel\Runtime\Exception\PropelException;
use Propel\Runtime\Map\TableMap;
use Propel\Runtime\Parser\AbstractParser;
use Propel\Runtime\Util\PropelDateTime;

/**
 * Base class that represents a row from the 'asso_individus_archive' table.
 *
 *
 *
 * @package    propel.generator..Base
 */
abstract class AssoIndividusArchive implements ActiveRecordInterface
{
    /**
     * TableMap class name
     */
    const TABLE_MAP = '\\Map\\AssoIndividusArchiveTableMap';


    /**
     * attribute to determine if this object has previously been saved.
     * @var boolean
     */
    protected $new = true;

    /**
     * attribute to determine whether this object has been deleted.
     * @var boolean
     */
    protected $deleted = false;

    /**
     * The columns that have been modified in current object.
     * Tracking modified columns allows us to only update modified columns.
     * @var array
     */
    protected $modifiedColumns = array();

    /**
     * The (virtual) columns that are added at runtime
     * The formatters can add supplementary columns based on a resultset
     * @var array
     */
    protected $virtualColumns = array();

    /**
     * The value for the id_individu field.
     *
     * @var        string
     */
    protected $id_individu;

    /**
     * The value for the nom field.
     *
     * @var        string
     */
    protected $nom;

    /**
     * The value for the bio field.
     *
     * @var        string
     */
    protected $bio;

    /**
     * The value for the email field.
     *
     * @var        string
     */
    protected $email;

    /**
     * The value for the login field.
     *
     * @var        string
     */
    protected $login;

    /**
     * The value for the pass field.
     *
     * @var        string
     */
    protected $pass;

    /**
     * The value for the alea_actuel field.
     *
     * @var        string
     */
    protected $alea_actuel;

    /**
     * The value for the alea_futur field.
     *
     * @var        string
     */
    protected $alea_futur;

    /**
     * The value for the token field.
     *
     * @var        string
     */
    protected $token;

    /**
     * The value for the token_time field.
     *
     * @var        string
     */
    protected $token_time;

    /**
     * The value for the civilite field.
     *
     * @var        string
     */
    protected $civilite;

    /**
     * The value for the sexe field.
     *
     * @var        string
     */
    protected $sexe;

    /**
     * The value for the nom_famille field.
     *
     * @var        string
     */
    protected $nom_famille;

    /**
     * The value for the prenom field.
     *
     * @var        string
     */
    protected $prenom;

    /**
     * The value for the naissance field.
     *
     * @var        DateTime
     */
    protected $naissance;

    /**
     * The value for the adresse field.
     *
     * @var        string
     */
    protected $adresse;

    /**
     * The value for the codepostal field.
     *
     * @var        string
     */
    protected $codepostal;

    /**
     * The value for the ville field.
     *
     * @var        string
     */
    protected $ville;

    /**
     * The value for the pays field.
     *
     * @var        string
     */
    protected $pays;

    /**
     * The value for the telephone field.
     *
     * @var        string
     */
    protected $telephone;

    /**
     * The value for the telephone_pro field.
     *
     * @var        string
     */
    protected $telephone_pro;

    /**
     * The value for the fax field.
     *
     * @var        string
     */
    protected $fax;

    /**
     * The value for the mobile field.
     *
     * @var        string
     */
    protected $mobile;

    /**
     * The value for the url field.
     *
     * @var        string
     */
    protected $url;

    /**
     * The value for the profession field.
     *
     * @var        string
     */
    protected $profession;

    /**
     * The value for the contact_souhait field.
     *
     * Note: this column has a database default value of: true
     * @var        boolean
     */
    protected $contact_souhait;

    /**
     * The value for the observation field.
     *
     * @var        string
     */
    protected $observation;

    /**
     * The value for the created_at field.
     *
     * @var        DateTime
     */
    protected $created_at;

    /**
     * The value for the updated_at field.
     *
     * @var        DateTime
     */
    protected $updated_at;

    /**
     * The value for the archived_at field.
     *
     * @var        DateTime
     */
    protected $archived_at;

    /**
     * Flag to prevent endless save loop, if this object is referenced
     * by another object which falls in this transaction.
     *
     * @var boolean
     */
    protected $alreadyInSave = false;

    /**
     * Applies default values to this object.
     * This method should be called from the object's constructor (or
     * equivalent initialization method).
     * @see __construct()
     */
    public function applyDefaultValues()
    {
        $this->contact_souhait = true;
    }

    /**
     * Initializes internal state of Base\AssoIndividusArchive object.
     * @see applyDefaults()
     */
    public function __construct()
    {
        $this->applyDefaultValues();
    }

    /**
     * Returns whether the object has been modified.
     *
     * @return boolean True if the object has been modified.
     */
    public function isModified()
    {
        return !!$this->modifiedColumns;
    }

    /**
     * Has specified column been modified?
     *
     * @param  string  $col column fully qualified name (TableMap::TYPE_COLNAME), e.g. Book::AUTHOR_ID
     * @return boolean True if $col has been modified.
     */
    public function isColumnModified($col)
    {
        return $this->modifiedColumns && isset($this->modifiedColumns[$col]);
    }

    /**
     * Get the columns that have been modified in this object.
     * @return array A unique list of the modified column names for this object.
     */
    public function getModifiedColumns()
    {
        return $this->modifiedColumns ? array_keys($this->modifiedColumns) : [];
    }

    /**
     * Returns whether the object has ever been saved.  This will
     * be false, if the object was retrieved from storage or was created
     * and then saved.
     *
     * @return boolean true, if the object has never been persisted.
     */
    public function isNew()
    {
        return $this->new;
    }

    /**
     * Setter for the isNew attribute.  This method will be called
     * by Propel-generated children and objects.
     *
     * @param boolean $b the state of the object.
     */
    public function setNew($b)
    {
        $this->new = (boolean) $b;
    }

    /**
     * Whether this object has been deleted.
     * @return boolean The deleted state of this object.
     */
    public function isDeleted()
    {
        return $this->deleted;
    }

    /**
     * Specify whether this object has been deleted.
     * @param  boolean $b The deleted state of this object.
     * @return void
     */
    public function setDeleted($b)
    {
        $this->deleted = (boolean) $b;
    }

    /**
     * Sets the modified state for the object to be false.
     * @param  string $col If supplied, only the specified column is reset.
     * @return void
     */
    public function resetModified($col = null)
    {
        if (null !== $col) {
            if (isset($this->modifiedColumns[$col])) {
                unset($this->modifiedColumns[$col]);
            }
        } else {
            $this->modifiedColumns = array();
        }
    }

    /**
     * Compares this with another <code>AssoIndividusArchive</code> instance.  If
     * <code>obj</code> is an instance of <code>AssoIndividusArchive</code>, delegates to
     * <code>equals(AssoIndividusArchive)</code>.  Otherwise, returns <code>false</code>.
     *
     * @param  mixed   $obj The object to compare to.
     * @return boolean Whether equal to the object specified.
     */
    public function equals($obj)
    {
        if (!$obj instanceof static) {
            return false;
        }

        if ($this === $obj) {
            return true;
        }

        if (null === $this->getPrimaryKey() || null === $obj->getPrimaryKey()) {
            return false;
        }

        return $this->getPrimaryKey() === $obj->getPrimaryKey();
    }

    /**
     * Get the associative array of the virtual columns in this object
     *
     * @return array
     */
    public function getVirtualColumns()
    {
        return $this->virtualColumns;
    }

    /**
     * Checks the existence of a virtual column in this object
     *
     * @param  string  $name The virtual column name
     * @return boolean
     */
    public function hasVirtualColumn($name)
    {
        return array_key_exists($name, $this->virtualColumns);
    }

    /**
     * Get the value of a virtual column in this object
     *
     * @param  string $name The virtual column name
     * @return mixed
     *
     * @throws PropelException
     */
    public function getVirtualColumn($name)
    {
        if (!$this->hasVirtualColumn($name)) {
            throw new PropelException(sprintf('Cannot get value of inexistent virtual column %s.', $name));
        }

        return $this->virtualColumns[$name];
    }

    /**
     * Set the value of a virtual column in this object
     *
     * @param string $name  The virtual column name
     * @param mixed  $value The value to give to the virtual column
     *
     * @return $this|AssoIndividusArchive The current object, for fluid interface
     */
    public function setVirtualColumn($name, $value)
    {
        $this->virtualColumns[$name] = $value;

        return $this;
    }

    /**
     * Logs a message using Propel::log().
     *
     * @param  string  $msg
     * @param  int     $priority One of the Propel::LOG_* logging levels
     * @return boolean
     */
    protected function log($msg, $priority = Propel::LOG_INFO)
    {
        return Propel::log(get_class($this) . ': ' . $msg, $priority);
    }

    /**
     * Export the current object properties to a string, using a given parser format
     * <code>
     * $book = BookQuery::create()->findPk(9012);
     * echo $book->exportTo('JSON');
     *  => {"Id":9012,"Title":"Don Juan","ISBN":"0140422161","Price":12.99,"PublisherId":1234,"AuthorId":5678}');
     * </code>
     *
     * @param  mixed   $parser                 A AbstractParser instance, or a format name ('XML', 'YAML', 'JSON', 'CSV')
     * @param  boolean $includeLazyLoadColumns (optional) Whether to include lazy load(ed) columns. Defaults to TRUE.
     * @return string  The exported data
     */
    public function exportTo($parser, $includeLazyLoadColumns = true)
    {
        if (!$parser instanceof AbstractParser) {
            $parser = AbstractParser::getParser($parser);
        }

        return $parser->fromArray($this->toArray(TableMap::TYPE_PHPNAME, $includeLazyLoadColumns, array(), true));
    }

    /**
     * Clean up internal collections prior to serializing
     * Avoids recursive loops that turn into segmentation faults when serializing
     */
    public function __sleep()
    {
        $this->clearAllReferences();

        $cls = new \ReflectionClass($this);
        $propertyNames = [];
        $serializableProperties = array_diff($cls->getProperties(), $cls->getProperties(\ReflectionProperty::IS_STATIC));

        foreach($serializableProperties as $property) {
            $propertyNames[] = $property->getName();
        }

        return $propertyNames;
    }

    /**
     * Get the [id_individu] column value.
     *
     * @return string
     */
    public function getIdIndividu()
    {
        return $this->id_individu;
    }

    /**
     * Get the [nom] column value.
     *
     * @return string
     */
    public function getNom()
    {
        return $this->nom;
    }

    /**
     * Get the [bio] column value.
     *
     * @return string
     */
    public function getBio()
    {
        return $this->bio;
    }

    /**
     * Get the [email] column value.
     *
     * @return string
     */
    public function getEmail()
    {
        return $this->email;
    }

    /**
     * Get the [login] column value.
     *
     * @return string
     */
    public function getLogin()
    {
        return $this->login;
    }

    /**
     * Get the [pass] column value.
     *
     * @return string
     */
    public function getPass()
    {
        return $this->pass;
    }

    /**
     * Get the [alea_actuel] column value.
     *
     * @return string
     */
    public function getAleaActuel()
    {
        return $this->alea_actuel;
    }

    /**
     * Get the [alea_futur] column value.
     *
     * @return string
     */
    public function getAleaFutur()
    {
        return $this->alea_futur;
    }

    /**
     * Get the [token] column value.
     *
     * @return string
     */
    public function getToken()
    {
        return $this->token;
    }

    /**
     * Get the [token_time] column value.
     *
     * @return string
     */
    public function getTokenTime()
    {
        return $this->token_time;
    }

    /**
     * Get the [civilite] column value.
     *
     * @return string
     */
    public function getCivilite()
    {
        return $this->civilite;
    }

    /**
     * Get the [sexe] column value.
     *
     * @return string
     */
    public function getSexe()
    {
        return $this->sexe;
    }

    /**
     * Get the [nom_famille] column value.
     *
     * @return string
     */
    public function getNomFamille()
    {
        return $this->nom_famille;
    }

    /**
     * Get the [prenom] column value.
     *
     * @return string
     */
    public function getPrenom()
    {
        return $this->prenom;
    }

    /**
     * Get the [optionally formatted] temporal [naissance] column value.
     *
     *
     * @param      string $format The date/time format string (either date()-style or strftime()-style).
     *                            If format is NULL, then the raw DateTime object will be returned.
     *
     * @return string|DateTime Formatted date/time value as string or DateTime object (if format is NULL), NULL if column is NULL, and 0 if column value is 0000-00-00
     *
     * @throws PropelException - if unable to parse/validate the date/time value.
     */
    public function getNaissance($format = NULL)
    {
        if ($format === null) {
            return $this->naissance;
        } else {
            return $this->naissance instanceof \DateTimeInterface ? $this->naissance->format($format) : null;
        }
    }

    /**
     * Get the [adresse] column value.
     *
     * @return string
     */
    public function getAdresse()
    {
        return $this->adresse;
    }

    /**
     * Get the [codepostal] column value.
     *
     * @return string
     */
    public function getCodepostal()
    {
        return $this->codepostal;
    }

    /**
     * Get the [ville] column value.
     *
     * @return string
     */
    public function getVille()
    {
        return $this->ville;
    }

    /**
     * Get the [pays] column value.
     *
     * @return string
     */
    public function getPays()
    {
        return $this->pays;
    }

    /**
     * Get the [telephone] column value.
     *
     * @return string
     */
    public function getTelephone()
    {
        return $this->telephone;
    }

    /**
     * Get the [telephone_pro] column value.
     *
     * @return string
     */
    public function getTelephonePro()
    {
        return $this->telephone_pro;
    }

    /**
     * Get the [fax] column value.
     *
     * @return string
     */
    public function getFax()
    {
        return $this->fax;
    }

    /**
     * Get the [mobile] column value.
     *
     * @return string
     */
    public function getMobile()
    {
        return $this->mobile;
    }

    /**
     * Get the [url] column value.
     *
     * @return string
     */
    public function getUrl()
    {
        return $this->url;
    }

    /**
     * Get the [profession] column value.
     *
     * @return string
     */
    public function getProfession()
    {
        return $this->profession;
    }

    /**
     * Get the [contact_souhait] column value.
     *
     * @return boolean
     */
    public function getContactSouhait()
    {
        return $this->contact_souhait;
    }

    /**
     * Get the [contact_souhait] column value.
     *
     * @return boolean
     */
    public function isContactSouhait()
    {
        return $this->getContactSouhait();
    }

    /**
     * Get the [observation] column value.
     *
     * @return string
     */
    public function getObservation()
    {
        return $this->observation;
    }

    /**
     * Get the [optionally formatted] temporal [created_at] column value.
     *
     *
     * @param      string $format The date/time format string (either date()-style or strftime()-style).
     *                            If format is NULL, then the raw DateTime object will be returned.
     *
     * @return string|DateTime Formatted date/time value as string or DateTime object (if format is NULL), NULL if column is NULL, and 0 if column value is 0000-00-00 00:00:00
     *
     * @throws PropelException - if unable to parse/validate the date/time value.
     */
    public function getCreatedAt($format = NULL)
    {
        if ($format === null) {
            return $this->created_at;
        } else {
            return $this->created_at instanceof \DateTimeInterface ? $this->created_at->format($format) : null;
        }
    }

    /**
     * Get the [optionally formatted] temporal [updated_at] column value.
     *
     *
     * @param      string $format The date/time format string (either date()-style or strftime()-style).
     *                            If format is NULL, then the raw DateTime object will be returned.
     *
     * @return string|DateTime Formatted date/time value as string or DateTime object (if format is NULL), NULL if column is NULL, and 0 if column value is 0000-00-00 00:00:00
     *
     * @throws PropelException - if unable to parse/validate the date/time value.
     */
    public function getUpdatedAt($format = NULL)
    {
        if ($format === null) {
            return $this->updated_at;
        } else {
            return $this->updated_at instanceof \DateTimeInterface ? $this->updated_at->format($format) : null;
        }
    }

    /**
     * Get the [optionally formatted] temporal [archived_at] column value.
     *
     *
     * @param      string $format The date/time format string (either date()-style or strftime()-style).
     *                            If format is NULL, then the raw DateTime object will be returned.
     *
     * @return string|DateTime Formatted date/time value as string or DateTime object (if format is NULL), NULL if column is NULL, and 0 if column value is 0000-00-00 00:00:00
     *
     * @throws PropelException - if unable to parse/validate the date/time value.
     */
    public function getArchivedAt($format = NULL)
    {
        if ($format === null) {
            return $this->archived_at;
        } else {
            return $this->archived_at instanceof \DateTimeInterface ? $this->archived_at->format($format) : null;
        }
    }

    /**
     * Set the value of [id_individu] column.
     *
     * @param string $v new value
     * @return $this|\AssoIndividusArchive The current object (for fluent API support)
     */
    public function setIdIndividu($v)
    {
        if ($v !== null) {
            $v = (string) $v;
        }

        if ($this->id_individu !== $v) {
            $this->id_individu = $v;
            $this->modifiedColumns[AssoIndividusArchiveTableMap::COL_ID_INDIVIDU] = true;
        }

        return $this;
    } // setIdIndividu()

    /**
     * Set the value of [nom] column.
     *
     * @param string $v new value
     * @return $this|\AssoIndividusArchive The current object (for fluent API support)
     */
    public function setNom($v)
    {
        if ($v !== null) {
            $v = (string) $v;
        }

        if ($this->nom !== $v) {
            $this->nom = $v;
            $this->modifiedColumns[AssoIndividusArchiveTableMap::COL_NOM] = true;
        }

        return $this;
    } // setNom()

    /**
     * Set the value of [bio] column.
     *
     * @param string $v new value
     * @return $this|\AssoIndividusArchive The current object (for fluent API support)
     */
    public function setBio($v)
    {
        if ($v !== null) {
            $v = (string) $v;
        }

        if ($this->bio !== $v) {
            $this->bio = $v;
            $this->modifiedColumns[AssoIndividusArchiveTableMap::COL_BIO] = true;
        }

        return $this;
    } // setBio()

    /**
     * Set the value of [email] column.
     *
     * @param string $v new value
     * @return $this|\AssoIndividusArchive The current object (for fluent API support)
     */
    public function setEmail($v)
    {
        if ($v !== null) {
            $v = (string) $v;
        }

        if ($this->email !== $v) {
            $this->email = $v;
            $this->modifiedColumns[AssoIndividusArchiveTableMap::COL_EMAIL] = true;
        }

        return $this;
    } // setEmail()

    /**
     * Set the value of [login] column.
     *
     * @param string $v new value
     * @return $this|\AssoIndividusArchive The current object (for fluent API support)
     */
    public function setLogin($v)
    {
        if ($v !== null) {
            $v = (string) $v;
        }

        if ($this->login !== $v) {
            $this->login = $v;
            $this->modifiedColumns[AssoIndividusArchiveTableMap::COL_LOGIN] = true;
        }

        return $this;
    } // setLogin()

    /**
     * Set the value of [pass] column.
     *
     * @param string $v new value
     * @return $this|\AssoIndividusArchive The current object (for fluent API support)
     */
    public function setPass($v)
    {
        if ($v !== null) {
            $v = (string) $v;
        }

        if ($this->pass !== $v) {
            $this->pass = $v;
            $this->modifiedColumns[AssoIndividusArchiveTableMap::COL_PASS] = true;
        }

        return $this;
    } // setPass()

    /**
     * Set the value of [alea_actuel] column.
     *
     * @param string $v new value
     * @return $this|\AssoIndividusArchive The current object (for fluent API support)
     */
    public function setAleaActuel($v)
    {
        if ($v !== null) {
            $v = (string) $v;
        }

        if ($this->alea_actuel !== $v) {
            $this->alea_actuel = $v;
            $this->modifiedColumns[AssoIndividusArchiveTableMap::COL_ALEA_ACTUEL] = true;
        }

        return $this;
    } // setAleaActuel()

    /**
     * Set the value of [alea_futur] column.
     *
     * @param string $v new value
     * @return $this|\AssoIndividusArchive The current object (for fluent API support)
     */
    public function setAleaFutur($v)
    {
        if ($v !== null) {
            $v = (string) $v;
        }

        if ($this->alea_futur !== $v) {
            $this->alea_futur = $v;
            $this->modifiedColumns[AssoIndividusArchiveTableMap::COL_ALEA_FUTUR] = true;
        }

        return $this;
    } // setAleaFutur()

    /**
     * Set the value of [token] column.
     *
     * @param string $v new value
     * @return $this|\AssoIndividusArchive The current object (for fluent API support)
     */
    public function setToken($v)
    {
        if ($v !== null) {
            $v = (string) $v;
        }

        if ($this->token !== $v) {
            $this->token = $v;
            $this->modifiedColumns[AssoIndividusArchiveTableMap::COL_TOKEN] = true;
        }

        return $this;
    } // setToken()

    /**
     * Set the value of [token_time] column.
     *
     * @param string $v new value
     * @return $this|\AssoIndividusArchive The current object (for fluent API support)
     */
    public function setTokenTime($v)
    {
        if ($v !== null) {
            $v = (string) $v;
        }

        if ($this->token_time !== $v) {
            $this->token_time = $v;
            $this->modifiedColumns[AssoIndividusArchiveTableMap::COL_TOKEN_TIME] = true;
        }

        return $this;
    } // setTokenTime()

    /**
     * Set the value of [civilite] column.
     *
     * @param string $v new value
     * @return $this|\AssoIndividusArchive The current object (for fluent API support)
     */
    public function setCivilite($v)
    {
        if ($v !== null) {
            $v = (string) $v;
        }

        if ($this->civilite !== $v) {
            $this->civilite = $v;
            $this->modifiedColumns[AssoIndividusArchiveTableMap::COL_CIVILITE] = true;
        }

        return $this;
    } // setCivilite()

    /**
     * Set the value of [sexe] column.
     *
     * @param string $v new value
     * @return $this|\AssoIndividusArchive The current object (for fluent API support)
     */
    public function setSexe($v)
    {
        if ($v !== null) {
            $v = (string) $v;
        }

        if ($this->sexe !== $v) {
            $this->sexe = $v;
            $this->modifiedColumns[AssoIndividusArchiveTableMap::COL_SEXE] = true;
        }

        return $this;
    } // setSexe()

    /**
     * Set the value of [nom_famille] column.
     *
     * @param string $v new value
     * @return $this|\AssoIndividusArchive The current object (for fluent API support)
     */
    public function setNomFamille($v)
    {
        if ($v !== null) {
            $v = (string) $v;
        }

        if ($this->nom_famille !== $v) {
            $this->nom_famille = $v;
            $this->modifiedColumns[AssoIndividusArchiveTableMap::COL_NOM_FAMILLE] = true;
        }

        return $this;
    } // setNomFamille()

    /**
     * Set the value of [prenom] column.
     *
     * @param string $v new value
     * @return $this|\AssoIndividusArchive The current object (for fluent API support)
     */
    public function setPrenom($v)
    {
        if ($v !== null) {
            $v = (string) $v;
        }

        if ($this->prenom !== $v) {
            $this->prenom = $v;
            $this->modifiedColumns[AssoIndividusArchiveTableMap::COL_PRENOM] = true;
        }

        return $this;
    } // setPrenom()

    /**
     * Sets the value of [naissance] column to a normalized version of the date/time value specified.
     *
     * @param  mixed $v string, integer (timestamp), or \DateTimeInterface value.
     *               Empty strings are treated as NULL.
     * @return $this|\AssoIndividusArchive The current object (for fluent API support)
     */
    public function setNaissance($v)
    {
        $dt = PropelDateTime::newInstance($v, null, 'DateTime');
        if ($this->naissance !== null || $dt !== null) {
            if ($this->naissance === null || $dt === null || $dt->format("Y-m-d") !== $this->naissance->format("Y-m-d")) {
                $this->naissance = $dt === null ? null : clone $dt;
                $this->modifiedColumns[AssoIndividusArchiveTableMap::COL_NAISSANCE] = true;
            }
        } // if either are not null

        return $this;
    } // setNaissance()

    /**
     * Set the value of [adresse] column.
     *
     * @param string $v new value
     * @return $this|\AssoIndividusArchive The current object (for fluent API support)
     */
    public function setAdresse($v)
    {
        if ($v !== null) {
            $v = (string) $v;
        }

        if ($this->adresse !== $v) {
            $this->adresse = $v;
            $this->modifiedColumns[AssoIndividusArchiveTableMap::COL_ADRESSE] = true;
        }

        return $this;
    } // setAdresse()

    /**
     * Set the value of [codepostal] column.
     *
     * @param string $v new value
     * @return $this|\AssoIndividusArchive The current object (for fluent API support)
     */
    public function setCodepostal($v)
    {
        if ($v !== null) {
            $v = (string) $v;
        }

        if ($this->codepostal !== $v) {
            $this->codepostal = $v;
            $this->modifiedColumns[AssoIndividusArchiveTableMap::COL_CODEPOSTAL] = true;
        }

        return $this;
    } // setCodepostal()

    /**
     * Set the value of [ville] column.
     *
     * @param string $v new value
     * @return $this|\AssoIndividusArchive The current object (for fluent API support)
     */
    public function setVille($v)
    {
        if ($v !== null) {
            $v = (string) $v;
        }

        if ($this->ville !== $v) {
            $this->ville = $v;
            $this->modifiedColumns[AssoIndividusArchiveTableMap::COL_VILLE] = true;
        }

        return $this;
    } // setVille()

    /**
     * Set the value of [pays] column.
     *
     * @param string $v new value
     * @return $this|\AssoIndividusArchive The current object (for fluent API support)
     */
    public function setPays($v)
    {
        if ($v !== null) {
            $v = (string) $v;
        }

        if ($this->pays !== $v) {
            $this->pays = $v;
            $this->modifiedColumns[AssoIndividusArchiveTableMap::COL_PAYS] = true;
        }

        return $this;
    } // setPays()

    /**
     * Set the value of [telephone] column.
     *
     * @param string $v new value
     * @return $this|\AssoIndividusArchive The current object (for fluent API support)
     */
    public function setTelephone($v)
    {
        if ($v !== null) {
            $v = (string) $v;
        }

        if ($this->telephone !== $v) {
            $this->telephone = $v;
            $this->modifiedColumns[AssoIndividusArchiveTableMap::COL_TELEPHONE] = true;
        }

        return $this;
    } // setTelephone()

    /**
     * Set the value of [telephone_pro] column.
     *
     * @param string $v new value
     * @return $this|\AssoIndividusArchive The current object (for fluent API support)
     */
    public function setTelephonePro($v)
    {
        if ($v !== null) {
            $v = (string) $v;
        }

        if ($this->telephone_pro !== $v) {
            $this->telephone_pro = $v;
            $this->modifiedColumns[AssoIndividusArchiveTableMap::COL_TELEPHONE_PRO] = true;
        }

        return $this;
    } // setTelephonePro()

    /**
     * Set the value of [fax] column.
     *
     * @param string $v new value
     * @return $this|\AssoIndividusArchive The current object (for fluent API support)
     */
    public function setFax($v)
    {
        if ($v !== null) {
            $v = (string) $v;
        }

        if ($this->fax !== $v) {
            $this->fax = $v;
            $this->modifiedColumns[AssoIndividusArchiveTableMap::COL_FAX] = true;
        }

        return $this;
    } // setFax()

    /**
     * Set the value of [mobile] column.
     *
     * @param string $v new value
     * @return $this|\AssoIndividusArchive The current object (for fluent API support)
     */
    public function setMobile($v)
    {
        if ($v !== null) {
            $v = (string) $v;
        }

        if ($this->mobile !== $v) {
            $this->mobile = $v;
            $this->modifiedColumns[AssoIndividusArchiveTableMap::COL_MOBILE] = true;
        }

        return $this;
    } // setMobile()

    /**
     * Set the value of [url] column.
     *
     * @param string $v new value
     * @return $this|\AssoIndividusArchive The current object (for fluent API support)
     */
    public function setUrl($v)
    {
        if ($v !== null) {
            $v = (string) $v;
        }

        if ($this->url !== $v) {
            $this->url = $v;
            $this->modifiedColumns[AssoIndividusArchiveTableMap::COL_URL] = true;
        }

        return $this;
    } // setUrl()

    /**
     * Set the value of [profession] column.
     *
     * @param string $v new value
     * @return $this|\AssoIndividusArchive The current object (for fluent API support)
     */
    public function setProfession($v)
    {
        if ($v !== null) {
            $v = (string) $v;
        }

        if ($this->profession !== $v) {
            $this->profession = $v;
            $this->modifiedColumns[AssoIndividusArchiveTableMap::COL_PROFESSION] = true;
        }

        return $this;
    } // setProfession()

    /**
     * Sets the value of the [contact_souhait] column.
     * Non-boolean arguments are converted using the following rules:
     *   * 1, '1', 'true',  'on',  and 'yes' are converted to boolean true
     *   * 0, '0', 'false', 'off', and 'no'  are converted to boolean false
     * Check on string values is case insensitive (so 'FaLsE' is seen as 'false').
     *
     * @param  boolean|integer|string $v The new value
     * @return $this|\AssoIndividusArchive The current object (for fluent API support)
     */
    public function setContactSouhait($v)
    {
        if ($v !== null) {
            if (is_string($v)) {
                $v = in_array(strtolower($v), array('false', 'off', '-', 'no', 'n', '0', '')) ? false : true;
            } else {
                $v = (boolean) $v;
            }
        }

        if ($this->contact_souhait !== $v) {
            $this->contact_souhait = $v;
            $this->modifiedColumns[AssoIndividusArchiveTableMap::COL_CONTACT_SOUHAIT] = true;
        }

        return $this;
    } // setContactSouhait()

    /**
     * Set the value of [observation] column.
     *
     * @param string $v new value
     * @return $this|\AssoIndividusArchive The current object (for fluent API support)
     */
    public function setObservation($v)
    {
        if ($v !== null) {
            $v = (string) $v;
        }

        if ($this->observation !== $v) {
            $this->observation = $v;
            $this->modifiedColumns[AssoIndividusArchiveTableMap::COL_OBSERVATION] = true;
        }

        return $this;
    } // setObservation()

    /**
     * Sets the value of [created_at] column to a normalized version of the date/time value specified.
     *
     * @param  mixed $v string, integer (timestamp), or \DateTimeInterface value.
     *               Empty strings are treated as NULL.
     * @return $this|\AssoIndividusArchive The current object (for fluent API support)
     */
    public function setCreatedAt($v)
    {
        $dt = PropelDateTime::newInstance($v, null, 'DateTime');
        if ($this->created_at !== null || $dt !== null) {
            if ($this->created_at === null || $dt === null || $dt->format("Y-m-d H:i:s.u") !== $this->created_at->format("Y-m-d H:i:s.u")) {
                $this->created_at = $dt === null ? null : clone $dt;
                $this->modifiedColumns[AssoIndividusArchiveTableMap::COL_CREATED_AT] = true;
            }
        } // if either are not null

        return $this;
    } // setCreatedAt()

    /**
     * Sets the value of [updated_at] column to a normalized version of the date/time value specified.
     *
     * @param  mixed $v string, integer (timestamp), or \DateTimeInterface value.
     *               Empty strings are treated as NULL.
     * @return $this|\AssoIndividusArchive The current object (for fluent API support)
     */
    public function setUpdatedAt($v)
    {
        $dt = PropelDateTime::newInstance($v, null, 'DateTime');
        if ($this->updated_at !== null || $dt !== null) {
            if ($this->updated_at === null || $dt === null || $dt->format("Y-m-d H:i:s.u") !== $this->updated_at->format("Y-m-d H:i:s.u")) {
                $this->updated_at = $dt === null ? null : clone $dt;
                $this->modifiedColumns[AssoIndividusArchiveTableMap::COL_UPDATED_AT] = true;
            }
        } // if either are not null

        return $this;
    } // setUpdatedAt()

    /**
     * Sets the value of [archived_at] column to a normalized version of the date/time value specified.
     *
     * @param  mixed $v string, integer (timestamp), or \DateTimeInterface value.
     *               Empty strings are treated as NULL.
     * @return $this|\AssoIndividusArchive The current object (for fluent API support)
     */
    public function setArchivedAt($v)
    {
        $dt = PropelDateTime::newInstance($v, null, 'DateTime');
        if ($this->archived_at !== null || $dt !== null) {
            if ($this->archived_at === null || $dt === null || $dt->format("Y-m-d H:i:s.u") !== $this->archived_at->format("Y-m-d H:i:s.u")) {
                $this->archived_at = $dt === null ? null : clone $dt;
                $this->modifiedColumns[AssoIndividusArchiveTableMap::COL_ARCHIVED_AT] = true;
            }
        } // if either are not null

        return $this;
    } // setArchivedAt()

    /**
     * Indicates whether the columns in this object are only set to default values.
     *
     * This method can be used in conjunction with isModified() to indicate whether an object is both
     * modified _and_ has some values set which are non-default.
     *
     * @return boolean Whether the columns in this object are only been set with default values.
     */
    public function hasOnlyDefaultValues()
    {
            if ($this->contact_souhait !== true) {
                return false;
            }

        // otherwise, everything was equal, so return TRUE
        return true;
    } // hasOnlyDefaultValues()

    /**
     * Hydrates (populates) the object variables with values from the database resultset.
     *
     * An offset (0-based "start column") is specified so that objects can be hydrated
     * with a subset of the columns in the resultset rows.  This is needed, for example,
     * for results of JOIN queries where the resultset row includes columns from two or
     * more tables.
     *
     * @param array   $row       The row returned by DataFetcher->fetch().
     * @param int     $startcol  0-based offset column which indicates which restultset column to start with.
     * @param boolean $rehydrate Whether this object is being re-hydrated from the database.
     * @param string  $indexType The index type of $row. Mostly DataFetcher->getIndexType().
                                  One of the class type constants TableMap::TYPE_PHPNAME, TableMap::TYPE_CAMELNAME
     *                            TableMap::TYPE_COLNAME, TableMap::TYPE_FIELDNAME, TableMap::TYPE_NUM.
     *
     * @return int             next starting column
     * @throws PropelException - Any caught Exception will be rewrapped as a PropelException.
     */
    public function hydrate($row, $startcol = 0, $rehydrate = false, $indexType = TableMap::TYPE_NUM)
    {
        try {

            $col = $row[TableMap::TYPE_NUM == $indexType ? 0 + $startcol : AssoIndividusArchiveTableMap::translateFieldName('IdIndividu', TableMap::TYPE_PHPNAME, $indexType)];
            $this->id_individu = (null !== $col) ? (string) $col : null;

            $col = $row[TableMap::TYPE_NUM == $indexType ? 1 + $startcol : AssoIndividusArchiveTableMap::translateFieldName('Nom', TableMap::TYPE_PHPNAME, $indexType)];
            $this->nom = (null !== $col) ? (string) $col : null;

            $col = $row[TableMap::TYPE_NUM == $indexType ? 2 + $startcol : AssoIndividusArchiveTableMap::translateFieldName('Bio', TableMap::TYPE_PHPNAME, $indexType)];
            $this->bio = (null !== $col) ? (string) $col : null;

            $col = $row[TableMap::TYPE_NUM == $indexType ? 3 + $startcol : AssoIndividusArchiveTableMap::translateFieldName('Email', TableMap::TYPE_PHPNAME, $indexType)];
            $this->email = (null !== $col) ? (string) $col : null;

            $col = $row[TableMap::TYPE_NUM == $indexType ? 4 + $startcol : AssoIndividusArchiveTableMap::translateFieldName('Login', TableMap::TYPE_PHPNAME, $indexType)];
            $this->login = (null !== $col) ? (string) $col : null;

            $col = $row[TableMap::TYPE_NUM == $indexType ? 5 + $startcol : AssoIndividusArchiveTableMap::translateFieldName('Pass', TableMap::TYPE_PHPNAME, $indexType)];
            $this->pass = (null !== $col) ? (string) $col : null;

            $col = $row[TableMap::TYPE_NUM == $indexType ? 6 + $startcol : AssoIndividusArchiveTableMap::translateFieldName('AleaActuel', TableMap::TYPE_PHPNAME, $indexType)];
            $this->alea_actuel = (null !== $col) ? (string) $col : null;

            $col = $row[TableMap::TYPE_NUM == $indexType ? 7 + $startcol : AssoIndividusArchiveTableMap::translateFieldName('AleaFutur', TableMap::TYPE_PHPNAME, $indexType)];
            $this->alea_futur = (null !== $col) ? (string) $col : null;

            $col = $row[TableMap::TYPE_NUM == $indexType ? 8 + $startcol : AssoIndividusArchiveTableMap::translateFieldName('Token', TableMap::TYPE_PHPNAME, $indexType)];
            $this->token = (null !== $col) ? (string) $col : null;

            $col = $row[TableMap::TYPE_NUM == $indexType ? 9 + $startcol : AssoIndividusArchiveTableMap::translateFieldName('TokenTime', TableMap::TYPE_PHPNAME, $indexType)];
            $this->token_time = (null !== $col) ? (string) $col : null;

            $col = $row[TableMap::TYPE_NUM == $indexType ? 10 + $startcol : AssoIndividusArchiveTableMap::translateFieldName('Civilite', TableMap::TYPE_PHPNAME, $indexType)];
            $this->civilite = (null !== $col) ? (string) $col : null;

            $col = $row[TableMap::TYPE_NUM == $indexType ? 11 + $startcol : AssoIndividusArchiveTableMap::translateFieldName('Sexe', TableMap::TYPE_PHPNAME, $indexType)];
            $this->sexe = (null !== $col) ? (string) $col : null;

            $col = $row[TableMap::TYPE_NUM == $indexType ? 12 + $startcol : AssoIndividusArchiveTableMap::translateFieldName('NomFamille', TableMap::TYPE_PHPNAME, $indexType)];
            $this->nom_famille = (null !== $col) ? (string) $col : null;

            $col = $row[TableMap::TYPE_NUM == $indexType ? 13 + $startcol : AssoIndividusArchiveTableMap::translateFieldName('Prenom', TableMap::TYPE_PHPNAME, $indexType)];
            $this->prenom = (null !== $col) ? (string) $col : null;

            $col = $row[TableMap::TYPE_NUM == $indexType ? 14 + $startcol : AssoIndividusArchiveTableMap::translateFieldName('Naissance', TableMap::TYPE_PHPNAME, $indexType)];
            if ($col === '0000-00-00') {
                $col = null;
            }
            $this->naissance = (null !== $col) ? PropelDateTime::newInstance($col, null, 'DateTime') : null;

            $col = $row[TableMap::TYPE_NUM == $indexType ? 15 + $startcol : AssoIndividusArchiveTableMap::translateFieldName('Adresse', TableMap::TYPE_PHPNAME, $indexType)];
            $this->adresse = (null !== $col) ? (string) $col : null;

            $col = $row[TableMap::TYPE_NUM == $indexType ? 16 + $startcol : AssoIndividusArchiveTableMap::translateFieldName('Codepostal', TableMap::TYPE_PHPNAME, $indexType)];
            $this->codepostal = (null !== $col) ? (string) $col : null;

            $col = $row[TableMap::TYPE_NUM == $indexType ? 17 + $startcol : AssoIndividusArchiveTableMap::translateFieldName('Ville', TableMap::TYPE_PHPNAME, $indexType)];
            $this->ville = (null !== $col) ? (string) $col : null;

            $col = $row[TableMap::TYPE_NUM == $indexType ? 18 + $startcol : AssoIndividusArchiveTableMap::translateFieldName('Pays', TableMap::TYPE_PHPNAME, $indexType)];
            $this->pays = (null !== $col) ? (string) $col : null;

            $col = $row[TableMap::TYPE_NUM == $indexType ? 19 + $startcol : AssoIndividusArchiveTableMap::translateFieldName('Telephone', TableMap::TYPE_PHPNAME, $indexType)];
            $this->telephone = (null !== $col) ? (string) $col : null;

            $col = $row[TableMap::TYPE_NUM == $indexType ? 20 + $startcol : AssoIndividusArchiveTableMap::translateFieldName('TelephonePro', TableMap::TYPE_PHPNAME, $indexType)];
            $this->telephone_pro = (null !== $col) ? (string) $col : null;

            $col = $row[TableMap::TYPE_NUM == $indexType ? 21 + $startcol : AssoIndividusArchiveTableMap::translateFieldName('Fax', TableMap::TYPE_PHPNAME, $indexType)];
            $this->fax = (null !== $col) ? (string) $col : null;

            $col = $row[TableMap::TYPE_NUM == $indexType ? 22 + $startcol : AssoIndividusArchiveTableMap::translateFieldName('Mobile', TableMap::TYPE_PHPNAME, $indexType)];
            $this->mobile = (null !== $col) ? (string) $col : null;

            $col = $row[TableMap::TYPE_NUM == $indexType ? 23 + $startcol : AssoIndividusArchiveTableMap::translateFieldName('Url', TableMap::TYPE_PHPNAME, $indexType)];
            $this->url = (null !== $col) ? (string) $col : null;

            $col = $row[TableMap::TYPE_NUM == $indexType ? 24 + $startcol : AssoIndividusArchiveTableMap::translateFieldName('Profession', TableMap::TYPE_PHPNAME, $indexType)];
            $this->profession = (null !== $col) ? (string) $col : null;

            $col = $row[TableMap::TYPE_NUM == $indexType ? 25 + $startcol : AssoIndividusArchiveTableMap::translateFieldName('ContactSouhait', TableMap::TYPE_PHPNAME, $indexType)];
            $this->contact_souhait = (null !== $col) ? (boolean) $col : null;

            $col = $row[TableMap::TYPE_NUM == $indexType ? 26 + $startcol : AssoIndividusArchiveTableMap::translateFieldName('Observation', TableMap::TYPE_PHPNAME, $indexType)];
            $this->observation = (null !== $col) ? (string) $col : null;

            $col = $row[TableMap::TYPE_NUM == $indexType ? 27 + $startcol : AssoIndividusArchiveTableMap::translateFieldName('CreatedAt', TableMap::TYPE_PHPNAME, $indexType)];
            if ($col === '0000-00-00 00:00:00') {
                $col = null;
            }
            $this->created_at = (null !== $col) ? PropelDateTime::newInstance($col, null, 'DateTime') : null;

            $col = $row[TableMap::TYPE_NUM == $indexType ? 28 + $startcol : AssoIndividusArchiveTableMap::translateFieldName('UpdatedAt', TableMap::TYPE_PHPNAME, $indexType)];
            if ($col === '0000-00-00 00:00:00') {
                $col = null;
            }
            $this->updated_at = (null !== $col) ? PropelDateTime::newInstance($col, null, 'DateTime') : null;

            $col = $row[TableMap::TYPE_NUM == $indexType ? 29 + $startcol : AssoIndividusArchiveTableMap::translateFieldName('ArchivedAt', TableMap::TYPE_PHPNAME, $indexType)];
            if ($col === '0000-00-00 00:00:00') {
                $col = null;
            }
            $this->archived_at = (null !== $col) ? PropelDateTime::newInstance($col, null, 'DateTime') : null;
            $this->resetModified();

            $this->setNew(false);

            if ($rehydrate) {
                $this->ensureConsistency();
            }

            return $startcol + 30; // 30 = AssoIndividusArchiveTableMap::NUM_HYDRATE_COLUMNS.

        } catch (Exception $e) {
            throw new PropelException(sprintf('Error populating %s object', '\\AssoIndividusArchive'), 0, $e);
        }
    }

    /**
     * Checks and repairs the internal consistency of the object.
     *
     * This method is executed after an already-instantiated object is re-hydrated
     * from the database.  It exists to check any foreign keys to make sure that
     * the objects related to the current object are correct based on foreign key.
     *
     * You can override this method in the stub class, but you should always invoke
     * the base method from the overridden method (i.e. parent::ensureConsistency()),
     * in case your model changes.
     *
     * @throws PropelException
     */
    public function ensureConsistency()
    {
    } // ensureConsistency

    /**
     * Reloads this object from datastore based on primary key and (optionally) resets all associated objects.
     *
     * This will only work if the object has been saved and has a valid primary key set.
     *
     * @param      boolean $deep (optional) Whether to also de-associated any related objects.
     * @param      ConnectionInterface $con (optional) The ConnectionInterface connection to use.
     * @return void
     * @throws PropelException - if this object is deleted, unsaved or doesn't have pk match in db
     */
    public function reload($deep = false, ConnectionInterface $con = null)
    {
        if ($this->isDeleted()) {
            throw new PropelException("Cannot reload a deleted object.");
        }

        if ($this->isNew()) {
            throw new PropelException("Cannot reload an unsaved object.");
        }

        if ($con === null) {
            $con = Propel::getServiceContainer()->getReadConnection(AssoIndividusArchiveTableMap::DATABASE_NAME);
        }

        // We don't need to alter the object instance pool; we're just modifying this instance
        // already in the pool.

        $dataFetcher = ChildAssoIndividusArchiveQuery::create(null, $this->buildPkeyCriteria())->setFormatter(ModelCriteria::FORMAT_STATEMENT)->find($con);
        $row = $dataFetcher->fetch();
        $dataFetcher->close();
        if (!$row) {
            throw new PropelException('Cannot find matching row in the database to reload object values.');
        }
        $this->hydrate($row, 0, true, $dataFetcher->getIndexType()); // rehydrate

        if ($deep) {  // also de-associate any related objects?

        } // if (deep)
    }

    /**
     * Removes this object from datastore and sets delete attribute.
     *
     * @param      ConnectionInterface $con
     * @return void
     * @throws PropelException
     * @see AssoIndividusArchive::setDeleted()
     * @see AssoIndividusArchive::isDeleted()
     */
    public function delete(ConnectionInterface $con = null)
    {
        if ($this->isDeleted()) {
            throw new PropelException("This object has already been deleted.");
        }

        if ($con === null) {
            $con = Propel::getServiceContainer()->getWriteConnection(AssoIndividusArchiveTableMap::DATABASE_NAME);
        }

        $con->transaction(function () use ($con) {
            $deleteQuery = ChildAssoIndividusArchiveQuery::create()
                ->filterByPrimaryKey($this->getPrimaryKey());
            $ret = $this->preDelete($con);
            if ($ret) {
                $deleteQuery->delete($con);
                $this->postDelete($con);
                $this->setDeleted(true);
            }
        });
    }

    /**
     * Persists this object to the database.
     *
     * If the object is new, it inserts it; otherwise an update is performed.
     * All modified related objects will also be persisted in the doSave()
     * method.  This method wraps all precipitate database operations in a
     * single transaction.
     *
     * @param      ConnectionInterface $con
     * @return int             The number of rows affected by this insert/update and any referring fk objects' save() operations.
     * @throws PropelException
     * @see doSave()
     */
    public function save(ConnectionInterface $con = null)
    {
        if ($this->isDeleted()) {
            throw new PropelException("You cannot save an object that has been deleted.");
        }

        if ($this->alreadyInSave) {
            return 0;
        }

        if ($con === null) {
            $con = Propel::getServiceContainer()->getWriteConnection(AssoIndividusArchiveTableMap::DATABASE_NAME);
        }

        return $con->transaction(function () use ($con) {
            $ret = $this->preSave($con);
            $isInsert = $this->isNew();
            if ($isInsert) {
                $ret = $ret && $this->preInsert($con);
            } else {
                $ret = $ret && $this->preUpdate($con);
            }
            if ($ret) {
                $affectedRows = $this->doSave($con);
                if ($isInsert) {
                    $this->postInsert($con);
                } else {
                    $this->postUpdate($con);
                }
                $this->postSave($con);
                AssoIndividusArchiveTableMap::addInstanceToPool($this);
            } else {
                $affectedRows = 0;
            }

            return $affectedRows;
        });
    }

    /**
     * Performs the work of inserting or updating the row in the database.
     *
     * If the object is new, it inserts it; otherwise an update is performed.
     * All related objects are also updated in this method.
     *
     * @param      ConnectionInterface $con
     * @return int             The number of rows affected by this insert/update and any referring fk objects' save() operations.
     * @throws PropelException
     * @see save()
     */
    protected function doSave(ConnectionInterface $con)
    {
        $affectedRows = 0; // initialize var to track total num of affected rows
        if (!$this->alreadyInSave) {
            $this->alreadyInSave = true;

            if ($this->isNew() || $this->isModified()) {
                // persist changes
                if ($this->isNew()) {
                    $this->doInsert($con);
                    $affectedRows += 1;
                } else {
                    $affectedRows += $this->doUpdate($con);
                }
                $this->resetModified();
            }

            $this->alreadyInSave = false;

        }

        return $affectedRows;
    } // doSave()

    /**
     * Insert the row in the database.
     *
     * @param      ConnectionInterface $con
     *
     * @throws PropelException
     * @see doSave()
     */
    protected function doInsert(ConnectionInterface $con)
    {
        $modifiedColumns = array();
        $index = 0;


         // check the columns in natural order for more readable SQL queries
        if ($this->isColumnModified(AssoIndividusArchiveTableMap::COL_ID_INDIVIDU)) {
            $modifiedColumns[':p' . $index++]  = '`id_individu`';
        }
        if ($this->isColumnModified(AssoIndividusArchiveTableMap::COL_NOM)) {
            $modifiedColumns[':p' . $index++]  = '`nom`';
        }
        if ($this->isColumnModified(AssoIndividusArchiveTableMap::COL_BIO)) {
            $modifiedColumns[':p' . $index++]  = '`bio`';
        }
        if ($this->isColumnModified(AssoIndividusArchiveTableMap::COL_EMAIL)) {
            $modifiedColumns[':p' . $index++]  = '`email`';
        }
        if ($this->isColumnModified(AssoIndividusArchiveTableMap::COL_LOGIN)) {
            $modifiedColumns[':p' . $index++]  = '`login`';
        }
        if ($this->isColumnModified(AssoIndividusArchiveTableMap::COL_PASS)) {
            $modifiedColumns[':p' . $index++]  = '`pass`';
        }
        if ($this->isColumnModified(AssoIndividusArchiveTableMap::COL_ALEA_ACTUEL)) {
            $modifiedColumns[':p' . $index++]  = '`alea_actuel`';
        }
        if ($this->isColumnModified(AssoIndividusArchiveTableMap::COL_ALEA_FUTUR)) {
            $modifiedColumns[':p' . $index++]  = '`alea_futur`';
        }
        if ($this->isColumnModified(AssoIndividusArchiveTableMap::COL_TOKEN)) {
            $modifiedColumns[':p' . $index++]  = '`token`';
        }
        if ($this->isColumnModified(AssoIndividusArchiveTableMap::COL_TOKEN_TIME)) {
            $modifiedColumns[':p' . $index++]  = '`token_time`';
        }
        if ($this->isColumnModified(AssoIndividusArchiveTableMap::COL_CIVILITE)) {
            $modifiedColumns[':p' . $index++]  = '`civilite`';
        }
        if ($this->isColumnModified(AssoIndividusArchiveTableMap::COL_SEXE)) {
            $modifiedColumns[':p' . $index++]  = '`sexe`';
        }
        if ($this->isColumnModified(AssoIndividusArchiveTableMap::COL_NOM_FAMILLE)) {
            $modifiedColumns[':p' . $index++]  = '`nom_famille`';
        }
        if ($this->isColumnModified(AssoIndividusArchiveTableMap::COL_PRENOM)) {
            $modifiedColumns[':p' . $index++]  = '`prenom`';
        }
        if ($this->isColumnModified(AssoIndividusArchiveTableMap::COL_NAISSANCE)) {
            $modifiedColumns[':p' . $index++]  = '`naissance`';
        }
        if ($this->isColumnModified(AssoIndividusArchiveTableMap::COL_ADRESSE)) {
            $modifiedColumns[':p' . $index++]  = '`adresse`';
        }
        if ($this->isColumnModified(AssoIndividusArchiveTableMap::COL_CODEPOSTAL)) {
            $modifiedColumns[':p' . $index++]  = '`codepostal`';
        }
        if ($this->isColumnModified(AssoIndividusArchiveTableMap::COL_VILLE)) {
            $modifiedColumns[':p' . $index++]  = '`ville`';
        }
        if ($this->isColumnModified(AssoIndividusArchiveTableMap::COL_PAYS)) {
            $modifiedColumns[':p' . $index++]  = '`pays`';
        }
        if ($this->isColumnModified(AssoIndividusArchiveTableMap::COL_TELEPHONE)) {
            $modifiedColumns[':p' . $index++]  = '`telephone`';
        }
        if ($this->isColumnModified(AssoIndividusArchiveTableMap::COL_TELEPHONE_PRO)) {
            $modifiedColumns[':p' . $index++]  = '`telephone_pro`';
        }
        if ($this->isColumnModified(AssoIndividusArchiveTableMap::COL_FAX)) {
            $modifiedColumns[':p' . $index++]  = '`fax`';
        }
        if ($this->isColumnModified(AssoIndividusArchiveTableMap::COL_MOBILE)) {
            $modifiedColumns[':p' . $index++]  = '`mobile`';
        }
        if ($this->isColumnModified(AssoIndividusArchiveTableMap::COL_URL)) {
            $modifiedColumns[':p' . $index++]  = '`url`';
        }
        if ($this->isColumnModified(AssoIndividusArchiveTableMap::COL_PROFESSION)) {
            $modifiedColumns[':p' . $index++]  = '`profession`';
        }
        if ($this->isColumnModified(AssoIndividusArchiveTableMap::COL_CONTACT_SOUHAIT)) {
            $modifiedColumns[':p' . $index++]  = '`contact_souhait`';
        }
        if ($this->isColumnModified(AssoIndividusArchiveTableMap::COL_OBSERVATION)) {
            $modifiedColumns[':p' . $index++]  = '`observation`';
        }
        if ($this->isColumnModified(AssoIndividusArchiveTableMap::COL_CREATED_AT)) {
            $modifiedColumns[':p' . $index++]  = '`created_at`';
        }
        if ($this->isColumnModified(AssoIndividusArchiveTableMap::COL_UPDATED_AT)) {
            $modifiedColumns[':p' . $index++]  = '`updated_at`';
        }
        if ($this->isColumnModified(AssoIndividusArchiveTableMap::COL_ARCHIVED_AT)) {
            $modifiedColumns[':p' . $index++]  = '`archived_at`';
        }

        $sql = sprintf(
            'INSERT INTO `asso_individus_archive` (%s) VALUES (%s)',
            implode(', ', $modifiedColumns),
            implode(', ', array_keys($modifiedColumns))
        );

        try {
            $stmt = $con->prepare($sql);
            foreach ($modifiedColumns as $identifier => $columnName) {
                switch ($columnName) {
                    case '`id_individu`':
                        $stmt->bindValue($identifier, $this->id_individu, PDO::PARAM_INT);
                        break;
                    case '`nom`':
                        $stmt->bindValue($identifier, $this->nom, PDO::PARAM_STR);
                        break;
                    case '`bio`':
                        $stmt->bindValue($identifier, $this->bio, PDO::PARAM_STR);
                        break;
                    case '`email`':
                        $stmt->bindValue($identifier, $this->email, PDO::PARAM_STR);
                        break;
                    case '`login`':
                        $stmt->bindValue($identifier, $this->login, PDO::PARAM_STR);
                        break;
                    case '`pass`':
                        $stmt->bindValue($identifier, $this->pass, PDO::PARAM_STR);
                        break;
                    case '`alea_actuel`':
                        $stmt->bindValue($identifier, $this->alea_actuel, PDO::PARAM_STR);
                        break;
                    case '`alea_futur`':
                        $stmt->bindValue($identifier, $this->alea_futur, PDO::PARAM_STR);
                        break;
                    case '`token`':
                        $stmt->bindValue($identifier, $this->token, PDO::PARAM_STR);
                        break;
                    case '`token_time`':
                        $stmt->bindValue($identifier, $this->token_time, PDO::PARAM_INT);
                        break;
                    case '`civilite`':
                        $stmt->bindValue($identifier, $this->civilite, PDO::PARAM_STR);
                        break;
                    case '`sexe`':
                        $stmt->bindValue($identifier, $this->sexe, PDO::PARAM_STR);
                        break;
                    case '`nom_famille`':
                        $stmt->bindValue($identifier, $this->nom_famille, PDO::PARAM_STR);
                        break;
                    case '`prenom`':
                        $stmt->bindValue($identifier, $this->prenom, PDO::PARAM_STR);
                        break;
                    case '`naissance`':
                        $stmt->bindValue($identifier, $this->naissance ? $this->naissance->format("Y-m-d H:i:s.u") : null, PDO::PARAM_STR);
                        break;
                    case '`adresse`':
                        $stmt->bindValue($identifier, $this->adresse, PDO::PARAM_STR);
                        break;
                    case '`codepostal`':
                        $stmt->bindValue($identifier, $this->codepostal, PDO::PARAM_STR);
                        break;
                    case '`ville`':
                        $stmt->bindValue($identifier, $this->ville, PDO::PARAM_STR);
                        break;
                    case '`pays`':
                        $stmt->bindValue($identifier, $this->pays, PDO::PARAM_STR);
                        break;
                    case '`telephone`':
                        $stmt->bindValue($identifier, $this->telephone, PDO::PARAM_STR);
                        break;
                    case '`telephone_pro`':
                        $stmt->bindValue($identifier, $this->telephone_pro, PDO::PARAM_STR);
                        break;
                    case '`fax`':
                        $stmt->bindValue($identifier, $this->fax, PDO::PARAM_STR);
                        break;
                    case '`mobile`':
                        $stmt->bindValue($identifier, $this->mobile, PDO::PARAM_STR);
                        break;
                    case '`url`':
                        $stmt->bindValue($identifier, $this->url, PDO::PARAM_STR);
                        break;
                    case '`profession`':
                        $stmt->bindValue($identifier, $this->profession, PDO::PARAM_STR);
                        break;
                    case '`contact_souhait`':
                        $stmt->bindValue($identifier, (int) $this->contact_souhait, PDO::PARAM_INT);
                        break;
                    case '`observation`':
                        $stmt->bindValue($identifier, $this->observation, PDO::PARAM_STR);
                        break;
                    case '`created_at`':
                        $stmt->bindValue($identifier, $this->created_at ? $this->created_at->format("Y-m-d H:i:s.u") : null, PDO::PARAM_STR);
                        break;
                    case '`updated_at`':
                        $stmt->bindValue($identifier, $this->updated_at ? $this->updated_at->format("Y-m-d H:i:s.u") : null, PDO::PARAM_STR);
                        break;
                    case '`archived_at`':
                        $stmt->bindValue($identifier, $this->archived_at ? $this->archived_at->format("Y-m-d H:i:s.u") : null, PDO::PARAM_STR);
                        break;
                }
            }
            $stmt->execute();
        } catch (Exception $e) {
            Propel::log($e->getMessage(), Propel::LOG_ERR);
            throw new PropelException(sprintf('Unable to execute INSERT statement [%s]', $sql), 0, $e);
        }

        $this->setNew(false);
    }

    /**
     * Update the row in the database.
     *
     * @param      ConnectionInterface $con
     *
     * @return Integer Number of updated rows
     * @see doSave()
     */
    protected function doUpdate(ConnectionInterface $con)
    {
        $selectCriteria = $this->buildPkeyCriteria();
        $valuesCriteria = $this->buildCriteria();

        return $selectCriteria->doUpdate($valuesCriteria, $con);
    }

    /**
     * Retrieves a field from the object by name passed in as a string.
     *
     * @param      string $name name
     * @param      string $type The type of fieldname the $name is of:
     *                     one of the class type constants TableMap::TYPE_PHPNAME, TableMap::TYPE_CAMELNAME
     *                     TableMap::TYPE_COLNAME, TableMap::TYPE_FIELDNAME, TableMap::TYPE_NUM.
     *                     Defaults to TableMap::TYPE_FIELDNAME.
     * @return mixed Value of field.
     */
    public function getByName($name, $type = TableMap::TYPE_FIELDNAME)
    {
        $pos = AssoIndividusArchiveTableMap::translateFieldName($name, $type, TableMap::TYPE_NUM);
        $field = $this->getByPosition($pos);

        return $field;
    }

    /**
     * Retrieves a field from the object by Position as specified in the xml schema.
     * Zero-based.
     *
     * @param      int $pos position in xml schema
     * @return mixed Value of field at $pos
     */
    public function getByPosition($pos)
    {
        switch ($pos) {
            case 0:
                return $this->getIdIndividu();
                break;
            case 1:
                return $this->getNom();
                break;
            case 2:
                return $this->getBio();
                break;
            case 3:
                return $this->getEmail();
                break;
            case 4:
                return $this->getLogin();
                break;
            case 5:
                return $this->getPass();
                break;
            case 6:
                return $this->getAleaActuel();
                break;
            case 7:
                return $this->getAleaFutur();
                break;
            case 8:
                return $this->getToken();
                break;
            case 9:
                return $this->getTokenTime();
                break;
            case 10:
                return $this->getCivilite();
                break;
            case 11:
                return $this->getSexe();
                break;
            case 12:
                return $this->getNomFamille();
                break;
            case 13:
                return $this->getPrenom();
                break;
            case 14:
                return $this->getNaissance();
                break;
            case 15:
                return $this->getAdresse();
                break;
            case 16:
                return $this->getCodepostal();
                break;
            case 17:
                return $this->getVille();
                break;
            case 18:
                return $this->getPays();
                break;
            case 19:
                return $this->getTelephone();
                break;
            case 20:
                return $this->getTelephonePro();
                break;
            case 21:
                return $this->getFax();
                break;
            case 22:
                return $this->getMobile();
                break;
            case 23:
                return $this->getUrl();
                break;
            case 24:
                return $this->getProfession();
                break;
            case 25:
                return $this->getContactSouhait();
                break;
            case 26:
                return $this->getObservation();
                break;
            case 27:
                return $this->getCreatedAt();
                break;
            case 28:
                return $this->getUpdatedAt();
                break;
            case 29:
                return $this->getArchivedAt();
                break;
            default:
                return null;
                break;
        } // switch()
    }

    /**
     * Exports the object as an array.
     *
     * You can specify the key type of the array by passing one of the class
     * type constants.
     *
     * @param     string  $keyType (optional) One of the class type constants TableMap::TYPE_PHPNAME, TableMap::TYPE_CAMELNAME,
     *                    TableMap::TYPE_COLNAME, TableMap::TYPE_FIELDNAME, TableMap::TYPE_NUM.
     *                    Defaults to TableMap::TYPE_FIELDNAME.
     * @param     boolean $includeLazyLoadColumns (optional) Whether to include lazy loaded columns. Defaults to TRUE.
     * @param     array $alreadyDumpedObjects List of objects to skip to avoid recursion
     *
     * @return array an associative array containing the field names (as keys) and field values
     */
    public function toArray($keyType = TableMap::TYPE_FIELDNAME, $includeLazyLoadColumns = true, $alreadyDumpedObjects = array())
    {

        if (isset($alreadyDumpedObjects['AssoIndividusArchive'][$this->hashCode()])) {
            return '*RECURSION*';
        }
        $alreadyDumpedObjects['AssoIndividusArchive'][$this->hashCode()] = true;
        $keys = AssoIndividusArchiveTableMap::getFieldNames($keyType);
        $result = array(
            $keys[0] => $this->getIdIndividu(),
            $keys[1] => $this->getNom(),
            $keys[2] => $this->getBio(),
            $keys[3] => $this->getEmail(),
            $keys[4] => $this->getLogin(),
            $keys[5] => $this->getPass(),
            $keys[6] => $this->getAleaActuel(),
            $keys[7] => $this->getAleaFutur(),
            $keys[8] => $this->getToken(),
            $keys[9] => $this->getTokenTime(),
            $keys[10] => $this->getCivilite(),
            $keys[11] => $this->getSexe(),
            $keys[12] => $this->getNomFamille(),
            $keys[13] => $this->getPrenom(),
            $keys[14] => $this->getNaissance(),
            $keys[15] => $this->getAdresse(),
            $keys[16] => $this->getCodepostal(),
            $keys[17] => $this->getVille(),
            $keys[18] => $this->getPays(),
            $keys[19] => $this->getTelephone(),
            $keys[20] => $this->getTelephonePro(),
            $keys[21] => $this->getFax(),
            $keys[22] => $this->getMobile(),
            $keys[23] => $this->getUrl(),
            $keys[24] => $this->getProfession(),
            $keys[25] => $this->getContactSouhait(),
            $keys[26] => $this->getObservation(),
            $keys[27] => $this->getCreatedAt(),
            $keys[28] => $this->getUpdatedAt(),
            $keys[29] => $this->getArchivedAt(),
        );
        if ($result[$keys[14]] instanceof \DateTimeInterface) {
            $result[$keys[14]] = $result[$keys[14]]->format('c');
        }

        if ($result[$keys[27]] instanceof \DateTimeInterface) {
            $result[$keys[27]] = $result[$keys[27]]->format('c');
        }

        if ($result[$keys[28]] instanceof \DateTimeInterface) {
            $result[$keys[28]] = $result[$keys[28]]->format('c');
        }

        if ($result[$keys[29]] instanceof \DateTimeInterface) {
            $result[$keys[29]] = $result[$keys[29]]->format('c');
        }

        $virtualColumns = $this->virtualColumns;
        foreach ($virtualColumns as $key => $virtualColumn) {
            $result[$key] = $virtualColumn;
        }


        return $result;
    }

    /**
     * Sets a field from the object by name passed in as a string.
     *
     * @param  string $name
     * @param  mixed  $value field value
     * @param  string $type The type of fieldname the $name is of:
     *                one of the class type constants TableMap::TYPE_PHPNAME, TableMap::TYPE_CAMELNAME
     *                TableMap::TYPE_COLNAME, TableMap::TYPE_FIELDNAME, TableMap::TYPE_NUM.
     *                Defaults to TableMap::TYPE_FIELDNAME.
     * @return $this|\AssoIndividusArchive
     */
    public function setByName($name, $value, $type = TableMap::TYPE_FIELDNAME)
    {
        $pos = AssoIndividusArchiveTableMap::translateFieldName($name, $type, TableMap::TYPE_NUM);

        return $this->setByPosition($pos, $value);
    }

    /**
     * Sets a field from the object by Position as specified in the xml schema.
     * Zero-based.
     *
     * @param  int $pos position in xml schema
     * @param  mixed $value field value
     * @return $this|\AssoIndividusArchive
     */
    public function setByPosition($pos, $value)
    {
        switch ($pos) {
            case 0:
                $this->setIdIndividu($value);
                break;
            case 1:
                $this->setNom($value);
                break;
            case 2:
                $this->setBio($value);
                break;
            case 3:
                $this->setEmail($value);
                break;
            case 4:
                $this->setLogin($value);
                break;
            case 5:
                $this->setPass($value);
                break;
            case 6:
                $this->setAleaActuel($value);
                break;
            case 7:
                $this->setAleaFutur($value);
                break;
            case 8:
                $this->setToken($value);
                break;
            case 9:
                $this->setTokenTime($value);
                break;
            case 10:
                $this->setCivilite($value);
                break;
            case 11:
                $this->setSexe($value);
                break;
            case 12:
                $this->setNomFamille($value);
                break;
            case 13:
                $this->setPrenom($value);
                break;
            case 14:
                $this->setNaissance($value);
                break;
            case 15:
                $this->setAdresse($value);
                break;
            case 16:
                $this->setCodepostal($value);
                break;
            case 17:
                $this->setVille($value);
                break;
            case 18:
                $this->setPays($value);
                break;
            case 19:
                $this->setTelephone($value);
                break;
            case 20:
                $this->setTelephonePro($value);
                break;
            case 21:
                $this->setFax($value);
                break;
            case 22:
                $this->setMobile($value);
                break;
            case 23:
                $this->setUrl($value);
                break;
            case 24:
                $this->setProfession($value);
                break;
            case 25:
                $this->setContactSouhait($value);
                break;
            case 26:
                $this->setObservation($value);
                break;
            case 27:
                $this->setCreatedAt($value);
                break;
            case 28:
                $this->setUpdatedAt($value);
                break;
            case 29:
                $this->setArchivedAt($value);
                break;
        } // switch()

        return $this;
    }

    /**
     * Populates the object using an array.
     *
     * This is particularly useful when populating an object from one of the
     * request arrays (e.g. $_POST).  This method goes through the column
     * names, checking to see whether a matching key exists in populated
     * array. If so the setByName() method is called for that column.
     *
     * You can specify the key type of the array by additionally passing one
     * of the class type constants TableMap::TYPE_PHPNAME, TableMap::TYPE_CAMELNAME,
     * TableMap::TYPE_COLNAME, TableMap::TYPE_FIELDNAME, TableMap::TYPE_NUM.
     * The default key type is the column's TableMap::TYPE_FIELDNAME.
     *
     * @param      array  $arr     An array to populate the object from.
     * @param      string $keyType The type of keys the array uses.
     * @return void
     */
    public function fromArray($arr, $keyType = TableMap::TYPE_FIELDNAME)
    {
        $keys = AssoIndividusArchiveTableMap::getFieldNames($keyType);

        if (array_key_exists($keys[0], $arr)) {
            $this->setIdIndividu($arr[$keys[0]]);
        }
        if (array_key_exists($keys[1], $arr)) {
            $this->setNom($arr[$keys[1]]);
        }
        if (array_key_exists($keys[2], $arr)) {
            $this->setBio($arr[$keys[2]]);
        }
        if (array_key_exists($keys[3], $arr)) {
            $this->setEmail($arr[$keys[3]]);
        }
        if (array_key_exists($keys[4], $arr)) {
            $this->setLogin($arr[$keys[4]]);
        }
        if (array_key_exists($keys[5], $arr)) {
            $this->setPass($arr[$keys[5]]);
        }
        if (array_key_exists($keys[6], $arr)) {
            $this->setAleaActuel($arr[$keys[6]]);
        }
        if (array_key_exists($keys[7], $arr)) {
            $this->setAleaFutur($arr[$keys[7]]);
        }
        if (array_key_exists($keys[8], $arr)) {
            $this->setToken($arr[$keys[8]]);
        }
        if (array_key_exists($keys[9], $arr)) {
            $this->setTokenTime($arr[$keys[9]]);
        }
        if (array_key_exists($keys[10], $arr)) {
            $this->setCivilite($arr[$keys[10]]);
        }
        if (array_key_exists($keys[11], $arr)) {
            $this->setSexe($arr[$keys[11]]);
        }
        if (array_key_exists($keys[12], $arr)) {
            $this->setNomFamille($arr[$keys[12]]);
        }
        if (array_key_exists($keys[13], $arr)) {
            $this->setPrenom($arr[$keys[13]]);
        }
        if (array_key_exists($keys[14], $arr)) {
            $this->setNaissance($arr[$keys[14]]);
        }
        if (array_key_exists($keys[15], $arr)) {
            $this->setAdresse($arr[$keys[15]]);
        }
        if (array_key_exists($keys[16], $arr)) {
            $this->setCodepostal($arr[$keys[16]]);
        }
        if (array_key_exists($keys[17], $arr)) {
            $this->setVille($arr[$keys[17]]);
        }
        if (array_key_exists($keys[18], $arr)) {
            $this->setPays($arr[$keys[18]]);
        }
        if (array_key_exists($keys[19], $arr)) {
            $this->setTelephone($arr[$keys[19]]);
        }
        if (array_key_exists($keys[20], $arr)) {
            $this->setTelephonePro($arr[$keys[20]]);
        }
        if (array_key_exists($keys[21], $arr)) {
            $this->setFax($arr[$keys[21]]);
        }
        if (array_key_exists($keys[22], $arr)) {
            $this->setMobile($arr[$keys[22]]);
        }
        if (array_key_exists($keys[23], $arr)) {
            $this->setUrl($arr[$keys[23]]);
        }
        if (array_key_exists($keys[24], $arr)) {
            $this->setProfession($arr[$keys[24]]);
        }
        if (array_key_exists($keys[25], $arr)) {
            $this->setContactSouhait($arr[$keys[25]]);
        }
        if (array_key_exists($keys[26], $arr)) {
            $this->setObservation($arr[$keys[26]]);
        }
        if (array_key_exists($keys[27], $arr)) {
            $this->setCreatedAt($arr[$keys[27]]);
        }
        if (array_key_exists($keys[28], $arr)) {
            $this->setUpdatedAt($arr[$keys[28]]);
        }
        if (array_key_exists($keys[29], $arr)) {
            $this->setArchivedAt($arr[$keys[29]]);
        }
    }

     /**
     * Populate the current object from a string, using a given parser format
     * <code>
     * $book = new Book();
     * $book->importFrom('JSON', '{"Id":9012,"Title":"Don Juan","ISBN":"0140422161","Price":12.99,"PublisherId":1234,"AuthorId":5678}');
     * </code>
     *
     * You can specify the key type of the array by additionally passing one
     * of the class type constants TableMap::TYPE_PHPNAME, TableMap::TYPE_CAMELNAME,
     * TableMap::TYPE_COLNAME, TableMap::TYPE_FIELDNAME, TableMap::TYPE_NUM.
     * The default key type is the column's TableMap::TYPE_FIELDNAME.
     *
     * @param mixed $parser A AbstractParser instance,
     *                       or a format name ('XML', 'YAML', 'JSON', 'CSV')
     * @param string $data The source data to import from
     * @param string $keyType The type of keys the array uses.
     *
     * @return $this|\AssoIndividusArchive The current object, for fluid interface
     */
    public function importFrom($parser, $data, $keyType = TableMap::TYPE_FIELDNAME)
    {
        if (!$parser instanceof AbstractParser) {
            $parser = AbstractParser::getParser($parser);
        }

        $this->fromArray($parser->toArray($data), $keyType);

        return $this;
    }

    /**
     * Build a Criteria object containing the values of all modified columns in this object.
     *
     * @return Criteria The Criteria object containing all modified values.
     */
    public function buildCriteria()
    {
        $criteria = new Criteria(AssoIndividusArchiveTableMap::DATABASE_NAME);

        if ($this->isColumnModified(AssoIndividusArchiveTableMap::COL_ID_INDIVIDU)) {
            $criteria->add(AssoIndividusArchiveTableMap::COL_ID_INDIVIDU, $this->id_individu);
        }
        if ($this->isColumnModified(AssoIndividusArchiveTableMap::COL_NOM)) {
            $criteria->add(AssoIndividusArchiveTableMap::COL_NOM, $this->nom);
        }
        if ($this->isColumnModified(AssoIndividusArchiveTableMap::COL_BIO)) {
            $criteria->add(AssoIndividusArchiveTableMap::COL_BIO, $this->bio);
        }
        if ($this->isColumnModified(AssoIndividusArchiveTableMap::COL_EMAIL)) {
            $criteria->add(AssoIndividusArchiveTableMap::COL_EMAIL, $this->email);
        }
        if ($this->isColumnModified(AssoIndividusArchiveTableMap::COL_LOGIN)) {
            $criteria->add(AssoIndividusArchiveTableMap::COL_LOGIN, $this->login);
        }
        if ($this->isColumnModified(AssoIndividusArchiveTableMap::COL_PASS)) {
            $criteria->add(AssoIndividusArchiveTableMap::COL_PASS, $this->pass);
        }
        if ($this->isColumnModified(AssoIndividusArchiveTableMap::COL_ALEA_ACTUEL)) {
            $criteria->add(AssoIndividusArchiveTableMap::COL_ALEA_ACTUEL, $this->alea_actuel);
        }
        if ($this->isColumnModified(AssoIndividusArchiveTableMap::COL_ALEA_FUTUR)) {
            $criteria->add(AssoIndividusArchiveTableMap::COL_ALEA_FUTUR, $this->alea_futur);
        }
        if ($this->isColumnModified(AssoIndividusArchiveTableMap::COL_TOKEN)) {
            $criteria->add(AssoIndividusArchiveTableMap::COL_TOKEN, $this->token);
        }
        if ($this->isColumnModified(AssoIndividusArchiveTableMap::COL_TOKEN_TIME)) {
            $criteria->add(AssoIndividusArchiveTableMap::COL_TOKEN_TIME, $this->token_time);
        }
        if ($this->isColumnModified(AssoIndividusArchiveTableMap::COL_CIVILITE)) {
            $criteria->add(AssoIndividusArchiveTableMap::COL_CIVILITE, $this->civilite);
        }
        if ($this->isColumnModified(AssoIndividusArchiveTableMap::COL_SEXE)) {
            $criteria->add(AssoIndividusArchiveTableMap::COL_SEXE, $this->sexe);
        }
        if ($this->isColumnModified(AssoIndividusArchiveTableMap::COL_NOM_FAMILLE)) {
            $criteria->add(AssoIndividusArchiveTableMap::COL_NOM_FAMILLE, $this->nom_famille);
        }
        if ($this->isColumnModified(AssoIndividusArchiveTableMap::COL_PRENOM)) {
            $criteria->add(AssoIndividusArchiveTableMap::COL_PRENOM, $this->prenom);
        }
        if ($this->isColumnModified(AssoIndividusArchiveTableMap::COL_NAISSANCE)) {
            $criteria->add(AssoIndividusArchiveTableMap::COL_NAISSANCE, $this->naissance);
        }
        if ($this->isColumnModified(AssoIndividusArchiveTableMap::COL_ADRESSE)) {
            $criteria->add(AssoIndividusArchiveTableMap::COL_ADRESSE, $this->adresse);
        }
        if ($this->isColumnModified(AssoIndividusArchiveTableMap::COL_CODEPOSTAL)) {
            $criteria->add(AssoIndividusArchiveTableMap::COL_CODEPOSTAL, $this->codepostal);
        }
        if ($this->isColumnModified(AssoIndividusArchiveTableMap::COL_VILLE)) {
            $criteria->add(AssoIndividusArchiveTableMap::COL_VILLE, $this->ville);
        }
        if ($this->isColumnModified(AssoIndividusArchiveTableMap::COL_PAYS)) {
            $criteria->add(AssoIndividusArchiveTableMap::COL_PAYS, $this->pays);
        }
        if ($this->isColumnModified(AssoIndividusArchiveTableMap::COL_TELEPHONE)) {
            $criteria->add(AssoIndividusArchiveTableMap::COL_TELEPHONE, $this->telephone);
        }
        if ($this->isColumnModified(AssoIndividusArchiveTableMap::COL_TELEPHONE_PRO)) {
            $criteria->add(AssoIndividusArchiveTableMap::COL_TELEPHONE_PRO, $this->telephone_pro);
        }
        if ($this->isColumnModified(AssoIndividusArchiveTableMap::COL_FAX)) {
            $criteria->add(AssoIndividusArchiveTableMap::COL_FAX, $this->fax);
        }
        if ($this->isColumnModified(AssoIndividusArchiveTableMap::COL_MOBILE)) {
            $criteria->add(AssoIndividusArchiveTableMap::COL_MOBILE, $this->mobile);
        }
        if ($this->isColumnModified(AssoIndividusArchiveTableMap::COL_URL)) {
            $criteria->add(AssoIndividusArchiveTableMap::COL_URL, $this->url);
        }
        if ($this->isColumnModified(AssoIndividusArchiveTableMap::COL_PROFESSION)) {
            $criteria->add(AssoIndividusArchiveTableMap::COL_PROFESSION, $this->profession);
        }
        if ($this->isColumnModified(AssoIndividusArchiveTableMap::COL_CONTACT_SOUHAIT)) {
            $criteria->add(AssoIndividusArchiveTableMap::COL_CONTACT_SOUHAIT, $this->contact_souhait);
        }
        if ($this->isColumnModified(AssoIndividusArchiveTableMap::COL_OBSERVATION)) {
            $criteria->add(AssoIndividusArchiveTableMap::COL_OBSERVATION, $this->observation);
        }
        if ($this->isColumnModified(AssoIndividusArchiveTableMap::COL_CREATED_AT)) {
            $criteria->add(AssoIndividusArchiveTableMap::COL_CREATED_AT, $this->created_at);
        }
        if ($this->isColumnModified(AssoIndividusArchiveTableMap::COL_UPDATED_AT)) {
            $criteria->add(AssoIndividusArchiveTableMap::COL_UPDATED_AT, $this->updated_at);
        }
        if ($this->isColumnModified(AssoIndividusArchiveTableMap::COL_ARCHIVED_AT)) {
            $criteria->add(AssoIndividusArchiveTableMap::COL_ARCHIVED_AT, $this->archived_at);
        }

        return $criteria;
    }

    /**
     * Builds a Criteria object containing the primary key for this object.
     *
     * Unlike buildCriteria() this method includes the primary key values regardless
     * of whether or not they have been modified.
     *
     * @throws LogicException if no primary key is defined
     *
     * @return Criteria The Criteria object containing value(s) for primary key(s).
     */
    public function buildPkeyCriteria()
    {
        $criteria = ChildAssoIndividusArchiveQuery::create();
        $criteria->add(AssoIndividusArchiveTableMap::COL_ID_INDIVIDU, $this->id_individu);

        return $criteria;
    }

    /**
     * If the primary key is not null, return the hashcode of the
     * primary key. Otherwise, return the hash code of the object.
     *
     * @return int Hashcode
     */
    public function hashCode()
    {
        $validPk = null !== $this->getIdIndividu();

        $validPrimaryKeyFKs = 0;
        $primaryKeyFKs = [];

        if ($validPk) {
            return crc32(json_encode($this->getPrimaryKey(), JSON_UNESCAPED_UNICODE));
        } elseif ($validPrimaryKeyFKs) {
            return crc32(json_encode($primaryKeyFKs, JSON_UNESCAPED_UNICODE));
        }

        return spl_object_hash($this);
    }

    /**
     * Returns the primary key for this object (row).
     * @return string
     */
    public function getPrimaryKey()
    {
        return $this->getIdIndividu();
    }

    /**
     * Generic method to set the primary key (id_individu column).
     *
     * @param       string $key Primary key.
     * @return void
     */
    public function setPrimaryKey($key)
    {
        $this->setIdIndividu($key);
    }

    /**
     * Returns true if the primary key for this object is null.
     * @return boolean
     */
    public function isPrimaryKeyNull()
    {
        return null === $this->getIdIndividu();
    }

    /**
     * Sets contents of passed object to values from current object.
     *
     * If desired, this method can also make copies of all associated (fkey referrers)
     * objects.
     *
     * @param      object $copyObj An object of \AssoIndividusArchive (or compatible) type.
     * @param      boolean $deepCopy Whether to also copy all rows that refer (by fkey) to the current row.
     * @param      boolean $makeNew Whether to reset autoincrement PKs and make the object new.
     * @throws PropelException
     */
    public function copyInto($copyObj, $deepCopy = false, $makeNew = true)
    {
        $copyObj->setIdIndividu($this->getIdIndividu());
        $copyObj->setNom($this->getNom());
        $copyObj->setBio($this->getBio());
        $copyObj->setEmail($this->getEmail());
        $copyObj->setLogin($this->getLogin());
        $copyObj->setPass($this->getPass());
        $copyObj->setAleaActuel($this->getAleaActuel());
        $copyObj->setAleaFutur($this->getAleaFutur());
        $copyObj->setToken($this->getToken());
        $copyObj->setTokenTime($this->getTokenTime());
        $copyObj->setCivilite($this->getCivilite());
        $copyObj->setSexe($this->getSexe());
        $copyObj->setNomFamille($this->getNomFamille());
        $copyObj->setPrenom($this->getPrenom());
        $copyObj->setNaissance($this->getNaissance());
        $copyObj->setAdresse($this->getAdresse());
        $copyObj->setCodepostal($this->getCodepostal());
        $copyObj->setVille($this->getVille());
        $copyObj->setPays($this->getPays());
        $copyObj->setTelephone($this->getTelephone());
        $copyObj->setTelephonePro($this->getTelephonePro());
        $copyObj->setFax($this->getFax());
        $copyObj->setMobile($this->getMobile());
        $copyObj->setUrl($this->getUrl());
        $copyObj->setProfession($this->getProfession());
        $copyObj->setContactSouhait($this->getContactSouhait());
        $copyObj->setObservation($this->getObservation());
        $copyObj->setCreatedAt($this->getCreatedAt());
        $copyObj->setUpdatedAt($this->getUpdatedAt());
        $copyObj->setArchivedAt($this->getArchivedAt());
        if ($makeNew) {
            $copyObj->setNew(true);
        }
    }

    /**
     * Makes a copy of this object that will be inserted as a new row in table when saved.
     * It creates a new object filling in the simple attributes, but skipping any primary
     * keys that are defined for the table.
     *
     * If desired, this method can also make copies of all associated (fkey referrers)
     * objects.
     *
     * @param  boolean $deepCopy Whether to also copy all rows that refer (by fkey) to the current row.
     * @return \AssoIndividusArchive Clone of current object.
     * @throws PropelException
     */
    public function copy($deepCopy = false)
    {
        // we use get_class(), because this might be a subclass
        $clazz = get_class($this);
        $copyObj = new $clazz();
        $this->copyInto($copyObj, $deepCopy);

        return $copyObj;
    }

    /**
     * Clears the current object, sets all attributes to their default values and removes
     * outgoing references as well as back-references (from other objects to this one. Results probably in a database
     * change of those foreign objects when you call `save` there).
     */
    public function clear()
    {
        $this->id_individu = null;
        $this->nom = null;
        $this->bio = null;
        $this->email = null;
        $this->login = null;
        $this->pass = null;
        $this->alea_actuel = null;
        $this->alea_futur = null;
        $this->token = null;
        $this->token_time = null;
        $this->civilite = null;
        $this->sexe = null;
        $this->nom_famille = null;
        $this->prenom = null;
        $this->naissance = null;
        $this->adresse = null;
        $this->codepostal = null;
        $this->ville = null;
        $this->pays = null;
        $this->telephone = null;
        $this->telephone_pro = null;
        $this->fax = null;
        $this->mobile = null;
        $this->url = null;
        $this->profession = null;
        $this->contact_souhait = null;
        $this->observation = null;
        $this->created_at = null;
        $this->updated_at = null;
        $this->archived_at = null;
        $this->alreadyInSave = false;
        $this->clearAllReferences();
        $this->applyDefaultValues();
        $this->resetModified();
        $this->setNew(true);
        $this->setDeleted(false);
    }

    /**
     * Resets all references and back-references to other model objects or collections of model objects.
     *
     * This method is used to reset all php object references (not the actual reference in the database).
     * Necessary for object serialisation.
     *
     * @param      boolean $deep Whether to also clear the references on all referrer objects.
     */
    public function clearAllReferences($deep = false)
    {
        if ($deep) {
        } // if ($deep)

    }

    /**
     * Return the string representation of this object
     *
     * @return string
     */
    public function __toString()
    {
        return (string) $this->exportTo(AssoIndividusArchiveTableMap::DEFAULT_STRING_FORMAT);
    }

    /**
     * Code to be run before persisting the object
     * @param  ConnectionInterface $con
     * @return boolean
     */
    public function preSave(ConnectionInterface $con = null)
    {
        if (is_callable('parent::preSave')) {
            return parent::preSave($con);
        }
        return true;
    }

    /**
     * Code to be run after persisting the object
     * @param ConnectionInterface $con
     */
    public function postSave(ConnectionInterface $con = null)
    {
        if (is_callable('parent::postSave')) {
            parent::postSave($con);
        }
    }

    /**
     * Code to be run before inserting to database
     * @param  ConnectionInterface $con
     * @return boolean
     */
    public function preInsert(ConnectionInterface $con = null)
    {
        if (is_callable('parent::preInsert')) {
            return parent::preInsert($con);
        }
        return true;
    }

    /**
     * Code to be run after inserting to database
     * @param ConnectionInterface $con
     */
    public function postInsert(ConnectionInterface $con = null)
    {
        if (is_callable('parent::postInsert')) {
            parent::postInsert($con);
        }
    }

    /**
     * Code to be run before updating the object in database
     * @param  ConnectionInterface $con
     * @return boolean
     */
    public function preUpdate(ConnectionInterface $con = null)
    {
        if (is_callable('parent::preUpdate')) {
            return parent::preUpdate($con);
        }
        return true;
    }

    /**
     * Code to be run after updating the object in database
     * @param ConnectionInterface $con
     */
    public function postUpdate(ConnectionInterface $con = null)
    {
        if (is_callable('parent::postUpdate')) {
            parent::postUpdate($con);
        }
    }

    /**
     * Code to be run before deleting the object in database
     * @param  ConnectionInterface $con
     * @return boolean
     */
    public function preDelete(ConnectionInterface $con = null)
    {
        if (is_callable('parent::preDelete')) {
            return parent::preDelete($con);
        }
        return true;
    }

    /**
     * Code to be run after deleting the object in database
     * @param ConnectionInterface $con
     */
    public function postDelete(ConnectionInterface $con = null)
    {
        if (is_callable('parent::postDelete')) {
            parent::postDelete($con);
        }
    }


    /**
     * Derived method to catches calls to undefined methods.
     *
     * Provides magic import/export method support (fromXML()/toXML(), fromYAML()/toYAML(), etc.).
     * Allows to define default __call() behavior if you overwrite __call()
     *
     * @param string $name
     * @param mixed  $params
     *
     * @return array|string
     */
    public function __call($name, $params)
    {
        if (0 === strpos($name, 'get')) {
            $virtualColumn = substr($name, 3);
            if ($this->hasVirtualColumn($virtualColumn)) {
                return $this->getVirtualColumn($virtualColumn);
            }

            $virtualColumn = lcfirst($virtualColumn);
            if ($this->hasVirtualColumn($virtualColumn)) {
                return $this->getVirtualColumn($virtualColumn);
            }
        }

        if (0 === strpos($name, 'from')) {
            $format = substr($name, 4);

            return $this->importFrom($format, reset($params));
        }

        if (0 === strpos($name, 'to')) {
            $format = substr($name, 2);
            $includeLazyLoadColumns = isset($params[0]) ? $params[0] : true;

            return $this->exportTo($format, $includeLazyLoadColumns);
        }

        throw new BadMethodCallException(sprintf('Call to undefined method: %s.', $name));
    }

}
