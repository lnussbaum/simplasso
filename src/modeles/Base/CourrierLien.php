<?php

namespace Base;

use \ComCourriersLiensArchive as ChildComCourriersLiensArchive;
use \ComCourriersLiensArchiveQuery as ChildComCourriersLiensArchiveQuery;
use \Courrier as ChildCourrier;
use \CourrierLien as ChildCourrierLien;
use \CourrierLienQuery as ChildCourrierLienQuery;
use \CourrierQuery as ChildCourrierQuery;
use \Individu as ChildIndividu;
use \IndividuQuery as ChildIndividuQuery;
use \Membre as ChildMembre;
use \MembreQuery as ChildMembreQuery;
use \DateTime;
use \Exception;
use \PDO;
use Map\CourrierLienTableMap;
use Propel\Runtime\Propel;
use Propel\Runtime\ActiveQuery\Criteria;
use Propel\Runtime\ActiveQuery\ModelCriteria;
use Propel\Runtime\ActiveRecord\ActiveRecordInterface;
use Propel\Runtime\Collection\Collection;
use Propel\Runtime\Connection\ConnectionInterface;
use Propel\Runtime\Exception\BadMethodCallException;
use Propel\Runtime\Exception\LogicException;
use Propel\Runtime\Exception\PropelException;
use Propel\Runtime\Map\TableMap;
use Propel\Runtime\Parser\AbstractParser;
use Propel\Runtime\Util\PropelDateTime;

/**
 * Base class that represents a row from the 'com_courriers_liens' table.
 *
 *
 *
 * @package    propel.generator..Base
 */
abstract class CourrierLien implements ActiveRecordInterface
{
    /**
     * TableMap class name
     */
    const TABLE_MAP = '\\Map\\CourrierLienTableMap';


    /**
     * attribute to determine if this object has previously been saved.
     * @var boolean
     */
    protected $new = true;

    /**
     * attribute to determine whether this object has been deleted.
     * @var boolean
     */
    protected $deleted = false;

    /**
     * The columns that have been modified in current object.
     * Tracking modified columns allows us to only update modified columns.
     * @var array
     */
    protected $modifiedColumns = array();

    /**
     * The (virtual) columns that are added at runtime
     * The formatters can add supplementary columns based on a resultset
     * @var array
     */
    protected $virtualColumns = array();

    /**
     * The value for the id_courrier_lien field.
     *
     * @var        string
     */
    protected $id_courrier_lien;

    /**
     * The value for the id_membre field.
     *
     * @var        string
     */
    protected $id_membre;

    /**
     * The value for the id_individu field.
     *
     * @var        string
     */
    protected $id_individu;

    /**
     * The value for the id_courrier field.
     *
     * @var        string
     */
    protected $id_courrier;

    /**
     * The value for the canal field.
     *
     * Note: this column has a database default value of: 'L'
     * @var        string
     */
    protected $canal;

    /**
     * The value for the date_envoi field.
     *
     * @var        DateTime
     */
    protected $date_envoi;

    /**
     * The value for the message_particulier field.
     *
     * @var        string
     */
    protected $message_particulier;

    /**
     * The value for the statut field.
     *
     * Note: this column has a database default value of: 'ok'
     * @var        string
     */
    protected $statut;

    /**
     * The value for the variables field.
     *
     * @var        string
     */
    protected $variables;

    /**
     * The value for the created_at field.
     *
     * @var        DateTime
     */
    protected $created_at;

    /**
     * The value for the updated_at field.
     *
     * @var        DateTime
     */
    protected $updated_at;

    /**
     * @var        ChildIndividu
     */
    protected $aIndividu;

    /**
     * @var        ChildMembre
     */
    protected $aMembre;

    /**
     * @var        ChildCourrier
     */
    protected $aCourrier;

    /**
     * Flag to prevent endless save loop, if this object is referenced
     * by another object which falls in this transaction.
     *
     * @var boolean
     */
    protected $alreadyInSave = false;

    // archivable behavior
    protected $archiveOnDelete = true;

    /**
     * Applies default values to this object.
     * This method should be called from the object's constructor (or
     * equivalent initialization method).
     * @see __construct()
     */
    public function applyDefaultValues()
    {
        $this->canal = 'L';
        $this->statut = 'ok';
    }

    /**
     * Initializes internal state of Base\CourrierLien object.
     * @see applyDefaults()
     */
    public function __construct()
    {
        $this->applyDefaultValues();
    }

    /**
     * Returns whether the object has been modified.
     *
     * @return boolean True if the object has been modified.
     */
    public function isModified()
    {
        return !!$this->modifiedColumns;
    }

    /**
     * Has specified column been modified?
     *
     * @param  string  $col column fully qualified name (TableMap::TYPE_COLNAME), e.g. Book::AUTHOR_ID
     * @return boolean True if $col has been modified.
     */
    public function isColumnModified($col)
    {
        return $this->modifiedColumns && isset($this->modifiedColumns[$col]);
    }

    /**
     * Get the columns that have been modified in this object.
     * @return array A unique list of the modified column names for this object.
     */
    public function getModifiedColumns()
    {
        return $this->modifiedColumns ? array_keys($this->modifiedColumns) : [];
    }

    /**
     * Returns whether the object has ever been saved.  This will
     * be false, if the object was retrieved from storage or was created
     * and then saved.
     *
     * @return boolean true, if the object has never been persisted.
     */
    public function isNew()
    {
        return $this->new;
    }

    /**
     * Setter for the isNew attribute.  This method will be called
     * by Propel-generated children and objects.
     *
     * @param boolean $b the state of the object.
     */
    public function setNew($b)
    {
        $this->new = (boolean) $b;
    }

    /**
     * Whether this object has been deleted.
     * @return boolean The deleted state of this object.
     */
    public function isDeleted()
    {
        return $this->deleted;
    }

    /**
     * Specify whether this object has been deleted.
     * @param  boolean $b The deleted state of this object.
     * @return void
     */
    public function setDeleted($b)
    {
        $this->deleted = (boolean) $b;
    }

    /**
     * Sets the modified state for the object to be false.
     * @param  string $col If supplied, only the specified column is reset.
     * @return void
     */
    public function resetModified($col = null)
    {
        if (null !== $col) {
            if (isset($this->modifiedColumns[$col])) {
                unset($this->modifiedColumns[$col]);
            }
        } else {
            $this->modifiedColumns = array();
        }
    }

    /**
     * Compares this with another <code>CourrierLien</code> instance.  If
     * <code>obj</code> is an instance of <code>CourrierLien</code>, delegates to
     * <code>equals(CourrierLien)</code>.  Otherwise, returns <code>false</code>.
     *
     * @param  mixed   $obj The object to compare to.
     * @return boolean Whether equal to the object specified.
     */
    public function equals($obj)
    {
        if (!$obj instanceof static) {
            return false;
        }

        if ($this === $obj) {
            return true;
        }

        if (null === $this->getPrimaryKey() || null === $obj->getPrimaryKey()) {
            return false;
        }

        return $this->getPrimaryKey() === $obj->getPrimaryKey();
    }

    /**
     * Get the associative array of the virtual columns in this object
     *
     * @return array
     */
    public function getVirtualColumns()
    {
        return $this->virtualColumns;
    }

    /**
     * Checks the existence of a virtual column in this object
     *
     * @param  string  $name The virtual column name
     * @return boolean
     */
    public function hasVirtualColumn($name)
    {
        return array_key_exists($name, $this->virtualColumns);
    }

    /**
     * Get the value of a virtual column in this object
     *
     * @param  string $name The virtual column name
     * @return mixed
     *
     * @throws PropelException
     */
    public function getVirtualColumn($name)
    {
        if (!$this->hasVirtualColumn($name)) {
            throw new PropelException(sprintf('Cannot get value of inexistent virtual column %s.', $name));
        }

        return $this->virtualColumns[$name];
    }

    /**
     * Set the value of a virtual column in this object
     *
     * @param string $name  The virtual column name
     * @param mixed  $value The value to give to the virtual column
     *
     * @return $this|CourrierLien The current object, for fluid interface
     */
    public function setVirtualColumn($name, $value)
    {
        $this->virtualColumns[$name] = $value;

        return $this;
    }

    /**
     * Logs a message using Propel::log().
     *
     * @param  string  $msg
     * @param  int     $priority One of the Propel::LOG_* logging levels
     * @return boolean
     */
    protected function log($msg, $priority = Propel::LOG_INFO)
    {
        return Propel::log(get_class($this) . ': ' . $msg, $priority);
    }

    /**
     * Export the current object properties to a string, using a given parser format
     * <code>
     * $book = BookQuery::create()->findPk(9012);
     * echo $book->exportTo('JSON');
     *  => {"Id":9012,"Title":"Don Juan","ISBN":"0140422161","Price":12.99,"PublisherId":1234,"AuthorId":5678}');
     * </code>
     *
     * @param  mixed   $parser                 A AbstractParser instance, or a format name ('XML', 'YAML', 'JSON', 'CSV')
     * @param  boolean $includeLazyLoadColumns (optional) Whether to include lazy load(ed) columns. Defaults to TRUE.
     * @return string  The exported data
     */
    public function exportTo($parser, $includeLazyLoadColumns = true)
    {
        if (!$parser instanceof AbstractParser) {
            $parser = AbstractParser::getParser($parser);
        }

        return $parser->fromArray($this->toArray(TableMap::TYPE_PHPNAME, $includeLazyLoadColumns, array(), true));
    }

    /**
     * Clean up internal collections prior to serializing
     * Avoids recursive loops that turn into segmentation faults when serializing
     */
    public function __sleep()
    {
        $this->clearAllReferences();

        $cls = new \ReflectionClass($this);
        $propertyNames = [];
        $serializableProperties = array_diff($cls->getProperties(), $cls->getProperties(\ReflectionProperty::IS_STATIC));

        foreach($serializableProperties as $property) {
            $propertyNames[] = $property->getName();
        }

        return $propertyNames;
    }

    /**
     * Get the [id_courrier_lien] column value.
     *
     * @return string
     */
    public function getIdCourrierLien()
    {
        return $this->id_courrier_lien;
    }

    /**
     * Get the [id_membre] column value.
     *
     * @return string
     */
    public function getIdMembre()
    {
        return $this->id_membre;
    }

    /**
     * Get the [id_individu] column value.
     *
     * @return string
     */
    public function getIdIndividu()
    {
        return $this->id_individu;
    }

    /**
     * Get the [id_courrier] column value.
     *
     * @return string
     */
    public function getIdCourrier()
    {
        return $this->id_courrier;
    }

    /**
     * Get the [canal] column value.
     *
     * @return string
     */
    public function getCanal()
    {
        return $this->canal;
    }

    /**
     * Get the [optionally formatted] temporal [date_envoi] column value.
     *
     *
     * @param      string $format The date/time format string (either date()-style or strftime()-style).
     *                            If format is NULL, then the raw DateTime object will be returned.
     *
     * @return string|DateTime Formatted date/time value as string or DateTime object (if format is NULL), NULL if column is NULL, and 0 if column value is 0000-00-00 00:00:00
     *
     * @throws PropelException - if unable to parse/validate the date/time value.
     */
    public function getDateEnvoi($format = NULL)
    {
        if ($format === null) {
            return $this->date_envoi;
        } else {
            return $this->date_envoi instanceof \DateTimeInterface ? $this->date_envoi->format($format) : null;
        }
    }

    /**
     * Get the [message_particulier] column value.
     *
     * @return string
     */
    public function getMessageParticulier()
    {
        return $this->message_particulier;
    }

    /**
     * Get the [statut] column value.
     *
     * @return string
     */
    public function getStatut()
    {
        return $this->statut;
    }

    /**
     * Get the [variables] column value.
     *
     * @return string
     */
    public function getVariables()
    {
        return $this->variables;
    }

    /**
     * Get the [optionally formatted] temporal [created_at] column value.
     *
     *
     * @param      string $format The date/time format string (either date()-style or strftime()-style).
     *                            If format is NULL, then the raw DateTime object will be returned.
     *
     * @return string|DateTime Formatted date/time value as string or DateTime object (if format is NULL), NULL if column is NULL, and 0 if column value is 0000-00-00 00:00:00
     *
     * @throws PropelException - if unable to parse/validate the date/time value.
     */
    public function getCreatedAt($format = NULL)
    {
        if ($format === null) {
            return $this->created_at;
        } else {
            return $this->created_at instanceof \DateTimeInterface ? $this->created_at->format($format) : null;
        }
    }

    /**
     * Get the [optionally formatted] temporal [updated_at] column value.
     *
     *
     * @param      string $format The date/time format string (either date()-style or strftime()-style).
     *                            If format is NULL, then the raw DateTime object will be returned.
     *
     * @return string|DateTime Formatted date/time value as string or DateTime object (if format is NULL), NULL if column is NULL, and 0 if column value is 0000-00-00 00:00:00
     *
     * @throws PropelException - if unable to parse/validate the date/time value.
     */
    public function getUpdatedAt($format = NULL)
    {
        if ($format === null) {
            return $this->updated_at;
        } else {
            return $this->updated_at instanceof \DateTimeInterface ? $this->updated_at->format($format) : null;
        }
    }

    /**
     * Set the value of [id_courrier_lien] column.
     *
     * @param string $v new value
     * @return $this|\CourrierLien The current object (for fluent API support)
     */
    public function setIdCourrierLien($v)
    {
        if ($v !== null) {
            $v = (string) $v;
        }

        if ($this->id_courrier_lien !== $v) {
            $this->id_courrier_lien = $v;
            $this->modifiedColumns[CourrierLienTableMap::COL_ID_COURRIER_LIEN] = true;
        }

        return $this;
    } // setIdCourrierLien()

    /**
     * Set the value of [id_membre] column.
     *
     * @param string $v new value
     * @return $this|\CourrierLien The current object (for fluent API support)
     */
    public function setIdMembre($v)
    {
        if ($v !== null) {
            $v = (string) $v;
        }

        if ($this->id_membre !== $v) {
            $this->id_membre = $v;
            $this->modifiedColumns[CourrierLienTableMap::COL_ID_MEMBRE] = true;
        }

        if ($this->aMembre !== null && $this->aMembre->getIdMembre() !== $v) {
            $this->aMembre = null;
        }

        return $this;
    } // setIdMembre()

    /**
     * Set the value of [id_individu] column.
     *
     * @param string $v new value
     * @return $this|\CourrierLien The current object (for fluent API support)
     */
    public function setIdIndividu($v)
    {
        if ($v !== null) {
            $v = (string) $v;
        }

        if ($this->id_individu !== $v) {
            $this->id_individu = $v;
            $this->modifiedColumns[CourrierLienTableMap::COL_ID_INDIVIDU] = true;
        }

        if ($this->aIndividu !== null && $this->aIndividu->getIdIndividu() !== $v) {
            $this->aIndividu = null;
        }

        return $this;
    } // setIdIndividu()

    /**
     * Set the value of [id_courrier] column.
     *
     * @param string $v new value
     * @return $this|\CourrierLien The current object (for fluent API support)
     */
    public function setIdCourrier($v)
    {
        if ($v !== null) {
            $v = (string) $v;
        }

        if ($this->id_courrier !== $v) {
            $this->id_courrier = $v;
            $this->modifiedColumns[CourrierLienTableMap::COL_ID_COURRIER] = true;
        }

        if ($this->aCourrier !== null && $this->aCourrier->getIdCourrier() !== $v) {
            $this->aCourrier = null;
        }

        return $this;
    } // setIdCourrier()

    /**
     * Set the value of [canal] column.
     *
     * @param string $v new value
     * @return $this|\CourrierLien The current object (for fluent API support)
     */
    public function setCanal($v)
    {
        if ($v !== null) {
            $v = (string) $v;
        }

        if ($this->canal !== $v) {
            $this->canal = $v;
            $this->modifiedColumns[CourrierLienTableMap::COL_CANAL] = true;
        }

        return $this;
    } // setCanal()

    /**
     * Sets the value of [date_envoi] column to a normalized version of the date/time value specified.
     *
     * @param  mixed $v string, integer (timestamp), or \DateTimeInterface value.
     *               Empty strings are treated as NULL.
     * @return $this|\CourrierLien The current object (for fluent API support)
     */
    public function setDateEnvoi($v)
    {
        $dt = PropelDateTime::newInstance($v, null, 'DateTime');
        if ($this->date_envoi !== null || $dt !== null) {
            if ($this->date_envoi === null || $dt === null || $dt->format("Y-m-d H:i:s.u") !== $this->date_envoi->format("Y-m-d H:i:s.u")) {
                $this->date_envoi = $dt === null ? null : clone $dt;
                $this->modifiedColumns[CourrierLienTableMap::COL_DATE_ENVOI] = true;
            }
        } // if either are not null

        return $this;
    } // setDateEnvoi()

    /**
     * Set the value of [message_particulier] column.
     *
     * @param string $v new value
     * @return $this|\CourrierLien The current object (for fluent API support)
     */
    public function setMessageParticulier($v)
    {
        if ($v !== null) {
            $v = (string) $v;
        }

        if ($this->message_particulier !== $v) {
            $this->message_particulier = $v;
            $this->modifiedColumns[CourrierLienTableMap::COL_MESSAGE_PARTICULIER] = true;
        }

        return $this;
    } // setMessageParticulier()

    /**
     * Set the value of [statut] column.
     *
     * @param string $v new value
     * @return $this|\CourrierLien The current object (for fluent API support)
     */
    public function setStatut($v)
    {
        if ($v !== null) {
            $v = (string) $v;
        }

        if ($this->statut !== $v) {
            $this->statut = $v;
            $this->modifiedColumns[CourrierLienTableMap::COL_STATUT] = true;
        }

        return $this;
    } // setStatut()

    /**
     * Set the value of [variables] column.
     *
     * @param string $v new value
     * @return $this|\CourrierLien The current object (for fluent API support)
     */
    public function setVariables($v)
    {
        if ($v !== null) {
            $v = (string) $v;
        }

        if ($this->variables !== $v) {
            $this->variables = $v;
            $this->modifiedColumns[CourrierLienTableMap::COL_VARIABLES] = true;
        }

        return $this;
    } // setVariables()

    /**
     * Sets the value of [created_at] column to a normalized version of the date/time value specified.
     *
     * @param  mixed $v string, integer (timestamp), or \DateTimeInterface value.
     *               Empty strings are treated as NULL.
     * @return $this|\CourrierLien The current object (for fluent API support)
     */
    public function setCreatedAt($v)
    {
        $dt = PropelDateTime::newInstance($v, null, 'DateTime');
        if ($this->created_at !== null || $dt !== null) {
            if ($this->created_at === null || $dt === null || $dt->format("Y-m-d H:i:s.u") !== $this->created_at->format("Y-m-d H:i:s.u")) {
                $this->created_at = $dt === null ? null : clone $dt;
                $this->modifiedColumns[CourrierLienTableMap::COL_CREATED_AT] = true;
            }
        } // if either are not null

        return $this;
    } // setCreatedAt()

    /**
     * Sets the value of [updated_at] column to a normalized version of the date/time value specified.
     *
     * @param  mixed $v string, integer (timestamp), or \DateTimeInterface value.
     *               Empty strings are treated as NULL.
     * @return $this|\CourrierLien The current object (for fluent API support)
     */
    public function setUpdatedAt($v)
    {
        $dt = PropelDateTime::newInstance($v, null, 'DateTime');
        if ($this->updated_at !== null || $dt !== null) {
            if ($this->updated_at === null || $dt === null || $dt->format("Y-m-d H:i:s.u") !== $this->updated_at->format("Y-m-d H:i:s.u")) {
                $this->updated_at = $dt === null ? null : clone $dt;
                $this->modifiedColumns[CourrierLienTableMap::COL_UPDATED_AT] = true;
            }
        } // if either are not null

        return $this;
    } // setUpdatedAt()

    /**
     * Indicates whether the columns in this object are only set to default values.
     *
     * This method can be used in conjunction with isModified() to indicate whether an object is both
     * modified _and_ has some values set which are non-default.
     *
     * @return boolean Whether the columns in this object are only been set with default values.
     */
    public function hasOnlyDefaultValues()
    {
            if ($this->canal !== 'L') {
                return false;
            }

            if ($this->statut !== 'ok') {
                return false;
            }

        // otherwise, everything was equal, so return TRUE
        return true;
    } // hasOnlyDefaultValues()

    /**
     * Hydrates (populates) the object variables with values from the database resultset.
     *
     * An offset (0-based "start column") is specified so that objects can be hydrated
     * with a subset of the columns in the resultset rows.  This is needed, for example,
     * for results of JOIN queries where the resultset row includes columns from two or
     * more tables.
     *
     * @param array   $row       The row returned by DataFetcher->fetch().
     * @param int     $startcol  0-based offset column which indicates which restultset column to start with.
     * @param boolean $rehydrate Whether this object is being re-hydrated from the database.
     * @param string  $indexType The index type of $row. Mostly DataFetcher->getIndexType().
                                  One of the class type constants TableMap::TYPE_PHPNAME, TableMap::TYPE_CAMELNAME
     *                            TableMap::TYPE_COLNAME, TableMap::TYPE_FIELDNAME, TableMap::TYPE_NUM.
     *
     * @return int             next starting column
     * @throws PropelException - Any caught Exception will be rewrapped as a PropelException.
     */
    public function hydrate($row, $startcol = 0, $rehydrate = false, $indexType = TableMap::TYPE_NUM)
    {
        try {

            $col = $row[TableMap::TYPE_NUM == $indexType ? 0 + $startcol : CourrierLienTableMap::translateFieldName('IdCourrierLien', TableMap::TYPE_PHPNAME, $indexType)];
            $this->id_courrier_lien = (null !== $col) ? (string) $col : null;

            $col = $row[TableMap::TYPE_NUM == $indexType ? 1 + $startcol : CourrierLienTableMap::translateFieldName('IdMembre', TableMap::TYPE_PHPNAME, $indexType)];
            $this->id_membre = (null !== $col) ? (string) $col : null;

            $col = $row[TableMap::TYPE_NUM == $indexType ? 2 + $startcol : CourrierLienTableMap::translateFieldName('IdIndividu', TableMap::TYPE_PHPNAME, $indexType)];
            $this->id_individu = (null !== $col) ? (string) $col : null;

            $col = $row[TableMap::TYPE_NUM == $indexType ? 3 + $startcol : CourrierLienTableMap::translateFieldName('IdCourrier', TableMap::TYPE_PHPNAME, $indexType)];
            $this->id_courrier = (null !== $col) ? (string) $col : null;

            $col = $row[TableMap::TYPE_NUM == $indexType ? 4 + $startcol : CourrierLienTableMap::translateFieldName('Canal', TableMap::TYPE_PHPNAME, $indexType)];
            $this->canal = (null !== $col) ? (string) $col : null;

            $col = $row[TableMap::TYPE_NUM == $indexType ? 5 + $startcol : CourrierLienTableMap::translateFieldName('DateEnvoi', TableMap::TYPE_PHPNAME, $indexType)];
            if ($col === '0000-00-00 00:00:00') {
                $col = null;
            }
            $this->date_envoi = (null !== $col) ? PropelDateTime::newInstance($col, null, 'DateTime') : null;

            $col = $row[TableMap::TYPE_NUM == $indexType ? 6 + $startcol : CourrierLienTableMap::translateFieldName('MessageParticulier', TableMap::TYPE_PHPNAME, $indexType)];
            $this->message_particulier = (null !== $col) ? (string) $col : null;

            $col = $row[TableMap::TYPE_NUM == $indexType ? 7 + $startcol : CourrierLienTableMap::translateFieldName('Statut', TableMap::TYPE_PHPNAME, $indexType)];
            $this->statut = (null !== $col) ? (string) $col : null;

            $col = $row[TableMap::TYPE_NUM == $indexType ? 8 + $startcol : CourrierLienTableMap::translateFieldName('Variables', TableMap::TYPE_PHPNAME, $indexType)];
            $this->variables = (null !== $col) ? (string) $col : null;

            $col = $row[TableMap::TYPE_NUM == $indexType ? 9 + $startcol : CourrierLienTableMap::translateFieldName('CreatedAt', TableMap::TYPE_PHPNAME, $indexType)];
            if ($col === '0000-00-00 00:00:00') {
                $col = null;
            }
            $this->created_at = (null !== $col) ? PropelDateTime::newInstance($col, null, 'DateTime') : null;

            $col = $row[TableMap::TYPE_NUM == $indexType ? 10 + $startcol : CourrierLienTableMap::translateFieldName('UpdatedAt', TableMap::TYPE_PHPNAME, $indexType)];
            if ($col === '0000-00-00 00:00:00') {
                $col = null;
            }
            $this->updated_at = (null !== $col) ? PropelDateTime::newInstance($col, null, 'DateTime') : null;
            $this->resetModified();

            $this->setNew(false);

            if ($rehydrate) {
                $this->ensureConsistency();
            }

            return $startcol + 11; // 11 = CourrierLienTableMap::NUM_HYDRATE_COLUMNS.

        } catch (Exception $e) {
            throw new PropelException(sprintf('Error populating %s object', '\\CourrierLien'), 0, $e);
        }
    }

    /**
     * Checks and repairs the internal consistency of the object.
     *
     * This method is executed after an already-instantiated object is re-hydrated
     * from the database.  It exists to check any foreign keys to make sure that
     * the objects related to the current object are correct based on foreign key.
     *
     * You can override this method in the stub class, but you should always invoke
     * the base method from the overridden method (i.e. parent::ensureConsistency()),
     * in case your model changes.
     *
     * @throws PropelException
     */
    public function ensureConsistency()
    {
        if ($this->aMembre !== null && $this->id_membre !== $this->aMembre->getIdMembre()) {
            $this->aMembre = null;
        }
        if ($this->aIndividu !== null && $this->id_individu !== $this->aIndividu->getIdIndividu()) {
            $this->aIndividu = null;
        }
        if ($this->aCourrier !== null && $this->id_courrier !== $this->aCourrier->getIdCourrier()) {
            $this->aCourrier = null;
        }
    } // ensureConsistency

    /**
     * Reloads this object from datastore based on primary key and (optionally) resets all associated objects.
     *
     * This will only work if the object has been saved and has a valid primary key set.
     *
     * @param      boolean $deep (optional) Whether to also de-associated any related objects.
     * @param      ConnectionInterface $con (optional) The ConnectionInterface connection to use.
     * @return void
     * @throws PropelException - if this object is deleted, unsaved or doesn't have pk match in db
     */
    public function reload($deep = false, ConnectionInterface $con = null)
    {
        if ($this->isDeleted()) {
            throw new PropelException("Cannot reload a deleted object.");
        }

        if ($this->isNew()) {
            throw new PropelException("Cannot reload an unsaved object.");
        }

        if ($con === null) {
            $con = Propel::getServiceContainer()->getReadConnection(CourrierLienTableMap::DATABASE_NAME);
        }

        // We don't need to alter the object instance pool; we're just modifying this instance
        // already in the pool.

        $dataFetcher = ChildCourrierLienQuery::create(null, $this->buildPkeyCriteria())->setFormatter(ModelCriteria::FORMAT_STATEMENT)->find($con);
        $row = $dataFetcher->fetch();
        $dataFetcher->close();
        if (!$row) {
            throw new PropelException('Cannot find matching row in the database to reload object values.');
        }
        $this->hydrate($row, 0, true, $dataFetcher->getIndexType()); // rehydrate

        if ($deep) {  // also de-associate any related objects?

            $this->aIndividu = null;
            $this->aMembre = null;
            $this->aCourrier = null;
        } // if (deep)
    }

    /**
     * Removes this object from datastore and sets delete attribute.
     *
     * @param      ConnectionInterface $con
     * @return void
     * @throws PropelException
     * @see CourrierLien::setDeleted()
     * @see CourrierLien::isDeleted()
     */
    public function delete(ConnectionInterface $con = null)
    {
        if ($this->isDeleted()) {
            throw new PropelException("This object has already been deleted.");
        }

        if ($con === null) {
            $con = Propel::getServiceContainer()->getWriteConnection(CourrierLienTableMap::DATABASE_NAME);
        }

        $con->transaction(function () use ($con) {
            $deleteQuery = ChildCourrierLienQuery::create()
                ->filterByPrimaryKey($this->getPrimaryKey());
            $ret = $this->preDelete($con);
            // archivable behavior
            if ($ret) {
                if ($this->archiveOnDelete) {
                    // do nothing yet. The object will be archived later when calling ChildCourrierLienQuery::delete().
                } else {
                    $deleteQuery->setArchiveOnDelete(false);
                    $this->archiveOnDelete = true;
                }
            }

            if ($ret) {
                $deleteQuery->delete($con);
                $this->postDelete($con);
                $this->setDeleted(true);
            }
        });
    }

    /**
     * Persists this object to the database.
     *
     * If the object is new, it inserts it; otherwise an update is performed.
     * All modified related objects will also be persisted in the doSave()
     * method.  This method wraps all precipitate database operations in a
     * single transaction.
     *
     * @param      ConnectionInterface $con
     * @return int             The number of rows affected by this insert/update and any referring fk objects' save() operations.
     * @throws PropelException
     * @see doSave()
     */
    public function save(ConnectionInterface $con = null)
    {
        if ($this->isDeleted()) {
            throw new PropelException("You cannot save an object that has been deleted.");
        }

        if ($this->alreadyInSave) {
            return 0;
        }

        if ($con === null) {
            $con = Propel::getServiceContainer()->getWriteConnection(CourrierLienTableMap::DATABASE_NAME);
        }

        return $con->transaction(function () use ($con) {
            $ret = $this->preSave($con);
            $isInsert = $this->isNew();
            if ($isInsert) {
                $ret = $ret && $this->preInsert($con);
                // timestampable behavior

                if (!$this->isColumnModified(CourrierLienTableMap::COL_CREATED_AT)) {
                    $this->setCreatedAt(\Propel\Runtime\Util\PropelDateTime::createHighPrecision());
                }
                if (!$this->isColumnModified(CourrierLienTableMap::COL_UPDATED_AT)) {
                    $this->setUpdatedAt(\Propel\Runtime\Util\PropelDateTime::createHighPrecision());
                }
            } else {
                $ret = $ret && $this->preUpdate($con);
                // timestampable behavior
                if ($this->isModified() && !$this->isColumnModified(CourrierLienTableMap::COL_UPDATED_AT)) {
                    $this->setUpdatedAt(\Propel\Runtime\Util\PropelDateTime::createHighPrecision());
                }
            }
            if ($ret) {
                $affectedRows = $this->doSave($con);
                if ($isInsert) {
                    $this->postInsert($con);
                } else {
                    $this->postUpdate($con);
                }
                $this->postSave($con);
                CourrierLienTableMap::addInstanceToPool($this);
            } else {
                $affectedRows = 0;
            }

            return $affectedRows;
        });
    }

    /**
     * Performs the work of inserting or updating the row in the database.
     *
     * If the object is new, it inserts it; otherwise an update is performed.
     * All related objects are also updated in this method.
     *
     * @param      ConnectionInterface $con
     * @return int             The number of rows affected by this insert/update and any referring fk objects' save() operations.
     * @throws PropelException
     * @see save()
     */
    protected function doSave(ConnectionInterface $con)
    {
        $affectedRows = 0; // initialize var to track total num of affected rows
        if (!$this->alreadyInSave) {
            $this->alreadyInSave = true;

            // We call the save method on the following object(s) if they
            // were passed to this object by their corresponding set
            // method.  This object relates to these object(s) by a
            // foreign key reference.

            if ($this->aIndividu !== null) {
                if ($this->aIndividu->isModified() || $this->aIndividu->isNew()) {
                    $affectedRows += $this->aIndividu->save($con);
                }
                $this->setIndividu($this->aIndividu);
            }

            if ($this->aMembre !== null) {
                if ($this->aMembre->isModified() || $this->aMembre->isNew()) {
                    $affectedRows += $this->aMembre->save($con);
                }
                $this->setMembre($this->aMembre);
            }

            if ($this->aCourrier !== null) {
                if ($this->aCourrier->isModified() || $this->aCourrier->isNew()) {
                    $affectedRows += $this->aCourrier->save($con);
                }
                $this->setCourrier($this->aCourrier);
            }

            if ($this->isNew() || $this->isModified()) {
                // persist changes
                if ($this->isNew()) {
                    $this->doInsert($con);
                    $affectedRows += 1;
                } else {
                    $affectedRows += $this->doUpdate($con);
                }
                $this->resetModified();
            }

            $this->alreadyInSave = false;

        }

        return $affectedRows;
    } // doSave()

    /**
     * Insert the row in the database.
     *
     * @param      ConnectionInterface $con
     *
     * @throws PropelException
     * @see doSave()
     */
    protected function doInsert(ConnectionInterface $con)
    {
        $modifiedColumns = array();
        $index = 0;

        $this->modifiedColumns[CourrierLienTableMap::COL_ID_COURRIER_LIEN] = true;
        if (null !== $this->id_courrier_lien) {
            throw new PropelException('Cannot insert a value for auto-increment primary key (' . CourrierLienTableMap::COL_ID_COURRIER_LIEN . ')');
        }

         // check the columns in natural order for more readable SQL queries
        if ($this->isColumnModified(CourrierLienTableMap::COL_ID_COURRIER_LIEN)) {
            $modifiedColumns[':p' . $index++]  = '`id_courrier_lien`';
        }
        if ($this->isColumnModified(CourrierLienTableMap::COL_ID_MEMBRE)) {
            $modifiedColumns[':p' . $index++]  = '`id_membre`';
        }
        if ($this->isColumnModified(CourrierLienTableMap::COL_ID_INDIVIDU)) {
            $modifiedColumns[':p' . $index++]  = '`id_individu`';
        }
        if ($this->isColumnModified(CourrierLienTableMap::COL_ID_COURRIER)) {
            $modifiedColumns[':p' . $index++]  = '`id_courrier`';
        }
        if ($this->isColumnModified(CourrierLienTableMap::COL_CANAL)) {
            $modifiedColumns[':p' . $index++]  = '`canal`';
        }
        if ($this->isColumnModified(CourrierLienTableMap::COL_DATE_ENVOI)) {
            $modifiedColumns[':p' . $index++]  = '`date_envoi`';
        }
        if ($this->isColumnModified(CourrierLienTableMap::COL_MESSAGE_PARTICULIER)) {
            $modifiedColumns[':p' . $index++]  = '`message_particulier`';
        }
        if ($this->isColumnModified(CourrierLienTableMap::COL_STATUT)) {
            $modifiedColumns[':p' . $index++]  = '`statut`';
        }
        if ($this->isColumnModified(CourrierLienTableMap::COL_VARIABLES)) {
            $modifiedColumns[':p' . $index++]  = '`variables`';
        }
        if ($this->isColumnModified(CourrierLienTableMap::COL_CREATED_AT)) {
            $modifiedColumns[':p' . $index++]  = '`created_at`';
        }
        if ($this->isColumnModified(CourrierLienTableMap::COL_UPDATED_AT)) {
            $modifiedColumns[':p' . $index++]  = '`updated_at`';
        }

        $sql = sprintf(
            'INSERT INTO `com_courriers_liens` (%s) VALUES (%s)',
            implode(', ', $modifiedColumns),
            implode(', ', array_keys($modifiedColumns))
        );

        try {
            $stmt = $con->prepare($sql);
            foreach ($modifiedColumns as $identifier => $columnName) {
                switch ($columnName) {
                    case '`id_courrier_lien`':
                        $stmt->bindValue($identifier, $this->id_courrier_lien, PDO::PARAM_INT);
                        break;
                    case '`id_membre`':
                        $stmt->bindValue($identifier, $this->id_membre, PDO::PARAM_INT);
                        break;
                    case '`id_individu`':
                        $stmt->bindValue($identifier, $this->id_individu, PDO::PARAM_INT);
                        break;
                    case '`id_courrier`':
                        $stmt->bindValue($identifier, $this->id_courrier, PDO::PARAM_INT);
                        break;
                    case '`canal`':
                        $stmt->bindValue($identifier, $this->canal, PDO::PARAM_STR);
                        break;
                    case '`date_envoi`':
                        $stmt->bindValue($identifier, $this->date_envoi ? $this->date_envoi->format("Y-m-d H:i:s.u") : null, PDO::PARAM_STR);
                        break;
                    case '`message_particulier`':
                        $stmt->bindValue($identifier, $this->message_particulier, PDO::PARAM_STR);
                        break;
                    case '`statut`':
                        $stmt->bindValue($identifier, $this->statut, PDO::PARAM_STR);
                        break;
                    case '`variables`':
                        $stmt->bindValue($identifier, $this->variables, PDO::PARAM_STR);
                        break;
                    case '`created_at`':
                        $stmt->bindValue($identifier, $this->created_at ? $this->created_at->format("Y-m-d H:i:s.u") : null, PDO::PARAM_STR);
                        break;
                    case '`updated_at`':
                        $stmt->bindValue($identifier, $this->updated_at ? $this->updated_at->format("Y-m-d H:i:s.u") : null, PDO::PARAM_STR);
                        break;
                }
            }
            $stmt->execute();
        } catch (Exception $e) {
            Propel::log($e->getMessage(), Propel::LOG_ERR);
            throw new PropelException(sprintf('Unable to execute INSERT statement [%s]', $sql), 0, $e);
        }

        try {
            $pk = $con->lastInsertId();
        } catch (Exception $e) {
            throw new PropelException('Unable to get autoincrement id.', 0, $e);
        }
        $this->setIdCourrierLien($pk);

        $this->setNew(false);
    }

    /**
     * Update the row in the database.
     *
     * @param      ConnectionInterface $con
     *
     * @return Integer Number of updated rows
     * @see doSave()
     */
    protected function doUpdate(ConnectionInterface $con)
    {
        $selectCriteria = $this->buildPkeyCriteria();
        $valuesCriteria = $this->buildCriteria();

        return $selectCriteria->doUpdate($valuesCriteria, $con);
    }

    /**
     * Retrieves a field from the object by name passed in as a string.
     *
     * @param      string $name name
     * @param      string $type The type of fieldname the $name is of:
     *                     one of the class type constants TableMap::TYPE_PHPNAME, TableMap::TYPE_CAMELNAME
     *                     TableMap::TYPE_COLNAME, TableMap::TYPE_FIELDNAME, TableMap::TYPE_NUM.
     *                     Defaults to TableMap::TYPE_FIELDNAME.
     * @return mixed Value of field.
     */
    public function getByName($name, $type = TableMap::TYPE_FIELDNAME)
    {
        $pos = CourrierLienTableMap::translateFieldName($name, $type, TableMap::TYPE_NUM);
        $field = $this->getByPosition($pos);

        return $field;
    }

    /**
     * Retrieves a field from the object by Position as specified in the xml schema.
     * Zero-based.
     *
     * @param      int $pos position in xml schema
     * @return mixed Value of field at $pos
     */
    public function getByPosition($pos)
    {
        switch ($pos) {
            case 0:
                return $this->getIdCourrierLien();
                break;
            case 1:
                return $this->getIdMembre();
                break;
            case 2:
                return $this->getIdIndividu();
                break;
            case 3:
                return $this->getIdCourrier();
                break;
            case 4:
                return $this->getCanal();
                break;
            case 5:
                return $this->getDateEnvoi();
                break;
            case 6:
                return $this->getMessageParticulier();
                break;
            case 7:
                return $this->getStatut();
                break;
            case 8:
                return $this->getVariables();
                break;
            case 9:
                return $this->getCreatedAt();
                break;
            case 10:
                return $this->getUpdatedAt();
                break;
            default:
                return null;
                break;
        } // switch()
    }

    /**
     * Exports the object as an array.
     *
     * You can specify the key type of the array by passing one of the class
     * type constants.
     *
     * @param     string  $keyType (optional) One of the class type constants TableMap::TYPE_PHPNAME, TableMap::TYPE_CAMELNAME,
     *                    TableMap::TYPE_COLNAME, TableMap::TYPE_FIELDNAME, TableMap::TYPE_NUM.
     *                    Defaults to TableMap::TYPE_FIELDNAME.
     * @param     boolean $includeLazyLoadColumns (optional) Whether to include lazy loaded columns. Defaults to TRUE.
     * @param     array $alreadyDumpedObjects List of objects to skip to avoid recursion
     * @param     boolean $includeForeignObjects (optional) Whether to include hydrated related objects. Default to FALSE.
     *
     * @return array an associative array containing the field names (as keys) and field values
     */
    public function toArray($keyType = TableMap::TYPE_FIELDNAME, $includeLazyLoadColumns = true, $alreadyDumpedObjects = array(), $includeForeignObjects = false)
    {

        if (isset($alreadyDumpedObjects['CourrierLien'][$this->hashCode()])) {
            return '*RECURSION*';
        }
        $alreadyDumpedObjects['CourrierLien'][$this->hashCode()] = true;
        $keys = CourrierLienTableMap::getFieldNames($keyType);
        $result = array(
            $keys[0] => $this->getIdCourrierLien(),
            $keys[1] => $this->getIdMembre(),
            $keys[2] => $this->getIdIndividu(),
            $keys[3] => $this->getIdCourrier(),
            $keys[4] => $this->getCanal(),
            $keys[5] => $this->getDateEnvoi(),
            $keys[6] => $this->getMessageParticulier(),
            $keys[7] => $this->getStatut(),
            $keys[8] => $this->getVariables(),
            $keys[9] => $this->getCreatedAt(),
            $keys[10] => $this->getUpdatedAt(),
        );
        if ($result[$keys[5]] instanceof \DateTimeInterface) {
            $result[$keys[5]] = $result[$keys[5]]->format('c');
        }

        if ($result[$keys[9]] instanceof \DateTimeInterface) {
            $result[$keys[9]] = $result[$keys[9]]->format('c');
        }

        if ($result[$keys[10]] instanceof \DateTimeInterface) {
            $result[$keys[10]] = $result[$keys[10]]->format('c');
        }

        $virtualColumns = $this->virtualColumns;
        foreach ($virtualColumns as $key => $virtualColumn) {
            $result[$key] = $virtualColumn;
        }

        if ($includeForeignObjects) {
            if (null !== $this->aIndividu) {

                switch ($keyType) {
                    case TableMap::TYPE_CAMELNAME:
                        $key = 'individu';
                        break;
                    case TableMap::TYPE_FIELDNAME:
                        $key = 'asso_individus';
                        break;
                    default:
                        $key = 'Individu';
                }

                $result[$key] = $this->aIndividu->toArray($keyType, $includeLazyLoadColumns,  $alreadyDumpedObjects, true);
            }
            if (null !== $this->aMembre) {

                switch ($keyType) {
                    case TableMap::TYPE_CAMELNAME:
                        $key = 'membre';
                        break;
                    case TableMap::TYPE_FIELDNAME:
                        $key = 'asso_membres';
                        break;
                    default:
                        $key = 'Membre';
                }

                $result[$key] = $this->aMembre->toArray($keyType, $includeLazyLoadColumns,  $alreadyDumpedObjects, true);
            }
            if (null !== $this->aCourrier) {

                switch ($keyType) {
                    case TableMap::TYPE_CAMELNAME:
                        $key = 'courrier';
                        break;
                    case TableMap::TYPE_FIELDNAME:
                        $key = 'com_courriers';
                        break;
                    default:
                        $key = 'Courrier';
                }

                $result[$key] = $this->aCourrier->toArray($keyType, $includeLazyLoadColumns,  $alreadyDumpedObjects, true);
            }
        }

        return $result;
    }

    /**
     * Sets a field from the object by name passed in as a string.
     *
     * @param  string $name
     * @param  mixed  $value field value
     * @param  string $type The type of fieldname the $name is of:
     *                one of the class type constants TableMap::TYPE_PHPNAME, TableMap::TYPE_CAMELNAME
     *                TableMap::TYPE_COLNAME, TableMap::TYPE_FIELDNAME, TableMap::TYPE_NUM.
     *                Defaults to TableMap::TYPE_FIELDNAME.
     * @return $this|\CourrierLien
     */
    public function setByName($name, $value, $type = TableMap::TYPE_FIELDNAME)
    {
        $pos = CourrierLienTableMap::translateFieldName($name, $type, TableMap::TYPE_NUM);

        return $this->setByPosition($pos, $value);
    }

    /**
     * Sets a field from the object by Position as specified in the xml schema.
     * Zero-based.
     *
     * @param  int $pos position in xml schema
     * @param  mixed $value field value
     * @return $this|\CourrierLien
     */
    public function setByPosition($pos, $value)
    {
        switch ($pos) {
            case 0:
                $this->setIdCourrierLien($value);
                break;
            case 1:
                $this->setIdMembre($value);
                break;
            case 2:
                $this->setIdIndividu($value);
                break;
            case 3:
                $this->setIdCourrier($value);
                break;
            case 4:
                $this->setCanal($value);
                break;
            case 5:
                $this->setDateEnvoi($value);
                break;
            case 6:
                $this->setMessageParticulier($value);
                break;
            case 7:
                $this->setStatut($value);
                break;
            case 8:
                $this->setVariables($value);
                break;
            case 9:
                $this->setCreatedAt($value);
                break;
            case 10:
                $this->setUpdatedAt($value);
                break;
        } // switch()

        return $this;
    }

    /**
     * Populates the object using an array.
     *
     * This is particularly useful when populating an object from one of the
     * request arrays (e.g. $_POST).  This method goes through the column
     * names, checking to see whether a matching key exists in populated
     * array. If so the setByName() method is called for that column.
     *
     * You can specify the key type of the array by additionally passing one
     * of the class type constants TableMap::TYPE_PHPNAME, TableMap::TYPE_CAMELNAME,
     * TableMap::TYPE_COLNAME, TableMap::TYPE_FIELDNAME, TableMap::TYPE_NUM.
     * The default key type is the column's TableMap::TYPE_FIELDNAME.
     *
     * @param      array  $arr     An array to populate the object from.
     * @param      string $keyType The type of keys the array uses.
     * @return void
     */
    public function fromArray($arr, $keyType = TableMap::TYPE_FIELDNAME)
    {
        $keys = CourrierLienTableMap::getFieldNames($keyType);

        if (array_key_exists($keys[0], $arr)) {
            $this->setIdCourrierLien($arr[$keys[0]]);
        }
        if (array_key_exists($keys[1], $arr)) {
            $this->setIdMembre($arr[$keys[1]]);
        }
        if (array_key_exists($keys[2], $arr)) {
            $this->setIdIndividu($arr[$keys[2]]);
        }
        if (array_key_exists($keys[3], $arr)) {
            $this->setIdCourrier($arr[$keys[3]]);
        }
        if (array_key_exists($keys[4], $arr)) {
            $this->setCanal($arr[$keys[4]]);
        }
        if (array_key_exists($keys[5], $arr)) {
            $this->setDateEnvoi($arr[$keys[5]]);
        }
        if (array_key_exists($keys[6], $arr)) {
            $this->setMessageParticulier($arr[$keys[6]]);
        }
        if (array_key_exists($keys[7], $arr)) {
            $this->setStatut($arr[$keys[7]]);
        }
        if (array_key_exists($keys[8], $arr)) {
            $this->setVariables($arr[$keys[8]]);
        }
        if (array_key_exists($keys[9], $arr)) {
            $this->setCreatedAt($arr[$keys[9]]);
        }
        if (array_key_exists($keys[10], $arr)) {
            $this->setUpdatedAt($arr[$keys[10]]);
        }
    }

     /**
     * Populate the current object from a string, using a given parser format
     * <code>
     * $book = new Book();
     * $book->importFrom('JSON', '{"Id":9012,"Title":"Don Juan","ISBN":"0140422161","Price":12.99,"PublisherId":1234,"AuthorId":5678}');
     * </code>
     *
     * You can specify the key type of the array by additionally passing one
     * of the class type constants TableMap::TYPE_PHPNAME, TableMap::TYPE_CAMELNAME,
     * TableMap::TYPE_COLNAME, TableMap::TYPE_FIELDNAME, TableMap::TYPE_NUM.
     * The default key type is the column's TableMap::TYPE_FIELDNAME.
     *
     * @param mixed $parser A AbstractParser instance,
     *                       or a format name ('XML', 'YAML', 'JSON', 'CSV')
     * @param string $data The source data to import from
     * @param string $keyType The type of keys the array uses.
     *
     * @return $this|\CourrierLien The current object, for fluid interface
     */
    public function importFrom($parser, $data, $keyType = TableMap::TYPE_FIELDNAME)
    {
        if (!$parser instanceof AbstractParser) {
            $parser = AbstractParser::getParser($parser);
        }

        $this->fromArray($parser->toArray($data), $keyType);

        return $this;
    }

    /**
     * Build a Criteria object containing the values of all modified columns in this object.
     *
     * @return Criteria The Criteria object containing all modified values.
     */
    public function buildCriteria()
    {
        $criteria = new Criteria(CourrierLienTableMap::DATABASE_NAME);

        if ($this->isColumnModified(CourrierLienTableMap::COL_ID_COURRIER_LIEN)) {
            $criteria->add(CourrierLienTableMap::COL_ID_COURRIER_LIEN, $this->id_courrier_lien);
        }
        if ($this->isColumnModified(CourrierLienTableMap::COL_ID_MEMBRE)) {
            $criteria->add(CourrierLienTableMap::COL_ID_MEMBRE, $this->id_membre);
        }
        if ($this->isColumnModified(CourrierLienTableMap::COL_ID_INDIVIDU)) {
            $criteria->add(CourrierLienTableMap::COL_ID_INDIVIDU, $this->id_individu);
        }
        if ($this->isColumnModified(CourrierLienTableMap::COL_ID_COURRIER)) {
            $criteria->add(CourrierLienTableMap::COL_ID_COURRIER, $this->id_courrier);
        }
        if ($this->isColumnModified(CourrierLienTableMap::COL_CANAL)) {
            $criteria->add(CourrierLienTableMap::COL_CANAL, $this->canal);
        }
        if ($this->isColumnModified(CourrierLienTableMap::COL_DATE_ENVOI)) {
            $criteria->add(CourrierLienTableMap::COL_DATE_ENVOI, $this->date_envoi);
        }
        if ($this->isColumnModified(CourrierLienTableMap::COL_MESSAGE_PARTICULIER)) {
            $criteria->add(CourrierLienTableMap::COL_MESSAGE_PARTICULIER, $this->message_particulier);
        }
        if ($this->isColumnModified(CourrierLienTableMap::COL_STATUT)) {
            $criteria->add(CourrierLienTableMap::COL_STATUT, $this->statut);
        }
        if ($this->isColumnModified(CourrierLienTableMap::COL_VARIABLES)) {
            $criteria->add(CourrierLienTableMap::COL_VARIABLES, $this->variables);
        }
        if ($this->isColumnModified(CourrierLienTableMap::COL_CREATED_AT)) {
            $criteria->add(CourrierLienTableMap::COL_CREATED_AT, $this->created_at);
        }
        if ($this->isColumnModified(CourrierLienTableMap::COL_UPDATED_AT)) {
            $criteria->add(CourrierLienTableMap::COL_UPDATED_AT, $this->updated_at);
        }

        return $criteria;
    }

    /**
     * Builds a Criteria object containing the primary key for this object.
     *
     * Unlike buildCriteria() this method includes the primary key values regardless
     * of whether or not they have been modified.
     *
     * @throws LogicException if no primary key is defined
     *
     * @return Criteria The Criteria object containing value(s) for primary key(s).
     */
    public function buildPkeyCriteria()
    {
        $criteria = ChildCourrierLienQuery::create();
        $criteria->add(CourrierLienTableMap::COL_ID_COURRIER_LIEN, $this->id_courrier_lien);

        return $criteria;
    }

    /**
     * If the primary key is not null, return the hashcode of the
     * primary key. Otherwise, return the hash code of the object.
     *
     * @return int Hashcode
     */
    public function hashCode()
    {
        $validPk = null !== $this->getIdCourrierLien();

        $validPrimaryKeyFKs = 0;
        $primaryKeyFKs = [];

        if ($validPk) {
            return crc32(json_encode($this->getPrimaryKey(), JSON_UNESCAPED_UNICODE));
        } elseif ($validPrimaryKeyFKs) {
            return crc32(json_encode($primaryKeyFKs, JSON_UNESCAPED_UNICODE));
        }

        return spl_object_hash($this);
    }

    /**
     * Returns the primary key for this object (row).
     * @return string
     */
    public function getPrimaryKey()
    {
        return $this->getIdCourrierLien();
    }

    /**
     * Generic method to set the primary key (id_courrier_lien column).
     *
     * @param       string $key Primary key.
     * @return void
     */
    public function setPrimaryKey($key)
    {
        $this->setIdCourrierLien($key);
    }

    /**
     * Returns true if the primary key for this object is null.
     * @return boolean
     */
    public function isPrimaryKeyNull()
    {
        return null === $this->getIdCourrierLien();
    }

    /**
     * Sets contents of passed object to values from current object.
     *
     * If desired, this method can also make copies of all associated (fkey referrers)
     * objects.
     *
     * @param      object $copyObj An object of \CourrierLien (or compatible) type.
     * @param      boolean $deepCopy Whether to also copy all rows that refer (by fkey) to the current row.
     * @param      boolean $makeNew Whether to reset autoincrement PKs and make the object new.
     * @throws PropelException
     */
    public function copyInto($copyObj, $deepCopy = false, $makeNew = true)
    {
        $copyObj->setIdMembre($this->getIdMembre());
        $copyObj->setIdIndividu($this->getIdIndividu());
        $copyObj->setIdCourrier($this->getIdCourrier());
        $copyObj->setCanal($this->getCanal());
        $copyObj->setDateEnvoi($this->getDateEnvoi());
        $copyObj->setMessageParticulier($this->getMessageParticulier());
        $copyObj->setStatut($this->getStatut());
        $copyObj->setVariables($this->getVariables());
        $copyObj->setCreatedAt($this->getCreatedAt());
        $copyObj->setUpdatedAt($this->getUpdatedAt());
        if ($makeNew) {
            $copyObj->setNew(true);
            $copyObj->setIdCourrierLien(NULL); // this is a auto-increment column, so set to default value
        }
    }

    /**
     * Makes a copy of this object that will be inserted as a new row in table when saved.
     * It creates a new object filling in the simple attributes, but skipping any primary
     * keys that are defined for the table.
     *
     * If desired, this method can also make copies of all associated (fkey referrers)
     * objects.
     *
     * @param  boolean $deepCopy Whether to also copy all rows that refer (by fkey) to the current row.
     * @return \CourrierLien Clone of current object.
     * @throws PropelException
     */
    public function copy($deepCopy = false)
    {
        // we use get_class(), because this might be a subclass
        $clazz = get_class($this);
        $copyObj = new $clazz();
        $this->copyInto($copyObj, $deepCopy);

        return $copyObj;
    }

    /**
     * Declares an association between this object and a ChildIndividu object.
     *
     * @param  ChildIndividu $v
     * @return $this|\CourrierLien The current object (for fluent API support)
     * @throws PropelException
     */
    public function setIndividu(ChildIndividu $v = null)
    {
        if ($v === null) {
            $this->setIdIndividu(NULL);
        } else {
            $this->setIdIndividu($v->getIdIndividu());
        }

        $this->aIndividu = $v;

        // Add binding for other direction of this n:n relationship.
        // If this object has already been added to the ChildIndividu object, it will not be re-added.
        if ($v !== null) {
            $v->addCourrierLien($this);
        }


        return $this;
    }


    /**
     * Get the associated ChildIndividu object
     *
     * @param  ConnectionInterface $con Optional Connection object.
     * @return ChildIndividu The associated ChildIndividu object.
     * @throws PropelException
     */
    public function getIndividu(ConnectionInterface $con = null)
    {
        if ($this->aIndividu === null && (($this->id_individu !== "" && $this->id_individu !== null))) {
            $this->aIndividu = ChildIndividuQuery::create()->findPk($this->id_individu, $con);
            /* The following can be used additionally to
                guarantee the related object contains a reference
                to this object.  This level of coupling may, however, be
                undesirable since it could result in an only partially populated collection
                in the referenced object.
                $this->aIndividu->addCourrierLiens($this);
             */
        }

        return $this->aIndividu;
    }

    /**
     * Declares an association between this object and a ChildMembre object.
     *
     * @param  ChildMembre $v
     * @return $this|\CourrierLien The current object (for fluent API support)
     * @throws PropelException
     */
    public function setMembre(ChildMembre $v = null)
    {
        if ($v === null) {
            $this->setIdMembre(NULL);
        } else {
            $this->setIdMembre($v->getIdMembre());
        }

        $this->aMembre = $v;

        // Add binding for other direction of this n:n relationship.
        // If this object has already been added to the ChildMembre object, it will not be re-added.
        if ($v !== null) {
            $v->addCourrierLien($this);
        }


        return $this;
    }


    /**
     * Get the associated ChildMembre object
     *
     * @param  ConnectionInterface $con Optional Connection object.
     * @return ChildMembre The associated ChildMembre object.
     * @throws PropelException
     */
    public function getMembre(ConnectionInterface $con = null)
    {
        if ($this->aMembre === null && (($this->id_membre !== "" && $this->id_membre !== null))) {
            $this->aMembre = ChildMembreQuery::create()->findPk($this->id_membre, $con);
            /* The following can be used additionally to
                guarantee the related object contains a reference
                to this object.  This level of coupling may, however, be
                undesirable since it could result in an only partially populated collection
                in the referenced object.
                $this->aMembre->addCourrierLiens($this);
             */
        }

        return $this->aMembre;
    }

    /**
     * Declares an association between this object and a ChildCourrier object.
     *
     * @param  ChildCourrier $v
     * @return $this|\CourrierLien The current object (for fluent API support)
     * @throws PropelException
     */
    public function setCourrier(ChildCourrier $v = null)
    {
        if ($v === null) {
            $this->setIdCourrier(NULL);
        } else {
            $this->setIdCourrier($v->getIdCourrier());
        }

        $this->aCourrier = $v;

        // Add binding for other direction of this n:n relationship.
        // If this object has already been added to the ChildCourrier object, it will not be re-added.
        if ($v !== null) {
            $v->addCourrierLien($this);
        }


        return $this;
    }


    /**
     * Get the associated ChildCourrier object
     *
     * @param  ConnectionInterface $con Optional Connection object.
     * @return ChildCourrier The associated ChildCourrier object.
     * @throws PropelException
     */
    public function getCourrier(ConnectionInterface $con = null)
    {
        if ($this->aCourrier === null && (($this->id_courrier !== "" && $this->id_courrier !== null))) {
            $this->aCourrier = ChildCourrierQuery::create()->findPk($this->id_courrier, $con);
            /* The following can be used additionally to
                guarantee the related object contains a reference
                to this object.  This level of coupling may, however, be
                undesirable since it could result in an only partially populated collection
                in the referenced object.
                $this->aCourrier->addCourrierLiens($this);
             */
        }

        return $this->aCourrier;
    }

    /**
     * Clears the current object, sets all attributes to their default values and removes
     * outgoing references as well as back-references (from other objects to this one. Results probably in a database
     * change of those foreign objects when you call `save` there).
     */
    public function clear()
    {
        if (null !== $this->aIndividu) {
            $this->aIndividu->removeCourrierLien($this);
        }
        if (null !== $this->aMembre) {
            $this->aMembre->removeCourrierLien($this);
        }
        if (null !== $this->aCourrier) {
            $this->aCourrier->removeCourrierLien($this);
        }
        $this->id_courrier_lien = null;
        $this->id_membre = null;
        $this->id_individu = null;
        $this->id_courrier = null;
        $this->canal = null;
        $this->date_envoi = null;
        $this->message_particulier = null;
        $this->statut = null;
        $this->variables = null;
        $this->created_at = null;
        $this->updated_at = null;
        $this->alreadyInSave = false;
        $this->clearAllReferences();
        $this->applyDefaultValues();
        $this->resetModified();
        $this->setNew(true);
        $this->setDeleted(false);
    }

    /**
     * Resets all references and back-references to other model objects or collections of model objects.
     *
     * This method is used to reset all php object references (not the actual reference in the database).
     * Necessary for object serialisation.
     *
     * @param      boolean $deep Whether to also clear the references on all referrer objects.
     */
    public function clearAllReferences($deep = false)
    {
        if ($deep) {
        } // if ($deep)

        $this->aIndividu = null;
        $this->aMembre = null;
        $this->aCourrier = null;
    }

    /**
     * Return the string representation of this object
     *
     * @return string
     */
    public function __toString()
    {
        return (string) $this->exportTo(CourrierLienTableMap::DEFAULT_STRING_FORMAT);
    }

    // timestampable behavior

    /**
     * Mark the current object so that the update date doesn't get updated during next save
     *
     * @return     $this|ChildCourrierLien The current object (for fluent API support)
     */
    public function keepUpdateDateUnchanged()
    {
        $this->modifiedColumns[CourrierLienTableMap::COL_UPDATED_AT] = true;

        return $this;
    }

    // archivable behavior

    /**
     * Get an archived version of the current object.
     *
     * @param ConnectionInterface $con Optional connection object
     *
     * @return     ChildComCourriersLiensArchive An archive object, or null if the current object was never archived
     */
    public function getArchive(ConnectionInterface $con = null)
    {
        if ($this->isNew()) {
            return null;
        }
        $archive = ChildComCourriersLiensArchiveQuery::create()
            ->filterByPrimaryKey($this->getPrimaryKey())
            ->findOne($con);

        return $archive;
    }
    /**
     * Copy the data of the current object into a $archiveTablePhpName archive object.
     * The archived object is then saved.
     * If the current object has already been archived, the archived object
     * is updated and not duplicated.
     *
     * @param ConnectionInterface $con Optional connection object
     *
     * @throws PropelException If the object is new
     *
     * @return     ChildComCourriersLiensArchive The archive object based on this object
     */
    public function archive(ConnectionInterface $con = null)
    {
        if ($this->isNew()) {
            throw new PropelException('New objects cannot be archived. You must save the current object before calling archive().');
        }
        $archive = $this->getArchive($con);
        if (!$archive) {
            $archive = new ChildComCourriersLiensArchive();
            $archive->setPrimaryKey($this->getPrimaryKey());
        }
        $this->copyInto($archive, $deepCopy = false, $makeNew = false);
        $archive->setArchivedAt(time());
        $archive->save($con);

        return $archive;
    }

    /**
     * Revert the the current object to the state it had when it was last archived.
     * The object must be saved afterwards if the changes must persist.
     *
     * @param ConnectionInterface $con Optional connection object
     *
     * @throws PropelException If the object has no corresponding archive.
     *
     * @return $this|ChildCourrierLien The current object (for fluent API support)
     */
    public function restoreFromArchive(ConnectionInterface $con = null)
    {
        $archive = $this->getArchive($con);
        if (!$archive) {
            throw new PropelException('The current object has never been archived and cannot be restored');
        }
        $this->populateFromArchive($archive);

        return $this;
    }

    /**
     * Populates the the current object based on a $archiveTablePhpName archive object.
     *
     * @param      ChildComCourriersLiensArchive $archive An archived object based on the same class
      * @param      Boolean $populateAutoIncrementPrimaryKeys
     *               If true, autoincrement columns are copied from the archive object.
     *               If false, autoincrement columns are left intact.
      *
     * @return     ChildCourrierLien The current object (for fluent API support)
     */
    public function populateFromArchive($archive, $populateAutoIncrementPrimaryKeys = false) {
        if ($populateAutoIncrementPrimaryKeys) {
            $this->setIdCourrierLien($archive->getIdCourrierLien());
        }
        $this->setIdMembre($archive->getIdMembre());
        $this->setIdIndividu($archive->getIdIndividu());
        $this->setIdCourrier($archive->getIdCourrier());
        $this->setCanal($archive->getCanal());
        $this->setDateEnvoi($archive->getDateEnvoi());
        $this->setMessageParticulier($archive->getMessageParticulier());
        $this->setStatut($archive->getStatut());
        $this->setVariables($archive->getVariables());
        $this->setCreatedAt($archive->getCreatedAt());
        $this->setUpdatedAt($archive->getUpdatedAt());

        return $this;
    }

    /**
     * Removes the object from the database without archiving it.
     *
     * @param ConnectionInterface $con Optional connection object
     *
     * @return $this|ChildCourrierLien The current object (for fluent API support)
     */
    public function deleteWithoutArchive(ConnectionInterface $con = null)
    {
        $this->archiveOnDelete = false;

        return $this->delete($con);
    }

    /**
     * Code to be run before persisting the object
     * @param  ConnectionInterface $con
     * @return boolean
     */
    public function preSave(ConnectionInterface $con = null)
    {
        if (is_callable('parent::preSave')) {
            return parent::preSave($con);
        }
        return true;
    }

    /**
     * Code to be run after persisting the object
     * @param ConnectionInterface $con
     */
    public function postSave(ConnectionInterface $con = null)
    {
        if (is_callable('parent::postSave')) {
            parent::postSave($con);
        }
    }

    /**
     * Code to be run before inserting to database
     * @param  ConnectionInterface $con
     * @return boolean
     */
    public function preInsert(ConnectionInterface $con = null)
    {
        if (is_callable('parent::preInsert')) {
            return parent::preInsert($con);
        }
        return true;
    }

    /**
     * Code to be run after inserting to database
     * @param ConnectionInterface $con
     */
    public function postInsert(ConnectionInterface $con = null)
    {
        if (is_callable('parent::postInsert')) {
            parent::postInsert($con);
        }
    }

    /**
     * Code to be run before updating the object in database
     * @param  ConnectionInterface $con
     * @return boolean
     */
    public function preUpdate(ConnectionInterface $con = null)
    {
        if (is_callable('parent::preUpdate')) {
            return parent::preUpdate($con);
        }
        return true;
    }

    /**
     * Code to be run after updating the object in database
     * @param ConnectionInterface $con
     */
    public function postUpdate(ConnectionInterface $con = null)
    {
        if (is_callable('parent::postUpdate')) {
            parent::postUpdate($con);
        }
    }

    /**
     * Code to be run before deleting the object in database
     * @param  ConnectionInterface $con
     * @return boolean
     */
    public function preDelete(ConnectionInterface $con = null)
    {
        if (is_callable('parent::preDelete')) {
            return parent::preDelete($con);
        }
        return true;
    }

    /**
     * Code to be run after deleting the object in database
     * @param ConnectionInterface $con
     */
    public function postDelete(ConnectionInterface $con = null)
    {
        if (is_callable('parent::postDelete')) {
            parent::postDelete($con);
        }
    }


    /**
     * Derived method to catches calls to undefined methods.
     *
     * Provides magic import/export method support (fromXML()/toXML(), fromYAML()/toYAML(), etc.).
     * Allows to define default __call() behavior if you overwrite __call()
     *
     * @param string $name
     * @param mixed  $params
     *
     * @return array|string
     */
    public function __call($name, $params)
    {
        if (0 === strpos($name, 'get')) {
            $virtualColumn = substr($name, 3);
            if ($this->hasVirtualColumn($virtualColumn)) {
                return $this->getVirtualColumn($virtualColumn);
            }

            $virtualColumn = lcfirst($virtualColumn);
            if ($this->hasVirtualColumn($virtualColumn)) {
                return $this->getVirtualColumn($virtualColumn);
            }
        }

        if (0 === strpos($name, 'from')) {
            $format = substr($name, 4);

            return $this->importFrom($format, reset($params));
        }

        if (0 === strpos($name, 'to')) {
            $format = substr($name, 2);
            $includeLazyLoadColumns = isset($params[0]) ? $params[0] : true;

            return $this->exportTo($format, $includeLazyLoadColumns);
        }

        throw new BadMethodCallException(sprintf('Call to undefined method: %s.', $name));
    }

}
