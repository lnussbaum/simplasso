<?php

namespace Base;

use \AsscJournalsArchive as ChildAsscJournalsArchive;
use \AsscJournalsArchiveQuery as ChildAsscJournalsArchiveQuery;
use \Exception;
use \PDO;
use Map\AsscJournalsArchiveTableMap;
use Propel\Runtime\Propel;
use Propel\Runtime\ActiveQuery\Criteria;
use Propel\Runtime\ActiveQuery\ModelCriteria;
use Propel\Runtime\Collection\ObjectCollection;
use Propel\Runtime\Connection\ConnectionInterface;
use Propel\Runtime\Exception\PropelException;

/**
 * Base class that represents a query for the 'assc_journals_archive' table.
 *
 *
 *
 * @method     ChildAsscJournalsArchiveQuery orderByIdJournal($order = Criteria::ASC) Order by the id_journal column
 * @method     ChildAsscJournalsArchiveQuery orderByNom($order = Criteria::ASC) Order by the nom column
 * @method     ChildAsscJournalsArchiveQuery orderByNomcourt($order = Criteria::ASC) Order by the nomcourt column
 * @method     ChildAsscJournalsArchiveQuery orderByIdCompte($order = Criteria::ASC) Order by the id_compte column
 * @method     ChildAsscJournalsArchiveQuery orderByActif($order = Criteria::ASC) Order by the actif column
 * @method     ChildAsscJournalsArchiveQuery orderByMouvement($order = Criteria::ASC) Order by the mouvement column
 * @method     ChildAsscJournalsArchiveQuery orderByIdEntite($order = Criteria::ASC) Order by the id_entite column
 * @method     ChildAsscJournalsArchiveQuery orderByObservation($order = Criteria::ASC) Order by the observation column
 * @method     ChildAsscJournalsArchiveQuery orderByCreatedAt($order = Criteria::ASC) Order by the created_at column
 * @method     ChildAsscJournalsArchiveQuery orderByUpdatedAt($order = Criteria::ASC) Order by the updated_at column
 * @method     ChildAsscJournalsArchiveQuery orderByArchivedAt($order = Criteria::ASC) Order by the archived_at column
 *
 * @method     ChildAsscJournalsArchiveQuery groupByIdJournal() Group by the id_journal column
 * @method     ChildAsscJournalsArchiveQuery groupByNom() Group by the nom column
 * @method     ChildAsscJournalsArchiveQuery groupByNomcourt() Group by the nomcourt column
 * @method     ChildAsscJournalsArchiveQuery groupByIdCompte() Group by the id_compte column
 * @method     ChildAsscJournalsArchiveQuery groupByActif() Group by the actif column
 * @method     ChildAsscJournalsArchiveQuery groupByMouvement() Group by the mouvement column
 * @method     ChildAsscJournalsArchiveQuery groupByIdEntite() Group by the id_entite column
 * @method     ChildAsscJournalsArchiveQuery groupByObservation() Group by the observation column
 * @method     ChildAsscJournalsArchiveQuery groupByCreatedAt() Group by the created_at column
 * @method     ChildAsscJournalsArchiveQuery groupByUpdatedAt() Group by the updated_at column
 * @method     ChildAsscJournalsArchiveQuery groupByArchivedAt() Group by the archived_at column
 *
 * @method     ChildAsscJournalsArchiveQuery leftJoin($relation) Adds a LEFT JOIN clause to the query
 * @method     ChildAsscJournalsArchiveQuery rightJoin($relation) Adds a RIGHT JOIN clause to the query
 * @method     ChildAsscJournalsArchiveQuery innerJoin($relation) Adds a INNER JOIN clause to the query
 *
 * @method     ChildAsscJournalsArchiveQuery leftJoinWith($relation) Adds a LEFT JOIN clause and with to the query
 * @method     ChildAsscJournalsArchiveQuery rightJoinWith($relation) Adds a RIGHT JOIN clause and with to the query
 * @method     ChildAsscJournalsArchiveQuery innerJoinWith($relation) Adds a INNER JOIN clause and with to the query
 *
 * @method     ChildAsscJournalsArchive findOne(ConnectionInterface $con = null) Return the first ChildAsscJournalsArchive matching the query
 * @method     ChildAsscJournalsArchive findOneOrCreate(ConnectionInterface $con = null) Return the first ChildAsscJournalsArchive matching the query, or a new ChildAsscJournalsArchive object populated from the query conditions when no match is found
 *
 * @method     ChildAsscJournalsArchive findOneByIdJournal(string $id_journal) Return the first ChildAsscJournalsArchive filtered by the id_journal column
 * @method     ChildAsscJournalsArchive findOneByNom(string $nom) Return the first ChildAsscJournalsArchive filtered by the nom column
 * @method     ChildAsscJournalsArchive findOneByNomcourt(string $nomcourt) Return the first ChildAsscJournalsArchive filtered by the nomcourt column
 * @method     ChildAsscJournalsArchive findOneByIdCompte(string $id_compte) Return the first ChildAsscJournalsArchive filtered by the id_compte column
 * @method     ChildAsscJournalsArchive findOneByActif(int $actif) Return the first ChildAsscJournalsArchive filtered by the actif column
 * @method     ChildAsscJournalsArchive findOneByMouvement(int $mouvement) Return the first ChildAsscJournalsArchive filtered by the mouvement column
 * @method     ChildAsscJournalsArchive findOneByIdEntite(string $id_entite) Return the first ChildAsscJournalsArchive filtered by the id_entite column
 * @method     ChildAsscJournalsArchive findOneByObservation(string $observation) Return the first ChildAsscJournalsArchive filtered by the observation column
 * @method     ChildAsscJournalsArchive findOneByCreatedAt(string $created_at) Return the first ChildAsscJournalsArchive filtered by the created_at column
 * @method     ChildAsscJournalsArchive findOneByUpdatedAt(string $updated_at) Return the first ChildAsscJournalsArchive filtered by the updated_at column
 * @method     ChildAsscJournalsArchive findOneByArchivedAt(string $archived_at) Return the first ChildAsscJournalsArchive filtered by the archived_at column *

 * @method     ChildAsscJournalsArchive requirePk($key, ConnectionInterface $con = null) Return the ChildAsscJournalsArchive by primary key and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildAsscJournalsArchive requireOne(ConnectionInterface $con = null) Return the first ChildAsscJournalsArchive matching the query and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 *
 * @method     ChildAsscJournalsArchive requireOneByIdJournal(string $id_journal) Return the first ChildAsscJournalsArchive filtered by the id_journal column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildAsscJournalsArchive requireOneByNom(string $nom) Return the first ChildAsscJournalsArchive filtered by the nom column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildAsscJournalsArchive requireOneByNomcourt(string $nomcourt) Return the first ChildAsscJournalsArchive filtered by the nomcourt column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildAsscJournalsArchive requireOneByIdCompte(string $id_compte) Return the first ChildAsscJournalsArchive filtered by the id_compte column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildAsscJournalsArchive requireOneByActif(int $actif) Return the first ChildAsscJournalsArchive filtered by the actif column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildAsscJournalsArchive requireOneByMouvement(int $mouvement) Return the first ChildAsscJournalsArchive filtered by the mouvement column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildAsscJournalsArchive requireOneByIdEntite(string $id_entite) Return the first ChildAsscJournalsArchive filtered by the id_entite column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildAsscJournalsArchive requireOneByObservation(string $observation) Return the first ChildAsscJournalsArchive filtered by the observation column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildAsscJournalsArchive requireOneByCreatedAt(string $created_at) Return the first ChildAsscJournalsArchive filtered by the created_at column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildAsscJournalsArchive requireOneByUpdatedAt(string $updated_at) Return the first ChildAsscJournalsArchive filtered by the updated_at column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildAsscJournalsArchive requireOneByArchivedAt(string $archived_at) Return the first ChildAsscJournalsArchive filtered by the archived_at column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 *
 * @method     ChildAsscJournalsArchive[]|ObjectCollection find(ConnectionInterface $con = null) Return ChildAsscJournalsArchive objects based on current ModelCriteria
 * @method     ChildAsscJournalsArchive[]|ObjectCollection findByIdJournal(string $id_journal) Return ChildAsscJournalsArchive objects filtered by the id_journal column
 * @method     ChildAsscJournalsArchive[]|ObjectCollection findByNom(string $nom) Return ChildAsscJournalsArchive objects filtered by the nom column
 * @method     ChildAsscJournalsArchive[]|ObjectCollection findByNomcourt(string $nomcourt) Return ChildAsscJournalsArchive objects filtered by the nomcourt column
 * @method     ChildAsscJournalsArchive[]|ObjectCollection findByIdCompte(string $id_compte) Return ChildAsscJournalsArchive objects filtered by the id_compte column
 * @method     ChildAsscJournalsArchive[]|ObjectCollection findByActif(int $actif) Return ChildAsscJournalsArchive objects filtered by the actif column
 * @method     ChildAsscJournalsArchive[]|ObjectCollection findByMouvement(int $mouvement) Return ChildAsscJournalsArchive objects filtered by the mouvement column
 * @method     ChildAsscJournalsArchive[]|ObjectCollection findByIdEntite(string $id_entite) Return ChildAsscJournalsArchive objects filtered by the id_entite column
 * @method     ChildAsscJournalsArchive[]|ObjectCollection findByObservation(string $observation) Return ChildAsscJournalsArchive objects filtered by the observation column
 * @method     ChildAsscJournalsArchive[]|ObjectCollection findByCreatedAt(string $created_at) Return ChildAsscJournalsArchive objects filtered by the created_at column
 * @method     ChildAsscJournalsArchive[]|ObjectCollection findByUpdatedAt(string $updated_at) Return ChildAsscJournalsArchive objects filtered by the updated_at column
 * @method     ChildAsscJournalsArchive[]|ObjectCollection findByArchivedAt(string $archived_at) Return ChildAsscJournalsArchive objects filtered by the archived_at column
 * @method     ChildAsscJournalsArchive[]|\Propel\Runtime\Util\PropelModelPager paginate($page = 1, $maxPerPage = 10, ConnectionInterface $con = null) Issue a SELECT query based on the current ModelCriteria and uses a page and a maximum number of results per page to compute an offset and a limit
 *
 */
abstract class AsscJournalsArchiveQuery extends ModelCriteria
{
    protected $entityNotFoundExceptionClass = '\\Propel\\Runtime\\Exception\\EntityNotFoundException';

    /**
     * Initializes internal state of \Base\AsscJournalsArchiveQuery object.
     *
     * @param     string $dbName The database name
     * @param     string $modelName The phpName of a model, e.g. 'Book'
     * @param     string $modelAlias The alias for the model in this query, e.g. 'b'
     */
    public function __construct($dbName = 'simplasso', $modelName = '\\AsscJournalsArchive', $modelAlias = null)
    {
        parent::__construct($dbName, $modelName, $modelAlias);
    }

    /**
     * Returns a new ChildAsscJournalsArchiveQuery object.
     *
     * @param     string $modelAlias The alias of a model in the query
     * @param     Criteria $criteria Optional Criteria to build the query from
     *
     * @return ChildAsscJournalsArchiveQuery
     */
    public static function create($modelAlias = null, Criteria $criteria = null)
    {
        if ($criteria instanceof ChildAsscJournalsArchiveQuery) {
            return $criteria;
        }
        $query = new ChildAsscJournalsArchiveQuery();
        if (null !== $modelAlias) {
            $query->setModelAlias($modelAlias);
        }
        if ($criteria instanceof Criteria) {
            $query->mergeWith($criteria);
        }

        return $query;
    }

    /**
     * Find object by primary key.
     * Propel uses the instance pool to skip the database if the object exists.
     * Go fast if the query is untouched.
     *
     * <code>
     * $obj  = $c->findPk(12, $con);
     * </code>
     *
     * @param mixed $key Primary key to use for the query
     * @param ConnectionInterface $con an optional connection object
     *
     * @return ChildAsscJournalsArchive|array|mixed the result, formatted by the current formatter
     */
    public function findPk($key, ConnectionInterface $con = null)
    {
        if ($key === null) {
            return null;
        }

        if ($con === null) {
            $con = Propel::getServiceContainer()->getReadConnection(AsscJournalsArchiveTableMap::DATABASE_NAME);
        }

        $this->basePreSelect($con);

        if (
            $this->formatter || $this->modelAlias || $this->with || $this->select
            || $this->selectColumns || $this->asColumns || $this->selectModifiers
            || $this->map || $this->having || $this->joins
        ) {
            return $this->findPkComplex($key, $con);
        }

        if ((null !== ($obj = AsscJournalsArchiveTableMap::getInstanceFromPool(null === $key || is_scalar($key) || is_callable([$key, '__toString']) ? (string) $key : $key)))) {
            // the object is already in the instance pool
            return $obj;
        }

        return $this->findPkSimple($key, $con);
    }

    /**
     * Find object by primary key using raw SQL to go fast.
     * Bypass doSelect() and the object formatter by using generated code.
     *
     * @param     mixed $key Primary key to use for the query
     * @param     ConnectionInterface $con A connection object
     *
     * @throws \Propel\Runtime\Exception\PropelException
     *
     * @return ChildAsscJournalsArchive A model object, or null if the key is not found
     */
    protected function findPkSimple($key, ConnectionInterface $con)
    {
        $sql = 'SELECT `id_journal`, `nom`, `nomcourt`, `id_compte`, `actif`, `mouvement`, `id_entite`, `observation`, `created_at`, `updated_at`, `archived_at` FROM `assc_journals_archive` WHERE `id_journal` = :p0';
        try {
            $stmt = $con->prepare($sql);
            $stmt->bindValue(':p0', $key, PDO::PARAM_INT);
            $stmt->execute();
        } catch (Exception $e) {
            Propel::log($e->getMessage(), Propel::LOG_ERR);
            throw new PropelException(sprintf('Unable to execute SELECT statement [%s]', $sql), 0, $e);
        }
        $obj = null;
        if ($row = $stmt->fetch(\PDO::FETCH_NUM)) {
            /** @var ChildAsscJournalsArchive $obj */
            $obj = new ChildAsscJournalsArchive();
            $obj->hydrate($row);
            AsscJournalsArchiveTableMap::addInstanceToPool($obj, null === $key || is_scalar($key) || is_callable([$key, '__toString']) ? (string) $key : $key);
        }
        $stmt->closeCursor();

        return $obj;
    }

    /**
     * Find object by primary key.
     *
     * @param     mixed $key Primary key to use for the query
     * @param     ConnectionInterface $con A connection object
     *
     * @return ChildAsscJournalsArchive|array|mixed the result, formatted by the current formatter
     */
    protected function findPkComplex($key, ConnectionInterface $con)
    {
        // As the query uses a PK condition, no limit(1) is necessary.
        $criteria = $this->isKeepQuery() ? clone $this : $this;
        $dataFetcher = $criteria
            ->filterByPrimaryKey($key)
            ->doSelect($con);

        return $criteria->getFormatter()->init($criteria)->formatOne($dataFetcher);
    }

    /**
     * Find objects by primary key
     * <code>
     * $objs = $c->findPks(array(12, 56, 832), $con);
     * </code>
     * @param     array $keys Primary keys to use for the query
     * @param     ConnectionInterface $con an optional connection object
     *
     * @return ObjectCollection|array|mixed the list of results, formatted by the current formatter
     */
    public function findPks($keys, ConnectionInterface $con = null)
    {
        if (null === $con) {
            $con = Propel::getServiceContainer()->getReadConnection($this->getDbName());
        }
        $this->basePreSelect($con);
        $criteria = $this->isKeepQuery() ? clone $this : $this;
        $dataFetcher = $criteria
            ->filterByPrimaryKeys($keys)
            ->doSelect($con);

        return $criteria->getFormatter()->init($criteria)->format($dataFetcher);
    }

    /**
     * Filter the query by primary key
     *
     * @param     mixed $key Primary key to use for the query
     *
     * @return $this|ChildAsscJournalsArchiveQuery The current query, for fluid interface
     */
    public function filterByPrimaryKey($key)
    {

        return $this->addUsingAlias(AsscJournalsArchiveTableMap::COL_ID_JOURNAL, $key, Criteria::EQUAL);
    }

    /**
     * Filter the query by a list of primary keys
     *
     * @param     array $keys The list of primary key to use for the query
     *
     * @return $this|ChildAsscJournalsArchiveQuery The current query, for fluid interface
     */
    public function filterByPrimaryKeys($keys)
    {

        return $this->addUsingAlias(AsscJournalsArchiveTableMap::COL_ID_JOURNAL, $keys, Criteria::IN);
    }

    /**
     * Filter the query on the id_journal column
     *
     * Example usage:
     * <code>
     * $query->filterByIdJournal(1234); // WHERE id_journal = 1234
     * $query->filterByIdJournal(array(12, 34)); // WHERE id_journal IN (12, 34)
     * $query->filterByIdJournal(array('min' => 12)); // WHERE id_journal > 12
     * </code>
     *
     * @param     mixed $idJournal The value to use as filter.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildAsscJournalsArchiveQuery The current query, for fluid interface
     */
    public function filterByIdJournal($idJournal = null, $comparison = null)
    {
        if (is_array($idJournal)) {
            $useMinMax = false;
            if (isset($idJournal['min'])) {
                $this->addUsingAlias(AsscJournalsArchiveTableMap::COL_ID_JOURNAL, $idJournal['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($idJournal['max'])) {
                $this->addUsingAlias(AsscJournalsArchiveTableMap::COL_ID_JOURNAL, $idJournal['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(AsscJournalsArchiveTableMap::COL_ID_JOURNAL, $idJournal, $comparison);
    }

    /**
     * Filter the query on the nom column
     *
     * Example usage:
     * <code>
     * $query->filterByNom('fooValue');   // WHERE nom = 'fooValue'
     * $query->filterByNom('%fooValue%', Criteria::LIKE); // WHERE nom LIKE '%fooValue%'
     * </code>
     *
     * @param     string $nom The value to use as filter.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildAsscJournalsArchiveQuery The current query, for fluid interface
     */
    public function filterByNom($nom = null, $comparison = null)
    {
        if (null === $comparison) {
            if (is_array($nom)) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(AsscJournalsArchiveTableMap::COL_NOM, $nom, $comparison);
    }

    /**
     * Filter the query on the nomcourt column
     *
     * Example usage:
     * <code>
     * $query->filterByNomcourt('fooValue');   // WHERE nomcourt = 'fooValue'
     * $query->filterByNomcourt('%fooValue%', Criteria::LIKE); // WHERE nomcourt LIKE '%fooValue%'
     * </code>
     *
     * @param     string $nomcourt The value to use as filter.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildAsscJournalsArchiveQuery The current query, for fluid interface
     */
    public function filterByNomcourt($nomcourt = null, $comparison = null)
    {
        if (null === $comparison) {
            if (is_array($nomcourt)) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(AsscJournalsArchiveTableMap::COL_NOMCOURT, $nomcourt, $comparison);
    }

    /**
     * Filter the query on the id_compte column
     *
     * Example usage:
     * <code>
     * $query->filterByIdCompte(1234); // WHERE id_compte = 1234
     * $query->filterByIdCompte(array(12, 34)); // WHERE id_compte IN (12, 34)
     * $query->filterByIdCompte(array('min' => 12)); // WHERE id_compte > 12
     * </code>
     *
     * @param     mixed $idCompte The value to use as filter.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildAsscJournalsArchiveQuery The current query, for fluid interface
     */
    public function filterByIdCompte($idCompte = null, $comparison = null)
    {
        if (is_array($idCompte)) {
            $useMinMax = false;
            if (isset($idCompte['min'])) {
                $this->addUsingAlias(AsscJournalsArchiveTableMap::COL_ID_COMPTE, $idCompte['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($idCompte['max'])) {
                $this->addUsingAlias(AsscJournalsArchiveTableMap::COL_ID_COMPTE, $idCompte['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(AsscJournalsArchiveTableMap::COL_ID_COMPTE, $idCompte, $comparison);
    }

    /**
     * Filter the query on the actif column
     *
     * Example usage:
     * <code>
     * $query->filterByActif(1234); // WHERE actif = 1234
     * $query->filterByActif(array(12, 34)); // WHERE actif IN (12, 34)
     * $query->filterByActif(array('min' => 12)); // WHERE actif > 12
     * </code>
     *
     * @param     mixed $actif The value to use as filter.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildAsscJournalsArchiveQuery The current query, for fluid interface
     */
    public function filterByActif($actif = null, $comparison = null)
    {
        if (is_array($actif)) {
            $useMinMax = false;
            if (isset($actif['min'])) {
                $this->addUsingAlias(AsscJournalsArchiveTableMap::COL_ACTIF, $actif['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($actif['max'])) {
                $this->addUsingAlias(AsscJournalsArchiveTableMap::COL_ACTIF, $actif['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(AsscJournalsArchiveTableMap::COL_ACTIF, $actif, $comparison);
    }

    /**
     * Filter the query on the mouvement column
     *
     * Example usage:
     * <code>
     * $query->filterByMouvement(1234); // WHERE mouvement = 1234
     * $query->filterByMouvement(array(12, 34)); // WHERE mouvement IN (12, 34)
     * $query->filterByMouvement(array('min' => 12)); // WHERE mouvement > 12
     * </code>
     *
     * @param     mixed $mouvement The value to use as filter.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildAsscJournalsArchiveQuery The current query, for fluid interface
     */
    public function filterByMouvement($mouvement = null, $comparison = null)
    {
        if (is_array($mouvement)) {
            $useMinMax = false;
            if (isset($mouvement['min'])) {
                $this->addUsingAlias(AsscJournalsArchiveTableMap::COL_MOUVEMENT, $mouvement['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($mouvement['max'])) {
                $this->addUsingAlias(AsscJournalsArchiveTableMap::COL_MOUVEMENT, $mouvement['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(AsscJournalsArchiveTableMap::COL_MOUVEMENT, $mouvement, $comparison);
    }

    /**
     * Filter the query on the id_entite column
     *
     * Example usage:
     * <code>
     * $query->filterByIdEntite(1234); // WHERE id_entite = 1234
     * $query->filterByIdEntite(array(12, 34)); // WHERE id_entite IN (12, 34)
     * $query->filterByIdEntite(array('min' => 12)); // WHERE id_entite > 12
     * </code>
     *
     * @param     mixed $idEntite The value to use as filter.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildAsscJournalsArchiveQuery The current query, for fluid interface
     */
    public function filterByIdEntite($idEntite = null, $comparison = null)
    {
        if (is_array($idEntite)) {
            $useMinMax = false;
            if (isset($idEntite['min'])) {
                $this->addUsingAlias(AsscJournalsArchiveTableMap::COL_ID_ENTITE, $idEntite['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($idEntite['max'])) {
                $this->addUsingAlias(AsscJournalsArchiveTableMap::COL_ID_ENTITE, $idEntite['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(AsscJournalsArchiveTableMap::COL_ID_ENTITE, $idEntite, $comparison);
    }

    /**
     * Filter the query on the observation column
     *
     * Example usage:
     * <code>
     * $query->filterByObservation('fooValue');   // WHERE observation = 'fooValue'
     * $query->filterByObservation('%fooValue%', Criteria::LIKE); // WHERE observation LIKE '%fooValue%'
     * </code>
     *
     * @param     string $observation The value to use as filter.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildAsscJournalsArchiveQuery The current query, for fluid interface
     */
    public function filterByObservation($observation = null, $comparison = null)
    {
        if (null === $comparison) {
            if (is_array($observation)) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(AsscJournalsArchiveTableMap::COL_OBSERVATION, $observation, $comparison);
    }

    /**
     * Filter the query on the created_at column
     *
     * Example usage:
     * <code>
     * $query->filterByCreatedAt('2011-03-14'); // WHERE created_at = '2011-03-14'
     * $query->filterByCreatedAt('now'); // WHERE created_at = '2011-03-14'
     * $query->filterByCreatedAt(array('max' => 'yesterday')); // WHERE created_at > '2011-03-13'
     * </code>
     *
     * @param     mixed $createdAt The value to use as filter.
     *              Values can be integers (unix timestamps), DateTime objects, or strings.
     *              Empty strings are treated as NULL.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildAsscJournalsArchiveQuery The current query, for fluid interface
     */
    public function filterByCreatedAt($createdAt = null, $comparison = null)
    {
        if (is_array($createdAt)) {
            $useMinMax = false;
            if (isset($createdAt['min'])) {
                $this->addUsingAlias(AsscJournalsArchiveTableMap::COL_CREATED_AT, $createdAt['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($createdAt['max'])) {
                $this->addUsingAlias(AsscJournalsArchiveTableMap::COL_CREATED_AT, $createdAt['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(AsscJournalsArchiveTableMap::COL_CREATED_AT, $createdAt, $comparison);
    }

    /**
     * Filter the query on the updated_at column
     *
     * Example usage:
     * <code>
     * $query->filterByUpdatedAt('2011-03-14'); // WHERE updated_at = '2011-03-14'
     * $query->filterByUpdatedAt('now'); // WHERE updated_at = '2011-03-14'
     * $query->filterByUpdatedAt(array('max' => 'yesterday')); // WHERE updated_at > '2011-03-13'
     * </code>
     *
     * @param     mixed $updatedAt The value to use as filter.
     *              Values can be integers (unix timestamps), DateTime objects, or strings.
     *              Empty strings are treated as NULL.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildAsscJournalsArchiveQuery The current query, for fluid interface
     */
    public function filterByUpdatedAt($updatedAt = null, $comparison = null)
    {
        if (is_array($updatedAt)) {
            $useMinMax = false;
            if (isset($updatedAt['min'])) {
                $this->addUsingAlias(AsscJournalsArchiveTableMap::COL_UPDATED_AT, $updatedAt['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($updatedAt['max'])) {
                $this->addUsingAlias(AsscJournalsArchiveTableMap::COL_UPDATED_AT, $updatedAt['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(AsscJournalsArchiveTableMap::COL_UPDATED_AT, $updatedAt, $comparison);
    }

    /**
     * Filter the query on the archived_at column
     *
     * Example usage:
     * <code>
     * $query->filterByArchivedAt('2011-03-14'); // WHERE archived_at = '2011-03-14'
     * $query->filterByArchivedAt('now'); // WHERE archived_at = '2011-03-14'
     * $query->filterByArchivedAt(array('max' => 'yesterday')); // WHERE archived_at > '2011-03-13'
     * </code>
     *
     * @param     mixed $archivedAt The value to use as filter.
     *              Values can be integers (unix timestamps), DateTime objects, or strings.
     *              Empty strings are treated as NULL.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildAsscJournalsArchiveQuery The current query, for fluid interface
     */
    public function filterByArchivedAt($archivedAt = null, $comparison = null)
    {
        if (is_array($archivedAt)) {
            $useMinMax = false;
            if (isset($archivedAt['min'])) {
                $this->addUsingAlias(AsscJournalsArchiveTableMap::COL_ARCHIVED_AT, $archivedAt['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($archivedAt['max'])) {
                $this->addUsingAlias(AsscJournalsArchiveTableMap::COL_ARCHIVED_AT, $archivedAt['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(AsscJournalsArchiveTableMap::COL_ARCHIVED_AT, $archivedAt, $comparison);
    }

    /**
     * Exclude object from result
     *
     * @param   ChildAsscJournalsArchive $asscJournalsArchive Object to remove from the list of results
     *
     * @return $this|ChildAsscJournalsArchiveQuery The current query, for fluid interface
     */
    public function prune($asscJournalsArchive = null)
    {
        if ($asscJournalsArchive) {
            $this->addUsingAlias(AsscJournalsArchiveTableMap::COL_ID_JOURNAL, $asscJournalsArchive->getIdJournal(), Criteria::NOT_EQUAL);
        }

        return $this;
    }

    /**
     * Deletes all rows from the assc_journals_archive table.
     *
     * @param ConnectionInterface $con the connection to use
     * @return int The number of affected rows (if supported by underlying database driver).
     */
    public function doDeleteAll(ConnectionInterface $con = null)
    {
        if (null === $con) {
            $con = Propel::getServiceContainer()->getWriteConnection(AsscJournalsArchiveTableMap::DATABASE_NAME);
        }

        // use transaction because $criteria could contain info
        // for more than one table or we could emulating ON DELETE CASCADE, etc.
        return $con->transaction(function () use ($con) {
            $affectedRows = 0; // initialize var to track total num of affected rows
            $affectedRows += parent::doDeleteAll($con);
            // Because this db requires some delete cascade/set null emulation, we have to
            // clear the cached instance *after* the emulation has happened (since
            // instances get re-added by the select statement contained therein).
            AsscJournalsArchiveTableMap::clearInstancePool();
            AsscJournalsArchiveTableMap::clearRelatedInstancePool();

            return $affectedRows;
        });
    }

    /**
     * Performs a DELETE on the database based on the current ModelCriteria
     *
     * @param ConnectionInterface $con the connection to use
     * @return int             The number of affected rows (if supported by underlying database driver).  This includes CASCADE-related rows
     *                         if supported by native driver or if emulated using Propel.
     * @throws PropelException Any exceptions caught during processing will be
     *                         rethrown wrapped into a PropelException.
     */
    public function delete(ConnectionInterface $con = null)
    {
        if (null === $con) {
            $con = Propel::getServiceContainer()->getWriteConnection(AsscJournalsArchiveTableMap::DATABASE_NAME);
        }

        $criteria = $this;

        // Set the correct dbName
        $criteria->setDbName(AsscJournalsArchiveTableMap::DATABASE_NAME);

        // use transaction because $criteria could contain info
        // for more than one table or we could emulating ON DELETE CASCADE, etc.
        return $con->transaction(function () use ($con, $criteria) {
            $affectedRows = 0; // initialize var to track total num of affected rows

            AsscJournalsArchiveTableMap::removeInstanceFromPool($criteria);

            $affectedRows += ModelCriteria::delete($con);
            AsscJournalsArchiveTableMap::clearRelatedInstancePool();

            return $affectedRows;
        });
    }

} // AsscJournalsArchiveQuery
