<?php

namespace Base;

use \AssoServicerendusArchive as ChildAssoServicerendusArchive;
use \AssoServicerendusArchiveQuery as ChildAssoServicerendusArchiveQuery;
use \Entite as ChildEntite;
use \EntiteQuery as ChildEntiteQuery;
use \Individu as ChildIndividu;
use \IndividuQuery as ChildIndividuQuery;
use \Membre as ChildMembre;
use \MembreQuery as ChildMembreQuery;
use \Prestation as ChildPrestation;
use \PrestationQuery as ChildPrestationQuery;
use \Servicepaiement as ChildServicepaiement;
use \ServicepaiementQuery as ChildServicepaiementQuery;
use \Servicerendu as ChildServicerendu;
use \ServicerenduIndividu as ChildServicerenduIndividu;
use \ServicerenduIndividuQuery as ChildServicerenduIndividuQuery;
use \ServicerenduQuery as ChildServicerenduQuery;
use \DateTime;
use \Exception;
use \PDO;
use Map\ServicepaiementTableMap;
use Map\ServicerenduIndividuTableMap;
use Map\ServicerenduTableMap;
use Propel\Runtime\Propel;
use Propel\Runtime\ActiveQuery\Criteria;
use Propel\Runtime\ActiveQuery\ModelCriteria;
use Propel\Runtime\ActiveRecord\ActiveRecordInterface;
use Propel\Runtime\Collection\Collection;
use Propel\Runtime\Collection\ObjectCollection;
use Propel\Runtime\Connection\ConnectionInterface;
use Propel\Runtime\Exception\BadMethodCallException;
use Propel\Runtime\Exception\LogicException;
use Propel\Runtime\Exception\PropelException;
use Propel\Runtime\Map\TableMap;
use Propel\Runtime\Parser\AbstractParser;
use Propel\Runtime\Util\PropelDateTime;

/**
 * Base class that represents a row from the 'asso_servicerendus' table.
 *
 *
 *
 * @package    propel.generator..Base
 */
abstract class Servicerendu implements ActiveRecordInterface
{
    /**
     * TableMap class name
     */
    const TABLE_MAP = '\\Map\\ServicerenduTableMap';


    /**
     * attribute to determine if this object has previously been saved.
     * @var boolean
     */
    protected $new = true;

    /**
     * attribute to determine whether this object has been deleted.
     * @var boolean
     */
    protected $deleted = false;

    /**
     * The columns that have been modified in current object.
     * Tracking modified columns allows us to only update modified columns.
     * @var array
     */
    protected $modifiedColumns = array();

    /**
     * The (virtual) columns that are added at runtime
     * The formatters can add supplementary columns based on a resultset
     * @var array
     */
    protected $virtualColumns = array();

    /**
     * The value for the id_servicerendu field.
     *
     * @var        string
     */
    protected $id_servicerendu;

    /**
     * The value for the id_membre field.
     *
     * @var        string
     */
    protected $id_membre;

    /**
     * The value for the montant field.
     * Montant
     * @var        double
     */
    protected $montant;

    /**
     * The value for the date_enregistrement field.
     *
     * @var        DateTime
     */
    protected $date_enregistrement;

    /**
     * The value for the date_debut field.
     *
     * Note: this column has a database default value of: '1980-01-01'
     * @var        DateTime
     */
    protected $date_debut;

    /**
     * The value for the date_fin field.
     *
     * Note: this column has a database default value of: '3000-12-31'
     * @var        DateTime
     */
    protected $date_fin;

    /**
     * The value for the origine field.
     *
     * @var        int
     */
    protected $origine;

    /**
     * The value for the premier field.
     *
     * Note: this column has a database default value of: 0
     * @var        int
     */
    protected $premier;

    /**
     * The value for the dernier field.
     *
     * Note: this column has a database default value of: 0
     * @var        int
     */
    protected $dernier;

    /**
     * The value for the id_entite field.
     *
     * Note: this column has a database default value of: 1
     * @var        int
     */
    protected $id_entite;

    /**
     * The value for the id_prestation field.
     *
     * @var        string
     */
    protected $id_prestation;

    /**
     * The value for the regle field.
     *
     * Note: this column has a database default value of: 1
     * @var        int
     */
    protected $regle;

    /**
     * The value for the comptabilise field.
     *
     * Note: this column has a database default value of: false
     * @var        boolean
     */
    protected $comptabilise;

    /**
     * The value for the observation field.
     *
     * @var        string
     */
    protected $observation;

    /**
     * The value for the id_tva field.
     *
     * Note: this column has a database default value of: '0'
     * @var        string
     */
    protected $id_tva;

    /**
     * The value for the quantite field.
     *
     * Note: this column has a database default value of: 1.0
     * @var        double
     */
    protected $quantite;

    /**
     * The value for the created_at field.
     *
     * @var        DateTime
     */
    protected $created_at;

    /**
     * The value for the updated_at field.
     *
     * @var        DateTime
     */
    protected $updated_at;

    /**
     * @var        ChildPrestation
     */
    protected $aPrestation;

    /**
     * @var        ChildEntite
     */
    protected $aEntite;

    /**
     * @var        ChildMembre
     */
    protected $aMembre;

    /**
     * @var        ChildServicerendu
     */
    protected $aServicerenduRelatedByOrigine;

    /**
     * @var        ObjectCollection|ChildServicepaiement[] Collection to store aggregation of ChildServicepaiement objects.
     */
    protected $collServicepaiements;
    protected $collServicepaiementsPartial;

    /**
     * @var        ObjectCollection|ChildServicerendu[] Collection to store aggregation of ChildServicerendu objects.
     */
    protected $collServicerendusRelatedByIdServicerendu;
    protected $collServicerendusRelatedByIdServicerenduPartial;

    /**
     * @var        ObjectCollection|ChildServicerenduIndividu[] Collection to store aggregation of ChildServicerenduIndividu objects.
     */
    protected $collServicerenduIndividus;
    protected $collServicerenduIndividusPartial;

    /**
     * @var        ObjectCollection|ChildIndividu[] Cross Collection to store aggregation of ChildIndividu objects.
     */
    protected $collIndividus;

    /**
     * @var bool
     */
    protected $collIndividusPartial;

    /**
     * Flag to prevent endless save loop, if this object is referenced
     * by another object which falls in this transaction.
     *
     * @var boolean
     */
    protected $alreadyInSave = false;

    // archivable behavior
    protected $archiveOnDelete = true;

    /**
     * An array of objects scheduled for deletion.
     * @var ObjectCollection|ChildIndividu[]
     */
    protected $individusScheduledForDeletion = null;

    /**
     * An array of objects scheduled for deletion.
     * @var ObjectCollection|ChildServicepaiement[]
     */
    protected $servicepaiementsScheduledForDeletion = null;

    /**
     * An array of objects scheduled for deletion.
     * @var ObjectCollection|ChildServicerendu[]
     */
    protected $servicerendusRelatedByIdServicerenduScheduledForDeletion = null;

    /**
     * An array of objects scheduled for deletion.
     * @var ObjectCollection|ChildServicerenduIndividu[]
     */
    protected $servicerenduIndividusScheduledForDeletion = null;

    /**
     * Applies default values to this object.
     * This method should be called from the object's constructor (or
     * equivalent initialization method).
     * @see __construct()
     */
    public function applyDefaultValues()
    {
        $this->date_debut = PropelDateTime::newInstance('1980-01-01', null, 'DateTime');
        $this->date_fin = PropelDateTime::newInstance('3000-12-31', null, 'DateTime');
        $this->premier = 0;
        $this->dernier = 0;
        $this->id_entite = 1;
        $this->regle = 1;
        $this->comptabilise = false;
        $this->id_tva = '0';
        $this->quantite = 1.0;
    }

    /**
     * Initializes internal state of Base\Servicerendu object.
     * @see applyDefaults()
     */
    public function __construct()
    {
        $this->applyDefaultValues();
    }

    /**
     * Returns whether the object has been modified.
     *
     * @return boolean True if the object has been modified.
     */
    public function isModified()
    {
        return !!$this->modifiedColumns;
    }

    /**
     * Has specified column been modified?
     *
     * @param  string  $col column fully qualified name (TableMap::TYPE_COLNAME), e.g. Book::AUTHOR_ID
     * @return boolean True if $col has been modified.
     */
    public function isColumnModified($col)
    {
        return $this->modifiedColumns && isset($this->modifiedColumns[$col]);
    }

    /**
     * Get the columns that have been modified in this object.
     * @return array A unique list of the modified column names for this object.
     */
    public function getModifiedColumns()
    {
        return $this->modifiedColumns ? array_keys($this->modifiedColumns) : [];
    }

    /**
     * Returns whether the object has ever been saved.  This will
     * be false, if the object was retrieved from storage or was created
     * and then saved.
     *
     * @return boolean true, if the object has never been persisted.
     */
    public function isNew()
    {
        return $this->new;
    }

    /**
     * Setter for the isNew attribute.  This method will be called
     * by Propel-generated children and objects.
     *
     * @param boolean $b the state of the object.
     */
    public function setNew($b)
    {
        $this->new = (boolean) $b;
    }

    /**
     * Whether this object has been deleted.
     * @return boolean The deleted state of this object.
     */
    public function isDeleted()
    {
        return $this->deleted;
    }

    /**
     * Specify whether this object has been deleted.
     * @param  boolean $b The deleted state of this object.
     * @return void
     */
    public function setDeleted($b)
    {
        $this->deleted = (boolean) $b;
    }

    /**
     * Sets the modified state for the object to be false.
     * @param  string $col If supplied, only the specified column is reset.
     * @return void
     */
    public function resetModified($col = null)
    {
        if (null !== $col) {
            if (isset($this->modifiedColumns[$col])) {
                unset($this->modifiedColumns[$col]);
            }
        } else {
            $this->modifiedColumns = array();
        }
    }

    /**
     * Compares this with another <code>Servicerendu</code> instance.  If
     * <code>obj</code> is an instance of <code>Servicerendu</code>, delegates to
     * <code>equals(Servicerendu)</code>.  Otherwise, returns <code>false</code>.
     *
     * @param  mixed   $obj The object to compare to.
     * @return boolean Whether equal to the object specified.
     */
    public function equals($obj)
    {
        if (!$obj instanceof static) {
            return false;
        }

        if ($this === $obj) {
            return true;
        }

        if (null === $this->getPrimaryKey() || null === $obj->getPrimaryKey()) {
            return false;
        }

        return $this->getPrimaryKey() === $obj->getPrimaryKey();
    }

    /**
     * Get the associative array of the virtual columns in this object
     *
     * @return array
     */
    public function getVirtualColumns()
    {
        return $this->virtualColumns;
    }

    /**
     * Checks the existence of a virtual column in this object
     *
     * @param  string  $name The virtual column name
     * @return boolean
     */
    public function hasVirtualColumn($name)
    {
        return array_key_exists($name, $this->virtualColumns);
    }

    /**
     * Get the value of a virtual column in this object
     *
     * @param  string $name The virtual column name
     * @return mixed
     *
     * @throws PropelException
     */
    public function getVirtualColumn($name)
    {
        if (!$this->hasVirtualColumn($name)) {
            throw new PropelException(sprintf('Cannot get value of inexistent virtual column %s.', $name));
        }

        return $this->virtualColumns[$name];
    }

    /**
     * Set the value of a virtual column in this object
     *
     * @param string $name  The virtual column name
     * @param mixed  $value The value to give to the virtual column
     *
     * @return $this|Servicerendu The current object, for fluid interface
     */
    public function setVirtualColumn($name, $value)
    {
        $this->virtualColumns[$name] = $value;

        return $this;
    }

    /**
     * Logs a message using Propel::log().
     *
     * @param  string  $msg
     * @param  int     $priority One of the Propel::LOG_* logging levels
     * @return boolean
     */
    protected function log($msg, $priority = Propel::LOG_INFO)
    {
        return Propel::log(get_class($this) . ': ' . $msg, $priority);
    }

    /**
     * Export the current object properties to a string, using a given parser format
     * <code>
     * $book = BookQuery::create()->findPk(9012);
     * echo $book->exportTo('JSON');
     *  => {"Id":9012,"Title":"Don Juan","ISBN":"0140422161","Price":12.99,"PublisherId":1234,"AuthorId":5678}');
     * </code>
     *
     * @param  mixed   $parser                 A AbstractParser instance, or a format name ('XML', 'YAML', 'JSON', 'CSV')
     * @param  boolean $includeLazyLoadColumns (optional) Whether to include lazy load(ed) columns. Defaults to TRUE.
     * @return string  The exported data
     */
    public function exportTo($parser, $includeLazyLoadColumns = true)
    {
        if (!$parser instanceof AbstractParser) {
            $parser = AbstractParser::getParser($parser);
        }

        return $parser->fromArray($this->toArray(TableMap::TYPE_PHPNAME, $includeLazyLoadColumns, array(), true));
    }

    /**
     * Clean up internal collections prior to serializing
     * Avoids recursive loops that turn into segmentation faults when serializing
     */
    public function __sleep()
    {
        $this->clearAllReferences();

        $cls = new \ReflectionClass($this);
        $propertyNames = [];
        $serializableProperties = array_diff($cls->getProperties(), $cls->getProperties(\ReflectionProperty::IS_STATIC));

        foreach($serializableProperties as $property) {
            $propertyNames[] = $property->getName();
        }

        return $propertyNames;
    }

    /**
     * Get the [id_servicerendu] column value.
     *
     * @return string
     */
    public function getIdServicerendu()
    {
        return $this->id_servicerendu;
    }

    /**
     * Get the [id_membre] column value.
     *
     * @return string
     */
    public function getIdMembre()
    {
        return $this->id_membre;
    }

    /**
     * Get the [montant] column value.
     * Montant
     * @return double
     */
    public function getMontant()
    {
        return $this->montant;
    }

    /**
     * Get the [optionally formatted] temporal [date_enregistrement] column value.
     *
     *
     * @param      string $format The date/time format string (either date()-style or strftime()-style).
     *                            If format is NULL, then the raw DateTime object will be returned.
     *
     * @return string|DateTime Formatted date/time value as string or DateTime object (if format is NULL), NULL if column is NULL, and 0 if column value is 0000-00-00
     *
     * @throws PropelException - if unable to parse/validate the date/time value.
     */
    public function getDateEnregistrement($format = NULL)
    {
        if ($format === null) {
            return $this->date_enregistrement;
        } else {
            return $this->date_enregistrement instanceof \DateTimeInterface ? $this->date_enregistrement->format($format) : null;
        }
    }

    /**
     * Get the [optionally formatted] temporal [date_debut] column value.
     *
     *
     * @param      string $format The date/time format string (either date()-style or strftime()-style).
     *                            If format is NULL, then the raw DateTime object will be returned.
     *
     * @return string|DateTime Formatted date/time value as string or DateTime object (if format is NULL), NULL if column is NULL, and 0 if column value is 0000-00-00
     *
     * @throws PropelException - if unable to parse/validate the date/time value.
     */
    public function getDateDebut($format = NULL)
    {
        if ($format === null) {
            return $this->date_debut;
        } else {
            return $this->date_debut instanceof \DateTimeInterface ? $this->date_debut->format($format) : null;
        }
    }

    /**
     * Get the [optionally formatted] temporal [date_fin] column value.
     *
     *
     * @param      string $format The date/time format string (either date()-style or strftime()-style).
     *                            If format is NULL, then the raw DateTime object will be returned.
     *
     * @return string|DateTime Formatted date/time value as string or DateTime object (if format is NULL), NULL if column is NULL, and 0 if column value is 0000-00-00
     *
     * @throws PropelException - if unable to parse/validate the date/time value.
     */
    public function getDateFin($format = NULL)
    {
        if ($format === null) {
            return $this->date_fin;
        } else {
            return $this->date_fin instanceof \DateTimeInterface ? $this->date_fin->format($format) : null;
        }
    }

    /**
     * Get the [origine] column value.
     *
     * @return int
     */
    public function getOrigine()
    {
        return $this->origine;
    }

    /**
     * Get the [premier] column value.
     *
     * @return int
     */
    public function getPremier()
    {
        return $this->premier;
    }

    /**
     * Get the [dernier] column value.
     *
     * @return int
     */
    public function getDernier()
    {
        return $this->dernier;
    }

    /**
     * Get the [id_entite] column value.
     *
     * @return int
     */
    public function getIdEntite()
    {
        return $this->id_entite;
    }

    /**
     * Get the [id_prestation] column value.
     *
     * @return string
     */
    public function getIdPrestation()
    {
        return $this->id_prestation;
    }

    /**
     * Get the [regle] column value.
     *
     * @return int
     */
    public function getRegle()
    {
        return $this->regle;
    }

    /**
     * Get the [comptabilise] column value.
     *
     * @return boolean
     */
    public function getComptabilise()
    {
        return $this->comptabilise;
    }

    /**
     * Get the [comptabilise] column value.
     *
     * @return boolean
     */
    public function isComptabilise()
    {
        return $this->getComptabilise();
    }

    /**
     * Get the [observation] column value.
     *
     * @return string
     */
    public function getObservation()
    {
        return $this->observation;
    }

    /**
     * Get the [id_tva] column value.
     *
     * @return string
     */
    public function getIdTva()
    {
        return $this->id_tva;
    }

    /**
     * Get the [quantite] column value.
     *
     * @return double
     */
    public function getQuantite()
    {
        return $this->quantite;
    }

    /**
     * Get the [optionally formatted] temporal [created_at] column value.
     *
     *
     * @param      string $format The date/time format string (either date()-style or strftime()-style).
     *                            If format is NULL, then the raw DateTime object will be returned.
     *
     * @return string|DateTime Formatted date/time value as string or DateTime object (if format is NULL), NULL if column is NULL, and 0 if column value is 0000-00-00 00:00:00
     *
     * @throws PropelException - if unable to parse/validate the date/time value.
     */
    public function getCreatedAt($format = NULL)
    {
        if ($format === null) {
            return $this->created_at;
        } else {
            return $this->created_at instanceof \DateTimeInterface ? $this->created_at->format($format) : null;
        }
    }

    /**
     * Get the [optionally formatted] temporal [updated_at] column value.
     *
     *
     * @param      string $format The date/time format string (either date()-style or strftime()-style).
     *                            If format is NULL, then the raw DateTime object will be returned.
     *
     * @return string|DateTime Formatted date/time value as string or DateTime object (if format is NULL), NULL if column is NULL, and 0 if column value is 0000-00-00 00:00:00
     *
     * @throws PropelException - if unable to parse/validate the date/time value.
     */
    public function getUpdatedAt($format = NULL)
    {
        if ($format === null) {
            return $this->updated_at;
        } else {
            return $this->updated_at instanceof \DateTimeInterface ? $this->updated_at->format($format) : null;
        }
    }

    /**
     * Set the value of [id_servicerendu] column.
     *
     * @param string $v new value
     * @return $this|\Servicerendu The current object (for fluent API support)
     */
    public function setIdServicerendu($v)
    {
        if ($v !== null) {
            $v = (string) $v;
        }

        if ($this->id_servicerendu !== $v) {
            $this->id_servicerendu = $v;
            $this->modifiedColumns[ServicerenduTableMap::COL_ID_SERVICERENDU] = true;
        }

        return $this;
    } // setIdServicerendu()

    /**
     * Set the value of [id_membre] column.
     *
     * @param string $v new value
     * @return $this|\Servicerendu The current object (for fluent API support)
     */
    public function setIdMembre($v)
    {
        if ($v !== null) {
            $v = (string) $v;
        }

        if ($this->id_membre !== $v) {
            $this->id_membre = $v;
            $this->modifiedColumns[ServicerenduTableMap::COL_ID_MEMBRE] = true;
        }

        if ($this->aMembre !== null && $this->aMembre->getIdMembre() !== $v) {
            $this->aMembre = null;
        }

        return $this;
    } // setIdMembre()

    /**
     * Set the value of [montant] column.
     * Montant
     * @param double $v new value
     * @return $this|\Servicerendu The current object (for fluent API support)
     */
    public function setMontant($v)
    {
        if ($v !== null) {
            $v = (double) $v;
        }

        if ($this->montant !== $v) {
            $this->montant = $v;
            $this->modifiedColumns[ServicerenduTableMap::COL_MONTANT] = true;
        }

        return $this;
    } // setMontant()

    /**
     * Sets the value of [date_enregistrement] column to a normalized version of the date/time value specified.
     *
     * @param  mixed $v string, integer (timestamp), or \DateTimeInterface value.
     *               Empty strings are treated as NULL.
     * @return $this|\Servicerendu The current object (for fluent API support)
     */
    public function setDateEnregistrement($v)
    {
        $dt = PropelDateTime::newInstance($v, null, 'DateTime');
        if ($this->date_enregistrement !== null || $dt !== null) {
            if ($this->date_enregistrement === null || $dt === null || $dt->format("Y-m-d") !== $this->date_enregistrement->format("Y-m-d")) {
                $this->date_enregistrement = $dt === null ? null : clone $dt;
                $this->modifiedColumns[ServicerenduTableMap::COL_DATE_ENREGISTREMENT] = true;
            }
        } // if either are not null

        return $this;
    } // setDateEnregistrement()

    /**
     * Sets the value of [date_debut] column to a normalized version of the date/time value specified.
     *
     * @param  mixed $v string, integer (timestamp), or \DateTimeInterface value.
     *               Empty strings are treated as NULL.
     * @return $this|\Servicerendu The current object (for fluent API support)
     */
    public function setDateDebut($v)
    {
        $dt = PropelDateTime::newInstance($v, null, 'DateTime');
        if ($this->date_debut !== null || $dt !== null) {
            if ( ($dt != $this->date_debut) // normalized values don't match
                || ($dt->format('Y-m-d') === '1980-01-01') // or the entered value matches the default
                 ) {
                $this->date_debut = $dt === null ? null : clone $dt;
                $this->modifiedColumns[ServicerenduTableMap::COL_DATE_DEBUT] = true;
            }
        } // if either are not null

        return $this;
    } // setDateDebut()

    /**
     * Sets the value of [date_fin] column to a normalized version of the date/time value specified.
     *
     * @param  mixed $v string, integer (timestamp), or \DateTimeInterface value.
     *               Empty strings are treated as NULL.
     * @return $this|\Servicerendu The current object (for fluent API support)
     */
    public function setDateFin($v)
    {
        $dt = PropelDateTime::newInstance($v, null, 'DateTime');
        if ($this->date_fin !== null || $dt !== null) {
            if ( ($dt != $this->date_fin) // normalized values don't match
                || ($dt->format('Y-m-d') === '3000-12-31') // or the entered value matches the default
                 ) {
                $this->date_fin = $dt === null ? null : clone $dt;
                $this->modifiedColumns[ServicerenduTableMap::COL_DATE_FIN] = true;
            }
        } // if either are not null

        return $this;
    } // setDateFin()

    /**
     * Set the value of [origine] column.
     *
     * @param int $v new value
     * @return $this|\Servicerendu The current object (for fluent API support)
     */
    public function setOrigine($v)
    {
        if ($v !== null) {
            $v = (int) $v;
        }

        if ($this->origine !== $v) {
            $this->origine = $v;
            $this->modifiedColumns[ServicerenduTableMap::COL_ORIGINE] = true;
        }

        if ($this->aServicerenduRelatedByOrigine !== null && $this->aServicerenduRelatedByOrigine->getIdServicerendu() !== $v) {
            $this->aServicerenduRelatedByOrigine = null;
        }

        return $this;
    } // setOrigine()

    /**
     * Set the value of [premier] column.
     *
     * @param int $v new value
     * @return $this|\Servicerendu The current object (for fluent API support)
     */
    public function setPremier($v)
    {
        if ($v !== null) {
            $v = (int) $v;
        }

        if ($this->premier !== $v) {
            $this->premier = $v;
            $this->modifiedColumns[ServicerenduTableMap::COL_PREMIER] = true;
        }

        return $this;
    } // setPremier()

    /**
     * Set the value of [dernier] column.
     *
     * @param int $v new value
     * @return $this|\Servicerendu The current object (for fluent API support)
     */
    public function setDernier($v)
    {
        if ($v !== null) {
            $v = (int) $v;
        }

        if ($this->dernier !== $v) {
            $this->dernier = $v;
            $this->modifiedColumns[ServicerenduTableMap::COL_DERNIER] = true;
        }

        return $this;
    } // setDernier()

    /**
     * Set the value of [id_entite] column.
     *
     * @param int $v new value
     * @return $this|\Servicerendu The current object (for fluent API support)
     */
    public function setIdEntite($v)
    {
        if ($v !== null) {
            $v = (int) $v;
        }

        if ($this->id_entite !== $v) {
            $this->id_entite = $v;
            $this->modifiedColumns[ServicerenduTableMap::COL_ID_ENTITE] = true;
        }

        if ($this->aEntite !== null && $this->aEntite->getIdEntite() !== $v) {
            $this->aEntite = null;
        }

        return $this;
    } // setIdEntite()

    /**
     * Set the value of [id_prestation] column.
     *
     * @param string $v new value
     * @return $this|\Servicerendu The current object (for fluent API support)
     */
    public function setIdPrestation($v)
    {
        if ($v !== null) {
            $v = (string) $v;
        }

        if ($this->id_prestation !== $v) {
            $this->id_prestation = $v;
            $this->modifiedColumns[ServicerenduTableMap::COL_ID_PRESTATION] = true;
        }

        if ($this->aPrestation !== null && $this->aPrestation->getIdPrestation() !== $v) {
            $this->aPrestation = null;
        }

        return $this;
    } // setIdPrestation()

    /**
     * Set the value of [regle] column.
     *
     * @param int $v new value
     * @return $this|\Servicerendu The current object (for fluent API support)
     */
    public function setRegle($v)
    {
        if ($v !== null) {
            $v = (int) $v;
        }

        if ($this->regle !== $v) {
            $this->regle = $v;
            $this->modifiedColumns[ServicerenduTableMap::COL_REGLE] = true;
        }

        return $this;
    } // setRegle()

    /**
     * Sets the value of the [comptabilise] column.
     * Non-boolean arguments are converted using the following rules:
     *   * 1, '1', 'true',  'on',  and 'yes' are converted to boolean true
     *   * 0, '0', 'false', 'off', and 'no'  are converted to boolean false
     * Check on string values is case insensitive (so 'FaLsE' is seen as 'false').
     *
     * @param  boolean|integer|string $v The new value
     * @return $this|\Servicerendu The current object (for fluent API support)
     */
    public function setComptabilise($v)
    {
        if ($v !== null) {
            if (is_string($v)) {
                $v = in_array(strtolower($v), array('false', 'off', '-', 'no', 'n', '0', '')) ? false : true;
            } else {
                $v = (boolean) $v;
            }
        }

        if ($this->comptabilise !== $v) {
            $this->comptabilise = $v;
            $this->modifiedColumns[ServicerenduTableMap::COL_COMPTABILISE] = true;
        }

        return $this;
    } // setComptabilise()

    /**
     * Set the value of [observation] column.
     *
     * @param string $v new value
     * @return $this|\Servicerendu The current object (for fluent API support)
     */
    public function setObservation($v)
    {
        if ($v !== null) {
            $v = (string) $v;
        }

        if ($this->observation !== $v) {
            $this->observation = $v;
            $this->modifiedColumns[ServicerenduTableMap::COL_OBSERVATION] = true;
        }

        return $this;
    } // setObservation()

    /**
     * Set the value of [id_tva] column.
     *
     * @param string $v new value
     * @return $this|\Servicerendu The current object (for fluent API support)
     */
    public function setIdTva($v)
    {
        if ($v !== null) {
            $v = (string) $v;
        }

        if ($this->id_tva !== $v) {
            $this->id_tva = $v;
            $this->modifiedColumns[ServicerenduTableMap::COL_ID_TVA] = true;
        }

        return $this;
    } // setIdTva()

    /**
     * Set the value of [quantite] column.
     *
     * @param double $v new value
     * @return $this|\Servicerendu The current object (for fluent API support)
     */
    public function setQuantite($v)
    {
        if ($v !== null) {
            $v = (double) $v;
        }

        if ($this->quantite !== $v) {
            $this->quantite = $v;
            $this->modifiedColumns[ServicerenduTableMap::COL_QUANTITE] = true;
        }

        return $this;
    } // setQuantite()

    /**
     * Sets the value of [created_at] column to a normalized version of the date/time value specified.
     *
     * @param  mixed $v string, integer (timestamp), or \DateTimeInterface value.
     *               Empty strings are treated as NULL.
     * @return $this|\Servicerendu The current object (for fluent API support)
     */
    public function setCreatedAt($v)
    {
        $dt = PropelDateTime::newInstance($v, null, 'DateTime');
        if ($this->created_at !== null || $dt !== null) {
            if ($this->created_at === null || $dt === null || $dt->format("Y-m-d H:i:s.u") !== $this->created_at->format("Y-m-d H:i:s.u")) {
                $this->created_at = $dt === null ? null : clone $dt;
                $this->modifiedColumns[ServicerenduTableMap::COL_CREATED_AT] = true;
            }
        } // if either are not null

        return $this;
    } // setCreatedAt()

    /**
     * Sets the value of [updated_at] column to a normalized version of the date/time value specified.
     *
     * @param  mixed $v string, integer (timestamp), or \DateTimeInterface value.
     *               Empty strings are treated as NULL.
     * @return $this|\Servicerendu The current object (for fluent API support)
     */
    public function setUpdatedAt($v)
    {
        $dt = PropelDateTime::newInstance($v, null, 'DateTime');
        if ($this->updated_at !== null || $dt !== null) {
            if ($this->updated_at === null || $dt === null || $dt->format("Y-m-d H:i:s.u") !== $this->updated_at->format("Y-m-d H:i:s.u")) {
                $this->updated_at = $dt === null ? null : clone $dt;
                $this->modifiedColumns[ServicerenduTableMap::COL_UPDATED_AT] = true;
            }
        } // if either are not null

        return $this;
    } // setUpdatedAt()

    /**
     * Indicates whether the columns in this object are only set to default values.
     *
     * This method can be used in conjunction with isModified() to indicate whether an object is both
     * modified _and_ has some values set which are non-default.
     *
     * @return boolean Whether the columns in this object are only been set with default values.
     */
    public function hasOnlyDefaultValues()
    {
            if ($this->date_debut && $this->date_debut->format('Y-m-d') !== '1980-01-01') {
                return false;
            }

            if ($this->date_fin && $this->date_fin->format('Y-m-d') !== '3000-12-31') {
                return false;
            }

            if ($this->premier !== 0) {
                return false;
            }

            if ($this->dernier !== 0) {
                return false;
            }

            if ($this->id_entite !== 1) {
                return false;
            }

            if ($this->regle !== 1) {
                return false;
            }

            if ($this->comptabilise !== false) {
                return false;
            }

            if ($this->id_tva !== '0') {
                return false;
            }

            if ($this->quantite !== 1.0) {
                return false;
            }

        // otherwise, everything was equal, so return TRUE
        return true;
    } // hasOnlyDefaultValues()

    /**
     * Hydrates (populates) the object variables with values from the database resultset.
     *
     * An offset (0-based "start column") is specified so that objects can be hydrated
     * with a subset of the columns in the resultset rows.  This is needed, for example,
     * for results of JOIN queries where the resultset row includes columns from two or
     * more tables.
     *
     * @param array   $row       The row returned by DataFetcher->fetch().
     * @param int     $startcol  0-based offset column which indicates which restultset column to start with.
     * @param boolean $rehydrate Whether this object is being re-hydrated from the database.
     * @param string  $indexType The index type of $row. Mostly DataFetcher->getIndexType().
                                  One of the class type constants TableMap::TYPE_PHPNAME, TableMap::TYPE_CAMELNAME
     *                            TableMap::TYPE_COLNAME, TableMap::TYPE_FIELDNAME, TableMap::TYPE_NUM.
     *
     * @return int             next starting column
     * @throws PropelException - Any caught Exception will be rewrapped as a PropelException.
     */
    public function hydrate($row, $startcol = 0, $rehydrate = false, $indexType = TableMap::TYPE_NUM)
    {
        try {

            $col = $row[TableMap::TYPE_NUM == $indexType ? 0 + $startcol : ServicerenduTableMap::translateFieldName('IdServicerendu', TableMap::TYPE_PHPNAME, $indexType)];
            $this->id_servicerendu = (null !== $col) ? (string) $col : null;

            $col = $row[TableMap::TYPE_NUM == $indexType ? 1 + $startcol : ServicerenduTableMap::translateFieldName('IdMembre', TableMap::TYPE_PHPNAME, $indexType)];
            $this->id_membre = (null !== $col) ? (string) $col : null;

            $col = $row[TableMap::TYPE_NUM == $indexType ? 2 + $startcol : ServicerenduTableMap::translateFieldName('Montant', TableMap::TYPE_PHPNAME, $indexType)];
            $this->montant = (null !== $col) ? (double) $col : null;

            $col = $row[TableMap::TYPE_NUM == $indexType ? 3 + $startcol : ServicerenduTableMap::translateFieldName('DateEnregistrement', TableMap::TYPE_PHPNAME, $indexType)];
            if ($col === '0000-00-00') {
                $col = null;
            }
            $this->date_enregistrement = (null !== $col) ? PropelDateTime::newInstance($col, null, 'DateTime') : null;

            $col = $row[TableMap::TYPE_NUM == $indexType ? 4 + $startcol : ServicerenduTableMap::translateFieldName('DateDebut', TableMap::TYPE_PHPNAME, $indexType)];
            if ($col === '0000-00-00') {
                $col = null;
            }
            $this->date_debut = (null !== $col) ? PropelDateTime::newInstance($col, null, 'DateTime') : null;

            $col = $row[TableMap::TYPE_NUM == $indexType ? 5 + $startcol : ServicerenduTableMap::translateFieldName('DateFin', TableMap::TYPE_PHPNAME, $indexType)];
            if ($col === '0000-00-00') {
                $col = null;
            }
            $this->date_fin = (null !== $col) ? PropelDateTime::newInstance($col, null, 'DateTime') : null;

            $col = $row[TableMap::TYPE_NUM == $indexType ? 6 + $startcol : ServicerenduTableMap::translateFieldName('Origine', TableMap::TYPE_PHPNAME, $indexType)];
            $this->origine = (null !== $col) ? (int) $col : null;

            $col = $row[TableMap::TYPE_NUM == $indexType ? 7 + $startcol : ServicerenduTableMap::translateFieldName('Premier', TableMap::TYPE_PHPNAME, $indexType)];
            $this->premier = (null !== $col) ? (int) $col : null;

            $col = $row[TableMap::TYPE_NUM == $indexType ? 8 + $startcol : ServicerenduTableMap::translateFieldName('Dernier', TableMap::TYPE_PHPNAME, $indexType)];
            $this->dernier = (null !== $col) ? (int) $col : null;

            $col = $row[TableMap::TYPE_NUM == $indexType ? 9 + $startcol : ServicerenduTableMap::translateFieldName('IdEntite', TableMap::TYPE_PHPNAME, $indexType)];
            $this->id_entite = (null !== $col) ? (int) $col : null;

            $col = $row[TableMap::TYPE_NUM == $indexType ? 10 + $startcol : ServicerenduTableMap::translateFieldName('IdPrestation', TableMap::TYPE_PHPNAME, $indexType)];
            $this->id_prestation = (null !== $col) ? (string) $col : null;

            $col = $row[TableMap::TYPE_NUM == $indexType ? 11 + $startcol : ServicerenduTableMap::translateFieldName('Regle', TableMap::TYPE_PHPNAME, $indexType)];
            $this->regle = (null !== $col) ? (int) $col : null;

            $col = $row[TableMap::TYPE_NUM == $indexType ? 12 + $startcol : ServicerenduTableMap::translateFieldName('Comptabilise', TableMap::TYPE_PHPNAME, $indexType)];
            $this->comptabilise = (null !== $col) ? (boolean) $col : null;

            $col = $row[TableMap::TYPE_NUM == $indexType ? 13 + $startcol : ServicerenduTableMap::translateFieldName('Observation', TableMap::TYPE_PHPNAME, $indexType)];
            $this->observation = (null !== $col) ? (string) $col : null;

            $col = $row[TableMap::TYPE_NUM == $indexType ? 14 + $startcol : ServicerenduTableMap::translateFieldName('IdTva', TableMap::TYPE_PHPNAME, $indexType)];
            $this->id_tva = (null !== $col) ? (string) $col : null;

            $col = $row[TableMap::TYPE_NUM == $indexType ? 15 + $startcol : ServicerenduTableMap::translateFieldName('Quantite', TableMap::TYPE_PHPNAME, $indexType)];
            $this->quantite = (null !== $col) ? (double) $col : null;

            $col = $row[TableMap::TYPE_NUM == $indexType ? 16 + $startcol : ServicerenduTableMap::translateFieldName('CreatedAt', TableMap::TYPE_PHPNAME, $indexType)];
            if ($col === '0000-00-00 00:00:00') {
                $col = null;
            }
            $this->created_at = (null !== $col) ? PropelDateTime::newInstance($col, null, 'DateTime') : null;

            $col = $row[TableMap::TYPE_NUM == $indexType ? 17 + $startcol : ServicerenduTableMap::translateFieldName('UpdatedAt', TableMap::TYPE_PHPNAME, $indexType)];
            if ($col === '0000-00-00 00:00:00') {
                $col = null;
            }
            $this->updated_at = (null !== $col) ? PropelDateTime::newInstance($col, null, 'DateTime') : null;
            $this->resetModified();

            $this->setNew(false);

            if ($rehydrate) {
                $this->ensureConsistency();
            }

            return $startcol + 18; // 18 = ServicerenduTableMap::NUM_HYDRATE_COLUMNS.

        } catch (Exception $e) {
            throw new PropelException(sprintf('Error populating %s object', '\\Servicerendu'), 0, $e);
        }
    }

    /**
     * Checks and repairs the internal consistency of the object.
     *
     * This method is executed after an already-instantiated object is re-hydrated
     * from the database.  It exists to check any foreign keys to make sure that
     * the objects related to the current object are correct based on foreign key.
     *
     * You can override this method in the stub class, but you should always invoke
     * the base method from the overridden method (i.e. parent::ensureConsistency()),
     * in case your model changes.
     *
     * @throws PropelException
     */
    public function ensureConsistency()
    {
        if ($this->aMembre !== null && $this->id_membre !== $this->aMembre->getIdMembre()) {
            $this->aMembre = null;
        }
        if ($this->aServicerenduRelatedByOrigine !== null && $this->origine !== $this->aServicerenduRelatedByOrigine->getIdServicerendu()) {
            $this->aServicerenduRelatedByOrigine = null;
        }
        if ($this->aEntite !== null && $this->id_entite !== $this->aEntite->getIdEntite()) {
            $this->aEntite = null;
        }
        if ($this->aPrestation !== null && $this->id_prestation !== $this->aPrestation->getIdPrestation()) {
            $this->aPrestation = null;
        }
    } // ensureConsistency

    /**
     * Reloads this object from datastore based on primary key and (optionally) resets all associated objects.
     *
     * This will only work if the object has been saved and has a valid primary key set.
     *
     * @param      boolean $deep (optional) Whether to also de-associated any related objects.
     * @param      ConnectionInterface $con (optional) The ConnectionInterface connection to use.
     * @return void
     * @throws PropelException - if this object is deleted, unsaved or doesn't have pk match in db
     */
    public function reload($deep = false, ConnectionInterface $con = null)
    {
        if ($this->isDeleted()) {
            throw new PropelException("Cannot reload a deleted object.");
        }

        if ($this->isNew()) {
            throw new PropelException("Cannot reload an unsaved object.");
        }

        if ($con === null) {
            $con = Propel::getServiceContainer()->getReadConnection(ServicerenduTableMap::DATABASE_NAME);
        }

        // We don't need to alter the object instance pool; we're just modifying this instance
        // already in the pool.

        $dataFetcher = ChildServicerenduQuery::create(null, $this->buildPkeyCriteria())->setFormatter(ModelCriteria::FORMAT_STATEMENT)->find($con);
        $row = $dataFetcher->fetch();
        $dataFetcher->close();
        if (!$row) {
            throw new PropelException('Cannot find matching row in the database to reload object values.');
        }
        $this->hydrate($row, 0, true, $dataFetcher->getIndexType()); // rehydrate

        if ($deep) {  // also de-associate any related objects?

            $this->aPrestation = null;
            $this->aEntite = null;
            $this->aMembre = null;
            $this->aServicerenduRelatedByOrigine = null;
            $this->collServicepaiements = null;

            $this->collServicerendusRelatedByIdServicerendu = null;

            $this->collServicerenduIndividus = null;

            $this->collIndividus = null;
        } // if (deep)
    }

    /**
     * Removes this object from datastore and sets delete attribute.
     *
     * @param      ConnectionInterface $con
     * @return void
     * @throws PropelException
     * @see Servicerendu::setDeleted()
     * @see Servicerendu::isDeleted()
     */
    public function delete(ConnectionInterface $con = null)
    {
        if ($this->isDeleted()) {
            throw new PropelException("This object has already been deleted.");
        }

        if ($con === null) {
            $con = Propel::getServiceContainer()->getWriteConnection(ServicerenduTableMap::DATABASE_NAME);
        }

        $con->transaction(function () use ($con) {
            $deleteQuery = ChildServicerenduQuery::create()
                ->filterByPrimaryKey($this->getPrimaryKey());
            $ret = $this->preDelete($con);
            // archivable behavior
            if ($ret) {
                if ($this->archiveOnDelete) {
                    // do nothing yet. The object will be archived later when calling ChildServicerenduQuery::delete().
                } else {
                    $deleteQuery->setArchiveOnDelete(false);
                    $this->archiveOnDelete = true;
                }
            }

            if ($ret) {
                $deleteQuery->delete($con);
                $this->postDelete($con);
                $this->setDeleted(true);
            }
        });
    }

    /**
     * Persists this object to the database.
     *
     * If the object is new, it inserts it; otherwise an update is performed.
     * All modified related objects will also be persisted in the doSave()
     * method.  This method wraps all precipitate database operations in a
     * single transaction.
     *
     * @param      ConnectionInterface $con
     * @return int             The number of rows affected by this insert/update and any referring fk objects' save() operations.
     * @throws PropelException
     * @see doSave()
     */
    public function save(ConnectionInterface $con = null)
    {
        if ($this->isDeleted()) {
            throw new PropelException("You cannot save an object that has been deleted.");
        }

        if ($this->alreadyInSave) {
            return 0;
        }

        if ($con === null) {
            $con = Propel::getServiceContainer()->getWriteConnection(ServicerenduTableMap::DATABASE_NAME);
        }

        return $con->transaction(function () use ($con) {
            $ret = $this->preSave($con);
            $isInsert = $this->isNew();
            if ($isInsert) {
                $ret = $ret && $this->preInsert($con);
                // timestampable behavior

                if (!$this->isColumnModified(ServicerenduTableMap::COL_CREATED_AT)) {
                    $this->setCreatedAt(\Propel\Runtime\Util\PropelDateTime::createHighPrecision());
                }
                if (!$this->isColumnModified(ServicerenduTableMap::COL_UPDATED_AT)) {
                    $this->setUpdatedAt(\Propel\Runtime\Util\PropelDateTime::createHighPrecision());
                }
            } else {
                $ret = $ret && $this->preUpdate($con);
                // timestampable behavior
                if ($this->isModified() && !$this->isColumnModified(ServicerenduTableMap::COL_UPDATED_AT)) {
                    $this->setUpdatedAt(\Propel\Runtime\Util\PropelDateTime::createHighPrecision());
                }
            }
            if ($ret) {
                $affectedRows = $this->doSave($con);
                if ($isInsert) {
                    $this->postInsert($con);
                } else {
                    $this->postUpdate($con);
                }
                $this->postSave($con);
                ServicerenduTableMap::addInstanceToPool($this);
            } else {
                $affectedRows = 0;
            }

            return $affectedRows;
        });
    }

    /**
     * Performs the work of inserting or updating the row in the database.
     *
     * If the object is new, it inserts it; otherwise an update is performed.
     * All related objects are also updated in this method.
     *
     * @param      ConnectionInterface $con
     * @return int             The number of rows affected by this insert/update and any referring fk objects' save() operations.
     * @throws PropelException
     * @see save()
     */
    protected function doSave(ConnectionInterface $con)
    {
        $affectedRows = 0; // initialize var to track total num of affected rows
        if (!$this->alreadyInSave) {
            $this->alreadyInSave = true;

            // We call the save method on the following object(s) if they
            // were passed to this object by their corresponding set
            // method.  This object relates to these object(s) by a
            // foreign key reference.

            if ($this->aPrestation !== null) {
                if ($this->aPrestation->isModified() || $this->aPrestation->isNew()) {
                    $affectedRows += $this->aPrestation->save($con);
                }
                $this->setPrestation($this->aPrestation);
            }

            if ($this->aEntite !== null) {
                if ($this->aEntite->isModified() || $this->aEntite->isNew()) {
                    $affectedRows += $this->aEntite->save($con);
                }
                $this->setEntite($this->aEntite);
            }

            if ($this->aMembre !== null) {
                if ($this->aMembre->isModified() || $this->aMembre->isNew()) {
                    $affectedRows += $this->aMembre->save($con);
                }
                $this->setMembre($this->aMembre);
            }

            if ($this->aServicerenduRelatedByOrigine !== null) {
                if ($this->aServicerenduRelatedByOrigine->isModified() || $this->aServicerenduRelatedByOrigine->isNew()) {
                    $affectedRows += $this->aServicerenduRelatedByOrigine->save($con);
                }
                $this->setServicerenduRelatedByOrigine($this->aServicerenduRelatedByOrigine);
            }

            if ($this->isNew() || $this->isModified()) {
                // persist changes
                if ($this->isNew()) {
                    $this->doInsert($con);
                    $affectedRows += 1;
                } else {
                    $affectedRows += $this->doUpdate($con);
                }
                $this->resetModified();
            }

            if ($this->individusScheduledForDeletion !== null) {
                if (!$this->individusScheduledForDeletion->isEmpty()) {
                    $pks = array();
                    foreach ($this->individusScheduledForDeletion as $entry) {
                        $entryPk = [];

                        $entryPk[1] = $this->getIdServicerendu();
                        $entryPk[0] = $entry->getIdIndividu();
                        $pks[] = $entryPk;
                    }

                    \ServicerenduIndividuQuery::create()
                        ->filterByPrimaryKeys($pks)
                        ->delete($con);

                    $this->individusScheduledForDeletion = null;
                }

            }

            if ($this->collIndividus) {
                foreach ($this->collIndividus as $individu) {
                    if (!$individu->isDeleted() && ($individu->isNew() || $individu->isModified())) {
                        $individu->save($con);
                    }
                }
            }


            if ($this->servicepaiementsScheduledForDeletion !== null) {
                if (!$this->servicepaiementsScheduledForDeletion->isEmpty()) {
                    \ServicepaiementQuery::create()
                        ->filterByPrimaryKeys($this->servicepaiementsScheduledForDeletion->getPrimaryKeys(false))
                        ->delete($con);
                    $this->servicepaiementsScheduledForDeletion = null;
                }
            }

            if ($this->collServicepaiements !== null) {
                foreach ($this->collServicepaiements as $referrerFK) {
                    if (!$referrerFK->isDeleted() && ($referrerFK->isNew() || $referrerFK->isModified())) {
                        $affectedRows += $referrerFK->save($con);
                    }
                }
            }

            if ($this->servicerendusRelatedByIdServicerenduScheduledForDeletion !== null) {
                if (!$this->servicerendusRelatedByIdServicerenduScheduledForDeletion->isEmpty()) {
                    \ServicerenduQuery::create()
                        ->filterByPrimaryKeys($this->servicerendusRelatedByIdServicerenduScheduledForDeletion->getPrimaryKeys(false))
                        ->delete($con);
                    $this->servicerendusRelatedByIdServicerenduScheduledForDeletion = null;
                }
            }

            if ($this->collServicerendusRelatedByIdServicerendu !== null) {
                foreach ($this->collServicerendusRelatedByIdServicerendu as $referrerFK) {
                    if (!$referrerFK->isDeleted() && ($referrerFK->isNew() || $referrerFK->isModified())) {
                        $affectedRows += $referrerFK->save($con);
                    }
                }
            }

            if ($this->servicerenduIndividusScheduledForDeletion !== null) {
                if (!$this->servicerenduIndividusScheduledForDeletion->isEmpty()) {
                    \ServicerenduIndividuQuery::create()
                        ->filterByPrimaryKeys($this->servicerenduIndividusScheduledForDeletion->getPrimaryKeys(false))
                        ->delete($con);
                    $this->servicerenduIndividusScheduledForDeletion = null;
                }
            }

            if ($this->collServicerenduIndividus !== null) {
                foreach ($this->collServicerenduIndividus as $referrerFK) {
                    if (!$referrerFK->isDeleted() && ($referrerFK->isNew() || $referrerFK->isModified())) {
                        $affectedRows += $referrerFK->save($con);
                    }
                }
            }

            $this->alreadyInSave = false;

        }

        return $affectedRows;
    } // doSave()

    /**
     * Insert the row in the database.
     *
     * @param      ConnectionInterface $con
     *
     * @throws PropelException
     * @see doSave()
     */
    protected function doInsert(ConnectionInterface $con)
    {
        $modifiedColumns = array();
        $index = 0;

        $this->modifiedColumns[ServicerenduTableMap::COL_ID_SERVICERENDU] = true;
        if (null !== $this->id_servicerendu) {
            throw new PropelException('Cannot insert a value for auto-increment primary key (' . ServicerenduTableMap::COL_ID_SERVICERENDU . ')');
        }

         // check the columns in natural order for more readable SQL queries
        if ($this->isColumnModified(ServicerenduTableMap::COL_ID_SERVICERENDU)) {
            $modifiedColumns[':p' . $index++]  = '`id_servicerendu`';
        }
        if ($this->isColumnModified(ServicerenduTableMap::COL_ID_MEMBRE)) {
            $modifiedColumns[':p' . $index++]  = '`id_membre`';
        }
        if ($this->isColumnModified(ServicerenduTableMap::COL_MONTANT)) {
            $modifiedColumns[':p' . $index++]  = '`montant`';
        }
        if ($this->isColumnModified(ServicerenduTableMap::COL_DATE_ENREGISTREMENT)) {
            $modifiedColumns[':p' . $index++]  = '`date_enregistrement`';
        }
        if ($this->isColumnModified(ServicerenduTableMap::COL_DATE_DEBUT)) {
            $modifiedColumns[':p' . $index++]  = '`date_debut`';
        }
        if ($this->isColumnModified(ServicerenduTableMap::COL_DATE_FIN)) {
            $modifiedColumns[':p' . $index++]  = '`date_fin`';
        }
        if ($this->isColumnModified(ServicerenduTableMap::COL_ORIGINE)) {
            $modifiedColumns[':p' . $index++]  = '`origine`';
        }
        if ($this->isColumnModified(ServicerenduTableMap::COL_PREMIER)) {
            $modifiedColumns[':p' . $index++]  = '`premier`';
        }
        if ($this->isColumnModified(ServicerenduTableMap::COL_DERNIER)) {
            $modifiedColumns[':p' . $index++]  = '`dernier`';
        }
        if ($this->isColumnModified(ServicerenduTableMap::COL_ID_ENTITE)) {
            $modifiedColumns[':p' . $index++]  = '`id_entite`';
        }
        if ($this->isColumnModified(ServicerenduTableMap::COL_ID_PRESTATION)) {
            $modifiedColumns[':p' . $index++]  = '`id_prestation`';
        }
        if ($this->isColumnModified(ServicerenduTableMap::COL_REGLE)) {
            $modifiedColumns[':p' . $index++]  = '`regle`';
        }
        if ($this->isColumnModified(ServicerenduTableMap::COL_COMPTABILISE)) {
            $modifiedColumns[':p' . $index++]  = '`comptabilise`';
        }
        if ($this->isColumnModified(ServicerenduTableMap::COL_OBSERVATION)) {
            $modifiedColumns[':p' . $index++]  = '`observation`';
        }
        if ($this->isColumnModified(ServicerenduTableMap::COL_ID_TVA)) {
            $modifiedColumns[':p' . $index++]  = '`id_tva`';
        }
        if ($this->isColumnModified(ServicerenduTableMap::COL_QUANTITE)) {
            $modifiedColumns[':p' . $index++]  = '`quantite`';
        }
        if ($this->isColumnModified(ServicerenduTableMap::COL_CREATED_AT)) {
            $modifiedColumns[':p' . $index++]  = '`created_at`';
        }
        if ($this->isColumnModified(ServicerenduTableMap::COL_UPDATED_AT)) {
            $modifiedColumns[':p' . $index++]  = '`updated_at`';
        }

        $sql = sprintf(
            'INSERT INTO `asso_servicerendus` (%s) VALUES (%s)',
            implode(', ', $modifiedColumns),
            implode(', ', array_keys($modifiedColumns))
        );

        try {
            $stmt = $con->prepare($sql);
            foreach ($modifiedColumns as $identifier => $columnName) {
                switch ($columnName) {
                    case '`id_servicerendu`':
                        $stmt->bindValue($identifier, $this->id_servicerendu, PDO::PARAM_INT);
                        break;
                    case '`id_membre`':
                        $stmt->bindValue($identifier, $this->id_membre, PDO::PARAM_INT);
                        break;
                    case '`montant`':
                        $stmt->bindValue($identifier, $this->montant, PDO::PARAM_STR);
                        break;
                    case '`date_enregistrement`':
                        $stmt->bindValue($identifier, $this->date_enregistrement ? $this->date_enregistrement->format("Y-m-d H:i:s.u") : null, PDO::PARAM_STR);
                        break;
                    case '`date_debut`':
                        $stmt->bindValue($identifier, $this->date_debut ? $this->date_debut->format("Y-m-d H:i:s.u") : null, PDO::PARAM_STR);
                        break;
                    case '`date_fin`':
                        $stmt->bindValue($identifier, $this->date_fin ? $this->date_fin->format("Y-m-d H:i:s.u") : null, PDO::PARAM_STR);
                        break;
                    case '`origine`':
                        $stmt->bindValue($identifier, $this->origine, PDO::PARAM_INT);
                        break;
                    case '`premier`':
                        $stmt->bindValue($identifier, $this->premier, PDO::PARAM_INT);
                        break;
                    case '`dernier`':
                        $stmt->bindValue($identifier, $this->dernier, PDO::PARAM_INT);
                        break;
                    case '`id_entite`':
                        $stmt->bindValue($identifier, $this->id_entite, PDO::PARAM_INT);
                        break;
                    case '`id_prestation`':
                        $stmt->bindValue($identifier, $this->id_prestation, PDO::PARAM_INT);
                        break;
                    case '`regle`':
                        $stmt->bindValue($identifier, $this->regle, PDO::PARAM_INT);
                        break;
                    case '`comptabilise`':
                        $stmt->bindValue($identifier, (int) $this->comptabilise, PDO::PARAM_INT);
                        break;
                    case '`observation`':
                        $stmt->bindValue($identifier, $this->observation, PDO::PARAM_STR);
                        break;
                    case '`id_tva`':
                        $stmt->bindValue($identifier, $this->id_tva, PDO::PARAM_INT);
                        break;
                    case '`quantite`':
                        $stmt->bindValue($identifier, $this->quantite, PDO::PARAM_STR);
                        break;
                    case '`created_at`':
                        $stmt->bindValue($identifier, $this->created_at ? $this->created_at->format("Y-m-d H:i:s.u") : null, PDO::PARAM_STR);
                        break;
                    case '`updated_at`':
                        $stmt->bindValue($identifier, $this->updated_at ? $this->updated_at->format("Y-m-d H:i:s.u") : null, PDO::PARAM_STR);
                        break;
                }
            }
            $stmt->execute();
        } catch (Exception $e) {
            Propel::log($e->getMessage(), Propel::LOG_ERR);
            throw new PropelException(sprintf('Unable to execute INSERT statement [%s]', $sql), 0, $e);
        }

        try {
            $pk = $con->lastInsertId();
        } catch (Exception $e) {
            throw new PropelException('Unable to get autoincrement id.', 0, $e);
        }
        $this->setIdServicerendu($pk);

        $this->setNew(false);
    }

    /**
     * Update the row in the database.
     *
     * @param      ConnectionInterface $con
     *
     * @return Integer Number of updated rows
     * @see doSave()
     */
    protected function doUpdate(ConnectionInterface $con)
    {
        $selectCriteria = $this->buildPkeyCriteria();
        $valuesCriteria = $this->buildCriteria();

        return $selectCriteria->doUpdate($valuesCriteria, $con);
    }

    /**
     * Retrieves a field from the object by name passed in as a string.
     *
     * @param      string $name name
     * @param      string $type The type of fieldname the $name is of:
     *                     one of the class type constants TableMap::TYPE_PHPNAME, TableMap::TYPE_CAMELNAME
     *                     TableMap::TYPE_COLNAME, TableMap::TYPE_FIELDNAME, TableMap::TYPE_NUM.
     *                     Defaults to TableMap::TYPE_FIELDNAME.
     * @return mixed Value of field.
     */
    public function getByName($name, $type = TableMap::TYPE_FIELDNAME)
    {
        $pos = ServicerenduTableMap::translateFieldName($name, $type, TableMap::TYPE_NUM);
        $field = $this->getByPosition($pos);

        return $field;
    }

    /**
     * Retrieves a field from the object by Position as specified in the xml schema.
     * Zero-based.
     *
     * @param      int $pos position in xml schema
     * @return mixed Value of field at $pos
     */
    public function getByPosition($pos)
    {
        switch ($pos) {
            case 0:
                return $this->getIdServicerendu();
                break;
            case 1:
                return $this->getIdMembre();
                break;
            case 2:
                return $this->getMontant();
                break;
            case 3:
                return $this->getDateEnregistrement();
                break;
            case 4:
                return $this->getDateDebut();
                break;
            case 5:
                return $this->getDateFin();
                break;
            case 6:
                return $this->getOrigine();
                break;
            case 7:
                return $this->getPremier();
                break;
            case 8:
                return $this->getDernier();
                break;
            case 9:
                return $this->getIdEntite();
                break;
            case 10:
                return $this->getIdPrestation();
                break;
            case 11:
                return $this->getRegle();
                break;
            case 12:
                return $this->getComptabilise();
                break;
            case 13:
                return $this->getObservation();
                break;
            case 14:
                return $this->getIdTva();
                break;
            case 15:
                return $this->getQuantite();
                break;
            case 16:
                return $this->getCreatedAt();
                break;
            case 17:
                return $this->getUpdatedAt();
                break;
            default:
                return null;
                break;
        } // switch()
    }

    /**
     * Exports the object as an array.
     *
     * You can specify the key type of the array by passing one of the class
     * type constants.
     *
     * @param     string  $keyType (optional) One of the class type constants TableMap::TYPE_PHPNAME, TableMap::TYPE_CAMELNAME,
     *                    TableMap::TYPE_COLNAME, TableMap::TYPE_FIELDNAME, TableMap::TYPE_NUM.
     *                    Defaults to TableMap::TYPE_FIELDNAME.
     * @param     boolean $includeLazyLoadColumns (optional) Whether to include lazy loaded columns. Defaults to TRUE.
     * @param     array $alreadyDumpedObjects List of objects to skip to avoid recursion
     * @param     boolean $includeForeignObjects (optional) Whether to include hydrated related objects. Default to FALSE.
     *
     * @return array an associative array containing the field names (as keys) and field values
     */
    public function toArray($keyType = TableMap::TYPE_FIELDNAME, $includeLazyLoadColumns = true, $alreadyDumpedObjects = array(), $includeForeignObjects = false)
    {

        if (isset($alreadyDumpedObjects['Servicerendu'][$this->hashCode()])) {
            return '*RECURSION*';
        }
        $alreadyDumpedObjects['Servicerendu'][$this->hashCode()] = true;
        $keys = ServicerenduTableMap::getFieldNames($keyType);
        $result = array(
            $keys[0] => $this->getIdServicerendu(),
            $keys[1] => $this->getIdMembre(),
            $keys[2] => $this->getMontant(),
            $keys[3] => $this->getDateEnregistrement(),
            $keys[4] => $this->getDateDebut(),
            $keys[5] => $this->getDateFin(),
            $keys[6] => $this->getOrigine(),
            $keys[7] => $this->getPremier(),
            $keys[8] => $this->getDernier(),
            $keys[9] => $this->getIdEntite(),
            $keys[10] => $this->getIdPrestation(),
            $keys[11] => $this->getRegle(),
            $keys[12] => $this->getComptabilise(),
            $keys[13] => $this->getObservation(),
            $keys[14] => $this->getIdTva(),
            $keys[15] => $this->getQuantite(),
            $keys[16] => $this->getCreatedAt(),
            $keys[17] => $this->getUpdatedAt(),
        );
        if ($result[$keys[3]] instanceof \DateTimeInterface) {
            $result[$keys[3]] = $result[$keys[3]]->format('c');
        }

        if ($result[$keys[4]] instanceof \DateTimeInterface) {
            $result[$keys[4]] = $result[$keys[4]]->format('c');
        }

        if ($result[$keys[5]] instanceof \DateTimeInterface) {
            $result[$keys[5]] = $result[$keys[5]]->format('c');
        }

        if ($result[$keys[16]] instanceof \DateTimeInterface) {
            $result[$keys[16]] = $result[$keys[16]]->format('c');
        }

        if ($result[$keys[17]] instanceof \DateTimeInterface) {
            $result[$keys[17]] = $result[$keys[17]]->format('c');
        }

        $virtualColumns = $this->virtualColumns;
        foreach ($virtualColumns as $key => $virtualColumn) {
            $result[$key] = $virtualColumn;
        }

        if ($includeForeignObjects) {
            if (null !== $this->aPrestation) {

                switch ($keyType) {
                    case TableMap::TYPE_CAMELNAME:
                        $key = 'prestation';
                        break;
                    case TableMap::TYPE_FIELDNAME:
                        $key = 'asso_prestations';
                        break;
                    default:
                        $key = 'Prestation';
                }

                $result[$key] = $this->aPrestation->toArray($keyType, $includeLazyLoadColumns,  $alreadyDumpedObjects, true);
            }
            if (null !== $this->aEntite) {

                switch ($keyType) {
                    case TableMap::TYPE_CAMELNAME:
                        $key = 'entite';
                        break;
                    case TableMap::TYPE_FIELDNAME:
                        $key = 'asso_entites';
                        break;
                    default:
                        $key = 'Entite';
                }

                $result[$key] = $this->aEntite->toArray($keyType, $includeLazyLoadColumns,  $alreadyDumpedObjects, true);
            }
            if (null !== $this->aMembre) {

                switch ($keyType) {
                    case TableMap::TYPE_CAMELNAME:
                        $key = 'membre';
                        break;
                    case TableMap::TYPE_FIELDNAME:
                        $key = 'asso_membres';
                        break;
                    default:
                        $key = 'Membre';
                }

                $result[$key] = $this->aMembre->toArray($keyType, $includeLazyLoadColumns,  $alreadyDumpedObjects, true);
            }
            if (null !== $this->aServicerenduRelatedByOrigine) {

                switch ($keyType) {
                    case TableMap::TYPE_CAMELNAME:
                        $key = 'servicerendu';
                        break;
                    case TableMap::TYPE_FIELDNAME:
                        $key = 'asso_servicerendus';
                        break;
                    default:
                        $key = 'Servicerendu';
                }

                $result[$key] = $this->aServicerenduRelatedByOrigine->toArray($keyType, $includeLazyLoadColumns,  $alreadyDumpedObjects, true);
            }
            if (null !== $this->collServicepaiements) {

                switch ($keyType) {
                    case TableMap::TYPE_CAMELNAME:
                        $key = 'servicepaiements';
                        break;
                    case TableMap::TYPE_FIELDNAME:
                        $key = 'asso_servicepaiementss';
                        break;
                    default:
                        $key = 'Servicepaiements';
                }

                $result[$key] = $this->collServicepaiements->toArray(null, false, $keyType, $includeLazyLoadColumns, $alreadyDumpedObjects);
            }
            if (null !== $this->collServicerendusRelatedByIdServicerendu) {

                switch ($keyType) {
                    case TableMap::TYPE_CAMELNAME:
                        $key = 'servicerendus';
                        break;
                    case TableMap::TYPE_FIELDNAME:
                        $key = 'asso_servicerenduses';
                        break;
                    default:
                        $key = 'Servicerendus';
                }

                $result[$key] = $this->collServicerendusRelatedByIdServicerendu->toArray(null, false, $keyType, $includeLazyLoadColumns, $alreadyDumpedObjects);
            }
            if (null !== $this->collServicerenduIndividus) {

                switch ($keyType) {
                    case TableMap::TYPE_CAMELNAME:
                        $key = 'servicerenduIndividus';
                        break;
                    case TableMap::TYPE_FIELDNAME:
                        $key = 'asso_servicerendus_individuses';
                        break;
                    default:
                        $key = 'ServicerenduIndividus';
                }

                $result[$key] = $this->collServicerenduIndividus->toArray(null, false, $keyType, $includeLazyLoadColumns, $alreadyDumpedObjects);
            }
        }

        return $result;
    }

    /**
     * Sets a field from the object by name passed in as a string.
     *
     * @param  string $name
     * @param  mixed  $value field value
     * @param  string $type The type of fieldname the $name is of:
     *                one of the class type constants TableMap::TYPE_PHPNAME, TableMap::TYPE_CAMELNAME
     *                TableMap::TYPE_COLNAME, TableMap::TYPE_FIELDNAME, TableMap::TYPE_NUM.
     *                Defaults to TableMap::TYPE_FIELDNAME.
     * @return $this|\Servicerendu
     */
    public function setByName($name, $value, $type = TableMap::TYPE_FIELDNAME)
    {
        $pos = ServicerenduTableMap::translateFieldName($name, $type, TableMap::TYPE_NUM);

        return $this->setByPosition($pos, $value);
    }

    /**
     * Sets a field from the object by Position as specified in the xml schema.
     * Zero-based.
     *
     * @param  int $pos position in xml schema
     * @param  mixed $value field value
     * @return $this|\Servicerendu
     */
    public function setByPosition($pos, $value)
    {
        switch ($pos) {
            case 0:
                $this->setIdServicerendu($value);
                break;
            case 1:
                $this->setIdMembre($value);
                break;
            case 2:
                $this->setMontant($value);
                break;
            case 3:
                $this->setDateEnregistrement($value);
                break;
            case 4:
                $this->setDateDebut($value);
                break;
            case 5:
                $this->setDateFin($value);
                break;
            case 6:
                $this->setOrigine($value);
                break;
            case 7:
                $this->setPremier($value);
                break;
            case 8:
                $this->setDernier($value);
                break;
            case 9:
                $this->setIdEntite($value);
                break;
            case 10:
                $this->setIdPrestation($value);
                break;
            case 11:
                $this->setRegle($value);
                break;
            case 12:
                $this->setComptabilise($value);
                break;
            case 13:
                $this->setObservation($value);
                break;
            case 14:
                $this->setIdTva($value);
                break;
            case 15:
                $this->setQuantite($value);
                break;
            case 16:
                $this->setCreatedAt($value);
                break;
            case 17:
                $this->setUpdatedAt($value);
                break;
        } // switch()

        return $this;
    }

    /**
     * Populates the object using an array.
     *
     * This is particularly useful when populating an object from one of the
     * request arrays (e.g. $_POST).  This method goes through the column
     * names, checking to see whether a matching key exists in populated
     * array. If so the setByName() method is called for that column.
     *
     * You can specify the key type of the array by additionally passing one
     * of the class type constants TableMap::TYPE_PHPNAME, TableMap::TYPE_CAMELNAME,
     * TableMap::TYPE_COLNAME, TableMap::TYPE_FIELDNAME, TableMap::TYPE_NUM.
     * The default key type is the column's TableMap::TYPE_FIELDNAME.
     *
     * @param      array  $arr     An array to populate the object from.
     * @param      string $keyType The type of keys the array uses.
     * @return void
     */
    public function fromArray($arr, $keyType = TableMap::TYPE_FIELDNAME)
    {
        $keys = ServicerenduTableMap::getFieldNames($keyType);

        if (array_key_exists($keys[0], $arr)) {
            $this->setIdServicerendu($arr[$keys[0]]);
        }
        if (array_key_exists($keys[1], $arr)) {
            $this->setIdMembre($arr[$keys[1]]);
        }
        if (array_key_exists($keys[2], $arr)) {
            $this->setMontant($arr[$keys[2]]);
        }
        if (array_key_exists($keys[3], $arr)) {
            $this->setDateEnregistrement($arr[$keys[3]]);
        }
        if (array_key_exists($keys[4], $arr)) {
            $this->setDateDebut($arr[$keys[4]]);
        }
        if (array_key_exists($keys[5], $arr)) {
            $this->setDateFin($arr[$keys[5]]);
        }
        if (array_key_exists($keys[6], $arr)) {
            $this->setOrigine($arr[$keys[6]]);
        }
        if (array_key_exists($keys[7], $arr)) {
            $this->setPremier($arr[$keys[7]]);
        }
        if (array_key_exists($keys[8], $arr)) {
            $this->setDernier($arr[$keys[8]]);
        }
        if (array_key_exists($keys[9], $arr)) {
            $this->setIdEntite($arr[$keys[9]]);
        }
        if (array_key_exists($keys[10], $arr)) {
            $this->setIdPrestation($arr[$keys[10]]);
        }
        if (array_key_exists($keys[11], $arr)) {
            $this->setRegle($arr[$keys[11]]);
        }
        if (array_key_exists($keys[12], $arr)) {
            $this->setComptabilise($arr[$keys[12]]);
        }
        if (array_key_exists($keys[13], $arr)) {
            $this->setObservation($arr[$keys[13]]);
        }
        if (array_key_exists($keys[14], $arr)) {
            $this->setIdTva($arr[$keys[14]]);
        }
        if (array_key_exists($keys[15], $arr)) {
            $this->setQuantite($arr[$keys[15]]);
        }
        if (array_key_exists($keys[16], $arr)) {
            $this->setCreatedAt($arr[$keys[16]]);
        }
        if (array_key_exists($keys[17], $arr)) {
            $this->setUpdatedAt($arr[$keys[17]]);
        }
    }

     /**
     * Populate the current object from a string, using a given parser format
     * <code>
     * $book = new Book();
     * $book->importFrom('JSON', '{"Id":9012,"Title":"Don Juan","ISBN":"0140422161","Price":12.99,"PublisherId":1234,"AuthorId":5678}');
     * </code>
     *
     * You can specify the key type of the array by additionally passing one
     * of the class type constants TableMap::TYPE_PHPNAME, TableMap::TYPE_CAMELNAME,
     * TableMap::TYPE_COLNAME, TableMap::TYPE_FIELDNAME, TableMap::TYPE_NUM.
     * The default key type is the column's TableMap::TYPE_FIELDNAME.
     *
     * @param mixed $parser A AbstractParser instance,
     *                       or a format name ('XML', 'YAML', 'JSON', 'CSV')
     * @param string $data The source data to import from
     * @param string $keyType The type of keys the array uses.
     *
     * @return $this|\Servicerendu The current object, for fluid interface
     */
    public function importFrom($parser, $data, $keyType = TableMap::TYPE_FIELDNAME)
    {
        if (!$parser instanceof AbstractParser) {
            $parser = AbstractParser::getParser($parser);
        }

        $this->fromArray($parser->toArray($data), $keyType);

        return $this;
    }

    /**
     * Build a Criteria object containing the values of all modified columns in this object.
     *
     * @return Criteria The Criteria object containing all modified values.
     */
    public function buildCriteria()
    {
        $criteria = new Criteria(ServicerenduTableMap::DATABASE_NAME);

        if ($this->isColumnModified(ServicerenduTableMap::COL_ID_SERVICERENDU)) {
            $criteria->add(ServicerenduTableMap::COL_ID_SERVICERENDU, $this->id_servicerendu);
        }
        if ($this->isColumnModified(ServicerenduTableMap::COL_ID_MEMBRE)) {
            $criteria->add(ServicerenduTableMap::COL_ID_MEMBRE, $this->id_membre);
        }
        if ($this->isColumnModified(ServicerenduTableMap::COL_MONTANT)) {
            $criteria->add(ServicerenduTableMap::COL_MONTANT, $this->montant);
        }
        if ($this->isColumnModified(ServicerenduTableMap::COL_DATE_ENREGISTREMENT)) {
            $criteria->add(ServicerenduTableMap::COL_DATE_ENREGISTREMENT, $this->date_enregistrement);
        }
        if ($this->isColumnModified(ServicerenduTableMap::COL_DATE_DEBUT)) {
            $criteria->add(ServicerenduTableMap::COL_DATE_DEBUT, $this->date_debut);
        }
        if ($this->isColumnModified(ServicerenduTableMap::COL_DATE_FIN)) {
            $criteria->add(ServicerenduTableMap::COL_DATE_FIN, $this->date_fin);
        }
        if ($this->isColumnModified(ServicerenduTableMap::COL_ORIGINE)) {
            $criteria->add(ServicerenduTableMap::COL_ORIGINE, $this->origine);
        }
        if ($this->isColumnModified(ServicerenduTableMap::COL_PREMIER)) {
            $criteria->add(ServicerenduTableMap::COL_PREMIER, $this->premier);
        }
        if ($this->isColumnModified(ServicerenduTableMap::COL_DERNIER)) {
            $criteria->add(ServicerenduTableMap::COL_DERNIER, $this->dernier);
        }
        if ($this->isColumnModified(ServicerenduTableMap::COL_ID_ENTITE)) {
            $criteria->add(ServicerenduTableMap::COL_ID_ENTITE, $this->id_entite);
        }
        if ($this->isColumnModified(ServicerenduTableMap::COL_ID_PRESTATION)) {
            $criteria->add(ServicerenduTableMap::COL_ID_PRESTATION, $this->id_prestation);
        }
        if ($this->isColumnModified(ServicerenduTableMap::COL_REGLE)) {
            $criteria->add(ServicerenduTableMap::COL_REGLE, $this->regle);
        }
        if ($this->isColumnModified(ServicerenduTableMap::COL_COMPTABILISE)) {
            $criteria->add(ServicerenduTableMap::COL_COMPTABILISE, $this->comptabilise);
        }
        if ($this->isColumnModified(ServicerenduTableMap::COL_OBSERVATION)) {
            $criteria->add(ServicerenduTableMap::COL_OBSERVATION, $this->observation);
        }
        if ($this->isColumnModified(ServicerenduTableMap::COL_ID_TVA)) {
            $criteria->add(ServicerenduTableMap::COL_ID_TVA, $this->id_tva);
        }
        if ($this->isColumnModified(ServicerenduTableMap::COL_QUANTITE)) {
            $criteria->add(ServicerenduTableMap::COL_QUANTITE, $this->quantite);
        }
        if ($this->isColumnModified(ServicerenduTableMap::COL_CREATED_AT)) {
            $criteria->add(ServicerenduTableMap::COL_CREATED_AT, $this->created_at);
        }
        if ($this->isColumnModified(ServicerenduTableMap::COL_UPDATED_AT)) {
            $criteria->add(ServicerenduTableMap::COL_UPDATED_AT, $this->updated_at);
        }

        return $criteria;
    }

    /**
     * Builds a Criteria object containing the primary key for this object.
     *
     * Unlike buildCriteria() this method includes the primary key values regardless
     * of whether or not they have been modified.
     *
     * @throws LogicException if no primary key is defined
     *
     * @return Criteria The Criteria object containing value(s) for primary key(s).
     */
    public function buildPkeyCriteria()
    {
        $criteria = ChildServicerenduQuery::create();
        $criteria->add(ServicerenduTableMap::COL_ID_SERVICERENDU, $this->id_servicerendu);

        return $criteria;
    }

    /**
     * If the primary key is not null, return the hashcode of the
     * primary key. Otherwise, return the hash code of the object.
     *
     * @return int Hashcode
     */
    public function hashCode()
    {
        $validPk = null !== $this->getIdServicerendu();

        $validPrimaryKeyFKs = 0;
        $primaryKeyFKs = [];

        if ($validPk) {
            return crc32(json_encode($this->getPrimaryKey(), JSON_UNESCAPED_UNICODE));
        } elseif ($validPrimaryKeyFKs) {
            return crc32(json_encode($primaryKeyFKs, JSON_UNESCAPED_UNICODE));
        }

        return spl_object_hash($this);
    }

    /**
     * Returns the primary key for this object (row).
     * @return string
     */
    public function getPrimaryKey()
    {
        return $this->getIdServicerendu();
    }

    /**
     * Generic method to set the primary key (id_servicerendu column).
     *
     * @param       string $key Primary key.
     * @return void
     */
    public function setPrimaryKey($key)
    {
        $this->setIdServicerendu($key);
    }

    /**
     * Returns true if the primary key for this object is null.
     * @return boolean
     */
    public function isPrimaryKeyNull()
    {
        return null === $this->getIdServicerendu();
    }

    /**
     * Sets contents of passed object to values from current object.
     *
     * If desired, this method can also make copies of all associated (fkey referrers)
     * objects.
     *
     * @param      object $copyObj An object of \Servicerendu (or compatible) type.
     * @param      boolean $deepCopy Whether to also copy all rows that refer (by fkey) to the current row.
     * @param      boolean $makeNew Whether to reset autoincrement PKs and make the object new.
     * @throws PropelException
     */
    public function copyInto($copyObj, $deepCopy = false, $makeNew = true)
    {
        $copyObj->setIdMembre($this->getIdMembre());
        $copyObj->setMontant($this->getMontant());
        $copyObj->setDateEnregistrement($this->getDateEnregistrement());
        $copyObj->setDateDebut($this->getDateDebut());
        $copyObj->setDateFin($this->getDateFin());
        $copyObj->setOrigine($this->getOrigine());
        $copyObj->setPremier($this->getPremier());
        $copyObj->setDernier($this->getDernier());
        $copyObj->setIdEntite($this->getIdEntite());
        $copyObj->setIdPrestation($this->getIdPrestation());
        $copyObj->setRegle($this->getRegle());
        $copyObj->setComptabilise($this->getComptabilise());
        $copyObj->setObservation($this->getObservation());
        $copyObj->setIdTva($this->getIdTva());
        $copyObj->setQuantite($this->getQuantite());
        $copyObj->setCreatedAt($this->getCreatedAt());
        $copyObj->setUpdatedAt($this->getUpdatedAt());

        if ($deepCopy) {
            // important: temporarily setNew(false) because this affects the behavior of
            // the getter/setter methods for fkey referrer objects.
            $copyObj->setNew(false);

            foreach ($this->getServicepaiements() as $relObj) {
                if ($relObj !== $this) {  // ensure that we don't try to copy a reference to ourselves
                    $copyObj->addServicepaiement($relObj->copy($deepCopy));
                }
            }

            foreach ($this->getServicerendusRelatedByIdServicerendu() as $relObj) {
                if ($relObj !== $this) {  // ensure that we don't try to copy a reference to ourselves
                    $copyObj->addServicerenduRelatedByIdServicerendu($relObj->copy($deepCopy));
                }
            }

            foreach ($this->getServicerenduIndividus() as $relObj) {
                if ($relObj !== $this) {  // ensure that we don't try to copy a reference to ourselves
                    $copyObj->addServicerenduIndividu($relObj->copy($deepCopy));
                }
            }

        } // if ($deepCopy)

        if ($makeNew) {
            $copyObj->setNew(true);
            $copyObj->setIdServicerendu(NULL); // this is a auto-increment column, so set to default value
        }
    }

    /**
     * Makes a copy of this object that will be inserted as a new row in table when saved.
     * It creates a new object filling in the simple attributes, but skipping any primary
     * keys that are defined for the table.
     *
     * If desired, this method can also make copies of all associated (fkey referrers)
     * objects.
     *
     * @param  boolean $deepCopy Whether to also copy all rows that refer (by fkey) to the current row.
     * @return \Servicerendu Clone of current object.
     * @throws PropelException
     */
    public function copy($deepCopy = false)
    {
        // we use get_class(), because this might be a subclass
        $clazz = get_class($this);
        $copyObj = new $clazz();
        $this->copyInto($copyObj, $deepCopy);

        return $copyObj;
    }

    /**
     * Declares an association between this object and a ChildPrestation object.
     *
     * @param  ChildPrestation $v
     * @return $this|\Servicerendu The current object (for fluent API support)
     * @throws PropelException
     */
    public function setPrestation(ChildPrestation $v = null)
    {
        if ($v === null) {
            $this->setIdPrestation(NULL);
        } else {
            $this->setIdPrestation($v->getIdPrestation());
        }

        $this->aPrestation = $v;

        // Add binding for other direction of this n:n relationship.
        // If this object has already been added to the ChildPrestation object, it will not be re-added.
        if ($v !== null) {
            $v->addServicerendu($this);
        }


        return $this;
    }


    /**
     * Get the associated ChildPrestation object
     *
     * @param  ConnectionInterface $con Optional Connection object.
     * @return ChildPrestation The associated ChildPrestation object.
     * @throws PropelException
     */
    public function getPrestation(ConnectionInterface $con = null)
    {
        if ($this->aPrestation === null && (($this->id_prestation !== "" && $this->id_prestation !== null))) {
            $this->aPrestation = ChildPrestationQuery::create()->findPk($this->id_prestation, $con);
            /* The following can be used additionally to
                guarantee the related object contains a reference
                to this object.  This level of coupling may, however, be
                undesirable since it could result in an only partially populated collection
                in the referenced object.
                $this->aPrestation->addServicerendus($this);
             */
        }

        return $this->aPrestation;
    }

    /**
     * Declares an association between this object and a ChildEntite object.
     *
     * @param  ChildEntite $v
     * @return $this|\Servicerendu The current object (for fluent API support)
     * @throws PropelException
     */
    public function setEntite(ChildEntite $v = null)
    {
        if ($v === null) {
            $this->setIdEntite(1);
        } else {
            $this->setIdEntite($v->getIdEntite());
        }

        $this->aEntite = $v;

        // Add binding for other direction of this n:n relationship.
        // If this object has already been added to the ChildEntite object, it will not be re-added.
        if ($v !== null) {
            $v->addServicerendu($this);
        }


        return $this;
    }


    /**
     * Get the associated ChildEntite object
     *
     * @param  ConnectionInterface $con Optional Connection object.
     * @return ChildEntite The associated ChildEntite object.
     * @throws PropelException
     */
    public function getEntite(ConnectionInterface $con = null)
    {
        if ($this->aEntite === null && ($this->id_entite != 0)) {
            $this->aEntite = ChildEntiteQuery::create()->findPk($this->id_entite, $con);
            /* The following can be used additionally to
                guarantee the related object contains a reference
                to this object.  This level of coupling may, however, be
                undesirable since it could result in an only partially populated collection
                in the referenced object.
                $this->aEntite->addServicerendus($this);
             */
        }

        return $this->aEntite;
    }

    /**
     * Declares an association between this object and a ChildMembre object.
     *
     * @param  ChildMembre $v
     * @return $this|\Servicerendu The current object (for fluent API support)
     * @throws PropelException
     */
    public function setMembre(ChildMembre $v = null)
    {
        if ($v === null) {
            $this->setIdMembre(NULL);
        } else {
            $this->setIdMembre($v->getIdMembre());
        }

        $this->aMembre = $v;

        // Add binding for other direction of this n:n relationship.
        // If this object has already been added to the ChildMembre object, it will not be re-added.
        if ($v !== null) {
            $v->addServicerendu($this);
        }


        return $this;
    }


    /**
     * Get the associated ChildMembre object
     *
     * @param  ConnectionInterface $con Optional Connection object.
     * @return ChildMembre The associated ChildMembre object.
     * @throws PropelException
     */
    public function getMembre(ConnectionInterface $con = null)
    {
        if ($this->aMembre === null && (($this->id_membre !== "" && $this->id_membre !== null))) {
            $this->aMembre = ChildMembreQuery::create()->findPk($this->id_membre, $con);
            /* The following can be used additionally to
                guarantee the related object contains a reference
                to this object.  This level of coupling may, however, be
                undesirable since it could result in an only partially populated collection
                in the referenced object.
                $this->aMembre->addServicerendus($this);
             */
        }

        return $this->aMembre;
    }

    /**
     * Declares an association between this object and a ChildServicerendu object.
     *
     * @param  ChildServicerendu $v
     * @return $this|\Servicerendu The current object (for fluent API support)
     * @throws PropelException
     */
    public function setServicerenduRelatedByOrigine(ChildServicerendu $v = null)
    {
        if ($v === null) {
            $this->setOrigine(NULL);
        } else {
            $this->setOrigine($v->getIdServicerendu());
        }

        $this->aServicerenduRelatedByOrigine = $v;

        // Add binding for other direction of this n:n relationship.
        // If this object has already been added to the ChildServicerendu object, it will not be re-added.
        if ($v !== null) {
            $v->addServicerenduRelatedByIdServicerendu($this);
        }


        return $this;
    }


    /**
     * Get the associated ChildServicerendu object
     *
     * @param  ConnectionInterface $con Optional Connection object.
     * @return ChildServicerendu The associated ChildServicerendu object.
     * @throws PropelException
     */
    public function getServicerenduRelatedByOrigine(ConnectionInterface $con = null)
    {
        if ($this->aServicerenduRelatedByOrigine === null && ($this->origine != 0)) {
            $this->aServicerenduRelatedByOrigine = ChildServicerenduQuery::create()->findPk($this->origine, $con);
            /* The following can be used additionally to
                guarantee the related object contains a reference
                to this object.  This level of coupling may, however, be
                undesirable since it could result in an only partially populated collection
                in the referenced object.
                $this->aServicerenduRelatedByOrigine->addServicerendusRelatedByIdServicerendu($this);
             */
        }

        return $this->aServicerenduRelatedByOrigine;
    }


    /**
     * Initializes a collection based on the name of a relation.
     * Avoids crafting an 'init[$relationName]s' method name
     * that wouldn't work when StandardEnglishPluralizer is used.
     *
     * @param      string $relationName The name of the relation to initialize
     * @return void
     */
    public function initRelation($relationName)
    {
        if ('Servicepaiement' == $relationName) {
            $this->initServicepaiements();
            return;
        }
        if ('ServicerenduRelatedByIdServicerendu' == $relationName) {
            $this->initServicerendusRelatedByIdServicerendu();
            return;
        }
        if ('ServicerenduIndividu' == $relationName) {
            $this->initServicerenduIndividus();
            return;
        }
    }

    /**
     * Clears out the collServicepaiements collection
     *
     * This does not modify the database; however, it will remove any associated objects, causing
     * them to be refetched by subsequent calls to accessor method.
     *
     * @return void
     * @see        addServicepaiements()
     */
    public function clearServicepaiements()
    {
        $this->collServicepaiements = null; // important to set this to NULL since that means it is uninitialized
    }

    /**
     * Reset is the collServicepaiements collection loaded partially.
     */
    public function resetPartialServicepaiements($v = true)
    {
        $this->collServicepaiementsPartial = $v;
    }

    /**
     * Initializes the collServicepaiements collection.
     *
     * By default this just sets the collServicepaiements collection to an empty array (like clearcollServicepaiements());
     * however, you may wish to override this method in your stub class to provide setting appropriate
     * to your application -- for example, setting the initial array to the values stored in database.
     *
     * @param      boolean $overrideExisting If set to true, the method call initializes
     *                                        the collection even if it is not empty
     *
     * @return void
     */
    public function initServicepaiements($overrideExisting = true)
    {
        if (null !== $this->collServicepaiements && !$overrideExisting) {
            return;
        }

        $collectionClassName = ServicepaiementTableMap::getTableMap()->getCollectionClassName();

        $this->collServicepaiements = new $collectionClassName;
        $this->collServicepaiements->setModel('\Servicepaiement');
    }

    /**
     * Gets an array of ChildServicepaiement objects which contain a foreign key that references this object.
     *
     * If the $criteria is not null, it is used to always fetch the results from the database.
     * Otherwise the results are fetched from the database the first time, then cached.
     * Next time the same method is called without $criteria, the cached collection is returned.
     * If this ChildServicerendu is new, it will return
     * an empty collection or the current collection; the criteria is ignored on a new object.
     *
     * @param      Criteria $criteria optional Criteria object to narrow the query
     * @param      ConnectionInterface $con optional connection object
     * @return ObjectCollection|ChildServicepaiement[] List of ChildServicepaiement objects
     * @throws PropelException
     */
    public function getServicepaiements(Criteria $criteria = null, ConnectionInterface $con = null)
    {
        $partial = $this->collServicepaiementsPartial && !$this->isNew();
        if (null === $this->collServicepaiements || null !== $criteria  || $partial) {
            if ($this->isNew() && null === $this->collServicepaiements) {
                // return empty collection
                $this->initServicepaiements();
            } else {
                $collServicepaiements = ChildServicepaiementQuery::create(null, $criteria)
                    ->filterByServicerendu($this)
                    ->find($con);

                if (null !== $criteria) {
                    if (false !== $this->collServicepaiementsPartial && count($collServicepaiements)) {
                        $this->initServicepaiements(false);

                        foreach ($collServicepaiements as $obj) {
                            if (false == $this->collServicepaiements->contains($obj)) {
                                $this->collServicepaiements->append($obj);
                            }
                        }

                        $this->collServicepaiementsPartial = true;
                    }

                    return $collServicepaiements;
                }

                if ($partial && $this->collServicepaiements) {
                    foreach ($this->collServicepaiements as $obj) {
                        if ($obj->isNew()) {
                            $collServicepaiements[] = $obj;
                        }
                    }
                }

                $this->collServicepaiements = $collServicepaiements;
                $this->collServicepaiementsPartial = false;
            }
        }

        return $this->collServicepaiements;
    }

    /**
     * Sets a collection of ChildServicepaiement objects related by a one-to-many relationship
     * to the current object.
     * It will also schedule objects for deletion based on a diff between old objects (aka persisted)
     * and new objects from the given Propel collection.
     *
     * @param      Collection $servicepaiements A Propel collection.
     * @param      ConnectionInterface $con Optional connection object
     * @return $this|ChildServicerendu The current object (for fluent API support)
     */
    public function setServicepaiements(Collection $servicepaiements, ConnectionInterface $con = null)
    {
        /** @var ChildServicepaiement[] $servicepaiementsToDelete */
        $servicepaiementsToDelete = $this->getServicepaiements(new Criteria(), $con)->diff($servicepaiements);


        $this->servicepaiementsScheduledForDeletion = $servicepaiementsToDelete;

        foreach ($servicepaiementsToDelete as $servicepaiementRemoved) {
            $servicepaiementRemoved->setServicerendu(null);
        }

        $this->collServicepaiements = null;
        foreach ($servicepaiements as $servicepaiement) {
            $this->addServicepaiement($servicepaiement);
        }

        $this->collServicepaiements = $servicepaiements;
        $this->collServicepaiementsPartial = false;

        return $this;
    }

    /**
     * Returns the number of related Servicepaiement objects.
     *
     * @param      Criteria $criteria
     * @param      boolean $distinct
     * @param      ConnectionInterface $con
     * @return int             Count of related Servicepaiement objects.
     * @throws PropelException
     */
    public function countServicepaiements(Criteria $criteria = null, $distinct = false, ConnectionInterface $con = null)
    {
        $partial = $this->collServicepaiementsPartial && !$this->isNew();
        if (null === $this->collServicepaiements || null !== $criteria || $partial) {
            if ($this->isNew() && null === $this->collServicepaiements) {
                return 0;
            }

            if ($partial && !$criteria) {
                return count($this->getServicepaiements());
            }

            $query = ChildServicepaiementQuery::create(null, $criteria);
            if ($distinct) {
                $query->distinct();
            }

            return $query
                ->filterByServicerendu($this)
                ->count($con);
        }

        return count($this->collServicepaiements);
    }

    /**
     * Method called to associate a ChildServicepaiement object to this object
     * through the ChildServicepaiement foreign key attribute.
     *
     * @param  ChildServicepaiement $l ChildServicepaiement
     * @return $this|\Servicerendu The current object (for fluent API support)
     */
    public function addServicepaiement(ChildServicepaiement $l)
    {
        if ($this->collServicepaiements === null) {
            $this->initServicepaiements();
            $this->collServicepaiementsPartial = true;
        }

        if (!$this->collServicepaiements->contains($l)) {
            $this->doAddServicepaiement($l);

            if ($this->servicepaiementsScheduledForDeletion and $this->servicepaiementsScheduledForDeletion->contains($l)) {
                $this->servicepaiementsScheduledForDeletion->remove($this->servicepaiementsScheduledForDeletion->search($l));
            }
        }

        return $this;
    }

    /**
     * @param ChildServicepaiement $servicepaiement The ChildServicepaiement object to add.
     */
    protected function doAddServicepaiement(ChildServicepaiement $servicepaiement)
    {
        $this->collServicepaiements[]= $servicepaiement;
        $servicepaiement->setServicerendu($this);
    }

    /**
     * @param  ChildServicepaiement $servicepaiement The ChildServicepaiement object to remove.
     * @return $this|ChildServicerendu The current object (for fluent API support)
     */
    public function removeServicepaiement(ChildServicepaiement $servicepaiement)
    {
        if ($this->getServicepaiements()->contains($servicepaiement)) {
            $pos = $this->collServicepaiements->search($servicepaiement);
            $this->collServicepaiements->remove($pos);
            if (null === $this->servicepaiementsScheduledForDeletion) {
                $this->servicepaiementsScheduledForDeletion = clone $this->collServicepaiements;
                $this->servicepaiementsScheduledForDeletion->clear();
            }
            $this->servicepaiementsScheduledForDeletion[]= clone $servicepaiement;
            $servicepaiement->setServicerendu(null);
        }

        return $this;
    }


    /**
     * If this collection has already been initialized with
     * an identical criteria, it returns the collection.
     * Otherwise if this Servicerendu is new, it will return
     * an empty collection; or if this Servicerendu has previously
     * been saved, it will retrieve related Servicepaiements from storage.
     *
     * This method is protected by default in order to keep the public
     * api reasonable.  You can provide public methods for those you
     * actually need in Servicerendu.
     *
     * @param      Criteria $criteria optional Criteria object to narrow the query
     * @param      ConnectionInterface $con optional connection object
     * @param      string $joinBehavior optional join type to use (defaults to Criteria::LEFT_JOIN)
     * @return ObjectCollection|ChildServicepaiement[] List of ChildServicepaiement objects
     */
    public function getServicepaiementsJoinPaiement(Criteria $criteria = null, ConnectionInterface $con = null, $joinBehavior = Criteria::LEFT_JOIN)
    {
        $query = ChildServicepaiementQuery::create(null, $criteria);
        $query->joinWith('Paiement', $joinBehavior);

        return $this->getServicepaiements($query, $con);
    }

    /**
     * Clears out the collServicerendusRelatedByIdServicerendu collection
     *
     * This does not modify the database; however, it will remove any associated objects, causing
     * them to be refetched by subsequent calls to accessor method.
     *
     * @return void
     * @see        addServicerendusRelatedByIdServicerendu()
     */
    public function clearServicerendusRelatedByIdServicerendu()
    {
        $this->collServicerendusRelatedByIdServicerendu = null; // important to set this to NULL since that means it is uninitialized
    }

    /**
     * Reset is the collServicerendusRelatedByIdServicerendu collection loaded partially.
     */
    public function resetPartialServicerendusRelatedByIdServicerendu($v = true)
    {
        $this->collServicerendusRelatedByIdServicerenduPartial = $v;
    }

    /**
     * Initializes the collServicerendusRelatedByIdServicerendu collection.
     *
     * By default this just sets the collServicerendusRelatedByIdServicerendu collection to an empty array (like clearcollServicerendusRelatedByIdServicerendu());
     * however, you may wish to override this method in your stub class to provide setting appropriate
     * to your application -- for example, setting the initial array to the values stored in database.
     *
     * @param      boolean $overrideExisting If set to true, the method call initializes
     *                                        the collection even if it is not empty
     *
     * @return void
     */
    public function initServicerendusRelatedByIdServicerendu($overrideExisting = true)
    {
        if (null !== $this->collServicerendusRelatedByIdServicerendu && !$overrideExisting) {
            return;
        }

        $collectionClassName = ServicerenduTableMap::getTableMap()->getCollectionClassName();

        $this->collServicerendusRelatedByIdServicerendu = new $collectionClassName;
        $this->collServicerendusRelatedByIdServicerendu->setModel('\Servicerendu');
    }

    /**
     * Gets an array of ChildServicerendu objects which contain a foreign key that references this object.
     *
     * If the $criteria is not null, it is used to always fetch the results from the database.
     * Otherwise the results are fetched from the database the first time, then cached.
     * Next time the same method is called without $criteria, the cached collection is returned.
     * If this ChildServicerendu is new, it will return
     * an empty collection or the current collection; the criteria is ignored on a new object.
     *
     * @param      Criteria $criteria optional Criteria object to narrow the query
     * @param      ConnectionInterface $con optional connection object
     * @return ObjectCollection|ChildServicerendu[] List of ChildServicerendu objects
     * @throws PropelException
     */
    public function getServicerendusRelatedByIdServicerendu(Criteria $criteria = null, ConnectionInterface $con = null)
    {
        $partial = $this->collServicerendusRelatedByIdServicerenduPartial && !$this->isNew();
        if (null === $this->collServicerendusRelatedByIdServicerendu || null !== $criteria  || $partial) {
            if ($this->isNew() && null === $this->collServicerendusRelatedByIdServicerendu) {
                // return empty collection
                $this->initServicerendusRelatedByIdServicerendu();
            } else {
                $collServicerendusRelatedByIdServicerendu = ChildServicerenduQuery::create(null, $criteria)
                    ->filterByServicerenduRelatedByOrigine($this)
                    ->find($con);

                if (null !== $criteria) {
                    if (false !== $this->collServicerendusRelatedByIdServicerenduPartial && count($collServicerendusRelatedByIdServicerendu)) {
                        $this->initServicerendusRelatedByIdServicerendu(false);

                        foreach ($collServicerendusRelatedByIdServicerendu as $obj) {
                            if (false == $this->collServicerendusRelatedByIdServicerendu->contains($obj)) {
                                $this->collServicerendusRelatedByIdServicerendu->append($obj);
                            }
                        }

                        $this->collServicerendusRelatedByIdServicerenduPartial = true;
                    }

                    return $collServicerendusRelatedByIdServicerendu;
                }

                if ($partial && $this->collServicerendusRelatedByIdServicerendu) {
                    foreach ($this->collServicerendusRelatedByIdServicerendu as $obj) {
                        if ($obj->isNew()) {
                            $collServicerendusRelatedByIdServicerendu[] = $obj;
                        }
                    }
                }

                $this->collServicerendusRelatedByIdServicerendu = $collServicerendusRelatedByIdServicerendu;
                $this->collServicerendusRelatedByIdServicerenduPartial = false;
            }
        }

        return $this->collServicerendusRelatedByIdServicerendu;
    }

    /**
     * Sets a collection of ChildServicerendu objects related by a one-to-many relationship
     * to the current object.
     * It will also schedule objects for deletion based on a diff between old objects (aka persisted)
     * and new objects from the given Propel collection.
     *
     * @param      Collection $servicerendusRelatedByIdServicerendu A Propel collection.
     * @param      ConnectionInterface $con Optional connection object
     * @return $this|ChildServicerendu The current object (for fluent API support)
     */
    public function setServicerendusRelatedByIdServicerendu(Collection $servicerendusRelatedByIdServicerendu, ConnectionInterface $con = null)
    {
        /** @var ChildServicerendu[] $servicerendusRelatedByIdServicerenduToDelete */
        $servicerendusRelatedByIdServicerenduToDelete = $this->getServicerendusRelatedByIdServicerendu(new Criteria(), $con)->diff($servicerendusRelatedByIdServicerendu);


        $this->servicerendusRelatedByIdServicerenduScheduledForDeletion = $servicerendusRelatedByIdServicerenduToDelete;

        foreach ($servicerendusRelatedByIdServicerenduToDelete as $servicerenduRelatedByIdServicerenduRemoved) {
            $servicerenduRelatedByIdServicerenduRemoved->setServicerenduRelatedByOrigine(null);
        }

        $this->collServicerendusRelatedByIdServicerendu = null;
        foreach ($servicerendusRelatedByIdServicerendu as $servicerenduRelatedByIdServicerendu) {
            $this->addServicerenduRelatedByIdServicerendu($servicerenduRelatedByIdServicerendu);
        }

        $this->collServicerendusRelatedByIdServicerendu = $servicerendusRelatedByIdServicerendu;
        $this->collServicerendusRelatedByIdServicerenduPartial = false;

        return $this;
    }

    /**
     * Returns the number of related Servicerendu objects.
     *
     * @param      Criteria $criteria
     * @param      boolean $distinct
     * @param      ConnectionInterface $con
     * @return int             Count of related Servicerendu objects.
     * @throws PropelException
     */
    public function countServicerendusRelatedByIdServicerendu(Criteria $criteria = null, $distinct = false, ConnectionInterface $con = null)
    {
        $partial = $this->collServicerendusRelatedByIdServicerenduPartial && !$this->isNew();
        if (null === $this->collServicerendusRelatedByIdServicerendu || null !== $criteria || $partial) {
            if ($this->isNew() && null === $this->collServicerendusRelatedByIdServicerendu) {
                return 0;
            }

            if ($partial && !$criteria) {
                return count($this->getServicerendusRelatedByIdServicerendu());
            }

            $query = ChildServicerenduQuery::create(null, $criteria);
            if ($distinct) {
                $query->distinct();
            }

            return $query
                ->filterByServicerenduRelatedByOrigine($this)
                ->count($con);
        }

        return count($this->collServicerendusRelatedByIdServicerendu);
    }

    /**
     * Method called to associate a ChildServicerendu object to this object
     * through the ChildServicerendu foreign key attribute.
     *
     * @param  ChildServicerendu $l ChildServicerendu
     * @return $this|\Servicerendu The current object (for fluent API support)
     */
    public function addServicerenduRelatedByIdServicerendu(ChildServicerendu $l)
    {
        if ($this->collServicerendusRelatedByIdServicerendu === null) {
            $this->initServicerendusRelatedByIdServicerendu();
            $this->collServicerendusRelatedByIdServicerenduPartial = true;
        }

        if (!$this->collServicerendusRelatedByIdServicerendu->contains($l)) {
            $this->doAddServicerenduRelatedByIdServicerendu($l);

            if ($this->servicerendusRelatedByIdServicerenduScheduledForDeletion and $this->servicerendusRelatedByIdServicerenduScheduledForDeletion->contains($l)) {
                $this->servicerendusRelatedByIdServicerenduScheduledForDeletion->remove($this->servicerendusRelatedByIdServicerenduScheduledForDeletion->search($l));
            }
        }

        return $this;
    }

    /**
     * @param ChildServicerendu $servicerenduRelatedByIdServicerendu The ChildServicerendu object to add.
     */
    protected function doAddServicerenduRelatedByIdServicerendu(ChildServicerendu $servicerenduRelatedByIdServicerendu)
    {
        $this->collServicerendusRelatedByIdServicerendu[]= $servicerenduRelatedByIdServicerendu;
        $servicerenduRelatedByIdServicerendu->setServicerenduRelatedByOrigine($this);
    }

    /**
     * @param  ChildServicerendu $servicerenduRelatedByIdServicerendu The ChildServicerendu object to remove.
     * @return $this|ChildServicerendu The current object (for fluent API support)
     */
    public function removeServicerenduRelatedByIdServicerendu(ChildServicerendu $servicerenduRelatedByIdServicerendu)
    {
        if ($this->getServicerendusRelatedByIdServicerendu()->contains($servicerenduRelatedByIdServicerendu)) {
            $pos = $this->collServicerendusRelatedByIdServicerendu->search($servicerenduRelatedByIdServicerendu);
            $this->collServicerendusRelatedByIdServicerendu->remove($pos);
            if (null === $this->servicerendusRelatedByIdServicerenduScheduledForDeletion) {
                $this->servicerendusRelatedByIdServicerenduScheduledForDeletion = clone $this->collServicerendusRelatedByIdServicerendu;
                $this->servicerendusRelatedByIdServicerenduScheduledForDeletion->clear();
            }
            $this->servicerendusRelatedByIdServicerenduScheduledForDeletion[]= $servicerenduRelatedByIdServicerendu;
            $servicerenduRelatedByIdServicerendu->setServicerenduRelatedByOrigine(null);
        }

        return $this;
    }


    /**
     * If this collection has already been initialized with
     * an identical criteria, it returns the collection.
     * Otherwise if this Servicerendu is new, it will return
     * an empty collection; or if this Servicerendu has previously
     * been saved, it will retrieve related ServicerendusRelatedByIdServicerendu from storage.
     *
     * This method is protected by default in order to keep the public
     * api reasonable.  You can provide public methods for those you
     * actually need in Servicerendu.
     *
     * @param      Criteria $criteria optional Criteria object to narrow the query
     * @param      ConnectionInterface $con optional connection object
     * @param      string $joinBehavior optional join type to use (defaults to Criteria::LEFT_JOIN)
     * @return ObjectCollection|ChildServicerendu[] List of ChildServicerendu objects
     */
    public function getServicerendusRelatedByIdServicerenduJoinPrestation(Criteria $criteria = null, ConnectionInterface $con = null, $joinBehavior = Criteria::LEFT_JOIN)
    {
        $query = ChildServicerenduQuery::create(null, $criteria);
        $query->joinWith('Prestation', $joinBehavior);

        return $this->getServicerendusRelatedByIdServicerendu($query, $con);
    }


    /**
     * If this collection has already been initialized with
     * an identical criteria, it returns the collection.
     * Otherwise if this Servicerendu is new, it will return
     * an empty collection; or if this Servicerendu has previously
     * been saved, it will retrieve related ServicerendusRelatedByIdServicerendu from storage.
     *
     * This method is protected by default in order to keep the public
     * api reasonable.  You can provide public methods for those you
     * actually need in Servicerendu.
     *
     * @param      Criteria $criteria optional Criteria object to narrow the query
     * @param      ConnectionInterface $con optional connection object
     * @param      string $joinBehavior optional join type to use (defaults to Criteria::LEFT_JOIN)
     * @return ObjectCollection|ChildServicerendu[] List of ChildServicerendu objects
     */
    public function getServicerendusRelatedByIdServicerenduJoinEntite(Criteria $criteria = null, ConnectionInterface $con = null, $joinBehavior = Criteria::LEFT_JOIN)
    {
        $query = ChildServicerenduQuery::create(null, $criteria);
        $query->joinWith('Entite', $joinBehavior);

        return $this->getServicerendusRelatedByIdServicerendu($query, $con);
    }


    /**
     * If this collection has already been initialized with
     * an identical criteria, it returns the collection.
     * Otherwise if this Servicerendu is new, it will return
     * an empty collection; or if this Servicerendu has previously
     * been saved, it will retrieve related ServicerendusRelatedByIdServicerendu from storage.
     *
     * This method is protected by default in order to keep the public
     * api reasonable.  You can provide public methods for those you
     * actually need in Servicerendu.
     *
     * @param      Criteria $criteria optional Criteria object to narrow the query
     * @param      ConnectionInterface $con optional connection object
     * @param      string $joinBehavior optional join type to use (defaults to Criteria::LEFT_JOIN)
     * @return ObjectCollection|ChildServicerendu[] List of ChildServicerendu objects
     */
    public function getServicerendusRelatedByIdServicerenduJoinMembre(Criteria $criteria = null, ConnectionInterface $con = null, $joinBehavior = Criteria::LEFT_JOIN)
    {
        $query = ChildServicerenduQuery::create(null, $criteria);
        $query->joinWith('Membre', $joinBehavior);

        return $this->getServicerendusRelatedByIdServicerendu($query, $con);
    }

    /**
     * Clears out the collServicerenduIndividus collection
     *
     * This does not modify the database; however, it will remove any associated objects, causing
     * them to be refetched by subsequent calls to accessor method.
     *
     * @return void
     * @see        addServicerenduIndividus()
     */
    public function clearServicerenduIndividus()
    {
        $this->collServicerenduIndividus = null; // important to set this to NULL since that means it is uninitialized
    }

    /**
     * Reset is the collServicerenduIndividus collection loaded partially.
     */
    public function resetPartialServicerenduIndividus($v = true)
    {
        $this->collServicerenduIndividusPartial = $v;
    }

    /**
     * Initializes the collServicerenduIndividus collection.
     *
     * By default this just sets the collServicerenduIndividus collection to an empty array (like clearcollServicerenduIndividus());
     * however, you may wish to override this method in your stub class to provide setting appropriate
     * to your application -- for example, setting the initial array to the values stored in database.
     *
     * @param      boolean $overrideExisting If set to true, the method call initializes
     *                                        the collection even if it is not empty
     *
     * @return void
     */
    public function initServicerenduIndividus($overrideExisting = true)
    {
        if (null !== $this->collServicerenduIndividus && !$overrideExisting) {
            return;
        }

        $collectionClassName = ServicerenduIndividuTableMap::getTableMap()->getCollectionClassName();

        $this->collServicerenduIndividus = new $collectionClassName;
        $this->collServicerenduIndividus->setModel('\ServicerenduIndividu');
    }

    /**
     * Gets an array of ChildServicerenduIndividu objects which contain a foreign key that references this object.
     *
     * If the $criteria is not null, it is used to always fetch the results from the database.
     * Otherwise the results are fetched from the database the first time, then cached.
     * Next time the same method is called without $criteria, the cached collection is returned.
     * If this ChildServicerendu is new, it will return
     * an empty collection or the current collection; the criteria is ignored on a new object.
     *
     * @param      Criteria $criteria optional Criteria object to narrow the query
     * @param      ConnectionInterface $con optional connection object
     * @return ObjectCollection|ChildServicerenduIndividu[] List of ChildServicerenduIndividu objects
     * @throws PropelException
     */
    public function getServicerenduIndividus(Criteria $criteria = null, ConnectionInterface $con = null)
    {
        $partial = $this->collServicerenduIndividusPartial && !$this->isNew();
        if (null === $this->collServicerenduIndividus || null !== $criteria  || $partial) {
            if ($this->isNew() && null === $this->collServicerenduIndividus) {
                // return empty collection
                $this->initServicerenduIndividus();
            } else {
                $collServicerenduIndividus = ChildServicerenduIndividuQuery::create(null, $criteria)
                    ->filterByServicerendu($this)
                    ->find($con);

                if (null !== $criteria) {
                    if (false !== $this->collServicerenduIndividusPartial && count($collServicerenduIndividus)) {
                        $this->initServicerenduIndividus(false);

                        foreach ($collServicerenduIndividus as $obj) {
                            if (false == $this->collServicerenduIndividus->contains($obj)) {
                                $this->collServicerenduIndividus->append($obj);
                            }
                        }

                        $this->collServicerenduIndividusPartial = true;
                    }

                    return $collServicerenduIndividus;
                }

                if ($partial && $this->collServicerenduIndividus) {
                    foreach ($this->collServicerenduIndividus as $obj) {
                        if ($obj->isNew()) {
                            $collServicerenduIndividus[] = $obj;
                        }
                    }
                }

                $this->collServicerenduIndividus = $collServicerenduIndividus;
                $this->collServicerenduIndividusPartial = false;
            }
        }

        return $this->collServicerenduIndividus;
    }

    /**
     * Sets a collection of ChildServicerenduIndividu objects related by a one-to-many relationship
     * to the current object.
     * It will also schedule objects for deletion based on a diff between old objects (aka persisted)
     * and new objects from the given Propel collection.
     *
     * @param      Collection $servicerenduIndividus A Propel collection.
     * @param      ConnectionInterface $con Optional connection object
     * @return $this|ChildServicerendu The current object (for fluent API support)
     */
    public function setServicerenduIndividus(Collection $servicerenduIndividus, ConnectionInterface $con = null)
    {
        /** @var ChildServicerenduIndividu[] $servicerenduIndividusToDelete */
        $servicerenduIndividusToDelete = $this->getServicerenduIndividus(new Criteria(), $con)->diff($servicerenduIndividus);


        //since at least one column in the foreign key is at the same time a PK
        //we can not just set a PK to NULL in the lines below. We have to store
        //a backup of all values, so we are able to manipulate these items based on the onDelete value later.
        $this->servicerenduIndividusScheduledForDeletion = clone $servicerenduIndividusToDelete;

        foreach ($servicerenduIndividusToDelete as $servicerenduIndividuRemoved) {
            $servicerenduIndividuRemoved->setServicerendu(null);
        }

        $this->collServicerenduIndividus = null;
        foreach ($servicerenduIndividus as $servicerenduIndividu) {
            $this->addServicerenduIndividu($servicerenduIndividu);
        }

        $this->collServicerenduIndividus = $servicerenduIndividus;
        $this->collServicerenduIndividusPartial = false;

        return $this;
    }

    /**
     * Returns the number of related ServicerenduIndividu objects.
     *
     * @param      Criteria $criteria
     * @param      boolean $distinct
     * @param      ConnectionInterface $con
     * @return int             Count of related ServicerenduIndividu objects.
     * @throws PropelException
     */
    public function countServicerenduIndividus(Criteria $criteria = null, $distinct = false, ConnectionInterface $con = null)
    {
        $partial = $this->collServicerenduIndividusPartial && !$this->isNew();
        if (null === $this->collServicerenduIndividus || null !== $criteria || $partial) {
            if ($this->isNew() && null === $this->collServicerenduIndividus) {
                return 0;
            }

            if ($partial && !$criteria) {
                return count($this->getServicerenduIndividus());
            }

            $query = ChildServicerenduIndividuQuery::create(null, $criteria);
            if ($distinct) {
                $query->distinct();
            }

            return $query
                ->filterByServicerendu($this)
                ->count($con);
        }

        return count($this->collServicerenduIndividus);
    }

    /**
     * Method called to associate a ChildServicerenduIndividu object to this object
     * through the ChildServicerenduIndividu foreign key attribute.
     *
     * @param  ChildServicerenduIndividu $l ChildServicerenduIndividu
     * @return $this|\Servicerendu The current object (for fluent API support)
     */
    public function addServicerenduIndividu(ChildServicerenduIndividu $l)
    {
        if ($this->collServicerenduIndividus === null) {
            $this->initServicerenduIndividus();
            $this->collServicerenduIndividusPartial = true;
        }

        if (!$this->collServicerenduIndividus->contains($l)) {
            $this->doAddServicerenduIndividu($l);

            if ($this->servicerenduIndividusScheduledForDeletion and $this->servicerenduIndividusScheduledForDeletion->contains($l)) {
                $this->servicerenduIndividusScheduledForDeletion->remove($this->servicerenduIndividusScheduledForDeletion->search($l));
            }
        }

        return $this;
    }

    /**
     * @param ChildServicerenduIndividu $servicerenduIndividu The ChildServicerenduIndividu object to add.
     */
    protected function doAddServicerenduIndividu(ChildServicerenduIndividu $servicerenduIndividu)
    {
        $this->collServicerenduIndividus[]= $servicerenduIndividu;
        $servicerenduIndividu->setServicerendu($this);
    }

    /**
     * @param  ChildServicerenduIndividu $servicerenduIndividu The ChildServicerenduIndividu object to remove.
     * @return $this|ChildServicerendu The current object (for fluent API support)
     */
    public function removeServicerenduIndividu(ChildServicerenduIndividu $servicerenduIndividu)
    {
        if ($this->getServicerenduIndividus()->contains($servicerenduIndividu)) {
            $pos = $this->collServicerenduIndividus->search($servicerenduIndividu);
            $this->collServicerenduIndividus->remove($pos);
            if (null === $this->servicerenduIndividusScheduledForDeletion) {
                $this->servicerenduIndividusScheduledForDeletion = clone $this->collServicerenduIndividus;
                $this->servicerenduIndividusScheduledForDeletion->clear();
            }
            $this->servicerenduIndividusScheduledForDeletion[]= clone $servicerenduIndividu;
            $servicerenduIndividu->setServicerendu(null);
        }

        return $this;
    }


    /**
     * If this collection has already been initialized with
     * an identical criteria, it returns the collection.
     * Otherwise if this Servicerendu is new, it will return
     * an empty collection; or if this Servicerendu has previously
     * been saved, it will retrieve related ServicerenduIndividus from storage.
     *
     * This method is protected by default in order to keep the public
     * api reasonable.  You can provide public methods for those you
     * actually need in Servicerendu.
     *
     * @param      Criteria $criteria optional Criteria object to narrow the query
     * @param      ConnectionInterface $con optional connection object
     * @param      string $joinBehavior optional join type to use (defaults to Criteria::LEFT_JOIN)
     * @return ObjectCollection|ChildServicerenduIndividu[] List of ChildServicerenduIndividu objects
     */
    public function getServicerenduIndividusJoinIndividu(Criteria $criteria = null, ConnectionInterface $con = null, $joinBehavior = Criteria::LEFT_JOIN)
    {
        $query = ChildServicerenduIndividuQuery::create(null, $criteria);
        $query->joinWith('Individu', $joinBehavior);

        return $this->getServicerenduIndividus($query, $con);
    }

    /**
     * Clears out the collIndividus collection
     *
     * This does not modify the database; however, it will remove any associated objects, causing
     * them to be refetched by subsequent calls to accessor method.
     *
     * @return void
     * @see        addIndividus()
     */
    public function clearIndividus()
    {
        $this->collIndividus = null; // important to set this to NULL since that means it is uninitialized
    }

    /**
     * Initializes the collIndividus crossRef collection.
     *
     * By default this just sets the collIndividus collection to an empty collection (like clearIndividus());
     * however, you may wish to override this method in your stub class to provide setting appropriate
     * to your application -- for example, setting the initial array to the values stored in database.
     *
     * @return void
     */
    public function initIndividus()
    {
        $collectionClassName = ServicerenduIndividuTableMap::getTableMap()->getCollectionClassName();

        $this->collIndividus = new $collectionClassName;
        $this->collIndividusPartial = true;
        $this->collIndividus->setModel('\Individu');
    }

    /**
     * Checks if the collIndividus collection is loaded.
     *
     * @return bool
     */
    public function isIndividusLoaded()
    {
        return null !== $this->collIndividus;
    }

    /**
     * Gets a collection of ChildIndividu objects related by a many-to-many relationship
     * to the current object by way of the asso_servicerendus_individus cross-reference table.
     *
     * If the $criteria is not null, it is used to always fetch the results from the database.
     * Otherwise the results are fetched from the database the first time, then cached.
     * Next time the same method is called without $criteria, the cached collection is returned.
     * If this ChildServicerendu is new, it will return
     * an empty collection or the current collection; the criteria is ignored on a new object.
     *
     * @param      Criteria $criteria Optional query object to filter the query
     * @param      ConnectionInterface $con Optional connection object
     *
     * @return ObjectCollection|ChildIndividu[] List of ChildIndividu objects
     */
    public function getIndividus(Criteria $criteria = null, ConnectionInterface $con = null)
    {
        $partial = $this->collIndividusPartial && !$this->isNew();
        if (null === $this->collIndividus || null !== $criteria || $partial) {
            if ($this->isNew()) {
                // return empty collection
                if (null === $this->collIndividus) {
                    $this->initIndividus();
                }
            } else {

                $query = ChildIndividuQuery::create(null, $criteria)
                    ->filterByServicerendu($this);
                $collIndividus = $query->find($con);
                if (null !== $criteria) {
                    return $collIndividus;
                }

                if ($partial && $this->collIndividus) {
                    //make sure that already added objects gets added to the list of the database.
                    foreach ($this->collIndividus as $obj) {
                        if (!$collIndividus->contains($obj)) {
                            $collIndividus[] = $obj;
                        }
                    }
                }

                $this->collIndividus = $collIndividus;
                $this->collIndividusPartial = false;
            }
        }

        return $this->collIndividus;
    }

    /**
     * Sets a collection of Individu objects related by a many-to-many relationship
     * to the current object by way of the asso_servicerendus_individus cross-reference table.
     * It will also schedule objects for deletion based on a diff between old objects (aka persisted)
     * and new objects from the given Propel collection.
     *
     * @param  Collection $individus A Propel collection.
     * @param  ConnectionInterface $con Optional connection object
     * @return $this|ChildServicerendu The current object (for fluent API support)
     */
    public function setIndividus(Collection $individus, ConnectionInterface $con = null)
    {
        $this->clearIndividus();
        $currentIndividus = $this->getIndividus();

        $individusScheduledForDeletion = $currentIndividus->diff($individus);

        foreach ($individusScheduledForDeletion as $toDelete) {
            $this->removeIndividu($toDelete);
        }

        foreach ($individus as $individu) {
            if (!$currentIndividus->contains($individu)) {
                $this->doAddIndividu($individu);
            }
        }

        $this->collIndividusPartial = false;
        $this->collIndividus = $individus;

        return $this;
    }

    /**
     * Gets the number of Individu objects related by a many-to-many relationship
     * to the current object by way of the asso_servicerendus_individus cross-reference table.
     *
     * @param      Criteria $criteria Optional query object to filter the query
     * @param      boolean $distinct Set to true to force count distinct
     * @param      ConnectionInterface $con Optional connection object
     *
     * @return int the number of related Individu objects
     */
    public function countIndividus(Criteria $criteria = null, $distinct = false, ConnectionInterface $con = null)
    {
        $partial = $this->collIndividusPartial && !$this->isNew();
        if (null === $this->collIndividus || null !== $criteria || $partial) {
            if ($this->isNew() && null === $this->collIndividus) {
                return 0;
            } else {

                if ($partial && !$criteria) {
                    return count($this->getIndividus());
                }

                $query = ChildIndividuQuery::create(null, $criteria);
                if ($distinct) {
                    $query->distinct();
                }

                return $query
                    ->filterByServicerendu($this)
                    ->count($con);
            }
        } else {
            return count($this->collIndividus);
        }
    }

    /**
     * Associate a ChildIndividu to this object
     * through the asso_servicerendus_individus cross reference table.
     *
     * @param ChildIndividu $individu
     * @return ChildServicerendu The current object (for fluent API support)
     */
    public function addIndividu(ChildIndividu $individu)
    {
        if ($this->collIndividus === null) {
            $this->initIndividus();
        }

        if (!$this->getIndividus()->contains($individu)) {
            // only add it if the **same** object is not already associated
            $this->collIndividus->push($individu);
            $this->doAddIndividu($individu);
        }

        return $this;
    }

    /**
     *
     * @param ChildIndividu $individu
     */
    protected function doAddIndividu(ChildIndividu $individu)
    {
        $servicerenduIndividu = new ChildServicerenduIndividu();

        $servicerenduIndividu->setIndividu($individu);

        $servicerenduIndividu->setServicerendu($this);

        $this->addServicerenduIndividu($servicerenduIndividu);

        // set the back reference to this object directly as using provided method either results
        // in endless loop or in multiple relations
        if (!$individu->isServicerendusLoaded()) {
            $individu->initServicerendus();
            $individu->getServicerendus()->push($this);
        } elseif (!$individu->getServicerendus()->contains($this)) {
            $individu->getServicerendus()->push($this);
        }

    }

    /**
     * Remove individu of this object
     * through the asso_servicerendus_individus cross reference table.
     *
     * @param ChildIndividu $individu
     * @return ChildServicerendu The current object (for fluent API support)
     */
    public function removeIndividu(ChildIndividu $individu)
    {
        if ($this->getIndividus()->contains($individu)) {
            $servicerenduIndividu = new ChildServicerenduIndividu();
            $servicerenduIndividu->setIndividu($individu);
            if ($individu->isServicerendusLoaded()) {
                //remove the back reference if available
                $individu->getServicerendus()->removeObject($this);
            }

            $servicerenduIndividu->setServicerendu($this);
            $this->removeServicerenduIndividu(clone $servicerenduIndividu);
            $servicerenduIndividu->clear();

            $this->collIndividus->remove($this->collIndividus->search($individu));

            if (null === $this->individusScheduledForDeletion) {
                $this->individusScheduledForDeletion = clone $this->collIndividus;
                $this->individusScheduledForDeletion->clear();
            }

            $this->individusScheduledForDeletion->push($individu);
        }


        return $this;
    }

    /**
     * Clears the current object, sets all attributes to their default values and removes
     * outgoing references as well as back-references (from other objects to this one. Results probably in a database
     * change of those foreign objects when you call `save` there).
     */
    public function clear()
    {
        if (null !== $this->aPrestation) {
            $this->aPrestation->removeServicerendu($this);
        }
        if (null !== $this->aEntite) {
            $this->aEntite->removeServicerendu($this);
        }
        if (null !== $this->aMembre) {
            $this->aMembre->removeServicerendu($this);
        }
        if (null !== $this->aServicerenduRelatedByOrigine) {
            $this->aServicerenduRelatedByOrigine->removeServicerenduRelatedByIdServicerendu($this);
        }
        $this->id_servicerendu = null;
        $this->id_membre = null;
        $this->montant = null;
        $this->date_enregistrement = null;
        $this->date_debut = null;
        $this->date_fin = null;
        $this->origine = null;
        $this->premier = null;
        $this->dernier = null;
        $this->id_entite = null;
        $this->id_prestation = null;
        $this->regle = null;
        $this->comptabilise = null;
        $this->observation = null;
        $this->id_tva = null;
        $this->quantite = null;
        $this->created_at = null;
        $this->updated_at = null;
        $this->alreadyInSave = false;
        $this->clearAllReferences();
        $this->applyDefaultValues();
        $this->resetModified();
        $this->setNew(true);
        $this->setDeleted(false);
    }

    /**
     * Resets all references and back-references to other model objects or collections of model objects.
     *
     * This method is used to reset all php object references (not the actual reference in the database).
     * Necessary for object serialisation.
     *
     * @param      boolean $deep Whether to also clear the references on all referrer objects.
     */
    public function clearAllReferences($deep = false)
    {
        if ($deep) {
            if ($this->collServicepaiements) {
                foreach ($this->collServicepaiements as $o) {
                    $o->clearAllReferences($deep);
                }
            }
            if ($this->collServicerendusRelatedByIdServicerendu) {
                foreach ($this->collServicerendusRelatedByIdServicerendu as $o) {
                    $o->clearAllReferences($deep);
                }
            }
            if ($this->collServicerenduIndividus) {
                foreach ($this->collServicerenduIndividus as $o) {
                    $o->clearAllReferences($deep);
                }
            }
            if ($this->collIndividus) {
                foreach ($this->collIndividus as $o) {
                    $o->clearAllReferences($deep);
                }
            }
        } // if ($deep)

        $this->collServicepaiements = null;
        $this->collServicerendusRelatedByIdServicerendu = null;
        $this->collServicerenduIndividus = null;
        $this->collIndividus = null;
        $this->aPrestation = null;
        $this->aEntite = null;
        $this->aMembre = null;
        $this->aServicerenduRelatedByOrigine = null;
    }

    /**
     * Return the string representation of this object
     *
     * @return string
     */
    public function __toString()
    {
        return (string) $this->exportTo(ServicerenduTableMap::DEFAULT_STRING_FORMAT);
    }

    // timestampable behavior

    /**
     * Mark the current object so that the update date doesn't get updated during next save
     *
     * @return     $this|ChildServicerendu The current object (for fluent API support)
     */
    public function keepUpdateDateUnchanged()
    {
        $this->modifiedColumns[ServicerenduTableMap::COL_UPDATED_AT] = true;

        return $this;
    }

    // archivable behavior

    /**
     * Get an archived version of the current object.
     *
     * @param ConnectionInterface $con Optional connection object
     *
     * @return     ChildAssoServicerendusArchive An archive object, or null if the current object was never archived
     */
    public function getArchive(ConnectionInterface $con = null)
    {
        if ($this->isNew()) {
            return null;
        }
        $archive = ChildAssoServicerendusArchiveQuery::create()
            ->filterByPrimaryKey($this->getPrimaryKey())
            ->findOne($con);

        return $archive;
    }
    /**
     * Copy the data of the current object into a $archiveTablePhpName archive object.
     * The archived object is then saved.
     * If the current object has already been archived, the archived object
     * is updated and not duplicated.
     *
     * @param ConnectionInterface $con Optional connection object
     *
     * @throws PropelException If the object is new
     *
     * @return     ChildAssoServicerendusArchive The archive object based on this object
     */
    public function archive(ConnectionInterface $con = null)
    {
        if ($this->isNew()) {
            throw new PropelException('New objects cannot be archived. You must save the current object before calling archive().');
        }
        $archive = $this->getArchive($con);
        if (!$archive) {
            $archive = new ChildAssoServicerendusArchive();
            $archive->setPrimaryKey($this->getPrimaryKey());
        }
        $this->copyInto($archive, $deepCopy = false, $makeNew = false);
        $archive->setArchivedAt(time());
        $archive->save($con);

        return $archive;
    }

    /**
     * Revert the the current object to the state it had when it was last archived.
     * The object must be saved afterwards if the changes must persist.
     *
     * @param ConnectionInterface $con Optional connection object
     *
     * @throws PropelException If the object has no corresponding archive.
     *
     * @return $this|ChildServicerendu The current object (for fluent API support)
     */
    public function restoreFromArchive(ConnectionInterface $con = null)
    {
        $archive = $this->getArchive($con);
        if (!$archive) {
            throw new PropelException('The current object has never been archived and cannot be restored');
        }
        $this->populateFromArchive($archive);

        return $this;
    }

    /**
     * Populates the the current object based on a $archiveTablePhpName archive object.
     *
     * @param      ChildAssoServicerendusArchive $archive An archived object based on the same class
      * @param      Boolean $populateAutoIncrementPrimaryKeys
     *               If true, autoincrement columns are copied from the archive object.
     *               If false, autoincrement columns are left intact.
      *
     * @return     ChildServicerendu The current object (for fluent API support)
     */
    public function populateFromArchive($archive, $populateAutoIncrementPrimaryKeys = false) {
        if ($populateAutoIncrementPrimaryKeys) {
            $this->setIdServicerendu($archive->getIdServicerendu());
        }
        $this->setIdMembre($archive->getIdMembre());
        $this->setMontant($archive->getMontant());
        $this->setDateEnregistrement($archive->getDateEnregistrement());
        $this->setDateDebut($archive->getDateDebut());
        $this->setDateFin($archive->getDateFin());
        $this->setOrigine($archive->getOrigine());
        $this->setPremier($archive->getPremier());
        $this->setDernier($archive->getDernier());
        $this->setIdEntite($archive->getIdEntite());
        $this->setIdPrestation($archive->getIdPrestation());
        $this->setRegle($archive->getRegle());
        $this->setComptabilise($archive->getComptabilise());
        $this->setObservation($archive->getObservation());
        $this->setIdTva($archive->getIdTva());
        $this->setQuantite($archive->getQuantite());
        $this->setCreatedAt($archive->getCreatedAt());
        $this->setUpdatedAt($archive->getUpdatedAt());

        return $this;
    }

    /**
     * Removes the object from the database without archiving it.
     *
     * @param ConnectionInterface $con Optional connection object
     *
     * @return $this|ChildServicerendu The current object (for fluent API support)
     */
    public function deleteWithoutArchive(ConnectionInterface $con = null)
    {
        $this->archiveOnDelete = false;

        return $this->delete($con);
    }

    /**
     * Code to be run before persisting the object
     * @param  ConnectionInterface $con
     * @return boolean
     */
    public function preSave(ConnectionInterface $con = null)
    {
        if (is_callable('parent::preSave')) {
            return parent::preSave($con);
        }
        return true;
    }

    /**
     * Code to be run after persisting the object
     * @param ConnectionInterface $con
     */
    public function postSave(ConnectionInterface $con = null)
    {
        if (is_callable('parent::postSave')) {
            parent::postSave($con);
        }
    }

    /**
     * Code to be run before inserting to database
     * @param  ConnectionInterface $con
     * @return boolean
     */
    public function preInsert(ConnectionInterface $con = null)
    {
        if (is_callable('parent::preInsert')) {
            return parent::preInsert($con);
        }
        return true;
    }

    /**
     * Code to be run after inserting to database
     * @param ConnectionInterface $con
     */
    public function postInsert(ConnectionInterface $con = null)
    {
        if (is_callable('parent::postInsert')) {
            parent::postInsert($con);
        }
    }

    /**
     * Code to be run before updating the object in database
     * @param  ConnectionInterface $con
     * @return boolean
     */
    public function preUpdate(ConnectionInterface $con = null)
    {
        if (is_callable('parent::preUpdate')) {
            return parent::preUpdate($con);
        }
        return true;
    }

    /**
     * Code to be run after updating the object in database
     * @param ConnectionInterface $con
     */
    public function postUpdate(ConnectionInterface $con = null)
    {
        if (is_callable('parent::postUpdate')) {
            parent::postUpdate($con);
        }
    }

    /**
     * Code to be run before deleting the object in database
     * @param  ConnectionInterface $con
     * @return boolean
     */
    public function preDelete(ConnectionInterface $con = null)
    {
        if (is_callable('parent::preDelete')) {
            return parent::preDelete($con);
        }
        return true;
    }

    /**
     * Code to be run after deleting the object in database
     * @param ConnectionInterface $con
     */
    public function postDelete(ConnectionInterface $con = null)
    {
        if (is_callable('parent::postDelete')) {
            parent::postDelete($con);
        }
    }


    /**
     * Derived method to catches calls to undefined methods.
     *
     * Provides magic import/export method support (fromXML()/toXML(), fromYAML()/toYAML(), etc.).
     * Allows to define default __call() behavior if you overwrite __call()
     *
     * @param string $name
     * @param mixed  $params
     *
     * @return array|string
     */
    public function __call($name, $params)
    {
        if (0 === strpos($name, 'get')) {
            $virtualColumn = substr($name, 3);
            if ($this->hasVirtualColumn($virtualColumn)) {
                return $this->getVirtualColumn($virtualColumn);
            }

            $virtualColumn = lcfirst($virtualColumn);
            if ($this->hasVirtualColumn($virtualColumn)) {
                return $this->getVirtualColumn($virtualColumn);
            }
        }

        if (0 === strpos($name, 'from')) {
            $format = substr($name, 4);

            return $this->importFrom($format, reset($params));
        }

        if (0 === strpos($name, 'to')) {
            $format = substr($name, 2);
            $includeLazyLoadColumns = isset($params[0]) ? $params[0] : true;

            return $this->exportTo($format, $includeLazyLoadColumns);
        }

        throw new BadMethodCallException(sprintf('Call to undefined method: %s.', $name));
    }

}
