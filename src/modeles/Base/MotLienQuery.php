<?php

namespace Base;

use \MotLien as ChildMotLien;
use \MotLienQuery as ChildMotLienQuery;
use \Exception;
use \PDO;
use Map\MotLienTableMap;
use Propel\Runtime\Propel;
use Propel\Runtime\ActiveQuery\Criteria;
use Propel\Runtime\ActiveQuery\ModelCriteria;
use Propel\Runtime\ActiveQuery\ModelJoin;
use Propel\Runtime\Collection\ObjectCollection;
use Propel\Runtime\Connection\ConnectionInterface;
use Propel\Runtime\Exception\PropelException;

/**
 * Base class that represents a query for the 'asso_mots_liens' table.
 *
 *
 *
 * @method     ChildMotLienQuery orderByIdMot($order = Criteria::ASC) Order by the id_mot column
 * @method     ChildMotLienQuery orderByIdObjet($order = Criteria::ASC) Order by the id_objet column
 * @method     ChildMotLienQuery orderByObjet($order = Criteria::ASC) Order by the objet column
 * @method     ChildMotLienQuery orderByObservation($order = Criteria::ASC) Order by the observation column
 *
 * @method     ChildMotLienQuery groupByIdMot() Group by the id_mot column
 * @method     ChildMotLienQuery groupByIdObjet() Group by the id_objet column
 * @method     ChildMotLienQuery groupByObjet() Group by the objet column
 * @method     ChildMotLienQuery groupByObservation() Group by the observation column
 *
 * @method     ChildMotLienQuery leftJoin($relation) Adds a LEFT JOIN clause to the query
 * @method     ChildMotLienQuery rightJoin($relation) Adds a RIGHT JOIN clause to the query
 * @method     ChildMotLienQuery innerJoin($relation) Adds a INNER JOIN clause to the query
 *
 * @method     ChildMotLienQuery leftJoinWith($relation) Adds a LEFT JOIN clause and with to the query
 * @method     ChildMotLienQuery rightJoinWith($relation) Adds a RIGHT JOIN clause and with to the query
 * @method     ChildMotLienQuery innerJoinWith($relation) Adds a INNER JOIN clause and with to the query
 *
 * @method     ChildMotLienQuery leftJoinMot($relationAlias = null) Adds a LEFT JOIN clause to the query using the Mot relation
 * @method     ChildMotLienQuery rightJoinMot($relationAlias = null) Adds a RIGHT JOIN clause to the query using the Mot relation
 * @method     ChildMotLienQuery innerJoinMot($relationAlias = null) Adds a INNER JOIN clause to the query using the Mot relation
 *
 * @method     ChildMotLienQuery joinWithMot($joinType = Criteria::INNER_JOIN) Adds a join clause and with to the query using the Mot relation
 *
 * @method     ChildMotLienQuery leftJoinWithMot() Adds a LEFT JOIN clause and with to the query using the Mot relation
 * @method     ChildMotLienQuery rightJoinWithMot() Adds a RIGHT JOIN clause and with to the query using the Mot relation
 * @method     ChildMotLienQuery innerJoinWithMot() Adds a INNER JOIN clause and with to the query using the Mot relation
 *
 * @method     \MotQuery endUse() Finalizes a secondary criteria and merges it with its primary Criteria
 *
 * @method     ChildMotLien findOne(ConnectionInterface $con = null) Return the first ChildMotLien matching the query
 * @method     ChildMotLien findOneOrCreate(ConnectionInterface $con = null) Return the first ChildMotLien matching the query, or a new ChildMotLien object populated from the query conditions when no match is found
 *
 * @method     ChildMotLien findOneByIdMot(string $id_mot) Return the first ChildMotLien filtered by the id_mot column
 * @method     ChildMotLien findOneByIdObjet(string $id_objet) Return the first ChildMotLien filtered by the id_objet column
 * @method     ChildMotLien findOneByObjet(string $objet) Return the first ChildMotLien filtered by the objet column
 * @method     ChildMotLien findOneByObservation(string $observation) Return the first ChildMotLien filtered by the observation column *

 * @method     ChildMotLien requirePk($key, ConnectionInterface $con = null) Return the ChildMotLien by primary key and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildMotLien requireOne(ConnectionInterface $con = null) Return the first ChildMotLien matching the query and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 *
 * @method     ChildMotLien requireOneByIdMot(string $id_mot) Return the first ChildMotLien filtered by the id_mot column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildMotLien requireOneByIdObjet(string $id_objet) Return the first ChildMotLien filtered by the id_objet column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildMotLien requireOneByObjet(string $objet) Return the first ChildMotLien filtered by the objet column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildMotLien requireOneByObservation(string $observation) Return the first ChildMotLien filtered by the observation column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 *
 * @method     ChildMotLien[]|ObjectCollection find(ConnectionInterface $con = null) Return ChildMotLien objects based on current ModelCriteria
 * @method     ChildMotLien[]|ObjectCollection findByIdMot(string $id_mot) Return ChildMotLien objects filtered by the id_mot column
 * @method     ChildMotLien[]|ObjectCollection findByIdObjet(string $id_objet) Return ChildMotLien objects filtered by the id_objet column
 * @method     ChildMotLien[]|ObjectCollection findByObjet(string $objet) Return ChildMotLien objects filtered by the objet column
 * @method     ChildMotLien[]|ObjectCollection findByObservation(string $observation) Return ChildMotLien objects filtered by the observation column
 * @method     ChildMotLien[]|\Propel\Runtime\Util\PropelModelPager paginate($page = 1, $maxPerPage = 10, ConnectionInterface $con = null) Issue a SELECT query based on the current ModelCriteria and uses a page and a maximum number of results per page to compute an offset and a limit
 *
 */
abstract class MotLienQuery extends ModelCriteria
{
    protected $entityNotFoundExceptionClass = '\\Propel\\Runtime\\Exception\\EntityNotFoundException';

    /**
     * Initializes internal state of \Base\MotLienQuery object.
     *
     * @param     string $dbName The database name
     * @param     string $modelName The phpName of a model, e.g. 'Book'
     * @param     string $modelAlias The alias for the model in this query, e.g. 'b'
     */
    public function __construct($dbName = 'simplasso', $modelName = '\\MotLien', $modelAlias = null)
    {
        parent::__construct($dbName, $modelName, $modelAlias);
    }

    /**
     * Returns a new ChildMotLienQuery object.
     *
     * @param     string $modelAlias The alias of a model in the query
     * @param     Criteria $criteria Optional Criteria to build the query from
     *
     * @return ChildMotLienQuery
     */
    public static function create($modelAlias = null, Criteria $criteria = null)
    {
        if ($criteria instanceof ChildMotLienQuery) {
            return $criteria;
        }
        $query = new ChildMotLienQuery();
        if (null !== $modelAlias) {
            $query->setModelAlias($modelAlias);
        }
        if ($criteria instanceof Criteria) {
            $query->mergeWith($criteria);
        }

        return $query;
    }

    /**
     * Find object by primary key.
     * Propel uses the instance pool to skip the database if the object exists.
     * Go fast if the query is untouched.
     *
     * <code>
     * $obj = $c->findPk(array(12, 34, 56), $con);
     * </code>
     *
     * @param array[$id_mot, $id_objet, $objet] $key Primary key to use for the query
     * @param ConnectionInterface $con an optional connection object
     *
     * @return ChildMotLien|array|mixed the result, formatted by the current formatter
     */
    public function findPk($key, ConnectionInterface $con = null)
    {
        if ($key === null) {
            return null;
        }

        if ($con === null) {
            $con = Propel::getServiceContainer()->getReadConnection(MotLienTableMap::DATABASE_NAME);
        }

        $this->basePreSelect($con);

        if (
            $this->formatter || $this->modelAlias || $this->with || $this->select
            || $this->selectColumns || $this->asColumns || $this->selectModifiers
            || $this->map || $this->having || $this->joins
        ) {
            return $this->findPkComplex($key, $con);
        }

        if ((null !== ($obj = MotLienTableMap::getInstanceFromPool(serialize([(null === $key[0] || is_scalar($key[0]) || is_callable([$key[0], '__toString']) ? (string) $key[0] : $key[0]), (null === $key[1] || is_scalar($key[1]) || is_callable([$key[1], '__toString']) ? (string) $key[1] : $key[1]), (null === $key[2] || is_scalar($key[2]) || is_callable([$key[2], '__toString']) ? (string) $key[2] : $key[2])]))))) {
            // the object is already in the instance pool
            return $obj;
        }

        return $this->findPkSimple($key, $con);
    }

    /**
     * Find object by primary key using raw SQL to go fast.
     * Bypass doSelect() and the object formatter by using generated code.
     *
     * @param     mixed $key Primary key to use for the query
     * @param     ConnectionInterface $con A connection object
     *
     * @throws \Propel\Runtime\Exception\PropelException
     *
     * @return ChildMotLien A model object, or null if the key is not found
     */
    protected function findPkSimple($key, ConnectionInterface $con)
    {
        $sql = 'SELECT `id_mot`, `id_objet`, `objet`, `observation` FROM `asso_mots_liens` WHERE `id_mot` = :p0 AND `id_objet` = :p1 AND `objet` = :p2';
        try {
            $stmt = $con->prepare($sql);
            $stmt->bindValue(':p0', $key[0], PDO::PARAM_INT);
            $stmt->bindValue(':p1', $key[1], PDO::PARAM_INT);
            $stmt->bindValue(':p2', $key[2], PDO::PARAM_STR);
            $stmt->execute();
        } catch (Exception $e) {
            Propel::log($e->getMessage(), Propel::LOG_ERR);
            throw new PropelException(sprintf('Unable to execute SELECT statement [%s]', $sql), 0, $e);
        }
        $obj = null;
        if ($row = $stmt->fetch(\PDO::FETCH_NUM)) {
            /** @var ChildMotLien $obj */
            $obj = new ChildMotLien();
            $obj->hydrate($row);
            MotLienTableMap::addInstanceToPool($obj, serialize([(null === $key[0] || is_scalar($key[0]) || is_callable([$key[0], '__toString']) ? (string) $key[0] : $key[0]), (null === $key[1] || is_scalar($key[1]) || is_callable([$key[1], '__toString']) ? (string) $key[1] : $key[1]), (null === $key[2] || is_scalar($key[2]) || is_callable([$key[2], '__toString']) ? (string) $key[2] : $key[2])]));
        }
        $stmt->closeCursor();

        return $obj;
    }

    /**
     * Find object by primary key.
     *
     * @param     mixed $key Primary key to use for the query
     * @param     ConnectionInterface $con A connection object
     *
     * @return ChildMotLien|array|mixed the result, formatted by the current formatter
     */
    protected function findPkComplex($key, ConnectionInterface $con)
    {
        // As the query uses a PK condition, no limit(1) is necessary.
        $criteria = $this->isKeepQuery() ? clone $this : $this;
        $dataFetcher = $criteria
            ->filterByPrimaryKey($key)
            ->doSelect($con);

        return $criteria->getFormatter()->init($criteria)->formatOne($dataFetcher);
    }

    /**
     * Find objects by primary key
     * <code>
     * $objs = $c->findPks(array(array(12, 56), array(832, 123), array(123, 456)), $con);
     * </code>
     * @param     array $keys Primary keys to use for the query
     * @param     ConnectionInterface $con an optional connection object
     *
     * @return ObjectCollection|array|mixed the list of results, formatted by the current formatter
     */
    public function findPks($keys, ConnectionInterface $con = null)
    {
        if (null === $con) {
            $con = Propel::getServiceContainer()->getReadConnection($this->getDbName());
        }
        $this->basePreSelect($con);
        $criteria = $this->isKeepQuery() ? clone $this : $this;
        $dataFetcher = $criteria
            ->filterByPrimaryKeys($keys)
            ->doSelect($con);

        return $criteria->getFormatter()->init($criteria)->format($dataFetcher);
    }

    /**
     * Filter the query by primary key
     *
     * @param     mixed $key Primary key to use for the query
     *
     * @return $this|ChildMotLienQuery The current query, for fluid interface
     */
    public function filterByPrimaryKey($key)
    {
        $this->addUsingAlias(MotLienTableMap::COL_ID_MOT, $key[0], Criteria::EQUAL);
        $this->addUsingAlias(MotLienTableMap::COL_ID_OBJET, $key[1], Criteria::EQUAL);
        $this->addUsingAlias(MotLienTableMap::COL_OBJET, $key[2], Criteria::EQUAL);

        return $this;
    }

    /**
     * Filter the query by a list of primary keys
     *
     * @param     array $keys The list of primary key to use for the query
     *
     * @return $this|ChildMotLienQuery The current query, for fluid interface
     */
    public function filterByPrimaryKeys($keys)
    {
        if (empty($keys)) {
            return $this->add(null, '1<>1', Criteria::CUSTOM);
        }
        foreach ($keys as $key) {
            $cton0 = $this->getNewCriterion(MotLienTableMap::COL_ID_MOT, $key[0], Criteria::EQUAL);
            $cton1 = $this->getNewCriterion(MotLienTableMap::COL_ID_OBJET, $key[1], Criteria::EQUAL);
            $cton0->addAnd($cton1);
            $cton2 = $this->getNewCriterion(MotLienTableMap::COL_OBJET, $key[2], Criteria::EQUAL);
            $cton0->addAnd($cton2);
            $this->addOr($cton0);
        }

        return $this;
    }

    /**
     * Filter the query on the id_mot column
     *
     * Example usage:
     * <code>
     * $query->filterByIdMot(1234); // WHERE id_mot = 1234
     * $query->filterByIdMot(array(12, 34)); // WHERE id_mot IN (12, 34)
     * $query->filterByIdMot(array('min' => 12)); // WHERE id_mot > 12
     * </code>
     *
     * @see       filterByMot()
     *
     * @param     mixed $idMot The value to use as filter.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildMotLienQuery The current query, for fluid interface
     */
    public function filterByIdMot($idMot = null, $comparison = null)
    {
        if (is_array($idMot)) {
            $useMinMax = false;
            if (isset($idMot['min'])) {
                $this->addUsingAlias(MotLienTableMap::COL_ID_MOT, $idMot['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($idMot['max'])) {
                $this->addUsingAlias(MotLienTableMap::COL_ID_MOT, $idMot['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(MotLienTableMap::COL_ID_MOT, $idMot, $comparison);
    }

    /**
     * Filter the query on the id_objet column
     *
     * Example usage:
     * <code>
     * $query->filterByIdObjet(1234); // WHERE id_objet = 1234
     * $query->filterByIdObjet(array(12, 34)); // WHERE id_objet IN (12, 34)
     * $query->filterByIdObjet(array('min' => 12)); // WHERE id_objet > 12
     * </code>
     *
     * @param     mixed $idObjet The value to use as filter.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildMotLienQuery The current query, for fluid interface
     */
    public function filterByIdObjet($idObjet = null, $comparison = null)
    {
        if (is_array($idObjet)) {
            $useMinMax = false;
            if (isset($idObjet['min'])) {
                $this->addUsingAlias(MotLienTableMap::COL_ID_OBJET, $idObjet['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($idObjet['max'])) {
                $this->addUsingAlias(MotLienTableMap::COL_ID_OBJET, $idObjet['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(MotLienTableMap::COL_ID_OBJET, $idObjet, $comparison);
    }

    /**
     * Filter the query on the objet column
     *
     * Example usage:
     * <code>
     * $query->filterByObjet('fooValue');   // WHERE objet = 'fooValue'
     * $query->filterByObjet('%fooValue%', Criteria::LIKE); // WHERE objet LIKE '%fooValue%'
     * </code>
     *
     * @param     string $objet The value to use as filter.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildMotLienQuery The current query, for fluid interface
     */
    public function filterByObjet($objet = null, $comparison = null)
    {
        if (null === $comparison) {
            if (is_array($objet)) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(MotLienTableMap::COL_OBJET, $objet, $comparison);
    }

    /**
     * Filter the query on the observation column
     *
     * Example usage:
     * <code>
     * $query->filterByObservation('fooValue');   // WHERE observation = 'fooValue'
     * $query->filterByObservation('%fooValue%', Criteria::LIKE); // WHERE observation LIKE '%fooValue%'
     * </code>
     *
     * @param     string $observation The value to use as filter.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildMotLienQuery The current query, for fluid interface
     */
    public function filterByObservation($observation = null, $comparison = null)
    {
        if (null === $comparison) {
            if (is_array($observation)) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(MotLienTableMap::COL_OBSERVATION, $observation, $comparison);
    }

    /**
     * Filter the query by a related \Mot object
     *
     * @param \Mot|ObjectCollection $mot The related object(s) to use as filter
     * @param string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @throws \Propel\Runtime\Exception\PropelException
     *
     * @return ChildMotLienQuery The current query, for fluid interface
     */
    public function filterByMot($mot, $comparison = null)
    {
        if ($mot instanceof \Mot) {
            return $this
                ->addUsingAlias(MotLienTableMap::COL_ID_MOT, $mot->getIdMot(), $comparison);
        } elseif ($mot instanceof ObjectCollection) {
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }

            return $this
                ->addUsingAlias(MotLienTableMap::COL_ID_MOT, $mot->toKeyValue('PrimaryKey', 'IdMot'), $comparison);
        } else {
            throw new PropelException('filterByMot() only accepts arguments of type \Mot or Collection');
        }
    }

    /**
     * Adds a JOIN clause to the query using the Mot relation
     *
     * @param     string $relationAlias optional alias for the relation
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return $this|ChildMotLienQuery The current query, for fluid interface
     */
    public function joinMot($relationAlias = null, $joinType = Criteria::INNER_JOIN)
    {
        $tableMap = $this->getTableMap();
        $relationMap = $tableMap->getRelation('Mot');

        // create a ModelJoin object for this join
        $join = new ModelJoin();
        $join->setJoinType($joinType);
        $join->setRelationMap($relationMap, $this->useAliasInSQL ? $this->getModelAlias() : null, $relationAlias);
        if ($previousJoin = $this->getPreviousJoin()) {
            $join->setPreviousJoin($previousJoin);
        }

        // add the ModelJoin to the current object
        if ($relationAlias) {
            $this->addAlias($relationAlias, $relationMap->getRightTable()->getName());
            $this->addJoinObject($join, $relationAlias);
        } else {
            $this->addJoinObject($join, 'Mot');
        }

        return $this;
    }

    /**
     * Use the Mot relation Mot object
     *
     * @see useQuery()
     *
     * @param     string $relationAlias optional alias for the relation,
     *                                   to be used as main alias in the secondary query
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return \MotQuery A secondary query class using the current class as primary query
     */
    public function useMotQuery($relationAlias = null, $joinType = Criteria::INNER_JOIN)
    {
        return $this
            ->joinMot($relationAlias, $joinType)
            ->useQuery($relationAlias ? $relationAlias : 'Mot', '\MotQuery');
    }

    /**
     * Exclude object from result
     *
     * @param   ChildMotLien $motLien Object to remove from the list of results
     *
     * @return $this|ChildMotLienQuery The current query, for fluid interface
     */
    public function prune($motLien = null)
    {
        if ($motLien) {
            $this->addCond('pruneCond0', $this->getAliasedColName(MotLienTableMap::COL_ID_MOT), $motLien->getIdMot(), Criteria::NOT_EQUAL);
            $this->addCond('pruneCond1', $this->getAliasedColName(MotLienTableMap::COL_ID_OBJET), $motLien->getIdObjet(), Criteria::NOT_EQUAL);
            $this->addCond('pruneCond2', $this->getAliasedColName(MotLienTableMap::COL_OBJET), $motLien->getObjet(), Criteria::NOT_EQUAL);
            $this->combine(array('pruneCond0', 'pruneCond1', 'pruneCond2'), Criteria::LOGICAL_OR);
        }

        return $this;
    }

    /**
     * Deletes all rows from the asso_mots_liens table.
     *
     * @param ConnectionInterface $con the connection to use
     * @return int The number of affected rows (if supported by underlying database driver).
     */
    public function doDeleteAll(ConnectionInterface $con = null)
    {
        if (null === $con) {
            $con = Propel::getServiceContainer()->getWriteConnection(MotLienTableMap::DATABASE_NAME);
        }

        // use transaction because $criteria could contain info
        // for more than one table or we could emulating ON DELETE CASCADE, etc.
        return $con->transaction(function () use ($con) {
            $affectedRows = 0; // initialize var to track total num of affected rows
            $affectedRows += parent::doDeleteAll($con);
            // Because this db requires some delete cascade/set null emulation, we have to
            // clear the cached instance *after* the emulation has happened (since
            // instances get re-added by the select statement contained therein).
            MotLienTableMap::clearInstancePool();
            MotLienTableMap::clearRelatedInstancePool();

            return $affectedRows;
        });
    }

    /**
     * Performs a DELETE on the database based on the current ModelCriteria
     *
     * @param ConnectionInterface $con the connection to use
     * @return int             The number of affected rows (if supported by underlying database driver).  This includes CASCADE-related rows
     *                         if supported by native driver or if emulated using Propel.
     * @throws PropelException Any exceptions caught during processing will be
     *                         rethrown wrapped into a PropelException.
     */
    public function delete(ConnectionInterface $con = null)
    {
        if (null === $con) {
            $con = Propel::getServiceContainer()->getWriteConnection(MotLienTableMap::DATABASE_NAME);
        }

        $criteria = $this;

        // Set the correct dbName
        $criteria->setDbName(MotLienTableMap::DATABASE_NAME);

        // use transaction because $criteria could contain info
        // for more than one table or we could emulating ON DELETE CASCADE, etc.
        return $con->transaction(function () use ($con, $criteria) {
            $affectedRows = 0; // initialize var to track total num of affected rows

            MotLienTableMap::removeInstanceFromPool($criteria);

            $affectedRows += ModelCriteria::delete($con);
            MotLienTableMap::clearRelatedInstancePool();

            return $affectedRows;
        });
    }

} // MotLienQuery
