<?php

namespace Base;

use \AssoPrestationslotsArchive as ChildAssoPrestationslotsArchive;
use \AssoPrestationslotsArchiveQuery as ChildAssoPrestationslotsArchiveQuery;
use \Entite as ChildEntite;
use \EntiteQuery as ChildEntiteQuery;
use \Prestation as ChildPrestation;
use \PrestationQuery as ChildPrestationQuery;
use \Prestationslot as ChildPrestationslot;
use \PrestationslotPrestation as ChildPrestationslotPrestation;
use \PrestationslotPrestationQuery as ChildPrestationslotPrestationQuery;
use \PrestationslotQuery as ChildPrestationslotQuery;
use \DateTime;
use \Exception;
use \PDO;
use Map\PrestationslotPrestationTableMap;
use Map\PrestationslotTableMap;
use Propel\Runtime\Propel;
use Propel\Runtime\ActiveQuery\Criteria;
use Propel\Runtime\ActiveQuery\ModelCriteria;
use Propel\Runtime\ActiveRecord\ActiveRecordInterface;
use Propel\Runtime\Collection\Collection;
use Propel\Runtime\Collection\ObjectCollection;
use Propel\Runtime\Connection\ConnectionInterface;
use Propel\Runtime\Exception\BadMethodCallException;
use Propel\Runtime\Exception\LogicException;
use Propel\Runtime\Exception\PropelException;
use Propel\Runtime\Map\TableMap;
use Propel\Runtime\Parser\AbstractParser;
use Propel\Runtime\Util\PropelDateTime;

/**
 * Base class that represents a row from the 'asso_prestationslots' table.
 *
 *
 *
 * @package    propel.generator..Base
 */
abstract class Prestationslot implements ActiveRecordInterface
{
    /**
     * TableMap class name
     */
    const TABLE_MAP = '\\Map\\PrestationslotTableMap';


    /**
     * attribute to determine if this object has previously been saved.
     * @var boolean
     */
    protected $new = true;

    /**
     * attribute to determine whether this object has been deleted.
     * @var boolean
     */
    protected $deleted = false;

    /**
     * The columns that have been modified in current object.
     * Tracking modified columns allows us to only update modified columns.
     * @var array
     */
    protected $modifiedColumns = array();

    /**
     * The (virtual) columns that are added at runtime
     * The formatters can add supplementary columns based on a resultset
     * @var array
     */
    protected $virtualColumns = array();

    /**
     * The value for the id_prestationslot field.
     *
     * @var        string
     */
    protected $id_prestationslot;

    /**
     * The value for the id_entite field.
     *
     * Note: this column has a database default value of: '1'
     * @var        string
     */
    protected $id_entite;

    /**
     * The value for the nom field.
     *
     * @var        string
     */
    protected $nom;

    /**
     * The value for the nomcourt field.
     *
     * @var        string
     */
    protected $nomcourt;

    /**
     * The value for the actif field.
     *
     * Note: this column has a database default value of: 1
     * @var        int
     */
    protected $actif;

    /**
     * The value for the observation field.
     *
     * @var        string
     */
    protected $observation;

    /**
     * The value for the created_at field.
     *
     * @var        DateTime
     */
    protected $created_at;

    /**
     * The value for the updated_at field.
     *
     * @var        DateTime
     */
    protected $updated_at;

    /**
     * @var        ChildEntite
     */
    protected $aEntite;

    /**
     * @var        ObjectCollection|ChildPrestationslotPrestation[] Collection to store aggregation of ChildPrestationslotPrestation objects.
     */
    protected $collPrestationslotPrestations;
    protected $collPrestationslotPrestationsPartial;

    /**
     * @var        ObjectCollection|ChildPrestation[] Cross Collection to store aggregation of ChildPrestation objects.
     */
    protected $collPrestations;

    /**
     * @var bool
     */
    protected $collPrestationsPartial;

    /**
     * Flag to prevent endless save loop, if this object is referenced
     * by another object which falls in this transaction.
     *
     * @var boolean
     */
    protected $alreadyInSave = false;

    // archivable behavior
    protected $archiveOnDelete = true;

    /**
     * An array of objects scheduled for deletion.
     * @var ObjectCollection|ChildPrestation[]
     */
    protected $prestationsScheduledForDeletion = null;

    /**
     * An array of objects scheduled for deletion.
     * @var ObjectCollection|ChildPrestationslotPrestation[]
     */
    protected $prestationslotPrestationsScheduledForDeletion = null;

    /**
     * Applies default values to this object.
     * This method should be called from the object's constructor (or
     * equivalent initialization method).
     * @see __construct()
     */
    public function applyDefaultValues()
    {
        $this->id_entite = '1';
        $this->actif = 1;
    }

    /**
     * Initializes internal state of Base\Prestationslot object.
     * @see applyDefaults()
     */
    public function __construct()
    {
        $this->applyDefaultValues();
    }

    /**
     * Returns whether the object has been modified.
     *
     * @return boolean True if the object has been modified.
     */
    public function isModified()
    {
        return !!$this->modifiedColumns;
    }

    /**
     * Has specified column been modified?
     *
     * @param  string  $col column fully qualified name (TableMap::TYPE_COLNAME), e.g. Book::AUTHOR_ID
     * @return boolean True if $col has been modified.
     */
    public function isColumnModified($col)
    {
        return $this->modifiedColumns && isset($this->modifiedColumns[$col]);
    }

    /**
     * Get the columns that have been modified in this object.
     * @return array A unique list of the modified column names for this object.
     */
    public function getModifiedColumns()
    {
        return $this->modifiedColumns ? array_keys($this->modifiedColumns) : [];
    }

    /**
     * Returns whether the object has ever been saved.  This will
     * be false, if the object was retrieved from storage or was created
     * and then saved.
     *
     * @return boolean true, if the object has never been persisted.
     */
    public function isNew()
    {
        return $this->new;
    }

    /**
     * Setter for the isNew attribute.  This method will be called
     * by Propel-generated children and objects.
     *
     * @param boolean $b the state of the object.
     */
    public function setNew($b)
    {
        $this->new = (boolean) $b;
    }

    /**
     * Whether this object has been deleted.
     * @return boolean The deleted state of this object.
     */
    public function isDeleted()
    {
        return $this->deleted;
    }

    /**
     * Specify whether this object has been deleted.
     * @param  boolean $b The deleted state of this object.
     * @return void
     */
    public function setDeleted($b)
    {
        $this->deleted = (boolean) $b;
    }

    /**
     * Sets the modified state for the object to be false.
     * @param  string $col If supplied, only the specified column is reset.
     * @return void
     */
    public function resetModified($col = null)
    {
        if (null !== $col) {
            if (isset($this->modifiedColumns[$col])) {
                unset($this->modifiedColumns[$col]);
            }
        } else {
            $this->modifiedColumns = array();
        }
    }

    /**
     * Compares this with another <code>Prestationslot</code> instance.  If
     * <code>obj</code> is an instance of <code>Prestationslot</code>, delegates to
     * <code>equals(Prestationslot)</code>.  Otherwise, returns <code>false</code>.
     *
     * @param  mixed   $obj The object to compare to.
     * @return boolean Whether equal to the object specified.
     */
    public function equals($obj)
    {
        if (!$obj instanceof static) {
            return false;
        }

        if ($this === $obj) {
            return true;
        }

        if (null === $this->getPrimaryKey() || null === $obj->getPrimaryKey()) {
            return false;
        }

        return $this->getPrimaryKey() === $obj->getPrimaryKey();
    }

    /**
     * Get the associative array of the virtual columns in this object
     *
     * @return array
     */
    public function getVirtualColumns()
    {
        return $this->virtualColumns;
    }

    /**
     * Checks the existence of a virtual column in this object
     *
     * @param  string  $name The virtual column name
     * @return boolean
     */
    public function hasVirtualColumn($name)
    {
        return array_key_exists($name, $this->virtualColumns);
    }

    /**
     * Get the value of a virtual column in this object
     *
     * @param  string $name The virtual column name
     * @return mixed
     *
     * @throws PropelException
     */
    public function getVirtualColumn($name)
    {
        if (!$this->hasVirtualColumn($name)) {
            throw new PropelException(sprintf('Cannot get value of inexistent virtual column %s.', $name));
        }

        return $this->virtualColumns[$name];
    }

    /**
     * Set the value of a virtual column in this object
     *
     * @param string $name  The virtual column name
     * @param mixed  $value The value to give to the virtual column
     *
     * @return $this|Prestationslot The current object, for fluid interface
     */
    public function setVirtualColumn($name, $value)
    {
        $this->virtualColumns[$name] = $value;

        return $this;
    }

    /**
     * Logs a message using Propel::log().
     *
     * @param  string  $msg
     * @param  int     $priority One of the Propel::LOG_* logging levels
     * @return boolean
     */
    protected function log($msg, $priority = Propel::LOG_INFO)
    {
        return Propel::log(get_class($this) . ': ' . $msg, $priority);
    }

    /**
     * Export the current object properties to a string, using a given parser format
     * <code>
     * $book = BookQuery::create()->findPk(9012);
     * echo $book->exportTo('JSON');
     *  => {"Id":9012,"Title":"Don Juan","ISBN":"0140422161","Price":12.99,"PublisherId":1234,"AuthorId":5678}');
     * </code>
     *
     * @param  mixed   $parser                 A AbstractParser instance, or a format name ('XML', 'YAML', 'JSON', 'CSV')
     * @param  boolean $includeLazyLoadColumns (optional) Whether to include lazy load(ed) columns. Defaults to TRUE.
     * @return string  The exported data
     */
    public function exportTo($parser, $includeLazyLoadColumns = true)
    {
        if (!$parser instanceof AbstractParser) {
            $parser = AbstractParser::getParser($parser);
        }

        return $parser->fromArray($this->toArray(TableMap::TYPE_PHPNAME, $includeLazyLoadColumns, array(), true));
    }

    /**
     * Clean up internal collections prior to serializing
     * Avoids recursive loops that turn into segmentation faults when serializing
     */
    public function __sleep()
    {
        $this->clearAllReferences();

        $cls = new \ReflectionClass($this);
        $propertyNames = [];
        $serializableProperties = array_diff($cls->getProperties(), $cls->getProperties(\ReflectionProperty::IS_STATIC));

        foreach($serializableProperties as $property) {
            $propertyNames[] = $property->getName();
        }

        return $propertyNames;
    }

    /**
     * Get the [id_prestationslot] column value.
     *
     * @return string
     */
    public function getIdPrestationslot()
    {
        return $this->id_prestationslot;
    }

    /**
     * Get the [id_entite] column value.
     *
     * @return string
     */
    public function getIdEntite()
    {
        return $this->id_entite;
    }

    /**
     * Get the [nom] column value.
     *
     * @return string
     */
    public function getNom()
    {
        return $this->nom;
    }

    /**
     * Get the [nomcourt] column value.
     *
     * @return string
     */
    public function getNomcourt()
    {
        return $this->nomcourt;
    }

    /**
     * Get the [actif] column value.
     *
     * @return int
     */
    public function getActif()
    {
        return $this->actif;
    }

    /**
     * Get the [observation] column value.
     *
     * @return string
     */
    public function getObservation()
    {
        return $this->observation;
    }

    /**
     * Get the [optionally formatted] temporal [created_at] column value.
     *
     *
     * @param      string $format The date/time format string (either date()-style or strftime()-style).
     *                            If format is NULL, then the raw DateTime object will be returned.
     *
     * @return string|DateTime Formatted date/time value as string or DateTime object (if format is NULL), NULL if column is NULL, and 0 if column value is 0000-00-00 00:00:00
     *
     * @throws PropelException - if unable to parse/validate the date/time value.
     */
    public function getCreatedAt($format = NULL)
    {
        if ($format === null) {
            return $this->created_at;
        } else {
            return $this->created_at instanceof \DateTimeInterface ? $this->created_at->format($format) : null;
        }
    }

    /**
     * Get the [optionally formatted] temporal [updated_at] column value.
     *
     *
     * @param      string $format The date/time format string (either date()-style or strftime()-style).
     *                            If format is NULL, then the raw DateTime object will be returned.
     *
     * @return string|DateTime Formatted date/time value as string or DateTime object (if format is NULL), NULL if column is NULL, and 0 if column value is 0000-00-00 00:00:00
     *
     * @throws PropelException - if unable to parse/validate the date/time value.
     */
    public function getUpdatedAt($format = NULL)
    {
        if ($format === null) {
            return $this->updated_at;
        } else {
            return $this->updated_at instanceof \DateTimeInterface ? $this->updated_at->format($format) : null;
        }
    }

    /**
     * Set the value of [id_prestationslot] column.
     *
     * @param string $v new value
     * @return $this|\Prestationslot The current object (for fluent API support)
     */
    public function setIdPrestationslot($v)
    {
        if ($v !== null) {
            $v = (string) $v;
        }

        if ($this->id_prestationslot !== $v) {
            $this->id_prestationslot = $v;
            $this->modifiedColumns[PrestationslotTableMap::COL_ID_PRESTATIONSLOT] = true;
        }

        return $this;
    } // setIdPrestationslot()

    /**
     * Set the value of [id_entite] column.
     *
     * @param string $v new value
     * @return $this|\Prestationslot The current object (for fluent API support)
     */
    public function setIdEntite($v)
    {
        if ($v !== null) {
            $v = (string) $v;
        }

        if ($this->id_entite !== $v) {
            $this->id_entite = $v;
            $this->modifiedColumns[PrestationslotTableMap::COL_ID_ENTITE] = true;
        }

        if ($this->aEntite !== null && $this->aEntite->getIdEntite() !== $v) {
            $this->aEntite = null;
        }

        return $this;
    } // setIdEntite()

    /**
     * Set the value of [nom] column.
     *
     * @param string $v new value
     * @return $this|\Prestationslot The current object (for fluent API support)
     */
    public function setNom($v)
    {
        if ($v !== null) {
            $v = (string) $v;
        }

        if ($this->nom !== $v) {
            $this->nom = $v;
            $this->modifiedColumns[PrestationslotTableMap::COL_NOM] = true;
        }

        return $this;
    } // setNom()

    /**
     * Set the value of [nomcourt] column.
     *
     * @param string $v new value
     * @return $this|\Prestationslot The current object (for fluent API support)
     */
    public function setNomcourt($v)
    {
        if ($v !== null) {
            $v = (string) $v;
        }

        if ($this->nomcourt !== $v) {
            $this->nomcourt = $v;
            $this->modifiedColumns[PrestationslotTableMap::COL_NOMCOURT] = true;
        }

        return $this;
    } // setNomcourt()

    /**
     * Set the value of [actif] column.
     *
     * @param int $v new value
     * @return $this|\Prestationslot The current object (for fluent API support)
     */
    public function setActif($v)
    {
        if ($v !== null) {
            $v = (int) $v;
        }

        if ($this->actif !== $v) {
            $this->actif = $v;
            $this->modifiedColumns[PrestationslotTableMap::COL_ACTIF] = true;
        }

        return $this;
    } // setActif()

    /**
     * Set the value of [observation] column.
     *
     * @param string $v new value
     * @return $this|\Prestationslot The current object (for fluent API support)
     */
    public function setObservation($v)
    {
        if ($v !== null) {
            $v = (string) $v;
        }

        if ($this->observation !== $v) {
            $this->observation = $v;
            $this->modifiedColumns[PrestationslotTableMap::COL_OBSERVATION] = true;
        }

        return $this;
    } // setObservation()

    /**
     * Sets the value of [created_at] column to a normalized version of the date/time value specified.
     *
     * @param  mixed $v string, integer (timestamp), or \DateTimeInterface value.
     *               Empty strings are treated as NULL.
     * @return $this|\Prestationslot The current object (for fluent API support)
     */
    public function setCreatedAt($v)
    {
        $dt = PropelDateTime::newInstance($v, null, 'DateTime');
        if ($this->created_at !== null || $dt !== null) {
            if ($this->created_at === null || $dt === null || $dt->format("Y-m-d H:i:s.u") !== $this->created_at->format("Y-m-d H:i:s.u")) {
                $this->created_at = $dt === null ? null : clone $dt;
                $this->modifiedColumns[PrestationslotTableMap::COL_CREATED_AT] = true;
            }
        } // if either are not null

        return $this;
    } // setCreatedAt()

    /**
     * Sets the value of [updated_at] column to a normalized version of the date/time value specified.
     *
     * @param  mixed $v string, integer (timestamp), or \DateTimeInterface value.
     *               Empty strings are treated as NULL.
     * @return $this|\Prestationslot The current object (for fluent API support)
     */
    public function setUpdatedAt($v)
    {
        $dt = PropelDateTime::newInstance($v, null, 'DateTime');
        if ($this->updated_at !== null || $dt !== null) {
            if ($this->updated_at === null || $dt === null || $dt->format("Y-m-d H:i:s.u") !== $this->updated_at->format("Y-m-d H:i:s.u")) {
                $this->updated_at = $dt === null ? null : clone $dt;
                $this->modifiedColumns[PrestationslotTableMap::COL_UPDATED_AT] = true;
            }
        } // if either are not null

        return $this;
    } // setUpdatedAt()

    /**
     * Indicates whether the columns in this object are only set to default values.
     *
     * This method can be used in conjunction with isModified() to indicate whether an object is both
     * modified _and_ has some values set which are non-default.
     *
     * @return boolean Whether the columns in this object are only been set with default values.
     */
    public function hasOnlyDefaultValues()
    {
            if ($this->id_entite !== '1') {
                return false;
            }

            if ($this->actif !== 1) {
                return false;
            }

        // otherwise, everything was equal, so return TRUE
        return true;
    } // hasOnlyDefaultValues()

    /**
     * Hydrates (populates) the object variables with values from the database resultset.
     *
     * An offset (0-based "start column") is specified so that objects can be hydrated
     * with a subset of the columns in the resultset rows.  This is needed, for example,
     * for results of JOIN queries where the resultset row includes columns from two or
     * more tables.
     *
     * @param array   $row       The row returned by DataFetcher->fetch().
     * @param int     $startcol  0-based offset column which indicates which restultset column to start with.
     * @param boolean $rehydrate Whether this object is being re-hydrated from the database.
     * @param string  $indexType The index type of $row. Mostly DataFetcher->getIndexType().
                                  One of the class type constants TableMap::TYPE_PHPNAME, TableMap::TYPE_CAMELNAME
     *                            TableMap::TYPE_COLNAME, TableMap::TYPE_FIELDNAME, TableMap::TYPE_NUM.
     *
     * @return int             next starting column
     * @throws PropelException - Any caught Exception will be rewrapped as a PropelException.
     */
    public function hydrate($row, $startcol = 0, $rehydrate = false, $indexType = TableMap::TYPE_NUM)
    {
        try {

            $col = $row[TableMap::TYPE_NUM == $indexType ? 0 + $startcol : PrestationslotTableMap::translateFieldName('IdPrestationslot', TableMap::TYPE_PHPNAME, $indexType)];
            $this->id_prestationslot = (null !== $col) ? (string) $col : null;

            $col = $row[TableMap::TYPE_NUM == $indexType ? 1 + $startcol : PrestationslotTableMap::translateFieldName('IdEntite', TableMap::TYPE_PHPNAME, $indexType)];
            $this->id_entite = (null !== $col) ? (string) $col : null;

            $col = $row[TableMap::TYPE_NUM == $indexType ? 2 + $startcol : PrestationslotTableMap::translateFieldName('Nom', TableMap::TYPE_PHPNAME, $indexType)];
            $this->nom = (null !== $col) ? (string) $col : null;

            $col = $row[TableMap::TYPE_NUM == $indexType ? 3 + $startcol : PrestationslotTableMap::translateFieldName('Nomcourt', TableMap::TYPE_PHPNAME, $indexType)];
            $this->nomcourt = (null !== $col) ? (string) $col : null;

            $col = $row[TableMap::TYPE_NUM == $indexType ? 4 + $startcol : PrestationslotTableMap::translateFieldName('Actif', TableMap::TYPE_PHPNAME, $indexType)];
            $this->actif = (null !== $col) ? (int) $col : null;

            $col = $row[TableMap::TYPE_NUM == $indexType ? 5 + $startcol : PrestationslotTableMap::translateFieldName('Observation', TableMap::TYPE_PHPNAME, $indexType)];
            $this->observation = (null !== $col) ? (string) $col : null;

            $col = $row[TableMap::TYPE_NUM == $indexType ? 6 + $startcol : PrestationslotTableMap::translateFieldName('CreatedAt', TableMap::TYPE_PHPNAME, $indexType)];
            if ($col === '0000-00-00 00:00:00') {
                $col = null;
            }
            $this->created_at = (null !== $col) ? PropelDateTime::newInstance($col, null, 'DateTime') : null;

            $col = $row[TableMap::TYPE_NUM == $indexType ? 7 + $startcol : PrestationslotTableMap::translateFieldName('UpdatedAt', TableMap::TYPE_PHPNAME, $indexType)];
            if ($col === '0000-00-00 00:00:00') {
                $col = null;
            }
            $this->updated_at = (null !== $col) ? PropelDateTime::newInstance($col, null, 'DateTime') : null;
            $this->resetModified();

            $this->setNew(false);

            if ($rehydrate) {
                $this->ensureConsistency();
            }

            return $startcol + 8; // 8 = PrestationslotTableMap::NUM_HYDRATE_COLUMNS.

        } catch (Exception $e) {
            throw new PropelException(sprintf('Error populating %s object', '\\Prestationslot'), 0, $e);
        }
    }

    /**
     * Checks and repairs the internal consistency of the object.
     *
     * This method is executed after an already-instantiated object is re-hydrated
     * from the database.  It exists to check any foreign keys to make sure that
     * the objects related to the current object are correct based on foreign key.
     *
     * You can override this method in the stub class, but you should always invoke
     * the base method from the overridden method (i.e. parent::ensureConsistency()),
     * in case your model changes.
     *
     * @throws PropelException
     */
    public function ensureConsistency()
    {
        if ($this->aEntite !== null && $this->id_entite !== $this->aEntite->getIdEntite()) {
            $this->aEntite = null;
        }
    } // ensureConsistency

    /**
     * Reloads this object from datastore based on primary key and (optionally) resets all associated objects.
     *
     * This will only work if the object has been saved and has a valid primary key set.
     *
     * @param      boolean $deep (optional) Whether to also de-associated any related objects.
     * @param      ConnectionInterface $con (optional) The ConnectionInterface connection to use.
     * @return void
     * @throws PropelException - if this object is deleted, unsaved or doesn't have pk match in db
     */
    public function reload($deep = false, ConnectionInterface $con = null)
    {
        if ($this->isDeleted()) {
            throw new PropelException("Cannot reload a deleted object.");
        }

        if ($this->isNew()) {
            throw new PropelException("Cannot reload an unsaved object.");
        }

        if ($con === null) {
            $con = Propel::getServiceContainer()->getReadConnection(PrestationslotTableMap::DATABASE_NAME);
        }

        // We don't need to alter the object instance pool; we're just modifying this instance
        // already in the pool.

        $dataFetcher = ChildPrestationslotQuery::create(null, $this->buildPkeyCriteria())->setFormatter(ModelCriteria::FORMAT_STATEMENT)->find($con);
        $row = $dataFetcher->fetch();
        $dataFetcher->close();
        if (!$row) {
            throw new PropelException('Cannot find matching row in the database to reload object values.');
        }
        $this->hydrate($row, 0, true, $dataFetcher->getIndexType()); // rehydrate

        if ($deep) {  // also de-associate any related objects?

            $this->aEntite = null;
            $this->collPrestationslotPrestations = null;

            $this->collPrestations = null;
        } // if (deep)
    }

    /**
     * Removes this object from datastore and sets delete attribute.
     *
     * @param      ConnectionInterface $con
     * @return void
     * @throws PropelException
     * @see Prestationslot::setDeleted()
     * @see Prestationslot::isDeleted()
     */
    public function delete(ConnectionInterface $con = null)
    {
        if ($this->isDeleted()) {
            throw new PropelException("This object has already been deleted.");
        }

        if ($con === null) {
            $con = Propel::getServiceContainer()->getWriteConnection(PrestationslotTableMap::DATABASE_NAME);
        }

        $con->transaction(function () use ($con) {
            $deleteQuery = ChildPrestationslotQuery::create()
                ->filterByPrimaryKey($this->getPrimaryKey());
            $ret = $this->preDelete($con);
            // archivable behavior
            if ($ret) {
                if ($this->archiveOnDelete) {
                    // do nothing yet. The object will be archived later when calling ChildPrestationslotQuery::delete().
                } else {
                    $deleteQuery->setArchiveOnDelete(false);
                    $this->archiveOnDelete = true;
                }
            }

            if ($ret) {
                $deleteQuery->delete($con);
                $this->postDelete($con);
                $this->setDeleted(true);
            }
        });
    }

    /**
     * Persists this object to the database.
     *
     * If the object is new, it inserts it; otherwise an update is performed.
     * All modified related objects will also be persisted in the doSave()
     * method.  This method wraps all precipitate database operations in a
     * single transaction.
     *
     * @param      ConnectionInterface $con
     * @return int             The number of rows affected by this insert/update and any referring fk objects' save() operations.
     * @throws PropelException
     * @see doSave()
     */
    public function save(ConnectionInterface $con = null)
    {
        if ($this->isDeleted()) {
            throw new PropelException("You cannot save an object that has been deleted.");
        }

        if ($this->alreadyInSave) {
            return 0;
        }

        if ($con === null) {
            $con = Propel::getServiceContainer()->getWriteConnection(PrestationslotTableMap::DATABASE_NAME);
        }

        return $con->transaction(function () use ($con) {
            $ret = $this->preSave($con);
            $isInsert = $this->isNew();
            if ($isInsert) {
                $ret = $ret && $this->preInsert($con);
                // timestampable behavior

                if (!$this->isColumnModified(PrestationslotTableMap::COL_CREATED_AT)) {
                    $this->setCreatedAt(\Propel\Runtime\Util\PropelDateTime::createHighPrecision());
                }
                if (!$this->isColumnModified(PrestationslotTableMap::COL_UPDATED_AT)) {
                    $this->setUpdatedAt(\Propel\Runtime\Util\PropelDateTime::createHighPrecision());
                }
            } else {
                $ret = $ret && $this->preUpdate($con);
                // timestampable behavior
                if ($this->isModified() && !$this->isColumnModified(PrestationslotTableMap::COL_UPDATED_AT)) {
                    $this->setUpdatedAt(\Propel\Runtime\Util\PropelDateTime::createHighPrecision());
                }
            }
            if ($ret) {
                $affectedRows = $this->doSave($con);
                if ($isInsert) {
                    $this->postInsert($con);
                } else {
                    $this->postUpdate($con);
                }
                $this->postSave($con);
                PrestationslotTableMap::addInstanceToPool($this);
            } else {
                $affectedRows = 0;
            }

            return $affectedRows;
        });
    }

    /**
     * Performs the work of inserting or updating the row in the database.
     *
     * If the object is new, it inserts it; otherwise an update is performed.
     * All related objects are also updated in this method.
     *
     * @param      ConnectionInterface $con
     * @return int             The number of rows affected by this insert/update and any referring fk objects' save() operations.
     * @throws PropelException
     * @see save()
     */
    protected function doSave(ConnectionInterface $con)
    {
        $affectedRows = 0; // initialize var to track total num of affected rows
        if (!$this->alreadyInSave) {
            $this->alreadyInSave = true;

            // We call the save method on the following object(s) if they
            // were passed to this object by their corresponding set
            // method.  This object relates to these object(s) by a
            // foreign key reference.

            if ($this->aEntite !== null) {
                if ($this->aEntite->isModified() || $this->aEntite->isNew()) {
                    $affectedRows += $this->aEntite->save($con);
                }
                $this->setEntite($this->aEntite);
            }

            if ($this->isNew() || $this->isModified()) {
                // persist changes
                if ($this->isNew()) {
                    $this->doInsert($con);
                    $affectedRows += 1;
                } else {
                    $affectedRows += $this->doUpdate($con);
                }
                $this->resetModified();
            }

            if ($this->prestationsScheduledForDeletion !== null) {
                if (!$this->prestationsScheduledForDeletion->isEmpty()) {
                    $pks = array();
                    foreach ($this->prestationsScheduledForDeletion as $entry) {
                        $entryPk = [];

                        $entryPk[0] = $this->getIdPrestationslot();
                        $entryPk[1] = $entry->getIdPrestation();
                        $pks[] = $entryPk;
                    }

                    \PrestationslotPrestationQuery::create()
                        ->filterByPrimaryKeys($pks)
                        ->delete($con);

                    $this->prestationsScheduledForDeletion = null;
                }

            }

            if ($this->collPrestations) {
                foreach ($this->collPrestations as $prestation) {
                    if (!$prestation->isDeleted() && ($prestation->isNew() || $prestation->isModified())) {
                        $prestation->save($con);
                    }
                }
            }


            if ($this->prestationslotPrestationsScheduledForDeletion !== null) {
                if (!$this->prestationslotPrestationsScheduledForDeletion->isEmpty()) {
                    \PrestationslotPrestationQuery::create()
                        ->filterByPrimaryKeys($this->prestationslotPrestationsScheduledForDeletion->getPrimaryKeys(false))
                        ->delete($con);
                    $this->prestationslotPrestationsScheduledForDeletion = null;
                }
            }

            if ($this->collPrestationslotPrestations !== null) {
                foreach ($this->collPrestationslotPrestations as $referrerFK) {
                    if (!$referrerFK->isDeleted() && ($referrerFK->isNew() || $referrerFK->isModified())) {
                        $affectedRows += $referrerFK->save($con);
                    }
                }
            }

            $this->alreadyInSave = false;

        }

        return $affectedRows;
    } // doSave()

    /**
     * Insert the row in the database.
     *
     * @param      ConnectionInterface $con
     *
     * @throws PropelException
     * @see doSave()
     */
    protected function doInsert(ConnectionInterface $con)
    {
        $modifiedColumns = array();
        $index = 0;

        $this->modifiedColumns[PrestationslotTableMap::COL_ID_PRESTATIONSLOT] = true;
        if (null !== $this->id_prestationslot) {
            throw new PropelException('Cannot insert a value for auto-increment primary key (' . PrestationslotTableMap::COL_ID_PRESTATIONSLOT . ')');
        }

         // check the columns in natural order for more readable SQL queries
        if ($this->isColumnModified(PrestationslotTableMap::COL_ID_PRESTATIONSLOT)) {
            $modifiedColumns[':p' . $index++]  = '`id_prestationslot`';
        }
        if ($this->isColumnModified(PrestationslotTableMap::COL_ID_ENTITE)) {
            $modifiedColumns[':p' . $index++]  = '`id_entite`';
        }
        if ($this->isColumnModified(PrestationslotTableMap::COL_NOM)) {
            $modifiedColumns[':p' . $index++]  = '`nom`';
        }
        if ($this->isColumnModified(PrestationslotTableMap::COL_NOMCOURT)) {
            $modifiedColumns[':p' . $index++]  = '`nomcourt`';
        }
        if ($this->isColumnModified(PrestationslotTableMap::COL_ACTIF)) {
            $modifiedColumns[':p' . $index++]  = '`actif`';
        }
        if ($this->isColumnModified(PrestationslotTableMap::COL_OBSERVATION)) {
            $modifiedColumns[':p' . $index++]  = '`observation`';
        }
        if ($this->isColumnModified(PrestationslotTableMap::COL_CREATED_AT)) {
            $modifiedColumns[':p' . $index++]  = '`created_at`';
        }
        if ($this->isColumnModified(PrestationslotTableMap::COL_UPDATED_AT)) {
            $modifiedColumns[':p' . $index++]  = '`updated_at`';
        }

        $sql = sprintf(
            'INSERT INTO `asso_prestationslots` (%s) VALUES (%s)',
            implode(', ', $modifiedColumns),
            implode(', ', array_keys($modifiedColumns))
        );

        try {
            $stmt = $con->prepare($sql);
            foreach ($modifiedColumns as $identifier => $columnName) {
                switch ($columnName) {
                    case '`id_prestationslot`':
                        $stmt->bindValue($identifier, $this->id_prestationslot, PDO::PARAM_INT);
                        break;
                    case '`id_entite`':
                        $stmt->bindValue($identifier, $this->id_entite, PDO::PARAM_INT);
                        break;
                    case '`nom`':
                        $stmt->bindValue($identifier, $this->nom, PDO::PARAM_STR);
                        break;
                    case '`nomcourt`':
                        $stmt->bindValue($identifier, $this->nomcourt, PDO::PARAM_STR);
                        break;
                    case '`actif`':
                        $stmt->bindValue($identifier, $this->actif, PDO::PARAM_INT);
                        break;
                    case '`observation`':
                        $stmt->bindValue($identifier, $this->observation, PDO::PARAM_STR);
                        break;
                    case '`created_at`':
                        $stmt->bindValue($identifier, $this->created_at ? $this->created_at->format("Y-m-d H:i:s.u") : null, PDO::PARAM_STR);
                        break;
                    case '`updated_at`':
                        $stmt->bindValue($identifier, $this->updated_at ? $this->updated_at->format("Y-m-d H:i:s.u") : null, PDO::PARAM_STR);
                        break;
                }
            }
            $stmt->execute();
        } catch (Exception $e) {
            Propel::log($e->getMessage(), Propel::LOG_ERR);
            throw new PropelException(sprintf('Unable to execute INSERT statement [%s]', $sql), 0, $e);
        }

        try {
            $pk = $con->lastInsertId();
        } catch (Exception $e) {
            throw new PropelException('Unable to get autoincrement id.', 0, $e);
        }
        $this->setIdPrestationslot($pk);

        $this->setNew(false);
    }

    /**
     * Update the row in the database.
     *
     * @param      ConnectionInterface $con
     *
     * @return Integer Number of updated rows
     * @see doSave()
     */
    protected function doUpdate(ConnectionInterface $con)
    {
        $selectCriteria = $this->buildPkeyCriteria();
        $valuesCriteria = $this->buildCriteria();

        return $selectCriteria->doUpdate($valuesCriteria, $con);
    }

    /**
     * Retrieves a field from the object by name passed in as a string.
     *
     * @param      string $name name
     * @param      string $type The type of fieldname the $name is of:
     *                     one of the class type constants TableMap::TYPE_PHPNAME, TableMap::TYPE_CAMELNAME
     *                     TableMap::TYPE_COLNAME, TableMap::TYPE_FIELDNAME, TableMap::TYPE_NUM.
     *                     Defaults to TableMap::TYPE_FIELDNAME.
     * @return mixed Value of field.
     */
    public function getByName($name, $type = TableMap::TYPE_FIELDNAME)
    {
        $pos = PrestationslotTableMap::translateFieldName($name, $type, TableMap::TYPE_NUM);
        $field = $this->getByPosition($pos);

        return $field;
    }

    /**
     * Retrieves a field from the object by Position as specified in the xml schema.
     * Zero-based.
     *
     * @param      int $pos position in xml schema
     * @return mixed Value of field at $pos
     */
    public function getByPosition($pos)
    {
        switch ($pos) {
            case 0:
                return $this->getIdPrestationslot();
                break;
            case 1:
                return $this->getIdEntite();
                break;
            case 2:
                return $this->getNom();
                break;
            case 3:
                return $this->getNomcourt();
                break;
            case 4:
                return $this->getActif();
                break;
            case 5:
                return $this->getObservation();
                break;
            case 6:
                return $this->getCreatedAt();
                break;
            case 7:
                return $this->getUpdatedAt();
                break;
            default:
                return null;
                break;
        } // switch()
    }

    /**
     * Exports the object as an array.
     *
     * You can specify the key type of the array by passing one of the class
     * type constants.
     *
     * @param     string  $keyType (optional) One of the class type constants TableMap::TYPE_PHPNAME, TableMap::TYPE_CAMELNAME,
     *                    TableMap::TYPE_COLNAME, TableMap::TYPE_FIELDNAME, TableMap::TYPE_NUM.
     *                    Defaults to TableMap::TYPE_FIELDNAME.
     * @param     boolean $includeLazyLoadColumns (optional) Whether to include lazy loaded columns. Defaults to TRUE.
     * @param     array $alreadyDumpedObjects List of objects to skip to avoid recursion
     * @param     boolean $includeForeignObjects (optional) Whether to include hydrated related objects. Default to FALSE.
     *
     * @return array an associative array containing the field names (as keys) and field values
     */
    public function toArray($keyType = TableMap::TYPE_FIELDNAME, $includeLazyLoadColumns = true, $alreadyDumpedObjects = array(), $includeForeignObjects = false)
    {

        if (isset($alreadyDumpedObjects['Prestationslot'][$this->hashCode()])) {
            return '*RECURSION*';
        }
        $alreadyDumpedObjects['Prestationslot'][$this->hashCode()] = true;
        $keys = PrestationslotTableMap::getFieldNames($keyType);
        $result = array(
            $keys[0] => $this->getIdPrestationslot(),
            $keys[1] => $this->getIdEntite(),
            $keys[2] => $this->getNom(),
            $keys[3] => $this->getNomcourt(),
            $keys[4] => $this->getActif(),
            $keys[5] => $this->getObservation(),
            $keys[6] => $this->getCreatedAt(),
            $keys[7] => $this->getUpdatedAt(),
        );
        if ($result[$keys[6]] instanceof \DateTimeInterface) {
            $result[$keys[6]] = $result[$keys[6]]->format('c');
        }

        if ($result[$keys[7]] instanceof \DateTimeInterface) {
            $result[$keys[7]] = $result[$keys[7]]->format('c');
        }

        $virtualColumns = $this->virtualColumns;
        foreach ($virtualColumns as $key => $virtualColumn) {
            $result[$key] = $virtualColumn;
        }

        if ($includeForeignObjects) {
            if (null !== $this->aEntite) {

                switch ($keyType) {
                    case TableMap::TYPE_CAMELNAME:
                        $key = 'entite';
                        break;
                    case TableMap::TYPE_FIELDNAME:
                        $key = 'asso_entites';
                        break;
                    default:
                        $key = 'Entite';
                }

                $result[$key] = $this->aEntite->toArray($keyType, $includeLazyLoadColumns,  $alreadyDumpedObjects, true);
            }
            if (null !== $this->collPrestationslotPrestations) {

                switch ($keyType) {
                    case TableMap::TYPE_CAMELNAME:
                        $key = 'prestationslotPrestations';
                        break;
                    case TableMap::TYPE_FIELDNAME:
                        $key = 'asso_prestationslots_prestationss';
                        break;
                    default:
                        $key = 'PrestationslotPrestations';
                }

                $result[$key] = $this->collPrestationslotPrestations->toArray(null, false, $keyType, $includeLazyLoadColumns, $alreadyDumpedObjects);
            }
        }

        return $result;
    }

    /**
     * Sets a field from the object by name passed in as a string.
     *
     * @param  string $name
     * @param  mixed  $value field value
     * @param  string $type The type of fieldname the $name is of:
     *                one of the class type constants TableMap::TYPE_PHPNAME, TableMap::TYPE_CAMELNAME
     *                TableMap::TYPE_COLNAME, TableMap::TYPE_FIELDNAME, TableMap::TYPE_NUM.
     *                Defaults to TableMap::TYPE_FIELDNAME.
     * @return $this|\Prestationslot
     */
    public function setByName($name, $value, $type = TableMap::TYPE_FIELDNAME)
    {
        $pos = PrestationslotTableMap::translateFieldName($name, $type, TableMap::TYPE_NUM);

        return $this->setByPosition($pos, $value);
    }

    /**
     * Sets a field from the object by Position as specified in the xml schema.
     * Zero-based.
     *
     * @param  int $pos position in xml schema
     * @param  mixed $value field value
     * @return $this|\Prestationslot
     */
    public function setByPosition($pos, $value)
    {
        switch ($pos) {
            case 0:
                $this->setIdPrestationslot($value);
                break;
            case 1:
                $this->setIdEntite($value);
                break;
            case 2:
                $this->setNom($value);
                break;
            case 3:
                $this->setNomcourt($value);
                break;
            case 4:
                $this->setActif($value);
                break;
            case 5:
                $this->setObservation($value);
                break;
            case 6:
                $this->setCreatedAt($value);
                break;
            case 7:
                $this->setUpdatedAt($value);
                break;
        } // switch()

        return $this;
    }

    /**
     * Populates the object using an array.
     *
     * This is particularly useful when populating an object from one of the
     * request arrays (e.g. $_POST).  This method goes through the column
     * names, checking to see whether a matching key exists in populated
     * array. If so the setByName() method is called for that column.
     *
     * You can specify the key type of the array by additionally passing one
     * of the class type constants TableMap::TYPE_PHPNAME, TableMap::TYPE_CAMELNAME,
     * TableMap::TYPE_COLNAME, TableMap::TYPE_FIELDNAME, TableMap::TYPE_NUM.
     * The default key type is the column's TableMap::TYPE_FIELDNAME.
     *
     * @param      array  $arr     An array to populate the object from.
     * @param      string $keyType The type of keys the array uses.
     * @return void
     */
    public function fromArray($arr, $keyType = TableMap::TYPE_FIELDNAME)
    {
        $keys = PrestationslotTableMap::getFieldNames($keyType);

        if (array_key_exists($keys[0], $arr)) {
            $this->setIdPrestationslot($arr[$keys[0]]);
        }
        if (array_key_exists($keys[1], $arr)) {
            $this->setIdEntite($arr[$keys[1]]);
        }
        if (array_key_exists($keys[2], $arr)) {
            $this->setNom($arr[$keys[2]]);
        }
        if (array_key_exists($keys[3], $arr)) {
            $this->setNomcourt($arr[$keys[3]]);
        }
        if (array_key_exists($keys[4], $arr)) {
            $this->setActif($arr[$keys[4]]);
        }
        if (array_key_exists($keys[5], $arr)) {
            $this->setObservation($arr[$keys[5]]);
        }
        if (array_key_exists($keys[6], $arr)) {
            $this->setCreatedAt($arr[$keys[6]]);
        }
        if (array_key_exists($keys[7], $arr)) {
            $this->setUpdatedAt($arr[$keys[7]]);
        }
    }

     /**
     * Populate the current object from a string, using a given parser format
     * <code>
     * $book = new Book();
     * $book->importFrom('JSON', '{"Id":9012,"Title":"Don Juan","ISBN":"0140422161","Price":12.99,"PublisherId":1234,"AuthorId":5678}');
     * </code>
     *
     * You can specify the key type of the array by additionally passing one
     * of the class type constants TableMap::TYPE_PHPNAME, TableMap::TYPE_CAMELNAME,
     * TableMap::TYPE_COLNAME, TableMap::TYPE_FIELDNAME, TableMap::TYPE_NUM.
     * The default key type is the column's TableMap::TYPE_FIELDNAME.
     *
     * @param mixed $parser A AbstractParser instance,
     *                       or a format name ('XML', 'YAML', 'JSON', 'CSV')
     * @param string $data The source data to import from
     * @param string $keyType The type of keys the array uses.
     *
     * @return $this|\Prestationslot The current object, for fluid interface
     */
    public function importFrom($parser, $data, $keyType = TableMap::TYPE_FIELDNAME)
    {
        if (!$parser instanceof AbstractParser) {
            $parser = AbstractParser::getParser($parser);
        }

        $this->fromArray($parser->toArray($data), $keyType);

        return $this;
    }

    /**
     * Build a Criteria object containing the values of all modified columns in this object.
     *
     * @return Criteria The Criteria object containing all modified values.
     */
    public function buildCriteria()
    {
        $criteria = new Criteria(PrestationslotTableMap::DATABASE_NAME);

        if ($this->isColumnModified(PrestationslotTableMap::COL_ID_PRESTATIONSLOT)) {
            $criteria->add(PrestationslotTableMap::COL_ID_PRESTATIONSLOT, $this->id_prestationslot);
        }
        if ($this->isColumnModified(PrestationslotTableMap::COL_ID_ENTITE)) {
            $criteria->add(PrestationslotTableMap::COL_ID_ENTITE, $this->id_entite);
        }
        if ($this->isColumnModified(PrestationslotTableMap::COL_NOM)) {
            $criteria->add(PrestationslotTableMap::COL_NOM, $this->nom);
        }
        if ($this->isColumnModified(PrestationslotTableMap::COL_NOMCOURT)) {
            $criteria->add(PrestationslotTableMap::COL_NOMCOURT, $this->nomcourt);
        }
        if ($this->isColumnModified(PrestationslotTableMap::COL_ACTIF)) {
            $criteria->add(PrestationslotTableMap::COL_ACTIF, $this->actif);
        }
        if ($this->isColumnModified(PrestationslotTableMap::COL_OBSERVATION)) {
            $criteria->add(PrestationslotTableMap::COL_OBSERVATION, $this->observation);
        }
        if ($this->isColumnModified(PrestationslotTableMap::COL_CREATED_AT)) {
            $criteria->add(PrestationslotTableMap::COL_CREATED_AT, $this->created_at);
        }
        if ($this->isColumnModified(PrestationslotTableMap::COL_UPDATED_AT)) {
            $criteria->add(PrestationslotTableMap::COL_UPDATED_AT, $this->updated_at);
        }

        return $criteria;
    }

    /**
     * Builds a Criteria object containing the primary key for this object.
     *
     * Unlike buildCriteria() this method includes the primary key values regardless
     * of whether or not they have been modified.
     *
     * @throws LogicException if no primary key is defined
     *
     * @return Criteria The Criteria object containing value(s) for primary key(s).
     */
    public function buildPkeyCriteria()
    {
        $criteria = ChildPrestationslotQuery::create();
        $criteria->add(PrestationslotTableMap::COL_ID_PRESTATIONSLOT, $this->id_prestationslot);

        return $criteria;
    }

    /**
     * If the primary key is not null, return the hashcode of the
     * primary key. Otherwise, return the hash code of the object.
     *
     * @return int Hashcode
     */
    public function hashCode()
    {
        $validPk = null !== $this->getIdPrestationslot();

        $validPrimaryKeyFKs = 0;
        $primaryKeyFKs = [];

        if ($validPk) {
            return crc32(json_encode($this->getPrimaryKey(), JSON_UNESCAPED_UNICODE));
        } elseif ($validPrimaryKeyFKs) {
            return crc32(json_encode($primaryKeyFKs, JSON_UNESCAPED_UNICODE));
        }

        return spl_object_hash($this);
    }

    /**
     * Returns the primary key for this object (row).
     * @return string
     */
    public function getPrimaryKey()
    {
        return $this->getIdPrestationslot();
    }

    /**
     * Generic method to set the primary key (id_prestationslot column).
     *
     * @param       string $key Primary key.
     * @return void
     */
    public function setPrimaryKey($key)
    {
        $this->setIdPrestationslot($key);
    }

    /**
     * Returns true if the primary key for this object is null.
     * @return boolean
     */
    public function isPrimaryKeyNull()
    {
        return null === $this->getIdPrestationslot();
    }

    /**
     * Sets contents of passed object to values from current object.
     *
     * If desired, this method can also make copies of all associated (fkey referrers)
     * objects.
     *
     * @param      object $copyObj An object of \Prestationslot (or compatible) type.
     * @param      boolean $deepCopy Whether to also copy all rows that refer (by fkey) to the current row.
     * @param      boolean $makeNew Whether to reset autoincrement PKs and make the object new.
     * @throws PropelException
     */
    public function copyInto($copyObj, $deepCopy = false, $makeNew = true)
    {
        $copyObj->setIdEntite($this->getIdEntite());
        $copyObj->setNom($this->getNom());
        $copyObj->setNomcourt($this->getNomcourt());
        $copyObj->setActif($this->getActif());
        $copyObj->setObservation($this->getObservation());
        $copyObj->setCreatedAt($this->getCreatedAt());
        $copyObj->setUpdatedAt($this->getUpdatedAt());

        if ($deepCopy) {
            // important: temporarily setNew(false) because this affects the behavior of
            // the getter/setter methods for fkey referrer objects.
            $copyObj->setNew(false);

            foreach ($this->getPrestationslotPrestations() as $relObj) {
                if ($relObj !== $this) {  // ensure that we don't try to copy a reference to ourselves
                    $copyObj->addPrestationslotPrestation($relObj->copy($deepCopy));
                }
            }

        } // if ($deepCopy)

        if ($makeNew) {
            $copyObj->setNew(true);
            $copyObj->setIdPrestationslot(NULL); // this is a auto-increment column, so set to default value
        }
    }

    /**
     * Makes a copy of this object that will be inserted as a new row in table when saved.
     * It creates a new object filling in the simple attributes, but skipping any primary
     * keys that are defined for the table.
     *
     * If desired, this method can also make copies of all associated (fkey referrers)
     * objects.
     *
     * @param  boolean $deepCopy Whether to also copy all rows that refer (by fkey) to the current row.
     * @return \Prestationslot Clone of current object.
     * @throws PropelException
     */
    public function copy($deepCopy = false)
    {
        // we use get_class(), because this might be a subclass
        $clazz = get_class($this);
        $copyObj = new $clazz();
        $this->copyInto($copyObj, $deepCopy);

        return $copyObj;
    }

    /**
     * Declares an association between this object and a ChildEntite object.
     *
     * @param  ChildEntite $v
     * @return $this|\Prestationslot The current object (for fluent API support)
     * @throws PropelException
     */
    public function setEntite(ChildEntite $v = null)
    {
        if ($v === null) {
            $this->setIdEntite('1');
        } else {
            $this->setIdEntite($v->getIdEntite());
        }

        $this->aEntite = $v;

        // Add binding for other direction of this n:n relationship.
        // If this object has already been added to the ChildEntite object, it will not be re-added.
        if ($v !== null) {
            $v->addPrestationslot($this);
        }


        return $this;
    }


    /**
     * Get the associated ChildEntite object
     *
     * @param  ConnectionInterface $con Optional Connection object.
     * @return ChildEntite The associated ChildEntite object.
     * @throws PropelException
     */
    public function getEntite(ConnectionInterface $con = null)
    {
        if ($this->aEntite === null && (($this->id_entite !== "" && $this->id_entite !== null))) {
            $this->aEntite = ChildEntiteQuery::create()->findPk($this->id_entite, $con);
            /* The following can be used additionally to
                guarantee the related object contains a reference
                to this object.  This level of coupling may, however, be
                undesirable since it could result in an only partially populated collection
                in the referenced object.
                $this->aEntite->addPrestationslots($this);
             */
        }

        return $this->aEntite;
    }


    /**
     * Initializes a collection based on the name of a relation.
     * Avoids crafting an 'init[$relationName]s' method name
     * that wouldn't work when StandardEnglishPluralizer is used.
     *
     * @param      string $relationName The name of the relation to initialize
     * @return void
     */
    public function initRelation($relationName)
    {
        if ('PrestationslotPrestation' == $relationName) {
            $this->initPrestationslotPrestations();
            return;
        }
    }

    /**
     * Clears out the collPrestationslotPrestations collection
     *
     * This does not modify the database; however, it will remove any associated objects, causing
     * them to be refetched by subsequent calls to accessor method.
     *
     * @return void
     * @see        addPrestationslotPrestations()
     */
    public function clearPrestationslotPrestations()
    {
        $this->collPrestationslotPrestations = null; // important to set this to NULL since that means it is uninitialized
    }

    /**
     * Reset is the collPrestationslotPrestations collection loaded partially.
     */
    public function resetPartialPrestationslotPrestations($v = true)
    {
        $this->collPrestationslotPrestationsPartial = $v;
    }

    /**
     * Initializes the collPrestationslotPrestations collection.
     *
     * By default this just sets the collPrestationslotPrestations collection to an empty array (like clearcollPrestationslotPrestations());
     * however, you may wish to override this method in your stub class to provide setting appropriate
     * to your application -- for example, setting the initial array to the values stored in database.
     *
     * @param      boolean $overrideExisting If set to true, the method call initializes
     *                                        the collection even if it is not empty
     *
     * @return void
     */
    public function initPrestationslotPrestations($overrideExisting = true)
    {
        if (null !== $this->collPrestationslotPrestations && !$overrideExisting) {
            return;
        }

        $collectionClassName = PrestationslotPrestationTableMap::getTableMap()->getCollectionClassName();

        $this->collPrestationslotPrestations = new $collectionClassName;
        $this->collPrestationslotPrestations->setModel('\PrestationslotPrestation');
    }

    /**
     * Gets an array of ChildPrestationslotPrestation objects which contain a foreign key that references this object.
     *
     * If the $criteria is not null, it is used to always fetch the results from the database.
     * Otherwise the results are fetched from the database the first time, then cached.
     * Next time the same method is called without $criteria, the cached collection is returned.
     * If this ChildPrestationslot is new, it will return
     * an empty collection or the current collection; the criteria is ignored on a new object.
     *
     * @param      Criteria $criteria optional Criteria object to narrow the query
     * @param      ConnectionInterface $con optional connection object
     * @return ObjectCollection|ChildPrestationslotPrestation[] List of ChildPrestationslotPrestation objects
     * @throws PropelException
     */
    public function getPrestationslotPrestations(Criteria $criteria = null, ConnectionInterface $con = null)
    {
        $partial = $this->collPrestationslotPrestationsPartial && !$this->isNew();
        if (null === $this->collPrestationslotPrestations || null !== $criteria  || $partial) {
            if ($this->isNew() && null === $this->collPrestationslotPrestations) {
                // return empty collection
                $this->initPrestationslotPrestations();
            } else {
                $collPrestationslotPrestations = ChildPrestationslotPrestationQuery::create(null, $criteria)
                    ->filterByPrestationslot($this)
                    ->find($con);

                if (null !== $criteria) {
                    if (false !== $this->collPrestationslotPrestationsPartial && count($collPrestationslotPrestations)) {
                        $this->initPrestationslotPrestations(false);

                        foreach ($collPrestationslotPrestations as $obj) {
                            if (false == $this->collPrestationslotPrestations->contains($obj)) {
                                $this->collPrestationslotPrestations->append($obj);
                            }
                        }

                        $this->collPrestationslotPrestationsPartial = true;
                    }

                    return $collPrestationslotPrestations;
                }

                if ($partial && $this->collPrestationslotPrestations) {
                    foreach ($this->collPrestationslotPrestations as $obj) {
                        if ($obj->isNew()) {
                            $collPrestationslotPrestations[] = $obj;
                        }
                    }
                }

                $this->collPrestationslotPrestations = $collPrestationslotPrestations;
                $this->collPrestationslotPrestationsPartial = false;
            }
        }

        return $this->collPrestationslotPrestations;
    }

    /**
     * Sets a collection of ChildPrestationslotPrestation objects related by a one-to-many relationship
     * to the current object.
     * It will also schedule objects for deletion based on a diff between old objects (aka persisted)
     * and new objects from the given Propel collection.
     *
     * @param      Collection $prestationslotPrestations A Propel collection.
     * @param      ConnectionInterface $con Optional connection object
     * @return $this|ChildPrestationslot The current object (for fluent API support)
     */
    public function setPrestationslotPrestations(Collection $prestationslotPrestations, ConnectionInterface $con = null)
    {
        /** @var ChildPrestationslotPrestation[] $prestationslotPrestationsToDelete */
        $prestationslotPrestationsToDelete = $this->getPrestationslotPrestations(new Criteria(), $con)->diff($prestationslotPrestations);


        //since at least one column in the foreign key is at the same time a PK
        //we can not just set a PK to NULL in the lines below. We have to store
        //a backup of all values, so we are able to manipulate these items based on the onDelete value later.
        $this->prestationslotPrestationsScheduledForDeletion = clone $prestationslotPrestationsToDelete;

        foreach ($prestationslotPrestationsToDelete as $prestationslotPrestationRemoved) {
            $prestationslotPrestationRemoved->setPrestationslot(null);
        }

        $this->collPrestationslotPrestations = null;
        foreach ($prestationslotPrestations as $prestationslotPrestation) {
            $this->addPrestationslotPrestation($prestationslotPrestation);
        }

        $this->collPrestationslotPrestations = $prestationslotPrestations;
        $this->collPrestationslotPrestationsPartial = false;

        return $this;
    }

    /**
     * Returns the number of related PrestationslotPrestation objects.
     *
     * @param      Criteria $criteria
     * @param      boolean $distinct
     * @param      ConnectionInterface $con
     * @return int             Count of related PrestationslotPrestation objects.
     * @throws PropelException
     */
    public function countPrestationslotPrestations(Criteria $criteria = null, $distinct = false, ConnectionInterface $con = null)
    {
        $partial = $this->collPrestationslotPrestationsPartial && !$this->isNew();
        if (null === $this->collPrestationslotPrestations || null !== $criteria || $partial) {
            if ($this->isNew() && null === $this->collPrestationslotPrestations) {
                return 0;
            }

            if ($partial && !$criteria) {
                return count($this->getPrestationslotPrestations());
            }

            $query = ChildPrestationslotPrestationQuery::create(null, $criteria);
            if ($distinct) {
                $query->distinct();
            }

            return $query
                ->filterByPrestationslot($this)
                ->count($con);
        }

        return count($this->collPrestationslotPrestations);
    }

    /**
     * Method called to associate a ChildPrestationslotPrestation object to this object
     * through the ChildPrestationslotPrestation foreign key attribute.
     *
     * @param  ChildPrestationslotPrestation $l ChildPrestationslotPrestation
     * @return $this|\Prestationslot The current object (for fluent API support)
     */
    public function addPrestationslotPrestation(ChildPrestationslotPrestation $l)
    {
        if ($this->collPrestationslotPrestations === null) {
            $this->initPrestationslotPrestations();
            $this->collPrestationslotPrestationsPartial = true;
        }

        if (!$this->collPrestationslotPrestations->contains($l)) {
            $this->doAddPrestationslotPrestation($l);

            if ($this->prestationslotPrestationsScheduledForDeletion and $this->prestationslotPrestationsScheduledForDeletion->contains($l)) {
                $this->prestationslotPrestationsScheduledForDeletion->remove($this->prestationslotPrestationsScheduledForDeletion->search($l));
            }
        }

        return $this;
    }

    /**
     * @param ChildPrestationslotPrestation $prestationslotPrestation The ChildPrestationslotPrestation object to add.
     */
    protected function doAddPrestationslotPrestation(ChildPrestationslotPrestation $prestationslotPrestation)
    {
        $this->collPrestationslotPrestations[]= $prestationslotPrestation;
        $prestationslotPrestation->setPrestationslot($this);
    }

    /**
     * @param  ChildPrestationslotPrestation $prestationslotPrestation The ChildPrestationslotPrestation object to remove.
     * @return $this|ChildPrestationslot The current object (for fluent API support)
     */
    public function removePrestationslotPrestation(ChildPrestationslotPrestation $prestationslotPrestation)
    {
        if ($this->getPrestationslotPrestations()->contains($prestationslotPrestation)) {
            $pos = $this->collPrestationslotPrestations->search($prestationslotPrestation);
            $this->collPrestationslotPrestations->remove($pos);
            if (null === $this->prestationslotPrestationsScheduledForDeletion) {
                $this->prestationslotPrestationsScheduledForDeletion = clone $this->collPrestationslotPrestations;
                $this->prestationslotPrestationsScheduledForDeletion->clear();
            }
            $this->prestationslotPrestationsScheduledForDeletion[]= clone $prestationslotPrestation;
            $prestationslotPrestation->setPrestationslot(null);
        }

        return $this;
    }


    /**
     * If this collection has already been initialized with
     * an identical criteria, it returns the collection.
     * Otherwise if this Prestationslot is new, it will return
     * an empty collection; or if this Prestationslot has previously
     * been saved, it will retrieve related PrestationslotPrestations from storage.
     *
     * This method is protected by default in order to keep the public
     * api reasonable.  You can provide public methods for those you
     * actually need in Prestationslot.
     *
     * @param      Criteria $criteria optional Criteria object to narrow the query
     * @param      ConnectionInterface $con optional connection object
     * @param      string $joinBehavior optional join type to use (defaults to Criteria::LEFT_JOIN)
     * @return ObjectCollection|ChildPrestationslotPrestation[] List of ChildPrestationslotPrestation objects
     */
    public function getPrestationslotPrestationsJoinPrestation(Criteria $criteria = null, ConnectionInterface $con = null, $joinBehavior = Criteria::LEFT_JOIN)
    {
        $query = ChildPrestationslotPrestationQuery::create(null, $criteria);
        $query->joinWith('Prestation', $joinBehavior);

        return $this->getPrestationslotPrestations($query, $con);
    }

    /**
     * Clears out the collPrestations collection
     *
     * This does not modify the database; however, it will remove any associated objects, causing
     * them to be refetched by subsequent calls to accessor method.
     *
     * @return void
     * @see        addPrestations()
     */
    public function clearPrestations()
    {
        $this->collPrestations = null; // important to set this to NULL since that means it is uninitialized
    }

    /**
     * Initializes the collPrestations crossRef collection.
     *
     * By default this just sets the collPrestations collection to an empty collection (like clearPrestations());
     * however, you may wish to override this method in your stub class to provide setting appropriate
     * to your application -- for example, setting the initial array to the values stored in database.
     *
     * @return void
     */
    public function initPrestations()
    {
        $collectionClassName = PrestationslotPrestationTableMap::getTableMap()->getCollectionClassName();

        $this->collPrestations = new $collectionClassName;
        $this->collPrestationsPartial = true;
        $this->collPrestations->setModel('\Prestation');
    }

    /**
     * Checks if the collPrestations collection is loaded.
     *
     * @return bool
     */
    public function isPrestationsLoaded()
    {
        return null !== $this->collPrestations;
    }

    /**
     * Gets a collection of ChildPrestation objects related by a many-to-many relationship
     * to the current object by way of the asso_prestationslots_prestations cross-reference table.
     *
     * If the $criteria is not null, it is used to always fetch the results from the database.
     * Otherwise the results are fetched from the database the first time, then cached.
     * Next time the same method is called without $criteria, the cached collection is returned.
     * If this ChildPrestationslot is new, it will return
     * an empty collection or the current collection; the criteria is ignored on a new object.
     *
     * @param      Criteria $criteria Optional query object to filter the query
     * @param      ConnectionInterface $con Optional connection object
     *
     * @return ObjectCollection|ChildPrestation[] List of ChildPrestation objects
     */
    public function getPrestations(Criteria $criteria = null, ConnectionInterface $con = null)
    {
        $partial = $this->collPrestationsPartial && !$this->isNew();
        if (null === $this->collPrestations || null !== $criteria || $partial) {
            if ($this->isNew()) {
                // return empty collection
                if (null === $this->collPrestations) {
                    $this->initPrestations();
                }
            } else {

                $query = ChildPrestationQuery::create(null, $criteria)
                    ->filterByPrestationslot($this);
                $collPrestations = $query->find($con);
                if (null !== $criteria) {
                    return $collPrestations;
                }

                if ($partial && $this->collPrestations) {
                    //make sure that already added objects gets added to the list of the database.
                    foreach ($this->collPrestations as $obj) {
                        if (!$collPrestations->contains($obj)) {
                            $collPrestations[] = $obj;
                        }
                    }
                }

                $this->collPrestations = $collPrestations;
                $this->collPrestationsPartial = false;
            }
        }

        return $this->collPrestations;
    }

    /**
     * Sets a collection of Prestation objects related by a many-to-many relationship
     * to the current object by way of the asso_prestationslots_prestations cross-reference table.
     * It will also schedule objects for deletion based on a diff between old objects (aka persisted)
     * and new objects from the given Propel collection.
     *
     * @param  Collection $prestations A Propel collection.
     * @param  ConnectionInterface $con Optional connection object
     * @return $this|ChildPrestationslot The current object (for fluent API support)
     */
    public function setPrestations(Collection $prestations, ConnectionInterface $con = null)
    {
        $this->clearPrestations();
        $currentPrestations = $this->getPrestations();

        $prestationsScheduledForDeletion = $currentPrestations->diff($prestations);

        foreach ($prestationsScheduledForDeletion as $toDelete) {
            $this->removePrestation($toDelete);
        }

        foreach ($prestations as $prestation) {
            if (!$currentPrestations->contains($prestation)) {
                $this->doAddPrestation($prestation);
            }
        }

        $this->collPrestationsPartial = false;
        $this->collPrestations = $prestations;

        return $this;
    }

    /**
     * Gets the number of Prestation objects related by a many-to-many relationship
     * to the current object by way of the asso_prestationslots_prestations cross-reference table.
     *
     * @param      Criteria $criteria Optional query object to filter the query
     * @param      boolean $distinct Set to true to force count distinct
     * @param      ConnectionInterface $con Optional connection object
     *
     * @return int the number of related Prestation objects
     */
    public function countPrestations(Criteria $criteria = null, $distinct = false, ConnectionInterface $con = null)
    {
        $partial = $this->collPrestationsPartial && !$this->isNew();
        if (null === $this->collPrestations || null !== $criteria || $partial) {
            if ($this->isNew() && null === $this->collPrestations) {
                return 0;
            } else {

                if ($partial && !$criteria) {
                    return count($this->getPrestations());
                }

                $query = ChildPrestationQuery::create(null, $criteria);
                if ($distinct) {
                    $query->distinct();
                }

                return $query
                    ->filterByPrestationslot($this)
                    ->count($con);
            }
        } else {
            return count($this->collPrestations);
        }
    }

    /**
     * Associate a ChildPrestation to this object
     * through the asso_prestationslots_prestations cross reference table.
     *
     * @param ChildPrestation $prestation
     * @return ChildPrestationslot The current object (for fluent API support)
     */
    public function addPrestation(ChildPrestation $prestation)
    {
        if ($this->collPrestations === null) {
            $this->initPrestations();
        }

        if (!$this->getPrestations()->contains($prestation)) {
            // only add it if the **same** object is not already associated
            $this->collPrestations->push($prestation);
            $this->doAddPrestation($prestation);
        }

        return $this;
    }

    /**
     *
     * @param ChildPrestation $prestation
     */
    protected function doAddPrestation(ChildPrestation $prestation)
    {
        $prestationslotPrestation = new ChildPrestationslotPrestation();

        $prestationslotPrestation->setPrestation($prestation);

        $prestationslotPrestation->setPrestationslot($this);

        $this->addPrestationslotPrestation($prestationslotPrestation);

        // set the back reference to this object directly as using provided method either results
        // in endless loop or in multiple relations
        if (!$prestation->isPrestationslotsLoaded()) {
            $prestation->initPrestationslots();
            $prestation->getPrestationslots()->push($this);
        } elseif (!$prestation->getPrestationslots()->contains($this)) {
            $prestation->getPrestationslots()->push($this);
        }

    }

    /**
     * Remove prestation of this object
     * through the asso_prestationslots_prestations cross reference table.
     *
     * @param ChildPrestation $prestation
     * @return ChildPrestationslot The current object (for fluent API support)
     */
    public function removePrestation(ChildPrestation $prestation)
    {
        if ($this->getPrestations()->contains($prestation)) {
            $prestationslotPrestation = new ChildPrestationslotPrestation();
            $prestationslotPrestation->setPrestation($prestation);
            if ($prestation->isPrestationslotsLoaded()) {
                //remove the back reference if available
                $prestation->getPrestationslots()->removeObject($this);
            }

            $prestationslotPrestation->setPrestationslot($this);
            $this->removePrestationslotPrestation(clone $prestationslotPrestation);
            $prestationslotPrestation->clear();

            $this->collPrestations->remove($this->collPrestations->search($prestation));

            if (null === $this->prestationsScheduledForDeletion) {
                $this->prestationsScheduledForDeletion = clone $this->collPrestations;
                $this->prestationsScheduledForDeletion->clear();
            }

            $this->prestationsScheduledForDeletion->push($prestation);
        }


        return $this;
    }

    /**
     * Clears the current object, sets all attributes to their default values and removes
     * outgoing references as well as back-references (from other objects to this one. Results probably in a database
     * change of those foreign objects when you call `save` there).
     */
    public function clear()
    {
        if (null !== $this->aEntite) {
            $this->aEntite->removePrestationslot($this);
        }
        $this->id_prestationslot = null;
        $this->id_entite = null;
        $this->nom = null;
        $this->nomcourt = null;
        $this->actif = null;
        $this->observation = null;
        $this->created_at = null;
        $this->updated_at = null;
        $this->alreadyInSave = false;
        $this->clearAllReferences();
        $this->applyDefaultValues();
        $this->resetModified();
        $this->setNew(true);
        $this->setDeleted(false);
    }

    /**
     * Resets all references and back-references to other model objects or collections of model objects.
     *
     * This method is used to reset all php object references (not the actual reference in the database).
     * Necessary for object serialisation.
     *
     * @param      boolean $deep Whether to also clear the references on all referrer objects.
     */
    public function clearAllReferences($deep = false)
    {
        if ($deep) {
            if ($this->collPrestationslotPrestations) {
                foreach ($this->collPrestationslotPrestations as $o) {
                    $o->clearAllReferences($deep);
                }
            }
            if ($this->collPrestations) {
                foreach ($this->collPrestations as $o) {
                    $o->clearAllReferences($deep);
                }
            }
        } // if ($deep)

        $this->collPrestationslotPrestations = null;
        $this->collPrestations = null;
        $this->aEntite = null;
    }

    /**
     * Return the string representation of this object
     *
     * @return string
     */
    public function __toString()
    {
        return (string) $this->exportTo(PrestationslotTableMap::DEFAULT_STRING_FORMAT);
    }

    // timestampable behavior

    /**
     * Mark the current object so that the update date doesn't get updated during next save
     *
     * @return     $this|ChildPrestationslot The current object (for fluent API support)
     */
    public function keepUpdateDateUnchanged()
    {
        $this->modifiedColumns[PrestationslotTableMap::COL_UPDATED_AT] = true;

        return $this;
    }

    // archivable behavior

    /**
     * Get an archived version of the current object.
     *
     * @param ConnectionInterface $con Optional connection object
     *
     * @return     ChildAssoPrestationslotsArchive An archive object, or null if the current object was never archived
     */
    public function getArchive(ConnectionInterface $con = null)
    {
        if ($this->isNew()) {
            return null;
        }
        $archive = ChildAssoPrestationslotsArchiveQuery::create()
            ->filterByPrimaryKey($this->getPrimaryKey())
            ->findOne($con);

        return $archive;
    }
    /**
     * Copy the data of the current object into a $archiveTablePhpName archive object.
     * The archived object is then saved.
     * If the current object has already been archived, the archived object
     * is updated and not duplicated.
     *
     * @param ConnectionInterface $con Optional connection object
     *
     * @throws PropelException If the object is new
     *
     * @return     ChildAssoPrestationslotsArchive The archive object based on this object
     */
    public function archive(ConnectionInterface $con = null)
    {
        if ($this->isNew()) {
            throw new PropelException('New objects cannot be archived. You must save the current object before calling archive().');
        }
        $archive = $this->getArchive($con);
        if (!$archive) {
            $archive = new ChildAssoPrestationslotsArchive();
            $archive->setPrimaryKey($this->getPrimaryKey());
        }
        $this->copyInto($archive, $deepCopy = false, $makeNew = false);
        $archive->setArchivedAt(time());
        $archive->save($con);

        return $archive;
    }

    /**
     * Revert the the current object to the state it had when it was last archived.
     * The object must be saved afterwards if the changes must persist.
     *
     * @param ConnectionInterface $con Optional connection object
     *
     * @throws PropelException If the object has no corresponding archive.
     *
     * @return $this|ChildPrestationslot The current object (for fluent API support)
     */
    public function restoreFromArchive(ConnectionInterface $con = null)
    {
        $archive = $this->getArchive($con);
        if (!$archive) {
            throw new PropelException('The current object has never been archived and cannot be restored');
        }
        $this->populateFromArchive($archive);

        return $this;
    }

    /**
     * Populates the the current object based on a $archiveTablePhpName archive object.
     *
     * @param      ChildAssoPrestationslotsArchive $archive An archived object based on the same class
      * @param      Boolean $populateAutoIncrementPrimaryKeys
     *               If true, autoincrement columns are copied from the archive object.
     *               If false, autoincrement columns are left intact.
      *
     * @return     ChildPrestationslot The current object (for fluent API support)
     */
    public function populateFromArchive($archive, $populateAutoIncrementPrimaryKeys = false) {
        if ($populateAutoIncrementPrimaryKeys) {
            $this->setIdPrestationslot($archive->getIdPrestationslot());
        }
        $this->setIdEntite($archive->getIdEntite());
        $this->setNom($archive->getNom());
        $this->setNomcourt($archive->getNomcourt());
        $this->setActif($archive->getActif());
        $this->setObservation($archive->getObservation());
        $this->setCreatedAt($archive->getCreatedAt());
        $this->setUpdatedAt($archive->getUpdatedAt());

        return $this;
    }

    /**
     * Removes the object from the database without archiving it.
     *
     * @param ConnectionInterface $con Optional connection object
     *
     * @return $this|ChildPrestationslot The current object (for fluent API support)
     */
    public function deleteWithoutArchive(ConnectionInterface $con = null)
    {
        $this->archiveOnDelete = false;

        return $this->delete($con);
    }

    /**
     * Code to be run before persisting the object
     * @param  ConnectionInterface $con
     * @return boolean
     */
    public function preSave(ConnectionInterface $con = null)
    {
        if (is_callable('parent::preSave')) {
            return parent::preSave($con);
        }
        return true;
    }

    /**
     * Code to be run after persisting the object
     * @param ConnectionInterface $con
     */
    public function postSave(ConnectionInterface $con = null)
    {
        if (is_callable('parent::postSave')) {
            parent::postSave($con);
        }
    }

    /**
     * Code to be run before inserting to database
     * @param  ConnectionInterface $con
     * @return boolean
     */
    public function preInsert(ConnectionInterface $con = null)
    {
        if (is_callable('parent::preInsert')) {
            return parent::preInsert($con);
        }
        return true;
    }

    /**
     * Code to be run after inserting to database
     * @param ConnectionInterface $con
     */
    public function postInsert(ConnectionInterface $con = null)
    {
        if (is_callable('parent::postInsert')) {
            parent::postInsert($con);
        }
    }

    /**
     * Code to be run before updating the object in database
     * @param  ConnectionInterface $con
     * @return boolean
     */
    public function preUpdate(ConnectionInterface $con = null)
    {
        if (is_callable('parent::preUpdate')) {
            return parent::preUpdate($con);
        }
        return true;
    }

    /**
     * Code to be run after updating the object in database
     * @param ConnectionInterface $con
     */
    public function postUpdate(ConnectionInterface $con = null)
    {
        if (is_callable('parent::postUpdate')) {
            parent::postUpdate($con);
        }
    }

    /**
     * Code to be run before deleting the object in database
     * @param  ConnectionInterface $con
     * @return boolean
     */
    public function preDelete(ConnectionInterface $con = null)
    {
        if (is_callable('parent::preDelete')) {
            return parent::preDelete($con);
        }
        return true;
    }

    /**
     * Code to be run after deleting the object in database
     * @param ConnectionInterface $con
     */
    public function postDelete(ConnectionInterface $con = null)
    {
        if (is_callable('parent::postDelete')) {
            parent::postDelete($con);
        }
    }


    /**
     * Derived method to catches calls to undefined methods.
     *
     * Provides magic import/export method support (fromXML()/toXML(), fromYAML()/toYAML(), etc.).
     * Allows to define default __call() behavior if you overwrite __call()
     *
     * @param string $name
     * @param mixed  $params
     *
     * @return array|string
     */
    public function __call($name, $params)
    {
        if (0 === strpos($name, 'get')) {
            $virtualColumn = substr($name, 3);
            if ($this->hasVirtualColumn($virtualColumn)) {
                return $this->getVirtualColumn($virtualColumn);
            }

            $virtualColumn = lcfirst($virtualColumn);
            if ($this->hasVirtualColumn($virtualColumn)) {
                return $this->getVirtualColumn($virtualColumn);
            }
        }

        if (0 === strpos($name, 'from')) {
            $format = substr($name, 4);

            return $this->importFrom($format, reset($params));
        }

        if (0 === strpos($name, 'to')) {
            $format = substr($name, 2);
            $includeLazyLoadColumns = isset($params[0]) ? $params[0] : true;

            return $this->exportTo($format, $includeLazyLoadColumns);
        }

        throw new BadMethodCallException(sprintf('Call to undefined method: %s.', $name));
    }

}
