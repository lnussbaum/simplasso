<?php

namespace Base;

use \MembreIndividu as ChildMembreIndividu;
use \MembreIndividuQuery as ChildMembreIndividuQuery;
use \Exception;
use \PDO;
use Map\MembreIndividuTableMap;
use Propel\Runtime\Propel;
use Propel\Runtime\ActiveQuery\Criteria;
use Propel\Runtime\ActiveQuery\ModelCriteria;
use Propel\Runtime\ActiveQuery\ModelJoin;
use Propel\Runtime\Collection\ObjectCollection;
use Propel\Runtime\Connection\ConnectionInterface;
use Propel\Runtime\Exception\PropelException;

/**
 * Base class that represents a query for the 'asso_membres_individus' table.
 *
 *
 *
 * @method     ChildMembreIndividuQuery orderByIdIndividu($order = Criteria::ASC) Order by the id_individu column
 * @method     ChildMembreIndividuQuery orderByIdMembre($order = Criteria::ASC) Order by the id_membre column
 * @method     ChildMembreIndividuQuery orderByDateDebut($order = Criteria::ASC) Order by the date_debut column
 * @method     ChildMembreIndividuQuery orderByDateFin($order = Criteria::ASC) Order by the date_fin column
 *
 * @method     ChildMembreIndividuQuery groupByIdIndividu() Group by the id_individu column
 * @method     ChildMembreIndividuQuery groupByIdMembre() Group by the id_membre column
 * @method     ChildMembreIndividuQuery groupByDateDebut() Group by the date_debut column
 * @method     ChildMembreIndividuQuery groupByDateFin() Group by the date_fin column
 *
 * @method     ChildMembreIndividuQuery leftJoin($relation) Adds a LEFT JOIN clause to the query
 * @method     ChildMembreIndividuQuery rightJoin($relation) Adds a RIGHT JOIN clause to the query
 * @method     ChildMembreIndividuQuery innerJoin($relation) Adds a INNER JOIN clause to the query
 *
 * @method     ChildMembreIndividuQuery leftJoinWith($relation) Adds a LEFT JOIN clause and with to the query
 * @method     ChildMembreIndividuQuery rightJoinWith($relation) Adds a RIGHT JOIN clause and with to the query
 * @method     ChildMembreIndividuQuery innerJoinWith($relation) Adds a INNER JOIN clause and with to the query
 *
 * @method     ChildMembreIndividuQuery leftJoinMembre($relationAlias = null) Adds a LEFT JOIN clause to the query using the Membre relation
 * @method     ChildMembreIndividuQuery rightJoinMembre($relationAlias = null) Adds a RIGHT JOIN clause to the query using the Membre relation
 * @method     ChildMembreIndividuQuery innerJoinMembre($relationAlias = null) Adds a INNER JOIN clause to the query using the Membre relation
 *
 * @method     ChildMembreIndividuQuery joinWithMembre($joinType = Criteria::INNER_JOIN) Adds a join clause and with to the query using the Membre relation
 *
 * @method     ChildMembreIndividuQuery leftJoinWithMembre() Adds a LEFT JOIN clause and with to the query using the Membre relation
 * @method     ChildMembreIndividuQuery rightJoinWithMembre() Adds a RIGHT JOIN clause and with to the query using the Membre relation
 * @method     ChildMembreIndividuQuery innerJoinWithMembre() Adds a INNER JOIN clause and with to the query using the Membre relation
 *
 * @method     ChildMembreIndividuQuery leftJoinIndividu($relationAlias = null) Adds a LEFT JOIN clause to the query using the Individu relation
 * @method     ChildMembreIndividuQuery rightJoinIndividu($relationAlias = null) Adds a RIGHT JOIN clause to the query using the Individu relation
 * @method     ChildMembreIndividuQuery innerJoinIndividu($relationAlias = null) Adds a INNER JOIN clause to the query using the Individu relation
 *
 * @method     ChildMembreIndividuQuery joinWithIndividu($joinType = Criteria::INNER_JOIN) Adds a join clause and with to the query using the Individu relation
 *
 * @method     ChildMembreIndividuQuery leftJoinWithIndividu() Adds a LEFT JOIN clause and with to the query using the Individu relation
 * @method     ChildMembreIndividuQuery rightJoinWithIndividu() Adds a RIGHT JOIN clause and with to the query using the Individu relation
 * @method     ChildMembreIndividuQuery innerJoinWithIndividu() Adds a INNER JOIN clause and with to the query using the Individu relation
 *
 * @method     \MembreQuery|\IndividuQuery endUse() Finalizes a secondary criteria and merges it with its primary Criteria
 *
 * @method     ChildMembreIndividu findOne(ConnectionInterface $con = null) Return the first ChildMembreIndividu matching the query
 * @method     ChildMembreIndividu findOneOrCreate(ConnectionInterface $con = null) Return the first ChildMembreIndividu matching the query, or a new ChildMembreIndividu object populated from the query conditions when no match is found
 *
 * @method     ChildMembreIndividu findOneByIdIndividu(string $id_individu) Return the first ChildMembreIndividu filtered by the id_individu column
 * @method     ChildMembreIndividu findOneByIdMembre(string $id_membre) Return the first ChildMembreIndividu filtered by the id_membre column
 * @method     ChildMembreIndividu findOneByDateDebut(string $date_debut) Return the first ChildMembreIndividu filtered by the date_debut column
 * @method     ChildMembreIndividu findOneByDateFin(string $date_fin) Return the first ChildMembreIndividu filtered by the date_fin column *

 * @method     ChildMembreIndividu requirePk($key, ConnectionInterface $con = null) Return the ChildMembreIndividu by primary key and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildMembreIndividu requireOne(ConnectionInterface $con = null) Return the first ChildMembreIndividu matching the query and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 *
 * @method     ChildMembreIndividu requireOneByIdIndividu(string $id_individu) Return the first ChildMembreIndividu filtered by the id_individu column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildMembreIndividu requireOneByIdMembre(string $id_membre) Return the first ChildMembreIndividu filtered by the id_membre column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildMembreIndividu requireOneByDateDebut(string $date_debut) Return the first ChildMembreIndividu filtered by the date_debut column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildMembreIndividu requireOneByDateFin(string $date_fin) Return the first ChildMembreIndividu filtered by the date_fin column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 *
 * @method     ChildMembreIndividu[]|ObjectCollection find(ConnectionInterface $con = null) Return ChildMembreIndividu objects based on current ModelCriteria
 * @method     ChildMembreIndividu[]|ObjectCollection findByIdIndividu(string $id_individu) Return ChildMembreIndividu objects filtered by the id_individu column
 * @method     ChildMembreIndividu[]|ObjectCollection findByIdMembre(string $id_membre) Return ChildMembreIndividu objects filtered by the id_membre column
 * @method     ChildMembreIndividu[]|ObjectCollection findByDateDebut(string $date_debut) Return ChildMembreIndividu objects filtered by the date_debut column
 * @method     ChildMembreIndividu[]|ObjectCollection findByDateFin(string $date_fin) Return ChildMembreIndividu objects filtered by the date_fin column
 * @method     ChildMembreIndividu[]|\Propel\Runtime\Util\PropelModelPager paginate($page = 1, $maxPerPage = 10, ConnectionInterface $con = null) Issue a SELECT query based on the current ModelCriteria and uses a page and a maximum number of results per page to compute an offset and a limit
 *
 */
abstract class MembreIndividuQuery extends ModelCriteria
{
    protected $entityNotFoundExceptionClass = '\\Propel\\Runtime\\Exception\\EntityNotFoundException';

    /**
     * Initializes internal state of \Base\MembreIndividuQuery object.
     *
     * @param     string $dbName The database name
     * @param     string $modelName The phpName of a model, e.g. 'Book'
     * @param     string $modelAlias The alias for the model in this query, e.g. 'b'
     */
    public function __construct($dbName = 'simplasso', $modelName = '\\MembreIndividu', $modelAlias = null)
    {
        parent::__construct($dbName, $modelName, $modelAlias);
    }

    /**
     * Returns a new ChildMembreIndividuQuery object.
     *
     * @param     string $modelAlias The alias of a model in the query
     * @param     Criteria $criteria Optional Criteria to build the query from
     *
     * @return ChildMembreIndividuQuery
     */
    public static function create($modelAlias = null, Criteria $criteria = null)
    {
        if ($criteria instanceof ChildMembreIndividuQuery) {
            return $criteria;
        }
        $query = new ChildMembreIndividuQuery();
        if (null !== $modelAlias) {
            $query->setModelAlias($modelAlias);
        }
        if ($criteria instanceof Criteria) {
            $query->mergeWith($criteria);
        }

        return $query;
    }

    /**
     * Find object by primary key.
     * Propel uses the instance pool to skip the database if the object exists.
     * Go fast if the query is untouched.
     *
     * <code>
     * $obj = $c->findPk(array(12, 34), $con);
     * </code>
     *
     * @param array[$id_individu, $id_membre] $key Primary key to use for the query
     * @param ConnectionInterface $con an optional connection object
     *
     * @return ChildMembreIndividu|array|mixed the result, formatted by the current formatter
     */
    public function findPk($key, ConnectionInterface $con = null)
    {
        if ($key === null) {
            return null;
        }

        if ($con === null) {
            $con = Propel::getServiceContainer()->getReadConnection(MembreIndividuTableMap::DATABASE_NAME);
        }

        $this->basePreSelect($con);

        if (
            $this->formatter || $this->modelAlias || $this->with || $this->select
            || $this->selectColumns || $this->asColumns || $this->selectModifiers
            || $this->map || $this->having || $this->joins
        ) {
            return $this->findPkComplex($key, $con);
        }

        if ((null !== ($obj = MembreIndividuTableMap::getInstanceFromPool(serialize([(null === $key[0] || is_scalar($key[0]) || is_callable([$key[0], '__toString']) ? (string) $key[0] : $key[0]), (null === $key[1] || is_scalar($key[1]) || is_callable([$key[1], '__toString']) ? (string) $key[1] : $key[1])]))))) {
            // the object is already in the instance pool
            return $obj;
        }

        return $this->findPkSimple($key, $con);
    }

    /**
     * Find object by primary key using raw SQL to go fast.
     * Bypass doSelect() and the object formatter by using generated code.
     *
     * @param     mixed $key Primary key to use for the query
     * @param     ConnectionInterface $con A connection object
     *
     * @throws \Propel\Runtime\Exception\PropelException
     *
     * @return ChildMembreIndividu A model object, or null if the key is not found
     */
    protected function findPkSimple($key, ConnectionInterface $con)
    {
        $sql = 'SELECT `id_individu`, `id_membre`, `date_debut`, `date_fin` FROM `asso_membres_individus` WHERE `id_individu` = :p0 AND `id_membre` = :p1';
        try {
            $stmt = $con->prepare($sql);
            $stmt->bindValue(':p0', $key[0], PDO::PARAM_INT);
            $stmt->bindValue(':p1', $key[1], PDO::PARAM_INT);
            $stmt->execute();
        } catch (Exception $e) {
            Propel::log($e->getMessage(), Propel::LOG_ERR);
            throw new PropelException(sprintf('Unable to execute SELECT statement [%s]', $sql), 0, $e);
        }
        $obj = null;
        if ($row = $stmt->fetch(\PDO::FETCH_NUM)) {
            /** @var ChildMembreIndividu $obj */
            $obj = new ChildMembreIndividu();
            $obj->hydrate($row);
            MembreIndividuTableMap::addInstanceToPool($obj, serialize([(null === $key[0] || is_scalar($key[0]) || is_callable([$key[0], '__toString']) ? (string) $key[0] : $key[0]), (null === $key[1] || is_scalar($key[1]) || is_callable([$key[1], '__toString']) ? (string) $key[1] : $key[1])]));
        }
        $stmt->closeCursor();

        return $obj;
    }

    /**
     * Find object by primary key.
     *
     * @param     mixed $key Primary key to use for the query
     * @param     ConnectionInterface $con A connection object
     *
     * @return ChildMembreIndividu|array|mixed the result, formatted by the current formatter
     */
    protected function findPkComplex($key, ConnectionInterface $con)
    {
        // As the query uses a PK condition, no limit(1) is necessary.
        $criteria = $this->isKeepQuery() ? clone $this : $this;
        $dataFetcher = $criteria
            ->filterByPrimaryKey($key)
            ->doSelect($con);

        return $criteria->getFormatter()->init($criteria)->formatOne($dataFetcher);
    }

    /**
     * Find objects by primary key
     * <code>
     * $objs = $c->findPks(array(array(12, 56), array(832, 123), array(123, 456)), $con);
     * </code>
     * @param     array $keys Primary keys to use for the query
     * @param     ConnectionInterface $con an optional connection object
     *
     * @return ObjectCollection|array|mixed the list of results, formatted by the current formatter
     */
    public function findPks($keys, ConnectionInterface $con = null)
    {
        if (null === $con) {
            $con = Propel::getServiceContainer()->getReadConnection($this->getDbName());
        }
        $this->basePreSelect($con);
        $criteria = $this->isKeepQuery() ? clone $this : $this;
        $dataFetcher = $criteria
            ->filterByPrimaryKeys($keys)
            ->doSelect($con);

        return $criteria->getFormatter()->init($criteria)->format($dataFetcher);
    }

    /**
     * Filter the query by primary key
     *
     * @param     mixed $key Primary key to use for the query
     *
     * @return $this|ChildMembreIndividuQuery The current query, for fluid interface
     */
    public function filterByPrimaryKey($key)
    {
        $this->addUsingAlias(MembreIndividuTableMap::COL_ID_INDIVIDU, $key[0], Criteria::EQUAL);
        $this->addUsingAlias(MembreIndividuTableMap::COL_ID_MEMBRE, $key[1], Criteria::EQUAL);

        return $this;
    }

    /**
     * Filter the query by a list of primary keys
     *
     * @param     array $keys The list of primary key to use for the query
     *
     * @return $this|ChildMembreIndividuQuery The current query, for fluid interface
     */
    public function filterByPrimaryKeys($keys)
    {
        if (empty($keys)) {
            return $this->add(null, '1<>1', Criteria::CUSTOM);
        }
        foreach ($keys as $key) {
            $cton0 = $this->getNewCriterion(MembreIndividuTableMap::COL_ID_INDIVIDU, $key[0], Criteria::EQUAL);
            $cton1 = $this->getNewCriterion(MembreIndividuTableMap::COL_ID_MEMBRE, $key[1], Criteria::EQUAL);
            $cton0->addAnd($cton1);
            $this->addOr($cton0);
        }

        return $this;
    }

    /**
     * Filter the query on the id_individu column
     *
     * Example usage:
     * <code>
     * $query->filterByIdIndividu(1234); // WHERE id_individu = 1234
     * $query->filterByIdIndividu(array(12, 34)); // WHERE id_individu IN (12, 34)
     * $query->filterByIdIndividu(array('min' => 12)); // WHERE id_individu > 12
     * </code>
     *
     * @see       filterByIndividu()
     *
     * @param     mixed $idIndividu The value to use as filter.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildMembreIndividuQuery The current query, for fluid interface
     */
    public function filterByIdIndividu($idIndividu = null, $comparison = null)
    {
        if (is_array($idIndividu)) {
            $useMinMax = false;
            if (isset($idIndividu['min'])) {
                $this->addUsingAlias(MembreIndividuTableMap::COL_ID_INDIVIDU, $idIndividu['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($idIndividu['max'])) {
                $this->addUsingAlias(MembreIndividuTableMap::COL_ID_INDIVIDU, $idIndividu['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(MembreIndividuTableMap::COL_ID_INDIVIDU, $idIndividu, $comparison);
    }

    /**
     * Filter the query on the id_membre column
     *
     * Example usage:
     * <code>
     * $query->filterByIdMembre(1234); // WHERE id_membre = 1234
     * $query->filterByIdMembre(array(12, 34)); // WHERE id_membre IN (12, 34)
     * $query->filterByIdMembre(array('min' => 12)); // WHERE id_membre > 12
     * </code>
     *
     * @see       filterByMembre()
     *
     * @param     mixed $idMembre The value to use as filter.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildMembreIndividuQuery The current query, for fluid interface
     */
    public function filterByIdMembre($idMembre = null, $comparison = null)
    {
        if (is_array($idMembre)) {
            $useMinMax = false;
            if (isset($idMembre['min'])) {
                $this->addUsingAlias(MembreIndividuTableMap::COL_ID_MEMBRE, $idMembre['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($idMembre['max'])) {
                $this->addUsingAlias(MembreIndividuTableMap::COL_ID_MEMBRE, $idMembre['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(MembreIndividuTableMap::COL_ID_MEMBRE, $idMembre, $comparison);
    }

    /**
     * Filter the query on the date_debut column
     *
     * Example usage:
     * <code>
     * $query->filterByDateDebut('2011-03-14'); // WHERE date_debut = '2011-03-14'
     * $query->filterByDateDebut('now'); // WHERE date_debut = '2011-03-14'
     * $query->filterByDateDebut(array('max' => 'yesterday')); // WHERE date_debut > '2011-03-13'
     * </code>
     *
     * @param     mixed $dateDebut The value to use as filter.
     *              Values can be integers (unix timestamps), DateTime objects, or strings.
     *              Empty strings are treated as NULL.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildMembreIndividuQuery The current query, for fluid interface
     */
    public function filterByDateDebut($dateDebut = null, $comparison = null)
    {
        if (is_array($dateDebut)) {
            $useMinMax = false;
            if (isset($dateDebut['min'])) {
                $this->addUsingAlias(MembreIndividuTableMap::COL_DATE_DEBUT, $dateDebut['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($dateDebut['max'])) {
                $this->addUsingAlias(MembreIndividuTableMap::COL_DATE_DEBUT, $dateDebut['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(MembreIndividuTableMap::COL_DATE_DEBUT, $dateDebut, $comparison);
    }

    /**
     * Filter the query on the date_fin column
     *
     * Example usage:
     * <code>
     * $query->filterByDateFin('2011-03-14'); // WHERE date_fin = '2011-03-14'
     * $query->filterByDateFin('now'); // WHERE date_fin = '2011-03-14'
     * $query->filterByDateFin(array('max' => 'yesterday')); // WHERE date_fin > '2011-03-13'
     * </code>
     *
     * @param     mixed $dateFin The value to use as filter.
     *              Values can be integers (unix timestamps), DateTime objects, or strings.
     *              Empty strings are treated as NULL.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildMembreIndividuQuery The current query, for fluid interface
     */
    public function filterByDateFin($dateFin = null, $comparison = null)
    {
        if (is_array($dateFin)) {
            $useMinMax = false;
            if (isset($dateFin['min'])) {
                $this->addUsingAlias(MembreIndividuTableMap::COL_DATE_FIN, $dateFin['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($dateFin['max'])) {
                $this->addUsingAlias(MembreIndividuTableMap::COL_DATE_FIN, $dateFin['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(MembreIndividuTableMap::COL_DATE_FIN, $dateFin, $comparison);
    }

    /**
     * Filter the query by a related \Membre object
     *
     * @param \Membre|ObjectCollection $membre The related object(s) to use as filter
     * @param string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @throws \Propel\Runtime\Exception\PropelException
     *
     * @return ChildMembreIndividuQuery The current query, for fluid interface
     */
    public function filterByMembre($membre, $comparison = null)
    {
        if ($membre instanceof \Membre) {
            return $this
                ->addUsingAlias(MembreIndividuTableMap::COL_ID_MEMBRE, $membre->getIdMembre(), $comparison);
        } elseif ($membre instanceof ObjectCollection) {
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }

            return $this
                ->addUsingAlias(MembreIndividuTableMap::COL_ID_MEMBRE, $membre->toKeyValue('PrimaryKey', 'IdMembre'), $comparison);
        } else {
            throw new PropelException('filterByMembre() only accepts arguments of type \Membre or Collection');
        }
    }

    /**
     * Adds a JOIN clause to the query using the Membre relation
     *
     * @param     string $relationAlias optional alias for the relation
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return $this|ChildMembreIndividuQuery The current query, for fluid interface
     */
    public function joinMembre($relationAlias = null, $joinType = Criteria::INNER_JOIN)
    {
        $tableMap = $this->getTableMap();
        $relationMap = $tableMap->getRelation('Membre');

        // create a ModelJoin object for this join
        $join = new ModelJoin();
        $join->setJoinType($joinType);
        $join->setRelationMap($relationMap, $this->useAliasInSQL ? $this->getModelAlias() : null, $relationAlias);
        if ($previousJoin = $this->getPreviousJoin()) {
            $join->setPreviousJoin($previousJoin);
        }

        // add the ModelJoin to the current object
        if ($relationAlias) {
            $this->addAlias($relationAlias, $relationMap->getRightTable()->getName());
            $this->addJoinObject($join, $relationAlias);
        } else {
            $this->addJoinObject($join, 'Membre');
        }

        return $this;
    }

    /**
     * Use the Membre relation Membre object
     *
     * @see useQuery()
     *
     * @param     string $relationAlias optional alias for the relation,
     *                                   to be used as main alias in the secondary query
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return \MembreQuery A secondary query class using the current class as primary query
     */
    public function useMembreQuery($relationAlias = null, $joinType = Criteria::INNER_JOIN)
    {
        return $this
            ->joinMembre($relationAlias, $joinType)
            ->useQuery($relationAlias ? $relationAlias : 'Membre', '\MembreQuery');
    }

    /**
     * Filter the query by a related \Individu object
     *
     * @param \Individu|ObjectCollection $individu The related object(s) to use as filter
     * @param string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @throws \Propel\Runtime\Exception\PropelException
     *
     * @return ChildMembreIndividuQuery The current query, for fluid interface
     */
    public function filterByIndividu($individu, $comparison = null)
    {
        if ($individu instanceof \Individu) {
            return $this
                ->addUsingAlias(MembreIndividuTableMap::COL_ID_INDIVIDU, $individu->getIdIndividu(), $comparison);
        } elseif ($individu instanceof ObjectCollection) {
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }

            return $this
                ->addUsingAlias(MembreIndividuTableMap::COL_ID_INDIVIDU, $individu->toKeyValue('PrimaryKey', 'IdIndividu'), $comparison);
        } else {
            throw new PropelException('filterByIndividu() only accepts arguments of type \Individu or Collection');
        }
    }

    /**
     * Adds a JOIN clause to the query using the Individu relation
     *
     * @param     string $relationAlias optional alias for the relation
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return $this|ChildMembreIndividuQuery The current query, for fluid interface
     */
    public function joinIndividu($relationAlias = null, $joinType = Criteria::INNER_JOIN)
    {
        $tableMap = $this->getTableMap();
        $relationMap = $tableMap->getRelation('Individu');

        // create a ModelJoin object for this join
        $join = new ModelJoin();
        $join->setJoinType($joinType);
        $join->setRelationMap($relationMap, $this->useAliasInSQL ? $this->getModelAlias() : null, $relationAlias);
        if ($previousJoin = $this->getPreviousJoin()) {
            $join->setPreviousJoin($previousJoin);
        }

        // add the ModelJoin to the current object
        if ($relationAlias) {
            $this->addAlias($relationAlias, $relationMap->getRightTable()->getName());
            $this->addJoinObject($join, $relationAlias);
        } else {
            $this->addJoinObject($join, 'Individu');
        }

        return $this;
    }

    /**
     * Use the Individu relation Individu object
     *
     * @see useQuery()
     *
     * @param     string $relationAlias optional alias for the relation,
     *                                   to be used as main alias in the secondary query
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return \IndividuQuery A secondary query class using the current class as primary query
     */
    public function useIndividuQuery($relationAlias = null, $joinType = Criteria::INNER_JOIN)
    {
        return $this
            ->joinIndividu($relationAlias, $joinType)
            ->useQuery($relationAlias ? $relationAlias : 'Individu', '\IndividuQuery');
    }

    /**
     * Exclude object from result
     *
     * @param   ChildMembreIndividu $membreIndividu Object to remove from the list of results
     *
     * @return $this|ChildMembreIndividuQuery The current query, for fluid interface
     */
    public function prune($membreIndividu = null)
    {
        if ($membreIndividu) {
            $this->addCond('pruneCond0', $this->getAliasedColName(MembreIndividuTableMap::COL_ID_INDIVIDU), $membreIndividu->getIdIndividu(), Criteria::NOT_EQUAL);
            $this->addCond('pruneCond1', $this->getAliasedColName(MembreIndividuTableMap::COL_ID_MEMBRE), $membreIndividu->getIdMembre(), Criteria::NOT_EQUAL);
            $this->combine(array('pruneCond0', 'pruneCond1'), Criteria::LOGICAL_OR);
        }

        return $this;
    }

    /**
     * Deletes all rows from the asso_membres_individus table.
     *
     * @param ConnectionInterface $con the connection to use
     * @return int The number of affected rows (if supported by underlying database driver).
     */
    public function doDeleteAll(ConnectionInterface $con = null)
    {
        if (null === $con) {
            $con = Propel::getServiceContainer()->getWriteConnection(MembreIndividuTableMap::DATABASE_NAME);
        }

        // use transaction because $criteria could contain info
        // for more than one table or we could emulating ON DELETE CASCADE, etc.
        return $con->transaction(function () use ($con) {
            $affectedRows = 0; // initialize var to track total num of affected rows
            $affectedRows += parent::doDeleteAll($con);
            // Because this db requires some delete cascade/set null emulation, we have to
            // clear the cached instance *after* the emulation has happened (since
            // instances get re-added by the select statement contained therein).
            MembreIndividuTableMap::clearInstancePool();
            MembreIndividuTableMap::clearRelatedInstancePool();

            return $affectedRows;
        });
    }

    /**
     * Performs a DELETE on the database based on the current ModelCriteria
     *
     * @param ConnectionInterface $con the connection to use
     * @return int             The number of affected rows (if supported by underlying database driver).  This includes CASCADE-related rows
     *                         if supported by native driver or if emulated using Propel.
     * @throws PropelException Any exceptions caught during processing will be
     *                         rethrown wrapped into a PropelException.
     */
    public function delete(ConnectionInterface $con = null)
    {
        if (null === $con) {
            $con = Propel::getServiceContainer()->getWriteConnection(MembreIndividuTableMap::DATABASE_NAME);
        }

        $criteria = $this;

        // Set the correct dbName
        $criteria->setDbName(MembreIndividuTableMap::DATABASE_NAME);

        // use transaction because $criteria could contain info
        // for more than one table or we could emulating ON DELETE CASCADE, etc.
        return $con->transaction(function () use ($con, $criteria) {
            $affectedRows = 0; // initialize var to track total num of affected rows

            MembreIndividuTableMap::removeInstanceFromPool($criteria);

            $affectedRows += ModelCriteria::delete($con);
            MembreIndividuTableMap::clearRelatedInstancePool();

            return $affectedRows;
        });
    }

} // MembreIndividuQuery
