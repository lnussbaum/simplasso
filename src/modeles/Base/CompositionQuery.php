<?php

namespace Base;

use \Composition as ChildComposition;
use \CompositionQuery as ChildCompositionQuery;
use \Exception;
use \PDO;
use Map\CompositionTableMap;
use Propel\Runtime\Propel;
use Propel\Runtime\ActiveQuery\Criteria;
use Propel\Runtime\ActiveQuery\ModelCriteria;
use Propel\Runtime\ActiveQuery\ModelJoin;
use Propel\Runtime\Collection\ObjectCollection;
use Propel\Runtime\Connection\ConnectionInterface;
use Propel\Runtime\Exception\PropelException;

/**
 * Base class that represents a query for the 'com_compositions' table.
 *
 *
 *
 * @method     ChildCompositionQuery orderByIdComposition($order = Criteria::ASC) Order by the id_composition column
 * @method     ChildCompositionQuery orderByNom($order = Criteria::ASC) Order by the nom column
 *
 * @method     ChildCompositionQuery groupByIdComposition() Group by the id_composition column
 * @method     ChildCompositionQuery groupByNom() Group by the nom column
 *
 * @method     ChildCompositionQuery leftJoin($relation) Adds a LEFT JOIN clause to the query
 * @method     ChildCompositionQuery rightJoin($relation) Adds a RIGHT JOIN clause to the query
 * @method     ChildCompositionQuery innerJoin($relation) Adds a INNER JOIN clause to the query
 *
 * @method     ChildCompositionQuery leftJoinWith($relation) Adds a LEFT JOIN clause and with to the query
 * @method     ChildCompositionQuery rightJoinWith($relation) Adds a RIGHT JOIN clause and with to the query
 * @method     ChildCompositionQuery innerJoinWith($relation) Adds a INNER JOIN clause and with to the query
 *
 * @method     ChildCompositionQuery leftJoinCourrier($relationAlias = null) Adds a LEFT JOIN clause to the query using the Courrier relation
 * @method     ChildCompositionQuery rightJoinCourrier($relationAlias = null) Adds a RIGHT JOIN clause to the query using the Courrier relation
 * @method     ChildCompositionQuery innerJoinCourrier($relationAlias = null) Adds a INNER JOIN clause to the query using the Courrier relation
 *
 * @method     ChildCompositionQuery joinWithCourrier($joinType = Criteria::INNER_JOIN) Adds a join clause and with to the query using the Courrier relation
 *
 * @method     ChildCompositionQuery leftJoinWithCourrier() Adds a LEFT JOIN clause and with to the query using the Courrier relation
 * @method     ChildCompositionQuery rightJoinWithCourrier() Adds a RIGHT JOIN clause and with to the query using the Courrier relation
 * @method     ChildCompositionQuery innerJoinWithCourrier() Adds a INNER JOIN clause and with to the query using the Courrier relation
 *
 * @method     ChildCompositionQuery leftJoinCompositionsBloc($relationAlias = null) Adds a LEFT JOIN clause to the query using the CompositionsBloc relation
 * @method     ChildCompositionQuery rightJoinCompositionsBloc($relationAlias = null) Adds a RIGHT JOIN clause to the query using the CompositionsBloc relation
 * @method     ChildCompositionQuery innerJoinCompositionsBloc($relationAlias = null) Adds a INNER JOIN clause to the query using the CompositionsBloc relation
 *
 * @method     ChildCompositionQuery joinWithCompositionsBloc($joinType = Criteria::INNER_JOIN) Adds a join clause and with to the query using the CompositionsBloc relation
 *
 * @method     ChildCompositionQuery leftJoinWithCompositionsBloc() Adds a LEFT JOIN clause and with to the query using the CompositionsBloc relation
 * @method     ChildCompositionQuery rightJoinWithCompositionsBloc() Adds a RIGHT JOIN clause and with to the query using the CompositionsBloc relation
 * @method     ChildCompositionQuery innerJoinWithCompositionsBloc() Adds a INNER JOIN clause and with to the query using the CompositionsBloc relation
 *
 * @method     \CourrierQuery|\CompositionsBlocQuery endUse() Finalizes a secondary criteria and merges it with its primary Criteria
 *
 * @method     ChildComposition findOne(ConnectionInterface $con = null) Return the first ChildComposition matching the query
 * @method     ChildComposition findOneOrCreate(ConnectionInterface $con = null) Return the first ChildComposition matching the query, or a new ChildComposition object populated from the query conditions when no match is found
 *
 * @method     ChildComposition findOneByIdComposition(int $id_composition) Return the first ChildComposition filtered by the id_composition column
 * @method     ChildComposition findOneByNom(string $nom) Return the first ChildComposition filtered by the nom column *

 * @method     ChildComposition requirePk($key, ConnectionInterface $con = null) Return the ChildComposition by primary key and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildComposition requireOne(ConnectionInterface $con = null) Return the first ChildComposition matching the query and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 *
 * @method     ChildComposition requireOneByIdComposition(int $id_composition) Return the first ChildComposition filtered by the id_composition column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildComposition requireOneByNom(string $nom) Return the first ChildComposition filtered by the nom column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 *
 * @method     ChildComposition[]|ObjectCollection find(ConnectionInterface $con = null) Return ChildComposition objects based on current ModelCriteria
 * @method     ChildComposition[]|ObjectCollection findByIdComposition(int $id_composition) Return ChildComposition objects filtered by the id_composition column
 * @method     ChildComposition[]|ObjectCollection findByNom(string $nom) Return ChildComposition objects filtered by the nom column
 * @method     ChildComposition[]|\Propel\Runtime\Util\PropelModelPager paginate($page = 1, $maxPerPage = 10, ConnectionInterface $con = null) Issue a SELECT query based on the current ModelCriteria and uses a page and a maximum number of results per page to compute an offset and a limit
 *
 */
abstract class CompositionQuery extends ModelCriteria
{
    protected $entityNotFoundExceptionClass = '\\Propel\\Runtime\\Exception\\EntityNotFoundException';

    /**
     * Initializes internal state of \Base\CompositionQuery object.
     *
     * @param     string $dbName The database name
     * @param     string $modelName The phpName of a model, e.g. 'Book'
     * @param     string $modelAlias The alias for the model in this query, e.g. 'b'
     */
    public function __construct($dbName = 'simplasso', $modelName = '\\Composition', $modelAlias = null)
    {
        parent::__construct($dbName, $modelName, $modelAlias);
    }

    /**
     * Returns a new ChildCompositionQuery object.
     *
     * @param     string $modelAlias The alias of a model in the query
     * @param     Criteria $criteria Optional Criteria to build the query from
     *
     * @return ChildCompositionQuery
     */
    public static function create($modelAlias = null, Criteria $criteria = null)
    {
        if ($criteria instanceof ChildCompositionQuery) {
            return $criteria;
        }
        $query = new ChildCompositionQuery();
        if (null !== $modelAlias) {
            $query->setModelAlias($modelAlias);
        }
        if ($criteria instanceof Criteria) {
            $query->mergeWith($criteria);
        }

        return $query;
    }

    /**
     * Find object by primary key.
     * Propel uses the instance pool to skip the database if the object exists.
     * Go fast if the query is untouched.
     *
     * <code>
     * $obj  = $c->findPk(12, $con);
     * </code>
     *
     * @param mixed $key Primary key to use for the query
     * @param ConnectionInterface $con an optional connection object
     *
     * @return ChildComposition|array|mixed the result, formatted by the current formatter
     */
    public function findPk($key, ConnectionInterface $con = null)
    {
        if ($key === null) {
            return null;
        }

        if ($con === null) {
            $con = Propel::getServiceContainer()->getReadConnection(CompositionTableMap::DATABASE_NAME);
        }

        $this->basePreSelect($con);

        if (
            $this->formatter || $this->modelAlias || $this->with || $this->select
            || $this->selectColumns || $this->asColumns || $this->selectModifiers
            || $this->map || $this->having || $this->joins
        ) {
            return $this->findPkComplex($key, $con);
        }

        if ((null !== ($obj = CompositionTableMap::getInstanceFromPool(null === $key || is_scalar($key) || is_callable([$key, '__toString']) ? (string) $key : $key)))) {
            // the object is already in the instance pool
            return $obj;
        }

        return $this->findPkSimple($key, $con);
    }

    /**
     * Find object by primary key using raw SQL to go fast.
     * Bypass doSelect() and the object formatter by using generated code.
     *
     * @param     mixed $key Primary key to use for the query
     * @param     ConnectionInterface $con A connection object
     *
     * @throws \Propel\Runtime\Exception\PropelException
     *
     * @return ChildComposition A model object, or null if the key is not found
     */
    protected function findPkSimple($key, ConnectionInterface $con)
    {
        $sql = 'SELECT `id_composition`, `nom` FROM `com_compositions` WHERE `id_composition` = :p0';
        try {
            $stmt = $con->prepare($sql);
            $stmt->bindValue(':p0', $key, PDO::PARAM_INT);
            $stmt->execute();
        } catch (Exception $e) {
            Propel::log($e->getMessage(), Propel::LOG_ERR);
            throw new PropelException(sprintf('Unable to execute SELECT statement [%s]', $sql), 0, $e);
        }
        $obj = null;
        if ($row = $stmt->fetch(\PDO::FETCH_NUM)) {
            /** @var ChildComposition $obj */
            $obj = new ChildComposition();
            $obj->hydrate($row);
            CompositionTableMap::addInstanceToPool($obj, null === $key || is_scalar($key) || is_callable([$key, '__toString']) ? (string) $key : $key);
        }
        $stmt->closeCursor();

        return $obj;
    }

    /**
     * Find object by primary key.
     *
     * @param     mixed $key Primary key to use for the query
     * @param     ConnectionInterface $con A connection object
     *
     * @return ChildComposition|array|mixed the result, formatted by the current formatter
     */
    protected function findPkComplex($key, ConnectionInterface $con)
    {
        // As the query uses a PK condition, no limit(1) is necessary.
        $criteria = $this->isKeepQuery() ? clone $this : $this;
        $dataFetcher = $criteria
            ->filterByPrimaryKey($key)
            ->doSelect($con);

        return $criteria->getFormatter()->init($criteria)->formatOne($dataFetcher);
    }

    /**
     * Find objects by primary key
     * <code>
     * $objs = $c->findPks(array(12, 56, 832), $con);
     * </code>
     * @param     array $keys Primary keys to use for the query
     * @param     ConnectionInterface $con an optional connection object
     *
     * @return ObjectCollection|array|mixed the list of results, formatted by the current formatter
     */
    public function findPks($keys, ConnectionInterface $con = null)
    {
        if (null === $con) {
            $con = Propel::getServiceContainer()->getReadConnection($this->getDbName());
        }
        $this->basePreSelect($con);
        $criteria = $this->isKeepQuery() ? clone $this : $this;
        $dataFetcher = $criteria
            ->filterByPrimaryKeys($keys)
            ->doSelect($con);

        return $criteria->getFormatter()->init($criteria)->format($dataFetcher);
    }

    /**
     * Filter the query by primary key
     *
     * @param     mixed $key Primary key to use for the query
     *
     * @return $this|ChildCompositionQuery The current query, for fluid interface
     */
    public function filterByPrimaryKey($key)
    {

        return $this->addUsingAlias(CompositionTableMap::COL_ID_COMPOSITION, $key, Criteria::EQUAL);
    }

    /**
     * Filter the query by a list of primary keys
     *
     * @param     array $keys The list of primary key to use for the query
     *
     * @return $this|ChildCompositionQuery The current query, for fluid interface
     */
    public function filterByPrimaryKeys($keys)
    {

        return $this->addUsingAlias(CompositionTableMap::COL_ID_COMPOSITION, $keys, Criteria::IN);
    }

    /**
     * Filter the query on the id_composition column
     *
     * Example usage:
     * <code>
     * $query->filterByIdComposition(1234); // WHERE id_composition = 1234
     * $query->filterByIdComposition(array(12, 34)); // WHERE id_composition IN (12, 34)
     * $query->filterByIdComposition(array('min' => 12)); // WHERE id_composition > 12
     * </code>
     *
     * @param     mixed $idComposition The value to use as filter.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildCompositionQuery The current query, for fluid interface
     */
    public function filterByIdComposition($idComposition = null, $comparison = null)
    {
        if (is_array($idComposition)) {
            $useMinMax = false;
            if (isset($idComposition['min'])) {
                $this->addUsingAlias(CompositionTableMap::COL_ID_COMPOSITION, $idComposition['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($idComposition['max'])) {
                $this->addUsingAlias(CompositionTableMap::COL_ID_COMPOSITION, $idComposition['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(CompositionTableMap::COL_ID_COMPOSITION, $idComposition, $comparison);
    }

    /**
     * Filter the query on the nom column
     *
     * Example usage:
     * <code>
     * $query->filterByNom('fooValue');   // WHERE nom = 'fooValue'
     * $query->filterByNom('%fooValue%', Criteria::LIKE); // WHERE nom LIKE '%fooValue%'
     * </code>
     *
     * @param     string $nom The value to use as filter.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildCompositionQuery The current query, for fluid interface
     */
    public function filterByNom($nom = null, $comparison = null)
    {
        if (null === $comparison) {
            if (is_array($nom)) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(CompositionTableMap::COL_NOM, $nom, $comparison);
    }

    /**
     * Filter the query by a related \Courrier object
     *
     * @param \Courrier|ObjectCollection $courrier the related object to use as filter
     * @param string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return ChildCompositionQuery The current query, for fluid interface
     */
    public function filterByCourrier($courrier, $comparison = null)
    {
        if ($courrier instanceof \Courrier) {
            return $this
                ->addUsingAlias(CompositionTableMap::COL_ID_COMPOSITION, $courrier->getIdComposition(), $comparison);
        } elseif ($courrier instanceof ObjectCollection) {
            return $this
                ->useCourrierQuery()
                ->filterByPrimaryKeys($courrier->getPrimaryKeys())
                ->endUse();
        } else {
            throw new PropelException('filterByCourrier() only accepts arguments of type \Courrier or Collection');
        }
    }

    /**
     * Adds a JOIN clause to the query using the Courrier relation
     *
     * @param     string $relationAlias optional alias for the relation
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return $this|ChildCompositionQuery The current query, for fluid interface
     */
    public function joinCourrier($relationAlias = null, $joinType = Criteria::LEFT_JOIN)
    {
        $tableMap = $this->getTableMap();
        $relationMap = $tableMap->getRelation('Courrier');

        // create a ModelJoin object for this join
        $join = new ModelJoin();
        $join->setJoinType($joinType);
        $join->setRelationMap($relationMap, $this->useAliasInSQL ? $this->getModelAlias() : null, $relationAlias);
        if ($previousJoin = $this->getPreviousJoin()) {
            $join->setPreviousJoin($previousJoin);
        }

        // add the ModelJoin to the current object
        if ($relationAlias) {
            $this->addAlias($relationAlias, $relationMap->getRightTable()->getName());
            $this->addJoinObject($join, $relationAlias);
        } else {
            $this->addJoinObject($join, 'Courrier');
        }

        return $this;
    }

    /**
     * Use the Courrier relation Courrier object
     *
     * @see useQuery()
     *
     * @param     string $relationAlias optional alias for the relation,
     *                                   to be used as main alias in the secondary query
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return \CourrierQuery A secondary query class using the current class as primary query
     */
    public function useCourrierQuery($relationAlias = null, $joinType = Criteria::LEFT_JOIN)
    {
        return $this
            ->joinCourrier($relationAlias, $joinType)
            ->useQuery($relationAlias ? $relationAlias : 'Courrier', '\CourrierQuery');
    }

    /**
     * Filter the query by a related \CompositionsBloc object
     *
     * @param \CompositionsBloc|ObjectCollection $compositionsBloc the related object to use as filter
     * @param string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return ChildCompositionQuery The current query, for fluid interface
     */
    public function filterByCompositionsBloc($compositionsBloc, $comparison = null)
    {
        if ($compositionsBloc instanceof \CompositionsBloc) {
            return $this
                ->addUsingAlias(CompositionTableMap::COL_ID_COMPOSITION, $compositionsBloc->getIdComposition(), $comparison);
        } elseif ($compositionsBloc instanceof ObjectCollection) {
            return $this
                ->useCompositionsBlocQuery()
                ->filterByPrimaryKeys($compositionsBloc->getPrimaryKeys())
                ->endUse();
        } else {
            throw new PropelException('filterByCompositionsBloc() only accepts arguments of type \CompositionsBloc or Collection');
        }
    }

    /**
     * Adds a JOIN clause to the query using the CompositionsBloc relation
     *
     * @param     string $relationAlias optional alias for the relation
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return $this|ChildCompositionQuery The current query, for fluid interface
     */
    public function joinCompositionsBloc($relationAlias = null, $joinType = Criteria::INNER_JOIN)
    {
        $tableMap = $this->getTableMap();
        $relationMap = $tableMap->getRelation('CompositionsBloc');

        // create a ModelJoin object for this join
        $join = new ModelJoin();
        $join->setJoinType($joinType);
        $join->setRelationMap($relationMap, $this->useAliasInSQL ? $this->getModelAlias() : null, $relationAlias);
        if ($previousJoin = $this->getPreviousJoin()) {
            $join->setPreviousJoin($previousJoin);
        }

        // add the ModelJoin to the current object
        if ($relationAlias) {
            $this->addAlias($relationAlias, $relationMap->getRightTable()->getName());
            $this->addJoinObject($join, $relationAlias);
        } else {
            $this->addJoinObject($join, 'CompositionsBloc');
        }

        return $this;
    }

    /**
     * Use the CompositionsBloc relation CompositionsBloc object
     *
     * @see useQuery()
     *
     * @param     string $relationAlias optional alias for the relation,
     *                                   to be used as main alias in the secondary query
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return \CompositionsBlocQuery A secondary query class using the current class as primary query
     */
    public function useCompositionsBlocQuery($relationAlias = null, $joinType = Criteria::INNER_JOIN)
    {
        return $this
            ->joinCompositionsBloc($relationAlias, $joinType)
            ->useQuery($relationAlias ? $relationAlias : 'CompositionsBloc', '\CompositionsBlocQuery');
    }

    /**
     * Filter the query by a related Bloc object
     * using the com_compositions_blocs table as cross reference
     *
     * @param Bloc $bloc the related object to use as filter
     * @param string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return ChildCompositionQuery The current query, for fluid interface
     */
    public function filterByBloc($bloc, $comparison = Criteria::EQUAL)
    {
        return $this
            ->useCompositionsBlocQuery()
            ->filterByBloc($bloc, $comparison)
            ->endUse();
    }

    /**
     * Exclude object from result
     *
     * @param   ChildComposition $composition Object to remove from the list of results
     *
     * @return $this|ChildCompositionQuery The current query, for fluid interface
     */
    public function prune($composition = null)
    {
        if ($composition) {
            $this->addUsingAlias(CompositionTableMap::COL_ID_COMPOSITION, $composition->getIdComposition(), Criteria::NOT_EQUAL);
        }

        return $this;
    }

    /**
     * Deletes all rows from the com_compositions table.
     *
     * @param ConnectionInterface $con the connection to use
     * @return int The number of affected rows (if supported by underlying database driver).
     */
    public function doDeleteAll(ConnectionInterface $con = null)
    {
        if (null === $con) {
            $con = Propel::getServiceContainer()->getWriteConnection(CompositionTableMap::DATABASE_NAME);
        }

        // use transaction because $criteria could contain info
        // for more than one table or we could emulating ON DELETE CASCADE, etc.
        return $con->transaction(function () use ($con) {
            $affectedRows = 0; // initialize var to track total num of affected rows
            $affectedRows += parent::doDeleteAll($con);
            // Because this db requires some delete cascade/set null emulation, we have to
            // clear the cached instance *after* the emulation has happened (since
            // instances get re-added by the select statement contained therein).
            CompositionTableMap::clearInstancePool();
            CompositionTableMap::clearRelatedInstancePool();

            return $affectedRows;
        });
    }

    /**
     * Performs a DELETE on the database based on the current ModelCriteria
     *
     * @param ConnectionInterface $con the connection to use
     * @return int             The number of affected rows (if supported by underlying database driver).  This includes CASCADE-related rows
     *                         if supported by native driver or if emulated using Propel.
     * @throws PropelException Any exceptions caught during processing will be
     *                         rethrown wrapped into a PropelException.
     */
    public function delete(ConnectionInterface $con = null)
    {
        if (null === $con) {
            $con = Propel::getServiceContainer()->getWriteConnection(CompositionTableMap::DATABASE_NAME);
        }

        $criteria = $this;

        // Set the correct dbName
        $criteria->setDbName(CompositionTableMap::DATABASE_NAME);

        // use transaction because $criteria could contain info
        // for more than one table or we could emulating ON DELETE CASCADE, etc.
        return $con->transaction(function () use ($con, $criteria) {
            $affectedRows = 0; // initialize var to track total num of affected rows

            CompositionTableMap::removeInstanceFromPool($criteria);

            $affectedRows += ModelCriteria::delete($con);
            CompositionTableMap::clearRelatedInstancePool();

            return $affectedRows;
        });
    }

} // CompositionQuery
