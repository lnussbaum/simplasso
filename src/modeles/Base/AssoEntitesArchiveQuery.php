<?php

namespace Base;

use \AssoEntitesArchive as ChildAssoEntitesArchive;
use \AssoEntitesArchiveQuery as ChildAssoEntitesArchiveQuery;
use \Exception;
use \PDO;
use Map\AssoEntitesArchiveTableMap;
use Propel\Runtime\Propel;
use Propel\Runtime\ActiveQuery\Criteria;
use Propel\Runtime\ActiveQuery\ModelCriteria;
use Propel\Runtime\Collection\ObjectCollection;
use Propel\Runtime\Connection\ConnectionInterface;
use Propel\Runtime\Exception\PropelException;

/**
 * Base class that represents a query for the 'asso_entites_archive' table.
 *
 *
 *
 * @method     ChildAssoEntitesArchiveQuery orderByIdEntite($order = Criteria::ASC) Order by the id_entite column
 * @method     ChildAssoEntitesArchiveQuery orderByNom($order = Criteria::ASC) Order by the nom column
 * @method     ChildAssoEntitesArchiveQuery orderByNomcourt($order = Criteria::ASC) Order by the nomcourt column
 * @method     ChildAssoEntitesArchiveQuery orderByAdresse($order = Criteria::ASC) Order by the adresse column
 * @method     ChildAssoEntitesArchiveQuery orderByCodepostal($order = Criteria::ASC) Order by the codepostal column
 * @method     ChildAssoEntitesArchiveQuery orderByVille($order = Criteria::ASC) Order by the ville column
 * @method     ChildAssoEntitesArchiveQuery orderByPays($order = Criteria::ASC) Order by the pays column
 * @method     ChildAssoEntitesArchiveQuery orderByTelephone($order = Criteria::ASC) Order by the telephone column
 * @method     ChildAssoEntitesArchiveQuery orderByFax($order = Criteria::ASC) Order by the fax column
 * @method     ChildAssoEntitesArchiveQuery orderByUrl($order = Criteria::ASC) Order by the url column
 * @method     ChildAssoEntitesArchiveQuery orderByAssujettitva($order = Criteria::ASC) Order by the assujettitva column
 * @method     ChildAssoEntitesArchiveQuery orderByEmail($order = Criteria::ASC) Order by the email column
 * @method     ChildAssoEntitesArchiveQuery orderByCreatedAt($order = Criteria::ASC) Order by the created_at column
 * @method     ChildAssoEntitesArchiveQuery orderByUpdatedAt($order = Criteria::ASC) Order by the updated_at column
 * @method     ChildAssoEntitesArchiveQuery orderByArchivedAt($order = Criteria::ASC) Order by the archived_at column
 *
 * @method     ChildAssoEntitesArchiveQuery groupByIdEntite() Group by the id_entite column
 * @method     ChildAssoEntitesArchiveQuery groupByNom() Group by the nom column
 * @method     ChildAssoEntitesArchiveQuery groupByNomcourt() Group by the nomcourt column
 * @method     ChildAssoEntitesArchiveQuery groupByAdresse() Group by the adresse column
 * @method     ChildAssoEntitesArchiveQuery groupByCodepostal() Group by the codepostal column
 * @method     ChildAssoEntitesArchiveQuery groupByVille() Group by the ville column
 * @method     ChildAssoEntitesArchiveQuery groupByPays() Group by the pays column
 * @method     ChildAssoEntitesArchiveQuery groupByTelephone() Group by the telephone column
 * @method     ChildAssoEntitesArchiveQuery groupByFax() Group by the fax column
 * @method     ChildAssoEntitesArchiveQuery groupByUrl() Group by the url column
 * @method     ChildAssoEntitesArchiveQuery groupByAssujettitva() Group by the assujettitva column
 * @method     ChildAssoEntitesArchiveQuery groupByEmail() Group by the email column
 * @method     ChildAssoEntitesArchiveQuery groupByCreatedAt() Group by the created_at column
 * @method     ChildAssoEntitesArchiveQuery groupByUpdatedAt() Group by the updated_at column
 * @method     ChildAssoEntitesArchiveQuery groupByArchivedAt() Group by the archived_at column
 *
 * @method     ChildAssoEntitesArchiveQuery leftJoin($relation) Adds a LEFT JOIN clause to the query
 * @method     ChildAssoEntitesArchiveQuery rightJoin($relation) Adds a RIGHT JOIN clause to the query
 * @method     ChildAssoEntitesArchiveQuery innerJoin($relation) Adds a INNER JOIN clause to the query
 *
 * @method     ChildAssoEntitesArchiveQuery leftJoinWith($relation) Adds a LEFT JOIN clause and with to the query
 * @method     ChildAssoEntitesArchiveQuery rightJoinWith($relation) Adds a RIGHT JOIN clause and with to the query
 * @method     ChildAssoEntitesArchiveQuery innerJoinWith($relation) Adds a INNER JOIN clause and with to the query
 *
 * @method     ChildAssoEntitesArchive findOne(ConnectionInterface $con = null) Return the first ChildAssoEntitesArchive matching the query
 * @method     ChildAssoEntitesArchive findOneOrCreate(ConnectionInterface $con = null) Return the first ChildAssoEntitesArchive matching the query, or a new ChildAssoEntitesArchive object populated from the query conditions when no match is found
 *
 * @method     ChildAssoEntitesArchive findOneByIdEntite(string $id_entite) Return the first ChildAssoEntitesArchive filtered by the id_entite column
 * @method     ChildAssoEntitesArchive findOneByNom(string $nom) Return the first ChildAssoEntitesArchive filtered by the nom column
 * @method     ChildAssoEntitesArchive findOneByNomcourt(string $nomcourt) Return the first ChildAssoEntitesArchive filtered by the nomcourt column
 * @method     ChildAssoEntitesArchive findOneByAdresse(string $adresse) Return the first ChildAssoEntitesArchive filtered by the adresse column
 * @method     ChildAssoEntitesArchive findOneByCodepostal(string $codepostal) Return the first ChildAssoEntitesArchive filtered by the codepostal column
 * @method     ChildAssoEntitesArchive findOneByVille(string $ville) Return the first ChildAssoEntitesArchive filtered by the ville column
 * @method     ChildAssoEntitesArchive findOneByPays(string $pays) Return the first ChildAssoEntitesArchive filtered by the pays column
 * @method     ChildAssoEntitesArchive findOneByTelephone(string $telephone) Return the first ChildAssoEntitesArchive filtered by the telephone column
 * @method     ChildAssoEntitesArchive findOneByFax(string $fax) Return the first ChildAssoEntitesArchive filtered by the fax column
 * @method     ChildAssoEntitesArchive findOneByUrl(string $url) Return the first ChildAssoEntitesArchive filtered by the url column
 * @method     ChildAssoEntitesArchive findOneByAssujettitva(int $assujettitva) Return the first ChildAssoEntitesArchive filtered by the assujettitva column
 * @method     ChildAssoEntitesArchive findOneByEmail(string $email) Return the first ChildAssoEntitesArchive filtered by the email column
 * @method     ChildAssoEntitesArchive findOneByCreatedAt(string $created_at) Return the first ChildAssoEntitesArchive filtered by the created_at column
 * @method     ChildAssoEntitesArchive findOneByUpdatedAt(string $updated_at) Return the first ChildAssoEntitesArchive filtered by the updated_at column
 * @method     ChildAssoEntitesArchive findOneByArchivedAt(string $archived_at) Return the first ChildAssoEntitesArchive filtered by the archived_at column *

 * @method     ChildAssoEntitesArchive requirePk($key, ConnectionInterface $con = null) Return the ChildAssoEntitesArchive by primary key and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildAssoEntitesArchive requireOne(ConnectionInterface $con = null) Return the first ChildAssoEntitesArchive matching the query and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 *
 * @method     ChildAssoEntitesArchive requireOneByIdEntite(string $id_entite) Return the first ChildAssoEntitesArchive filtered by the id_entite column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildAssoEntitesArchive requireOneByNom(string $nom) Return the first ChildAssoEntitesArchive filtered by the nom column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildAssoEntitesArchive requireOneByNomcourt(string $nomcourt) Return the first ChildAssoEntitesArchive filtered by the nomcourt column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildAssoEntitesArchive requireOneByAdresse(string $adresse) Return the first ChildAssoEntitesArchive filtered by the adresse column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildAssoEntitesArchive requireOneByCodepostal(string $codepostal) Return the first ChildAssoEntitesArchive filtered by the codepostal column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildAssoEntitesArchive requireOneByVille(string $ville) Return the first ChildAssoEntitesArchive filtered by the ville column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildAssoEntitesArchive requireOneByPays(string $pays) Return the first ChildAssoEntitesArchive filtered by the pays column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildAssoEntitesArchive requireOneByTelephone(string $telephone) Return the first ChildAssoEntitesArchive filtered by the telephone column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildAssoEntitesArchive requireOneByFax(string $fax) Return the first ChildAssoEntitesArchive filtered by the fax column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildAssoEntitesArchive requireOneByUrl(string $url) Return the first ChildAssoEntitesArchive filtered by the url column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildAssoEntitesArchive requireOneByAssujettitva(int $assujettitva) Return the first ChildAssoEntitesArchive filtered by the assujettitva column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildAssoEntitesArchive requireOneByEmail(string $email) Return the first ChildAssoEntitesArchive filtered by the email column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildAssoEntitesArchive requireOneByCreatedAt(string $created_at) Return the first ChildAssoEntitesArchive filtered by the created_at column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildAssoEntitesArchive requireOneByUpdatedAt(string $updated_at) Return the first ChildAssoEntitesArchive filtered by the updated_at column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildAssoEntitesArchive requireOneByArchivedAt(string $archived_at) Return the first ChildAssoEntitesArchive filtered by the archived_at column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 *
 * @method     ChildAssoEntitesArchive[]|ObjectCollection find(ConnectionInterface $con = null) Return ChildAssoEntitesArchive objects based on current ModelCriteria
 * @method     ChildAssoEntitesArchive[]|ObjectCollection findByIdEntite(string $id_entite) Return ChildAssoEntitesArchive objects filtered by the id_entite column
 * @method     ChildAssoEntitesArchive[]|ObjectCollection findByNom(string $nom) Return ChildAssoEntitesArchive objects filtered by the nom column
 * @method     ChildAssoEntitesArchive[]|ObjectCollection findByNomcourt(string $nomcourt) Return ChildAssoEntitesArchive objects filtered by the nomcourt column
 * @method     ChildAssoEntitesArchive[]|ObjectCollection findByAdresse(string $adresse) Return ChildAssoEntitesArchive objects filtered by the adresse column
 * @method     ChildAssoEntitesArchive[]|ObjectCollection findByCodepostal(string $codepostal) Return ChildAssoEntitesArchive objects filtered by the codepostal column
 * @method     ChildAssoEntitesArchive[]|ObjectCollection findByVille(string $ville) Return ChildAssoEntitesArchive objects filtered by the ville column
 * @method     ChildAssoEntitesArchive[]|ObjectCollection findByPays(string $pays) Return ChildAssoEntitesArchive objects filtered by the pays column
 * @method     ChildAssoEntitesArchive[]|ObjectCollection findByTelephone(string $telephone) Return ChildAssoEntitesArchive objects filtered by the telephone column
 * @method     ChildAssoEntitesArchive[]|ObjectCollection findByFax(string $fax) Return ChildAssoEntitesArchive objects filtered by the fax column
 * @method     ChildAssoEntitesArchive[]|ObjectCollection findByUrl(string $url) Return ChildAssoEntitesArchive objects filtered by the url column
 * @method     ChildAssoEntitesArchive[]|ObjectCollection findByAssujettitva(int $assujettitva) Return ChildAssoEntitesArchive objects filtered by the assujettitva column
 * @method     ChildAssoEntitesArchive[]|ObjectCollection findByEmail(string $email) Return ChildAssoEntitesArchive objects filtered by the email column
 * @method     ChildAssoEntitesArchive[]|ObjectCollection findByCreatedAt(string $created_at) Return ChildAssoEntitesArchive objects filtered by the created_at column
 * @method     ChildAssoEntitesArchive[]|ObjectCollection findByUpdatedAt(string $updated_at) Return ChildAssoEntitesArchive objects filtered by the updated_at column
 * @method     ChildAssoEntitesArchive[]|ObjectCollection findByArchivedAt(string $archived_at) Return ChildAssoEntitesArchive objects filtered by the archived_at column
 * @method     ChildAssoEntitesArchive[]|\Propel\Runtime\Util\PropelModelPager paginate($page = 1, $maxPerPage = 10, ConnectionInterface $con = null) Issue a SELECT query based on the current ModelCriteria and uses a page and a maximum number of results per page to compute an offset and a limit
 *
 */
abstract class AssoEntitesArchiveQuery extends ModelCriteria
{
    protected $entityNotFoundExceptionClass = '\\Propel\\Runtime\\Exception\\EntityNotFoundException';

    /**
     * Initializes internal state of \Base\AssoEntitesArchiveQuery object.
     *
     * @param     string $dbName The database name
     * @param     string $modelName The phpName of a model, e.g. 'Book'
     * @param     string $modelAlias The alias for the model in this query, e.g. 'b'
     */
    public function __construct($dbName = 'simplasso', $modelName = '\\AssoEntitesArchive', $modelAlias = null)
    {
        parent::__construct($dbName, $modelName, $modelAlias);
    }

    /**
     * Returns a new ChildAssoEntitesArchiveQuery object.
     *
     * @param     string $modelAlias The alias of a model in the query
     * @param     Criteria $criteria Optional Criteria to build the query from
     *
     * @return ChildAssoEntitesArchiveQuery
     */
    public static function create($modelAlias = null, Criteria $criteria = null)
    {
        if ($criteria instanceof ChildAssoEntitesArchiveQuery) {
            return $criteria;
        }
        $query = new ChildAssoEntitesArchiveQuery();
        if (null !== $modelAlias) {
            $query->setModelAlias($modelAlias);
        }
        if ($criteria instanceof Criteria) {
            $query->mergeWith($criteria);
        }

        return $query;
    }

    /**
     * Find object by primary key.
     * Propel uses the instance pool to skip the database if the object exists.
     * Go fast if the query is untouched.
     *
     * <code>
     * $obj  = $c->findPk(12, $con);
     * </code>
     *
     * @param mixed $key Primary key to use for the query
     * @param ConnectionInterface $con an optional connection object
     *
     * @return ChildAssoEntitesArchive|array|mixed the result, formatted by the current formatter
     */
    public function findPk($key, ConnectionInterface $con = null)
    {
        if ($key === null) {
            return null;
        }

        if ($con === null) {
            $con = Propel::getServiceContainer()->getReadConnection(AssoEntitesArchiveTableMap::DATABASE_NAME);
        }

        $this->basePreSelect($con);

        if (
            $this->formatter || $this->modelAlias || $this->with || $this->select
            || $this->selectColumns || $this->asColumns || $this->selectModifiers
            || $this->map || $this->having || $this->joins
        ) {
            return $this->findPkComplex($key, $con);
        }

        if ((null !== ($obj = AssoEntitesArchiveTableMap::getInstanceFromPool(null === $key || is_scalar($key) || is_callable([$key, '__toString']) ? (string) $key : $key)))) {
            // the object is already in the instance pool
            return $obj;
        }

        return $this->findPkSimple($key, $con);
    }

    /**
     * Find object by primary key using raw SQL to go fast.
     * Bypass doSelect() and the object formatter by using generated code.
     *
     * @param     mixed $key Primary key to use for the query
     * @param     ConnectionInterface $con A connection object
     *
     * @throws \Propel\Runtime\Exception\PropelException
     *
     * @return ChildAssoEntitesArchive A model object, or null if the key is not found
     */
    protected function findPkSimple($key, ConnectionInterface $con)
    {
        $sql = 'SELECT `id_entite`, `nom`, `nomcourt`, `adresse`, `codepostal`, `ville`, `pays`, `telephone`, `fax`, `url`, `assujettitva`, `email`, `created_at`, `updated_at`, `archived_at` FROM `asso_entites_archive` WHERE `id_entite` = :p0';
        try {
            $stmt = $con->prepare($sql);
            $stmt->bindValue(':p0', $key, PDO::PARAM_INT);
            $stmt->execute();
        } catch (Exception $e) {
            Propel::log($e->getMessage(), Propel::LOG_ERR);
            throw new PropelException(sprintf('Unable to execute SELECT statement [%s]', $sql), 0, $e);
        }
        $obj = null;
        if ($row = $stmt->fetch(\PDO::FETCH_NUM)) {
            /** @var ChildAssoEntitesArchive $obj */
            $obj = new ChildAssoEntitesArchive();
            $obj->hydrate($row);
            AssoEntitesArchiveTableMap::addInstanceToPool($obj, null === $key || is_scalar($key) || is_callable([$key, '__toString']) ? (string) $key : $key);
        }
        $stmt->closeCursor();

        return $obj;
    }

    /**
     * Find object by primary key.
     *
     * @param     mixed $key Primary key to use for the query
     * @param     ConnectionInterface $con A connection object
     *
     * @return ChildAssoEntitesArchive|array|mixed the result, formatted by the current formatter
     */
    protected function findPkComplex($key, ConnectionInterface $con)
    {
        // As the query uses a PK condition, no limit(1) is necessary.
        $criteria = $this->isKeepQuery() ? clone $this : $this;
        $dataFetcher = $criteria
            ->filterByPrimaryKey($key)
            ->doSelect($con);

        return $criteria->getFormatter()->init($criteria)->formatOne($dataFetcher);
    }

    /**
     * Find objects by primary key
     * <code>
     * $objs = $c->findPks(array(12, 56, 832), $con);
     * </code>
     * @param     array $keys Primary keys to use for the query
     * @param     ConnectionInterface $con an optional connection object
     *
     * @return ObjectCollection|array|mixed the list of results, formatted by the current formatter
     */
    public function findPks($keys, ConnectionInterface $con = null)
    {
        if (null === $con) {
            $con = Propel::getServiceContainer()->getReadConnection($this->getDbName());
        }
        $this->basePreSelect($con);
        $criteria = $this->isKeepQuery() ? clone $this : $this;
        $dataFetcher = $criteria
            ->filterByPrimaryKeys($keys)
            ->doSelect($con);

        return $criteria->getFormatter()->init($criteria)->format($dataFetcher);
    }

    /**
     * Filter the query by primary key
     *
     * @param     mixed $key Primary key to use for the query
     *
     * @return $this|ChildAssoEntitesArchiveQuery The current query, for fluid interface
     */
    public function filterByPrimaryKey($key)
    {

        return $this->addUsingAlias(AssoEntitesArchiveTableMap::COL_ID_ENTITE, $key, Criteria::EQUAL);
    }

    /**
     * Filter the query by a list of primary keys
     *
     * @param     array $keys The list of primary key to use for the query
     *
     * @return $this|ChildAssoEntitesArchiveQuery The current query, for fluid interface
     */
    public function filterByPrimaryKeys($keys)
    {

        return $this->addUsingAlias(AssoEntitesArchiveTableMap::COL_ID_ENTITE, $keys, Criteria::IN);
    }

    /**
     * Filter the query on the id_entite column
     *
     * Example usage:
     * <code>
     * $query->filterByIdEntite(1234); // WHERE id_entite = 1234
     * $query->filterByIdEntite(array(12, 34)); // WHERE id_entite IN (12, 34)
     * $query->filterByIdEntite(array('min' => 12)); // WHERE id_entite > 12
     * </code>
     *
     * @param     mixed $idEntite The value to use as filter.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildAssoEntitesArchiveQuery The current query, for fluid interface
     */
    public function filterByIdEntite($idEntite = null, $comparison = null)
    {
        if (is_array($idEntite)) {
            $useMinMax = false;
            if (isset($idEntite['min'])) {
                $this->addUsingAlias(AssoEntitesArchiveTableMap::COL_ID_ENTITE, $idEntite['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($idEntite['max'])) {
                $this->addUsingAlias(AssoEntitesArchiveTableMap::COL_ID_ENTITE, $idEntite['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(AssoEntitesArchiveTableMap::COL_ID_ENTITE, $idEntite, $comparison);
    }

    /**
     * Filter the query on the nom column
     *
     * Example usage:
     * <code>
     * $query->filterByNom('fooValue');   // WHERE nom = 'fooValue'
     * $query->filterByNom('%fooValue%', Criteria::LIKE); // WHERE nom LIKE '%fooValue%'
     * </code>
     *
     * @param     string $nom The value to use as filter.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildAssoEntitesArchiveQuery The current query, for fluid interface
     */
    public function filterByNom($nom = null, $comparison = null)
    {
        if (null === $comparison) {
            if (is_array($nom)) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(AssoEntitesArchiveTableMap::COL_NOM, $nom, $comparison);
    }

    /**
     * Filter the query on the nomcourt column
     *
     * Example usage:
     * <code>
     * $query->filterByNomcourt('fooValue');   // WHERE nomcourt = 'fooValue'
     * $query->filterByNomcourt('%fooValue%', Criteria::LIKE); // WHERE nomcourt LIKE '%fooValue%'
     * </code>
     *
     * @param     string $nomcourt The value to use as filter.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildAssoEntitesArchiveQuery The current query, for fluid interface
     */
    public function filterByNomcourt($nomcourt = null, $comparison = null)
    {
        if (null === $comparison) {
            if (is_array($nomcourt)) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(AssoEntitesArchiveTableMap::COL_NOMCOURT, $nomcourt, $comparison);
    }

    /**
     * Filter the query on the adresse column
     *
     * Example usage:
     * <code>
     * $query->filterByAdresse('fooValue');   // WHERE adresse = 'fooValue'
     * $query->filterByAdresse('%fooValue%', Criteria::LIKE); // WHERE adresse LIKE '%fooValue%'
     * </code>
     *
     * @param     string $adresse The value to use as filter.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildAssoEntitesArchiveQuery The current query, for fluid interface
     */
    public function filterByAdresse($adresse = null, $comparison = null)
    {
        if (null === $comparison) {
            if (is_array($adresse)) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(AssoEntitesArchiveTableMap::COL_ADRESSE, $adresse, $comparison);
    }

    /**
     * Filter the query on the codepostal column
     *
     * Example usage:
     * <code>
     * $query->filterByCodepostal('fooValue');   // WHERE codepostal = 'fooValue'
     * $query->filterByCodepostal('%fooValue%', Criteria::LIKE); // WHERE codepostal LIKE '%fooValue%'
     * </code>
     *
     * @param     string $codepostal The value to use as filter.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildAssoEntitesArchiveQuery The current query, for fluid interface
     */
    public function filterByCodepostal($codepostal = null, $comparison = null)
    {
        if (null === $comparison) {
            if (is_array($codepostal)) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(AssoEntitesArchiveTableMap::COL_CODEPOSTAL, $codepostal, $comparison);
    }

    /**
     * Filter the query on the ville column
     *
     * Example usage:
     * <code>
     * $query->filterByVille('fooValue');   // WHERE ville = 'fooValue'
     * $query->filterByVille('%fooValue%', Criteria::LIKE); // WHERE ville LIKE '%fooValue%'
     * </code>
     *
     * @param     string $ville The value to use as filter.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildAssoEntitesArchiveQuery The current query, for fluid interface
     */
    public function filterByVille($ville = null, $comparison = null)
    {
        if (null === $comparison) {
            if (is_array($ville)) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(AssoEntitesArchiveTableMap::COL_VILLE, $ville, $comparison);
    }

    /**
     * Filter the query on the pays column
     *
     * Example usage:
     * <code>
     * $query->filterByPays('fooValue');   // WHERE pays = 'fooValue'
     * $query->filterByPays('%fooValue%', Criteria::LIKE); // WHERE pays LIKE '%fooValue%'
     * </code>
     *
     * @param     string $pays The value to use as filter.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildAssoEntitesArchiveQuery The current query, for fluid interface
     */
    public function filterByPays($pays = null, $comparison = null)
    {
        if (null === $comparison) {
            if (is_array($pays)) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(AssoEntitesArchiveTableMap::COL_PAYS, $pays, $comparison);
    }

    /**
     * Filter the query on the telephone column
     *
     * Example usage:
     * <code>
     * $query->filterByTelephone('fooValue');   // WHERE telephone = 'fooValue'
     * $query->filterByTelephone('%fooValue%', Criteria::LIKE); // WHERE telephone LIKE '%fooValue%'
     * </code>
     *
     * @param     string $telephone The value to use as filter.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildAssoEntitesArchiveQuery The current query, for fluid interface
     */
    public function filterByTelephone($telephone = null, $comparison = null)
    {
        if (null === $comparison) {
            if (is_array($telephone)) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(AssoEntitesArchiveTableMap::COL_TELEPHONE, $telephone, $comparison);
    }

    /**
     * Filter the query on the fax column
     *
     * Example usage:
     * <code>
     * $query->filterByFax('fooValue');   // WHERE fax = 'fooValue'
     * $query->filterByFax('%fooValue%', Criteria::LIKE); // WHERE fax LIKE '%fooValue%'
     * </code>
     *
     * @param     string $fax The value to use as filter.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildAssoEntitesArchiveQuery The current query, for fluid interface
     */
    public function filterByFax($fax = null, $comparison = null)
    {
        if (null === $comparison) {
            if (is_array($fax)) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(AssoEntitesArchiveTableMap::COL_FAX, $fax, $comparison);
    }

    /**
     * Filter the query on the url column
     *
     * Example usage:
     * <code>
     * $query->filterByUrl('fooValue');   // WHERE url = 'fooValue'
     * $query->filterByUrl('%fooValue%', Criteria::LIKE); // WHERE url LIKE '%fooValue%'
     * </code>
     *
     * @param     string $url The value to use as filter.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildAssoEntitesArchiveQuery The current query, for fluid interface
     */
    public function filterByUrl($url = null, $comparison = null)
    {
        if (null === $comparison) {
            if (is_array($url)) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(AssoEntitesArchiveTableMap::COL_URL, $url, $comparison);
    }

    /**
     * Filter the query on the assujettitva column
     *
     * Example usage:
     * <code>
     * $query->filterByAssujettitva(1234); // WHERE assujettitva = 1234
     * $query->filterByAssujettitva(array(12, 34)); // WHERE assujettitva IN (12, 34)
     * $query->filterByAssujettitva(array('min' => 12)); // WHERE assujettitva > 12
     * </code>
     *
     * @param     mixed $assujettitva The value to use as filter.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildAssoEntitesArchiveQuery The current query, for fluid interface
     */
    public function filterByAssujettitva($assujettitva = null, $comparison = null)
    {
        if (is_array($assujettitva)) {
            $useMinMax = false;
            if (isset($assujettitva['min'])) {
                $this->addUsingAlias(AssoEntitesArchiveTableMap::COL_ASSUJETTITVA, $assujettitva['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($assujettitva['max'])) {
                $this->addUsingAlias(AssoEntitesArchiveTableMap::COL_ASSUJETTITVA, $assujettitva['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(AssoEntitesArchiveTableMap::COL_ASSUJETTITVA, $assujettitva, $comparison);
    }

    /**
     * Filter the query on the email column
     *
     * Example usage:
     * <code>
     * $query->filterByEmail('fooValue');   // WHERE email = 'fooValue'
     * $query->filterByEmail('%fooValue%', Criteria::LIKE); // WHERE email LIKE '%fooValue%'
     * </code>
     *
     * @param     string $email The value to use as filter.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildAssoEntitesArchiveQuery The current query, for fluid interface
     */
    public function filterByEmail($email = null, $comparison = null)
    {
        if (null === $comparison) {
            if (is_array($email)) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(AssoEntitesArchiveTableMap::COL_EMAIL, $email, $comparison);
    }

    /**
     * Filter the query on the created_at column
     *
     * Example usage:
     * <code>
     * $query->filterByCreatedAt('2011-03-14'); // WHERE created_at = '2011-03-14'
     * $query->filterByCreatedAt('now'); // WHERE created_at = '2011-03-14'
     * $query->filterByCreatedAt(array('max' => 'yesterday')); // WHERE created_at > '2011-03-13'
     * </code>
     *
     * @param     mixed $createdAt The value to use as filter.
     *              Values can be integers (unix timestamps), DateTime objects, or strings.
     *              Empty strings are treated as NULL.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildAssoEntitesArchiveQuery The current query, for fluid interface
     */
    public function filterByCreatedAt($createdAt = null, $comparison = null)
    {
        if (is_array($createdAt)) {
            $useMinMax = false;
            if (isset($createdAt['min'])) {
                $this->addUsingAlias(AssoEntitesArchiveTableMap::COL_CREATED_AT, $createdAt['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($createdAt['max'])) {
                $this->addUsingAlias(AssoEntitesArchiveTableMap::COL_CREATED_AT, $createdAt['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(AssoEntitesArchiveTableMap::COL_CREATED_AT, $createdAt, $comparison);
    }

    /**
     * Filter the query on the updated_at column
     *
     * Example usage:
     * <code>
     * $query->filterByUpdatedAt('2011-03-14'); // WHERE updated_at = '2011-03-14'
     * $query->filterByUpdatedAt('now'); // WHERE updated_at = '2011-03-14'
     * $query->filterByUpdatedAt(array('max' => 'yesterday')); // WHERE updated_at > '2011-03-13'
     * </code>
     *
     * @param     mixed $updatedAt The value to use as filter.
     *              Values can be integers (unix timestamps), DateTime objects, or strings.
     *              Empty strings are treated as NULL.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildAssoEntitesArchiveQuery The current query, for fluid interface
     */
    public function filterByUpdatedAt($updatedAt = null, $comparison = null)
    {
        if (is_array($updatedAt)) {
            $useMinMax = false;
            if (isset($updatedAt['min'])) {
                $this->addUsingAlias(AssoEntitesArchiveTableMap::COL_UPDATED_AT, $updatedAt['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($updatedAt['max'])) {
                $this->addUsingAlias(AssoEntitesArchiveTableMap::COL_UPDATED_AT, $updatedAt['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(AssoEntitesArchiveTableMap::COL_UPDATED_AT, $updatedAt, $comparison);
    }

    /**
     * Filter the query on the archived_at column
     *
     * Example usage:
     * <code>
     * $query->filterByArchivedAt('2011-03-14'); // WHERE archived_at = '2011-03-14'
     * $query->filterByArchivedAt('now'); // WHERE archived_at = '2011-03-14'
     * $query->filterByArchivedAt(array('max' => 'yesterday')); // WHERE archived_at > '2011-03-13'
     * </code>
     *
     * @param     mixed $archivedAt The value to use as filter.
     *              Values can be integers (unix timestamps), DateTime objects, or strings.
     *              Empty strings are treated as NULL.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildAssoEntitesArchiveQuery The current query, for fluid interface
     */
    public function filterByArchivedAt($archivedAt = null, $comparison = null)
    {
        if (is_array($archivedAt)) {
            $useMinMax = false;
            if (isset($archivedAt['min'])) {
                $this->addUsingAlias(AssoEntitesArchiveTableMap::COL_ARCHIVED_AT, $archivedAt['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($archivedAt['max'])) {
                $this->addUsingAlias(AssoEntitesArchiveTableMap::COL_ARCHIVED_AT, $archivedAt['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(AssoEntitesArchiveTableMap::COL_ARCHIVED_AT, $archivedAt, $comparison);
    }

    /**
     * Exclude object from result
     *
     * @param   ChildAssoEntitesArchive $assoEntitesArchive Object to remove from the list of results
     *
     * @return $this|ChildAssoEntitesArchiveQuery The current query, for fluid interface
     */
    public function prune($assoEntitesArchive = null)
    {
        if ($assoEntitesArchive) {
            $this->addUsingAlias(AssoEntitesArchiveTableMap::COL_ID_ENTITE, $assoEntitesArchive->getIdEntite(), Criteria::NOT_EQUAL);
        }

        return $this;
    }

    /**
     * Deletes all rows from the asso_entites_archive table.
     *
     * @param ConnectionInterface $con the connection to use
     * @return int The number of affected rows (if supported by underlying database driver).
     */
    public function doDeleteAll(ConnectionInterface $con = null)
    {
        if (null === $con) {
            $con = Propel::getServiceContainer()->getWriteConnection(AssoEntitesArchiveTableMap::DATABASE_NAME);
        }

        // use transaction because $criteria could contain info
        // for more than one table or we could emulating ON DELETE CASCADE, etc.
        return $con->transaction(function () use ($con) {
            $affectedRows = 0; // initialize var to track total num of affected rows
            $affectedRows += parent::doDeleteAll($con);
            // Because this db requires some delete cascade/set null emulation, we have to
            // clear the cached instance *after* the emulation has happened (since
            // instances get re-added by the select statement contained therein).
            AssoEntitesArchiveTableMap::clearInstancePool();
            AssoEntitesArchiveTableMap::clearRelatedInstancePool();

            return $affectedRows;
        });
    }

    /**
     * Performs a DELETE on the database based on the current ModelCriteria
     *
     * @param ConnectionInterface $con the connection to use
     * @return int             The number of affected rows (if supported by underlying database driver).  This includes CASCADE-related rows
     *                         if supported by native driver or if emulated using Propel.
     * @throws PropelException Any exceptions caught during processing will be
     *                         rethrown wrapped into a PropelException.
     */
    public function delete(ConnectionInterface $con = null)
    {
        if (null === $con) {
            $con = Propel::getServiceContainer()->getWriteConnection(AssoEntitesArchiveTableMap::DATABASE_NAME);
        }

        $criteria = $this;

        // Set the correct dbName
        $criteria->setDbName(AssoEntitesArchiveTableMap::DATABASE_NAME);

        // use transaction because $criteria could contain info
        // for more than one table or we could emulating ON DELETE CASCADE, etc.
        return $con->transaction(function () use ($con, $criteria) {
            $affectedRows = 0; // initialize var to track total num of affected rows

            AssoEntitesArchiveTableMap::removeInstanceFromPool($criteria);

            $affectedRows += ModelCriteria::delete($con);
            AssoEntitesArchiveTableMap::clearRelatedInstancePool();

            return $affectedRows;
        });
    }

} // AssoEntitesArchiveQuery
