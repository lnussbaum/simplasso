<?php

namespace Base;

use \Arrondissement as ChildArrondissement;
use \ArrondissementQuery as ChildArrondissementQuery;
use \Exception;
use \PDO;
use Map\ArrondissementTableMap;
use Propel\Runtime\Propel;
use Propel\Runtime\ActiveQuery\Criteria;
use Propel\Runtime\ActiveQuery\ModelCriteria;
use Propel\Runtime\Collection\ObjectCollection;
use Propel\Runtime\Connection\ConnectionInterface;
use Propel\Runtime\Exception\PropelException;

/**
 * Base class that represents a query for the 'geo_arrondissements' table.
 *
 *
 *
 * @method     ChildArrondissementQuery orderByIdArrondissement($order = Criteria::ASC) Order by the id_arrondissement column
 * @method     ChildArrondissementQuery orderByPays($order = Criteria::ASC) Order by the pays column
 * @method     ChildArrondissementQuery orderByDepartement($order = Criteria::ASC) Order by the departement column
 * @method     ChildArrondissementQuery orderByRegion($order = Criteria::ASC) Order by the region column
 * @method     ChildArrondissementQuery orderByCode($order = Criteria::ASC) Order by the code column
 * @method     ChildArrondissementQuery orderByChefLieu($order = Criteria::ASC) Order by the chef_lieu column
 * @method     ChildArrondissementQuery orderByTypeCharniere($order = Criteria::ASC) Order by the type_charniere column
 * @method     ChildArrondissementQuery orderByNom($order = Criteria::ASC) Order by the nom column
 * @method     ChildArrondissementQuery orderByZonage($order = Criteria::ASC) Order by the zonage column
 * @method     ChildArrondissementQuery orderByCreatedAt($order = Criteria::ASC) Order by the created_at column
 * @method     ChildArrondissementQuery orderByUpdatedAt($order = Criteria::ASC) Order by the updated_at column
 *
 * @method     ChildArrondissementQuery groupByIdArrondissement() Group by the id_arrondissement column
 * @method     ChildArrondissementQuery groupByPays() Group by the pays column
 * @method     ChildArrondissementQuery groupByDepartement() Group by the departement column
 * @method     ChildArrondissementQuery groupByRegion() Group by the region column
 * @method     ChildArrondissementQuery groupByCode() Group by the code column
 * @method     ChildArrondissementQuery groupByChefLieu() Group by the chef_lieu column
 * @method     ChildArrondissementQuery groupByTypeCharniere() Group by the type_charniere column
 * @method     ChildArrondissementQuery groupByNom() Group by the nom column
 * @method     ChildArrondissementQuery groupByZonage() Group by the zonage column
 * @method     ChildArrondissementQuery groupByCreatedAt() Group by the created_at column
 * @method     ChildArrondissementQuery groupByUpdatedAt() Group by the updated_at column
 *
 * @method     ChildArrondissementQuery leftJoin($relation) Adds a LEFT JOIN clause to the query
 * @method     ChildArrondissementQuery rightJoin($relation) Adds a RIGHT JOIN clause to the query
 * @method     ChildArrondissementQuery innerJoin($relation) Adds a INNER JOIN clause to the query
 *
 * @method     ChildArrondissementQuery leftJoinWith($relation) Adds a LEFT JOIN clause and with to the query
 * @method     ChildArrondissementQuery rightJoinWith($relation) Adds a RIGHT JOIN clause and with to the query
 * @method     ChildArrondissementQuery innerJoinWith($relation) Adds a INNER JOIN clause and with to the query
 *
 * @method     ChildArrondissement findOne(ConnectionInterface $con = null) Return the first ChildArrondissement matching the query
 * @method     ChildArrondissement findOneOrCreate(ConnectionInterface $con = null) Return the first ChildArrondissement matching the query, or a new ChildArrondissement object populated from the query conditions when no match is found
 *
 * @method     ChildArrondissement findOneByIdArrondissement(int $id_arrondissement) Return the first ChildArrondissement filtered by the id_arrondissement column
 * @method     ChildArrondissement findOneByPays(string $pays) Return the first ChildArrondissement filtered by the pays column
 * @method     ChildArrondissement findOneByDepartement(string $departement) Return the first ChildArrondissement filtered by the departement column
 * @method     ChildArrondissement findOneByRegion(int $region) Return the first ChildArrondissement filtered by the region column
 * @method     ChildArrondissement findOneByCode(string $code) Return the first ChildArrondissement filtered by the code column
 * @method     ChildArrondissement findOneByChefLieu(string $chef_lieu) Return the first ChildArrondissement filtered by the chef_lieu column
 * @method     ChildArrondissement findOneByTypeCharniere(boolean $type_charniere) Return the first ChildArrondissement filtered by the type_charniere column
 * @method     ChildArrondissement findOneByNom(string $nom) Return the first ChildArrondissement filtered by the nom column
 * @method     ChildArrondissement findOneByZonage(string $zonage) Return the first ChildArrondissement filtered by the zonage column
 * @method     ChildArrondissement findOneByCreatedAt(string $created_at) Return the first ChildArrondissement filtered by the created_at column
 * @method     ChildArrondissement findOneByUpdatedAt(string $updated_at) Return the first ChildArrondissement filtered by the updated_at column *

 * @method     ChildArrondissement requirePk($key, ConnectionInterface $con = null) Return the ChildArrondissement by primary key and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildArrondissement requireOne(ConnectionInterface $con = null) Return the first ChildArrondissement matching the query and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 *
 * @method     ChildArrondissement requireOneByIdArrondissement(int $id_arrondissement) Return the first ChildArrondissement filtered by the id_arrondissement column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildArrondissement requireOneByPays(string $pays) Return the first ChildArrondissement filtered by the pays column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildArrondissement requireOneByDepartement(string $departement) Return the first ChildArrondissement filtered by the departement column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildArrondissement requireOneByRegion(int $region) Return the first ChildArrondissement filtered by the region column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildArrondissement requireOneByCode(string $code) Return the first ChildArrondissement filtered by the code column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildArrondissement requireOneByChefLieu(string $chef_lieu) Return the first ChildArrondissement filtered by the chef_lieu column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildArrondissement requireOneByTypeCharniere(boolean $type_charniere) Return the first ChildArrondissement filtered by the type_charniere column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildArrondissement requireOneByNom(string $nom) Return the first ChildArrondissement filtered by the nom column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildArrondissement requireOneByZonage(string $zonage) Return the first ChildArrondissement filtered by the zonage column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildArrondissement requireOneByCreatedAt(string $created_at) Return the first ChildArrondissement filtered by the created_at column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildArrondissement requireOneByUpdatedAt(string $updated_at) Return the first ChildArrondissement filtered by the updated_at column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 *
 * @method     ChildArrondissement[]|ObjectCollection find(ConnectionInterface $con = null) Return ChildArrondissement objects based on current ModelCriteria
 * @method     ChildArrondissement[]|ObjectCollection findByIdArrondissement(int $id_arrondissement) Return ChildArrondissement objects filtered by the id_arrondissement column
 * @method     ChildArrondissement[]|ObjectCollection findByPays(string $pays) Return ChildArrondissement objects filtered by the pays column
 * @method     ChildArrondissement[]|ObjectCollection findByDepartement(string $departement) Return ChildArrondissement objects filtered by the departement column
 * @method     ChildArrondissement[]|ObjectCollection findByRegion(int $region) Return ChildArrondissement objects filtered by the region column
 * @method     ChildArrondissement[]|ObjectCollection findByCode(string $code) Return ChildArrondissement objects filtered by the code column
 * @method     ChildArrondissement[]|ObjectCollection findByChefLieu(string $chef_lieu) Return ChildArrondissement objects filtered by the chef_lieu column
 * @method     ChildArrondissement[]|ObjectCollection findByTypeCharniere(boolean $type_charniere) Return ChildArrondissement objects filtered by the type_charniere column
 * @method     ChildArrondissement[]|ObjectCollection findByNom(string $nom) Return ChildArrondissement objects filtered by the nom column
 * @method     ChildArrondissement[]|ObjectCollection findByZonage(string $zonage) Return ChildArrondissement objects filtered by the zonage column
 * @method     ChildArrondissement[]|ObjectCollection findByCreatedAt(string $created_at) Return ChildArrondissement objects filtered by the created_at column
 * @method     ChildArrondissement[]|ObjectCollection findByUpdatedAt(string $updated_at) Return ChildArrondissement objects filtered by the updated_at column
 * @method     ChildArrondissement[]|\Propel\Runtime\Util\PropelModelPager paginate($page = 1, $maxPerPage = 10, ConnectionInterface $con = null) Issue a SELECT query based on the current ModelCriteria and uses a page and a maximum number of results per page to compute an offset and a limit
 *
 */
abstract class ArrondissementQuery extends ModelCriteria
{
    protected $entityNotFoundExceptionClass = '\\Propel\\Runtime\\Exception\\EntityNotFoundException';

    /**
     * Initializes internal state of \Base\ArrondissementQuery object.
     *
     * @param     string $dbName The database name
     * @param     string $modelName The phpName of a model, e.g. 'Book'
     * @param     string $modelAlias The alias for the model in this query, e.g. 'b'
     */
    public function __construct($dbName = 'simplasso', $modelName = '\\Arrondissement', $modelAlias = null)
    {
        parent::__construct($dbName, $modelName, $modelAlias);
    }

    /**
     * Returns a new ChildArrondissementQuery object.
     *
     * @param     string $modelAlias The alias of a model in the query
     * @param     Criteria $criteria Optional Criteria to build the query from
     *
     * @return ChildArrondissementQuery
     */
    public static function create($modelAlias = null, Criteria $criteria = null)
    {
        if ($criteria instanceof ChildArrondissementQuery) {
            return $criteria;
        }
        $query = new ChildArrondissementQuery();
        if (null !== $modelAlias) {
            $query->setModelAlias($modelAlias);
        }
        if ($criteria instanceof Criteria) {
            $query->mergeWith($criteria);
        }

        return $query;
    }

    /**
     * Find object by primary key.
     * Propel uses the instance pool to skip the database if the object exists.
     * Go fast if the query is untouched.
     *
     * <code>
     * $obj  = $c->findPk(12, $con);
     * </code>
     *
     * @param mixed $key Primary key to use for the query
     * @param ConnectionInterface $con an optional connection object
     *
     * @return ChildArrondissement|array|mixed the result, formatted by the current formatter
     */
    public function findPk($key, ConnectionInterface $con = null)
    {
        if ($key === null) {
            return null;
        }

        if ($con === null) {
            $con = Propel::getServiceContainer()->getReadConnection(ArrondissementTableMap::DATABASE_NAME);
        }

        $this->basePreSelect($con);

        if (
            $this->formatter || $this->modelAlias || $this->with || $this->select
            || $this->selectColumns || $this->asColumns || $this->selectModifiers
            || $this->map || $this->having || $this->joins
        ) {
            return $this->findPkComplex($key, $con);
        }

        if ((null !== ($obj = ArrondissementTableMap::getInstanceFromPool(null === $key || is_scalar($key) || is_callable([$key, '__toString']) ? (string) $key : $key)))) {
            // the object is already in the instance pool
            return $obj;
        }

        return $this->findPkSimple($key, $con);
    }

    /**
     * Find object by primary key using raw SQL to go fast.
     * Bypass doSelect() and the object formatter by using generated code.
     *
     * @param     mixed $key Primary key to use for the query
     * @param     ConnectionInterface $con A connection object
     *
     * @throws \Propel\Runtime\Exception\PropelException
     *
     * @return ChildArrondissement A model object, or null if the key is not found
     */
    protected function findPkSimple($key, ConnectionInterface $con)
    {
        $sql = 'SELECT `id_arrondissement`, `pays`, `departement`, `region`, `code`, `chef_lieu`, `type_charniere`, `nom`, `zonage`, `created_at`, `updated_at` FROM `geo_arrondissements` WHERE `id_arrondissement` = :p0';
        try {
            $stmt = $con->prepare($sql);
            $stmt->bindValue(':p0', $key, PDO::PARAM_INT);
            $stmt->execute();
        } catch (Exception $e) {
            Propel::log($e->getMessage(), Propel::LOG_ERR);
            throw new PropelException(sprintf('Unable to execute SELECT statement [%s]', $sql), 0, $e);
        }
        $obj = null;
        if ($row = $stmt->fetch(\PDO::FETCH_NUM)) {
            /** @var ChildArrondissement $obj */
            $obj = new ChildArrondissement();
            $obj->hydrate($row);
            ArrondissementTableMap::addInstanceToPool($obj, null === $key || is_scalar($key) || is_callable([$key, '__toString']) ? (string) $key : $key);
        }
        $stmt->closeCursor();

        return $obj;
    }

    /**
     * Find object by primary key.
     *
     * @param     mixed $key Primary key to use for the query
     * @param     ConnectionInterface $con A connection object
     *
     * @return ChildArrondissement|array|mixed the result, formatted by the current formatter
     */
    protected function findPkComplex($key, ConnectionInterface $con)
    {
        // As the query uses a PK condition, no limit(1) is necessary.
        $criteria = $this->isKeepQuery() ? clone $this : $this;
        $dataFetcher = $criteria
            ->filterByPrimaryKey($key)
            ->doSelect($con);

        return $criteria->getFormatter()->init($criteria)->formatOne($dataFetcher);
    }

    /**
     * Find objects by primary key
     * <code>
     * $objs = $c->findPks(array(12, 56, 832), $con);
     * </code>
     * @param     array $keys Primary keys to use for the query
     * @param     ConnectionInterface $con an optional connection object
     *
     * @return ObjectCollection|array|mixed the list of results, formatted by the current formatter
     */
    public function findPks($keys, ConnectionInterface $con = null)
    {
        if (null === $con) {
            $con = Propel::getServiceContainer()->getReadConnection($this->getDbName());
        }
        $this->basePreSelect($con);
        $criteria = $this->isKeepQuery() ? clone $this : $this;
        $dataFetcher = $criteria
            ->filterByPrimaryKeys($keys)
            ->doSelect($con);

        return $criteria->getFormatter()->init($criteria)->format($dataFetcher);
    }

    /**
     * Filter the query by primary key
     *
     * @param     mixed $key Primary key to use for the query
     *
     * @return $this|ChildArrondissementQuery The current query, for fluid interface
     */
    public function filterByPrimaryKey($key)
    {

        return $this->addUsingAlias(ArrondissementTableMap::COL_ID_ARRONDISSEMENT, $key, Criteria::EQUAL);
    }

    /**
     * Filter the query by a list of primary keys
     *
     * @param     array $keys The list of primary key to use for the query
     *
     * @return $this|ChildArrondissementQuery The current query, for fluid interface
     */
    public function filterByPrimaryKeys($keys)
    {

        return $this->addUsingAlias(ArrondissementTableMap::COL_ID_ARRONDISSEMENT, $keys, Criteria::IN);
    }

    /**
     * Filter the query on the id_arrondissement column
     *
     * Example usage:
     * <code>
     * $query->filterByIdArrondissement(1234); // WHERE id_arrondissement = 1234
     * $query->filterByIdArrondissement(array(12, 34)); // WHERE id_arrondissement IN (12, 34)
     * $query->filterByIdArrondissement(array('min' => 12)); // WHERE id_arrondissement > 12
     * </code>
     *
     * @param     mixed $idArrondissement The value to use as filter.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildArrondissementQuery The current query, for fluid interface
     */
    public function filterByIdArrondissement($idArrondissement = null, $comparison = null)
    {
        if (is_array($idArrondissement)) {
            $useMinMax = false;
            if (isset($idArrondissement['min'])) {
                $this->addUsingAlias(ArrondissementTableMap::COL_ID_ARRONDISSEMENT, $idArrondissement['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($idArrondissement['max'])) {
                $this->addUsingAlias(ArrondissementTableMap::COL_ID_ARRONDISSEMENT, $idArrondissement['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(ArrondissementTableMap::COL_ID_ARRONDISSEMENT, $idArrondissement, $comparison);
    }

    /**
     * Filter the query on the pays column
     *
     * Example usage:
     * <code>
     * $query->filterByPays('fooValue');   // WHERE pays = 'fooValue'
     * $query->filterByPays('%fooValue%', Criteria::LIKE); // WHERE pays LIKE '%fooValue%'
     * </code>
     *
     * @param     string $pays The value to use as filter.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildArrondissementQuery The current query, for fluid interface
     */
    public function filterByPays($pays = null, $comparison = null)
    {
        if (null === $comparison) {
            if (is_array($pays)) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(ArrondissementTableMap::COL_PAYS, $pays, $comparison);
    }

    /**
     * Filter the query on the departement column
     *
     * Example usage:
     * <code>
     * $query->filterByDepartement('fooValue');   // WHERE departement = 'fooValue'
     * $query->filterByDepartement('%fooValue%', Criteria::LIKE); // WHERE departement LIKE '%fooValue%'
     * </code>
     *
     * @param     string $departement The value to use as filter.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildArrondissementQuery The current query, for fluid interface
     */
    public function filterByDepartement($departement = null, $comparison = null)
    {
        if (null === $comparison) {
            if (is_array($departement)) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(ArrondissementTableMap::COL_DEPARTEMENT, $departement, $comparison);
    }

    /**
     * Filter the query on the region column
     *
     * Example usage:
     * <code>
     * $query->filterByRegion(1234); // WHERE region = 1234
     * $query->filterByRegion(array(12, 34)); // WHERE region IN (12, 34)
     * $query->filterByRegion(array('min' => 12)); // WHERE region > 12
     * </code>
     *
     * @param     mixed $region The value to use as filter.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildArrondissementQuery The current query, for fluid interface
     */
    public function filterByRegion($region = null, $comparison = null)
    {
        if (is_array($region)) {
            $useMinMax = false;
            if (isset($region['min'])) {
                $this->addUsingAlias(ArrondissementTableMap::COL_REGION, $region['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($region['max'])) {
                $this->addUsingAlias(ArrondissementTableMap::COL_REGION, $region['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(ArrondissementTableMap::COL_REGION, $region, $comparison);
    }

    /**
     * Filter the query on the code column
     *
     * Example usage:
     * <code>
     * $query->filterByCode('fooValue');   // WHERE code = 'fooValue'
     * $query->filterByCode('%fooValue%', Criteria::LIKE); // WHERE code LIKE '%fooValue%'
     * </code>
     *
     * @param     string $code The value to use as filter.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildArrondissementQuery The current query, for fluid interface
     */
    public function filterByCode($code = null, $comparison = null)
    {
        if (null === $comparison) {
            if (is_array($code)) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(ArrondissementTableMap::COL_CODE, $code, $comparison);
    }

    /**
     * Filter the query on the chef_lieu column
     *
     * Example usage:
     * <code>
     * $query->filterByChefLieu('fooValue');   // WHERE chef_lieu = 'fooValue'
     * $query->filterByChefLieu('%fooValue%', Criteria::LIKE); // WHERE chef_lieu LIKE '%fooValue%'
     * </code>
     *
     * @param     string $chefLieu The value to use as filter.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildArrondissementQuery The current query, for fluid interface
     */
    public function filterByChefLieu($chefLieu = null, $comparison = null)
    {
        if (null === $comparison) {
            if (is_array($chefLieu)) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(ArrondissementTableMap::COL_CHEF_LIEU, $chefLieu, $comparison);
    }

    /**
     * Filter the query on the type_charniere column
     *
     * Example usage:
     * <code>
     * $query->filterByTypeCharniere(true); // WHERE type_charniere = true
     * $query->filterByTypeCharniere('yes'); // WHERE type_charniere = true
     * </code>
     *
     * @param     boolean|string $typeCharniere The value to use as filter.
     *              Non-boolean arguments are converted using the following rules:
     *                * 1, '1', 'true',  'on',  and 'yes' are converted to boolean true
     *                * 0, '0', 'false', 'off', and 'no'  are converted to boolean false
     *              Check on string values is case insensitive (so 'FaLsE' is seen as 'false').
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildArrondissementQuery The current query, for fluid interface
     */
    public function filterByTypeCharniere($typeCharniere = null, $comparison = null)
    {
        if (is_string($typeCharniere)) {
            $typeCharniere = in_array(strtolower($typeCharniere), array('false', 'off', '-', 'no', 'n', '0', '')) ? false : true;
        }

        return $this->addUsingAlias(ArrondissementTableMap::COL_TYPE_CHARNIERE, $typeCharniere, $comparison);
    }

    /**
     * Filter the query on the nom column
     *
     * Example usage:
     * <code>
     * $query->filterByNom('fooValue');   // WHERE nom = 'fooValue'
     * $query->filterByNom('%fooValue%', Criteria::LIKE); // WHERE nom LIKE '%fooValue%'
     * </code>
     *
     * @param     string $nom The value to use as filter.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildArrondissementQuery The current query, for fluid interface
     */
    public function filterByNom($nom = null, $comparison = null)
    {
        if (null === $comparison) {
            if (is_array($nom)) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(ArrondissementTableMap::COL_NOM, $nom, $comparison);
    }

    /**
     * Filter the query on the zonage column
     *
     * Example usage:
     * <code>
     * $query->filterByZonage('fooValue');   // WHERE zonage = 'fooValue'
     * $query->filterByZonage('%fooValue%', Criteria::LIKE); // WHERE zonage LIKE '%fooValue%'
     * </code>
     *
     * @param     string $zonage The value to use as filter.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildArrondissementQuery The current query, for fluid interface
     */
    public function filterByZonage($zonage = null, $comparison = null)
    {
        if (null === $comparison) {
            if (is_array($zonage)) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(ArrondissementTableMap::COL_ZONAGE, $zonage, $comparison);
    }

    /**
     * Filter the query on the created_at column
     *
     * Example usage:
     * <code>
     * $query->filterByCreatedAt('2011-03-14'); // WHERE created_at = '2011-03-14'
     * $query->filterByCreatedAt('now'); // WHERE created_at = '2011-03-14'
     * $query->filterByCreatedAt(array('max' => 'yesterday')); // WHERE created_at > '2011-03-13'
     * </code>
     *
     * @param     mixed $createdAt The value to use as filter.
     *              Values can be integers (unix timestamps), DateTime objects, or strings.
     *              Empty strings are treated as NULL.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildArrondissementQuery The current query, for fluid interface
     */
    public function filterByCreatedAt($createdAt = null, $comparison = null)
    {
        if (is_array($createdAt)) {
            $useMinMax = false;
            if (isset($createdAt['min'])) {
                $this->addUsingAlias(ArrondissementTableMap::COL_CREATED_AT, $createdAt['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($createdAt['max'])) {
                $this->addUsingAlias(ArrondissementTableMap::COL_CREATED_AT, $createdAt['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(ArrondissementTableMap::COL_CREATED_AT, $createdAt, $comparison);
    }

    /**
     * Filter the query on the updated_at column
     *
     * Example usage:
     * <code>
     * $query->filterByUpdatedAt('2011-03-14'); // WHERE updated_at = '2011-03-14'
     * $query->filterByUpdatedAt('now'); // WHERE updated_at = '2011-03-14'
     * $query->filterByUpdatedAt(array('max' => 'yesterday')); // WHERE updated_at > '2011-03-13'
     * </code>
     *
     * @param     mixed $updatedAt The value to use as filter.
     *              Values can be integers (unix timestamps), DateTime objects, or strings.
     *              Empty strings are treated as NULL.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildArrondissementQuery The current query, for fluid interface
     */
    public function filterByUpdatedAt($updatedAt = null, $comparison = null)
    {
        if (is_array($updatedAt)) {
            $useMinMax = false;
            if (isset($updatedAt['min'])) {
                $this->addUsingAlias(ArrondissementTableMap::COL_UPDATED_AT, $updatedAt['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($updatedAt['max'])) {
                $this->addUsingAlias(ArrondissementTableMap::COL_UPDATED_AT, $updatedAt['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(ArrondissementTableMap::COL_UPDATED_AT, $updatedAt, $comparison);
    }

    /**
     * Exclude object from result
     *
     * @param   ChildArrondissement $arrondissement Object to remove from the list of results
     *
     * @return $this|ChildArrondissementQuery The current query, for fluid interface
     */
    public function prune($arrondissement = null)
    {
        if ($arrondissement) {
            $this->addUsingAlias(ArrondissementTableMap::COL_ID_ARRONDISSEMENT, $arrondissement->getIdArrondissement(), Criteria::NOT_EQUAL);
        }

        return $this;
    }

    /**
     * Deletes all rows from the geo_arrondissements table.
     *
     * @param ConnectionInterface $con the connection to use
     * @return int The number of affected rows (if supported by underlying database driver).
     */
    public function doDeleteAll(ConnectionInterface $con = null)
    {
        if (null === $con) {
            $con = Propel::getServiceContainer()->getWriteConnection(ArrondissementTableMap::DATABASE_NAME);
        }

        // use transaction because $criteria could contain info
        // for more than one table or we could emulating ON DELETE CASCADE, etc.
        return $con->transaction(function () use ($con) {
            $affectedRows = 0; // initialize var to track total num of affected rows
            $affectedRows += parent::doDeleteAll($con);
            // Because this db requires some delete cascade/set null emulation, we have to
            // clear the cached instance *after* the emulation has happened (since
            // instances get re-added by the select statement contained therein).
            ArrondissementTableMap::clearInstancePool();
            ArrondissementTableMap::clearRelatedInstancePool();

            return $affectedRows;
        });
    }

    /**
     * Performs a DELETE on the database based on the current ModelCriteria
     *
     * @param ConnectionInterface $con the connection to use
     * @return int             The number of affected rows (if supported by underlying database driver).  This includes CASCADE-related rows
     *                         if supported by native driver or if emulated using Propel.
     * @throws PropelException Any exceptions caught during processing will be
     *                         rethrown wrapped into a PropelException.
     */
    public function delete(ConnectionInterface $con = null)
    {
        if (null === $con) {
            $con = Propel::getServiceContainer()->getWriteConnection(ArrondissementTableMap::DATABASE_NAME);
        }

        $criteria = $this;

        // Set the correct dbName
        $criteria->setDbName(ArrondissementTableMap::DATABASE_NAME);

        // use transaction because $criteria could contain info
        // for more than one table or we could emulating ON DELETE CASCADE, etc.
        return $con->transaction(function () use ($con, $criteria) {
            $affectedRows = 0; // initialize var to track total num of affected rows

            ArrondissementTableMap::removeInstanceFromPool($criteria);

            $affectedRows += ModelCriteria::delete($con);
            ArrondissementTableMap::clearRelatedInstancePool();

            return $affectedRows;
        });
    }

    // timestampable behavior

    /**
     * Filter by the latest updated
     *
     * @param      int $nbDays Maximum age of the latest update in days
     *
     * @return     $this|ChildArrondissementQuery The current query, for fluid interface
     */
    public function recentlyUpdated($nbDays = 7)
    {
        return $this->addUsingAlias(ArrondissementTableMap::COL_UPDATED_AT, time() - $nbDays * 24 * 60 * 60, Criteria::GREATER_EQUAL);
    }

    /**
     * Order by update date desc
     *
     * @return     $this|ChildArrondissementQuery The current query, for fluid interface
     */
    public function lastUpdatedFirst()
    {
        return $this->addDescendingOrderByColumn(ArrondissementTableMap::COL_UPDATED_AT);
    }

    /**
     * Order by update date asc
     *
     * @return     $this|ChildArrondissementQuery The current query, for fluid interface
     */
    public function firstUpdatedFirst()
    {
        return $this->addAscendingOrderByColumn(ArrondissementTableMap::COL_UPDATED_AT);
    }

    /**
     * Order by create date desc
     *
     * @return     $this|ChildArrondissementQuery The current query, for fluid interface
     */
    public function lastCreatedFirst()
    {
        return $this->addDescendingOrderByColumn(ArrondissementTableMap::COL_CREATED_AT);
    }

    /**
     * Filter by the latest created
     *
     * @param      int $nbDays Maximum age of in days
     *
     * @return     $this|ChildArrondissementQuery The current query, for fluid interface
     */
    public function recentlyCreated($nbDays = 7)
    {
        return $this->addUsingAlias(ArrondissementTableMap::COL_CREATED_AT, time() - $nbDays * 24 * 60 * 60, Criteria::GREATER_EQUAL);
    }

    /**
     * Order by create date asc
     *
     * @return     $this|ChildArrondissementQuery The current query, for fluid interface
     */
    public function firstCreatedFirst()
    {
        return $this->addAscendingOrderByColumn(ArrondissementTableMap::COL_CREATED_AT);
    }

} // ArrondissementQuery
