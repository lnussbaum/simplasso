<?php

namespace Base;

use \CommuneLien as ChildCommuneLien;
use \CommuneLienQuery as ChildCommuneLienQuery;
use \Exception;
use \PDO;
use Map\CommuneLienTableMap;
use Propel\Runtime\Propel;
use Propel\Runtime\ActiveQuery\Criteria;
use Propel\Runtime\ActiveQuery\ModelCriteria;
use Propel\Runtime\Collection\ObjectCollection;
use Propel\Runtime\Connection\ConnectionInterface;
use Propel\Runtime\Exception\PropelException;

/**
 * Base class that represents a query for the 'geo_communes_liens' table.
 *
 *
 *
 * @method     ChildCommuneLienQuery orderByIdCommune($order = Criteria::ASC) Order by the id_commune column
 * @method     ChildCommuneLienQuery orderByObjet($order = Criteria::ASC) Order by the objet column
 * @method     ChildCommuneLienQuery orderByIdObjet($order = Criteria::ASC) Order by the id_objet column
 * @method     ChildCommuneLienQuery orderByCreatedAt($order = Criteria::ASC) Order by the created_at column
 * @method     ChildCommuneLienQuery orderByUpdatedAt($order = Criteria::ASC) Order by the updated_at column
 *
 * @method     ChildCommuneLienQuery groupByIdCommune() Group by the id_commune column
 * @method     ChildCommuneLienQuery groupByObjet() Group by the objet column
 * @method     ChildCommuneLienQuery groupByIdObjet() Group by the id_objet column
 * @method     ChildCommuneLienQuery groupByCreatedAt() Group by the created_at column
 * @method     ChildCommuneLienQuery groupByUpdatedAt() Group by the updated_at column
 *
 * @method     ChildCommuneLienQuery leftJoin($relation) Adds a LEFT JOIN clause to the query
 * @method     ChildCommuneLienQuery rightJoin($relation) Adds a RIGHT JOIN clause to the query
 * @method     ChildCommuneLienQuery innerJoin($relation) Adds a INNER JOIN clause to the query
 *
 * @method     ChildCommuneLienQuery leftJoinWith($relation) Adds a LEFT JOIN clause and with to the query
 * @method     ChildCommuneLienQuery rightJoinWith($relation) Adds a RIGHT JOIN clause and with to the query
 * @method     ChildCommuneLienQuery innerJoinWith($relation) Adds a INNER JOIN clause and with to the query
 *
 * @method     ChildCommuneLien findOne(ConnectionInterface $con = null) Return the first ChildCommuneLien matching the query
 * @method     ChildCommuneLien findOneOrCreate(ConnectionInterface $con = null) Return the first ChildCommuneLien matching the query, or a new ChildCommuneLien object populated from the query conditions when no match is found
 *
 * @method     ChildCommuneLien findOneByIdCommune(int $id_commune) Return the first ChildCommuneLien filtered by the id_commune column
 * @method     ChildCommuneLien findOneByObjet(string $objet) Return the first ChildCommuneLien filtered by the objet column
 * @method     ChildCommuneLien findOneByIdObjet(string $id_objet) Return the first ChildCommuneLien filtered by the id_objet column
 * @method     ChildCommuneLien findOneByCreatedAt(string $created_at) Return the first ChildCommuneLien filtered by the created_at column
 * @method     ChildCommuneLien findOneByUpdatedAt(string $updated_at) Return the first ChildCommuneLien filtered by the updated_at column *

 * @method     ChildCommuneLien requirePk($key, ConnectionInterface $con = null) Return the ChildCommuneLien by primary key and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildCommuneLien requireOne(ConnectionInterface $con = null) Return the first ChildCommuneLien matching the query and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 *
 * @method     ChildCommuneLien requireOneByIdCommune(int $id_commune) Return the first ChildCommuneLien filtered by the id_commune column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildCommuneLien requireOneByObjet(string $objet) Return the first ChildCommuneLien filtered by the objet column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildCommuneLien requireOneByIdObjet(string $id_objet) Return the first ChildCommuneLien filtered by the id_objet column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildCommuneLien requireOneByCreatedAt(string $created_at) Return the first ChildCommuneLien filtered by the created_at column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildCommuneLien requireOneByUpdatedAt(string $updated_at) Return the first ChildCommuneLien filtered by the updated_at column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 *
 * @method     ChildCommuneLien[]|ObjectCollection find(ConnectionInterface $con = null) Return ChildCommuneLien objects based on current ModelCriteria
 * @method     ChildCommuneLien[]|ObjectCollection findByIdCommune(int $id_commune) Return ChildCommuneLien objects filtered by the id_commune column
 * @method     ChildCommuneLien[]|ObjectCollection findByObjet(string $objet) Return ChildCommuneLien objects filtered by the objet column
 * @method     ChildCommuneLien[]|ObjectCollection findByIdObjet(string $id_objet) Return ChildCommuneLien objects filtered by the id_objet column
 * @method     ChildCommuneLien[]|ObjectCollection findByCreatedAt(string $created_at) Return ChildCommuneLien objects filtered by the created_at column
 * @method     ChildCommuneLien[]|ObjectCollection findByUpdatedAt(string $updated_at) Return ChildCommuneLien objects filtered by the updated_at column
 * @method     ChildCommuneLien[]|\Propel\Runtime\Util\PropelModelPager paginate($page = 1, $maxPerPage = 10, ConnectionInterface $con = null) Issue a SELECT query based on the current ModelCriteria and uses a page and a maximum number of results per page to compute an offset and a limit
 *
 */
abstract class CommuneLienQuery extends ModelCriteria
{
    protected $entityNotFoundExceptionClass = '\\Propel\\Runtime\\Exception\\EntityNotFoundException';

    /**
     * Initializes internal state of \Base\CommuneLienQuery object.
     *
     * @param     string $dbName The database name
     * @param     string $modelName The phpName of a model, e.g. 'Book'
     * @param     string $modelAlias The alias for the model in this query, e.g. 'b'
     */
    public function __construct($dbName = 'simplasso', $modelName = '\\CommuneLien', $modelAlias = null)
    {
        parent::__construct($dbName, $modelName, $modelAlias);
    }

    /**
     * Returns a new ChildCommuneLienQuery object.
     *
     * @param     string $modelAlias The alias of a model in the query
     * @param     Criteria $criteria Optional Criteria to build the query from
     *
     * @return ChildCommuneLienQuery
     */
    public static function create($modelAlias = null, Criteria $criteria = null)
    {
        if ($criteria instanceof ChildCommuneLienQuery) {
            return $criteria;
        }
        $query = new ChildCommuneLienQuery();
        if (null !== $modelAlias) {
            $query->setModelAlias($modelAlias);
        }
        if ($criteria instanceof Criteria) {
            $query->mergeWith($criteria);
        }

        return $query;
    }

    /**
     * Find object by primary key.
     * Propel uses the instance pool to skip the database if the object exists.
     * Go fast if the query is untouched.
     *
     * <code>
     * $obj = $c->findPk(array(12, 34, 56), $con);
     * </code>
     *
     * @param array[$id_commune, $objet, $id_objet] $key Primary key to use for the query
     * @param ConnectionInterface $con an optional connection object
     *
     * @return ChildCommuneLien|array|mixed the result, formatted by the current formatter
     */
    public function findPk($key, ConnectionInterface $con = null)
    {
        if ($key === null) {
            return null;
        }

        if ($con === null) {
            $con = Propel::getServiceContainer()->getReadConnection(CommuneLienTableMap::DATABASE_NAME);
        }

        $this->basePreSelect($con);

        if (
            $this->formatter || $this->modelAlias || $this->with || $this->select
            || $this->selectColumns || $this->asColumns || $this->selectModifiers
            || $this->map || $this->having || $this->joins
        ) {
            return $this->findPkComplex($key, $con);
        }

        if ((null !== ($obj = CommuneLienTableMap::getInstanceFromPool(serialize([(null === $key[0] || is_scalar($key[0]) || is_callable([$key[0], '__toString']) ? (string) $key[0] : $key[0]), (null === $key[1] || is_scalar($key[1]) || is_callable([$key[1], '__toString']) ? (string) $key[1] : $key[1]), (null === $key[2] || is_scalar($key[2]) || is_callable([$key[2], '__toString']) ? (string) $key[2] : $key[2])]))))) {
            // the object is already in the instance pool
            return $obj;
        }

        return $this->findPkSimple($key, $con);
    }

    /**
     * Find object by primary key using raw SQL to go fast.
     * Bypass doSelect() and the object formatter by using generated code.
     *
     * @param     mixed $key Primary key to use for the query
     * @param     ConnectionInterface $con A connection object
     *
     * @throws \Propel\Runtime\Exception\PropelException
     *
     * @return ChildCommuneLien A model object, or null if the key is not found
     */
    protected function findPkSimple($key, ConnectionInterface $con)
    {
        $sql = 'SELECT `id_commune`, `objet`, `id_objet`, `created_at`, `updated_at` FROM `geo_communes_liens` WHERE `id_commune` = :p0 AND `objet` = :p1 AND `id_objet` = :p2';
        try {
            $stmt = $con->prepare($sql);
            $stmt->bindValue(':p0', $key[0], PDO::PARAM_INT);
            $stmt->bindValue(':p1', $key[1], PDO::PARAM_STR);
            $stmt->bindValue(':p2', $key[2], PDO::PARAM_INT);
            $stmt->execute();
        } catch (Exception $e) {
            Propel::log($e->getMessage(), Propel::LOG_ERR);
            throw new PropelException(sprintf('Unable to execute SELECT statement [%s]', $sql), 0, $e);
        }
        $obj = null;
        if ($row = $stmt->fetch(\PDO::FETCH_NUM)) {
            /** @var ChildCommuneLien $obj */
            $obj = new ChildCommuneLien();
            $obj->hydrate($row);
            CommuneLienTableMap::addInstanceToPool($obj, serialize([(null === $key[0] || is_scalar($key[0]) || is_callable([$key[0], '__toString']) ? (string) $key[0] : $key[0]), (null === $key[1] || is_scalar($key[1]) || is_callable([$key[1], '__toString']) ? (string) $key[1] : $key[1]), (null === $key[2] || is_scalar($key[2]) || is_callable([$key[2], '__toString']) ? (string) $key[2] : $key[2])]));
        }
        $stmt->closeCursor();

        return $obj;
    }

    /**
     * Find object by primary key.
     *
     * @param     mixed $key Primary key to use for the query
     * @param     ConnectionInterface $con A connection object
     *
     * @return ChildCommuneLien|array|mixed the result, formatted by the current formatter
     */
    protected function findPkComplex($key, ConnectionInterface $con)
    {
        // As the query uses a PK condition, no limit(1) is necessary.
        $criteria = $this->isKeepQuery() ? clone $this : $this;
        $dataFetcher = $criteria
            ->filterByPrimaryKey($key)
            ->doSelect($con);

        return $criteria->getFormatter()->init($criteria)->formatOne($dataFetcher);
    }

    /**
     * Find objects by primary key
     * <code>
     * $objs = $c->findPks(array(array(12, 56), array(832, 123), array(123, 456)), $con);
     * </code>
     * @param     array $keys Primary keys to use for the query
     * @param     ConnectionInterface $con an optional connection object
     *
     * @return ObjectCollection|array|mixed the list of results, formatted by the current formatter
     */
    public function findPks($keys, ConnectionInterface $con = null)
    {
        if (null === $con) {
            $con = Propel::getServiceContainer()->getReadConnection($this->getDbName());
        }
        $this->basePreSelect($con);
        $criteria = $this->isKeepQuery() ? clone $this : $this;
        $dataFetcher = $criteria
            ->filterByPrimaryKeys($keys)
            ->doSelect($con);

        return $criteria->getFormatter()->init($criteria)->format($dataFetcher);
    }

    /**
     * Filter the query by primary key
     *
     * @param     mixed $key Primary key to use for the query
     *
     * @return $this|ChildCommuneLienQuery The current query, for fluid interface
     */
    public function filterByPrimaryKey($key)
    {
        $this->addUsingAlias(CommuneLienTableMap::COL_ID_COMMUNE, $key[0], Criteria::EQUAL);
        $this->addUsingAlias(CommuneLienTableMap::COL_OBJET, $key[1], Criteria::EQUAL);
        $this->addUsingAlias(CommuneLienTableMap::COL_ID_OBJET, $key[2], Criteria::EQUAL);

        return $this;
    }

    /**
     * Filter the query by a list of primary keys
     *
     * @param     array $keys The list of primary key to use for the query
     *
     * @return $this|ChildCommuneLienQuery The current query, for fluid interface
     */
    public function filterByPrimaryKeys($keys)
    {
        if (empty($keys)) {
            return $this->add(null, '1<>1', Criteria::CUSTOM);
        }
        foreach ($keys as $key) {
            $cton0 = $this->getNewCriterion(CommuneLienTableMap::COL_ID_COMMUNE, $key[0], Criteria::EQUAL);
            $cton1 = $this->getNewCriterion(CommuneLienTableMap::COL_OBJET, $key[1], Criteria::EQUAL);
            $cton0->addAnd($cton1);
            $cton2 = $this->getNewCriterion(CommuneLienTableMap::COL_ID_OBJET, $key[2], Criteria::EQUAL);
            $cton0->addAnd($cton2);
            $this->addOr($cton0);
        }

        return $this;
    }

    /**
     * Filter the query on the id_commune column
     *
     * Example usage:
     * <code>
     * $query->filterByIdCommune(1234); // WHERE id_commune = 1234
     * $query->filterByIdCommune(array(12, 34)); // WHERE id_commune IN (12, 34)
     * $query->filterByIdCommune(array('min' => 12)); // WHERE id_commune > 12
     * </code>
     *
     * @param     mixed $idCommune The value to use as filter.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildCommuneLienQuery The current query, for fluid interface
     */
    public function filterByIdCommune($idCommune = null, $comparison = null)
    {
        if (is_array($idCommune)) {
            $useMinMax = false;
            if (isset($idCommune['min'])) {
                $this->addUsingAlias(CommuneLienTableMap::COL_ID_COMMUNE, $idCommune['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($idCommune['max'])) {
                $this->addUsingAlias(CommuneLienTableMap::COL_ID_COMMUNE, $idCommune['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(CommuneLienTableMap::COL_ID_COMMUNE, $idCommune, $comparison);
    }

    /**
     * Filter the query on the objet column
     *
     * Example usage:
     * <code>
     * $query->filterByObjet('fooValue');   // WHERE objet = 'fooValue'
     * $query->filterByObjet('%fooValue%', Criteria::LIKE); // WHERE objet LIKE '%fooValue%'
     * </code>
     *
     * @param     string $objet The value to use as filter.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildCommuneLienQuery The current query, for fluid interface
     */
    public function filterByObjet($objet = null, $comparison = null)
    {
        if (null === $comparison) {
            if (is_array($objet)) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(CommuneLienTableMap::COL_OBJET, $objet, $comparison);
    }

    /**
     * Filter the query on the id_objet column
     *
     * Example usage:
     * <code>
     * $query->filterByIdObjet(1234); // WHERE id_objet = 1234
     * $query->filterByIdObjet(array(12, 34)); // WHERE id_objet IN (12, 34)
     * $query->filterByIdObjet(array('min' => 12)); // WHERE id_objet > 12
     * </code>
     *
     * @param     mixed $idObjet The value to use as filter.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildCommuneLienQuery The current query, for fluid interface
     */
    public function filterByIdObjet($idObjet = null, $comparison = null)
    {
        if (is_array($idObjet)) {
            $useMinMax = false;
            if (isset($idObjet['min'])) {
                $this->addUsingAlias(CommuneLienTableMap::COL_ID_OBJET, $idObjet['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($idObjet['max'])) {
                $this->addUsingAlias(CommuneLienTableMap::COL_ID_OBJET, $idObjet['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(CommuneLienTableMap::COL_ID_OBJET, $idObjet, $comparison);
    }

    /**
     * Filter the query on the created_at column
     *
     * Example usage:
     * <code>
     * $query->filterByCreatedAt('2011-03-14'); // WHERE created_at = '2011-03-14'
     * $query->filterByCreatedAt('now'); // WHERE created_at = '2011-03-14'
     * $query->filterByCreatedAt(array('max' => 'yesterday')); // WHERE created_at > '2011-03-13'
     * </code>
     *
     * @param     mixed $createdAt The value to use as filter.
     *              Values can be integers (unix timestamps), DateTime objects, or strings.
     *              Empty strings are treated as NULL.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildCommuneLienQuery The current query, for fluid interface
     */
    public function filterByCreatedAt($createdAt = null, $comparison = null)
    {
        if (is_array($createdAt)) {
            $useMinMax = false;
            if (isset($createdAt['min'])) {
                $this->addUsingAlias(CommuneLienTableMap::COL_CREATED_AT, $createdAt['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($createdAt['max'])) {
                $this->addUsingAlias(CommuneLienTableMap::COL_CREATED_AT, $createdAt['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(CommuneLienTableMap::COL_CREATED_AT, $createdAt, $comparison);
    }

    /**
     * Filter the query on the updated_at column
     *
     * Example usage:
     * <code>
     * $query->filterByUpdatedAt('2011-03-14'); // WHERE updated_at = '2011-03-14'
     * $query->filterByUpdatedAt('now'); // WHERE updated_at = '2011-03-14'
     * $query->filterByUpdatedAt(array('max' => 'yesterday')); // WHERE updated_at > '2011-03-13'
     * </code>
     *
     * @param     mixed $updatedAt The value to use as filter.
     *              Values can be integers (unix timestamps), DateTime objects, or strings.
     *              Empty strings are treated as NULL.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildCommuneLienQuery The current query, for fluid interface
     */
    public function filterByUpdatedAt($updatedAt = null, $comparison = null)
    {
        if (is_array($updatedAt)) {
            $useMinMax = false;
            if (isset($updatedAt['min'])) {
                $this->addUsingAlias(CommuneLienTableMap::COL_UPDATED_AT, $updatedAt['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($updatedAt['max'])) {
                $this->addUsingAlias(CommuneLienTableMap::COL_UPDATED_AT, $updatedAt['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(CommuneLienTableMap::COL_UPDATED_AT, $updatedAt, $comparison);
    }

    /**
     * Exclude object from result
     *
     * @param   ChildCommuneLien $communeLien Object to remove from the list of results
     *
     * @return $this|ChildCommuneLienQuery The current query, for fluid interface
     */
    public function prune($communeLien = null)
    {
        if ($communeLien) {
            $this->addCond('pruneCond0', $this->getAliasedColName(CommuneLienTableMap::COL_ID_COMMUNE), $communeLien->getIdCommune(), Criteria::NOT_EQUAL);
            $this->addCond('pruneCond1', $this->getAliasedColName(CommuneLienTableMap::COL_OBJET), $communeLien->getObjet(), Criteria::NOT_EQUAL);
            $this->addCond('pruneCond2', $this->getAliasedColName(CommuneLienTableMap::COL_ID_OBJET), $communeLien->getIdObjet(), Criteria::NOT_EQUAL);
            $this->combine(array('pruneCond0', 'pruneCond1', 'pruneCond2'), Criteria::LOGICAL_OR);
        }

        return $this;
    }

    /**
     * Deletes all rows from the geo_communes_liens table.
     *
     * @param ConnectionInterface $con the connection to use
     * @return int The number of affected rows (if supported by underlying database driver).
     */
    public function doDeleteAll(ConnectionInterface $con = null)
    {
        if (null === $con) {
            $con = Propel::getServiceContainer()->getWriteConnection(CommuneLienTableMap::DATABASE_NAME);
        }

        // use transaction because $criteria could contain info
        // for more than one table or we could emulating ON DELETE CASCADE, etc.
        return $con->transaction(function () use ($con) {
            $affectedRows = 0; // initialize var to track total num of affected rows
            $affectedRows += parent::doDeleteAll($con);
            // Because this db requires some delete cascade/set null emulation, we have to
            // clear the cached instance *after* the emulation has happened (since
            // instances get re-added by the select statement contained therein).
            CommuneLienTableMap::clearInstancePool();
            CommuneLienTableMap::clearRelatedInstancePool();

            return $affectedRows;
        });
    }

    /**
     * Performs a DELETE on the database based on the current ModelCriteria
     *
     * @param ConnectionInterface $con the connection to use
     * @return int             The number of affected rows (if supported by underlying database driver).  This includes CASCADE-related rows
     *                         if supported by native driver or if emulated using Propel.
     * @throws PropelException Any exceptions caught during processing will be
     *                         rethrown wrapped into a PropelException.
     */
    public function delete(ConnectionInterface $con = null)
    {
        if (null === $con) {
            $con = Propel::getServiceContainer()->getWriteConnection(CommuneLienTableMap::DATABASE_NAME);
        }

        $criteria = $this;

        // Set the correct dbName
        $criteria->setDbName(CommuneLienTableMap::DATABASE_NAME);

        // use transaction because $criteria could contain info
        // for more than one table or we could emulating ON DELETE CASCADE, etc.
        return $con->transaction(function () use ($con, $criteria) {
            $affectedRows = 0; // initialize var to track total num of affected rows

            CommuneLienTableMap::removeInstanceFromPool($criteria);

            $affectedRows += ModelCriteria::delete($con);
            CommuneLienTableMap::clearRelatedInstancePool();

            return $affectedRows;
        });
    }

    // timestampable behavior

    /**
     * Filter by the latest updated
     *
     * @param      int $nbDays Maximum age of the latest update in days
     *
     * @return     $this|ChildCommuneLienQuery The current query, for fluid interface
     */
    public function recentlyUpdated($nbDays = 7)
    {
        return $this->addUsingAlias(CommuneLienTableMap::COL_UPDATED_AT, time() - $nbDays * 24 * 60 * 60, Criteria::GREATER_EQUAL);
    }

    /**
     * Order by update date desc
     *
     * @return     $this|ChildCommuneLienQuery The current query, for fluid interface
     */
    public function lastUpdatedFirst()
    {
        return $this->addDescendingOrderByColumn(CommuneLienTableMap::COL_UPDATED_AT);
    }

    /**
     * Order by update date asc
     *
     * @return     $this|ChildCommuneLienQuery The current query, for fluid interface
     */
    public function firstUpdatedFirst()
    {
        return $this->addAscendingOrderByColumn(CommuneLienTableMap::COL_UPDATED_AT);
    }

    /**
     * Order by create date desc
     *
     * @return     $this|ChildCommuneLienQuery The current query, for fluid interface
     */
    public function lastCreatedFirst()
    {
        return $this->addDescendingOrderByColumn(CommuneLienTableMap::COL_CREATED_AT);
    }

    /**
     * Filter by the latest created
     *
     * @param      int $nbDays Maximum age of in days
     *
     * @return     $this|ChildCommuneLienQuery The current query, for fluid interface
     */
    public function recentlyCreated($nbDays = 7)
    {
        return $this->addUsingAlias(CommuneLienTableMap::COL_CREATED_AT, time() - $nbDays * 24 * 60 * 60, Criteria::GREATER_EQUAL);
    }

    /**
     * Order by create date asc
     *
     * @return     $this|ChildCommuneLienQuery The current query, for fluid interface
     */
    public function firstCreatedFirst()
    {
        return $this->addAscendingOrderByColumn(CommuneLienTableMap::COL_CREATED_AT);
    }

} // CommuneLienQuery
