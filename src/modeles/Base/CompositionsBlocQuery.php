<?php

namespace Base;

use \CompositionsBloc as ChildCompositionsBloc;
use \CompositionsBlocQuery as ChildCompositionsBlocQuery;
use \Exception;
use \PDO;
use Map\CompositionsBlocTableMap;
use Propel\Runtime\Propel;
use Propel\Runtime\ActiveQuery\Criteria;
use Propel\Runtime\ActiveQuery\ModelCriteria;
use Propel\Runtime\ActiveQuery\ModelJoin;
use Propel\Runtime\Collection\ObjectCollection;
use Propel\Runtime\Connection\ConnectionInterface;
use Propel\Runtime\Exception\PropelException;

/**
 * Base class that represents a query for the 'com_compositions_blocs' table.
 *
 *
 *
 * @method     ChildCompositionsBlocQuery orderByIdComposition($order = Criteria::ASC) Order by the id_composition column
 * @method     ChildCompositionsBlocQuery orderByIdBloc($order = Criteria::ASC) Order by the id_bloc column
 * @method     ChildCompositionsBlocQuery orderByOrdre($order = Criteria::ASC) Order by the ordre column
 * @method     ChildCompositionsBlocQuery orderByVariables($order = Criteria::ASC) Order by the variables column
 *
 * @method     ChildCompositionsBlocQuery groupByIdComposition() Group by the id_composition column
 * @method     ChildCompositionsBlocQuery groupByIdBloc() Group by the id_bloc column
 * @method     ChildCompositionsBlocQuery groupByOrdre() Group by the ordre column
 * @method     ChildCompositionsBlocQuery groupByVariables() Group by the variables column
 *
 * @method     ChildCompositionsBlocQuery leftJoin($relation) Adds a LEFT JOIN clause to the query
 * @method     ChildCompositionsBlocQuery rightJoin($relation) Adds a RIGHT JOIN clause to the query
 * @method     ChildCompositionsBlocQuery innerJoin($relation) Adds a INNER JOIN clause to the query
 *
 * @method     ChildCompositionsBlocQuery leftJoinWith($relation) Adds a LEFT JOIN clause and with to the query
 * @method     ChildCompositionsBlocQuery rightJoinWith($relation) Adds a RIGHT JOIN clause and with to the query
 * @method     ChildCompositionsBlocQuery innerJoinWith($relation) Adds a INNER JOIN clause and with to the query
 *
 * @method     ChildCompositionsBlocQuery leftJoinComposition($relationAlias = null) Adds a LEFT JOIN clause to the query using the Composition relation
 * @method     ChildCompositionsBlocQuery rightJoinComposition($relationAlias = null) Adds a RIGHT JOIN clause to the query using the Composition relation
 * @method     ChildCompositionsBlocQuery innerJoinComposition($relationAlias = null) Adds a INNER JOIN clause to the query using the Composition relation
 *
 * @method     ChildCompositionsBlocQuery joinWithComposition($joinType = Criteria::INNER_JOIN) Adds a join clause and with to the query using the Composition relation
 *
 * @method     ChildCompositionsBlocQuery leftJoinWithComposition() Adds a LEFT JOIN clause and with to the query using the Composition relation
 * @method     ChildCompositionsBlocQuery rightJoinWithComposition() Adds a RIGHT JOIN clause and with to the query using the Composition relation
 * @method     ChildCompositionsBlocQuery innerJoinWithComposition() Adds a INNER JOIN clause and with to the query using the Composition relation
 *
 * @method     ChildCompositionsBlocQuery leftJoinBloc($relationAlias = null) Adds a LEFT JOIN clause to the query using the Bloc relation
 * @method     ChildCompositionsBlocQuery rightJoinBloc($relationAlias = null) Adds a RIGHT JOIN clause to the query using the Bloc relation
 * @method     ChildCompositionsBlocQuery innerJoinBloc($relationAlias = null) Adds a INNER JOIN clause to the query using the Bloc relation
 *
 * @method     ChildCompositionsBlocQuery joinWithBloc($joinType = Criteria::INNER_JOIN) Adds a join clause and with to the query using the Bloc relation
 *
 * @method     ChildCompositionsBlocQuery leftJoinWithBloc() Adds a LEFT JOIN clause and with to the query using the Bloc relation
 * @method     ChildCompositionsBlocQuery rightJoinWithBloc() Adds a RIGHT JOIN clause and with to the query using the Bloc relation
 * @method     ChildCompositionsBlocQuery innerJoinWithBloc() Adds a INNER JOIN clause and with to the query using the Bloc relation
 *
 * @method     \CompositionQuery|\BlocQuery endUse() Finalizes a secondary criteria and merges it with its primary Criteria
 *
 * @method     ChildCompositionsBloc findOne(ConnectionInterface $con = null) Return the first ChildCompositionsBloc matching the query
 * @method     ChildCompositionsBloc findOneOrCreate(ConnectionInterface $con = null) Return the first ChildCompositionsBloc matching the query, or a new ChildCompositionsBloc object populated from the query conditions when no match is found
 *
 * @method     ChildCompositionsBloc findOneByIdComposition(int $id_composition) Return the first ChildCompositionsBloc filtered by the id_composition column
 * @method     ChildCompositionsBloc findOneByIdBloc(int $id_bloc) Return the first ChildCompositionsBloc filtered by the id_bloc column
 * @method     ChildCompositionsBloc findOneByOrdre(int $ordre) Return the first ChildCompositionsBloc filtered by the ordre column
 * @method     ChildCompositionsBloc findOneByVariables(string $variables) Return the first ChildCompositionsBloc filtered by the variables column *

 * @method     ChildCompositionsBloc requirePk($key, ConnectionInterface $con = null) Return the ChildCompositionsBloc by primary key and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildCompositionsBloc requireOne(ConnectionInterface $con = null) Return the first ChildCompositionsBloc matching the query and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 *
 * @method     ChildCompositionsBloc requireOneByIdComposition(int $id_composition) Return the first ChildCompositionsBloc filtered by the id_composition column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildCompositionsBloc requireOneByIdBloc(int $id_bloc) Return the first ChildCompositionsBloc filtered by the id_bloc column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildCompositionsBloc requireOneByOrdre(int $ordre) Return the first ChildCompositionsBloc filtered by the ordre column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildCompositionsBloc requireOneByVariables(string $variables) Return the first ChildCompositionsBloc filtered by the variables column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 *
 * @method     ChildCompositionsBloc[]|ObjectCollection find(ConnectionInterface $con = null) Return ChildCompositionsBloc objects based on current ModelCriteria
 * @method     ChildCompositionsBloc[]|ObjectCollection findByIdComposition(int $id_composition) Return ChildCompositionsBloc objects filtered by the id_composition column
 * @method     ChildCompositionsBloc[]|ObjectCollection findByIdBloc(int $id_bloc) Return ChildCompositionsBloc objects filtered by the id_bloc column
 * @method     ChildCompositionsBloc[]|ObjectCollection findByOrdre(int $ordre) Return ChildCompositionsBloc objects filtered by the ordre column
 * @method     ChildCompositionsBloc[]|ObjectCollection findByVariables(string $variables) Return ChildCompositionsBloc objects filtered by the variables column
 * @method     ChildCompositionsBloc[]|\Propel\Runtime\Util\PropelModelPager paginate($page = 1, $maxPerPage = 10, ConnectionInterface $con = null) Issue a SELECT query based on the current ModelCriteria and uses a page and a maximum number of results per page to compute an offset and a limit
 *
 */
abstract class CompositionsBlocQuery extends ModelCriteria
{
    protected $entityNotFoundExceptionClass = '\\Propel\\Runtime\\Exception\\EntityNotFoundException';

    /**
     * Initializes internal state of \Base\CompositionsBlocQuery object.
     *
     * @param     string $dbName The database name
     * @param     string $modelName The phpName of a model, e.g. 'Book'
     * @param     string $modelAlias The alias for the model in this query, e.g. 'b'
     */
    public function __construct($dbName = 'simplasso', $modelName = '\\CompositionsBloc', $modelAlias = null)
    {
        parent::__construct($dbName, $modelName, $modelAlias);
    }

    /**
     * Returns a new ChildCompositionsBlocQuery object.
     *
     * @param     string $modelAlias The alias of a model in the query
     * @param     Criteria $criteria Optional Criteria to build the query from
     *
     * @return ChildCompositionsBlocQuery
     */
    public static function create($modelAlias = null, Criteria $criteria = null)
    {
        if ($criteria instanceof ChildCompositionsBlocQuery) {
            return $criteria;
        }
        $query = new ChildCompositionsBlocQuery();
        if (null !== $modelAlias) {
            $query->setModelAlias($modelAlias);
        }
        if ($criteria instanceof Criteria) {
            $query->mergeWith($criteria);
        }

        return $query;
    }

    /**
     * Find object by primary key.
     * Propel uses the instance pool to skip the database if the object exists.
     * Go fast if the query is untouched.
     *
     * <code>
     * $obj = $c->findPk(array(12, 34), $con);
     * </code>
     *
     * @param array[$id_composition, $id_bloc] $key Primary key to use for the query
     * @param ConnectionInterface $con an optional connection object
     *
     * @return ChildCompositionsBloc|array|mixed the result, formatted by the current formatter
     */
    public function findPk($key, ConnectionInterface $con = null)
    {
        if ($key === null) {
            return null;
        }

        if ($con === null) {
            $con = Propel::getServiceContainer()->getReadConnection(CompositionsBlocTableMap::DATABASE_NAME);
        }

        $this->basePreSelect($con);

        if (
            $this->formatter || $this->modelAlias || $this->with || $this->select
            || $this->selectColumns || $this->asColumns || $this->selectModifiers
            || $this->map || $this->having || $this->joins
        ) {
            return $this->findPkComplex($key, $con);
        }

        if ((null !== ($obj = CompositionsBlocTableMap::getInstanceFromPool(serialize([(null === $key[0] || is_scalar($key[0]) || is_callable([$key[0], '__toString']) ? (string) $key[0] : $key[0]), (null === $key[1] || is_scalar($key[1]) || is_callable([$key[1], '__toString']) ? (string) $key[1] : $key[1])]))))) {
            // the object is already in the instance pool
            return $obj;
        }

        return $this->findPkSimple($key, $con);
    }

    /**
     * Find object by primary key using raw SQL to go fast.
     * Bypass doSelect() and the object formatter by using generated code.
     *
     * @param     mixed $key Primary key to use for the query
     * @param     ConnectionInterface $con A connection object
     *
     * @throws \Propel\Runtime\Exception\PropelException
     *
     * @return ChildCompositionsBloc A model object, or null if the key is not found
     */
    protected function findPkSimple($key, ConnectionInterface $con)
    {
        $sql = 'SELECT `id_composition`, `id_bloc`, `ordre`, `variables` FROM `com_compositions_blocs` WHERE `id_composition` = :p0 AND `id_bloc` = :p1';
        try {
            $stmt = $con->prepare($sql);
            $stmt->bindValue(':p0', $key[0], PDO::PARAM_INT);
            $stmt->bindValue(':p1', $key[1], PDO::PARAM_INT);
            $stmt->execute();
        } catch (Exception $e) {
            Propel::log($e->getMessage(), Propel::LOG_ERR);
            throw new PropelException(sprintf('Unable to execute SELECT statement [%s]', $sql), 0, $e);
        }
        $obj = null;
        if ($row = $stmt->fetch(\PDO::FETCH_NUM)) {
            /** @var ChildCompositionsBloc $obj */
            $obj = new ChildCompositionsBloc();
            $obj->hydrate($row);
            CompositionsBlocTableMap::addInstanceToPool($obj, serialize([(null === $key[0] || is_scalar($key[0]) || is_callable([$key[0], '__toString']) ? (string) $key[0] : $key[0]), (null === $key[1] || is_scalar($key[1]) || is_callable([$key[1], '__toString']) ? (string) $key[1] : $key[1])]));
        }
        $stmt->closeCursor();

        return $obj;
    }

    /**
     * Find object by primary key.
     *
     * @param     mixed $key Primary key to use for the query
     * @param     ConnectionInterface $con A connection object
     *
     * @return ChildCompositionsBloc|array|mixed the result, formatted by the current formatter
     */
    protected function findPkComplex($key, ConnectionInterface $con)
    {
        // As the query uses a PK condition, no limit(1) is necessary.
        $criteria = $this->isKeepQuery() ? clone $this : $this;
        $dataFetcher = $criteria
            ->filterByPrimaryKey($key)
            ->doSelect($con);

        return $criteria->getFormatter()->init($criteria)->formatOne($dataFetcher);
    }

    /**
     * Find objects by primary key
     * <code>
     * $objs = $c->findPks(array(array(12, 56), array(832, 123), array(123, 456)), $con);
     * </code>
     * @param     array $keys Primary keys to use for the query
     * @param     ConnectionInterface $con an optional connection object
     *
     * @return ObjectCollection|array|mixed the list of results, formatted by the current formatter
     */
    public function findPks($keys, ConnectionInterface $con = null)
    {
        if (null === $con) {
            $con = Propel::getServiceContainer()->getReadConnection($this->getDbName());
        }
        $this->basePreSelect($con);
        $criteria = $this->isKeepQuery() ? clone $this : $this;
        $dataFetcher = $criteria
            ->filterByPrimaryKeys($keys)
            ->doSelect($con);

        return $criteria->getFormatter()->init($criteria)->format($dataFetcher);
    }

    /**
     * Filter the query by primary key
     *
     * @param     mixed $key Primary key to use for the query
     *
     * @return $this|ChildCompositionsBlocQuery The current query, for fluid interface
     */
    public function filterByPrimaryKey($key)
    {
        $this->addUsingAlias(CompositionsBlocTableMap::COL_ID_COMPOSITION, $key[0], Criteria::EQUAL);
        $this->addUsingAlias(CompositionsBlocTableMap::COL_ID_BLOC, $key[1], Criteria::EQUAL);

        return $this;
    }

    /**
     * Filter the query by a list of primary keys
     *
     * @param     array $keys The list of primary key to use for the query
     *
     * @return $this|ChildCompositionsBlocQuery The current query, for fluid interface
     */
    public function filterByPrimaryKeys($keys)
    {
        if (empty($keys)) {
            return $this->add(null, '1<>1', Criteria::CUSTOM);
        }
        foreach ($keys as $key) {
            $cton0 = $this->getNewCriterion(CompositionsBlocTableMap::COL_ID_COMPOSITION, $key[0], Criteria::EQUAL);
            $cton1 = $this->getNewCriterion(CompositionsBlocTableMap::COL_ID_BLOC, $key[1], Criteria::EQUAL);
            $cton0->addAnd($cton1);
            $this->addOr($cton0);
        }

        return $this;
    }

    /**
     * Filter the query on the id_composition column
     *
     * Example usage:
     * <code>
     * $query->filterByIdComposition(1234); // WHERE id_composition = 1234
     * $query->filterByIdComposition(array(12, 34)); // WHERE id_composition IN (12, 34)
     * $query->filterByIdComposition(array('min' => 12)); // WHERE id_composition > 12
     * </code>
     *
     * @see       filterByComposition()
     *
     * @param     mixed $idComposition The value to use as filter.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildCompositionsBlocQuery The current query, for fluid interface
     */
    public function filterByIdComposition($idComposition = null, $comparison = null)
    {
        if (is_array($idComposition)) {
            $useMinMax = false;
            if (isset($idComposition['min'])) {
                $this->addUsingAlias(CompositionsBlocTableMap::COL_ID_COMPOSITION, $idComposition['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($idComposition['max'])) {
                $this->addUsingAlias(CompositionsBlocTableMap::COL_ID_COMPOSITION, $idComposition['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(CompositionsBlocTableMap::COL_ID_COMPOSITION, $idComposition, $comparison);
    }

    /**
     * Filter the query on the id_bloc column
     *
     * Example usage:
     * <code>
     * $query->filterByIdBloc(1234); // WHERE id_bloc = 1234
     * $query->filterByIdBloc(array(12, 34)); // WHERE id_bloc IN (12, 34)
     * $query->filterByIdBloc(array('min' => 12)); // WHERE id_bloc > 12
     * </code>
     *
     * @see       filterByBloc()
     *
     * @param     mixed $idBloc The value to use as filter.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildCompositionsBlocQuery The current query, for fluid interface
     */
    public function filterByIdBloc($idBloc = null, $comparison = null)
    {
        if (is_array($idBloc)) {
            $useMinMax = false;
            if (isset($idBloc['min'])) {
                $this->addUsingAlias(CompositionsBlocTableMap::COL_ID_BLOC, $idBloc['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($idBloc['max'])) {
                $this->addUsingAlias(CompositionsBlocTableMap::COL_ID_BLOC, $idBloc['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(CompositionsBlocTableMap::COL_ID_BLOC, $idBloc, $comparison);
    }

    /**
     * Filter the query on the ordre column
     *
     * Example usage:
     * <code>
     * $query->filterByOrdre(1234); // WHERE ordre = 1234
     * $query->filterByOrdre(array(12, 34)); // WHERE ordre IN (12, 34)
     * $query->filterByOrdre(array('min' => 12)); // WHERE ordre > 12
     * </code>
     *
     * @param     mixed $ordre The value to use as filter.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildCompositionsBlocQuery The current query, for fluid interface
     */
    public function filterByOrdre($ordre = null, $comparison = null)
    {
        if (is_array($ordre)) {
            $useMinMax = false;
            if (isset($ordre['min'])) {
                $this->addUsingAlias(CompositionsBlocTableMap::COL_ORDRE, $ordre['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($ordre['max'])) {
                $this->addUsingAlias(CompositionsBlocTableMap::COL_ORDRE, $ordre['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(CompositionsBlocTableMap::COL_ORDRE, $ordre, $comparison);
    }

    /**
     * Filter the query on the variables column
     *
     * Example usage:
     * <code>
     * $query->filterByVariables('fooValue');   // WHERE variables = 'fooValue'
     * $query->filterByVariables('%fooValue%', Criteria::LIKE); // WHERE variables LIKE '%fooValue%'
     * </code>
     *
     * @param     string $variables The value to use as filter.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildCompositionsBlocQuery The current query, for fluid interface
     */
    public function filterByVariables($variables = null, $comparison = null)
    {
        if (null === $comparison) {
            if (is_array($variables)) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(CompositionsBlocTableMap::COL_VARIABLES, $variables, $comparison);
    }

    /**
     * Filter the query by a related \Composition object
     *
     * @param \Composition|ObjectCollection $composition The related object(s) to use as filter
     * @param string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @throws \Propel\Runtime\Exception\PropelException
     *
     * @return ChildCompositionsBlocQuery The current query, for fluid interface
     */
    public function filterByComposition($composition, $comparison = null)
    {
        if ($composition instanceof \Composition) {
            return $this
                ->addUsingAlias(CompositionsBlocTableMap::COL_ID_COMPOSITION, $composition->getIdComposition(), $comparison);
        } elseif ($composition instanceof ObjectCollection) {
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }

            return $this
                ->addUsingAlias(CompositionsBlocTableMap::COL_ID_COMPOSITION, $composition->toKeyValue('PrimaryKey', 'IdComposition'), $comparison);
        } else {
            throw new PropelException('filterByComposition() only accepts arguments of type \Composition or Collection');
        }
    }

    /**
     * Adds a JOIN clause to the query using the Composition relation
     *
     * @param     string $relationAlias optional alias for the relation
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return $this|ChildCompositionsBlocQuery The current query, for fluid interface
     */
    public function joinComposition($relationAlias = null, $joinType = Criteria::INNER_JOIN)
    {
        $tableMap = $this->getTableMap();
        $relationMap = $tableMap->getRelation('Composition');

        // create a ModelJoin object for this join
        $join = new ModelJoin();
        $join->setJoinType($joinType);
        $join->setRelationMap($relationMap, $this->useAliasInSQL ? $this->getModelAlias() : null, $relationAlias);
        if ($previousJoin = $this->getPreviousJoin()) {
            $join->setPreviousJoin($previousJoin);
        }

        // add the ModelJoin to the current object
        if ($relationAlias) {
            $this->addAlias($relationAlias, $relationMap->getRightTable()->getName());
            $this->addJoinObject($join, $relationAlias);
        } else {
            $this->addJoinObject($join, 'Composition');
        }

        return $this;
    }

    /**
     * Use the Composition relation Composition object
     *
     * @see useQuery()
     *
     * @param     string $relationAlias optional alias for the relation,
     *                                   to be used as main alias in the secondary query
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return \CompositionQuery A secondary query class using the current class as primary query
     */
    public function useCompositionQuery($relationAlias = null, $joinType = Criteria::INNER_JOIN)
    {
        return $this
            ->joinComposition($relationAlias, $joinType)
            ->useQuery($relationAlias ? $relationAlias : 'Composition', '\CompositionQuery');
    }

    /**
     * Filter the query by a related \Bloc object
     *
     * @param \Bloc|ObjectCollection $bloc The related object(s) to use as filter
     * @param string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @throws \Propel\Runtime\Exception\PropelException
     *
     * @return ChildCompositionsBlocQuery The current query, for fluid interface
     */
    public function filterByBloc($bloc, $comparison = null)
    {
        if ($bloc instanceof \Bloc) {
            return $this
                ->addUsingAlias(CompositionsBlocTableMap::COL_ID_BLOC, $bloc->getIdBloc(), $comparison);
        } elseif ($bloc instanceof ObjectCollection) {
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }

            return $this
                ->addUsingAlias(CompositionsBlocTableMap::COL_ID_BLOC, $bloc->toKeyValue('PrimaryKey', 'IdBloc'), $comparison);
        } else {
            throw new PropelException('filterByBloc() only accepts arguments of type \Bloc or Collection');
        }
    }

    /**
     * Adds a JOIN clause to the query using the Bloc relation
     *
     * @param     string $relationAlias optional alias for the relation
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return $this|ChildCompositionsBlocQuery The current query, for fluid interface
     */
    public function joinBloc($relationAlias = null, $joinType = Criteria::INNER_JOIN)
    {
        $tableMap = $this->getTableMap();
        $relationMap = $tableMap->getRelation('Bloc');

        // create a ModelJoin object for this join
        $join = new ModelJoin();
        $join->setJoinType($joinType);
        $join->setRelationMap($relationMap, $this->useAliasInSQL ? $this->getModelAlias() : null, $relationAlias);
        if ($previousJoin = $this->getPreviousJoin()) {
            $join->setPreviousJoin($previousJoin);
        }

        // add the ModelJoin to the current object
        if ($relationAlias) {
            $this->addAlias($relationAlias, $relationMap->getRightTable()->getName());
            $this->addJoinObject($join, $relationAlias);
        } else {
            $this->addJoinObject($join, 'Bloc');
        }

        return $this;
    }

    /**
     * Use the Bloc relation Bloc object
     *
     * @see useQuery()
     *
     * @param     string $relationAlias optional alias for the relation,
     *                                   to be used as main alias in the secondary query
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return \BlocQuery A secondary query class using the current class as primary query
     */
    public function useBlocQuery($relationAlias = null, $joinType = Criteria::INNER_JOIN)
    {
        return $this
            ->joinBloc($relationAlias, $joinType)
            ->useQuery($relationAlias ? $relationAlias : 'Bloc', '\BlocQuery');
    }

    /**
     * Exclude object from result
     *
     * @param   ChildCompositionsBloc $compositionsBloc Object to remove from the list of results
     *
     * @return $this|ChildCompositionsBlocQuery The current query, for fluid interface
     */
    public function prune($compositionsBloc = null)
    {
        if ($compositionsBloc) {
            $this->addCond('pruneCond0', $this->getAliasedColName(CompositionsBlocTableMap::COL_ID_COMPOSITION), $compositionsBloc->getIdComposition(), Criteria::NOT_EQUAL);
            $this->addCond('pruneCond1', $this->getAliasedColName(CompositionsBlocTableMap::COL_ID_BLOC), $compositionsBloc->getIdBloc(), Criteria::NOT_EQUAL);
            $this->combine(array('pruneCond0', 'pruneCond1'), Criteria::LOGICAL_OR);
        }

        return $this;
    }

    /**
     * Deletes all rows from the com_compositions_blocs table.
     *
     * @param ConnectionInterface $con the connection to use
     * @return int The number of affected rows (if supported by underlying database driver).
     */
    public function doDeleteAll(ConnectionInterface $con = null)
    {
        if (null === $con) {
            $con = Propel::getServiceContainer()->getWriteConnection(CompositionsBlocTableMap::DATABASE_NAME);
        }

        // use transaction because $criteria could contain info
        // for more than one table or we could emulating ON DELETE CASCADE, etc.
        return $con->transaction(function () use ($con) {
            $affectedRows = 0; // initialize var to track total num of affected rows
            $affectedRows += parent::doDeleteAll($con);
            // Because this db requires some delete cascade/set null emulation, we have to
            // clear the cached instance *after* the emulation has happened (since
            // instances get re-added by the select statement contained therein).
            CompositionsBlocTableMap::clearInstancePool();
            CompositionsBlocTableMap::clearRelatedInstancePool();

            return $affectedRows;
        });
    }

    /**
     * Performs a DELETE on the database based on the current ModelCriteria
     *
     * @param ConnectionInterface $con the connection to use
     * @return int             The number of affected rows (if supported by underlying database driver).  This includes CASCADE-related rows
     *                         if supported by native driver or if emulated using Propel.
     * @throws PropelException Any exceptions caught during processing will be
     *                         rethrown wrapped into a PropelException.
     */
    public function delete(ConnectionInterface $con = null)
    {
        if (null === $con) {
            $con = Propel::getServiceContainer()->getWriteConnection(CompositionsBlocTableMap::DATABASE_NAME);
        }

        $criteria = $this;

        // Set the correct dbName
        $criteria->setDbName(CompositionsBlocTableMap::DATABASE_NAME);

        // use transaction because $criteria could contain info
        // for more than one table or we could emulating ON DELETE CASCADE, etc.
        return $con->transaction(function () use ($con, $criteria) {
            $affectedRows = 0; // initialize var to track total num of affected rows

            CompositionsBlocTableMap::removeInstanceFromPool($criteria);

            $affectedRows += ModelCriteria::delete($con);
            CompositionsBlocTableMap::clearRelatedInstancePool();

            return $affectedRows;
        });
    }

} // CompositionsBlocQuery
