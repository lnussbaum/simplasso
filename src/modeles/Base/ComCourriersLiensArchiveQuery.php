<?php

namespace Base;

use \ComCourriersLiensArchive as ChildComCourriersLiensArchive;
use \ComCourriersLiensArchiveQuery as ChildComCourriersLiensArchiveQuery;
use \Exception;
use \PDO;
use Map\ComCourriersLiensArchiveTableMap;
use Propel\Runtime\Propel;
use Propel\Runtime\ActiveQuery\Criteria;
use Propel\Runtime\ActiveQuery\ModelCriteria;
use Propel\Runtime\Collection\ObjectCollection;
use Propel\Runtime\Connection\ConnectionInterface;
use Propel\Runtime\Exception\PropelException;

/**
 * Base class that represents a query for the 'com_courriers_liens_archive' table.
 *
 *
 *
 * @method     ChildComCourriersLiensArchiveQuery orderByIdCourrierLien($order = Criteria::ASC) Order by the id_courrier_lien column
 * @method     ChildComCourriersLiensArchiveQuery orderByIdMembre($order = Criteria::ASC) Order by the id_membre column
 * @method     ChildComCourriersLiensArchiveQuery orderByIdIndividu($order = Criteria::ASC) Order by the id_individu column
 * @method     ChildComCourriersLiensArchiveQuery orderByIdCourrier($order = Criteria::ASC) Order by the id_courrier column
 * @method     ChildComCourriersLiensArchiveQuery orderByCanal($order = Criteria::ASC) Order by the canal column
 * @method     ChildComCourriersLiensArchiveQuery orderByDateEnvoi($order = Criteria::ASC) Order by the date_envoi column
 * @method     ChildComCourriersLiensArchiveQuery orderByMessageParticulier($order = Criteria::ASC) Order by the message_particulier column
 * @method     ChildComCourriersLiensArchiveQuery orderByStatut($order = Criteria::ASC) Order by the statut column
 * @method     ChildComCourriersLiensArchiveQuery orderByVariables($order = Criteria::ASC) Order by the variables column
 * @method     ChildComCourriersLiensArchiveQuery orderByCreatedAt($order = Criteria::ASC) Order by the created_at column
 * @method     ChildComCourriersLiensArchiveQuery orderByUpdatedAt($order = Criteria::ASC) Order by the updated_at column
 * @method     ChildComCourriersLiensArchiveQuery orderByArchivedAt($order = Criteria::ASC) Order by the archived_at column
 *
 * @method     ChildComCourriersLiensArchiveQuery groupByIdCourrierLien() Group by the id_courrier_lien column
 * @method     ChildComCourriersLiensArchiveQuery groupByIdMembre() Group by the id_membre column
 * @method     ChildComCourriersLiensArchiveQuery groupByIdIndividu() Group by the id_individu column
 * @method     ChildComCourriersLiensArchiveQuery groupByIdCourrier() Group by the id_courrier column
 * @method     ChildComCourriersLiensArchiveQuery groupByCanal() Group by the canal column
 * @method     ChildComCourriersLiensArchiveQuery groupByDateEnvoi() Group by the date_envoi column
 * @method     ChildComCourriersLiensArchiveQuery groupByMessageParticulier() Group by the message_particulier column
 * @method     ChildComCourriersLiensArchiveQuery groupByStatut() Group by the statut column
 * @method     ChildComCourriersLiensArchiveQuery groupByVariables() Group by the variables column
 * @method     ChildComCourriersLiensArchiveQuery groupByCreatedAt() Group by the created_at column
 * @method     ChildComCourriersLiensArchiveQuery groupByUpdatedAt() Group by the updated_at column
 * @method     ChildComCourriersLiensArchiveQuery groupByArchivedAt() Group by the archived_at column
 *
 * @method     ChildComCourriersLiensArchiveQuery leftJoin($relation) Adds a LEFT JOIN clause to the query
 * @method     ChildComCourriersLiensArchiveQuery rightJoin($relation) Adds a RIGHT JOIN clause to the query
 * @method     ChildComCourriersLiensArchiveQuery innerJoin($relation) Adds a INNER JOIN clause to the query
 *
 * @method     ChildComCourriersLiensArchiveQuery leftJoinWith($relation) Adds a LEFT JOIN clause and with to the query
 * @method     ChildComCourriersLiensArchiveQuery rightJoinWith($relation) Adds a RIGHT JOIN clause and with to the query
 * @method     ChildComCourriersLiensArchiveQuery innerJoinWith($relation) Adds a INNER JOIN clause and with to the query
 *
 * @method     ChildComCourriersLiensArchive findOne(ConnectionInterface $con = null) Return the first ChildComCourriersLiensArchive matching the query
 * @method     ChildComCourriersLiensArchive findOneOrCreate(ConnectionInterface $con = null) Return the first ChildComCourriersLiensArchive matching the query, or a new ChildComCourriersLiensArchive object populated from the query conditions when no match is found
 *
 * @method     ChildComCourriersLiensArchive findOneByIdCourrierLien(string $id_courrier_lien) Return the first ChildComCourriersLiensArchive filtered by the id_courrier_lien column
 * @method     ChildComCourriersLiensArchive findOneByIdMembre(string $id_membre) Return the first ChildComCourriersLiensArchive filtered by the id_membre column
 * @method     ChildComCourriersLiensArchive findOneByIdIndividu(string $id_individu) Return the first ChildComCourriersLiensArchive filtered by the id_individu column
 * @method     ChildComCourriersLiensArchive findOneByIdCourrier(string $id_courrier) Return the first ChildComCourriersLiensArchive filtered by the id_courrier column
 * @method     ChildComCourriersLiensArchive findOneByCanal(string $canal) Return the first ChildComCourriersLiensArchive filtered by the canal column
 * @method     ChildComCourriersLiensArchive findOneByDateEnvoi(string $date_envoi) Return the first ChildComCourriersLiensArchive filtered by the date_envoi column
 * @method     ChildComCourriersLiensArchive findOneByMessageParticulier(string $message_particulier) Return the first ChildComCourriersLiensArchive filtered by the message_particulier column
 * @method     ChildComCourriersLiensArchive findOneByStatut(string $statut) Return the first ChildComCourriersLiensArchive filtered by the statut column
 * @method     ChildComCourriersLiensArchive findOneByVariables(string $variables) Return the first ChildComCourriersLiensArchive filtered by the variables column
 * @method     ChildComCourriersLiensArchive findOneByCreatedAt(string $created_at) Return the first ChildComCourriersLiensArchive filtered by the created_at column
 * @method     ChildComCourriersLiensArchive findOneByUpdatedAt(string $updated_at) Return the first ChildComCourriersLiensArchive filtered by the updated_at column
 * @method     ChildComCourriersLiensArchive findOneByArchivedAt(string $archived_at) Return the first ChildComCourriersLiensArchive filtered by the archived_at column *

 * @method     ChildComCourriersLiensArchive requirePk($key, ConnectionInterface $con = null) Return the ChildComCourriersLiensArchive by primary key and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildComCourriersLiensArchive requireOne(ConnectionInterface $con = null) Return the first ChildComCourriersLiensArchive matching the query and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 *
 * @method     ChildComCourriersLiensArchive requireOneByIdCourrierLien(string $id_courrier_lien) Return the first ChildComCourriersLiensArchive filtered by the id_courrier_lien column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildComCourriersLiensArchive requireOneByIdMembre(string $id_membre) Return the first ChildComCourriersLiensArchive filtered by the id_membre column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildComCourriersLiensArchive requireOneByIdIndividu(string $id_individu) Return the first ChildComCourriersLiensArchive filtered by the id_individu column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildComCourriersLiensArchive requireOneByIdCourrier(string $id_courrier) Return the first ChildComCourriersLiensArchive filtered by the id_courrier column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildComCourriersLiensArchive requireOneByCanal(string $canal) Return the first ChildComCourriersLiensArchive filtered by the canal column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildComCourriersLiensArchive requireOneByDateEnvoi(string $date_envoi) Return the first ChildComCourriersLiensArchive filtered by the date_envoi column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildComCourriersLiensArchive requireOneByMessageParticulier(string $message_particulier) Return the first ChildComCourriersLiensArchive filtered by the message_particulier column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildComCourriersLiensArchive requireOneByStatut(string $statut) Return the first ChildComCourriersLiensArchive filtered by the statut column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildComCourriersLiensArchive requireOneByVariables(string $variables) Return the first ChildComCourriersLiensArchive filtered by the variables column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildComCourriersLiensArchive requireOneByCreatedAt(string $created_at) Return the first ChildComCourriersLiensArchive filtered by the created_at column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildComCourriersLiensArchive requireOneByUpdatedAt(string $updated_at) Return the first ChildComCourriersLiensArchive filtered by the updated_at column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildComCourriersLiensArchive requireOneByArchivedAt(string $archived_at) Return the first ChildComCourriersLiensArchive filtered by the archived_at column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 *
 * @method     ChildComCourriersLiensArchive[]|ObjectCollection find(ConnectionInterface $con = null) Return ChildComCourriersLiensArchive objects based on current ModelCriteria
 * @method     ChildComCourriersLiensArchive[]|ObjectCollection findByIdCourrierLien(string $id_courrier_lien) Return ChildComCourriersLiensArchive objects filtered by the id_courrier_lien column
 * @method     ChildComCourriersLiensArchive[]|ObjectCollection findByIdMembre(string $id_membre) Return ChildComCourriersLiensArchive objects filtered by the id_membre column
 * @method     ChildComCourriersLiensArchive[]|ObjectCollection findByIdIndividu(string $id_individu) Return ChildComCourriersLiensArchive objects filtered by the id_individu column
 * @method     ChildComCourriersLiensArchive[]|ObjectCollection findByIdCourrier(string $id_courrier) Return ChildComCourriersLiensArchive objects filtered by the id_courrier column
 * @method     ChildComCourriersLiensArchive[]|ObjectCollection findByCanal(string $canal) Return ChildComCourriersLiensArchive objects filtered by the canal column
 * @method     ChildComCourriersLiensArchive[]|ObjectCollection findByDateEnvoi(string $date_envoi) Return ChildComCourriersLiensArchive objects filtered by the date_envoi column
 * @method     ChildComCourriersLiensArchive[]|ObjectCollection findByMessageParticulier(string $message_particulier) Return ChildComCourriersLiensArchive objects filtered by the message_particulier column
 * @method     ChildComCourriersLiensArchive[]|ObjectCollection findByStatut(string $statut) Return ChildComCourriersLiensArchive objects filtered by the statut column
 * @method     ChildComCourriersLiensArchive[]|ObjectCollection findByVariables(string $variables) Return ChildComCourriersLiensArchive objects filtered by the variables column
 * @method     ChildComCourriersLiensArchive[]|ObjectCollection findByCreatedAt(string $created_at) Return ChildComCourriersLiensArchive objects filtered by the created_at column
 * @method     ChildComCourriersLiensArchive[]|ObjectCollection findByUpdatedAt(string $updated_at) Return ChildComCourriersLiensArchive objects filtered by the updated_at column
 * @method     ChildComCourriersLiensArchive[]|ObjectCollection findByArchivedAt(string $archived_at) Return ChildComCourriersLiensArchive objects filtered by the archived_at column
 * @method     ChildComCourriersLiensArchive[]|\Propel\Runtime\Util\PropelModelPager paginate($page = 1, $maxPerPage = 10, ConnectionInterface $con = null) Issue a SELECT query based on the current ModelCriteria and uses a page and a maximum number of results per page to compute an offset and a limit
 *
 */
abstract class ComCourriersLiensArchiveQuery extends ModelCriteria
{
    protected $entityNotFoundExceptionClass = '\\Propel\\Runtime\\Exception\\EntityNotFoundException';

    /**
     * Initializes internal state of \Base\ComCourriersLiensArchiveQuery object.
     *
     * @param     string $dbName The database name
     * @param     string $modelName The phpName of a model, e.g. 'Book'
     * @param     string $modelAlias The alias for the model in this query, e.g. 'b'
     */
    public function __construct($dbName = 'simplasso', $modelName = '\\ComCourriersLiensArchive', $modelAlias = null)
    {
        parent::__construct($dbName, $modelName, $modelAlias);
    }

    /**
     * Returns a new ChildComCourriersLiensArchiveQuery object.
     *
     * @param     string $modelAlias The alias of a model in the query
     * @param     Criteria $criteria Optional Criteria to build the query from
     *
     * @return ChildComCourriersLiensArchiveQuery
     */
    public static function create($modelAlias = null, Criteria $criteria = null)
    {
        if ($criteria instanceof ChildComCourriersLiensArchiveQuery) {
            return $criteria;
        }
        $query = new ChildComCourriersLiensArchiveQuery();
        if (null !== $modelAlias) {
            $query->setModelAlias($modelAlias);
        }
        if ($criteria instanceof Criteria) {
            $query->mergeWith($criteria);
        }

        return $query;
    }

    /**
     * Find object by primary key.
     * Propel uses the instance pool to skip the database if the object exists.
     * Go fast if the query is untouched.
     *
     * <code>
     * $obj  = $c->findPk(12, $con);
     * </code>
     *
     * @param mixed $key Primary key to use for the query
     * @param ConnectionInterface $con an optional connection object
     *
     * @return ChildComCourriersLiensArchive|array|mixed the result, formatted by the current formatter
     */
    public function findPk($key, ConnectionInterface $con = null)
    {
        if ($key === null) {
            return null;
        }

        if ($con === null) {
            $con = Propel::getServiceContainer()->getReadConnection(ComCourriersLiensArchiveTableMap::DATABASE_NAME);
        }

        $this->basePreSelect($con);

        if (
            $this->formatter || $this->modelAlias || $this->with || $this->select
            || $this->selectColumns || $this->asColumns || $this->selectModifiers
            || $this->map || $this->having || $this->joins
        ) {
            return $this->findPkComplex($key, $con);
        }

        if ((null !== ($obj = ComCourriersLiensArchiveTableMap::getInstanceFromPool(null === $key || is_scalar($key) || is_callable([$key, '__toString']) ? (string) $key : $key)))) {
            // the object is already in the instance pool
            return $obj;
        }

        return $this->findPkSimple($key, $con);
    }

    /**
     * Find object by primary key using raw SQL to go fast.
     * Bypass doSelect() and the object formatter by using generated code.
     *
     * @param     mixed $key Primary key to use for the query
     * @param     ConnectionInterface $con A connection object
     *
     * @throws \Propel\Runtime\Exception\PropelException
     *
     * @return ChildComCourriersLiensArchive A model object, or null if the key is not found
     */
    protected function findPkSimple($key, ConnectionInterface $con)
    {
        $sql = 'SELECT `id_courrier_lien`, `id_membre`, `id_individu`, `id_courrier`, `canal`, `date_envoi`, `message_particulier`, `statut`, `variables`, `created_at`, `updated_at`, `archived_at` FROM `com_courriers_liens_archive` WHERE `id_courrier_lien` = :p0';
        try {
            $stmt = $con->prepare($sql);
            $stmt->bindValue(':p0', $key, PDO::PARAM_INT);
            $stmt->execute();
        } catch (Exception $e) {
            Propel::log($e->getMessage(), Propel::LOG_ERR);
            throw new PropelException(sprintf('Unable to execute SELECT statement [%s]', $sql), 0, $e);
        }
        $obj = null;
        if ($row = $stmt->fetch(\PDO::FETCH_NUM)) {
            /** @var ChildComCourriersLiensArchive $obj */
            $obj = new ChildComCourriersLiensArchive();
            $obj->hydrate($row);
            ComCourriersLiensArchiveTableMap::addInstanceToPool($obj, null === $key || is_scalar($key) || is_callable([$key, '__toString']) ? (string) $key : $key);
        }
        $stmt->closeCursor();

        return $obj;
    }

    /**
     * Find object by primary key.
     *
     * @param     mixed $key Primary key to use for the query
     * @param     ConnectionInterface $con A connection object
     *
     * @return ChildComCourriersLiensArchive|array|mixed the result, formatted by the current formatter
     */
    protected function findPkComplex($key, ConnectionInterface $con)
    {
        // As the query uses a PK condition, no limit(1) is necessary.
        $criteria = $this->isKeepQuery() ? clone $this : $this;
        $dataFetcher = $criteria
            ->filterByPrimaryKey($key)
            ->doSelect($con);

        return $criteria->getFormatter()->init($criteria)->formatOne($dataFetcher);
    }

    /**
     * Find objects by primary key
     * <code>
     * $objs = $c->findPks(array(12, 56, 832), $con);
     * </code>
     * @param     array $keys Primary keys to use for the query
     * @param     ConnectionInterface $con an optional connection object
     *
     * @return ObjectCollection|array|mixed the list of results, formatted by the current formatter
     */
    public function findPks($keys, ConnectionInterface $con = null)
    {
        if (null === $con) {
            $con = Propel::getServiceContainer()->getReadConnection($this->getDbName());
        }
        $this->basePreSelect($con);
        $criteria = $this->isKeepQuery() ? clone $this : $this;
        $dataFetcher = $criteria
            ->filterByPrimaryKeys($keys)
            ->doSelect($con);

        return $criteria->getFormatter()->init($criteria)->format($dataFetcher);
    }

    /**
     * Filter the query by primary key
     *
     * @param     mixed $key Primary key to use for the query
     *
     * @return $this|ChildComCourriersLiensArchiveQuery The current query, for fluid interface
     */
    public function filterByPrimaryKey($key)
    {

        return $this->addUsingAlias(ComCourriersLiensArchiveTableMap::COL_ID_COURRIER_LIEN, $key, Criteria::EQUAL);
    }

    /**
     * Filter the query by a list of primary keys
     *
     * @param     array $keys The list of primary key to use for the query
     *
     * @return $this|ChildComCourriersLiensArchiveQuery The current query, for fluid interface
     */
    public function filterByPrimaryKeys($keys)
    {

        return $this->addUsingAlias(ComCourriersLiensArchiveTableMap::COL_ID_COURRIER_LIEN, $keys, Criteria::IN);
    }

    /**
     * Filter the query on the id_courrier_lien column
     *
     * Example usage:
     * <code>
     * $query->filterByIdCourrierLien(1234); // WHERE id_courrier_lien = 1234
     * $query->filterByIdCourrierLien(array(12, 34)); // WHERE id_courrier_lien IN (12, 34)
     * $query->filterByIdCourrierLien(array('min' => 12)); // WHERE id_courrier_lien > 12
     * </code>
     *
     * @param     mixed $idCourrierLien The value to use as filter.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildComCourriersLiensArchiveQuery The current query, for fluid interface
     */
    public function filterByIdCourrierLien($idCourrierLien = null, $comparison = null)
    {
        if (is_array($idCourrierLien)) {
            $useMinMax = false;
            if (isset($idCourrierLien['min'])) {
                $this->addUsingAlias(ComCourriersLiensArchiveTableMap::COL_ID_COURRIER_LIEN, $idCourrierLien['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($idCourrierLien['max'])) {
                $this->addUsingAlias(ComCourriersLiensArchiveTableMap::COL_ID_COURRIER_LIEN, $idCourrierLien['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(ComCourriersLiensArchiveTableMap::COL_ID_COURRIER_LIEN, $idCourrierLien, $comparison);
    }

    /**
     * Filter the query on the id_membre column
     *
     * Example usage:
     * <code>
     * $query->filterByIdMembre(1234); // WHERE id_membre = 1234
     * $query->filterByIdMembre(array(12, 34)); // WHERE id_membre IN (12, 34)
     * $query->filterByIdMembre(array('min' => 12)); // WHERE id_membre > 12
     * </code>
     *
     * @param     mixed $idMembre The value to use as filter.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildComCourriersLiensArchiveQuery The current query, for fluid interface
     */
    public function filterByIdMembre($idMembre = null, $comparison = null)
    {
        if (is_array($idMembre)) {
            $useMinMax = false;
            if (isset($idMembre['min'])) {
                $this->addUsingAlias(ComCourriersLiensArchiveTableMap::COL_ID_MEMBRE, $idMembre['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($idMembre['max'])) {
                $this->addUsingAlias(ComCourriersLiensArchiveTableMap::COL_ID_MEMBRE, $idMembre['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(ComCourriersLiensArchiveTableMap::COL_ID_MEMBRE, $idMembre, $comparison);
    }

    /**
     * Filter the query on the id_individu column
     *
     * Example usage:
     * <code>
     * $query->filterByIdIndividu(1234); // WHERE id_individu = 1234
     * $query->filterByIdIndividu(array(12, 34)); // WHERE id_individu IN (12, 34)
     * $query->filterByIdIndividu(array('min' => 12)); // WHERE id_individu > 12
     * </code>
     *
     * @param     mixed $idIndividu The value to use as filter.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildComCourriersLiensArchiveQuery The current query, for fluid interface
     */
    public function filterByIdIndividu($idIndividu = null, $comparison = null)
    {
        if (is_array($idIndividu)) {
            $useMinMax = false;
            if (isset($idIndividu['min'])) {
                $this->addUsingAlias(ComCourriersLiensArchiveTableMap::COL_ID_INDIVIDU, $idIndividu['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($idIndividu['max'])) {
                $this->addUsingAlias(ComCourriersLiensArchiveTableMap::COL_ID_INDIVIDU, $idIndividu['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(ComCourriersLiensArchiveTableMap::COL_ID_INDIVIDU, $idIndividu, $comparison);
    }

    /**
     * Filter the query on the id_courrier column
     *
     * Example usage:
     * <code>
     * $query->filterByIdCourrier(1234); // WHERE id_courrier = 1234
     * $query->filterByIdCourrier(array(12, 34)); // WHERE id_courrier IN (12, 34)
     * $query->filterByIdCourrier(array('min' => 12)); // WHERE id_courrier > 12
     * </code>
     *
     * @param     mixed $idCourrier The value to use as filter.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildComCourriersLiensArchiveQuery The current query, for fluid interface
     */
    public function filterByIdCourrier($idCourrier = null, $comparison = null)
    {
        if (is_array($idCourrier)) {
            $useMinMax = false;
            if (isset($idCourrier['min'])) {
                $this->addUsingAlias(ComCourriersLiensArchiveTableMap::COL_ID_COURRIER, $idCourrier['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($idCourrier['max'])) {
                $this->addUsingAlias(ComCourriersLiensArchiveTableMap::COL_ID_COURRIER, $idCourrier['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(ComCourriersLiensArchiveTableMap::COL_ID_COURRIER, $idCourrier, $comparison);
    }

    /**
     * Filter the query on the canal column
     *
     * Example usage:
     * <code>
     * $query->filterByCanal('fooValue');   // WHERE canal = 'fooValue'
     * $query->filterByCanal('%fooValue%', Criteria::LIKE); // WHERE canal LIKE '%fooValue%'
     * </code>
     *
     * @param     string $canal The value to use as filter.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildComCourriersLiensArchiveQuery The current query, for fluid interface
     */
    public function filterByCanal($canal = null, $comparison = null)
    {
        if (null === $comparison) {
            if (is_array($canal)) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(ComCourriersLiensArchiveTableMap::COL_CANAL, $canal, $comparison);
    }

    /**
     * Filter the query on the date_envoi column
     *
     * Example usage:
     * <code>
     * $query->filterByDateEnvoi('2011-03-14'); // WHERE date_envoi = '2011-03-14'
     * $query->filterByDateEnvoi('now'); // WHERE date_envoi = '2011-03-14'
     * $query->filterByDateEnvoi(array('max' => 'yesterday')); // WHERE date_envoi > '2011-03-13'
     * </code>
     *
     * @param     mixed $dateEnvoi The value to use as filter.
     *              Values can be integers (unix timestamps), DateTime objects, or strings.
     *              Empty strings are treated as NULL.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildComCourriersLiensArchiveQuery The current query, for fluid interface
     */
    public function filterByDateEnvoi($dateEnvoi = null, $comparison = null)
    {
        if (is_array($dateEnvoi)) {
            $useMinMax = false;
            if (isset($dateEnvoi['min'])) {
                $this->addUsingAlias(ComCourriersLiensArchiveTableMap::COL_DATE_ENVOI, $dateEnvoi['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($dateEnvoi['max'])) {
                $this->addUsingAlias(ComCourriersLiensArchiveTableMap::COL_DATE_ENVOI, $dateEnvoi['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(ComCourriersLiensArchiveTableMap::COL_DATE_ENVOI, $dateEnvoi, $comparison);
    }

    /**
     * Filter the query on the message_particulier column
     *
     * Example usage:
     * <code>
     * $query->filterByMessageParticulier('fooValue');   // WHERE message_particulier = 'fooValue'
     * $query->filterByMessageParticulier('%fooValue%', Criteria::LIKE); // WHERE message_particulier LIKE '%fooValue%'
     * </code>
     *
     * @param     string $messageParticulier The value to use as filter.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildComCourriersLiensArchiveQuery The current query, for fluid interface
     */
    public function filterByMessageParticulier($messageParticulier = null, $comparison = null)
    {
        if (null === $comparison) {
            if (is_array($messageParticulier)) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(ComCourriersLiensArchiveTableMap::COL_MESSAGE_PARTICULIER, $messageParticulier, $comparison);
    }

    /**
     * Filter the query on the statut column
     *
     * Example usage:
     * <code>
     * $query->filterByStatut('fooValue');   // WHERE statut = 'fooValue'
     * $query->filterByStatut('%fooValue%', Criteria::LIKE); // WHERE statut LIKE '%fooValue%'
     * </code>
     *
     * @param     string $statut The value to use as filter.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildComCourriersLiensArchiveQuery The current query, for fluid interface
     */
    public function filterByStatut($statut = null, $comparison = null)
    {
        if (null === $comparison) {
            if (is_array($statut)) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(ComCourriersLiensArchiveTableMap::COL_STATUT, $statut, $comparison);
    }

    /**
     * Filter the query on the variables column
     *
     * Example usage:
     * <code>
     * $query->filterByVariables('fooValue');   // WHERE variables = 'fooValue'
     * $query->filterByVariables('%fooValue%', Criteria::LIKE); // WHERE variables LIKE '%fooValue%'
     * </code>
     *
     * @param     string $variables The value to use as filter.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildComCourriersLiensArchiveQuery The current query, for fluid interface
     */
    public function filterByVariables($variables = null, $comparison = null)
    {
        if (null === $comparison) {
            if (is_array($variables)) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(ComCourriersLiensArchiveTableMap::COL_VARIABLES, $variables, $comparison);
    }

    /**
     * Filter the query on the created_at column
     *
     * Example usage:
     * <code>
     * $query->filterByCreatedAt('2011-03-14'); // WHERE created_at = '2011-03-14'
     * $query->filterByCreatedAt('now'); // WHERE created_at = '2011-03-14'
     * $query->filterByCreatedAt(array('max' => 'yesterday')); // WHERE created_at > '2011-03-13'
     * </code>
     *
     * @param     mixed $createdAt The value to use as filter.
     *              Values can be integers (unix timestamps), DateTime objects, or strings.
     *              Empty strings are treated as NULL.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildComCourriersLiensArchiveQuery The current query, for fluid interface
     */
    public function filterByCreatedAt($createdAt = null, $comparison = null)
    {
        if (is_array($createdAt)) {
            $useMinMax = false;
            if (isset($createdAt['min'])) {
                $this->addUsingAlias(ComCourriersLiensArchiveTableMap::COL_CREATED_AT, $createdAt['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($createdAt['max'])) {
                $this->addUsingAlias(ComCourriersLiensArchiveTableMap::COL_CREATED_AT, $createdAt['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(ComCourriersLiensArchiveTableMap::COL_CREATED_AT, $createdAt, $comparison);
    }

    /**
     * Filter the query on the updated_at column
     *
     * Example usage:
     * <code>
     * $query->filterByUpdatedAt('2011-03-14'); // WHERE updated_at = '2011-03-14'
     * $query->filterByUpdatedAt('now'); // WHERE updated_at = '2011-03-14'
     * $query->filterByUpdatedAt(array('max' => 'yesterday')); // WHERE updated_at > '2011-03-13'
     * </code>
     *
     * @param     mixed $updatedAt The value to use as filter.
     *              Values can be integers (unix timestamps), DateTime objects, or strings.
     *              Empty strings are treated as NULL.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildComCourriersLiensArchiveQuery The current query, for fluid interface
     */
    public function filterByUpdatedAt($updatedAt = null, $comparison = null)
    {
        if (is_array($updatedAt)) {
            $useMinMax = false;
            if (isset($updatedAt['min'])) {
                $this->addUsingAlias(ComCourriersLiensArchiveTableMap::COL_UPDATED_AT, $updatedAt['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($updatedAt['max'])) {
                $this->addUsingAlias(ComCourriersLiensArchiveTableMap::COL_UPDATED_AT, $updatedAt['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(ComCourriersLiensArchiveTableMap::COL_UPDATED_AT, $updatedAt, $comparison);
    }

    /**
     * Filter the query on the archived_at column
     *
     * Example usage:
     * <code>
     * $query->filterByArchivedAt('2011-03-14'); // WHERE archived_at = '2011-03-14'
     * $query->filterByArchivedAt('now'); // WHERE archived_at = '2011-03-14'
     * $query->filterByArchivedAt(array('max' => 'yesterday')); // WHERE archived_at > '2011-03-13'
     * </code>
     *
     * @param     mixed $archivedAt The value to use as filter.
     *              Values can be integers (unix timestamps), DateTime objects, or strings.
     *              Empty strings are treated as NULL.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildComCourriersLiensArchiveQuery The current query, for fluid interface
     */
    public function filterByArchivedAt($archivedAt = null, $comparison = null)
    {
        if (is_array($archivedAt)) {
            $useMinMax = false;
            if (isset($archivedAt['min'])) {
                $this->addUsingAlias(ComCourriersLiensArchiveTableMap::COL_ARCHIVED_AT, $archivedAt['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($archivedAt['max'])) {
                $this->addUsingAlias(ComCourriersLiensArchiveTableMap::COL_ARCHIVED_AT, $archivedAt['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(ComCourriersLiensArchiveTableMap::COL_ARCHIVED_AT, $archivedAt, $comparison);
    }

    /**
     * Exclude object from result
     *
     * @param   ChildComCourriersLiensArchive $comCourriersLiensArchive Object to remove from the list of results
     *
     * @return $this|ChildComCourriersLiensArchiveQuery The current query, for fluid interface
     */
    public function prune($comCourriersLiensArchive = null)
    {
        if ($comCourriersLiensArchive) {
            $this->addUsingAlias(ComCourriersLiensArchiveTableMap::COL_ID_COURRIER_LIEN, $comCourriersLiensArchive->getIdCourrierLien(), Criteria::NOT_EQUAL);
        }

        return $this;
    }

    /**
     * Deletes all rows from the com_courriers_liens_archive table.
     *
     * @param ConnectionInterface $con the connection to use
     * @return int The number of affected rows (if supported by underlying database driver).
     */
    public function doDeleteAll(ConnectionInterface $con = null)
    {
        if (null === $con) {
            $con = Propel::getServiceContainer()->getWriteConnection(ComCourriersLiensArchiveTableMap::DATABASE_NAME);
        }

        // use transaction because $criteria could contain info
        // for more than one table or we could emulating ON DELETE CASCADE, etc.
        return $con->transaction(function () use ($con) {
            $affectedRows = 0; // initialize var to track total num of affected rows
            $affectedRows += parent::doDeleteAll($con);
            // Because this db requires some delete cascade/set null emulation, we have to
            // clear the cached instance *after* the emulation has happened (since
            // instances get re-added by the select statement contained therein).
            ComCourriersLiensArchiveTableMap::clearInstancePool();
            ComCourriersLiensArchiveTableMap::clearRelatedInstancePool();

            return $affectedRows;
        });
    }

    /**
     * Performs a DELETE on the database based on the current ModelCriteria
     *
     * @param ConnectionInterface $con the connection to use
     * @return int             The number of affected rows (if supported by underlying database driver).  This includes CASCADE-related rows
     *                         if supported by native driver or if emulated using Propel.
     * @throws PropelException Any exceptions caught during processing will be
     *                         rethrown wrapped into a PropelException.
     */
    public function delete(ConnectionInterface $con = null)
    {
        if (null === $con) {
            $con = Propel::getServiceContainer()->getWriteConnection(ComCourriersLiensArchiveTableMap::DATABASE_NAME);
        }

        $criteria = $this;

        // Set the correct dbName
        $criteria->setDbName(ComCourriersLiensArchiveTableMap::DATABASE_NAME);

        // use transaction because $criteria could contain info
        // for more than one table or we could emulating ON DELETE CASCADE, etc.
        return $con->transaction(function () use ($con, $criteria) {
            $affectedRows = 0; // initialize var to track total num of affected rows

            ComCourriersLiensArchiveTableMap::removeInstanceFromPool($criteria);

            $affectedRows += ModelCriteria::delete($con);
            ComCourriersLiensArchiveTableMap::clearRelatedInstancePool();

            return $affectedRows;
        });
    }

} // ComCourriersLiensArchiveQuery
