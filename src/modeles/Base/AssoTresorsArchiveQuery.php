<?php

namespace Base;

use \AssoTresorsArchive as ChildAssoTresorsArchive;
use \AssoTresorsArchiveQuery as ChildAssoTresorsArchiveQuery;
use \Exception;
use \PDO;
use Map\AssoTresorsArchiveTableMap;
use Propel\Runtime\Propel;
use Propel\Runtime\ActiveQuery\Criteria;
use Propel\Runtime\ActiveQuery\ModelCriteria;
use Propel\Runtime\Collection\ObjectCollection;
use Propel\Runtime\Connection\ConnectionInterface;
use Propel\Runtime\Exception\PropelException;

/**
 * Base class that represents a query for the 'asso_tresors_archive' table.
 *
 *
 *
 * @method     ChildAssoTresorsArchiveQuery orderByIdTresor($order = Criteria::ASC) Order by the id_tresor column
 * @method     ChildAssoTresorsArchiveQuery orderByIdEntite($order = Criteria::ASC) Order by the id_entite column
 * @method     ChildAssoTresorsArchiveQuery orderByNom($order = Criteria::ASC) Order by the nom column
 * @method     ChildAssoTresorsArchiveQuery orderByNomcourt($order = Criteria::ASC) Order by the nomcourt column
 * @method     ChildAssoTresorsArchiveQuery orderByIban($order = Criteria::ASC) Order by the iban column
 * @method     ChildAssoTresorsArchiveQuery orderByBic($order = Criteria::ASC) Order by the bic column
 * @method     ChildAssoTresorsArchiveQuery orderByIdCompterb($order = Criteria::ASC) Order by the id_compterb column
 * @method     ChildAssoTresorsArchiveQuery orderByIdCompte($order = Criteria::ASC) Order by the id_compte column
 * @method     ChildAssoTresorsArchiveQuery orderByRemise($order = Criteria::ASC) Order by the remise column
 * @method     ChildAssoTresorsArchiveQuery orderByObservation($order = Criteria::ASC) Order by the observation column
 * @method     ChildAssoTresorsArchiveQuery orderByCreatedAt($order = Criteria::ASC) Order by the created_at column
 * @method     ChildAssoTresorsArchiveQuery orderByUpdatedAt($order = Criteria::ASC) Order by the updated_at column
 * @method     ChildAssoTresorsArchiveQuery orderByArchivedAt($order = Criteria::ASC) Order by the archived_at column
 *
 * @method     ChildAssoTresorsArchiveQuery groupByIdTresor() Group by the id_tresor column
 * @method     ChildAssoTresorsArchiveQuery groupByIdEntite() Group by the id_entite column
 * @method     ChildAssoTresorsArchiveQuery groupByNom() Group by the nom column
 * @method     ChildAssoTresorsArchiveQuery groupByNomcourt() Group by the nomcourt column
 * @method     ChildAssoTresorsArchiveQuery groupByIban() Group by the iban column
 * @method     ChildAssoTresorsArchiveQuery groupByBic() Group by the bic column
 * @method     ChildAssoTresorsArchiveQuery groupByIdCompterb() Group by the id_compterb column
 * @method     ChildAssoTresorsArchiveQuery groupByIdCompte() Group by the id_compte column
 * @method     ChildAssoTresorsArchiveQuery groupByRemise() Group by the remise column
 * @method     ChildAssoTresorsArchiveQuery groupByObservation() Group by the observation column
 * @method     ChildAssoTresorsArchiveQuery groupByCreatedAt() Group by the created_at column
 * @method     ChildAssoTresorsArchiveQuery groupByUpdatedAt() Group by the updated_at column
 * @method     ChildAssoTresorsArchiveQuery groupByArchivedAt() Group by the archived_at column
 *
 * @method     ChildAssoTresorsArchiveQuery leftJoin($relation) Adds a LEFT JOIN clause to the query
 * @method     ChildAssoTresorsArchiveQuery rightJoin($relation) Adds a RIGHT JOIN clause to the query
 * @method     ChildAssoTresorsArchiveQuery innerJoin($relation) Adds a INNER JOIN clause to the query
 *
 * @method     ChildAssoTresorsArchiveQuery leftJoinWith($relation) Adds a LEFT JOIN clause and with to the query
 * @method     ChildAssoTresorsArchiveQuery rightJoinWith($relation) Adds a RIGHT JOIN clause and with to the query
 * @method     ChildAssoTresorsArchiveQuery innerJoinWith($relation) Adds a INNER JOIN clause and with to the query
 *
 * @method     ChildAssoTresorsArchive findOne(ConnectionInterface $con = null) Return the first ChildAssoTresorsArchive matching the query
 * @method     ChildAssoTresorsArchive findOneOrCreate(ConnectionInterface $con = null) Return the first ChildAssoTresorsArchive matching the query, or a new ChildAssoTresorsArchive object populated from the query conditions when no match is found
 *
 * @method     ChildAssoTresorsArchive findOneByIdTresor(string $id_tresor) Return the first ChildAssoTresorsArchive filtered by the id_tresor column
 * @method     ChildAssoTresorsArchive findOneByIdEntite(string $id_entite) Return the first ChildAssoTresorsArchive filtered by the id_entite column
 * @method     ChildAssoTresorsArchive findOneByNom(string $nom) Return the first ChildAssoTresorsArchive filtered by the nom column
 * @method     ChildAssoTresorsArchive findOneByNomcourt(string $nomcourt) Return the first ChildAssoTresorsArchive filtered by the nomcourt column
 * @method     ChildAssoTresorsArchive findOneByIban(string $iban) Return the first ChildAssoTresorsArchive filtered by the iban column
 * @method     ChildAssoTresorsArchive findOneByBic(string $bic) Return the first ChildAssoTresorsArchive filtered by the bic column
 * @method     ChildAssoTresorsArchive findOneByIdCompterb(string $id_compterb) Return the first ChildAssoTresorsArchive filtered by the id_compterb column
 * @method     ChildAssoTresorsArchive findOneByIdCompte(string $id_compte) Return the first ChildAssoTresorsArchive filtered by the id_compte column
 * @method     ChildAssoTresorsArchive findOneByRemise(string $remise) Return the first ChildAssoTresorsArchive filtered by the remise column
 * @method     ChildAssoTresorsArchive findOneByObservation(string $observation) Return the first ChildAssoTresorsArchive filtered by the observation column
 * @method     ChildAssoTresorsArchive findOneByCreatedAt(string $created_at) Return the first ChildAssoTresorsArchive filtered by the created_at column
 * @method     ChildAssoTresorsArchive findOneByUpdatedAt(string $updated_at) Return the first ChildAssoTresorsArchive filtered by the updated_at column
 * @method     ChildAssoTresorsArchive findOneByArchivedAt(string $archived_at) Return the first ChildAssoTresorsArchive filtered by the archived_at column *

 * @method     ChildAssoTresorsArchive requirePk($key, ConnectionInterface $con = null) Return the ChildAssoTresorsArchive by primary key and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildAssoTresorsArchive requireOne(ConnectionInterface $con = null) Return the first ChildAssoTresorsArchive matching the query and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 *
 * @method     ChildAssoTresorsArchive requireOneByIdTresor(string $id_tresor) Return the first ChildAssoTresorsArchive filtered by the id_tresor column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildAssoTresorsArchive requireOneByIdEntite(string $id_entite) Return the first ChildAssoTresorsArchive filtered by the id_entite column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildAssoTresorsArchive requireOneByNom(string $nom) Return the first ChildAssoTresorsArchive filtered by the nom column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildAssoTresorsArchive requireOneByNomcourt(string $nomcourt) Return the first ChildAssoTresorsArchive filtered by the nomcourt column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildAssoTresorsArchive requireOneByIban(string $iban) Return the first ChildAssoTresorsArchive filtered by the iban column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildAssoTresorsArchive requireOneByBic(string $bic) Return the first ChildAssoTresorsArchive filtered by the bic column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildAssoTresorsArchive requireOneByIdCompterb(string $id_compterb) Return the first ChildAssoTresorsArchive filtered by the id_compterb column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildAssoTresorsArchive requireOneByIdCompte(string $id_compte) Return the first ChildAssoTresorsArchive filtered by the id_compte column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildAssoTresorsArchive requireOneByRemise(string $remise) Return the first ChildAssoTresorsArchive filtered by the remise column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildAssoTresorsArchive requireOneByObservation(string $observation) Return the first ChildAssoTresorsArchive filtered by the observation column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildAssoTresorsArchive requireOneByCreatedAt(string $created_at) Return the first ChildAssoTresorsArchive filtered by the created_at column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildAssoTresorsArchive requireOneByUpdatedAt(string $updated_at) Return the first ChildAssoTresorsArchive filtered by the updated_at column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildAssoTresorsArchive requireOneByArchivedAt(string $archived_at) Return the first ChildAssoTresorsArchive filtered by the archived_at column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 *
 * @method     ChildAssoTresorsArchive[]|ObjectCollection find(ConnectionInterface $con = null) Return ChildAssoTresorsArchive objects based on current ModelCriteria
 * @method     ChildAssoTresorsArchive[]|ObjectCollection findByIdTresor(string $id_tresor) Return ChildAssoTresorsArchive objects filtered by the id_tresor column
 * @method     ChildAssoTresorsArchive[]|ObjectCollection findByIdEntite(string $id_entite) Return ChildAssoTresorsArchive objects filtered by the id_entite column
 * @method     ChildAssoTresorsArchive[]|ObjectCollection findByNom(string $nom) Return ChildAssoTresorsArchive objects filtered by the nom column
 * @method     ChildAssoTresorsArchive[]|ObjectCollection findByNomcourt(string $nomcourt) Return ChildAssoTresorsArchive objects filtered by the nomcourt column
 * @method     ChildAssoTresorsArchive[]|ObjectCollection findByIban(string $iban) Return ChildAssoTresorsArchive objects filtered by the iban column
 * @method     ChildAssoTresorsArchive[]|ObjectCollection findByBic(string $bic) Return ChildAssoTresorsArchive objects filtered by the bic column
 * @method     ChildAssoTresorsArchive[]|ObjectCollection findByIdCompterb(string $id_compterb) Return ChildAssoTresorsArchive objects filtered by the id_compterb column
 * @method     ChildAssoTresorsArchive[]|ObjectCollection findByIdCompte(string $id_compte) Return ChildAssoTresorsArchive objects filtered by the id_compte column
 * @method     ChildAssoTresorsArchive[]|ObjectCollection findByRemise(string $remise) Return ChildAssoTresorsArchive objects filtered by the remise column
 * @method     ChildAssoTresorsArchive[]|ObjectCollection findByObservation(string $observation) Return ChildAssoTresorsArchive objects filtered by the observation column
 * @method     ChildAssoTresorsArchive[]|ObjectCollection findByCreatedAt(string $created_at) Return ChildAssoTresorsArchive objects filtered by the created_at column
 * @method     ChildAssoTresorsArchive[]|ObjectCollection findByUpdatedAt(string $updated_at) Return ChildAssoTresorsArchive objects filtered by the updated_at column
 * @method     ChildAssoTresorsArchive[]|ObjectCollection findByArchivedAt(string $archived_at) Return ChildAssoTresorsArchive objects filtered by the archived_at column
 * @method     ChildAssoTresorsArchive[]|\Propel\Runtime\Util\PropelModelPager paginate($page = 1, $maxPerPage = 10, ConnectionInterface $con = null) Issue a SELECT query based on the current ModelCriteria and uses a page and a maximum number of results per page to compute an offset and a limit
 *
 */
abstract class AssoTresorsArchiveQuery extends ModelCriteria
{
    protected $entityNotFoundExceptionClass = '\\Propel\\Runtime\\Exception\\EntityNotFoundException';

    /**
     * Initializes internal state of \Base\AssoTresorsArchiveQuery object.
     *
     * @param     string $dbName The database name
     * @param     string $modelName The phpName of a model, e.g. 'Book'
     * @param     string $modelAlias The alias for the model in this query, e.g. 'b'
     */
    public function __construct($dbName = 'simplasso', $modelName = '\\AssoTresorsArchive', $modelAlias = null)
    {
        parent::__construct($dbName, $modelName, $modelAlias);
    }

    /**
     * Returns a new ChildAssoTresorsArchiveQuery object.
     *
     * @param     string $modelAlias The alias of a model in the query
     * @param     Criteria $criteria Optional Criteria to build the query from
     *
     * @return ChildAssoTresorsArchiveQuery
     */
    public static function create($modelAlias = null, Criteria $criteria = null)
    {
        if ($criteria instanceof ChildAssoTresorsArchiveQuery) {
            return $criteria;
        }
        $query = new ChildAssoTresorsArchiveQuery();
        if (null !== $modelAlias) {
            $query->setModelAlias($modelAlias);
        }
        if ($criteria instanceof Criteria) {
            $query->mergeWith($criteria);
        }

        return $query;
    }

    /**
     * Find object by primary key.
     * Propel uses the instance pool to skip the database if the object exists.
     * Go fast if the query is untouched.
     *
     * <code>
     * $obj  = $c->findPk(12, $con);
     * </code>
     *
     * @param mixed $key Primary key to use for the query
     * @param ConnectionInterface $con an optional connection object
     *
     * @return ChildAssoTresorsArchive|array|mixed the result, formatted by the current formatter
     */
    public function findPk($key, ConnectionInterface $con = null)
    {
        if ($key === null) {
            return null;
        }

        if ($con === null) {
            $con = Propel::getServiceContainer()->getReadConnection(AssoTresorsArchiveTableMap::DATABASE_NAME);
        }

        $this->basePreSelect($con);

        if (
            $this->formatter || $this->modelAlias || $this->with || $this->select
            || $this->selectColumns || $this->asColumns || $this->selectModifiers
            || $this->map || $this->having || $this->joins
        ) {
            return $this->findPkComplex($key, $con);
        }

        if ((null !== ($obj = AssoTresorsArchiveTableMap::getInstanceFromPool(null === $key || is_scalar($key) || is_callable([$key, '__toString']) ? (string) $key : $key)))) {
            // the object is already in the instance pool
            return $obj;
        }

        return $this->findPkSimple($key, $con);
    }

    /**
     * Find object by primary key using raw SQL to go fast.
     * Bypass doSelect() and the object formatter by using generated code.
     *
     * @param     mixed $key Primary key to use for the query
     * @param     ConnectionInterface $con A connection object
     *
     * @throws \Propel\Runtime\Exception\PropelException
     *
     * @return ChildAssoTresorsArchive A model object, or null if the key is not found
     */
    protected function findPkSimple($key, ConnectionInterface $con)
    {
        $sql = 'SELECT `id_tresor`, `id_entite`, `nom`, `nomcourt`, `iban`, `bic`, `id_compterb`, `id_compte`, `remise`, `observation`, `created_at`, `updated_at`, `archived_at` FROM `asso_tresors_archive` WHERE `id_tresor` = :p0';
        try {
            $stmt = $con->prepare($sql);
            $stmt->bindValue(':p0', $key, PDO::PARAM_INT);
            $stmt->execute();
        } catch (Exception $e) {
            Propel::log($e->getMessage(), Propel::LOG_ERR);
            throw new PropelException(sprintf('Unable to execute SELECT statement [%s]', $sql), 0, $e);
        }
        $obj = null;
        if ($row = $stmt->fetch(\PDO::FETCH_NUM)) {
            /** @var ChildAssoTresorsArchive $obj */
            $obj = new ChildAssoTresorsArchive();
            $obj->hydrate($row);
            AssoTresorsArchiveTableMap::addInstanceToPool($obj, null === $key || is_scalar($key) || is_callable([$key, '__toString']) ? (string) $key : $key);
        }
        $stmt->closeCursor();

        return $obj;
    }

    /**
     * Find object by primary key.
     *
     * @param     mixed $key Primary key to use for the query
     * @param     ConnectionInterface $con A connection object
     *
     * @return ChildAssoTresorsArchive|array|mixed the result, formatted by the current formatter
     */
    protected function findPkComplex($key, ConnectionInterface $con)
    {
        // As the query uses a PK condition, no limit(1) is necessary.
        $criteria = $this->isKeepQuery() ? clone $this : $this;
        $dataFetcher = $criteria
            ->filterByPrimaryKey($key)
            ->doSelect($con);

        return $criteria->getFormatter()->init($criteria)->formatOne($dataFetcher);
    }

    /**
     * Find objects by primary key
     * <code>
     * $objs = $c->findPks(array(12, 56, 832), $con);
     * </code>
     * @param     array $keys Primary keys to use for the query
     * @param     ConnectionInterface $con an optional connection object
     *
     * @return ObjectCollection|array|mixed the list of results, formatted by the current formatter
     */
    public function findPks($keys, ConnectionInterface $con = null)
    {
        if (null === $con) {
            $con = Propel::getServiceContainer()->getReadConnection($this->getDbName());
        }
        $this->basePreSelect($con);
        $criteria = $this->isKeepQuery() ? clone $this : $this;
        $dataFetcher = $criteria
            ->filterByPrimaryKeys($keys)
            ->doSelect($con);

        return $criteria->getFormatter()->init($criteria)->format($dataFetcher);
    }

    /**
     * Filter the query by primary key
     *
     * @param     mixed $key Primary key to use for the query
     *
     * @return $this|ChildAssoTresorsArchiveQuery The current query, for fluid interface
     */
    public function filterByPrimaryKey($key)
    {

        return $this->addUsingAlias(AssoTresorsArchiveTableMap::COL_ID_TRESOR, $key, Criteria::EQUAL);
    }

    /**
     * Filter the query by a list of primary keys
     *
     * @param     array $keys The list of primary key to use for the query
     *
     * @return $this|ChildAssoTresorsArchiveQuery The current query, for fluid interface
     */
    public function filterByPrimaryKeys($keys)
    {

        return $this->addUsingAlias(AssoTresorsArchiveTableMap::COL_ID_TRESOR, $keys, Criteria::IN);
    }

    /**
     * Filter the query on the id_tresor column
     *
     * Example usage:
     * <code>
     * $query->filterByIdTresor(1234); // WHERE id_tresor = 1234
     * $query->filterByIdTresor(array(12, 34)); // WHERE id_tresor IN (12, 34)
     * $query->filterByIdTresor(array('min' => 12)); // WHERE id_tresor > 12
     * </code>
     *
     * @param     mixed $idTresor The value to use as filter.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildAssoTresorsArchiveQuery The current query, for fluid interface
     */
    public function filterByIdTresor($idTresor = null, $comparison = null)
    {
        if (is_array($idTresor)) {
            $useMinMax = false;
            if (isset($idTresor['min'])) {
                $this->addUsingAlias(AssoTresorsArchiveTableMap::COL_ID_TRESOR, $idTresor['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($idTresor['max'])) {
                $this->addUsingAlias(AssoTresorsArchiveTableMap::COL_ID_TRESOR, $idTresor['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(AssoTresorsArchiveTableMap::COL_ID_TRESOR, $idTresor, $comparison);
    }

    /**
     * Filter the query on the id_entite column
     *
     * Example usage:
     * <code>
     * $query->filterByIdEntite(1234); // WHERE id_entite = 1234
     * $query->filterByIdEntite(array(12, 34)); // WHERE id_entite IN (12, 34)
     * $query->filterByIdEntite(array('min' => 12)); // WHERE id_entite > 12
     * </code>
     *
     * @param     mixed $idEntite The value to use as filter.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildAssoTresorsArchiveQuery The current query, for fluid interface
     */
    public function filterByIdEntite($idEntite = null, $comparison = null)
    {
        if (is_array($idEntite)) {
            $useMinMax = false;
            if (isset($idEntite['min'])) {
                $this->addUsingAlias(AssoTresorsArchiveTableMap::COL_ID_ENTITE, $idEntite['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($idEntite['max'])) {
                $this->addUsingAlias(AssoTresorsArchiveTableMap::COL_ID_ENTITE, $idEntite['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(AssoTresorsArchiveTableMap::COL_ID_ENTITE, $idEntite, $comparison);
    }

    /**
     * Filter the query on the nom column
     *
     * Example usage:
     * <code>
     * $query->filterByNom('fooValue');   // WHERE nom = 'fooValue'
     * $query->filterByNom('%fooValue%', Criteria::LIKE); // WHERE nom LIKE '%fooValue%'
     * </code>
     *
     * @param     string $nom The value to use as filter.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildAssoTresorsArchiveQuery The current query, for fluid interface
     */
    public function filterByNom($nom = null, $comparison = null)
    {
        if (null === $comparison) {
            if (is_array($nom)) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(AssoTresorsArchiveTableMap::COL_NOM, $nom, $comparison);
    }

    /**
     * Filter the query on the nomcourt column
     *
     * Example usage:
     * <code>
     * $query->filterByNomcourt('fooValue');   // WHERE nomcourt = 'fooValue'
     * $query->filterByNomcourt('%fooValue%', Criteria::LIKE); // WHERE nomcourt LIKE '%fooValue%'
     * </code>
     *
     * @param     string $nomcourt The value to use as filter.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildAssoTresorsArchiveQuery The current query, for fluid interface
     */
    public function filterByNomcourt($nomcourt = null, $comparison = null)
    {
        if (null === $comparison) {
            if (is_array($nomcourt)) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(AssoTresorsArchiveTableMap::COL_NOMCOURT, $nomcourt, $comparison);
    }

    /**
     * Filter the query on the iban column
     *
     * Example usage:
     * <code>
     * $query->filterByIban('fooValue');   // WHERE iban = 'fooValue'
     * $query->filterByIban('%fooValue%', Criteria::LIKE); // WHERE iban LIKE '%fooValue%'
     * </code>
     *
     * @param     string $iban The value to use as filter.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildAssoTresorsArchiveQuery The current query, for fluid interface
     */
    public function filterByIban($iban = null, $comparison = null)
    {
        if (null === $comparison) {
            if (is_array($iban)) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(AssoTresorsArchiveTableMap::COL_IBAN, $iban, $comparison);
    }

    /**
     * Filter the query on the bic column
     *
     * Example usage:
     * <code>
     * $query->filterByBic('fooValue');   // WHERE bic = 'fooValue'
     * $query->filterByBic('%fooValue%', Criteria::LIKE); // WHERE bic LIKE '%fooValue%'
     * </code>
     *
     * @param     string $bic The value to use as filter.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildAssoTresorsArchiveQuery The current query, for fluid interface
     */
    public function filterByBic($bic = null, $comparison = null)
    {
        if (null === $comparison) {
            if (is_array($bic)) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(AssoTresorsArchiveTableMap::COL_BIC, $bic, $comparison);
    }

    /**
     * Filter the query on the id_compterb column
     *
     * Example usage:
     * <code>
     * $query->filterByIdCompterb(1234); // WHERE id_compterb = 1234
     * $query->filterByIdCompterb(array(12, 34)); // WHERE id_compterb IN (12, 34)
     * $query->filterByIdCompterb(array('min' => 12)); // WHERE id_compterb > 12
     * </code>
     *
     * @param     mixed $idCompterb The value to use as filter.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildAssoTresorsArchiveQuery The current query, for fluid interface
     */
    public function filterByIdCompterb($idCompterb = null, $comparison = null)
    {
        if (is_array($idCompterb)) {
            $useMinMax = false;
            if (isset($idCompterb['min'])) {
                $this->addUsingAlias(AssoTresorsArchiveTableMap::COL_ID_COMPTERB, $idCompterb['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($idCompterb['max'])) {
                $this->addUsingAlias(AssoTresorsArchiveTableMap::COL_ID_COMPTERB, $idCompterb['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(AssoTresorsArchiveTableMap::COL_ID_COMPTERB, $idCompterb, $comparison);
    }

    /**
     * Filter the query on the id_compte column
     *
     * Example usage:
     * <code>
     * $query->filterByIdCompte(1234); // WHERE id_compte = 1234
     * $query->filterByIdCompte(array(12, 34)); // WHERE id_compte IN (12, 34)
     * $query->filterByIdCompte(array('min' => 12)); // WHERE id_compte > 12
     * </code>
     *
     * @param     mixed $idCompte The value to use as filter.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildAssoTresorsArchiveQuery The current query, for fluid interface
     */
    public function filterByIdCompte($idCompte = null, $comparison = null)
    {
        if (is_array($idCompte)) {
            $useMinMax = false;
            if (isset($idCompte['min'])) {
                $this->addUsingAlias(AssoTresorsArchiveTableMap::COL_ID_COMPTE, $idCompte['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($idCompte['max'])) {
                $this->addUsingAlias(AssoTresorsArchiveTableMap::COL_ID_COMPTE, $idCompte['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(AssoTresorsArchiveTableMap::COL_ID_COMPTE, $idCompte, $comparison);
    }

    /**
     * Filter the query on the remise column
     *
     * Example usage:
     * <code>
     * $query->filterByRemise(1234); // WHERE remise = 1234
     * $query->filterByRemise(array(12, 34)); // WHERE remise IN (12, 34)
     * $query->filterByRemise(array('min' => 12)); // WHERE remise > 12
     * </code>
     *
     * @param     mixed $remise The value to use as filter.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildAssoTresorsArchiveQuery The current query, for fluid interface
     */
    public function filterByRemise($remise = null, $comparison = null)
    {
        if (is_array($remise)) {
            $useMinMax = false;
            if (isset($remise['min'])) {
                $this->addUsingAlias(AssoTresorsArchiveTableMap::COL_REMISE, $remise['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($remise['max'])) {
                $this->addUsingAlias(AssoTresorsArchiveTableMap::COL_REMISE, $remise['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(AssoTresorsArchiveTableMap::COL_REMISE, $remise, $comparison);
    }

    /**
     * Filter the query on the observation column
     *
     * Example usage:
     * <code>
     * $query->filterByObservation('fooValue');   // WHERE observation = 'fooValue'
     * $query->filterByObservation('%fooValue%', Criteria::LIKE); // WHERE observation LIKE '%fooValue%'
     * </code>
     *
     * @param     string $observation The value to use as filter.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildAssoTresorsArchiveQuery The current query, for fluid interface
     */
    public function filterByObservation($observation = null, $comparison = null)
    {
        if (null === $comparison) {
            if (is_array($observation)) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(AssoTresorsArchiveTableMap::COL_OBSERVATION, $observation, $comparison);
    }

    /**
     * Filter the query on the created_at column
     *
     * Example usage:
     * <code>
     * $query->filterByCreatedAt('2011-03-14'); // WHERE created_at = '2011-03-14'
     * $query->filterByCreatedAt('now'); // WHERE created_at = '2011-03-14'
     * $query->filterByCreatedAt(array('max' => 'yesterday')); // WHERE created_at > '2011-03-13'
     * </code>
     *
     * @param     mixed $createdAt The value to use as filter.
     *              Values can be integers (unix timestamps), DateTime objects, or strings.
     *              Empty strings are treated as NULL.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildAssoTresorsArchiveQuery The current query, for fluid interface
     */
    public function filterByCreatedAt($createdAt = null, $comparison = null)
    {
        if (is_array($createdAt)) {
            $useMinMax = false;
            if (isset($createdAt['min'])) {
                $this->addUsingAlias(AssoTresorsArchiveTableMap::COL_CREATED_AT, $createdAt['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($createdAt['max'])) {
                $this->addUsingAlias(AssoTresorsArchiveTableMap::COL_CREATED_AT, $createdAt['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(AssoTresorsArchiveTableMap::COL_CREATED_AT, $createdAt, $comparison);
    }

    /**
     * Filter the query on the updated_at column
     *
     * Example usage:
     * <code>
     * $query->filterByUpdatedAt('2011-03-14'); // WHERE updated_at = '2011-03-14'
     * $query->filterByUpdatedAt('now'); // WHERE updated_at = '2011-03-14'
     * $query->filterByUpdatedAt(array('max' => 'yesterday')); // WHERE updated_at > '2011-03-13'
     * </code>
     *
     * @param     mixed $updatedAt The value to use as filter.
     *              Values can be integers (unix timestamps), DateTime objects, or strings.
     *              Empty strings are treated as NULL.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildAssoTresorsArchiveQuery The current query, for fluid interface
     */
    public function filterByUpdatedAt($updatedAt = null, $comparison = null)
    {
        if (is_array($updatedAt)) {
            $useMinMax = false;
            if (isset($updatedAt['min'])) {
                $this->addUsingAlias(AssoTresorsArchiveTableMap::COL_UPDATED_AT, $updatedAt['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($updatedAt['max'])) {
                $this->addUsingAlias(AssoTresorsArchiveTableMap::COL_UPDATED_AT, $updatedAt['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(AssoTresorsArchiveTableMap::COL_UPDATED_AT, $updatedAt, $comparison);
    }

    /**
     * Filter the query on the archived_at column
     *
     * Example usage:
     * <code>
     * $query->filterByArchivedAt('2011-03-14'); // WHERE archived_at = '2011-03-14'
     * $query->filterByArchivedAt('now'); // WHERE archived_at = '2011-03-14'
     * $query->filterByArchivedAt(array('max' => 'yesterday')); // WHERE archived_at > '2011-03-13'
     * </code>
     *
     * @param     mixed $archivedAt The value to use as filter.
     *              Values can be integers (unix timestamps), DateTime objects, or strings.
     *              Empty strings are treated as NULL.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildAssoTresorsArchiveQuery The current query, for fluid interface
     */
    public function filterByArchivedAt($archivedAt = null, $comparison = null)
    {
        if (is_array($archivedAt)) {
            $useMinMax = false;
            if (isset($archivedAt['min'])) {
                $this->addUsingAlias(AssoTresorsArchiveTableMap::COL_ARCHIVED_AT, $archivedAt['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($archivedAt['max'])) {
                $this->addUsingAlias(AssoTresorsArchiveTableMap::COL_ARCHIVED_AT, $archivedAt['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(AssoTresorsArchiveTableMap::COL_ARCHIVED_AT, $archivedAt, $comparison);
    }

    /**
     * Exclude object from result
     *
     * @param   ChildAssoTresorsArchive $assoTresorsArchive Object to remove from the list of results
     *
     * @return $this|ChildAssoTresorsArchiveQuery The current query, for fluid interface
     */
    public function prune($assoTresorsArchive = null)
    {
        if ($assoTresorsArchive) {
            $this->addUsingAlias(AssoTresorsArchiveTableMap::COL_ID_TRESOR, $assoTresorsArchive->getIdTresor(), Criteria::NOT_EQUAL);
        }

        return $this;
    }

    /**
     * Deletes all rows from the asso_tresors_archive table.
     *
     * @param ConnectionInterface $con the connection to use
     * @return int The number of affected rows (if supported by underlying database driver).
     */
    public function doDeleteAll(ConnectionInterface $con = null)
    {
        if (null === $con) {
            $con = Propel::getServiceContainer()->getWriteConnection(AssoTresorsArchiveTableMap::DATABASE_NAME);
        }

        // use transaction because $criteria could contain info
        // for more than one table or we could emulating ON DELETE CASCADE, etc.
        return $con->transaction(function () use ($con) {
            $affectedRows = 0; // initialize var to track total num of affected rows
            $affectedRows += parent::doDeleteAll($con);
            // Because this db requires some delete cascade/set null emulation, we have to
            // clear the cached instance *after* the emulation has happened (since
            // instances get re-added by the select statement contained therein).
            AssoTresorsArchiveTableMap::clearInstancePool();
            AssoTresorsArchiveTableMap::clearRelatedInstancePool();

            return $affectedRows;
        });
    }

    /**
     * Performs a DELETE on the database based on the current ModelCriteria
     *
     * @param ConnectionInterface $con the connection to use
     * @return int             The number of affected rows (if supported by underlying database driver).  This includes CASCADE-related rows
     *                         if supported by native driver or if emulated using Propel.
     * @throws PropelException Any exceptions caught during processing will be
     *                         rethrown wrapped into a PropelException.
     */
    public function delete(ConnectionInterface $con = null)
    {
        if (null === $con) {
            $con = Propel::getServiceContainer()->getWriteConnection(AssoTresorsArchiveTableMap::DATABASE_NAME);
        }

        $criteria = $this;

        // Set the correct dbName
        $criteria->setDbName(AssoTresorsArchiveTableMap::DATABASE_NAME);

        // use transaction because $criteria could contain info
        // for more than one table or we could emulating ON DELETE CASCADE, etc.
        return $con->transaction(function () use ($con, $criteria) {
            $affectedRows = 0; // initialize var to track total num of affected rows

            AssoTresorsArchiveTableMap::removeInstanceFromPool($criteria);

            $affectedRows += ModelCriteria::delete($con);
            AssoTresorsArchiveTableMap::clearRelatedInstancePool();

            return $affectedRows;
        });
    }

} // AssoTresorsArchiveQuery
