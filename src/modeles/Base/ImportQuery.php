<?php

namespace Base;

use \AssoImportsArchive as ChildAssoImportsArchive;
use \Import as ChildImport;
use \ImportQuery as ChildImportQuery;
use \Exception;
use \PDO;
use Map\ImportTableMap;
use Propel\Runtime\Propel;
use Propel\Runtime\ActiveQuery\Criteria;
use Propel\Runtime\ActiveQuery\ModelCriteria;
use Propel\Runtime\ActiveQuery\ModelJoin;
use Propel\Runtime\Collection\ObjectCollection;
use Propel\Runtime\Connection\ConnectionInterface;
use Propel\Runtime\Exception\PropelException;

/**
 * Base class that represents a query for the 'asso_imports' table.
 *
 *
 *
 * @method     ChildImportQuery orderByIdImport($order = Criteria::ASC) Order by the id_import column
 * @method     ChildImportQuery orderByModele($order = Criteria::ASC) Order by the modele column
 * @method     ChildImportQuery orderByAvancement($order = Criteria::ASC) Order by the avancement column
 * @method     ChildImportQuery orderByNom($order = Criteria::ASC) Order by the nom column
 * @method     ChildImportQuery orderByNomprovisoire($order = Criteria::ASC) Order by the nomprovisoire column
 * @method     ChildImportQuery orderByTaille($order = Criteria::ASC) Order by the taille column
 * @method     ChildImportQuery orderByControlemd5($order = Criteria::ASC) Order by the controlemd5 column
 * @method     ChildImportQuery orderByOriginals($order = Criteria::ASC) Order by the originals column
 * @method     ChildImportQuery orderByInformations($order = Criteria::ASC) Order by the informations column
 * @method     ChildImportQuery orderByModifications($order = Criteria::ASC) Order by the modifications column
 * @method     ChildImportQuery orderByNbLigne($order = Criteria::ASC) Order by the nb_ligne column
 * @method     ChildImportQuery orderByLigneEnCours($order = Criteria::ASC) Order by the ligne_en_cours column
 * @method     ChildImportQuery orderByCreatedAt($order = Criteria::ASC) Order by the created_at column
 * @method     ChildImportQuery orderByUpdatedAt($order = Criteria::ASC) Order by the updated_at column
 *
 * @method     ChildImportQuery groupByIdImport() Group by the id_import column
 * @method     ChildImportQuery groupByModele() Group by the modele column
 * @method     ChildImportQuery groupByAvancement() Group by the avancement column
 * @method     ChildImportQuery groupByNom() Group by the nom column
 * @method     ChildImportQuery groupByNomprovisoire() Group by the nomprovisoire column
 * @method     ChildImportQuery groupByTaille() Group by the taille column
 * @method     ChildImportQuery groupByControlemd5() Group by the controlemd5 column
 * @method     ChildImportQuery groupByOriginals() Group by the originals column
 * @method     ChildImportQuery groupByInformations() Group by the informations column
 * @method     ChildImportQuery groupByModifications() Group by the modifications column
 * @method     ChildImportQuery groupByNbLigne() Group by the nb_ligne column
 * @method     ChildImportQuery groupByLigneEnCours() Group by the ligne_en_cours column
 * @method     ChildImportQuery groupByCreatedAt() Group by the created_at column
 * @method     ChildImportQuery groupByUpdatedAt() Group by the updated_at column
 *
 * @method     ChildImportQuery leftJoin($relation) Adds a LEFT JOIN clause to the query
 * @method     ChildImportQuery rightJoin($relation) Adds a RIGHT JOIN clause to the query
 * @method     ChildImportQuery innerJoin($relation) Adds a INNER JOIN clause to the query
 *
 * @method     ChildImportQuery leftJoinWith($relation) Adds a LEFT JOIN clause and with to the query
 * @method     ChildImportQuery rightJoinWith($relation) Adds a RIGHT JOIN clause and with to the query
 * @method     ChildImportQuery innerJoinWith($relation) Adds a INNER JOIN clause and with to the query
 *
 * @method     ChildImportQuery leftJoinImportligne($relationAlias = null) Adds a LEFT JOIN clause to the query using the Importligne relation
 * @method     ChildImportQuery rightJoinImportligne($relationAlias = null) Adds a RIGHT JOIN clause to the query using the Importligne relation
 * @method     ChildImportQuery innerJoinImportligne($relationAlias = null) Adds a INNER JOIN clause to the query using the Importligne relation
 *
 * @method     ChildImportQuery joinWithImportligne($joinType = Criteria::INNER_JOIN) Adds a join clause and with to the query using the Importligne relation
 *
 * @method     ChildImportQuery leftJoinWithImportligne() Adds a LEFT JOIN clause and with to the query using the Importligne relation
 * @method     ChildImportQuery rightJoinWithImportligne() Adds a RIGHT JOIN clause and with to the query using the Importligne relation
 * @method     ChildImportQuery innerJoinWithImportligne() Adds a INNER JOIN clause and with to the query using the Importligne relation
 *
 * @method     \ImportligneQuery endUse() Finalizes a secondary criteria and merges it with its primary Criteria
 *
 * @method     ChildImport findOne(ConnectionInterface $con = null) Return the first ChildImport matching the query
 * @method     ChildImport findOneOrCreate(ConnectionInterface $con = null) Return the first ChildImport matching the query, or a new ChildImport object populated from the query conditions when no match is found
 *
 * @method     ChildImport findOneByIdImport(string $id_import) Return the first ChildImport filtered by the id_import column
 * @method     ChildImport findOneByModele(string $modele) Return the first ChildImport filtered by the modele column
 * @method     ChildImport findOneByAvancement(int $avancement) Return the first ChildImport filtered by the avancement column
 * @method     ChildImport findOneByNom(string $nom) Return the first ChildImport filtered by the nom column
 * @method     ChildImport findOneByNomprovisoire(string $nomprovisoire) Return the first ChildImport filtered by the nomprovisoire column
 * @method     ChildImport findOneByTaille(string $taille) Return the first ChildImport filtered by the taille column
 * @method     ChildImport findOneByControlemd5(string $controlemd5) Return the first ChildImport filtered by the controlemd5 column
 * @method     ChildImport findOneByOriginals(string $originals) Return the first ChildImport filtered by the originals column
 * @method     ChildImport findOneByInformations(string $informations) Return the first ChildImport filtered by the informations column
 * @method     ChildImport findOneByModifications(string $modifications) Return the first ChildImport filtered by the modifications column
 * @method     ChildImport findOneByNbLigne(string $nb_ligne) Return the first ChildImport filtered by the nb_ligne column
 * @method     ChildImport findOneByLigneEnCours(string $ligne_en_cours) Return the first ChildImport filtered by the ligne_en_cours column
 * @method     ChildImport findOneByCreatedAt(string $created_at) Return the first ChildImport filtered by the created_at column
 * @method     ChildImport findOneByUpdatedAt(string $updated_at) Return the first ChildImport filtered by the updated_at column *

 * @method     ChildImport requirePk($key, ConnectionInterface $con = null) Return the ChildImport by primary key and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildImport requireOne(ConnectionInterface $con = null) Return the first ChildImport matching the query and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 *
 * @method     ChildImport requireOneByIdImport(string $id_import) Return the first ChildImport filtered by the id_import column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildImport requireOneByModele(string $modele) Return the first ChildImport filtered by the modele column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildImport requireOneByAvancement(int $avancement) Return the first ChildImport filtered by the avancement column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildImport requireOneByNom(string $nom) Return the first ChildImport filtered by the nom column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildImport requireOneByNomprovisoire(string $nomprovisoire) Return the first ChildImport filtered by the nomprovisoire column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildImport requireOneByTaille(string $taille) Return the first ChildImport filtered by the taille column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildImport requireOneByControlemd5(string $controlemd5) Return the first ChildImport filtered by the controlemd5 column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildImport requireOneByOriginals(string $originals) Return the first ChildImport filtered by the originals column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildImport requireOneByInformations(string $informations) Return the first ChildImport filtered by the informations column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildImport requireOneByModifications(string $modifications) Return the first ChildImport filtered by the modifications column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildImport requireOneByNbLigne(string $nb_ligne) Return the first ChildImport filtered by the nb_ligne column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildImport requireOneByLigneEnCours(string $ligne_en_cours) Return the first ChildImport filtered by the ligne_en_cours column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildImport requireOneByCreatedAt(string $created_at) Return the first ChildImport filtered by the created_at column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildImport requireOneByUpdatedAt(string $updated_at) Return the first ChildImport filtered by the updated_at column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 *
 * @method     ChildImport[]|ObjectCollection find(ConnectionInterface $con = null) Return ChildImport objects based on current ModelCriteria
 * @method     ChildImport[]|ObjectCollection findByIdImport(string $id_import) Return ChildImport objects filtered by the id_import column
 * @method     ChildImport[]|ObjectCollection findByModele(string $modele) Return ChildImport objects filtered by the modele column
 * @method     ChildImport[]|ObjectCollection findByAvancement(int $avancement) Return ChildImport objects filtered by the avancement column
 * @method     ChildImport[]|ObjectCollection findByNom(string $nom) Return ChildImport objects filtered by the nom column
 * @method     ChildImport[]|ObjectCollection findByNomprovisoire(string $nomprovisoire) Return ChildImport objects filtered by the nomprovisoire column
 * @method     ChildImport[]|ObjectCollection findByTaille(string $taille) Return ChildImport objects filtered by the taille column
 * @method     ChildImport[]|ObjectCollection findByControlemd5(string $controlemd5) Return ChildImport objects filtered by the controlemd5 column
 * @method     ChildImport[]|ObjectCollection findByOriginals(string $originals) Return ChildImport objects filtered by the originals column
 * @method     ChildImport[]|ObjectCollection findByInformations(string $informations) Return ChildImport objects filtered by the informations column
 * @method     ChildImport[]|ObjectCollection findByModifications(string $modifications) Return ChildImport objects filtered by the modifications column
 * @method     ChildImport[]|ObjectCollection findByNbLigne(string $nb_ligne) Return ChildImport objects filtered by the nb_ligne column
 * @method     ChildImport[]|ObjectCollection findByLigneEnCours(string $ligne_en_cours) Return ChildImport objects filtered by the ligne_en_cours column
 * @method     ChildImport[]|ObjectCollection findByCreatedAt(string $created_at) Return ChildImport objects filtered by the created_at column
 * @method     ChildImport[]|ObjectCollection findByUpdatedAt(string $updated_at) Return ChildImport objects filtered by the updated_at column
 * @method     ChildImport[]|\Propel\Runtime\Util\PropelModelPager paginate($page = 1, $maxPerPage = 10, ConnectionInterface $con = null) Issue a SELECT query based on the current ModelCriteria and uses a page and a maximum number of results per page to compute an offset and a limit
 *
 */
abstract class ImportQuery extends ModelCriteria
{

    // archivable behavior
    protected $archiveOnDelete = true;
protected $entityNotFoundExceptionClass = '\\Propel\\Runtime\\Exception\\EntityNotFoundException';

    /**
     * Initializes internal state of \Base\ImportQuery object.
     *
     * @param     string $dbName The database name
     * @param     string $modelName The phpName of a model, e.g. 'Book'
     * @param     string $modelAlias The alias for the model in this query, e.g. 'b'
     */
    public function __construct($dbName = 'simplasso', $modelName = '\\Import', $modelAlias = null)
    {
        parent::__construct($dbName, $modelName, $modelAlias);
    }

    /**
     * Returns a new ChildImportQuery object.
     *
     * @param     string $modelAlias The alias of a model in the query
     * @param     Criteria $criteria Optional Criteria to build the query from
     *
     * @return ChildImportQuery
     */
    public static function create($modelAlias = null, Criteria $criteria = null)
    {
        if ($criteria instanceof ChildImportQuery) {
            return $criteria;
        }
        $query = new ChildImportQuery();
        if (null !== $modelAlias) {
            $query->setModelAlias($modelAlias);
        }
        if ($criteria instanceof Criteria) {
            $query->mergeWith($criteria);
        }

        return $query;
    }

    /**
     * Find object by primary key.
     * Propel uses the instance pool to skip the database if the object exists.
     * Go fast if the query is untouched.
     *
     * <code>
     * $obj  = $c->findPk(12, $con);
     * </code>
     *
     * @param mixed $key Primary key to use for the query
     * @param ConnectionInterface $con an optional connection object
     *
     * @return ChildImport|array|mixed the result, formatted by the current formatter
     */
    public function findPk($key, ConnectionInterface $con = null)
    {
        if ($key === null) {
            return null;
        }

        if ($con === null) {
            $con = Propel::getServiceContainer()->getReadConnection(ImportTableMap::DATABASE_NAME);
        }

        $this->basePreSelect($con);

        if (
            $this->formatter || $this->modelAlias || $this->with || $this->select
            || $this->selectColumns || $this->asColumns || $this->selectModifiers
            || $this->map || $this->having || $this->joins
        ) {
            return $this->findPkComplex($key, $con);
        }

        if ((null !== ($obj = ImportTableMap::getInstanceFromPool(null === $key || is_scalar($key) || is_callable([$key, '__toString']) ? (string) $key : $key)))) {
            // the object is already in the instance pool
            return $obj;
        }

        return $this->findPkSimple($key, $con);
    }

    /**
     * Find object by primary key using raw SQL to go fast.
     * Bypass doSelect() and the object formatter by using generated code.
     *
     * @param     mixed $key Primary key to use for the query
     * @param     ConnectionInterface $con A connection object
     *
     * @throws \Propel\Runtime\Exception\PropelException
     *
     * @return ChildImport A model object, or null if the key is not found
     */
    protected function findPkSimple($key, ConnectionInterface $con)
    {
        $sql = 'SELECT `id_import`, `modele`, `avancement`, `nom`, `nomprovisoire`, `taille`, `controlemd5`, `nb_ligne`, `ligne_en_cours`, `created_at`, `updated_at` FROM `asso_imports` WHERE `id_import` = :p0';
        try {
            $stmt = $con->prepare($sql);
            $stmt->bindValue(':p0', $key, PDO::PARAM_INT);
            $stmt->execute();
        } catch (Exception $e) {
            Propel::log($e->getMessage(), Propel::LOG_ERR);
            throw new PropelException(sprintf('Unable to execute SELECT statement [%s]', $sql), 0, $e);
        }
        $obj = null;
        if ($row = $stmt->fetch(\PDO::FETCH_NUM)) {
            /** @var ChildImport $obj */
            $obj = new ChildImport();
            $obj->hydrate($row);
            ImportTableMap::addInstanceToPool($obj, null === $key || is_scalar($key) || is_callable([$key, '__toString']) ? (string) $key : $key);
        }
        $stmt->closeCursor();

        return $obj;
    }

    /**
     * Find object by primary key.
     *
     * @param     mixed $key Primary key to use for the query
     * @param     ConnectionInterface $con A connection object
     *
     * @return ChildImport|array|mixed the result, formatted by the current formatter
     */
    protected function findPkComplex($key, ConnectionInterface $con)
    {
        // As the query uses a PK condition, no limit(1) is necessary.
        $criteria = $this->isKeepQuery() ? clone $this : $this;
        $dataFetcher = $criteria
            ->filterByPrimaryKey($key)
            ->doSelect($con);

        return $criteria->getFormatter()->init($criteria)->formatOne($dataFetcher);
    }

    /**
     * Find objects by primary key
     * <code>
     * $objs = $c->findPks(array(12, 56, 832), $con);
     * </code>
     * @param     array $keys Primary keys to use for the query
     * @param     ConnectionInterface $con an optional connection object
     *
     * @return ObjectCollection|array|mixed the list of results, formatted by the current formatter
     */
    public function findPks($keys, ConnectionInterface $con = null)
    {
        if (null === $con) {
            $con = Propel::getServiceContainer()->getReadConnection($this->getDbName());
        }
        $this->basePreSelect($con);
        $criteria = $this->isKeepQuery() ? clone $this : $this;
        $dataFetcher = $criteria
            ->filterByPrimaryKeys($keys)
            ->doSelect($con);

        return $criteria->getFormatter()->init($criteria)->format($dataFetcher);
    }

    /**
     * Filter the query by primary key
     *
     * @param     mixed $key Primary key to use for the query
     *
     * @return $this|ChildImportQuery The current query, for fluid interface
     */
    public function filterByPrimaryKey($key)
    {

        return $this->addUsingAlias(ImportTableMap::COL_ID_IMPORT, $key, Criteria::EQUAL);
    }

    /**
     * Filter the query by a list of primary keys
     *
     * @param     array $keys The list of primary key to use for the query
     *
     * @return $this|ChildImportQuery The current query, for fluid interface
     */
    public function filterByPrimaryKeys($keys)
    {

        return $this->addUsingAlias(ImportTableMap::COL_ID_IMPORT, $keys, Criteria::IN);
    }

    /**
     * Filter the query on the id_import column
     *
     * Example usage:
     * <code>
     * $query->filterByIdImport(1234); // WHERE id_import = 1234
     * $query->filterByIdImport(array(12, 34)); // WHERE id_import IN (12, 34)
     * $query->filterByIdImport(array('min' => 12)); // WHERE id_import > 12
     * </code>
     *
     * @param     mixed $idImport The value to use as filter.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildImportQuery The current query, for fluid interface
     */
    public function filterByIdImport($idImport = null, $comparison = null)
    {
        if (is_array($idImport)) {
            $useMinMax = false;
            if (isset($idImport['min'])) {
                $this->addUsingAlias(ImportTableMap::COL_ID_IMPORT, $idImport['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($idImport['max'])) {
                $this->addUsingAlias(ImportTableMap::COL_ID_IMPORT, $idImport['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(ImportTableMap::COL_ID_IMPORT, $idImport, $comparison);
    }

    /**
     * Filter the query on the modele column
     *
     * Example usage:
     * <code>
     * $query->filterByModele('fooValue');   // WHERE modele = 'fooValue'
     * $query->filterByModele('%fooValue%', Criteria::LIKE); // WHERE modele LIKE '%fooValue%'
     * </code>
     *
     * @param     string $modele The value to use as filter.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildImportQuery The current query, for fluid interface
     */
    public function filterByModele($modele = null, $comparison = null)
    {
        if (null === $comparison) {
            if (is_array($modele)) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(ImportTableMap::COL_MODELE, $modele, $comparison);
    }

    /**
     * Filter the query on the avancement column
     *
     * Example usage:
     * <code>
     * $query->filterByAvancement(1234); // WHERE avancement = 1234
     * $query->filterByAvancement(array(12, 34)); // WHERE avancement IN (12, 34)
     * $query->filterByAvancement(array('min' => 12)); // WHERE avancement > 12
     * </code>
     *
     * @param     mixed $avancement The value to use as filter.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildImportQuery The current query, for fluid interface
     */
    public function filterByAvancement($avancement = null, $comparison = null)
    {
        if (is_array($avancement)) {
            $useMinMax = false;
            if (isset($avancement['min'])) {
                $this->addUsingAlias(ImportTableMap::COL_AVANCEMENT, $avancement['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($avancement['max'])) {
                $this->addUsingAlias(ImportTableMap::COL_AVANCEMENT, $avancement['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(ImportTableMap::COL_AVANCEMENT, $avancement, $comparison);
    }

    /**
     * Filter the query on the nom column
     *
     * Example usage:
     * <code>
     * $query->filterByNom('fooValue');   // WHERE nom = 'fooValue'
     * $query->filterByNom('%fooValue%', Criteria::LIKE); // WHERE nom LIKE '%fooValue%'
     * </code>
     *
     * @param     string $nom The value to use as filter.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildImportQuery The current query, for fluid interface
     */
    public function filterByNom($nom = null, $comparison = null)
    {
        if (null === $comparison) {
            if (is_array($nom)) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(ImportTableMap::COL_NOM, $nom, $comparison);
    }

    /**
     * Filter the query on the nomprovisoire column
     *
     * Example usage:
     * <code>
     * $query->filterByNomprovisoire('fooValue');   // WHERE nomprovisoire = 'fooValue'
     * $query->filterByNomprovisoire('%fooValue%', Criteria::LIKE); // WHERE nomprovisoire LIKE '%fooValue%'
     * </code>
     *
     * @param     string $nomprovisoire The value to use as filter.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildImportQuery The current query, for fluid interface
     */
    public function filterByNomprovisoire($nomprovisoire = null, $comparison = null)
    {
        if (null === $comparison) {
            if (is_array($nomprovisoire)) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(ImportTableMap::COL_NOMPROVISOIRE, $nomprovisoire, $comparison);
    }

    /**
     * Filter the query on the taille column
     *
     * Example usage:
     * <code>
     * $query->filterByTaille(1234); // WHERE taille = 1234
     * $query->filterByTaille(array(12, 34)); // WHERE taille IN (12, 34)
     * $query->filterByTaille(array('min' => 12)); // WHERE taille > 12
     * </code>
     *
     * @param     mixed $taille The value to use as filter.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildImportQuery The current query, for fluid interface
     */
    public function filterByTaille($taille = null, $comparison = null)
    {
        if (is_array($taille)) {
            $useMinMax = false;
            if (isset($taille['min'])) {
                $this->addUsingAlias(ImportTableMap::COL_TAILLE, $taille['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($taille['max'])) {
                $this->addUsingAlias(ImportTableMap::COL_TAILLE, $taille['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(ImportTableMap::COL_TAILLE, $taille, $comparison);
    }

    /**
     * Filter the query on the controlemd5 column
     *
     * Example usage:
     * <code>
     * $query->filterByControlemd5('fooValue');   // WHERE controlemd5 = 'fooValue'
     * $query->filterByControlemd5('%fooValue%', Criteria::LIKE); // WHERE controlemd5 LIKE '%fooValue%'
     * </code>
     *
     * @param     string $controlemd5 The value to use as filter.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildImportQuery The current query, for fluid interface
     */
    public function filterByControlemd5($controlemd5 = null, $comparison = null)
    {
        if (null === $comparison) {
            if (is_array($controlemd5)) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(ImportTableMap::COL_CONTROLEMD5, $controlemd5, $comparison);
    }

    /**
     * Filter the query on the originals column
     *
     * Example usage:
     * <code>
     * $query->filterByOriginals('fooValue');   // WHERE originals = 'fooValue'
     * $query->filterByOriginals('%fooValue%', Criteria::LIKE); // WHERE originals LIKE '%fooValue%'
     * </code>
     *
     * @param     string $originals The value to use as filter.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildImportQuery The current query, for fluid interface
     */
    public function filterByOriginals($originals = null, $comparison = null)
    {
        if (null === $comparison) {
            if (is_array($originals)) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(ImportTableMap::COL_ORIGINALS, $originals, $comparison);
    }

    /**
     * Filter the query on the informations column
     *
     * Example usage:
     * <code>
     * $query->filterByInformations('fooValue');   // WHERE informations = 'fooValue'
     * $query->filterByInformations('%fooValue%', Criteria::LIKE); // WHERE informations LIKE '%fooValue%'
     * </code>
     *
     * @param     string $informations The value to use as filter.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildImportQuery The current query, for fluid interface
     */
    public function filterByInformations($informations = null, $comparison = null)
    {
        if (null === $comparison) {
            if (is_array($informations)) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(ImportTableMap::COL_INFORMATIONS, $informations, $comparison);
    }

    /**
     * Filter the query on the modifications column
     *
     * Example usage:
     * <code>
     * $query->filterByModifications('fooValue');   // WHERE modifications = 'fooValue'
     * $query->filterByModifications('%fooValue%', Criteria::LIKE); // WHERE modifications LIKE '%fooValue%'
     * </code>
     *
     * @param     string $modifications The value to use as filter.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildImportQuery The current query, for fluid interface
     */
    public function filterByModifications($modifications = null, $comparison = null)
    {
        if (null === $comparison) {
            if (is_array($modifications)) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(ImportTableMap::COL_MODIFICATIONS, $modifications, $comparison);
    }

    /**
     * Filter the query on the nb_ligne column
     *
     * Example usage:
     * <code>
     * $query->filterByNbLigne(1234); // WHERE nb_ligne = 1234
     * $query->filterByNbLigne(array(12, 34)); // WHERE nb_ligne IN (12, 34)
     * $query->filterByNbLigne(array('min' => 12)); // WHERE nb_ligne > 12
     * </code>
     *
     * @param     mixed $nbLigne The value to use as filter.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildImportQuery The current query, for fluid interface
     */
    public function filterByNbLigne($nbLigne = null, $comparison = null)
    {
        if (is_array($nbLigne)) {
            $useMinMax = false;
            if (isset($nbLigne['min'])) {
                $this->addUsingAlias(ImportTableMap::COL_NB_LIGNE, $nbLigne['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($nbLigne['max'])) {
                $this->addUsingAlias(ImportTableMap::COL_NB_LIGNE, $nbLigne['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(ImportTableMap::COL_NB_LIGNE, $nbLigne, $comparison);
    }

    /**
     * Filter the query on the ligne_en_cours column
     *
     * Example usage:
     * <code>
     * $query->filterByLigneEnCours(1234); // WHERE ligne_en_cours = 1234
     * $query->filterByLigneEnCours(array(12, 34)); // WHERE ligne_en_cours IN (12, 34)
     * $query->filterByLigneEnCours(array('min' => 12)); // WHERE ligne_en_cours > 12
     * </code>
     *
     * @param     mixed $ligneEnCours The value to use as filter.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildImportQuery The current query, for fluid interface
     */
    public function filterByLigneEnCours($ligneEnCours = null, $comparison = null)
    {
        if (is_array($ligneEnCours)) {
            $useMinMax = false;
            if (isset($ligneEnCours['min'])) {
                $this->addUsingAlias(ImportTableMap::COL_LIGNE_EN_COURS, $ligneEnCours['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($ligneEnCours['max'])) {
                $this->addUsingAlias(ImportTableMap::COL_LIGNE_EN_COURS, $ligneEnCours['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(ImportTableMap::COL_LIGNE_EN_COURS, $ligneEnCours, $comparison);
    }

    /**
     * Filter the query on the created_at column
     *
     * Example usage:
     * <code>
     * $query->filterByCreatedAt('2011-03-14'); // WHERE created_at = '2011-03-14'
     * $query->filterByCreatedAt('now'); // WHERE created_at = '2011-03-14'
     * $query->filterByCreatedAt(array('max' => 'yesterday')); // WHERE created_at > '2011-03-13'
     * </code>
     *
     * @param     mixed $createdAt The value to use as filter.
     *              Values can be integers (unix timestamps), DateTime objects, or strings.
     *              Empty strings are treated as NULL.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildImportQuery The current query, for fluid interface
     */
    public function filterByCreatedAt($createdAt = null, $comparison = null)
    {
        if (is_array($createdAt)) {
            $useMinMax = false;
            if (isset($createdAt['min'])) {
                $this->addUsingAlias(ImportTableMap::COL_CREATED_AT, $createdAt['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($createdAt['max'])) {
                $this->addUsingAlias(ImportTableMap::COL_CREATED_AT, $createdAt['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(ImportTableMap::COL_CREATED_AT, $createdAt, $comparison);
    }

    /**
     * Filter the query on the updated_at column
     *
     * Example usage:
     * <code>
     * $query->filterByUpdatedAt('2011-03-14'); // WHERE updated_at = '2011-03-14'
     * $query->filterByUpdatedAt('now'); // WHERE updated_at = '2011-03-14'
     * $query->filterByUpdatedAt(array('max' => 'yesterday')); // WHERE updated_at > '2011-03-13'
     * </code>
     *
     * @param     mixed $updatedAt The value to use as filter.
     *              Values can be integers (unix timestamps), DateTime objects, or strings.
     *              Empty strings are treated as NULL.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildImportQuery The current query, for fluid interface
     */
    public function filterByUpdatedAt($updatedAt = null, $comparison = null)
    {
        if (is_array($updatedAt)) {
            $useMinMax = false;
            if (isset($updatedAt['min'])) {
                $this->addUsingAlias(ImportTableMap::COL_UPDATED_AT, $updatedAt['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($updatedAt['max'])) {
                $this->addUsingAlias(ImportTableMap::COL_UPDATED_AT, $updatedAt['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(ImportTableMap::COL_UPDATED_AT, $updatedAt, $comparison);
    }

    /**
     * Filter the query by a related \Importligne object
     *
     * @param \Importligne|ObjectCollection $importligne the related object to use as filter
     * @param string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return ChildImportQuery The current query, for fluid interface
     */
    public function filterByImportligne($importligne, $comparison = null)
    {
        if ($importligne instanceof \Importligne) {
            return $this
                ->addUsingAlias(ImportTableMap::COL_ID_IMPORT, $importligne->getIdImport(), $comparison);
        } elseif ($importligne instanceof ObjectCollection) {
            return $this
                ->useImportligneQuery()
                ->filterByPrimaryKeys($importligne->getPrimaryKeys())
                ->endUse();
        } else {
            throw new PropelException('filterByImportligne() only accepts arguments of type \Importligne or Collection');
        }
    }

    /**
     * Adds a JOIN clause to the query using the Importligne relation
     *
     * @param     string $relationAlias optional alias for the relation
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return $this|ChildImportQuery The current query, for fluid interface
     */
    public function joinImportligne($relationAlias = null, $joinType = Criteria::INNER_JOIN)
    {
        $tableMap = $this->getTableMap();
        $relationMap = $tableMap->getRelation('Importligne');

        // create a ModelJoin object for this join
        $join = new ModelJoin();
        $join->setJoinType($joinType);
        $join->setRelationMap($relationMap, $this->useAliasInSQL ? $this->getModelAlias() : null, $relationAlias);
        if ($previousJoin = $this->getPreviousJoin()) {
            $join->setPreviousJoin($previousJoin);
        }

        // add the ModelJoin to the current object
        if ($relationAlias) {
            $this->addAlias($relationAlias, $relationMap->getRightTable()->getName());
            $this->addJoinObject($join, $relationAlias);
        } else {
            $this->addJoinObject($join, 'Importligne');
        }

        return $this;
    }

    /**
     * Use the Importligne relation Importligne object
     *
     * @see useQuery()
     *
     * @param     string $relationAlias optional alias for the relation,
     *                                   to be used as main alias in the secondary query
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return \ImportligneQuery A secondary query class using the current class as primary query
     */
    public function useImportligneQuery($relationAlias = null, $joinType = Criteria::INNER_JOIN)
    {
        return $this
            ->joinImportligne($relationAlias, $joinType)
            ->useQuery($relationAlias ? $relationAlias : 'Importligne', '\ImportligneQuery');
    }

    /**
     * Exclude object from result
     *
     * @param   ChildImport $import Object to remove from the list of results
     *
     * @return $this|ChildImportQuery The current query, for fluid interface
     */
    public function prune($import = null)
    {
        if ($import) {
            $this->addUsingAlias(ImportTableMap::COL_ID_IMPORT, $import->getIdImport(), Criteria::NOT_EQUAL);
        }

        return $this;
    }

    /**
     * Code to execute before every DELETE statement
     *
     * @param     ConnectionInterface $con The connection object used by the query
     */
    protected function basePreDelete(ConnectionInterface $con)
    {
        // archivable behavior

        if ($this->archiveOnDelete) {
            $this->archive($con);
        } else {
            $this->archiveOnDelete = true;
        }


        return $this->preDelete($con);
    }

    /**
     * Deletes all rows from the asso_imports table.
     *
     * @param ConnectionInterface $con the connection to use
     * @return int The number of affected rows (if supported by underlying database driver).
     */
    public function doDeleteAll(ConnectionInterface $con = null)
    {
        if (null === $con) {
            $con = Propel::getServiceContainer()->getWriteConnection(ImportTableMap::DATABASE_NAME);
        }

        // use transaction because $criteria could contain info
        // for more than one table or we could emulating ON DELETE CASCADE, etc.
        return $con->transaction(function () use ($con) {
            $affectedRows = 0; // initialize var to track total num of affected rows
            $affectedRows += parent::doDeleteAll($con);
            // Because this db requires some delete cascade/set null emulation, we have to
            // clear the cached instance *after* the emulation has happened (since
            // instances get re-added by the select statement contained therein).
            ImportTableMap::clearInstancePool();
            ImportTableMap::clearRelatedInstancePool();

            return $affectedRows;
        });
    }

    /**
     * Performs a DELETE on the database based on the current ModelCriteria
     *
     * @param ConnectionInterface $con the connection to use
     * @return int             The number of affected rows (if supported by underlying database driver).  This includes CASCADE-related rows
     *                         if supported by native driver or if emulated using Propel.
     * @throws PropelException Any exceptions caught during processing will be
     *                         rethrown wrapped into a PropelException.
     */
    public function delete(ConnectionInterface $con = null)
    {
        if (null === $con) {
            $con = Propel::getServiceContainer()->getWriteConnection(ImportTableMap::DATABASE_NAME);
        }

        $criteria = $this;

        // Set the correct dbName
        $criteria->setDbName(ImportTableMap::DATABASE_NAME);

        // use transaction because $criteria could contain info
        // for more than one table or we could emulating ON DELETE CASCADE, etc.
        return $con->transaction(function () use ($con, $criteria) {
            $affectedRows = 0; // initialize var to track total num of affected rows

            ImportTableMap::removeInstanceFromPool($criteria);

            $affectedRows += ModelCriteria::delete($con);
            ImportTableMap::clearRelatedInstancePool();

            return $affectedRows;
        });
    }

    // timestampable behavior

    /**
     * Filter by the latest updated
     *
     * @param      int $nbDays Maximum age of the latest update in days
     *
     * @return     $this|ChildImportQuery The current query, for fluid interface
     */
    public function recentlyUpdated($nbDays = 7)
    {
        return $this->addUsingAlias(ImportTableMap::COL_UPDATED_AT, time() - $nbDays * 24 * 60 * 60, Criteria::GREATER_EQUAL);
    }

    /**
     * Order by update date desc
     *
     * @return     $this|ChildImportQuery The current query, for fluid interface
     */
    public function lastUpdatedFirst()
    {
        return $this->addDescendingOrderByColumn(ImportTableMap::COL_UPDATED_AT);
    }

    /**
     * Order by update date asc
     *
     * @return     $this|ChildImportQuery The current query, for fluid interface
     */
    public function firstUpdatedFirst()
    {
        return $this->addAscendingOrderByColumn(ImportTableMap::COL_UPDATED_AT);
    }

    /**
     * Order by create date desc
     *
     * @return     $this|ChildImportQuery The current query, for fluid interface
     */
    public function lastCreatedFirst()
    {
        return $this->addDescendingOrderByColumn(ImportTableMap::COL_CREATED_AT);
    }

    /**
     * Filter by the latest created
     *
     * @param      int $nbDays Maximum age of in days
     *
     * @return     $this|ChildImportQuery The current query, for fluid interface
     */
    public function recentlyCreated($nbDays = 7)
    {
        return $this->addUsingAlias(ImportTableMap::COL_CREATED_AT, time() - $nbDays * 24 * 60 * 60, Criteria::GREATER_EQUAL);
    }

    /**
     * Order by create date asc
     *
     * @return     $this|ChildImportQuery The current query, for fluid interface
     */
    public function firstCreatedFirst()
    {
        return $this->addAscendingOrderByColumn(ImportTableMap::COL_CREATED_AT);
    }

    // archivable behavior

    /**
     * Copy the data of the objects satisfying the query into ChildAssoImportsArchive archive objects.
     * The archived objects are then saved.
     * If any of the objects has already been archived, the archived object
     * is updated and not duplicated.
     * Warning: This termination methods issues 2n+1 queries.
     *
     * @param      ConnectionInterface $con    Connection to use.
     * @param      Boolean $useLittleMemory    Whether or not to use OnDemandFormatter to retrieve objects.
     *               Set to false if the identity map matters.
     *               Set to true (default) to use less memory.
     *
     * @return     int the number of archived objects
     */
    public function archive($con = null, $useLittleMemory = true)
    {
        $criteria = clone $this;
        // prepare the query
        $criteria->setWith(array());
        if ($useLittleMemory) {
            $criteria->setFormatter(ModelCriteria::FORMAT_ON_DEMAND);
        }
        if ($con === null) {
            $con = Propel::getServiceContainer()->getWriteConnection(ImportTableMap::DATABASE_NAME);
        }

        return $con->transaction(function () use ($con, $criteria) {
            $totalArchivedObjects = 0;

            // archive all results one by one
            foreach ($criteria->find($con) as $object) {
                $object->archive($con);
                $totalArchivedObjects++;
            }

            return $totalArchivedObjects;
        });
    }

    /**
     * Enable/disable auto-archiving on delete for the next query.
     *
     * @param boolean True if the query must archive deleted objects, false otherwise.
     */
    public function setArchiveOnDelete($archiveOnDelete)
    {
        $this->archiveOnDelete = $archiveOnDelete;
    }

    /**
     * Delete records matching the current query without archiving them.
     *
     * @param      ConnectionInterface $con    Connection to use.
     *
     * @return integer the number of deleted rows
     */
    public function deleteWithoutArchive($con = null)
    {
        $this->archiveOnDelete = false;

        return $this->delete($con);
    }

    /**
     * Delete all records without archiving them.
     *
     * @param      ConnectionInterface $con    Connection to use.
     *
     * @return integer the number of deleted rows
     */
    public function deleteAllWithoutArchive($con = null)
    {
        $this->archiveOnDelete = false;

        return $this->deleteAll($con);
    }

} // ImportQuery
