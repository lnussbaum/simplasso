<?php

namespace Base;

use \AsscComptesArchive as ChildAsscComptesArchive;
use \AsscComptesArchiveQuery as ChildAsscComptesArchiveQuery;
use \Compte as ChildCompte;
use \CompteQuery as ChildCompteQuery;
use \Ecriture as ChildEcriture;
use \EcritureQuery as ChildEcritureQuery;
use \Entite as ChildEntite;
use \EntiteQuery as ChildEntiteQuery;
use \Journal as ChildJournal;
use \JournalQuery as ChildJournalQuery;
use \Poste as ChildPoste;
use \PosteQuery as ChildPosteQuery;
use \Prestation as ChildPrestation;
use \PrestationQuery as ChildPrestationQuery;
use \Tresor as ChildTresor;
use \TresorQuery as ChildTresorQuery;
use \Tva as ChildTva;
use \TvaQuery as ChildTvaQuery;
use \DateTime;
use \Exception;
use \PDO;
use Map\CompteTableMap;
use Map\EcritureTableMap;
use Map\JournalTableMap;
use Map\PrestationTableMap;
use Map\TresorTableMap;
use Map\TvaTableMap;
use Propel\Runtime\Propel;
use Propel\Runtime\ActiveQuery\Criteria;
use Propel\Runtime\ActiveQuery\ModelCriteria;
use Propel\Runtime\ActiveRecord\ActiveRecordInterface;
use Propel\Runtime\Collection\Collection;
use Propel\Runtime\Collection\ObjectCollection;
use Propel\Runtime\Connection\ConnectionInterface;
use Propel\Runtime\Exception\BadMethodCallException;
use Propel\Runtime\Exception\LogicException;
use Propel\Runtime\Exception\PropelException;
use Propel\Runtime\Map\TableMap;
use Propel\Runtime\Parser\AbstractParser;
use Propel\Runtime\Util\PropelDateTime;

/**
 * Base class that represents a row from the 'assc_comptes' table.
 *
 *
 *
 * @package    propel.generator..Base
 */
abstract class Compte implements ActiveRecordInterface
{
    /**
     * TableMap class name
     */
    const TABLE_MAP = '\\Map\\CompteTableMap';


    /**
     * attribute to determine if this object has previously been saved.
     * @var boolean
     */
    protected $new = true;

    /**
     * attribute to determine whether this object has been deleted.
     * @var boolean
     */
    protected $deleted = false;

    /**
     * The columns that have been modified in current object.
     * Tracking modified columns allows us to only update modified columns.
     * @var array
     */
    protected $modifiedColumns = array();

    /**
     * The (virtual) columns that are added at runtime
     * The formatters can add supplementary columns based on a resultset
     * @var array
     */
    protected $virtualColumns = array();

    /**
     * The value for the id_compte field.
     *
     * @var        string
     */
    protected $id_compte;

    /**
     * The value for the ncompte field.
     *
     * Note: this column has a database default value of: '512NEF'
     * @var        string
     */
    protected $ncompte;

    /**
     * The value for the nom field.
     *
     * Note: this column has a database default value of: 'Compte courant NEF'
     * @var        string
     */
    protected $nom;

    /**
     * The value for the nomcourt field.
     * nomcourt
     * Note: this column has a database default value of: 'CC NEF'
     * @var        string
     */
    protected $nomcourt;

    /**
     * The value for the id_poste field.
     *
     * Note: this column has a database default value of: '1'
     * @var        string
     */
    protected $id_poste;

    /**
     * The value for the type_op field.
     *
     * Note: this column has a database default value of: 3
     * @var        int
     */
    protected $type_op;

    /**
     * The value for the solde_anterieur field.
     *
     * @var        double
     */
    protected $solde_anterieur;

    /**
     * The value for the date_anterieure field.
     *
     * @var        DateTime
     */
    protected $date_anterieure;

    /**
     * The value for the id_entite field.
     *
     * Note: this column has a database default value of: '1'
     * @var        string
     */
    protected $id_entite;

    /**
     * The value for the observation field.
     *
     * @var        string
     */
    protected $observation;

    /**
     * The value for the created_at field.
     *
     * @var        DateTime
     */
    protected $created_at;

    /**
     * The value for the updated_at field.
     *
     * @var        DateTime
     */
    protected $updated_at;

    /**
     * @var        ChildEntite
     */
    protected $aEntite;

    /**
     * @var        ChildPoste
     */
    protected $aPoste;

    /**
     * @var        ObjectCollection|ChildEcriture[] Collection to store aggregation of ChildEcriture objects.
     */
    protected $collEcritures;
    protected $collEcrituresPartial;

    /**
     * @var        ObjectCollection|ChildJournal[] Collection to store aggregation of ChildJournal objects.
     */
    protected $collJournals;
    protected $collJournalsPartial;

    /**
     * @var        ObjectCollection|ChildPrestation[] Collection to store aggregation of ChildPrestation objects.
     */
    protected $collPrestations;
    protected $collPrestationsPartial;

    /**
     * @var        ObjectCollection|ChildTresor[] Collection to store aggregation of ChildTresor objects.
     */
    protected $collTresors;
    protected $collTresorsPartial;

    /**
     * @var        ObjectCollection|ChildTva[] Collection to store aggregation of ChildTva objects.
     */
    protected $collTvas;
    protected $collTvasPartial;

    /**
     * Flag to prevent endless save loop, if this object is referenced
     * by another object which falls in this transaction.
     *
     * @var boolean
     */
    protected $alreadyInSave = false;

    // archivable behavior
    protected $archiveOnDelete = true;

    /**
     * An array of objects scheduled for deletion.
     * @var ObjectCollection|ChildEcriture[]
     */
    protected $ecrituresScheduledForDeletion = null;

    /**
     * An array of objects scheduled for deletion.
     * @var ObjectCollection|ChildJournal[]
     */
    protected $journalsScheduledForDeletion = null;

    /**
     * An array of objects scheduled for deletion.
     * @var ObjectCollection|ChildPrestation[]
     */
    protected $prestationsScheduledForDeletion = null;

    /**
     * An array of objects scheduled for deletion.
     * @var ObjectCollection|ChildTresor[]
     */
    protected $tresorsScheduledForDeletion = null;

    /**
     * An array of objects scheduled for deletion.
     * @var ObjectCollection|ChildTva[]
     */
    protected $tvasScheduledForDeletion = null;

    /**
     * Applies default values to this object.
     * This method should be called from the object's constructor (or
     * equivalent initialization method).
     * @see __construct()
     */
    public function applyDefaultValues()
    {
        $this->ncompte = '512NEF';
        $this->nom = 'Compte courant NEF';
        $this->nomcourt = 'CC NEF';
        $this->id_poste = '1';
        $this->type_op = 3;
        $this->id_entite = '1';
    }

    /**
     * Initializes internal state of Base\Compte object.
     * @see applyDefaults()
     */
    public function __construct()
    {
        $this->applyDefaultValues();
    }

    /**
     * Returns whether the object has been modified.
     *
     * @return boolean True if the object has been modified.
     */
    public function isModified()
    {
        return !!$this->modifiedColumns;
    }

    /**
     * Has specified column been modified?
     *
     * @param  string  $col column fully qualified name (TableMap::TYPE_COLNAME), e.g. Book::AUTHOR_ID
     * @return boolean True if $col has been modified.
     */
    public function isColumnModified($col)
    {
        return $this->modifiedColumns && isset($this->modifiedColumns[$col]);
    }

    /**
     * Get the columns that have been modified in this object.
     * @return array A unique list of the modified column names for this object.
     */
    public function getModifiedColumns()
    {
        return $this->modifiedColumns ? array_keys($this->modifiedColumns) : [];
    }

    /**
     * Returns whether the object has ever been saved.  This will
     * be false, if the object was retrieved from storage or was created
     * and then saved.
     *
     * @return boolean true, if the object has never been persisted.
     */
    public function isNew()
    {
        return $this->new;
    }

    /**
     * Setter for the isNew attribute.  This method will be called
     * by Propel-generated children and objects.
     *
     * @param boolean $b the state of the object.
     */
    public function setNew($b)
    {
        $this->new = (boolean) $b;
    }

    /**
     * Whether this object has been deleted.
     * @return boolean The deleted state of this object.
     */
    public function isDeleted()
    {
        return $this->deleted;
    }

    /**
     * Specify whether this object has been deleted.
     * @param  boolean $b The deleted state of this object.
     * @return void
     */
    public function setDeleted($b)
    {
        $this->deleted = (boolean) $b;
    }

    /**
     * Sets the modified state for the object to be false.
     * @param  string $col If supplied, only the specified column is reset.
     * @return void
     */
    public function resetModified($col = null)
    {
        if (null !== $col) {
            if (isset($this->modifiedColumns[$col])) {
                unset($this->modifiedColumns[$col]);
            }
        } else {
            $this->modifiedColumns = array();
        }
    }

    /**
     * Compares this with another <code>Compte</code> instance.  If
     * <code>obj</code> is an instance of <code>Compte</code>, delegates to
     * <code>equals(Compte)</code>.  Otherwise, returns <code>false</code>.
     *
     * @param  mixed   $obj The object to compare to.
     * @return boolean Whether equal to the object specified.
     */
    public function equals($obj)
    {
        if (!$obj instanceof static) {
            return false;
        }

        if ($this === $obj) {
            return true;
        }

        if (null === $this->getPrimaryKey() || null === $obj->getPrimaryKey()) {
            return false;
        }

        return $this->getPrimaryKey() === $obj->getPrimaryKey();
    }

    /**
     * Get the associative array of the virtual columns in this object
     *
     * @return array
     */
    public function getVirtualColumns()
    {
        return $this->virtualColumns;
    }

    /**
     * Checks the existence of a virtual column in this object
     *
     * @param  string  $name The virtual column name
     * @return boolean
     */
    public function hasVirtualColumn($name)
    {
        return array_key_exists($name, $this->virtualColumns);
    }

    /**
     * Get the value of a virtual column in this object
     *
     * @param  string $name The virtual column name
     * @return mixed
     *
     * @throws PropelException
     */
    public function getVirtualColumn($name)
    {
        if (!$this->hasVirtualColumn($name)) {
            throw new PropelException(sprintf('Cannot get value of inexistent virtual column %s.', $name));
        }

        return $this->virtualColumns[$name];
    }

    /**
     * Set the value of a virtual column in this object
     *
     * @param string $name  The virtual column name
     * @param mixed  $value The value to give to the virtual column
     *
     * @return $this|Compte The current object, for fluid interface
     */
    public function setVirtualColumn($name, $value)
    {
        $this->virtualColumns[$name] = $value;

        return $this;
    }

    /**
     * Logs a message using Propel::log().
     *
     * @param  string  $msg
     * @param  int     $priority One of the Propel::LOG_* logging levels
     * @return boolean
     */
    protected function log($msg, $priority = Propel::LOG_INFO)
    {
        return Propel::log(get_class($this) . ': ' . $msg, $priority);
    }

    /**
     * Export the current object properties to a string, using a given parser format
     * <code>
     * $book = BookQuery::create()->findPk(9012);
     * echo $book->exportTo('JSON');
     *  => {"Id":9012,"Title":"Don Juan","ISBN":"0140422161","Price":12.99,"PublisherId":1234,"AuthorId":5678}');
     * </code>
     *
     * @param  mixed   $parser                 A AbstractParser instance, or a format name ('XML', 'YAML', 'JSON', 'CSV')
     * @param  boolean $includeLazyLoadColumns (optional) Whether to include lazy load(ed) columns. Defaults to TRUE.
     * @return string  The exported data
     */
    public function exportTo($parser, $includeLazyLoadColumns = true)
    {
        if (!$parser instanceof AbstractParser) {
            $parser = AbstractParser::getParser($parser);
        }

        return $parser->fromArray($this->toArray(TableMap::TYPE_PHPNAME, $includeLazyLoadColumns, array(), true));
    }

    /**
     * Clean up internal collections prior to serializing
     * Avoids recursive loops that turn into segmentation faults when serializing
     */
    public function __sleep()
    {
        $this->clearAllReferences();

        $cls = new \ReflectionClass($this);
        $propertyNames = [];
        $serializableProperties = array_diff($cls->getProperties(), $cls->getProperties(\ReflectionProperty::IS_STATIC));

        foreach($serializableProperties as $property) {
            $propertyNames[] = $property->getName();
        }

        return $propertyNames;
    }

    /**
     * Get the [id_compte] column value.
     *
     * @return string
     */
    public function getIdCompte()
    {
        return $this->id_compte;
    }

    /**
     * Get the [ncompte] column value.
     *
     * @return string
     */
    public function getNcompte()
    {
        return $this->ncompte;
    }

    /**
     * Get the [nom] column value.
     *
     * @return string
     */
    public function getNom()
    {
        return $this->nom;
    }

    /**
     * Get the [nomcourt] column value.
     * nomcourt
     * @return string
     */
    public function getNomcourt()
    {
        return $this->nomcourt;
    }

    /**
     * Get the [id_poste] column value.
     *
     * @return string
     */
    public function getIdPoste()
    {
        return $this->id_poste;
    }

    /**
     * Get the [type_op] column value.
     *
     * @return int
     */
    public function getTypeOp()
    {
        return $this->type_op;
    }

    /**
     * Get the [solde_anterieur] column value.
     *
     * @return double
     */
    public function getSoldeAnterieur()
    {
        return $this->solde_anterieur;
    }

    /**
     * Get the [optionally formatted] temporal [date_anterieure] column value.
     *
     *
     * @param      string $format The date/time format string (either date()-style or strftime()-style).
     *                            If format is NULL, then the raw DateTime object will be returned.
     *
     * @return string|DateTime Formatted date/time value as string or DateTime object (if format is NULL), NULL if column is NULL, and 0 if column value is 0000-00-00
     *
     * @throws PropelException - if unable to parse/validate the date/time value.
     */
    public function getDateAnterieure($format = NULL)
    {
        if ($format === null) {
            return $this->date_anterieure;
        } else {
            return $this->date_anterieure instanceof \DateTimeInterface ? $this->date_anterieure->format($format) : null;
        }
    }

    /**
     * Get the [id_entite] column value.
     *
     * @return string
     */
    public function getIdEntite()
    {
        return $this->id_entite;
    }

    /**
     * Get the [observation] column value.
     *
     * @return string
     */
    public function getObservation()
    {
        return $this->observation;
    }

    /**
     * Get the [optionally formatted] temporal [created_at] column value.
     *
     *
     * @param      string $format The date/time format string (either date()-style or strftime()-style).
     *                            If format is NULL, then the raw DateTime object will be returned.
     *
     * @return string|DateTime Formatted date/time value as string or DateTime object (if format is NULL), NULL if column is NULL, and 0 if column value is 0000-00-00 00:00:00
     *
     * @throws PropelException - if unable to parse/validate the date/time value.
     */
    public function getCreatedAt($format = NULL)
    {
        if ($format === null) {
            return $this->created_at;
        } else {
            return $this->created_at instanceof \DateTimeInterface ? $this->created_at->format($format) : null;
        }
    }

    /**
     * Get the [optionally formatted] temporal [updated_at] column value.
     *
     *
     * @param      string $format The date/time format string (either date()-style or strftime()-style).
     *                            If format is NULL, then the raw DateTime object will be returned.
     *
     * @return string|DateTime Formatted date/time value as string or DateTime object (if format is NULL), NULL if column is NULL, and 0 if column value is 0000-00-00 00:00:00
     *
     * @throws PropelException - if unable to parse/validate the date/time value.
     */
    public function getUpdatedAt($format = NULL)
    {
        if ($format === null) {
            return $this->updated_at;
        } else {
            return $this->updated_at instanceof \DateTimeInterface ? $this->updated_at->format($format) : null;
        }
    }

    /**
     * Set the value of [id_compte] column.
     *
     * @param string $v new value
     * @return $this|\Compte The current object (for fluent API support)
     */
    public function setIdCompte($v)
    {
        if ($v !== null) {
            $v = (string) $v;
        }

        if ($this->id_compte !== $v) {
            $this->id_compte = $v;
            $this->modifiedColumns[CompteTableMap::COL_ID_COMPTE] = true;
        }

        return $this;
    } // setIdCompte()

    /**
     * Set the value of [ncompte] column.
     *
     * @param string $v new value
     * @return $this|\Compte The current object (for fluent API support)
     */
    public function setNcompte($v)
    {
        if ($v !== null) {
            $v = (string) $v;
        }

        if ($this->ncompte !== $v) {
            $this->ncompte = $v;
            $this->modifiedColumns[CompteTableMap::COL_NCOMPTE] = true;
        }

        return $this;
    } // setNcompte()

    /**
     * Set the value of [nom] column.
     *
     * @param string $v new value
     * @return $this|\Compte The current object (for fluent API support)
     */
    public function setNom($v)
    {
        if ($v !== null) {
            $v = (string) $v;
        }

        if ($this->nom !== $v) {
            $this->nom = $v;
            $this->modifiedColumns[CompteTableMap::COL_NOM] = true;
        }

        return $this;
    } // setNom()

    /**
     * Set the value of [nomcourt] column.
     * nomcourt
     * @param string $v new value
     * @return $this|\Compte The current object (for fluent API support)
     */
    public function setNomcourt($v)
    {
        if ($v !== null) {
            $v = (string) $v;
        }

        if ($this->nomcourt !== $v) {
            $this->nomcourt = $v;
            $this->modifiedColumns[CompteTableMap::COL_NOMCOURT] = true;
        }

        return $this;
    } // setNomcourt()

    /**
     * Set the value of [id_poste] column.
     *
     * @param string $v new value
     * @return $this|\Compte The current object (for fluent API support)
     */
    public function setIdPoste($v)
    {
        if ($v !== null) {
            $v = (string) $v;
        }

        if ($this->id_poste !== $v) {
            $this->id_poste = $v;
            $this->modifiedColumns[CompteTableMap::COL_ID_POSTE] = true;
        }

        if ($this->aPoste !== null && $this->aPoste->getIdPoste() !== $v) {
            $this->aPoste = null;
        }

        return $this;
    } // setIdPoste()

    /**
     * Set the value of [type_op] column.
     *
     * @param int $v new value
     * @return $this|\Compte The current object (for fluent API support)
     */
    public function setTypeOp($v)
    {
        if ($v !== null) {
            $v = (int) $v;
        }

        if ($this->type_op !== $v) {
            $this->type_op = $v;
            $this->modifiedColumns[CompteTableMap::COL_TYPE_OP] = true;
        }

        return $this;
    } // setTypeOp()

    /**
     * Set the value of [solde_anterieur] column.
     *
     * @param double $v new value
     * @return $this|\Compte The current object (for fluent API support)
     */
    public function setSoldeAnterieur($v)
    {
        if ($v !== null) {
            $v = (double) $v;
        }

        if ($this->solde_anterieur !== $v) {
            $this->solde_anterieur = $v;
            $this->modifiedColumns[CompteTableMap::COL_SOLDE_ANTERIEUR] = true;
        }

        return $this;
    } // setSoldeAnterieur()

    /**
     * Sets the value of [date_anterieure] column to a normalized version of the date/time value specified.
     *
     * @param  mixed $v string, integer (timestamp), or \DateTimeInterface value.
     *               Empty strings are treated as NULL.
     * @return $this|\Compte The current object (for fluent API support)
     */
    public function setDateAnterieure($v)
    {
        $dt = PropelDateTime::newInstance($v, null, 'DateTime');
        if ($this->date_anterieure !== null || $dt !== null) {
            if ($this->date_anterieure === null || $dt === null || $dt->format("Y-m-d") !== $this->date_anterieure->format("Y-m-d")) {
                $this->date_anterieure = $dt === null ? null : clone $dt;
                $this->modifiedColumns[CompteTableMap::COL_DATE_ANTERIEURE] = true;
            }
        } // if either are not null

        return $this;
    } // setDateAnterieure()

    /**
     * Set the value of [id_entite] column.
     *
     * @param string $v new value
     * @return $this|\Compte The current object (for fluent API support)
     */
    public function setIdEntite($v)
    {
        if ($v !== null) {
            $v = (string) $v;
        }

        if ($this->id_entite !== $v) {
            $this->id_entite = $v;
            $this->modifiedColumns[CompteTableMap::COL_ID_ENTITE] = true;
        }

        if ($this->aEntite !== null && $this->aEntite->getIdEntite() !== $v) {
            $this->aEntite = null;
        }

        return $this;
    } // setIdEntite()

    /**
     * Set the value of [observation] column.
     *
     * @param string $v new value
     * @return $this|\Compte The current object (for fluent API support)
     */
    public function setObservation($v)
    {
        if ($v !== null) {
            $v = (string) $v;
        }

        if ($this->observation !== $v) {
            $this->observation = $v;
            $this->modifiedColumns[CompteTableMap::COL_OBSERVATION] = true;
        }

        return $this;
    } // setObservation()

    /**
     * Sets the value of [created_at] column to a normalized version of the date/time value specified.
     *
     * @param  mixed $v string, integer (timestamp), or \DateTimeInterface value.
     *               Empty strings are treated as NULL.
     * @return $this|\Compte The current object (for fluent API support)
     */
    public function setCreatedAt($v)
    {
        $dt = PropelDateTime::newInstance($v, null, 'DateTime');
        if ($this->created_at !== null || $dt !== null) {
            if ($this->created_at === null || $dt === null || $dt->format("Y-m-d H:i:s.u") !== $this->created_at->format("Y-m-d H:i:s.u")) {
                $this->created_at = $dt === null ? null : clone $dt;
                $this->modifiedColumns[CompteTableMap::COL_CREATED_AT] = true;
            }
        } // if either are not null

        return $this;
    } // setCreatedAt()

    /**
     * Sets the value of [updated_at] column to a normalized version of the date/time value specified.
     *
     * @param  mixed $v string, integer (timestamp), or \DateTimeInterface value.
     *               Empty strings are treated as NULL.
     * @return $this|\Compte The current object (for fluent API support)
     */
    public function setUpdatedAt($v)
    {
        $dt = PropelDateTime::newInstance($v, null, 'DateTime');
        if ($this->updated_at !== null || $dt !== null) {
            if ($this->updated_at === null || $dt === null || $dt->format("Y-m-d H:i:s.u") !== $this->updated_at->format("Y-m-d H:i:s.u")) {
                $this->updated_at = $dt === null ? null : clone $dt;
                $this->modifiedColumns[CompteTableMap::COL_UPDATED_AT] = true;
            }
        } // if either are not null

        return $this;
    } // setUpdatedAt()

    /**
     * Indicates whether the columns in this object are only set to default values.
     *
     * This method can be used in conjunction with isModified() to indicate whether an object is both
     * modified _and_ has some values set which are non-default.
     *
     * @return boolean Whether the columns in this object are only been set with default values.
     */
    public function hasOnlyDefaultValues()
    {
            if ($this->ncompte !== '512NEF') {
                return false;
            }

            if ($this->nom !== 'Compte courant NEF') {
                return false;
            }

            if ($this->nomcourt !== 'CC NEF') {
                return false;
            }

            if ($this->id_poste !== '1') {
                return false;
            }

            if ($this->type_op !== 3) {
                return false;
            }

            if ($this->id_entite !== '1') {
                return false;
            }

        // otherwise, everything was equal, so return TRUE
        return true;
    } // hasOnlyDefaultValues()

    /**
     * Hydrates (populates) the object variables with values from the database resultset.
     *
     * An offset (0-based "start column") is specified so that objects can be hydrated
     * with a subset of the columns in the resultset rows.  This is needed, for example,
     * for results of JOIN queries where the resultset row includes columns from two or
     * more tables.
     *
     * @param array   $row       The row returned by DataFetcher->fetch().
     * @param int     $startcol  0-based offset column which indicates which restultset column to start with.
     * @param boolean $rehydrate Whether this object is being re-hydrated from the database.
     * @param string  $indexType The index type of $row. Mostly DataFetcher->getIndexType().
                                  One of the class type constants TableMap::TYPE_PHPNAME, TableMap::TYPE_CAMELNAME
     *                            TableMap::TYPE_COLNAME, TableMap::TYPE_FIELDNAME, TableMap::TYPE_NUM.
     *
     * @return int             next starting column
     * @throws PropelException - Any caught Exception will be rewrapped as a PropelException.
     */
    public function hydrate($row, $startcol = 0, $rehydrate = false, $indexType = TableMap::TYPE_NUM)
    {
        try {

            $col = $row[TableMap::TYPE_NUM == $indexType ? 0 + $startcol : CompteTableMap::translateFieldName('IdCompte', TableMap::TYPE_PHPNAME, $indexType)];
            $this->id_compte = (null !== $col) ? (string) $col : null;

            $col = $row[TableMap::TYPE_NUM == $indexType ? 1 + $startcol : CompteTableMap::translateFieldName('Ncompte', TableMap::TYPE_PHPNAME, $indexType)];
            $this->ncompte = (null !== $col) ? (string) $col : null;

            $col = $row[TableMap::TYPE_NUM == $indexType ? 2 + $startcol : CompteTableMap::translateFieldName('Nom', TableMap::TYPE_PHPNAME, $indexType)];
            $this->nom = (null !== $col) ? (string) $col : null;

            $col = $row[TableMap::TYPE_NUM == $indexType ? 3 + $startcol : CompteTableMap::translateFieldName('Nomcourt', TableMap::TYPE_PHPNAME, $indexType)];
            $this->nomcourt = (null !== $col) ? (string) $col : null;

            $col = $row[TableMap::TYPE_NUM == $indexType ? 4 + $startcol : CompteTableMap::translateFieldName('IdPoste', TableMap::TYPE_PHPNAME, $indexType)];
            $this->id_poste = (null !== $col) ? (string) $col : null;

            $col = $row[TableMap::TYPE_NUM == $indexType ? 5 + $startcol : CompteTableMap::translateFieldName('TypeOp', TableMap::TYPE_PHPNAME, $indexType)];
            $this->type_op = (null !== $col) ? (int) $col : null;

            $col = $row[TableMap::TYPE_NUM == $indexType ? 6 + $startcol : CompteTableMap::translateFieldName('SoldeAnterieur', TableMap::TYPE_PHPNAME, $indexType)];
            $this->solde_anterieur = (null !== $col) ? (double) $col : null;

            $col = $row[TableMap::TYPE_NUM == $indexType ? 7 + $startcol : CompteTableMap::translateFieldName('DateAnterieure', TableMap::TYPE_PHPNAME, $indexType)];
            if ($col === '0000-00-00') {
                $col = null;
            }
            $this->date_anterieure = (null !== $col) ? PropelDateTime::newInstance($col, null, 'DateTime') : null;

            $col = $row[TableMap::TYPE_NUM == $indexType ? 8 + $startcol : CompteTableMap::translateFieldName('IdEntite', TableMap::TYPE_PHPNAME, $indexType)];
            $this->id_entite = (null !== $col) ? (string) $col : null;

            $col = $row[TableMap::TYPE_NUM == $indexType ? 9 + $startcol : CompteTableMap::translateFieldName('Observation', TableMap::TYPE_PHPNAME, $indexType)];
            $this->observation = (null !== $col) ? (string) $col : null;

            $col = $row[TableMap::TYPE_NUM == $indexType ? 10 + $startcol : CompteTableMap::translateFieldName('CreatedAt', TableMap::TYPE_PHPNAME, $indexType)];
            if ($col === '0000-00-00 00:00:00') {
                $col = null;
            }
            $this->created_at = (null !== $col) ? PropelDateTime::newInstance($col, null, 'DateTime') : null;

            $col = $row[TableMap::TYPE_NUM == $indexType ? 11 + $startcol : CompteTableMap::translateFieldName('UpdatedAt', TableMap::TYPE_PHPNAME, $indexType)];
            if ($col === '0000-00-00 00:00:00') {
                $col = null;
            }
            $this->updated_at = (null !== $col) ? PropelDateTime::newInstance($col, null, 'DateTime') : null;
            $this->resetModified();

            $this->setNew(false);

            if ($rehydrate) {
                $this->ensureConsistency();
            }

            return $startcol + 12; // 12 = CompteTableMap::NUM_HYDRATE_COLUMNS.

        } catch (Exception $e) {
            throw new PropelException(sprintf('Error populating %s object', '\\Compte'), 0, $e);
        }
    }

    /**
     * Checks and repairs the internal consistency of the object.
     *
     * This method is executed after an already-instantiated object is re-hydrated
     * from the database.  It exists to check any foreign keys to make sure that
     * the objects related to the current object are correct based on foreign key.
     *
     * You can override this method in the stub class, but you should always invoke
     * the base method from the overridden method (i.e. parent::ensureConsistency()),
     * in case your model changes.
     *
     * @throws PropelException
     */
    public function ensureConsistency()
    {
        if ($this->aPoste !== null && $this->id_poste !== $this->aPoste->getIdPoste()) {
            $this->aPoste = null;
        }
        if ($this->aEntite !== null && $this->id_entite !== $this->aEntite->getIdEntite()) {
            $this->aEntite = null;
        }
    } // ensureConsistency

    /**
     * Reloads this object from datastore based on primary key and (optionally) resets all associated objects.
     *
     * This will only work if the object has been saved and has a valid primary key set.
     *
     * @param      boolean $deep (optional) Whether to also de-associated any related objects.
     * @param      ConnectionInterface $con (optional) The ConnectionInterface connection to use.
     * @return void
     * @throws PropelException - if this object is deleted, unsaved or doesn't have pk match in db
     */
    public function reload($deep = false, ConnectionInterface $con = null)
    {
        if ($this->isDeleted()) {
            throw new PropelException("Cannot reload a deleted object.");
        }

        if ($this->isNew()) {
            throw new PropelException("Cannot reload an unsaved object.");
        }

        if ($con === null) {
            $con = Propel::getServiceContainer()->getReadConnection(CompteTableMap::DATABASE_NAME);
        }

        // We don't need to alter the object instance pool; we're just modifying this instance
        // already in the pool.

        $dataFetcher = ChildCompteQuery::create(null, $this->buildPkeyCriteria())->setFormatter(ModelCriteria::FORMAT_STATEMENT)->find($con);
        $row = $dataFetcher->fetch();
        $dataFetcher->close();
        if (!$row) {
            throw new PropelException('Cannot find matching row in the database to reload object values.');
        }
        $this->hydrate($row, 0, true, $dataFetcher->getIndexType()); // rehydrate

        if ($deep) {  // also de-associate any related objects?

            $this->aEntite = null;
            $this->aPoste = null;
            $this->collEcritures = null;

            $this->collJournals = null;

            $this->collPrestations = null;

            $this->collTresors = null;

            $this->collTvas = null;

        } // if (deep)
    }

    /**
     * Removes this object from datastore and sets delete attribute.
     *
     * @param      ConnectionInterface $con
     * @return void
     * @throws PropelException
     * @see Compte::setDeleted()
     * @see Compte::isDeleted()
     */
    public function delete(ConnectionInterface $con = null)
    {
        if ($this->isDeleted()) {
            throw new PropelException("This object has already been deleted.");
        }

        if ($con === null) {
            $con = Propel::getServiceContainer()->getWriteConnection(CompteTableMap::DATABASE_NAME);
        }

        $con->transaction(function () use ($con) {
            $deleteQuery = ChildCompteQuery::create()
                ->filterByPrimaryKey($this->getPrimaryKey());
            $ret = $this->preDelete($con);
            // archivable behavior
            if ($ret) {
                if ($this->archiveOnDelete) {
                    // do nothing yet. The object will be archived later when calling ChildCompteQuery::delete().
                } else {
                    $deleteQuery->setArchiveOnDelete(false);
                    $this->archiveOnDelete = true;
                }
            }

            if ($ret) {
                $deleteQuery->delete($con);
                $this->postDelete($con);
                $this->setDeleted(true);
            }
        });
    }

    /**
     * Persists this object to the database.
     *
     * If the object is new, it inserts it; otherwise an update is performed.
     * All modified related objects will also be persisted in the doSave()
     * method.  This method wraps all precipitate database operations in a
     * single transaction.
     *
     * @param      ConnectionInterface $con
     * @return int             The number of rows affected by this insert/update and any referring fk objects' save() operations.
     * @throws PropelException
     * @see doSave()
     */
    public function save(ConnectionInterface $con = null)
    {
        if ($this->isDeleted()) {
            throw new PropelException("You cannot save an object that has been deleted.");
        }

        if ($this->alreadyInSave) {
            return 0;
        }

        if ($con === null) {
            $con = Propel::getServiceContainer()->getWriteConnection(CompteTableMap::DATABASE_NAME);
        }

        return $con->transaction(function () use ($con) {
            $ret = $this->preSave($con);
            $isInsert = $this->isNew();
            if ($isInsert) {
                $ret = $ret && $this->preInsert($con);
                // timestampable behavior

                if (!$this->isColumnModified(CompteTableMap::COL_CREATED_AT)) {
                    $this->setCreatedAt(\Propel\Runtime\Util\PropelDateTime::createHighPrecision());
                }
                if (!$this->isColumnModified(CompteTableMap::COL_UPDATED_AT)) {
                    $this->setUpdatedAt(\Propel\Runtime\Util\PropelDateTime::createHighPrecision());
                }
            } else {
                $ret = $ret && $this->preUpdate($con);
                // timestampable behavior
                if ($this->isModified() && !$this->isColumnModified(CompteTableMap::COL_UPDATED_AT)) {
                    $this->setUpdatedAt(\Propel\Runtime\Util\PropelDateTime::createHighPrecision());
                }
            }
            if ($ret) {
                $affectedRows = $this->doSave($con);
                if ($isInsert) {
                    $this->postInsert($con);
                } else {
                    $this->postUpdate($con);
                }
                $this->postSave($con);
                CompteTableMap::addInstanceToPool($this);
            } else {
                $affectedRows = 0;
            }

            return $affectedRows;
        });
    }

    /**
     * Performs the work of inserting or updating the row in the database.
     *
     * If the object is new, it inserts it; otherwise an update is performed.
     * All related objects are also updated in this method.
     *
     * @param      ConnectionInterface $con
     * @return int             The number of rows affected by this insert/update and any referring fk objects' save() operations.
     * @throws PropelException
     * @see save()
     */
    protected function doSave(ConnectionInterface $con)
    {
        $affectedRows = 0; // initialize var to track total num of affected rows
        if (!$this->alreadyInSave) {
            $this->alreadyInSave = true;

            // We call the save method on the following object(s) if they
            // were passed to this object by their corresponding set
            // method.  This object relates to these object(s) by a
            // foreign key reference.

            if ($this->aEntite !== null) {
                if ($this->aEntite->isModified() || $this->aEntite->isNew()) {
                    $affectedRows += $this->aEntite->save($con);
                }
                $this->setEntite($this->aEntite);
            }

            if ($this->aPoste !== null) {
                if ($this->aPoste->isModified() || $this->aPoste->isNew()) {
                    $affectedRows += $this->aPoste->save($con);
                }
                $this->setPoste($this->aPoste);
            }

            if ($this->isNew() || $this->isModified()) {
                // persist changes
                if ($this->isNew()) {
                    $this->doInsert($con);
                    $affectedRows += 1;
                } else {
                    $affectedRows += $this->doUpdate($con);
                }
                $this->resetModified();
            }

            if ($this->ecrituresScheduledForDeletion !== null) {
                if (!$this->ecrituresScheduledForDeletion->isEmpty()) {
                    \EcritureQuery::create()
                        ->filterByPrimaryKeys($this->ecrituresScheduledForDeletion->getPrimaryKeys(false))
                        ->delete($con);
                    $this->ecrituresScheduledForDeletion = null;
                }
            }

            if ($this->collEcritures !== null) {
                foreach ($this->collEcritures as $referrerFK) {
                    if (!$referrerFK->isDeleted() && ($referrerFK->isNew() || $referrerFK->isModified())) {
                        $affectedRows += $referrerFK->save($con);
                    }
                }
            }

            if ($this->journalsScheduledForDeletion !== null) {
                if (!$this->journalsScheduledForDeletion->isEmpty()) {
                    \JournalQuery::create()
                        ->filterByPrimaryKeys($this->journalsScheduledForDeletion->getPrimaryKeys(false))
                        ->delete($con);
                    $this->journalsScheduledForDeletion = null;
                }
            }

            if ($this->collJournals !== null) {
                foreach ($this->collJournals as $referrerFK) {
                    if (!$referrerFK->isDeleted() && ($referrerFK->isNew() || $referrerFK->isModified())) {
                        $affectedRows += $referrerFK->save($con);
                    }
                }
            }

            if ($this->prestationsScheduledForDeletion !== null) {
                if (!$this->prestationsScheduledForDeletion->isEmpty()) {
                    \PrestationQuery::create()
                        ->filterByPrimaryKeys($this->prestationsScheduledForDeletion->getPrimaryKeys(false))
                        ->delete($con);
                    $this->prestationsScheduledForDeletion = null;
                }
            }

            if ($this->collPrestations !== null) {
                foreach ($this->collPrestations as $referrerFK) {
                    if (!$referrerFK->isDeleted() && ($referrerFK->isNew() || $referrerFK->isModified())) {
                        $affectedRows += $referrerFK->save($con);
                    }
                }
            }

            if ($this->tresorsScheduledForDeletion !== null) {
                if (!$this->tresorsScheduledForDeletion->isEmpty()) {
                    \TresorQuery::create()
                        ->filterByPrimaryKeys($this->tresorsScheduledForDeletion->getPrimaryKeys(false))
                        ->delete($con);
                    $this->tresorsScheduledForDeletion = null;
                }
            }

            if ($this->collTresors !== null) {
                foreach ($this->collTresors as $referrerFK) {
                    if (!$referrerFK->isDeleted() && ($referrerFK->isNew() || $referrerFK->isModified())) {
                        $affectedRows += $referrerFK->save($con);
                    }
                }
            }

            if ($this->tvasScheduledForDeletion !== null) {
                if (!$this->tvasScheduledForDeletion->isEmpty()) {
                    \TvaQuery::create()
                        ->filterByPrimaryKeys($this->tvasScheduledForDeletion->getPrimaryKeys(false))
                        ->delete($con);
                    $this->tvasScheduledForDeletion = null;
                }
            }

            if ($this->collTvas !== null) {
                foreach ($this->collTvas as $referrerFK) {
                    if (!$referrerFK->isDeleted() && ($referrerFK->isNew() || $referrerFK->isModified())) {
                        $affectedRows += $referrerFK->save($con);
                    }
                }
            }

            $this->alreadyInSave = false;

        }

        return $affectedRows;
    } // doSave()

    /**
     * Insert the row in the database.
     *
     * @param      ConnectionInterface $con
     *
     * @throws PropelException
     * @see doSave()
     */
    protected function doInsert(ConnectionInterface $con)
    {
        $modifiedColumns = array();
        $index = 0;

        $this->modifiedColumns[CompteTableMap::COL_ID_COMPTE] = true;
        if (null !== $this->id_compte) {
            throw new PropelException('Cannot insert a value for auto-increment primary key (' . CompteTableMap::COL_ID_COMPTE . ')');
        }

         // check the columns in natural order for more readable SQL queries
        if ($this->isColumnModified(CompteTableMap::COL_ID_COMPTE)) {
            $modifiedColumns[':p' . $index++]  = '`id_compte`';
        }
        if ($this->isColumnModified(CompteTableMap::COL_NCOMPTE)) {
            $modifiedColumns[':p' . $index++]  = '`ncompte`';
        }
        if ($this->isColumnModified(CompteTableMap::COL_NOM)) {
            $modifiedColumns[':p' . $index++]  = '`nom`';
        }
        if ($this->isColumnModified(CompteTableMap::COL_NOMCOURT)) {
            $modifiedColumns[':p' . $index++]  = '`nomcourt`';
        }
        if ($this->isColumnModified(CompteTableMap::COL_ID_POSTE)) {
            $modifiedColumns[':p' . $index++]  = '`id_poste`';
        }
        if ($this->isColumnModified(CompteTableMap::COL_TYPE_OP)) {
            $modifiedColumns[':p' . $index++]  = '`type_op`';
        }
        if ($this->isColumnModified(CompteTableMap::COL_SOLDE_ANTERIEUR)) {
            $modifiedColumns[':p' . $index++]  = '`solde_anterieur`';
        }
        if ($this->isColumnModified(CompteTableMap::COL_DATE_ANTERIEURE)) {
            $modifiedColumns[':p' . $index++]  = '`date_anterieure`';
        }
        if ($this->isColumnModified(CompteTableMap::COL_ID_ENTITE)) {
            $modifiedColumns[':p' . $index++]  = '`id_entite`';
        }
        if ($this->isColumnModified(CompteTableMap::COL_OBSERVATION)) {
            $modifiedColumns[':p' . $index++]  = '`observation`';
        }
        if ($this->isColumnModified(CompteTableMap::COL_CREATED_AT)) {
            $modifiedColumns[':p' . $index++]  = '`created_at`';
        }
        if ($this->isColumnModified(CompteTableMap::COL_UPDATED_AT)) {
            $modifiedColumns[':p' . $index++]  = '`updated_at`';
        }

        $sql = sprintf(
            'INSERT INTO `assc_comptes` (%s) VALUES (%s)',
            implode(', ', $modifiedColumns),
            implode(', ', array_keys($modifiedColumns))
        );

        try {
            $stmt = $con->prepare($sql);
            foreach ($modifiedColumns as $identifier => $columnName) {
                switch ($columnName) {
                    case '`id_compte`':
                        $stmt->bindValue($identifier, $this->id_compte, PDO::PARAM_INT);
                        break;
                    case '`ncompte`':
                        $stmt->bindValue($identifier, $this->ncompte, PDO::PARAM_STR);
                        break;
                    case '`nom`':
                        $stmt->bindValue($identifier, $this->nom, PDO::PARAM_STR);
                        break;
                    case '`nomcourt`':
                        $stmt->bindValue($identifier, $this->nomcourt, PDO::PARAM_STR);
                        break;
                    case '`id_poste`':
                        $stmt->bindValue($identifier, $this->id_poste, PDO::PARAM_INT);
                        break;
                    case '`type_op`':
                        $stmt->bindValue($identifier, $this->type_op, PDO::PARAM_INT);
                        break;
                    case '`solde_anterieur`':
                        $stmt->bindValue($identifier, $this->solde_anterieur, PDO::PARAM_STR);
                        break;
                    case '`date_anterieure`':
                        $stmt->bindValue($identifier, $this->date_anterieure ? $this->date_anterieure->format("Y-m-d H:i:s.u") : null, PDO::PARAM_STR);
                        break;
                    case '`id_entite`':
                        $stmt->bindValue($identifier, $this->id_entite, PDO::PARAM_INT);
                        break;
                    case '`observation`':
                        $stmt->bindValue($identifier, $this->observation, PDO::PARAM_STR);
                        break;
                    case '`created_at`':
                        $stmt->bindValue($identifier, $this->created_at ? $this->created_at->format("Y-m-d H:i:s.u") : null, PDO::PARAM_STR);
                        break;
                    case '`updated_at`':
                        $stmt->bindValue($identifier, $this->updated_at ? $this->updated_at->format("Y-m-d H:i:s.u") : null, PDO::PARAM_STR);
                        break;
                }
            }
            $stmt->execute();
        } catch (Exception $e) {
            Propel::log($e->getMessage(), Propel::LOG_ERR);
            throw new PropelException(sprintf('Unable to execute INSERT statement [%s]', $sql), 0, $e);
        }

        try {
            $pk = $con->lastInsertId();
        } catch (Exception $e) {
            throw new PropelException('Unable to get autoincrement id.', 0, $e);
        }
        $this->setIdCompte($pk);

        $this->setNew(false);
    }

    /**
     * Update the row in the database.
     *
     * @param      ConnectionInterface $con
     *
     * @return Integer Number of updated rows
     * @see doSave()
     */
    protected function doUpdate(ConnectionInterface $con)
    {
        $selectCriteria = $this->buildPkeyCriteria();
        $valuesCriteria = $this->buildCriteria();

        return $selectCriteria->doUpdate($valuesCriteria, $con);
    }

    /**
     * Retrieves a field from the object by name passed in as a string.
     *
     * @param      string $name name
     * @param      string $type The type of fieldname the $name is of:
     *                     one of the class type constants TableMap::TYPE_PHPNAME, TableMap::TYPE_CAMELNAME
     *                     TableMap::TYPE_COLNAME, TableMap::TYPE_FIELDNAME, TableMap::TYPE_NUM.
     *                     Defaults to TableMap::TYPE_FIELDNAME.
     * @return mixed Value of field.
     */
    public function getByName($name, $type = TableMap::TYPE_FIELDNAME)
    {
        $pos = CompteTableMap::translateFieldName($name, $type, TableMap::TYPE_NUM);
        $field = $this->getByPosition($pos);

        return $field;
    }

    /**
     * Retrieves a field from the object by Position as specified in the xml schema.
     * Zero-based.
     *
     * @param      int $pos position in xml schema
     * @return mixed Value of field at $pos
     */
    public function getByPosition($pos)
    {
        switch ($pos) {
            case 0:
                return $this->getIdCompte();
                break;
            case 1:
                return $this->getNcompte();
                break;
            case 2:
                return $this->getNom();
                break;
            case 3:
                return $this->getNomcourt();
                break;
            case 4:
                return $this->getIdPoste();
                break;
            case 5:
                return $this->getTypeOp();
                break;
            case 6:
                return $this->getSoldeAnterieur();
                break;
            case 7:
                return $this->getDateAnterieure();
                break;
            case 8:
                return $this->getIdEntite();
                break;
            case 9:
                return $this->getObservation();
                break;
            case 10:
                return $this->getCreatedAt();
                break;
            case 11:
                return $this->getUpdatedAt();
                break;
            default:
                return null;
                break;
        } // switch()
    }

    /**
     * Exports the object as an array.
     *
     * You can specify the key type of the array by passing one of the class
     * type constants.
     *
     * @param     string  $keyType (optional) One of the class type constants TableMap::TYPE_PHPNAME, TableMap::TYPE_CAMELNAME,
     *                    TableMap::TYPE_COLNAME, TableMap::TYPE_FIELDNAME, TableMap::TYPE_NUM.
     *                    Defaults to TableMap::TYPE_FIELDNAME.
     * @param     boolean $includeLazyLoadColumns (optional) Whether to include lazy loaded columns. Defaults to TRUE.
     * @param     array $alreadyDumpedObjects List of objects to skip to avoid recursion
     * @param     boolean $includeForeignObjects (optional) Whether to include hydrated related objects. Default to FALSE.
     *
     * @return array an associative array containing the field names (as keys) and field values
     */
    public function toArray($keyType = TableMap::TYPE_FIELDNAME, $includeLazyLoadColumns = true, $alreadyDumpedObjects = array(), $includeForeignObjects = false)
    {

        if (isset($alreadyDumpedObjects['Compte'][$this->hashCode()])) {
            return '*RECURSION*';
        }
        $alreadyDumpedObjects['Compte'][$this->hashCode()] = true;
        $keys = CompteTableMap::getFieldNames($keyType);
        $result = array(
            $keys[0] => $this->getIdCompte(),
            $keys[1] => $this->getNcompte(),
            $keys[2] => $this->getNom(),
            $keys[3] => $this->getNomcourt(),
            $keys[4] => $this->getIdPoste(),
            $keys[5] => $this->getTypeOp(),
            $keys[6] => $this->getSoldeAnterieur(),
            $keys[7] => $this->getDateAnterieure(),
            $keys[8] => $this->getIdEntite(),
            $keys[9] => $this->getObservation(),
            $keys[10] => $this->getCreatedAt(),
            $keys[11] => $this->getUpdatedAt(),
        );
        if ($result[$keys[7]] instanceof \DateTimeInterface) {
            $result[$keys[7]] = $result[$keys[7]]->format('c');
        }

        if ($result[$keys[10]] instanceof \DateTimeInterface) {
            $result[$keys[10]] = $result[$keys[10]]->format('c');
        }

        if ($result[$keys[11]] instanceof \DateTimeInterface) {
            $result[$keys[11]] = $result[$keys[11]]->format('c');
        }

        $virtualColumns = $this->virtualColumns;
        foreach ($virtualColumns as $key => $virtualColumn) {
            $result[$key] = $virtualColumn;
        }

        if ($includeForeignObjects) {
            if (null !== $this->aEntite) {

                switch ($keyType) {
                    case TableMap::TYPE_CAMELNAME:
                        $key = 'entite';
                        break;
                    case TableMap::TYPE_FIELDNAME:
                        $key = 'asso_entites';
                        break;
                    default:
                        $key = 'Entite';
                }

                $result[$key] = $this->aEntite->toArray($keyType, $includeLazyLoadColumns,  $alreadyDumpedObjects, true);
            }
            if (null !== $this->aPoste) {

                switch ($keyType) {
                    case TableMap::TYPE_CAMELNAME:
                        $key = 'poste';
                        break;
                    case TableMap::TYPE_FIELDNAME:
                        $key = 'assc_postes';
                        break;
                    default:
                        $key = 'Poste';
                }

                $result[$key] = $this->aPoste->toArray($keyType, $includeLazyLoadColumns,  $alreadyDumpedObjects, true);
            }
            if (null !== $this->collEcritures) {

                switch ($keyType) {
                    case TableMap::TYPE_CAMELNAME:
                        $key = 'ecritures';
                        break;
                    case TableMap::TYPE_FIELDNAME:
                        $key = 'assc_ecrituress';
                        break;
                    default:
                        $key = 'Ecritures';
                }

                $result[$key] = $this->collEcritures->toArray(null, false, $keyType, $includeLazyLoadColumns, $alreadyDumpedObjects);
            }
            if (null !== $this->collJournals) {

                switch ($keyType) {
                    case TableMap::TYPE_CAMELNAME:
                        $key = 'journals';
                        break;
                    case TableMap::TYPE_FIELDNAME:
                        $key = 'assc_journalss';
                        break;
                    default:
                        $key = 'Journals';
                }

                $result[$key] = $this->collJournals->toArray(null, false, $keyType, $includeLazyLoadColumns, $alreadyDumpedObjects);
            }
            if (null !== $this->collPrestations) {

                switch ($keyType) {
                    case TableMap::TYPE_CAMELNAME:
                        $key = 'prestations';
                        break;
                    case TableMap::TYPE_FIELDNAME:
                        $key = 'asso_prestationss';
                        break;
                    default:
                        $key = 'Prestations';
                }

                $result[$key] = $this->collPrestations->toArray(null, false, $keyType, $includeLazyLoadColumns, $alreadyDumpedObjects);
            }
            if (null !== $this->collTresors) {

                switch ($keyType) {
                    case TableMap::TYPE_CAMELNAME:
                        $key = 'tresors';
                        break;
                    case TableMap::TYPE_FIELDNAME:
                        $key = 'asso_tresorss';
                        break;
                    default:
                        $key = 'Tresors';
                }

                $result[$key] = $this->collTresors->toArray(null, false, $keyType, $includeLazyLoadColumns, $alreadyDumpedObjects);
            }
            if (null !== $this->collTvas) {

                switch ($keyType) {
                    case TableMap::TYPE_CAMELNAME:
                        $key = 'tvas';
                        break;
                    case TableMap::TYPE_FIELDNAME:
                        $key = 'asso_tvass';
                        break;
                    default:
                        $key = 'Tvas';
                }

                $result[$key] = $this->collTvas->toArray(null, false, $keyType, $includeLazyLoadColumns, $alreadyDumpedObjects);
            }
        }

        return $result;
    }

    /**
     * Sets a field from the object by name passed in as a string.
     *
     * @param  string $name
     * @param  mixed  $value field value
     * @param  string $type The type of fieldname the $name is of:
     *                one of the class type constants TableMap::TYPE_PHPNAME, TableMap::TYPE_CAMELNAME
     *                TableMap::TYPE_COLNAME, TableMap::TYPE_FIELDNAME, TableMap::TYPE_NUM.
     *                Defaults to TableMap::TYPE_FIELDNAME.
     * @return $this|\Compte
     */
    public function setByName($name, $value, $type = TableMap::TYPE_FIELDNAME)
    {
        $pos = CompteTableMap::translateFieldName($name, $type, TableMap::TYPE_NUM);

        return $this->setByPosition($pos, $value);
    }

    /**
     * Sets a field from the object by Position as specified in the xml schema.
     * Zero-based.
     *
     * @param  int $pos position in xml schema
     * @param  mixed $value field value
     * @return $this|\Compte
     */
    public function setByPosition($pos, $value)
    {
        switch ($pos) {
            case 0:
                $this->setIdCompte($value);
                break;
            case 1:
                $this->setNcompte($value);
                break;
            case 2:
                $this->setNom($value);
                break;
            case 3:
                $this->setNomcourt($value);
                break;
            case 4:
                $this->setIdPoste($value);
                break;
            case 5:
                $this->setTypeOp($value);
                break;
            case 6:
                $this->setSoldeAnterieur($value);
                break;
            case 7:
                $this->setDateAnterieure($value);
                break;
            case 8:
                $this->setIdEntite($value);
                break;
            case 9:
                $this->setObservation($value);
                break;
            case 10:
                $this->setCreatedAt($value);
                break;
            case 11:
                $this->setUpdatedAt($value);
                break;
        } // switch()

        return $this;
    }

    /**
     * Populates the object using an array.
     *
     * This is particularly useful when populating an object from one of the
     * request arrays (e.g. $_POST).  This method goes through the column
     * names, checking to see whether a matching key exists in populated
     * array. If so the setByName() method is called for that column.
     *
     * You can specify the key type of the array by additionally passing one
     * of the class type constants TableMap::TYPE_PHPNAME, TableMap::TYPE_CAMELNAME,
     * TableMap::TYPE_COLNAME, TableMap::TYPE_FIELDNAME, TableMap::TYPE_NUM.
     * The default key type is the column's TableMap::TYPE_FIELDNAME.
     *
     * @param      array  $arr     An array to populate the object from.
     * @param      string $keyType The type of keys the array uses.
     * @return void
     */
    public function fromArray($arr, $keyType = TableMap::TYPE_FIELDNAME)
    {
        $keys = CompteTableMap::getFieldNames($keyType);

        if (array_key_exists($keys[0], $arr)) {
            $this->setIdCompte($arr[$keys[0]]);
        }
        if (array_key_exists($keys[1], $arr)) {
            $this->setNcompte($arr[$keys[1]]);
        }
        if (array_key_exists($keys[2], $arr)) {
            $this->setNom($arr[$keys[2]]);
        }
        if (array_key_exists($keys[3], $arr)) {
            $this->setNomcourt($arr[$keys[3]]);
        }
        if (array_key_exists($keys[4], $arr)) {
            $this->setIdPoste($arr[$keys[4]]);
        }
        if (array_key_exists($keys[5], $arr)) {
            $this->setTypeOp($arr[$keys[5]]);
        }
        if (array_key_exists($keys[6], $arr)) {
            $this->setSoldeAnterieur($arr[$keys[6]]);
        }
        if (array_key_exists($keys[7], $arr)) {
            $this->setDateAnterieure($arr[$keys[7]]);
        }
        if (array_key_exists($keys[8], $arr)) {
            $this->setIdEntite($arr[$keys[8]]);
        }
        if (array_key_exists($keys[9], $arr)) {
            $this->setObservation($arr[$keys[9]]);
        }
        if (array_key_exists($keys[10], $arr)) {
            $this->setCreatedAt($arr[$keys[10]]);
        }
        if (array_key_exists($keys[11], $arr)) {
            $this->setUpdatedAt($arr[$keys[11]]);
        }
    }

     /**
     * Populate the current object from a string, using a given parser format
     * <code>
     * $book = new Book();
     * $book->importFrom('JSON', '{"Id":9012,"Title":"Don Juan","ISBN":"0140422161","Price":12.99,"PublisherId":1234,"AuthorId":5678}');
     * </code>
     *
     * You can specify the key type of the array by additionally passing one
     * of the class type constants TableMap::TYPE_PHPNAME, TableMap::TYPE_CAMELNAME,
     * TableMap::TYPE_COLNAME, TableMap::TYPE_FIELDNAME, TableMap::TYPE_NUM.
     * The default key type is the column's TableMap::TYPE_FIELDNAME.
     *
     * @param mixed $parser A AbstractParser instance,
     *                       or a format name ('XML', 'YAML', 'JSON', 'CSV')
     * @param string $data The source data to import from
     * @param string $keyType The type of keys the array uses.
     *
     * @return $this|\Compte The current object, for fluid interface
     */
    public function importFrom($parser, $data, $keyType = TableMap::TYPE_FIELDNAME)
    {
        if (!$parser instanceof AbstractParser) {
            $parser = AbstractParser::getParser($parser);
        }

        $this->fromArray($parser->toArray($data), $keyType);

        return $this;
    }

    /**
     * Build a Criteria object containing the values of all modified columns in this object.
     *
     * @return Criteria The Criteria object containing all modified values.
     */
    public function buildCriteria()
    {
        $criteria = new Criteria(CompteTableMap::DATABASE_NAME);

        if ($this->isColumnModified(CompteTableMap::COL_ID_COMPTE)) {
            $criteria->add(CompteTableMap::COL_ID_COMPTE, $this->id_compte);
        }
        if ($this->isColumnModified(CompteTableMap::COL_NCOMPTE)) {
            $criteria->add(CompteTableMap::COL_NCOMPTE, $this->ncompte);
        }
        if ($this->isColumnModified(CompteTableMap::COL_NOM)) {
            $criteria->add(CompteTableMap::COL_NOM, $this->nom);
        }
        if ($this->isColumnModified(CompteTableMap::COL_NOMCOURT)) {
            $criteria->add(CompteTableMap::COL_NOMCOURT, $this->nomcourt);
        }
        if ($this->isColumnModified(CompteTableMap::COL_ID_POSTE)) {
            $criteria->add(CompteTableMap::COL_ID_POSTE, $this->id_poste);
        }
        if ($this->isColumnModified(CompteTableMap::COL_TYPE_OP)) {
            $criteria->add(CompteTableMap::COL_TYPE_OP, $this->type_op);
        }
        if ($this->isColumnModified(CompteTableMap::COL_SOLDE_ANTERIEUR)) {
            $criteria->add(CompteTableMap::COL_SOLDE_ANTERIEUR, $this->solde_anterieur);
        }
        if ($this->isColumnModified(CompteTableMap::COL_DATE_ANTERIEURE)) {
            $criteria->add(CompteTableMap::COL_DATE_ANTERIEURE, $this->date_anterieure);
        }
        if ($this->isColumnModified(CompteTableMap::COL_ID_ENTITE)) {
            $criteria->add(CompteTableMap::COL_ID_ENTITE, $this->id_entite);
        }
        if ($this->isColumnModified(CompteTableMap::COL_OBSERVATION)) {
            $criteria->add(CompteTableMap::COL_OBSERVATION, $this->observation);
        }
        if ($this->isColumnModified(CompteTableMap::COL_CREATED_AT)) {
            $criteria->add(CompteTableMap::COL_CREATED_AT, $this->created_at);
        }
        if ($this->isColumnModified(CompteTableMap::COL_UPDATED_AT)) {
            $criteria->add(CompteTableMap::COL_UPDATED_AT, $this->updated_at);
        }

        return $criteria;
    }

    /**
     * Builds a Criteria object containing the primary key for this object.
     *
     * Unlike buildCriteria() this method includes the primary key values regardless
     * of whether or not they have been modified.
     *
     * @throws LogicException if no primary key is defined
     *
     * @return Criteria The Criteria object containing value(s) for primary key(s).
     */
    public function buildPkeyCriteria()
    {
        $criteria = ChildCompteQuery::create();
        $criteria->add(CompteTableMap::COL_ID_COMPTE, $this->id_compte);

        return $criteria;
    }

    /**
     * If the primary key is not null, return the hashcode of the
     * primary key. Otherwise, return the hash code of the object.
     *
     * @return int Hashcode
     */
    public function hashCode()
    {
        $validPk = null !== $this->getIdCompte();

        $validPrimaryKeyFKs = 0;
        $primaryKeyFKs = [];

        if ($validPk) {
            return crc32(json_encode($this->getPrimaryKey(), JSON_UNESCAPED_UNICODE));
        } elseif ($validPrimaryKeyFKs) {
            return crc32(json_encode($primaryKeyFKs, JSON_UNESCAPED_UNICODE));
        }

        return spl_object_hash($this);
    }

    /**
     * Returns the primary key for this object (row).
     * @return string
     */
    public function getPrimaryKey()
    {
        return $this->getIdCompte();
    }

    /**
     * Generic method to set the primary key (id_compte column).
     *
     * @param       string $key Primary key.
     * @return void
     */
    public function setPrimaryKey($key)
    {
        $this->setIdCompte($key);
    }

    /**
     * Returns true if the primary key for this object is null.
     * @return boolean
     */
    public function isPrimaryKeyNull()
    {
        return null === $this->getIdCompte();
    }

    /**
     * Sets contents of passed object to values from current object.
     *
     * If desired, this method can also make copies of all associated (fkey referrers)
     * objects.
     *
     * @param      object $copyObj An object of \Compte (or compatible) type.
     * @param      boolean $deepCopy Whether to also copy all rows that refer (by fkey) to the current row.
     * @param      boolean $makeNew Whether to reset autoincrement PKs and make the object new.
     * @throws PropelException
     */
    public function copyInto($copyObj, $deepCopy = false, $makeNew = true)
    {
        $copyObj->setNcompte($this->getNcompte());
        $copyObj->setNom($this->getNom());
        $copyObj->setNomcourt($this->getNomcourt());
        $copyObj->setIdPoste($this->getIdPoste());
        $copyObj->setTypeOp($this->getTypeOp());
        $copyObj->setSoldeAnterieur($this->getSoldeAnterieur());
        $copyObj->setDateAnterieure($this->getDateAnterieure());
        $copyObj->setIdEntite($this->getIdEntite());
        $copyObj->setObservation($this->getObservation());
        $copyObj->setCreatedAt($this->getCreatedAt());
        $copyObj->setUpdatedAt($this->getUpdatedAt());

        if ($deepCopy) {
            // important: temporarily setNew(false) because this affects the behavior of
            // the getter/setter methods for fkey referrer objects.
            $copyObj->setNew(false);

            foreach ($this->getEcritures() as $relObj) {
                if ($relObj !== $this) {  // ensure that we don't try to copy a reference to ourselves
                    $copyObj->addEcriture($relObj->copy($deepCopy));
                }
            }

            foreach ($this->getJournals() as $relObj) {
                if ($relObj !== $this) {  // ensure that we don't try to copy a reference to ourselves
                    $copyObj->addJournal($relObj->copy($deepCopy));
                }
            }

            foreach ($this->getPrestations() as $relObj) {
                if ($relObj !== $this) {  // ensure that we don't try to copy a reference to ourselves
                    $copyObj->addPrestation($relObj->copy($deepCopy));
                }
            }

            foreach ($this->getTresors() as $relObj) {
                if ($relObj !== $this) {  // ensure that we don't try to copy a reference to ourselves
                    $copyObj->addTresor($relObj->copy($deepCopy));
                }
            }

            foreach ($this->getTvas() as $relObj) {
                if ($relObj !== $this) {  // ensure that we don't try to copy a reference to ourselves
                    $copyObj->addTva($relObj->copy($deepCopy));
                }
            }

        } // if ($deepCopy)

        if ($makeNew) {
            $copyObj->setNew(true);
            $copyObj->setIdCompte(NULL); // this is a auto-increment column, so set to default value
        }
    }

    /**
     * Makes a copy of this object that will be inserted as a new row in table when saved.
     * It creates a new object filling in the simple attributes, but skipping any primary
     * keys that are defined for the table.
     *
     * If desired, this method can also make copies of all associated (fkey referrers)
     * objects.
     *
     * @param  boolean $deepCopy Whether to also copy all rows that refer (by fkey) to the current row.
     * @return \Compte Clone of current object.
     * @throws PropelException
     */
    public function copy($deepCopy = false)
    {
        // we use get_class(), because this might be a subclass
        $clazz = get_class($this);
        $copyObj = new $clazz();
        $this->copyInto($copyObj, $deepCopy);

        return $copyObj;
    }

    /**
     * Declares an association between this object and a ChildEntite object.
     *
     * @param  ChildEntite $v
     * @return $this|\Compte The current object (for fluent API support)
     * @throws PropelException
     */
    public function setEntite(ChildEntite $v = null)
    {
        if ($v === null) {
            $this->setIdEntite('1');
        } else {
            $this->setIdEntite($v->getIdEntite());
        }

        $this->aEntite = $v;

        // Add binding for other direction of this n:n relationship.
        // If this object has already been added to the ChildEntite object, it will not be re-added.
        if ($v !== null) {
            $v->addCompte($this);
        }


        return $this;
    }


    /**
     * Get the associated ChildEntite object
     *
     * @param  ConnectionInterface $con Optional Connection object.
     * @return ChildEntite The associated ChildEntite object.
     * @throws PropelException
     */
    public function getEntite(ConnectionInterface $con = null)
    {
        if ($this->aEntite === null && (($this->id_entite !== "" && $this->id_entite !== null))) {
            $this->aEntite = ChildEntiteQuery::create()->findPk($this->id_entite, $con);
            /* The following can be used additionally to
                guarantee the related object contains a reference
                to this object.  This level of coupling may, however, be
                undesirable since it could result in an only partially populated collection
                in the referenced object.
                $this->aEntite->addComptes($this);
             */
        }

        return $this->aEntite;
    }

    /**
     * Declares an association between this object and a ChildPoste object.
     *
     * @param  ChildPoste $v
     * @return $this|\Compte The current object (for fluent API support)
     * @throws PropelException
     */
    public function setPoste(ChildPoste $v = null)
    {
        if ($v === null) {
            $this->setIdPoste('1');
        } else {
            $this->setIdPoste($v->getIdPoste());
        }

        $this->aPoste = $v;

        // Add binding for other direction of this n:n relationship.
        // If this object has already been added to the ChildPoste object, it will not be re-added.
        if ($v !== null) {
            $v->addCompte($this);
        }


        return $this;
    }


    /**
     * Get the associated ChildPoste object
     *
     * @param  ConnectionInterface $con Optional Connection object.
     * @return ChildPoste The associated ChildPoste object.
     * @throws PropelException
     */
    public function getPoste(ConnectionInterface $con = null)
    {
        if ($this->aPoste === null && (($this->id_poste !== "" && $this->id_poste !== null))) {
            $this->aPoste = ChildPosteQuery::create()->findPk($this->id_poste, $con);
            /* The following can be used additionally to
                guarantee the related object contains a reference
                to this object.  This level of coupling may, however, be
                undesirable since it could result in an only partially populated collection
                in the referenced object.
                $this->aPoste->addComptes($this);
             */
        }

        return $this->aPoste;
    }


    /**
     * Initializes a collection based on the name of a relation.
     * Avoids crafting an 'init[$relationName]s' method name
     * that wouldn't work when StandardEnglishPluralizer is used.
     *
     * @param      string $relationName The name of the relation to initialize
     * @return void
     */
    public function initRelation($relationName)
    {
        if ('Ecriture' == $relationName) {
            $this->initEcritures();
            return;
        }
        if ('Journal' == $relationName) {
            $this->initJournals();
            return;
        }
        if ('Prestation' == $relationName) {
            $this->initPrestations();
            return;
        }
        if ('Tresor' == $relationName) {
            $this->initTresors();
            return;
        }
        if ('Tva' == $relationName) {
            $this->initTvas();
            return;
        }
    }

    /**
     * Clears out the collEcritures collection
     *
     * This does not modify the database; however, it will remove any associated objects, causing
     * them to be refetched by subsequent calls to accessor method.
     *
     * @return void
     * @see        addEcritures()
     */
    public function clearEcritures()
    {
        $this->collEcritures = null; // important to set this to NULL since that means it is uninitialized
    }

    /**
     * Reset is the collEcritures collection loaded partially.
     */
    public function resetPartialEcritures($v = true)
    {
        $this->collEcrituresPartial = $v;
    }

    /**
     * Initializes the collEcritures collection.
     *
     * By default this just sets the collEcritures collection to an empty array (like clearcollEcritures());
     * however, you may wish to override this method in your stub class to provide setting appropriate
     * to your application -- for example, setting the initial array to the values stored in database.
     *
     * @param      boolean $overrideExisting If set to true, the method call initializes
     *                                        the collection even if it is not empty
     *
     * @return void
     */
    public function initEcritures($overrideExisting = true)
    {
        if (null !== $this->collEcritures && !$overrideExisting) {
            return;
        }

        $collectionClassName = EcritureTableMap::getTableMap()->getCollectionClassName();

        $this->collEcritures = new $collectionClassName;
        $this->collEcritures->setModel('\Ecriture');
    }

    /**
     * Gets an array of ChildEcriture objects which contain a foreign key that references this object.
     *
     * If the $criteria is not null, it is used to always fetch the results from the database.
     * Otherwise the results are fetched from the database the first time, then cached.
     * Next time the same method is called without $criteria, the cached collection is returned.
     * If this ChildCompte is new, it will return
     * an empty collection or the current collection; the criteria is ignored on a new object.
     *
     * @param      Criteria $criteria optional Criteria object to narrow the query
     * @param      ConnectionInterface $con optional connection object
     * @return ObjectCollection|ChildEcriture[] List of ChildEcriture objects
     * @throws PropelException
     */
    public function getEcritures(Criteria $criteria = null, ConnectionInterface $con = null)
    {
        $partial = $this->collEcrituresPartial && !$this->isNew();
        if (null === $this->collEcritures || null !== $criteria  || $partial) {
            if ($this->isNew() && null === $this->collEcritures) {
                // return empty collection
                $this->initEcritures();
            } else {
                $collEcritures = ChildEcritureQuery::create(null, $criteria)
                    ->filterByCompte($this)
                    ->find($con);

                if (null !== $criteria) {
                    if (false !== $this->collEcrituresPartial && count($collEcritures)) {
                        $this->initEcritures(false);

                        foreach ($collEcritures as $obj) {
                            if (false == $this->collEcritures->contains($obj)) {
                                $this->collEcritures->append($obj);
                            }
                        }

                        $this->collEcrituresPartial = true;
                    }

                    return $collEcritures;
                }

                if ($partial && $this->collEcritures) {
                    foreach ($this->collEcritures as $obj) {
                        if ($obj->isNew()) {
                            $collEcritures[] = $obj;
                        }
                    }
                }

                $this->collEcritures = $collEcritures;
                $this->collEcrituresPartial = false;
            }
        }

        return $this->collEcritures;
    }

    /**
     * Sets a collection of ChildEcriture objects related by a one-to-many relationship
     * to the current object.
     * It will also schedule objects for deletion based on a diff between old objects (aka persisted)
     * and new objects from the given Propel collection.
     *
     * @param      Collection $ecritures A Propel collection.
     * @param      ConnectionInterface $con Optional connection object
     * @return $this|ChildCompte The current object (for fluent API support)
     */
    public function setEcritures(Collection $ecritures, ConnectionInterface $con = null)
    {
        /** @var ChildEcriture[] $ecrituresToDelete */
        $ecrituresToDelete = $this->getEcritures(new Criteria(), $con)->diff($ecritures);


        $this->ecrituresScheduledForDeletion = $ecrituresToDelete;

        foreach ($ecrituresToDelete as $ecritureRemoved) {
            $ecritureRemoved->setCompte(null);
        }

        $this->collEcritures = null;
        foreach ($ecritures as $ecriture) {
            $this->addEcriture($ecriture);
        }

        $this->collEcritures = $ecritures;
        $this->collEcrituresPartial = false;

        return $this;
    }

    /**
     * Returns the number of related Ecriture objects.
     *
     * @param      Criteria $criteria
     * @param      boolean $distinct
     * @param      ConnectionInterface $con
     * @return int             Count of related Ecriture objects.
     * @throws PropelException
     */
    public function countEcritures(Criteria $criteria = null, $distinct = false, ConnectionInterface $con = null)
    {
        $partial = $this->collEcrituresPartial && !$this->isNew();
        if (null === $this->collEcritures || null !== $criteria || $partial) {
            if ($this->isNew() && null === $this->collEcritures) {
                return 0;
            }

            if ($partial && !$criteria) {
                return count($this->getEcritures());
            }

            $query = ChildEcritureQuery::create(null, $criteria);
            if ($distinct) {
                $query->distinct();
            }

            return $query
                ->filterByCompte($this)
                ->count($con);
        }

        return count($this->collEcritures);
    }

    /**
     * Method called to associate a ChildEcriture object to this object
     * through the ChildEcriture foreign key attribute.
     *
     * @param  ChildEcriture $l ChildEcriture
     * @return $this|\Compte The current object (for fluent API support)
     */
    public function addEcriture(ChildEcriture $l)
    {
        if ($this->collEcritures === null) {
            $this->initEcritures();
            $this->collEcrituresPartial = true;
        }

        if (!$this->collEcritures->contains($l)) {
            $this->doAddEcriture($l);

            if ($this->ecrituresScheduledForDeletion and $this->ecrituresScheduledForDeletion->contains($l)) {
                $this->ecrituresScheduledForDeletion->remove($this->ecrituresScheduledForDeletion->search($l));
            }
        }

        return $this;
    }

    /**
     * @param ChildEcriture $ecriture The ChildEcriture object to add.
     */
    protected function doAddEcriture(ChildEcriture $ecriture)
    {
        $this->collEcritures[]= $ecriture;
        $ecriture->setCompte($this);
    }

    /**
     * @param  ChildEcriture $ecriture The ChildEcriture object to remove.
     * @return $this|ChildCompte The current object (for fluent API support)
     */
    public function removeEcriture(ChildEcriture $ecriture)
    {
        if ($this->getEcritures()->contains($ecriture)) {
            $pos = $this->collEcritures->search($ecriture);
            $this->collEcritures->remove($pos);
            if (null === $this->ecrituresScheduledForDeletion) {
                $this->ecrituresScheduledForDeletion = clone $this->collEcritures;
                $this->ecrituresScheduledForDeletion->clear();
            }
            $this->ecrituresScheduledForDeletion[]= clone $ecriture;
            $ecriture->setCompte(null);
        }

        return $this;
    }


    /**
     * If this collection has already been initialized with
     * an identical criteria, it returns the collection.
     * Otherwise if this Compte is new, it will return
     * an empty collection; or if this Compte has previously
     * been saved, it will retrieve related Ecritures from storage.
     *
     * This method is protected by default in order to keep the public
     * api reasonable.  You can provide public methods for those you
     * actually need in Compte.
     *
     * @param      Criteria $criteria optional Criteria object to narrow the query
     * @param      ConnectionInterface $con optional connection object
     * @param      string $joinBehavior optional join type to use (defaults to Criteria::LEFT_JOIN)
     * @return ObjectCollection|ChildEcriture[] List of ChildEcriture objects
     */
    public function getEcrituresJoinJournal(Criteria $criteria = null, ConnectionInterface $con = null, $joinBehavior = Criteria::LEFT_JOIN)
    {
        $query = ChildEcritureQuery::create(null, $criteria);
        $query->joinWith('Journal', $joinBehavior);

        return $this->getEcritures($query, $con);
    }


    /**
     * If this collection has already been initialized with
     * an identical criteria, it returns the collection.
     * Otherwise if this Compte is new, it will return
     * an empty collection; or if this Compte has previously
     * been saved, it will retrieve related Ecritures from storage.
     *
     * This method is protected by default in order to keep the public
     * api reasonable.  You can provide public methods for those you
     * actually need in Compte.
     *
     * @param      Criteria $criteria optional Criteria object to narrow the query
     * @param      ConnectionInterface $con optional connection object
     * @param      string $joinBehavior optional join type to use (defaults to Criteria::LEFT_JOIN)
     * @return ObjectCollection|ChildEcriture[] List of ChildEcriture objects
     */
    public function getEcrituresJoinActivite(Criteria $criteria = null, ConnectionInterface $con = null, $joinBehavior = Criteria::LEFT_JOIN)
    {
        $query = ChildEcritureQuery::create(null, $criteria);
        $query->joinWith('Activite', $joinBehavior);

        return $this->getEcritures($query, $con);
    }


    /**
     * If this collection has already been initialized with
     * an identical criteria, it returns the collection.
     * Otherwise if this Compte is new, it will return
     * an empty collection; or if this Compte has previously
     * been saved, it will retrieve related Ecritures from storage.
     *
     * This method is protected by default in order to keep the public
     * api reasonable.  You can provide public methods for those you
     * actually need in Compte.
     *
     * @param      Criteria $criteria optional Criteria object to narrow the query
     * @param      ConnectionInterface $con optional connection object
     * @param      string $joinBehavior optional join type to use (defaults to Criteria::LEFT_JOIN)
     * @return ObjectCollection|ChildEcriture[] List of ChildEcriture objects
     */
    public function getEcrituresJoinEntite(Criteria $criteria = null, ConnectionInterface $con = null, $joinBehavior = Criteria::LEFT_JOIN)
    {
        $query = ChildEcritureQuery::create(null, $criteria);
        $query->joinWith('Entite', $joinBehavior);

        return $this->getEcritures($query, $con);
    }

    /**
     * Clears out the collJournals collection
     *
     * This does not modify the database; however, it will remove any associated objects, causing
     * them to be refetched by subsequent calls to accessor method.
     *
     * @return void
     * @see        addJournals()
     */
    public function clearJournals()
    {
        $this->collJournals = null; // important to set this to NULL since that means it is uninitialized
    }

    /**
     * Reset is the collJournals collection loaded partially.
     */
    public function resetPartialJournals($v = true)
    {
        $this->collJournalsPartial = $v;
    }

    /**
     * Initializes the collJournals collection.
     *
     * By default this just sets the collJournals collection to an empty array (like clearcollJournals());
     * however, you may wish to override this method in your stub class to provide setting appropriate
     * to your application -- for example, setting the initial array to the values stored in database.
     *
     * @param      boolean $overrideExisting If set to true, the method call initializes
     *                                        the collection even if it is not empty
     *
     * @return void
     */
    public function initJournals($overrideExisting = true)
    {
        if (null !== $this->collJournals && !$overrideExisting) {
            return;
        }

        $collectionClassName = JournalTableMap::getTableMap()->getCollectionClassName();

        $this->collJournals = new $collectionClassName;
        $this->collJournals->setModel('\Journal');
    }

    /**
     * Gets an array of ChildJournal objects which contain a foreign key that references this object.
     *
     * If the $criteria is not null, it is used to always fetch the results from the database.
     * Otherwise the results are fetched from the database the first time, then cached.
     * Next time the same method is called without $criteria, the cached collection is returned.
     * If this ChildCompte is new, it will return
     * an empty collection or the current collection; the criteria is ignored on a new object.
     *
     * @param      Criteria $criteria optional Criteria object to narrow the query
     * @param      ConnectionInterface $con optional connection object
     * @return ObjectCollection|ChildJournal[] List of ChildJournal objects
     * @throws PropelException
     */
    public function getJournals(Criteria $criteria = null, ConnectionInterface $con = null)
    {
        $partial = $this->collJournalsPartial && !$this->isNew();
        if (null === $this->collJournals || null !== $criteria  || $partial) {
            if ($this->isNew() && null === $this->collJournals) {
                // return empty collection
                $this->initJournals();
            } else {
                $collJournals = ChildJournalQuery::create(null, $criteria)
                    ->filterByCompte($this)
                    ->find($con);

                if (null !== $criteria) {
                    if (false !== $this->collJournalsPartial && count($collJournals)) {
                        $this->initJournals(false);

                        foreach ($collJournals as $obj) {
                            if (false == $this->collJournals->contains($obj)) {
                                $this->collJournals->append($obj);
                            }
                        }

                        $this->collJournalsPartial = true;
                    }

                    return $collJournals;
                }

                if ($partial && $this->collJournals) {
                    foreach ($this->collJournals as $obj) {
                        if ($obj->isNew()) {
                            $collJournals[] = $obj;
                        }
                    }
                }

                $this->collJournals = $collJournals;
                $this->collJournalsPartial = false;
            }
        }

        return $this->collJournals;
    }

    /**
     * Sets a collection of ChildJournal objects related by a one-to-many relationship
     * to the current object.
     * It will also schedule objects for deletion based on a diff between old objects (aka persisted)
     * and new objects from the given Propel collection.
     *
     * @param      Collection $journals A Propel collection.
     * @param      ConnectionInterface $con Optional connection object
     * @return $this|ChildCompte The current object (for fluent API support)
     */
    public function setJournals(Collection $journals, ConnectionInterface $con = null)
    {
        /** @var ChildJournal[] $journalsToDelete */
        $journalsToDelete = $this->getJournals(new Criteria(), $con)->diff($journals);


        $this->journalsScheduledForDeletion = $journalsToDelete;

        foreach ($journalsToDelete as $journalRemoved) {
            $journalRemoved->setCompte(null);
        }

        $this->collJournals = null;
        foreach ($journals as $journal) {
            $this->addJournal($journal);
        }

        $this->collJournals = $journals;
        $this->collJournalsPartial = false;

        return $this;
    }

    /**
     * Returns the number of related Journal objects.
     *
     * @param      Criteria $criteria
     * @param      boolean $distinct
     * @param      ConnectionInterface $con
     * @return int             Count of related Journal objects.
     * @throws PropelException
     */
    public function countJournals(Criteria $criteria = null, $distinct = false, ConnectionInterface $con = null)
    {
        $partial = $this->collJournalsPartial && !$this->isNew();
        if (null === $this->collJournals || null !== $criteria || $partial) {
            if ($this->isNew() && null === $this->collJournals) {
                return 0;
            }

            if ($partial && !$criteria) {
                return count($this->getJournals());
            }

            $query = ChildJournalQuery::create(null, $criteria);
            if ($distinct) {
                $query->distinct();
            }

            return $query
                ->filterByCompte($this)
                ->count($con);
        }

        return count($this->collJournals);
    }

    /**
     * Method called to associate a ChildJournal object to this object
     * through the ChildJournal foreign key attribute.
     *
     * @param  ChildJournal $l ChildJournal
     * @return $this|\Compte The current object (for fluent API support)
     */
    public function addJournal(ChildJournal $l)
    {
        if ($this->collJournals === null) {
            $this->initJournals();
            $this->collJournalsPartial = true;
        }

        if (!$this->collJournals->contains($l)) {
            $this->doAddJournal($l);

            if ($this->journalsScheduledForDeletion and $this->journalsScheduledForDeletion->contains($l)) {
                $this->journalsScheduledForDeletion->remove($this->journalsScheduledForDeletion->search($l));
            }
        }

        return $this;
    }

    /**
     * @param ChildJournal $journal The ChildJournal object to add.
     */
    protected function doAddJournal(ChildJournal $journal)
    {
        $this->collJournals[]= $journal;
        $journal->setCompte($this);
    }

    /**
     * @param  ChildJournal $journal The ChildJournal object to remove.
     * @return $this|ChildCompte The current object (for fluent API support)
     */
    public function removeJournal(ChildJournal $journal)
    {
        if ($this->getJournals()->contains($journal)) {
            $pos = $this->collJournals->search($journal);
            $this->collJournals->remove($pos);
            if (null === $this->journalsScheduledForDeletion) {
                $this->journalsScheduledForDeletion = clone $this->collJournals;
                $this->journalsScheduledForDeletion->clear();
            }
            $this->journalsScheduledForDeletion[]= clone $journal;
            $journal->setCompte(null);
        }

        return $this;
    }


    /**
     * If this collection has already been initialized with
     * an identical criteria, it returns the collection.
     * Otherwise if this Compte is new, it will return
     * an empty collection; or if this Compte has previously
     * been saved, it will retrieve related Journals from storage.
     *
     * This method is protected by default in order to keep the public
     * api reasonable.  You can provide public methods for those you
     * actually need in Compte.
     *
     * @param      Criteria $criteria optional Criteria object to narrow the query
     * @param      ConnectionInterface $con optional connection object
     * @param      string $joinBehavior optional join type to use (defaults to Criteria::LEFT_JOIN)
     * @return ObjectCollection|ChildJournal[] List of ChildJournal objects
     */
    public function getJournalsJoinEntite(Criteria $criteria = null, ConnectionInterface $con = null, $joinBehavior = Criteria::LEFT_JOIN)
    {
        $query = ChildJournalQuery::create(null, $criteria);
        $query->joinWith('Entite', $joinBehavior);

        return $this->getJournals($query, $con);
    }

    /**
     * Clears out the collPrestations collection
     *
     * This does not modify the database; however, it will remove any associated objects, causing
     * them to be refetched by subsequent calls to accessor method.
     *
     * @return void
     * @see        addPrestations()
     */
    public function clearPrestations()
    {
        $this->collPrestations = null; // important to set this to NULL since that means it is uninitialized
    }

    /**
     * Reset is the collPrestations collection loaded partially.
     */
    public function resetPartialPrestations($v = true)
    {
        $this->collPrestationsPartial = $v;
    }

    /**
     * Initializes the collPrestations collection.
     *
     * By default this just sets the collPrestations collection to an empty array (like clearcollPrestations());
     * however, you may wish to override this method in your stub class to provide setting appropriate
     * to your application -- for example, setting the initial array to the values stored in database.
     *
     * @param      boolean $overrideExisting If set to true, the method call initializes
     *                                        the collection even if it is not empty
     *
     * @return void
     */
    public function initPrestations($overrideExisting = true)
    {
        if (null !== $this->collPrestations && !$overrideExisting) {
            return;
        }

        $collectionClassName = PrestationTableMap::getTableMap()->getCollectionClassName();

        $this->collPrestations = new $collectionClassName;
        $this->collPrestations->setModel('\Prestation');
    }

    /**
     * Gets an array of ChildPrestation objects which contain a foreign key that references this object.
     *
     * If the $criteria is not null, it is used to always fetch the results from the database.
     * Otherwise the results are fetched from the database the first time, then cached.
     * Next time the same method is called without $criteria, the cached collection is returned.
     * If this ChildCompte is new, it will return
     * an empty collection or the current collection; the criteria is ignored on a new object.
     *
     * @param      Criteria $criteria optional Criteria object to narrow the query
     * @param      ConnectionInterface $con optional connection object
     * @return ObjectCollection|ChildPrestation[] List of ChildPrestation objects
     * @throws PropelException
     */
    public function getPrestations(Criteria $criteria = null, ConnectionInterface $con = null)
    {
        $partial = $this->collPrestationsPartial && !$this->isNew();
        if (null === $this->collPrestations || null !== $criteria  || $partial) {
            if ($this->isNew() && null === $this->collPrestations) {
                // return empty collection
                $this->initPrestations();
            } else {
                $collPrestations = ChildPrestationQuery::create(null, $criteria)
                    ->filterByCompte($this)
                    ->find($con);

                if (null !== $criteria) {
                    if (false !== $this->collPrestationsPartial && count($collPrestations)) {
                        $this->initPrestations(false);

                        foreach ($collPrestations as $obj) {
                            if (false == $this->collPrestations->contains($obj)) {
                                $this->collPrestations->append($obj);
                            }
                        }

                        $this->collPrestationsPartial = true;
                    }

                    return $collPrestations;
                }

                if ($partial && $this->collPrestations) {
                    foreach ($this->collPrestations as $obj) {
                        if ($obj->isNew()) {
                            $collPrestations[] = $obj;
                        }
                    }
                }

                $this->collPrestations = $collPrestations;
                $this->collPrestationsPartial = false;
            }
        }

        return $this->collPrestations;
    }

    /**
     * Sets a collection of ChildPrestation objects related by a one-to-many relationship
     * to the current object.
     * It will also schedule objects for deletion based on a diff between old objects (aka persisted)
     * and new objects from the given Propel collection.
     *
     * @param      Collection $prestations A Propel collection.
     * @param      ConnectionInterface $con Optional connection object
     * @return $this|ChildCompte The current object (for fluent API support)
     */
    public function setPrestations(Collection $prestations, ConnectionInterface $con = null)
    {
        /** @var ChildPrestation[] $prestationsToDelete */
        $prestationsToDelete = $this->getPrestations(new Criteria(), $con)->diff($prestations);


        $this->prestationsScheduledForDeletion = $prestationsToDelete;

        foreach ($prestationsToDelete as $prestationRemoved) {
            $prestationRemoved->setCompte(null);
        }

        $this->collPrestations = null;
        foreach ($prestations as $prestation) {
            $this->addPrestation($prestation);
        }

        $this->collPrestations = $prestations;
        $this->collPrestationsPartial = false;

        return $this;
    }

    /**
     * Returns the number of related Prestation objects.
     *
     * @param      Criteria $criteria
     * @param      boolean $distinct
     * @param      ConnectionInterface $con
     * @return int             Count of related Prestation objects.
     * @throws PropelException
     */
    public function countPrestations(Criteria $criteria = null, $distinct = false, ConnectionInterface $con = null)
    {
        $partial = $this->collPrestationsPartial && !$this->isNew();
        if (null === $this->collPrestations || null !== $criteria || $partial) {
            if ($this->isNew() && null === $this->collPrestations) {
                return 0;
            }

            if ($partial && !$criteria) {
                return count($this->getPrestations());
            }

            $query = ChildPrestationQuery::create(null, $criteria);
            if ($distinct) {
                $query->distinct();
            }

            return $query
                ->filterByCompte($this)
                ->count($con);
        }

        return count($this->collPrestations);
    }

    /**
     * Method called to associate a ChildPrestation object to this object
     * through the ChildPrestation foreign key attribute.
     *
     * @param  ChildPrestation $l ChildPrestation
     * @return $this|\Compte The current object (for fluent API support)
     */
    public function addPrestation(ChildPrestation $l)
    {
        if ($this->collPrestations === null) {
            $this->initPrestations();
            $this->collPrestationsPartial = true;
        }

        if (!$this->collPrestations->contains($l)) {
            $this->doAddPrestation($l);

            if ($this->prestationsScheduledForDeletion and $this->prestationsScheduledForDeletion->contains($l)) {
                $this->prestationsScheduledForDeletion->remove($this->prestationsScheduledForDeletion->search($l));
            }
        }

        return $this;
    }

    /**
     * @param ChildPrestation $prestation The ChildPrestation object to add.
     */
    protected function doAddPrestation(ChildPrestation $prestation)
    {
        $this->collPrestations[]= $prestation;
        $prestation->setCompte($this);
    }

    /**
     * @param  ChildPrestation $prestation The ChildPrestation object to remove.
     * @return $this|ChildCompte The current object (for fluent API support)
     */
    public function removePrestation(ChildPrestation $prestation)
    {
        if ($this->getPrestations()->contains($prestation)) {
            $pos = $this->collPrestations->search($prestation);
            $this->collPrestations->remove($pos);
            if (null === $this->prestationsScheduledForDeletion) {
                $this->prestationsScheduledForDeletion = clone $this->collPrestations;
                $this->prestationsScheduledForDeletion->clear();
            }
            $this->prestationsScheduledForDeletion[]= $prestation;
            $prestation->setCompte(null);
        }

        return $this;
    }


    /**
     * If this collection has already been initialized with
     * an identical criteria, it returns the collection.
     * Otherwise if this Compte is new, it will return
     * an empty collection; or if this Compte has previously
     * been saved, it will retrieve related Prestations from storage.
     *
     * This method is protected by default in order to keep the public
     * api reasonable.  You can provide public methods for those you
     * actually need in Compte.
     *
     * @param      Criteria $criteria optional Criteria object to narrow the query
     * @param      ConnectionInterface $con optional connection object
     * @param      string $joinBehavior optional join type to use (defaults to Criteria::LEFT_JOIN)
     * @return ObjectCollection|ChildPrestation[] List of ChildPrestation objects
     */
    public function getPrestationsJoinEntite(Criteria $criteria = null, ConnectionInterface $con = null, $joinBehavior = Criteria::LEFT_JOIN)
    {
        $query = ChildPrestationQuery::create(null, $criteria);
        $query->joinWith('Entite', $joinBehavior);

        return $this->getPrestations($query, $con);
    }

    /**
     * Clears out the collTresors collection
     *
     * This does not modify the database; however, it will remove any associated objects, causing
     * them to be refetched by subsequent calls to accessor method.
     *
     * @return void
     * @see        addTresors()
     */
    public function clearTresors()
    {
        $this->collTresors = null; // important to set this to NULL since that means it is uninitialized
    }

    /**
     * Reset is the collTresors collection loaded partially.
     */
    public function resetPartialTresors($v = true)
    {
        $this->collTresorsPartial = $v;
    }

    /**
     * Initializes the collTresors collection.
     *
     * By default this just sets the collTresors collection to an empty array (like clearcollTresors());
     * however, you may wish to override this method in your stub class to provide setting appropriate
     * to your application -- for example, setting the initial array to the values stored in database.
     *
     * @param      boolean $overrideExisting If set to true, the method call initializes
     *                                        the collection even if it is not empty
     *
     * @return void
     */
    public function initTresors($overrideExisting = true)
    {
        if (null !== $this->collTresors && !$overrideExisting) {
            return;
        }

        $collectionClassName = TresorTableMap::getTableMap()->getCollectionClassName();

        $this->collTresors = new $collectionClassName;
        $this->collTresors->setModel('\Tresor');
    }

    /**
     * Gets an array of ChildTresor objects which contain a foreign key that references this object.
     *
     * If the $criteria is not null, it is used to always fetch the results from the database.
     * Otherwise the results are fetched from the database the first time, then cached.
     * Next time the same method is called without $criteria, the cached collection is returned.
     * If this ChildCompte is new, it will return
     * an empty collection or the current collection; the criteria is ignored on a new object.
     *
     * @param      Criteria $criteria optional Criteria object to narrow the query
     * @param      ConnectionInterface $con optional connection object
     * @return ObjectCollection|ChildTresor[] List of ChildTresor objects
     * @throws PropelException
     */
    public function getTresors(Criteria $criteria = null, ConnectionInterface $con = null)
    {
        $partial = $this->collTresorsPartial && !$this->isNew();
        if (null === $this->collTresors || null !== $criteria  || $partial) {
            if ($this->isNew() && null === $this->collTresors) {
                // return empty collection
                $this->initTresors();
            } else {
                $collTresors = ChildTresorQuery::create(null, $criteria)
                    ->filterByCompte($this)
                    ->find($con);

                if (null !== $criteria) {
                    if (false !== $this->collTresorsPartial && count($collTresors)) {
                        $this->initTresors(false);

                        foreach ($collTresors as $obj) {
                            if (false == $this->collTresors->contains($obj)) {
                                $this->collTresors->append($obj);
                            }
                        }

                        $this->collTresorsPartial = true;
                    }

                    return $collTresors;
                }

                if ($partial && $this->collTresors) {
                    foreach ($this->collTresors as $obj) {
                        if ($obj->isNew()) {
                            $collTresors[] = $obj;
                        }
                    }
                }

                $this->collTresors = $collTresors;
                $this->collTresorsPartial = false;
            }
        }

        return $this->collTresors;
    }

    /**
     * Sets a collection of ChildTresor objects related by a one-to-many relationship
     * to the current object.
     * It will also schedule objects for deletion based on a diff between old objects (aka persisted)
     * and new objects from the given Propel collection.
     *
     * @param      Collection $tresors A Propel collection.
     * @param      ConnectionInterface $con Optional connection object
     * @return $this|ChildCompte The current object (for fluent API support)
     */
    public function setTresors(Collection $tresors, ConnectionInterface $con = null)
    {
        /** @var ChildTresor[] $tresorsToDelete */
        $tresorsToDelete = $this->getTresors(new Criteria(), $con)->diff($tresors);


        $this->tresorsScheduledForDeletion = $tresorsToDelete;

        foreach ($tresorsToDelete as $tresorRemoved) {
            $tresorRemoved->setCompte(null);
        }

        $this->collTresors = null;
        foreach ($tresors as $tresor) {
            $this->addTresor($tresor);
        }

        $this->collTresors = $tresors;
        $this->collTresorsPartial = false;

        return $this;
    }

    /**
     * Returns the number of related Tresor objects.
     *
     * @param      Criteria $criteria
     * @param      boolean $distinct
     * @param      ConnectionInterface $con
     * @return int             Count of related Tresor objects.
     * @throws PropelException
     */
    public function countTresors(Criteria $criteria = null, $distinct = false, ConnectionInterface $con = null)
    {
        $partial = $this->collTresorsPartial && !$this->isNew();
        if (null === $this->collTresors || null !== $criteria || $partial) {
            if ($this->isNew() && null === $this->collTresors) {
                return 0;
            }

            if ($partial && !$criteria) {
                return count($this->getTresors());
            }

            $query = ChildTresorQuery::create(null, $criteria);
            if ($distinct) {
                $query->distinct();
            }

            return $query
                ->filterByCompte($this)
                ->count($con);
        }

        return count($this->collTresors);
    }

    /**
     * Method called to associate a ChildTresor object to this object
     * through the ChildTresor foreign key attribute.
     *
     * @param  ChildTresor $l ChildTresor
     * @return $this|\Compte The current object (for fluent API support)
     */
    public function addTresor(ChildTresor $l)
    {
        if ($this->collTresors === null) {
            $this->initTresors();
            $this->collTresorsPartial = true;
        }

        if (!$this->collTresors->contains($l)) {
            $this->doAddTresor($l);

            if ($this->tresorsScheduledForDeletion and $this->tresorsScheduledForDeletion->contains($l)) {
                $this->tresorsScheduledForDeletion->remove($this->tresorsScheduledForDeletion->search($l));
            }
        }

        return $this;
    }

    /**
     * @param ChildTresor $tresor The ChildTresor object to add.
     */
    protected function doAddTresor(ChildTresor $tresor)
    {
        $this->collTresors[]= $tresor;
        $tresor->setCompte($this);
    }

    /**
     * @param  ChildTresor $tresor The ChildTresor object to remove.
     * @return $this|ChildCompte The current object (for fluent API support)
     */
    public function removeTresor(ChildTresor $tresor)
    {
        if ($this->getTresors()->contains($tresor)) {
            $pos = $this->collTresors->search($tresor);
            $this->collTresors->remove($pos);
            if (null === $this->tresorsScheduledForDeletion) {
                $this->tresorsScheduledForDeletion = clone $this->collTresors;
                $this->tresorsScheduledForDeletion->clear();
            }
            $this->tresorsScheduledForDeletion[]= $tresor;
            $tresor->setCompte(null);
        }

        return $this;
    }


    /**
     * If this collection has already been initialized with
     * an identical criteria, it returns the collection.
     * Otherwise if this Compte is new, it will return
     * an empty collection; or if this Compte has previously
     * been saved, it will retrieve related Tresors from storage.
     *
     * This method is protected by default in order to keep the public
     * api reasonable.  You can provide public methods for those you
     * actually need in Compte.
     *
     * @param      Criteria $criteria optional Criteria object to narrow the query
     * @param      ConnectionInterface $con optional connection object
     * @param      string $joinBehavior optional join type to use (defaults to Criteria::LEFT_JOIN)
     * @return ObjectCollection|ChildTresor[] List of ChildTresor objects
     */
    public function getTresorsJoinEntite(Criteria $criteria = null, ConnectionInterface $con = null, $joinBehavior = Criteria::LEFT_JOIN)
    {
        $query = ChildTresorQuery::create(null, $criteria);
        $query->joinWith('Entite', $joinBehavior);

        return $this->getTresors($query, $con);
    }

    /**
     * Clears out the collTvas collection
     *
     * This does not modify the database; however, it will remove any associated objects, causing
     * them to be refetched by subsequent calls to accessor method.
     *
     * @return void
     * @see        addTvas()
     */
    public function clearTvas()
    {
        $this->collTvas = null; // important to set this to NULL since that means it is uninitialized
    }

    /**
     * Reset is the collTvas collection loaded partially.
     */
    public function resetPartialTvas($v = true)
    {
        $this->collTvasPartial = $v;
    }

    /**
     * Initializes the collTvas collection.
     *
     * By default this just sets the collTvas collection to an empty array (like clearcollTvas());
     * however, you may wish to override this method in your stub class to provide setting appropriate
     * to your application -- for example, setting the initial array to the values stored in database.
     *
     * @param      boolean $overrideExisting If set to true, the method call initializes
     *                                        the collection even if it is not empty
     *
     * @return void
     */
    public function initTvas($overrideExisting = true)
    {
        if (null !== $this->collTvas && !$overrideExisting) {
            return;
        }

        $collectionClassName = TvaTableMap::getTableMap()->getCollectionClassName();

        $this->collTvas = new $collectionClassName;
        $this->collTvas->setModel('\Tva');
    }

    /**
     * Gets an array of ChildTva objects which contain a foreign key that references this object.
     *
     * If the $criteria is not null, it is used to always fetch the results from the database.
     * Otherwise the results are fetched from the database the first time, then cached.
     * Next time the same method is called without $criteria, the cached collection is returned.
     * If this ChildCompte is new, it will return
     * an empty collection or the current collection; the criteria is ignored on a new object.
     *
     * @param      Criteria $criteria optional Criteria object to narrow the query
     * @param      ConnectionInterface $con optional connection object
     * @return ObjectCollection|ChildTva[] List of ChildTva objects
     * @throws PropelException
     */
    public function getTvas(Criteria $criteria = null, ConnectionInterface $con = null)
    {
        $partial = $this->collTvasPartial && !$this->isNew();
        if (null === $this->collTvas || null !== $criteria  || $partial) {
            if ($this->isNew() && null === $this->collTvas) {
                // return empty collection
                $this->initTvas();
            } else {
                $collTvas = ChildTvaQuery::create(null, $criteria)
                    ->filterByCompte($this)
                    ->find($con);

                if (null !== $criteria) {
                    if (false !== $this->collTvasPartial && count($collTvas)) {
                        $this->initTvas(false);

                        foreach ($collTvas as $obj) {
                            if (false == $this->collTvas->contains($obj)) {
                                $this->collTvas->append($obj);
                            }
                        }

                        $this->collTvasPartial = true;
                    }

                    return $collTvas;
                }

                if ($partial && $this->collTvas) {
                    foreach ($this->collTvas as $obj) {
                        if ($obj->isNew()) {
                            $collTvas[] = $obj;
                        }
                    }
                }

                $this->collTvas = $collTvas;
                $this->collTvasPartial = false;
            }
        }

        return $this->collTvas;
    }

    /**
     * Sets a collection of ChildTva objects related by a one-to-many relationship
     * to the current object.
     * It will also schedule objects for deletion based on a diff between old objects (aka persisted)
     * and new objects from the given Propel collection.
     *
     * @param      Collection $tvas A Propel collection.
     * @param      ConnectionInterface $con Optional connection object
     * @return $this|ChildCompte The current object (for fluent API support)
     */
    public function setTvas(Collection $tvas, ConnectionInterface $con = null)
    {
        /** @var ChildTva[] $tvasToDelete */
        $tvasToDelete = $this->getTvas(new Criteria(), $con)->diff($tvas);


        $this->tvasScheduledForDeletion = $tvasToDelete;

        foreach ($tvasToDelete as $tvaRemoved) {
            $tvaRemoved->setCompte(null);
        }

        $this->collTvas = null;
        foreach ($tvas as $tva) {
            $this->addTva($tva);
        }

        $this->collTvas = $tvas;
        $this->collTvasPartial = false;

        return $this;
    }

    /**
     * Returns the number of related Tva objects.
     *
     * @param      Criteria $criteria
     * @param      boolean $distinct
     * @param      ConnectionInterface $con
     * @return int             Count of related Tva objects.
     * @throws PropelException
     */
    public function countTvas(Criteria $criteria = null, $distinct = false, ConnectionInterface $con = null)
    {
        $partial = $this->collTvasPartial && !$this->isNew();
        if (null === $this->collTvas || null !== $criteria || $partial) {
            if ($this->isNew() && null === $this->collTvas) {
                return 0;
            }

            if ($partial && !$criteria) {
                return count($this->getTvas());
            }

            $query = ChildTvaQuery::create(null, $criteria);
            if ($distinct) {
                $query->distinct();
            }

            return $query
                ->filterByCompte($this)
                ->count($con);
        }

        return count($this->collTvas);
    }

    /**
     * Method called to associate a ChildTva object to this object
     * through the ChildTva foreign key attribute.
     *
     * @param  ChildTva $l ChildTva
     * @return $this|\Compte The current object (for fluent API support)
     */
    public function addTva(ChildTva $l)
    {
        if ($this->collTvas === null) {
            $this->initTvas();
            $this->collTvasPartial = true;
        }

        if (!$this->collTvas->contains($l)) {
            $this->doAddTva($l);

            if ($this->tvasScheduledForDeletion and $this->tvasScheduledForDeletion->contains($l)) {
                $this->tvasScheduledForDeletion->remove($this->tvasScheduledForDeletion->search($l));
            }
        }

        return $this;
    }

    /**
     * @param ChildTva $tva The ChildTva object to add.
     */
    protected function doAddTva(ChildTva $tva)
    {
        $this->collTvas[]= $tva;
        $tva->setCompte($this);
    }

    /**
     * @param  ChildTva $tva The ChildTva object to remove.
     * @return $this|ChildCompte The current object (for fluent API support)
     */
    public function removeTva(ChildTva $tva)
    {
        if ($this->getTvas()->contains($tva)) {
            $pos = $this->collTvas->search($tva);
            $this->collTvas->remove($pos);
            if (null === $this->tvasScheduledForDeletion) {
                $this->tvasScheduledForDeletion = clone $this->collTvas;
                $this->tvasScheduledForDeletion->clear();
            }
            $this->tvasScheduledForDeletion[]= clone $tva;
            $tva->setCompte(null);
        }

        return $this;
    }


    /**
     * If this collection has already been initialized with
     * an identical criteria, it returns the collection.
     * Otherwise if this Compte is new, it will return
     * an empty collection; or if this Compte has previously
     * been saved, it will retrieve related Tvas from storage.
     *
     * This method is protected by default in order to keep the public
     * api reasonable.  You can provide public methods for those you
     * actually need in Compte.
     *
     * @param      Criteria $criteria optional Criteria object to narrow the query
     * @param      ConnectionInterface $con optional connection object
     * @param      string $joinBehavior optional join type to use (defaults to Criteria::LEFT_JOIN)
     * @return ObjectCollection|ChildTva[] List of ChildTva objects
     */
    public function getTvasJoinEntite(Criteria $criteria = null, ConnectionInterface $con = null, $joinBehavior = Criteria::LEFT_JOIN)
    {
        $query = ChildTvaQuery::create(null, $criteria);
        $query->joinWith('Entite', $joinBehavior);

        return $this->getTvas($query, $con);
    }

    /**
     * Clears the current object, sets all attributes to their default values and removes
     * outgoing references as well as back-references (from other objects to this one. Results probably in a database
     * change of those foreign objects when you call `save` there).
     */
    public function clear()
    {
        if (null !== $this->aEntite) {
            $this->aEntite->removeCompte($this);
        }
        if (null !== $this->aPoste) {
            $this->aPoste->removeCompte($this);
        }
        $this->id_compte = null;
        $this->ncompte = null;
        $this->nom = null;
        $this->nomcourt = null;
        $this->id_poste = null;
        $this->type_op = null;
        $this->solde_anterieur = null;
        $this->date_anterieure = null;
        $this->id_entite = null;
        $this->observation = null;
        $this->created_at = null;
        $this->updated_at = null;
        $this->alreadyInSave = false;
        $this->clearAllReferences();
        $this->applyDefaultValues();
        $this->resetModified();
        $this->setNew(true);
        $this->setDeleted(false);
    }

    /**
     * Resets all references and back-references to other model objects or collections of model objects.
     *
     * This method is used to reset all php object references (not the actual reference in the database).
     * Necessary for object serialisation.
     *
     * @param      boolean $deep Whether to also clear the references on all referrer objects.
     */
    public function clearAllReferences($deep = false)
    {
        if ($deep) {
            if ($this->collEcritures) {
                foreach ($this->collEcritures as $o) {
                    $o->clearAllReferences($deep);
                }
            }
            if ($this->collJournals) {
                foreach ($this->collJournals as $o) {
                    $o->clearAllReferences($deep);
                }
            }
            if ($this->collPrestations) {
                foreach ($this->collPrestations as $o) {
                    $o->clearAllReferences($deep);
                }
            }
            if ($this->collTresors) {
                foreach ($this->collTresors as $o) {
                    $o->clearAllReferences($deep);
                }
            }
            if ($this->collTvas) {
                foreach ($this->collTvas as $o) {
                    $o->clearAllReferences($deep);
                }
            }
        } // if ($deep)

        $this->collEcritures = null;
        $this->collJournals = null;
        $this->collPrestations = null;
        $this->collTresors = null;
        $this->collTvas = null;
        $this->aEntite = null;
        $this->aPoste = null;
    }

    /**
     * Return the string representation of this object
     *
     * @return string
     */
    public function __toString()
    {
        return (string) $this->exportTo(CompteTableMap::DEFAULT_STRING_FORMAT);
    }

    // timestampable behavior

    /**
     * Mark the current object so that the update date doesn't get updated during next save
     *
     * @return     $this|ChildCompte The current object (for fluent API support)
     */
    public function keepUpdateDateUnchanged()
    {
        $this->modifiedColumns[CompteTableMap::COL_UPDATED_AT] = true;

        return $this;
    }

    // archivable behavior

    /**
     * Get an archived version of the current object.
     *
     * @param ConnectionInterface $con Optional connection object
     *
     * @return     ChildAsscComptesArchive An archive object, or null if the current object was never archived
     */
    public function getArchive(ConnectionInterface $con = null)
    {
        if ($this->isNew()) {
            return null;
        }
        $archive = ChildAsscComptesArchiveQuery::create()
            ->filterByPrimaryKey($this->getPrimaryKey())
            ->findOne($con);

        return $archive;
    }
    /**
     * Copy the data of the current object into a $archiveTablePhpName archive object.
     * The archived object is then saved.
     * If the current object has already been archived, the archived object
     * is updated and not duplicated.
     *
     * @param ConnectionInterface $con Optional connection object
     *
     * @throws PropelException If the object is new
     *
     * @return     ChildAsscComptesArchive The archive object based on this object
     */
    public function archive(ConnectionInterface $con = null)
    {
        if ($this->isNew()) {
            throw new PropelException('New objects cannot be archived. You must save the current object before calling archive().');
        }
        $archive = $this->getArchive($con);
        if (!$archive) {
            $archive = new ChildAsscComptesArchive();
            $archive->setPrimaryKey($this->getPrimaryKey());
        }
        $this->copyInto($archive, $deepCopy = false, $makeNew = false);
        $archive->setArchivedAt(time());
        $archive->save($con);

        return $archive;
    }

    /**
     * Revert the the current object to the state it had when it was last archived.
     * The object must be saved afterwards if the changes must persist.
     *
     * @param ConnectionInterface $con Optional connection object
     *
     * @throws PropelException If the object has no corresponding archive.
     *
     * @return $this|ChildCompte The current object (for fluent API support)
     */
    public function restoreFromArchive(ConnectionInterface $con = null)
    {
        $archive = $this->getArchive($con);
        if (!$archive) {
            throw new PropelException('The current object has never been archived and cannot be restored');
        }
        $this->populateFromArchive($archive);

        return $this;
    }

    /**
     * Populates the the current object based on a $archiveTablePhpName archive object.
     *
     * @param      ChildAsscComptesArchive $archive An archived object based on the same class
      * @param      Boolean $populateAutoIncrementPrimaryKeys
     *               If true, autoincrement columns are copied from the archive object.
     *               If false, autoincrement columns are left intact.
      *
     * @return     ChildCompte The current object (for fluent API support)
     */
    public function populateFromArchive($archive, $populateAutoIncrementPrimaryKeys = false) {
        if ($populateAutoIncrementPrimaryKeys) {
            $this->setIdCompte($archive->getIdCompte());
        }
        $this->setNcompte($archive->getNcompte());
        $this->setNom($archive->getNom());
        $this->setNomcourt($archive->getNomcourt());
        $this->setIdPoste($archive->getIdPoste());
        $this->setTypeOp($archive->getTypeOp());
        $this->setSoldeAnterieur($archive->getSoldeAnterieur());
        $this->setDateAnterieure($archive->getDateAnterieure());
        $this->setIdEntite($archive->getIdEntite());
        $this->setObservation($archive->getObservation());
        $this->setCreatedAt($archive->getCreatedAt());
        $this->setUpdatedAt($archive->getUpdatedAt());

        return $this;
    }

    /**
     * Removes the object from the database without archiving it.
     *
     * @param ConnectionInterface $con Optional connection object
     *
     * @return $this|ChildCompte The current object (for fluent API support)
     */
    public function deleteWithoutArchive(ConnectionInterface $con = null)
    {
        $this->archiveOnDelete = false;

        return $this->delete($con);
    }

    /**
     * Code to be run before persisting the object
     * @param  ConnectionInterface $con
     * @return boolean
     */
    public function preSave(ConnectionInterface $con = null)
    {
        if (is_callable('parent::preSave')) {
            return parent::preSave($con);
        }
        return true;
    }

    /**
     * Code to be run after persisting the object
     * @param ConnectionInterface $con
     */
    public function postSave(ConnectionInterface $con = null)
    {
        if (is_callable('parent::postSave')) {
            parent::postSave($con);
        }
    }

    /**
     * Code to be run before inserting to database
     * @param  ConnectionInterface $con
     * @return boolean
     */
    public function preInsert(ConnectionInterface $con = null)
    {
        if (is_callable('parent::preInsert')) {
            return parent::preInsert($con);
        }
        return true;
    }

    /**
     * Code to be run after inserting to database
     * @param ConnectionInterface $con
     */
    public function postInsert(ConnectionInterface $con = null)
    {
        if (is_callable('parent::postInsert')) {
            parent::postInsert($con);
        }
    }

    /**
     * Code to be run before updating the object in database
     * @param  ConnectionInterface $con
     * @return boolean
     */
    public function preUpdate(ConnectionInterface $con = null)
    {
        if (is_callable('parent::preUpdate')) {
            return parent::preUpdate($con);
        }
        return true;
    }

    /**
     * Code to be run after updating the object in database
     * @param ConnectionInterface $con
     */
    public function postUpdate(ConnectionInterface $con = null)
    {
        if (is_callable('parent::postUpdate')) {
            parent::postUpdate($con);
        }
    }

    /**
     * Code to be run before deleting the object in database
     * @param  ConnectionInterface $con
     * @return boolean
     */
    public function preDelete(ConnectionInterface $con = null)
    {
        if (is_callable('parent::preDelete')) {
            return parent::preDelete($con);
        }
        return true;
    }

    /**
     * Code to be run after deleting the object in database
     * @param ConnectionInterface $con
     */
    public function postDelete(ConnectionInterface $con = null)
    {
        if (is_callable('parent::postDelete')) {
            parent::postDelete($con);
        }
    }


    /**
     * Derived method to catches calls to undefined methods.
     *
     * Provides magic import/export method support (fromXML()/toXML(), fromYAML()/toYAML(), etc.).
     * Allows to define default __call() behavior if you overwrite __call()
     *
     * @param string $name
     * @param mixed  $params
     *
     * @return array|string
     */
    public function __call($name, $params)
    {
        if (0 === strpos($name, 'get')) {
            $virtualColumn = substr($name, 3);
            if ($this->hasVirtualColumn($virtualColumn)) {
                return $this->getVirtualColumn($virtualColumn);
            }

            $virtualColumn = lcfirst($virtualColumn);
            if ($this->hasVirtualColumn($virtualColumn)) {
                return $this->getVirtualColumn($virtualColumn);
            }
        }

        if (0 === strpos($name, 'from')) {
            $format = substr($name, 4);

            return $this->importFrom($format, reset($params));
        }

        if (0 === strpos($name, 'to')) {
            $format = substr($name, 2);
            $includeLazyLoadColumns = isset($params[0]) ? $params[0] : true;

            return $this->exportTo($format, $includeLazyLoadColumns);
        }

        throw new BadMethodCallException(sprintf('Call to undefined method: %s.', $name));
    }

}
