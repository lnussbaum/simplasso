<?php

namespace Base;

use \AssoPrestationsArchive as ChildAssoPrestationsArchive;
use \AssoPrestationsArchiveQuery as ChildAssoPrestationsArchiveQuery;
use \Exception;
use \PDO;
use Map\AssoPrestationsArchiveTableMap;
use Propel\Runtime\Propel;
use Propel\Runtime\ActiveQuery\Criteria;
use Propel\Runtime\ActiveQuery\ModelCriteria;
use Propel\Runtime\Collection\ObjectCollection;
use Propel\Runtime\Connection\ConnectionInterface;
use Propel\Runtime\Exception\PropelException;

/**
 * Base class that represents a query for the 'asso_prestations_archive' table.
 *
 *
 *
 * @method     ChildAssoPrestationsArchiveQuery orderByIdPrestation($order = Criteria::ASC) Order by the id_prestation column
 * @method     ChildAssoPrestationsArchiveQuery orderByNomGroupe($order = Criteria::ASC) Order by the nom_groupe column
 * @method     ChildAssoPrestationsArchiveQuery orderByNom($order = Criteria::ASC) Order by the nom column
 * @method     ChildAssoPrestationsArchiveQuery orderByNomcourt($order = Criteria::ASC) Order by the nomcourt column
 * @method     ChildAssoPrestationsArchiveQuery orderByDescriptif($order = Criteria::ASC) Order by the descriptif column
 * @method     ChildAssoPrestationsArchiveQuery orderByIdEntite($order = Criteria::ASC) Order by the id_entite column
 * @method     ChildAssoPrestationsArchiveQuery orderByIdTva($order = Criteria::ASC) Order by the id_tva column
 * @method     ChildAssoPrestationsArchiveQuery orderByRetardJours($order = Criteria::ASC) Order by the retard_jours column
 * @method     ChildAssoPrestationsArchiveQuery orderByNombreNumero($order = Criteria::ASC) Order by the nombre_numero column
 * @method     ChildAssoPrestationsArchiveQuery orderByProchainNumero($order = Criteria::ASC) Order by the prochain_numero column
 * @method     ChildAssoPrestationsArchiveQuery orderByPrixlibre($order = Criteria::ASC) Order by the prixlibre column
 * @method     ChildAssoPrestationsArchiveQuery orderByQuantite($order = Criteria::ASC) Order by the quantite column
 * @method     ChildAssoPrestationsArchiveQuery orderByIdUnite($order = Criteria::ASC) Order by the id_unite column
 * @method     ChildAssoPrestationsArchiveQuery orderByIdCompte($order = Criteria::ASC) Order by the id_compte column
 * @method     ChildAssoPrestationsArchiveQuery orderByPrestationType($order = Criteria::ASC) Order by the prestation_type column
 * @method     ChildAssoPrestationsArchiveQuery orderByActive($order = Criteria::ASC) Order by the active column
 * @method     ChildAssoPrestationsArchiveQuery orderByNbVoix($order = Criteria::ASC) Order by the nb_voix column
 * @method     ChildAssoPrestationsArchiveQuery orderByPeriodique($order = Criteria::ASC) Order by the periodique column
 * @method     ChildAssoPrestationsArchiveQuery orderBySigne($order = Criteria::ASC) Order by the signe column
 * @method     ChildAssoPrestationsArchiveQuery orderByObjetBeneficiaire($order = Criteria::ASC) Order by the objet_beneficiaire column
 * @method     ChildAssoPrestationsArchiveQuery orderByCreatedAt($order = Criteria::ASC) Order by the created_at column
 * @method     ChildAssoPrestationsArchiveQuery orderByUpdatedAt($order = Criteria::ASC) Order by the updated_at column
 * @method     ChildAssoPrestationsArchiveQuery orderByArchivedAt($order = Criteria::ASC) Order by the archived_at column
 *
 * @method     ChildAssoPrestationsArchiveQuery groupByIdPrestation() Group by the id_prestation column
 * @method     ChildAssoPrestationsArchiveQuery groupByNomGroupe() Group by the nom_groupe column
 * @method     ChildAssoPrestationsArchiveQuery groupByNom() Group by the nom column
 * @method     ChildAssoPrestationsArchiveQuery groupByNomcourt() Group by the nomcourt column
 * @method     ChildAssoPrestationsArchiveQuery groupByDescriptif() Group by the descriptif column
 * @method     ChildAssoPrestationsArchiveQuery groupByIdEntite() Group by the id_entite column
 * @method     ChildAssoPrestationsArchiveQuery groupByIdTva() Group by the id_tva column
 * @method     ChildAssoPrestationsArchiveQuery groupByRetardJours() Group by the retard_jours column
 * @method     ChildAssoPrestationsArchiveQuery groupByNombreNumero() Group by the nombre_numero column
 * @method     ChildAssoPrestationsArchiveQuery groupByProchainNumero() Group by the prochain_numero column
 * @method     ChildAssoPrestationsArchiveQuery groupByPrixlibre() Group by the prixlibre column
 * @method     ChildAssoPrestationsArchiveQuery groupByQuantite() Group by the quantite column
 * @method     ChildAssoPrestationsArchiveQuery groupByIdUnite() Group by the id_unite column
 * @method     ChildAssoPrestationsArchiveQuery groupByIdCompte() Group by the id_compte column
 * @method     ChildAssoPrestationsArchiveQuery groupByPrestationType() Group by the prestation_type column
 * @method     ChildAssoPrestationsArchiveQuery groupByActive() Group by the active column
 * @method     ChildAssoPrestationsArchiveQuery groupByNbVoix() Group by the nb_voix column
 * @method     ChildAssoPrestationsArchiveQuery groupByPeriodique() Group by the periodique column
 * @method     ChildAssoPrestationsArchiveQuery groupBySigne() Group by the signe column
 * @method     ChildAssoPrestationsArchiveQuery groupByObjetBeneficiaire() Group by the objet_beneficiaire column
 * @method     ChildAssoPrestationsArchiveQuery groupByCreatedAt() Group by the created_at column
 * @method     ChildAssoPrestationsArchiveQuery groupByUpdatedAt() Group by the updated_at column
 * @method     ChildAssoPrestationsArchiveQuery groupByArchivedAt() Group by the archived_at column
 *
 * @method     ChildAssoPrestationsArchiveQuery leftJoin($relation) Adds a LEFT JOIN clause to the query
 * @method     ChildAssoPrestationsArchiveQuery rightJoin($relation) Adds a RIGHT JOIN clause to the query
 * @method     ChildAssoPrestationsArchiveQuery innerJoin($relation) Adds a INNER JOIN clause to the query
 *
 * @method     ChildAssoPrestationsArchiveQuery leftJoinWith($relation) Adds a LEFT JOIN clause and with to the query
 * @method     ChildAssoPrestationsArchiveQuery rightJoinWith($relation) Adds a RIGHT JOIN clause and with to the query
 * @method     ChildAssoPrestationsArchiveQuery innerJoinWith($relation) Adds a INNER JOIN clause and with to the query
 *
 * @method     ChildAssoPrestationsArchive findOne(ConnectionInterface $con = null) Return the first ChildAssoPrestationsArchive matching the query
 * @method     ChildAssoPrestationsArchive findOneOrCreate(ConnectionInterface $con = null) Return the first ChildAssoPrestationsArchive matching the query, or a new ChildAssoPrestationsArchive object populated from the query conditions when no match is found
 *
 * @method     ChildAssoPrestationsArchive findOneByIdPrestation(string $id_prestation) Return the first ChildAssoPrestationsArchive filtered by the id_prestation column
 * @method     ChildAssoPrestationsArchive findOneByNomGroupe(string $nom_groupe) Return the first ChildAssoPrestationsArchive filtered by the nom_groupe column
 * @method     ChildAssoPrestationsArchive findOneByNom(string $nom) Return the first ChildAssoPrestationsArchive filtered by the nom column
 * @method     ChildAssoPrestationsArchive findOneByNomcourt(string $nomcourt) Return the first ChildAssoPrestationsArchive filtered by the nomcourt column
 * @method     ChildAssoPrestationsArchive findOneByDescriptif(string $descriptif) Return the first ChildAssoPrestationsArchive filtered by the descriptif column
 * @method     ChildAssoPrestationsArchive findOneByIdEntite(string $id_entite) Return the first ChildAssoPrestationsArchive filtered by the id_entite column
 * @method     ChildAssoPrestationsArchive findOneByIdTva(string $id_tva) Return the first ChildAssoPrestationsArchive filtered by the id_tva column
 * @method     ChildAssoPrestationsArchive findOneByRetardJours(int $retard_jours) Return the first ChildAssoPrestationsArchive filtered by the retard_jours column
 * @method     ChildAssoPrestationsArchive findOneByNombreNumero(int $nombre_numero) Return the first ChildAssoPrestationsArchive filtered by the nombre_numero column
 * @method     ChildAssoPrestationsArchive findOneByProchainNumero(int $prochain_numero) Return the first ChildAssoPrestationsArchive filtered by the prochain_numero column
 * @method     ChildAssoPrestationsArchive findOneByPrixlibre(boolean $prixlibre) Return the first ChildAssoPrestationsArchive filtered by the prixlibre column
 * @method     ChildAssoPrestationsArchive findOneByQuantite(boolean $quantite) Return the first ChildAssoPrestationsArchive filtered by the quantite column
 * @method     ChildAssoPrestationsArchive findOneByIdUnite(string $id_unite) Return the first ChildAssoPrestationsArchive filtered by the id_unite column
 * @method     ChildAssoPrestationsArchive findOneByIdCompte(string $id_compte) Return the first ChildAssoPrestationsArchive filtered by the id_compte column
 * @method     ChildAssoPrestationsArchive findOneByPrestationType(int $prestation_type) Return the first ChildAssoPrestationsArchive filtered by the prestation_type column
 * @method     ChildAssoPrestationsArchive findOneByActive(boolean $active) Return the first ChildAssoPrestationsArchive filtered by the active column
 * @method     ChildAssoPrestationsArchive findOneByNbVoix(int $nb_voix) Return the first ChildAssoPrestationsArchive filtered by the nb_voix column
 * @method     ChildAssoPrestationsArchive findOneByPeriodique(string $periodique) Return the first ChildAssoPrestationsArchive filtered by the periodique column
 * @method     ChildAssoPrestationsArchive findOneBySigne(string $signe) Return the first ChildAssoPrestationsArchive filtered by the signe column
 * @method     ChildAssoPrestationsArchive findOneByObjetBeneficiaire(string $objet_beneficiaire) Return the first ChildAssoPrestationsArchive filtered by the objet_beneficiaire column
 * @method     ChildAssoPrestationsArchive findOneByCreatedAt(string $created_at) Return the first ChildAssoPrestationsArchive filtered by the created_at column
 * @method     ChildAssoPrestationsArchive findOneByUpdatedAt(string $updated_at) Return the first ChildAssoPrestationsArchive filtered by the updated_at column
 * @method     ChildAssoPrestationsArchive findOneByArchivedAt(string $archived_at) Return the first ChildAssoPrestationsArchive filtered by the archived_at column *

 * @method     ChildAssoPrestationsArchive requirePk($key, ConnectionInterface $con = null) Return the ChildAssoPrestationsArchive by primary key and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildAssoPrestationsArchive requireOne(ConnectionInterface $con = null) Return the first ChildAssoPrestationsArchive matching the query and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 *
 * @method     ChildAssoPrestationsArchive requireOneByIdPrestation(string $id_prestation) Return the first ChildAssoPrestationsArchive filtered by the id_prestation column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildAssoPrestationsArchive requireOneByNomGroupe(string $nom_groupe) Return the first ChildAssoPrestationsArchive filtered by the nom_groupe column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildAssoPrestationsArchive requireOneByNom(string $nom) Return the first ChildAssoPrestationsArchive filtered by the nom column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildAssoPrestationsArchive requireOneByNomcourt(string $nomcourt) Return the first ChildAssoPrestationsArchive filtered by the nomcourt column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildAssoPrestationsArchive requireOneByDescriptif(string $descriptif) Return the first ChildAssoPrestationsArchive filtered by the descriptif column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildAssoPrestationsArchive requireOneByIdEntite(string $id_entite) Return the first ChildAssoPrestationsArchive filtered by the id_entite column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildAssoPrestationsArchive requireOneByIdTva(string $id_tva) Return the first ChildAssoPrestationsArchive filtered by the id_tva column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildAssoPrestationsArchive requireOneByRetardJours(int $retard_jours) Return the first ChildAssoPrestationsArchive filtered by the retard_jours column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildAssoPrestationsArchive requireOneByNombreNumero(int $nombre_numero) Return the first ChildAssoPrestationsArchive filtered by the nombre_numero column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildAssoPrestationsArchive requireOneByProchainNumero(int $prochain_numero) Return the first ChildAssoPrestationsArchive filtered by the prochain_numero column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildAssoPrestationsArchive requireOneByPrixlibre(boolean $prixlibre) Return the first ChildAssoPrestationsArchive filtered by the prixlibre column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildAssoPrestationsArchive requireOneByQuantite(boolean $quantite) Return the first ChildAssoPrestationsArchive filtered by the quantite column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildAssoPrestationsArchive requireOneByIdUnite(string $id_unite) Return the first ChildAssoPrestationsArchive filtered by the id_unite column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildAssoPrestationsArchive requireOneByIdCompte(string $id_compte) Return the first ChildAssoPrestationsArchive filtered by the id_compte column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildAssoPrestationsArchive requireOneByPrestationType(int $prestation_type) Return the first ChildAssoPrestationsArchive filtered by the prestation_type column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildAssoPrestationsArchive requireOneByActive(boolean $active) Return the first ChildAssoPrestationsArchive filtered by the active column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildAssoPrestationsArchive requireOneByNbVoix(int $nb_voix) Return the first ChildAssoPrestationsArchive filtered by the nb_voix column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildAssoPrestationsArchive requireOneByPeriodique(string $periodique) Return the first ChildAssoPrestationsArchive filtered by the periodique column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildAssoPrestationsArchive requireOneBySigne(string $signe) Return the first ChildAssoPrestationsArchive filtered by the signe column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildAssoPrestationsArchive requireOneByObjetBeneficiaire(string $objet_beneficiaire) Return the first ChildAssoPrestationsArchive filtered by the objet_beneficiaire column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildAssoPrestationsArchive requireOneByCreatedAt(string $created_at) Return the first ChildAssoPrestationsArchive filtered by the created_at column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildAssoPrestationsArchive requireOneByUpdatedAt(string $updated_at) Return the first ChildAssoPrestationsArchive filtered by the updated_at column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildAssoPrestationsArchive requireOneByArchivedAt(string $archived_at) Return the first ChildAssoPrestationsArchive filtered by the archived_at column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 *
 * @method     ChildAssoPrestationsArchive[]|ObjectCollection find(ConnectionInterface $con = null) Return ChildAssoPrestationsArchive objects based on current ModelCriteria
 * @method     ChildAssoPrestationsArchive[]|ObjectCollection findByIdPrestation(string $id_prestation) Return ChildAssoPrestationsArchive objects filtered by the id_prestation column
 * @method     ChildAssoPrestationsArchive[]|ObjectCollection findByNomGroupe(string $nom_groupe) Return ChildAssoPrestationsArchive objects filtered by the nom_groupe column
 * @method     ChildAssoPrestationsArchive[]|ObjectCollection findByNom(string $nom) Return ChildAssoPrestationsArchive objects filtered by the nom column
 * @method     ChildAssoPrestationsArchive[]|ObjectCollection findByNomcourt(string $nomcourt) Return ChildAssoPrestationsArchive objects filtered by the nomcourt column
 * @method     ChildAssoPrestationsArchive[]|ObjectCollection findByDescriptif(string $descriptif) Return ChildAssoPrestationsArchive objects filtered by the descriptif column
 * @method     ChildAssoPrestationsArchive[]|ObjectCollection findByIdEntite(string $id_entite) Return ChildAssoPrestationsArchive objects filtered by the id_entite column
 * @method     ChildAssoPrestationsArchive[]|ObjectCollection findByIdTva(string $id_tva) Return ChildAssoPrestationsArchive objects filtered by the id_tva column
 * @method     ChildAssoPrestationsArchive[]|ObjectCollection findByRetardJours(int $retard_jours) Return ChildAssoPrestationsArchive objects filtered by the retard_jours column
 * @method     ChildAssoPrestationsArchive[]|ObjectCollection findByNombreNumero(int $nombre_numero) Return ChildAssoPrestationsArchive objects filtered by the nombre_numero column
 * @method     ChildAssoPrestationsArchive[]|ObjectCollection findByProchainNumero(int $prochain_numero) Return ChildAssoPrestationsArchive objects filtered by the prochain_numero column
 * @method     ChildAssoPrestationsArchive[]|ObjectCollection findByPrixlibre(boolean $prixlibre) Return ChildAssoPrestationsArchive objects filtered by the prixlibre column
 * @method     ChildAssoPrestationsArchive[]|ObjectCollection findByQuantite(boolean $quantite) Return ChildAssoPrestationsArchive objects filtered by the quantite column
 * @method     ChildAssoPrestationsArchive[]|ObjectCollection findByIdUnite(string $id_unite) Return ChildAssoPrestationsArchive objects filtered by the id_unite column
 * @method     ChildAssoPrestationsArchive[]|ObjectCollection findByIdCompte(string $id_compte) Return ChildAssoPrestationsArchive objects filtered by the id_compte column
 * @method     ChildAssoPrestationsArchive[]|ObjectCollection findByPrestationType(int $prestation_type) Return ChildAssoPrestationsArchive objects filtered by the prestation_type column
 * @method     ChildAssoPrestationsArchive[]|ObjectCollection findByActive(boolean $active) Return ChildAssoPrestationsArchive objects filtered by the active column
 * @method     ChildAssoPrestationsArchive[]|ObjectCollection findByNbVoix(int $nb_voix) Return ChildAssoPrestationsArchive objects filtered by the nb_voix column
 * @method     ChildAssoPrestationsArchive[]|ObjectCollection findByPeriodique(string $periodique) Return ChildAssoPrestationsArchive objects filtered by the periodique column
 * @method     ChildAssoPrestationsArchive[]|ObjectCollection findBySigne(string $signe) Return ChildAssoPrestationsArchive objects filtered by the signe column
 * @method     ChildAssoPrestationsArchive[]|ObjectCollection findByObjetBeneficiaire(string $objet_beneficiaire) Return ChildAssoPrestationsArchive objects filtered by the objet_beneficiaire column
 * @method     ChildAssoPrestationsArchive[]|ObjectCollection findByCreatedAt(string $created_at) Return ChildAssoPrestationsArchive objects filtered by the created_at column
 * @method     ChildAssoPrestationsArchive[]|ObjectCollection findByUpdatedAt(string $updated_at) Return ChildAssoPrestationsArchive objects filtered by the updated_at column
 * @method     ChildAssoPrestationsArchive[]|ObjectCollection findByArchivedAt(string $archived_at) Return ChildAssoPrestationsArchive objects filtered by the archived_at column
 * @method     ChildAssoPrestationsArchive[]|\Propel\Runtime\Util\PropelModelPager paginate($page = 1, $maxPerPage = 10, ConnectionInterface $con = null) Issue a SELECT query based on the current ModelCriteria and uses a page and a maximum number of results per page to compute an offset and a limit
 *
 */
abstract class AssoPrestationsArchiveQuery extends ModelCriteria
{
    protected $entityNotFoundExceptionClass = '\\Propel\\Runtime\\Exception\\EntityNotFoundException';

    /**
     * Initializes internal state of \Base\AssoPrestationsArchiveQuery object.
     *
     * @param     string $dbName The database name
     * @param     string $modelName The phpName of a model, e.g. 'Book'
     * @param     string $modelAlias The alias for the model in this query, e.g. 'b'
     */
    public function __construct($dbName = 'simplasso', $modelName = '\\AssoPrestationsArchive', $modelAlias = null)
    {
        parent::__construct($dbName, $modelName, $modelAlias);
    }

    /**
     * Returns a new ChildAssoPrestationsArchiveQuery object.
     *
     * @param     string $modelAlias The alias of a model in the query
     * @param     Criteria $criteria Optional Criteria to build the query from
     *
     * @return ChildAssoPrestationsArchiveQuery
     */
    public static function create($modelAlias = null, Criteria $criteria = null)
    {
        if ($criteria instanceof ChildAssoPrestationsArchiveQuery) {
            return $criteria;
        }
        $query = new ChildAssoPrestationsArchiveQuery();
        if (null !== $modelAlias) {
            $query->setModelAlias($modelAlias);
        }
        if ($criteria instanceof Criteria) {
            $query->mergeWith($criteria);
        }

        return $query;
    }

    /**
     * Find object by primary key.
     * Propel uses the instance pool to skip the database if the object exists.
     * Go fast if the query is untouched.
     *
     * <code>
     * $obj  = $c->findPk(12, $con);
     * </code>
     *
     * @param mixed $key Primary key to use for the query
     * @param ConnectionInterface $con an optional connection object
     *
     * @return ChildAssoPrestationsArchive|array|mixed the result, formatted by the current formatter
     */
    public function findPk($key, ConnectionInterface $con = null)
    {
        if ($key === null) {
            return null;
        }

        if ($con === null) {
            $con = Propel::getServiceContainer()->getReadConnection(AssoPrestationsArchiveTableMap::DATABASE_NAME);
        }

        $this->basePreSelect($con);

        if (
            $this->formatter || $this->modelAlias || $this->with || $this->select
            || $this->selectColumns || $this->asColumns || $this->selectModifiers
            || $this->map || $this->having || $this->joins
        ) {
            return $this->findPkComplex($key, $con);
        }

        if ((null !== ($obj = AssoPrestationsArchiveTableMap::getInstanceFromPool(null === $key || is_scalar($key) || is_callable([$key, '__toString']) ? (string) $key : $key)))) {
            // the object is already in the instance pool
            return $obj;
        }

        return $this->findPkSimple($key, $con);
    }

    /**
     * Find object by primary key using raw SQL to go fast.
     * Bypass doSelect() and the object formatter by using generated code.
     *
     * @param     mixed $key Primary key to use for the query
     * @param     ConnectionInterface $con A connection object
     *
     * @throws \Propel\Runtime\Exception\PropelException
     *
     * @return ChildAssoPrestationsArchive A model object, or null if the key is not found
     */
    protected function findPkSimple($key, ConnectionInterface $con)
    {
        $sql = 'SELECT `id_prestation`, `nom_groupe`, `nom`, `nomcourt`, `descriptif`, `id_entite`, `id_tva`, `retard_jours`, `nombre_numero`, `prochain_numero`, `prixlibre`, `quantite`, `id_unite`, `id_compte`, `prestation_type`, `active`, `nb_voix`, `periodique`, `signe`, `objet_beneficiaire`, `created_at`, `updated_at`, `archived_at` FROM `asso_prestations_archive` WHERE `id_prestation` = :p0';
        try {
            $stmt = $con->prepare($sql);
            $stmt->bindValue(':p0', $key, PDO::PARAM_INT);
            $stmt->execute();
        } catch (Exception $e) {
            Propel::log($e->getMessage(), Propel::LOG_ERR);
            throw new PropelException(sprintf('Unable to execute SELECT statement [%s]', $sql), 0, $e);
        }
        $obj = null;
        if ($row = $stmt->fetch(\PDO::FETCH_NUM)) {
            $cls = AssoPrestationsArchiveTableMap::getOMClass($row, 0, false);
            /** @var ChildAssoPrestationsArchive $obj */
            $obj = new $cls();
            $obj->hydrate($row);
            AssoPrestationsArchiveTableMap::addInstanceToPool($obj, null === $key || is_scalar($key) || is_callable([$key, '__toString']) ? (string) $key : $key);
        }
        $stmt->closeCursor();

        return $obj;
    }

    /**
     * Find object by primary key.
     *
     * @param     mixed $key Primary key to use for the query
     * @param     ConnectionInterface $con A connection object
     *
     * @return ChildAssoPrestationsArchive|array|mixed the result, formatted by the current formatter
     */
    protected function findPkComplex($key, ConnectionInterface $con)
    {
        // As the query uses a PK condition, no limit(1) is necessary.
        $criteria = $this->isKeepQuery() ? clone $this : $this;
        $dataFetcher = $criteria
            ->filterByPrimaryKey($key)
            ->doSelect($con);

        return $criteria->getFormatter()->init($criteria)->formatOne($dataFetcher);
    }

    /**
     * Find objects by primary key
     * <code>
     * $objs = $c->findPks(array(12, 56, 832), $con);
     * </code>
     * @param     array $keys Primary keys to use for the query
     * @param     ConnectionInterface $con an optional connection object
     *
     * @return ObjectCollection|array|mixed the list of results, formatted by the current formatter
     */
    public function findPks($keys, ConnectionInterface $con = null)
    {
        if (null === $con) {
            $con = Propel::getServiceContainer()->getReadConnection($this->getDbName());
        }
        $this->basePreSelect($con);
        $criteria = $this->isKeepQuery() ? clone $this : $this;
        $dataFetcher = $criteria
            ->filterByPrimaryKeys($keys)
            ->doSelect($con);

        return $criteria->getFormatter()->init($criteria)->format($dataFetcher);
    }

    /**
     * Filter the query by primary key
     *
     * @param     mixed $key Primary key to use for the query
     *
     * @return $this|ChildAssoPrestationsArchiveQuery The current query, for fluid interface
     */
    public function filterByPrimaryKey($key)
    {

        return $this->addUsingAlias(AssoPrestationsArchiveTableMap::COL_ID_PRESTATION, $key, Criteria::EQUAL);
    }

    /**
     * Filter the query by a list of primary keys
     *
     * @param     array $keys The list of primary key to use for the query
     *
     * @return $this|ChildAssoPrestationsArchiveQuery The current query, for fluid interface
     */
    public function filterByPrimaryKeys($keys)
    {

        return $this->addUsingAlias(AssoPrestationsArchiveTableMap::COL_ID_PRESTATION, $keys, Criteria::IN);
    }

    /**
     * Filter the query on the id_prestation column
     *
     * Example usage:
     * <code>
     * $query->filterByIdPrestation(1234); // WHERE id_prestation = 1234
     * $query->filterByIdPrestation(array(12, 34)); // WHERE id_prestation IN (12, 34)
     * $query->filterByIdPrestation(array('min' => 12)); // WHERE id_prestation > 12
     * </code>
     *
     * @param     mixed $idPrestation The value to use as filter.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildAssoPrestationsArchiveQuery The current query, for fluid interface
     */
    public function filterByIdPrestation($idPrestation = null, $comparison = null)
    {
        if (is_array($idPrestation)) {
            $useMinMax = false;
            if (isset($idPrestation['min'])) {
                $this->addUsingAlias(AssoPrestationsArchiveTableMap::COL_ID_PRESTATION, $idPrestation['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($idPrestation['max'])) {
                $this->addUsingAlias(AssoPrestationsArchiveTableMap::COL_ID_PRESTATION, $idPrestation['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(AssoPrestationsArchiveTableMap::COL_ID_PRESTATION, $idPrestation, $comparison);
    }

    /**
     * Filter the query on the nom_groupe column
     *
     * Example usage:
     * <code>
     * $query->filterByNomGroupe('fooValue');   // WHERE nom_groupe = 'fooValue'
     * $query->filterByNomGroupe('%fooValue%', Criteria::LIKE); // WHERE nom_groupe LIKE '%fooValue%'
     * </code>
     *
     * @param     string $nomGroupe The value to use as filter.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildAssoPrestationsArchiveQuery The current query, for fluid interface
     */
    public function filterByNomGroupe($nomGroupe = null, $comparison = null)
    {
        if (null === $comparison) {
            if (is_array($nomGroupe)) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(AssoPrestationsArchiveTableMap::COL_NOM_GROUPE, $nomGroupe, $comparison);
    }

    /**
     * Filter the query on the nom column
     *
     * Example usage:
     * <code>
     * $query->filterByNom('fooValue');   // WHERE nom = 'fooValue'
     * $query->filterByNom('%fooValue%', Criteria::LIKE); // WHERE nom LIKE '%fooValue%'
     * </code>
     *
     * @param     string $nom The value to use as filter.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildAssoPrestationsArchiveQuery The current query, for fluid interface
     */
    public function filterByNom($nom = null, $comparison = null)
    {
        if (null === $comparison) {
            if (is_array($nom)) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(AssoPrestationsArchiveTableMap::COL_NOM, $nom, $comparison);
    }

    /**
     * Filter the query on the nomcourt column
     *
     * Example usage:
     * <code>
     * $query->filterByNomcourt('fooValue');   // WHERE nomcourt = 'fooValue'
     * $query->filterByNomcourt('%fooValue%', Criteria::LIKE); // WHERE nomcourt LIKE '%fooValue%'
     * </code>
     *
     * @param     string $nomcourt The value to use as filter.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildAssoPrestationsArchiveQuery The current query, for fluid interface
     */
    public function filterByNomcourt($nomcourt = null, $comparison = null)
    {
        if (null === $comparison) {
            if (is_array($nomcourt)) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(AssoPrestationsArchiveTableMap::COL_NOMCOURT, $nomcourt, $comparison);
    }

    /**
     * Filter the query on the descriptif column
     *
     * Example usage:
     * <code>
     * $query->filterByDescriptif('fooValue');   // WHERE descriptif = 'fooValue'
     * $query->filterByDescriptif('%fooValue%', Criteria::LIKE); // WHERE descriptif LIKE '%fooValue%'
     * </code>
     *
     * @param     string $descriptif The value to use as filter.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildAssoPrestationsArchiveQuery The current query, for fluid interface
     */
    public function filterByDescriptif($descriptif = null, $comparison = null)
    {
        if (null === $comparison) {
            if (is_array($descriptif)) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(AssoPrestationsArchiveTableMap::COL_DESCRIPTIF, $descriptif, $comparison);
    }

    /**
     * Filter the query on the id_entite column
     *
     * Example usage:
     * <code>
     * $query->filterByIdEntite(1234); // WHERE id_entite = 1234
     * $query->filterByIdEntite(array(12, 34)); // WHERE id_entite IN (12, 34)
     * $query->filterByIdEntite(array('min' => 12)); // WHERE id_entite > 12
     * </code>
     *
     * @param     mixed $idEntite The value to use as filter.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildAssoPrestationsArchiveQuery The current query, for fluid interface
     */
    public function filterByIdEntite($idEntite = null, $comparison = null)
    {
        if (is_array($idEntite)) {
            $useMinMax = false;
            if (isset($idEntite['min'])) {
                $this->addUsingAlias(AssoPrestationsArchiveTableMap::COL_ID_ENTITE, $idEntite['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($idEntite['max'])) {
                $this->addUsingAlias(AssoPrestationsArchiveTableMap::COL_ID_ENTITE, $idEntite['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(AssoPrestationsArchiveTableMap::COL_ID_ENTITE, $idEntite, $comparison);
    }

    /**
     * Filter the query on the id_tva column
     *
     * Example usage:
     * <code>
     * $query->filterByIdTva(1234); // WHERE id_tva = 1234
     * $query->filterByIdTva(array(12, 34)); // WHERE id_tva IN (12, 34)
     * $query->filterByIdTva(array('min' => 12)); // WHERE id_tva > 12
     * </code>
     *
     * @param     mixed $idTva The value to use as filter.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildAssoPrestationsArchiveQuery The current query, for fluid interface
     */
    public function filterByIdTva($idTva = null, $comparison = null)
    {
        if (is_array($idTva)) {
            $useMinMax = false;
            if (isset($idTva['min'])) {
                $this->addUsingAlias(AssoPrestationsArchiveTableMap::COL_ID_TVA, $idTva['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($idTva['max'])) {
                $this->addUsingAlias(AssoPrestationsArchiveTableMap::COL_ID_TVA, $idTva['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(AssoPrestationsArchiveTableMap::COL_ID_TVA, $idTva, $comparison);
    }

    /**
     * Filter the query on the retard_jours column
     *
     * Example usage:
     * <code>
     * $query->filterByRetardJours(1234); // WHERE retard_jours = 1234
     * $query->filterByRetardJours(array(12, 34)); // WHERE retard_jours IN (12, 34)
     * $query->filterByRetardJours(array('min' => 12)); // WHERE retard_jours > 12
     * </code>
     *
     * @param     mixed $retardJours The value to use as filter.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildAssoPrestationsArchiveQuery The current query, for fluid interface
     */
    public function filterByRetardJours($retardJours = null, $comparison = null)
    {
        if (is_array($retardJours)) {
            $useMinMax = false;
            if (isset($retardJours['min'])) {
                $this->addUsingAlias(AssoPrestationsArchiveTableMap::COL_RETARD_JOURS, $retardJours['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($retardJours['max'])) {
                $this->addUsingAlias(AssoPrestationsArchiveTableMap::COL_RETARD_JOURS, $retardJours['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(AssoPrestationsArchiveTableMap::COL_RETARD_JOURS, $retardJours, $comparison);
    }

    /**
     * Filter the query on the nombre_numero column
     *
     * Example usage:
     * <code>
     * $query->filterByNombreNumero(1234); // WHERE nombre_numero = 1234
     * $query->filterByNombreNumero(array(12, 34)); // WHERE nombre_numero IN (12, 34)
     * $query->filterByNombreNumero(array('min' => 12)); // WHERE nombre_numero > 12
     * </code>
     *
     * @param     mixed $nombreNumero The value to use as filter.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildAssoPrestationsArchiveQuery The current query, for fluid interface
     */
    public function filterByNombreNumero($nombreNumero = null, $comparison = null)
    {
        if (is_array($nombreNumero)) {
            $useMinMax = false;
            if (isset($nombreNumero['min'])) {
                $this->addUsingAlias(AssoPrestationsArchiveTableMap::COL_NOMBRE_NUMERO, $nombreNumero['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($nombreNumero['max'])) {
                $this->addUsingAlias(AssoPrestationsArchiveTableMap::COL_NOMBRE_NUMERO, $nombreNumero['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(AssoPrestationsArchiveTableMap::COL_NOMBRE_NUMERO, $nombreNumero, $comparison);
    }

    /**
     * Filter the query on the prochain_numero column
     *
     * Example usage:
     * <code>
     * $query->filterByProchainNumero(1234); // WHERE prochain_numero = 1234
     * $query->filterByProchainNumero(array(12, 34)); // WHERE prochain_numero IN (12, 34)
     * $query->filterByProchainNumero(array('min' => 12)); // WHERE prochain_numero > 12
     * </code>
     *
     * @param     mixed $prochainNumero The value to use as filter.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildAssoPrestationsArchiveQuery The current query, for fluid interface
     */
    public function filterByProchainNumero($prochainNumero = null, $comparison = null)
    {
        if (is_array($prochainNumero)) {
            $useMinMax = false;
            if (isset($prochainNumero['min'])) {
                $this->addUsingAlias(AssoPrestationsArchiveTableMap::COL_PROCHAIN_NUMERO, $prochainNumero['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($prochainNumero['max'])) {
                $this->addUsingAlias(AssoPrestationsArchiveTableMap::COL_PROCHAIN_NUMERO, $prochainNumero['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(AssoPrestationsArchiveTableMap::COL_PROCHAIN_NUMERO, $prochainNumero, $comparison);
    }

    /**
     * Filter the query on the prixlibre column
     *
     * Example usage:
     * <code>
     * $query->filterByPrixlibre(true); // WHERE prixlibre = true
     * $query->filterByPrixlibre('yes'); // WHERE prixlibre = true
     * </code>
     *
     * @param     boolean|string $prixlibre The value to use as filter.
     *              Non-boolean arguments are converted using the following rules:
     *                * 1, '1', 'true',  'on',  and 'yes' are converted to boolean true
     *                * 0, '0', 'false', 'off', and 'no'  are converted to boolean false
     *              Check on string values is case insensitive (so 'FaLsE' is seen as 'false').
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildAssoPrestationsArchiveQuery The current query, for fluid interface
     */
    public function filterByPrixlibre($prixlibre = null, $comparison = null)
    {
        if (is_string($prixlibre)) {
            $prixlibre = in_array(strtolower($prixlibre), array('false', 'off', '-', 'no', 'n', '0', '')) ? false : true;
        }

        return $this->addUsingAlias(AssoPrestationsArchiveTableMap::COL_PRIXLIBRE, $prixlibre, $comparison);
    }

    /**
     * Filter the query on the quantite column
     *
     * Example usage:
     * <code>
     * $query->filterByQuantite(true); // WHERE quantite = true
     * $query->filterByQuantite('yes'); // WHERE quantite = true
     * </code>
     *
     * @param     boolean|string $quantite The value to use as filter.
     *              Non-boolean arguments are converted using the following rules:
     *                * 1, '1', 'true',  'on',  and 'yes' are converted to boolean true
     *                * 0, '0', 'false', 'off', and 'no'  are converted to boolean false
     *              Check on string values is case insensitive (so 'FaLsE' is seen as 'false').
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildAssoPrestationsArchiveQuery The current query, for fluid interface
     */
    public function filterByQuantite($quantite = null, $comparison = null)
    {
        if (is_string($quantite)) {
            $quantite = in_array(strtolower($quantite), array('false', 'off', '-', 'no', 'n', '0', '')) ? false : true;
        }

        return $this->addUsingAlias(AssoPrestationsArchiveTableMap::COL_QUANTITE, $quantite, $comparison);
    }

    /**
     * Filter the query on the id_unite column
     *
     * Example usage:
     * <code>
     * $query->filterByIdUnite(1234); // WHERE id_unite = 1234
     * $query->filterByIdUnite(array(12, 34)); // WHERE id_unite IN (12, 34)
     * $query->filterByIdUnite(array('min' => 12)); // WHERE id_unite > 12
     * </code>
     *
     * @param     mixed $idUnite The value to use as filter.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildAssoPrestationsArchiveQuery The current query, for fluid interface
     */
    public function filterByIdUnite($idUnite = null, $comparison = null)
    {
        if (is_array($idUnite)) {
            $useMinMax = false;
            if (isset($idUnite['min'])) {
                $this->addUsingAlias(AssoPrestationsArchiveTableMap::COL_ID_UNITE, $idUnite['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($idUnite['max'])) {
                $this->addUsingAlias(AssoPrestationsArchiveTableMap::COL_ID_UNITE, $idUnite['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(AssoPrestationsArchiveTableMap::COL_ID_UNITE, $idUnite, $comparison);
    }

    /**
     * Filter the query on the id_compte column
     *
     * Example usage:
     * <code>
     * $query->filterByIdCompte(1234); // WHERE id_compte = 1234
     * $query->filterByIdCompte(array(12, 34)); // WHERE id_compte IN (12, 34)
     * $query->filterByIdCompte(array('min' => 12)); // WHERE id_compte > 12
     * </code>
     *
     * @param     mixed $idCompte The value to use as filter.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildAssoPrestationsArchiveQuery The current query, for fluid interface
     */
    public function filterByIdCompte($idCompte = null, $comparison = null)
    {
        if (is_array($idCompte)) {
            $useMinMax = false;
            if (isset($idCompte['min'])) {
                $this->addUsingAlias(AssoPrestationsArchiveTableMap::COL_ID_COMPTE, $idCompte['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($idCompte['max'])) {
                $this->addUsingAlias(AssoPrestationsArchiveTableMap::COL_ID_COMPTE, $idCompte['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(AssoPrestationsArchiveTableMap::COL_ID_COMPTE, $idCompte, $comparison);
    }

    /**
     * Filter the query on the prestation_type column
     *
     * Example usage:
     * <code>
     * $query->filterByPrestationType(1234); // WHERE prestation_type = 1234
     * $query->filterByPrestationType(array(12, 34)); // WHERE prestation_type IN (12, 34)
     * $query->filterByPrestationType(array('min' => 12)); // WHERE prestation_type > 12
     * </code>
     *
     * @param     mixed $prestationType The value to use as filter.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildAssoPrestationsArchiveQuery The current query, for fluid interface
     */
    public function filterByPrestationType($prestationType = null, $comparison = null)
    {
        if (is_array($prestationType)) {
            $useMinMax = false;
            if (isset($prestationType['min'])) {
                $this->addUsingAlias(AssoPrestationsArchiveTableMap::COL_PRESTATION_TYPE, $prestationType['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($prestationType['max'])) {
                $this->addUsingAlias(AssoPrestationsArchiveTableMap::COL_PRESTATION_TYPE, $prestationType['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(AssoPrestationsArchiveTableMap::COL_PRESTATION_TYPE, $prestationType, $comparison);
    }

    /**
     * Filter the query on the active column
     *
     * Example usage:
     * <code>
     * $query->filterByActive(true); // WHERE active = true
     * $query->filterByActive('yes'); // WHERE active = true
     * </code>
     *
     * @param     boolean|string $active The value to use as filter.
     *              Non-boolean arguments are converted using the following rules:
     *                * 1, '1', 'true',  'on',  and 'yes' are converted to boolean true
     *                * 0, '0', 'false', 'off', and 'no'  are converted to boolean false
     *              Check on string values is case insensitive (so 'FaLsE' is seen as 'false').
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildAssoPrestationsArchiveQuery The current query, for fluid interface
     */
    public function filterByActive($active = null, $comparison = null)
    {
        if (is_string($active)) {
            $active = in_array(strtolower($active), array('false', 'off', '-', 'no', 'n', '0', '')) ? false : true;
        }

        return $this->addUsingAlias(AssoPrestationsArchiveTableMap::COL_ACTIVE, $active, $comparison);
    }

    /**
     * Filter the query on the nb_voix column
     *
     * Example usage:
     * <code>
     * $query->filterByNbVoix(1234); // WHERE nb_voix = 1234
     * $query->filterByNbVoix(array(12, 34)); // WHERE nb_voix IN (12, 34)
     * $query->filterByNbVoix(array('min' => 12)); // WHERE nb_voix > 12
     * </code>
     *
     * @param     mixed $nbVoix The value to use as filter.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildAssoPrestationsArchiveQuery The current query, for fluid interface
     */
    public function filterByNbVoix($nbVoix = null, $comparison = null)
    {
        if (is_array($nbVoix)) {
            $useMinMax = false;
            if (isset($nbVoix['min'])) {
                $this->addUsingAlias(AssoPrestationsArchiveTableMap::COL_NB_VOIX, $nbVoix['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($nbVoix['max'])) {
                $this->addUsingAlias(AssoPrestationsArchiveTableMap::COL_NB_VOIX, $nbVoix['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(AssoPrestationsArchiveTableMap::COL_NB_VOIX, $nbVoix, $comparison);
    }

    /**
     * Filter the query on the periodique column
     *
     * Example usage:
     * <code>
     * $query->filterByPeriodique('fooValue');   // WHERE periodique = 'fooValue'
     * $query->filterByPeriodique('%fooValue%', Criteria::LIKE); // WHERE periodique LIKE '%fooValue%'
     * </code>
     *
     * @param     string $periodique The value to use as filter.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildAssoPrestationsArchiveQuery The current query, for fluid interface
     */
    public function filterByPeriodique($periodique = null, $comparison = null)
    {
        if (null === $comparison) {
            if (is_array($periodique)) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(AssoPrestationsArchiveTableMap::COL_PERIODIQUE, $periodique, $comparison);
    }

    /**
     * Filter the query on the signe column
     *
     * Example usage:
     * <code>
     * $query->filterBySigne('fooValue');   // WHERE signe = 'fooValue'
     * $query->filterBySigne('%fooValue%', Criteria::LIKE); // WHERE signe LIKE '%fooValue%'
     * </code>
     *
     * @param     string $signe The value to use as filter.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildAssoPrestationsArchiveQuery The current query, for fluid interface
     */
    public function filterBySigne($signe = null, $comparison = null)
    {
        if (null === $comparison) {
            if (is_array($signe)) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(AssoPrestationsArchiveTableMap::COL_SIGNE, $signe, $comparison);
    }

    /**
     * Filter the query on the objet_beneficiaire column
     *
     * Example usage:
     * <code>
     * $query->filterByObjetBeneficiaire('fooValue');   // WHERE objet_beneficiaire = 'fooValue'
     * $query->filterByObjetBeneficiaire('%fooValue%', Criteria::LIKE); // WHERE objet_beneficiaire LIKE '%fooValue%'
     * </code>
     *
     * @param     string $objetBeneficiaire The value to use as filter.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildAssoPrestationsArchiveQuery The current query, for fluid interface
     */
    public function filterByObjetBeneficiaire($objetBeneficiaire = null, $comparison = null)
    {
        if (null === $comparison) {
            if (is_array($objetBeneficiaire)) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(AssoPrestationsArchiveTableMap::COL_OBJET_BENEFICIAIRE, $objetBeneficiaire, $comparison);
    }

    /**
     * Filter the query on the created_at column
     *
     * Example usage:
     * <code>
     * $query->filterByCreatedAt('2011-03-14'); // WHERE created_at = '2011-03-14'
     * $query->filterByCreatedAt('now'); // WHERE created_at = '2011-03-14'
     * $query->filterByCreatedAt(array('max' => 'yesterday')); // WHERE created_at > '2011-03-13'
     * </code>
     *
     * @param     mixed $createdAt The value to use as filter.
     *              Values can be integers (unix timestamps), DateTime objects, or strings.
     *              Empty strings are treated as NULL.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildAssoPrestationsArchiveQuery The current query, for fluid interface
     */
    public function filterByCreatedAt($createdAt = null, $comparison = null)
    {
        if (is_array($createdAt)) {
            $useMinMax = false;
            if (isset($createdAt['min'])) {
                $this->addUsingAlias(AssoPrestationsArchiveTableMap::COL_CREATED_AT, $createdAt['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($createdAt['max'])) {
                $this->addUsingAlias(AssoPrestationsArchiveTableMap::COL_CREATED_AT, $createdAt['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(AssoPrestationsArchiveTableMap::COL_CREATED_AT, $createdAt, $comparison);
    }

    /**
     * Filter the query on the updated_at column
     *
     * Example usage:
     * <code>
     * $query->filterByUpdatedAt('2011-03-14'); // WHERE updated_at = '2011-03-14'
     * $query->filterByUpdatedAt('now'); // WHERE updated_at = '2011-03-14'
     * $query->filterByUpdatedAt(array('max' => 'yesterday')); // WHERE updated_at > '2011-03-13'
     * </code>
     *
     * @param     mixed $updatedAt The value to use as filter.
     *              Values can be integers (unix timestamps), DateTime objects, or strings.
     *              Empty strings are treated as NULL.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildAssoPrestationsArchiveQuery The current query, for fluid interface
     */
    public function filterByUpdatedAt($updatedAt = null, $comparison = null)
    {
        if (is_array($updatedAt)) {
            $useMinMax = false;
            if (isset($updatedAt['min'])) {
                $this->addUsingAlias(AssoPrestationsArchiveTableMap::COL_UPDATED_AT, $updatedAt['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($updatedAt['max'])) {
                $this->addUsingAlias(AssoPrestationsArchiveTableMap::COL_UPDATED_AT, $updatedAt['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(AssoPrestationsArchiveTableMap::COL_UPDATED_AT, $updatedAt, $comparison);
    }

    /**
     * Filter the query on the archived_at column
     *
     * Example usage:
     * <code>
     * $query->filterByArchivedAt('2011-03-14'); // WHERE archived_at = '2011-03-14'
     * $query->filterByArchivedAt('now'); // WHERE archived_at = '2011-03-14'
     * $query->filterByArchivedAt(array('max' => 'yesterday')); // WHERE archived_at > '2011-03-13'
     * </code>
     *
     * @param     mixed $archivedAt The value to use as filter.
     *              Values can be integers (unix timestamps), DateTime objects, or strings.
     *              Empty strings are treated as NULL.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildAssoPrestationsArchiveQuery The current query, for fluid interface
     */
    public function filterByArchivedAt($archivedAt = null, $comparison = null)
    {
        if (is_array($archivedAt)) {
            $useMinMax = false;
            if (isset($archivedAt['min'])) {
                $this->addUsingAlias(AssoPrestationsArchiveTableMap::COL_ARCHIVED_AT, $archivedAt['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($archivedAt['max'])) {
                $this->addUsingAlias(AssoPrestationsArchiveTableMap::COL_ARCHIVED_AT, $archivedAt['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(AssoPrestationsArchiveTableMap::COL_ARCHIVED_AT, $archivedAt, $comparison);
    }

    /**
     * Exclude object from result
     *
     * @param   ChildAssoPrestationsArchive $assoPrestationsArchive Object to remove from the list of results
     *
     * @return $this|ChildAssoPrestationsArchiveQuery The current query, for fluid interface
     */
    public function prune($assoPrestationsArchive = null)
    {
        if ($assoPrestationsArchive) {
            $this->addUsingAlias(AssoPrestationsArchiveTableMap::COL_ID_PRESTATION, $assoPrestationsArchive->getIdPrestation(), Criteria::NOT_EQUAL);
        }

        return $this;
    }

    /**
     * Deletes all rows from the asso_prestations_archive table.
     *
     * @param ConnectionInterface $con the connection to use
     * @return int The number of affected rows (if supported by underlying database driver).
     */
    public function doDeleteAll(ConnectionInterface $con = null)
    {
        if (null === $con) {
            $con = Propel::getServiceContainer()->getWriteConnection(AssoPrestationsArchiveTableMap::DATABASE_NAME);
        }

        // use transaction because $criteria could contain info
        // for more than one table or we could emulating ON DELETE CASCADE, etc.
        return $con->transaction(function () use ($con) {
            $affectedRows = 0; // initialize var to track total num of affected rows
            $affectedRows += parent::doDeleteAll($con);
            // Because this db requires some delete cascade/set null emulation, we have to
            // clear the cached instance *after* the emulation has happened (since
            // instances get re-added by the select statement contained therein).
            AssoPrestationsArchiveTableMap::clearInstancePool();
            AssoPrestationsArchiveTableMap::clearRelatedInstancePool();

            return $affectedRows;
        });
    }

    /**
     * Performs a DELETE on the database based on the current ModelCriteria
     *
     * @param ConnectionInterface $con the connection to use
     * @return int             The number of affected rows (if supported by underlying database driver).  This includes CASCADE-related rows
     *                         if supported by native driver or if emulated using Propel.
     * @throws PropelException Any exceptions caught during processing will be
     *                         rethrown wrapped into a PropelException.
     */
    public function delete(ConnectionInterface $con = null)
    {
        if (null === $con) {
            $con = Propel::getServiceContainer()->getWriteConnection(AssoPrestationsArchiveTableMap::DATABASE_NAME);
        }

        $criteria = $this;

        // Set the correct dbName
        $criteria->setDbName(AssoPrestationsArchiveTableMap::DATABASE_NAME);

        // use transaction because $criteria could contain info
        // for more than one table or we could emulating ON DELETE CASCADE, etc.
        return $con->transaction(function () use ($con, $criteria) {
            $affectedRows = 0; // initialize var to track total num of affected rows

            AssoPrestationsArchiveTableMap::removeInstanceFromPool($criteria);

            $affectedRows += ModelCriteria::delete($con);
            AssoPrestationsArchiveTableMap::clearRelatedInstancePool();

            return $affectedRows;
        });
    }

} // AssoPrestationsArchiveQuery
