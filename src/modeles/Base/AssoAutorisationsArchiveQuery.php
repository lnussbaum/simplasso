<?php

namespace Base;

use \AssoAutorisationsArchive as ChildAssoAutorisationsArchive;
use \AssoAutorisationsArchiveQuery as ChildAssoAutorisationsArchiveQuery;
use \Exception;
use \PDO;
use Map\AssoAutorisationsArchiveTableMap;
use Propel\Runtime\Propel;
use Propel\Runtime\ActiveQuery\Criteria;
use Propel\Runtime\ActiveQuery\ModelCriteria;
use Propel\Runtime\Collection\ObjectCollection;
use Propel\Runtime\Connection\ConnectionInterface;
use Propel\Runtime\Exception\PropelException;

/**
 * Base class that represents a query for the 'asso_autorisations_archive' table.
 *
 *
 *
 * @method     ChildAssoAutorisationsArchiveQuery orderByIdAutorisation($order = Criteria::ASC) Order by the id_autorisation column
 * @method     ChildAssoAutorisationsArchiveQuery orderByIdIndividu($order = Criteria::ASC) Order by the id_individu column
 * @method     ChildAssoAutorisationsArchiveQuery orderByProfil($order = Criteria::ASC) Order by the profil column
 * @method     ChildAssoAutorisationsArchiveQuery orderByNiveau($order = Criteria::ASC) Order by the niveau column
 * @method     ChildAssoAutorisationsArchiveQuery orderByIdEntite($order = Criteria::ASC) Order by the id_entite column
 * @method     ChildAssoAutorisationsArchiveQuery orderByIdRestriction($order = Criteria::ASC) Order by the id_restriction column
 * @method     ChildAssoAutorisationsArchiveQuery orderByVariables($order = Criteria::ASC) Order by the variables column
 * @method     ChildAssoAutorisationsArchiveQuery orderByCreatedAt($order = Criteria::ASC) Order by the created_at column
 * @method     ChildAssoAutorisationsArchiveQuery orderByUpdatedAt($order = Criteria::ASC) Order by the updated_at column
 * @method     ChildAssoAutorisationsArchiveQuery orderByArchivedAt($order = Criteria::ASC) Order by the archived_at column
 *
 * @method     ChildAssoAutorisationsArchiveQuery groupByIdAutorisation() Group by the id_autorisation column
 * @method     ChildAssoAutorisationsArchiveQuery groupByIdIndividu() Group by the id_individu column
 * @method     ChildAssoAutorisationsArchiveQuery groupByProfil() Group by the profil column
 * @method     ChildAssoAutorisationsArchiveQuery groupByNiveau() Group by the niveau column
 * @method     ChildAssoAutorisationsArchiveQuery groupByIdEntite() Group by the id_entite column
 * @method     ChildAssoAutorisationsArchiveQuery groupByIdRestriction() Group by the id_restriction column
 * @method     ChildAssoAutorisationsArchiveQuery groupByVariables() Group by the variables column
 * @method     ChildAssoAutorisationsArchiveQuery groupByCreatedAt() Group by the created_at column
 * @method     ChildAssoAutorisationsArchiveQuery groupByUpdatedAt() Group by the updated_at column
 * @method     ChildAssoAutorisationsArchiveQuery groupByArchivedAt() Group by the archived_at column
 *
 * @method     ChildAssoAutorisationsArchiveQuery leftJoin($relation) Adds a LEFT JOIN clause to the query
 * @method     ChildAssoAutorisationsArchiveQuery rightJoin($relation) Adds a RIGHT JOIN clause to the query
 * @method     ChildAssoAutorisationsArchiveQuery innerJoin($relation) Adds a INNER JOIN clause to the query
 *
 * @method     ChildAssoAutorisationsArchiveQuery leftJoinWith($relation) Adds a LEFT JOIN clause and with to the query
 * @method     ChildAssoAutorisationsArchiveQuery rightJoinWith($relation) Adds a RIGHT JOIN clause and with to the query
 * @method     ChildAssoAutorisationsArchiveQuery innerJoinWith($relation) Adds a INNER JOIN clause and with to the query
 *
 * @method     ChildAssoAutorisationsArchive findOne(ConnectionInterface $con = null) Return the first ChildAssoAutorisationsArchive matching the query
 * @method     ChildAssoAutorisationsArchive findOneOrCreate(ConnectionInterface $con = null) Return the first ChildAssoAutorisationsArchive matching the query, or a new ChildAssoAutorisationsArchive object populated from the query conditions when no match is found
 *
 * @method     ChildAssoAutorisationsArchive findOneByIdAutorisation(string $id_autorisation) Return the first ChildAssoAutorisationsArchive filtered by the id_autorisation column
 * @method     ChildAssoAutorisationsArchive findOneByIdIndividu(string $id_individu) Return the first ChildAssoAutorisationsArchive filtered by the id_individu column
 * @method     ChildAssoAutorisationsArchive findOneByProfil(string $profil) Return the first ChildAssoAutorisationsArchive filtered by the profil column
 * @method     ChildAssoAutorisationsArchive findOneByNiveau(int $niveau) Return the first ChildAssoAutorisationsArchive filtered by the niveau column
 * @method     ChildAssoAutorisationsArchive findOneByIdEntite(string $id_entite) Return the first ChildAssoAutorisationsArchive filtered by the id_entite column
 * @method     ChildAssoAutorisationsArchive findOneByIdRestriction(int $id_restriction) Return the first ChildAssoAutorisationsArchive filtered by the id_restriction column
 * @method     ChildAssoAutorisationsArchive findOneByVariables(string $variables) Return the first ChildAssoAutorisationsArchive filtered by the variables column
 * @method     ChildAssoAutorisationsArchive findOneByCreatedAt(string $created_at) Return the first ChildAssoAutorisationsArchive filtered by the created_at column
 * @method     ChildAssoAutorisationsArchive findOneByUpdatedAt(string $updated_at) Return the first ChildAssoAutorisationsArchive filtered by the updated_at column
 * @method     ChildAssoAutorisationsArchive findOneByArchivedAt(string $archived_at) Return the first ChildAssoAutorisationsArchive filtered by the archived_at column *

 * @method     ChildAssoAutorisationsArchive requirePk($key, ConnectionInterface $con = null) Return the ChildAssoAutorisationsArchive by primary key and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildAssoAutorisationsArchive requireOne(ConnectionInterface $con = null) Return the first ChildAssoAutorisationsArchive matching the query and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 *
 * @method     ChildAssoAutorisationsArchive requireOneByIdAutorisation(string $id_autorisation) Return the first ChildAssoAutorisationsArchive filtered by the id_autorisation column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildAssoAutorisationsArchive requireOneByIdIndividu(string $id_individu) Return the first ChildAssoAutorisationsArchive filtered by the id_individu column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildAssoAutorisationsArchive requireOneByProfil(string $profil) Return the first ChildAssoAutorisationsArchive filtered by the profil column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildAssoAutorisationsArchive requireOneByNiveau(int $niveau) Return the first ChildAssoAutorisationsArchive filtered by the niveau column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildAssoAutorisationsArchive requireOneByIdEntite(string $id_entite) Return the first ChildAssoAutorisationsArchive filtered by the id_entite column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildAssoAutorisationsArchive requireOneByIdRestriction(int $id_restriction) Return the first ChildAssoAutorisationsArchive filtered by the id_restriction column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildAssoAutorisationsArchive requireOneByVariables(string $variables) Return the first ChildAssoAutorisationsArchive filtered by the variables column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildAssoAutorisationsArchive requireOneByCreatedAt(string $created_at) Return the first ChildAssoAutorisationsArchive filtered by the created_at column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildAssoAutorisationsArchive requireOneByUpdatedAt(string $updated_at) Return the first ChildAssoAutorisationsArchive filtered by the updated_at column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildAssoAutorisationsArchive requireOneByArchivedAt(string $archived_at) Return the first ChildAssoAutorisationsArchive filtered by the archived_at column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 *
 * @method     ChildAssoAutorisationsArchive[]|ObjectCollection find(ConnectionInterface $con = null) Return ChildAssoAutorisationsArchive objects based on current ModelCriteria
 * @method     ChildAssoAutorisationsArchive[]|ObjectCollection findByIdAutorisation(string $id_autorisation) Return ChildAssoAutorisationsArchive objects filtered by the id_autorisation column
 * @method     ChildAssoAutorisationsArchive[]|ObjectCollection findByIdIndividu(string $id_individu) Return ChildAssoAutorisationsArchive objects filtered by the id_individu column
 * @method     ChildAssoAutorisationsArchive[]|ObjectCollection findByProfil(string $profil) Return ChildAssoAutorisationsArchive objects filtered by the profil column
 * @method     ChildAssoAutorisationsArchive[]|ObjectCollection findByNiveau(int $niveau) Return ChildAssoAutorisationsArchive objects filtered by the niveau column
 * @method     ChildAssoAutorisationsArchive[]|ObjectCollection findByIdEntite(string $id_entite) Return ChildAssoAutorisationsArchive objects filtered by the id_entite column
 * @method     ChildAssoAutorisationsArchive[]|ObjectCollection findByIdRestriction(int $id_restriction) Return ChildAssoAutorisationsArchive objects filtered by the id_restriction column
 * @method     ChildAssoAutorisationsArchive[]|ObjectCollection findByVariables(string $variables) Return ChildAssoAutorisationsArchive objects filtered by the variables column
 * @method     ChildAssoAutorisationsArchive[]|ObjectCollection findByCreatedAt(string $created_at) Return ChildAssoAutorisationsArchive objects filtered by the created_at column
 * @method     ChildAssoAutorisationsArchive[]|ObjectCollection findByUpdatedAt(string $updated_at) Return ChildAssoAutorisationsArchive objects filtered by the updated_at column
 * @method     ChildAssoAutorisationsArchive[]|ObjectCollection findByArchivedAt(string $archived_at) Return ChildAssoAutorisationsArchive objects filtered by the archived_at column
 * @method     ChildAssoAutorisationsArchive[]|\Propel\Runtime\Util\PropelModelPager paginate($page = 1, $maxPerPage = 10, ConnectionInterface $con = null) Issue a SELECT query based on the current ModelCriteria and uses a page and a maximum number of results per page to compute an offset and a limit
 *
 */
abstract class AssoAutorisationsArchiveQuery extends ModelCriteria
{
    protected $entityNotFoundExceptionClass = '\\Propel\\Runtime\\Exception\\EntityNotFoundException';

    /**
     * Initializes internal state of \Base\AssoAutorisationsArchiveQuery object.
     *
     * @param     string $dbName The database name
     * @param     string $modelName The phpName of a model, e.g. 'Book'
     * @param     string $modelAlias The alias for the model in this query, e.g. 'b'
     */
    public function __construct($dbName = 'simplasso', $modelName = '\\AssoAutorisationsArchive', $modelAlias = null)
    {
        parent::__construct($dbName, $modelName, $modelAlias);
    }

    /**
     * Returns a new ChildAssoAutorisationsArchiveQuery object.
     *
     * @param     string $modelAlias The alias of a model in the query
     * @param     Criteria $criteria Optional Criteria to build the query from
     *
     * @return ChildAssoAutorisationsArchiveQuery
     */
    public static function create($modelAlias = null, Criteria $criteria = null)
    {
        if ($criteria instanceof ChildAssoAutorisationsArchiveQuery) {
            return $criteria;
        }
        $query = new ChildAssoAutorisationsArchiveQuery();
        if (null !== $modelAlias) {
            $query->setModelAlias($modelAlias);
        }
        if ($criteria instanceof Criteria) {
            $query->mergeWith($criteria);
        }

        return $query;
    }

    /**
     * Find object by primary key.
     * Propel uses the instance pool to skip the database if the object exists.
     * Go fast if the query is untouched.
     *
     * <code>
     * $obj  = $c->findPk(12, $con);
     * </code>
     *
     * @param mixed $key Primary key to use for the query
     * @param ConnectionInterface $con an optional connection object
     *
     * @return ChildAssoAutorisationsArchive|array|mixed the result, formatted by the current formatter
     */
    public function findPk($key, ConnectionInterface $con = null)
    {
        if ($key === null) {
            return null;
        }

        if ($con === null) {
            $con = Propel::getServiceContainer()->getReadConnection(AssoAutorisationsArchiveTableMap::DATABASE_NAME);
        }

        $this->basePreSelect($con);

        if (
            $this->formatter || $this->modelAlias || $this->with || $this->select
            || $this->selectColumns || $this->asColumns || $this->selectModifiers
            || $this->map || $this->having || $this->joins
        ) {
            return $this->findPkComplex($key, $con);
        }

        if ((null !== ($obj = AssoAutorisationsArchiveTableMap::getInstanceFromPool(null === $key || is_scalar($key) || is_callable([$key, '__toString']) ? (string) $key : $key)))) {
            // the object is already in the instance pool
            return $obj;
        }

        return $this->findPkSimple($key, $con);
    }

    /**
     * Find object by primary key using raw SQL to go fast.
     * Bypass doSelect() and the object formatter by using generated code.
     *
     * @param     mixed $key Primary key to use for the query
     * @param     ConnectionInterface $con A connection object
     *
     * @throws \Propel\Runtime\Exception\PropelException
     *
     * @return ChildAssoAutorisationsArchive A model object, or null if the key is not found
     */
    protected function findPkSimple($key, ConnectionInterface $con)
    {
        $sql = 'SELECT `id_autorisation`, `id_individu`, `profil`, `niveau`, `id_entite`, `id_restriction`, `variables`, `created_at`, `updated_at`, `archived_at` FROM `asso_autorisations_archive` WHERE `id_autorisation` = :p0';
        try {
            $stmt = $con->prepare($sql);
            $stmt->bindValue(':p0', $key, PDO::PARAM_INT);
            $stmt->execute();
        } catch (Exception $e) {
            Propel::log($e->getMessage(), Propel::LOG_ERR);
            throw new PropelException(sprintf('Unable to execute SELECT statement [%s]', $sql), 0, $e);
        }
        $obj = null;
        if ($row = $stmt->fetch(\PDO::FETCH_NUM)) {
            /** @var ChildAssoAutorisationsArchive $obj */
            $obj = new ChildAssoAutorisationsArchive();
            $obj->hydrate($row);
            AssoAutorisationsArchiveTableMap::addInstanceToPool($obj, null === $key || is_scalar($key) || is_callable([$key, '__toString']) ? (string) $key : $key);
        }
        $stmt->closeCursor();

        return $obj;
    }

    /**
     * Find object by primary key.
     *
     * @param     mixed $key Primary key to use for the query
     * @param     ConnectionInterface $con A connection object
     *
     * @return ChildAssoAutorisationsArchive|array|mixed the result, formatted by the current formatter
     */
    protected function findPkComplex($key, ConnectionInterface $con)
    {
        // As the query uses a PK condition, no limit(1) is necessary.
        $criteria = $this->isKeepQuery() ? clone $this : $this;
        $dataFetcher = $criteria
            ->filterByPrimaryKey($key)
            ->doSelect($con);

        return $criteria->getFormatter()->init($criteria)->formatOne($dataFetcher);
    }

    /**
     * Find objects by primary key
     * <code>
     * $objs = $c->findPks(array(12, 56, 832), $con);
     * </code>
     * @param     array $keys Primary keys to use for the query
     * @param     ConnectionInterface $con an optional connection object
     *
     * @return ObjectCollection|array|mixed the list of results, formatted by the current formatter
     */
    public function findPks($keys, ConnectionInterface $con = null)
    {
        if (null === $con) {
            $con = Propel::getServiceContainer()->getReadConnection($this->getDbName());
        }
        $this->basePreSelect($con);
        $criteria = $this->isKeepQuery() ? clone $this : $this;
        $dataFetcher = $criteria
            ->filterByPrimaryKeys($keys)
            ->doSelect($con);

        return $criteria->getFormatter()->init($criteria)->format($dataFetcher);
    }

    /**
     * Filter the query by primary key
     *
     * @param     mixed $key Primary key to use for the query
     *
     * @return $this|ChildAssoAutorisationsArchiveQuery The current query, for fluid interface
     */
    public function filterByPrimaryKey($key)
    {

        return $this->addUsingAlias(AssoAutorisationsArchiveTableMap::COL_ID_AUTORISATION, $key, Criteria::EQUAL);
    }

    /**
     * Filter the query by a list of primary keys
     *
     * @param     array $keys The list of primary key to use for the query
     *
     * @return $this|ChildAssoAutorisationsArchiveQuery The current query, for fluid interface
     */
    public function filterByPrimaryKeys($keys)
    {

        return $this->addUsingAlias(AssoAutorisationsArchiveTableMap::COL_ID_AUTORISATION, $keys, Criteria::IN);
    }

    /**
     * Filter the query on the id_autorisation column
     *
     * Example usage:
     * <code>
     * $query->filterByIdAutorisation(1234); // WHERE id_autorisation = 1234
     * $query->filterByIdAutorisation(array(12, 34)); // WHERE id_autorisation IN (12, 34)
     * $query->filterByIdAutorisation(array('min' => 12)); // WHERE id_autorisation > 12
     * </code>
     *
     * @param     mixed $idAutorisation The value to use as filter.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildAssoAutorisationsArchiveQuery The current query, for fluid interface
     */
    public function filterByIdAutorisation($idAutorisation = null, $comparison = null)
    {
        if (is_array($idAutorisation)) {
            $useMinMax = false;
            if (isset($idAutorisation['min'])) {
                $this->addUsingAlias(AssoAutorisationsArchiveTableMap::COL_ID_AUTORISATION, $idAutorisation['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($idAutorisation['max'])) {
                $this->addUsingAlias(AssoAutorisationsArchiveTableMap::COL_ID_AUTORISATION, $idAutorisation['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(AssoAutorisationsArchiveTableMap::COL_ID_AUTORISATION, $idAutorisation, $comparison);
    }

    /**
     * Filter the query on the id_individu column
     *
     * Example usage:
     * <code>
     * $query->filterByIdIndividu(1234); // WHERE id_individu = 1234
     * $query->filterByIdIndividu(array(12, 34)); // WHERE id_individu IN (12, 34)
     * $query->filterByIdIndividu(array('min' => 12)); // WHERE id_individu > 12
     * </code>
     *
     * @param     mixed $idIndividu The value to use as filter.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildAssoAutorisationsArchiveQuery The current query, for fluid interface
     */
    public function filterByIdIndividu($idIndividu = null, $comparison = null)
    {
        if (is_array($idIndividu)) {
            $useMinMax = false;
            if (isset($idIndividu['min'])) {
                $this->addUsingAlias(AssoAutorisationsArchiveTableMap::COL_ID_INDIVIDU, $idIndividu['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($idIndividu['max'])) {
                $this->addUsingAlias(AssoAutorisationsArchiveTableMap::COL_ID_INDIVIDU, $idIndividu['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(AssoAutorisationsArchiveTableMap::COL_ID_INDIVIDU, $idIndividu, $comparison);
    }

    /**
     * Filter the query on the profil column
     *
     * Example usage:
     * <code>
     * $query->filterByProfil('fooValue');   // WHERE profil = 'fooValue'
     * $query->filterByProfil('%fooValue%', Criteria::LIKE); // WHERE profil LIKE '%fooValue%'
     * </code>
     *
     * @param     string $profil The value to use as filter.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildAssoAutorisationsArchiveQuery The current query, for fluid interface
     */
    public function filterByProfil($profil = null, $comparison = null)
    {
        if (null === $comparison) {
            if (is_array($profil)) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(AssoAutorisationsArchiveTableMap::COL_PROFIL, $profil, $comparison);
    }

    /**
     * Filter the query on the niveau column
     *
     * Example usage:
     * <code>
     * $query->filterByNiveau(1234); // WHERE niveau = 1234
     * $query->filterByNiveau(array(12, 34)); // WHERE niveau IN (12, 34)
     * $query->filterByNiveau(array('min' => 12)); // WHERE niveau > 12
     * </code>
     *
     * @param     mixed $niveau The value to use as filter.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildAssoAutorisationsArchiveQuery The current query, for fluid interface
     */
    public function filterByNiveau($niveau = null, $comparison = null)
    {
        if (is_array($niveau)) {
            $useMinMax = false;
            if (isset($niveau['min'])) {
                $this->addUsingAlias(AssoAutorisationsArchiveTableMap::COL_NIVEAU, $niveau['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($niveau['max'])) {
                $this->addUsingAlias(AssoAutorisationsArchiveTableMap::COL_NIVEAU, $niveau['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(AssoAutorisationsArchiveTableMap::COL_NIVEAU, $niveau, $comparison);
    }

    /**
     * Filter the query on the id_entite column
     *
     * Example usage:
     * <code>
     * $query->filterByIdEntite(1234); // WHERE id_entite = 1234
     * $query->filterByIdEntite(array(12, 34)); // WHERE id_entite IN (12, 34)
     * $query->filterByIdEntite(array('min' => 12)); // WHERE id_entite > 12
     * </code>
     *
     * @param     mixed $idEntite The value to use as filter.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildAssoAutorisationsArchiveQuery The current query, for fluid interface
     */
    public function filterByIdEntite($idEntite = null, $comparison = null)
    {
        if (is_array($idEntite)) {
            $useMinMax = false;
            if (isset($idEntite['min'])) {
                $this->addUsingAlias(AssoAutorisationsArchiveTableMap::COL_ID_ENTITE, $idEntite['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($idEntite['max'])) {
                $this->addUsingAlias(AssoAutorisationsArchiveTableMap::COL_ID_ENTITE, $idEntite['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(AssoAutorisationsArchiveTableMap::COL_ID_ENTITE, $idEntite, $comparison);
    }

    /**
     * Filter the query on the id_restriction column
     *
     * Example usage:
     * <code>
     * $query->filterByIdRestriction(1234); // WHERE id_restriction = 1234
     * $query->filterByIdRestriction(array(12, 34)); // WHERE id_restriction IN (12, 34)
     * $query->filterByIdRestriction(array('min' => 12)); // WHERE id_restriction > 12
     * </code>
     *
     * @param     mixed $idRestriction The value to use as filter.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildAssoAutorisationsArchiveQuery The current query, for fluid interface
     */
    public function filterByIdRestriction($idRestriction = null, $comparison = null)
    {
        if (is_array($idRestriction)) {
            $useMinMax = false;
            if (isset($idRestriction['min'])) {
                $this->addUsingAlias(AssoAutorisationsArchiveTableMap::COL_ID_RESTRICTION, $idRestriction['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($idRestriction['max'])) {
                $this->addUsingAlias(AssoAutorisationsArchiveTableMap::COL_ID_RESTRICTION, $idRestriction['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(AssoAutorisationsArchiveTableMap::COL_ID_RESTRICTION, $idRestriction, $comparison);
    }

    /**
     * Filter the query on the variables column
     *
     * Example usage:
     * <code>
     * $query->filterByVariables('fooValue');   // WHERE variables = 'fooValue'
     * $query->filterByVariables('%fooValue%', Criteria::LIKE); // WHERE variables LIKE '%fooValue%'
     * </code>
     *
     * @param     string $variables The value to use as filter.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildAssoAutorisationsArchiveQuery The current query, for fluid interface
     */
    public function filterByVariables($variables = null, $comparison = null)
    {
        if (null === $comparison) {
            if (is_array($variables)) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(AssoAutorisationsArchiveTableMap::COL_VARIABLES, $variables, $comparison);
    }

    /**
     * Filter the query on the created_at column
     *
     * Example usage:
     * <code>
     * $query->filterByCreatedAt('2011-03-14'); // WHERE created_at = '2011-03-14'
     * $query->filterByCreatedAt('now'); // WHERE created_at = '2011-03-14'
     * $query->filterByCreatedAt(array('max' => 'yesterday')); // WHERE created_at > '2011-03-13'
     * </code>
     *
     * @param     mixed $createdAt The value to use as filter.
     *              Values can be integers (unix timestamps), DateTime objects, or strings.
     *              Empty strings are treated as NULL.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildAssoAutorisationsArchiveQuery The current query, for fluid interface
     */
    public function filterByCreatedAt($createdAt = null, $comparison = null)
    {
        if (is_array($createdAt)) {
            $useMinMax = false;
            if (isset($createdAt['min'])) {
                $this->addUsingAlias(AssoAutorisationsArchiveTableMap::COL_CREATED_AT, $createdAt['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($createdAt['max'])) {
                $this->addUsingAlias(AssoAutorisationsArchiveTableMap::COL_CREATED_AT, $createdAt['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(AssoAutorisationsArchiveTableMap::COL_CREATED_AT, $createdAt, $comparison);
    }

    /**
     * Filter the query on the updated_at column
     *
     * Example usage:
     * <code>
     * $query->filterByUpdatedAt('2011-03-14'); // WHERE updated_at = '2011-03-14'
     * $query->filterByUpdatedAt('now'); // WHERE updated_at = '2011-03-14'
     * $query->filterByUpdatedAt(array('max' => 'yesterday')); // WHERE updated_at > '2011-03-13'
     * </code>
     *
     * @param     mixed $updatedAt The value to use as filter.
     *              Values can be integers (unix timestamps), DateTime objects, or strings.
     *              Empty strings are treated as NULL.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildAssoAutorisationsArchiveQuery The current query, for fluid interface
     */
    public function filterByUpdatedAt($updatedAt = null, $comparison = null)
    {
        if (is_array($updatedAt)) {
            $useMinMax = false;
            if (isset($updatedAt['min'])) {
                $this->addUsingAlias(AssoAutorisationsArchiveTableMap::COL_UPDATED_AT, $updatedAt['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($updatedAt['max'])) {
                $this->addUsingAlias(AssoAutorisationsArchiveTableMap::COL_UPDATED_AT, $updatedAt['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(AssoAutorisationsArchiveTableMap::COL_UPDATED_AT, $updatedAt, $comparison);
    }

    /**
     * Filter the query on the archived_at column
     *
     * Example usage:
     * <code>
     * $query->filterByArchivedAt('2011-03-14'); // WHERE archived_at = '2011-03-14'
     * $query->filterByArchivedAt('now'); // WHERE archived_at = '2011-03-14'
     * $query->filterByArchivedAt(array('max' => 'yesterday')); // WHERE archived_at > '2011-03-13'
     * </code>
     *
     * @param     mixed $archivedAt The value to use as filter.
     *              Values can be integers (unix timestamps), DateTime objects, or strings.
     *              Empty strings are treated as NULL.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildAssoAutorisationsArchiveQuery The current query, for fluid interface
     */
    public function filterByArchivedAt($archivedAt = null, $comparison = null)
    {
        if (is_array($archivedAt)) {
            $useMinMax = false;
            if (isset($archivedAt['min'])) {
                $this->addUsingAlias(AssoAutorisationsArchiveTableMap::COL_ARCHIVED_AT, $archivedAt['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($archivedAt['max'])) {
                $this->addUsingAlias(AssoAutorisationsArchiveTableMap::COL_ARCHIVED_AT, $archivedAt['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(AssoAutorisationsArchiveTableMap::COL_ARCHIVED_AT, $archivedAt, $comparison);
    }

    /**
     * Exclude object from result
     *
     * @param   ChildAssoAutorisationsArchive $assoAutorisationsArchive Object to remove from the list of results
     *
     * @return $this|ChildAssoAutorisationsArchiveQuery The current query, for fluid interface
     */
    public function prune($assoAutorisationsArchive = null)
    {
        if ($assoAutorisationsArchive) {
            $this->addUsingAlias(AssoAutorisationsArchiveTableMap::COL_ID_AUTORISATION, $assoAutorisationsArchive->getIdAutorisation(), Criteria::NOT_EQUAL);
        }

        return $this;
    }

    /**
     * Deletes all rows from the asso_autorisations_archive table.
     *
     * @param ConnectionInterface $con the connection to use
     * @return int The number of affected rows (if supported by underlying database driver).
     */
    public function doDeleteAll(ConnectionInterface $con = null)
    {
        if (null === $con) {
            $con = Propel::getServiceContainer()->getWriteConnection(AssoAutorisationsArchiveTableMap::DATABASE_NAME);
        }

        // use transaction because $criteria could contain info
        // for more than one table or we could emulating ON DELETE CASCADE, etc.
        return $con->transaction(function () use ($con) {
            $affectedRows = 0; // initialize var to track total num of affected rows
            $affectedRows += parent::doDeleteAll($con);
            // Because this db requires some delete cascade/set null emulation, we have to
            // clear the cached instance *after* the emulation has happened (since
            // instances get re-added by the select statement contained therein).
            AssoAutorisationsArchiveTableMap::clearInstancePool();
            AssoAutorisationsArchiveTableMap::clearRelatedInstancePool();

            return $affectedRows;
        });
    }

    /**
     * Performs a DELETE on the database based on the current ModelCriteria
     *
     * @param ConnectionInterface $con the connection to use
     * @return int             The number of affected rows (if supported by underlying database driver).  This includes CASCADE-related rows
     *                         if supported by native driver or if emulated using Propel.
     * @throws PropelException Any exceptions caught during processing will be
     *                         rethrown wrapped into a PropelException.
     */
    public function delete(ConnectionInterface $con = null)
    {
        if (null === $con) {
            $con = Propel::getServiceContainer()->getWriteConnection(AssoAutorisationsArchiveTableMap::DATABASE_NAME);
        }

        $criteria = $this;

        // Set the correct dbName
        $criteria->setDbName(AssoAutorisationsArchiveTableMap::DATABASE_NAME);

        // use transaction because $criteria could contain info
        // for more than one table or we could emulating ON DELETE CASCADE, etc.
        return $con->transaction(function () use ($con, $criteria) {
            $affectedRows = 0; // initialize var to track total num of affected rows

            AssoAutorisationsArchiveTableMap::removeInstanceFromPool($criteria);

            $affectedRows += ModelCriteria::delete($con);
            AssoAutorisationsArchiveTableMap::clearRelatedInstancePool();

            return $affectedRows;
        });
    }

} // AssoAutorisationsArchiveQuery
