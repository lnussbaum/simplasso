<?php

namespace Base;

use \Position as ChildPosition;
use \PositionQuery as ChildPositionQuery;
use \Exception;
use \PDO;
use Map\PositionTableMap;
use Propel\Runtime\Propel;
use Propel\Runtime\ActiveQuery\Criteria;
use Propel\Runtime\ActiveQuery\ModelCriteria;
use Propel\Runtime\ActiveQuery\ModelJoin;
use Propel\Runtime\Collection\ObjectCollection;
use Propel\Runtime\Connection\ConnectionInterface;
use Propel\Runtime\Exception\PropelException;

/**
 * Base class that represents a query for the 'geo_positions' table.
 *
 *
 *
 * @method     ChildPositionQuery orderByIdPosition($order = Criteria::ASC) Order by the id_position column
 * @method     ChildPositionQuery orderByTitre($order = Criteria::ASC) Order by the titre column
 * @method     ChildPositionQuery orderByDescriptif($order = Criteria::ASC) Order by the descriptif column
 * @method     ChildPositionQuery orderByLat($order = Criteria::ASC) Order by the lat column
 * @method     ChildPositionQuery orderByLon($order = Criteria::ASC) Order by the lon column
 * @method     ChildPositionQuery orderByZoom($order = Criteria::ASC) Order by the zoom column
 * @method     ChildPositionQuery orderByCreatedAt($order = Criteria::ASC) Order by the created_at column
 * @method     ChildPositionQuery orderByUpdatedAt($order = Criteria::ASC) Order by the updated_at column
 *
 * @method     ChildPositionQuery groupByIdPosition() Group by the id_position column
 * @method     ChildPositionQuery groupByTitre() Group by the titre column
 * @method     ChildPositionQuery groupByDescriptif() Group by the descriptif column
 * @method     ChildPositionQuery groupByLat() Group by the lat column
 * @method     ChildPositionQuery groupByLon() Group by the lon column
 * @method     ChildPositionQuery groupByZoom() Group by the zoom column
 * @method     ChildPositionQuery groupByCreatedAt() Group by the created_at column
 * @method     ChildPositionQuery groupByUpdatedAt() Group by the updated_at column
 *
 * @method     ChildPositionQuery leftJoin($relation) Adds a LEFT JOIN clause to the query
 * @method     ChildPositionQuery rightJoin($relation) Adds a RIGHT JOIN clause to the query
 * @method     ChildPositionQuery innerJoin($relation) Adds a INNER JOIN clause to the query
 *
 * @method     ChildPositionQuery leftJoinWith($relation) Adds a LEFT JOIN clause and with to the query
 * @method     ChildPositionQuery rightJoinWith($relation) Adds a RIGHT JOIN clause and with to the query
 * @method     ChildPositionQuery innerJoinWith($relation) Adds a INNER JOIN clause and with to the query
 *
 * @method     ChildPositionQuery leftJoinPositionLien($relationAlias = null) Adds a LEFT JOIN clause to the query using the PositionLien relation
 * @method     ChildPositionQuery rightJoinPositionLien($relationAlias = null) Adds a RIGHT JOIN clause to the query using the PositionLien relation
 * @method     ChildPositionQuery innerJoinPositionLien($relationAlias = null) Adds a INNER JOIN clause to the query using the PositionLien relation
 *
 * @method     ChildPositionQuery joinWithPositionLien($joinType = Criteria::INNER_JOIN) Adds a join clause and with to the query using the PositionLien relation
 *
 * @method     ChildPositionQuery leftJoinWithPositionLien() Adds a LEFT JOIN clause and with to the query using the PositionLien relation
 * @method     ChildPositionQuery rightJoinWithPositionLien() Adds a RIGHT JOIN clause and with to the query using the PositionLien relation
 * @method     ChildPositionQuery innerJoinWithPositionLien() Adds a INNER JOIN clause and with to the query using the PositionLien relation
 *
 * @method     \PositionLienQuery endUse() Finalizes a secondary criteria and merges it with its primary Criteria
 *
 * @method     ChildPosition findOne(ConnectionInterface $con = null) Return the first ChildPosition matching the query
 * @method     ChildPosition findOneOrCreate(ConnectionInterface $con = null) Return the first ChildPosition matching the query, or a new ChildPosition object populated from the query conditions when no match is found
 *
 * @method     ChildPosition findOneByIdPosition(string $id_position) Return the first ChildPosition filtered by the id_position column
 * @method     ChildPosition findOneByTitre(string $titre) Return the first ChildPosition filtered by the titre column
 * @method     ChildPosition findOneByDescriptif(string $descriptif) Return the first ChildPosition filtered by the descriptif column
 * @method     ChildPosition findOneByLat(double $lat) Return the first ChildPosition filtered by the lat column
 * @method     ChildPosition findOneByLon(double $lon) Return the first ChildPosition filtered by the lon column
 * @method     ChildPosition findOneByZoom(int $zoom) Return the first ChildPosition filtered by the zoom column
 * @method     ChildPosition findOneByCreatedAt(string $created_at) Return the first ChildPosition filtered by the created_at column
 * @method     ChildPosition findOneByUpdatedAt(string $updated_at) Return the first ChildPosition filtered by the updated_at column *

 * @method     ChildPosition requirePk($key, ConnectionInterface $con = null) Return the ChildPosition by primary key and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildPosition requireOne(ConnectionInterface $con = null) Return the first ChildPosition matching the query and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 *
 * @method     ChildPosition requireOneByIdPosition(string $id_position) Return the first ChildPosition filtered by the id_position column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildPosition requireOneByTitre(string $titre) Return the first ChildPosition filtered by the titre column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildPosition requireOneByDescriptif(string $descriptif) Return the first ChildPosition filtered by the descriptif column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildPosition requireOneByLat(double $lat) Return the first ChildPosition filtered by the lat column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildPosition requireOneByLon(double $lon) Return the first ChildPosition filtered by the lon column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildPosition requireOneByZoom(int $zoom) Return the first ChildPosition filtered by the zoom column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildPosition requireOneByCreatedAt(string $created_at) Return the first ChildPosition filtered by the created_at column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildPosition requireOneByUpdatedAt(string $updated_at) Return the first ChildPosition filtered by the updated_at column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 *
 * @method     ChildPosition[]|ObjectCollection find(ConnectionInterface $con = null) Return ChildPosition objects based on current ModelCriteria
 * @method     ChildPosition[]|ObjectCollection findByIdPosition(string $id_position) Return ChildPosition objects filtered by the id_position column
 * @method     ChildPosition[]|ObjectCollection findByTitre(string $titre) Return ChildPosition objects filtered by the titre column
 * @method     ChildPosition[]|ObjectCollection findByDescriptif(string $descriptif) Return ChildPosition objects filtered by the descriptif column
 * @method     ChildPosition[]|ObjectCollection findByLat(double $lat) Return ChildPosition objects filtered by the lat column
 * @method     ChildPosition[]|ObjectCollection findByLon(double $lon) Return ChildPosition objects filtered by the lon column
 * @method     ChildPosition[]|ObjectCollection findByZoom(int $zoom) Return ChildPosition objects filtered by the zoom column
 * @method     ChildPosition[]|ObjectCollection findByCreatedAt(string $created_at) Return ChildPosition objects filtered by the created_at column
 * @method     ChildPosition[]|ObjectCollection findByUpdatedAt(string $updated_at) Return ChildPosition objects filtered by the updated_at column
 * @method     ChildPosition[]|\Propel\Runtime\Util\PropelModelPager paginate($page = 1, $maxPerPage = 10, ConnectionInterface $con = null) Issue a SELECT query based on the current ModelCriteria and uses a page and a maximum number of results per page to compute an offset and a limit
 *
 */
abstract class PositionQuery extends ModelCriteria
{
    protected $entityNotFoundExceptionClass = '\\Propel\\Runtime\\Exception\\EntityNotFoundException';

    /**
     * Initializes internal state of \Base\PositionQuery object.
     *
     * @param     string $dbName The database name
     * @param     string $modelName The phpName of a model, e.g. 'Book'
     * @param     string $modelAlias The alias for the model in this query, e.g. 'b'
     */
    public function __construct($dbName = 'simplasso', $modelName = '\\Position', $modelAlias = null)
    {
        parent::__construct($dbName, $modelName, $modelAlias);
    }

    /**
     * Returns a new ChildPositionQuery object.
     *
     * @param     string $modelAlias The alias of a model in the query
     * @param     Criteria $criteria Optional Criteria to build the query from
     *
     * @return ChildPositionQuery
     */
    public static function create($modelAlias = null, Criteria $criteria = null)
    {
        if ($criteria instanceof ChildPositionQuery) {
            return $criteria;
        }
        $query = new ChildPositionQuery();
        if (null !== $modelAlias) {
            $query->setModelAlias($modelAlias);
        }
        if ($criteria instanceof Criteria) {
            $query->mergeWith($criteria);
        }

        return $query;
    }

    /**
     * Find object by primary key.
     * Propel uses the instance pool to skip the database if the object exists.
     * Go fast if the query is untouched.
     *
     * <code>
     * $obj  = $c->findPk(12, $con);
     * </code>
     *
     * @param mixed $key Primary key to use for the query
     * @param ConnectionInterface $con an optional connection object
     *
     * @return ChildPosition|array|mixed the result, formatted by the current formatter
     */
    public function findPk($key, ConnectionInterface $con = null)
    {
        if ($key === null) {
            return null;
        }

        if ($con === null) {
            $con = Propel::getServiceContainer()->getReadConnection(PositionTableMap::DATABASE_NAME);
        }

        $this->basePreSelect($con);

        if (
            $this->formatter || $this->modelAlias || $this->with || $this->select
            || $this->selectColumns || $this->asColumns || $this->selectModifiers
            || $this->map || $this->having || $this->joins
        ) {
            return $this->findPkComplex($key, $con);
        }

        if ((null !== ($obj = PositionTableMap::getInstanceFromPool(null === $key || is_scalar($key) || is_callable([$key, '__toString']) ? (string) $key : $key)))) {
            // the object is already in the instance pool
            return $obj;
        }

        return $this->findPkSimple($key, $con);
    }

    /**
     * Find object by primary key using raw SQL to go fast.
     * Bypass doSelect() and the object formatter by using generated code.
     *
     * @param     mixed $key Primary key to use for the query
     * @param     ConnectionInterface $con A connection object
     *
     * @throws \Propel\Runtime\Exception\PropelException
     *
     * @return ChildPosition A model object, or null if the key is not found
     */
    protected function findPkSimple($key, ConnectionInterface $con)
    {
        $sql = 'SELECT `id_position`, `titre`, `descriptif`, `lat`, `lon`, `zoom`, `created_at`, `updated_at` FROM `geo_positions` WHERE `id_position` = :p0';
        try {
            $stmt = $con->prepare($sql);
            $stmt->bindValue(':p0', $key, PDO::PARAM_INT);
            $stmt->execute();
        } catch (Exception $e) {
            Propel::log($e->getMessage(), Propel::LOG_ERR);
            throw new PropelException(sprintf('Unable to execute SELECT statement [%s]', $sql), 0, $e);
        }
        $obj = null;
        if ($row = $stmt->fetch(\PDO::FETCH_NUM)) {
            /** @var ChildPosition $obj */
            $obj = new ChildPosition();
            $obj->hydrate($row);
            PositionTableMap::addInstanceToPool($obj, null === $key || is_scalar($key) || is_callable([$key, '__toString']) ? (string) $key : $key);
        }
        $stmt->closeCursor();

        return $obj;
    }

    /**
     * Find object by primary key.
     *
     * @param     mixed $key Primary key to use for the query
     * @param     ConnectionInterface $con A connection object
     *
     * @return ChildPosition|array|mixed the result, formatted by the current formatter
     */
    protected function findPkComplex($key, ConnectionInterface $con)
    {
        // As the query uses a PK condition, no limit(1) is necessary.
        $criteria = $this->isKeepQuery() ? clone $this : $this;
        $dataFetcher = $criteria
            ->filterByPrimaryKey($key)
            ->doSelect($con);

        return $criteria->getFormatter()->init($criteria)->formatOne($dataFetcher);
    }

    /**
     * Find objects by primary key
     * <code>
     * $objs = $c->findPks(array(12, 56, 832), $con);
     * </code>
     * @param     array $keys Primary keys to use for the query
     * @param     ConnectionInterface $con an optional connection object
     *
     * @return ObjectCollection|array|mixed the list of results, formatted by the current formatter
     */
    public function findPks($keys, ConnectionInterface $con = null)
    {
        if (null === $con) {
            $con = Propel::getServiceContainer()->getReadConnection($this->getDbName());
        }
        $this->basePreSelect($con);
        $criteria = $this->isKeepQuery() ? clone $this : $this;
        $dataFetcher = $criteria
            ->filterByPrimaryKeys($keys)
            ->doSelect($con);

        return $criteria->getFormatter()->init($criteria)->format($dataFetcher);
    }

    /**
     * Filter the query by primary key
     *
     * @param     mixed $key Primary key to use for the query
     *
     * @return $this|ChildPositionQuery The current query, for fluid interface
     */
    public function filterByPrimaryKey($key)
    {

        return $this->addUsingAlias(PositionTableMap::COL_ID_POSITION, $key, Criteria::EQUAL);
    }

    /**
     * Filter the query by a list of primary keys
     *
     * @param     array $keys The list of primary key to use for the query
     *
     * @return $this|ChildPositionQuery The current query, for fluid interface
     */
    public function filterByPrimaryKeys($keys)
    {

        return $this->addUsingAlias(PositionTableMap::COL_ID_POSITION, $keys, Criteria::IN);
    }

    /**
     * Filter the query on the id_position column
     *
     * Example usage:
     * <code>
     * $query->filterByIdPosition(1234); // WHERE id_position = 1234
     * $query->filterByIdPosition(array(12, 34)); // WHERE id_position IN (12, 34)
     * $query->filterByIdPosition(array('min' => 12)); // WHERE id_position > 12
     * </code>
     *
     * @param     mixed $idPosition The value to use as filter.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildPositionQuery The current query, for fluid interface
     */
    public function filterByIdPosition($idPosition = null, $comparison = null)
    {
        if (is_array($idPosition)) {
            $useMinMax = false;
            if (isset($idPosition['min'])) {
                $this->addUsingAlias(PositionTableMap::COL_ID_POSITION, $idPosition['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($idPosition['max'])) {
                $this->addUsingAlias(PositionTableMap::COL_ID_POSITION, $idPosition['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(PositionTableMap::COL_ID_POSITION, $idPosition, $comparison);
    }

    /**
     * Filter the query on the titre column
     *
     * Example usage:
     * <code>
     * $query->filterByTitre('fooValue');   // WHERE titre = 'fooValue'
     * $query->filterByTitre('%fooValue%', Criteria::LIKE); // WHERE titre LIKE '%fooValue%'
     * </code>
     *
     * @param     string $titre The value to use as filter.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildPositionQuery The current query, for fluid interface
     */
    public function filterByTitre($titre = null, $comparison = null)
    {
        if (null === $comparison) {
            if (is_array($titre)) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(PositionTableMap::COL_TITRE, $titre, $comparison);
    }

    /**
     * Filter the query on the descriptif column
     *
     * Example usage:
     * <code>
     * $query->filterByDescriptif('fooValue');   // WHERE descriptif = 'fooValue'
     * $query->filterByDescriptif('%fooValue%', Criteria::LIKE); // WHERE descriptif LIKE '%fooValue%'
     * </code>
     *
     * @param     string $descriptif The value to use as filter.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildPositionQuery The current query, for fluid interface
     */
    public function filterByDescriptif($descriptif = null, $comparison = null)
    {
        if (null === $comparison) {
            if (is_array($descriptif)) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(PositionTableMap::COL_DESCRIPTIF, $descriptif, $comparison);
    }

    /**
     * Filter the query on the lat column
     *
     * Example usage:
     * <code>
     * $query->filterByLat(1234); // WHERE lat = 1234
     * $query->filterByLat(array(12, 34)); // WHERE lat IN (12, 34)
     * $query->filterByLat(array('min' => 12)); // WHERE lat > 12
     * </code>
     *
     * @param     mixed $lat The value to use as filter.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildPositionQuery The current query, for fluid interface
     */
    public function filterByLat($lat = null, $comparison = null)
    {
        if (is_array($lat)) {
            $useMinMax = false;
            if (isset($lat['min'])) {
                $this->addUsingAlias(PositionTableMap::COL_LAT, $lat['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($lat['max'])) {
                $this->addUsingAlias(PositionTableMap::COL_LAT, $lat['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(PositionTableMap::COL_LAT, $lat, $comparison);
    }

    /**
     * Filter the query on the lon column
     *
     * Example usage:
     * <code>
     * $query->filterByLon(1234); // WHERE lon = 1234
     * $query->filterByLon(array(12, 34)); // WHERE lon IN (12, 34)
     * $query->filterByLon(array('min' => 12)); // WHERE lon > 12
     * </code>
     *
     * @param     mixed $lon The value to use as filter.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildPositionQuery The current query, for fluid interface
     */
    public function filterByLon($lon = null, $comparison = null)
    {
        if (is_array($lon)) {
            $useMinMax = false;
            if (isset($lon['min'])) {
                $this->addUsingAlias(PositionTableMap::COL_LON, $lon['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($lon['max'])) {
                $this->addUsingAlias(PositionTableMap::COL_LON, $lon['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(PositionTableMap::COL_LON, $lon, $comparison);
    }

    /**
     * Filter the query on the zoom column
     *
     * Example usage:
     * <code>
     * $query->filterByZoom(1234); // WHERE zoom = 1234
     * $query->filterByZoom(array(12, 34)); // WHERE zoom IN (12, 34)
     * $query->filterByZoom(array('min' => 12)); // WHERE zoom > 12
     * </code>
     *
     * @param     mixed $zoom The value to use as filter.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildPositionQuery The current query, for fluid interface
     */
    public function filterByZoom($zoom = null, $comparison = null)
    {
        if (is_array($zoom)) {
            $useMinMax = false;
            if (isset($zoom['min'])) {
                $this->addUsingAlias(PositionTableMap::COL_ZOOM, $zoom['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($zoom['max'])) {
                $this->addUsingAlias(PositionTableMap::COL_ZOOM, $zoom['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(PositionTableMap::COL_ZOOM, $zoom, $comparison);
    }

    /**
     * Filter the query on the created_at column
     *
     * Example usage:
     * <code>
     * $query->filterByCreatedAt('2011-03-14'); // WHERE created_at = '2011-03-14'
     * $query->filterByCreatedAt('now'); // WHERE created_at = '2011-03-14'
     * $query->filterByCreatedAt(array('max' => 'yesterday')); // WHERE created_at > '2011-03-13'
     * </code>
     *
     * @param     mixed $createdAt The value to use as filter.
     *              Values can be integers (unix timestamps), DateTime objects, or strings.
     *              Empty strings are treated as NULL.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildPositionQuery The current query, for fluid interface
     */
    public function filterByCreatedAt($createdAt = null, $comparison = null)
    {
        if (is_array($createdAt)) {
            $useMinMax = false;
            if (isset($createdAt['min'])) {
                $this->addUsingAlias(PositionTableMap::COL_CREATED_AT, $createdAt['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($createdAt['max'])) {
                $this->addUsingAlias(PositionTableMap::COL_CREATED_AT, $createdAt['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(PositionTableMap::COL_CREATED_AT, $createdAt, $comparison);
    }

    /**
     * Filter the query on the updated_at column
     *
     * Example usage:
     * <code>
     * $query->filterByUpdatedAt('2011-03-14'); // WHERE updated_at = '2011-03-14'
     * $query->filterByUpdatedAt('now'); // WHERE updated_at = '2011-03-14'
     * $query->filterByUpdatedAt(array('max' => 'yesterday')); // WHERE updated_at > '2011-03-13'
     * </code>
     *
     * @param     mixed $updatedAt The value to use as filter.
     *              Values can be integers (unix timestamps), DateTime objects, or strings.
     *              Empty strings are treated as NULL.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildPositionQuery The current query, for fluid interface
     */
    public function filterByUpdatedAt($updatedAt = null, $comparison = null)
    {
        if (is_array($updatedAt)) {
            $useMinMax = false;
            if (isset($updatedAt['min'])) {
                $this->addUsingAlias(PositionTableMap::COL_UPDATED_AT, $updatedAt['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($updatedAt['max'])) {
                $this->addUsingAlias(PositionTableMap::COL_UPDATED_AT, $updatedAt['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(PositionTableMap::COL_UPDATED_AT, $updatedAt, $comparison);
    }

    /**
     * Filter the query by a related \PositionLien object
     *
     * @param \PositionLien|ObjectCollection $positionLien the related object to use as filter
     * @param string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return ChildPositionQuery The current query, for fluid interface
     */
    public function filterByPositionLien($positionLien, $comparison = null)
    {
        if ($positionLien instanceof \PositionLien) {
            return $this
                ->addUsingAlias(PositionTableMap::COL_ID_POSITION, $positionLien->getIdPosition(), $comparison);
        } elseif ($positionLien instanceof ObjectCollection) {
            return $this
                ->usePositionLienQuery()
                ->filterByPrimaryKeys($positionLien->getPrimaryKeys())
                ->endUse();
        } else {
            throw new PropelException('filterByPositionLien() only accepts arguments of type \PositionLien or Collection');
        }
    }

    /**
     * Adds a JOIN clause to the query using the PositionLien relation
     *
     * @param     string $relationAlias optional alias for the relation
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return $this|ChildPositionQuery The current query, for fluid interface
     */
    public function joinPositionLien($relationAlias = null, $joinType = Criteria::INNER_JOIN)
    {
        $tableMap = $this->getTableMap();
        $relationMap = $tableMap->getRelation('PositionLien');

        // create a ModelJoin object for this join
        $join = new ModelJoin();
        $join->setJoinType($joinType);
        $join->setRelationMap($relationMap, $this->useAliasInSQL ? $this->getModelAlias() : null, $relationAlias);
        if ($previousJoin = $this->getPreviousJoin()) {
            $join->setPreviousJoin($previousJoin);
        }

        // add the ModelJoin to the current object
        if ($relationAlias) {
            $this->addAlias($relationAlias, $relationMap->getRightTable()->getName());
            $this->addJoinObject($join, $relationAlias);
        } else {
            $this->addJoinObject($join, 'PositionLien');
        }

        return $this;
    }

    /**
     * Use the PositionLien relation PositionLien object
     *
     * @see useQuery()
     *
     * @param     string $relationAlias optional alias for the relation,
     *                                   to be used as main alias in the secondary query
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return \PositionLienQuery A secondary query class using the current class as primary query
     */
    public function usePositionLienQuery($relationAlias = null, $joinType = Criteria::INNER_JOIN)
    {
        return $this
            ->joinPositionLien($relationAlias, $joinType)
            ->useQuery($relationAlias ? $relationAlias : 'PositionLien', '\PositionLienQuery');
    }

    /**
     * Exclude object from result
     *
     * @param   ChildPosition $position Object to remove from the list of results
     *
     * @return $this|ChildPositionQuery The current query, for fluid interface
     */
    public function prune($position = null)
    {
        if ($position) {
            $this->addUsingAlias(PositionTableMap::COL_ID_POSITION, $position->getIdPosition(), Criteria::NOT_EQUAL);
        }

        return $this;
    }

    /**
     * Deletes all rows from the geo_positions table.
     *
     * @param ConnectionInterface $con the connection to use
     * @return int The number of affected rows (if supported by underlying database driver).
     */
    public function doDeleteAll(ConnectionInterface $con = null)
    {
        if (null === $con) {
            $con = Propel::getServiceContainer()->getWriteConnection(PositionTableMap::DATABASE_NAME);
        }

        // use transaction because $criteria could contain info
        // for more than one table or we could emulating ON DELETE CASCADE, etc.
        return $con->transaction(function () use ($con) {
            $affectedRows = 0; // initialize var to track total num of affected rows
            $affectedRows += parent::doDeleteAll($con);
            // Because this db requires some delete cascade/set null emulation, we have to
            // clear the cached instance *after* the emulation has happened (since
            // instances get re-added by the select statement contained therein).
            PositionTableMap::clearInstancePool();
            PositionTableMap::clearRelatedInstancePool();

            return $affectedRows;
        });
    }

    /**
     * Performs a DELETE on the database based on the current ModelCriteria
     *
     * @param ConnectionInterface $con the connection to use
     * @return int             The number of affected rows (if supported by underlying database driver).  This includes CASCADE-related rows
     *                         if supported by native driver or if emulated using Propel.
     * @throws PropelException Any exceptions caught during processing will be
     *                         rethrown wrapped into a PropelException.
     */
    public function delete(ConnectionInterface $con = null)
    {
        if (null === $con) {
            $con = Propel::getServiceContainer()->getWriteConnection(PositionTableMap::DATABASE_NAME);
        }

        $criteria = $this;

        // Set the correct dbName
        $criteria->setDbName(PositionTableMap::DATABASE_NAME);

        // use transaction because $criteria could contain info
        // for more than one table or we could emulating ON DELETE CASCADE, etc.
        return $con->transaction(function () use ($con, $criteria) {
            $affectedRows = 0; // initialize var to track total num of affected rows

            PositionTableMap::removeInstanceFromPool($criteria);

            $affectedRows += ModelCriteria::delete($con);
            PositionTableMap::clearRelatedInstancePool();

            return $affectedRows;
        });
    }

    // timestampable behavior

    /**
     * Filter by the latest updated
     *
     * @param      int $nbDays Maximum age of the latest update in days
     *
     * @return     $this|ChildPositionQuery The current query, for fluid interface
     */
    public function recentlyUpdated($nbDays = 7)
    {
        return $this->addUsingAlias(PositionTableMap::COL_UPDATED_AT, time() - $nbDays * 24 * 60 * 60, Criteria::GREATER_EQUAL);
    }

    /**
     * Order by update date desc
     *
     * @return     $this|ChildPositionQuery The current query, for fluid interface
     */
    public function lastUpdatedFirst()
    {
        return $this->addDescendingOrderByColumn(PositionTableMap::COL_UPDATED_AT);
    }

    /**
     * Order by update date asc
     *
     * @return     $this|ChildPositionQuery The current query, for fluid interface
     */
    public function firstUpdatedFirst()
    {
        return $this->addAscendingOrderByColumn(PositionTableMap::COL_UPDATED_AT);
    }

    /**
     * Order by create date desc
     *
     * @return     $this|ChildPositionQuery The current query, for fluid interface
     */
    public function lastCreatedFirst()
    {
        return $this->addDescendingOrderByColumn(PositionTableMap::COL_CREATED_AT);
    }

    /**
     * Filter by the latest created
     *
     * @param      int $nbDays Maximum age of in days
     *
     * @return     $this|ChildPositionQuery The current query, for fluid interface
     */
    public function recentlyCreated($nbDays = 7)
    {
        return $this->addUsingAlias(PositionTableMap::COL_CREATED_AT, time() - $nbDays * 24 * 60 * 60, Criteria::GREATER_EQUAL);
    }

    /**
     * Order by create date asc
     *
     * @return     $this|ChildPositionQuery The current query, for fluid interface
     */
    public function firstCreatedFirst()
    {
        return $this->addAscendingOrderByColumn(PositionTableMap::COL_CREATED_AT);
    }

} // PositionQuery
