<?php

namespace Base;

use \AssoTresorsArchiveQuery as ChildAssoTresorsArchiveQuery;
use \DateTime;
use \Exception;
use \PDO;
use Map\AssoTresorsArchiveTableMap;
use Propel\Runtime\Propel;
use Propel\Runtime\ActiveQuery\Criteria;
use Propel\Runtime\ActiveQuery\ModelCriteria;
use Propel\Runtime\ActiveRecord\ActiveRecordInterface;
use Propel\Runtime\Collection\Collection;
use Propel\Runtime\Connection\ConnectionInterface;
use Propel\Runtime\Exception\BadMethodCallException;
use Propel\Runtime\Exception\LogicException;
use Propel\Runtime\Exception\PropelException;
use Propel\Runtime\Map\TableMap;
use Propel\Runtime\Parser\AbstractParser;
use Propel\Runtime\Util\PropelDateTime;

/**
 * Base class that represents a row from the 'asso_tresors_archive' table.
 *
 *
 *
 * @package    propel.generator..Base
 */
abstract class AssoTresorsArchive implements ActiveRecordInterface
{
    /**
     * TableMap class name
     */
    const TABLE_MAP = '\\Map\\AssoTresorsArchiveTableMap';


    /**
     * attribute to determine if this object has previously been saved.
     * @var boolean
     */
    protected $new = true;

    /**
     * attribute to determine whether this object has been deleted.
     * @var boolean
     */
    protected $deleted = false;

    /**
     * The columns that have been modified in current object.
     * Tracking modified columns allows us to only update modified columns.
     * @var array
     */
    protected $modifiedColumns = array();

    /**
     * The (virtual) columns that are added at runtime
     * The formatters can add supplementary columns based on a resultset
     * @var array
     */
    protected $virtualColumns = array();

    /**
     * The value for the id_tresor field.
     * Mode de paiement
     * @var        string
     */
    protected $id_tresor;

    /**
     * The value for the id_entite field.
     *
     * Note: this column has a database default value of: '1'
     * @var        string
     */
    protected $id_entite;

    /**
     * The value for the nom field.
     *
     * Note: this column has a database default value of: 'Moyen paiement'
     * @var        string
     */
    protected $nom;

    /**
     * The value for the nomcourt field.
     *
     * Note: this column has a database default value of: 'MP'
     * @var        string
     */
    protected $nomcourt;

    /**
     * The value for the iban field.
     * Numero international
     * Note: this column has a database default value of: 'Iban'
     * @var        string
     */
    protected $iban;

    /**
     * The value for the bic field.
     * code BIC
     * Note: this column has a database default value of: 'code BIC'
     * @var        string
     */
    protected $bic;

    /**
     * The value for the id_compterb field.
     * financier attente
     * @var        string
     */
    protected $id_compterb;

    /**
     * The value for the id_compte field.
     * tiers
     * @var        string
     */
    protected $id_compte;

    /**
     * The value for the remise field.
     * Dernière remise
     * Note: this column has a database default value of: '0'
     * @var        string
     */
    protected $remise;

    /**
     * The value for the observation field.
     * Observation
     * @var        string
     */
    protected $observation;

    /**
     * The value for the created_at field.
     *
     * @var        DateTime
     */
    protected $created_at;

    /**
     * The value for the updated_at field.
     *
     * @var        DateTime
     */
    protected $updated_at;

    /**
     * The value for the archived_at field.
     *
     * @var        DateTime
     */
    protected $archived_at;

    /**
     * Flag to prevent endless save loop, if this object is referenced
     * by another object which falls in this transaction.
     *
     * @var boolean
     */
    protected $alreadyInSave = false;

    /**
     * Applies default values to this object.
     * This method should be called from the object's constructor (or
     * equivalent initialization method).
     * @see __construct()
     */
    public function applyDefaultValues()
    {
        $this->id_entite = '1';
        $this->nom = 'Moyen paiement';
        $this->nomcourt = 'MP';
        $this->iban = 'Iban';
        $this->bic = 'code BIC';
        $this->remise = '0';
    }

    /**
     * Initializes internal state of Base\AssoTresorsArchive object.
     * @see applyDefaults()
     */
    public function __construct()
    {
        $this->applyDefaultValues();
    }

    /**
     * Returns whether the object has been modified.
     *
     * @return boolean True if the object has been modified.
     */
    public function isModified()
    {
        return !!$this->modifiedColumns;
    }

    /**
     * Has specified column been modified?
     *
     * @param  string  $col column fully qualified name (TableMap::TYPE_COLNAME), e.g. Book::AUTHOR_ID
     * @return boolean True if $col has been modified.
     */
    public function isColumnModified($col)
    {
        return $this->modifiedColumns && isset($this->modifiedColumns[$col]);
    }

    /**
     * Get the columns that have been modified in this object.
     * @return array A unique list of the modified column names for this object.
     */
    public function getModifiedColumns()
    {
        return $this->modifiedColumns ? array_keys($this->modifiedColumns) : [];
    }

    /**
     * Returns whether the object has ever been saved.  This will
     * be false, if the object was retrieved from storage or was created
     * and then saved.
     *
     * @return boolean true, if the object has never been persisted.
     */
    public function isNew()
    {
        return $this->new;
    }

    /**
     * Setter for the isNew attribute.  This method will be called
     * by Propel-generated children and objects.
     *
     * @param boolean $b the state of the object.
     */
    public function setNew($b)
    {
        $this->new = (boolean) $b;
    }

    /**
     * Whether this object has been deleted.
     * @return boolean The deleted state of this object.
     */
    public function isDeleted()
    {
        return $this->deleted;
    }

    /**
     * Specify whether this object has been deleted.
     * @param  boolean $b The deleted state of this object.
     * @return void
     */
    public function setDeleted($b)
    {
        $this->deleted = (boolean) $b;
    }

    /**
     * Sets the modified state for the object to be false.
     * @param  string $col If supplied, only the specified column is reset.
     * @return void
     */
    public function resetModified($col = null)
    {
        if (null !== $col) {
            if (isset($this->modifiedColumns[$col])) {
                unset($this->modifiedColumns[$col]);
            }
        } else {
            $this->modifiedColumns = array();
        }
    }

    /**
     * Compares this with another <code>AssoTresorsArchive</code> instance.  If
     * <code>obj</code> is an instance of <code>AssoTresorsArchive</code>, delegates to
     * <code>equals(AssoTresorsArchive)</code>.  Otherwise, returns <code>false</code>.
     *
     * @param  mixed   $obj The object to compare to.
     * @return boolean Whether equal to the object specified.
     */
    public function equals($obj)
    {
        if (!$obj instanceof static) {
            return false;
        }

        if ($this === $obj) {
            return true;
        }

        if (null === $this->getPrimaryKey() || null === $obj->getPrimaryKey()) {
            return false;
        }

        return $this->getPrimaryKey() === $obj->getPrimaryKey();
    }

    /**
     * Get the associative array of the virtual columns in this object
     *
     * @return array
     */
    public function getVirtualColumns()
    {
        return $this->virtualColumns;
    }

    /**
     * Checks the existence of a virtual column in this object
     *
     * @param  string  $name The virtual column name
     * @return boolean
     */
    public function hasVirtualColumn($name)
    {
        return array_key_exists($name, $this->virtualColumns);
    }

    /**
     * Get the value of a virtual column in this object
     *
     * @param  string $name The virtual column name
     * @return mixed
     *
     * @throws PropelException
     */
    public function getVirtualColumn($name)
    {
        if (!$this->hasVirtualColumn($name)) {
            throw new PropelException(sprintf('Cannot get value of inexistent virtual column %s.', $name));
        }

        return $this->virtualColumns[$name];
    }

    /**
     * Set the value of a virtual column in this object
     *
     * @param string $name  The virtual column name
     * @param mixed  $value The value to give to the virtual column
     *
     * @return $this|AssoTresorsArchive The current object, for fluid interface
     */
    public function setVirtualColumn($name, $value)
    {
        $this->virtualColumns[$name] = $value;

        return $this;
    }

    /**
     * Logs a message using Propel::log().
     *
     * @param  string  $msg
     * @param  int     $priority One of the Propel::LOG_* logging levels
     * @return boolean
     */
    protected function log($msg, $priority = Propel::LOG_INFO)
    {
        return Propel::log(get_class($this) . ': ' . $msg, $priority);
    }

    /**
     * Export the current object properties to a string, using a given parser format
     * <code>
     * $book = BookQuery::create()->findPk(9012);
     * echo $book->exportTo('JSON');
     *  => {"Id":9012,"Title":"Don Juan","ISBN":"0140422161","Price":12.99,"PublisherId":1234,"AuthorId":5678}');
     * </code>
     *
     * @param  mixed   $parser                 A AbstractParser instance, or a format name ('XML', 'YAML', 'JSON', 'CSV')
     * @param  boolean $includeLazyLoadColumns (optional) Whether to include lazy load(ed) columns. Defaults to TRUE.
     * @return string  The exported data
     */
    public function exportTo($parser, $includeLazyLoadColumns = true)
    {
        if (!$parser instanceof AbstractParser) {
            $parser = AbstractParser::getParser($parser);
        }

        return $parser->fromArray($this->toArray(TableMap::TYPE_PHPNAME, $includeLazyLoadColumns, array(), true));
    }

    /**
     * Clean up internal collections prior to serializing
     * Avoids recursive loops that turn into segmentation faults when serializing
     */
    public function __sleep()
    {
        $this->clearAllReferences();

        $cls = new \ReflectionClass($this);
        $propertyNames = [];
        $serializableProperties = array_diff($cls->getProperties(), $cls->getProperties(\ReflectionProperty::IS_STATIC));

        foreach($serializableProperties as $property) {
            $propertyNames[] = $property->getName();
        }

        return $propertyNames;
    }

    /**
     * Get the [id_tresor] column value.
     * Mode de paiement
     * @return string
     */
    public function getIdTresor()
    {
        return $this->id_tresor;
    }

    /**
     * Get the [id_entite] column value.
     *
     * @return string
     */
    public function getIdEntite()
    {
        return $this->id_entite;
    }

    /**
     * Get the [nom] column value.
     *
     * @return string
     */
    public function getNom()
    {
        return $this->nom;
    }

    /**
     * Get the [nomcourt] column value.
     *
     * @return string
     */
    public function getNomcourt()
    {
        return $this->nomcourt;
    }

    /**
     * Get the [iban] column value.
     * Numero international
     * @return string
     */
    public function getIban()
    {
        return $this->iban;
    }

    /**
     * Get the [bic] column value.
     * code BIC
     * @return string
     */
    public function getBic()
    {
        return $this->bic;
    }

    /**
     * Get the [id_compterb] column value.
     * financier attente
     * @return string
     */
    public function getIdCompterb()
    {
        return $this->id_compterb;
    }

    /**
     * Get the [id_compte] column value.
     * tiers
     * @return string
     */
    public function getIdCompte()
    {
        return $this->id_compte;
    }

    /**
     * Get the [remise] column value.
     * Dernière remise
     * @return string
     */
    public function getRemise()
    {
        return $this->remise;
    }

    /**
     * Get the [observation] column value.
     * Observation
     * @return string
     */
    public function getObservation()
    {
        return $this->observation;
    }

    /**
     * Get the [optionally formatted] temporal [created_at] column value.
     *
     *
     * @param      string $format The date/time format string (either date()-style or strftime()-style).
     *                            If format is NULL, then the raw DateTime object will be returned.
     *
     * @return string|DateTime Formatted date/time value as string or DateTime object (if format is NULL), NULL if column is NULL, and 0 if column value is 0000-00-00 00:00:00
     *
     * @throws PropelException - if unable to parse/validate the date/time value.
     */
    public function getCreatedAt($format = NULL)
    {
        if ($format === null) {
            return $this->created_at;
        } else {
            return $this->created_at instanceof \DateTimeInterface ? $this->created_at->format($format) : null;
        }
    }

    /**
     * Get the [optionally formatted] temporal [updated_at] column value.
     *
     *
     * @param      string $format The date/time format string (either date()-style or strftime()-style).
     *                            If format is NULL, then the raw DateTime object will be returned.
     *
     * @return string|DateTime Formatted date/time value as string or DateTime object (if format is NULL), NULL if column is NULL, and 0 if column value is 0000-00-00 00:00:00
     *
     * @throws PropelException - if unable to parse/validate the date/time value.
     */
    public function getUpdatedAt($format = NULL)
    {
        if ($format === null) {
            return $this->updated_at;
        } else {
            return $this->updated_at instanceof \DateTimeInterface ? $this->updated_at->format($format) : null;
        }
    }

    /**
     * Get the [optionally formatted] temporal [archived_at] column value.
     *
     *
     * @param      string $format The date/time format string (either date()-style or strftime()-style).
     *                            If format is NULL, then the raw DateTime object will be returned.
     *
     * @return string|DateTime Formatted date/time value as string or DateTime object (if format is NULL), NULL if column is NULL, and 0 if column value is 0000-00-00 00:00:00
     *
     * @throws PropelException - if unable to parse/validate the date/time value.
     */
    public function getArchivedAt($format = NULL)
    {
        if ($format === null) {
            return $this->archived_at;
        } else {
            return $this->archived_at instanceof \DateTimeInterface ? $this->archived_at->format($format) : null;
        }
    }

    /**
     * Set the value of [id_tresor] column.
     * Mode de paiement
     * @param string $v new value
     * @return $this|\AssoTresorsArchive The current object (for fluent API support)
     */
    public function setIdTresor($v)
    {
        if ($v !== null) {
            $v = (string) $v;
        }

        if ($this->id_tresor !== $v) {
            $this->id_tresor = $v;
            $this->modifiedColumns[AssoTresorsArchiveTableMap::COL_ID_TRESOR] = true;
        }

        return $this;
    } // setIdTresor()

    /**
     * Set the value of [id_entite] column.
     *
     * @param string $v new value
     * @return $this|\AssoTresorsArchive The current object (for fluent API support)
     */
    public function setIdEntite($v)
    {
        if ($v !== null) {
            $v = (string) $v;
        }

        if ($this->id_entite !== $v) {
            $this->id_entite = $v;
            $this->modifiedColumns[AssoTresorsArchiveTableMap::COL_ID_ENTITE] = true;
        }

        return $this;
    } // setIdEntite()

    /**
     * Set the value of [nom] column.
     *
     * @param string $v new value
     * @return $this|\AssoTresorsArchive The current object (for fluent API support)
     */
    public function setNom($v)
    {
        if ($v !== null) {
            $v = (string) $v;
        }

        if ($this->nom !== $v) {
            $this->nom = $v;
            $this->modifiedColumns[AssoTresorsArchiveTableMap::COL_NOM] = true;
        }

        return $this;
    } // setNom()

    /**
     * Set the value of [nomcourt] column.
     *
     * @param string $v new value
     * @return $this|\AssoTresorsArchive The current object (for fluent API support)
     */
    public function setNomcourt($v)
    {
        if ($v !== null) {
            $v = (string) $v;
        }

        if ($this->nomcourt !== $v) {
            $this->nomcourt = $v;
            $this->modifiedColumns[AssoTresorsArchiveTableMap::COL_NOMCOURT] = true;
        }

        return $this;
    } // setNomcourt()

    /**
     * Set the value of [iban] column.
     * Numero international
     * @param string $v new value
     * @return $this|\AssoTresorsArchive The current object (for fluent API support)
     */
    public function setIban($v)
    {
        if ($v !== null) {
            $v = (string) $v;
        }

        if ($this->iban !== $v) {
            $this->iban = $v;
            $this->modifiedColumns[AssoTresorsArchiveTableMap::COL_IBAN] = true;
        }

        return $this;
    } // setIban()

    /**
     * Set the value of [bic] column.
     * code BIC
     * @param string $v new value
     * @return $this|\AssoTresorsArchive The current object (for fluent API support)
     */
    public function setBic($v)
    {
        if ($v !== null) {
            $v = (string) $v;
        }

        if ($this->bic !== $v) {
            $this->bic = $v;
            $this->modifiedColumns[AssoTresorsArchiveTableMap::COL_BIC] = true;
        }

        return $this;
    } // setBic()

    /**
     * Set the value of [id_compterb] column.
     * financier attente
     * @param string $v new value
     * @return $this|\AssoTresorsArchive The current object (for fluent API support)
     */
    public function setIdCompterb($v)
    {
        if ($v !== null) {
            $v = (string) $v;
        }

        if ($this->id_compterb !== $v) {
            $this->id_compterb = $v;
            $this->modifiedColumns[AssoTresorsArchiveTableMap::COL_ID_COMPTERB] = true;
        }

        return $this;
    } // setIdCompterb()

    /**
     * Set the value of [id_compte] column.
     * tiers
     * @param string $v new value
     * @return $this|\AssoTresorsArchive The current object (for fluent API support)
     */
    public function setIdCompte($v)
    {
        if ($v !== null) {
            $v = (string) $v;
        }

        if ($this->id_compte !== $v) {
            $this->id_compte = $v;
            $this->modifiedColumns[AssoTresorsArchiveTableMap::COL_ID_COMPTE] = true;
        }

        return $this;
    } // setIdCompte()

    /**
     * Set the value of [remise] column.
     * Dernière remise
     * @param string $v new value
     * @return $this|\AssoTresorsArchive The current object (for fluent API support)
     */
    public function setRemise($v)
    {
        if ($v !== null) {
            $v = (string) $v;
        }

        if ($this->remise !== $v) {
            $this->remise = $v;
            $this->modifiedColumns[AssoTresorsArchiveTableMap::COL_REMISE] = true;
        }

        return $this;
    } // setRemise()

    /**
     * Set the value of [observation] column.
     * Observation
     * @param string $v new value
     * @return $this|\AssoTresorsArchive The current object (for fluent API support)
     */
    public function setObservation($v)
    {
        if ($v !== null) {
            $v = (string) $v;
        }

        if ($this->observation !== $v) {
            $this->observation = $v;
            $this->modifiedColumns[AssoTresorsArchiveTableMap::COL_OBSERVATION] = true;
        }

        return $this;
    } // setObservation()

    /**
     * Sets the value of [created_at] column to a normalized version of the date/time value specified.
     *
     * @param  mixed $v string, integer (timestamp), or \DateTimeInterface value.
     *               Empty strings are treated as NULL.
     * @return $this|\AssoTresorsArchive The current object (for fluent API support)
     */
    public function setCreatedAt($v)
    {
        $dt = PropelDateTime::newInstance($v, null, 'DateTime');
        if ($this->created_at !== null || $dt !== null) {
            if ($this->created_at === null || $dt === null || $dt->format("Y-m-d H:i:s.u") !== $this->created_at->format("Y-m-d H:i:s.u")) {
                $this->created_at = $dt === null ? null : clone $dt;
                $this->modifiedColumns[AssoTresorsArchiveTableMap::COL_CREATED_AT] = true;
            }
        } // if either are not null

        return $this;
    } // setCreatedAt()

    /**
     * Sets the value of [updated_at] column to a normalized version of the date/time value specified.
     *
     * @param  mixed $v string, integer (timestamp), or \DateTimeInterface value.
     *               Empty strings are treated as NULL.
     * @return $this|\AssoTresorsArchive The current object (for fluent API support)
     */
    public function setUpdatedAt($v)
    {
        $dt = PropelDateTime::newInstance($v, null, 'DateTime');
        if ($this->updated_at !== null || $dt !== null) {
            if ($this->updated_at === null || $dt === null || $dt->format("Y-m-d H:i:s.u") !== $this->updated_at->format("Y-m-d H:i:s.u")) {
                $this->updated_at = $dt === null ? null : clone $dt;
                $this->modifiedColumns[AssoTresorsArchiveTableMap::COL_UPDATED_AT] = true;
            }
        } // if either are not null

        return $this;
    } // setUpdatedAt()

    /**
     * Sets the value of [archived_at] column to a normalized version of the date/time value specified.
     *
     * @param  mixed $v string, integer (timestamp), or \DateTimeInterface value.
     *               Empty strings are treated as NULL.
     * @return $this|\AssoTresorsArchive The current object (for fluent API support)
     */
    public function setArchivedAt($v)
    {
        $dt = PropelDateTime::newInstance($v, null, 'DateTime');
        if ($this->archived_at !== null || $dt !== null) {
            if ($this->archived_at === null || $dt === null || $dt->format("Y-m-d H:i:s.u") !== $this->archived_at->format("Y-m-d H:i:s.u")) {
                $this->archived_at = $dt === null ? null : clone $dt;
                $this->modifiedColumns[AssoTresorsArchiveTableMap::COL_ARCHIVED_AT] = true;
            }
        } // if either are not null

        return $this;
    } // setArchivedAt()

    /**
     * Indicates whether the columns in this object are only set to default values.
     *
     * This method can be used in conjunction with isModified() to indicate whether an object is both
     * modified _and_ has some values set which are non-default.
     *
     * @return boolean Whether the columns in this object are only been set with default values.
     */
    public function hasOnlyDefaultValues()
    {
            if ($this->id_entite !== '1') {
                return false;
            }

            if ($this->nom !== 'Moyen paiement') {
                return false;
            }

            if ($this->nomcourt !== 'MP') {
                return false;
            }

            if ($this->iban !== 'Iban') {
                return false;
            }

            if ($this->bic !== 'code BIC') {
                return false;
            }

            if ($this->remise !== '0') {
                return false;
            }

        // otherwise, everything was equal, so return TRUE
        return true;
    } // hasOnlyDefaultValues()

    /**
     * Hydrates (populates) the object variables with values from the database resultset.
     *
     * An offset (0-based "start column") is specified so that objects can be hydrated
     * with a subset of the columns in the resultset rows.  This is needed, for example,
     * for results of JOIN queries where the resultset row includes columns from two or
     * more tables.
     *
     * @param array   $row       The row returned by DataFetcher->fetch().
     * @param int     $startcol  0-based offset column which indicates which restultset column to start with.
     * @param boolean $rehydrate Whether this object is being re-hydrated from the database.
     * @param string  $indexType The index type of $row. Mostly DataFetcher->getIndexType().
                                  One of the class type constants TableMap::TYPE_PHPNAME, TableMap::TYPE_CAMELNAME
     *                            TableMap::TYPE_COLNAME, TableMap::TYPE_FIELDNAME, TableMap::TYPE_NUM.
     *
     * @return int             next starting column
     * @throws PropelException - Any caught Exception will be rewrapped as a PropelException.
     */
    public function hydrate($row, $startcol = 0, $rehydrate = false, $indexType = TableMap::TYPE_NUM)
    {
        try {

            $col = $row[TableMap::TYPE_NUM == $indexType ? 0 + $startcol : AssoTresorsArchiveTableMap::translateFieldName('IdTresor', TableMap::TYPE_PHPNAME, $indexType)];
            $this->id_tresor = (null !== $col) ? (string) $col : null;

            $col = $row[TableMap::TYPE_NUM == $indexType ? 1 + $startcol : AssoTresorsArchiveTableMap::translateFieldName('IdEntite', TableMap::TYPE_PHPNAME, $indexType)];
            $this->id_entite = (null !== $col) ? (string) $col : null;

            $col = $row[TableMap::TYPE_NUM == $indexType ? 2 + $startcol : AssoTresorsArchiveTableMap::translateFieldName('Nom', TableMap::TYPE_PHPNAME, $indexType)];
            $this->nom = (null !== $col) ? (string) $col : null;

            $col = $row[TableMap::TYPE_NUM == $indexType ? 3 + $startcol : AssoTresorsArchiveTableMap::translateFieldName('Nomcourt', TableMap::TYPE_PHPNAME, $indexType)];
            $this->nomcourt = (null !== $col) ? (string) $col : null;

            $col = $row[TableMap::TYPE_NUM == $indexType ? 4 + $startcol : AssoTresorsArchiveTableMap::translateFieldName('Iban', TableMap::TYPE_PHPNAME, $indexType)];
            $this->iban = (null !== $col) ? (string) $col : null;

            $col = $row[TableMap::TYPE_NUM == $indexType ? 5 + $startcol : AssoTresorsArchiveTableMap::translateFieldName('Bic', TableMap::TYPE_PHPNAME, $indexType)];
            $this->bic = (null !== $col) ? (string) $col : null;

            $col = $row[TableMap::TYPE_NUM == $indexType ? 6 + $startcol : AssoTresorsArchiveTableMap::translateFieldName('IdCompterb', TableMap::TYPE_PHPNAME, $indexType)];
            $this->id_compterb = (null !== $col) ? (string) $col : null;

            $col = $row[TableMap::TYPE_NUM == $indexType ? 7 + $startcol : AssoTresorsArchiveTableMap::translateFieldName('IdCompte', TableMap::TYPE_PHPNAME, $indexType)];
            $this->id_compte = (null !== $col) ? (string) $col : null;

            $col = $row[TableMap::TYPE_NUM == $indexType ? 8 + $startcol : AssoTresorsArchiveTableMap::translateFieldName('Remise', TableMap::TYPE_PHPNAME, $indexType)];
            $this->remise = (null !== $col) ? (string) $col : null;

            $col = $row[TableMap::TYPE_NUM == $indexType ? 9 + $startcol : AssoTresorsArchiveTableMap::translateFieldName('Observation', TableMap::TYPE_PHPNAME, $indexType)];
            $this->observation = (null !== $col) ? (string) $col : null;

            $col = $row[TableMap::TYPE_NUM == $indexType ? 10 + $startcol : AssoTresorsArchiveTableMap::translateFieldName('CreatedAt', TableMap::TYPE_PHPNAME, $indexType)];
            if ($col === '0000-00-00 00:00:00') {
                $col = null;
            }
            $this->created_at = (null !== $col) ? PropelDateTime::newInstance($col, null, 'DateTime') : null;

            $col = $row[TableMap::TYPE_NUM == $indexType ? 11 + $startcol : AssoTresorsArchiveTableMap::translateFieldName('UpdatedAt', TableMap::TYPE_PHPNAME, $indexType)];
            if ($col === '0000-00-00 00:00:00') {
                $col = null;
            }
            $this->updated_at = (null !== $col) ? PropelDateTime::newInstance($col, null, 'DateTime') : null;

            $col = $row[TableMap::TYPE_NUM == $indexType ? 12 + $startcol : AssoTresorsArchiveTableMap::translateFieldName('ArchivedAt', TableMap::TYPE_PHPNAME, $indexType)];
            if ($col === '0000-00-00 00:00:00') {
                $col = null;
            }
            $this->archived_at = (null !== $col) ? PropelDateTime::newInstance($col, null, 'DateTime') : null;
            $this->resetModified();

            $this->setNew(false);

            if ($rehydrate) {
                $this->ensureConsistency();
            }

            return $startcol + 13; // 13 = AssoTresorsArchiveTableMap::NUM_HYDRATE_COLUMNS.

        } catch (Exception $e) {
            throw new PropelException(sprintf('Error populating %s object', '\\AssoTresorsArchive'), 0, $e);
        }
    }

    /**
     * Checks and repairs the internal consistency of the object.
     *
     * This method is executed after an already-instantiated object is re-hydrated
     * from the database.  It exists to check any foreign keys to make sure that
     * the objects related to the current object are correct based on foreign key.
     *
     * You can override this method in the stub class, but you should always invoke
     * the base method from the overridden method (i.e. parent::ensureConsistency()),
     * in case your model changes.
     *
     * @throws PropelException
     */
    public function ensureConsistency()
    {
    } // ensureConsistency

    /**
     * Reloads this object from datastore based on primary key and (optionally) resets all associated objects.
     *
     * This will only work if the object has been saved and has a valid primary key set.
     *
     * @param      boolean $deep (optional) Whether to also de-associated any related objects.
     * @param      ConnectionInterface $con (optional) The ConnectionInterface connection to use.
     * @return void
     * @throws PropelException - if this object is deleted, unsaved or doesn't have pk match in db
     */
    public function reload($deep = false, ConnectionInterface $con = null)
    {
        if ($this->isDeleted()) {
            throw new PropelException("Cannot reload a deleted object.");
        }

        if ($this->isNew()) {
            throw new PropelException("Cannot reload an unsaved object.");
        }

        if ($con === null) {
            $con = Propel::getServiceContainer()->getReadConnection(AssoTresorsArchiveTableMap::DATABASE_NAME);
        }

        // We don't need to alter the object instance pool; we're just modifying this instance
        // already in the pool.

        $dataFetcher = ChildAssoTresorsArchiveQuery::create(null, $this->buildPkeyCriteria())->setFormatter(ModelCriteria::FORMAT_STATEMENT)->find($con);
        $row = $dataFetcher->fetch();
        $dataFetcher->close();
        if (!$row) {
            throw new PropelException('Cannot find matching row in the database to reload object values.');
        }
        $this->hydrate($row, 0, true, $dataFetcher->getIndexType()); // rehydrate

        if ($deep) {  // also de-associate any related objects?

        } // if (deep)
    }

    /**
     * Removes this object from datastore and sets delete attribute.
     *
     * @param      ConnectionInterface $con
     * @return void
     * @throws PropelException
     * @see AssoTresorsArchive::setDeleted()
     * @see AssoTresorsArchive::isDeleted()
     */
    public function delete(ConnectionInterface $con = null)
    {
        if ($this->isDeleted()) {
            throw new PropelException("This object has already been deleted.");
        }

        if ($con === null) {
            $con = Propel::getServiceContainer()->getWriteConnection(AssoTresorsArchiveTableMap::DATABASE_NAME);
        }

        $con->transaction(function () use ($con) {
            $deleteQuery = ChildAssoTresorsArchiveQuery::create()
                ->filterByPrimaryKey($this->getPrimaryKey());
            $ret = $this->preDelete($con);
            if ($ret) {
                $deleteQuery->delete($con);
                $this->postDelete($con);
                $this->setDeleted(true);
            }
        });
    }

    /**
     * Persists this object to the database.
     *
     * If the object is new, it inserts it; otherwise an update is performed.
     * All modified related objects will also be persisted in the doSave()
     * method.  This method wraps all precipitate database operations in a
     * single transaction.
     *
     * @param      ConnectionInterface $con
     * @return int             The number of rows affected by this insert/update and any referring fk objects' save() operations.
     * @throws PropelException
     * @see doSave()
     */
    public function save(ConnectionInterface $con = null)
    {
        if ($this->isDeleted()) {
            throw new PropelException("You cannot save an object that has been deleted.");
        }

        if ($this->alreadyInSave) {
            return 0;
        }

        if ($con === null) {
            $con = Propel::getServiceContainer()->getWriteConnection(AssoTresorsArchiveTableMap::DATABASE_NAME);
        }

        return $con->transaction(function () use ($con) {
            $ret = $this->preSave($con);
            $isInsert = $this->isNew();
            if ($isInsert) {
                $ret = $ret && $this->preInsert($con);
            } else {
                $ret = $ret && $this->preUpdate($con);
            }
            if ($ret) {
                $affectedRows = $this->doSave($con);
                if ($isInsert) {
                    $this->postInsert($con);
                } else {
                    $this->postUpdate($con);
                }
                $this->postSave($con);
                AssoTresorsArchiveTableMap::addInstanceToPool($this);
            } else {
                $affectedRows = 0;
            }

            return $affectedRows;
        });
    }

    /**
     * Performs the work of inserting or updating the row in the database.
     *
     * If the object is new, it inserts it; otherwise an update is performed.
     * All related objects are also updated in this method.
     *
     * @param      ConnectionInterface $con
     * @return int             The number of rows affected by this insert/update and any referring fk objects' save() operations.
     * @throws PropelException
     * @see save()
     */
    protected function doSave(ConnectionInterface $con)
    {
        $affectedRows = 0; // initialize var to track total num of affected rows
        if (!$this->alreadyInSave) {
            $this->alreadyInSave = true;

            if ($this->isNew() || $this->isModified()) {
                // persist changes
                if ($this->isNew()) {
                    $this->doInsert($con);
                    $affectedRows += 1;
                } else {
                    $affectedRows += $this->doUpdate($con);
                }
                $this->resetModified();
            }

            $this->alreadyInSave = false;

        }

        return $affectedRows;
    } // doSave()

    /**
     * Insert the row in the database.
     *
     * @param      ConnectionInterface $con
     *
     * @throws PropelException
     * @see doSave()
     */
    protected function doInsert(ConnectionInterface $con)
    {
        $modifiedColumns = array();
        $index = 0;


         // check the columns in natural order for more readable SQL queries
        if ($this->isColumnModified(AssoTresorsArchiveTableMap::COL_ID_TRESOR)) {
            $modifiedColumns[':p' . $index++]  = '`id_tresor`';
        }
        if ($this->isColumnModified(AssoTresorsArchiveTableMap::COL_ID_ENTITE)) {
            $modifiedColumns[':p' . $index++]  = '`id_entite`';
        }
        if ($this->isColumnModified(AssoTresorsArchiveTableMap::COL_NOM)) {
            $modifiedColumns[':p' . $index++]  = '`nom`';
        }
        if ($this->isColumnModified(AssoTresorsArchiveTableMap::COL_NOMCOURT)) {
            $modifiedColumns[':p' . $index++]  = '`nomcourt`';
        }
        if ($this->isColumnModified(AssoTresorsArchiveTableMap::COL_IBAN)) {
            $modifiedColumns[':p' . $index++]  = '`iban`';
        }
        if ($this->isColumnModified(AssoTresorsArchiveTableMap::COL_BIC)) {
            $modifiedColumns[':p' . $index++]  = '`bic`';
        }
        if ($this->isColumnModified(AssoTresorsArchiveTableMap::COL_ID_COMPTERB)) {
            $modifiedColumns[':p' . $index++]  = '`id_compterb`';
        }
        if ($this->isColumnModified(AssoTresorsArchiveTableMap::COL_ID_COMPTE)) {
            $modifiedColumns[':p' . $index++]  = '`id_compte`';
        }
        if ($this->isColumnModified(AssoTresorsArchiveTableMap::COL_REMISE)) {
            $modifiedColumns[':p' . $index++]  = '`remise`';
        }
        if ($this->isColumnModified(AssoTresorsArchiveTableMap::COL_OBSERVATION)) {
            $modifiedColumns[':p' . $index++]  = '`observation`';
        }
        if ($this->isColumnModified(AssoTresorsArchiveTableMap::COL_CREATED_AT)) {
            $modifiedColumns[':p' . $index++]  = '`created_at`';
        }
        if ($this->isColumnModified(AssoTresorsArchiveTableMap::COL_UPDATED_AT)) {
            $modifiedColumns[':p' . $index++]  = '`updated_at`';
        }
        if ($this->isColumnModified(AssoTresorsArchiveTableMap::COL_ARCHIVED_AT)) {
            $modifiedColumns[':p' . $index++]  = '`archived_at`';
        }

        $sql = sprintf(
            'INSERT INTO `asso_tresors_archive` (%s) VALUES (%s)',
            implode(', ', $modifiedColumns),
            implode(', ', array_keys($modifiedColumns))
        );

        try {
            $stmt = $con->prepare($sql);
            foreach ($modifiedColumns as $identifier => $columnName) {
                switch ($columnName) {
                    case '`id_tresor`':
                        $stmt->bindValue($identifier, $this->id_tresor, PDO::PARAM_INT);
                        break;
                    case '`id_entite`':
                        $stmt->bindValue($identifier, $this->id_entite, PDO::PARAM_INT);
                        break;
                    case '`nom`':
                        $stmt->bindValue($identifier, $this->nom, PDO::PARAM_STR);
                        break;
                    case '`nomcourt`':
                        $stmt->bindValue($identifier, $this->nomcourt, PDO::PARAM_STR);
                        break;
                    case '`iban`':
                        $stmt->bindValue($identifier, $this->iban, PDO::PARAM_STR);
                        break;
                    case '`bic`':
                        $stmt->bindValue($identifier, $this->bic, PDO::PARAM_STR);
                        break;
                    case '`id_compterb`':
                        $stmt->bindValue($identifier, $this->id_compterb, PDO::PARAM_INT);
                        break;
                    case '`id_compte`':
                        $stmt->bindValue($identifier, $this->id_compte, PDO::PARAM_INT);
                        break;
                    case '`remise`':
                        $stmt->bindValue($identifier, $this->remise, PDO::PARAM_INT);
                        break;
                    case '`observation`':
                        $stmt->bindValue($identifier, $this->observation, PDO::PARAM_STR);
                        break;
                    case '`created_at`':
                        $stmt->bindValue($identifier, $this->created_at ? $this->created_at->format("Y-m-d H:i:s.u") : null, PDO::PARAM_STR);
                        break;
                    case '`updated_at`':
                        $stmt->bindValue($identifier, $this->updated_at ? $this->updated_at->format("Y-m-d H:i:s.u") : null, PDO::PARAM_STR);
                        break;
                    case '`archived_at`':
                        $stmt->bindValue($identifier, $this->archived_at ? $this->archived_at->format("Y-m-d H:i:s.u") : null, PDO::PARAM_STR);
                        break;
                }
            }
            $stmt->execute();
        } catch (Exception $e) {
            Propel::log($e->getMessage(), Propel::LOG_ERR);
            throw new PropelException(sprintf('Unable to execute INSERT statement [%s]', $sql), 0, $e);
        }

        $this->setNew(false);
    }

    /**
     * Update the row in the database.
     *
     * @param      ConnectionInterface $con
     *
     * @return Integer Number of updated rows
     * @see doSave()
     */
    protected function doUpdate(ConnectionInterface $con)
    {
        $selectCriteria = $this->buildPkeyCriteria();
        $valuesCriteria = $this->buildCriteria();

        return $selectCriteria->doUpdate($valuesCriteria, $con);
    }

    /**
     * Retrieves a field from the object by name passed in as a string.
     *
     * @param      string $name name
     * @param      string $type The type of fieldname the $name is of:
     *                     one of the class type constants TableMap::TYPE_PHPNAME, TableMap::TYPE_CAMELNAME
     *                     TableMap::TYPE_COLNAME, TableMap::TYPE_FIELDNAME, TableMap::TYPE_NUM.
     *                     Defaults to TableMap::TYPE_FIELDNAME.
     * @return mixed Value of field.
     */
    public function getByName($name, $type = TableMap::TYPE_FIELDNAME)
    {
        $pos = AssoTresorsArchiveTableMap::translateFieldName($name, $type, TableMap::TYPE_NUM);
        $field = $this->getByPosition($pos);

        return $field;
    }

    /**
     * Retrieves a field from the object by Position as specified in the xml schema.
     * Zero-based.
     *
     * @param      int $pos position in xml schema
     * @return mixed Value of field at $pos
     */
    public function getByPosition($pos)
    {
        switch ($pos) {
            case 0:
                return $this->getIdTresor();
                break;
            case 1:
                return $this->getIdEntite();
                break;
            case 2:
                return $this->getNom();
                break;
            case 3:
                return $this->getNomcourt();
                break;
            case 4:
                return $this->getIban();
                break;
            case 5:
                return $this->getBic();
                break;
            case 6:
                return $this->getIdCompterb();
                break;
            case 7:
                return $this->getIdCompte();
                break;
            case 8:
                return $this->getRemise();
                break;
            case 9:
                return $this->getObservation();
                break;
            case 10:
                return $this->getCreatedAt();
                break;
            case 11:
                return $this->getUpdatedAt();
                break;
            case 12:
                return $this->getArchivedAt();
                break;
            default:
                return null;
                break;
        } // switch()
    }

    /**
     * Exports the object as an array.
     *
     * You can specify the key type of the array by passing one of the class
     * type constants.
     *
     * @param     string  $keyType (optional) One of the class type constants TableMap::TYPE_PHPNAME, TableMap::TYPE_CAMELNAME,
     *                    TableMap::TYPE_COLNAME, TableMap::TYPE_FIELDNAME, TableMap::TYPE_NUM.
     *                    Defaults to TableMap::TYPE_FIELDNAME.
     * @param     boolean $includeLazyLoadColumns (optional) Whether to include lazy loaded columns. Defaults to TRUE.
     * @param     array $alreadyDumpedObjects List of objects to skip to avoid recursion
     *
     * @return array an associative array containing the field names (as keys) and field values
     */
    public function toArray($keyType = TableMap::TYPE_FIELDNAME, $includeLazyLoadColumns = true, $alreadyDumpedObjects = array())
    {

        if (isset($alreadyDumpedObjects['AssoTresorsArchive'][$this->hashCode()])) {
            return '*RECURSION*';
        }
        $alreadyDumpedObjects['AssoTresorsArchive'][$this->hashCode()] = true;
        $keys = AssoTresorsArchiveTableMap::getFieldNames($keyType);
        $result = array(
            $keys[0] => $this->getIdTresor(),
            $keys[1] => $this->getIdEntite(),
            $keys[2] => $this->getNom(),
            $keys[3] => $this->getNomcourt(),
            $keys[4] => $this->getIban(),
            $keys[5] => $this->getBic(),
            $keys[6] => $this->getIdCompterb(),
            $keys[7] => $this->getIdCompte(),
            $keys[8] => $this->getRemise(),
            $keys[9] => $this->getObservation(),
            $keys[10] => $this->getCreatedAt(),
            $keys[11] => $this->getUpdatedAt(),
            $keys[12] => $this->getArchivedAt(),
        );
        if ($result[$keys[10]] instanceof \DateTimeInterface) {
            $result[$keys[10]] = $result[$keys[10]]->format('c');
        }

        if ($result[$keys[11]] instanceof \DateTimeInterface) {
            $result[$keys[11]] = $result[$keys[11]]->format('c');
        }

        if ($result[$keys[12]] instanceof \DateTimeInterface) {
            $result[$keys[12]] = $result[$keys[12]]->format('c');
        }

        $virtualColumns = $this->virtualColumns;
        foreach ($virtualColumns as $key => $virtualColumn) {
            $result[$key] = $virtualColumn;
        }


        return $result;
    }

    /**
     * Sets a field from the object by name passed in as a string.
     *
     * @param  string $name
     * @param  mixed  $value field value
     * @param  string $type The type of fieldname the $name is of:
     *                one of the class type constants TableMap::TYPE_PHPNAME, TableMap::TYPE_CAMELNAME
     *                TableMap::TYPE_COLNAME, TableMap::TYPE_FIELDNAME, TableMap::TYPE_NUM.
     *                Defaults to TableMap::TYPE_FIELDNAME.
     * @return $this|\AssoTresorsArchive
     */
    public function setByName($name, $value, $type = TableMap::TYPE_FIELDNAME)
    {
        $pos = AssoTresorsArchiveTableMap::translateFieldName($name, $type, TableMap::TYPE_NUM);

        return $this->setByPosition($pos, $value);
    }

    /**
     * Sets a field from the object by Position as specified in the xml schema.
     * Zero-based.
     *
     * @param  int $pos position in xml schema
     * @param  mixed $value field value
     * @return $this|\AssoTresorsArchive
     */
    public function setByPosition($pos, $value)
    {
        switch ($pos) {
            case 0:
                $this->setIdTresor($value);
                break;
            case 1:
                $this->setIdEntite($value);
                break;
            case 2:
                $this->setNom($value);
                break;
            case 3:
                $this->setNomcourt($value);
                break;
            case 4:
                $this->setIban($value);
                break;
            case 5:
                $this->setBic($value);
                break;
            case 6:
                $this->setIdCompterb($value);
                break;
            case 7:
                $this->setIdCompte($value);
                break;
            case 8:
                $this->setRemise($value);
                break;
            case 9:
                $this->setObservation($value);
                break;
            case 10:
                $this->setCreatedAt($value);
                break;
            case 11:
                $this->setUpdatedAt($value);
                break;
            case 12:
                $this->setArchivedAt($value);
                break;
        } // switch()

        return $this;
    }

    /**
     * Populates the object using an array.
     *
     * This is particularly useful when populating an object from one of the
     * request arrays (e.g. $_POST).  This method goes through the column
     * names, checking to see whether a matching key exists in populated
     * array. If so the setByName() method is called for that column.
     *
     * You can specify the key type of the array by additionally passing one
     * of the class type constants TableMap::TYPE_PHPNAME, TableMap::TYPE_CAMELNAME,
     * TableMap::TYPE_COLNAME, TableMap::TYPE_FIELDNAME, TableMap::TYPE_NUM.
     * The default key type is the column's TableMap::TYPE_FIELDNAME.
     *
     * @param      array  $arr     An array to populate the object from.
     * @param      string $keyType The type of keys the array uses.
     * @return void
     */
    public function fromArray($arr, $keyType = TableMap::TYPE_FIELDNAME)
    {
        $keys = AssoTresorsArchiveTableMap::getFieldNames($keyType);

        if (array_key_exists($keys[0], $arr)) {
            $this->setIdTresor($arr[$keys[0]]);
        }
        if (array_key_exists($keys[1], $arr)) {
            $this->setIdEntite($arr[$keys[1]]);
        }
        if (array_key_exists($keys[2], $arr)) {
            $this->setNom($arr[$keys[2]]);
        }
        if (array_key_exists($keys[3], $arr)) {
            $this->setNomcourt($arr[$keys[3]]);
        }
        if (array_key_exists($keys[4], $arr)) {
            $this->setIban($arr[$keys[4]]);
        }
        if (array_key_exists($keys[5], $arr)) {
            $this->setBic($arr[$keys[5]]);
        }
        if (array_key_exists($keys[6], $arr)) {
            $this->setIdCompterb($arr[$keys[6]]);
        }
        if (array_key_exists($keys[7], $arr)) {
            $this->setIdCompte($arr[$keys[7]]);
        }
        if (array_key_exists($keys[8], $arr)) {
            $this->setRemise($arr[$keys[8]]);
        }
        if (array_key_exists($keys[9], $arr)) {
            $this->setObservation($arr[$keys[9]]);
        }
        if (array_key_exists($keys[10], $arr)) {
            $this->setCreatedAt($arr[$keys[10]]);
        }
        if (array_key_exists($keys[11], $arr)) {
            $this->setUpdatedAt($arr[$keys[11]]);
        }
        if (array_key_exists($keys[12], $arr)) {
            $this->setArchivedAt($arr[$keys[12]]);
        }
    }

     /**
     * Populate the current object from a string, using a given parser format
     * <code>
     * $book = new Book();
     * $book->importFrom('JSON', '{"Id":9012,"Title":"Don Juan","ISBN":"0140422161","Price":12.99,"PublisherId":1234,"AuthorId":5678}');
     * </code>
     *
     * You can specify the key type of the array by additionally passing one
     * of the class type constants TableMap::TYPE_PHPNAME, TableMap::TYPE_CAMELNAME,
     * TableMap::TYPE_COLNAME, TableMap::TYPE_FIELDNAME, TableMap::TYPE_NUM.
     * The default key type is the column's TableMap::TYPE_FIELDNAME.
     *
     * @param mixed $parser A AbstractParser instance,
     *                       or a format name ('XML', 'YAML', 'JSON', 'CSV')
     * @param string $data The source data to import from
     * @param string $keyType The type of keys the array uses.
     *
     * @return $this|\AssoTresorsArchive The current object, for fluid interface
     */
    public function importFrom($parser, $data, $keyType = TableMap::TYPE_FIELDNAME)
    {
        if (!$parser instanceof AbstractParser) {
            $parser = AbstractParser::getParser($parser);
        }

        $this->fromArray($parser->toArray($data), $keyType);

        return $this;
    }

    /**
     * Build a Criteria object containing the values of all modified columns in this object.
     *
     * @return Criteria The Criteria object containing all modified values.
     */
    public function buildCriteria()
    {
        $criteria = new Criteria(AssoTresorsArchiveTableMap::DATABASE_NAME);

        if ($this->isColumnModified(AssoTresorsArchiveTableMap::COL_ID_TRESOR)) {
            $criteria->add(AssoTresorsArchiveTableMap::COL_ID_TRESOR, $this->id_tresor);
        }
        if ($this->isColumnModified(AssoTresorsArchiveTableMap::COL_ID_ENTITE)) {
            $criteria->add(AssoTresorsArchiveTableMap::COL_ID_ENTITE, $this->id_entite);
        }
        if ($this->isColumnModified(AssoTresorsArchiveTableMap::COL_NOM)) {
            $criteria->add(AssoTresorsArchiveTableMap::COL_NOM, $this->nom);
        }
        if ($this->isColumnModified(AssoTresorsArchiveTableMap::COL_NOMCOURT)) {
            $criteria->add(AssoTresorsArchiveTableMap::COL_NOMCOURT, $this->nomcourt);
        }
        if ($this->isColumnModified(AssoTresorsArchiveTableMap::COL_IBAN)) {
            $criteria->add(AssoTresorsArchiveTableMap::COL_IBAN, $this->iban);
        }
        if ($this->isColumnModified(AssoTresorsArchiveTableMap::COL_BIC)) {
            $criteria->add(AssoTresorsArchiveTableMap::COL_BIC, $this->bic);
        }
        if ($this->isColumnModified(AssoTresorsArchiveTableMap::COL_ID_COMPTERB)) {
            $criteria->add(AssoTresorsArchiveTableMap::COL_ID_COMPTERB, $this->id_compterb);
        }
        if ($this->isColumnModified(AssoTresorsArchiveTableMap::COL_ID_COMPTE)) {
            $criteria->add(AssoTresorsArchiveTableMap::COL_ID_COMPTE, $this->id_compte);
        }
        if ($this->isColumnModified(AssoTresorsArchiveTableMap::COL_REMISE)) {
            $criteria->add(AssoTresorsArchiveTableMap::COL_REMISE, $this->remise);
        }
        if ($this->isColumnModified(AssoTresorsArchiveTableMap::COL_OBSERVATION)) {
            $criteria->add(AssoTresorsArchiveTableMap::COL_OBSERVATION, $this->observation);
        }
        if ($this->isColumnModified(AssoTresorsArchiveTableMap::COL_CREATED_AT)) {
            $criteria->add(AssoTresorsArchiveTableMap::COL_CREATED_AT, $this->created_at);
        }
        if ($this->isColumnModified(AssoTresorsArchiveTableMap::COL_UPDATED_AT)) {
            $criteria->add(AssoTresorsArchiveTableMap::COL_UPDATED_AT, $this->updated_at);
        }
        if ($this->isColumnModified(AssoTresorsArchiveTableMap::COL_ARCHIVED_AT)) {
            $criteria->add(AssoTresorsArchiveTableMap::COL_ARCHIVED_AT, $this->archived_at);
        }

        return $criteria;
    }

    /**
     * Builds a Criteria object containing the primary key for this object.
     *
     * Unlike buildCriteria() this method includes the primary key values regardless
     * of whether or not they have been modified.
     *
     * @throws LogicException if no primary key is defined
     *
     * @return Criteria The Criteria object containing value(s) for primary key(s).
     */
    public function buildPkeyCriteria()
    {
        $criteria = ChildAssoTresorsArchiveQuery::create();
        $criteria->add(AssoTresorsArchiveTableMap::COL_ID_TRESOR, $this->id_tresor);

        return $criteria;
    }

    /**
     * If the primary key is not null, return the hashcode of the
     * primary key. Otherwise, return the hash code of the object.
     *
     * @return int Hashcode
     */
    public function hashCode()
    {
        $validPk = null !== $this->getIdTresor();

        $validPrimaryKeyFKs = 0;
        $primaryKeyFKs = [];

        if ($validPk) {
            return crc32(json_encode($this->getPrimaryKey(), JSON_UNESCAPED_UNICODE));
        } elseif ($validPrimaryKeyFKs) {
            return crc32(json_encode($primaryKeyFKs, JSON_UNESCAPED_UNICODE));
        }

        return spl_object_hash($this);
    }

    /**
     * Returns the primary key for this object (row).
     * @return string
     */
    public function getPrimaryKey()
    {
        return $this->getIdTresor();
    }

    /**
     * Generic method to set the primary key (id_tresor column).
     *
     * @param       string $key Primary key.
     * @return void
     */
    public function setPrimaryKey($key)
    {
        $this->setIdTresor($key);
    }

    /**
     * Returns true if the primary key for this object is null.
     * @return boolean
     */
    public function isPrimaryKeyNull()
    {
        return null === $this->getIdTresor();
    }

    /**
     * Sets contents of passed object to values from current object.
     *
     * If desired, this method can also make copies of all associated (fkey referrers)
     * objects.
     *
     * @param      object $copyObj An object of \AssoTresorsArchive (or compatible) type.
     * @param      boolean $deepCopy Whether to also copy all rows that refer (by fkey) to the current row.
     * @param      boolean $makeNew Whether to reset autoincrement PKs and make the object new.
     * @throws PropelException
     */
    public function copyInto($copyObj, $deepCopy = false, $makeNew = true)
    {
        $copyObj->setIdTresor($this->getIdTresor());
        $copyObj->setIdEntite($this->getIdEntite());
        $copyObj->setNom($this->getNom());
        $copyObj->setNomcourt($this->getNomcourt());
        $copyObj->setIban($this->getIban());
        $copyObj->setBic($this->getBic());
        $copyObj->setIdCompterb($this->getIdCompterb());
        $copyObj->setIdCompte($this->getIdCompte());
        $copyObj->setRemise($this->getRemise());
        $copyObj->setObservation($this->getObservation());
        $copyObj->setCreatedAt($this->getCreatedAt());
        $copyObj->setUpdatedAt($this->getUpdatedAt());
        $copyObj->setArchivedAt($this->getArchivedAt());
        if ($makeNew) {
            $copyObj->setNew(true);
        }
    }

    /**
     * Makes a copy of this object that will be inserted as a new row in table when saved.
     * It creates a new object filling in the simple attributes, but skipping any primary
     * keys that are defined for the table.
     *
     * If desired, this method can also make copies of all associated (fkey referrers)
     * objects.
     *
     * @param  boolean $deepCopy Whether to also copy all rows that refer (by fkey) to the current row.
     * @return \AssoTresorsArchive Clone of current object.
     * @throws PropelException
     */
    public function copy($deepCopy = false)
    {
        // we use get_class(), because this might be a subclass
        $clazz = get_class($this);
        $copyObj = new $clazz();
        $this->copyInto($copyObj, $deepCopy);

        return $copyObj;
    }

    /**
     * Clears the current object, sets all attributes to their default values and removes
     * outgoing references as well as back-references (from other objects to this one. Results probably in a database
     * change of those foreign objects when you call `save` there).
     */
    public function clear()
    {
        $this->id_tresor = null;
        $this->id_entite = null;
        $this->nom = null;
        $this->nomcourt = null;
        $this->iban = null;
        $this->bic = null;
        $this->id_compterb = null;
        $this->id_compte = null;
        $this->remise = null;
        $this->observation = null;
        $this->created_at = null;
        $this->updated_at = null;
        $this->archived_at = null;
        $this->alreadyInSave = false;
        $this->clearAllReferences();
        $this->applyDefaultValues();
        $this->resetModified();
        $this->setNew(true);
        $this->setDeleted(false);
    }

    /**
     * Resets all references and back-references to other model objects or collections of model objects.
     *
     * This method is used to reset all php object references (not the actual reference in the database).
     * Necessary for object serialisation.
     *
     * @param      boolean $deep Whether to also clear the references on all referrer objects.
     */
    public function clearAllReferences($deep = false)
    {
        if ($deep) {
        } // if ($deep)

    }

    /**
     * Return the string representation of this object
     *
     * @return string
     */
    public function __toString()
    {
        return (string) $this->exportTo(AssoTresorsArchiveTableMap::DEFAULT_STRING_FORMAT);
    }

    /**
     * Code to be run before persisting the object
     * @param  ConnectionInterface $con
     * @return boolean
     */
    public function preSave(ConnectionInterface $con = null)
    {
        if (is_callable('parent::preSave')) {
            return parent::preSave($con);
        }
        return true;
    }

    /**
     * Code to be run after persisting the object
     * @param ConnectionInterface $con
     */
    public function postSave(ConnectionInterface $con = null)
    {
        if (is_callable('parent::postSave')) {
            parent::postSave($con);
        }
    }

    /**
     * Code to be run before inserting to database
     * @param  ConnectionInterface $con
     * @return boolean
     */
    public function preInsert(ConnectionInterface $con = null)
    {
        if (is_callable('parent::preInsert')) {
            return parent::preInsert($con);
        }
        return true;
    }

    /**
     * Code to be run after inserting to database
     * @param ConnectionInterface $con
     */
    public function postInsert(ConnectionInterface $con = null)
    {
        if (is_callable('parent::postInsert')) {
            parent::postInsert($con);
        }
    }

    /**
     * Code to be run before updating the object in database
     * @param  ConnectionInterface $con
     * @return boolean
     */
    public function preUpdate(ConnectionInterface $con = null)
    {
        if (is_callable('parent::preUpdate')) {
            return parent::preUpdate($con);
        }
        return true;
    }

    /**
     * Code to be run after updating the object in database
     * @param ConnectionInterface $con
     */
    public function postUpdate(ConnectionInterface $con = null)
    {
        if (is_callable('parent::postUpdate')) {
            parent::postUpdate($con);
        }
    }

    /**
     * Code to be run before deleting the object in database
     * @param  ConnectionInterface $con
     * @return boolean
     */
    public function preDelete(ConnectionInterface $con = null)
    {
        if (is_callable('parent::preDelete')) {
            return parent::preDelete($con);
        }
        return true;
    }

    /**
     * Code to be run after deleting the object in database
     * @param ConnectionInterface $con
     */
    public function postDelete(ConnectionInterface $con = null)
    {
        if (is_callable('parent::postDelete')) {
            parent::postDelete($con);
        }
    }


    /**
     * Derived method to catches calls to undefined methods.
     *
     * Provides magic import/export method support (fromXML()/toXML(), fromYAML()/toYAML(), etc.).
     * Allows to define default __call() behavior if you overwrite __call()
     *
     * @param string $name
     * @param mixed  $params
     *
     * @return array|string
     */
    public function __call($name, $params)
    {
        if (0 === strpos($name, 'get')) {
            $virtualColumn = substr($name, 3);
            if ($this->hasVirtualColumn($virtualColumn)) {
                return $this->getVirtualColumn($virtualColumn);
            }

            $virtualColumn = lcfirst($virtualColumn);
            if ($this->hasVirtualColumn($virtualColumn)) {
                return $this->getVirtualColumn($virtualColumn);
            }
        }

        if (0 === strpos($name, 'from')) {
            $format = substr($name, 4);

            return $this->importFrom($format, reset($params));
        }

        if (0 === strpos($name, 'to')) {
            $format = substr($name, 2);
            $includeLazyLoadColumns = isset($params[0]) ? $params[0] : true;

            return $this->exportTo($format, $includeLazyLoadColumns);
        }

        throw new BadMethodCallException(sprintf('Call to undefined method: %s.', $name));
    }

}
