<?php

namespace Base;

use \AsscPiecesArchive as ChildAsscPiecesArchive;
use \AsscPiecesArchiveQuery as ChildAsscPiecesArchiveQuery;
use \Exception;
use \PDO;
use Map\AsscPiecesArchiveTableMap;
use Propel\Runtime\Propel;
use Propel\Runtime\ActiveQuery\Criteria;
use Propel\Runtime\ActiveQuery\ModelCriteria;
use Propel\Runtime\Collection\ObjectCollection;
use Propel\Runtime\Connection\ConnectionInterface;
use Propel\Runtime\Exception\PropelException;

/**
 * Base class that represents a query for the 'assc_pieces_archive' table.
 *
 *
 *
 * @method     ChildAsscPiecesArchiveQuery orderByIdPiece($order = Criteria::ASC) Order by the id_piece column
 * @method     ChildAsscPiecesArchiveQuery orderByIdEcriture($order = Criteria::ASC) Order by the id_ecriture column
 * @method     ChildAsscPiecesArchiveQuery orderByClassement($order = Criteria::ASC) Order by the classement column
 * @method     ChildAsscPiecesArchiveQuery orderByNom($order = Criteria::ASC) Order by the nom column
 * @method     ChildAsscPiecesArchiveQuery orderByIdJournal($order = Criteria::ASC) Order by the id_journal column
 * @method     ChildAsscPiecesArchiveQuery orderByIdEntite($order = Criteria::ASC) Order by the id_entite column
 * @method     ChildAsscPiecesArchiveQuery orderByDatePiece($order = Criteria::ASC) Order by the date_piece column
 * @method     ChildAsscPiecesArchiveQuery orderByObservation($order = Criteria::ASC) Order by the observation column
 * @method     ChildAsscPiecesArchiveQuery orderByEcranSaisie($order = Criteria::ASC) Order by the ecran_saisie column
 * @method     ChildAsscPiecesArchiveQuery orderByEtat($order = Criteria::ASC) Order by the etat column
 * @method     ChildAsscPiecesArchiveQuery orderByNbLigne($order = Criteria::ASC) Order by the nb_ligne column
 * @method     ChildAsscPiecesArchiveQuery orderByCreatedAt($order = Criteria::ASC) Order by the created_at column
 * @method     ChildAsscPiecesArchiveQuery orderByUpdatedAt($order = Criteria::ASC) Order by the updated_at column
 * @method     ChildAsscPiecesArchiveQuery orderByArchivedAt($order = Criteria::ASC) Order by the archived_at column
 *
 * @method     ChildAsscPiecesArchiveQuery groupByIdPiece() Group by the id_piece column
 * @method     ChildAsscPiecesArchiveQuery groupByIdEcriture() Group by the id_ecriture column
 * @method     ChildAsscPiecesArchiveQuery groupByClassement() Group by the classement column
 * @method     ChildAsscPiecesArchiveQuery groupByNom() Group by the nom column
 * @method     ChildAsscPiecesArchiveQuery groupByIdJournal() Group by the id_journal column
 * @method     ChildAsscPiecesArchiveQuery groupByIdEntite() Group by the id_entite column
 * @method     ChildAsscPiecesArchiveQuery groupByDatePiece() Group by the date_piece column
 * @method     ChildAsscPiecesArchiveQuery groupByObservation() Group by the observation column
 * @method     ChildAsscPiecesArchiveQuery groupByEcranSaisie() Group by the ecran_saisie column
 * @method     ChildAsscPiecesArchiveQuery groupByEtat() Group by the etat column
 * @method     ChildAsscPiecesArchiveQuery groupByNbLigne() Group by the nb_ligne column
 * @method     ChildAsscPiecesArchiveQuery groupByCreatedAt() Group by the created_at column
 * @method     ChildAsscPiecesArchiveQuery groupByUpdatedAt() Group by the updated_at column
 * @method     ChildAsscPiecesArchiveQuery groupByArchivedAt() Group by the archived_at column
 *
 * @method     ChildAsscPiecesArchiveQuery leftJoin($relation) Adds a LEFT JOIN clause to the query
 * @method     ChildAsscPiecesArchiveQuery rightJoin($relation) Adds a RIGHT JOIN clause to the query
 * @method     ChildAsscPiecesArchiveQuery innerJoin($relation) Adds a INNER JOIN clause to the query
 *
 * @method     ChildAsscPiecesArchiveQuery leftJoinWith($relation) Adds a LEFT JOIN clause and with to the query
 * @method     ChildAsscPiecesArchiveQuery rightJoinWith($relation) Adds a RIGHT JOIN clause and with to the query
 * @method     ChildAsscPiecesArchiveQuery innerJoinWith($relation) Adds a INNER JOIN clause and with to the query
 *
 * @method     ChildAsscPiecesArchive findOne(ConnectionInterface $con = null) Return the first ChildAsscPiecesArchive matching the query
 * @method     ChildAsscPiecesArchive findOneOrCreate(ConnectionInterface $con = null) Return the first ChildAsscPiecesArchive matching the query, or a new ChildAsscPiecesArchive object populated from the query conditions when no match is found
 *
 * @method     ChildAsscPiecesArchive findOneByIdPiece(string $id_piece) Return the first ChildAsscPiecesArchive filtered by the id_piece column
 * @method     ChildAsscPiecesArchive findOneByIdEcriture(string $id_ecriture) Return the first ChildAsscPiecesArchive filtered by the id_ecriture column
 * @method     ChildAsscPiecesArchive findOneByClassement(string $classement) Return the first ChildAsscPiecesArchive filtered by the classement column
 * @method     ChildAsscPiecesArchive findOneByNom(string $nom) Return the first ChildAsscPiecesArchive filtered by the nom column
 * @method     ChildAsscPiecesArchive findOneByIdJournal(int $id_journal) Return the first ChildAsscPiecesArchive filtered by the id_journal column
 * @method     ChildAsscPiecesArchive findOneByIdEntite(string $id_entite) Return the first ChildAsscPiecesArchive filtered by the id_entite column
 * @method     ChildAsscPiecesArchive findOneByDatePiece(string $date_piece) Return the first ChildAsscPiecesArchive filtered by the date_piece column
 * @method     ChildAsscPiecesArchive findOneByObservation(string $observation) Return the first ChildAsscPiecesArchive filtered by the observation column
 * @method     ChildAsscPiecesArchive findOneByEcranSaisie(string $ecran_saisie) Return the first ChildAsscPiecesArchive filtered by the ecran_saisie column
 * @method     ChildAsscPiecesArchive findOneByEtat(int $etat) Return the first ChildAsscPiecesArchive filtered by the etat column
 * @method     ChildAsscPiecesArchive findOneByNbLigne(int $nb_ligne) Return the first ChildAsscPiecesArchive filtered by the nb_ligne column
 * @method     ChildAsscPiecesArchive findOneByCreatedAt(string $created_at) Return the first ChildAsscPiecesArchive filtered by the created_at column
 * @method     ChildAsscPiecesArchive findOneByUpdatedAt(string $updated_at) Return the first ChildAsscPiecesArchive filtered by the updated_at column
 * @method     ChildAsscPiecesArchive findOneByArchivedAt(string $archived_at) Return the first ChildAsscPiecesArchive filtered by the archived_at column *

 * @method     ChildAsscPiecesArchive requirePk($key, ConnectionInterface $con = null) Return the ChildAsscPiecesArchive by primary key and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildAsscPiecesArchive requireOne(ConnectionInterface $con = null) Return the first ChildAsscPiecesArchive matching the query and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 *
 * @method     ChildAsscPiecesArchive requireOneByIdPiece(string $id_piece) Return the first ChildAsscPiecesArchive filtered by the id_piece column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildAsscPiecesArchive requireOneByIdEcriture(string $id_ecriture) Return the first ChildAsscPiecesArchive filtered by the id_ecriture column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildAsscPiecesArchive requireOneByClassement(string $classement) Return the first ChildAsscPiecesArchive filtered by the classement column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildAsscPiecesArchive requireOneByNom(string $nom) Return the first ChildAsscPiecesArchive filtered by the nom column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildAsscPiecesArchive requireOneByIdJournal(int $id_journal) Return the first ChildAsscPiecesArchive filtered by the id_journal column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildAsscPiecesArchive requireOneByIdEntite(string $id_entite) Return the first ChildAsscPiecesArchive filtered by the id_entite column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildAsscPiecesArchive requireOneByDatePiece(string $date_piece) Return the first ChildAsscPiecesArchive filtered by the date_piece column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildAsscPiecesArchive requireOneByObservation(string $observation) Return the first ChildAsscPiecesArchive filtered by the observation column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildAsscPiecesArchive requireOneByEcranSaisie(string $ecran_saisie) Return the first ChildAsscPiecesArchive filtered by the ecran_saisie column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildAsscPiecesArchive requireOneByEtat(int $etat) Return the first ChildAsscPiecesArchive filtered by the etat column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildAsscPiecesArchive requireOneByNbLigne(int $nb_ligne) Return the first ChildAsscPiecesArchive filtered by the nb_ligne column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildAsscPiecesArchive requireOneByCreatedAt(string $created_at) Return the first ChildAsscPiecesArchive filtered by the created_at column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildAsscPiecesArchive requireOneByUpdatedAt(string $updated_at) Return the first ChildAsscPiecesArchive filtered by the updated_at column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildAsscPiecesArchive requireOneByArchivedAt(string $archived_at) Return the first ChildAsscPiecesArchive filtered by the archived_at column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 *
 * @method     ChildAsscPiecesArchive[]|ObjectCollection find(ConnectionInterface $con = null) Return ChildAsscPiecesArchive objects based on current ModelCriteria
 * @method     ChildAsscPiecesArchive[]|ObjectCollection findByIdPiece(string $id_piece) Return ChildAsscPiecesArchive objects filtered by the id_piece column
 * @method     ChildAsscPiecesArchive[]|ObjectCollection findByIdEcriture(string $id_ecriture) Return ChildAsscPiecesArchive objects filtered by the id_ecriture column
 * @method     ChildAsscPiecesArchive[]|ObjectCollection findByClassement(string $classement) Return ChildAsscPiecesArchive objects filtered by the classement column
 * @method     ChildAsscPiecesArchive[]|ObjectCollection findByNom(string $nom) Return ChildAsscPiecesArchive objects filtered by the nom column
 * @method     ChildAsscPiecesArchive[]|ObjectCollection findByIdJournal(int $id_journal) Return ChildAsscPiecesArchive objects filtered by the id_journal column
 * @method     ChildAsscPiecesArchive[]|ObjectCollection findByIdEntite(string $id_entite) Return ChildAsscPiecesArchive objects filtered by the id_entite column
 * @method     ChildAsscPiecesArchive[]|ObjectCollection findByDatePiece(string $date_piece) Return ChildAsscPiecesArchive objects filtered by the date_piece column
 * @method     ChildAsscPiecesArchive[]|ObjectCollection findByObservation(string $observation) Return ChildAsscPiecesArchive objects filtered by the observation column
 * @method     ChildAsscPiecesArchive[]|ObjectCollection findByEcranSaisie(string $ecran_saisie) Return ChildAsscPiecesArchive objects filtered by the ecran_saisie column
 * @method     ChildAsscPiecesArchive[]|ObjectCollection findByEtat(int $etat) Return ChildAsscPiecesArchive objects filtered by the etat column
 * @method     ChildAsscPiecesArchive[]|ObjectCollection findByNbLigne(int $nb_ligne) Return ChildAsscPiecesArchive objects filtered by the nb_ligne column
 * @method     ChildAsscPiecesArchive[]|ObjectCollection findByCreatedAt(string $created_at) Return ChildAsscPiecesArchive objects filtered by the created_at column
 * @method     ChildAsscPiecesArchive[]|ObjectCollection findByUpdatedAt(string $updated_at) Return ChildAsscPiecesArchive objects filtered by the updated_at column
 * @method     ChildAsscPiecesArchive[]|ObjectCollection findByArchivedAt(string $archived_at) Return ChildAsscPiecesArchive objects filtered by the archived_at column
 * @method     ChildAsscPiecesArchive[]|\Propel\Runtime\Util\PropelModelPager paginate($page = 1, $maxPerPage = 10, ConnectionInterface $con = null) Issue a SELECT query based on the current ModelCriteria and uses a page and a maximum number of results per page to compute an offset and a limit
 *
 */
abstract class AsscPiecesArchiveQuery extends ModelCriteria
{
    protected $entityNotFoundExceptionClass = '\\Propel\\Runtime\\Exception\\EntityNotFoundException';

    /**
     * Initializes internal state of \Base\AsscPiecesArchiveQuery object.
     *
     * @param     string $dbName The database name
     * @param     string $modelName The phpName of a model, e.g. 'Book'
     * @param     string $modelAlias The alias for the model in this query, e.g. 'b'
     */
    public function __construct($dbName = 'simplasso', $modelName = '\\AsscPiecesArchive', $modelAlias = null)
    {
        parent::__construct($dbName, $modelName, $modelAlias);
    }

    /**
     * Returns a new ChildAsscPiecesArchiveQuery object.
     *
     * @param     string $modelAlias The alias of a model in the query
     * @param     Criteria $criteria Optional Criteria to build the query from
     *
     * @return ChildAsscPiecesArchiveQuery
     */
    public static function create($modelAlias = null, Criteria $criteria = null)
    {
        if ($criteria instanceof ChildAsscPiecesArchiveQuery) {
            return $criteria;
        }
        $query = new ChildAsscPiecesArchiveQuery();
        if (null !== $modelAlias) {
            $query->setModelAlias($modelAlias);
        }
        if ($criteria instanceof Criteria) {
            $query->mergeWith($criteria);
        }

        return $query;
    }

    /**
     * Find object by primary key.
     * Propel uses the instance pool to skip the database if the object exists.
     * Go fast if the query is untouched.
     *
     * <code>
     * $obj  = $c->findPk(12, $con);
     * </code>
     *
     * @param mixed $key Primary key to use for the query
     * @param ConnectionInterface $con an optional connection object
     *
     * @return ChildAsscPiecesArchive|array|mixed the result, formatted by the current formatter
     */
    public function findPk($key, ConnectionInterface $con = null)
    {
        if ($key === null) {
            return null;
        }

        if ($con === null) {
            $con = Propel::getServiceContainer()->getReadConnection(AsscPiecesArchiveTableMap::DATABASE_NAME);
        }

        $this->basePreSelect($con);

        if (
            $this->formatter || $this->modelAlias || $this->with || $this->select
            || $this->selectColumns || $this->asColumns || $this->selectModifiers
            || $this->map || $this->having || $this->joins
        ) {
            return $this->findPkComplex($key, $con);
        }

        if ((null !== ($obj = AsscPiecesArchiveTableMap::getInstanceFromPool(null === $key || is_scalar($key) || is_callable([$key, '__toString']) ? (string) $key : $key)))) {
            // the object is already in the instance pool
            return $obj;
        }

        return $this->findPkSimple($key, $con);
    }

    /**
     * Find object by primary key using raw SQL to go fast.
     * Bypass doSelect() and the object formatter by using generated code.
     *
     * @param     mixed $key Primary key to use for the query
     * @param     ConnectionInterface $con A connection object
     *
     * @throws \Propel\Runtime\Exception\PropelException
     *
     * @return ChildAsscPiecesArchive A model object, or null if the key is not found
     */
    protected function findPkSimple($key, ConnectionInterface $con)
    {
        $sql = 'SELECT `id_piece`, `id_ecriture`, `classement`, `nom`, `id_journal`, `id_entite`, `date_piece`, `observation`, `ecran_saisie`, `etat`, `nb_ligne`, `created_at`, `updated_at`, `archived_at` FROM `assc_pieces_archive` WHERE `id_piece` = :p0';
        try {
            $stmt = $con->prepare($sql);
            $stmt->bindValue(':p0', $key, PDO::PARAM_INT);
            $stmt->execute();
        } catch (Exception $e) {
            Propel::log($e->getMessage(), Propel::LOG_ERR);
            throw new PropelException(sprintf('Unable to execute SELECT statement [%s]', $sql), 0, $e);
        }
        $obj = null;
        if ($row = $stmt->fetch(\PDO::FETCH_NUM)) {
            /** @var ChildAsscPiecesArchive $obj */
            $obj = new ChildAsscPiecesArchive();
            $obj->hydrate($row);
            AsscPiecesArchiveTableMap::addInstanceToPool($obj, null === $key || is_scalar($key) || is_callable([$key, '__toString']) ? (string) $key : $key);
        }
        $stmt->closeCursor();

        return $obj;
    }

    /**
     * Find object by primary key.
     *
     * @param     mixed $key Primary key to use for the query
     * @param     ConnectionInterface $con A connection object
     *
     * @return ChildAsscPiecesArchive|array|mixed the result, formatted by the current formatter
     */
    protected function findPkComplex($key, ConnectionInterface $con)
    {
        // As the query uses a PK condition, no limit(1) is necessary.
        $criteria = $this->isKeepQuery() ? clone $this : $this;
        $dataFetcher = $criteria
            ->filterByPrimaryKey($key)
            ->doSelect($con);

        return $criteria->getFormatter()->init($criteria)->formatOne($dataFetcher);
    }

    /**
     * Find objects by primary key
     * <code>
     * $objs = $c->findPks(array(12, 56, 832), $con);
     * </code>
     * @param     array $keys Primary keys to use for the query
     * @param     ConnectionInterface $con an optional connection object
     *
     * @return ObjectCollection|array|mixed the list of results, formatted by the current formatter
     */
    public function findPks($keys, ConnectionInterface $con = null)
    {
        if (null === $con) {
            $con = Propel::getServiceContainer()->getReadConnection($this->getDbName());
        }
        $this->basePreSelect($con);
        $criteria = $this->isKeepQuery() ? clone $this : $this;
        $dataFetcher = $criteria
            ->filterByPrimaryKeys($keys)
            ->doSelect($con);

        return $criteria->getFormatter()->init($criteria)->format($dataFetcher);
    }

    /**
     * Filter the query by primary key
     *
     * @param     mixed $key Primary key to use for the query
     *
     * @return $this|ChildAsscPiecesArchiveQuery The current query, for fluid interface
     */
    public function filterByPrimaryKey($key)
    {

        return $this->addUsingAlias(AsscPiecesArchiveTableMap::COL_ID_PIECE, $key, Criteria::EQUAL);
    }

    /**
     * Filter the query by a list of primary keys
     *
     * @param     array $keys The list of primary key to use for the query
     *
     * @return $this|ChildAsscPiecesArchiveQuery The current query, for fluid interface
     */
    public function filterByPrimaryKeys($keys)
    {

        return $this->addUsingAlias(AsscPiecesArchiveTableMap::COL_ID_PIECE, $keys, Criteria::IN);
    }

    /**
     * Filter the query on the id_piece column
     *
     * Example usage:
     * <code>
     * $query->filterByIdPiece(1234); // WHERE id_piece = 1234
     * $query->filterByIdPiece(array(12, 34)); // WHERE id_piece IN (12, 34)
     * $query->filterByIdPiece(array('min' => 12)); // WHERE id_piece > 12
     * </code>
     *
     * @param     mixed $idPiece The value to use as filter.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildAsscPiecesArchiveQuery The current query, for fluid interface
     */
    public function filterByIdPiece($idPiece = null, $comparison = null)
    {
        if (is_array($idPiece)) {
            $useMinMax = false;
            if (isset($idPiece['min'])) {
                $this->addUsingAlias(AsscPiecesArchiveTableMap::COL_ID_PIECE, $idPiece['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($idPiece['max'])) {
                $this->addUsingAlias(AsscPiecesArchiveTableMap::COL_ID_PIECE, $idPiece['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(AsscPiecesArchiveTableMap::COL_ID_PIECE, $idPiece, $comparison);
    }

    /**
     * Filter the query on the id_ecriture column
     *
     * Example usage:
     * <code>
     * $query->filterByIdEcriture(1234); // WHERE id_ecriture = 1234
     * $query->filterByIdEcriture(array(12, 34)); // WHERE id_ecriture IN (12, 34)
     * $query->filterByIdEcriture(array('min' => 12)); // WHERE id_ecriture > 12
     * </code>
     *
     * @param     mixed $idEcriture The value to use as filter.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildAsscPiecesArchiveQuery The current query, for fluid interface
     */
    public function filterByIdEcriture($idEcriture = null, $comparison = null)
    {
        if (is_array($idEcriture)) {
            $useMinMax = false;
            if (isset($idEcriture['min'])) {
                $this->addUsingAlias(AsscPiecesArchiveTableMap::COL_ID_ECRITURE, $idEcriture['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($idEcriture['max'])) {
                $this->addUsingAlias(AsscPiecesArchiveTableMap::COL_ID_ECRITURE, $idEcriture['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(AsscPiecesArchiveTableMap::COL_ID_ECRITURE, $idEcriture, $comparison);
    }

    /**
     * Filter the query on the classement column
     *
     * Example usage:
     * <code>
     * $query->filterByClassement('fooValue');   // WHERE classement = 'fooValue'
     * $query->filterByClassement('%fooValue%', Criteria::LIKE); // WHERE classement LIKE '%fooValue%'
     * </code>
     *
     * @param     string $classement The value to use as filter.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildAsscPiecesArchiveQuery The current query, for fluid interface
     */
    public function filterByClassement($classement = null, $comparison = null)
    {
        if (null === $comparison) {
            if (is_array($classement)) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(AsscPiecesArchiveTableMap::COL_CLASSEMENT, $classement, $comparison);
    }

    /**
     * Filter the query on the nom column
     *
     * Example usage:
     * <code>
     * $query->filterByNom('fooValue');   // WHERE nom = 'fooValue'
     * $query->filterByNom('%fooValue%', Criteria::LIKE); // WHERE nom LIKE '%fooValue%'
     * </code>
     *
     * @param     string $nom The value to use as filter.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildAsscPiecesArchiveQuery The current query, for fluid interface
     */
    public function filterByNom($nom = null, $comparison = null)
    {
        if (null === $comparison) {
            if (is_array($nom)) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(AsscPiecesArchiveTableMap::COL_NOM, $nom, $comparison);
    }

    /**
     * Filter the query on the id_journal column
     *
     * Example usage:
     * <code>
     * $query->filterByIdJournal(1234); // WHERE id_journal = 1234
     * $query->filterByIdJournal(array(12, 34)); // WHERE id_journal IN (12, 34)
     * $query->filterByIdJournal(array('min' => 12)); // WHERE id_journal > 12
     * </code>
     *
     * @param     mixed $idJournal The value to use as filter.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildAsscPiecesArchiveQuery The current query, for fluid interface
     */
    public function filterByIdJournal($idJournal = null, $comparison = null)
    {
        if (is_array($idJournal)) {
            $useMinMax = false;
            if (isset($idJournal['min'])) {
                $this->addUsingAlias(AsscPiecesArchiveTableMap::COL_ID_JOURNAL, $idJournal['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($idJournal['max'])) {
                $this->addUsingAlias(AsscPiecesArchiveTableMap::COL_ID_JOURNAL, $idJournal['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(AsscPiecesArchiveTableMap::COL_ID_JOURNAL, $idJournal, $comparison);
    }

    /**
     * Filter the query on the id_entite column
     *
     * Example usage:
     * <code>
     * $query->filterByIdEntite(1234); // WHERE id_entite = 1234
     * $query->filterByIdEntite(array(12, 34)); // WHERE id_entite IN (12, 34)
     * $query->filterByIdEntite(array('min' => 12)); // WHERE id_entite > 12
     * </code>
     *
     * @param     mixed $idEntite The value to use as filter.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildAsscPiecesArchiveQuery The current query, for fluid interface
     */
    public function filterByIdEntite($idEntite = null, $comparison = null)
    {
        if (is_array($idEntite)) {
            $useMinMax = false;
            if (isset($idEntite['min'])) {
                $this->addUsingAlias(AsscPiecesArchiveTableMap::COL_ID_ENTITE, $idEntite['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($idEntite['max'])) {
                $this->addUsingAlias(AsscPiecesArchiveTableMap::COL_ID_ENTITE, $idEntite['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(AsscPiecesArchiveTableMap::COL_ID_ENTITE, $idEntite, $comparison);
    }

    /**
     * Filter the query on the date_piece column
     *
     * Example usage:
     * <code>
     * $query->filterByDatePiece('2011-03-14'); // WHERE date_piece = '2011-03-14'
     * $query->filterByDatePiece('now'); // WHERE date_piece = '2011-03-14'
     * $query->filterByDatePiece(array('max' => 'yesterday')); // WHERE date_piece > '2011-03-13'
     * </code>
     *
     * @param     mixed $datePiece The value to use as filter.
     *              Values can be integers (unix timestamps), DateTime objects, or strings.
     *              Empty strings are treated as NULL.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildAsscPiecesArchiveQuery The current query, for fluid interface
     */
    public function filterByDatePiece($datePiece = null, $comparison = null)
    {
        if (is_array($datePiece)) {
            $useMinMax = false;
            if (isset($datePiece['min'])) {
                $this->addUsingAlias(AsscPiecesArchiveTableMap::COL_DATE_PIECE, $datePiece['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($datePiece['max'])) {
                $this->addUsingAlias(AsscPiecesArchiveTableMap::COL_DATE_PIECE, $datePiece['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(AsscPiecesArchiveTableMap::COL_DATE_PIECE, $datePiece, $comparison);
    }

    /**
     * Filter the query on the observation column
     *
     * Example usage:
     * <code>
     * $query->filterByObservation('fooValue');   // WHERE observation = 'fooValue'
     * $query->filterByObservation('%fooValue%', Criteria::LIKE); // WHERE observation LIKE '%fooValue%'
     * </code>
     *
     * @param     string $observation The value to use as filter.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildAsscPiecesArchiveQuery The current query, for fluid interface
     */
    public function filterByObservation($observation = null, $comparison = null)
    {
        if (null === $comparison) {
            if (is_array($observation)) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(AsscPiecesArchiveTableMap::COL_OBSERVATION, $observation, $comparison);
    }

    /**
     * Filter the query on the ecran_saisie column
     *
     * Example usage:
     * <code>
     * $query->filterByEcranSaisie('fooValue');   // WHERE ecran_saisie = 'fooValue'
     * $query->filterByEcranSaisie('%fooValue%', Criteria::LIKE); // WHERE ecran_saisie LIKE '%fooValue%'
     * </code>
     *
     * @param     string $ecranSaisie The value to use as filter.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildAsscPiecesArchiveQuery The current query, for fluid interface
     */
    public function filterByEcranSaisie($ecranSaisie = null, $comparison = null)
    {
        if (null === $comparison) {
            if (is_array($ecranSaisie)) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(AsscPiecesArchiveTableMap::COL_ECRAN_SAISIE, $ecranSaisie, $comparison);
    }

    /**
     * Filter the query on the etat column
     *
     * Example usage:
     * <code>
     * $query->filterByEtat(1234); // WHERE etat = 1234
     * $query->filterByEtat(array(12, 34)); // WHERE etat IN (12, 34)
     * $query->filterByEtat(array('min' => 12)); // WHERE etat > 12
     * </code>
     *
     * @param     mixed $etat The value to use as filter.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildAsscPiecesArchiveQuery The current query, for fluid interface
     */
    public function filterByEtat($etat = null, $comparison = null)
    {
        if (is_array($etat)) {
            $useMinMax = false;
            if (isset($etat['min'])) {
                $this->addUsingAlias(AsscPiecesArchiveTableMap::COL_ETAT, $etat['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($etat['max'])) {
                $this->addUsingAlias(AsscPiecesArchiveTableMap::COL_ETAT, $etat['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(AsscPiecesArchiveTableMap::COL_ETAT, $etat, $comparison);
    }

    /**
     * Filter the query on the nb_ligne column
     *
     * Example usage:
     * <code>
     * $query->filterByNbLigne(1234); // WHERE nb_ligne = 1234
     * $query->filterByNbLigne(array(12, 34)); // WHERE nb_ligne IN (12, 34)
     * $query->filterByNbLigne(array('min' => 12)); // WHERE nb_ligne > 12
     * </code>
     *
     * @param     mixed $nbLigne The value to use as filter.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildAsscPiecesArchiveQuery The current query, for fluid interface
     */
    public function filterByNbLigne($nbLigne = null, $comparison = null)
    {
        if (is_array($nbLigne)) {
            $useMinMax = false;
            if (isset($nbLigne['min'])) {
                $this->addUsingAlias(AsscPiecesArchiveTableMap::COL_NB_LIGNE, $nbLigne['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($nbLigne['max'])) {
                $this->addUsingAlias(AsscPiecesArchiveTableMap::COL_NB_LIGNE, $nbLigne['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(AsscPiecesArchiveTableMap::COL_NB_LIGNE, $nbLigne, $comparison);
    }

    /**
     * Filter the query on the created_at column
     *
     * Example usage:
     * <code>
     * $query->filterByCreatedAt('2011-03-14'); // WHERE created_at = '2011-03-14'
     * $query->filterByCreatedAt('now'); // WHERE created_at = '2011-03-14'
     * $query->filterByCreatedAt(array('max' => 'yesterday')); // WHERE created_at > '2011-03-13'
     * </code>
     *
     * @param     mixed $createdAt The value to use as filter.
     *              Values can be integers (unix timestamps), DateTime objects, or strings.
     *              Empty strings are treated as NULL.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildAsscPiecesArchiveQuery The current query, for fluid interface
     */
    public function filterByCreatedAt($createdAt = null, $comparison = null)
    {
        if (is_array($createdAt)) {
            $useMinMax = false;
            if (isset($createdAt['min'])) {
                $this->addUsingAlias(AsscPiecesArchiveTableMap::COL_CREATED_AT, $createdAt['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($createdAt['max'])) {
                $this->addUsingAlias(AsscPiecesArchiveTableMap::COL_CREATED_AT, $createdAt['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(AsscPiecesArchiveTableMap::COL_CREATED_AT, $createdAt, $comparison);
    }

    /**
     * Filter the query on the updated_at column
     *
     * Example usage:
     * <code>
     * $query->filterByUpdatedAt('2011-03-14'); // WHERE updated_at = '2011-03-14'
     * $query->filterByUpdatedAt('now'); // WHERE updated_at = '2011-03-14'
     * $query->filterByUpdatedAt(array('max' => 'yesterday')); // WHERE updated_at > '2011-03-13'
     * </code>
     *
     * @param     mixed $updatedAt The value to use as filter.
     *              Values can be integers (unix timestamps), DateTime objects, or strings.
     *              Empty strings are treated as NULL.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildAsscPiecesArchiveQuery The current query, for fluid interface
     */
    public function filterByUpdatedAt($updatedAt = null, $comparison = null)
    {
        if (is_array($updatedAt)) {
            $useMinMax = false;
            if (isset($updatedAt['min'])) {
                $this->addUsingAlias(AsscPiecesArchiveTableMap::COL_UPDATED_AT, $updatedAt['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($updatedAt['max'])) {
                $this->addUsingAlias(AsscPiecesArchiveTableMap::COL_UPDATED_AT, $updatedAt['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(AsscPiecesArchiveTableMap::COL_UPDATED_AT, $updatedAt, $comparison);
    }

    /**
     * Filter the query on the archived_at column
     *
     * Example usage:
     * <code>
     * $query->filterByArchivedAt('2011-03-14'); // WHERE archived_at = '2011-03-14'
     * $query->filterByArchivedAt('now'); // WHERE archived_at = '2011-03-14'
     * $query->filterByArchivedAt(array('max' => 'yesterday')); // WHERE archived_at > '2011-03-13'
     * </code>
     *
     * @param     mixed $archivedAt The value to use as filter.
     *              Values can be integers (unix timestamps), DateTime objects, or strings.
     *              Empty strings are treated as NULL.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildAsscPiecesArchiveQuery The current query, for fluid interface
     */
    public function filterByArchivedAt($archivedAt = null, $comparison = null)
    {
        if (is_array($archivedAt)) {
            $useMinMax = false;
            if (isset($archivedAt['min'])) {
                $this->addUsingAlias(AsscPiecesArchiveTableMap::COL_ARCHIVED_AT, $archivedAt['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($archivedAt['max'])) {
                $this->addUsingAlias(AsscPiecesArchiveTableMap::COL_ARCHIVED_AT, $archivedAt['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(AsscPiecesArchiveTableMap::COL_ARCHIVED_AT, $archivedAt, $comparison);
    }

    /**
     * Exclude object from result
     *
     * @param   ChildAsscPiecesArchive $asscPiecesArchive Object to remove from the list of results
     *
     * @return $this|ChildAsscPiecesArchiveQuery The current query, for fluid interface
     */
    public function prune($asscPiecesArchive = null)
    {
        if ($asscPiecesArchive) {
            $this->addUsingAlias(AsscPiecesArchiveTableMap::COL_ID_PIECE, $asscPiecesArchive->getIdPiece(), Criteria::NOT_EQUAL);
        }

        return $this;
    }

    /**
     * Deletes all rows from the assc_pieces_archive table.
     *
     * @param ConnectionInterface $con the connection to use
     * @return int The number of affected rows (if supported by underlying database driver).
     */
    public function doDeleteAll(ConnectionInterface $con = null)
    {
        if (null === $con) {
            $con = Propel::getServiceContainer()->getWriteConnection(AsscPiecesArchiveTableMap::DATABASE_NAME);
        }

        // use transaction because $criteria could contain info
        // for more than one table or we could emulating ON DELETE CASCADE, etc.
        return $con->transaction(function () use ($con) {
            $affectedRows = 0; // initialize var to track total num of affected rows
            $affectedRows += parent::doDeleteAll($con);
            // Because this db requires some delete cascade/set null emulation, we have to
            // clear the cached instance *after* the emulation has happened (since
            // instances get re-added by the select statement contained therein).
            AsscPiecesArchiveTableMap::clearInstancePool();
            AsscPiecesArchiveTableMap::clearRelatedInstancePool();

            return $affectedRows;
        });
    }

    /**
     * Performs a DELETE on the database based on the current ModelCriteria
     *
     * @param ConnectionInterface $con the connection to use
     * @return int             The number of affected rows (if supported by underlying database driver).  This includes CASCADE-related rows
     *                         if supported by native driver or if emulated using Propel.
     * @throws PropelException Any exceptions caught during processing will be
     *                         rethrown wrapped into a PropelException.
     */
    public function delete(ConnectionInterface $con = null)
    {
        if (null === $con) {
            $con = Propel::getServiceContainer()->getWriteConnection(AsscPiecesArchiveTableMap::DATABASE_NAME);
        }

        $criteria = $this;

        // Set the correct dbName
        $criteria->setDbName(AsscPiecesArchiveTableMap::DATABASE_NAME);

        // use transaction because $criteria could contain info
        // for more than one table or we could emulating ON DELETE CASCADE, etc.
        return $con->transaction(function () use ($con, $criteria) {
            $affectedRows = 0; // initialize var to track total num of affected rows

            AsscPiecesArchiveTableMap::removeInstanceFromPool($criteria);

            $affectedRows += ModelCriteria::delete($con);
            AsscPiecesArchiveTableMap::clearRelatedInstancePool();

            return $affectedRows;
        });
    }

} // AsscPiecesArchiveQuery
