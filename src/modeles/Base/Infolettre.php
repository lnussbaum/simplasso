<?php

namespace Base;

use \Infolettre as ChildInfolettre;
use \InfolettreInscrit as ChildInfolettreInscrit;
use \InfolettreInscritQuery as ChildInfolettreInscritQuery;
use \InfolettreQuery as ChildInfolettreQuery;
use \DateTime;
use \Exception;
use \PDO;
use Map\InfolettreInscritTableMap;
use Map\InfolettreTableMap;
use Propel\Runtime\Propel;
use Propel\Runtime\ActiveQuery\Criteria;
use Propel\Runtime\ActiveQuery\ModelCriteria;
use Propel\Runtime\ActiveRecord\ActiveRecordInterface;
use Propel\Runtime\Collection\Collection;
use Propel\Runtime\Collection\ObjectCollection;
use Propel\Runtime\Connection\ConnectionInterface;
use Propel\Runtime\Exception\BadMethodCallException;
use Propel\Runtime\Exception\LogicException;
use Propel\Runtime\Exception\PropelException;
use Propel\Runtime\Map\TableMap;
use Propel\Runtime\Parser\AbstractParser;
use Propel\Runtime\Util\PropelDateTime;

/**
 * Base class that represents a row from the 'com_infolettres' table.
 *
 *
 *
 * @package    propel.generator..Base
 */
abstract class Infolettre implements ActiveRecordInterface
{
    /**
     * TableMap class name
     */
    const TABLE_MAP = '\\Map\\InfolettreTableMap';


    /**
     * attribute to determine if this object has previously been saved.
     * @var boolean
     */
    protected $new = true;

    /**
     * attribute to determine whether this object has been deleted.
     * @var boolean
     */
    protected $deleted = false;

    /**
     * The columns that have been modified in current object.
     * Tracking modified columns allows us to only update modified columns.
     * @var array
     */
    protected $modifiedColumns = array();

    /**
     * The (virtual) columns that are added at runtime
     * The formatters can add supplementary columns based on a resultset
     * @var array
     */
    protected $virtualColumns = array();

    /**
     * The value for the id_infolettre field.
     *
     * @var        string
     */
    protected $id_infolettre;

    /**
     * The value for the identifiant field.
     *
     * @var        string
     */
    protected $identifiant;

    /**
     * The value for the nom field.
     *
     * @var        string
     */
    protected $nom;

    /**
     * The value for the descriptif field.
     *
     * @var        string
     */
    protected $descriptif;

    /**
     * The value for the automatique field.
     *
     * @var        boolean
     */
    protected $automatique;

    /**
     * The value for the date_synchro field.
     *
     * @var        DateTime
     */
    protected $date_synchro;

    /**
     * The value for the active field.
     *
     * @var        boolean
     */
    protected $active;

    /**
     * The value for the liaison_auto field.
     *
     * @var        boolean
     */
    protected $liaison_auto;

    /**
     * The value for the options field.
     *
     * @var        string
     */
    protected $options;

    /**
     * The value for the position field.
     *
     * Note: this column has a database default value of: 0
     * @var        int
     */
    protected $position;

    /**
     * The value for the created_at field.
     *
     * @var        DateTime
     */
    protected $created_at;

    /**
     * The value for the updated_at field.
     *
     * @var        DateTime
     */
    protected $updated_at;

    /**
     * @var        ObjectCollection|ChildInfolettreInscrit[] Collection to store aggregation of ChildInfolettreInscrit objects.
     */
    protected $collInfolettreInscrits;
    protected $collInfolettreInscritsPartial;

    /**
     * Flag to prevent endless save loop, if this object is referenced
     * by another object which falls in this transaction.
     *
     * @var boolean
     */
    protected $alreadyInSave = false;

    /**
     * An array of objects scheduled for deletion.
     * @var ObjectCollection|ChildInfolettreInscrit[]
     */
    protected $infolettreInscritsScheduledForDeletion = null;

    /**
     * Applies default values to this object.
     * This method should be called from the object's constructor (or
     * equivalent initialization method).
     * @see __construct()
     */
    public function applyDefaultValues()
    {
        $this->position = 0;
    }

    /**
     * Initializes internal state of Base\Infolettre object.
     * @see applyDefaults()
     */
    public function __construct()
    {
        $this->applyDefaultValues();
    }

    /**
     * Returns whether the object has been modified.
     *
     * @return boolean True if the object has been modified.
     */
    public function isModified()
    {
        return !!$this->modifiedColumns;
    }

    /**
     * Has specified column been modified?
     *
     * @param  string  $col column fully qualified name (TableMap::TYPE_COLNAME), e.g. Book::AUTHOR_ID
     * @return boolean True if $col has been modified.
     */
    public function isColumnModified($col)
    {
        return $this->modifiedColumns && isset($this->modifiedColumns[$col]);
    }

    /**
     * Get the columns that have been modified in this object.
     * @return array A unique list of the modified column names for this object.
     */
    public function getModifiedColumns()
    {
        return $this->modifiedColumns ? array_keys($this->modifiedColumns) : [];
    }

    /**
     * Returns whether the object has ever been saved.  This will
     * be false, if the object was retrieved from storage or was created
     * and then saved.
     *
     * @return boolean true, if the object has never been persisted.
     */
    public function isNew()
    {
        return $this->new;
    }

    /**
     * Setter for the isNew attribute.  This method will be called
     * by Propel-generated children and objects.
     *
     * @param boolean $b the state of the object.
     */
    public function setNew($b)
    {
        $this->new = (boolean) $b;
    }

    /**
     * Whether this object has been deleted.
     * @return boolean The deleted state of this object.
     */
    public function isDeleted()
    {
        return $this->deleted;
    }

    /**
     * Specify whether this object has been deleted.
     * @param  boolean $b The deleted state of this object.
     * @return void
     */
    public function setDeleted($b)
    {
        $this->deleted = (boolean) $b;
    }

    /**
     * Sets the modified state for the object to be false.
     * @param  string $col If supplied, only the specified column is reset.
     * @return void
     */
    public function resetModified($col = null)
    {
        if (null !== $col) {
            if (isset($this->modifiedColumns[$col])) {
                unset($this->modifiedColumns[$col]);
            }
        } else {
            $this->modifiedColumns = array();
        }
    }

    /**
     * Compares this with another <code>Infolettre</code> instance.  If
     * <code>obj</code> is an instance of <code>Infolettre</code>, delegates to
     * <code>equals(Infolettre)</code>.  Otherwise, returns <code>false</code>.
     *
     * @param  mixed   $obj The object to compare to.
     * @return boolean Whether equal to the object specified.
     */
    public function equals($obj)
    {
        if (!$obj instanceof static) {
            return false;
        }

        if ($this === $obj) {
            return true;
        }

        if (null === $this->getPrimaryKey() || null === $obj->getPrimaryKey()) {
            return false;
        }

        return $this->getPrimaryKey() === $obj->getPrimaryKey();
    }

    /**
     * Get the associative array of the virtual columns in this object
     *
     * @return array
     */
    public function getVirtualColumns()
    {
        return $this->virtualColumns;
    }

    /**
     * Checks the existence of a virtual column in this object
     *
     * @param  string  $name The virtual column name
     * @return boolean
     */
    public function hasVirtualColumn($name)
    {
        return array_key_exists($name, $this->virtualColumns);
    }

    /**
     * Get the value of a virtual column in this object
     *
     * @param  string $name The virtual column name
     * @return mixed
     *
     * @throws PropelException
     */
    public function getVirtualColumn($name)
    {
        if (!$this->hasVirtualColumn($name)) {
            throw new PropelException(sprintf('Cannot get value of inexistent virtual column %s.', $name));
        }

        return $this->virtualColumns[$name];
    }

    /**
     * Set the value of a virtual column in this object
     *
     * @param string $name  The virtual column name
     * @param mixed  $value The value to give to the virtual column
     *
     * @return $this|Infolettre The current object, for fluid interface
     */
    public function setVirtualColumn($name, $value)
    {
        $this->virtualColumns[$name] = $value;

        return $this;
    }

    /**
     * Logs a message using Propel::log().
     *
     * @param  string  $msg
     * @param  int     $priority One of the Propel::LOG_* logging levels
     * @return boolean
     */
    protected function log($msg, $priority = Propel::LOG_INFO)
    {
        return Propel::log(get_class($this) . ': ' . $msg, $priority);
    }

    /**
     * Export the current object properties to a string, using a given parser format
     * <code>
     * $book = BookQuery::create()->findPk(9012);
     * echo $book->exportTo('JSON');
     *  => {"Id":9012,"Title":"Don Juan","ISBN":"0140422161","Price":12.99,"PublisherId":1234,"AuthorId":5678}');
     * </code>
     *
     * @param  mixed   $parser                 A AbstractParser instance, or a format name ('XML', 'YAML', 'JSON', 'CSV')
     * @param  boolean $includeLazyLoadColumns (optional) Whether to include lazy load(ed) columns. Defaults to TRUE.
     * @return string  The exported data
     */
    public function exportTo($parser, $includeLazyLoadColumns = true)
    {
        if (!$parser instanceof AbstractParser) {
            $parser = AbstractParser::getParser($parser);
        }

        return $parser->fromArray($this->toArray(TableMap::TYPE_PHPNAME, $includeLazyLoadColumns, array(), true));
    }

    /**
     * Clean up internal collections prior to serializing
     * Avoids recursive loops that turn into segmentation faults when serializing
     */
    public function __sleep()
    {
        $this->clearAllReferences();

        $cls = new \ReflectionClass($this);
        $propertyNames = [];
        $serializableProperties = array_diff($cls->getProperties(), $cls->getProperties(\ReflectionProperty::IS_STATIC));

        foreach($serializableProperties as $property) {
            $propertyNames[] = $property->getName();
        }

        return $propertyNames;
    }

    /**
     * Get the [id_infolettre] column value.
     *
     * @return string
     */
    public function getIdInfolettre()
    {
        return $this->id_infolettre;
    }

    /**
     * Get the [identifiant] column value.
     *
     * @return string
     */
    public function getIdentifiant()
    {
        return $this->identifiant;
    }

    /**
     * Get the [nom] column value.
     *
     * @return string
     */
    public function getNom()
    {
        return $this->nom;
    }

    /**
     * Get the [descriptif] column value.
     *
     * @return string
     */
    public function getDescriptif()
    {
        return $this->descriptif;
    }

    /**
     * Get the [automatique] column value.
     *
     * @return boolean
     */
    public function getAutomatique()
    {
        return $this->automatique;
    }

    /**
     * Get the [automatique] column value.
     *
     * @return boolean
     */
    public function isAutomatique()
    {
        return $this->getAutomatique();
    }

    /**
     * Get the [optionally formatted] temporal [date_synchro] column value.
     *
     *
     * @param      string $format The date/time format string (either date()-style or strftime()-style).
     *                            If format is NULL, then the raw DateTime object will be returned.
     *
     * @return string|DateTime Formatted date/time value as string or DateTime object (if format is NULL), NULL if column is NULL, and 0 if column value is 0000-00-00 00:00:00
     *
     * @throws PropelException - if unable to parse/validate the date/time value.
     */
    public function getDateSynchro($format = NULL)
    {
        if ($format === null) {
            return $this->date_synchro;
        } else {
            return $this->date_synchro instanceof \DateTimeInterface ? $this->date_synchro->format($format) : null;
        }
    }

    /**
     * Get the [active] column value.
     *
     * @return boolean
     */
    public function getActive()
    {
        return $this->active;
    }

    /**
     * Get the [active] column value.
     *
     * @return boolean
     */
    public function isActive()
    {
        return $this->getActive();
    }

    /**
     * Get the [liaison_auto] column value.
     *
     * @return boolean
     */
    public function getLiaisonAuto()
    {
        return $this->liaison_auto;
    }

    /**
     * Get the [liaison_auto] column value.
     *
     * @return boolean
     */
    public function isLiaisonAuto()
    {
        return $this->getLiaisonAuto();
    }

    /**
     * Get the [options] column value.
     *
     * @return string
     */
    public function getOptions()
    {
        return $this->options;
    }

    /**
     * Get the [position] column value.
     *
     * @return int
     */
    public function getPosition()
    {
        return $this->position;
    }

    /**
     * Get the [optionally formatted] temporal [created_at] column value.
     *
     *
     * @param      string $format The date/time format string (either date()-style or strftime()-style).
     *                            If format is NULL, then the raw DateTime object will be returned.
     *
     * @return string|DateTime Formatted date/time value as string or DateTime object (if format is NULL), NULL if column is NULL, and 0 if column value is 0000-00-00 00:00:00
     *
     * @throws PropelException - if unable to parse/validate the date/time value.
     */
    public function getCreatedAt($format = NULL)
    {
        if ($format === null) {
            return $this->created_at;
        } else {
            return $this->created_at instanceof \DateTimeInterface ? $this->created_at->format($format) : null;
        }
    }

    /**
     * Get the [optionally formatted] temporal [updated_at] column value.
     *
     *
     * @param      string $format The date/time format string (either date()-style or strftime()-style).
     *                            If format is NULL, then the raw DateTime object will be returned.
     *
     * @return string|DateTime Formatted date/time value as string or DateTime object (if format is NULL), NULL if column is NULL, and 0 if column value is 0000-00-00 00:00:00
     *
     * @throws PropelException - if unable to parse/validate the date/time value.
     */
    public function getUpdatedAt($format = NULL)
    {
        if ($format === null) {
            return $this->updated_at;
        } else {
            return $this->updated_at instanceof \DateTimeInterface ? $this->updated_at->format($format) : null;
        }
    }

    /**
     * Set the value of [id_infolettre] column.
     *
     * @param string $v new value
     * @return $this|\Infolettre The current object (for fluent API support)
     */
    public function setIdInfolettre($v)
    {
        if ($v !== null) {
            $v = (string) $v;
        }

        if ($this->id_infolettre !== $v) {
            $this->id_infolettre = $v;
            $this->modifiedColumns[InfolettreTableMap::COL_ID_INFOLETTRE] = true;
        }

        return $this;
    } // setIdInfolettre()

    /**
     * Set the value of [identifiant] column.
     *
     * @param string $v new value
     * @return $this|\Infolettre The current object (for fluent API support)
     */
    public function setIdentifiant($v)
    {
        if ($v !== null) {
            $v = (string) $v;
        }

        if ($this->identifiant !== $v) {
            $this->identifiant = $v;
            $this->modifiedColumns[InfolettreTableMap::COL_IDENTIFIANT] = true;
        }

        return $this;
    } // setIdentifiant()

    /**
     * Set the value of [nom] column.
     *
     * @param string $v new value
     * @return $this|\Infolettre The current object (for fluent API support)
     */
    public function setNom($v)
    {
        if ($v !== null) {
            $v = (string) $v;
        }

        if ($this->nom !== $v) {
            $this->nom = $v;
            $this->modifiedColumns[InfolettreTableMap::COL_NOM] = true;
        }

        return $this;
    } // setNom()

    /**
     * Set the value of [descriptif] column.
     *
     * @param string $v new value
     * @return $this|\Infolettre The current object (for fluent API support)
     */
    public function setDescriptif($v)
    {
        if ($v !== null) {
            $v = (string) $v;
        }

        if ($this->descriptif !== $v) {
            $this->descriptif = $v;
            $this->modifiedColumns[InfolettreTableMap::COL_DESCRIPTIF] = true;
        }

        return $this;
    } // setDescriptif()

    /**
     * Sets the value of the [automatique] column.
     * Non-boolean arguments are converted using the following rules:
     *   * 1, '1', 'true',  'on',  and 'yes' are converted to boolean true
     *   * 0, '0', 'false', 'off', and 'no'  are converted to boolean false
     * Check on string values is case insensitive (so 'FaLsE' is seen as 'false').
     *
     * @param  boolean|integer|string $v The new value
     * @return $this|\Infolettre The current object (for fluent API support)
     */
    public function setAutomatique($v)
    {
        if ($v !== null) {
            if (is_string($v)) {
                $v = in_array(strtolower($v), array('false', 'off', '-', 'no', 'n', '0', '')) ? false : true;
            } else {
                $v = (boolean) $v;
            }
        }

        if ($this->automatique !== $v) {
            $this->automatique = $v;
            $this->modifiedColumns[InfolettreTableMap::COL_AUTOMATIQUE] = true;
        }

        return $this;
    } // setAutomatique()

    /**
     * Sets the value of [date_synchro] column to a normalized version of the date/time value specified.
     *
     * @param  mixed $v string, integer (timestamp), or \DateTimeInterface value.
     *               Empty strings are treated as NULL.
     * @return $this|\Infolettre The current object (for fluent API support)
     */
    public function setDateSynchro($v)
    {
        $dt = PropelDateTime::newInstance($v, null, 'DateTime');
        if ($this->date_synchro !== null || $dt !== null) {
            if ($this->date_synchro === null || $dt === null || $dt->format("Y-m-d H:i:s.u") !== $this->date_synchro->format("Y-m-d H:i:s.u")) {
                $this->date_synchro = $dt === null ? null : clone $dt;
                $this->modifiedColumns[InfolettreTableMap::COL_DATE_SYNCHRO] = true;
            }
        } // if either are not null

        return $this;
    } // setDateSynchro()

    /**
     * Sets the value of the [active] column.
     * Non-boolean arguments are converted using the following rules:
     *   * 1, '1', 'true',  'on',  and 'yes' are converted to boolean true
     *   * 0, '0', 'false', 'off', and 'no'  are converted to boolean false
     * Check on string values is case insensitive (so 'FaLsE' is seen as 'false').
     *
     * @param  boolean|integer|string $v The new value
     * @return $this|\Infolettre The current object (for fluent API support)
     */
    public function setActive($v)
    {
        if ($v !== null) {
            if (is_string($v)) {
                $v = in_array(strtolower($v), array('false', 'off', '-', 'no', 'n', '0', '')) ? false : true;
            } else {
                $v = (boolean) $v;
            }
        }

        if ($this->active !== $v) {
            $this->active = $v;
            $this->modifiedColumns[InfolettreTableMap::COL_ACTIVE] = true;
        }

        return $this;
    } // setActive()

    /**
     * Sets the value of the [liaison_auto] column.
     * Non-boolean arguments are converted using the following rules:
     *   * 1, '1', 'true',  'on',  and 'yes' are converted to boolean true
     *   * 0, '0', 'false', 'off', and 'no'  are converted to boolean false
     * Check on string values is case insensitive (so 'FaLsE' is seen as 'false').
     *
     * @param  boolean|integer|string $v The new value
     * @return $this|\Infolettre The current object (for fluent API support)
     */
    public function setLiaisonAuto($v)
    {
        if ($v !== null) {
            if (is_string($v)) {
                $v = in_array(strtolower($v), array('false', 'off', '-', 'no', 'n', '0', '')) ? false : true;
            } else {
                $v = (boolean) $v;
            }
        }

        if ($this->liaison_auto !== $v) {
            $this->liaison_auto = $v;
            $this->modifiedColumns[InfolettreTableMap::COL_LIAISON_AUTO] = true;
        }

        return $this;
    } // setLiaisonAuto()

    /**
     * Set the value of [options] column.
     *
     * @param string $v new value
     * @return $this|\Infolettre The current object (for fluent API support)
     */
    public function setOptions($v)
    {
        if ($v !== null) {
            $v = (string) $v;
        }

        if ($this->options !== $v) {
            $this->options = $v;
            $this->modifiedColumns[InfolettreTableMap::COL_OPTIONS] = true;
        }

        return $this;
    } // setOptions()

    /**
     * Set the value of [position] column.
     *
     * @param int $v new value
     * @return $this|\Infolettre The current object (for fluent API support)
     */
    public function setPosition($v)
    {
        if ($v !== null) {
            $v = (int) $v;
        }

        if ($this->position !== $v) {
            $this->position = $v;
            $this->modifiedColumns[InfolettreTableMap::COL_POSITION] = true;
        }

        return $this;
    } // setPosition()

    /**
     * Sets the value of [created_at] column to a normalized version of the date/time value specified.
     *
     * @param  mixed $v string, integer (timestamp), or \DateTimeInterface value.
     *               Empty strings are treated as NULL.
     * @return $this|\Infolettre The current object (for fluent API support)
     */
    public function setCreatedAt($v)
    {
        $dt = PropelDateTime::newInstance($v, null, 'DateTime');
        if ($this->created_at !== null || $dt !== null) {
            if ($this->created_at === null || $dt === null || $dt->format("Y-m-d H:i:s.u") !== $this->created_at->format("Y-m-d H:i:s.u")) {
                $this->created_at = $dt === null ? null : clone $dt;
                $this->modifiedColumns[InfolettreTableMap::COL_CREATED_AT] = true;
            }
        } // if either are not null

        return $this;
    } // setCreatedAt()

    /**
     * Sets the value of [updated_at] column to a normalized version of the date/time value specified.
     *
     * @param  mixed $v string, integer (timestamp), or \DateTimeInterface value.
     *               Empty strings are treated as NULL.
     * @return $this|\Infolettre The current object (for fluent API support)
     */
    public function setUpdatedAt($v)
    {
        $dt = PropelDateTime::newInstance($v, null, 'DateTime');
        if ($this->updated_at !== null || $dt !== null) {
            if ($this->updated_at === null || $dt === null || $dt->format("Y-m-d H:i:s.u") !== $this->updated_at->format("Y-m-d H:i:s.u")) {
                $this->updated_at = $dt === null ? null : clone $dt;
                $this->modifiedColumns[InfolettreTableMap::COL_UPDATED_AT] = true;
            }
        } // if either are not null

        return $this;
    } // setUpdatedAt()

    /**
     * Indicates whether the columns in this object are only set to default values.
     *
     * This method can be used in conjunction with isModified() to indicate whether an object is both
     * modified _and_ has some values set which are non-default.
     *
     * @return boolean Whether the columns in this object are only been set with default values.
     */
    public function hasOnlyDefaultValues()
    {
            if ($this->position !== 0) {
                return false;
            }

        // otherwise, everything was equal, so return TRUE
        return true;
    } // hasOnlyDefaultValues()

    /**
     * Hydrates (populates) the object variables with values from the database resultset.
     *
     * An offset (0-based "start column") is specified so that objects can be hydrated
     * with a subset of the columns in the resultset rows.  This is needed, for example,
     * for results of JOIN queries where the resultset row includes columns from two or
     * more tables.
     *
     * @param array   $row       The row returned by DataFetcher->fetch().
     * @param int     $startcol  0-based offset column which indicates which restultset column to start with.
     * @param boolean $rehydrate Whether this object is being re-hydrated from the database.
     * @param string  $indexType The index type of $row. Mostly DataFetcher->getIndexType().
                                  One of the class type constants TableMap::TYPE_PHPNAME, TableMap::TYPE_CAMELNAME
     *                            TableMap::TYPE_COLNAME, TableMap::TYPE_FIELDNAME, TableMap::TYPE_NUM.
     *
     * @return int             next starting column
     * @throws PropelException - Any caught Exception will be rewrapped as a PropelException.
     */
    public function hydrate($row, $startcol = 0, $rehydrate = false, $indexType = TableMap::TYPE_NUM)
    {
        try {

            $col = $row[TableMap::TYPE_NUM == $indexType ? 0 + $startcol : InfolettreTableMap::translateFieldName('IdInfolettre', TableMap::TYPE_PHPNAME, $indexType)];
            $this->id_infolettre = (null !== $col) ? (string) $col : null;

            $col = $row[TableMap::TYPE_NUM == $indexType ? 1 + $startcol : InfolettreTableMap::translateFieldName('Identifiant', TableMap::TYPE_PHPNAME, $indexType)];
            $this->identifiant = (null !== $col) ? (string) $col : null;

            $col = $row[TableMap::TYPE_NUM == $indexType ? 2 + $startcol : InfolettreTableMap::translateFieldName('Nom', TableMap::TYPE_PHPNAME, $indexType)];
            $this->nom = (null !== $col) ? (string) $col : null;

            $col = $row[TableMap::TYPE_NUM == $indexType ? 3 + $startcol : InfolettreTableMap::translateFieldName('Descriptif', TableMap::TYPE_PHPNAME, $indexType)];
            $this->descriptif = (null !== $col) ? (string) $col : null;

            $col = $row[TableMap::TYPE_NUM == $indexType ? 4 + $startcol : InfolettreTableMap::translateFieldName('Automatique', TableMap::TYPE_PHPNAME, $indexType)];
            $this->automatique = (null !== $col) ? (boolean) $col : null;

            $col = $row[TableMap::TYPE_NUM == $indexType ? 5 + $startcol : InfolettreTableMap::translateFieldName('DateSynchro', TableMap::TYPE_PHPNAME, $indexType)];
            if ($col === '0000-00-00 00:00:00') {
                $col = null;
            }
            $this->date_synchro = (null !== $col) ? PropelDateTime::newInstance($col, null, 'DateTime') : null;

            $col = $row[TableMap::TYPE_NUM == $indexType ? 6 + $startcol : InfolettreTableMap::translateFieldName('Active', TableMap::TYPE_PHPNAME, $indexType)];
            $this->active = (null !== $col) ? (boolean) $col : null;

            $col = $row[TableMap::TYPE_NUM == $indexType ? 7 + $startcol : InfolettreTableMap::translateFieldName('LiaisonAuto', TableMap::TYPE_PHPNAME, $indexType)];
            $this->liaison_auto = (null !== $col) ? (boolean) $col : null;

            $col = $row[TableMap::TYPE_NUM == $indexType ? 8 + $startcol : InfolettreTableMap::translateFieldName('Options', TableMap::TYPE_PHPNAME, $indexType)];
            $this->options = (null !== $col) ? (string) $col : null;

            $col = $row[TableMap::TYPE_NUM == $indexType ? 9 + $startcol : InfolettreTableMap::translateFieldName('Position', TableMap::TYPE_PHPNAME, $indexType)];
            $this->position = (null !== $col) ? (int) $col : null;

            $col = $row[TableMap::TYPE_NUM == $indexType ? 10 + $startcol : InfolettreTableMap::translateFieldName('CreatedAt', TableMap::TYPE_PHPNAME, $indexType)];
            if ($col === '0000-00-00 00:00:00') {
                $col = null;
            }
            $this->created_at = (null !== $col) ? PropelDateTime::newInstance($col, null, 'DateTime') : null;

            $col = $row[TableMap::TYPE_NUM == $indexType ? 11 + $startcol : InfolettreTableMap::translateFieldName('UpdatedAt', TableMap::TYPE_PHPNAME, $indexType)];
            if ($col === '0000-00-00 00:00:00') {
                $col = null;
            }
            $this->updated_at = (null !== $col) ? PropelDateTime::newInstance($col, null, 'DateTime') : null;
            $this->resetModified();

            $this->setNew(false);

            if ($rehydrate) {
                $this->ensureConsistency();
            }

            return $startcol + 12; // 12 = InfolettreTableMap::NUM_HYDRATE_COLUMNS.

        } catch (Exception $e) {
            throw new PropelException(sprintf('Error populating %s object', '\\Infolettre'), 0, $e);
        }
    }

    /**
     * Checks and repairs the internal consistency of the object.
     *
     * This method is executed after an already-instantiated object is re-hydrated
     * from the database.  It exists to check any foreign keys to make sure that
     * the objects related to the current object are correct based on foreign key.
     *
     * You can override this method in the stub class, but you should always invoke
     * the base method from the overridden method (i.e. parent::ensureConsistency()),
     * in case your model changes.
     *
     * @throws PropelException
     */
    public function ensureConsistency()
    {
    } // ensureConsistency

    /**
     * Reloads this object from datastore based on primary key and (optionally) resets all associated objects.
     *
     * This will only work if the object has been saved and has a valid primary key set.
     *
     * @param      boolean $deep (optional) Whether to also de-associated any related objects.
     * @param      ConnectionInterface $con (optional) The ConnectionInterface connection to use.
     * @return void
     * @throws PropelException - if this object is deleted, unsaved or doesn't have pk match in db
     */
    public function reload($deep = false, ConnectionInterface $con = null)
    {
        if ($this->isDeleted()) {
            throw new PropelException("Cannot reload a deleted object.");
        }

        if ($this->isNew()) {
            throw new PropelException("Cannot reload an unsaved object.");
        }

        if ($con === null) {
            $con = Propel::getServiceContainer()->getReadConnection(InfolettreTableMap::DATABASE_NAME);
        }

        // We don't need to alter the object instance pool; we're just modifying this instance
        // already in the pool.

        $dataFetcher = ChildInfolettreQuery::create(null, $this->buildPkeyCriteria())->setFormatter(ModelCriteria::FORMAT_STATEMENT)->find($con);
        $row = $dataFetcher->fetch();
        $dataFetcher->close();
        if (!$row) {
            throw new PropelException('Cannot find matching row in the database to reload object values.');
        }
        $this->hydrate($row, 0, true, $dataFetcher->getIndexType()); // rehydrate

        if ($deep) {  // also de-associate any related objects?

            $this->collInfolettreInscrits = null;

        } // if (deep)
    }

    /**
     * Removes this object from datastore and sets delete attribute.
     *
     * @param      ConnectionInterface $con
     * @return void
     * @throws PropelException
     * @see Infolettre::setDeleted()
     * @see Infolettre::isDeleted()
     */
    public function delete(ConnectionInterface $con = null)
    {
        if ($this->isDeleted()) {
            throw new PropelException("This object has already been deleted.");
        }

        if ($con === null) {
            $con = Propel::getServiceContainer()->getWriteConnection(InfolettreTableMap::DATABASE_NAME);
        }

        $con->transaction(function () use ($con) {
            $deleteQuery = ChildInfolettreQuery::create()
                ->filterByPrimaryKey($this->getPrimaryKey());
            $ret = $this->preDelete($con);
            if ($ret) {
                $deleteQuery->delete($con);
                $this->postDelete($con);
                $this->setDeleted(true);
            }
        });
    }

    /**
     * Persists this object to the database.
     *
     * If the object is new, it inserts it; otherwise an update is performed.
     * All modified related objects will also be persisted in the doSave()
     * method.  This method wraps all precipitate database operations in a
     * single transaction.
     *
     * @param      ConnectionInterface $con
     * @return int             The number of rows affected by this insert/update and any referring fk objects' save() operations.
     * @throws PropelException
     * @see doSave()
     */
    public function save(ConnectionInterface $con = null)
    {
        if ($this->isDeleted()) {
            throw new PropelException("You cannot save an object that has been deleted.");
        }

        if ($this->alreadyInSave) {
            return 0;
        }

        if ($con === null) {
            $con = Propel::getServiceContainer()->getWriteConnection(InfolettreTableMap::DATABASE_NAME);
        }

        return $con->transaction(function () use ($con) {
            $ret = $this->preSave($con);
            $isInsert = $this->isNew();
            if ($isInsert) {
                $ret = $ret && $this->preInsert($con);
                // timestampable behavior

                if (!$this->isColumnModified(InfolettreTableMap::COL_CREATED_AT)) {
                    $this->setCreatedAt(\Propel\Runtime\Util\PropelDateTime::createHighPrecision());
                }
                if (!$this->isColumnModified(InfolettreTableMap::COL_UPDATED_AT)) {
                    $this->setUpdatedAt(\Propel\Runtime\Util\PropelDateTime::createHighPrecision());
                }
            } else {
                $ret = $ret && $this->preUpdate($con);
                // timestampable behavior
                if ($this->isModified() && !$this->isColumnModified(InfolettreTableMap::COL_UPDATED_AT)) {
                    $this->setUpdatedAt(\Propel\Runtime\Util\PropelDateTime::createHighPrecision());
                }
            }
            if ($ret) {
                $affectedRows = $this->doSave($con);
                if ($isInsert) {
                    $this->postInsert($con);
                } else {
                    $this->postUpdate($con);
                }
                $this->postSave($con);
                InfolettreTableMap::addInstanceToPool($this);
            } else {
                $affectedRows = 0;
            }

            return $affectedRows;
        });
    }

    /**
     * Performs the work of inserting or updating the row in the database.
     *
     * If the object is new, it inserts it; otherwise an update is performed.
     * All related objects are also updated in this method.
     *
     * @param      ConnectionInterface $con
     * @return int             The number of rows affected by this insert/update and any referring fk objects' save() operations.
     * @throws PropelException
     * @see save()
     */
    protected function doSave(ConnectionInterface $con)
    {
        $affectedRows = 0; // initialize var to track total num of affected rows
        if (!$this->alreadyInSave) {
            $this->alreadyInSave = true;

            if ($this->isNew() || $this->isModified()) {
                // persist changes
                if ($this->isNew()) {
                    $this->doInsert($con);
                    $affectedRows += 1;
                } else {
                    $affectedRows += $this->doUpdate($con);
                }
                $this->resetModified();
            }

            if ($this->infolettreInscritsScheduledForDeletion !== null) {
                if (!$this->infolettreInscritsScheduledForDeletion->isEmpty()) {
                    \InfolettreInscritQuery::create()
                        ->filterByPrimaryKeys($this->infolettreInscritsScheduledForDeletion->getPrimaryKeys(false))
                        ->delete($con);
                    $this->infolettreInscritsScheduledForDeletion = null;
                }
            }

            if ($this->collInfolettreInscrits !== null) {
                foreach ($this->collInfolettreInscrits as $referrerFK) {
                    if (!$referrerFK->isDeleted() && ($referrerFK->isNew() || $referrerFK->isModified())) {
                        $affectedRows += $referrerFK->save($con);
                    }
                }
            }

            $this->alreadyInSave = false;

        }

        return $affectedRows;
    } // doSave()

    /**
     * Insert the row in the database.
     *
     * @param      ConnectionInterface $con
     *
     * @throws PropelException
     * @see doSave()
     */
    protected function doInsert(ConnectionInterface $con)
    {
        $modifiedColumns = array();
        $index = 0;

        $this->modifiedColumns[InfolettreTableMap::COL_ID_INFOLETTRE] = true;
        if (null !== $this->id_infolettre) {
            throw new PropelException('Cannot insert a value for auto-increment primary key (' . InfolettreTableMap::COL_ID_INFOLETTRE . ')');
        }

         // check the columns in natural order for more readable SQL queries
        if ($this->isColumnModified(InfolettreTableMap::COL_ID_INFOLETTRE)) {
            $modifiedColumns[':p' . $index++]  = '`id_infolettre`';
        }
        if ($this->isColumnModified(InfolettreTableMap::COL_IDENTIFIANT)) {
            $modifiedColumns[':p' . $index++]  = '`identifiant`';
        }
        if ($this->isColumnModified(InfolettreTableMap::COL_NOM)) {
            $modifiedColumns[':p' . $index++]  = '`nom`';
        }
        if ($this->isColumnModified(InfolettreTableMap::COL_DESCRIPTIF)) {
            $modifiedColumns[':p' . $index++]  = '`descriptif`';
        }
        if ($this->isColumnModified(InfolettreTableMap::COL_AUTOMATIQUE)) {
            $modifiedColumns[':p' . $index++]  = '`automatique`';
        }
        if ($this->isColumnModified(InfolettreTableMap::COL_DATE_SYNCHRO)) {
            $modifiedColumns[':p' . $index++]  = '`date_synchro`';
        }
        if ($this->isColumnModified(InfolettreTableMap::COL_ACTIVE)) {
            $modifiedColumns[':p' . $index++]  = '`active`';
        }
        if ($this->isColumnModified(InfolettreTableMap::COL_LIAISON_AUTO)) {
            $modifiedColumns[':p' . $index++]  = '`liaison_auto`';
        }
        if ($this->isColumnModified(InfolettreTableMap::COL_OPTIONS)) {
            $modifiedColumns[':p' . $index++]  = '`options`';
        }
        if ($this->isColumnModified(InfolettreTableMap::COL_POSITION)) {
            $modifiedColumns[':p' . $index++]  = '`position`';
        }
        if ($this->isColumnModified(InfolettreTableMap::COL_CREATED_AT)) {
            $modifiedColumns[':p' . $index++]  = '`created_at`';
        }
        if ($this->isColumnModified(InfolettreTableMap::COL_UPDATED_AT)) {
            $modifiedColumns[':p' . $index++]  = '`updated_at`';
        }

        $sql = sprintf(
            'INSERT INTO `com_infolettres` (%s) VALUES (%s)',
            implode(', ', $modifiedColumns),
            implode(', ', array_keys($modifiedColumns))
        );

        try {
            $stmt = $con->prepare($sql);
            foreach ($modifiedColumns as $identifier => $columnName) {
                switch ($columnName) {
                    case '`id_infolettre`':
                        $stmt->bindValue($identifier, $this->id_infolettre, PDO::PARAM_INT);
                        break;
                    case '`identifiant`':
                        $stmt->bindValue($identifier, $this->identifiant, PDO::PARAM_STR);
                        break;
                    case '`nom`':
                        $stmt->bindValue($identifier, $this->nom, PDO::PARAM_STR);
                        break;
                    case '`descriptif`':
                        $stmt->bindValue($identifier, $this->descriptif, PDO::PARAM_STR);
                        break;
                    case '`automatique`':
                        $stmt->bindValue($identifier, (int) $this->automatique, PDO::PARAM_INT);
                        break;
                    case '`date_synchro`':
                        $stmt->bindValue($identifier, $this->date_synchro ? $this->date_synchro->format("Y-m-d H:i:s.u") : null, PDO::PARAM_STR);
                        break;
                    case '`active`':
                        $stmt->bindValue($identifier, (int) $this->active, PDO::PARAM_INT);
                        break;
                    case '`liaison_auto`':
                        $stmt->bindValue($identifier, (int) $this->liaison_auto, PDO::PARAM_INT);
                        break;
                    case '`options`':
                        $stmt->bindValue($identifier, $this->options, PDO::PARAM_STR);
                        break;
                    case '`position`':
                        $stmt->bindValue($identifier, $this->position, PDO::PARAM_INT);
                        break;
                    case '`created_at`':
                        $stmt->bindValue($identifier, $this->created_at ? $this->created_at->format("Y-m-d H:i:s.u") : null, PDO::PARAM_STR);
                        break;
                    case '`updated_at`':
                        $stmt->bindValue($identifier, $this->updated_at ? $this->updated_at->format("Y-m-d H:i:s.u") : null, PDO::PARAM_STR);
                        break;
                }
            }
            $stmt->execute();
        } catch (Exception $e) {
            Propel::log($e->getMessage(), Propel::LOG_ERR);
            throw new PropelException(sprintf('Unable to execute INSERT statement [%s]', $sql), 0, $e);
        }

        try {
            $pk = $con->lastInsertId();
        } catch (Exception $e) {
            throw new PropelException('Unable to get autoincrement id.', 0, $e);
        }
        $this->setIdInfolettre($pk);

        $this->setNew(false);
    }

    /**
     * Update the row in the database.
     *
     * @param      ConnectionInterface $con
     *
     * @return Integer Number of updated rows
     * @see doSave()
     */
    protected function doUpdate(ConnectionInterface $con)
    {
        $selectCriteria = $this->buildPkeyCriteria();
        $valuesCriteria = $this->buildCriteria();

        return $selectCriteria->doUpdate($valuesCriteria, $con);
    }

    /**
     * Retrieves a field from the object by name passed in as a string.
     *
     * @param      string $name name
     * @param      string $type The type of fieldname the $name is of:
     *                     one of the class type constants TableMap::TYPE_PHPNAME, TableMap::TYPE_CAMELNAME
     *                     TableMap::TYPE_COLNAME, TableMap::TYPE_FIELDNAME, TableMap::TYPE_NUM.
     *                     Defaults to TableMap::TYPE_FIELDNAME.
     * @return mixed Value of field.
     */
    public function getByName($name, $type = TableMap::TYPE_FIELDNAME)
    {
        $pos = InfolettreTableMap::translateFieldName($name, $type, TableMap::TYPE_NUM);
        $field = $this->getByPosition($pos);

        return $field;
    }

    /**
     * Retrieves a field from the object by Position as specified in the xml schema.
     * Zero-based.
     *
     * @param      int $pos position in xml schema
     * @return mixed Value of field at $pos
     */
    public function getByPosition($pos)
    {
        switch ($pos) {
            case 0:
                return $this->getIdInfolettre();
                break;
            case 1:
                return $this->getIdentifiant();
                break;
            case 2:
                return $this->getNom();
                break;
            case 3:
                return $this->getDescriptif();
                break;
            case 4:
                return $this->getAutomatique();
                break;
            case 5:
                return $this->getDateSynchro();
                break;
            case 6:
                return $this->getActive();
                break;
            case 7:
                return $this->getLiaisonAuto();
                break;
            case 8:
                return $this->getOptions();
                break;
            case 9:
                return $this->getPosition();
                break;
            case 10:
                return $this->getCreatedAt();
                break;
            case 11:
                return $this->getUpdatedAt();
                break;
            default:
                return null;
                break;
        } // switch()
    }

    /**
     * Exports the object as an array.
     *
     * You can specify the key type of the array by passing one of the class
     * type constants.
     *
     * @param     string  $keyType (optional) One of the class type constants TableMap::TYPE_PHPNAME, TableMap::TYPE_CAMELNAME,
     *                    TableMap::TYPE_COLNAME, TableMap::TYPE_FIELDNAME, TableMap::TYPE_NUM.
     *                    Defaults to TableMap::TYPE_FIELDNAME.
     * @param     boolean $includeLazyLoadColumns (optional) Whether to include lazy loaded columns. Defaults to TRUE.
     * @param     array $alreadyDumpedObjects List of objects to skip to avoid recursion
     * @param     boolean $includeForeignObjects (optional) Whether to include hydrated related objects. Default to FALSE.
     *
     * @return array an associative array containing the field names (as keys) and field values
     */
    public function toArray($keyType = TableMap::TYPE_FIELDNAME, $includeLazyLoadColumns = true, $alreadyDumpedObjects = array(), $includeForeignObjects = false)
    {

        if (isset($alreadyDumpedObjects['Infolettre'][$this->hashCode()])) {
            return '*RECURSION*';
        }
        $alreadyDumpedObjects['Infolettre'][$this->hashCode()] = true;
        $keys = InfolettreTableMap::getFieldNames($keyType);
        $result = array(
            $keys[0] => $this->getIdInfolettre(),
            $keys[1] => $this->getIdentifiant(),
            $keys[2] => $this->getNom(),
            $keys[3] => $this->getDescriptif(),
            $keys[4] => $this->getAutomatique(),
            $keys[5] => $this->getDateSynchro(),
            $keys[6] => $this->getActive(),
            $keys[7] => $this->getLiaisonAuto(),
            $keys[8] => $this->getOptions(),
            $keys[9] => $this->getPosition(),
            $keys[10] => $this->getCreatedAt(),
            $keys[11] => $this->getUpdatedAt(),
        );
        if ($result[$keys[5]] instanceof \DateTimeInterface) {
            $result[$keys[5]] = $result[$keys[5]]->format('c');
        }

        if ($result[$keys[10]] instanceof \DateTimeInterface) {
            $result[$keys[10]] = $result[$keys[10]]->format('c');
        }

        if ($result[$keys[11]] instanceof \DateTimeInterface) {
            $result[$keys[11]] = $result[$keys[11]]->format('c');
        }

        $virtualColumns = $this->virtualColumns;
        foreach ($virtualColumns as $key => $virtualColumn) {
            $result[$key] = $virtualColumn;
        }

        if ($includeForeignObjects) {
            if (null !== $this->collInfolettreInscrits) {

                switch ($keyType) {
                    case TableMap::TYPE_CAMELNAME:
                        $key = 'infolettreInscrits';
                        break;
                    case TableMap::TYPE_FIELDNAME:
                        $key = 'com_infolettres_inscritss';
                        break;
                    default:
                        $key = 'InfolettreInscrits';
                }

                $result[$key] = $this->collInfolettreInscrits->toArray(null, false, $keyType, $includeLazyLoadColumns, $alreadyDumpedObjects);
            }
        }

        return $result;
    }

    /**
     * Sets a field from the object by name passed in as a string.
     *
     * @param  string $name
     * @param  mixed  $value field value
     * @param  string $type The type of fieldname the $name is of:
     *                one of the class type constants TableMap::TYPE_PHPNAME, TableMap::TYPE_CAMELNAME
     *                TableMap::TYPE_COLNAME, TableMap::TYPE_FIELDNAME, TableMap::TYPE_NUM.
     *                Defaults to TableMap::TYPE_FIELDNAME.
     * @return $this|\Infolettre
     */
    public function setByName($name, $value, $type = TableMap::TYPE_FIELDNAME)
    {
        $pos = InfolettreTableMap::translateFieldName($name, $type, TableMap::TYPE_NUM);

        return $this->setByPosition($pos, $value);
    }

    /**
     * Sets a field from the object by Position as specified in the xml schema.
     * Zero-based.
     *
     * @param  int $pos position in xml schema
     * @param  mixed $value field value
     * @return $this|\Infolettre
     */
    public function setByPosition($pos, $value)
    {
        switch ($pos) {
            case 0:
                $this->setIdInfolettre($value);
                break;
            case 1:
                $this->setIdentifiant($value);
                break;
            case 2:
                $this->setNom($value);
                break;
            case 3:
                $this->setDescriptif($value);
                break;
            case 4:
                $this->setAutomatique($value);
                break;
            case 5:
                $this->setDateSynchro($value);
                break;
            case 6:
                $this->setActive($value);
                break;
            case 7:
                $this->setLiaisonAuto($value);
                break;
            case 8:
                $this->setOptions($value);
                break;
            case 9:
                $this->setPosition($value);
                break;
            case 10:
                $this->setCreatedAt($value);
                break;
            case 11:
                $this->setUpdatedAt($value);
                break;
        } // switch()

        return $this;
    }

    /**
     * Populates the object using an array.
     *
     * This is particularly useful when populating an object from one of the
     * request arrays (e.g. $_POST).  This method goes through the column
     * names, checking to see whether a matching key exists in populated
     * array. If so the setByName() method is called for that column.
     *
     * You can specify the key type of the array by additionally passing one
     * of the class type constants TableMap::TYPE_PHPNAME, TableMap::TYPE_CAMELNAME,
     * TableMap::TYPE_COLNAME, TableMap::TYPE_FIELDNAME, TableMap::TYPE_NUM.
     * The default key type is the column's TableMap::TYPE_FIELDNAME.
     *
     * @param      array  $arr     An array to populate the object from.
     * @param      string $keyType The type of keys the array uses.
     * @return void
     */
    public function fromArray($arr, $keyType = TableMap::TYPE_FIELDNAME)
    {
        $keys = InfolettreTableMap::getFieldNames($keyType);

        if (array_key_exists($keys[0], $arr)) {
            $this->setIdInfolettre($arr[$keys[0]]);
        }
        if (array_key_exists($keys[1], $arr)) {
            $this->setIdentifiant($arr[$keys[1]]);
        }
        if (array_key_exists($keys[2], $arr)) {
            $this->setNom($arr[$keys[2]]);
        }
        if (array_key_exists($keys[3], $arr)) {
            $this->setDescriptif($arr[$keys[3]]);
        }
        if (array_key_exists($keys[4], $arr)) {
            $this->setAutomatique($arr[$keys[4]]);
        }
        if (array_key_exists($keys[5], $arr)) {
            $this->setDateSynchro($arr[$keys[5]]);
        }
        if (array_key_exists($keys[6], $arr)) {
            $this->setActive($arr[$keys[6]]);
        }
        if (array_key_exists($keys[7], $arr)) {
            $this->setLiaisonAuto($arr[$keys[7]]);
        }
        if (array_key_exists($keys[8], $arr)) {
            $this->setOptions($arr[$keys[8]]);
        }
        if (array_key_exists($keys[9], $arr)) {
            $this->setPosition($arr[$keys[9]]);
        }
        if (array_key_exists($keys[10], $arr)) {
            $this->setCreatedAt($arr[$keys[10]]);
        }
        if (array_key_exists($keys[11], $arr)) {
            $this->setUpdatedAt($arr[$keys[11]]);
        }
    }

     /**
     * Populate the current object from a string, using a given parser format
     * <code>
     * $book = new Book();
     * $book->importFrom('JSON', '{"Id":9012,"Title":"Don Juan","ISBN":"0140422161","Price":12.99,"PublisherId":1234,"AuthorId":5678}');
     * </code>
     *
     * You can specify the key type of the array by additionally passing one
     * of the class type constants TableMap::TYPE_PHPNAME, TableMap::TYPE_CAMELNAME,
     * TableMap::TYPE_COLNAME, TableMap::TYPE_FIELDNAME, TableMap::TYPE_NUM.
     * The default key type is the column's TableMap::TYPE_FIELDNAME.
     *
     * @param mixed $parser A AbstractParser instance,
     *                       or a format name ('XML', 'YAML', 'JSON', 'CSV')
     * @param string $data The source data to import from
     * @param string $keyType The type of keys the array uses.
     *
     * @return $this|\Infolettre The current object, for fluid interface
     */
    public function importFrom($parser, $data, $keyType = TableMap::TYPE_FIELDNAME)
    {
        if (!$parser instanceof AbstractParser) {
            $parser = AbstractParser::getParser($parser);
        }

        $this->fromArray($parser->toArray($data), $keyType);

        return $this;
    }

    /**
     * Build a Criteria object containing the values of all modified columns in this object.
     *
     * @return Criteria The Criteria object containing all modified values.
     */
    public function buildCriteria()
    {
        $criteria = new Criteria(InfolettreTableMap::DATABASE_NAME);

        if ($this->isColumnModified(InfolettreTableMap::COL_ID_INFOLETTRE)) {
            $criteria->add(InfolettreTableMap::COL_ID_INFOLETTRE, $this->id_infolettre);
        }
        if ($this->isColumnModified(InfolettreTableMap::COL_IDENTIFIANT)) {
            $criteria->add(InfolettreTableMap::COL_IDENTIFIANT, $this->identifiant);
        }
        if ($this->isColumnModified(InfolettreTableMap::COL_NOM)) {
            $criteria->add(InfolettreTableMap::COL_NOM, $this->nom);
        }
        if ($this->isColumnModified(InfolettreTableMap::COL_DESCRIPTIF)) {
            $criteria->add(InfolettreTableMap::COL_DESCRIPTIF, $this->descriptif);
        }
        if ($this->isColumnModified(InfolettreTableMap::COL_AUTOMATIQUE)) {
            $criteria->add(InfolettreTableMap::COL_AUTOMATIQUE, $this->automatique);
        }
        if ($this->isColumnModified(InfolettreTableMap::COL_DATE_SYNCHRO)) {
            $criteria->add(InfolettreTableMap::COL_DATE_SYNCHRO, $this->date_synchro);
        }
        if ($this->isColumnModified(InfolettreTableMap::COL_ACTIVE)) {
            $criteria->add(InfolettreTableMap::COL_ACTIVE, $this->active);
        }
        if ($this->isColumnModified(InfolettreTableMap::COL_LIAISON_AUTO)) {
            $criteria->add(InfolettreTableMap::COL_LIAISON_AUTO, $this->liaison_auto);
        }
        if ($this->isColumnModified(InfolettreTableMap::COL_OPTIONS)) {
            $criteria->add(InfolettreTableMap::COL_OPTIONS, $this->options);
        }
        if ($this->isColumnModified(InfolettreTableMap::COL_POSITION)) {
            $criteria->add(InfolettreTableMap::COL_POSITION, $this->position);
        }
        if ($this->isColumnModified(InfolettreTableMap::COL_CREATED_AT)) {
            $criteria->add(InfolettreTableMap::COL_CREATED_AT, $this->created_at);
        }
        if ($this->isColumnModified(InfolettreTableMap::COL_UPDATED_AT)) {
            $criteria->add(InfolettreTableMap::COL_UPDATED_AT, $this->updated_at);
        }

        return $criteria;
    }

    /**
     * Builds a Criteria object containing the primary key for this object.
     *
     * Unlike buildCriteria() this method includes the primary key values regardless
     * of whether or not they have been modified.
     *
     * @throws LogicException if no primary key is defined
     *
     * @return Criteria The Criteria object containing value(s) for primary key(s).
     */
    public function buildPkeyCriteria()
    {
        $criteria = ChildInfolettreQuery::create();
        $criteria->add(InfolettreTableMap::COL_ID_INFOLETTRE, $this->id_infolettre);

        return $criteria;
    }

    /**
     * If the primary key is not null, return the hashcode of the
     * primary key. Otherwise, return the hash code of the object.
     *
     * @return int Hashcode
     */
    public function hashCode()
    {
        $validPk = null !== $this->getIdInfolettre();

        $validPrimaryKeyFKs = 0;
        $primaryKeyFKs = [];

        if ($validPk) {
            return crc32(json_encode($this->getPrimaryKey(), JSON_UNESCAPED_UNICODE));
        } elseif ($validPrimaryKeyFKs) {
            return crc32(json_encode($primaryKeyFKs, JSON_UNESCAPED_UNICODE));
        }

        return spl_object_hash($this);
    }

    /**
     * Returns the primary key for this object (row).
     * @return string
     */
    public function getPrimaryKey()
    {
        return $this->getIdInfolettre();
    }

    /**
     * Generic method to set the primary key (id_infolettre column).
     *
     * @param       string $key Primary key.
     * @return void
     */
    public function setPrimaryKey($key)
    {
        $this->setIdInfolettre($key);
    }

    /**
     * Returns true if the primary key for this object is null.
     * @return boolean
     */
    public function isPrimaryKeyNull()
    {
        return null === $this->getIdInfolettre();
    }

    /**
     * Sets contents of passed object to values from current object.
     *
     * If desired, this method can also make copies of all associated (fkey referrers)
     * objects.
     *
     * @param      object $copyObj An object of \Infolettre (or compatible) type.
     * @param      boolean $deepCopy Whether to also copy all rows that refer (by fkey) to the current row.
     * @param      boolean $makeNew Whether to reset autoincrement PKs and make the object new.
     * @throws PropelException
     */
    public function copyInto($copyObj, $deepCopy = false, $makeNew = true)
    {
        $copyObj->setIdentifiant($this->getIdentifiant());
        $copyObj->setNom($this->getNom());
        $copyObj->setDescriptif($this->getDescriptif());
        $copyObj->setAutomatique($this->getAutomatique());
        $copyObj->setDateSynchro($this->getDateSynchro());
        $copyObj->setActive($this->getActive());
        $copyObj->setLiaisonAuto($this->getLiaisonAuto());
        $copyObj->setOptions($this->getOptions());
        $copyObj->setPosition($this->getPosition());
        $copyObj->setCreatedAt($this->getCreatedAt());
        $copyObj->setUpdatedAt($this->getUpdatedAt());

        if ($deepCopy) {
            // important: temporarily setNew(false) because this affects the behavior of
            // the getter/setter methods for fkey referrer objects.
            $copyObj->setNew(false);

            foreach ($this->getInfolettreInscrits() as $relObj) {
                if ($relObj !== $this) {  // ensure that we don't try to copy a reference to ourselves
                    $copyObj->addInfolettreInscrit($relObj->copy($deepCopy));
                }
            }

        } // if ($deepCopy)

        if ($makeNew) {
            $copyObj->setNew(true);
            $copyObj->setIdInfolettre(NULL); // this is a auto-increment column, so set to default value
        }
    }

    /**
     * Makes a copy of this object that will be inserted as a new row in table when saved.
     * It creates a new object filling in the simple attributes, but skipping any primary
     * keys that are defined for the table.
     *
     * If desired, this method can also make copies of all associated (fkey referrers)
     * objects.
     *
     * @param  boolean $deepCopy Whether to also copy all rows that refer (by fkey) to the current row.
     * @return \Infolettre Clone of current object.
     * @throws PropelException
     */
    public function copy($deepCopy = false)
    {
        // we use get_class(), because this might be a subclass
        $clazz = get_class($this);
        $copyObj = new $clazz();
        $this->copyInto($copyObj, $deepCopy);

        return $copyObj;
    }


    /**
     * Initializes a collection based on the name of a relation.
     * Avoids crafting an 'init[$relationName]s' method name
     * that wouldn't work when StandardEnglishPluralizer is used.
     *
     * @param      string $relationName The name of the relation to initialize
     * @return void
     */
    public function initRelation($relationName)
    {
        if ('InfolettreInscrit' == $relationName) {
            $this->initInfolettreInscrits();
            return;
        }
    }

    /**
     * Clears out the collInfolettreInscrits collection
     *
     * This does not modify the database; however, it will remove any associated objects, causing
     * them to be refetched by subsequent calls to accessor method.
     *
     * @return void
     * @see        addInfolettreInscrits()
     */
    public function clearInfolettreInscrits()
    {
        $this->collInfolettreInscrits = null; // important to set this to NULL since that means it is uninitialized
    }

    /**
     * Reset is the collInfolettreInscrits collection loaded partially.
     */
    public function resetPartialInfolettreInscrits($v = true)
    {
        $this->collInfolettreInscritsPartial = $v;
    }

    /**
     * Initializes the collInfolettreInscrits collection.
     *
     * By default this just sets the collInfolettreInscrits collection to an empty array (like clearcollInfolettreInscrits());
     * however, you may wish to override this method in your stub class to provide setting appropriate
     * to your application -- for example, setting the initial array to the values stored in database.
     *
     * @param      boolean $overrideExisting If set to true, the method call initializes
     *                                        the collection even if it is not empty
     *
     * @return void
     */
    public function initInfolettreInscrits($overrideExisting = true)
    {
        if (null !== $this->collInfolettreInscrits && !$overrideExisting) {
            return;
        }

        $collectionClassName = InfolettreInscritTableMap::getTableMap()->getCollectionClassName();

        $this->collInfolettreInscrits = new $collectionClassName;
        $this->collInfolettreInscrits->setModel('\InfolettreInscrit');
    }

    /**
     * Gets an array of ChildInfolettreInscrit objects which contain a foreign key that references this object.
     *
     * If the $criteria is not null, it is used to always fetch the results from the database.
     * Otherwise the results are fetched from the database the first time, then cached.
     * Next time the same method is called without $criteria, the cached collection is returned.
     * If this ChildInfolettre is new, it will return
     * an empty collection or the current collection; the criteria is ignored on a new object.
     *
     * @param      Criteria $criteria optional Criteria object to narrow the query
     * @param      ConnectionInterface $con optional connection object
     * @return ObjectCollection|ChildInfolettreInscrit[] List of ChildInfolettreInscrit objects
     * @throws PropelException
     */
    public function getInfolettreInscrits(Criteria $criteria = null, ConnectionInterface $con = null)
    {
        $partial = $this->collInfolettreInscritsPartial && !$this->isNew();
        if (null === $this->collInfolettreInscrits || null !== $criteria  || $partial) {
            if ($this->isNew() && null === $this->collInfolettreInscrits) {
                // return empty collection
                $this->initInfolettreInscrits();
            } else {
                $collInfolettreInscrits = ChildInfolettreInscritQuery::create(null, $criteria)
                    ->filterByInfolettre($this)
                    ->find($con);

                if (null !== $criteria) {
                    if (false !== $this->collInfolettreInscritsPartial && count($collInfolettreInscrits)) {
                        $this->initInfolettreInscrits(false);

                        foreach ($collInfolettreInscrits as $obj) {
                            if (false == $this->collInfolettreInscrits->contains($obj)) {
                                $this->collInfolettreInscrits->append($obj);
                            }
                        }

                        $this->collInfolettreInscritsPartial = true;
                    }

                    return $collInfolettreInscrits;
                }

                if ($partial && $this->collInfolettreInscrits) {
                    foreach ($this->collInfolettreInscrits as $obj) {
                        if ($obj->isNew()) {
                            $collInfolettreInscrits[] = $obj;
                        }
                    }
                }

                $this->collInfolettreInscrits = $collInfolettreInscrits;
                $this->collInfolettreInscritsPartial = false;
            }
        }

        return $this->collInfolettreInscrits;
    }

    /**
     * Sets a collection of ChildInfolettreInscrit objects related by a one-to-many relationship
     * to the current object.
     * It will also schedule objects for deletion based on a diff between old objects (aka persisted)
     * and new objects from the given Propel collection.
     *
     * @param      Collection $infolettreInscrits A Propel collection.
     * @param      ConnectionInterface $con Optional connection object
     * @return $this|ChildInfolettre The current object (for fluent API support)
     */
    public function setInfolettreInscrits(Collection $infolettreInscrits, ConnectionInterface $con = null)
    {
        /** @var ChildInfolettreInscrit[] $infolettreInscritsToDelete */
        $infolettreInscritsToDelete = $this->getInfolettreInscrits(new Criteria(), $con)->diff($infolettreInscrits);


        $this->infolettreInscritsScheduledForDeletion = $infolettreInscritsToDelete;

        foreach ($infolettreInscritsToDelete as $infolettreInscritRemoved) {
            $infolettreInscritRemoved->setInfolettre(null);
        }

        $this->collInfolettreInscrits = null;
        foreach ($infolettreInscrits as $infolettreInscrit) {
            $this->addInfolettreInscrit($infolettreInscrit);
        }

        $this->collInfolettreInscrits = $infolettreInscrits;
        $this->collInfolettreInscritsPartial = false;

        return $this;
    }

    /**
     * Returns the number of related InfolettreInscrit objects.
     *
     * @param      Criteria $criteria
     * @param      boolean $distinct
     * @param      ConnectionInterface $con
     * @return int             Count of related InfolettreInscrit objects.
     * @throws PropelException
     */
    public function countInfolettreInscrits(Criteria $criteria = null, $distinct = false, ConnectionInterface $con = null)
    {
        $partial = $this->collInfolettreInscritsPartial && !$this->isNew();
        if (null === $this->collInfolettreInscrits || null !== $criteria || $partial) {
            if ($this->isNew() && null === $this->collInfolettreInscrits) {
                return 0;
            }

            if ($partial && !$criteria) {
                return count($this->getInfolettreInscrits());
            }

            $query = ChildInfolettreInscritQuery::create(null, $criteria);
            if ($distinct) {
                $query->distinct();
            }

            return $query
                ->filterByInfolettre($this)
                ->count($con);
        }

        return count($this->collInfolettreInscrits);
    }

    /**
     * Method called to associate a ChildInfolettreInscrit object to this object
     * through the ChildInfolettreInscrit foreign key attribute.
     *
     * @param  ChildInfolettreInscrit $l ChildInfolettreInscrit
     * @return $this|\Infolettre The current object (for fluent API support)
     */
    public function addInfolettreInscrit(ChildInfolettreInscrit $l)
    {
        if ($this->collInfolettreInscrits === null) {
            $this->initInfolettreInscrits();
            $this->collInfolettreInscritsPartial = true;
        }

        if (!$this->collInfolettreInscrits->contains($l)) {
            $this->doAddInfolettreInscrit($l);

            if ($this->infolettreInscritsScheduledForDeletion and $this->infolettreInscritsScheduledForDeletion->contains($l)) {
                $this->infolettreInscritsScheduledForDeletion->remove($this->infolettreInscritsScheduledForDeletion->search($l));
            }
        }

        return $this;
    }

    /**
     * @param ChildInfolettreInscrit $infolettreInscrit The ChildInfolettreInscrit object to add.
     */
    protected function doAddInfolettreInscrit(ChildInfolettreInscrit $infolettreInscrit)
    {
        $this->collInfolettreInscrits[]= $infolettreInscrit;
        $infolettreInscrit->setInfolettre($this);
    }

    /**
     * @param  ChildInfolettreInscrit $infolettreInscrit The ChildInfolettreInscrit object to remove.
     * @return $this|ChildInfolettre The current object (for fluent API support)
     */
    public function removeInfolettreInscrit(ChildInfolettreInscrit $infolettreInscrit)
    {
        if ($this->getInfolettreInscrits()->contains($infolettreInscrit)) {
            $pos = $this->collInfolettreInscrits->search($infolettreInscrit);
            $this->collInfolettreInscrits->remove($pos);
            if (null === $this->infolettreInscritsScheduledForDeletion) {
                $this->infolettreInscritsScheduledForDeletion = clone $this->collInfolettreInscrits;
                $this->infolettreInscritsScheduledForDeletion->clear();
            }
            $this->infolettreInscritsScheduledForDeletion[]= clone $infolettreInscrit;
            $infolettreInscrit->setInfolettre(null);
        }

        return $this;
    }

    /**
     * Clears the current object, sets all attributes to their default values and removes
     * outgoing references as well as back-references (from other objects to this one. Results probably in a database
     * change of those foreign objects when you call `save` there).
     */
    public function clear()
    {
        $this->id_infolettre = null;
        $this->identifiant = null;
        $this->nom = null;
        $this->descriptif = null;
        $this->automatique = null;
        $this->date_synchro = null;
        $this->active = null;
        $this->liaison_auto = null;
        $this->options = null;
        $this->position = null;
        $this->created_at = null;
        $this->updated_at = null;
        $this->alreadyInSave = false;
        $this->clearAllReferences();
        $this->applyDefaultValues();
        $this->resetModified();
        $this->setNew(true);
        $this->setDeleted(false);
    }

    /**
     * Resets all references and back-references to other model objects or collections of model objects.
     *
     * This method is used to reset all php object references (not the actual reference in the database).
     * Necessary for object serialisation.
     *
     * @param      boolean $deep Whether to also clear the references on all referrer objects.
     */
    public function clearAllReferences($deep = false)
    {
        if ($deep) {
            if ($this->collInfolettreInscrits) {
                foreach ($this->collInfolettreInscrits as $o) {
                    $o->clearAllReferences($deep);
                }
            }
        } // if ($deep)

        $this->collInfolettreInscrits = null;
    }

    /**
     * Return the string representation of this object
     *
     * @return string
     */
    public function __toString()
    {
        return (string) $this->exportTo(InfolettreTableMap::DEFAULT_STRING_FORMAT);
    }

    // timestampable behavior

    /**
     * Mark the current object so that the update date doesn't get updated during next save
     *
     * @return     $this|ChildInfolettre The current object (for fluent API support)
     */
    public function keepUpdateDateUnchanged()
    {
        $this->modifiedColumns[InfolettreTableMap::COL_UPDATED_AT] = true;

        return $this;
    }

    /**
     * Code to be run before persisting the object
     * @param  ConnectionInterface $con
     * @return boolean
     */
    public function preSave(ConnectionInterface $con = null)
    {
        if (is_callable('parent::preSave')) {
            return parent::preSave($con);
        }
        return true;
    }

    /**
     * Code to be run after persisting the object
     * @param ConnectionInterface $con
     */
    public function postSave(ConnectionInterface $con = null)
    {
        if (is_callable('parent::postSave')) {
            parent::postSave($con);
        }
    }

    /**
     * Code to be run before inserting to database
     * @param  ConnectionInterface $con
     * @return boolean
     */
    public function preInsert(ConnectionInterface $con = null)
    {
        if (is_callable('parent::preInsert')) {
            return parent::preInsert($con);
        }
        return true;
    }

    /**
     * Code to be run after inserting to database
     * @param ConnectionInterface $con
     */
    public function postInsert(ConnectionInterface $con = null)
    {
        if (is_callable('parent::postInsert')) {
            parent::postInsert($con);
        }
    }

    /**
     * Code to be run before updating the object in database
     * @param  ConnectionInterface $con
     * @return boolean
     */
    public function preUpdate(ConnectionInterface $con = null)
    {
        if (is_callable('parent::preUpdate')) {
            return parent::preUpdate($con);
        }
        return true;
    }

    /**
     * Code to be run after updating the object in database
     * @param ConnectionInterface $con
     */
    public function postUpdate(ConnectionInterface $con = null)
    {
        if (is_callable('parent::postUpdate')) {
            parent::postUpdate($con);
        }
    }

    /**
     * Code to be run before deleting the object in database
     * @param  ConnectionInterface $con
     * @return boolean
     */
    public function preDelete(ConnectionInterface $con = null)
    {
        if (is_callable('parent::preDelete')) {
            return parent::preDelete($con);
        }
        return true;
    }

    /**
     * Code to be run after deleting the object in database
     * @param ConnectionInterface $con
     */
    public function postDelete(ConnectionInterface $con = null)
    {
        if (is_callable('parent::postDelete')) {
            parent::postDelete($con);
        }
    }


    /**
     * Derived method to catches calls to undefined methods.
     *
     * Provides magic import/export method support (fromXML()/toXML(), fromYAML()/toYAML(), etc.).
     * Allows to define default __call() behavior if you overwrite __call()
     *
     * @param string $name
     * @param mixed  $params
     *
     * @return array|string
     */
    public function __call($name, $params)
    {
        if (0 === strpos($name, 'get')) {
            $virtualColumn = substr($name, 3);
            if ($this->hasVirtualColumn($virtualColumn)) {
                return $this->getVirtualColumn($virtualColumn);
            }

            $virtualColumn = lcfirst($virtualColumn);
            if ($this->hasVirtualColumn($virtualColumn)) {
                return $this->getVirtualColumn($virtualColumn);
            }
        }

        if (0 === strpos($name, 'from')) {
            $format = substr($name, 4);

            return $this->importFrom($format, reset($params));
        }

        if (0 === strpos($name, 'to')) {
            $format = substr($name, 2);
            $includeLazyLoadColumns = isset($params[0]) ? $params[0] : true;

            return $this->exportTo($format, $includeLazyLoadColumns);
        }

        throw new BadMethodCallException(sprintf('Call to undefined method: %s.', $name));
    }

}
