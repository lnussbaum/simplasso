<?php

namespace Base;

use \Individu as ChildIndividu;
use \IndividuQuery as ChildIndividuQuery;
use \Log as ChildLog;
use \LogLien as ChildLogLien;
use \LogLienQuery as ChildLogLienQuery;
use \LogQuery as ChildLogQuery;
use \DateTime;
use \Exception;
use \PDO;
use Map\LogLienTableMap;
use Map\LogTableMap;
use Propel\Runtime\Propel;
use Propel\Runtime\ActiveQuery\Criteria;
use Propel\Runtime\ActiveQuery\ModelCriteria;
use Propel\Runtime\ActiveRecord\ActiveRecordInterface;
use Propel\Runtime\Collection\Collection;
use Propel\Runtime\Collection\ObjectCollection;
use Propel\Runtime\Connection\ConnectionInterface;
use Propel\Runtime\Exception\BadMethodCallException;
use Propel\Runtime\Exception\LogicException;
use Propel\Runtime\Exception\PropelException;
use Propel\Runtime\Map\TableMap;
use Propel\Runtime\Parser\AbstractParser;
use Propel\Runtime\Util\PropelDateTime;

/**
 * Base class that represents a row from the 'asso_logs' table.
 *
 *
 *
 * @package    propel.generator..Base
 */
abstract class Log implements ActiveRecordInterface
{
    /**
     * TableMap class name
     */
    const TABLE_MAP = '\\Map\\LogTableMap';


    /**
     * attribute to determine if this object has previously been saved.
     * @var boolean
     */
    protected $new = true;

    /**
     * attribute to determine whether this object has been deleted.
     * @var boolean
     */
    protected $deleted = false;

    /**
     * The columns that have been modified in current object.
     * Tracking modified columns allows us to only update modified columns.
     * @var array
     */
    protected $modifiedColumns = array();

    /**
     * The (virtual) columns that are added at runtime
     * The formatters can add supplementary columns based on a resultset
     * @var array
     */
    protected $virtualColumns = array();

    /**
     * The value for the id_log field.
     * identifiant du log
     * @var        string
     */
    protected $id_log;

    /**
     * The value for the id_individu field.
     * identifiant de l individu
     * @var        string
     */
    protected $id_individu;

    /**
     * The value for the id_entite field.
     * identifiant de l entité
     * @var        string
     */
    protected $id_entite;

    /**
     * The value for the code field.
     * description
     * @var        string
     */
    protected $code;

    /**
     * The value for the variables field.
     *
     * @var        string
     */
    protected $variables;

    /**
     * The value for the date_operation field.
     * Date de l operation
     * @var        DateTime
     */
    protected $date_operation;

    /**
     * The value for the observation field.
     * Observation
     * @var        string
     */
    protected $observation;

    /**
     * @var        ChildIndividu
     */
    protected $aIndividu;

    /**
     * @var        ObjectCollection|ChildLogLien[] Collection to store aggregation of ChildLogLien objects.
     */
    protected $collLogLiens;
    protected $collLogLiensPartial;

    /**
     * Flag to prevent endless save loop, if this object is referenced
     * by another object which falls in this transaction.
     *
     * @var boolean
     */
    protected $alreadyInSave = false;

    /**
     * An array of objects scheduled for deletion.
     * @var ObjectCollection|ChildLogLien[]
     */
    protected $logLiensScheduledForDeletion = null;

    /**
     * Initializes internal state of Base\Log object.
     */
    public function __construct()
    {
    }

    /**
     * Returns whether the object has been modified.
     *
     * @return boolean True if the object has been modified.
     */
    public function isModified()
    {
        return !!$this->modifiedColumns;
    }

    /**
     * Has specified column been modified?
     *
     * @param  string  $col column fully qualified name (TableMap::TYPE_COLNAME), e.g. Book::AUTHOR_ID
     * @return boolean True if $col has been modified.
     */
    public function isColumnModified($col)
    {
        return $this->modifiedColumns && isset($this->modifiedColumns[$col]);
    }

    /**
     * Get the columns that have been modified in this object.
     * @return array A unique list of the modified column names for this object.
     */
    public function getModifiedColumns()
    {
        return $this->modifiedColumns ? array_keys($this->modifiedColumns) : [];
    }

    /**
     * Returns whether the object has ever been saved.  This will
     * be false, if the object was retrieved from storage or was created
     * and then saved.
     *
     * @return boolean true, if the object has never been persisted.
     */
    public function isNew()
    {
        return $this->new;
    }

    /**
     * Setter for the isNew attribute.  This method will be called
     * by Propel-generated children and objects.
     *
     * @param boolean $b the state of the object.
     */
    public function setNew($b)
    {
        $this->new = (boolean) $b;
    }

    /**
     * Whether this object has been deleted.
     * @return boolean The deleted state of this object.
     */
    public function isDeleted()
    {
        return $this->deleted;
    }

    /**
     * Specify whether this object has been deleted.
     * @param  boolean $b The deleted state of this object.
     * @return void
     */
    public function setDeleted($b)
    {
        $this->deleted = (boolean) $b;
    }

    /**
     * Sets the modified state for the object to be false.
     * @param  string $col If supplied, only the specified column is reset.
     * @return void
     */
    public function resetModified($col = null)
    {
        if (null !== $col) {
            if (isset($this->modifiedColumns[$col])) {
                unset($this->modifiedColumns[$col]);
            }
        } else {
            $this->modifiedColumns = array();
        }
    }

    /**
     * Compares this with another <code>Log</code> instance.  If
     * <code>obj</code> is an instance of <code>Log</code>, delegates to
     * <code>equals(Log)</code>.  Otherwise, returns <code>false</code>.
     *
     * @param  mixed   $obj The object to compare to.
     * @return boolean Whether equal to the object specified.
     */
    public function equals($obj)
    {
        if (!$obj instanceof static) {
            return false;
        }

        if ($this === $obj) {
            return true;
        }

        if (null === $this->getPrimaryKey() || null === $obj->getPrimaryKey()) {
            return false;
        }

        return $this->getPrimaryKey() === $obj->getPrimaryKey();
    }

    /**
     * Get the associative array of the virtual columns in this object
     *
     * @return array
     */
    public function getVirtualColumns()
    {
        return $this->virtualColumns;
    }

    /**
     * Checks the existence of a virtual column in this object
     *
     * @param  string  $name The virtual column name
     * @return boolean
     */
    public function hasVirtualColumn($name)
    {
        return array_key_exists($name, $this->virtualColumns);
    }

    /**
     * Get the value of a virtual column in this object
     *
     * @param  string $name The virtual column name
     * @return mixed
     *
     * @throws PropelException
     */
    public function getVirtualColumn($name)
    {
        if (!$this->hasVirtualColumn($name)) {
            throw new PropelException(sprintf('Cannot get value of inexistent virtual column %s.', $name));
        }

        return $this->virtualColumns[$name];
    }

    /**
     * Set the value of a virtual column in this object
     *
     * @param string $name  The virtual column name
     * @param mixed  $value The value to give to the virtual column
     *
     * @return $this|Log The current object, for fluid interface
     */
    public function setVirtualColumn($name, $value)
    {
        $this->virtualColumns[$name] = $value;

        return $this;
    }

    /**
     * Logs a message using Propel::log().
     *
     * @param  string  $msg
     * @param  int     $priority One of the Propel::LOG_* logging levels
     * @return boolean
     */
    protected function log($msg, $priority = Propel::LOG_INFO)
    {
        return Propel::log(get_class($this) . ': ' . $msg, $priority);
    }

    /**
     * Export the current object properties to a string, using a given parser format
     * <code>
     * $book = BookQuery::create()->findPk(9012);
     * echo $book->exportTo('JSON');
     *  => {"Id":9012,"Title":"Don Juan","ISBN":"0140422161","Price":12.99,"PublisherId":1234,"AuthorId":5678}');
     * </code>
     *
     * @param  mixed   $parser                 A AbstractParser instance, or a format name ('XML', 'YAML', 'JSON', 'CSV')
     * @param  boolean $includeLazyLoadColumns (optional) Whether to include lazy load(ed) columns. Defaults to TRUE.
     * @return string  The exported data
     */
    public function exportTo($parser, $includeLazyLoadColumns = true)
    {
        if (!$parser instanceof AbstractParser) {
            $parser = AbstractParser::getParser($parser);
        }

        return $parser->fromArray($this->toArray(TableMap::TYPE_PHPNAME, $includeLazyLoadColumns, array(), true));
    }

    /**
     * Clean up internal collections prior to serializing
     * Avoids recursive loops that turn into segmentation faults when serializing
     */
    public function __sleep()
    {
        $this->clearAllReferences();

        $cls = new \ReflectionClass($this);
        $propertyNames = [];
        $serializableProperties = array_diff($cls->getProperties(), $cls->getProperties(\ReflectionProperty::IS_STATIC));

        foreach($serializableProperties as $property) {
            $propertyNames[] = $property->getName();
        }

        return $propertyNames;
    }

    /**
     * Get the [id_log] column value.
     * identifiant du log
     * @return string
     */
    public function getIdLog()
    {
        return $this->id_log;
    }

    /**
     * Get the [id_individu] column value.
     * identifiant de l individu
     * @return string
     */
    public function getIdIndividu()
    {
        return $this->id_individu;
    }

    /**
     * Get the [id_entite] column value.
     * identifiant de l entité
     * @return string
     */
    public function getIdEntite()
    {
        return $this->id_entite;
    }

    /**
     * Get the [code] column value.
     * description
     * @return string
     */
    public function getCode()
    {
        return $this->code;
    }

    /**
     * Get the [variables] column value.
     *
     * @return string
     */
    public function getVariables()
    {
        return $this->variables;
    }

    /**
     * Get the [optionally formatted] temporal [date_operation] column value.
     * Date de l operation
     *
     * @param      string $format The date/time format string (either date()-style or strftime()-style).
     *                            If format is NULL, then the raw DateTime object will be returned.
     *
     * @return string|DateTime Formatted date/time value as string or DateTime object (if format is NULL), NULL if column is NULL, and 0 if column value is 0000-00-00 00:00:00
     *
     * @throws PropelException - if unable to parse/validate the date/time value.
     */
    public function getDateOperation($format = NULL)
    {
        if ($format === null) {
            return $this->date_operation;
        } else {
            return $this->date_operation instanceof \DateTimeInterface ? $this->date_operation->format($format) : null;
        }
    }

    /**
     * Get the [observation] column value.
     * Observation
     * @return string
     */
    public function getObservation()
    {
        return $this->observation;
    }

    /**
     * Set the value of [id_log] column.
     * identifiant du log
     * @param string $v new value
     * @return $this|\Log The current object (for fluent API support)
     */
    public function setIdLog($v)
    {
        if ($v !== null) {
            $v = (string) $v;
        }

        if ($this->id_log !== $v) {
            $this->id_log = $v;
            $this->modifiedColumns[LogTableMap::COL_ID_LOG] = true;
        }

        return $this;
    } // setIdLog()

    /**
     * Set the value of [id_individu] column.
     * identifiant de l individu
     * @param string $v new value
     * @return $this|\Log The current object (for fluent API support)
     */
    public function setIdIndividu($v)
    {
        if ($v !== null) {
            $v = (string) $v;
        }

        if ($this->id_individu !== $v) {
            $this->id_individu = $v;
            $this->modifiedColumns[LogTableMap::COL_ID_INDIVIDU] = true;
        }

        if ($this->aIndividu !== null && $this->aIndividu->getIdIndividu() !== $v) {
            $this->aIndividu = null;
        }

        return $this;
    } // setIdIndividu()

    /**
     * Set the value of [id_entite] column.
     * identifiant de l entité
     * @param string $v new value
     * @return $this|\Log The current object (for fluent API support)
     */
    public function setIdEntite($v)
    {
        if ($v !== null) {
            $v = (string) $v;
        }

        if ($this->id_entite !== $v) {
            $this->id_entite = $v;
            $this->modifiedColumns[LogTableMap::COL_ID_ENTITE] = true;
        }

        return $this;
    } // setIdEntite()

    /**
     * Set the value of [code] column.
     * description
     * @param string $v new value
     * @return $this|\Log The current object (for fluent API support)
     */
    public function setCode($v)
    {
        if ($v !== null) {
            $v = (string) $v;
        }

        if ($this->code !== $v) {
            $this->code = $v;
            $this->modifiedColumns[LogTableMap::COL_CODE] = true;
        }

        return $this;
    } // setCode()

    /**
     * Set the value of [variables] column.
     *
     * @param string $v new value
     * @return $this|\Log The current object (for fluent API support)
     */
    public function setVariables($v)
    {
        if ($v !== null) {
            $v = (string) $v;
        }

        if ($this->variables !== $v) {
            $this->variables = $v;
            $this->modifiedColumns[LogTableMap::COL_VARIABLES] = true;
        }

        return $this;
    } // setVariables()

    /**
     * Sets the value of [date_operation] column to a normalized version of the date/time value specified.
     * Date de l operation
     * @param  mixed $v string, integer (timestamp), or \DateTimeInterface value.
     *               Empty strings are treated as NULL.
     * @return $this|\Log The current object (for fluent API support)
     */
    public function setDateOperation($v)
    {
        $dt = PropelDateTime::newInstance($v, null, 'DateTime');
        if ($this->date_operation !== null || $dt !== null) {
            if ($this->date_operation === null || $dt === null || $dt->format("Y-m-d H:i:s.u") !== $this->date_operation->format("Y-m-d H:i:s.u")) {
                $this->date_operation = $dt === null ? null : clone $dt;
                $this->modifiedColumns[LogTableMap::COL_DATE_OPERATION] = true;
            }
        } // if either are not null

        return $this;
    } // setDateOperation()

    /**
     * Set the value of [observation] column.
     * Observation
     * @param string $v new value
     * @return $this|\Log The current object (for fluent API support)
     */
    public function setObservation($v)
    {
        if ($v !== null) {
            $v = (string) $v;
        }

        if ($this->observation !== $v) {
            $this->observation = $v;
            $this->modifiedColumns[LogTableMap::COL_OBSERVATION] = true;
        }

        return $this;
    } // setObservation()

    /**
     * Indicates whether the columns in this object are only set to default values.
     *
     * This method can be used in conjunction with isModified() to indicate whether an object is both
     * modified _and_ has some values set which are non-default.
     *
     * @return boolean Whether the columns in this object are only been set with default values.
     */
    public function hasOnlyDefaultValues()
    {
        // otherwise, everything was equal, so return TRUE
        return true;
    } // hasOnlyDefaultValues()

    /**
     * Hydrates (populates) the object variables with values from the database resultset.
     *
     * An offset (0-based "start column") is specified so that objects can be hydrated
     * with a subset of the columns in the resultset rows.  This is needed, for example,
     * for results of JOIN queries where the resultset row includes columns from two or
     * more tables.
     *
     * @param array   $row       The row returned by DataFetcher->fetch().
     * @param int     $startcol  0-based offset column which indicates which restultset column to start with.
     * @param boolean $rehydrate Whether this object is being re-hydrated from the database.
     * @param string  $indexType The index type of $row. Mostly DataFetcher->getIndexType().
                                  One of the class type constants TableMap::TYPE_PHPNAME, TableMap::TYPE_CAMELNAME
     *                            TableMap::TYPE_COLNAME, TableMap::TYPE_FIELDNAME, TableMap::TYPE_NUM.
     *
     * @return int             next starting column
     * @throws PropelException - Any caught Exception will be rewrapped as a PropelException.
     */
    public function hydrate($row, $startcol = 0, $rehydrate = false, $indexType = TableMap::TYPE_NUM)
    {
        try {

            $col = $row[TableMap::TYPE_NUM == $indexType ? 0 + $startcol : LogTableMap::translateFieldName('IdLog', TableMap::TYPE_PHPNAME, $indexType)];
            $this->id_log = (null !== $col) ? (string) $col : null;

            $col = $row[TableMap::TYPE_NUM == $indexType ? 1 + $startcol : LogTableMap::translateFieldName('IdIndividu', TableMap::TYPE_PHPNAME, $indexType)];
            $this->id_individu = (null !== $col) ? (string) $col : null;

            $col = $row[TableMap::TYPE_NUM == $indexType ? 2 + $startcol : LogTableMap::translateFieldName('IdEntite', TableMap::TYPE_PHPNAME, $indexType)];
            $this->id_entite = (null !== $col) ? (string) $col : null;

            $col = $row[TableMap::TYPE_NUM == $indexType ? 3 + $startcol : LogTableMap::translateFieldName('Code', TableMap::TYPE_PHPNAME, $indexType)];
            $this->code = (null !== $col) ? (string) $col : null;

            $col = $row[TableMap::TYPE_NUM == $indexType ? 4 + $startcol : LogTableMap::translateFieldName('Variables', TableMap::TYPE_PHPNAME, $indexType)];
            $this->variables = (null !== $col) ? (string) $col : null;

            $col = $row[TableMap::TYPE_NUM == $indexType ? 5 + $startcol : LogTableMap::translateFieldName('DateOperation', TableMap::TYPE_PHPNAME, $indexType)];
            if ($col === '0000-00-00 00:00:00') {
                $col = null;
            }
            $this->date_operation = (null !== $col) ? PropelDateTime::newInstance($col, null, 'DateTime') : null;

            $col = $row[TableMap::TYPE_NUM == $indexType ? 6 + $startcol : LogTableMap::translateFieldName('Observation', TableMap::TYPE_PHPNAME, $indexType)];
            $this->observation = (null !== $col) ? (string) $col : null;
            $this->resetModified();

            $this->setNew(false);

            if ($rehydrate) {
                $this->ensureConsistency();
            }

            return $startcol + 7; // 7 = LogTableMap::NUM_HYDRATE_COLUMNS.

        } catch (Exception $e) {
            throw new PropelException(sprintf('Error populating %s object', '\\Log'), 0, $e);
        }
    }

    /**
     * Checks and repairs the internal consistency of the object.
     *
     * This method is executed after an already-instantiated object is re-hydrated
     * from the database.  It exists to check any foreign keys to make sure that
     * the objects related to the current object are correct based on foreign key.
     *
     * You can override this method in the stub class, but you should always invoke
     * the base method from the overridden method (i.e. parent::ensureConsistency()),
     * in case your model changes.
     *
     * @throws PropelException
     */
    public function ensureConsistency()
    {
        if ($this->aIndividu !== null && $this->id_individu !== $this->aIndividu->getIdIndividu()) {
            $this->aIndividu = null;
        }
    } // ensureConsistency

    /**
     * Reloads this object from datastore based on primary key and (optionally) resets all associated objects.
     *
     * This will only work if the object has been saved and has a valid primary key set.
     *
     * @param      boolean $deep (optional) Whether to also de-associated any related objects.
     * @param      ConnectionInterface $con (optional) The ConnectionInterface connection to use.
     * @return void
     * @throws PropelException - if this object is deleted, unsaved or doesn't have pk match in db
     */
    public function reload($deep = false, ConnectionInterface $con = null)
    {
        if ($this->isDeleted()) {
            throw new PropelException("Cannot reload a deleted object.");
        }

        if ($this->isNew()) {
            throw new PropelException("Cannot reload an unsaved object.");
        }

        if ($con === null) {
            $con = Propel::getServiceContainer()->getReadConnection(LogTableMap::DATABASE_NAME);
        }

        // We don't need to alter the object instance pool; we're just modifying this instance
        // already in the pool.

        $dataFetcher = ChildLogQuery::create(null, $this->buildPkeyCriteria())->setFormatter(ModelCriteria::FORMAT_STATEMENT)->find($con);
        $row = $dataFetcher->fetch();
        $dataFetcher->close();
        if (!$row) {
            throw new PropelException('Cannot find matching row in the database to reload object values.');
        }
        $this->hydrate($row, 0, true, $dataFetcher->getIndexType()); // rehydrate

        if ($deep) {  // also de-associate any related objects?

            $this->aIndividu = null;
            $this->collLogLiens = null;

        } // if (deep)
    }

    /**
     * Removes this object from datastore and sets delete attribute.
     *
     * @param      ConnectionInterface $con
     * @return void
     * @throws PropelException
     * @see Log::setDeleted()
     * @see Log::isDeleted()
     */
    public function delete(ConnectionInterface $con = null)
    {
        if ($this->isDeleted()) {
            throw new PropelException("This object has already been deleted.");
        }

        if ($con === null) {
            $con = Propel::getServiceContainer()->getWriteConnection(LogTableMap::DATABASE_NAME);
        }

        $con->transaction(function () use ($con) {
            $deleteQuery = ChildLogQuery::create()
                ->filterByPrimaryKey($this->getPrimaryKey());
            $ret = $this->preDelete($con);
            if ($ret) {
                $deleteQuery->delete($con);
                $this->postDelete($con);
                $this->setDeleted(true);
            }
        });
    }

    /**
     * Persists this object to the database.
     *
     * If the object is new, it inserts it; otherwise an update is performed.
     * All modified related objects will also be persisted in the doSave()
     * method.  This method wraps all precipitate database operations in a
     * single transaction.
     *
     * @param      ConnectionInterface $con
     * @return int             The number of rows affected by this insert/update and any referring fk objects' save() operations.
     * @throws PropelException
     * @see doSave()
     */
    public function save(ConnectionInterface $con = null)
    {
        if ($this->isDeleted()) {
            throw new PropelException("You cannot save an object that has been deleted.");
        }

        if ($this->alreadyInSave) {
            return 0;
        }

        if ($con === null) {
            $con = Propel::getServiceContainer()->getWriteConnection(LogTableMap::DATABASE_NAME);
        }

        return $con->transaction(function () use ($con) {
            $ret = $this->preSave($con);
            $isInsert = $this->isNew();
            if ($isInsert) {
                $ret = $ret && $this->preInsert($con);
            } else {
                $ret = $ret && $this->preUpdate($con);
            }
            if ($ret) {
                $affectedRows = $this->doSave($con);
                if ($isInsert) {
                    $this->postInsert($con);
                } else {
                    $this->postUpdate($con);
                }
                $this->postSave($con);
                LogTableMap::addInstanceToPool($this);
            } else {
                $affectedRows = 0;
            }

            return $affectedRows;
        });
    }

    /**
     * Performs the work of inserting or updating the row in the database.
     *
     * If the object is new, it inserts it; otherwise an update is performed.
     * All related objects are also updated in this method.
     *
     * @param      ConnectionInterface $con
     * @return int             The number of rows affected by this insert/update and any referring fk objects' save() operations.
     * @throws PropelException
     * @see save()
     */
    protected function doSave(ConnectionInterface $con)
    {
        $affectedRows = 0; // initialize var to track total num of affected rows
        if (!$this->alreadyInSave) {
            $this->alreadyInSave = true;

            // We call the save method on the following object(s) if they
            // were passed to this object by their corresponding set
            // method.  This object relates to these object(s) by a
            // foreign key reference.

            if ($this->aIndividu !== null) {
                if ($this->aIndividu->isModified() || $this->aIndividu->isNew()) {
                    $affectedRows += $this->aIndividu->save($con);
                }
                $this->setIndividu($this->aIndividu);
            }

            if ($this->isNew() || $this->isModified()) {
                // persist changes
                if ($this->isNew()) {
                    $this->doInsert($con);
                    $affectedRows += 1;
                } else {
                    $affectedRows += $this->doUpdate($con);
                }
                $this->resetModified();
            }

            if ($this->logLiensScheduledForDeletion !== null) {
                if (!$this->logLiensScheduledForDeletion->isEmpty()) {
                    \LogLienQuery::create()
                        ->filterByPrimaryKeys($this->logLiensScheduledForDeletion->getPrimaryKeys(false))
                        ->delete($con);
                    $this->logLiensScheduledForDeletion = null;
                }
            }

            if ($this->collLogLiens !== null) {
                foreach ($this->collLogLiens as $referrerFK) {
                    if (!$referrerFK->isDeleted() && ($referrerFK->isNew() || $referrerFK->isModified())) {
                        $affectedRows += $referrerFK->save($con);
                    }
                }
            }

            $this->alreadyInSave = false;

        }

        return $affectedRows;
    } // doSave()

    /**
     * Insert the row in the database.
     *
     * @param      ConnectionInterface $con
     *
     * @throws PropelException
     * @see doSave()
     */
    protected function doInsert(ConnectionInterface $con)
    {
        $modifiedColumns = array();
        $index = 0;

        $this->modifiedColumns[LogTableMap::COL_ID_LOG] = true;
        if (null !== $this->id_log) {
            throw new PropelException('Cannot insert a value for auto-increment primary key (' . LogTableMap::COL_ID_LOG . ')');
        }

         // check the columns in natural order for more readable SQL queries
        if ($this->isColumnModified(LogTableMap::COL_ID_LOG)) {
            $modifiedColumns[':p' . $index++]  = '`id_log`';
        }
        if ($this->isColumnModified(LogTableMap::COL_ID_INDIVIDU)) {
            $modifiedColumns[':p' . $index++]  = '`id_individu`';
        }
        if ($this->isColumnModified(LogTableMap::COL_ID_ENTITE)) {
            $modifiedColumns[':p' . $index++]  = '`id_entite`';
        }
        if ($this->isColumnModified(LogTableMap::COL_CODE)) {
            $modifiedColumns[':p' . $index++]  = '`code`';
        }
        if ($this->isColumnModified(LogTableMap::COL_VARIABLES)) {
            $modifiedColumns[':p' . $index++]  = '`variables`';
        }
        if ($this->isColumnModified(LogTableMap::COL_DATE_OPERATION)) {
            $modifiedColumns[':p' . $index++]  = '`date_operation`';
        }
        if ($this->isColumnModified(LogTableMap::COL_OBSERVATION)) {
            $modifiedColumns[':p' . $index++]  = '`observation`';
        }

        $sql = sprintf(
            'INSERT INTO `asso_logs` (%s) VALUES (%s)',
            implode(', ', $modifiedColumns),
            implode(', ', array_keys($modifiedColumns))
        );

        try {
            $stmt = $con->prepare($sql);
            foreach ($modifiedColumns as $identifier => $columnName) {
                switch ($columnName) {
                    case '`id_log`':
                        $stmt->bindValue($identifier, $this->id_log, PDO::PARAM_INT);
                        break;
                    case '`id_individu`':
                        $stmt->bindValue($identifier, $this->id_individu, PDO::PARAM_INT);
                        break;
                    case '`id_entite`':
                        $stmt->bindValue($identifier, $this->id_entite, PDO::PARAM_INT);
                        break;
                    case '`code`':
                        $stmt->bindValue($identifier, $this->code, PDO::PARAM_STR);
                        break;
                    case '`variables`':
                        $stmt->bindValue($identifier, $this->variables, PDO::PARAM_STR);
                        break;
                    case '`date_operation`':
                        $stmt->bindValue($identifier, $this->date_operation ? $this->date_operation->format("Y-m-d H:i:s.u") : null, PDO::PARAM_STR);
                        break;
                    case '`observation`':
                        $stmt->bindValue($identifier, $this->observation, PDO::PARAM_STR);
                        break;
                }
            }
            $stmt->execute();
        } catch (Exception $e) {
            Propel::log($e->getMessage(), Propel::LOG_ERR);
            throw new PropelException(sprintf('Unable to execute INSERT statement [%s]', $sql), 0, $e);
        }

        try {
            $pk = $con->lastInsertId();
        } catch (Exception $e) {
            throw new PropelException('Unable to get autoincrement id.', 0, $e);
        }
        $this->setIdLog($pk);

        $this->setNew(false);
    }

    /**
     * Update the row in the database.
     *
     * @param      ConnectionInterface $con
     *
     * @return Integer Number of updated rows
     * @see doSave()
     */
    protected function doUpdate(ConnectionInterface $con)
    {
        $selectCriteria = $this->buildPkeyCriteria();
        $valuesCriteria = $this->buildCriteria();

        return $selectCriteria->doUpdate($valuesCriteria, $con);
    }

    /**
     * Retrieves a field from the object by name passed in as a string.
     *
     * @param      string $name name
     * @param      string $type The type of fieldname the $name is of:
     *                     one of the class type constants TableMap::TYPE_PHPNAME, TableMap::TYPE_CAMELNAME
     *                     TableMap::TYPE_COLNAME, TableMap::TYPE_FIELDNAME, TableMap::TYPE_NUM.
     *                     Defaults to TableMap::TYPE_FIELDNAME.
     * @return mixed Value of field.
     */
    public function getByName($name, $type = TableMap::TYPE_FIELDNAME)
    {
        $pos = LogTableMap::translateFieldName($name, $type, TableMap::TYPE_NUM);
        $field = $this->getByPosition($pos);

        return $field;
    }

    /**
     * Retrieves a field from the object by Position as specified in the xml schema.
     * Zero-based.
     *
     * @param      int $pos position in xml schema
     * @return mixed Value of field at $pos
     */
    public function getByPosition($pos)
    {
        switch ($pos) {
            case 0:
                return $this->getIdLog();
                break;
            case 1:
                return $this->getIdIndividu();
                break;
            case 2:
                return $this->getIdEntite();
                break;
            case 3:
                return $this->getCode();
                break;
            case 4:
                return $this->getVariables();
                break;
            case 5:
                return $this->getDateOperation();
                break;
            case 6:
                return $this->getObservation();
                break;
            default:
                return null;
                break;
        } // switch()
    }

    /**
     * Exports the object as an array.
     *
     * You can specify the key type of the array by passing one of the class
     * type constants.
     *
     * @param     string  $keyType (optional) One of the class type constants TableMap::TYPE_PHPNAME, TableMap::TYPE_CAMELNAME,
     *                    TableMap::TYPE_COLNAME, TableMap::TYPE_FIELDNAME, TableMap::TYPE_NUM.
     *                    Defaults to TableMap::TYPE_FIELDNAME.
     * @param     boolean $includeLazyLoadColumns (optional) Whether to include lazy loaded columns. Defaults to TRUE.
     * @param     array $alreadyDumpedObjects List of objects to skip to avoid recursion
     * @param     boolean $includeForeignObjects (optional) Whether to include hydrated related objects. Default to FALSE.
     *
     * @return array an associative array containing the field names (as keys) and field values
     */
    public function toArray($keyType = TableMap::TYPE_FIELDNAME, $includeLazyLoadColumns = true, $alreadyDumpedObjects = array(), $includeForeignObjects = false)
    {

        if (isset($alreadyDumpedObjects['Log'][$this->hashCode()])) {
            return '*RECURSION*';
        }
        $alreadyDumpedObjects['Log'][$this->hashCode()] = true;
        $keys = LogTableMap::getFieldNames($keyType);
        $result = array(
            $keys[0] => $this->getIdLog(),
            $keys[1] => $this->getIdIndividu(),
            $keys[2] => $this->getIdEntite(),
            $keys[3] => $this->getCode(),
            $keys[4] => $this->getVariables(),
            $keys[5] => $this->getDateOperation(),
            $keys[6] => $this->getObservation(),
        );
        if ($result[$keys[5]] instanceof \DateTimeInterface) {
            $result[$keys[5]] = $result[$keys[5]]->format('c');
        }

        $virtualColumns = $this->virtualColumns;
        foreach ($virtualColumns as $key => $virtualColumn) {
            $result[$key] = $virtualColumn;
        }

        if ($includeForeignObjects) {
            if (null !== $this->aIndividu) {

                switch ($keyType) {
                    case TableMap::TYPE_CAMELNAME:
                        $key = 'individu';
                        break;
                    case TableMap::TYPE_FIELDNAME:
                        $key = 'asso_individus';
                        break;
                    default:
                        $key = 'Individu';
                }

                $result[$key] = $this->aIndividu->toArray($keyType, $includeLazyLoadColumns,  $alreadyDumpedObjects, true);
            }
            if (null !== $this->collLogLiens) {

                switch ($keyType) {
                    case TableMap::TYPE_CAMELNAME:
                        $key = 'logLiens';
                        break;
                    case TableMap::TYPE_FIELDNAME:
                        $key = 'asso_logs_lienss';
                        break;
                    default:
                        $key = 'LogLiens';
                }

                $result[$key] = $this->collLogLiens->toArray(null, false, $keyType, $includeLazyLoadColumns, $alreadyDumpedObjects);
            }
        }

        return $result;
    }

    /**
     * Sets a field from the object by name passed in as a string.
     *
     * @param  string $name
     * @param  mixed  $value field value
     * @param  string $type The type of fieldname the $name is of:
     *                one of the class type constants TableMap::TYPE_PHPNAME, TableMap::TYPE_CAMELNAME
     *                TableMap::TYPE_COLNAME, TableMap::TYPE_FIELDNAME, TableMap::TYPE_NUM.
     *                Defaults to TableMap::TYPE_FIELDNAME.
     * @return $this|\Log
     */
    public function setByName($name, $value, $type = TableMap::TYPE_FIELDNAME)
    {
        $pos = LogTableMap::translateFieldName($name, $type, TableMap::TYPE_NUM);

        return $this->setByPosition($pos, $value);
    }

    /**
     * Sets a field from the object by Position as specified in the xml schema.
     * Zero-based.
     *
     * @param  int $pos position in xml schema
     * @param  mixed $value field value
     * @return $this|\Log
     */
    public function setByPosition($pos, $value)
    {
        switch ($pos) {
            case 0:
                $this->setIdLog($value);
                break;
            case 1:
                $this->setIdIndividu($value);
                break;
            case 2:
                $this->setIdEntite($value);
                break;
            case 3:
                $this->setCode($value);
                break;
            case 4:
                $this->setVariables($value);
                break;
            case 5:
                $this->setDateOperation($value);
                break;
            case 6:
                $this->setObservation($value);
                break;
        } // switch()

        return $this;
    }

    /**
     * Populates the object using an array.
     *
     * This is particularly useful when populating an object from one of the
     * request arrays (e.g. $_POST).  This method goes through the column
     * names, checking to see whether a matching key exists in populated
     * array. If so the setByName() method is called for that column.
     *
     * You can specify the key type of the array by additionally passing one
     * of the class type constants TableMap::TYPE_PHPNAME, TableMap::TYPE_CAMELNAME,
     * TableMap::TYPE_COLNAME, TableMap::TYPE_FIELDNAME, TableMap::TYPE_NUM.
     * The default key type is the column's TableMap::TYPE_FIELDNAME.
     *
     * @param      array  $arr     An array to populate the object from.
     * @param      string $keyType The type of keys the array uses.
     * @return void
     */
    public function fromArray($arr, $keyType = TableMap::TYPE_FIELDNAME)
    {
        $keys = LogTableMap::getFieldNames($keyType);

        if (array_key_exists($keys[0], $arr)) {
            $this->setIdLog($arr[$keys[0]]);
        }
        if (array_key_exists($keys[1], $arr)) {
            $this->setIdIndividu($arr[$keys[1]]);
        }
        if (array_key_exists($keys[2], $arr)) {
            $this->setIdEntite($arr[$keys[2]]);
        }
        if (array_key_exists($keys[3], $arr)) {
            $this->setCode($arr[$keys[3]]);
        }
        if (array_key_exists($keys[4], $arr)) {
            $this->setVariables($arr[$keys[4]]);
        }
        if (array_key_exists($keys[5], $arr)) {
            $this->setDateOperation($arr[$keys[5]]);
        }
        if (array_key_exists($keys[6], $arr)) {
            $this->setObservation($arr[$keys[6]]);
        }
    }

     /**
     * Populate the current object from a string, using a given parser format
     * <code>
     * $book = new Book();
     * $book->importFrom('JSON', '{"Id":9012,"Title":"Don Juan","ISBN":"0140422161","Price":12.99,"PublisherId":1234,"AuthorId":5678}');
     * </code>
     *
     * You can specify the key type of the array by additionally passing one
     * of the class type constants TableMap::TYPE_PHPNAME, TableMap::TYPE_CAMELNAME,
     * TableMap::TYPE_COLNAME, TableMap::TYPE_FIELDNAME, TableMap::TYPE_NUM.
     * The default key type is the column's TableMap::TYPE_FIELDNAME.
     *
     * @param mixed $parser A AbstractParser instance,
     *                       or a format name ('XML', 'YAML', 'JSON', 'CSV')
     * @param string $data The source data to import from
     * @param string $keyType The type of keys the array uses.
     *
     * @return $this|\Log The current object, for fluid interface
     */
    public function importFrom($parser, $data, $keyType = TableMap::TYPE_FIELDNAME)
    {
        if (!$parser instanceof AbstractParser) {
            $parser = AbstractParser::getParser($parser);
        }

        $this->fromArray($parser->toArray($data), $keyType);

        return $this;
    }

    /**
     * Build a Criteria object containing the values of all modified columns in this object.
     *
     * @return Criteria The Criteria object containing all modified values.
     */
    public function buildCriteria()
    {
        $criteria = new Criteria(LogTableMap::DATABASE_NAME);

        if ($this->isColumnModified(LogTableMap::COL_ID_LOG)) {
            $criteria->add(LogTableMap::COL_ID_LOG, $this->id_log);
        }
        if ($this->isColumnModified(LogTableMap::COL_ID_INDIVIDU)) {
            $criteria->add(LogTableMap::COL_ID_INDIVIDU, $this->id_individu);
        }
        if ($this->isColumnModified(LogTableMap::COL_ID_ENTITE)) {
            $criteria->add(LogTableMap::COL_ID_ENTITE, $this->id_entite);
        }
        if ($this->isColumnModified(LogTableMap::COL_CODE)) {
            $criteria->add(LogTableMap::COL_CODE, $this->code);
        }
        if ($this->isColumnModified(LogTableMap::COL_VARIABLES)) {
            $criteria->add(LogTableMap::COL_VARIABLES, $this->variables);
        }
        if ($this->isColumnModified(LogTableMap::COL_DATE_OPERATION)) {
            $criteria->add(LogTableMap::COL_DATE_OPERATION, $this->date_operation);
        }
        if ($this->isColumnModified(LogTableMap::COL_OBSERVATION)) {
            $criteria->add(LogTableMap::COL_OBSERVATION, $this->observation);
        }

        return $criteria;
    }

    /**
     * Builds a Criteria object containing the primary key for this object.
     *
     * Unlike buildCriteria() this method includes the primary key values regardless
     * of whether or not they have been modified.
     *
     * @throws LogicException if no primary key is defined
     *
     * @return Criteria The Criteria object containing value(s) for primary key(s).
     */
    public function buildPkeyCriteria()
    {
        $criteria = ChildLogQuery::create();
        $criteria->add(LogTableMap::COL_ID_LOG, $this->id_log);

        return $criteria;
    }

    /**
     * If the primary key is not null, return the hashcode of the
     * primary key. Otherwise, return the hash code of the object.
     *
     * @return int Hashcode
     */
    public function hashCode()
    {
        $validPk = null !== $this->getIdLog();

        $validPrimaryKeyFKs = 0;
        $primaryKeyFKs = [];

        if ($validPk) {
            return crc32(json_encode($this->getPrimaryKey(), JSON_UNESCAPED_UNICODE));
        } elseif ($validPrimaryKeyFKs) {
            return crc32(json_encode($primaryKeyFKs, JSON_UNESCAPED_UNICODE));
        }

        return spl_object_hash($this);
    }

    /**
     * Returns the primary key for this object (row).
     * @return string
     */
    public function getPrimaryKey()
    {
        return $this->getIdLog();
    }

    /**
     * Generic method to set the primary key (id_log column).
     *
     * @param       string $key Primary key.
     * @return void
     */
    public function setPrimaryKey($key)
    {
        $this->setIdLog($key);
    }

    /**
     * Returns true if the primary key for this object is null.
     * @return boolean
     */
    public function isPrimaryKeyNull()
    {
        return null === $this->getIdLog();
    }

    /**
     * Sets contents of passed object to values from current object.
     *
     * If desired, this method can also make copies of all associated (fkey referrers)
     * objects.
     *
     * @param      object $copyObj An object of \Log (or compatible) type.
     * @param      boolean $deepCopy Whether to also copy all rows that refer (by fkey) to the current row.
     * @param      boolean $makeNew Whether to reset autoincrement PKs and make the object new.
     * @throws PropelException
     */
    public function copyInto($copyObj, $deepCopy = false, $makeNew = true)
    {
        $copyObj->setIdIndividu($this->getIdIndividu());
        $copyObj->setIdEntite($this->getIdEntite());
        $copyObj->setCode($this->getCode());
        $copyObj->setVariables($this->getVariables());
        $copyObj->setDateOperation($this->getDateOperation());
        $copyObj->setObservation($this->getObservation());

        if ($deepCopy) {
            // important: temporarily setNew(false) because this affects the behavior of
            // the getter/setter methods for fkey referrer objects.
            $copyObj->setNew(false);

            foreach ($this->getLogLiens() as $relObj) {
                if ($relObj !== $this) {  // ensure that we don't try to copy a reference to ourselves
                    $copyObj->addLogLien($relObj->copy($deepCopy));
                }
            }

        } // if ($deepCopy)

        if ($makeNew) {
            $copyObj->setNew(true);
            $copyObj->setIdLog(NULL); // this is a auto-increment column, so set to default value
        }
    }

    /**
     * Makes a copy of this object that will be inserted as a new row in table when saved.
     * It creates a new object filling in the simple attributes, but skipping any primary
     * keys that are defined for the table.
     *
     * If desired, this method can also make copies of all associated (fkey referrers)
     * objects.
     *
     * @param  boolean $deepCopy Whether to also copy all rows that refer (by fkey) to the current row.
     * @return \Log Clone of current object.
     * @throws PropelException
     */
    public function copy($deepCopy = false)
    {
        // we use get_class(), because this might be a subclass
        $clazz = get_class($this);
        $copyObj = new $clazz();
        $this->copyInto($copyObj, $deepCopy);

        return $copyObj;
    }

    /**
     * Declares an association between this object and a ChildIndividu object.
     *
     * @param  ChildIndividu $v
     * @return $this|\Log The current object (for fluent API support)
     * @throws PropelException
     */
    public function setIndividu(ChildIndividu $v = null)
    {
        if ($v === null) {
            $this->setIdIndividu(NULL);
        } else {
            $this->setIdIndividu($v->getIdIndividu());
        }

        $this->aIndividu = $v;

        // Add binding for other direction of this n:n relationship.
        // If this object has already been added to the ChildIndividu object, it will not be re-added.
        if ($v !== null) {
            $v->addLog($this);
        }


        return $this;
    }


    /**
     * Get the associated ChildIndividu object
     *
     * @param  ConnectionInterface $con Optional Connection object.
     * @return ChildIndividu The associated ChildIndividu object.
     * @throws PropelException
     */
    public function getIndividu(ConnectionInterface $con = null)
    {
        if ($this->aIndividu === null && (($this->id_individu !== "" && $this->id_individu !== null))) {
            $this->aIndividu = ChildIndividuQuery::create()->findPk($this->id_individu, $con);
            /* The following can be used additionally to
                guarantee the related object contains a reference
                to this object.  This level of coupling may, however, be
                undesirable since it could result in an only partially populated collection
                in the referenced object.
                $this->aIndividu->addLogs($this);
             */
        }

        return $this->aIndividu;
    }


    /**
     * Initializes a collection based on the name of a relation.
     * Avoids crafting an 'init[$relationName]s' method name
     * that wouldn't work when StandardEnglishPluralizer is used.
     *
     * @param      string $relationName The name of the relation to initialize
     * @return void
     */
    public function initRelation($relationName)
    {
        if ('LogLien' == $relationName) {
            $this->initLogLiens();
            return;
        }
    }

    /**
     * Clears out the collLogLiens collection
     *
     * This does not modify the database; however, it will remove any associated objects, causing
     * them to be refetched by subsequent calls to accessor method.
     *
     * @return void
     * @see        addLogLiens()
     */
    public function clearLogLiens()
    {
        $this->collLogLiens = null; // important to set this to NULL since that means it is uninitialized
    }

    /**
     * Reset is the collLogLiens collection loaded partially.
     */
    public function resetPartialLogLiens($v = true)
    {
        $this->collLogLiensPartial = $v;
    }

    /**
     * Initializes the collLogLiens collection.
     *
     * By default this just sets the collLogLiens collection to an empty array (like clearcollLogLiens());
     * however, you may wish to override this method in your stub class to provide setting appropriate
     * to your application -- for example, setting the initial array to the values stored in database.
     *
     * @param      boolean $overrideExisting If set to true, the method call initializes
     *                                        the collection even if it is not empty
     *
     * @return void
     */
    public function initLogLiens($overrideExisting = true)
    {
        if (null !== $this->collLogLiens && !$overrideExisting) {
            return;
        }

        $collectionClassName = LogLienTableMap::getTableMap()->getCollectionClassName();

        $this->collLogLiens = new $collectionClassName;
        $this->collLogLiens->setModel('\LogLien');
    }

    /**
     * Gets an array of ChildLogLien objects which contain a foreign key that references this object.
     *
     * If the $criteria is not null, it is used to always fetch the results from the database.
     * Otherwise the results are fetched from the database the first time, then cached.
     * Next time the same method is called without $criteria, the cached collection is returned.
     * If this ChildLog is new, it will return
     * an empty collection or the current collection; the criteria is ignored on a new object.
     *
     * @param      Criteria $criteria optional Criteria object to narrow the query
     * @param      ConnectionInterface $con optional connection object
     * @return ObjectCollection|ChildLogLien[] List of ChildLogLien objects
     * @throws PropelException
     */
    public function getLogLiens(Criteria $criteria = null, ConnectionInterface $con = null)
    {
        $partial = $this->collLogLiensPartial && !$this->isNew();
        if (null === $this->collLogLiens || null !== $criteria  || $partial) {
            if ($this->isNew() && null === $this->collLogLiens) {
                // return empty collection
                $this->initLogLiens();
            } else {
                $collLogLiens = ChildLogLienQuery::create(null, $criteria)
                    ->filterByLog($this)
                    ->find($con);

                if (null !== $criteria) {
                    if (false !== $this->collLogLiensPartial && count($collLogLiens)) {
                        $this->initLogLiens(false);

                        foreach ($collLogLiens as $obj) {
                            if (false == $this->collLogLiens->contains($obj)) {
                                $this->collLogLiens->append($obj);
                            }
                        }

                        $this->collLogLiensPartial = true;
                    }

                    return $collLogLiens;
                }

                if ($partial && $this->collLogLiens) {
                    foreach ($this->collLogLiens as $obj) {
                        if ($obj->isNew()) {
                            $collLogLiens[] = $obj;
                        }
                    }
                }

                $this->collLogLiens = $collLogLiens;
                $this->collLogLiensPartial = false;
            }
        }

        return $this->collLogLiens;
    }

    /**
     * Sets a collection of ChildLogLien objects related by a one-to-many relationship
     * to the current object.
     * It will also schedule objects for deletion based on a diff between old objects (aka persisted)
     * and new objects from the given Propel collection.
     *
     * @param      Collection $logLiens A Propel collection.
     * @param      ConnectionInterface $con Optional connection object
     * @return $this|ChildLog The current object (for fluent API support)
     */
    public function setLogLiens(Collection $logLiens, ConnectionInterface $con = null)
    {
        /** @var ChildLogLien[] $logLiensToDelete */
        $logLiensToDelete = $this->getLogLiens(new Criteria(), $con)->diff($logLiens);


        //since at least one column in the foreign key is at the same time a PK
        //we can not just set a PK to NULL in the lines below. We have to store
        //a backup of all values, so we are able to manipulate these items based on the onDelete value later.
        $this->logLiensScheduledForDeletion = clone $logLiensToDelete;

        foreach ($logLiensToDelete as $logLienRemoved) {
            $logLienRemoved->setLog(null);
        }

        $this->collLogLiens = null;
        foreach ($logLiens as $logLien) {
            $this->addLogLien($logLien);
        }

        $this->collLogLiens = $logLiens;
        $this->collLogLiensPartial = false;

        return $this;
    }

    /**
     * Returns the number of related LogLien objects.
     *
     * @param      Criteria $criteria
     * @param      boolean $distinct
     * @param      ConnectionInterface $con
     * @return int             Count of related LogLien objects.
     * @throws PropelException
     */
    public function countLogLiens(Criteria $criteria = null, $distinct = false, ConnectionInterface $con = null)
    {
        $partial = $this->collLogLiensPartial && !$this->isNew();
        if (null === $this->collLogLiens || null !== $criteria || $partial) {
            if ($this->isNew() && null === $this->collLogLiens) {
                return 0;
            }

            if ($partial && !$criteria) {
                return count($this->getLogLiens());
            }

            $query = ChildLogLienQuery::create(null, $criteria);
            if ($distinct) {
                $query->distinct();
            }

            return $query
                ->filterByLog($this)
                ->count($con);
        }

        return count($this->collLogLiens);
    }

    /**
     * Method called to associate a ChildLogLien object to this object
     * through the ChildLogLien foreign key attribute.
     *
     * @param  ChildLogLien $l ChildLogLien
     * @return $this|\Log The current object (for fluent API support)
     */
    public function addLogLien(ChildLogLien $l)
    {
        if ($this->collLogLiens === null) {
            $this->initLogLiens();
            $this->collLogLiensPartial = true;
        }

        if (!$this->collLogLiens->contains($l)) {
            $this->doAddLogLien($l);

            if ($this->logLiensScheduledForDeletion and $this->logLiensScheduledForDeletion->contains($l)) {
                $this->logLiensScheduledForDeletion->remove($this->logLiensScheduledForDeletion->search($l));
            }
        }

        return $this;
    }

    /**
     * @param ChildLogLien $logLien The ChildLogLien object to add.
     */
    protected function doAddLogLien(ChildLogLien $logLien)
    {
        $this->collLogLiens[]= $logLien;
        $logLien->setLog($this);
    }

    /**
     * @param  ChildLogLien $logLien The ChildLogLien object to remove.
     * @return $this|ChildLog The current object (for fluent API support)
     */
    public function removeLogLien(ChildLogLien $logLien)
    {
        if ($this->getLogLiens()->contains($logLien)) {
            $pos = $this->collLogLiens->search($logLien);
            $this->collLogLiens->remove($pos);
            if (null === $this->logLiensScheduledForDeletion) {
                $this->logLiensScheduledForDeletion = clone $this->collLogLiens;
                $this->logLiensScheduledForDeletion->clear();
            }
            $this->logLiensScheduledForDeletion[]= clone $logLien;
            $logLien->setLog(null);
        }

        return $this;
    }

    /**
     * Clears the current object, sets all attributes to their default values and removes
     * outgoing references as well as back-references (from other objects to this one. Results probably in a database
     * change of those foreign objects when you call `save` there).
     */
    public function clear()
    {
        if (null !== $this->aIndividu) {
            $this->aIndividu->removeLog($this);
        }
        $this->id_log = null;
        $this->id_individu = null;
        $this->id_entite = null;
        $this->code = null;
        $this->variables = null;
        $this->date_operation = null;
        $this->observation = null;
        $this->alreadyInSave = false;
        $this->clearAllReferences();
        $this->resetModified();
        $this->setNew(true);
        $this->setDeleted(false);
    }

    /**
     * Resets all references and back-references to other model objects or collections of model objects.
     *
     * This method is used to reset all php object references (not the actual reference in the database).
     * Necessary for object serialisation.
     *
     * @param      boolean $deep Whether to also clear the references on all referrer objects.
     */
    public function clearAllReferences($deep = false)
    {
        if ($deep) {
            if ($this->collLogLiens) {
                foreach ($this->collLogLiens as $o) {
                    $o->clearAllReferences($deep);
                }
            }
        } // if ($deep)

        $this->collLogLiens = null;
        $this->aIndividu = null;
    }

    /**
     * Return the string representation of this object
     *
     * @return string
     */
    public function __toString()
    {
        return (string) $this->exportTo(LogTableMap::DEFAULT_STRING_FORMAT);
    }

    /**
     * Code to be run before persisting the object
     * @param  ConnectionInterface $con
     * @return boolean
     */
    public function preSave(ConnectionInterface $con = null)
    {
        if (is_callable('parent::preSave')) {
            return parent::preSave($con);
        }
        return true;
    }

    /**
     * Code to be run after persisting the object
     * @param ConnectionInterface $con
     */
    public function postSave(ConnectionInterface $con = null)
    {
        if (is_callable('parent::postSave')) {
            parent::postSave($con);
        }
    }

    /**
     * Code to be run before inserting to database
     * @param  ConnectionInterface $con
     * @return boolean
     */
    public function preInsert(ConnectionInterface $con = null)
    {
        if (is_callable('parent::preInsert')) {
            return parent::preInsert($con);
        }
        return true;
    }

    /**
     * Code to be run after inserting to database
     * @param ConnectionInterface $con
     */
    public function postInsert(ConnectionInterface $con = null)
    {
        if (is_callable('parent::postInsert')) {
            parent::postInsert($con);
        }
    }

    /**
     * Code to be run before updating the object in database
     * @param  ConnectionInterface $con
     * @return boolean
     */
    public function preUpdate(ConnectionInterface $con = null)
    {
        if (is_callable('parent::preUpdate')) {
            return parent::preUpdate($con);
        }
        return true;
    }

    /**
     * Code to be run after updating the object in database
     * @param ConnectionInterface $con
     */
    public function postUpdate(ConnectionInterface $con = null)
    {
        if (is_callable('parent::postUpdate')) {
            parent::postUpdate($con);
        }
    }

    /**
     * Code to be run before deleting the object in database
     * @param  ConnectionInterface $con
     * @return boolean
     */
    public function preDelete(ConnectionInterface $con = null)
    {
        if (is_callable('parent::preDelete')) {
            return parent::preDelete($con);
        }
        return true;
    }

    /**
     * Code to be run after deleting the object in database
     * @param ConnectionInterface $con
     */
    public function postDelete(ConnectionInterface $con = null)
    {
        if (is_callable('parent::postDelete')) {
            parent::postDelete($con);
        }
    }


    /**
     * Derived method to catches calls to undefined methods.
     *
     * Provides magic import/export method support (fromXML()/toXML(), fromYAML()/toYAML(), etc.).
     * Allows to define default __call() behavior if you overwrite __call()
     *
     * @param string $name
     * @param mixed  $params
     *
     * @return array|string
     */
    public function __call($name, $params)
    {
        if (0 === strpos($name, 'get')) {
            $virtualColumn = substr($name, 3);
            if ($this->hasVirtualColumn($virtualColumn)) {
                return $this->getVirtualColumn($virtualColumn);
            }

            $virtualColumn = lcfirst($virtualColumn);
            if ($this->hasVirtualColumn($virtualColumn)) {
                return $this->getVirtualColumn($virtualColumn);
            }
        }

        if (0 === strpos($name, 'from')) {
            $format = substr($name, 4);

            return $this->importFrom($format, reset($params));
        }

        if (0 === strpos($name, 'to')) {
            $format = substr($name, 2);
            $includeLazyLoadColumns = isset($params[0]) ? $params[0] : true;

            return $this->exportTo($format, $includeLazyLoadColumns);
        }

        throw new BadMethodCallException(sprintf('Call to undefined method: %s.', $name));
    }

}
