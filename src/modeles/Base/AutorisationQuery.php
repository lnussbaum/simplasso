<?php

namespace Base;

use \AssoAutorisationsArchive as ChildAssoAutorisationsArchive;
use \Autorisation as ChildAutorisation;
use \AutorisationQuery as ChildAutorisationQuery;
use \Exception;
use \PDO;
use Map\AutorisationTableMap;
use Propel\Runtime\Propel;
use Propel\Runtime\ActiveQuery\Criteria;
use Propel\Runtime\ActiveQuery\ModelCriteria;
use Propel\Runtime\ActiveQuery\ModelJoin;
use Propel\Runtime\Collection\ObjectCollection;
use Propel\Runtime\Connection\ConnectionInterface;
use Propel\Runtime\Exception\PropelException;

/**
 * Base class that represents a query for the 'asso_autorisations' table.
 *
 *
 *
 * @method     ChildAutorisationQuery orderByIdAutorisation($order = Criteria::ASC) Order by the id_autorisation column
 * @method     ChildAutorisationQuery orderByIdIndividu($order = Criteria::ASC) Order by the id_individu column
 * @method     ChildAutorisationQuery orderByProfil($order = Criteria::ASC) Order by the profil column
 * @method     ChildAutorisationQuery orderByNiveau($order = Criteria::ASC) Order by the niveau column
 * @method     ChildAutorisationQuery orderByIdEntite($order = Criteria::ASC) Order by the id_entite column
 * @method     ChildAutorisationQuery orderByIdRestriction($order = Criteria::ASC) Order by the id_restriction column
 * @method     ChildAutorisationQuery orderByVariables($order = Criteria::ASC) Order by the variables column
 * @method     ChildAutorisationQuery orderByCreatedAt($order = Criteria::ASC) Order by the created_at column
 * @method     ChildAutorisationQuery orderByUpdatedAt($order = Criteria::ASC) Order by the updated_at column
 *
 * @method     ChildAutorisationQuery groupByIdAutorisation() Group by the id_autorisation column
 * @method     ChildAutorisationQuery groupByIdIndividu() Group by the id_individu column
 * @method     ChildAutorisationQuery groupByProfil() Group by the profil column
 * @method     ChildAutorisationQuery groupByNiveau() Group by the niveau column
 * @method     ChildAutorisationQuery groupByIdEntite() Group by the id_entite column
 * @method     ChildAutorisationQuery groupByIdRestriction() Group by the id_restriction column
 * @method     ChildAutorisationQuery groupByVariables() Group by the variables column
 * @method     ChildAutorisationQuery groupByCreatedAt() Group by the created_at column
 * @method     ChildAutorisationQuery groupByUpdatedAt() Group by the updated_at column
 *
 * @method     ChildAutorisationQuery leftJoin($relation) Adds a LEFT JOIN clause to the query
 * @method     ChildAutorisationQuery rightJoin($relation) Adds a RIGHT JOIN clause to the query
 * @method     ChildAutorisationQuery innerJoin($relation) Adds a INNER JOIN clause to the query
 *
 * @method     ChildAutorisationQuery leftJoinWith($relation) Adds a LEFT JOIN clause and with to the query
 * @method     ChildAutorisationQuery rightJoinWith($relation) Adds a RIGHT JOIN clause and with to the query
 * @method     ChildAutorisationQuery innerJoinWith($relation) Adds a INNER JOIN clause and with to the query
 *
 * @method     ChildAutorisationQuery leftJoinEntite($relationAlias = null) Adds a LEFT JOIN clause to the query using the Entite relation
 * @method     ChildAutorisationQuery rightJoinEntite($relationAlias = null) Adds a RIGHT JOIN clause to the query using the Entite relation
 * @method     ChildAutorisationQuery innerJoinEntite($relationAlias = null) Adds a INNER JOIN clause to the query using the Entite relation
 *
 * @method     ChildAutorisationQuery joinWithEntite($joinType = Criteria::INNER_JOIN) Adds a join clause and with to the query using the Entite relation
 *
 * @method     ChildAutorisationQuery leftJoinWithEntite() Adds a LEFT JOIN clause and with to the query using the Entite relation
 * @method     ChildAutorisationQuery rightJoinWithEntite() Adds a RIGHT JOIN clause and with to the query using the Entite relation
 * @method     ChildAutorisationQuery innerJoinWithEntite() Adds a INNER JOIN clause and with to the query using the Entite relation
 *
 * @method     ChildAutorisationQuery leftJoinIndividu($relationAlias = null) Adds a LEFT JOIN clause to the query using the Individu relation
 * @method     ChildAutorisationQuery rightJoinIndividu($relationAlias = null) Adds a RIGHT JOIN clause to the query using the Individu relation
 * @method     ChildAutorisationQuery innerJoinIndividu($relationAlias = null) Adds a INNER JOIN clause to the query using the Individu relation
 *
 * @method     ChildAutorisationQuery joinWithIndividu($joinType = Criteria::INNER_JOIN) Adds a join clause and with to the query using the Individu relation
 *
 * @method     ChildAutorisationQuery leftJoinWithIndividu() Adds a LEFT JOIN clause and with to the query using the Individu relation
 * @method     ChildAutorisationQuery rightJoinWithIndividu() Adds a RIGHT JOIN clause and with to the query using the Individu relation
 * @method     ChildAutorisationQuery innerJoinWithIndividu() Adds a INNER JOIN clause and with to the query using the Individu relation
 *
 * @method     \EntiteQuery|\IndividuQuery endUse() Finalizes a secondary criteria and merges it with its primary Criteria
 *
 * @method     ChildAutorisation findOne(ConnectionInterface $con = null) Return the first ChildAutorisation matching the query
 * @method     ChildAutorisation findOneOrCreate(ConnectionInterface $con = null) Return the first ChildAutorisation matching the query, or a new ChildAutorisation object populated from the query conditions when no match is found
 *
 * @method     ChildAutorisation findOneByIdAutorisation(string $id_autorisation) Return the first ChildAutorisation filtered by the id_autorisation column
 * @method     ChildAutorisation findOneByIdIndividu(string $id_individu) Return the first ChildAutorisation filtered by the id_individu column
 * @method     ChildAutorisation findOneByProfil(string $profil) Return the first ChildAutorisation filtered by the profil column
 * @method     ChildAutorisation findOneByNiveau(int $niveau) Return the first ChildAutorisation filtered by the niveau column
 * @method     ChildAutorisation findOneByIdEntite(string $id_entite) Return the first ChildAutorisation filtered by the id_entite column
 * @method     ChildAutorisation findOneByIdRestriction(int $id_restriction) Return the first ChildAutorisation filtered by the id_restriction column
 * @method     ChildAutorisation findOneByVariables(string $variables) Return the first ChildAutorisation filtered by the variables column
 * @method     ChildAutorisation findOneByCreatedAt(string $created_at) Return the first ChildAutorisation filtered by the created_at column
 * @method     ChildAutorisation findOneByUpdatedAt(string $updated_at) Return the first ChildAutorisation filtered by the updated_at column *

 * @method     ChildAutorisation requirePk($key, ConnectionInterface $con = null) Return the ChildAutorisation by primary key and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildAutorisation requireOne(ConnectionInterface $con = null) Return the first ChildAutorisation matching the query and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 *
 * @method     ChildAutorisation requireOneByIdAutorisation(string $id_autorisation) Return the first ChildAutorisation filtered by the id_autorisation column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildAutorisation requireOneByIdIndividu(string $id_individu) Return the first ChildAutorisation filtered by the id_individu column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildAutorisation requireOneByProfil(string $profil) Return the first ChildAutorisation filtered by the profil column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildAutorisation requireOneByNiveau(int $niveau) Return the first ChildAutorisation filtered by the niveau column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildAutorisation requireOneByIdEntite(string $id_entite) Return the first ChildAutorisation filtered by the id_entite column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildAutorisation requireOneByIdRestriction(int $id_restriction) Return the first ChildAutorisation filtered by the id_restriction column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildAutorisation requireOneByVariables(string $variables) Return the first ChildAutorisation filtered by the variables column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildAutorisation requireOneByCreatedAt(string $created_at) Return the first ChildAutorisation filtered by the created_at column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildAutorisation requireOneByUpdatedAt(string $updated_at) Return the first ChildAutorisation filtered by the updated_at column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 *
 * @method     ChildAutorisation[]|ObjectCollection find(ConnectionInterface $con = null) Return ChildAutorisation objects based on current ModelCriteria
 * @method     ChildAutorisation[]|ObjectCollection findByIdAutorisation(string $id_autorisation) Return ChildAutorisation objects filtered by the id_autorisation column
 * @method     ChildAutorisation[]|ObjectCollection findByIdIndividu(string $id_individu) Return ChildAutorisation objects filtered by the id_individu column
 * @method     ChildAutorisation[]|ObjectCollection findByProfil(string $profil) Return ChildAutorisation objects filtered by the profil column
 * @method     ChildAutorisation[]|ObjectCollection findByNiveau(int $niveau) Return ChildAutorisation objects filtered by the niveau column
 * @method     ChildAutorisation[]|ObjectCollection findByIdEntite(string $id_entite) Return ChildAutorisation objects filtered by the id_entite column
 * @method     ChildAutorisation[]|ObjectCollection findByIdRestriction(int $id_restriction) Return ChildAutorisation objects filtered by the id_restriction column
 * @method     ChildAutorisation[]|ObjectCollection findByVariables(string $variables) Return ChildAutorisation objects filtered by the variables column
 * @method     ChildAutorisation[]|ObjectCollection findByCreatedAt(string $created_at) Return ChildAutorisation objects filtered by the created_at column
 * @method     ChildAutorisation[]|ObjectCollection findByUpdatedAt(string $updated_at) Return ChildAutorisation objects filtered by the updated_at column
 * @method     ChildAutorisation[]|\Propel\Runtime\Util\PropelModelPager paginate($page = 1, $maxPerPage = 10, ConnectionInterface $con = null) Issue a SELECT query based on the current ModelCriteria and uses a page and a maximum number of results per page to compute an offset and a limit
 *
 */
abstract class AutorisationQuery extends ModelCriteria
{

    // archivable behavior
    protected $archiveOnDelete = true;
protected $entityNotFoundExceptionClass = '\\Propel\\Runtime\\Exception\\EntityNotFoundException';

    /**
     * Initializes internal state of \Base\AutorisationQuery object.
     *
     * @param     string $dbName The database name
     * @param     string $modelName The phpName of a model, e.g. 'Book'
     * @param     string $modelAlias The alias for the model in this query, e.g. 'b'
     */
    public function __construct($dbName = 'simplasso', $modelName = '\\Autorisation', $modelAlias = null)
    {
        parent::__construct($dbName, $modelName, $modelAlias);
    }

    /**
     * Returns a new ChildAutorisationQuery object.
     *
     * @param     string $modelAlias The alias of a model in the query
     * @param     Criteria $criteria Optional Criteria to build the query from
     *
     * @return ChildAutorisationQuery
     */
    public static function create($modelAlias = null, Criteria $criteria = null)
    {
        if ($criteria instanceof ChildAutorisationQuery) {
            return $criteria;
        }
        $query = new ChildAutorisationQuery();
        if (null !== $modelAlias) {
            $query->setModelAlias($modelAlias);
        }
        if ($criteria instanceof Criteria) {
            $query->mergeWith($criteria);
        }

        return $query;
    }

    /**
     * Find object by primary key.
     * Propel uses the instance pool to skip the database if the object exists.
     * Go fast if the query is untouched.
     *
     * <code>
     * $obj  = $c->findPk(12, $con);
     * </code>
     *
     * @param mixed $key Primary key to use for the query
     * @param ConnectionInterface $con an optional connection object
     *
     * @return ChildAutorisation|array|mixed the result, formatted by the current formatter
     */
    public function findPk($key, ConnectionInterface $con = null)
    {
        if ($key === null) {
            return null;
        }

        if ($con === null) {
            $con = Propel::getServiceContainer()->getReadConnection(AutorisationTableMap::DATABASE_NAME);
        }

        $this->basePreSelect($con);

        if (
            $this->formatter || $this->modelAlias || $this->with || $this->select
            || $this->selectColumns || $this->asColumns || $this->selectModifiers
            || $this->map || $this->having || $this->joins
        ) {
            return $this->findPkComplex($key, $con);
        }

        if ((null !== ($obj = AutorisationTableMap::getInstanceFromPool(null === $key || is_scalar($key) || is_callable([$key, '__toString']) ? (string) $key : $key)))) {
            // the object is already in the instance pool
            return $obj;
        }

        return $this->findPkSimple($key, $con);
    }

    /**
     * Find object by primary key using raw SQL to go fast.
     * Bypass doSelect() and the object formatter by using generated code.
     *
     * @param     mixed $key Primary key to use for the query
     * @param     ConnectionInterface $con A connection object
     *
     * @throws \Propel\Runtime\Exception\PropelException
     *
     * @return ChildAutorisation A model object, or null if the key is not found
     */
    protected function findPkSimple($key, ConnectionInterface $con)
    {
        $sql = 'SELECT `id_autorisation`, `id_individu`, `profil`, `niveau`, `id_entite`, `id_restriction`, `variables`, `created_at`, `updated_at` FROM `asso_autorisations` WHERE `id_autorisation` = :p0';
        try {
            $stmt = $con->prepare($sql);
            $stmt->bindValue(':p0', $key, PDO::PARAM_INT);
            $stmt->execute();
        } catch (Exception $e) {
            Propel::log($e->getMessage(), Propel::LOG_ERR);
            throw new PropelException(sprintf('Unable to execute SELECT statement [%s]', $sql), 0, $e);
        }
        $obj = null;
        if ($row = $stmt->fetch(\PDO::FETCH_NUM)) {
            /** @var ChildAutorisation $obj */
            $obj = new ChildAutorisation();
            $obj->hydrate($row);
            AutorisationTableMap::addInstanceToPool($obj, null === $key || is_scalar($key) || is_callable([$key, '__toString']) ? (string) $key : $key);
        }
        $stmt->closeCursor();

        return $obj;
    }

    /**
     * Find object by primary key.
     *
     * @param     mixed $key Primary key to use for the query
     * @param     ConnectionInterface $con A connection object
     *
     * @return ChildAutorisation|array|mixed the result, formatted by the current formatter
     */
    protected function findPkComplex($key, ConnectionInterface $con)
    {
        // As the query uses a PK condition, no limit(1) is necessary.
        $criteria = $this->isKeepQuery() ? clone $this : $this;
        $dataFetcher = $criteria
            ->filterByPrimaryKey($key)
            ->doSelect($con);

        return $criteria->getFormatter()->init($criteria)->formatOne($dataFetcher);
    }

    /**
     * Find objects by primary key
     * <code>
     * $objs = $c->findPks(array(12, 56, 832), $con);
     * </code>
     * @param     array $keys Primary keys to use for the query
     * @param     ConnectionInterface $con an optional connection object
     *
     * @return ObjectCollection|array|mixed the list of results, formatted by the current formatter
     */
    public function findPks($keys, ConnectionInterface $con = null)
    {
        if (null === $con) {
            $con = Propel::getServiceContainer()->getReadConnection($this->getDbName());
        }
        $this->basePreSelect($con);
        $criteria = $this->isKeepQuery() ? clone $this : $this;
        $dataFetcher = $criteria
            ->filterByPrimaryKeys($keys)
            ->doSelect($con);

        return $criteria->getFormatter()->init($criteria)->format($dataFetcher);
    }

    /**
     * Filter the query by primary key
     *
     * @param     mixed $key Primary key to use for the query
     *
     * @return $this|ChildAutorisationQuery The current query, for fluid interface
     */
    public function filterByPrimaryKey($key)
    {

        return $this->addUsingAlias(AutorisationTableMap::COL_ID_AUTORISATION, $key, Criteria::EQUAL);
    }

    /**
     * Filter the query by a list of primary keys
     *
     * @param     array $keys The list of primary key to use for the query
     *
     * @return $this|ChildAutorisationQuery The current query, for fluid interface
     */
    public function filterByPrimaryKeys($keys)
    {

        return $this->addUsingAlias(AutorisationTableMap::COL_ID_AUTORISATION, $keys, Criteria::IN);
    }

    /**
     * Filter the query on the id_autorisation column
     *
     * Example usage:
     * <code>
     * $query->filterByIdAutorisation(1234); // WHERE id_autorisation = 1234
     * $query->filterByIdAutorisation(array(12, 34)); // WHERE id_autorisation IN (12, 34)
     * $query->filterByIdAutorisation(array('min' => 12)); // WHERE id_autorisation > 12
     * </code>
     *
     * @param     mixed $idAutorisation The value to use as filter.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildAutorisationQuery The current query, for fluid interface
     */
    public function filterByIdAutorisation($idAutorisation = null, $comparison = null)
    {
        if (is_array($idAutorisation)) {
            $useMinMax = false;
            if (isset($idAutorisation['min'])) {
                $this->addUsingAlias(AutorisationTableMap::COL_ID_AUTORISATION, $idAutorisation['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($idAutorisation['max'])) {
                $this->addUsingAlias(AutorisationTableMap::COL_ID_AUTORISATION, $idAutorisation['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(AutorisationTableMap::COL_ID_AUTORISATION, $idAutorisation, $comparison);
    }

    /**
     * Filter the query on the id_individu column
     *
     * Example usage:
     * <code>
     * $query->filterByIdIndividu(1234); // WHERE id_individu = 1234
     * $query->filterByIdIndividu(array(12, 34)); // WHERE id_individu IN (12, 34)
     * $query->filterByIdIndividu(array('min' => 12)); // WHERE id_individu > 12
     * </code>
     *
     * @see       filterByIndividu()
     *
     * @param     mixed $idIndividu The value to use as filter.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildAutorisationQuery The current query, for fluid interface
     */
    public function filterByIdIndividu($idIndividu = null, $comparison = null)
    {
        if (is_array($idIndividu)) {
            $useMinMax = false;
            if (isset($idIndividu['min'])) {
                $this->addUsingAlias(AutorisationTableMap::COL_ID_INDIVIDU, $idIndividu['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($idIndividu['max'])) {
                $this->addUsingAlias(AutorisationTableMap::COL_ID_INDIVIDU, $idIndividu['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(AutorisationTableMap::COL_ID_INDIVIDU, $idIndividu, $comparison);
    }

    /**
     * Filter the query on the profil column
     *
     * Example usage:
     * <code>
     * $query->filterByProfil('fooValue');   // WHERE profil = 'fooValue'
     * $query->filterByProfil('%fooValue%', Criteria::LIKE); // WHERE profil LIKE '%fooValue%'
     * </code>
     *
     * @param     string $profil The value to use as filter.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildAutorisationQuery The current query, for fluid interface
     */
    public function filterByProfil($profil = null, $comparison = null)
    {
        if (null === $comparison) {
            if (is_array($profil)) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(AutorisationTableMap::COL_PROFIL, $profil, $comparison);
    }

    /**
     * Filter the query on the niveau column
     *
     * Example usage:
     * <code>
     * $query->filterByNiveau(1234); // WHERE niveau = 1234
     * $query->filterByNiveau(array(12, 34)); // WHERE niveau IN (12, 34)
     * $query->filterByNiveau(array('min' => 12)); // WHERE niveau > 12
     * </code>
     *
     * @param     mixed $niveau The value to use as filter.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildAutorisationQuery The current query, for fluid interface
     */
    public function filterByNiveau($niveau = null, $comparison = null)
    {
        if (is_array($niveau)) {
            $useMinMax = false;
            if (isset($niveau['min'])) {
                $this->addUsingAlias(AutorisationTableMap::COL_NIVEAU, $niveau['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($niveau['max'])) {
                $this->addUsingAlias(AutorisationTableMap::COL_NIVEAU, $niveau['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(AutorisationTableMap::COL_NIVEAU, $niveau, $comparison);
    }

    /**
     * Filter the query on the id_entite column
     *
     * Example usage:
     * <code>
     * $query->filterByIdEntite(1234); // WHERE id_entite = 1234
     * $query->filterByIdEntite(array(12, 34)); // WHERE id_entite IN (12, 34)
     * $query->filterByIdEntite(array('min' => 12)); // WHERE id_entite > 12
     * </code>
     *
     * @see       filterByEntite()
     *
     * @param     mixed $idEntite The value to use as filter.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildAutorisationQuery The current query, for fluid interface
     */
    public function filterByIdEntite($idEntite = null, $comparison = null)
    {
        if (is_array($idEntite)) {
            $useMinMax = false;
            if (isset($idEntite['min'])) {
                $this->addUsingAlias(AutorisationTableMap::COL_ID_ENTITE, $idEntite['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($idEntite['max'])) {
                $this->addUsingAlias(AutorisationTableMap::COL_ID_ENTITE, $idEntite['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(AutorisationTableMap::COL_ID_ENTITE, $idEntite, $comparison);
    }

    /**
     * Filter the query on the id_restriction column
     *
     * Example usage:
     * <code>
     * $query->filterByIdRestriction(1234); // WHERE id_restriction = 1234
     * $query->filterByIdRestriction(array(12, 34)); // WHERE id_restriction IN (12, 34)
     * $query->filterByIdRestriction(array('min' => 12)); // WHERE id_restriction > 12
     * </code>
     *
     * @param     mixed $idRestriction The value to use as filter.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildAutorisationQuery The current query, for fluid interface
     */
    public function filterByIdRestriction($idRestriction = null, $comparison = null)
    {
        if (is_array($idRestriction)) {
            $useMinMax = false;
            if (isset($idRestriction['min'])) {
                $this->addUsingAlias(AutorisationTableMap::COL_ID_RESTRICTION, $idRestriction['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($idRestriction['max'])) {
                $this->addUsingAlias(AutorisationTableMap::COL_ID_RESTRICTION, $idRestriction['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(AutorisationTableMap::COL_ID_RESTRICTION, $idRestriction, $comparison);
    }

    /**
     * Filter the query on the variables column
     *
     * Example usage:
     * <code>
     * $query->filterByVariables('fooValue');   // WHERE variables = 'fooValue'
     * $query->filterByVariables('%fooValue%', Criteria::LIKE); // WHERE variables LIKE '%fooValue%'
     * </code>
     *
     * @param     string $variables The value to use as filter.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildAutorisationQuery The current query, for fluid interface
     */
    public function filterByVariables($variables = null, $comparison = null)
    {
        if (null === $comparison) {
            if (is_array($variables)) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(AutorisationTableMap::COL_VARIABLES, $variables, $comparison);
    }

    /**
     * Filter the query on the created_at column
     *
     * Example usage:
     * <code>
     * $query->filterByCreatedAt('2011-03-14'); // WHERE created_at = '2011-03-14'
     * $query->filterByCreatedAt('now'); // WHERE created_at = '2011-03-14'
     * $query->filterByCreatedAt(array('max' => 'yesterday')); // WHERE created_at > '2011-03-13'
     * </code>
     *
     * @param     mixed $createdAt The value to use as filter.
     *              Values can be integers (unix timestamps), DateTime objects, or strings.
     *              Empty strings are treated as NULL.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildAutorisationQuery The current query, for fluid interface
     */
    public function filterByCreatedAt($createdAt = null, $comparison = null)
    {
        if (is_array($createdAt)) {
            $useMinMax = false;
            if (isset($createdAt['min'])) {
                $this->addUsingAlias(AutorisationTableMap::COL_CREATED_AT, $createdAt['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($createdAt['max'])) {
                $this->addUsingAlias(AutorisationTableMap::COL_CREATED_AT, $createdAt['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(AutorisationTableMap::COL_CREATED_AT, $createdAt, $comparison);
    }

    /**
     * Filter the query on the updated_at column
     *
     * Example usage:
     * <code>
     * $query->filterByUpdatedAt('2011-03-14'); // WHERE updated_at = '2011-03-14'
     * $query->filterByUpdatedAt('now'); // WHERE updated_at = '2011-03-14'
     * $query->filterByUpdatedAt(array('max' => 'yesterday')); // WHERE updated_at > '2011-03-13'
     * </code>
     *
     * @param     mixed $updatedAt The value to use as filter.
     *              Values can be integers (unix timestamps), DateTime objects, or strings.
     *              Empty strings are treated as NULL.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildAutorisationQuery The current query, for fluid interface
     */
    public function filterByUpdatedAt($updatedAt = null, $comparison = null)
    {
        if (is_array($updatedAt)) {
            $useMinMax = false;
            if (isset($updatedAt['min'])) {
                $this->addUsingAlias(AutorisationTableMap::COL_UPDATED_AT, $updatedAt['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($updatedAt['max'])) {
                $this->addUsingAlias(AutorisationTableMap::COL_UPDATED_AT, $updatedAt['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(AutorisationTableMap::COL_UPDATED_AT, $updatedAt, $comparison);
    }

    /**
     * Filter the query by a related \Entite object
     *
     * @param \Entite|ObjectCollection $entite The related object(s) to use as filter
     * @param string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @throws \Propel\Runtime\Exception\PropelException
     *
     * @return ChildAutorisationQuery The current query, for fluid interface
     */
    public function filterByEntite($entite, $comparison = null)
    {
        if ($entite instanceof \Entite) {
            return $this
                ->addUsingAlias(AutorisationTableMap::COL_ID_ENTITE, $entite->getIdEntite(), $comparison);
        } elseif ($entite instanceof ObjectCollection) {
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }

            return $this
                ->addUsingAlias(AutorisationTableMap::COL_ID_ENTITE, $entite->toKeyValue('PrimaryKey', 'IdEntite'), $comparison);
        } else {
            throw new PropelException('filterByEntite() only accepts arguments of type \Entite or Collection');
        }
    }

    /**
     * Adds a JOIN clause to the query using the Entite relation
     *
     * @param     string $relationAlias optional alias for the relation
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return $this|ChildAutorisationQuery The current query, for fluid interface
     */
    public function joinEntite($relationAlias = null, $joinType = Criteria::INNER_JOIN)
    {
        $tableMap = $this->getTableMap();
        $relationMap = $tableMap->getRelation('Entite');

        // create a ModelJoin object for this join
        $join = new ModelJoin();
        $join->setJoinType($joinType);
        $join->setRelationMap($relationMap, $this->useAliasInSQL ? $this->getModelAlias() : null, $relationAlias);
        if ($previousJoin = $this->getPreviousJoin()) {
            $join->setPreviousJoin($previousJoin);
        }

        // add the ModelJoin to the current object
        if ($relationAlias) {
            $this->addAlias($relationAlias, $relationMap->getRightTable()->getName());
            $this->addJoinObject($join, $relationAlias);
        } else {
            $this->addJoinObject($join, 'Entite');
        }

        return $this;
    }

    /**
     * Use the Entite relation Entite object
     *
     * @see useQuery()
     *
     * @param     string $relationAlias optional alias for the relation,
     *                                   to be used as main alias in the secondary query
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return \EntiteQuery A secondary query class using the current class as primary query
     */
    public function useEntiteQuery($relationAlias = null, $joinType = Criteria::INNER_JOIN)
    {
        return $this
            ->joinEntite($relationAlias, $joinType)
            ->useQuery($relationAlias ? $relationAlias : 'Entite', '\EntiteQuery');
    }

    /**
     * Filter the query by a related \Individu object
     *
     * @param \Individu|ObjectCollection $individu The related object(s) to use as filter
     * @param string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @throws \Propel\Runtime\Exception\PropelException
     *
     * @return ChildAutorisationQuery The current query, for fluid interface
     */
    public function filterByIndividu($individu, $comparison = null)
    {
        if ($individu instanceof \Individu) {
            return $this
                ->addUsingAlias(AutorisationTableMap::COL_ID_INDIVIDU, $individu->getIdIndividu(), $comparison);
        } elseif ($individu instanceof ObjectCollection) {
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }

            return $this
                ->addUsingAlias(AutorisationTableMap::COL_ID_INDIVIDU, $individu->toKeyValue('PrimaryKey', 'IdIndividu'), $comparison);
        } else {
            throw new PropelException('filterByIndividu() only accepts arguments of type \Individu or Collection');
        }
    }

    /**
     * Adds a JOIN clause to the query using the Individu relation
     *
     * @param     string $relationAlias optional alias for the relation
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return $this|ChildAutorisationQuery The current query, for fluid interface
     */
    public function joinIndividu($relationAlias = null, $joinType = Criteria::INNER_JOIN)
    {
        $tableMap = $this->getTableMap();
        $relationMap = $tableMap->getRelation('Individu');

        // create a ModelJoin object for this join
        $join = new ModelJoin();
        $join->setJoinType($joinType);
        $join->setRelationMap($relationMap, $this->useAliasInSQL ? $this->getModelAlias() : null, $relationAlias);
        if ($previousJoin = $this->getPreviousJoin()) {
            $join->setPreviousJoin($previousJoin);
        }

        // add the ModelJoin to the current object
        if ($relationAlias) {
            $this->addAlias($relationAlias, $relationMap->getRightTable()->getName());
            $this->addJoinObject($join, $relationAlias);
        } else {
            $this->addJoinObject($join, 'Individu');
        }

        return $this;
    }

    /**
     * Use the Individu relation Individu object
     *
     * @see useQuery()
     *
     * @param     string $relationAlias optional alias for the relation,
     *                                   to be used as main alias in the secondary query
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return \IndividuQuery A secondary query class using the current class as primary query
     */
    public function useIndividuQuery($relationAlias = null, $joinType = Criteria::INNER_JOIN)
    {
        return $this
            ->joinIndividu($relationAlias, $joinType)
            ->useQuery($relationAlias ? $relationAlias : 'Individu', '\IndividuQuery');
    }

    /**
     * Exclude object from result
     *
     * @param   ChildAutorisation $autorisation Object to remove from the list of results
     *
     * @return $this|ChildAutorisationQuery The current query, for fluid interface
     */
    public function prune($autorisation = null)
    {
        if ($autorisation) {
            $this->addUsingAlias(AutorisationTableMap::COL_ID_AUTORISATION, $autorisation->getIdAutorisation(), Criteria::NOT_EQUAL);
        }

        return $this;
    }

    /**
     * Code to execute before every DELETE statement
     *
     * @param     ConnectionInterface $con The connection object used by the query
     */
    protected function basePreDelete(ConnectionInterface $con)
    {
        // archivable behavior

        if ($this->archiveOnDelete) {
            $this->archive($con);
        } else {
            $this->archiveOnDelete = true;
        }


        return $this->preDelete($con);
    }

    /**
     * Deletes all rows from the asso_autorisations table.
     *
     * @param ConnectionInterface $con the connection to use
     * @return int The number of affected rows (if supported by underlying database driver).
     */
    public function doDeleteAll(ConnectionInterface $con = null)
    {
        if (null === $con) {
            $con = Propel::getServiceContainer()->getWriteConnection(AutorisationTableMap::DATABASE_NAME);
        }

        // use transaction because $criteria could contain info
        // for more than one table or we could emulating ON DELETE CASCADE, etc.
        return $con->transaction(function () use ($con) {
            $affectedRows = 0; // initialize var to track total num of affected rows
            $affectedRows += parent::doDeleteAll($con);
            // Because this db requires some delete cascade/set null emulation, we have to
            // clear the cached instance *after* the emulation has happened (since
            // instances get re-added by the select statement contained therein).
            AutorisationTableMap::clearInstancePool();
            AutorisationTableMap::clearRelatedInstancePool();

            return $affectedRows;
        });
    }

    /**
     * Performs a DELETE on the database based on the current ModelCriteria
     *
     * @param ConnectionInterface $con the connection to use
     * @return int             The number of affected rows (if supported by underlying database driver).  This includes CASCADE-related rows
     *                         if supported by native driver or if emulated using Propel.
     * @throws PropelException Any exceptions caught during processing will be
     *                         rethrown wrapped into a PropelException.
     */
    public function delete(ConnectionInterface $con = null)
    {
        if (null === $con) {
            $con = Propel::getServiceContainer()->getWriteConnection(AutorisationTableMap::DATABASE_NAME);
        }

        $criteria = $this;

        // Set the correct dbName
        $criteria->setDbName(AutorisationTableMap::DATABASE_NAME);

        // use transaction because $criteria could contain info
        // for more than one table or we could emulating ON DELETE CASCADE, etc.
        return $con->transaction(function () use ($con, $criteria) {
            $affectedRows = 0; // initialize var to track total num of affected rows

            AutorisationTableMap::removeInstanceFromPool($criteria);

            $affectedRows += ModelCriteria::delete($con);
            AutorisationTableMap::clearRelatedInstancePool();

            return $affectedRows;
        });
    }

    // timestampable behavior

    /**
     * Filter by the latest updated
     *
     * @param      int $nbDays Maximum age of the latest update in days
     *
     * @return     $this|ChildAutorisationQuery The current query, for fluid interface
     */
    public function recentlyUpdated($nbDays = 7)
    {
        return $this->addUsingAlias(AutorisationTableMap::COL_UPDATED_AT, time() - $nbDays * 24 * 60 * 60, Criteria::GREATER_EQUAL);
    }

    /**
     * Order by update date desc
     *
     * @return     $this|ChildAutorisationQuery The current query, for fluid interface
     */
    public function lastUpdatedFirst()
    {
        return $this->addDescendingOrderByColumn(AutorisationTableMap::COL_UPDATED_AT);
    }

    /**
     * Order by update date asc
     *
     * @return     $this|ChildAutorisationQuery The current query, for fluid interface
     */
    public function firstUpdatedFirst()
    {
        return $this->addAscendingOrderByColumn(AutorisationTableMap::COL_UPDATED_AT);
    }

    /**
     * Order by create date desc
     *
     * @return     $this|ChildAutorisationQuery The current query, for fluid interface
     */
    public function lastCreatedFirst()
    {
        return $this->addDescendingOrderByColumn(AutorisationTableMap::COL_CREATED_AT);
    }

    /**
     * Filter by the latest created
     *
     * @param      int $nbDays Maximum age of in days
     *
     * @return     $this|ChildAutorisationQuery The current query, for fluid interface
     */
    public function recentlyCreated($nbDays = 7)
    {
        return $this->addUsingAlias(AutorisationTableMap::COL_CREATED_AT, time() - $nbDays * 24 * 60 * 60, Criteria::GREATER_EQUAL);
    }

    /**
     * Order by create date asc
     *
     * @return     $this|ChildAutorisationQuery The current query, for fluid interface
     */
    public function firstCreatedFirst()
    {
        return $this->addAscendingOrderByColumn(AutorisationTableMap::COL_CREATED_AT);
    }

    // archivable behavior

    /**
     * Copy the data of the objects satisfying the query into ChildAssoAutorisationsArchive archive objects.
     * The archived objects are then saved.
     * If any of the objects has already been archived, the archived object
     * is updated and not duplicated.
     * Warning: This termination methods issues 2n+1 queries.
     *
     * @param      ConnectionInterface $con    Connection to use.
     * @param      Boolean $useLittleMemory    Whether or not to use OnDemandFormatter to retrieve objects.
     *               Set to false if the identity map matters.
     *               Set to true (default) to use less memory.
     *
     * @return     int the number of archived objects
     */
    public function archive($con = null, $useLittleMemory = true)
    {
        $criteria = clone $this;
        // prepare the query
        $criteria->setWith(array());
        if ($useLittleMemory) {
            $criteria->setFormatter(ModelCriteria::FORMAT_ON_DEMAND);
        }
        if ($con === null) {
            $con = Propel::getServiceContainer()->getWriteConnection(AutorisationTableMap::DATABASE_NAME);
        }

        return $con->transaction(function () use ($con, $criteria) {
            $totalArchivedObjects = 0;

            // archive all results one by one
            foreach ($criteria->find($con) as $object) {
                $object->archive($con);
                $totalArchivedObjects++;
            }

            return $totalArchivedObjects;
        });
    }

    /**
     * Enable/disable auto-archiving on delete for the next query.
     *
     * @param boolean True if the query must archive deleted objects, false otherwise.
     */
    public function setArchiveOnDelete($archiveOnDelete)
    {
        $this->archiveOnDelete = $archiveOnDelete;
    }

    /**
     * Delete records matching the current query without archiving them.
     *
     * @param      ConnectionInterface $con    Connection to use.
     *
     * @return integer the number of deleted rows
     */
    public function deleteWithoutArchive($con = null)
    {
        $this->archiveOnDelete = false;

        return $this->delete($con);
    }

    /**
     * Delete all records without archiving them.
     *
     * @param      ConnectionInterface $con    Connection to use.
     *
     * @return integer the number of deleted rows
     */
    public function deleteAllWithoutArchive($con = null)
    {
        $this->archiveOnDelete = false;

        return $this->deleteAll($con);
    }

} // AutorisationQuery
