<?php

namespace Base;

use \GedLien as ChildGedLien;
use \GedLienQuery as ChildGedLienQuery;
use \Exception;
use \PDO;
use Map\GedLienTableMap;
use Propel\Runtime\Propel;
use Propel\Runtime\ActiveQuery\Criteria;
use Propel\Runtime\ActiveQuery\ModelCriteria;
use Propel\Runtime\ActiveQuery\ModelJoin;
use Propel\Runtime\Collection\ObjectCollection;
use Propel\Runtime\Connection\ConnectionInterface;
use Propel\Runtime\Exception\PropelException;

/**
 * Base class that represents a query for the 'assc_geds_liens' table.
 *
 *
 *
 * @method     ChildGedLienQuery orderByIdGed($order = Criteria::ASC) Order by the id_ged column
 * @method     ChildGedLienQuery orderByIdObjet($order = Criteria::ASC) Order by the id_objet column
 * @method     ChildGedLienQuery orderByObjet($order = Criteria::ASC) Order by the objet column
 * @method     ChildGedLienQuery orderByUsage($order = Criteria::ASC) Order by the usage column
 *
 * @method     ChildGedLienQuery groupByIdGed() Group by the id_ged column
 * @method     ChildGedLienQuery groupByIdObjet() Group by the id_objet column
 * @method     ChildGedLienQuery groupByObjet() Group by the objet column
 * @method     ChildGedLienQuery groupByUsage() Group by the usage column
 *
 * @method     ChildGedLienQuery leftJoin($relation) Adds a LEFT JOIN clause to the query
 * @method     ChildGedLienQuery rightJoin($relation) Adds a RIGHT JOIN clause to the query
 * @method     ChildGedLienQuery innerJoin($relation) Adds a INNER JOIN clause to the query
 *
 * @method     ChildGedLienQuery leftJoinWith($relation) Adds a LEFT JOIN clause and with to the query
 * @method     ChildGedLienQuery rightJoinWith($relation) Adds a RIGHT JOIN clause and with to the query
 * @method     ChildGedLienQuery innerJoinWith($relation) Adds a INNER JOIN clause and with to the query
 *
 * @method     ChildGedLienQuery leftJoinGed($relationAlias = null) Adds a LEFT JOIN clause to the query using the Ged relation
 * @method     ChildGedLienQuery rightJoinGed($relationAlias = null) Adds a RIGHT JOIN clause to the query using the Ged relation
 * @method     ChildGedLienQuery innerJoinGed($relationAlias = null) Adds a INNER JOIN clause to the query using the Ged relation
 *
 * @method     ChildGedLienQuery joinWithGed($joinType = Criteria::INNER_JOIN) Adds a join clause and with to the query using the Ged relation
 *
 * @method     ChildGedLienQuery leftJoinWithGed() Adds a LEFT JOIN clause and with to the query using the Ged relation
 * @method     ChildGedLienQuery rightJoinWithGed() Adds a RIGHT JOIN clause and with to the query using the Ged relation
 * @method     ChildGedLienQuery innerJoinWithGed() Adds a INNER JOIN clause and with to the query using the Ged relation
 *
 * @method     \GedQuery endUse() Finalizes a secondary criteria and merges it with its primary Criteria
 *
 * @method     ChildGedLien findOne(ConnectionInterface $con = null) Return the first ChildGedLien matching the query
 * @method     ChildGedLien findOneOrCreate(ConnectionInterface $con = null) Return the first ChildGedLien matching the query, or a new ChildGedLien object populated from the query conditions when no match is found
 *
 * @method     ChildGedLien findOneByIdGed(string $id_ged) Return the first ChildGedLien filtered by the id_ged column
 * @method     ChildGedLien findOneByIdObjet(string $id_objet) Return the first ChildGedLien filtered by the id_objet column
 * @method     ChildGedLien findOneByObjet(string $objet) Return the first ChildGedLien filtered by the objet column
 * @method     ChildGedLien findOneByUsage(string $usage) Return the first ChildGedLien filtered by the usage column *

 * @method     ChildGedLien requirePk($key, ConnectionInterface $con = null) Return the ChildGedLien by primary key and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildGedLien requireOne(ConnectionInterface $con = null) Return the first ChildGedLien matching the query and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 *
 * @method     ChildGedLien requireOneByIdGed(string $id_ged) Return the first ChildGedLien filtered by the id_ged column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildGedLien requireOneByIdObjet(string $id_objet) Return the first ChildGedLien filtered by the id_objet column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildGedLien requireOneByObjet(string $objet) Return the first ChildGedLien filtered by the objet column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildGedLien requireOneByUsage(string $usage) Return the first ChildGedLien filtered by the usage column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 *
 * @method     ChildGedLien[]|ObjectCollection find(ConnectionInterface $con = null) Return ChildGedLien objects based on current ModelCriteria
 * @method     ChildGedLien[]|ObjectCollection findByIdGed(string $id_ged) Return ChildGedLien objects filtered by the id_ged column
 * @method     ChildGedLien[]|ObjectCollection findByIdObjet(string $id_objet) Return ChildGedLien objects filtered by the id_objet column
 * @method     ChildGedLien[]|ObjectCollection findByObjet(string $objet) Return ChildGedLien objects filtered by the objet column
 * @method     ChildGedLien[]|ObjectCollection findByUsage(string $usage) Return ChildGedLien objects filtered by the usage column
 * @method     ChildGedLien[]|\Propel\Runtime\Util\PropelModelPager paginate($page = 1, $maxPerPage = 10, ConnectionInterface $con = null) Issue a SELECT query based on the current ModelCriteria and uses a page and a maximum number of results per page to compute an offset and a limit
 *
 */
abstract class GedLienQuery extends ModelCriteria
{
    protected $entityNotFoundExceptionClass = '\\Propel\\Runtime\\Exception\\EntityNotFoundException';

    /**
     * Initializes internal state of \Base\GedLienQuery object.
     *
     * @param     string $dbName The database name
     * @param     string $modelName The phpName of a model, e.g. 'Book'
     * @param     string $modelAlias The alias for the model in this query, e.g. 'b'
     */
    public function __construct($dbName = 'simplasso', $modelName = '\\GedLien', $modelAlias = null)
    {
        parent::__construct($dbName, $modelName, $modelAlias);
    }

    /**
     * Returns a new ChildGedLienQuery object.
     *
     * @param     string $modelAlias The alias of a model in the query
     * @param     Criteria $criteria Optional Criteria to build the query from
     *
     * @return ChildGedLienQuery
     */
    public static function create($modelAlias = null, Criteria $criteria = null)
    {
        if ($criteria instanceof ChildGedLienQuery) {
            return $criteria;
        }
        $query = new ChildGedLienQuery();
        if (null !== $modelAlias) {
            $query->setModelAlias($modelAlias);
        }
        if ($criteria instanceof Criteria) {
            $query->mergeWith($criteria);
        }

        return $query;
    }

    /**
     * Find object by primary key.
     * Propel uses the instance pool to skip the database if the object exists.
     * Go fast if the query is untouched.
     *
     * <code>
     * $obj = $c->findPk(array(12, 34, 56), $con);
     * </code>
     *
     * @param array[$id_ged, $id_objet, $objet] $key Primary key to use for the query
     * @param ConnectionInterface $con an optional connection object
     *
     * @return ChildGedLien|array|mixed the result, formatted by the current formatter
     */
    public function findPk($key, ConnectionInterface $con = null)
    {
        if ($key === null) {
            return null;
        }

        if ($con === null) {
            $con = Propel::getServiceContainer()->getReadConnection(GedLienTableMap::DATABASE_NAME);
        }

        $this->basePreSelect($con);

        if (
            $this->formatter || $this->modelAlias || $this->with || $this->select
            || $this->selectColumns || $this->asColumns || $this->selectModifiers
            || $this->map || $this->having || $this->joins
        ) {
            return $this->findPkComplex($key, $con);
        }

        if ((null !== ($obj = GedLienTableMap::getInstanceFromPool(serialize([(null === $key[0] || is_scalar($key[0]) || is_callable([$key[0], '__toString']) ? (string) $key[0] : $key[0]), (null === $key[1] || is_scalar($key[1]) || is_callable([$key[1], '__toString']) ? (string) $key[1] : $key[1]), (null === $key[2] || is_scalar($key[2]) || is_callable([$key[2], '__toString']) ? (string) $key[2] : $key[2])]))))) {
            // the object is already in the instance pool
            return $obj;
        }

        return $this->findPkSimple($key, $con);
    }

    /**
     * Find object by primary key using raw SQL to go fast.
     * Bypass doSelect() and the object formatter by using generated code.
     *
     * @param     mixed $key Primary key to use for the query
     * @param     ConnectionInterface $con A connection object
     *
     * @throws \Propel\Runtime\Exception\PropelException
     *
     * @return ChildGedLien A model object, or null if the key is not found
     */
    protected function findPkSimple($key, ConnectionInterface $con)
    {
        $sql = 'SELECT `id_ged`, `id_objet`, `objet`, `usage` FROM `assc_geds_liens` WHERE `id_ged` = :p0 AND `id_objet` = :p1 AND `objet` = :p2';
        try {
            $stmt = $con->prepare($sql);
            $stmt->bindValue(':p0', $key[0], PDO::PARAM_INT);
            $stmt->bindValue(':p1', $key[1], PDO::PARAM_INT);
            $stmt->bindValue(':p2', $key[2], PDO::PARAM_STR);
            $stmt->execute();
        } catch (Exception $e) {
            Propel::log($e->getMessage(), Propel::LOG_ERR);
            throw new PropelException(sprintf('Unable to execute SELECT statement [%s]', $sql), 0, $e);
        }
        $obj = null;
        if ($row = $stmt->fetch(\PDO::FETCH_NUM)) {
            /** @var ChildGedLien $obj */
            $obj = new ChildGedLien();
            $obj->hydrate($row);
            GedLienTableMap::addInstanceToPool($obj, serialize([(null === $key[0] || is_scalar($key[0]) || is_callable([$key[0], '__toString']) ? (string) $key[0] : $key[0]), (null === $key[1] || is_scalar($key[1]) || is_callable([$key[1], '__toString']) ? (string) $key[1] : $key[1]), (null === $key[2] || is_scalar($key[2]) || is_callable([$key[2], '__toString']) ? (string) $key[2] : $key[2])]));
        }
        $stmt->closeCursor();

        return $obj;
    }

    /**
     * Find object by primary key.
     *
     * @param     mixed $key Primary key to use for the query
     * @param     ConnectionInterface $con A connection object
     *
     * @return ChildGedLien|array|mixed the result, formatted by the current formatter
     */
    protected function findPkComplex($key, ConnectionInterface $con)
    {
        // As the query uses a PK condition, no limit(1) is necessary.
        $criteria = $this->isKeepQuery() ? clone $this : $this;
        $dataFetcher = $criteria
            ->filterByPrimaryKey($key)
            ->doSelect($con);

        return $criteria->getFormatter()->init($criteria)->formatOne($dataFetcher);
    }

    /**
     * Find objects by primary key
     * <code>
     * $objs = $c->findPks(array(array(12, 56), array(832, 123), array(123, 456)), $con);
     * </code>
     * @param     array $keys Primary keys to use for the query
     * @param     ConnectionInterface $con an optional connection object
     *
     * @return ObjectCollection|array|mixed the list of results, formatted by the current formatter
     */
    public function findPks($keys, ConnectionInterface $con = null)
    {
        if (null === $con) {
            $con = Propel::getServiceContainer()->getReadConnection($this->getDbName());
        }
        $this->basePreSelect($con);
        $criteria = $this->isKeepQuery() ? clone $this : $this;
        $dataFetcher = $criteria
            ->filterByPrimaryKeys($keys)
            ->doSelect($con);

        return $criteria->getFormatter()->init($criteria)->format($dataFetcher);
    }

    /**
     * Filter the query by primary key
     *
     * @param     mixed $key Primary key to use for the query
     *
     * @return $this|ChildGedLienQuery The current query, for fluid interface
     */
    public function filterByPrimaryKey($key)
    {
        $this->addUsingAlias(GedLienTableMap::COL_ID_GED, $key[0], Criteria::EQUAL);
        $this->addUsingAlias(GedLienTableMap::COL_ID_OBJET, $key[1], Criteria::EQUAL);
        $this->addUsingAlias(GedLienTableMap::COL_OBJET, $key[2], Criteria::EQUAL);

        return $this;
    }

    /**
     * Filter the query by a list of primary keys
     *
     * @param     array $keys The list of primary key to use for the query
     *
     * @return $this|ChildGedLienQuery The current query, for fluid interface
     */
    public function filterByPrimaryKeys($keys)
    {
        if (empty($keys)) {
            return $this->add(null, '1<>1', Criteria::CUSTOM);
        }
        foreach ($keys as $key) {
            $cton0 = $this->getNewCriterion(GedLienTableMap::COL_ID_GED, $key[0], Criteria::EQUAL);
            $cton1 = $this->getNewCriterion(GedLienTableMap::COL_ID_OBJET, $key[1], Criteria::EQUAL);
            $cton0->addAnd($cton1);
            $cton2 = $this->getNewCriterion(GedLienTableMap::COL_OBJET, $key[2], Criteria::EQUAL);
            $cton0->addAnd($cton2);
            $this->addOr($cton0);
        }

        return $this;
    }

    /**
     * Filter the query on the id_ged column
     *
     * Example usage:
     * <code>
     * $query->filterByIdGed(1234); // WHERE id_ged = 1234
     * $query->filterByIdGed(array(12, 34)); // WHERE id_ged IN (12, 34)
     * $query->filterByIdGed(array('min' => 12)); // WHERE id_ged > 12
     * </code>
     *
     * @see       filterByGed()
     *
     * @param     mixed $idGed The value to use as filter.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildGedLienQuery The current query, for fluid interface
     */
    public function filterByIdGed($idGed = null, $comparison = null)
    {
        if (is_array($idGed)) {
            $useMinMax = false;
            if (isset($idGed['min'])) {
                $this->addUsingAlias(GedLienTableMap::COL_ID_GED, $idGed['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($idGed['max'])) {
                $this->addUsingAlias(GedLienTableMap::COL_ID_GED, $idGed['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(GedLienTableMap::COL_ID_GED, $idGed, $comparison);
    }

    /**
     * Filter the query on the id_objet column
     *
     * Example usage:
     * <code>
     * $query->filterByIdObjet(1234); // WHERE id_objet = 1234
     * $query->filterByIdObjet(array(12, 34)); // WHERE id_objet IN (12, 34)
     * $query->filterByIdObjet(array('min' => 12)); // WHERE id_objet > 12
     * </code>
     *
     * @param     mixed $idObjet The value to use as filter.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildGedLienQuery The current query, for fluid interface
     */
    public function filterByIdObjet($idObjet = null, $comparison = null)
    {
        if (is_array($idObjet)) {
            $useMinMax = false;
            if (isset($idObjet['min'])) {
                $this->addUsingAlias(GedLienTableMap::COL_ID_OBJET, $idObjet['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($idObjet['max'])) {
                $this->addUsingAlias(GedLienTableMap::COL_ID_OBJET, $idObjet['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(GedLienTableMap::COL_ID_OBJET, $idObjet, $comparison);
    }

    /**
     * Filter the query on the objet column
     *
     * Example usage:
     * <code>
     * $query->filterByObjet('fooValue');   // WHERE objet = 'fooValue'
     * $query->filterByObjet('%fooValue%', Criteria::LIKE); // WHERE objet LIKE '%fooValue%'
     * </code>
     *
     * @param     string $objet The value to use as filter.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildGedLienQuery The current query, for fluid interface
     */
    public function filterByObjet($objet = null, $comparison = null)
    {
        if (null === $comparison) {
            if (is_array($objet)) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(GedLienTableMap::COL_OBJET, $objet, $comparison);
    }

    /**
     * Filter the query on the usage column
     *
     * Example usage:
     * <code>
     * $query->filterByUsage('fooValue');   // WHERE usage = 'fooValue'
     * $query->filterByUsage('%fooValue%', Criteria::LIKE); // WHERE usage LIKE '%fooValue%'
     * </code>
     *
     * @param     string $usage The value to use as filter.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildGedLienQuery The current query, for fluid interface
     */
    public function filterByUsage($usage = null, $comparison = null)
    {
        if (null === $comparison) {
            if (is_array($usage)) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(GedLienTableMap::COL_USAGE, $usage, $comparison);
    }

    /**
     * Filter the query by a related \Ged object
     *
     * @param \Ged|ObjectCollection $ged The related object(s) to use as filter
     * @param string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @throws \Propel\Runtime\Exception\PropelException
     *
     * @return ChildGedLienQuery The current query, for fluid interface
     */
    public function filterByGed($ged, $comparison = null)
    {
        if ($ged instanceof \Ged) {
            return $this
                ->addUsingAlias(GedLienTableMap::COL_ID_GED, $ged->getIdGed(), $comparison);
        } elseif ($ged instanceof ObjectCollection) {
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }

            return $this
                ->addUsingAlias(GedLienTableMap::COL_ID_GED, $ged->toKeyValue('PrimaryKey', 'IdGed'), $comparison);
        } else {
            throw new PropelException('filterByGed() only accepts arguments of type \Ged or Collection');
        }
    }

    /**
     * Adds a JOIN clause to the query using the Ged relation
     *
     * @param     string $relationAlias optional alias for the relation
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return $this|ChildGedLienQuery The current query, for fluid interface
     */
    public function joinGed($relationAlias = null, $joinType = Criteria::INNER_JOIN)
    {
        $tableMap = $this->getTableMap();
        $relationMap = $tableMap->getRelation('Ged');

        // create a ModelJoin object for this join
        $join = new ModelJoin();
        $join->setJoinType($joinType);
        $join->setRelationMap($relationMap, $this->useAliasInSQL ? $this->getModelAlias() : null, $relationAlias);
        if ($previousJoin = $this->getPreviousJoin()) {
            $join->setPreviousJoin($previousJoin);
        }

        // add the ModelJoin to the current object
        if ($relationAlias) {
            $this->addAlias($relationAlias, $relationMap->getRightTable()->getName());
            $this->addJoinObject($join, $relationAlias);
        } else {
            $this->addJoinObject($join, 'Ged');
        }

        return $this;
    }

    /**
     * Use the Ged relation Ged object
     *
     * @see useQuery()
     *
     * @param     string $relationAlias optional alias for the relation,
     *                                   to be used as main alias in the secondary query
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return \GedQuery A secondary query class using the current class as primary query
     */
    public function useGedQuery($relationAlias = null, $joinType = Criteria::INNER_JOIN)
    {
        return $this
            ->joinGed($relationAlias, $joinType)
            ->useQuery($relationAlias ? $relationAlias : 'Ged', '\GedQuery');
    }

    /**
     * Exclude object from result
     *
     * @param   ChildGedLien $gedLien Object to remove from the list of results
     *
     * @return $this|ChildGedLienQuery The current query, for fluid interface
     */
    public function prune($gedLien = null)
    {
        if ($gedLien) {
            $this->addCond('pruneCond0', $this->getAliasedColName(GedLienTableMap::COL_ID_GED), $gedLien->getIdGed(), Criteria::NOT_EQUAL);
            $this->addCond('pruneCond1', $this->getAliasedColName(GedLienTableMap::COL_ID_OBJET), $gedLien->getIdObjet(), Criteria::NOT_EQUAL);
            $this->addCond('pruneCond2', $this->getAliasedColName(GedLienTableMap::COL_OBJET), $gedLien->getObjet(), Criteria::NOT_EQUAL);
            $this->combine(array('pruneCond0', 'pruneCond1', 'pruneCond2'), Criteria::LOGICAL_OR);
        }

        return $this;
    }

    /**
     * Deletes all rows from the assc_geds_liens table.
     *
     * @param ConnectionInterface $con the connection to use
     * @return int The number of affected rows (if supported by underlying database driver).
     */
    public function doDeleteAll(ConnectionInterface $con = null)
    {
        if (null === $con) {
            $con = Propel::getServiceContainer()->getWriteConnection(GedLienTableMap::DATABASE_NAME);
        }

        // use transaction because $criteria could contain info
        // for more than one table or we could emulating ON DELETE CASCADE, etc.
        return $con->transaction(function () use ($con) {
            $affectedRows = 0; // initialize var to track total num of affected rows
            $affectedRows += parent::doDeleteAll($con);
            // Because this db requires some delete cascade/set null emulation, we have to
            // clear the cached instance *after* the emulation has happened (since
            // instances get re-added by the select statement contained therein).
            GedLienTableMap::clearInstancePool();
            GedLienTableMap::clearRelatedInstancePool();

            return $affectedRows;
        });
    }

    /**
     * Performs a DELETE on the database based on the current ModelCriteria
     *
     * @param ConnectionInterface $con the connection to use
     * @return int             The number of affected rows (if supported by underlying database driver).  This includes CASCADE-related rows
     *                         if supported by native driver or if emulated using Propel.
     * @throws PropelException Any exceptions caught during processing will be
     *                         rethrown wrapped into a PropelException.
     */
    public function delete(ConnectionInterface $con = null)
    {
        if (null === $con) {
            $con = Propel::getServiceContainer()->getWriteConnection(GedLienTableMap::DATABASE_NAME);
        }

        $criteria = $this;

        // Set the correct dbName
        $criteria->setDbName(GedLienTableMap::DATABASE_NAME);

        // use transaction because $criteria could contain info
        // for more than one table or we could emulating ON DELETE CASCADE, etc.
        return $con->transaction(function () use ($con, $criteria) {
            $affectedRows = 0; // initialize var to track total num of affected rows

            GedLienTableMap::removeInstanceFromPool($criteria);

            $affectedRows += ModelCriteria::delete($con);
            GedLienTableMap::clearRelatedInstancePool();

            return $affectedRows;
        });
    }

} // GedLienQuery
