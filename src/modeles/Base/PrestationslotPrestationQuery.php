<?php

namespace Base;

use \PrestationslotPrestation as ChildPrestationslotPrestation;
use \PrestationslotPrestationQuery as ChildPrestationslotPrestationQuery;
use \Exception;
use \PDO;
use Map\PrestationslotPrestationTableMap;
use Propel\Runtime\Propel;
use Propel\Runtime\ActiveQuery\Criteria;
use Propel\Runtime\ActiveQuery\ModelCriteria;
use Propel\Runtime\ActiveQuery\ModelJoin;
use Propel\Runtime\Collection\ObjectCollection;
use Propel\Runtime\Connection\ConnectionInterface;
use Propel\Runtime\Exception\PropelException;

/**
 * Base class that represents a query for the 'asso_prestationslots_prestations' table.
 *
 *
 *
 * @method     ChildPrestationslotPrestationQuery orderByIdPrestationslot($order = Criteria::ASC) Order by the id_prestationslot column
 * @method     ChildPrestationslotPrestationQuery orderByIdPrestation($order = Criteria::ASC) Order by the id_prestation column
 * @method     ChildPrestationslotPrestationQuery orderByLigne($order = Criteria::ASC) Order by the ligne column
 * @method     ChildPrestationslotPrestationQuery orderByQuantite($order = Criteria::ASC) Order by the quantite column
 * @method     ChildPrestationslotPrestationQuery orderByCreatedAt($order = Criteria::ASC) Order by the created_at column
 * @method     ChildPrestationslotPrestationQuery orderByUpdatedAt($order = Criteria::ASC) Order by the updated_at column
 *
 * @method     ChildPrestationslotPrestationQuery groupByIdPrestationslot() Group by the id_prestationslot column
 * @method     ChildPrestationslotPrestationQuery groupByIdPrestation() Group by the id_prestation column
 * @method     ChildPrestationslotPrestationQuery groupByLigne() Group by the ligne column
 * @method     ChildPrestationslotPrestationQuery groupByQuantite() Group by the quantite column
 * @method     ChildPrestationslotPrestationQuery groupByCreatedAt() Group by the created_at column
 * @method     ChildPrestationslotPrestationQuery groupByUpdatedAt() Group by the updated_at column
 *
 * @method     ChildPrestationslotPrestationQuery leftJoin($relation) Adds a LEFT JOIN clause to the query
 * @method     ChildPrestationslotPrestationQuery rightJoin($relation) Adds a RIGHT JOIN clause to the query
 * @method     ChildPrestationslotPrestationQuery innerJoin($relation) Adds a INNER JOIN clause to the query
 *
 * @method     ChildPrestationslotPrestationQuery leftJoinWith($relation) Adds a LEFT JOIN clause and with to the query
 * @method     ChildPrestationslotPrestationQuery rightJoinWith($relation) Adds a RIGHT JOIN clause and with to the query
 * @method     ChildPrestationslotPrestationQuery innerJoinWith($relation) Adds a INNER JOIN clause and with to the query
 *
 * @method     ChildPrestationslotPrestationQuery leftJoinPrestation($relationAlias = null) Adds a LEFT JOIN clause to the query using the Prestation relation
 * @method     ChildPrestationslotPrestationQuery rightJoinPrestation($relationAlias = null) Adds a RIGHT JOIN clause to the query using the Prestation relation
 * @method     ChildPrestationslotPrestationQuery innerJoinPrestation($relationAlias = null) Adds a INNER JOIN clause to the query using the Prestation relation
 *
 * @method     ChildPrestationslotPrestationQuery joinWithPrestation($joinType = Criteria::INNER_JOIN) Adds a join clause and with to the query using the Prestation relation
 *
 * @method     ChildPrestationslotPrestationQuery leftJoinWithPrestation() Adds a LEFT JOIN clause and with to the query using the Prestation relation
 * @method     ChildPrestationslotPrestationQuery rightJoinWithPrestation() Adds a RIGHT JOIN clause and with to the query using the Prestation relation
 * @method     ChildPrestationslotPrestationQuery innerJoinWithPrestation() Adds a INNER JOIN clause and with to the query using the Prestation relation
 *
 * @method     ChildPrestationslotPrestationQuery leftJoinPrestationslot($relationAlias = null) Adds a LEFT JOIN clause to the query using the Prestationslot relation
 * @method     ChildPrestationslotPrestationQuery rightJoinPrestationslot($relationAlias = null) Adds a RIGHT JOIN clause to the query using the Prestationslot relation
 * @method     ChildPrestationslotPrestationQuery innerJoinPrestationslot($relationAlias = null) Adds a INNER JOIN clause to the query using the Prestationslot relation
 *
 * @method     ChildPrestationslotPrestationQuery joinWithPrestationslot($joinType = Criteria::INNER_JOIN) Adds a join clause and with to the query using the Prestationslot relation
 *
 * @method     ChildPrestationslotPrestationQuery leftJoinWithPrestationslot() Adds a LEFT JOIN clause and with to the query using the Prestationslot relation
 * @method     ChildPrestationslotPrestationQuery rightJoinWithPrestationslot() Adds a RIGHT JOIN clause and with to the query using the Prestationslot relation
 * @method     ChildPrestationslotPrestationQuery innerJoinWithPrestationslot() Adds a INNER JOIN clause and with to the query using the Prestationslot relation
 *
 * @method     \PrestationQuery|\PrestationslotQuery endUse() Finalizes a secondary criteria and merges it with its primary Criteria
 *
 * @method     ChildPrestationslotPrestation findOne(ConnectionInterface $con = null) Return the first ChildPrestationslotPrestation matching the query
 * @method     ChildPrestationslotPrestation findOneOrCreate(ConnectionInterface $con = null) Return the first ChildPrestationslotPrestation matching the query, or a new ChildPrestationslotPrestation object populated from the query conditions when no match is found
 *
 * @method     ChildPrestationslotPrestation findOneByIdPrestationslot(string $id_prestationslot) Return the first ChildPrestationslotPrestation filtered by the id_prestationslot column
 * @method     ChildPrestationslotPrestation findOneByIdPrestation(string $id_prestation) Return the first ChildPrestationslotPrestation filtered by the id_prestation column
 * @method     ChildPrestationslotPrestation findOneByLigne(string $ligne) Return the first ChildPrestationslotPrestation filtered by the ligne column
 * @method     ChildPrestationslotPrestation findOneByQuantite(double $quantite) Return the first ChildPrestationslotPrestation filtered by the quantite column
 * @method     ChildPrestationslotPrestation findOneByCreatedAt(string $created_at) Return the first ChildPrestationslotPrestation filtered by the created_at column
 * @method     ChildPrestationslotPrestation findOneByUpdatedAt(string $updated_at) Return the first ChildPrestationslotPrestation filtered by the updated_at column *

 * @method     ChildPrestationslotPrestation requirePk($key, ConnectionInterface $con = null) Return the ChildPrestationslotPrestation by primary key and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildPrestationslotPrestation requireOne(ConnectionInterface $con = null) Return the first ChildPrestationslotPrestation matching the query and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 *
 * @method     ChildPrestationslotPrestation requireOneByIdPrestationslot(string $id_prestationslot) Return the first ChildPrestationslotPrestation filtered by the id_prestationslot column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildPrestationslotPrestation requireOneByIdPrestation(string $id_prestation) Return the first ChildPrestationslotPrestation filtered by the id_prestation column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildPrestationslotPrestation requireOneByLigne(string $ligne) Return the first ChildPrestationslotPrestation filtered by the ligne column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildPrestationslotPrestation requireOneByQuantite(double $quantite) Return the first ChildPrestationslotPrestation filtered by the quantite column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildPrestationslotPrestation requireOneByCreatedAt(string $created_at) Return the first ChildPrestationslotPrestation filtered by the created_at column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildPrestationslotPrestation requireOneByUpdatedAt(string $updated_at) Return the first ChildPrestationslotPrestation filtered by the updated_at column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 *
 * @method     ChildPrestationslotPrestation[]|ObjectCollection find(ConnectionInterface $con = null) Return ChildPrestationslotPrestation objects based on current ModelCriteria
 * @method     ChildPrestationslotPrestation[]|ObjectCollection findByIdPrestationslot(string $id_prestationslot) Return ChildPrestationslotPrestation objects filtered by the id_prestationslot column
 * @method     ChildPrestationslotPrestation[]|ObjectCollection findByIdPrestation(string $id_prestation) Return ChildPrestationslotPrestation objects filtered by the id_prestation column
 * @method     ChildPrestationslotPrestation[]|ObjectCollection findByLigne(string $ligne) Return ChildPrestationslotPrestation objects filtered by the ligne column
 * @method     ChildPrestationslotPrestation[]|ObjectCollection findByQuantite(double $quantite) Return ChildPrestationslotPrestation objects filtered by the quantite column
 * @method     ChildPrestationslotPrestation[]|ObjectCollection findByCreatedAt(string $created_at) Return ChildPrestationslotPrestation objects filtered by the created_at column
 * @method     ChildPrestationslotPrestation[]|ObjectCollection findByUpdatedAt(string $updated_at) Return ChildPrestationslotPrestation objects filtered by the updated_at column
 * @method     ChildPrestationslotPrestation[]|\Propel\Runtime\Util\PropelModelPager paginate($page = 1, $maxPerPage = 10, ConnectionInterface $con = null) Issue a SELECT query based on the current ModelCriteria and uses a page and a maximum number of results per page to compute an offset and a limit
 *
 */
abstract class PrestationslotPrestationQuery extends ModelCriteria
{
    protected $entityNotFoundExceptionClass = '\\Propel\\Runtime\\Exception\\EntityNotFoundException';

    /**
     * Initializes internal state of \Base\PrestationslotPrestationQuery object.
     *
     * @param     string $dbName The database name
     * @param     string $modelName The phpName of a model, e.g. 'Book'
     * @param     string $modelAlias The alias for the model in this query, e.g. 'b'
     */
    public function __construct($dbName = 'simplasso', $modelName = '\\PrestationslotPrestation', $modelAlias = null)
    {
        parent::__construct($dbName, $modelName, $modelAlias);
    }

    /**
     * Returns a new ChildPrestationslotPrestationQuery object.
     *
     * @param     string $modelAlias The alias of a model in the query
     * @param     Criteria $criteria Optional Criteria to build the query from
     *
     * @return ChildPrestationslotPrestationQuery
     */
    public static function create($modelAlias = null, Criteria $criteria = null)
    {
        if ($criteria instanceof ChildPrestationslotPrestationQuery) {
            return $criteria;
        }
        $query = new ChildPrestationslotPrestationQuery();
        if (null !== $modelAlias) {
            $query->setModelAlias($modelAlias);
        }
        if ($criteria instanceof Criteria) {
            $query->mergeWith($criteria);
        }

        return $query;
    }

    /**
     * Find object by primary key.
     * Propel uses the instance pool to skip the database if the object exists.
     * Go fast if the query is untouched.
     *
     * <code>
     * $obj = $c->findPk(array(12, 34), $con);
     * </code>
     *
     * @param array[$id_prestationslot, $id_prestation] $key Primary key to use for the query
     * @param ConnectionInterface $con an optional connection object
     *
     * @return ChildPrestationslotPrestation|array|mixed the result, formatted by the current formatter
     */
    public function findPk($key, ConnectionInterface $con = null)
    {
        if ($key === null) {
            return null;
        }

        if ($con === null) {
            $con = Propel::getServiceContainer()->getReadConnection(PrestationslotPrestationTableMap::DATABASE_NAME);
        }

        $this->basePreSelect($con);

        if (
            $this->formatter || $this->modelAlias || $this->with || $this->select
            || $this->selectColumns || $this->asColumns || $this->selectModifiers
            || $this->map || $this->having || $this->joins
        ) {
            return $this->findPkComplex($key, $con);
        }

        if ((null !== ($obj = PrestationslotPrestationTableMap::getInstanceFromPool(serialize([(null === $key[0] || is_scalar($key[0]) || is_callable([$key[0], '__toString']) ? (string) $key[0] : $key[0]), (null === $key[1] || is_scalar($key[1]) || is_callable([$key[1], '__toString']) ? (string) $key[1] : $key[1])]))))) {
            // the object is already in the instance pool
            return $obj;
        }

        return $this->findPkSimple($key, $con);
    }

    /**
     * Find object by primary key using raw SQL to go fast.
     * Bypass doSelect() and the object formatter by using generated code.
     *
     * @param     mixed $key Primary key to use for the query
     * @param     ConnectionInterface $con A connection object
     *
     * @throws \Propel\Runtime\Exception\PropelException
     *
     * @return ChildPrestationslotPrestation A model object, or null if the key is not found
     */
    protected function findPkSimple($key, ConnectionInterface $con)
    {
        $sql = 'SELECT `id_prestationslot`, `id_prestation`, `ligne`, `quantite`, `created_at`, `updated_at` FROM `asso_prestationslots_prestations` WHERE `id_prestationslot` = :p0 AND `id_prestation` = :p1';
        try {
            $stmt = $con->prepare($sql);
            $stmt->bindValue(':p0', $key[0], PDO::PARAM_INT);
            $stmt->bindValue(':p1', $key[1], PDO::PARAM_INT);
            $stmt->execute();
        } catch (Exception $e) {
            Propel::log($e->getMessage(), Propel::LOG_ERR);
            throw new PropelException(sprintf('Unable to execute SELECT statement [%s]', $sql), 0, $e);
        }
        $obj = null;
        if ($row = $stmt->fetch(\PDO::FETCH_NUM)) {
            /** @var ChildPrestationslotPrestation $obj */
            $obj = new ChildPrestationslotPrestation();
            $obj->hydrate($row);
            PrestationslotPrestationTableMap::addInstanceToPool($obj, serialize([(null === $key[0] || is_scalar($key[0]) || is_callable([$key[0], '__toString']) ? (string) $key[0] : $key[0]), (null === $key[1] || is_scalar($key[1]) || is_callable([$key[1], '__toString']) ? (string) $key[1] : $key[1])]));
        }
        $stmt->closeCursor();

        return $obj;
    }

    /**
     * Find object by primary key.
     *
     * @param     mixed $key Primary key to use for the query
     * @param     ConnectionInterface $con A connection object
     *
     * @return ChildPrestationslotPrestation|array|mixed the result, formatted by the current formatter
     */
    protected function findPkComplex($key, ConnectionInterface $con)
    {
        // As the query uses a PK condition, no limit(1) is necessary.
        $criteria = $this->isKeepQuery() ? clone $this : $this;
        $dataFetcher = $criteria
            ->filterByPrimaryKey($key)
            ->doSelect($con);

        return $criteria->getFormatter()->init($criteria)->formatOne($dataFetcher);
    }

    /**
     * Find objects by primary key
     * <code>
     * $objs = $c->findPks(array(array(12, 56), array(832, 123), array(123, 456)), $con);
     * </code>
     * @param     array $keys Primary keys to use for the query
     * @param     ConnectionInterface $con an optional connection object
     *
     * @return ObjectCollection|array|mixed the list of results, formatted by the current formatter
     */
    public function findPks($keys, ConnectionInterface $con = null)
    {
        if (null === $con) {
            $con = Propel::getServiceContainer()->getReadConnection($this->getDbName());
        }
        $this->basePreSelect($con);
        $criteria = $this->isKeepQuery() ? clone $this : $this;
        $dataFetcher = $criteria
            ->filterByPrimaryKeys($keys)
            ->doSelect($con);

        return $criteria->getFormatter()->init($criteria)->format($dataFetcher);
    }

    /**
     * Filter the query by primary key
     *
     * @param     mixed $key Primary key to use for the query
     *
     * @return $this|ChildPrestationslotPrestationQuery The current query, for fluid interface
     */
    public function filterByPrimaryKey($key)
    {
        $this->addUsingAlias(PrestationslotPrestationTableMap::COL_ID_PRESTATIONSLOT, $key[0], Criteria::EQUAL);
        $this->addUsingAlias(PrestationslotPrestationTableMap::COL_ID_PRESTATION, $key[1], Criteria::EQUAL);

        return $this;
    }

    /**
     * Filter the query by a list of primary keys
     *
     * @param     array $keys The list of primary key to use for the query
     *
     * @return $this|ChildPrestationslotPrestationQuery The current query, for fluid interface
     */
    public function filterByPrimaryKeys($keys)
    {
        if (empty($keys)) {
            return $this->add(null, '1<>1', Criteria::CUSTOM);
        }
        foreach ($keys as $key) {
            $cton0 = $this->getNewCriterion(PrestationslotPrestationTableMap::COL_ID_PRESTATIONSLOT, $key[0], Criteria::EQUAL);
            $cton1 = $this->getNewCriterion(PrestationslotPrestationTableMap::COL_ID_PRESTATION, $key[1], Criteria::EQUAL);
            $cton0->addAnd($cton1);
            $this->addOr($cton0);
        }

        return $this;
    }

    /**
     * Filter the query on the id_prestationslot column
     *
     * Example usage:
     * <code>
     * $query->filterByIdPrestationslot(1234); // WHERE id_prestationslot = 1234
     * $query->filterByIdPrestationslot(array(12, 34)); // WHERE id_prestationslot IN (12, 34)
     * $query->filterByIdPrestationslot(array('min' => 12)); // WHERE id_prestationslot > 12
     * </code>
     *
     * @see       filterByPrestationslot()
     *
     * @param     mixed $idPrestationslot The value to use as filter.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildPrestationslotPrestationQuery The current query, for fluid interface
     */
    public function filterByIdPrestationslot($idPrestationslot = null, $comparison = null)
    {
        if (is_array($idPrestationslot)) {
            $useMinMax = false;
            if (isset($idPrestationslot['min'])) {
                $this->addUsingAlias(PrestationslotPrestationTableMap::COL_ID_PRESTATIONSLOT, $idPrestationslot['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($idPrestationslot['max'])) {
                $this->addUsingAlias(PrestationslotPrestationTableMap::COL_ID_PRESTATIONSLOT, $idPrestationslot['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(PrestationslotPrestationTableMap::COL_ID_PRESTATIONSLOT, $idPrestationslot, $comparison);
    }

    /**
     * Filter the query on the id_prestation column
     *
     * Example usage:
     * <code>
     * $query->filterByIdPrestation(1234); // WHERE id_prestation = 1234
     * $query->filterByIdPrestation(array(12, 34)); // WHERE id_prestation IN (12, 34)
     * $query->filterByIdPrestation(array('min' => 12)); // WHERE id_prestation > 12
     * </code>
     *
     * @see       filterByPrestation()
     *
     * @param     mixed $idPrestation The value to use as filter.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildPrestationslotPrestationQuery The current query, for fluid interface
     */
    public function filterByIdPrestation($idPrestation = null, $comparison = null)
    {
        if (is_array($idPrestation)) {
            $useMinMax = false;
            if (isset($idPrestation['min'])) {
                $this->addUsingAlias(PrestationslotPrestationTableMap::COL_ID_PRESTATION, $idPrestation['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($idPrestation['max'])) {
                $this->addUsingAlias(PrestationslotPrestationTableMap::COL_ID_PRESTATION, $idPrestation['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(PrestationslotPrestationTableMap::COL_ID_PRESTATION, $idPrestation, $comparison);
    }

    /**
     * Filter the query on the ligne column
     *
     * Example usage:
     * <code>
     * $query->filterByLigne(1234); // WHERE ligne = 1234
     * $query->filterByLigne(array(12, 34)); // WHERE ligne IN (12, 34)
     * $query->filterByLigne(array('min' => 12)); // WHERE ligne > 12
     * </code>
     *
     * @param     mixed $ligne The value to use as filter.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildPrestationslotPrestationQuery The current query, for fluid interface
     */
    public function filterByLigne($ligne = null, $comparison = null)
    {
        if (is_array($ligne)) {
            $useMinMax = false;
            if (isset($ligne['min'])) {
                $this->addUsingAlias(PrestationslotPrestationTableMap::COL_LIGNE, $ligne['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($ligne['max'])) {
                $this->addUsingAlias(PrestationslotPrestationTableMap::COL_LIGNE, $ligne['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(PrestationslotPrestationTableMap::COL_LIGNE, $ligne, $comparison);
    }

    /**
     * Filter the query on the quantite column
     *
     * Example usage:
     * <code>
     * $query->filterByQuantite(1234); // WHERE quantite = 1234
     * $query->filterByQuantite(array(12, 34)); // WHERE quantite IN (12, 34)
     * $query->filterByQuantite(array('min' => 12)); // WHERE quantite > 12
     * </code>
     *
     * @param     mixed $quantite The value to use as filter.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildPrestationslotPrestationQuery The current query, for fluid interface
     */
    public function filterByQuantite($quantite = null, $comparison = null)
    {
        if (is_array($quantite)) {
            $useMinMax = false;
            if (isset($quantite['min'])) {
                $this->addUsingAlias(PrestationslotPrestationTableMap::COL_QUANTITE, $quantite['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($quantite['max'])) {
                $this->addUsingAlias(PrestationslotPrestationTableMap::COL_QUANTITE, $quantite['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(PrestationslotPrestationTableMap::COL_QUANTITE, $quantite, $comparison);
    }

    /**
     * Filter the query on the created_at column
     *
     * Example usage:
     * <code>
     * $query->filterByCreatedAt('2011-03-14'); // WHERE created_at = '2011-03-14'
     * $query->filterByCreatedAt('now'); // WHERE created_at = '2011-03-14'
     * $query->filterByCreatedAt(array('max' => 'yesterday')); // WHERE created_at > '2011-03-13'
     * </code>
     *
     * @param     mixed $createdAt The value to use as filter.
     *              Values can be integers (unix timestamps), DateTime objects, or strings.
     *              Empty strings are treated as NULL.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildPrestationslotPrestationQuery The current query, for fluid interface
     */
    public function filterByCreatedAt($createdAt = null, $comparison = null)
    {
        if (is_array($createdAt)) {
            $useMinMax = false;
            if (isset($createdAt['min'])) {
                $this->addUsingAlias(PrestationslotPrestationTableMap::COL_CREATED_AT, $createdAt['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($createdAt['max'])) {
                $this->addUsingAlias(PrestationslotPrestationTableMap::COL_CREATED_AT, $createdAt['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(PrestationslotPrestationTableMap::COL_CREATED_AT, $createdAt, $comparison);
    }

    /**
     * Filter the query on the updated_at column
     *
     * Example usage:
     * <code>
     * $query->filterByUpdatedAt('2011-03-14'); // WHERE updated_at = '2011-03-14'
     * $query->filterByUpdatedAt('now'); // WHERE updated_at = '2011-03-14'
     * $query->filterByUpdatedAt(array('max' => 'yesterday')); // WHERE updated_at > '2011-03-13'
     * </code>
     *
     * @param     mixed $updatedAt The value to use as filter.
     *              Values can be integers (unix timestamps), DateTime objects, or strings.
     *              Empty strings are treated as NULL.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildPrestationslotPrestationQuery The current query, for fluid interface
     */
    public function filterByUpdatedAt($updatedAt = null, $comparison = null)
    {
        if (is_array($updatedAt)) {
            $useMinMax = false;
            if (isset($updatedAt['min'])) {
                $this->addUsingAlias(PrestationslotPrestationTableMap::COL_UPDATED_AT, $updatedAt['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($updatedAt['max'])) {
                $this->addUsingAlias(PrestationslotPrestationTableMap::COL_UPDATED_AT, $updatedAt['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(PrestationslotPrestationTableMap::COL_UPDATED_AT, $updatedAt, $comparison);
    }

    /**
     * Filter the query by a related \Prestation object
     *
     * @param \Prestation|ObjectCollection $prestation The related object(s) to use as filter
     * @param string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @throws \Propel\Runtime\Exception\PropelException
     *
     * @return ChildPrestationslotPrestationQuery The current query, for fluid interface
     */
    public function filterByPrestation($prestation, $comparison = null)
    {
        if ($prestation instanceof \Prestation) {
            return $this
                ->addUsingAlias(PrestationslotPrestationTableMap::COL_ID_PRESTATION, $prestation->getIdPrestation(), $comparison);
        } elseif ($prestation instanceof ObjectCollection) {
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }

            return $this
                ->addUsingAlias(PrestationslotPrestationTableMap::COL_ID_PRESTATION, $prestation->toKeyValue('PrimaryKey', 'IdPrestation'), $comparison);
        } else {
            throw new PropelException('filterByPrestation() only accepts arguments of type \Prestation or Collection');
        }
    }

    /**
     * Adds a JOIN clause to the query using the Prestation relation
     *
     * @param     string $relationAlias optional alias for the relation
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return $this|ChildPrestationslotPrestationQuery The current query, for fluid interface
     */
    public function joinPrestation($relationAlias = null, $joinType = Criteria::INNER_JOIN)
    {
        $tableMap = $this->getTableMap();
        $relationMap = $tableMap->getRelation('Prestation');

        // create a ModelJoin object for this join
        $join = new ModelJoin();
        $join->setJoinType($joinType);
        $join->setRelationMap($relationMap, $this->useAliasInSQL ? $this->getModelAlias() : null, $relationAlias);
        if ($previousJoin = $this->getPreviousJoin()) {
            $join->setPreviousJoin($previousJoin);
        }

        // add the ModelJoin to the current object
        if ($relationAlias) {
            $this->addAlias($relationAlias, $relationMap->getRightTable()->getName());
            $this->addJoinObject($join, $relationAlias);
        } else {
            $this->addJoinObject($join, 'Prestation');
        }

        return $this;
    }

    /**
     * Use the Prestation relation Prestation object
     *
     * @see useQuery()
     *
     * @param     string $relationAlias optional alias for the relation,
     *                                   to be used as main alias in the secondary query
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return \PrestationQuery A secondary query class using the current class as primary query
     */
    public function usePrestationQuery($relationAlias = null, $joinType = Criteria::INNER_JOIN)
    {
        return $this
            ->joinPrestation($relationAlias, $joinType)
            ->useQuery($relationAlias ? $relationAlias : 'Prestation', '\PrestationQuery');
    }

    /**
     * Filter the query by a related \Prestationslot object
     *
     * @param \Prestationslot|ObjectCollection $prestationslot The related object(s) to use as filter
     * @param string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @throws \Propel\Runtime\Exception\PropelException
     *
     * @return ChildPrestationslotPrestationQuery The current query, for fluid interface
     */
    public function filterByPrestationslot($prestationslot, $comparison = null)
    {
        if ($prestationslot instanceof \Prestationslot) {
            return $this
                ->addUsingAlias(PrestationslotPrestationTableMap::COL_ID_PRESTATIONSLOT, $prestationslot->getIdPrestationslot(), $comparison);
        } elseif ($prestationslot instanceof ObjectCollection) {
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }

            return $this
                ->addUsingAlias(PrestationslotPrestationTableMap::COL_ID_PRESTATIONSLOT, $prestationslot->toKeyValue('PrimaryKey', 'IdPrestationslot'), $comparison);
        } else {
            throw new PropelException('filterByPrestationslot() only accepts arguments of type \Prestationslot or Collection');
        }
    }

    /**
     * Adds a JOIN clause to the query using the Prestationslot relation
     *
     * @param     string $relationAlias optional alias for the relation
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return $this|ChildPrestationslotPrestationQuery The current query, for fluid interface
     */
    public function joinPrestationslot($relationAlias = null, $joinType = Criteria::INNER_JOIN)
    {
        $tableMap = $this->getTableMap();
        $relationMap = $tableMap->getRelation('Prestationslot');

        // create a ModelJoin object for this join
        $join = new ModelJoin();
        $join->setJoinType($joinType);
        $join->setRelationMap($relationMap, $this->useAliasInSQL ? $this->getModelAlias() : null, $relationAlias);
        if ($previousJoin = $this->getPreviousJoin()) {
            $join->setPreviousJoin($previousJoin);
        }

        // add the ModelJoin to the current object
        if ($relationAlias) {
            $this->addAlias($relationAlias, $relationMap->getRightTable()->getName());
            $this->addJoinObject($join, $relationAlias);
        } else {
            $this->addJoinObject($join, 'Prestationslot');
        }

        return $this;
    }

    /**
     * Use the Prestationslot relation Prestationslot object
     *
     * @see useQuery()
     *
     * @param     string $relationAlias optional alias for the relation,
     *                                   to be used as main alias in the secondary query
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return \PrestationslotQuery A secondary query class using the current class as primary query
     */
    public function usePrestationslotQuery($relationAlias = null, $joinType = Criteria::INNER_JOIN)
    {
        return $this
            ->joinPrestationslot($relationAlias, $joinType)
            ->useQuery($relationAlias ? $relationAlias : 'Prestationslot', '\PrestationslotQuery');
    }

    /**
     * Exclude object from result
     *
     * @param   ChildPrestationslotPrestation $prestationslotPrestation Object to remove from the list of results
     *
     * @return $this|ChildPrestationslotPrestationQuery The current query, for fluid interface
     */
    public function prune($prestationslotPrestation = null)
    {
        if ($prestationslotPrestation) {
            $this->addCond('pruneCond0', $this->getAliasedColName(PrestationslotPrestationTableMap::COL_ID_PRESTATIONSLOT), $prestationslotPrestation->getIdPrestationslot(), Criteria::NOT_EQUAL);
            $this->addCond('pruneCond1', $this->getAliasedColName(PrestationslotPrestationTableMap::COL_ID_PRESTATION), $prestationslotPrestation->getIdPrestation(), Criteria::NOT_EQUAL);
            $this->combine(array('pruneCond0', 'pruneCond1'), Criteria::LOGICAL_OR);
        }

        return $this;
    }

    /**
     * Deletes all rows from the asso_prestationslots_prestations table.
     *
     * @param ConnectionInterface $con the connection to use
     * @return int The number of affected rows (if supported by underlying database driver).
     */
    public function doDeleteAll(ConnectionInterface $con = null)
    {
        if (null === $con) {
            $con = Propel::getServiceContainer()->getWriteConnection(PrestationslotPrestationTableMap::DATABASE_NAME);
        }

        // use transaction because $criteria could contain info
        // for more than one table or we could emulating ON DELETE CASCADE, etc.
        return $con->transaction(function () use ($con) {
            $affectedRows = 0; // initialize var to track total num of affected rows
            $affectedRows += parent::doDeleteAll($con);
            // Because this db requires some delete cascade/set null emulation, we have to
            // clear the cached instance *after* the emulation has happened (since
            // instances get re-added by the select statement contained therein).
            PrestationslotPrestationTableMap::clearInstancePool();
            PrestationslotPrestationTableMap::clearRelatedInstancePool();

            return $affectedRows;
        });
    }

    /**
     * Performs a DELETE on the database based on the current ModelCriteria
     *
     * @param ConnectionInterface $con the connection to use
     * @return int             The number of affected rows (if supported by underlying database driver).  This includes CASCADE-related rows
     *                         if supported by native driver or if emulated using Propel.
     * @throws PropelException Any exceptions caught during processing will be
     *                         rethrown wrapped into a PropelException.
     */
    public function delete(ConnectionInterface $con = null)
    {
        if (null === $con) {
            $con = Propel::getServiceContainer()->getWriteConnection(PrestationslotPrestationTableMap::DATABASE_NAME);
        }

        $criteria = $this;

        // Set the correct dbName
        $criteria->setDbName(PrestationslotPrestationTableMap::DATABASE_NAME);

        // use transaction because $criteria could contain info
        // for more than one table or we could emulating ON DELETE CASCADE, etc.
        return $con->transaction(function () use ($con, $criteria) {
            $affectedRows = 0; // initialize var to track total num of affected rows

            PrestationslotPrestationTableMap::removeInstanceFromPool($criteria);

            $affectedRows += ModelCriteria::delete($con);
            PrestationslotPrestationTableMap::clearRelatedInstancePool();

            return $affectedRows;
        });
    }

    // timestampable behavior

    /**
     * Filter by the latest updated
     *
     * @param      int $nbDays Maximum age of the latest update in days
     *
     * @return     $this|ChildPrestationslotPrestationQuery The current query, for fluid interface
     */
    public function recentlyUpdated($nbDays = 7)
    {
        return $this->addUsingAlias(PrestationslotPrestationTableMap::COL_UPDATED_AT, time() - $nbDays * 24 * 60 * 60, Criteria::GREATER_EQUAL);
    }

    /**
     * Order by update date desc
     *
     * @return     $this|ChildPrestationslotPrestationQuery The current query, for fluid interface
     */
    public function lastUpdatedFirst()
    {
        return $this->addDescendingOrderByColumn(PrestationslotPrestationTableMap::COL_UPDATED_AT);
    }

    /**
     * Order by update date asc
     *
     * @return     $this|ChildPrestationslotPrestationQuery The current query, for fluid interface
     */
    public function firstUpdatedFirst()
    {
        return $this->addAscendingOrderByColumn(PrestationslotPrestationTableMap::COL_UPDATED_AT);
    }

    /**
     * Order by create date desc
     *
     * @return     $this|ChildPrestationslotPrestationQuery The current query, for fluid interface
     */
    public function lastCreatedFirst()
    {
        return $this->addDescendingOrderByColumn(PrestationslotPrestationTableMap::COL_CREATED_AT);
    }

    /**
     * Filter by the latest created
     *
     * @param      int $nbDays Maximum age of in days
     *
     * @return     $this|ChildPrestationslotPrestationQuery The current query, for fluid interface
     */
    public function recentlyCreated($nbDays = 7)
    {
        return $this->addUsingAlias(PrestationslotPrestationTableMap::COL_CREATED_AT, time() - $nbDays * 24 * 60 * 60, Criteria::GREATER_EQUAL);
    }

    /**
     * Order by create date asc
     *
     * @return     $this|ChildPrestationslotPrestationQuery The current query, for fluid interface
     */
    public function firstCreatedFirst()
    {
        return $this->addAscendingOrderByColumn(PrestationslotPrestationTableMap::COL_CREATED_AT);
    }

} // PrestationslotPrestationQuery
