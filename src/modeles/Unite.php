<?php

use Base\Unite as BaseUnite;

/**
 * Skeleton subclass for representing a row from the 'asso_unites' table.
 *
 *
 *
 * You should add additional methods to this class to meet the
 * application requirements.  This class will only be generated as
 * long as it does not already exist in the output directory.
 *
 */
class Unite extends BaseUnite
{
    public function getQuiCree(){
        return Log::getQuiCree('unite',$this->id_unite);
    }

    public function getQuiModifie(){
        return Log::getQuiModifie('unite',$this->id_unite);
    }

}
