<?php

use Base\Paiement as BasePaiement;

/**
 * Skeleton subclass for representing a row from the 'asso_paiements' table.
 *
 *
 *
 * You should add additional methods to this class to meet the
 * application requirements.  This class will only be generated as
 * long as it does not already exist in the output directory.
 *
 */
class Paiement extends BasePaiement
{




    public function getNomTitulaire(){

        $nom='';
        switch($this->getObjet()){
            case 'individu':
                $nom = IndividuQuery::create()->findPk($this->getIdObjet())->getNom();
                break;
            case 'membre':
                $nom = MembreQuery::create()->findPk($this->getIdObjet())->getNom();
                break;
        }

        return $nom;
    }


    public function getQuiCree()
    {
        return Log::getQuiCree('paiement', $this->id_paiement);
    }

    public function getQuiModifie()
    {
        return Log::getQuiModifie('paiement', $this->id_paiement);
    }


}
