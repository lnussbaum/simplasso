<?php

use Base\ZoneQuery as BaseZoneQuery;

/**
 * Skeleton subclass for performing query and update operations on the 'geo_zones' table.
 *
 *
 *
 * You should add additional methods to this class to meet the
 * application requirements.  This class will only be generated as
 * long as it does not already exist in the output directory.
 *
 */
class ZoneQuery extends BaseZoneQuery
{


    public static $champs_recherche = ['nom','descriptif'];

    public static function getWhere($params = array()) {
       list($from, $where) = getWhereObjet('zone', $params);
        $pr = descr('zone.nom_sql');
        if ($id_zonegroupe = request_ou_options('id_zonegroupe', $params)){
                    $where .= ' AND '.$pr.'.id_zonegroupe ='.$id_zonegroupe;
            };
        return [$from, $where];
    }

    public static function getAll($tab_id, $order = array(), $offset = 0, $limit = 0) {
        return getAllObjet('zone', $tab_id, $order, $offset, $limit);
    }

}
