<?php

use Base\Tache as BaseTache;

/**
 * Skeleton subclass for representing a row from the 'asso_taches' table.
 *
 *
 *
 * You should add additional methods to this class to meet the
 * application requirements.  This class will only be generated as
 * long as it does not already exist in the output directory.
 *
 */
class Tache extends BaseTache
{
    public function getQuiCree() {
        return Log::getQuiCree('zone', $this->id_zone);
    }

    public function getQuiModifie() {
        return Log::getQuiModifie('zone', $this->id_zone);
    }

    function execute(){
        global $app;
        $fichier_fonction = $this->getFonction();
        $fonction = 'tache_'.$this->getFonction();
        include($app['basepath'] . '/src/tache/'.$fichier_fonction.'.php');
        $this->setStatut(1)->save();
        $args=$this->getArgs();
        if (!empty($args))
            $fini = $fonction($args);
        else
            $fini = $fonction();
        if($fini)
            $this->setStatut(2)->save();
        else
            $this->setStatut(1)->save();
    }

    function getArgs() {
        return json_decode($this->args,true);
    }

    function setArgs($args) {
        return parent::setArgs(json_encode($args));
    }


}
