<?php

use Base\AssoTresorsArchiveQuery as BaseAssoTresorsArchiveQuery;

/**
 * Skeleton subclass for performing query and update operations on the 'asso_tresors_archive' table.
 *
 *
 *
 * You should add additional methods to this class to meet the
 * application requirements.  This class will only be generated as
 * long as it does not already exist in the output directory.
 *
 */
class AssoTresorsArchiveQuery extends BaseAssoTresorsArchiveQuery
{

}
