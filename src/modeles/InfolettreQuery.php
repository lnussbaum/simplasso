<?php

use Base\InfolettreQuery as BaseInfolettreQuery;

/**
 * Skeleton subclass for performing query and update operations on the 'com_infolettres' table.
 *
 *
 *
 * You should add additional methods to this class to meet the
 * application requirements.  This class will only be generated as
 * long as it does not already exist in the output directory.
 *
 */
class InfolettreQuery extends BaseInfolettreQuery
{
    public static $champs_recherche = ['nom','descriptif'];


    public static function getAll($tab_id, $order = array(), $offset = 0, $limit = 0) {
        return getAllObjet('infolettre', $tab_id, $order, $offset, $limit);
    }

}
