<?php

use Base\CompositionsBloc as BaseCompositionsBloc;

/**
 * Skeleton subclass for representing a row from the 'com_compositions_blocs' table.
 *
 *
 *
 * You should add additional methods to this class to meet the
 * application requirements.  This class will only be generated as
 * long as it does not already exist in the output directory.
 *
 */
class CompositionsBloc extends BaseCompositionsBloc
{

}
