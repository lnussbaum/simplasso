<?php

use Base\ServicerenduQuery as BaseServicerenduQuery;
use Propel\Runtime\ActiveQuery\Criteria as Criteria;
/**
 * Skeleton subclass for performing query and update operations on the 'asso_servicerendus' table.
 *
 *
 *
 * You should add additional methods to this class to meet the
 * application requirements.  This class will only be generated as
 * long as it does not already exist in the output directory.
 *
 */
class ServicerenduQuery extends BaseServicerenduQuery
{

    public static $champs_recherche = [];

    static function getAll($filtre_id=array(),$order='',$offset=0,$limit=0){
        return  ServicerenduQuery::getAllByTypePrestation('',$filtre_id,$order,$offset,$limit);
    }


    static function getAllByTypePrestation($alias='',$filtre_id=array(),$order='',$offset=0,$limit=0,$options=array()){

        if (!(empty($alias) or $alias==='servicerendu')){
            $prestation_type = getValeurObjetAlias('servicerendu',$alias);
            if(!empty($prestation_type)){
                $tab_prestation = array_keys(getPrestationDeType($prestation_type));
            }
        }




        $q = ServicerenduQuery::create();

        $q = $q
            ->distinct()
            ->usePrestationQuery()
            ->endUse()
            ->useServicepaiementQuery()
            ->endUse()
            ->leftJoin('Servicepaiement');
        if (!empty($filtre_id)){
            $q = $q->filterByPrimaryKeys($filtre_id);
        }
        if (!empty($tab_prestation)){
            $q = $q->filterByIdPrestation($tab_prestation);
        }
        if (isset($options['date_debut'])&& is_a($options['date_debut'],'DateTime'))
            $q = $q->filterByDateEnregistrement($options['date_debut'],Criteria::GREATER_EQUAL);
        if (isset($options['date_fin']) && is_a($options['date_fin'],'DateTime'))
            $q = $q->filterByDateEnregistrement($options['date_fin'],Criteria::LESS_THAN);


        if (!empty($order) && is_array($order)) {
            foreach($order as $k=>$o){
                $q=$q->orderBy($k,$o);
            }
        }
        if ($offset > 0)
            $q = $q->offset($offset);
        if ($limit > 0)
            $q = $q->limit($limit);

        return    $q->find();
    }


    public static function getBy($id_entite,$tab_id_membre,$tab_id_prestation,$date_debut,$date_fin){

        global $app;
        $tab=[];
        list($from,$where) = ServicerenduQuery::getWhere([
            'id_entite'=>$id_entite,
            'id_membre'=>$tab_id_membre,
            'id_prestation'=>$tab_id_prestation,
            'periode'=>$date_debut.' - '.$date_fin
        ]);
        $tab_nomcourt_prestation = table_simplifier(tab('prestation'),'nomcourt');
        $pr=descr('servicerendu.nom_sql');
        $statement =  $app['db']->executeQuery('SELECT '.$pr.'.id_membre as id_membre,'.$pr.'.id_prestation as id_prestation FROM '.implode(',', array_values($from)).' WHERE '.$where);
        while($sr = $statement->fetch()){
            $tab[$sr['id_membre']][]=$tab_nomcourt_prestation[$sr['id_prestation']];

        }
        return $tab;

    }


    public static function getNbPouvoir($id_entite,$tab_id_membre,$tab_id_prestation,$date_pouvoir){

        global $app;
        $tab=[];
        list($from,$where) = ServicerenduQuery::getWhere([
            'id_entite'=>$id_entite,
            'id_membre'=>$tab_id_membre,
            'id_prestation'=>$tab_id_prestation,
            'date'=>$date_pouvoir
        ]);
        $tab_nomcourt_prestation = table_simplifier(tab('prestation'),'nb_voix');
        $pr=descr('servicerendu.nom_sql');
        $statement =  $app['db']->executeQuery('SELECT '.$pr.'.id_membre as id_membre,'.$pr.'.id_prestation as id_prestation FROM '.implode(',', array_values($from)).' WHERE '.$where);
        while($sr = $statement->fetch()){
            if(!isset($tab[$sr['id_membre']])) $tab[$sr['id_membre']] = 0;
            $tab[$sr['id_membre']] += $tab_nomcourt_prestation[$sr['id_prestation']];

        }
        return $tab;

    }



    public static function getWhere($params = array())
    {
    $objet='servicerendu';
        list($from, $where)  = getWhereObjet($objet,$params);
        $cle = descr('servicerendu.cle_sql');

        $pr=descr('servicerendu.nom_sql');

        $where .= objet_selection_restriction_mot('servicerendu');

        if ($search = request_ou_options('search', $params)) {
            $recherche = trim($search['value']);

            if (!empty($recherche)) {
                $where .= ' AND (';
                if (intval($recherche) > 0) {
                    $where .=  $cle . ' = \'' . $recherche . '\' OR';
                }
                $recherche_m = '+'.str_replace(' ',' +',str_replace(array('@','.'),' ',$recherche))."*";
                $from[] = 'asso_membres am';
                $where .=   ' am.id_membre  = '.$pr.'.id_membre and( am.nom like \'%'.$recherche.'%\'';
                $where .= ' OR am.id_membre IN (select al.id_membre from asso_individus as `a`, asso_membres_individus as `al` where a.id_individu = al.id_individu'
                    . ' AND ( email like \'%'.$recherche.'%\'  OR ( MATCH (nom_famille,prenom) AGAINST (\'' . $recherche_m . '\' IN BOOLEAN MODE))))))';
            }
        }

        if (!empty($params['sql'])){
            if(!is_array($params['sql']))
                $params['sql']=[$params['sql']];
            $where .= ' AND '.$pr.'.'.implode(' AND '.$pr.'.',$params['sql']);
        }


        if ($prestation_type = request_ou_options('prestation_type', $params)) {
            $from[] = 'asso_prestations p';
            $where .=' AND '.$pr.'.id_prestation = p.id_prestation AND p.prestation_type='.$prestation_type;
        }

        if ($id_entite = request_ou_options('id_entite', $params)) {
            $id_entite = is_array($id_entite)?$id_entite:[$id_entite];
            $where .= ' AND '.$pr.'.id_entite IN ('.implode(',',$id_entite).')';
        }


        if ($id_prestation = request_ou_options('id_prestation', $params)) {
            $id_prestation = is_array($id_prestation)?$id_prestation:[$id_prestation];
            $where .= ' AND '.$pr.'.id_prestation IN ('.implode(',',$id_prestation).')';
        }

        if ($id_membre = request_ou_options('id_membre', $params)) {
            $id_membre = is_array($id_membre)?$id_membre:[$id_membre];
            $where .= ' AND '.$pr.'.id_membre IN ('.implode(',',$id_membre).')';
        }

        if ($id_individu = request_ou_options('id_individu', $params)) {
            $id_individu = is_array($id_individu)?$id_individu:[$id_individu];
            $from[] = 'asso_servicerendus_individus sri';
            $where .= ' AND '.$pr.'.id_servicerendu = sri.id_servicerendu AND sri.id_individu IN ('.implode(',',$id_individu).')';
        }


        if ($date = request_ou_options('date', $params)) {
            $where .= ' AND ( CAST(\''.$date.'\' AS DATE) >= date_debut and CAST(\''.$date.'\' AS DATE) <=  date_fin )';
        }


        if ($periode = request_ou_options('periode', $params)) {
            list($date_debut,$date_fin) = explode(' - ',$periode);
            $where .= ' AND (( CAST(\''.$date_debut.'\' AS DATE) >= date_debut and CAST(\''.$date_debut.'\' AS DATE) <=  date_fin )';
            $where .= ' OR ( date_debut <= CAST(\''.$date_debut.'\' AS DATE) and  date_debut >= CAST(\''.$date_fin.'\' AS DATE)))';

        }


        $where_origine = ' AND '.$pr.'.origine is NULL';
        if ($origine_value = request_ou_options('origine', $params)) {
            $origine_value = is_array($origine_value)?$origine_value:[$origine_value];
            $where_origine = ' AND '.$pr.'.origine IN ('.implode(',',$origine_value).')';
        }
        $where .= $where_origine;

        return [$from, $where] ;


    }







    static function getSuper($tab_id_membre)
    {
        global $app;
        $result=array();

        $q = ServicerenduQuery::create()->filterByPrimaryKeys($tab_id_membre)
            ->usePrestationQuery()
            ->endUse()
            ->leftJoin('Prestation')
            ->useServicepaiementQuery()
            ->endUse()
            ->leftJoin('Servicepaiement')
            ->useMembreQuery()
            ->endUse()
            ->leftJoin('Membre');

        if (!empty($order) && is_array($order)) {
            foreach($order as $k=>$o){
                $q=$q->orderBy($k,strtoupper($o));
            }

        }
        /*
                $sql = 'SELECT sr.created_at  ,sr.date_debut as date_debut,sr.date_fin as date_fin,sr.id_service_rendu ,en.nom as nom,pr.prestationtype as prestation_type ,sr.regle as regle , sr.montant,  sum(sp.montant) as spmt
                        FROM asso_servicerendus sr left join asso_servicepaiements sp on sr.id_servicerendu=sp.id_servicerendu left join asso_entites en on  sr.id_entite=en.id_entite  left join asso_prestations pr on sr.id_prestation=pr.id_prestation
                        WHERE sr.id_membre=' . $contex['idinit']['id_membre'] . ' group by sr.id_servicerendu order by sr.date_fin';
                $res2 = $app['db']->fetchAll($sql);
                foreach ($res2 as $e) {
                    $result[$e['id']] = array(
                        'nom' => addslashes($e['nom']),
                        'montant' => $e['srmt'],
                        'type' => $e['prestation_type']
                    );
                }*/

        return $q->find();
    }




    static function listeServiceRenduEtSommePaiementDUnMembre($id_membre)
    {
        global $app;

        $sql = 'SELECT distinct DATE_FORMAT(sr.date_debut,"%d/%m/%Y") AS tdd,DATE_FORMAT(sr.date_fin,"%d/%m/%Y") AS tdf,
            sr.id_servicerendu ,sr.id_entite ,en.nom as nomen, pr.nom as nompr
            ,pr.prestation_type as prestationtype,pr.prixlibre as prixlibre ,sr.regle as regle , sr.montant*sr.quantite as sr_total,  sum(sp.montant) as montantpaye
            FROM asso_servicerendus sr left join asso_servicepaiements sp on sr.id_servicerendu=sp.id_servicerendu left join asso_entites en on  sr.id_entite=en.id_entite  left join asso_prestations pr on sr.id_prestation=pr.id_prestation
            WHERE  sr.id_membre=' . $id_membre . ' and sr.id_entite IN  (' . implode(',', suc('entite')) . ')  group by sr.id_servicerendu order by sr.id_servicerendu';
        return $app['db']->fetchAll($sql);
    }


    static function listeDesServiceRenduPourUnPaiement($id_paiement,$id_membre=null)
    {

        global $app;
        $sql =  'SELECT distinct DATE_FORMAT(sr.date_debut,"%d/%m/%Y") AS tdd,DATE_FORMAT(sr.date_fin,"%d/%m/%Y") AS tdf,
                sr.id_servicerendu ,sr.id_entite ,en.nom as nomen, pr.nom as nompr
                ,pr.prestation_type as prestationtype,pr.prixlibre as prixlibre ,sr.regle as regle , sr.montant,  sp.montant as montantpaye,sp.id_servicepaiement as id_servicepaiement
                FROM asso_servicerendus sr left join asso_servicepaiements sp on sr.id_servicerendu=sp.id_servicerendu left join asso_entites en on  sr.id_entite=en.id_entite  left join asso_prestations pr on sr.id_prestation=pr.id_prestation';
        $sql .= ($id_membre)? ' WHERE sr.id_membre=' . $id_membre :' WHERE 1=1';
        if (!empty($id_paiement)) {
            $sql .= ' AND  sr.id_servicerendu=' . $id_paiement ;
            $sql .= ' ORDER BY sr.date_fin';
        }
        else{
            $sql='';
        }
        if (!empty($sql))
            return $app['db']->fetchAll($sql);
        return false;
    }



}
