<?php

use Base\PositionQuery as BasePositionQuery;

/**
 * Skeleton subclass for performing query and update operations on the 'geo_positions' table.
 *
 *
 *
 * You should add additional methods to this class to meet the
 * application requirements.  This class will only be generated as
 * long as it does not already exist in the output directory.
 *
 */
class PositionQuery extends BasePositionQuery
{
    static function getAll($objet,$selection){
        global $app;
        $nomQuery = nom_query($objet);
        $objet_adresses = $objet;
        switch ($objet) {

            case 'membre':
                $sous_requete='1=1';
                if($selection=='courante'){
                    $tab_request = $app['session']->get('selection_courante_membre');
                    list($sous_requete, $nb_total) = objet_selection_restriction_nb('membre',$tab_request);
                    $sous_requete = 'asso_membres.id_membre IN('.$sous_requete.')';
                }

                $tab_objet = MembreQuery::create()->where($sous_requete)->find();
                foreach($tab_objet as $ob)
                    $tab_id[$ob->getPrimaryKey()] = $ob->getIdIndividuTitulaire();
                $objet_adresses = 'individu';

                break;
            case 'individu':
                $sous_requete='1=1';
                if($selection=='courante'){
                    $tab_request = $app['session']->get('selection_courante_individu');
                    $sous_requete = getSelectionObjet('individu',$tab_request);
                    $sous_requete = 'asso_individus.id_individu IN('.$sous_requete.')';
                }

                $tab_objet = IndividuQuery::create()
                    ->where($sous_requete)
                    ->useMembreIndividuQuery()
                    ->endUse()
                    ->leftJoinwith('MembreIndividu')
                    ->find();
                foreach($tab_objet as $ob)
                    $tab_id[$ob->getPrimaryKey()] = $ob->getPrimaryKey();
                break;
            default:
                $tab_objet = $nomQuery::create()->find();
                break;

        }

        $tab_positionlien = PositionLienQuery::create()->filterByObjet($objet_adresses);
        if   (in_array($objet,['membre','individu']))
            $tab_positionlien = $tab_positionlien->filterByIdObjet($tab_id);
        $tab_positionlien =$tab_positionlien->usePositionQuery()
            ->endUse()
            ->find();
        $tab_position=array();
        $tab_id = array_flip($tab_id);
        foreach($tab_positionlien as $gl){
            $position=$gl->getPosition();

            $id_objet=($objet=='membre')?$tab_id[$gl->getIdObjet()]:$gl->getIdObjet();
            $tab_position[$gl->getIdObjet().''] = [
                $id_objet,
                $position->getTitre()."",
                $position->getDescriptif()."",
                $position->getLat(),
                $position->getLon(),
                $objet
            ];
        }

        switch ($objet) {

            case 'membre':

                $cotisation = table_simplifier(getPrestationDeType('cotisation'));
                $tab_sr = ServicerenduQuery::create()
                    ->filterByIdPrestation(array_keys($cotisation))
                    ->orderByDateFin('ASC')
                    ->find()
                    ->getdata();
                //todo a meliorer pour ne sortir q'un enrpositiontremet la date de fin la plus élevés puis findone() et supprimer la boucle tab_objet2
                $temp = new DateTime();
                $temp5 = $temp->format('Ymd');
                foreach ($tab_sr as $sr) {

//                    if ($membre->getIdMembre()<20 or $membre->getIdMembre()==149) {
//                        echo '<br>membre '.$membre->getIdMembre().' '.$temp5.' :'.$membre->getDateFin()->format('Ymd');
//                    }
                    if (isset($tab_position[$sr->getIdMembre() . ''])) {
                        if ($sr->getDateFin()->format('Ymd') < $temp5) {
                            $tab_position[$sr->getIdMembre() . ''][5] = 'membre2';
                        }else
                            $tab_position[$sr->getIdMembre() . ''][5] = 'membre';
                        $tab_position[$sr->getIdMembre() . ''][2] = 'Dernière cotisation ' . $sr->getDateFin()->format('d/m/Y');
                        $tab_position[$sr->getIdMembre() . ''][0] = $sr->getIdMembre();
                    }
                }
                foreach ($tab_objet as $membre) {
                    if (isset($tab_position[$membre->getPrimaryKey() . ''])) {

                        $tab_position[$membre->getPrimaryKey() . ''][1] = $tab_position[$membre->getPrimaryKey() . ''][1] . $membre->getNom();
                        if ($membre->getDateSortie()) {
                            $tab_position[$membre->getPrimaryKey() . ''][2] = 'Sorti le ' . $membre->getDateSortie()->format('d/m/Y');
                            $tab_position[$membre->getPrimaryKey() . ''][5] = 'membre1';
                        }
                    }
                }
                break;
            case 'individu':
                foreach ($tab_objet as $objet_data) {
                    if (isset($tab_position[$objet_data->getPrimaryKey() . ''])) {
                        $tab_position[$objet_data->getPrimaryKey() . ''][1] = $objet_data->getNom();
                        $tab_position[$objet_data->getPrimaryKey() . ''][2] = $objet_data->getAdresse() . ' ' . $objet_data->getVille();
                    }
                }
                /*foreach ($tab_membresindividus as $objet_data) {
                    if (isset($tab_position[$objet_data->getIdIndividu() . ''])) {
                        $tab_position[$objet_data->getIdIndividu() . ''][5] = 'membre';
                    }
                }*/
                break;

            default:
                foreach ($tab_objet as $objet_data) {
                    if (isset($tab_position[$objet_data->getPrimaryKey() . ''])) {
                        $tab_position[$objet_data->getPrimaryKey() . ''][1] .= $objet_data->getNom();
                        $tab_position[$objet_data->getPrimaryKey() . ''][2] .= $objet_data->getAdresse() . ' ' . $objet_data->getVille();;
                    }
                }
                break;

        }
        $tab_position2 = array();
        foreach ($tab_position as $g) {
            $tab_position2[] = $g;
        }

//        echo '<br>Objet :'.$objet.' Adresse :'.$objet_adresses.'  '.(count($tab_objet));
//        echo '<br> tab points :'.count($tab_position);
//        echo '<br>points :'.count($tab_position2);
        return $tab_position2;


    }
}
