<?php

use Base\MembreIndividuQuery as BaseMembreIndividuQuery;

/**
 * Skeleton subclass for performing query and update operations on the 'asso_membres_individus' table.
 *
 *
 *
 * You should add additional methods to this class to meet the
 * application requirements.  This class will only be generated as
 * long as it does not already exist in the output directory.
 *
 */
class MembreIndividuQuery extends BaseMembreIndividuQuery
{




    public static function getCompilationNomIndividu($tab_id_membre,$date,$avec_titulaire=true){


        global $app;
        $tab=[];

        list($from,$where) = MembreIndividuQuery::getWhere([
            'id_membre' => $tab_id_membre,
            'date'=> $date
        ]);
        $pr=descr('membre.nom_sql');
        $pri=descr('individu.nom_sql');

        $statement =  $app['db']->executeQuery('SELECT '.$pr.'.id_membre as id_membre,'.$pr.'.nom as nom_membre,'.$pri.'.nom as nom FROM '.implode(',', array_values($from)).' WHERE '.$where);
        while($mi = $statement->fetch()){
            if($avec_titulaire || $mi['nom']!=$mi['nom_membre'])
                $tab[$mi['id_membre']][]= $mi['nom'];
        }

        return $tab;
    }
    public static function getWhere($params = array())
    {
        $pr='mi';
        $prm=descr('membre.nom_sql');
        $pri=descr('individu.nom_sql');

        $from=['asso_membres_individus '.$pr,'asso_individus '.$pri,'asso_membres '.$prm];
        $where = $prm.'.id_membre = '.$pr.'.id_membre and '.$pr.'.id_individu = '.$pri.'.id_individu';

        if ($id_membre = request_ou_options('id_membre', $params)) {
            $id_membre = is_array($id_membre)?$id_membre:[$id_membre];
            $where .= ' AND '.$pr.'.id_membre IN ('.implode(',',$id_membre).')';
        }


        if ($date = request_ou_options('date', $params)) {
            $where .= ' AND ( CAST(\''.$date.'\' AS DATE) >= '.$pr.'.date_debut and (CAST(\''.$date.'\' AS DATE) <=  '.$pr.'.date_fin ) OR date_fin is NULL  )';
        }


        if ($periode = request_ou_options('periode', $params)) {
            list($date_debut,$date_fin) = explode(' - ',$periode);
            $where .= ' AND (( CAST(\''.$date_debut.'\' AS DATE) >= '.$pr.'.date_debut and CAST(\''.$date_debut.'\' AS DATE) <=  '.$pr.'.date_fin )';
            $where .= ' OR ( '.$pr.'.date_debut <= CAST(\''.$date_debut.'\' AS DATE) and  '.$pr.'.date_debut >= CAST(\''.$date_fin.'\' AS DATE)))';

        }



        return [$from, $where] ;


    }


}
