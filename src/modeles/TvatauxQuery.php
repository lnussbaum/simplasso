<?php

use Base\TvatauxQuery as BaseTvatauxQuery;

/**
 * Skeleton subclass for performing query and update operations on the 'asso_tvatauxs' table.
 *
 *
 *
 * You should add additional methods to this class to meet the
 * application requirements.  This class will only be generated as
 * long as it does not already exist in the output directory.
 *
 */
class TvatauxQuery extends BaseTvatauxQuery
{

}
