<?php

use Base\Poste as BasePoste;

/**
 * Skeleton subclass for representing a row from the 'assc_postes' table.
 *
 *
 *
 * You should add additional methods to this class to meet the
 * application requirements.  This class will only be generated as
 * long as it does not already exist in the output directory.
 *
 */
class Poste extends BasePoste
{
    function getPostenom(){
        return $this->poste_comptable.' '.$this->nom;
    }
    function getcountcomptes($idEntite){
        $valeur=CompteQuery::create()->filterByIdEntite($idEntite)->filterByIdPoste($this->id_poste)->count();
        if ($valeur==0) return '';
        return $valeur;
    }

    public function getQuiCree(){
        return Log::getQuiCree('poste',$this->id_poste);
    }

    public function getQuiModifie(){
        return Log::getQuiModifie('poste',$this->id_poste);
    }

}
