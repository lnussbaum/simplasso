<?php

use Base\DepartementQuery as BaseDepartementQuery;

/**
 * Skeleton subclass for performing query and update operations on the 'geo_departements' table.
 *
 *
 *
 * You should add additional methods to this class to meet the
 * application requirements.  This class will only be generated as
 * long as it does not already exist in the output directory.
 *
 */
class DepartementQuery extends BaseDepartementQuery
{
    public static $champs_recherche = ['nom','code'];

    public static function getAll($tab_id, $order = array(), $offset = 0, $limit = 0) {
        return getAllObjet('departement', $tab_id, $order, $offset, $limit);
    }

}
