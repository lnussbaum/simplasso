<?php

use Base\AsscGedsArchive as BaseAsscGedsArchive;

/**
 * Skeleton subclass for representing a row from the 'assc_geds_archive' table.
 *
 *
 *
 * You should add additional methods to this class to meet the
 * application requirements.  This class will only be generated as
 * long as it does not already exist in the output directory.
 *
 */
class AsscGedsArchive extends BaseAsscGedsArchive
{

}
