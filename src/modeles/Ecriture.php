<?php

use Base\Ecriture as BaseEcriture;

/**
 * Skeleton subclass for representing a row from the 'assc_ecritures' table.
 *
 *
 *
 * You should add additional methods to this class to meet the
 * application requirements.  This class will only be generated as
 * long as it does not already exist in the output directory.
 *
 */
class Ecriture extends BaseEcriture
{
    static function findAll($profil=""){
        global $app;

        $req=EcritureQuery::create();
        if(!empty($profil)){
            $tab_entite=$app['user']->getEntitesParProfil($profil);
            $req->filterByIdEntite($tab_entite);
        }
        return $req->find();
    }


    static function findAllPiece($profil=""){
        global $app;

        $req=EcritureQuery::create();
        if(!empty($profil)){
            $tab_entite=$app['user']->getEntitesParProfil($profil);
            $req->filterByIdEntite($tab_entite);
        }
        return $req->find();
    }

    function getcountecritures(){
        return EcritureQuery::create()->findByidPiece($this->id_piece)->count();

    }

    function getSumcPiece(){
        return EcritureQuery::create()->findByidPiece($this->Credit)->sum();
    }

    public function getQuiCree(){
        return Log::getQuiCree('ecriture',$this->id_ecriture);
    }

    public function getQuiModifie(){
        return Log::getQuiModifie('ecriture',$this->id_ecriture);
    }

}
