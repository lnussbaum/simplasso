<?php

use Base\CourrierQuery as BaseCourrierQuery;

/**
 * Skeleton subclass for performing query and update operations on the 'asso_courriers' table.
 *
 *
 *
 * You should add additional methods to this class to meet the
 * application requirements.  This class will only be generated as
 * long as it does not already exist in the output directory.
 *
 */
class CourrierQuery extends BaseCourrierQuery
{

    public static $champs_recherche = ['numero','observation'];

    public static function getAll($tab_id, $order = array(), $offset = 0, $limit = 0)
    {
        return getAllObjet('courrier', $tab_id, $order, $offset, $limit);

    }





}
