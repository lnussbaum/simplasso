<?php

use Base\Courrier as BaseCourrier;

/**
 * Skeleton subclass for representing a row from the 'asso_courriers' table.
 *
 *
 *
 * You should add additional methods to this class to meet the
 * application requirements.  This class will only be generated as
 * long as it does not already exist in the output directory.
 *
 */
class Courrier extends BaseCourrier
{
    function getCanal(){
        return str_split(parent::getCanal());
    }

    function setCanal($value){
        return parent::setCanal(implode('',$value));
    }


    function getToutesLesVariables(){
        $tab_variables=[];
        foreach($this->getValeur() as $tab_c){
            foreach($tab_c as $tab){
                if(is_array($tab))
                    $tab_variables = array_merge($tab_variables,table_simplifier($tab,'variables'));
            }
        }
        return array_unique($tab_variables);
    }


    function getEmailSujet(){

        $tab_valeur = $this->getValeur();
        $tab=['texte'=>'','variables'=>[]];
        if(isset($tab_valeur['email_sujet']['texte']))
            $tab['texte']=$tab_valeur['email_sujet']['texte'];

        if(isset($tab_valeur['email_sujet']['variables']))
            $tab['variables']=$tab_valeur['email_sujet']['variables'];
        return $tab;
    }


    function getValeur()
    {
        $tab=json_decode($this->variables, true);
        if(is_array($tab))
            ksort($tab);
        return $tab;
    }

    function setValeur($valeur){
        return $this->setVariables(json_encode($valeur));
    }

    function getListeVariables($canal){
        $tab=array();
        $valeur = $this->getValeur();
        if (isset($valeur[$canal])){
            foreach($valeur[$canal] as $bloc){
                if (isset($bloc['variables']))
                    $tab+=$bloc['variables'];
            }
            return array_keys($tab);
        }
        return [];

    }

    function getOptions($canal){
        $tab=[];
        $valeur = $this->getValeur();
        if (isset($valeur['options'][$canal])){
             return $valeur['options'][$canal];
        }
        return $tab;

    }


    function estSms(){
        return in_array('S',$this->getCanal());
    }

    function estEmail(){
        return in_array ('E',$this->getCanal());
    }

    function estLettre(){
        return in_array('L',$this->getCanal());
    }

    function getBlocSMS(){
        $valeur = $this->getValeur();
        if (isset($valeur['sms'])){
            return table_trier_par($valeur['sms'],'ordre');
        }
        return [];
    }

    function getBlocEmail(){
        $valeur = $this->getValeur();
        if (isset($valeur['email'])){
            return table_trier_par($valeur['email'],'ordre');
        }
        return [];
    }


    function getBlocLettre(){
        $valeur = $this->getValeur();
        if (isset($valeur['lettre'])){
            return table_trier_par($valeur['lettre'],'ordre');
        }
        return [];
    }


 function getPieceJointeEmail(){

        $tab_lien =  GedLienQuery::create()->filterByIdObjet($this->getPrimaryKey())->filterByObjet('courrier')->filterByUsage('pj')->leftJoinGed()->find();
        $tab_ged = [];
        foreach($tab_lien as $lien){
            $tab_ged[] = $lien->getGed();
        }
    return $tab_ged;
 }




}
