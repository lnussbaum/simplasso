<?php

use Base\Servicerendu as BaseServicerendu;

/**
 * Skeleton subclass for representing a row from the 'asso_servicerendus' table.
 *
 *
 *
 * You should add additional methods to this class to meet the
 * application requirements.  This class will only be generated as
 * long as it does not already exist in the output directory.
 *
 */
class Servicerendu extends BaseServicerendu
{

    public function getSommeServicepaiement(){
        $tab=$this->getServicepaiements()->toKeyValue('IdServicepaiement','Montant');
        return array_sum($tab);
    }

    public function getDesignation(){
        $tab=tab('prestation.'.$this->getIdPrestation());
        return $tab['nom'];

    }


    public function getBeneficiaire(){
        if ($this->getIdMembre()==null){

        }

    }





    public function getTotal()
    {
        return $this->getMontant()*$this->getQuantite();
    }


    public function getQuiCree()
    {
        return Log::getQuiCree('servicerendu', $this->id_servicerendu);
    }

    public function getQuiModifie()
    {
        return Log::getQuiModifie('servicerendu', $this->id_servicerendu);
    }


    public function ajouterIndividuDuMembre(){
        global $app;
        $tab_individus = $this->getMembre()->getIndividusEnCours();
        foreach($tab_individus as &$ind){
            $sri = new ServicerenduIndividu();
            $sri->setIdIndividu($ind->getIdIndividu());
            $sri->setIdServicerendu($this->getIdServicerendu());
            try{$sri->save();}
            catch (Exception $e) {
                $app->log('Doublons id_ind => '.$ind->getIdIndividu().' - id_sr => '.$this->getIdServicerendu().PHP_EOL);

            }

        }


    }

}
