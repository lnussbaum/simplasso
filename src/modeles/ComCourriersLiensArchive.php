<?php

use Base\ComCourriersLiensArchive as BaseComCourriersLiensArchive;

/**
 * Skeleton subclass for representing a row from the 'com_courriers_liens_archive' table.
 *
 *
 *
 * You should add additional methods to this class to meet the
 * application requirements.  This class will only be generated as
 * long as it does not already exist in the output directory.
 *
 */
class ComCourriersLiensArchive extends BaseComCourriersLiensArchive
{

}
