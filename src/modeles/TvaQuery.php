<?php

use Base\TvaQuery as BaseTvaQuery;

/**
 * Skeleton subclass for performing query and update operations on the 'asso_tvas' table.
 *
 *
 *
 * You should add additional methods to this class to meet the
 * application requirements.  This class will only be generated as
 * long as it does not already exist in the output directory.
 *
 */
class TvaQuery extends BaseTvaQuery
{

    public static $champs_recherche = ['nom'];


    public static function getAll($tab_id, $order = array(), $offset = 0, $limit = 0) {
        return getAllObjet('tva', $tab_id, $order, $offset, $limit);
    }

}
