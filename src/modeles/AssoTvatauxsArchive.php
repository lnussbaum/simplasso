<?php

use Base\AssoTvatauxsArchive as BaseAssoTvatauxsArchive;

/**
 * Skeleton subclass for representing a row from the 'asso_tvatauxs_archive' table.
 *
 *
 *
 * You should add additional methods to this class to meet the
 * application requirements.  This class will only be generated as
 * long as it does not already exist in the output directory.
 *
 */
class AssoTvatauxsArchive extends BaseAssoTvatauxsArchive
{

}
