<?php

use Base\Log as BaseLog;
use Propel\Runtime\ActiveQuery\Criteria as Criteria;
/**
 * Skeleton subclass for representing a row from the 'asso_logs' table.
 *
 *
 *
 * You should add additional methods to this class to meet the
 * application requirements.  This class will only be generated as
 * long as it does not already exist in the output directory.
 *
 */
class Log extends BaseLog
{

    static function EnrOp(
        $sufixe,
        $objet = '',
        $objet_data = null,
        $id = 0,
        $prefixe = '',
        $observation = '',
        $date_param = '',
        $id_individu = -1
    )
    {

        if (is_bool($sufixe)) {
            $sufixe = ($sufixe) ? 'MOD' : 'CRE';
        }

        if ($objet == '') {
            $objet = sac('objet');
            if (!$objet) {
                return false;
            }
        }

        if (isset($objet_data)) {
            $temp['objet'] = $objet;
            if (!$id  && method_exists($objet_data, 'getPrimaryKey')) {
                $id = $objet_data->getPrimaryKey();
            }
            $objet_data = charger_objet($objet,$id);
            if (!$objet_data) {
                return false;
            }

        }

        $prefixe = ($prefixe) ? $prefixe : descr($objet.'.log');
        $prefixe = strtoupper($prefixe);
        if (substr($prefixe, 0, 3) == 'SR_' && $id > 0  ) {
            $prefixe = substr($prefixe, 0, 2) . tab('prestation.'.$objet_data->getIdPrestation().'.prestation_type');
        }
        $prefixe = (substr($prefixe . '---', 0, 3));


        Log::EnrOps($prefixe . $sufixe, array('id_' . $objet => $id), array($objet => $id), $observation, $date_param,
            $id_individu);

    }

    static function EnrOps(
        $code,
        $variables = array(),
        $objets = array(),
        $observation = '',
        $date_param = '',
        $id_individu = -1
    )
    {

        if ($id_individu <= 0) {
            $id_individu = suc('operateur.id');
        }
        $log = new Log();
        $date = date('Y-m-d H:i:s');
        if (!empty($date_param)) {
            $date = $date_param;
        }
        $tab_value = array(
            'id_individu' => $id_individu,
            'id_entite' => sac('en_cours.id_entite'),
            'code' => $code,
            'variables' => json_encode($variables),
            'date_operation' => $date,
            'observation' => $observation
        );

        $log->fromArray($tab_value);

        $log->save();
        foreach ($objets as $obj => $tab_id_objet) {
            if (!is_array($tab_id_objet)) {
                $tab_id_objet = array($tab_id_objet);
            }

            foreach ($tab_id_objet as $id_objet) {
                $logliens = new LogLien();
                $logliens->setIdLog($log->getIdLog());
                $logliens->setObjet($obj);
                $logliens->setIdObjet($id_objet);
                $logliens->save();
            }
        }
        return $log->getPrimaryKey();

    }

    static function getQuiCree($objet, $id_objet)
    {

        $log = LogQuery::create()
            ->filterByCode('%CRE', Criteria::LIKE)
            ->useLogLienQuery('ll', 'left join')
            ->filterByObjet($objet)
            ->filterByIdObjet($id_objet)
            ->endUse()
            ->orderByDateOperation('ASC')
            ->findOne();

        if ($log) {
            return $log->getIdIndividu();
        } else {
            return '';
        }
    }

    static function getQuiModifie($objet, $id_objet)
    {

        $log = LogQuery::create()
            ->filterByCode('%MOD', Criteria::LIKE)
            ->useLogLienQuery('ll', 'left join')
            ->filterByObjet($objet)
            ->filterByIdObjet($id_objet)
            ->endUse()
            ->orderByDateOperation('DESC')
            ->findOne();
        if ($log) {
            return $log->getIdIndividu();
        } else {
            return '';
        }
    }

// maintenue pour enregistrement avec plusieurs liaisons  : objets avec un S

    static function log_operations($objet, $id_objet, $prefs = array(), $nb_operation = null)
    {
        global $app;
        $tab_operations = Log::getAllByObjet($objet, $id_objet, 'DESC');
        return Log::dessineTimeline($tab_operations, $prefs, $nb_operation = 15);
    }

    static function getAllByObjet($objet = "", $id_objet = 0, $ordre = 'ASC', $id_individu = 0)
    {

        $tab_id = LogLienQuery::create()->filterByObjet($objet)->filterByIdObjet($id_objet)->find()->toKeyValue('idLog',
            'idLog');
        $query = LogQuery::create()->filterByPrimaryKeys($tab_id);
        return $query
            ->useIndividuQuery()
            ->endUse()
            ->orderByDateOperation($ordre)
            ->find();
    }

    static function dessineTimeline($tab_operations, $prefs, $nb_operation = 15)
    {
        global $app;
        $tab_objet_log = getObjetLog();
        $tab_op = array();
        if (!isset($prefs['decoupage']))
            $prefs['decoupage'] = 'jour';
        if (!isset($prefs['regroupement']))
            $prefs['regroupement'] = false;

        foreach ($tab_operations as $operation) {
            if ($prefs['decoupage'] == 'jour') {
                $date_groupe = floor(($operation->getDateOperation()->getTimestamp()) / 86400) * 86400;
            } elseif ($prefs['decoupage'] == 'heure') {
                $date_groupe = floor(($operation->getDateOperation()->getTimestamp()) / 3600) * 3600;
            } else {
                $date_groupe = ($operation->getDateOperation()->getTimestamp());
            }
            $code_operation = 'LOG_' . $operation->getCode();
            $variables = $operation->getVariables_en_chaine();
            //Regroupement des Logs d'un meme type
            if ($prefs['regroupement']) {

                if (isset($tab_op[$date_groupe][$code_operation]) or
                    isset($tab_op[$date_groupe]['LOL_' . substr($code_operation, 4)])
                ) {
                    if (isset($tab_op[$date_groupe][$code_operation])) {
                        $tab_op[$date_groupe][$code_operation]['code'] = 'LOL_' . substr($code_operation, 4);
                        $tab_op[$date_groupe]['LOL_' . substr($code_operation, 4)] = $tab_op[$date_groupe][$code_operation];
                        unset($tab_op[$date_groupe][$code_operation]);
                    }
                    $code_operation = 'LOL_' . substr($code_operation, 4);
                }
            }

            $operateur = [$operation->getIdIndividu() => $operation->getIndividu()->getNom()];
            if ($prefs['regroupement']) {
                if (isset($tab_op[$date_groupe][$code_operation]['operateur'])) {
                    $tab_op[$date_groupe][$code_operation]['operateur'] += $operateur;
                } else {
                    $tab_op[$date_groupe][$code_operation] = [
                        'code' => $code_operation,
                        'operateur' => $operateur];
                }
            } else {
                $tab_op[$date_groupe][$operation->getPrimaryKey()] = [
                    'code' => $code_operation,
                    'date' => $operation->getDateOperation(),
                    'operateur' => $operateur];
            }


            // Transformation des id_membre en lien vers les membres
            if (is_array($variables)) {
                foreach ($variables as $key => $id) {
                    $ob = substr($key, 3);//enlever id_


                    if (in_array($ob, array_keys(liste_des_objets()))) {
                        $lien = '<a href="' . $app->path($ob, array($key => $id)) . '">' . $id . '</a>';
                        $ob_tab = '%url_' . $ob . '%';
                        if (in_array($ob, $tab_objet_log)) {
                            if ($ob == 'import') {
                                $lien = $id . ' ' . $operation->getObservation();
                            } elseif (substr($ob, 0, 3) == 'sr_' ) {
                                $ob_tab = '%url_servicerendu%';
                                $lien = '<a href="' . $app->path('servicerendu',
                                        array('id_servicerendu' => $id)) . '">' . $id . '</a>';
                            }
                        }
                    }
                    if (substr($code_operation, -3) == 'SUP') {
                        $lien = $id;
                    }
                }

                if ($prefs['regroupement']) {
                    $tab_op[$date_groupe][$code_operation]['variables'][$ob_tab][$id] = $lien;

                } else {
                    $tab_op[$date_groupe][$operation->getPrimaryKey()]['variables'][$ob_tab][$id] = $lien;
                }

            }
        }
        foreach ($tab_op as $date => &$var) {
            foreach ($var as $log => &$var1) {
                $temp2 = array();
                $i = 0;
                if (isset($var1['variables'])) {
                    foreach ($var1['variables'] as $url => &$var2) {
                        $i++;
                        /*  if ($i == 1) {
                              $temp2[$i] = $var2;
                          } else {
                              var_dump($temp2[$i]);
                              //if(isset($temp2[($i - 1)]) && !is_array($temp2[($i - 1)]))
                                 $temp2[$i] = explode(', ', $temp2[($i - 1)] . ', ' . implode(', ', $var2));
                          }*/
                        $var2 = implode(', ', $var2);
                    }


                }
            }
        }

        return $tab_op;
    }

    static function getTimeline($prefs, $nb_operation = 15)
    {
        global $app;
        $id_user = 0;
        if ($prefs['filtre_operateur'] == 'moi') {
            $id_user = suc('operateur.id');
        }
        $tab_operations = Log::getAll($id_user, 'DESC', $nb_operation);
        return Log::dessineTimeline($tab_operations, $prefs, $nb_operation = 15);
    }

    static function getAll($id_individu = 0, $ordre = 'ASC', $limite = 30)
    {


        $query = LogQuery::create();
        if ($id_individu) {
            $query->filterByIdIndividu($id_individu);
        }
        return $query
            ->useIndividuQuery()
            ->endUse()
            ->leftJoinwith('Individu')
            ->limit($limite)
            ->orderByDateOperation($ordre)
            ->find();
    }

    function log_affiche()
    {
        global $app;
        $texte_individu = '';
        $variables = json_decode($this->variables, true);
        if ($this->id_individu) {
            if ($individu = $this->getIndividu()) {
                $texte_individu = ' par ' . $individu->getNom();
            }
        }

        if (!is_array($variables)) {
            $variables = array($variables);
        }

        // Cas particulier nessecitant d'être plus précis
        if (isset($variables['id_membre'])) {
            $variables['url_membre'] = $app->url('membre', array('id_membre' => $variables['id_membre']));
        }


        $texte = $app->trans($this->code);
        return array(
            'date' => $this->getDateOperation('d/m/Y'),
            'texte' => $texte,
            'individu' => $texte_individu
        );

    }

    function getVariables()
    {
        return json_decode($this->variables, true);

    }

    function getVariables_en_chaine()
    {
        $tab = json_decode($this->variables, true);
        if (is_array($tab)) {
            foreach ($tab as &$t) {
                if (is_array($t)) {
                    $t = implode(', ', $t);
                }
            }
        }
        return $tab;

    }


}
