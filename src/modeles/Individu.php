<?php

use Base\Individu as BaseIndividu;
use Propel\Runtime\ActiveQuery\Criteria as Criteria;
/**
 * Skeleton subclass for representing a row from the 'asso_individus' table.
 *
 *
 *
 * You should add additional methods to this class to meet the
 * application requirements.  This class will only be generated as
 * long as it does not already exist in the output directory.
 *
 */
class Individu extends BaseIndividu
{

    function setLogin($login) {
        return parent::setLogin(strtolower($login));
    }


    function setPass($pass) {
        if (!empty($pass) and $this->getPass() != $pass) {

            $alea_actuel = \creer_uniqid();
            $this->setAleaActuel($alea_actuel);
            $obj = new \nanoSha2();
            parent::setPass($obj->hash($alea_actuel . $pass));
        }
        return $this;
    }

    public function isPasswordResetRequestExpired($ttl) {
        $timeRequested = $this->getTokenTime();
        if (!$timeRequested) {
            return true;
        }
        return $timeRequested + $ttl < time();
    }


    function setPrenom($texte) {
        $this->setNom($this->nom_famille . ' ' . $texte);
        return parent::setPrenom($texte);
    }


    function setNomFamille($texte) {
        $this->setNom($texte . ' ' . $this->prenom);
        return parent::setNomFamille($texte);
    }

    public function isSubscribeNewsletter($id_newsletter_liste) {
        global $app;
        if ($app['apispip']) {
            $tab_nl = $app['API_spip']->newsletter_statut($this->getEmail());

            if (isset($tab_nl[$id_newsletter_liste])) {
                return $tab_nl[$id_newsletter_liste];
            }
        }
        return false;
    }

    public function getIdInfolettreInscrit() {
        return InfolettreInscritQuery::create()->filterByEmail($this->email)->find()->toKeyValue('idInfolettre',
            'idInfolettre');
    }


    public function estTitulaire($id_membre) {

        return MembreQuery::create()->filterByPrimaryKey($id_membre)->filterByIdIndividuTitulaire($this->getIdIndividu())->exists();

    }

    public function estMembre() {
        if ($tab_mi = $this->getMembreIndividus()->toKeyValue('IdMembre', 'IdMembre')) {
            return MembreQuery::create()->filterByDateSortie(null)->filterByPrimaryKeys($tab_mi)->exists();
        }

        return false;
    }


    public function getMembre($actif = null) {
        if ($tab_mi = $this->getMembreIndividus()->toKeyValue('IdMembre', 'IdMembre')) {
            if ($actif === false) {
                return MembreQuery::create()->filterByDateSortie(null, Criteria::ISNOTNULL)->findPks($tab_mi);
            } elseif ($actif === true) {
                return MembreQuery::create()->filterByDateSortie(null, Criteria::ISNULL)->findPks($tab_mi);
            }
            return MembreQuery::create()->findPks($tab_mi);
        }

        return array();
    }

    public function getQuiCree() {
        return Log::getQuiCree('membre', $this->id_individu);
    }

    public function getQuiModifie() {
        return Log::getQuiModifie('membre', $this->id_individu);
    }

    public function getMots() {
        return MotLienQuery::create()
            ->filterByObjet('individu')
            ->filterByIdObjet($this->getPrimaryKey())
            ->find()->toKeyValue('IdMot', 'IdMot');

    }

    public function afficheMots() {
        $tab_mots = MotLienQuery::create()
            ->filterByObjet('individu')
            ->filterByIdObjet($this->getPrimaryKey())
            ->find()->toKeyValue('IdMot', 'Observation');

        $tab = array();
        foreach ($tab_mots as $k => $obs) {
            $tab[$k] = tab('mot.' . $k . '.nom') . (($obs) ? ' : ' . $obs : '') . " ";
        }
        return $tab;
    }

    public function setMots($tab_mots) {
        MotLienQuery::create()
            ->filterByObjet('individu')
            ->filterByIdObjet($this->getPrimaryKey())
            ->delete();

        foreach ($tab_mots as $mot) {
            $motlien = new MotLien();
            $motlien->setIdMot($mot)->setIdObjet($this->getIdIndividu())->setObjet('individu')->save();
        }
        return true;
    }

    public function estGeoreference(){
        return PositionLienQuery::create()->filterByObjet('individu')->filterByIdObjet($this->getPrimaryKey())->exists();
    }

    public function getPosition(){
        $position =  PositionLienQuery::create()->filterByObjet('individu')->filterByIdObjet($this->getPrimaryKey())->findOne();
        if($position)
            return $position->getPosition();
        return null;
    }

    public function getTelephones() {
        $temp = [];
        if ($mobile = $this->getMobile()) {
            $temp[] = '<i class="fa fa-mobile" aria-hidden="true"></i> ' . afficher_telephone( $mobile);
        }
        if ($telephone = $this->getTelephone()) {
            $temp[] = '<i class="fa fa-phone" aria-hidden="true"></i> ' . afficher_telephone( $telephone);
        }
        if(empty($temp) && $telephone_pro = $this->getTelephonePro()){
            $temp[] = '<i class="fa fa-phone" aria-hidden="true"></i> ' . afficher_telephone( $telephone_pro);
        }
        return implode('<br>', $temp);
    }

    public function getCotisations($id_entite = null, $en_cours = false) {
        return $this->getServicerendu('cotisation', $id_entite, $en_cours);
    }

    public function getServicerendu($type_prestation = null, $id_entite = null, $en_cours = false) {
        $req = ServicerenduQuery::create()->filterByIndividu($this);
        if ($type_prestation) {
            $tab_prestations = array_keys(getPrestationEntite($type_prestation));
            $req = $req->filterByIdPrestation($tab_prestations);
        }
        if ($en_cours) {
            $req = $req->filterByDateDebut(new DateTime(), Criteria::LESS_EQUAL)
                ->filterByDateFin(new DateTime(), Criteria::GREATER_THAN);
        }
        if ($id_entite) {
            $req = $req->filterByIdEntite($id_entite);
        }
        return $req->orderByDateFin('DESC')->find();

    }

    public function getAbonnement($id_entite = null, $en_cours = false) {
        return $this->getServicerendu('abonnement', $id_entite, $en_cours);
    }

    public function getDon($id_entite = null, $en_cours = false) {
        return $this->getServicerendu('cotisation', $id_entite, $en_cours);
    }

    public function getVente($id_entite = null, $en_cours = false) {
        return $this->getServicerendu('vente', $id_entite, $en_cours);
    }

    public function getPerte($id_entite = null, $en_cours = false) {
        return $this->getServicerendu('perte', $id_entite, $en_cours);
    }

    public function getAdhesion($id_entite = null, $en_cours = false) {
        return $this->getServicerendu('adhesion', $id_entite, $en_cours);
    }


    public function getEtatCotisation($id_entite = null) {
        return $this->getEtatServicerendu('cotisation', $id_entite);

    }

    public function getEtatServicerendu($type_prestation = null, $id_entite = null) {
        $req = ServicerenduQuery::create()
            ->filterByIndividu($this);
        if ($type_prestation) {
            $tab_prestations = array_keys(getPrestationEntite($type_prestation,$id_entite));
            $req = $req->filterByIdPrestation($tab_prestations);
        }
        $req = $req
            ->filterByDateDebut(new DateTime(), Criteria::LESS_EQUAL)
            ->filterByDateFin(new DateTime(), Criteria::GREATER_THAN);
        if ($id_entite) {
            $req = $req->filterByIdEntite($id_entite);
        }
        return $req->exists();

    }

    public function getEtatAdhesion($id_entite = null) {
        return $this->getEtatServicerendu('adhesion', $id_entite);
    }

    public function estDecede(){
        $id_mot_decede = table_filtrer_valeur_premiere(tab('mot'),'nomcourt','ind_dcd')['id_mot'];
        return in_array($id_mot_decede,$this->getMots());
    }

}
