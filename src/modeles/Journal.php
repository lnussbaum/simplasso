<?php

use Base\Journal as BaseJournal;

/**
 * Skeleton subclass for representing a row from the 'assc_journals' table.
 *
 *
 *
 * You should add additional methods to this class to meet the
 * application requirements.  This class will only be generated as
 * long as it does not already exist in the output directory.
 *
 */
class Journal extends BaseJournal
{
    public function getQuiCree() {

        return Log::getQuiCree('journal', $this->id_journal);
    }

    public function getQuiModifie() {
        return Log::getQuiModifie('journal', $this->id_journal);
    }

}
