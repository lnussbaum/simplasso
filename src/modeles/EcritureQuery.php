<?php

use Base\EcritureQuery as BaseEcritureQuery;

/**
 * Skeleton subclass for performing query and update operations on the 'assc_ecritures' table.
 *
 *
 *
 * You should add additional methods to this class to meet the
 * application requirements.  This class will only be generated as
 * long as it does not already exist in the output directory.
 *
 */
class EcritureQuery extends BaseEcritureQuery
{
    static function getSumdPiece() {
        $query = EcritureQuery::create()
            ->withColumn('SUM(debit)', 'sum_debit')
            ->withColumn('SUM(credit)', 'sum_credit')
            ->select(array('id_ecriture','sum_debit','sum_credit'))
            ->groupBy('idPiece');
        return $query->find();

    }

}
