<?php

use Base\AssoPaiementsArchiveQuery as BaseAssoPaiementsArchiveQuery;

/**
 * Skeleton subclass for performing query and update operations on the 'asso_paiements_archive' table.
 *
 *
 *
 * You should add additional methods to this class to meet the
 * application requirements.  This class will only be generated as
 * long as it does not already exist in the output directory.
 *
 */
class AssoPaiementsArchiveQuery extends BaseAssoPaiementsArchiveQuery
{

}
