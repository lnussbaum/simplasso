<?php

use Base\ComCourriersLiensArchiveQuery as BaseComCourriersLiensArchiveQuery;

/**
 * Skeleton subclass for performing query and update operations on the 'com_courriers_liens_archive' table.
 *
 *
 *
 * You should add additional methods to this class to meet the
 * application requirements.  This class will only be generated as
 * long as it does not already exist in the output directory.
 *
 */
class ComCourriersLiensArchiveQuery extends BaseComCourriersLiensArchiveQuery
{

}
