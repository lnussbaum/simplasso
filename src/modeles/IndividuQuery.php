<?php

use Base\IndividuQuery as BaseIndividuQuery;
use Propel\Runtime\ActiveQuery\Criteria;

/**
 * Skeleton subclass for performing query and update operations on the 'asso_individus' table.
 *
 *
 *
 * You should add additional methods to this class to meet the
 * application requirements.  This class will only be generated as
 * long as it does not already exist in the output directory.
 *
 */
class IndividuQuery extends BaseIndividuQuery
{

    public static function getAll($tab_id_individu, $order = array(), $offset = 0, $limit = 0, $options = array()) {


        $q = IndividuQuery::create();
        if (isset($options['not_in']) && $options['not_in']) {
            $q = $q->filterBy('IdIndividu', $tab_id_individu, Criteria::NOT_IN);
        } else {
            $q = $q->filterByPrimaryKeys($tab_id_individu);
        }


        $q = $q->useMembreIndividuQuery()
            ->endUse()
            ->leftJoinwith('MembreIndividu');

        if (!empty($order) && is_array($order)) {
            foreach ($order as $k => $o) {
                $q = $q->orderBy($k, $o);
            }
        }
        if ($offset > 0) {
            $q = $q->offset($offset);
        }
        if ($limit > 0) {
            $q = $q->limit($limit);
        }

        return $q->find();
    }



    public static function getMembre() {
        if ($tab_mi = IndividuQuery::create()->getMembreIndividus()->toKeyValue('IdMembre', 'IdMembre')) {
            return MembreQuery::create()->findPks($tab_mi);
        }
        return array();
    }



    public static function getWhere($params = array()) {

        $where = "1=1";
        $pr = descr('individu.nom_sql');
        $tables = array($pr => descr('individu.table_sql'));
        $where .= objet_selection_restriction_mot('individu');


        // La recherche
        if ($search = request_ou_options('search', $params)) {
            $recherche = trim($search['value']);


            if (!empty($recherche)) {
                $colle = 'AND';
                if (intval($recherche) > 0) {
                    $where .= ' and id_individu = \'' . $recherche . '\'';
                    $colle = 'OR';
                }
                $recherche_m = '+' . str_replace(' ', '* +', str_replace(array('@', '.'), ' ', $recherche)) . "*";
                $where .= ' ' . $colle . '( email like \'%' . $recherche . '%\' OR MATCH (nom,nom_famille,prenom) AGAINST (\'' . $recherche_m . '\' IN BOOLEAN MODE))';

            }
        }



        $motgroupe = ['mots'=>'OR'] + liste_motgroupe($pr);
        foreach ($motgroupe as $mg=>$mg_operateur_par_defaut) {
            if ($mots = request_ou_options($mg, $params)) {
                if (!is_array($mots)) {
                    $mots = [$mots];
                }
                $op = $mg_operateur_par_defaut == "AND" ? '= ' . count($mots) : '> 0';
                $where .= ' AND (select count(ml' . $mg . '.id_mot) from asso_mots_liens ml' . $mg . ' where ml' . $mg . '.id_mot IN (' . implode(',',
                        $mots) . ') AND ml' . $mg . '.id_objet=' . $pr . '.id_individu and ml' . $mg . '.objet=\'individu\' )'.$op;
            }
        }



        if ($est_decede = request_ou_options('est_decede', $params)) {
            if($est_decede=='non')
                $nb=0;
            else
                $nb=1;
            $id_mot_decede = table_filtrer_valeur_premiere(tab('mot'),'nomcourt','ind_dcd')['id_mot'];
            $where .= ' and (select count(mldcd.id_mot) from asso_mots_liens mldcd where mldcd.id_mot = '.$id_mot_decede.' and mldcd.id_objet='.$pr.'.id_individu and mldcd.objet=\'individu\')='.$nb ;

        }


        if ($est_adherent = request_ou_options('est_adherent', $params)) {
            $where .= ' and (select count(mi.id_membre) from asso_membres_individus mi, asso_membres m where mi.id_individu = '.$pr.'.id_individu and m.id_membre = mi.id_membre and (m.date_sortie > NOW() OR m.date_sortie is null ))>0';
        }

        if ($est_georeference = request_ou_options('est_georeference', $params)) {
            $operateur = ($est_georeference)? '>' : '=';
            $where .= ' and (select count(p.id_position) from geo_positions_liens p WHERE   p.id_objet= '.$pr.'.id_individu and p.objet=\'individu\' )'.$operateur.'0';
        }

        // Filtres géographiques
        // Régions, Départements, Arrondissements,communes
        $where_geo = '';
        if ($geo_region = request_ou_options('region', $params)) {
            $where_geo .= " AND region IN (" . implode(',',$geo_region).')';
        }
        if ($geo_dep = request_ou_options('departement', $params)) {
            $where_geo .= " AND departement IN (" . implode(',',$geo_dep).')';
        }
        if ($geo_arr = request_ou_options('arrondissement', $params)) {
            $where_geo .= " AND arrondissement IN (" . implode(',',$geo_arr).')';
        }

        $select_geo='';
        if ($geo_com = request_ou_options('commune', $params)) {
            if (!empty($where_geo)) {
                $where_geo .= " AND id_commune IN (" . implode(',', $geo_com) . ')';
            }else
            {
                $select_geo = implode(',', $geo_com) ;
            }
        }
        if (!empty($where_geo)) {
            $select_geo = 'select id_commune from geo_communes where 1=1 ' . $where_geo;
        }


        if (!empty($select_geo)) {
            $tables['ccl'] = 'geo_communes_liens';
            $where .= ' AND ccl.objet=\'individu\' AND ccl.id_objet='.$pr.'.id_individu AND ccl.id_commune IN (' . $select_geo . ')';
        }


        //Filtre Zone
        if ($geo_zone = request_ou_options('zone', $params)) {
            $tables['zl'] = 'geo_zones_liens';
            $where .= ' AND zl.objet=\'individu\' AND zl.id_objet='.$pr.'.id_individu AND zl.id_zone IN (' . implode(',', $geo_zone) . ')';
        }


        $filtres_coords = ['email','adresse','telephone','telephone_pro','mobile'];
        foreach($filtres_coords as $fc){

            if ($filtre_c_valeur = request_ou_options($fc, $params)) {

                if ($filtre_c_valeur == 'oui') {
                    $where .= ' AND '.$pr.'.'.$fc.'!=\'\'';
                } elseif ($filtre_c_valeur == 'non') {
                    $where .= ' AND '.$pr.'.'.$fc.'=\'\'';
                }elseif ($filtre_c_valeur=='npai'){
                    $code = 'npai_'.substr($fc,0,5);
                    if ($fc == 'telephone_pro')
                        $code = 'npai_tele2';
                    $id_mot = table_filtrer_valeur_premiere(tab('mot'),'nomcourt',$code)['id_mot'];
                    $where .= ' and EXISTS (select * from asso_mots_liens ml'.$fc.' where ml'.$fc.'.id_mot ='.$id_mot.' and ml'.$fc.'.id_objet='.$pr.'.id_individu_titulaire and ml'.$fc.'.objet=\'individu\')';
                }
            }


        }




        $where = selection_ajouter_limitation_id('individu',$where,$params);

        if (request_ou_options('inverse_selection', $params)) {
            $where = 'NOT ('.$where.')';
        }


        foreach ($tables as $k => $t) {
            $from[$k] = $t . ' ' . $k;
        }
        return [$from, $where];

    }

    static function loginExiste($login, $id_individu_a_exclure = '0') {

        return IndividuQuery::create()->where('id_membre<>' . $id_individu_a_exclure)->filterByLogin($login . '%')->findOne();

    }

    static function donnerLogin($nom, $prenom) {


        $login = IndividuQuery::genererLogin($nom, $prenom);
        $tab_login = IndividuQuery::create()->filterByLogin($login . '%',Criteria::LIKE)->orderByLogin()->find()->toKeyValue('PrimaryKey',
            "login");

        if (in_array($login, $tab_login)) {
            $num = 1;
            while (in_array($login . $num, $tab_login)) {
                $num++;
            }
            return $login . $num;
        }

        return $login;
    }

    static function genererLogin($nom, $prenom = '') {
        $prenom = strtolower(substr(str_replace(array(' ', '\''), '', $prenom), 0, 1));
        $nom = strtolower(substr(str_replace(array(' ', '\''), '', $nom), 0, 9));
        return $prenom . $nom;


    }


}
