<?php

use Base\CommuneQuery as BaseCommuneQuery;

/**
 * Skeleton subclass for performing query and update operations on the 'geo_communes' table.
 *
 *
 *
 * You should add additional methods to this class to meet the
 * application requirements.  This class will only be generated as
 * long as it does not already exist in the output directory.
 *
 */
class CommuneQuery extends BaseCommuneQuery
{

    public static $champs_recherche = ['nom','autre_nom','code'];

    public static function getWhere($params = array()) {
        global $app;

        $where = "1=1";
        $pr = descr('commune.nom_sql');
        $cle = descr('commune.cle_sql');
        $tables = array($pr => descr('commune.table_sql'));

        //print_r(request_ou_options('search', $params));
        // La recherche
        if (($search = request_ou_options('search', $params))) {

            $recherche = trim($search['value']);
            if (!empty($recherche)) {
                $colle = 'AND';
                if (intval($recherche) > 0) {
                    $where = $cle . ' = \'' . $recherche . '\'';
                    $colle = 'OR';
                }
                $where .= ' ' . $colle . ' ( ' . $pr . '.' . implode(' like '.$app['db']->quote($recherche.'%').' OR ' . $pr . '.',
                        CommuneQuery::$champs_recherche) . ' like '.$app['db']->quote($recherche.'%').')';
            }

        }

        if ($nom = strtolower(request_ou_options('nom', $params)))  {

            $strict=true;
            $champs_recherche = [$pr.'.nom',$pr.'.autre_nom','CONCAT('.$pr.'.article,'.$pr.'.nom)'];
            if($strict){
                $where .= ' AND ( LOWER(' .  implode(') = ' . $app['db']->quote($nom) . ' OR LOWER(',
                        $champs_recherche) . ') = ' . $app['db']->quote($nom) . ')';
            }else
            {
                $where .= ' AND ( LOWER(' .  implode(') like '.$app['db']->quote($nom.'%').' OR LOWER(',
                        $champs_recherche) . ') like '.$app['db']->quote($nom.'%').')';
            }
            



        }



        if ($geo_region = request_ou_options('region', $params)) {
            if (!is_array($geo_region))$geo_region=[$geo_region];
            $where .= " AND region IN (" . implode(',',$geo_region).')';
        }
        if ($geo_dep = request_ou_options('departement', $params)) {
            if (!is_array($geo_dep))$geo_dep=[$geo_dep];
            $where .= " AND departement IN (" . implode(',',$geo_dep).')';
        }
        if ($geo_arr = request_ou_options('arrondissement', $params)) {
            if (!is_array($geo_dep))$geo_arr =[$geo_arr ];
            $where .= " AND arrondissement IN (" . implode(',',$geo_arr).')';
        }



        $where = selection_ajouter_limitation_id('individu',$where,$params);

        if (request_ou_options('inverse_selection', $params)) {
            $where = 'NOT ('.$where.')';
        }


        foreach ($tables as $k => $t) {
            $from[$k] = $t . ' ' . $k;
        }


        return [$from, $where];

    }


    public static function getAll($tab_id, $order = array(), $offset = 0, $limit = 0) {
        return getAllObjet('commune', $tab_id, $order, $offset, $limit);
    }

}
