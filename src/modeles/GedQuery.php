<?php

use Base\GedQuery as BaseGedQuery;

/**
 * Skeleton subclass for performing query and update operations on the 'assc_geds' table.
 *
 *
 *
 * You should add additional methods to this class to meet the
 * application requirements.  This class will only be generated as
 * long as it does not already exist in the output directory.
 *
 */
class GedQuery extends BaseGedQuery
{
    static public function getOneByObjet($objet, $id_objet,$usage="")
    {

        $req = GedLienQuery::create()->joinGed()->filterByObjet($objet);
        if($usage)
            $req->filterByUsage($usage);
        $lien = $req->findOneByIdObjet($id_objet);
        if ($lien) {
            return $lien->getGed();
        }
        return null;

    }

}
