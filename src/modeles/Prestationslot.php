<?php

use Base\Prestationslot as BasePrestationslot;

/**
 * Skeleton subclass for representing a row from the 'asso_prestationslots' table.
 *
 *
 *
 * You should add additional methods to this class to meet the
 * application requirements.  This class will only be generated as
 * long as it does not already exist in the output directory.
 *
 */
class Prestationslot extends BasePrestationslot
{

    function getMontant(){

        $tab_prestations_du_lot = $this->getPrestationslotPrestations();
        $tab_prestation = tab('prestation');
        $montant = 0;
        foreach($tab_prestations_du_lot as $p){

            if($p->getIdPrestation()>0 && !empty($tab_prestation[$p->getIdPrestation()]['prix'])){

                $montant+= ($p->getQuantite()+0)*table_valeur_date($tab_prestation[$p->getIdPrestation()]['prix']);
            }
        }
        return $montant;
    }

}
