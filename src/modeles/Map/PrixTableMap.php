<?php

namespace Map;

use \Prix;
use \PrixQuery;
use Propel\Runtime\Propel;
use Propel\Runtime\ActiveQuery\Criteria;
use Propel\Runtime\ActiveQuery\InstancePoolTrait;
use Propel\Runtime\Connection\ConnectionInterface;
use Propel\Runtime\DataFetcher\DataFetcherInterface;
use Propel\Runtime\Exception\PropelException;
use Propel\Runtime\Map\RelationMap;
use Propel\Runtime\Map\TableMap;
use Propel\Runtime\Map\TableMapTrait;


/**
 * This class defines the structure of the 'asso_prixs' table.
 *
 *
 *
 * This map class is used by Propel to do runtime db structure discovery.
 * For example, the createSelectSql() method checks the type of a given column used in an
 * ORDER BY clause to know whether it needs to apply SQL to make the ORDER BY case-insensitive
 * (i.e. if it's a text column type).
 *
 */
class PrixTableMap extends TableMap
{
    use InstancePoolTrait;
    use TableMapTrait;

    /**
     * The (dot-path) name of this class
     */
    const CLASS_NAME = '.Map.PrixTableMap';

    /**
     * The default database name for this class
     */
    const DATABASE_NAME = 'simplasso';

    /**
     * The table name for this class
     */
    const TABLE_NAME = 'asso_prixs';

    /**
     * The related Propel class for this table
     */
    const OM_CLASS = '\\Prix';

    /**
     * A class that can be returned by this tableMap
     */
    const CLASS_DEFAULT = 'Prix';

    /**
     * The total number of columns
     */
    const NUM_COLUMNS = 8;

    /**
     * The number of lazy-loaded columns
     */
    const NUM_LAZY_LOAD_COLUMNS = 0;

    /**
     * The number of columns to hydrate (NUM_COLUMNS - NUM_LAZY_LOAD_COLUMNS)
     */
    const NUM_HYDRATE_COLUMNS = 8;

    /**
     * the column name for the id_prix field
     */
    const COL_ID_PRIX = 'asso_prixs.id_prix';

    /**
     * the column name for the id_prestation field
     */
    const COL_ID_PRESTATION = 'asso_prixs.id_prestation';

    /**
     * the column name for the montant field
     */
    const COL_MONTANT = 'asso_prixs.montant';

    /**
     * the column name for the date_debut field
     */
    const COL_DATE_DEBUT = 'asso_prixs.date_debut';

    /**
     * the column name for the date_fin field
     */
    const COL_DATE_FIN = 'asso_prixs.date_fin';

    /**
     * the column name for the observation field
     */
    const COL_OBSERVATION = 'asso_prixs.observation';

    /**
     * the column name for the created_at field
     */
    const COL_CREATED_AT = 'asso_prixs.created_at';

    /**
     * the column name for the updated_at field
     */
    const COL_UPDATED_AT = 'asso_prixs.updated_at';

    /**
     * The default string format for model objects of the related table
     */
    const DEFAULT_STRING_FORMAT = 'YAML';

    /**
     * holds an array of fieldnames
     *
     * first dimension keys are the type constants
     * e.g. self::$fieldNames[self::TYPE_PHPNAME][0] = 'Id'
     */
    protected static $fieldNames = array (
        self::TYPE_PHPNAME       => array('IdPrix', 'IdPrestation', 'Montant', 'DateDebut', 'DateFin', 'Observation', 'CreatedAt', 'UpdatedAt', ),
        self::TYPE_CAMELNAME     => array('idPrix', 'idPrestation', 'montant', 'dateDebut', 'dateFin', 'observation', 'createdAt', 'updatedAt', ),
        self::TYPE_COLNAME       => array(PrixTableMap::COL_ID_PRIX, PrixTableMap::COL_ID_PRESTATION, PrixTableMap::COL_MONTANT, PrixTableMap::COL_DATE_DEBUT, PrixTableMap::COL_DATE_FIN, PrixTableMap::COL_OBSERVATION, PrixTableMap::COL_CREATED_AT, PrixTableMap::COL_UPDATED_AT, ),
        self::TYPE_FIELDNAME     => array('id_prix', 'id_prestation', 'montant', 'date_debut', 'date_fin', 'observation', 'created_at', 'updated_at', ),
        self::TYPE_NUM           => array(0, 1, 2, 3, 4, 5, 6, 7, )
    );

    /**
     * holds an array of keys for quick access to the fieldnames array
     *
     * first dimension keys are the type constants
     * e.g. self::$fieldKeys[self::TYPE_PHPNAME]['Id'] = 0
     */
    protected static $fieldKeys = array (
        self::TYPE_PHPNAME       => array('IdPrix' => 0, 'IdPrestation' => 1, 'Montant' => 2, 'DateDebut' => 3, 'DateFin' => 4, 'Observation' => 5, 'CreatedAt' => 6, 'UpdatedAt' => 7, ),
        self::TYPE_CAMELNAME     => array('idPrix' => 0, 'idPrestation' => 1, 'montant' => 2, 'dateDebut' => 3, 'dateFin' => 4, 'observation' => 5, 'createdAt' => 6, 'updatedAt' => 7, ),
        self::TYPE_COLNAME       => array(PrixTableMap::COL_ID_PRIX => 0, PrixTableMap::COL_ID_PRESTATION => 1, PrixTableMap::COL_MONTANT => 2, PrixTableMap::COL_DATE_DEBUT => 3, PrixTableMap::COL_DATE_FIN => 4, PrixTableMap::COL_OBSERVATION => 5, PrixTableMap::COL_CREATED_AT => 6, PrixTableMap::COL_UPDATED_AT => 7, ),
        self::TYPE_FIELDNAME     => array('id_prix' => 0, 'id_prestation' => 1, 'montant' => 2, 'date_debut' => 3, 'date_fin' => 4, 'observation' => 5, 'created_at' => 6, 'updated_at' => 7, ),
        self::TYPE_NUM           => array(0, 1, 2, 3, 4, 5, 6, 7, )
    );

    /**
     * Initialize the table attributes and columns
     * Relations are not initialized by this method since they are lazy loaded
     *
     * @return void
     * @throws PropelException
     */
    public function initialize()
    {
        // attributes
        $this->setName('asso_prixs');
        $this->setPhpName('Prix');
        $this->setIdentifierQuoting(true);
        $this->setClassName('\\Prix');
        $this->setPackage('');
        $this->setUseIdGenerator(true);
        // columns
        $this->addPrimaryKey('id_prix', 'IdPrix', 'BIGINT', true, 21, null);
        $this->addForeignKey('id_prestation', 'IdPrestation', 'BIGINT', 'asso_prestations', 'id_prestation', true, 21, null);
        $this->addColumn('montant', 'Montant', 'DOUBLE', true, 8, 0);
        $this->addColumn('date_debut', 'DateDebut', 'DATE', true, null, '1980-01-01');
        $this->addColumn('date_fin', 'DateFin', 'DATE', true, null, '3000-12-31');
        $this->addColumn('observation', 'Observation', 'VARCHAR', false, 100, null);
        $this->addColumn('created_at', 'CreatedAt', 'TIMESTAMP', false, null, null);
        $this->addColumn('updated_at', 'UpdatedAt', 'TIMESTAMP', false, null, null);
    } // initialize()

    /**
     * Build the RelationMap objects for this table relationships
     */
    public function buildRelations()
    {
        $this->addRelation('Prestation', '\\Prestation', RelationMap::MANY_TO_ONE, array (
  0 =>
  array (
    0 => ':id_prestation',
    1 => ':id_prestation',
  ),
), 'CASCADE', null, null, false);
    } // buildRelations()

    /**
     *
     * Gets the list of behaviors registered for this table
     *
     * @return array Associative array (name => parameters) of behaviors
     */
    public function getBehaviors()
    {
        return array(
            'timestampable' => array('create_column' => 'created_at', 'update_column' => 'updated_at', 'disable_created_at' => 'false', 'disable_updated_at' => 'false', ),
            'archivable' => array('archive_table' => '', 'archive_phpname' => '', 'archive_class' => '', 'log_archived_at' => 'true', 'archived_at_column' => 'archived_at', 'archive_on_insert' => 'false', 'archive_on_update' => 'false', 'archive_on_delete' => 'true', ),
        );
    } // getBehaviors()

    /**
     * Retrieves a string version of the primary key from the DB resultset row that can be used to uniquely identify a row in this table.
     *
     * For tables with a single-column primary key, that simple pkey value will be returned.  For tables with
     * a multi-column primary key, a serialize()d version of the primary key will be returned.
     *
     * @param array  $row       resultset row.
     * @param int    $offset    The 0-based offset for reading from the resultset row.
     * @param string $indexType One of the class type constants TableMap::TYPE_PHPNAME, TableMap::TYPE_CAMELNAME
     *                           TableMap::TYPE_COLNAME, TableMap::TYPE_FIELDNAME, TableMap::TYPE_NUM
     *
     * @return string The primary key hash of the row
     */
    public static function getPrimaryKeyHashFromRow($row, $offset = 0, $indexType = TableMap::TYPE_NUM)
    {
        // If the PK cannot be derived from the row, return NULL.
        if ($row[TableMap::TYPE_NUM == $indexType ? 0 + $offset : static::translateFieldName('IdPrix', TableMap::TYPE_PHPNAME, $indexType)] === null) {
            return null;
        }

        return null === $row[TableMap::TYPE_NUM == $indexType ? 0 + $offset : static::translateFieldName('IdPrix', TableMap::TYPE_PHPNAME, $indexType)] || is_scalar($row[TableMap::TYPE_NUM == $indexType ? 0 + $offset : static::translateFieldName('IdPrix', TableMap::TYPE_PHPNAME, $indexType)]) || is_callable([$row[TableMap::TYPE_NUM == $indexType ? 0 + $offset : static::translateFieldName('IdPrix', TableMap::TYPE_PHPNAME, $indexType)], '__toString']) ? (string) $row[TableMap::TYPE_NUM == $indexType ? 0 + $offset : static::translateFieldName('IdPrix', TableMap::TYPE_PHPNAME, $indexType)] : $row[TableMap::TYPE_NUM == $indexType ? 0 + $offset : static::translateFieldName('IdPrix', TableMap::TYPE_PHPNAME, $indexType)];
    }

    /**
     * Retrieves the primary key from the DB resultset row
     * For tables with a single-column primary key, that simple pkey value will be returned.  For tables with
     * a multi-column primary key, an array of the primary key columns will be returned.
     *
     * @param array  $row       resultset row.
     * @param int    $offset    The 0-based offset for reading from the resultset row.
     * @param string $indexType One of the class type constants TableMap::TYPE_PHPNAME, TableMap::TYPE_CAMELNAME
     *                           TableMap::TYPE_COLNAME, TableMap::TYPE_FIELDNAME, TableMap::TYPE_NUM
     *
     * @return mixed The primary key of the row
     */
    public static function getPrimaryKeyFromRow($row, $offset = 0, $indexType = TableMap::TYPE_NUM)
    {
        return (string) $row[
            $indexType == TableMap::TYPE_NUM
                ? 0 + $offset
                : self::translateFieldName('IdPrix', TableMap::TYPE_PHPNAME, $indexType)
        ];
    }

    /**
     * The class that the tableMap will make instances of.
     *
     * If $withPrefix is true, the returned path
     * uses a dot-path notation which is translated into a path
     * relative to a location on the PHP include_path.
     * (e.g. path.to.MyClass -> 'path/to/MyClass.php')
     *
     * @param boolean $withPrefix Whether or not to return the path with the class name
     * @return string path.to.ClassName
     */
    public static function getOMClass($withPrefix = true)
    {
        return $withPrefix ? PrixTableMap::CLASS_DEFAULT : PrixTableMap::OM_CLASS;
    }

    /**
     * Populates an object of the default type or an object that inherit from the default.
     *
     * @param array  $row       row returned by DataFetcher->fetch().
     * @param int    $offset    The 0-based offset for reading from the resultset row.
     * @param string $indexType The index type of $row. Mostly DataFetcher->getIndexType().
                                 One of the class type constants TableMap::TYPE_PHPNAME, TableMap::TYPE_CAMELNAME
     *                           TableMap::TYPE_COLNAME, TableMap::TYPE_FIELDNAME, TableMap::TYPE_NUM.
     *
     * @throws PropelException Any exceptions caught during processing will be
     *                         rethrown wrapped into a PropelException.
     * @return array           (Prix object, last column rank)
     */
    public static function populateObject($row, $offset = 0, $indexType = TableMap::TYPE_NUM)
    {
        $key = PrixTableMap::getPrimaryKeyHashFromRow($row, $offset, $indexType);
        if (null !== ($obj = PrixTableMap::getInstanceFromPool($key))) {
            // We no longer rehydrate the object, since this can cause data loss.
            // See http://www.propelorm.org/ticket/509
            // $obj->hydrate($row, $offset, true); // rehydrate
            $col = $offset + PrixTableMap::NUM_HYDRATE_COLUMNS;
        } else {
            $cls = PrixTableMap::OM_CLASS;
            /** @var Prix $obj */
            $obj = new $cls();
            $col = $obj->hydrate($row, $offset, false, $indexType);
            PrixTableMap::addInstanceToPool($obj, $key);
        }

        return array($obj, $col);
    }

    /**
     * The returned array will contain objects of the default type or
     * objects that inherit from the default.
     *
     * @param DataFetcherInterface $dataFetcher
     * @return array
     * @throws PropelException Any exceptions caught during processing will be
     *                         rethrown wrapped into a PropelException.
     */
    public static function populateObjects(DataFetcherInterface $dataFetcher)
    {
        $results = array();

        // set the class once to avoid overhead in the loop
        $cls = static::getOMClass(false);
        // populate the object(s)
        while ($row = $dataFetcher->fetch()) {
            $key = PrixTableMap::getPrimaryKeyHashFromRow($row, 0, $dataFetcher->getIndexType());
            if (null !== ($obj = PrixTableMap::getInstanceFromPool($key))) {
                // We no longer rehydrate the object, since this can cause data loss.
                // See http://www.propelorm.org/ticket/509
                // $obj->hydrate($row, 0, true); // rehydrate
                $results[] = $obj;
            } else {
                /** @var Prix $obj */
                $obj = new $cls();
                $obj->hydrate($row);
                $results[] = $obj;
                PrixTableMap::addInstanceToPool($obj, $key);
            } // if key exists
        }

        return $results;
    }
    /**
     * Add all the columns needed to create a new object.
     *
     * Note: any columns that were marked with lazyLoad="true" in the
     * XML schema will not be added to the select list and only loaded
     * on demand.
     *
     * @param Criteria $criteria object containing the columns to add.
     * @param string   $alias    optional table alias
     * @throws PropelException Any exceptions caught during processing will be
     *                         rethrown wrapped into a PropelException.
     */
    public static function addSelectColumns(Criteria $criteria, $alias = null)
    {
        if (null === $alias) {
            $criteria->addSelectColumn(PrixTableMap::COL_ID_PRIX);
            $criteria->addSelectColumn(PrixTableMap::COL_ID_PRESTATION);
            $criteria->addSelectColumn(PrixTableMap::COL_MONTANT);
            $criteria->addSelectColumn(PrixTableMap::COL_DATE_DEBUT);
            $criteria->addSelectColumn(PrixTableMap::COL_DATE_FIN);
            $criteria->addSelectColumn(PrixTableMap::COL_OBSERVATION);
            $criteria->addSelectColumn(PrixTableMap::COL_CREATED_AT);
            $criteria->addSelectColumn(PrixTableMap::COL_UPDATED_AT);
        } else {
            $criteria->addSelectColumn($alias . '.id_prix');
            $criteria->addSelectColumn($alias . '.id_prestation');
            $criteria->addSelectColumn($alias . '.montant');
            $criteria->addSelectColumn($alias . '.date_debut');
            $criteria->addSelectColumn($alias . '.date_fin');
            $criteria->addSelectColumn($alias . '.observation');
            $criteria->addSelectColumn($alias . '.created_at');
            $criteria->addSelectColumn($alias . '.updated_at');
        }
    }

    /**
     * Returns the TableMap related to this object.
     * This method is not needed for general use but a specific application could have a need.
     * @return TableMap
     * @throws PropelException Any exceptions caught during processing will be
     *                         rethrown wrapped into a PropelException.
     */
    public static function getTableMap()
    {
        return Propel::getServiceContainer()->getDatabaseMap(PrixTableMap::DATABASE_NAME)->getTable(PrixTableMap::TABLE_NAME);
    }

    /**
     * Add a TableMap instance to the database for this tableMap class.
     */
    public static function buildTableMap()
    {
        $dbMap = Propel::getServiceContainer()->getDatabaseMap(PrixTableMap::DATABASE_NAME);
        if (!$dbMap->hasTable(PrixTableMap::TABLE_NAME)) {
            $dbMap->addTableObject(new PrixTableMap());
        }
    }

    /**
     * Performs a DELETE on the database, given a Prix or Criteria object OR a primary key value.
     *
     * @param mixed               $values Criteria or Prix object or primary key or array of primary keys
     *              which is used to create the DELETE statement
     * @param  ConnectionInterface $con the connection to use
     * @return int             The number of affected rows (if supported by underlying database driver).  This includes CASCADE-related rows
     *                         if supported by native driver or if emulated using Propel.
     * @throws PropelException Any exceptions caught during processing will be
     *                         rethrown wrapped into a PropelException.
     */
     public static function doDelete($values, ConnectionInterface $con = null)
     {
        if (null === $con) {
            $con = Propel::getServiceContainer()->getWriteConnection(PrixTableMap::DATABASE_NAME);
        }

        if ($values instanceof Criteria) {
            // rename for clarity
            $criteria = $values;
        } elseif ($values instanceof \Prix) { // it's a model object
            // create criteria based on pk values
            $criteria = $values->buildPkeyCriteria();
        } else { // it's a primary key, or an array of pks
            $criteria = new Criteria(PrixTableMap::DATABASE_NAME);
            $criteria->add(PrixTableMap::COL_ID_PRIX, (array) $values, Criteria::IN);
        }

        $query = PrixQuery::create()->mergeWith($criteria);

        if ($values instanceof Criteria) {
            PrixTableMap::clearInstancePool();
        } elseif (!is_object($values)) { // it's a primary key, or an array of pks
            foreach ((array) $values as $singleval) {
                PrixTableMap::removeInstanceFromPool($singleval);
            }
        }

        return $query->delete($con);
    }

    /**
     * Deletes all rows from the asso_prixs table.
     *
     * @param ConnectionInterface $con the connection to use
     * @return int The number of affected rows (if supported by underlying database driver).
     */
    public static function doDeleteAll(ConnectionInterface $con = null)
    {
        return PrixQuery::create()->doDeleteAll($con);
    }

    /**
     * Performs an INSERT on the database, given a Prix or Criteria object.
     *
     * @param mixed               $criteria Criteria or Prix object containing data that is used to create the INSERT statement.
     * @param ConnectionInterface $con the ConnectionInterface connection to use
     * @return mixed           The new primary key.
     * @throws PropelException Any exceptions caught during processing will be
     *                         rethrown wrapped into a PropelException.
     */
    public static function doInsert($criteria, ConnectionInterface $con = null)
    {
        if (null === $con) {
            $con = Propel::getServiceContainer()->getWriteConnection(PrixTableMap::DATABASE_NAME);
        }

        if ($criteria instanceof Criteria) {
            $criteria = clone $criteria; // rename for clarity
        } else {
            $criteria = $criteria->buildCriteria(); // build Criteria from Prix object
        }

        if ($criteria->containsKey(PrixTableMap::COL_ID_PRIX) && $criteria->keyContainsValue(PrixTableMap::COL_ID_PRIX) ) {
            throw new PropelException('Cannot insert a value for auto-increment primary key ('.PrixTableMap::COL_ID_PRIX.')');
        }


        // Set the correct dbName
        $query = PrixQuery::create()->mergeWith($criteria);

        // use transaction because $criteria could contain info
        // for more than one table (I guess, conceivably)
        return $con->transaction(function () use ($con, $query) {
            return $query->doInsert($con);
        });
    }

} // PrixTableMap
// This is the static code needed to register the TableMap for this table with the main Propel class.
//
PrixTableMap::buildTableMap();
