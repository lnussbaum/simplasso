<?php

namespace Map;

use \Tache;
use \TacheQuery;
use Propel\Runtime\Propel;
use Propel\Runtime\ActiveQuery\Criteria;
use Propel\Runtime\ActiveQuery\InstancePoolTrait;
use Propel\Runtime\Connection\ConnectionInterface;
use Propel\Runtime\DataFetcher\DataFetcherInterface;
use Propel\Runtime\Exception\PropelException;
use Propel\Runtime\Map\RelationMap;
use Propel\Runtime\Map\TableMap;
use Propel\Runtime\Map\TableMapTrait;


/**
 * This class defines the structure of the 'asso_taches' table.
 *
 *
 *
 * This map class is used by Propel to do runtime db structure discovery.
 * For example, the createSelectSql() method checks the type of a given column used in an
 * ORDER BY clause to know whether it needs to apply SQL to make the ORDER BY case-insensitive
 * (i.e. if it's a text column type).
 *
 */
class TacheTableMap extends TableMap
{
    use InstancePoolTrait;
    use TableMapTrait;

    /**
     * The (dot-path) name of this class
     */
    const CLASS_NAME = '.Map.TacheTableMap';

    /**
     * The default database name for this class
     */
    const DATABASE_NAME = 'simplasso';

    /**
     * The table name for this class
     */
    const TABLE_NAME = 'asso_taches';

    /**
     * The related Propel class for this table
     */
    const OM_CLASS = '\\Tache';

    /**
     * A class that can be returned by this tableMap
     */
    const CLASS_DEFAULT = 'Tache';

    /**
     * The total number of columns
     */
    const NUM_COLUMNS = 9;

    /**
     * The number of lazy-loaded columns
     */
    const NUM_LAZY_LOAD_COLUMNS = 0;

    /**
     * The number of columns to hydrate (NUM_COLUMNS - NUM_LAZY_LOAD_COLUMNS)
     */
    const NUM_HYDRATE_COLUMNS = 9;

    /**
     * the column name for the id_tache field
     */
    const COL_ID_TACHE = 'asso_taches.id_tache';

    /**
     * the column name for the descriptif field
     */
    const COL_DESCRIPTIF = 'asso_taches.descriptif';

    /**
     * the column name for the fonction field
     */
    const COL_FONCTION = 'asso_taches.fonction';

    /**
     * the column name for the args field
     */
    const COL_ARGS = 'asso_taches.args';

    /**
     * the column name for the statut field
     */
    const COL_STATUT = 'asso_taches.statut';

    /**
     * the column name for the priorite field
     */
    const COL_PRIORITE = 'asso_taches.priorite';

    /**
     * the column name for the date_execution field
     */
    const COL_DATE_EXECUTION = 'asso_taches.date_execution';

    /**
     * the column name for the created_at field
     */
    const COL_CREATED_AT = 'asso_taches.created_at';

    /**
     * the column name for the updated_at field
     */
    const COL_UPDATED_AT = 'asso_taches.updated_at';

    /**
     * The default string format for model objects of the related table
     */
    const DEFAULT_STRING_FORMAT = 'YAML';

    /**
     * holds an array of fieldnames
     *
     * first dimension keys are the type constants
     * e.g. self::$fieldNames[self::TYPE_PHPNAME][0] = 'Id'
     */
    protected static $fieldNames = array (
        self::TYPE_PHPNAME       => array('IdTache', 'Descriptif', 'Fonction', 'Args', 'Statut', 'Priorite', 'DateExecution', 'CreatedAt', 'UpdatedAt', ),
        self::TYPE_CAMELNAME     => array('idTache', 'descriptif', 'fonction', 'args', 'statut', 'priorite', 'dateExecution', 'createdAt', 'updatedAt', ),
        self::TYPE_COLNAME       => array(TacheTableMap::COL_ID_TACHE, TacheTableMap::COL_DESCRIPTIF, TacheTableMap::COL_FONCTION, TacheTableMap::COL_ARGS, TacheTableMap::COL_STATUT, TacheTableMap::COL_PRIORITE, TacheTableMap::COL_DATE_EXECUTION, TacheTableMap::COL_CREATED_AT, TacheTableMap::COL_UPDATED_AT, ),
        self::TYPE_FIELDNAME     => array('id_tache', 'descriptif', 'fonction', 'args', 'statut', 'priorite', 'date_execution', 'created_at', 'updated_at', ),
        self::TYPE_NUM           => array(0, 1, 2, 3, 4, 5, 6, 7, 8, )
    );

    /**
     * holds an array of keys for quick access to the fieldnames array
     *
     * first dimension keys are the type constants
     * e.g. self::$fieldKeys[self::TYPE_PHPNAME]['Id'] = 0
     */
    protected static $fieldKeys = array (
        self::TYPE_PHPNAME       => array('IdTache' => 0, 'Descriptif' => 1, 'Fonction' => 2, 'Args' => 3, 'Statut' => 4, 'Priorite' => 5, 'DateExecution' => 6, 'CreatedAt' => 7, 'UpdatedAt' => 8, ),
        self::TYPE_CAMELNAME     => array('idTache' => 0, 'descriptif' => 1, 'fonction' => 2, 'args' => 3, 'statut' => 4, 'priorite' => 5, 'dateExecution' => 6, 'createdAt' => 7, 'updatedAt' => 8, ),
        self::TYPE_COLNAME       => array(TacheTableMap::COL_ID_TACHE => 0, TacheTableMap::COL_DESCRIPTIF => 1, TacheTableMap::COL_FONCTION => 2, TacheTableMap::COL_ARGS => 3, TacheTableMap::COL_STATUT => 4, TacheTableMap::COL_PRIORITE => 5, TacheTableMap::COL_DATE_EXECUTION => 6, TacheTableMap::COL_CREATED_AT => 7, TacheTableMap::COL_UPDATED_AT => 8, ),
        self::TYPE_FIELDNAME     => array('id_tache' => 0, 'descriptif' => 1, 'fonction' => 2, 'args' => 3, 'statut' => 4, 'priorite' => 5, 'date_execution' => 6, 'created_at' => 7, 'updated_at' => 8, ),
        self::TYPE_NUM           => array(0, 1, 2, 3, 4, 5, 6, 7, 8, )
    );

    /**
     * Initialize the table attributes and columns
     * Relations are not initialized by this method since they are lazy loaded
     *
     * @return void
     * @throws PropelException
     */
    public function initialize()
    {
        // attributes
        $this->setName('asso_taches');
        $this->setPhpName('Tache');
        $this->setIdentifierQuoting(true);
        $this->setClassName('\\Tache');
        $this->setPackage('');
        $this->setUseIdGenerator(true);
        // columns
        $this->addPrimaryKey('id_tache', 'IdTache', 'BIGINT', true, 21, null);
        $this->addColumn('descriptif', 'Descriptif', 'LONGVARCHAR', true, null, null);
        $this->addColumn('fonction', 'Fonction', 'LONGVARCHAR', true, null, null);
        $this->addColumn('args', 'Args', 'LONGVARCHAR', true, null, null);
        $this->addColumn('statut', 'Statut', 'INTEGER', true, 1, 1);
        $this->addColumn('priorite', 'Priorite', 'INTEGER', true, 2, 50);
        $this->addColumn('date_execution', 'DateExecution', 'TIMESTAMP', true, null, null);
        $this->addColumn('created_at', 'CreatedAt', 'TIMESTAMP', false, null, null);
        $this->addColumn('updated_at', 'UpdatedAt', 'TIMESTAMP', false, null, null);
    } // initialize()

    /**
     * Build the RelationMap objects for this table relationships
     */
    public function buildRelations()
    {
    } // buildRelations()

    /**
     *
     * Gets the list of behaviors registered for this table
     *
     * @return array Associative array (name => parameters) of behaviors
     */
    public function getBehaviors()
    {
        return array(
            'timestampable' => array('create_column' => 'created_at', 'update_column' => 'updated_at', 'disable_created_at' => 'false', 'disable_updated_at' => 'false', ),
        );
    } // getBehaviors()

    /**
     * Retrieves a string version of the primary key from the DB resultset row that can be used to uniquely identify a row in this table.
     *
     * For tables with a single-column primary key, that simple pkey value will be returned.  For tables with
     * a multi-column primary key, a serialize()d version of the primary key will be returned.
     *
     * @param array  $row       resultset row.
     * @param int    $offset    The 0-based offset for reading from the resultset row.
     * @param string $indexType One of the class type constants TableMap::TYPE_PHPNAME, TableMap::TYPE_CAMELNAME
     *                           TableMap::TYPE_COLNAME, TableMap::TYPE_FIELDNAME, TableMap::TYPE_NUM
     *
     * @return string The primary key hash of the row
     */
    public static function getPrimaryKeyHashFromRow($row, $offset = 0, $indexType = TableMap::TYPE_NUM)
    {
        // If the PK cannot be derived from the row, return NULL.
        if ($row[TableMap::TYPE_NUM == $indexType ? 0 + $offset : static::translateFieldName('IdTache', TableMap::TYPE_PHPNAME, $indexType)] === null) {
            return null;
        }

        return null === $row[TableMap::TYPE_NUM == $indexType ? 0 + $offset : static::translateFieldName('IdTache', TableMap::TYPE_PHPNAME, $indexType)] || is_scalar($row[TableMap::TYPE_NUM == $indexType ? 0 + $offset : static::translateFieldName('IdTache', TableMap::TYPE_PHPNAME, $indexType)]) || is_callable([$row[TableMap::TYPE_NUM == $indexType ? 0 + $offset : static::translateFieldName('IdTache', TableMap::TYPE_PHPNAME, $indexType)], '__toString']) ? (string) $row[TableMap::TYPE_NUM == $indexType ? 0 + $offset : static::translateFieldName('IdTache', TableMap::TYPE_PHPNAME, $indexType)] : $row[TableMap::TYPE_NUM == $indexType ? 0 + $offset : static::translateFieldName('IdTache', TableMap::TYPE_PHPNAME, $indexType)];
    }

    /**
     * Retrieves the primary key from the DB resultset row
     * For tables with a single-column primary key, that simple pkey value will be returned.  For tables with
     * a multi-column primary key, an array of the primary key columns will be returned.
     *
     * @param array  $row       resultset row.
     * @param int    $offset    The 0-based offset for reading from the resultset row.
     * @param string $indexType One of the class type constants TableMap::TYPE_PHPNAME, TableMap::TYPE_CAMELNAME
     *                           TableMap::TYPE_COLNAME, TableMap::TYPE_FIELDNAME, TableMap::TYPE_NUM
     *
     * @return mixed The primary key of the row
     */
    public static function getPrimaryKeyFromRow($row, $offset = 0, $indexType = TableMap::TYPE_NUM)
    {
        return (string) $row[
            $indexType == TableMap::TYPE_NUM
                ? 0 + $offset
                : self::translateFieldName('IdTache', TableMap::TYPE_PHPNAME, $indexType)
        ];
    }

    /**
     * The class that the tableMap will make instances of.
     *
     * If $withPrefix is true, the returned path
     * uses a dot-path notation which is translated into a path
     * relative to a location on the PHP include_path.
     * (e.g. path.to.MyClass -> 'path/to/MyClass.php')
     *
     * @param boolean $withPrefix Whether or not to return the path with the class name
     * @return string path.to.ClassName
     */
    public static function getOMClass($withPrefix = true)
    {
        return $withPrefix ? TacheTableMap::CLASS_DEFAULT : TacheTableMap::OM_CLASS;
    }

    /**
     * Populates an object of the default type or an object that inherit from the default.
     *
     * @param array  $row       row returned by DataFetcher->fetch().
     * @param int    $offset    The 0-based offset for reading from the resultset row.
     * @param string $indexType The index type of $row. Mostly DataFetcher->getIndexType().
                                 One of the class type constants TableMap::TYPE_PHPNAME, TableMap::TYPE_CAMELNAME
     *                           TableMap::TYPE_COLNAME, TableMap::TYPE_FIELDNAME, TableMap::TYPE_NUM.
     *
     * @throws PropelException Any exceptions caught during processing will be
     *                         rethrown wrapped into a PropelException.
     * @return array           (Tache object, last column rank)
     */
    public static function populateObject($row, $offset = 0, $indexType = TableMap::TYPE_NUM)
    {
        $key = TacheTableMap::getPrimaryKeyHashFromRow($row, $offset, $indexType);
        if (null !== ($obj = TacheTableMap::getInstanceFromPool($key))) {
            // We no longer rehydrate the object, since this can cause data loss.
            // See http://www.propelorm.org/ticket/509
            // $obj->hydrate($row, $offset, true); // rehydrate
            $col = $offset + TacheTableMap::NUM_HYDRATE_COLUMNS;
        } else {
            $cls = TacheTableMap::OM_CLASS;
            /** @var Tache $obj */
            $obj = new $cls();
            $col = $obj->hydrate($row, $offset, false, $indexType);
            TacheTableMap::addInstanceToPool($obj, $key);
        }

        return array($obj, $col);
    }

    /**
     * The returned array will contain objects of the default type or
     * objects that inherit from the default.
     *
     * @param DataFetcherInterface $dataFetcher
     * @return array
     * @throws PropelException Any exceptions caught during processing will be
     *                         rethrown wrapped into a PropelException.
     */
    public static function populateObjects(DataFetcherInterface $dataFetcher)
    {
        $results = array();

        // set the class once to avoid overhead in the loop
        $cls = static::getOMClass(false);
        // populate the object(s)
        while ($row = $dataFetcher->fetch()) {
            $key = TacheTableMap::getPrimaryKeyHashFromRow($row, 0, $dataFetcher->getIndexType());
            if (null !== ($obj = TacheTableMap::getInstanceFromPool($key))) {
                // We no longer rehydrate the object, since this can cause data loss.
                // See http://www.propelorm.org/ticket/509
                // $obj->hydrate($row, 0, true); // rehydrate
                $results[] = $obj;
            } else {
                /** @var Tache $obj */
                $obj = new $cls();
                $obj->hydrate($row);
                $results[] = $obj;
                TacheTableMap::addInstanceToPool($obj, $key);
            } // if key exists
        }

        return $results;
    }
    /**
     * Add all the columns needed to create a new object.
     *
     * Note: any columns that were marked with lazyLoad="true" in the
     * XML schema will not be added to the select list and only loaded
     * on demand.
     *
     * @param Criteria $criteria object containing the columns to add.
     * @param string   $alias    optional table alias
     * @throws PropelException Any exceptions caught during processing will be
     *                         rethrown wrapped into a PropelException.
     */
    public static function addSelectColumns(Criteria $criteria, $alias = null)
    {
        if (null === $alias) {
            $criteria->addSelectColumn(TacheTableMap::COL_ID_TACHE);
            $criteria->addSelectColumn(TacheTableMap::COL_DESCRIPTIF);
            $criteria->addSelectColumn(TacheTableMap::COL_FONCTION);
            $criteria->addSelectColumn(TacheTableMap::COL_ARGS);
            $criteria->addSelectColumn(TacheTableMap::COL_STATUT);
            $criteria->addSelectColumn(TacheTableMap::COL_PRIORITE);
            $criteria->addSelectColumn(TacheTableMap::COL_DATE_EXECUTION);
            $criteria->addSelectColumn(TacheTableMap::COL_CREATED_AT);
            $criteria->addSelectColumn(TacheTableMap::COL_UPDATED_AT);
        } else {
            $criteria->addSelectColumn($alias . '.id_tache');
            $criteria->addSelectColumn($alias . '.descriptif');
            $criteria->addSelectColumn($alias . '.fonction');
            $criteria->addSelectColumn($alias . '.args');
            $criteria->addSelectColumn($alias . '.statut');
            $criteria->addSelectColumn($alias . '.priorite');
            $criteria->addSelectColumn($alias . '.date_execution');
            $criteria->addSelectColumn($alias . '.created_at');
            $criteria->addSelectColumn($alias . '.updated_at');
        }
    }

    /**
     * Returns the TableMap related to this object.
     * This method is not needed for general use but a specific application could have a need.
     * @return TableMap
     * @throws PropelException Any exceptions caught during processing will be
     *                         rethrown wrapped into a PropelException.
     */
    public static function getTableMap()
    {
        return Propel::getServiceContainer()->getDatabaseMap(TacheTableMap::DATABASE_NAME)->getTable(TacheTableMap::TABLE_NAME);
    }

    /**
     * Add a TableMap instance to the database for this tableMap class.
     */
    public static function buildTableMap()
    {
        $dbMap = Propel::getServiceContainer()->getDatabaseMap(TacheTableMap::DATABASE_NAME);
        if (!$dbMap->hasTable(TacheTableMap::TABLE_NAME)) {
            $dbMap->addTableObject(new TacheTableMap());
        }
    }

    /**
     * Performs a DELETE on the database, given a Tache or Criteria object OR a primary key value.
     *
     * @param mixed               $values Criteria or Tache object or primary key or array of primary keys
     *              which is used to create the DELETE statement
     * @param  ConnectionInterface $con the connection to use
     * @return int             The number of affected rows (if supported by underlying database driver).  This includes CASCADE-related rows
     *                         if supported by native driver or if emulated using Propel.
     * @throws PropelException Any exceptions caught during processing will be
     *                         rethrown wrapped into a PropelException.
     */
     public static function doDelete($values, ConnectionInterface $con = null)
     {
        if (null === $con) {
            $con = Propel::getServiceContainer()->getWriteConnection(TacheTableMap::DATABASE_NAME);
        }

        if ($values instanceof Criteria) {
            // rename for clarity
            $criteria = $values;
        } elseif ($values instanceof \Tache) { // it's a model object
            // create criteria based on pk values
            $criteria = $values->buildPkeyCriteria();
        } else { // it's a primary key, or an array of pks
            $criteria = new Criteria(TacheTableMap::DATABASE_NAME);
            $criteria->add(TacheTableMap::COL_ID_TACHE, (array) $values, Criteria::IN);
        }

        $query = TacheQuery::create()->mergeWith($criteria);

        if ($values instanceof Criteria) {
            TacheTableMap::clearInstancePool();
        } elseif (!is_object($values)) { // it's a primary key, or an array of pks
            foreach ((array) $values as $singleval) {
                TacheTableMap::removeInstanceFromPool($singleval);
            }
        }

        return $query->delete($con);
    }

    /**
     * Deletes all rows from the asso_taches table.
     *
     * @param ConnectionInterface $con the connection to use
     * @return int The number of affected rows (if supported by underlying database driver).
     */
    public static function doDeleteAll(ConnectionInterface $con = null)
    {
        return TacheQuery::create()->doDeleteAll($con);
    }

    /**
     * Performs an INSERT on the database, given a Tache or Criteria object.
     *
     * @param mixed               $criteria Criteria or Tache object containing data that is used to create the INSERT statement.
     * @param ConnectionInterface $con the ConnectionInterface connection to use
     * @return mixed           The new primary key.
     * @throws PropelException Any exceptions caught during processing will be
     *                         rethrown wrapped into a PropelException.
     */
    public static function doInsert($criteria, ConnectionInterface $con = null)
    {
        if (null === $con) {
            $con = Propel::getServiceContainer()->getWriteConnection(TacheTableMap::DATABASE_NAME);
        }

        if ($criteria instanceof Criteria) {
            $criteria = clone $criteria; // rename for clarity
        } else {
            $criteria = $criteria->buildCriteria(); // build Criteria from Tache object
        }

        if ($criteria->containsKey(TacheTableMap::COL_ID_TACHE) && $criteria->keyContainsValue(TacheTableMap::COL_ID_TACHE) ) {
            throw new PropelException('Cannot insert a value for auto-increment primary key ('.TacheTableMap::COL_ID_TACHE.')');
        }


        // Set the correct dbName
        $query = TacheQuery::create()->mergeWith($criteria);

        // use transaction because $criteria could contain info
        // for more than one table (I guess, conceivably)
        return $con->transaction(function () use ($con, $query) {
            return $query->doInsert($con);
        });
    }

} // TacheTableMap
// This is the static code needed to register the TableMap for this table with the main Propel class.
//
TacheTableMap::buildTableMap();
