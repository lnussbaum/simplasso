<?php

namespace Map;

use \Arrondissement;
use \ArrondissementQuery;
use Propel\Runtime\Propel;
use Propel\Runtime\ActiveQuery\Criteria;
use Propel\Runtime\ActiveQuery\InstancePoolTrait;
use Propel\Runtime\Connection\ConnectionInterface;
use Propel\Runtime\DataFetcher\DataFetcherInterface;
use Propel\Runtime\Exception\PropelException;
use Propel\Runtime\Map\RelationMap;
use Propel\Runtime\Map\TableMap;
use Propel\Runtime\Map\TableMapTrait;


/**
 * This class defines the structure of the 'geo_arrondissements' table.
 *
 *
 *
 * This map class is used by Propel to do runtime db structure discovery.
 * For example, the createSelectSql() method checks the type of a given column used in an
 * ORDER BY clause to know whether it needs to apply SQL to make the ORDER BY case-insensitive
 * (i.e. if it's a text column type).
 *
 */
class ArrondissementTableMap extends TableMap
{
    use InstancePoolTrait;
    use TableMapTrait;

    /**
     * The (dot-path) name of this class
     */
    const CLASS_NAME = '.Map.ArrondissementTableMap';

    /**
     * The default database name for this class
     */
    const DATABASE_NAME = 'simplasso';

    /**
     * The table name for this class
     */
    const TABLE_NAME = 'geo_arrondissements';

    /**
     * The related Propel class for this table
     */
    const OM_CLASS = '\\Arrondissement';

    /**
     * A class that can be returned by this tableMap
     */
    const CLASS_DEFAULT = 'Arrondissement';

    /**
     * The total number of columns
     */
    const NUM_COLUMNS = 11;

    /**
     * The number of lazy-loaded columns
     */
    const NUM_LAZY_LOAD_COLUMNS = 0;

    /**
     * The number of columns to hydrate (NUM_COLUMNS - NUM_LAZY_LOAD_COLUMNS)
     */
    const NUM_HYDRATE_COLUMNS = 11;

    /**
     * the column name for the id_arrondissement field
     */
    const COL_ID_ARRONDISSEMENT = 'geo_arrondissements.id_arrondissement';

    /**
     * the column name for the pays field
     */
    const COL_PAYS = 'geo_arrondissements.pays';

    /**
     * the column name for the departement field
     */
    const COL_DEPARTEMENT = 'geo_arrondissements.departement';

    /**
     * the column name for the region field
     */
    const COL_REGION = 'geo_arrondissements.region';

    /**
     * the column name for the code field
     */
    const COL_CODE = 'geo_arrondissements.code';

    /**
     * the column name for the chef_lieu field
     */
    const COL_CHEF_LIEU = 'geo_arrondissements.chef_lieu';

    /**
     * the column name for the type_charniere field
     */
    const COL_TYPE_CHARNIERE = 'geo_arrondissements.type_charniere';

    /**
     * the column name for the nom field
     */
    const COL_NOM = 'geo_arrondissements.nom';

    /**
     * the column name for the zonage field
     */
    const COL_ZONAGE = 'geo_arrondissements.zonage';

    /**
     * the column name for the created_at field
     */
    const COL_CREATED_AT = 'geo_arrondissements.created_at';

    /**
     * the column name for the updated_at field
     */
    const COL_UPDATED_AT = 'geo_arrondissements.updated_at';

    /**
     * The default string format for model objects of the related table
     */
    const DEFAULT_STRING_FORMAT = 'YAML';

    /**
     * holds an array of fieldnames
     *
     * first dimension keys are the type constants
     * e.g. self::$fieldNames[self::TYPE_PHPNAME][0] = 'Id'
     */
    protected static $fieldNames = array (
        self::TYPE_PHPNAME       => array('IdArrondissement', 'Pays', 'Departement', 'Region', 'Code', 'ChefLieu', 'TypeCharniere', 'Nom', 'Zonage', 'CreatedAt', 'UpdatedAt', ),
        self::TYPE_CAMELNAME     => array('idArrondissement', 'pays', 'departement', 'region', 'code', 'chefLieu', 'typeCharniere', 'nom', 'zonage', 'createdAt', 'updatedAt', ),
        self::TYPE_COLNAME       => array(ArrondissementTableMap::COL_ID_ARRONDISSEMENT, ArrondissementTableMap::COL_PAYS, ArrondissementTableMap::COL_DEPARTEMENT, ArrondissementTableMap::COL_REGION, ArrondissementTableMap::COL_CODE, ArrondissementTableMap::COL_CHEF_LIEU, ArrondissementTableMap::COL_TYPE_CHARNIERE, ArrondissementTableMap::COL_NOM, ArrondissementTableMap::COL_ZONAGE, ArrondissementTableMap::COL_CREATED_AT, ArrondissementTableMap::COL_UPDATED_AT, ),
        self::TYPE_FIELDNAME     => array('id_arrondissement', 'pays', 'departement', 'region', 'code', 'chef_lieu', 'type_charniere', 'nom', 'zonage', 'created_at', 'updated_at', ),
        self::TYPE_NUM           => array(0, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, )
    );

    /**
     * holds an array of keys for quick access to the fieldnames array
     *
     * first dimension keys are the type constants
     * e.g. self::$fieldKeys[self::TYPE_PHPNAME]['Id'] = 0
     */
    protected static $fieldKeys = array (
        self::TYPE_PHPNAME       => array('IdArrondissement' => 0, 'Pays' => 1, 'Departement' => 2, 'Region' => 3, 'Code' => 4, 'ChefLieu' => 5, 'TypeCharniere' => 6, 'Nom' => 7, 'Zonage' => 8, 'CreatedAt' => 9, 'UpdatedAt' => 10, ),
        self::TYPE_CAMELNAME     => array('idArrondissement' => 0, 'pays' => 1, 'departement' => 2, 'region' => 3, 'code' => 4, 'chefLieu' => 5, 'typeCharniere' => 6, 'nom' => 7, 'zonage' => 8, 'createdAt' => 9, 'updatedAt' => 10, ),
        self::TYPE_COLNAME       => array(ArrondissementTableMap::COL_ID_ARRONDISSEMENT => 0, ArrondissementTableMap::COL_PAYS => 1, ArrondissementTableMap::COL_DEPARTEMENT => 2, ArrondissementTableMap::COL_REGION => 3, ArrondissementTableMap::COL_CODE => 4, ArrondissementTableMap::COL_CHEF_LIEU => 5, ArrondissementTableMap::COL_TYPE_CHARNIERE => 6, ArrondissementTableMap::COL_NOM => 7, ArrondissementTableMap::COL_ZONAGE => 8, ArrondissementTableMap::COL_CREATED_AT => 9, ArrondissementTableMap::COL_UPDATED_AT => 10, ),
        self::TYPE_FIELDNAME     => array('id_arrondissement' => 0, 'pays' => 1, 'departement' => 2, 'region' => 3, 'code' => 4, 'chef_lieu' => 5, 'type_charniere' => 6, 'nom' => 7, 'zonage' => 8, 'created_at' => 9, 'updated_at' => 10, ),
        self::TYPE_NUM           => array(0, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, )
    );

    /**
     * Initialize the table attributes and columns
     * Relations are not initialized by this method since they are lazy loaded
     *
     * @return void
     * @throws PropelException
     */
    public function initialize()
    {
        // attributes
        $this->setName('geo_arrondissements');
        $this->setPhpName('Arrondissement');
        $this->setIdentifierQuoting(true);
        $this->setClassName('\\Arrondissement');
        $this->setPackage('');
        $this->setUseIdGenerator(true);
        // columns
        $this->addPrimaryKey('id_arrondissement', 'IdArrondissement', 'INTEGER', true, 10, null);
        $this->addColumn('pays', 'Pays', 'VARCHAR', true, 2, null);
        $this->addColumn('departement', 'Departement', 'VARCHAR', true, 3, null);
        $this->addColumn('region', 'Region', 'TINYINT', true, 2, null);
        $this->addColumn('code', 'Code', 'VARCHAR', true, 3, null);
        $this->addColumn('chef_lieu', 'ChefLieu', 'VARCHAR', true, 5, null);
        $this->addColumn('type_charniere', 'TypeCharniere', 'BOOLEAN', true, 1, null);
        $this->addColumn('nom', 'Nom', 'VARCHAR', true, 70, null);
        $this->addColumn('zonage', 'Zonage', 'LONGVARCHAR', false, null, null);
        $this->addColumn('created_at', 'CreatedAt', 'TIMESTAMP', false, null, null);
        $this->addColumn('updated_at', 'UpdatedAt', 'TIMESTAMP', false, null, null);
    } // initialize()

    /**
     * Build the RelationMap objects for this table relationships
     */
    public function buildRelations()
    {
    } // buildRelations()

    /**
     *
     * Gets the list of behaviors registered for this table
     *
     * @return array Associative array (name => parameters) of behaviors
     */
    public function getBehaviors()
    {
        return array(
            'timestampable' => array('create_column' => 'created_at', 'update_column' => 'updated_at', 'disable_created_at' => 'false', 'disable_updated_at' => 'false', ),
        );
    } // getBehaviors()

    /**
     * Retrieves a string version of the primary key from the DB resultset row that can be used to uniquely identify a row in this table.
     *
     * For tables with a single-column primary key, that simple pkey value will be returned.  For tables with
     * a multi-column primary key, a serialize()d version of the primary key will be returned.
     *
     * @param array  $row       resultset row.
     * @param int    $offset    The 0-based offset for reading from the resultset row.
     * @param string $indexType One of the class type constants TableMap::TYPE_PHPNAME, TableMap::TYPE_CAMELNAME
     *                           TableMap::TYPE_COLNAME, TableMap::TYPE_FIELDNAME, TableMap::TYPE_NUM
     *
     * @return string The primary key hash of the row
     */
    public static function getPrimaryKeyHashFromRow($row, $offset = 0, $indexType = TableMap::TYPE_NUM)
    {
        // If the PK cannot be derived from the row, return NULL.
        if ($row[TableMap::TYPE_NUM == $indexType ? 0 + $offset : static::translateFieldName('IdArrondissement', TableMap::TYPE_PHPNAME, $indexType)] === null) {
            return null;
        }

        return null === $row[TableMap::TYPE_NUM == $indexType ? 0 + $offset : static::translateFieldName('IdArrondissement', TableMap::TYPE_PHPNAME, $indexType)] || is_scalar($row[TableMap::TYPE_NUM == $indexType ? 0 + $offset : static::translateFieldName('IdArrondissement', TableMap::TYPE_PHPNAME, $indexType)]) || is_callable([$row[TableMap::TYPE_NUM == $indexType ? 0 + $offset : static::translateFieldName('IdArrondissement', TableMap::TYPE_PHPNAME, $indexType)], '__toString']) ? (string) $row[TableMap::TYPE_NUM == $indexType ? 0 + $offset : static::translateFieldName('IdArrondissement', TableMap::TYPE_PHPNAME, $indexType)] : $row[TableMap::TYPE_NUM == $indexType ? 0 + $offset : static::translateFieldName('IdArrondissement', TableMap::TYPE_PHPNAME, $indexType)];
    }

    /**
     * Retrieves the primary key from the DB resultset row
     * For tables with a single-column primary key, that simple pkey value will be returned.  For tables with
     * a multi-column primary key, an array of the primary key columns will be returned.
     *
     * @param array  $row       resultset row.
     * @param int    $offset    The 0-based offset for reading from the resultset row.
     * @param string $indexType One of the class type constants TableMap::TYPE_PHPNAME, TableMap::TYPE_CAMELNAME
     *                           TableMap::TYPE_COLNAME, TableMap::TYPE_FIELDNAME, TableMap::TYPE_NUM
     *
     * @return mixed The primary key of the row
     */
    public static function getPrimaryKeyFromRow($row, $offset = 0, $indexType = TableMap::TYPE_NUM)
    {
        return (int) $row[
            $indexType == TableMap::TYPE_NUM
                ? 0 + $offset
                : self::translateFieldName('IdArrondissement', TableMap::TYPE_PHPNAME, $indexType)
        ];
    }

    /**
     * The class that the tableMap will make instances of.
     *
     * If $withPrefix is true, the returned path
     * uses a dot-path notation which is translated into a path
     * relative to a location on the PHP include_path.
     * (e.g. path.to.MyClass -> 'path/to/MyClass.php')
     *
     * @param boolean $withPrefix Whether or not to return the path with the class name
     * @return string path.to.ClassName
     */
    public static function getOMClass($withPrefix = true)
    {
        return $withPrefix ? ArrondissementTableMap::CLASS_DEFAULT : ArrondissementTableMap::OM_CLASS;
    }

    /**
     * Populates an object of the default type or an object that inherit from the default.
     *
     * @param array  $row       row returned by DataFetcher->fetch().
     * @param int    $offset    The 0-based offset for reading from the resultset row.
     * @param string $indexType The index type of $row. Mostly DataFetcher->getIndexType().
                                 One of the class type constants TableMap::TYPE_PHPNAME, TableMap::TYPE_CAMELNAME
     *                           TableMap::TYPE_COLNAME, TableMap::TYPE_FIELDNAME, TableMap::TYPE_NUM.
     *
     * @throws PropelException Any exceptions caught during processing will be
     *                         rethrown wrapped into a PropelException.
     * @return array           (Arrondissement object, last column rank)
     */
    public static function populateObject($row, $offset = 0, $indexType = TableMap::TYPE_NUM)
    {
        $key = ArrondissementTableMap::getPrimaryKeyHashFromRow($row, $offset, $indexType);
        if (null !== ($obj = ArrondissementTableMap::getInstanceFromPool($key))) {
            // We no longer rehydrate the object, since this can cause data loss.
            // See http://www.propelorm.org/ticket/509
            // $obj->hydrate($row, $offset, true); // rehydrate
            $col = $offset + ArrondissementTableMap::NUM_HYDRATE_COLUMNS;
        } else {
            $cls = ArrondissementTableMap::OM_CLASS;
            /** @var Arrondissement $obj */
            $obj = new $cls();
            $col = $obj->hydrate($row, $offset, false, $indexType);
            ArrondissementTableMap::addInstanceToPool($obj, $key);
        }

        return array($obj, $col);
    }

    /**
     * The returned array will contain objects of the default type or
     * objects that inherit from the default.
     *
     * @param DataFetcherInterface $dataFetcher
     * @return array
     * @throws PropelException Any exceptions caught during processing will be
     *                         rethrown wrapped into a PropelException.
     */
    public static function populateObjects(DataFetcherInterface $dataFetcher)
    {
        $results = array();

        // set the class once to avoid overhead in the loop
        $cls = static::getOMClass(false);
        // populate the object(s)
        while ($row = $dataFetcher->fetch()) {
            $key = ArrondissementTableMap::getPrimaryKeyHashFromRow($row, 0, $dataFetcher->getIndexType());
            if (null !== ($obj = ArrondissementTableMap::getInstanceFromPool($key))) {
                // We no longer rehydrate the object, since this can cause data loss.
                // See http://www.propelorm.org/ticket/509
                // $obj->hydrate($row, 0, true); // rehydrate
                $results[] = $obj;
            } else {
                /** @var Arrondissement $obj */
                $obj = new $cls();
                $obj->hydrate($row);
                $results[] = $obj;
                ArrondissementTableMap::addInstanceToPool($obj, $key);
            } // if key exists
        }

        return $results;
    }
    /**
     * Add all the columns needed to create a new object.
     *
     * Note: any columns that were marked with lazyLoad="true" in the
     * XML schema will not be added to the select list and only loaded
     * on demand.
     *
     * @param Criteria $criteria object containing the columns to add.
     * @param string   $alias    optional table alias
     * @throws PropelException Any exceptions caught during processing will be
     *                         rethrown wrapped into a PropelException.
     */
    public static function addSelectColumns(Criteria $criteria, $alias = null)
    {
        if (null === $alias) {
            $criteria->addSelectColumn(ArrondissementTableMap::COL_ID_ARRONDISSEMENT);
            $criteria->addSelectColumn(ArrondissementTableMap::COL_PAYS);
            $criteria->addSelectColumn(ArrondissementTableMap::COL_DEPARTEMENT);
            $criteria->addSelectColumn(ArrondissementTableMap::COL_REGION);
            $criteria->addSelectColumn(ArrondissementTableMap::COL_CODE);
            $criteria->addSelectColumn(ArrondissementTableMap::COL_CHEF_LIEU);
            $criteria->addSelectColumn(ArrondissementTableMap::COL_TYPE_CHARNIERE);
            $criteria->addSelectColumn(ArrondissementTableMap::COL_NOM);
            $criteria->addSelectColumn(ArrondissementTableMap::COL_ZONAGE);
            $criteria->addSelectColumn(ArrondissementTableMap::COL_CREATED_AT);
            $criteria->addSelectColumn(ArrondissementTableMap::COL_UPDATED_AT);
        } else {
            $criteria->addSelectColumn($alias . '.id_arrondissement');
            $criteria->addSelectColumn($alias . '.pays');
            $criteria->addSelectColumn($alias . '.departement');
            $criteria->addSelectColumn($alias . '.region');
            $criteria->addSelectColumn($alias . '.code');
            $criteria->addSelectColumn($alias . '.chef_lieu');
            $criteria->addSelectColumn($alias . '.type_charniere');
            $criteria->addSelectColumn($alias . '.nom');
            $criteria->addSelectColumn($alias . '.zonage');
            $criteria->addSelectColumn($alias . '.created_at');
            $criteria->addSelectColumn($alias . '.updated_at');
        }
    }

    /**
     * Returns the TableMap related to this object.
     * This method is not needed for general use but a specific application could have a need.
     * @return TableMap
     * @throws PropelException Any exceptions caught during processing will be
     *                         rethrown wrapped into a PropelException.
     */
    public static function getTableMap()
    {
        return Propel::getServiceContainer()->getDatabaseMap(ArrondissementTableMap::DATABASE_NAME)->getTable(ArrondissementTableMap::TABLE_NAME);
    }

    /**
     * Add a TableMap instance to the database for this tableMap class.
     */
    public static function buildTableMap()
    {
        $dbMap = Propel::getServiceContainer()->getDatabaseMap(ArrondissementTableMap::DATABASE_NAME);
        if (!$dbMap->hasTable(ArrondissementTableMap::TABLE_NAME)) {
            $dbMap->addTableObject(new ArrondissementTableMap());
        }
    }

    /**
     * Performs a DELETE on the database, given a Arrondissement or Criteria object OR a primary key value.
     *
     * @param mixed               $values Criteria or Arrondissement object or primary key or array of primary keys
     *              which is used to create the DELETE statement
     * @param  ConnectionInterface $con the connection to use
     * @return int             The number of affected rows (if supported by underlying database driver).  This includes CASCADE-related rows
     *                         if supported by native driver or if emulated using Propel.
     * @throws PropelException Any exceptions caught during processing will be
     *                         rethrown wrapped into a PropelException.
     */
     public static function doDelete($values, ConnectionInterface $con = null)
     {
        if (null === $con) {
            $con = Propel::getServiceContainer()->getWriteConnection(ArrondissementTableMap::DATABASE_NAME);
        }

        if ($values instanceof Criteria) {
            // rename for clarity
            $criteria = $values;
        } elseif ($values instanceof \Arrondissement) { // it's a model object
            // create criteria based on pk values
            $criteria = $values->buildPkeyCriteria();
        } else { // it's a primary key, or an array of pks
            $criteria = new Criteria(ArrondissementTableMap::DATABASE_NAME);
            $criteria->add(ArrondissementTableMap::COL_ID_ARRONDISSEMENT, (array) $values, Criteria::IN);
        }

        $query = ArrondissementQuery::create()->mergeWith($criteria);

        if ($values instanceof Criteria) {
            ArrondissementTableMap::clearInstancePool();
        } elseif (!is_object($values)) { // it's a primary key, or an array of pks
            foreach ((array) $values as $singleval) {
                ArrondissementTableMap::removeInstanceFromPool($singleval);
            }
        }

        return $query->delete($con);
    }

    /**
     * Deletes all rows from the geo_arrondissements table.
     *
     * @param ConnectionInterface $con the connection to use
     * @return int The number of affected rows (if supported by underlying database driver).
     */
    public static function doDeleteAll(ConnectionInterface $con = null)
    {
        return ArrondissementQuery::create()->doDeleteAll($con);
    }

    /**
     * Performs an INSERT on the database, given a Arrondissement or Criteria object.
     *
     * @param mixed               $criteria Criteria or Arrondissement object containing data that is used to create the INSERT statement.
     * @param ConnectionInterface $con the ConnectionInterface connection to use
     * @return mixed           The new primary key.
     * @throws PropelException Any exceptions caught during processing will be
     *                         rethrown wrapped into a PropelException.
     */
    public static function doInsert($criteria, ConnectionInterface $con = null)
    {
        if (null === $con) {
            $con = Propel::getServiceContainer()->getWriteConnection(ArrondissementTableMap::DATABASE_NAME);
        }

        if ($criteria instanceof Criteria) {
            $criteria = clone $criteria; // rename for clarity
        } else {
            $criteria = $criteria->buildCriteria(); // build Criteria from Arrondissement object
        }

        if ($criteria->containsKey(ArrondissementTableMap::COL_ID_ARRONDISSEMENT) && $criteria->keyContainsValue(ArrondissementTableMap::COL_ID_ARRONDISSEMENT) ) {
            throw new PropelException('Cannot insert a value for auto-increment primary key ('.ArrondissementTableMap::COL_ID_ARRONDISSEMENT.')');
        }


        // Set the correct dbName
        $query = ArrondissementQuery::create()->mergeWith($criteria);

        // use transaction because $criteria could contain info
        // for more than one table (I guess, conceivably)
        return $con->transaction(function () use ($con, $query) {
            return $query->doInsert($con);
        });
    }

} // ArrondissementTableMap
// This is the static code needed to register the TableMap for this table with the main Propel class.
//
ArrondissementTableMap::buildTableMap();
