<?php

namespace Map;

use \ComCourriersLiensArchive;
use \ComCourriersLiensArchiveQuery;
use Propel\Runtime\Propel;
use Propel\Runtime\ActiveQuery\Criteria;
use Propel\Runtime\ActiveQuery\InstancePoolTrait;
use Propel\Runtime\Connection\ConnectionInterface;
use Propel\Runtime\DataFetcher\DataFetcherInterface;
use Propel\Runtime\Exception\PropelException;
use Propel\Runtime\Map\RelationMap;
use Propel\Runtime\Map\TableMap;
use Propel\Runtime\Map\TableMapTrait;


/**
 * This class defines the structure of the 'com_courriers_liens_archive' table.
 *
 *
 *
 * This map class is used by Propel to do runtime db structure discovery.
 * For example, the createSelectSql() method checks the type of a given column used in an
 * ORDER BY clause to know whether it needs to apply SQL to make the ORDER BY case-insensitive
 * (i.e. if it's a text column type).
 *
 */
class ComCourriersLiensArchiveTableMap extends TableMap
{
    use InstancePoolTrait;
    use TableMapTrait;

    /**
     * The (dot-path) name of this class
     */
    const CLASS_NAME = '.Map.ComCourriersLiensArchiveTableMap';

    /**
     * The default database name for this class
     */
    const DATABASE_NAME = 'simplasso';

    /**
     * The table name for this class
     */
    const TABLE_NAME = 'com_courriers_liens_archive';

    /**
     * The related Propel class for this table
     */
    const OM_CLASS = '\\ComCourriersLiensArchive';

    /**
     * A class that can be returned by this tableMap
     */
    const CLASS_DEFAULT = 'ComCourriersLiensArchive';

    /**
     * The total number of columns
     */
    const NUM_COLUMNS = 12;

    /**
     * The number of lazy-loaded columns
     */
    const NUM_LAZY_LOAD_COLUMNS = 0;

    /**
     * The number of columns to hydrate (NUM_COLUMNS - NUM_LAZY_LOAD_COLUMNS)
     */
    const NUM_HYDRATE_COLUMNS = 12;

    /**
     * the column name for the id_courrier_lien field
     */
    const COL_ID_COURRIER_LIEN = 'com_courriers_liens_archive.id_courrier_lien';

    /**
     * the column name for the id_membre field
     */
    const COL_ID_MEMBRE = 'com_courriers_liens_archive.id_membre';

    /**
     * the column name for the id_individu field
     */
    const COL_ID_INDIVIDU = 'com_courriers_liens_archive.id_individu';

    /**
     * the column name for the id_courrier field
     */
    const COL_ID_COURRIER = 'com_courriers_liens_archive.id_courrier';

    /**
     * the column name for the canal field
     */
    const COL_CANAL = 'com_courriers_liens_archive.canal';

    /**
     * the column name for the date_envoi field
     */
    const COL_DATE_ENVOI = 'com_courriers_liens_archive.date_envoi';

    /**
     * the column name for the message_particulier field
     */
    const COL_MESSAGE_PARTICULIER = 'com_courriers_liens_archive.message_particulier';

    /**
     * the column name for the statut field
     */
    const COL_STATUT = 'com_courriers_liens_archive.statut';

    /**
     * the column name for the variables field
     */
    const COL_VARIABLES = 'com_courriers_liens_archive.variables';

    /**
     * the column name for the created_at field
     */
    const COL_CREATED_AT = 'com_courriers_liens_archive.created_at';

    /**
     * the column name for the updated_at field
     */
    const COL_UPDATED_AT = 'com_courriers_liens_archive.updated_at';

    /**
     * the column name for the archived_at field
     */
    const COL_ARCHIVED_AT = 'com_courriers_liens_archive.archived_at';

    /**
     * The default string format for model objects of the related table
     */
    const DEFAULT_STRING_FORMAT = 'YAML';

    /**
     * holds an array of fieldnames
     *
     * first dimension keys are the type constants
     * e.g. self::$fieldNames[self::TYPE_PHPNAME][0] = 'Id'
     */
    protected static $fieldNames = array (
        self::TYPE_PHPNAME       => array('IdCourrierLien', 'IdMembre', 'IdIndividu', 'IdCourrier', 'Canal', 'DateEnvoi', 'MessageParticulier', 'Statut', 'Variables', 'CreatedAt', 'UpdatedAt', 'ArchivedAt', ),
        self::TYPE_CAMELNAME     => array('idCourrierLien', 'idMembre', 'idIndividu', 'idCourrier', 'canal', 'dateEnvoi', 'messageParticulier', 'statut', 'variables', 'createdAt', 'updatedAt', 'archivedAt', ),
        self::TYPE_COLNAME       => array(ComCourriersLiensArchiveTableMap::COL_ID_COURRIER_LIEN, ComCourriersLiensArchiveTableMap::COL_ID_MEMBRE, ComCourriersLiensArchiveTableMap::COL_ID_INDIVIDU, ComCourriersLiensArchiveTableMap::COL_ID_COURRIER, ComCourriersLiensArchiveTableMap::COL_CANAL, ComCourriersLiensArchiveTableMap::COL_DATE_ENVOI, ComCourriersLiensArchiveTableMap::COL_MESSAGE_PARTICULIER, ComCourriersLiensArchiveTableMap::COL_STATUT, ComCourriersLiensArchiveTableMap::COL_VARIABLES, ComCourriersLiensArchiveTableMap::COL_CREATED_AT, ComCourriersLiensArchiveTableMap::COL_UPDATED_AT, ComCourriersLiensArchiveTableMap::COL_ARCHIVED_AT, ),
        self::TYPE_FIELDNAME     => array('id_courrier_lien', 'id_membre', 'id_individu', 'id_courrier', 'canal', 'date_envoi', 'message_particulier', 'statut', 'variables', 'created_at', 'updated_at', 'archived_at', ),
        self::TYPE_NUM           => array(0, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, )
    );

    /**
     * holds an array of keys for quick access to the fieldnames array
     *
     * first dimension keys are the type constants
     * e.g. self::$fieldKeys[self::TYPE_PHPNAME]['Id'] = 0
     */
    protected static $fieldKeys = array (
        self::TYPE_PHPNAME       => array('IdCourrierLien' => 0, 'IdMembre' => 1, 'IdIndividu' => 2, 'IdCourrier' => 3, 'Canal' => 4, 'DateEnvoi' => 5, 'MessageParticulier' => 6, 'Statut' => 7, 'Variables' => 8, 'CreatedAt' => 9, 'UpdatedAt' => 10, 'ArchivedAt' => 11, ),
        self::TYPE_CAMELNAME     => array('idCourrierLien' => 0, 'idMembre' => 1, 'idIndividu' => 2, 'idCourrier' => 3, 'canal' => 4, 'dateEnvoi' => 5, 'messageParticulier' => 6, 'statut' => 7, 'variables' => 8, 'createdAt' => 9, 'updatedAt' => 10, 'archivedAt' => 11, ),
        self::TYPE_COLNAME       => array(ComCourriersLiensArchiveTableMap::COL_ID_COURRIER_LIEN => 0, ComCourriersLiensArchiveTableMap::COL_ID_MEMBRE => 1, ComCourriersLiensArchiveTableMap::COL_ID_INDIVIDU => 2, ComCourriersLiensArchiveTableMap::COL_ID_COURRIER => 3, ComCourriersLiensArchiveTableMap::COL_CANAL => 4, ComCourriersLiensArchiveTableMap::COL_DATE_ENVOI => 5, ComCourriersLiensArchiveTableMap::COL_MESSAGE_PARTICULIER => 6, ComCourriersLiensArchiveTableMap::COL_STATUT => 7, ComCourriersLiensArchiveTableMap::COL_VARIABLES => 8, ComCourriersLiensArchiveTableMap::COL_CREATED_AT => 9, ComCourriersLiensArchiveTableMap::COL_UPDATED_AT => 10, ComCourriersLiensArchiveTableMap::COL_ARCHIVED_AT => 11, ),
        self::TYPE_FIELDNAME     => array('id_courrier_lien' => 0, 'id_membre' => 1, 'id_individu' => 2, 'id_courrier' => 3, 'canal' => 4, 'date_envoi' => 5, 'message_particulier' => 6, 'statut' => 7, 'variables' => 8, 'created_at' => 9, 'updated_at' => 10, 'archived_at' => 11, ),
        self::TYPE_NUM           => array(0, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, )
    );

    /**
     * Initialize the table attributes and columns
     * Relations are not initialized by this method since they are lazy loaded
     *
     * @return void
     * @throws PropelException
     */
    public function initialize()
    {
        // attributes
        $this->setName('com_courriers_liens_archive');
        $this->setPhpName('ComCourriersLiensArchive');
        $this->setIdentifierQuoting(true);
        $this->setClassName('\\ComCourriersLiensArchive');
        $this->setPackage('');
        $this->setUseIdGenerator(false);
        // columns
        $this->addPrimaryKey('id_courrier_lien', 'IdCourrierLien', 'BIGINT', true, 21, null);
        $this->addColumn('id_membre', 'IdMembre', 'BIGINT', false, 21, null);
        $this->addColumn('id_individu', 'IdIndividu', 'BIGINT', true, 21, null);
        $this->addColumn('id_courrier', 'IdCourrier', 'BIGINT', true, 21, null);
        $this->addColumn('canal', 'Canal', 'VARCHAR', true, 3, 'L');
        $this->addColumn('date_envoi', 'DateEnvoi', 'TIMESTAMP', false, null, null);
        $this->addColumn('message_particulier', 'MessageParticulier', 'LONGVARCHAR', false, null, null);
        $this->addColumn('statut', 'Statut', 'VARCHAR', true, 10, 'ok');
        $this->addColumn('variables', 'Variables', 'LONGVARCHAR', false, null, null);
        $this->addColumn('created_at', 'CreatedAt', 'TIMESTAMP', false, null, null);
        $this->addColumn('updated_at', 'UpdatedAt', 'TIMESTAMP', false, null, null);
        $this->addColumn('archived_at', 'ArchivedAt', 'TIMESTAMP', false, null, null);
    } // initialize()

    /**
     * Build the RelationMap objects for this table relationships
     */
    public function buildRelations()
    {
    } // buildRelations()

    /**
     * Retrieves a string version of the primary key from the DB resultset row that can be used to uniquely identify a row in this table.
     *
     * For tables with a single-column primary key, that simple pkey value will be returned.  For tables with
     * a multi-column primary key, a serialize()d version of the primary key will be returned.
     *
     * @param array  $row       resultset row.
     * @param int    $offset    The 0-based offset for reading from the resultset row.
     * @param string $indexType One of the class type constants TableMap::TYPE_PHPNAME, TableMap::TYPE_CAMELNAME
     *                           TableMap::TYPE_COLNAME, TableMap::TYPE_FIELDNAME, TableMap::TYPE_NUM
     *
     * @return string The primary key hash of the row
     */
    public static function getPrimaryKeyHashFromRow($row, $offset = 0, $indexType = TableMap::TYPE_NUM)
    {
        // If the PK cannot be derived from the row, return NULL.
        if ($row[TableMap::TYPE_NUM == $indexType ? 0 + $offset : static::translateFieldName('IdCourrierLien', TableMap::TYPE_PHPNAME, $indexType)] === null) {
            return null;
        }

        return null === $row[TableMap::TYPE_NUM == $indexType ? 0 + $offset : static::translateFieldName('IdCourrierLien', TableMap::TYPE_PHPNAME, $indexType)] || is_scalar($row[TableMap::TYPE_NUM == $indexType ? 0 + $offset : static::translateFieldName('IdCourrierLien', TableMap::TYPE_PHPNAME, $indexType)]) || is_callable([$row[TableMap::TYPE_NUM == $indexType ? 0 + $offset : static::translateFieldName('IdCourrierLien', TableMap::TYPE_PHPNAME, $indexType)], '__toString']) ? (string) $row[TableMap::TYPE_NUM == $indexType ? 0 + $offset : static::translateFieldName('IdCourrierLien', TableMap::TYPE_PHPNAME, $indexType)] : $row[TableMap::TYPE_NUM == $indexType ? 0 + $offset : static::translateFieldName('IdCourrierLien', TableMap::TYPE_PHPNAME, $indexType)];
    }

    /**
     * Retrieves the primary key from the DB resultset row
     * For tables with a single-column primary key, that simple pkey value will be returned.  For tables with
     * a multi-column primary key, an array of the primary key columns will be returned.
     *
     * @param array  $row       resultset row.
     * @param int    $offset    The 0-based offset for reading from the resultset row.
     * @param string $indexType One of the class type constants TableMap::TYPE_PHPNAME, TableMap::TYPE_CAMELNAME
     *                           TableMap::TYPE_COLNAME, TableMap::TYPE_FIELDNAME, TableMap::TYPE_NUM
     *
     * @return mixed The primary key of the row
     */
    public static function getPrimaryKeyFromRow($row, $offset = 0, $indexType = TableMap::TYPE_NUM)
    {
        return (string) $row[
            $indexType == TableMap::TYPE_NUM
                ? 0 + $offset
                : self::translateFieldName('IdCourrierLien', TableMap::TYPE_PHPNAME, $indexType)
        ];
    }

    /**
     * The class that the tableMap will make instances of.
     *
     * If $withPrefix is true, the returned path
     * uses a dot-path notation which is translated into a path
     * relative to a location on the PHP include_path.
     * (e.g. path.to.MyClass -> 'path/to/MyClass.php')
     *
     * @param boolean $withPrefix Whether or not to return the path with the class name
     * @return string path.to.ClassName
     */
    public static function getOMClass($withPrefix = true)
    {
        return $withPrefix ? ComCourriersLiensArchiveTableMap::CLASS_DEFAULT : ComCourriersLiensArchiveTableMap::OM_CLASS;
    }

    /**
     * Populates an object of the default type or an object that inherit from the default.
     *
     * @param array  $row       row returned by DataFetcher->fetch().
     * @param int    $offset    The 0-based offset for reading from the resultset row.
     * @param string $indexType The index type of $row. Mostly DataFetcher->getIndexType().
                                 One of the class type constants TableMap::TYPE_PHPNAME, TableMap::TYPE_CAMELNAME
     *                           TableMap::TYPE_COLNAME, TableMap::TYPE_FIELDNAME, TableMap::TYPE_NUM.
     *
     * @throws PropelException Any exceptions caught during processing will be
     *                         rethrown wrapped into a PropelException.
     * @return array           (ComCourriersLiensArchive object, last column rank)
     */
    public static function populateObject($row, $offset = 0, $indexType = TableMap::TYPE_NUM)
    {
        $key = ComCourriersLiensArchiveTableMap::getPrimaryKeyHashFromRow($row, $offset, $indexType);
        if (null !== ($obj = ComCourriersLiensArchiveTableMap::getInstanceFromPool($key))) {
            // We no longer rehydrate the object, since this can cause data loss.
            // See http://www.propelorm.org/ticket/509
            // $obj->hydrate($row, $offset, true); // rehydrate
            $col = $offset + ComCourriersLiensArchiveTableMap::NUM_HYDRATE_COLUMNS;
        } else {
            $cls = ComCourriersLiensArchiveTableMap::OM_CLASS;
            /** @var ComCourriersLiensArchive $obj */
            $obj = new $cls();
            $col = $obj->hydrate($row, $offset, false, $indexType);
            ComCourriersLiensArchiveTableMap::addInstanceToPool($obj, $key);
        }

        return array($obj, $col);
    }

    /**
     * The returned array will contain objects of the default type or
     * objects that inherit from the default.
     *
     * @param DataFetcherInterface $dataFetcher
     * @return array
     * @throws PropelException Any exceptions caught during processing will be
     *                         rethrown wrapped into a PropelException.
     */
    public static function populateObjects(DataFetcherInterface $dataFetcher)
    {
        $results = array();

        // set the class once to avoid overhead in the loop
        $cls = static::getOMClass(false);
        // populate the object(s)
        while ($row = $dataFetcher->fetch()) {
            $key = ComCourriersLiensArchiveTableMap::getPrimaryKeyHashFromRow($row, 0, $dataFetcher->getIndexType());
            if (null !== ($obj = ComCourriersLiensArchiveTableMap::getInstanceFromPool($key))) {
                // We no longer rehydrate the object, since this can cause data loss.
                // See http://www.propelorm.org/ticket/509
                // $obj->hydrate($row, 0, true); // rehydrate
                $results[] = $obj;
            } else {
                /** @var ComCourriersLiensArchive $obj */
                $obj = new $cls();
                $obj->hydrate($row);
                $results[] = $obj;
                ComCourriersLiensArchiveTableMap::addInstanceToPool($obj, $key);
            } // if key exists
        }

        return $results;
    }
    /**
     * Add all the columns needed to create a new object.
     *
     * Note: any columns that were marked with lazyLoad="true" in the
     * XML schema will not be added to the select list and only loaded
     * on demand.
     *
     * @param Criteria $criteria object containing the columns to add.
     * @param string   $alias    optional table alias
     * @throws PropelException Any exceptions caught during processing will be
     *                         rethrown wrapped into a PropelException.
     */
    public static function addSelectColumns(Criteria $criteria, $alias = null)
    {
        if (null === $alias) {
            $criteria->addSelectColumn(ComCourriersLiensArchiveTableMap::COL_ID_COURRIER_LIEN);
            $criteria->addSelectColumn(ComCourriersLiensArchiveTableMap::COL_ID_MEMBRE);
            $criteria->addSelectColumn(ComCourriersLiensArchiveTableMap::COL_ID_INDIVIDU);
            $criteria->addSelectColumn(ComCourriersLiensArchiveTableMap::COL_ID_COURRIER);
            $criteria->addSelectColumn(ComCourriersLiensArchiveTableMap::COL_CANAL);
            $criteria->addSelectColumn(ComCourriersLiensArchiveTableMap::COL_DATE_ENVOI);
            $criteria->addSelectColumn(ComCourriersLiensArchiveTableMap::COL_MESSAGE_PARTICULIER);
            $criteria->addSelectColumn(ComCourriersLiensArchiveTableMap::COL_STATUT);
            $criteria->addSelectColumn(ComCourriersLiensArchiveTableMap::COL_VARIABLES);
            $criteria->addSelectColumn(ComCourriersLiensArchiveTableMap::COL_CREATED_AT);
            $criteria->addSelectColumn(ComCourriersLiensArchiveTableMap::COL_UPDATED_AT);
            $criteria->addSelectColumn(ComCourriersLiensArchiveTableMap::COL_ARCHIVED_AT);
        } else {
            $criteria->addSelectColumn($alias . '.id_courrier_lien');
            $criteria->addSelectColumn($alias . '.id_membre');
            $criteria->addSelectColumn($alias . '.id_individu');
            $criteria->addSelectColumn($alias . '.id_courrier');
            $criteria->addSelectColumn($alias . '.canal');
            $criteria->addSelectColumn($alias . '.date_envoi');
            $criteria->addSelectColumn($alias . '.message_particulier');
            $criteria->addSelectColumn($alias . '.statut');
            $criteria->addSelectColumn($alias . '.variables');
            $criteria->addSelectColumn($alias . '.created_at');
            $criteria->addSelectColumn($alias . '.updated_at');
            $criteria->addSelectColumn($alias . '.archived_at');
        }
    }

    /**
     * Returns the TableMap related to this object.
     * This method is not needed for general use but a specific application could have a need.
     * @return TableMap
     * @throws PropelException Any exceptions caught during processing will be
     *                         rethrown wrapped into a PropelException.
     */
    public static function getTableMap()
    {
        return Propel::getServiceContainer()->getDatabaseMap(ComCourriersLiensArchiveTableMap::DATABASE_NAME)->getTable(ComCourriersLiensArchiveTableMap::TABLE_NAME);
    }

    /**
     * Add a TableMap instance to the database for this tableMap class.
     */
    public static function buildTableMap()
    {
        $dbMap = Propel::getServiceContainer()->getDatabaseMap(ComCourriersLiensArchiveTableMap::DATABASE_NAME);
        if (!$dbMap->hasTable(ComCourriersLiensArchiveTableMap::TABLE_NAME)) {
            $dbMap->addTableObject(new ComCourriersLiensArchiveTableMap());
        }
    }

    /**
     * Performs a DELETE on the database, given a ComCourriersLiensArchive or Criteria object OR a primary key value.
     *
     * @param mixed               $values Criteria or ComCourriersLiensArchive object or primary key or array of primary keys
     *              which is used to create the DELETE statement
     * @param  ConnectionInterface $con the connection to use
     * @return int             The number of affected rows (if supported by underlying database driver).  This includes CASCADE-related rows
     *                         if supported by native driver or if emulated using Propel.
     * @throws PropelException Any exceptions caught during processing will be
     *                         rethrown wrapped into a PropelException.
     */
     public static function doDelete($values, ConnectionInterface $con = null)
     {
        if (null === $con) {
            $con = Propel::getServiceContainer()->getWriteConnection(ComCourriersLiensArchiveTableMap::DATABASE_NAME);
        }

        if ($values instanceof Criteria) {
            // rename for clarity
            $criteria = $values;
        } elseif ($values instanceof \ComCourriersLiensArchive) { // it's a model object
            // create criteria based on pk values
            $criteria = $values->buildPkeyCriteria();
        } else { // it's a primary key, or an array of pks
            $criteria = new Criteria(ComCourriersLiensArchiveTableMap::DATABASE_NAME);
            $criteria->add(ComCourriersLiensArchiveTableMap::COL_ID_COURRIER_LIEN, (array) $values, Criteria::IN);
        }

        $query = ComCourriersLiensArchiveQuery::create()->mergeWith($criteria);

        if ($values instanceof Criteria) {
            ComCourriersLiensArchiveTableMap::clearInstancePool();
        } elseif (!is_object($values)) { // it's a primary key, or an array of pks
            foreach ((array) $values as $singleval) {
                ComCourriersLiensArchiveTableMap::removeInstanceFromPool($singleval);
            }
        }

        return $query->delete($con);
    }

    /**
     * Deletes all rows from the com_courriers_liens_archive table.
     *
     * @param ConnectionInterface $con the connection to use
     * @return int The number of affected rows (if supported by underlying database driver).
     */
    public static function doDeleteAll(ConnectionInterface $con = null)
    {
        return ComCourriersLiensArchiveQuery::create()->doDeleteAll($con);
    }

    /**
     * Performs an INSERT on the database, given a ComCourriersLiensArchive or Criteria object.
     *
     * @param mixed               $criteria Criteria or ComCourriersLiensArchive object containing data that is used to create the INSERT statement.
     * @param ConnectionInterface $con the ConnectionInterface connection to use
     * @return mixed           The new primary key.
     * @throws PropelException Any exceptions caught during processing will be
     *                         rethrown wrapped into a PropelException.
     */
    public static function doInsert($criteria, ConnectionInterface $con = null)
    {
        if (null === $con) {
            $con = Propel::getServiceContainer()->getWriteConnection(ComCourriersLiensArchiveTableMap::DATABASE_NAME);
        }

        if ($criteria instanceof Criteria) {
            $criteria = clone $criteria; // rename for clarity
        } else {
            $criteria = $criteria->buildCriteria(); // build Criteria from ComCourriersLiensArchive object
        }


        // Set the correct dbName
        $query = ComCourriersLiensArchiveQuery::create()->mergeWith($criteria);

        // use transaction because $criteria could contain info
        // for more than one table (I guess, conceivably)
        return $con->transaction(function () use ($con, $query) {
            return $query->doInsert($con);
        });
    }

} // ComCourriersLiensArchiveTableMap
// This is the static code needed to register the TableMap for this table with the main Propel class.
//
ComCourriersLiensArchiveTableMap::buildTableMap();
