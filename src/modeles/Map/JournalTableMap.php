<?php

namespace Map;

use \Journal;
use \JournalQuery;
use Propel\Runtime\Propel;
use Propel\Runtime\ActiveQuery\Criteria;
use Propel\Runtime\ActiveQuery\InstancePoolTrait;
use Propel\Runtime\Connection\ConnectionInterface;
use Propel\Runtime\DataFetcher\DataFetcherInterface;
use Propel\Runtime\Exception\PropelException;
use Propel\Runtime\Map\RelationMap;
use Propel\Runtime\Map\TableMap;
use Propel\Runtime\Map\TableMapTrait;


/**
 * This class defines the structure of the 'assc_journals' table.
 *
 *
 *
 * This map class is used by Propel to do runtime db structure discovery.
 * For example, the createSelectSql() method checks the type of a given column used in an
 * ORDER BY clause to know whether it needs to apply SQL to make the ORDER BY case-insensitive
 * (i.e. if it's a text column type).
 *
 */
class JournalTableMap extends TableMap
{
    use InstancePoolTrait;
    use TableMapTrait;

    /**
     * The (dot-path) name of this class
     */
    const CLASS_NAME = '.Map.JournalTableMap';

    /**
     * The default database name for this class
     */
    const DATABASE_NAME = 'simplasso';

    /**
     * The table name for this class
     */
    const TABLE_NAME = 'assc_journals';

    /**
     * The related Propel class for this table
     */
    const OM_CLASS = '\\Journal';

    /**
     * A class that can be returned by this tableMap
     */
    const CLASS_DEFAULT = 'Journal';

    /**
     * The total number of columns
     */
    const NUM_COLUMNS = 10;

    /**
     * The number of lazy-loaded columns
     */
    const NUM_LAZY_LOAD_COLUMNS = 0;

    /**
     * The number of columns to hydrate (NUM_COLUMNS - NUM_LAZY_LOAD_COLUMNS)
     */
    const NUM_HYDRATE_COLUMNS = 10;

    /**
     * the column name for the id_journal field
     */
    const COL_ID_JOURNAL = 'assc_journals.id_journal';

    /**
     * the column name for the nom field
     */
    const COL_NOM = 'assc_journals.nom';

    /**
     * the column name for the nomcourt field
     */
    const COL_NOMCOURT = 'assc_journals.nomcourt';

    /**
     * the column name for the id_compte field
     */
    const COL_ID_COMPTE = 'assc_journals.id_compte';

    /**
     * the column name for the actif field
     */
    const COL_ACTIF = 'assc_journals.actif';

    /**
     * the column name for the mouvement field
     */
    const COL_MOUVEMENT = 'assc_journals.mouvement';

    /**
     * the column name for the id_entite field
     */
    const COL_ID_ENTITE = 'assc_journals.id_entite';

    /**
     * the column name for the observation field
     */
    const COL_OBSERVATION = 'assc_journals.observation';

    /**
     * the column name for the created_at field
     */
    const COL_CREATED_AT = 'assc_journals.created_at';

    /**
     * the column name for the updated_at field
     */
    const COL_UPDATED_AT = 'assc_journals.updated_at';

    /**
     * The default string format for model objects of the related table
     */
    const DEFAULT_STRING_FORMAT = 'YAML';

    /**
     * holds an array of fieldnames
     *
     * first dimension keys are the type constants
     * e.g. self::$fieldNames[self::TYPE_PHPNAME][0] = 'Id'
     */
    protected static $fieldNames = array (
        self::TYPE_PHPNAME       => array('IdJournal', 'Nom', 'Nomcourt', 'IdCompte', 'Actif', 'Mouvement', 'IdEntite', 'Observation', 'CreatedAt', 'UpdatedAt', ),
        self::TYPE_CAMELNAME     => array('idJournal', 'nom', 'nomcourt', 'idCompte', 'actif', 'mouvement', 'idEntite', 'observation', 'createdAt', 'updatedAt', ),
        self::TYPE_COLNAME       => array(JournalTableMap::COL_ID_JOURNAL, JournalTableMap::COL_NOM, JournalTableMap::COL_NOMCOURT, JournalTableMap::COL_ID_COMPTE, JournalTableMap::COL_ACTIF, JournalTableMap::COL_MOUVEMENT, JournalTableMap::COL_ID_ENTITE, JournalTableMap::COL_OBSERVATION, JournalTableMap::COL_CREATED_AT, JournalTableMap::COL_UPDATED_AT, ),
        self::TYPE_FIELDNAME     => array('id_journal', 'nom', 'nomcourt', 'id_compte', 'actif', 'mouvement', 'id_entite', 'observation', 'created_at', 'updated_at', ),
        self::TYPE_NUM           => array(0, 1, 2, 3, 4, 5, 6, 7, 8, 9, )
    );

    /**
     * holds an array of keys for quick access to the fieldnames array
     *
     * first dimension keys are the type constants
     * e.g. self::$fieldKeys[self::TYPE_PHPNAME]['Id'] = 0
     */
    protected static $fieldKeys = array (
        self::TYPE_PHPNAME       => array('IdJournal' => 0, 'Nom' => 1, 'Nomcourt' => 2, 'IdCompte' => 3, 'Actif' => 4, 'Mouvement' => 5, 'IdEntite' => 6, 'Observation' => 7, 'CreatedAt' => 8, 'UpdatedAt' => 9, ),
        self::TYPE_CAMELNAME     => array('idJournal' => 0, 'nom' => 1, 'nomcourt' => 2, 'idCompte' => 3, 'actif' => 4, 'mouvement' => 5, 'idEntite' => 6, 'observation' => 7, 'createdAt' => 8, 'updatedAt' => 9, ),
        self::TYPE_COLNAME       => array(JournalTableMap::COL_ID_JOURNAL => 0, JournalTableMap::COL_NOM => 1, JournalTableMap::COL_NOMCOURT => 2, JournalTableMap::COL_ID_COMPTE => 3, JournalTableMap::COL_ACTIF => 4, JournalTableMap::COL_MOUVEMENT => 5, JournalTableMap::COL_ID_ENTITE => 6, JournalTableMap::COL_OBSERVATION => 7, JournalTableMap::COL_CREATED_AT => 8, JournalTableMap::COL_UPDATED_AT => 9, ),
        self::TYPE_FIELDNAME     => array('id_journal' => 0, 'nom' => 1, 'nomcourt' => 2, 'id_compte' => 3, 'actif' => 4, 'mouvement' => 5, 'id_entite' => 6, 'observation' => 7, 'created_at' => 8, 'updated_at' => 9, ),
        self::TYPE_NUM           => array(0, 1, 2, 3, 4, 5, 6, 7, 8, 9, )
    );

    /**
     * Initialize the table attributes and columns
     * Relations are not initialized by this method since they are lazy loaded
     *
     * @return void
     * @throws PropelException
     */
    public function initialize()
    {
        // attributes
        $this->setName('assc_journals');
        $this->setPhpName('Journal');
        $this->setIdentifierQuoting(true);
        $this->setClassName('\\Journal');
        $this->setPackage('');
        $this->setUseIdGenerator(true);
        // columns
        $this->addPrimaryKey('id_journal', 'IdJournal', 'BIGINT', true, 21, null);
        $this->addColumn('nom', 'Nom', 'VARCHAR', true, 25, 'Journal CC NEF ');
        $this->addColumn('nomcourt', 'Nomcourt', 'VARCHAR', true, 6, 'CC NEF');
        $this->addForeignKey('id_compte', 'IdCompte', 'BIGINT', 'assc_comptes', 'id_compte', true, 21, 1);
        $this->addColumn('actif', 'Actif', 'INTEGER', true, 1, 1);
        $this->addColumn('mouvement', 'Mouvement', 'INTEGER', true, 1, 3);
        $this->addForeignKey('id_entite', 'IdEntite', 'BIGINT', 'asso_entites', 'id_entite', true, 21, 1);
        $this->addColumn('observation', 'Observation', 'LONGVARCHAR', false, null, null);
        $this->addColumn('created_at', 'CreatedAt', 'TIMESTAMP', false, null, null);
        $this->addColumn('updated_at', 'UpdatedAt', 'TIMESTAMP', false, null, null);
    } // initialize()

    /**
     * Build the RelationMap objects for this table relationships
     */
    public function buildRelations()
    {
        $this->addRelation('Entite', '\\Entite', RelationMap::MANY_TO_ONE, array (
  0 =>
  array (
    0 => ':id_entite',
    1 => ':id_entite',
  ),
), 'CASCADE', null, null, false);
        $this->addRelation('Compte', '\\Compte', RelationMap::MANY_TO_ONE, array (
  0 =>
  array (
    0 => ':id_compte',
    1 => ':id_compte',
  ),
), 'CASCADE', null, null, false);
        $this->addRelation('Ecriture', '\\Ecriture', RelationMap::ONE_TO_MANY, array (
  0 =>
  array (
    0 => ':id_journal',
    1 => ':id_journal',
  ),
), 'CASCADE', null, 'Ecritures', false);
        $this->addRelation('Piece', '\\Piece', RelationMap::ONE_TO_MANY, array (
  0 =>
  array (
    0 => ':id_journal',
    1 => ':id_journal',
  ),
), 'CASCADE', null, 'Pieces', false);
    } // buildRelations()

    /**
     *
     * Gets the list of behaviors registered for this table
     *
     * @return array Associative array (name => parameters) of behaviors
     */
    public function getBehaviors()
    {
        return array(
            'timestampable' => array('create_column' => 'created_at', 'update_column' => 'updated_at', 'disable_created_at' => 'false', 'disable_updated_at' => 'false', ),
            'archivable' => array('archive_table' => '', 'archive_phpname' => '', 'archive_class' => '', 'log_archived_at' => 'true', 'archived_at_column' => 'archived_at', 'archive_on_insert' => 'false', 'archive_on_update' => 'false', 'archive_on_delete' => 'true', ),
        );
    } // getBehaviors()
    /**
     * Method to invalidate the instance pool of all tables related to assc_journals     * by a foreign key with ON DELETE CASCADE
     */
    public static function clearRelatedInstancePool()
    {
        // Invalidate objects in related instance pools,
        // since one or more of them may be deleted by ON DELETE CASCADE/SETNULL rule.
        EcritureTableMap::clearInstancePool();
        PieceTableMap::clearInstancePool();
    }

    /**
     * Retrieves a string version of the primary key from the DB resultset row that can be used to uniquely identify a row in this table.
     *
     * For tables with a single-column primary key, that simple pkey value will be returned.  For tables with
     * a multi-column primary key, a serialize()d version of the primary key will be returned.
     *
     * @param array  $row       resultset row.
     * @param int    $offset    The 0-based offset for reading from the resultset row.
     * @param string $indexType One of the class type constants TableMap::TYPE_PHPNAME, TableMap::TYPE_CAMELNAME
     *                           TableMap::TYPE_COLNAME, TableMap::TYPE_FIELDNAME, TableMap::TYPE_NUM
     *
     * @return string The primary key hash of the row
     */
    public static function getPrimaryKeyHashFromRow($row, $offset = 0, $indexType = TableMap::TYPE_NUM)
    {
        // If the PK cannot be derived from the row, return NULL.
        if ($row[TableMap::TYPE_NUM == $indexType ? 0 + $offset : static::translateFieldName('IdJournal', TableMap::TYPE_PHPNAME, $indexType)] === null) {
            return null;
        }

        return null === $row[TableMap::TYPE_NUM == $indexType ? 0 + $offset : static::translateFieldName('IdJournal', TableMap::TYPE_PHPNAME, $indexType)] || is_scalar($row[TableMap::TYPE_NUM == $indexType ? 0 + $offset : static::translateFieldName('IdJournal', TableMap::TYPE_PHPNAME, $indexType)]) || is_callable([$row[TableMap::TYPE_NUM == $indexType ? 0 + $offset : static::translateFieldName('IdJournal', TableMap::TYPE_PHPNAME, $indexType)], '__toString']) ? (string) $row[TableMap::TYPE_NUM == $indexType ? 0 + $offset : static::translateFieldName('IdJournal', TableMap::TYPE_PHPNAME, $indexType)] : $row[TableMap::TYPE_NUM == $indexType ? 0 + $offset : static::translateFieldName('IdJournal', TableMap::TYPE_PHPNAME, $indexType)];
    }

    /**
     * Retrieves the primary key from the DB resultset row
     * For tables with a single-column primary key, that simple pkey value will be returned.  For tables with
     * a multi-column primary key, an array of the primary key columns will be returned.
     *
     * @param array  $row       resultset row.
     * @param int    $offset    The 0-based offset for reading from the resultset row.
     * @param string $indexType One of the class type constants TableMap::TYPE_PHPNAME, TableMap::TYPE_CAMELNAME
     *                           TableMap::TYPE_COLNAME, TableMap::TYPE_FIELDNAME, TableMap::TYPE_NUM
     *
     * @return mixed The primary key of the row
     */
    public static function getPrimaryKeyFromRow($row, $offset = 0, $indexType = TableMap::TYPE_NUM)
    {
        return (string) $row[
            $indexType == TableMap::TYPE_NUM
                ? 0 + $offset
                : self::translateFieldName('IdJournal', TableMap::TYPE_PHPNAME, $indexType)
        ];
    }

    /**
     * The class that the tableMap will make instances of.
     *
     * If $withPrefix is true, the returned path
     * uses a dot-path notation which is translated into a path
     * relative to a location on the PHP include_path.
     * (e.g. path.to.MyClass -> 'path/to/MyClass.php')
     *
     * @param boolean $withPrefix Whether or not to return the path with the class name
     * @return string path.to.ClassName
     */
    public static function getOMClass($withPrefix = true)
    {
        return $withPrefix ? JournalTableMap::CLASS_DEFAULT : JournalTableMap::OM_CLASS;
    }

    /**
     * Populates an object of the default type or an object that inherit from the default.
     *
     * @param array  $row       row returned by DataFetcher->fetch().
     * @param int    $offset    The 0-based offset for reading from the resultset row.
     * @param string $indexType The index type of $row. Mostly DataFetcher->getIndexType().
                                 One of the class type constants TableMap::TYPE_PHPNAME, TableMap::TYPE_CAMELNAME
     *                           TableMap::TYPE_COLNAME, TableMap::TYPE_FIELDNAME, TableMap::TYPE_NUM.
     *
     * @throws PropelException Any exceptions caught during processing will be
     *                         rethrown wrapped into a PropelException.
     * @return array           (Journal object, last column rank)
     */
    public static function populateObject($row, $offset = 0, $indexType = TableMap::TYPE_NUM)
    {
        $key = JournalTableMap::getPrimaryKeyHashFromRow($row, $offset, $indexType);
        if (null !== ($obj = JournalTableMap::getInstanceFromPool($key))) {
            // We no longer rehydrate the object, since this can cause data loss.
            // See http://www.propelorm.org/ticket/509
            // $obj->hydrate($row, $offset, true); // rehydrate
            $col = $offset + JournalTableMap::NUM_HYDRATE_COLUMNS;
        } else {
            $cls = JournalTableMap::OM_CLASS;
            /** @var Journal $obj */
            $obj = new $cls();
            $col = $obj->hydrate($row, $offset, false, $indexType);
            JournalTableMap::addInstanceToPool($obj, $key);
        }

        return array($obj, $col);
    }

    /**
     * The returned array will contain objects of the default type or
     * objects that inherit from the default.
     *
     * @param DataFetcherInterface $dataFetcher
     * @return array
     * @throws PropelException Any exceptions caught during processing will be
     *                         rethrown wrapped into a PropelException.
     */
    public static function populateObjects(DataFetcherInterface $dataFetcher)
    {
        $results = array();

        // set the class once to avoid overhead in the loop
        $cls = static::getOMClass(false);
        // populate the object(s)
        while ($row = $dataFetcher->fetch()) {
            $key = JournalTableMap::getPrimaryKeyHashFromRow($row, 0, $dataFetcher->getIndexType());
            if (null !== ($obj = JournalTableMap::getInstanceFromPool($key))) {
                // We no longer rehydrate the object, since this can cause data loss.
                // See http://www.propelorm.org/ticket/509
                // $obj->hydrate($row, 0, true); // rehydrate
                $results[] = $obj;
            } else {
                /** @var Journal $obj */
                $obj = new $cls();
                $obj->hydrate($row);
                $results[] = $obj;
                JournalTableMap::addInstanceToPool($obj, $key);
            } // if key exists
        }

        return $results;
    }
    /**
     * Add all the columns needed to create a new object.
     *
     * Note: any columns that were marked with lazyLoad="true" in the
     * XML schema will not be added to the select list and only loaded
     * on demand.
     *
     * @param Criteria $criteria object containing the columns to add.
     * @param string   $alias    optional table alias
     * @throws PropelException Any exceptions caught during processing will be
     *                         rethrown wrapped into a PropelException.
     */
    public static function addSelectColumns(Criteria $criteria, $alias = null)
    {
        if (null === $alias) {
            $criteria->addSelectColumn(JournalTableMap::COL_ID_JOURNAL);
            $criteria->addSelectColumn(JournalTableMap::COL_NOM);
            $criteria->addSelectColumn(JournalTableMap::COL_NOMCOURT);
            $criteria->addSelectColumn(JournalTableMap::COL_ID_COMPTE);
            $criteria->addSelectColumn(JournalTableMap::COL_ACTIF);
            $criteria->addSelectColumn(JournalTableMap::COL_MOUVEMENT);
            $criteria->addSelectColumn(JournalTableMap::COL_ID_ENTITE);
            $criteria->addSelectColumn(JournalTableMap::COL_OBSERVATION);
            $criteria->addSelectColumn(JournalTableMap::COL_CREATED_AT);
            $criteria->addSelectColumn(JournalTableMap::COL_UPDATED_AT);
        } else {
            $criteria->addSelectColumn($alias . '.id_journal');
            $criteria->addSelectColumn($alias . '.nom');
            $criteria->addSelectColumn($alias . '.nomcourt');
            $criteria->addSelectColumn($alias . '.id_compte');
            $criteria->addSelectColumn($alias . '.actif');
            $criteria->addSelectColumn($alias . '.mouvement');
            $criteria->addSelectColumn($alias . '.id_entite');
            $criteria->addSelectColumn($alias . '.observation');
            $criteria->addSelectColumn($alias . '.created_at');
            $criteria->addSelectColumn($alias . '.updated_at');
        }
    }

    /**
     * Returns the TableMap related to this object.
     * This method is not needed for general use but a specific application could have a need.
     * @return TableMap
     * @throws PropelException Any exceptions caught during processing will be
     *                         rethrown wrapped into a PropelException.
     */
    public static function getTableMap()
    {
        return Propel::getServiceContainer()->getDatabaseMap(JournalTableMap::DATABASE_NAME)->getTable(JournalTableMap::TABLE_NAME);
    }

    /**
     * Add a TableMap instance to the database for this tableMap class.
     */
    public static function buildTableMap()
    {
        $dbMap = Propel::getServiceContainer()->getDatabaseMap(JournalTableMap::DATABASE_NAME);
        if (!$dbMap->hasTable(JournalTableMap::TABLE_NAME)) {
            $dbMap->addTableObject(new JournalTableMap());
        }
    }

    /**
     * Performs a DELETE on the database, given a Journal or Criteria object OR a primary key value.
     *
     * @param mixed               $values Criteria or Journal object or primary key or array of primary keys
     *              which is used to create the DELETE statement
     * @param  ConnectionInterface $con the connection to use
     * @return int             The number of affected rows (if supported by underlying database driver).  This includes CASCADE-related rows
     *                         if supported by native driver or if emulated using Propel.
     * @throws PropelException Any exceptions caught during processing will be
     *                         rethrown wrapped into a PropelException.
     */
     public static function doDelete($values, ConnectionInterface $con = null)
     {
        if (null === $con) {
            $con = Propel::getServiceContainer()->getWriteConnection(JournalTableMap::DATABASE_NAME);
        }

        if ($values instanceof Criteria) {
            // rename for clarity
            $criteria = $values;
        } elseif ($values instanceof \Journal) { // it's a model object
            // create criteria based on pk values
            $criteria = $values->buildPkeyCriteria();
        } else { // it's a primary key, or an array of pks
            $criteria = new Criteria(JournalTableMap::DATABASE_NAME);
            $criteria->add(JournalTableMap::COL_ID_JOURNAL, (array) $values, Criteria::IN);
        }

        $query = JournalQuery::create()->mergeWith($criteria);

        if ($values instanceof Criteria) {
            JournalTableMap::clearInstancePool();
        } elseif (!is_object($values)) { // it's a primary key, or an array of pks
            foreach ((array) $values as $singleval) {
                JournalTableMap::removeInstanceFromPool($singleval);
            }
        }

        return $query->delete($con);
    }

    /**
     * Deletes all rows from the assc_journals table.
     *
     * @param ConnectionInterface $con the connection to use
     * @return int The number of affected rows (if supported by underlying database driver).
     */
    public static function doDeleteAll(ConnectionInterface $con = null)
    {
        return JournalQuery::create()->doDeleteAll($con);
    }

    /**
     * Performs an INSERT on the database, given a Journal or Criteria object.
     *
     * @param mixed               $criteria Criteria or Journal object containing data that is used to create the INSERT statement.
     * @param ConnectionInterface $con the ConnectionInterface connection to use
     * @return mixed           The new primary key.
     * @throws PropelException Any exceptions caught during processing will be
     *                         rethrown wrapped into a PropelException.
     */
    public static function doInsert($criteria, ConnectionInterface $con = null)
    {
        if (null === $con) {
            $con = Propel::getServiceContainer()->getWriteConnection(JournalTableMap::DATABASE_NAME);
        }

        if ($criteria instanceof Criteria) {
            $criteria = clone $criteria; // rename for clarity
        } else {
            $criteria = $criteria->buildCriteria(); // build Criteria from Journal object
        }

        if ($criteria->containsKey(JournalTableMap::COL_ID_JOURNAL) && $criteria->keyContainsValue(JournalTableMap::COL_ID_JOURNAL) ) {
            throw new PropelException('Cannot insert a value for auto-increment primary key ('.JournalTableMap::COL_ID_JOURNAL.')');
        }


        // Set the correct dbName
        $query = JournalQuery::create()->mergeWith($criteria);

        // use transaction because $criteria could contain info
        // for more than one table (I guess, conceivably)
        return $con->transaction(function () use ($con, $query) {
            return $query->doInsert($con);
        });
    }

} // JournalTableMap
// This is the static code needed to register the TableMap for this table with the main Propel class.
//
JournalTableMap::buildTableMap();
