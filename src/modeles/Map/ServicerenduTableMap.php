<?php

namespace Map;

use \Servicerendu;
use \ServicerenduQuery;
use Propel\Runtime\Propel;
use Propel\Runtime\ActiveQuery\Criteria;
use Propel\Runtime\ActiveQuery\InstancePoolTrait;
use Propel\Runtime\Connection\ConnectionInterface;
use Propel\Runtime\DataFetcher\DataFetcherInterface;
use Propel\Runtime\Exception\PropelException;
use Propel\Runtime\Map\RelationMap;
use Propel\Runtime\Map\TableMap;
use Propel\Runtime\Map\TableMapTrait;


/**
 * This class defines the structure of the 'asso_servicerendus' table.
 *
 *
 *
 * This map class is used by Propel to do runtime db structure discovery.
 * For example, the createSelectSql() method checks the type of a given column used in an
 * ORDER BY clause to know whether it needs to apply SQL to make the ORDER BY case-insensitive
 * (i.e. if it's a text column type).
 *
 */
class ServicerenduTableMap extends TableMap
{
    use InstancePoolTrait;
    use TableMapTrait;

    /**
     * The (dot-path) name of this class
     */
    const CLASS_NAME = '.Map.ServicerenduTableMap';

    /**
     * The default database name for this class
     */
    const DATABASE_NAME = 'simplasso';

    /**
     * The table name for this class
     */
    const TABLE_NAME = 'asso_servicerendus';

    /**
     * The related Propel class for this table
     */
    const OM_CLASS = '\\Servicerendu';

    /**
     * A class that can be returned by this tableMap
     */
    const CLASS_DEFAULT = 'Servicerendu';

    /**
     * The total number of columns
     */
    const NUM_COLUMNS = 18;

    /**
     * The number of lazy-loaded columns
     */
    const NUM_LAZY_LOAD_COLUMNS = 0;

    /**
     * The number of columns to hydrate (NUM_COLUMNS - NUM_LAZY_LOAD_COLUMNS)
     */
    const NUM_HYDRATE_COLUMNS = 18;

    /**
     * the column name for the id_servicerendu field
     */
    const COL_ID_SERVICERENDU = 'asso_servicerendus.id_servicerendu';

    /**
     * the column name for the id_membre field
     */
    const COL_ID_MEMBRE = 'asso_servicerendus.id_membre';

    /**
     * the column name for the montant field
     */
    const COL_MONTANT = 'asso_servicerendus.montant';

    /**
     * the column name for the date_enregistrement field
     */
    const COL_DATE_ENREGISTREMENT = 'asso_servicerendus.date_enregistrement';

    /**
     * the column name for the date_debut field
     */
    const COL_DATE_DEBUT = 'asso_servicerendus.date_debut';

    /**
     * the column name for the date_fin field
     */
    const COL_DATE_FIN = 'asso_servicerendus.date_fin';

    /**
     * the column name for the origine field
     */
    const COL_ORIGINE = 'asso_servicerendus.origine';

    /**
     * the column name for the premier field
     */
    const COL_PREMIER = 'asso_servicerendus.premier';

    /**
     * the column name for the dernier field
     */
    const COL_DERNIER = 'asso_servicerendus.dernier';

    /**
     * the column name for the id_entite field
     */
    const COL_ID_ENTITE = 'asso_servicerendus.id_entite';

    /**
     * the column name for the id_prestation field
     */
    const COL_ID_PRESTATION = 'asso_servicerendus.id_prestation';

    /**
     * the column name for the regle field
     */
    const COL_REGLE = 'asso_servicerendus.regle';

    /**
     * the column name for the comptabilise field
     */
    const COL_COMPTABILISE = 'asso_servicerendus.comptabilise';

    /**
     * the column name for the observation field
     */
    const COL_OBSERVATION = 'asso_servicerendus.observation';

    /**
     * the column name for the id_tva field
     */
    const COL_ID_TVA = 'asso_servicerendus.id_tva';

    /**
     * the column name for the quantite field
     */
    const COL_QUANTITE = 'asso_servicerendus.quantite';

    /**
     * the column name for the created_at field
     */
    const COL_CREATED_AT = 'asso_servicerendus.created_at';

    /**
     * the column name for the updated_at field
     */
    const COL_UPDATED_AT = 'asso_servicerendus.updated_at';

    /**
     * The default string format for model objects of the related table
     */
    const DEFAULT_STRING_FORMAT = 'YAML';

    /**
     * holds an array of fieldnames
     *
     * first dimension keys are the type constants
     * e.g. self::$fieldNames[self::TYPE_PHPNAME][0] = 'Id'
     */
    protected static $fieldNames = array (
        self::TYPE_PHPNAME       => array('IdServicerendu', 'IdMembre', 'Montant', 'DateEnregistrement', 'DateDebut', 'DateFin', 'Origine', 'Premier', 'Dernier', 'IdEntite', 'IdPrestation', 'Regle', 'Comptabilise', 'Observation', 'IdTva', 'Quantite', 'CreatedAt', 'UpdatedAt', ),
        self::TYPE_CAMELNAME     => array('idServicerendu', 'idMembre', 'montant', 'dateEnregistrement', 'dateDebut', 'dateFin', 'origine', 'premier', 'dernier', 'idEntite', 'idPrestation', 'regle', 'comptabilise', 'observation', 'idTva', 'quantite', 'createdAt', 'updatedAt', ),
        self::TYPE_COLNAME       => array(ServicerenduTableMap::COL_ID_SERVICERENDU, ServicerenduTableMap::COL_ID_MEMBRE, ServicerenduTableMap::COL_MONTANT, ServicerenduTableMap::COL_DATE_ENREGISTREMENT, ServicerenduTableMap::COL_DATE_DEBUT, ServicerenduTableMap::COL_DATE_FIN, ServicerenduTableMap::COL_ORIGINE, ServicerenduTableMap::COL_PREMIER, ServicerenduTableMap::COL_DERNIER, ServicerenduTableMap::COL_ID_ENTITE, ServicerenduTableMap::COL_ID_PRESTATION, ServicerenduTableMap::COL_REGLE, ServicerenduTableMap::COL_COMPTABILISE, ServicerenduTableMap::COL_OBSERVATION, ServicerenduTableMap::COL_ID_TVA, ServicerenduTableMap::COL_QUANTITE, ServicerenduTableMap::COL_CREATED_AT, ServicerenduTableMap::COL_UPDATED_AT, ),
        self::TYPE_FIELDNAME     => array('id_servicerendu', 'id_membre', 'montant', 'date_enregistrement', 'date_debut', 'date_fin', 'origine', 'premier', 'dernier', 'id_entite', 'id_prestation', 'regle', 'comptabilise', 'observation', 'id_tva', 'quantite', 'created_at', 'updated_at', ),
        self::TYPE_NUM           => array(0, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15, 16, 17, )
    );

    /**
     * holds an array of keys for quick access to the fieldnames array
     *
     * first dimension keys are the type constants
     * e.g. self::$fieldKeys[self::TYPE_PHPNAME]['Id'] = 0
     */
    protected static $fieldKeys = array (
        self::TYPE_PHPNAME       => array('IdServicerendu' => 0, 'IdMembre' => 1, 'Montant' => 2, 'DateEnregistrement' => 3, 'DateDebut' => 4, 'DateFin' => 5, 'Origine' => 6, 'Premier' => 7, 'Dernier' => 8, 'IdEntite' => 9, 'IdPrestation' => 10, 'Regle' => 11, 'Comptabilise' => 12, 'Observation' => 13, 'IdTva' => 14, 'Quantite' => 15, 'CreatedAt' => 16, 'UpdatedAt' => 17, ),
        self::TYPE_CAMELNAME     => array('idServicerendu' => 0, 'idMembre' => 1, 'montant' => 2, 'dateEnregistrement' => 3, 'dateDebut' => 4, 'dateFin' => 5, 'origine' => 6, 'premier' => 7, 'dernier' => 8, 'idEntite' => 9, 'idPrestation' => 10, 'regle' => 11, 'comptabilise' => 12, 'observation' => 13, 'idTva' => 14, 'quantite' => 15, 'createdAt' => 16, 'updatedAt' => 17, ),
        self::TYPE_COLNAME       => array(ServicerenduTableMap::COL_ID_SERVICERENDU => 0, ServicerenduTableMap::COL_ID_MEMBRE => 1, ServicerenduTableMap::COL_MONTANT => 2, ServicerenduTableMap::COL_DATE_ENREGISTREMENT => 3, ServicerenduTableMap::COL_DATE_DEBUT => 4, ServicerenduTableMap::COL_DATE_FIN => 5, ServicerenduTableMap::COL_ORIGINE => 6, ServicerenduTableMap::COL_PREMIER => 7, ServicerenduTableMap::COL_DERNIER => 8, ServicerenduTableMap::COL_ID_ENTITE => 9, ServicerenduTableMap::COL_ID_PRESTATION => 10, ServicerenduTableMap::COL_REGLE => 11, ServicerenduTableMap::COL_COMPTABILISE => 12, ServicerenduTableMap::COL_OBSERVATION => 13, ServicerenduTableMap::COL_ID_TVA => 14, ServicerenduTableMap::COL_QUANTITE => 15, ServicerenduTableMap::COL_CREATED_AT => 16, ServicerenduTableMap::COL_UPDATED_AT => 17, ),
        self::TYPE_FIELDNAME     => array('id_servicerendu' => 0, 'id_membre' => 1, 'montant' => 2, 'date_enregistrement' => 3, 'date_debut' => 4, 'date_fin' => 5, 'origine' => 6, 'premier' => 7, 'dernier' => 8, 'id_entite' => 9, 'id_prestation' => 10, 'regle' => 11, 'comptabilise' => 12, 'observation' => 13, 'id_tva' => 14, 'quantite' => 15, 'created_at' => 16, 'updated_at' => 17, ),
        self::TYPE_NUM           => array(0, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15, 16, 17, )
    );

    /**
     * Initialize the table attributes and columns
     * Relations are not initialized by this method since they are lazy loaded
     *
     * @return void
     * @throws PropelException
     */
    public function initialize()
    {
        // attributes
        $this->setName('asso_servicerendus');
        $this->setPhpName('Servicerendu');
        $this->setIdentifierQuoting(true);
        $this->setClassName('\\Servicerendu');
        $this->setPackage('');
        $this->setUseIdGenerator(true);
        // columns
        $this->addPrimaryKey('id_servicerendu', 'IdServicerendu', 'BIGINT', true, null, null);
        $this->addForeignKey('id_membre', 'IdMembre', 'BIGINT', 'asso_membres', 'id_membre', false, 21, null);
        $this->addColumn('montant', 'Montant', 'DOUBLE', true, 12, null);
        $this->addColumn('date_enregistrement', 'DateEnregistrement', 'DATE', false, null, null);
        $this->addColumn('date_debut', 'DateDebut', 'DATE', true, null, '1980-01-01');
        $this->addColumn('date_fin', 'DateFin', 'DATE', true, null, '3000-12-31');
        $this->addForeignKey('origine', 'Origine', 'INTEGER', 'asso_servicerendus', 'id_servicerendu', false, null, null);
        $this->addColumn('premier', 'Premier', 'INTEGER', false, null, 0);
        $this->addColumn('dernier', 'Dernier', 'INTEGER', false, null, 0);
        $this->addForeignKey('id_entite', 'IdEntite', 'INTEGER', 'asso_entites', 'id_entite', true, 21, 1);
        $this->addForeignKey('id_prestation', 'IdPrestation', 'BIGINT', 'asso_prestations', 'id_prestation', true, 21, null);
        $this->addColumn('regle', 'Regle', 'INTEGER', true, null, 1);
        $this->addColumn('comptabilise', 'Comptabilise', 'BOOLEAN', true, 1, false);
        $this->addColumn('observation', 'Observation', 'LONGVARCHAR', false, null, null);
        $this->addColumn('id_tva', 'IdTva', 'BIGINT', true, 21, 0);
        $this->addColumn('quantite', 'Quantite', 'DOUBLE', true, null, 1);
        $this->addColumn('created_at', 'CreatedAt', 'TIMESTAMP', false, null, null);
        $this->addColumn('updated_at', 'UpdatedAt', 'TIMESTAMP', false, null, null);
    } // initialize()

    /**
     * Build the RelationMap objects for this table relationships
     */
    public function buildRelations()
    {
        $this->addRelation('Prestation', '\\Prestation', RelationMap::MANY_TO_ONE, array (
  0 =>
  array (
    0 => ':id_prestation',
    1 => ':id_prestation',
  ),
), 'CASCADE', null, null, false);
        $this->addRelation('Entite', '\\Entite', RelationMap::MANY_TO_ONE, array (
  0 =>
  array (
    0 => ':id_entite',
    1 => ':id_entite',
  ),
), 'CASCADE', null, null, false);
        $this->addRelation('Membre', '\\Membre', RelationMap::MANY_TO_ONE, array (
  0 =>
  array (
    0 => ':id_membre',
    1 => ':id_membre',
  ),
), 'CASCADE', null, null, false);
        $this->addRelation('ServicerenduRelatedByOrigine', '\\Servicerendu', RelationMap::MANY_TO_ONE, array (
  0 =>
  array (
    0 => ':origine',
    1 => ':id_servicerendu',
  ),
), 'CASCADE', null, null, false);
        $this->addRelation('Servicepaiement', '\\Servicepaiement', RelationMap::ONE_TO_MANY, array (
  0 =>
  array (
    0 => ':id_servicerendu',
    1 => ':id_servicerendu',
  ),
), 'CASCADE', null, 'Servicepaiements', false);
        $this->addRelation('ServicerenduRelatedByIdServicerendu', '\\Servicerendu', RelationMap::ONE_TO_MANY, array (
  0 =>
  array (
    0 => ':origine',
    1 => ':id_servicerendu',
  ),
), 'CASCADE', null, 'ServicerendusRelatedByIdServicerendu', false);
        $this->addRelation('ServicerenduIndividu', '\\ServicerenduIndividu', RelationMap::ONE_TO_MANY, array (
  0 =>
  array (
    0 => ':id_servicerendu',
    1 => ':id_servicerendu',
  ),
), 'CASCADE', null, 'ServicerenduIndividus', false);
        $this->addRelation('Individu', '\\Individu', RelationMap::MANY_TO_MANY, array(), 'CASCADE', null, 'Individus');
    } // buildRelations()

    /**
     *
     * Gets the list of behaviors registered for this table
     *
     * @return array Associative array (name => parameters) of behaviors
     */
    public function getBehaviors()
    {
        return array(
            'timestampable' => array('create_column' => 'created_at', 'update_column' => 'updated_at', 'disable_created_at' => 'false', 'disable_updated_at' => 'false', ),
            'archivable' => array('archive_table' => '', 'archive_phpname' => '', 'archive_class' => '', 'log_archived_at' => 'true', 'archived_at_column' => 'archived_at', 'archive_on_insert' => 'false', 'archive_on_update' => 'false', 'archive_on_delete' => 'true', ),
        );
    } // getBehaviors()
    /**
     * Method to invalidate the instance pool of all tables related to asso_servicerendus     * by a foreign key with ON DELETE CASCADE
     */
    public static function clearRelatedInstancePool()
    {
        // Invalidate objects in related instance pools,
        // since one or more of them may be deleted by ON DELETE CASCADE/SETNULL rule.
        ServicepaiementTableMap::clearInstancePool();
        ServicerenduTableMap::clearInstancePool();
        ServicerenduIndividuTableMap::clearInstancePool();
    }

    /**
     * Retrieves a string version of the primary key from the DB resultset row that can be used to uniquely identify a row in this table.
     *
     * For tables with a single-column primary key, that simple pkey value will be returned.  For tables with
     * a multi-column primary key, a serialize()d version of the primary key will be returned.
     *
     * @param array  $row       resultset row.
     * @param int    $offset    The 0-based offset for reading from the resultset row.
     * @param string $indexType One of the class type constants TableMap::TYPE_PHPNAME, TableMap::TYPE_CAMELNAME
     *                           TableMap::TYPE_COLNAME, TableMap::TYPE_FIELDNAME, TableMap::TYPE_NUM
     *
     * @return string The primary key hash of the row
     */
    public static function getPrimaryKeyHashFromRow($row, $offset = 0, $indexType = TableMap::TYPE_NUM)
    {
        // If the PK cannot be derived from the row, return NULL.
        if ($row[TableMap::TYPE_NUM == $indexType ? 0 + $offset : static::translateFieldName('IdServicerendu', TableMap::TYPE_PHPNAME, $indexType)] === null) {
            return null;
        }

        return null === $row[TableMap::TYPE_NUM == $indexType ? 0 + $offset : static::translateFieldName('IdServicerendu', TableMap::TYPE_PHPNAME, $indexType)] || is_scalar($row[TableMap::TYPE_NUM == $indexType ? 0 + $offset : static::translateFieldName('IdServicerendu', TableMap::TYPE_PHPNAME, $indexType)]) || is_callable([$row[TableMap::TYPE_NUM == $indexType ? 0 + $offset : static::translateFieldName('IdServicerendu', TableMap::TYPE_PHPNAME, $indexType)], '__toString']) ? (string) $row[TableMap::TYPE_NUM == $indexType ? 0 + $offset : static::translateFieldName('IdServicerendu', TableMap::TYPE_PHPNAME, $indexType)] : $row[TableMap::TYPE_NUM == $indexType ? 0 + $offset : static::translateFieldName('IdServicerendu', TableMap::TYPE_PHPNAME, $indexType)];
    }

    /**
     * Retrieves the primary key from the DB resultset row
     * For tables with a single-column primary key, that simple pkey value will be returned.  For tables with
     * a multi-column primary key, an array of the primary key columns will be returned.
     *
     * @param array  $row       resultset row.
     * @param int    $offset    The 0-based offset for reading from the resultset row.
     * @param string $indexType One of the class type constants TableMap::TYPE_PHPNAME, TableMap::TYPE_CAMELNAME
     *                           TableMap::TYPE_COLNAME, TableMap::TYPE_FIELDNAME, TableMap::TYPE_NUM
     *
     * @return mixed The primary key of the row
     */
    public static function getPrimaryKeyFromRow($row, $offset = 0, $indexType = TableMap::TYPE_NUM)
    {
        return (string) $row[
            $indexType == TableMap::TYPE_NUM
                ? 0 + $offset
                : self::translateFieldName('IdServicerendu', TableMap::TYPE_PHPNAME, $indexType)
        ];
    }

    /**
     * The class that the tableMap will make instances of.
     *
     * If $withPrefix is true, the returned path
     * uses a dot-path notation which is translated into a path
     * relative to a location on the PHP include_path.
     * (e.g. path.to.MyClass -> 'path/to/MyClass.php')
     *
     * @param boolean $withPrefix Whether or not to return the path with the class name
     * @return string path.to.ClassName
     */
    public static function getOMClass($withPrefix = true)
    {
        return $withPrefix ? ServicerenduTableMap::CLASS_DEFAULT : ServicerenduTableMap::OM_CLASS;
    }

    /**
     * Populates an object of the default type or an object that inherit from the default.
     *
     * @param array  $row       row returned by DataFetcher->fetch().
     * @param int    $offset    The 0-based offset for reading from the resultset row.
     * @param string $indexType The index type of $row. Mostly DataFetcher->getIndexType().
                                 One of the class type constants TableMap::TYPE_PHPNAME, TableMap::TYPE_CAMELNAME
     *                           TableMap::TYPE_COLNAME, TableMap::TYPE_FIELDNAME, TableMap::TYPE_NUM.
     *
     * @throws PropelException Any exceptions caught during processing will be
     *                         rethrown wrapped into a PropelException.
     * @return array           (Servicerendu object, last column rank)
     */
    public static function populateObject($row, $offset = 0, $indexType = TableMap::TYPE_NUM)
    {
        $key = ServicerenduTableMap::getPrimaryKeyHashFromRow($row, $offset, $indexType);
        if (null !== ($obj = ServicerenduTableMap::getInstanceFromPool($key))) {
            // We no longer rehydrate the object, since this can cause data loss.
            // See http://www.propelorm.org/ticket/509
            // $obj->hydrate($row, $offset, true); // rehydrate
            $col = $offset + ServicerenduTableMap::NUM_HYDRATE_COLUMNS;
        } else {
            $cls = ServicerenduTableMap::OM_CLASS;
            /** @var Servicerendu $obj */
            $obj = new $cls();
            $col = $obj->hydrate($row, $offset, false, $indexType);
            ServicerenduTableMap::addInstanceToPool($obj, $key);
        }

        return array($obj, $col);
    }

    /**
     * The returned array will contain objects of the default type or
     * objects that inherit from the default.
     *
     * @param DataFetcherInterface $dataFetcher
     * @return array
     * @throws PropelException Any exceptions caught during processing will be
     *                         rethrown wrapped into a PropelException.
     */
    public static function populateObjects(DataFetcherInterface $dataFetcher)
    {
        $results = array();

        // set the class once to avoid overhead in the loop
        $cls = static::getOMClass(false);
        // populate the object(s)
        while ($row = $dataFetcher->fetch()) {
            $key = ServicerenduTableMap::getPrimaryKeyHashFromRow($row, 0, $dataFetcher->getIndexType());
            if (null !== ($obj = ServicerenduTableMap::getInstanceFromPool($key))) {
                // We no longer rehydrate the object, since this can cause data loss.
                // See http://www.propelorm.org/ticket/509
                // $obj->hydrate($row, 0, true); // rehydrate
                $results[] = $obj;
            } else {
                /** @var Servicerendu $obj */
                $obj = new $cls();
                $obj->hydrate($row);
                $results[] = $obj;
                ServicerenduTableMap::addInstanceToPool($obj, $key);
            } // if key exists
        }

        return $results;
    }
    /**
     * Add all the columns needed to create a new object.
     *
     * Note: any columns that were marked with lazyLoad="true" in the
     * XML schema will not be added to the select list and only loaded
     * on demand.
     *
     * @param Criteria $criteria object containing the columns to add.
     * @param string   $alias    optional table alias
     * @throws PropelException Any exceptions caught during processing will be
     *                         rethrown wrapped into a PropelException.
     */
    public static function addSelectColumns(Criteria $criteria, $alias = null)
    {
        if (null === $alias) {
            $criteria->addSelectColumn(ServicerenduTableMap::COL_ID_SERVICERENDU);
            $criteria->addSelectColumn(ServicerenduTableMap::COL_ID_MEMBRE);
            $criteria->addSelectColumn(ServicerenduTableMap::COL_MONTANT);
            $criteria->addSelectColumn(ServicerenduTableMap::COL_DATE_ENREGISTREMENT);
            $criteria->addSelectColumn(ServicerenduTableMap::COL_DATE_DEBUT);
            $criteria->addSelectColumn(ServicerenduTableMap::COL_DATE_FIN);
            $criteria->addSelectColumn(ServicerenduTableMap::COL_ORIGINE);
            $criteria->addSelectColumn(ServicerenduTableMap::COL_PREMIER);
            $criteria->addSelectColumn(ServicerenduTableMap::COL_DERNIER);
            $criteria->addSelectColumn(ServicerenduTableMap::COL_ID_ENTITE);
            $criteria->addSelectColumn(ServicerenduTableMap::COL_ID_PRESTATION);
            $criteria->addSelectColumn(ServicerenduTableMap::COL_REGLE);
            $criteria->addSelectColumn(ServicerenduTableMap::COL_COMPTABILISE);
            $criteria->addSelectColumn(ServicerenduTableMap::COL_OBSERVATION);
            $criteria->addSelectColumn(ServicerenduTableMap::COL_ID_TVA);
            $criteria->addSelectColumn(ServicerenduTableMap::COL_QUANTITE);
            $criteria->addSelectColumn(ServicerenduTableMap::COL_CREATED_AT);
            $criteria->addSelectColumn(ServicerenduTableMap::COL_UPDATED_AT);
        } else {
            $criteria->addSelectColumn($alias . '.id_servicerendu');
            $criteria->addSelectColumn($alias . '.id_membre');
            $criteria->addSelectColumn($alias . '.montant');
            $criteria->addSelectColumn($alias . '.date_enregistrement');
            $criteria->addSelectColumn($alias . '.date_debut');
            $criteria->addSelectColumn($alias . '.date_fin');
            $criteria->addSelectColumn($alias . '.origine');
            $criteria->addSelectColumn($alias . '.premier');
            $criteria->addSelectColumn($alias . '.dernier');
            $criteria->addSelectColumn($alias . '.id_entite');
            $criteria->addSelectColumn($alias . '.id_prestation');
            $criteria->addSelectColumn($alias . '.regle');
            $criteria->addSelectColumn($alias . '.comptabilise');
            $criteria->addSelectColumn($alias . '.observation');
            $criteria->addSelectColumn($alias . '.id_tva');
            $criteria->addSelectColumn($alias . '.quantite');
            $criteria->addSelectColumn($alias . '.created_at');
            $criteria->addSelectColumn($alias . '.updated_at');
        }
    }

    /**
     * Returns the TableMap related to this object.
     * This method is not needed for general use but a specific application could have a need.
     * @return TableMap
     * @throws PropelException Any exceptions caught during processing will be
     *                         rethrown wrapped into a PropelException.
     */
    public static function getTableMap()
    {
        return Propel::getServiceContainer()->getDatabaseMap(ServicerenduTableMap::DATABASE_NAME)->getTable(ServicerenduTableMap::TABLE_NAME);
    }

    /**
     * Add a TableMap instance to the database for this tableMap class.
     */
    public static function buildTableMap()
    {
        $dbMap = Propel::getServiceContainer()->getDatabaseMap(ServicerenduTableMap::DATABASE_NAME);
        if (!$dbMap->hasTable(ServicerenduTableMap::TABLE_NAME)) {
            $dbMap->addTableObject(new ServicerenduTableMap());
        }
    }

    /**
     * Performs a DELETE on the database, given a Servicerendu or Criteria object OR a primary key value.
     *
     * @param mixed               $values Criteria or Servicerendu object or primary key or array of primary keys
     *              which is used to create the DELETE statement
     * @param  ConnectionInterface $con the connection to use
     * @return int             The number of affected rows (if supported by underlying database driver).  This includes CASCADE-related rows
     *                         if supported by native driver or if emulated using Propel.
     * @throws PropelException Any exceptions caught during processing will be
     *                         rethrown wrapped into a PropelException.
     */
     public static function doDelete($values, ConnectionInterface $con = null)
     {
        if (null === $con) {
            $con = Propel::getServiceContainer()->getWriteConnection(ServicerenduTableMap::DATABASE_NAME);
        }

        if ($values instanceof Criteria) {
            // rename for clarity
            $criteria = $values;
        } elseif ($values instanceof \Servicerendu) { // it's a model object
            // create criteria based on pk values
            $criteria = $values->buildPkeyCriteria();
        } else { // it's a primary key, or an array of pks
            $criteria = new Criteria(ServicerenduTableMap::DATABASE_NAME);
            $criteria->add(ServicerenduTableMap::COL_ID_SERVICERENDU, (array) $values, Criteria::IN);
        }

        $query = ServicerenduQuery::create()->mergeWith($criteria);

        if ($values instanceof Criteria) {
            ServicerenduTableMap::clearInstancePool();
        } elseif (!is_object($values)) { // it's a primary key, or an array of pks
            foreach ((array) $values as $singleval) {
                ServicerenduTableMap::removeInstanceFromPool($singleval);
            }
        }

        return $query->delete($con);
    }

    /**
     * Deletes all rows from the asso_servicerendus table.
     *
     * @param ConnectionInterface $con the connection to use
     * @return int The number of affected rows (if supported by underlying database driver).
     */
    public static function doDeleteAll(ConnectionInterface $con = null)
    {
        return ServicerenduQuery::create()->doDeleteAll($con);
    }

    /**
     * Performs an INSERT on the database, given a Servicerendu or Criteria object.
     *
     * @param mixed               $criteria Criteria or Servicerendu object containing data that is used to create the INSERT statement.
     * @param ConnectionInterface $con the ConnectionInterface connection to use
     * @return mixed           The new primary key.
     * @throws PropelException Any exceptions caught during processing will be
     *                         rethrown wrapped into a PropelException.
     */
    public static function doInsert($criteria, ConnectionInterface $con = null)
    {
        if (null === $con) {
            $con = Propel::getServiceContainer()->getWriteConnection(ServicerenduTableMap::DATABASE_NAME);
        }

        if ($criteria instanceof Criteria) {
            $criteria = clone $criteria; // rename for clarity
        } else {
            $criteria = $criteria->buildCriteria(); // build Criteria from Servicerendu object
        }

        if ($criteria->containsKey(ServicerenduTableMap::COL_ID_SERVICERENDU) && $criteria->keyContainsValue(ServicerenduTableMap::COL_ID_SERVICERENDU) ) {
            throw new PropelException('Cannot insert a value for auto-increment primary key ('.ServicerenduTableMap::COL_ID_SERVICERENDU.')');
        }


        // Set the correct dbName
        $query = ServicerenduQuery::create()->mergeWith($criteria);

        // use transaction because $criteria could contain info
        // for more than one table (I guess, conceivably)
        return $con->transaction(function () use ($con, $query) {
            return $query->doInsert($con);
        });
    }

} // ServicerenduTableMap
// This is the static code needed to register the TableMap for this table with the main Propel class.
//
ServicerenduTableMap::buildTableMap();
