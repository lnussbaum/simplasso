<?php

namespace Map;

use \Individu;
use \IndividuQuery;
use Propel\Runtime\Propel;
use Propel\Runtime\ActiveQuery\Criteria;
use Propel\Runtime\ActiveQuery\InstancePoolTrait;
use Propel\Runtime\Connection\ConnectionInterface;
use Propel\Runtime\DataFetcher\DataFetcherInterface;
use Propel\Runtime\Exception\PropelException;
use Propel\Runtime\Map\RelationMap;
use Propel\Runtime\Map\TableMap;
use Propel\Runtime\Map\TableMapTrait;


/**
 * This class defines the structure of the 'asso_individus' table.
 *
 *
 *
 * This map class is used by Propel to do runtime db structure discovery.
 * For example, the createSelectSql() method checks the type of a given column used in an
 * ORDER BY clause to know whether it needs to apply SQL to make the ORDER BY case-insensitive
 * (i.e. if it's a text column type).
 *
 */
class IndividuTableMap extends TableMap
{
    use InstancePoolTrait;
    use TableMapTrait;

    /**
     * The (dot-path) name of this class
     */
    const CLASS_NAME = '.Map.IndividuTableMap';

    /**
     * The default database name for this class
     */
    const DATABASE_NAME = 'simplasso';

    /**
     * The table name for this class
     */
    const TABLE_NAME = 'asso_individus';

    /**
     * The related Propel class for this table
     */
    const OM_CLASS = '\\Individu';

    /**
     * A class that can be returned by this tableMap
     */
    const CLASS_DEFAULT = 'Individu';

    /**
     * The total number of columns
     */
    const NUM_COLUMNS = 29;

    /**
     * The number of lazy-loaded columns
     */
    const NUM_LAZY_LOAD_COLUMNS = 0;

    /**
     * The number of columns to hydrate (NUM_COLUMNS - NUM_LAZY_LOAD_COLUMNS)
     */
    const NUM_HYDRATE_COLUMNS = 29;

    /**
     * the column name for the id_individu field
     */
    const COL_ID_INDIVIDU = 'asso_individus.id_individu';

    /**
     * the column name for the nom field
     */
    const COL_NOM = 'asso_individus.nom';

    /**
     * the column name for the bio field
     */
    const COL_BIO = 'asso_individus.bio';

    /**
     * the column name for the email field
     */
    const COL_EMAIL = 'asso_individus.email';

    /**
     * the column name for the login field
     */
    const COL_LOGIN = 'asso_individus.login';

    /**
     * the column name for the pass field
     */
    const COL_PASS = 'asso_individus.pass';

    /**
     * the column name for the alea_actuel field
     */
    const COL_ALEA_ACTUEL = 'asso_individus.alea_actuel';

    /**
     * the column name for the alea_futur field
     */
    const COL_ALEA_FUTUR = 'asso_individus.alea_futur';

    /**
     * the column name for the token field
     */
    const COL_TOKEN = 'asso_individus.token';

    /**
     * the column name for the token_time field
     */
    const COL_TOKEN_TIME = 'asso_individus.token_time';

    /**
     * the column name for the civilite field
     */
    const COL_CIVILITE = 'asso_individus.civilite';

    /**
     * the column name for the sexe field
     */
    const COL_SEXE = 'asso_individus.sexe';

    /**
     * the column name for the nom_famille field
     */
    const COL_NOM_FAMILLE = 'asso_individus.nom_famille';

    /**
     * the column name for the prenom field
     */
    const COL_PRENOM = 'asso_individus.prenom';

    /**
     * the column name for the naissance field
     */
    const COL_NAISSANCE = 'asso_individus.naissance';

    /**
     * the column name for the adresse field
     */
    const COL_ADRESSE = 'asso_individus.adresse';

    /**
     * the column name for the codepostal field
     */
    const COL_CODEPOSTAL = 'asso_individus.codepostal';

    /**
     * the column name for the ville field
     */
    const COL_VILLE = 'asso_individus.ville';

    /**
     * the column name for the pays field
     */
    const COL_PAYS = 'asso_individus.pays';

    /**
     * the column name for the telephone field
     */
    const COL_TELEPHONE = 'asso_individus.telephone';

    /**
     * the column name for the telephone_pro field
     */
    const COL_TELEPHONE_PRO = 'asso_individus.telephone_pro';

    /**
     * the column name for the fax field
     */
    const COL_FAX = 'asso_individus.fax';

    /**
     * the column name for the mobile field
     */
    const COL_MOBILE = 'asso_individus.mobile';

    /**
     * the column name for the url field
     */
    const COL_URL = 'asso_individus.url';

    /**
     * the column name for the profession field
     */
    const COL_PROFESSION = 'asso_individus.profession';

    /**
     * the column name for the contact_souhait field
     */
    const COL_CONTACT_SOUHAIT = 'asso_individus.contact_souhait';

    /**
     * the column name for the observation field
     */
    const COL_OBSERVATION = 'asso_individus.observation';

    /**
     * the column name for the created_at field
     */
    const COL_CREATED_AT = 'asso_individus.created_at';

    /**
     * the column name for the updated_at field
     */
    const COL_UPDATED_AT = 'asso_individus.updated_at';

    /**
     * The default string format for model objects of the related table
     */
    const DEFAULT_STRING_FORMAT = 'YAML';

    /**
     * holds an array of fieldnames
     *
     * first dimension keys are the type constants
     * e.g. self::$fieldNames[self::TYPE_PHPNAME][0] = 'Id'
     */
    protected static $fieldNames = array (
        self::TYPE_PHPNAME       => array('IdIndividu', 'Nom', 'Bio', 'Email', 'Login', 'Pass', 'AleaActuel', 'AleaFutur', 'Token', 'TokenTime', 'Civilite', 'Sexe', 'NomFamille', 'Prenom', 'Naissance', 'Adresse', 'Codepostal', 'Ville', 'Pays', 'Telephone', 'TelephonePro', 'Fax', 'Mobile', 'Url', 'Profession', 'ContactSouhait', 'Observation', 'CreatedAt', 'UpdatedAt', ),
        self::TYPE_CAMELNAME     => array('idIndividu', 'nom', 'bio', 'email', 'login', 'pass', 'aleaActuel', 'aleaFutur', 'token', 'tokenTime', 'civilite', 'sexe', 'nomFamille', 'prenom', 'naissance', 'adresse', 'codepostal', 'ville', 'pays', 'telephone', 'telephonePro', 'fax', 'mobile', 'url', 'profession', 'contactSouhait', 'observation', 'createdAt', 'updatedAt', ),
        self::TYPE_COLNAME       => array(IndividuTableMap::COL_ID_INDIVIDU, IndividuTableMap::COL_NOM, IndividuTableMap::COL_BIO, IndividuTableMap::COL_EMAIL, IndividuTableMap::COL_LOGIN, IndividuTableMap::COL_PASS, IndividuTableMap::COL_ALEA_ACTUEL, IndividuTableMap::COL_ALEA_FUTUR, IndividuTableMap::COL_TOKEN, IndividuTableMap::COL_TOKEN_TIME, IndividuTableMap::COL_CIVILITE, IndividuTableMap::COL_SEXE, IndividuTableMap::COL_NOM_FAMILLE, IndividuTableMap::COL_PRENOM, IndividuTableMap::COL_NAISSANCE, IndividuTableMap::COL_ADRESSE, IndividuTableMap::COL_CODEPOSTAL, IndividuTableMap::COL_VILLE, IndividuTableMap::COL_PAYS, IndividuTableMap::COL_TELEPHONE, IndividuTableMap::COL_TELEPHONE_PRO, IndividuTableMap::COL_FAX, IndividuTableMap::COL_MOBILE, IndividuTableMap::COL_URL, IndividuTableMap::COL_PROFESSION, IndividuTableMap::COL_CONTACT_SOUHAIT, IndividuTableMap::COL_OBSERVATION, IndividuTableMap::COL_CREATED_AT, IndividuTableMap::COL_UPDATED_AT, ),
        self::TYPE_FIELDNAME     => array('id_individu', 'nom', 'bio', 'email', 'login', 'pass', 'alea_actuel', 'alea_futur', 'token', 'token_time', 'civilite', 'sexe', 'nom_famille', 'prenom', 'naissance', 'adresse', 'codepostal', 'ville', 'pays', 'telephone', 'telephone_pro', 'fax', 'mobile', 'url', 'profession', 'contact_souhait', 'observation', 'created_at', 'updated_at', ),
        self::TYPE_NUM           => array(0, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15, 16, 17, 18, 19, 20, 21, 22, 23, 24, 25, 26, 27, 28, )
    );

    /**
     * holds an array of keys for quick access to the fieldnames array
     *
     * first dimension keys are the type constants
     * e.g. self::$fieldKeys[self::TYPE_PHPNAME]['Id'] = 0
     */
    protected static $fieldKeys = array (
        self::TYPE_PHPNAME       => array('IdIndividu' => 0, 'Nom' => 1, 'Bio' => 2, 'Email' => 3, 'Login' => 4, 'Pass' => 5, 'AleaActuel' => 6, 'AleaFutur' => 7, 'Token' => 8, 'TokenTime' => 9, 'Civilite' => 10, 'Sexe' => 11, 'NomFamille' => 12, 'Prenom' => 13, 'Naissance' => 14, 'Adresse' => 15, 'Codepostal' => 16, 'Ville' => 17, 'Pays' => 18, 'Telephone' => 19, 'TelephonePro' => 20, 'Fax' => 21, 'Mobile' => 22, 'Url' => 23, 'Profession' => 24, 'ContactSouhait' => 25, 'Observation' => 26, 'CreatedAt' => 27, 'UpdatedAt' => 28, ),
        self::TYPE_CAMELNAME     => array('idIndividu' => 0, 'nom' => 1, 'bio' => 2, 'email' => 3, 'login' => 4, 'pass' => 5, 'aleaActuel' => 6, 'aleaFutur' => 7, 'token' => 8, 'tokenTime' => 9, 'civilite' => 10, 'sexe' => 11, 'nomFamille' => 12, 'prenom' => 13, 'naissance' => 14, 'adresse' => 15, 'codepostal' => 16, 'ville' => 17, 'pays' => 18, 'telephone' => 19, 'telephonePro' => 20, 'fax' => 21, 'mobile' => 22, 'url' => 23, 'profession' => 24, 'contactSouhait' => 25, 'observation' => 26, 'createdAt' => 27, 'updatedAt' => 28, ),
        self::TYPE_COLNAME       => array(IndividuTableMap::COL_ID_INDIVIDU => 0, IndividuTableMap::COL_NOM => 1, IndividuTableMap::COL_BIO => 2, IndividuTableMap::COL_EMAIL => 3, IndividuTableMap::COL_LOGIN => 4, IndividuTableMap::COL_PASS => 5, IndividuTableMap::COL_ALEA_ACTUEL => 6, IndividuTableMap::COL_ALEA_FUTUR => 7, IndividuTableMap::COL_TOKEN => 8, IndividuTableMap::COL_TOKEN_TIME => 9, IndividuTableMap::COL_CIVILITE => 10, IndividuTableMap::COL_SEXE => 11, IndividuTableMap::COL_NOM_FAMILLE => 12, IndividuTableMap::COL_PRENOM => 13, IndividuTableMap::COL_NAISSANCE => 14, IndividuTableMap::COL_ADRESSE => 15, IndividuTableMap::COL_CODEPOSTAL => 16, IndividuTableMap::COL_VILLE => 17, IndividuTableMap::COL_PAYS => 18, IndividuTableMap::COL_TELEPHONE => 19, IndividuTableMap::COL_TELEPHONE_PRO => 20, IndividuTableMap::COL_FAX => 21, IndividuTableMap::COL_MOBILE => 22, IndividuTableMap::COL_URL => 23, IndividuTableMap::COL_PROFESSION => 24, IndividuTableMap::COL_CONTACT_SOUHAIT => 25, IndividuTableMap::COL_OBSERVATION => 26, IndividuTableMap::COL_CREATED_AT => 27, IndividuTableMap::COL_UPDATED_AT => 28, ),
        self::TYPE_FIELDNAME     => array('id_individu' => 0, 'nom' => 1, 'bio' => 2, 'email' => 3, 'login' => 4, 'pass' => 5, 'alea_actuel' => 6, 'alea_futur' => 7, 'token' => 8, 'token_time' => 9, 'civilite' => 10, 'sexe' => 11, 'nom_famille' => 12, 'prenom' => 13, 'naissance' => 14, 'adresse' => 15, 'codepostal' => 16, 'ville' => 17, 'pays' => 18, 'telephone' => 19, 'telephone_pro' => 20, 'fax' => 21, 'mobile' => 22, 'url' => 23, 'profession' => 24, 'contact_souhait' => 25, 'observation' => 26, 'created_at' => 27, 'updated_at' => 28, ),
        self::TYPE_NUM           => array(0, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15, 16, 17, 18, 19, 20, 21, 22, 23, 24, 25, 26, 27, 28, )
    );

    /**
     * Initialize the table attributes and columns
     * Relations are not initialized by this method since they are lazy loaded
     *
     * @return void
     * @throws PropelException
     */
    public function initialize()
    {
        // attributes
        $this->setName('asso_individus');
        $this->setPhpName('Individu');
        $this->setIdentifierQuoting(true);
        $this->setClassName('\\Individu');
        $this->setPackage('');
        $this->setUseIdGenerator(true);
        // columns
        $this->addPrimaryKey('id_individu', 'IdIndividu', 'BIGINT', true, 21, null);
        $this->addColumn('nom', 'Nom', 'LONGVARCHAR', true, null, null);
        $this->addColumn('bio', 'Bio', 'LONGVARCHAR', false, null, null);
        $this->addColumn('email', 'Email', 'VARCHAR', false, 255, null);
        $this->addColumn('login', 'Login', 'VARCHAR', false, 200, null);
        $this->addColumn('pass', 'Pass', 'VARCHAR', false, 128, null);
        $this->addColumn('alea_actuel', 'AleaActuel', 'VARCHAR', false, 50, null);
        $this->addColumn('alea_futur', 'AleaFutur', 'VARCHAR', false, 50, null);
        $this->addColumn('token', 'Token', 'VARCHAR', false, 50, null);
        $this->addColumn('token_time', 'TokenTime', 'BIGINT', false, null, null);
        $this->addColumn('civilite', 'Civilite', 'VARCHAR', false, 40, null);
        $this->addColumn('sexe', 'Sexe', 'VARCHAR', false, 2, null);
        $this->addColumn('nom_famille', 'NomFamille', 'LONGVARCHAR', false, null, null);
        $this->addColumn('prenom', 'Prenom', 'LONGVARCHAR', false, null, null);
        $this->addColumn('naissance', 'Naissance', 'DATE', false, null, null);
        $this->addColumn('adresse', 'Adresse', 'LONGVARCHAR', false, null, null);
        $this->addColumn('codepostal', 'Codepostal', 'LONGVARCHAR', false, null, null);
        $this->addColumn('ville', 'Ville', 'LONGVARCHAR', false, null, null);
        $this->addColumn('pays', 'Pays', 'VARCHAR', false, 2, null);
        $this->addColumn('telephone', 'Telephone', 'VARCHAR', false, 50, null);
        $this->addColumn('telephone_pro', 'TelephonePro', 'VARCHAR', false, 50, null);
        $this->addColumn('fax', 'Fax', 'VARCHAR', false, 50, null);
        $this->addColumn('mobile', 'Mobile', 'VARCHAR', false, 50, null);
        $this->addColumn('url', 'Url', 'LONGVARCHAR', false, null, null);
        $this->addColumn('profession', 'Profession', 'LONGVARCHAR', false, null, null);
        $this->addColumn('contact_souhait', 'ContactSouhait', 'BOOLEAN', true, 1, true);
        $this->addColumn('observation', 'Observation', 'LONGVARCHAR', false, null, null);
        $this->addColumn('created_at', 'CreatedAt', 'TIMESTAMP', false, null, null);
        $this->addColumn('updated_at', 'UpdatedAt', 'TIMESTAMP', false, null, null);
    } // initialize()

    /**
     * Build the RelationMap objects for this table relationships
     */
    public function buildRelations()
    {
        $this->addRelation('Autorisation', '\\Autorisation', RelationMap::ONE_TO_MANY, array (
  0 =>
  array (
    0 => ':id_individu',
    1 => ':id_individu',
  ),
), 'CASCADE', null, 'Autorisations', false);
        $this->addRelation('Log', '\\Log', RelationMap::ONE_TO_MANY, array (
  0 =>
  array (
    0 => ':id_individu',
    1 => ':id_individu',
  ),
), 'CASCADE', null, 'Logs', false);
        $this->addRelation('Membres0', '\\Membre', RelationMap::ONE_TO_MANY, array (
  0 =>
  array (
    0 => ':id_individu_titulaire',
    1 => ':id_individu',
  ),
), 'CASCADE', null, 'Membres0s', false);
        $this->addRelation('MembreIndividu', '\\MembreIndividu', RelationMap::ONE_TO_MANY, array (
  0 =>
  array (
    0 => ':id_individu',
    1 => ':id_individu',
  ),
), 'CASCADE', null, 'MembreIndividus', false);
        $this->addRelation('ServicerenduIndividu', '\\ServicerenduIndividu', RelationMap::ONE_TO_MANY, array (
  0 =>
  array (
    0 => ':id_individu',
    1 => ':id_individu',
  ),
), 'CASCADE', null, 'ServicerenduIndividus', false);
        $this->addRelation('NotificationIndividu', '\\NotificationIndividu', RelationMap::ONE_TO_MANY, array (
  0 =>
  array (
    0 => ':id_individu',
    1 => ':id_individu',
  ),
), 'CASCADE', null, 'NotificationIndividus', false);
        $this->addRelation('CourrierLien', '\\CourrierLien', RelationMap::ONE_TO_MANY, array (
  0 =>
  array (
    0 => ':id_individu',
    1 => ':id_individu',
  ),
), 'CASCADE', null, 'CourrierLiens', false);
        $this->addRelation('Membre', '\\Membre', RelationMap::MANY_TO_MANY, array(), 'CASCADE', null, 'Membres');
        $this->addRelation('Servicerendu', '\\Servicerendu', RelationMap::MANY_TO_MANY, array(), 'CASCADE', null, 'Servicerendus');
        $this->addRelation('Notification', '\\Notification', RelationMap::MANY_TO_MANY, array(), 'CASCADE', null, 'Notifications');
    } // buildRelations()

    /**
     *
     * Gets the list of behaviors registered for this table
     *
     * @return array Associative array (name => parameters) of behaviors
     */
    public function getBehaviors()
    {
        return array(
            'timestampable' => array('create_column' => 'created_at', 'update_column' => 'updated_at', 'disable_created_at' => 'false', 'disable_updated_at' => 'false', ),
            'archivable' => array('archive_table' => '', 'archive_phpname' => '', 'archive_class' => '', 'log_archived_at' => 'true', 'archived_at_column' => 'archived_at', 'archive_on_insert' => 'false', 'archive_on_update' => 'false', 'archive_on_delete' => 'true', ),
        );
    } // getBehaviors()
    /**
     * Method to invalidate the instance pool of all tables related to asso_individus     * by a foreign key with ON DELETE CASCADE
     */
    public static function clearRelatedInstancePool()
    {
        // Invalidate objects in related instance pools,
        // since one or more of them may be deleted by ON DELETE CASCADE/SETNULL rule.
        AutorisationTableMap::clearInstancePool();
        LogTableMap::clearInstancePool();
        MembreTableMap::clearInstancePool();
        MembreIndividuTableMap::clearInstancePool();
        ServicerenduIndividuTableMap::clearInstancePool();
        NotificationIndividuTableMap::clearInstancePool();
        CourrierLienTableMap::clearInstancePool();
    }

    /**
     * Retrieves a string version of the primary key from the DB resultset row that can be used to uniquely identify a row in this table.
     *
     * For tables with a single-column primary key, that simple pkey value will be returned.  For tables with
     * a multi-column primary key, a serialize()d version of the primary key will be returned.
     *
     * @param array  $row       resultset row.
     * @param int    $offset    The 0-based offset for reading from the resultset row.
     * @param string $indexType One of the class type constants TableMap::TYPE_PHPNAME, TableMap::TYPE_CAMELNAME
     *                           TableMap::TYPE_COLNAME, TableMap::TYPE_FIELDNAME, TableMap::TYPE_NUM
     *
     * @return string The primary key hash of the row
     */
    public static function getPrimaryKeyHashFromRow($row, $offset = 0, $indexType = TableMap::TYPE_NUM)
    {
        // If the PK cannot be derived from the row, return NULL.
        if ($row[TableMap::TYPE_NUM == $indexType ? 0 + $offset : static::translateFieldName('IdIndividu', TableMap::TYPE_PHPNAME, $indexType)] === null) {
            return null;
        }

        return null === $row[TableMap::TYPE_NUM == $indexType ? 0 + $offset : static::translateFieldName('IdIndividu', TableMap::TYPE_PHPNAME, $indexType)] || is_scalar($row[TableMap::TYPE_NUM == $indexType ? 0 + $offset : static::translateFieldName('IdIndividu', TableMap::TYPE_PHPNAME, $indexType)]) || is_callable([$row[TableMap::TYPE_NUM == $indexType ? 0 + $offset : static::translateFieldName('IdIndividu', TableMap::TYPE_PHPNAME, $indexType)], '__toString']) ? (string) $row[TableMap::TYPE_NUM == $indexType ? 0 + $offset : static::translateFieldName('IdIndividu', TableMap::TYPE_PHPNAME, $indexType)] : $row[TableMap::TYPE_NUM == $indexType ? 0 + $offset : static::translateFieldName('IdIndividu', TableMap::TYPE_PHPNAME, $indexType)];
    }

    /**
     * Retrieves the primary key from the DB resultset row
     * For tables with a single-column primary key, that simple pkey value will be returned.  For tables with
     * a multi-column primary key, an array of the primary key columns will be returned.
     *
     * @param array  $row       resultset row.
     * @param int    $offset    The 0-based offset for reading from the resultset row.
     * @param string $indexType One of the class type constants TableMap::TYPE_PHPNAME, TableMap::TYPE_CAMELNAME
     *                           TableMap::TYPE_COLNAME, TableMap::TYPE_FIELDNAME, TableMap::TYPE_NUM
     *
     * @return mixed The primary key of the row
     */
    public static function getPrimaryKeyFromRow($row, $offset = 0, $indexType = TableMap::TYPE_NUM)
    {
        return (string) $row[
            $indexType == TableMap::TYPE_NUM
                ? 0 + $offset
                : self::translateFieldName('IdIndividu', TableMap::TYPE_PHPNAME, $indexType)
        ];
    }

    /**
     * The class that the tableMap will make instances of.
     *
     * If $withPrefix is true, the returned path
     * uses a dot-path notation which is translated into a path
     * relative to a location on the PHP include_path.
     * (e.g. path.to.MyClass -> 'path/to/MyClass.php')
     *
     * @param boolean $withPrefix Whether or not to return the path with the class name
     * @return string path.to.ClassName
     */
    public static function getOMClass($withPrefix = true)
    {
        return $withPrefix ? IndividuTableMap::CLASS_DEFAULT : IndividuTableMap::OM_CLASS;
    }

    /**
     * Populates an object of the default type or an object that inherit from the default.
     *
     * @param array  $row       row returned by DataFetcher->fetch().
     * @param int    $offset    The 0-based offset for reading from the resultset row.
     * @param string $indexType The index type of $row. Mostly DataFetcher->getIndexType().
                                 One of the class type constants TableMap::TYPE_PHPNAME, TableMap::TYPE_CAMELNAME
     *                           TableMap::TYPE_COLNAME, TableMap::TYPE_FIELDNAME, TableMap::TYPE_NUM.
     *
     * @throws PropelException Any exceptions caught during processing will be
     *                         rethrown wrapped into a PropelException.
     * @return array           (Individu object, last column rank)
     */
    public static function populateObject($row, $offset = 0, $indexType = TableMap::TYPE_NUM)
    {
        $key = IndividuTableMap::getPrimaryKeyHashFromRow($row, $offset, $indexType);
        if (null !== ($obj = IndividuTableMap::getInstanceFromPool($key))) {
            // We no longer rehydrate the object, since this can cause data loss.
            // See http://www.propelorm.org/ticket/509
            // $obj->hydrate($row, $offset, true); // rehydrate
            $col = $offset + IndividuTableMap::NUM_HYDRATE_COLUMNS;
        } else {
            $cls = IndividuTableMap::OM_CLASS;
            /** @var Individu $obj */
            $obj = new $cls();
            $col = $obj->hydrate($row, $offset, false, $indexType);
            IndividuTableMap::addInstanceToPool($obj, $key);
        }

        return array($obj, $col);
    }

    /**
     * The returned array will contain objects of the default type or
     * objects that inherit from the default.
     *
     * @param DataFetcherInterface $dataFetcher
     * @return array
     * @throws PropelException Any exceptions caught during processing will be
     *                         rethrown wrapped into a PropelException.
     */
    public static function populateObjects(DataFetcherInterface $dataFetcher)
    {
        $results = array();

        // set the class once to avoid overhead in the loop
        $cls = static::getOMClass(false);
        // populate the object(s)
        while ($row = $dataFetcher->fetch()) {
            $key = IndividuTableMap::getPrimaryKeyHashFromRow($row, 0, $dataFetcher->getIndexType());
            if (null !== ($obj = IndividuTableMap::getInstanceFromPool($key))) {
                // We no longer rehydrate the object, since this can cause data loss.
                // See http://www.propelorm.org/ticket/509
                // $obj->hydrate($row, 0, true); // rehydrate
                $results[] = $obj;
            } else {
                /** @var Individu $obj */
                $obj = new $cls();
                $obj->hydrate($row);
                $results[] = $obj;
                IndividuTableMap::addInstanceToPool($obj, $key);
            } // if key exists
        }

        return $results;
    }
    /**
     * Add all the columns needed to create a new object.
     *
     * Note: any columns that were marked with lazyLoad="true" in the
     * XML schema will not be added to the select list and only loaded
     * on demand.
     *
     * @param Criteria $criteria object containing the columns to add.
     * @param string   $alias    optional table alias
     * @throws PropelException Any exceptions caught during processing will be
     *                         rethrown wrapped into a PropelException.
     */
    public static function addSelectColumns(Criteria $criteria, $alias = null)
    {
        if (null === $alias) {
            $criteria->addSelectColumn(IndividuTableMap::COL_ID_INDIVIDU);
            $criteria->addSelectColumn(IndividuTableMap::COL_NOM);
            $criteria->addSelectColumn(IndividuTableMap::COL_BIO);
            $criteria->addSelectColumn(IndividuTableMap::COL_EMAIL);
            $criteria->addSelectColumn(IndividuTableMap::COL_LOGIN);
            $criteria->addSelectColumn(IndividuTableMap::COL_PASS);
            $criteria->addSelectColumn(IndividuTableMap::COL_ALEA_ACTUEL);
            $criteria->addSelectColumn(IndividuTableMap::COL_ALEA_FUTUR);
            $criteria->addSelectColumn(IndividuTableMap::COL_TOKEN);
            $criteria->addSelectColumn(IndividuTableMap::COL_TOKEN_TIME);
            $criteria->addSelectColumn(IndividuTableMap::COL_CIVILITE);
            $criteria->addSelectColumn(IndividuTableMap::COL_SEXE);
            $criteria->addSelectColumn(IndividuTableMap::COL_NOM_FAMILLE);
            $criteria->addSelectColumn(IndividuTableMap::COL_PRENOM);
            $criteria->addSelectColumn(IndividuTableMap::COL_NAISSANCE);
            $criteria->addSelectColumn(IndividuTableMap::COL_ADRESSE);
            $criteria->addSelectColumn(IndividuTableMap::COL_CODEPOSTAL);
            $criteria->addSelectColumn(IndividuTableMap::COL_VILLE);
            $criteria->addSelectColumn(IndividuTableMap::COL_PAYS);
            $criteria->addSelectColumn(IndividuTableMap::COL_TELEPHONE);
            $criteria->addSelectColumn(IndividuTableMap::COL_TELEPHONE_PRO);
            $criteria->addSelectColumn(IndividuTableMap::COL_FAX);
            $criteria->addSelectColumn(IndividuTableMap::COL_MOBILE);
            $criteria->addSelectColumn(IndividuTableMap::COL_URL);
            $criteria->addSelectColumn(IndividuTableMap::COL_PROFESSION);
            $criteria->addSelectColumn(IndividuTableMap::COL_CONTACT_SOUHAIT);
            $criteria->addSelectColumn(IndividuTableMap::COL_OBSERVATION);
            $criteria->addSelectColumn(IndividuTableMap::COL_CREATED_AT);
            $criteria->addSelectColumn(IndividuTableMap::COL_UPDATED_AT);
        } else {
            $criteria->addSelectColumn($alias . '.id_individu');
            $criteria->addSelectColumn($alias . '.nom');
            $criteria->addSelectColumn($alias . '.bio');
            $criteria->addSelectColumn($alias . '.email');
            $criteria->addSelectColumn($alias . '.login');
            $criteria->addSelectColumn($alias . '.pass');
            $criteria->addSelectColumn($alias . '.alea_actuel');
            $criteria->addSelectColumn($alias . '.alea_futur');
            $criteria->addSelectColumn($alias . '.token');
            $criteria->addSelectColumn($alias . '.token_time');
            $criteria->addSelectColumn($alias . '.civilite');
            $criteria->addSelectColumn($alias . '.sexe');
            $criteria->addSelectColumn($alias . '.nom_famille');
            $criteria->addSelectColumn($alias . '.prenom');
            $criteria->addSelectColumn($alias . '.naissance');
            $criteria->addSelectColumn($alias . '.adresse');
            $criteria->addSelectColumn($alias . '.codepostal');
            $criteria->addSelectColumn($alias . '.ville');
            $criteria->addSelectColumn($alias . '.pays');
            $criteria->addSelectColumn($alias . '.telephone');
            $criteria->addSelectColumn($alias . '.telephone_pro');
            $criteria->addSelectColumn($alias . '.fax');
            $criteria->addSelectColumn($alias . '.mobile');
            $criteria->addSelectColumn($alias . '.url');
            $criteria->addSelectColumn($alias . '.profession');
            $criteria->addSelectColumn($alias . '.contact_souhait');
            $criteria->addSelectColumn($alias . '.observation');
            $criteria->addSelectColumn($alias . '.created_at');
            $criteria->addSelectColumn($alias . '.updated_at');
        }
    }

    /**
     * Returns the TableMap related to this object.
     * This method is not needed for general use but a specific application could have a need.
     * @return TableMap
     * @throws PropelException Any exceptions caught during processing will be
     *                         rethrown wrapped into a PropelException.
     */
    public static function getTableMap()
    {
        return Propel::getServiceContainer()->getDatabaseMap(IndividuTableMap::DATABASE_NAME)->getTable(IndividuTableMap::TABLE_NAME);
    }

    /**
     * Add a TableMap instance to the database for this tableMap class.
     */
    public static function buildTableMap()
    {
        $dbMap = Propel::getServiceContainer()->getDatabaseMap(IndividuTableMap::DATABASE_NAME);
        if (!$dbMap->hasTable(IndividuTableMap::TABLE_NAME)) {
            $dbMap->addTableObject(new IndividuTableMap());
        }
    }

    /**
     * Performs a DELETE on the database, given a Individu or Criteria object OR a primary key value.
     *
     * @param mixed               $values Criteria or Individu object or primary key or array of primary keys
     *              which is used to create the DELETE statement
     * @param  ConnectionInterface $con the connection to use
     * @return int             The number of affected rows (if supported by underlying database driver).  This includes CASCADE-related rows
     *                         if supported by native driver or if emulated using Propel.
     * @throws PropelException Any exceptions caught during processing will be
     *                         rethrown wrapped into a PropelException.
     */
     public static function doDelete($values, ConnectionInterface $con = null)
     {
        if (null === $con) {
            $con = Propel::getServiceContainer()->getWriteConnection(IndividuTableMap::DATABASE_NAME);
        }

        if ($values instanceof Criteria) {
            // rename for clarity
            $criteria = $values;
        } elseif ($values instanceof \Individu) { // it's a model object
            // create criteria based on pk values
            $criteria = $values->buildPkeyCriteria();
        } else { // it's a primary key, or an array of pks
            $criteria = new Criteria(IndividuTableMap::DATABASE_NAME);
            $criteria->add(IndividuTableMap::COL_ID_INDIVIDU, (array) $values, Criteria::IN);
        }

        $query = IndividuQuery::create()->mergeWith($criteria);

        if ($values instanceof Criteria) {
            IndividuTableMap::clearInstancePool();
        } elseif (!is_object($values)) { // it's a primary key, or an array of pks
            foreach ((array) $values as $singleval) {
                IndividuTableMap::removeInstanceFromPool($singleval);
            }
        }

        return $query->delete($con);
    }

    /**
     * Deletes all rows from the asso_individus table.
     *
     * @param ConnectionInterface $con the connection to use
     * @return int The number of affected rows (if supported by underlying database driver).
     */
    public static function doDeleteAll(ConnectionInterface $con = null)
    {
        return IndividuQuery::create()->doDeleteAll($con);
    }

    /**
     * Performs an INSERT on the database, given a Individu or Criteria object.
     *
     * @param mixed               $criteria Criteria or Individu object containing data that is used to create the INSERT statement.
     * @param ConnectionInterface $con the ConnectionInterface connection to use
     * @return mixed           The new primary key.
     * @throws PropelException Any exceptions caught during processing will be
     *                         rethrown wrapped into a PropelException.
     */
    public static function doInsert($criteria, ConnectionInterface $con = null)
    {
        if (null === $con) {
            $con = Propel::getServiceContainer()->getWriteConnection(IndividuTableMap::DATABASE_NAME);
        }

        if ($criteria instanceof Criteria) {
            $criteria = clone $criteria; // rename for clarity
        } else {
            $criteria = $criteria->buildCriteria(); // build Criteria from Individu object
        }

        if ($criteria->containsKey(IndividuTableMap::COL_ID_INDIVIDU) && $criteria->keyContainsValue(IndividuTableMap::COL_ID_INDIVIDU) ) {
            throw new PropelException('Cannot insert a value for auto-increment primary key ('.IndividuTableMap::COL_ID_INDIVIDU.')');
        }


        // Set the correct dbName
        $query = IndividuQuery::create()->mergeWith($criteria);

        // use transaction because $criteria could contain info
        // for more than one table (I guess, conceivably)
        return $con->transaction(function () use ($con, $query) {
            return $query->doInsert($con);
        });
    }

} // IndividuTableMap
// This is the static code needed to register the TableMap for this table with the main Propel class.
//
IndividuTableMap::buildTableMap();
