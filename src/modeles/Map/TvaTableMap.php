<?php

namespace Map;

use \Tva;
use \TvaQuery;
use Propel\Runtime\Propel;
use Propel\Runtime\ActiveQuery\Criteria;
use Propel\Runtime\ActiveQuery\InstancePoolTrait;
use Propel\Runtime\Connection\ConnectionInterface;
use Propel\Runtime\DataFetcher\DataFetcherInterface;
use Propel\Runtime\Exception\PropelException;
use Propel\Runtime\Map\RelationMap;
use Propel\Runtime\Map\TableMap;
use Propel\Runtime\Map\TableMapTrait;


/**
 * This class defines the structure of the 'asso_tvas' table.
 *
 *
 *
 * This map class is used by Propel to do runtime db structure discovery.
 * For example, the createSelectSql() method checks the type of a given column used in an
 * ORDER BY clause to know whether it needs to apply SQL to make the ORDER BY case-insensitive
 * (i.e. if it's a text column type).
 *
 */
class TvaTableMap extends TableMap
{
    use InstancePoolTrait;
    use TableMapTrait;

    /**
     * The (dot-path) name of this class
     */
    const CLASS_NAME = '.Map.TvaTableMap';

    /**
     * The default database name for this class
     */
    const DATABASE_NAME = 'simplasso';

    /**
     * The table name for this class
     */
    const TABLE_NAME = 'asso_tvas';

    /**
     * The related Propel class for this table
     */
    const OM_CLASS = '\\Tva';

    /**
     * A class that can be returned by this tableMap
     */
    const CLASS_DEFAULT = 'Tva';

    /**
     * The total number of columns
     */
    const NUM_COLUMNS = 11;

    /**
     * The number of lazy-loaded columns
     */
    const NUM_LAZY_LOAD_COLUMNS = 0;

    /**
     * The number of columns to hydrate (NUM_COLUMNS - NUM_LAZY_LOAD_COLUMNS)
     */
    const NUM_HYDRATE_COLUMNS = 11;

    /**
     * the column name for the id_tva field
     */
    const COL_ID_TVA = 'asso_tvas.id_tva';

    /**
     * the column name for the nom field
     */
    const COL_NOM = 'asso_tvas.nom';

    /**
     * the column name for the nomcourt field
     */
    const COL_NOMCOURT = 'asso_tvas.nomcourt';

    /**
     * the column name for the id_compte field
     */
    const COL_ID_COMPTE = 'asso_tvas.id_compte';

    /**
     * the column name for the actif field
     */
    const COL_ACTIF = 'asso_tvas.actif';

    /**
     * the column name for the signe field
     */
    const COL_SIGNE = 'asso_tvas.signe';

    /**
     * the column name for the encaissement field
     */
    const COL_ENCAISSEMENT = 'asso_tvas.encaissement';

    /**
     * the column name for the id_entite field
     */
    const COL_ID_ENTITE = 'asso_tvas.id_entite';

    /**
     * the column name for the observation field
     */
    const COL_OBSERVATION = 'asso_tvas.observation';

    /**
     * the column name for the created_at field
     */
    const COL_CREATED_AT = 'asso_tvas.created_at';

    /**
     * the column name for the updated_at field
     */
    const COL_UPDATED_AT = 'asso_tvas.updated_at';

    /**
     * The default string format for model objects of the related table
     */
    const DEFAULT_STRING_FORMAT = 'YAML';

    /**
     * holds an array of fieldnames
     *
     * first dimension keys are the type constants
     * e.g. self::$fieldNames[self::TYPE_PHPNAME][0] = 'Id'
     */
    protected static $fieldNames = array (
        self::TYPE_PHPNAME       => array('IdTva', 'Nom', 'Nomcourt', 'IdCompte', 'Actif', 'Signe', 'Encaissement', 'IdEntite', 'Observation', 'CreatedAt', 'UpdatedAt', ),
        self::TYPE_CAMELNAME     => array('idTva', 'nom', 'nomcourt', 'idCompte', 'actif', 'signe', 'encaissement', 'idEntite', 'observation', 'createdAt', 'updatedAt', ),
        self::TYPE_COLNAME       => array(TvaTableMap::COL_ID_TVA, TvaTableMap::COL_NOM, TvaTableMap::COL_NOMCOURT, TvaTableMap::COL_ID_COMPTE, TvaTableMap::COL_ACTIF, TvaTableMap::COL_SIGNE, TvaTableMap::COL_ENCAISSEMENT, TvaTableMap::COL_ID_ENTITE, TvaTableMap::COL_OBSERVATION, TvaTableMap::COL_CREATED_AT, TvaTableMap::COL_UPDATED_AT, ),
        self::TYPE_FIELDNAME     => array('id_tva', 'nom', 'nomcourt', 'id_compte', 'actif', 'signe', 'encaissement', 'id_entite', 'observation', 'created_at', 'updated_at', ),
        self::TYPE_NUM           => array(0, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, )
    );

    /**
     * holds an array of keys for quick access to the fieldnames array
     *
     * first dimension keys are the type constants
     * e.g. self::$fieldKeys[self::TYPE_PHPNAME]['Id'] = 0
     */
    protected static $fieldKeys = array (
        self::TYPE_PHPNAME       => array('IdTva' => 0, 'Nom' => 1, 'Nomcourt' => 2, 'IdCompte' => 3, 'Actif' => 4, 'Signe' => 5, 'Encaissement' => 6, 'IdEntite' => 7, 'Observation' => 8, 'CreatedAt' => 9, 'UpdatedAt' => 10, ),
        self::TYPE_CAMELNAME     => array('idTva' => 0, 'nom' => 1, 'nomcourt' => 2, 'idCompte' => 3, 'actif' => 4, 'signe' => 5, 'encaissement' => 6, 'idEntite' => 7, 'observation' => 8, 'createdAt' => 9, 'updatedAt' => 10, ),
        self::TYPE_COLNAME       => array(TvaTableMap::COL_ID_TVA => 0, TvaTableMap::COL_NOM => 1, TvaTableMap::COL_NOMCOURT => 2, TvaTableMap::COL_ID_COMPTE => 3, TvaTableMap::COL_ACTIF => 4, TvaTableMap::COL_SIGNE => 5, TvaTableMap::COL_ENCAISSEMENT => 6, TvaTableMap::COL_ID_ENTITE => 7, TvaTableMap::COL_OBSERVATION => 8, TvaTableMap::COL_CREATED_AT => 9, TvaTableMap::COL_UPDATED_AT => 10, ),
        self::TYPE_FIELDNAME     => array('id_tva' => 0, 'nom' => 1, 'nomcourt' => 2, 'id_compte' => 3, 'actif' => 4, 'signe' => 5, 'encaissement' => 6, 'id_entite' => 7, 'observation' => 8, 'created_at' => 9, 'updated_at' => 10, ),
        self::TYPE_NUM           => array(0, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, )
    );

    /**
     * Initialize the table attributes and columns
     * Relations are not initialized by this method since they are lazy loaded
     *
     * @return void
     * @throws PropelException
     */
    public function initialize()
    {
        // attributes
        $this->setName('asso_tvas');
        $this->setPhpName('Tva');
        $this->setIdentifierQuoting(true);
        $this->setClassName('\\Tva');
        $this->setPackage('');
        $this->setUseIdGenerator(true);
        // columns
        $this->addPrimaryKey('id_tva', 'IdTva', 'BIGINT', true, 21, null);
        $this->addColumn('nom', 'Nom', 'VARCHAR', true, 25, 'Tva % ');
        $this->addColumn('nomcourt', 'Nomcourt', 'VARCHAR', true, 6, '20.00');
        $this->addForeignKey('id_compte', 'IdCompte', 'BIGINT', 'assc_comptes', 'id_compte', true, 21, 1);
        $this->addColumn('actif', 'Actif', 'INTEGER', true, 1, 1);
        $this->addColumn('signe', 'Signe', 'VARCHAR', true, 1, '+');
        $this->addColumn('encaissement', 'Encaissement', 'INTEGER', true, 1, 0);
        $this->addForeignKey('id_entite', 'IdEntite', 'BIGINT', 'asso_entites', 'id_entite', true, 21, 1);
        $this->addColumn('observation', 'Observation', 'LONGVARCHAR', false, null, null);
        $this->addColumn('created_at', 'CreatedAt', 'TIMESTAMP', false, null, null);
        $this->addColumn('updated_at', 'UpdatedAt', 'TIMESTAMP', false, null, null);
    } // initialize()

    /**
     * Build the RelationMap objects for this table relationships
     */
    public function buildRelations()
    {
        $this->addRelation('Entite', '\\Entite', RelationMap::MANY_TO_ONE, array (
  0 =>
  array (
    0 => ':id_entite',
    1 => ':id_entite',
  ),
), 'CASCADE', null, null, false);
        $this->addRelation('Compte', '\\Compte', RelationMap::MANY_TO_ONE, array (
  0 =>
  array (
    0 => ':id_compte',
    1 => ':id_compte',
  ),
), 'CASCADE', null, null, false);
        $this->addRelation('Tvataux', '\\Tvataux', RelationMap::ONE_TO_MANY, array (
  0 =>
  array (
    0 => ':id_tva',
    1 => ':id_tva',
  ),
), 'CASCADE', null, 'Tvatauxes', false);
    } // buildRelations()

    /**
     *
     * Gets the list of behaviors registered for this table
     *
     * @return array Associative array (name => parameters) of behaviors
     */
    public function getBehaviors()
    {
        return array(
            'timestampable' => array('create_column' => 'created_at', 'update_column' => 'updated_at', 'disable_created_at' => 'false', 'disable_updated_at' => 'false', ),
            'archivable' => array('archive_table' => '', 'archive_phpname' => '', 'archive_class' => '', 'log_archived_at' => 'true', 'archived_at_column' => 'archived_at', 'archive_on_insert' => 'false', 'archive_on_update' => 'false', 'archive_on_delete' => 'true', ),
        );
    } // getBehaviors()
    /**
     * Method to invalidate the instance pool of all tables related to asso_tvas     * by a foreign key with ON DELETE CASCADE
     */
    public static function clearRelatedInstancePool()
    {
        // Invalidate objects in related instance pools,
        // since one or more of them may be deleted by ON DELETE CASCADE/SETNULL rule.
        TvatauxTableMap::clearInstancePool();
    }

    /**
     * Retrieves a string version of the primary key from the DB resultset row that can be used to uniquely identify a row in this table.
     *
     * For tables with a single-column primary key, that simple pkey value will be returned.  For tables with
     * a multi-column primary key, a serialize()d version of the primary key will be returned.
     *
     * @param array  $row       resultset row.
     * @param int    $offset    The 0-based offset for reading from the resultset row.
     * @param string $indexType One of the class type constants TableMap::TYPE_PHPNAME, TableMap::TYPE_CAMELNAME
     *                           TableMap::TYPE_COLNAME, TableMap::TYPE_FIELDNAME, TableMap::TYPE_NUM
     *
     * @return string The primary key hash of the row
     */
    public static function getPrimaryKeyHashFromRow($row, $offset = 0, $indexType = TableMap::TYPE_NUM)
    {
        // If the PK cannot be derived from the row, return NULL.
        if ($row[TableMap::TYPE_NUM == $indexType ? 0 + $offset : static::translateFieldName('IdTva', TableMap::TYPE_PHPNAME, $indexType)] === null) {
            return null;
        }

        return null === $row[TableMap::TYPE_NUM == $indexType ? 0 + $offset : static::translateFieldName('IdTva', TableMap::TYPE_PHPNAME, $indexType)] || is_scalar($row[TableMap::TYPE_NUM == $indexType ? 0 + $offset : static::translateFieldName('IdTva', TableMap::TYPE_PHPNAME, $indexType)]) || is_callable([$row[TableMap::TYPE_NUM == $indexType ? 0 + $offset : static::translateFieldName('IdTva', TableMap::TYPE_PHPNAME, $indexType)], '__toString']) ? (string) $row[TableMap::TYPE_NUM == $indexType ? 0 + $offset : static::translateFieldName('IdTva', TableMap::TYPE_PHPNAME, $indexType)] : $row[TableMap::TYPE_NUM == $indexType ? 0 + $offset : static::translateFieldName('IdTva', TableMap::TYPE_PHPNAME, $indexType)];
    }

    /**
     * Retrieves the primary key from the DB resultset row
     * For tables with a single-column primary key, that simple pkey value will be returned.  For tables with
     * a multi-column primary key, an array of the primary key columns will be returned.
     *
     * @param array  $row       resultset row.
     * @param int    $offset    The 0-based offset for reading from the resultset row.
     * @param string $indexType One of the class type constants TableMap::TYPE_PHPNAME, TableMap::TYPE_CAMELNAME
     *                           TableMap::TYPE_COLNAME, TableMap::TYPE_FIELDNAME, TableMap::TYPE_NUM
     *
     * @return mixed The primary key of the row
     */
    public static function getPrimaryKeyFromRow($row, $offset = 0, $indexType = TableMap::TYPE_NUM)
    {
        return (string) $row[
            $indexType == TableMap::TYPE_NUM
                ? 0 + $offset
                : self::translateFieldName('IdTva', TableMap::TYPE_PHPNAME, $indexType)
        ];
    }

    /**
     * The class that the tableMap will make instances of.
     *
     * If $withPrefix is true, the returned path
     * uses a dot-path notation which is translated into a path
     * relative to a location on the PHP include_path.
     * (e.g. path.to.MyClass -> 'path/to/MyClass.php')
     *
     * @param boolean $withPrefix Whether or not to return the path with the class name
     * @return string path.to.ClassName
     */
    public static function getOMClass($withPrefix = true)
    {
        return $withPrefix ? TvaTableMap::CLASS_DEFAULT : TvaTableMap::OM_CLASS;
    }

    /**
     * Populates an object of the default type or an object that inherit from the default.
     *
     * @param array  $row       row returned by DataFetcher->fetch().
     * @param int    $offset    The 0-based offset for reading from the resultset row.
     * @param string $indexType The index type of $row. Mostly DataFetcher->getIndexType().
                                 One of the class type constants TableMap::TYPE_PHPNAME, TableMap::TYPE_CAMELNAME
     *                           TableMap::TYPE_COLNAME, TableMap::TYPE_FIELDNAME, TableMap::TYPE_NUM.
     *
     * @throws PropelException Any exceptions caught during processing will be
     *                         rethrown wrapped into a PropelException.
     * @return array           (Tva object, last column rank)
     */
    public static function populateObject($row, $offset = 0, $indexType = TableMap::TYPE_NUM)
    {
        $key = TvaTableMap::getPrimaryKeyHashFromRow($row, $offset, $indexType);
        if (null !== ($obj = TvaTableMap::getInstanceFromPool($key))) {
            // We no longer rehydrate the object, since this can cause data loss.
            // See http://www.propelorm.org/ticket/509
            // $obj->hydrate($row, $offset, true); // rehydrate
            $col = $offset + TvaTableMap::NUM_HYDRATE_COLUMNS;
        } else {
            $cls = TvaTableMap::OM_CLASS;
            /** @var Tva $obj */
            $obj = new $cls();
            $col = $obj->hydrate($row, $offset, false, $indexType);
            TvaTableMap::addInstanceToPool($obj, $key);
        }

        return array($obj, $col);
    }

    /**
     * The returned array will contain objects of the default type or
     * objects that inherit from the default.
     *
     * @param DataFetcherInterface $dataFetcher
     * @return array
     * @throws PropelException Any exceptions caught during processing will be
     *                         rethrown wrapped into a PropelException.
     */
    public static function populateObjects(DataFetcherInterface $dataFetcher)
    {
        $results = array();

        // set the class once to avoid overhead in the loop
        $cls = static::getOMClass(false);
        // populate the object(s)
        while ($row = $dataFetcher->fetch()) {
            $key = TvaTableMap::getPrimaryKeyHashFromRow($row, 0, $dataFetcher->getIndexType());
            if (null !== ($obj = TvaTableMap::getInstanceFromPool($key))) {
                // We no longer rehydrate the object, since this can cause data loss.
                // See http://www.propelorm.org/ticket/509
                // $obj->hydrate($row, 0, true); // rehydrate
                $results[] = $obj;
            } else {
                /** @var Tva $obj */
                $obj = new $cls();
                $obj->hydrate($row);
                $results[] = $obj;
                TvaTableMap::addInstanceToPool($obj, $key);
            } // if key exists
        }

        return $results;
    }
    /**
     * Add all the columns needed to create a new object.
     *
     * Note: any columns that were marked with lazyLoad="true" in the
     * XML schema will not be added to the select list and only loaded
     * on demand.
     *
     * @param Criteria $criteria object containing the columns to add.
     * @param string   $alias    optional table alias
     * @throws PropelException Any exceptions caught during processing will be
     *                         rethrown wrapped into a PropelException.
     */
    public static function addSelectColumns(Criteria $criteria, $alias = null)
    {
        if (null === $alias) {
            $criteria->addSelectColumn(TvaTableMap::COL_ID_TVA);
            $criteria->addSelectColumn(TvaTableMap::COL_NOM);
            $criteria->addSelectColumn(TvaTableMap::COL_NOMCOURT);
            $criteria->addSelectColumn(TvaTableMap::COL_ID_COMPTE);
            $criteria->addSelectColumn(TvaTableMap::COL_ACTIF);
            $criteria->addSelectColumn(TvaTableMap::COL_SIGNE);
            $criteria->addSelectColumn(TvaTableMap::COL_ENCAISSEMENT);
            $criteria->addSelectColumn(TvaTableMap::COL_ID_ENTITE);
            $criteria->addSelectColumn(TvaTableMap::COL_OBSERVATION);
            $criteria->addSelectColumn(TvaTableMap::COL_CREATED_AT);
            $criteria->addSelectColumn(TvaTableMap::COL_UPDATED_AT);
        } else {
            $criteria->addSelectColumn($alias . '.id_tva');
            $criteria->addSelectColumn($alias . '.nom');
            $criteria->addSelectColumn($alias . '.nomcourt');
            $criteria->addSelectColumn($alias . '.id_compte');
            $criteria->addSelectColumn($alias . '.actif');
            $criteria->addSelectColumn($alias . '.signe');
            $criteria->addSelectColumn($alias . '.encaissement');
            $criteria->addSelectColumn($alias . '.id_entite');
            $criteria->addSelectColumn($alias . '.observation');
            $criteria->addSelectColumn($alias . '.created_at');
            $criteria->addSelectColumn($alias . '.updated_at');
        }
    }

    /**
     * Returns the TableMap related to this object.
     * This method is not needed for general use but a specific application could have a need.
     * @return TableMap
     * @throws PropelException Any exceptions caught during processing will be
     *                         rethrown wrapped into a PropelException.
     */
    public static function getTableMap()
    {
        return Propel::getServiceContainer()->getDatabaseMap(TvaTableMap::DATABASE_NAME)->getTable(TvaTableMap::TABLE_NAME);
    }

    /**
     * Add a TableMap instance to the database for this tableMap class.
     */
    public static function buildTableMap()
    {
        $dbMap = Propel::getServiceContainer()->getDatabaseMap(TvaTableMap::DATABASE_NAME);
        if (!$dbMap->hasTable(TvaTableMap::TABLE_NAME)) {
            $dbMap->addTableObject(new TvaTableMap());
        }
    }

    /**
     * Performs a DELETE on the database, given a Tva or Criteria object OR a primary key value.
     *
     * @param mixed               $values Criteria or Tva object or primary key or array of primary keys
     *              which is used to create the DELETE statement
     * @param  ConnectionInterface $con the connection to use
     * @return int             The number of affected rows (if supported by underlying database driver).  This includes CASCADE-related rows
     *                         if supported by native driver or if emulated using Propel.
     * @throws PropelException Any exceptions caught during processing will be
     *                         rethrown wrapped into a PropelException.
     */
     public static function doDelete($values, ConnectionInterface $con = null)
     {
        if (null === $con) {
            $con = Propel::getServiceContainer()->getWriteConnection(TvaTableMap::DATABASE_NAME);
        }

        if ($values instanceof Criteria) {
            // rename for clarity
            $criteria = $values;
        } elseif ($values instanceof \Tva) { // it's a model object
            // create criteria based on pk values
            $criteria = $values->buildPkeyCriteria();
        } else { // it's a primary key, or an array of pks
            $criteria = new Criteria(TvaTableMap::DATABASE_NAME);
            $criteria->add(TvaTableMap::COL_ID_TVA, (array) $values, Criteria::IN);
        }

        $query = TvaQuery::create()->mergeWith($criteria);

        if ($values instanceof Criteria) {
            TvaTableMap::clearInstancePool();
        } elseif (!is_object($values)) { // it's a primary key, or an array of pks
            foreach ((array) $values as $singleval) {
                TvaTableMap::removeInstanceFromPool($singleval);
            }
        }

        return $query->delete($con);
    }

    /**
     * Deletes all rows from the asso_tvas table.
     *
     * @param ConnectionInterface $con the connection to use
     * @return int The number of affected rows (if supported by underlying database driver).
     */
    public static function doDeleteAll(ConnectionInterface $con = null)
    {
        return TvaQuery::create()->doDeleteAll($con);
    }

    /**
     * Performs an INSERT on the database, given a Tva or Criteria object.
     *
     * @param mixed               $criteria Criteria or Tva object containing data that is used to create the INSERT statement.
     * @param ConnectionInterface $con the ConnectionInterface connection to use
     * @return mixed           The new primary key.
     * @throws PropelException Any exceptions caught during processing will be
     *                         rethrown wrapped into a PropelException.
     */
    public static function doInsert($criteria, ConnectionInterface $con = null)
    {
        if (null === $con) {
            $con = Propel::getServiceContainer()->getWriteConnection(TvaTableMap::DATABASE_NAME);
        }

        if ($criteria instanceof Criteria) {
            $criteria = clone $criteria; // rename for clarity
        } else {
            $criteria = $criteria->buildCriteria(); // build Criteria from Tva object
        }

        if ($criteria->containsKey(TvaTableMap::COL_ID_TVA) && $criteria->keyContainsValue(TvaTableMap::COL_ID_TVA) ) {
            throw new PropelException('Cannot insert a value for auto-increment primary key ('.TvaTableMap::COL_ID_TVA.')');
        }


        // Set the correct dbName
        $query = TvaQuery::create()->mergeWith($criteria);

        // use transaction because $criteria could contain info
        // for more than one table (I guess, conceivably)
        return $con->transaction(function () use ($con, $query) {
            return $query->doInsert($con);
        });
    }

} // TvaTableMap
// This is the static code needed to register the TableMap for this table with the main Propel class.
//
TvaTableMap::buildTableMap();
