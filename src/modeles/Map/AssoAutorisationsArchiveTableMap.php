<?php

namespace Map;

use \AssoAutorisationsArchive;
use \AssoAutorisationsArchiveQuery;
use Propel\Runtime\Propel;
use Propel\Runtime\ActiveQuery\Criteria;
use Propel\Runtime\ActiveQuery\InstancePoolTrait;
use Propel\Runtime\Connection\ConnectionInterface;
use Propel\Runtime\DataFetcher\DataFetcherInterface;
use Propel\Runtime\Exception\PropelException;
use Propel\Runtime\Map\RelationMap;
use Propel\Runtime\Map\TableMap;
use Propel\Runtime\Map\TableMapTrait;


/**
 * This class defines the structure of the 'asso_autorisations_archive' table.
 *
 *
 *
 * This map class is used by Propel to do runtime db structure discovery.
 * For example, the createSelectSql() method checks the type of a given column used in an
 * ORDER BY clause to know whether it needs to apply SQL to make the ORDER BY case-insensitive
 * (i.e. if it's a text column type).
 *
 */
class AssoAutorisationsArchiveTableMap extends TableMap
{
    use InstancePoolTrait;
    use TableMapTrait;

    /**
     * The (dot-path) name of this class
     */
    const CLASS_NAME = '.Map.AssoAutorisationsArchiveTableMap';

    /**
     * The default database name for this class
     */
    const DATABASE_NAME = 'simplasso';

    /**
     * The table name for this class
     */
    const TABLE_NAME = 'asso_autorisations_archive';

    /**
     * The related Propel class for this table
     */
    const OM_CLASS = '\\AssoAutorisationsArchive';

    /**
     * A class that can be returned by this tableMap
     */
    const CLASS_DEFAULT = 'AssoAutorisationsArchive';

    /**
     * The total number of columns
     */
    const NUM_COLUMNS = 10;

    /**
     * The number of lazy-loaded columns
     */
    const NUM_LAZY_LOAD_COLUMNS = 0;

    /**
     * The number of columns to hydrate (NUM_COLUMNS - NUM_LAZY_LOAD_COLUMNS)
     */
    const NUM_HYDRATE_COLUMNS = 10;

    /**
     * the column name for the id_autorisation field
     */
    const COL_ID_AUTORISATION = 'asso_autorisations_archive.id_autorisation';

    /**
     * the column name for the id_individu field
     */
    const COL_ID_INDIVIDU = 'asso_autorisations_archive.id_individu';

    /**
     * the column name for the profil field
     */
    const COL_PROFIL = 'asso_autorisations_archive.profil';

    /**
     * the column name for the niveau field
     */
    const COL_NIVEAU = 'asso_autorisations_archive.niveau';

    /**
     * the column name for the id_entite field
     */
    const COL_ID_ENTITE = 'asso_autorisations_archive.id_entite';

    /**
     * the column name for the id_restriction field
     */
    const COL_ID_RESTRICTION = 'asso_autorisations_archive.id_restriction';

    /**
     * the column name for the variables field
     */
    const COL_VARIABLES = 'asso_autorisations_archive.variables';

    /**
     * the column name for the created_at field
     */
    const COL_CREATED_AT = 'asso_autorisations_archive.created_at';

    /**
     * the column name for the updated_at field
     */
    const COL_UPDATED_AT = 'asso_autorisations_archive.updated_at';

    /**
     * the column name for the archived_at field
     */
    const COL_ARCHIVED_AT = 'asso_autorisations_archive.archived_at';

    /**
     * The default string format for model objects of the related table
     */
    const DEFAULT_STRING_FORMAT = 'YAML';

    /**
     * holds an array of fieldnames
     *
     * first dimension keys are the type constants
     * e.g. self::$fieldNames[self::TYPE_PHPNAME][0] = 'Id'
     */
    protected static $fieldNames = array (
        self::TYPE_PHPNAME       => array('IdAutorisation', 'IdIndividu', 'Profil', 'Niveau', 'IdEntite', 'IdRestriction', 'Variables', 'CreatedAt', 'UpdatedAt', 'ArchivedAt', ),
        self::TYPE_CAMELNAME     => array('idAutorisation', 'idIndividu', 'profil', 'niveau', 'idEntite', 'idRestriction', 'variables', 'createdAt', 'updatedAt', 'archivedAt', ),
        self::TYPE_COLNAME       => array(AssoAutorisationsArchiveTableMap::COL_ID_AUTORISATION, AssoAutorisationsArchiveTableMap::COL_ID_INDIVIDU, AssoAutorisationsArchiveTableMap::COL_PROFIL, AssoAutorisationsArchiveTableMap::COL_NIVEAU, AssoAutorisationsArchiveTableMap::COL_ID_ENTITE, AssoAutorisationsArchiveTableMap::COL_ID_RESTRICTION, AssoAutorisationsArchiveTableMap::COL_VARIABLES, AssoAutorisationsArchiveTableMap::COL_CREATED_AT, AssoAutorisationsArchiveTableMap::COL_UPDATED_AT, AssoAutorisationsArchiveTableMap::COL_ARCHIVED_AT, ),
        self::TYPE_FIELDNAME     => array('id_autorisation', 'id_individu', 'profil', 'niveau', 'id_entite', 'id_restriction', 'variables', 'created_at', 'updated_at', 'archived_at', ),
        self::TYPE_NUM           => array(0, 1, 2, 3, 4, 5, 6, 7, 8, 9, )
    );

    /**
     * holds an array of keys for quick access to the fieldnames array
     *
     * first dimension keys are the type constants
     * e.g. self::$fieldKeys[self::TYPE_PHPNAME]['Id'] = 0
     */
    protected static $fieldKeys = array (
        self::TYPE_PHPNAME       => array('IdAutorisation' => 0, 'IdIndividu' => 1, 'Profil' => 2, 'Niveau' => 3, 'IdEntite' => 4, 'IdRestriction' => 5, 'Variables' => 6, 'CreatedAt' => 7, 'UpdatedAt' => 8, 'ArchivedAt' => 9, ),
        self::TYPE_CAMELNAME     => array('idAutorisation' => 0, 'idIndividu' => 1, 'profil' => 2, 'niveau' => 3, 'idEntite' => 4, 'idRestriction' => 5, 'variables' => 6, 'createdAt' => 7, 'updatedAt' => 8, 'archivedAt' => 9, ),
        self::TYPE_COLNAME       => array(AssoAutorisationsArchiveTableMap::COL_ID_AUTORISATION => 0, AssoAutorisationsArchiveTableMap::COL_ID_INDIVIDU => 1, AssoAutorisationsArchiveTableMap::COL_PROFIL => 2, AssoAutorisationsArchiveTableMap::COL_NIVEAU => 3, AssoAutorisationsArchiveTableMap::COL_ID_ENTITE => 4, AssoAutorisationsArchiveTableMap::COL_ID_RESTRICTION => 5, AssoAutorisationsArchiveTableMap::COL_VARIABLES => 6, AssoAutorisationsArchiveTableMap::COL_CREATED_AT => 7, AssoAutorisationsArchiveTableMap::COL_UPDATED_AT => 8, AssoAutorisationsArchiveTableMap::COL_ARCHIVED_AT => 9, ),
        self::TYPE_FIELDNAME     => array('id_autorisation' => 0, 'id_individu' => 1, 'profil' => 2, 'niveau' => 3, 'id_entite' => 4, 'id_restriction' => 5, 'variables' => 6, 'created_at' => 7, 'updated_at' => 8, 'archived_at' => 9, ),
        self::TYPE_NUM           => array(0, 1, 2, 3, 4, 5, 6, 7, 8, 9, )
    );

    /**
     * Initialize the table attributes and columns
     * Relations are not initialized by this method since they are lazy loaded
     *
     * @return void
     * @throws PropelException
     */
    public function initialize()
    {
        // attributes
        $this->setName('asso_autorisations_archive');
        $this->setPhpName('AssoAutorisationsArchive');
        $this->setIdentifierQuoting(true);
        $this->setClassName('\\AssoAutorisationsArchive');
        $this->setPackage('');
        $this->setUseIdGenerator(false);
        // columns
        $this->addPrimaryKey('id_autorisation', 'IdAutorisation', 'BIGINT', true, 21, null);
        $this->addColumn('id_individu', 'IdIndividu', 'BIGINT', true, 21, null);
        $this->addColumn('profil', 'Profil', 'VARCHAR', true, 14, null);
        $this->addColumn('niveau', 'Niveau', 'INTEGER', true, 1, 1);
        $this->addColumn('id_entite', 'IdEntite', 'BIGINT', true, 21, 1);
        $this->addColumn('id_restriction', 'IdRestriction', 'INTEGER', false, null, null);
        $this->addColumn('variables', 'Variables', 'CLOB', false, null, null);
        $this->addColumn('created_at', 'CreatedAt', 'TIMESTAMP', false, null, null);
        $this->addColumn('updated_at', 'UpdatedAt', 'TIMESTAMP', false, null, null);
        $this->addColumn('archived_at', 'ArchivedAt', 'TIMESTAMP', false, null, null);
    } // initialize()

    /**
     * Build the RelationMap objects for this table relationships
     */
    public function buildRelations()
    {
    } // buildRelations()

    /**
     * Retrieves a string version of the primary key from the DB resultset row that can be used to uniquely identify a row in this table.
     *
     * For tables with a single-column primary key, that simple pkey value will be returned.  For tables with
     * a multi-column primary key, a serialize()d version of the primary key will be returned.
     *
     * @param array  $row       resultset row.
     * @param int    $offset    The 0-based offset for reading from the resultset row.
     * @param string $indexType One of the class type constants TableMap::TYPE_PHPNAME, TableMap::TYPE_CAMELNAME
     *                           TableMap::TYPE_COLNAME, TableMap::TYPE_FIELDNAME, TableMap::TYPE_NUM
     *
     * @return string The primary key hash of the row
     */
    public static function getPrimaryKeyHashFromRow($row, $offset = 0, $indexType = TableMap::TYPE_NUM)
    {
        // If the PK cannot be derived from the row, return NULL.
        if ($row[TableMap::TYPE_NUM == $indexType ? 0 + $offset : static::translateFieldName('IdAutorisation', TableMap::TYPE_PHPNAME, $indexType)] === null) {
            return null;
        }

        return null === $row[TableMap::TYPE_NUM == $indexType ? 0 + $offset : static::translateFieldName('IdAutorisation', TableMap::TYPE_PHPNAME, $indexType)] || is_scalar($row[TableMap::TYPE_NUM == $indexType ? 0 + $offset : static::translateFieldName('IdAutorisation', TableMap::TYPE_PHPNAME, $indexType)]) || is_callable([$row[TableMap::TYPE_NUM == $indexType ? 0 + $offset : static::translateFieldName('IdAutorisation', TableMap::TYPE_PHPNAME, $indexType)], '__toString']) ? (string) $row[TableMap::TYPE_NUM == $indexType ? 0 + $offset : static::translateFieldName('IdAutorisation', TableMap::TYPE_PHPNAME, $indexType)] : $row[TableMap::TYPE_NUM == $indexType ? 0 + $offset : static::translateFieldName('IdAutorisation', TableMap::TYPE_PHPNAME, $indexType)];
    }

    /**
     * Retrieves the primary key from the DB resultset row
     * For tables with a single-column primary key, that simple pkey value will be returned.  For tables with
     * a multi-column primary key, an array of the primary key columns will be returned.
     *
     * @param array  $row       resultset row.
     * @param int    $offset    The 0-based offset for reading from the resultset row.
     * @param string $indexType One of the class type constants TableMap::TYPE_PHPNAME, TableMap::TYPE_CAMELNAME
     *                           TableMap::TYPE_COLNAME, TableMap::TYPE_FIELDNAME, TableMap::TYPE_NUM
     *
     * @return mixed The primary key of the row
     */
    public static function getPrimaryKeyFromRow($row, $offset = 0, $indexType = TableMap::TYPE_NUM)
    {
        return (string) $row[
            $indexType == TableMap::TYPE_NUM
                ? 0 + $offset
                : self::translateFieldName('IdAutorisation', TableMap::TYPE_PHPNAME, $indexType)
        ];
    }

    /**
     * The class that the tableMap will make instances of.
     *
     * If $withPrefix is true, the returned path
     * uses a dot-path notation which is translated into a path
     * relative to a location on the PHP include_path.
     * (e.g. path.to.MyClass -> 'path/to/MyClass.php')
     *
     * @param boolean $withPrefix Whether or not to return the path with the class name
     * @return string path.to.ClassName
     */
    public static function getOMClass($withPrefix = true)
    {
        return $withPrefix ? AssoAutorisationsArchiveTableMap::CLASS_DEFAULT : AssoAutorisationsArchiveTableMap::OM_CLASS;
    }

    /**
     * Populates an object of the default type or an object that inherit from the default.
     *
     * @param array  $row       row returned by DataFetcher->fetch().
     * @param int    $offset    The 0-based offset for reading from the resultset row.
     * @param string $indexType The index type of $row. Mostly DataFetcher->getIndexType().
                                 One of the class type constants TableMap::TYPE_PHPNAME, TableMap::TYPE_CAMELNAME
     *                           TableMap::TYPE_COLNAME, TableMap::TYPE_FIELDNAME, TableMap::TYPE_NUM.
     *
     * @throws PropelException Any exceptions caught during processing will be
     *                         rethrown wrapped into a PropelException.
     * @return array           (AssoAutorisationsArchive object, last column rank)
     */
    public static function populateObject($row, $offset = 0, $indexType = TableMap::TYPE_NUM)
    {
        $key = AssoAutorisationsArchiveTableMap::getPrimaryKeyHashFromRow($row, $offset, $indexType);
        if (null !== ($obj = AssoAutorisationsArchiveTableMap::getInstanceFromPool($key))) {
            // We no longer rehydrate the object, since this can cause data loss.
            // See http://www.propelorm.org/ticket/509
            // $obj->hydrate($row, $offset, true); // rehydrate
            $col = $offset + AssoAutorisationsArchiveTableMap::NUM_HYDRATE_COLUMNS;
        } else {
            $cls = AssoAutorisationsArchiveTableMap::OM_CLASS;
            /** @var AssoAutorisationsArchive $obj */
            $obj = new $cls();
            $col = $obj->hydrate($row, $offset, false, $indexType);
            AssoAutorisationsArchiveTableMap::addInstanceToPool($obj, $key);
        }

        return array($obj, $col);
    }

    /**
     * The returned array will contain objects of the default type or
     * objects that inherit from the default.
     *
     * @param DataFetcherInterface $dataFetcher
     * @return array
     * @throws PropelException Any exceptions caught during processing will be
     *                         rethrown wrapped into a PropelException.
     */
    public static function populateObjects(DataFetcherInterface $dataFetcher)
    {
        $results = array();

        // set the class once to avoid overhead in the loop
        $cls = static::getOMClass(false);
        // populate the object(s)
        while ($row = $dataFetcher->fetch()) {
            $key = AssoAutorisationsArchiveTableMap::getPrimaryKeyHashFromRow($row, 0, $dataFetcher->getIndexType());
            if (null !== ($obj = AssoAutorisationsArchiveTableMap::getInstanceFromPool($key))) {
                // We no longer rehydrate the object, since this can cause data loss.
                // See http://www.propelorm.org/ticket/509
                // $obj->hydrate($row, 0, true); // rehydrate
                $results[] = $obj;
            } else {
                /** @var AssoAutorisationsArchive $obj */
                $obj = new $cls();
                $obj->hydrate($row);
                $results[] = $obj;
                AssoAutorisationsArchiveTableMap::addInstanceToPool($obj, $key);
            } // if key exists
        }

        return $results;
    }
    /**
     * Add all the columns needed to create a new object.
     *
     * Note: any columns that were marked with lazyLoad="true" in the
     * XML schema will not be added to the select list and only loaded
     * on demand.
     *
     * @param Criteria $criteria object containing the columns to add.
     * @param string   $alias    optional table alias
     * @throws PropelException Any exceptions caught during processing will be
     *                         rethrown wrapped into a PropelException.
     */
    public static function addSelectColumns(Criteria $criteria, $alias = null)
    {
        if (null === $alias) {
            $criteria->addSelectColumn(AssoAutorisationsArchiveTableMap::COL_ID_AUTORISATION);
            $criteria->addSelectColumn(AssoAutorisationsArchiveTableMap::COL_ID_INDIVIDU);
            $criteria->addSelectColumn(AssoAutorisationsArchiveTableMap::COL_PROFIL);
            $criteria->addSelectColumn(AssoAutorisationsArchiveTableMap::COL_NIVEAU);
            $criteria->addSelectColumn(AssoAutorisationsArchiveTableMap::COL_ID_ENTITE);
            $criteria->addSelectColumn(AssoAutorisationsArchiveTableMap::COL_ID_RESTRICTION);
            $criteria->addSelectColumn(AssoAutorisationsArchiveTableMap::COL_VARIABLES);
            $criteria->addSelectColumn(AssoAutorisationsArchiveTableMap::COL_CREATED_AT);
            $criteria->addSelectColumn(AssoAutorisationsArchiveTableMap::COL_UPDATED_AT);
            $criteria->addSelectColumn(AssoAutorisationsArchiveTableMap::COL_ARCHIVED_AT);
        } else {
            $criteria->addSelectColumn($alias . '.id_autorisation');
            $criteria->addSelectColumn($alias . '.id_individu');
            $criteria->addSelectColumn($alias . '.profil');
            $criteria->addSelectColumn($alias . '.niveau');
            $criteria->addSelectColumn($alias . '.id_entite');
            $criteria->addSelectColumn($alias . '.id_restriction');
            $criteria->addSelectColumn($alias . '.variables');
            $criteria->addSelectColumn($alias . '.created_at');
            $criteria->addSelectColumn($alias . '.updated_at');
            $criteria->addSelectColumn($alias . '.archived_at');
        }
    }

    /**
     * Returns the TableMap related to this object.
     * This method is not needed for general use but a specific application could have a need.
     * @return TableMap
     * @throws PropelException Any exceptions caught during processing will be
     *                         rethrown wrapped into a PropelException.
     */
    public static function getTableMap()
    {
        return Propel::getServiceContainer()->getDatabaseMap(AssoAutorisationsArchiveTableMap::DATABASE_NAME)->getTable(AssoAutorisationsArchiveTableMap::TABLE_NAME);
    }

    /**
     * Add a TableMap instance to the database for this tableMap class.
     */
    public static function buildTableMap()
    {
        $dbMap = Propel::getServiceContainer()->getDatabaseMap(AssoAutorisationsArchiveTableMap::DATABASE_NAME);
        if (!$dbMap->hasTable(AssoAutorisationsArchiveTableMap::TABLE_NAME)) {
            $dbMap->addTableObject(new AssoAutorisationsArchiveTableMap());
        }
    }

    /**
     * Performs a DELETE on the database, given a AssoAutorisationsArchive or Criteria object OR a primary key value.
     *
     * @param mixed               $values Criteria or AssoAutorisationsArchive object or primary key or array of primary keys
     *              which is used to create the DELETE statement
     * @param  ConnectionInterface $con the connection to use
     * @return int             The number of affected rows (if supported by underlying database driver).  This includes CASCADE-related rows
     *                         if supported by native driver or if emulated using Propel.
     * @throws PropelException Any exceptions caught during processing will be
     *                         rethrown wrapped into a PropelException.
     */
     public static function doDelete($values, ConnectionInterface $con = null)
     {
        if (null === $con) {
            $con = Propel::getServiceContainer()->getWriteConnection(AssoAutorisationsArchiveTableMap::DATABASE_NAME);
        }

        if ($values instanceof Criteria) {
            // rename for clarity
            $criteria = $values;
        } elseif ($values instanceof \AssoAutorisationsArchive) { // it's a model object
            // create criteria based on pk values
            $criteria = $values->buildPkeyCriteria();
        } else { // it's a primary key, or an array of pks
            $criteria = new Criteria(AssoAutorisationsArchiveTableMap::DATABASE_NAME);
            $criteria->add(AssoAutorisationsArchiveTableMap::COL_ID_AUTORISATION, (array) $values, Criteria::IN);
        }

        $query = AssoAutorisationsArchiveQuery::create()->mergeWith($criteria);

        if ($values instanceof Criteria) {
            AssoAutorisationsArchiveTableMap::clearInstancePool();
        } elseif (!is_object($values)) { // it's a primary key, or an array of pks
            foreach ((array) $values as $singleval) {
                AssoAutorisationsArchiveTableMap::removeInstanceFromPool($singleval);
            }
        }

        return $query->delete($con);
    }

    /**
     * Deletes all rows from the asso_autorisations_archive table.
     *
     * @param ConnectionInterface $con the connection to use
     * @return int The number of affected rows (if supported by underlying database driver).
     */
    public static function doDeleteAll(ConnectionInterface $con = null)
    {
        return AssoAutorisationsArchiveQuery::create()->doDeleteAll($con);
    }

    /**
     * Performs an INSERT on the database, given a AssoAutorisationsArchive or Criteria object.
     *
     * @param mixed               $criteria Criteria or AssoAutorisationsArchive object containing data that is used to create the INSERT statement.
     * @param ConnectionInterface $con the ConnectionInterface connection to use
     * @return mixed           The new primary key.
     * @throws PropelException Any exceptions caught during processing will be
     *                         rethrown wrapped into a PropelException.
     */
    public static function doInsert($criteria, ConnectionInterface $con = null)
    {
        if (null === $con) {
            $con = Propel::getServiceContainer()->getWriteConnection(AssoAutorisationsArchiveTableMap::DATABASE_NAME);
        }

        if ($criteria instanceof Criteria) {
            $criteria = clone $criteria; // rename for clarity
        } else {
            $criteria = $criteria->buildCriteria(); // build Criteria from AssoAutorisationsArchive object
        }


        // Set the correct dbName
        $query = AssoAutorisationsArchiveQuery::create()->mergeWith($criteria);

        // use transaction because $criteria could contain info
        // for more than one table (I guess, conceivably)
        return $con->transaction(function () use ($con, $query) {
            return $query->doInsert($con);
        });
    }

} // AssoAutorisationsArchiveTableMap
// This is the static code needed to register the TableMap for this table with the main Propel class.
//
AssoAutorisationsArchiveTableMap::buildTableMap();
