<?php

namespace Map;

use \Motgroupe;
use \MotgroupeQuery;
use Propel\Runtime\Propel;
use Propel\Runtime\ActiveQuery\Criteria;
use Propel\Runtime\ActiveQuery\InstancePoolTrait;
use Propel\Runtime\Connection\ConnectionInterface;
use Propel\Runtime\DataFetcher\DataFetcherInterface;
use Propel\Runtime\Exception\PropelException;
use Propel\Runtime\Map\RelationMap;
use Propel\Runtime\Map\TableMap;
use Propel\Runtime\Map\TableMapTrait;


/**
 * This class defines the structure of the 'asso_motgroupes' table.
 *
 *
 *
 * This map class is used by Propel to do runtime db structure discovery.
 * For example, the createSelectSql() method checks the type of a given column used in an
 * ORDER BY clause to know whether it needs to apply SQL to make the ORDER BY case-insensitive
 * (i.e. if it's a text column type).
 *
 */
class MotgroupeTableMap extends TableMap
{
    use InstancePoolTrait;
    use TableMapTrait;

    /**
     * The (dot-path) name of this class
     */
    const CLASS_NAME = '.Map.MotgroupeTableMap';

    /**
     * The default database name for this class
     */
    const DATABASE_NAME = 'simplasso';

    /**
     * The table name for this class
     */
    const TABLE_NAME = 'asso_motgroupes';

    /**
     * The related Propel class for this table
     */
    const OM_CLASS = '\\Motgroupe';

    /**
     * A class that can be returned by this tableMap
     */
    const CLASS_DEFAULT = 'Motgroupe';

    /**
     * The total number of columns
     */
    const NUM_COLUMNS = 13;

    /**
     * The number of lazy-loaded columns
     */
    const NUM_LAZY_LOAD_COLUMNS = 0;

    /**
     * The number of columns to hydrate (NUM_COLUMNS - NUM_LAZY_LOAD_COLUMNS)
     */
    const NUM_HYDRATE_COLUMNS = 13;

    /**
     * the column name for the id_motgroupe field
     */
    const COL_ID_MOTGROUPE = 'asso_motgroupes.id_motgroupe';

    /**
     * the column name for the id_parent field
     */
    const COL_ID_PARENT = 'asso_motgroupes.id_parent';

    /**
     * the column name for the nom field
     */
    const COL_NOM = 'asso_motgroupes.nom';

    /**
     * the column name for the nomcourt field
     */
    const COL_NOMCOURT = 'asso_motgroupes.nomcourt';

    /**
     * the column name for the descriptif field
     */
    const COL_DESCRIPTIF = 'asso_motgroupes.descriptif';

    /**
     * the column name for the texte field
     */
    const COL_TEXTE = 'asso_motgroupes.texte';

    /**
     * the column name for the importance field
     */
    const COL_IMPORTANCE = 'asso_motgroupes.importance';

    /**
     * the column name for the objets_en_lien field
     */
    const COL_OBJETS_EN_LIEN = 'asso_motgroupes.objets_en_lien';

    /**
     * the column name for the systeme field
     */
    const COL_SYSTEME = 'asso_motgroupes.systeme';

    /**
     * the column name for the actif field
     */
    const COL_ACTIF = 'asso_motgroupes.actif';

    /**
     * the column name for the options field
     */
    const COL_OPTIONS = 'asso_motgroupes.options';

    /**
     * the column name for the created_at field
     */
    const COL_CREATED_AT = 'asso_motgroupes.created_at';

    /**
     * the column name for the updated_at field
     */
    const COL_UPDATED_AT = 'asso_motgroupes.updated_at';

    /**
     * The default string format for model objects of the related table
     */
    const DEFAULT_STRING_FORMAT = 'YAML';

    /**
     * holds an array of fieldnames
     *
     * first dimension keys are the type constants
     * e.g. self::$fieldNames[self::TYPE_PHPNAME][0] = 'Id'
     */
    protected static $fieldNames = array (
        self::TYPE_PHPNAME       => array('IdMotgroupe', 'IdParent', 'Nom', 'Nomcourt', 'Descriptif', 'Texte', 'Importance', 'ObjetsEnLien', 'Systeme', 'Actif', 'Options', 'CreatedAt', 'UpdatedAt', ),
        self::TYPE_CAMELNAME     => array('idMotgroupe', 'idParent', 'nom', 'nomcourt', 'descriptif', 'texte', 'importance', 'objetsEnLien', 'systeme', 'actif', 'options', 'createdAt', 'updatedAt', ),
        self::TYPE_COLNAME       => array(MotgroupeTableMap::COL_ID_MOTGROUPE, MotgroupeTableMap::COL_ID_PARENT, MotgroupeTableMap::COL_NOM, MotgroupeTableMap::COL_NOMCOURT, MotgroupeTableMap::COL_DESCRIPTIF, MotgroupeTableMap::COL_TEXTE, MotgroupeTableMap::COL_IMPORTANCE, MotgroupeTableMap::COL_OBJETS_EN_LIEN, MotgroupeTableMap::COL_SYSTEME, MotgroupeTableMap::COL_ACTIF, MotgroupeTableMap::COL_OPTIONS, MotgroupeTableMap::COL_CREATED_AT, MotgroupeTableMap::COL_UPDATED_AT, ),
        self::TYPE_FIELDNAME     => array('id_motgroupe', 'id_parent', 'nom', 'nomcourt', 'descriptif', 'texte', 'importance', 'objets_en_lien', 'systeme', 'actif', 'options', 'created_at', 'updated_at', ),
        self::TYPE_NUM           => array(0, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, )
    );

    /**
     * holds an array of keys for quick access to the fieldnames array
     *
     * first dimension keys are the type constants
     * e.g. self::$fieldKeys[self::TYPE_PHPNAME]['Id'] = 0
     */
    protected static $fieldKeys = array (
        self::TYPE_PHPNAME       => array('IdMotgroupe' => 0, 'IdParent' => 1, 'Nom' => 2, 'Nomcourt' => 3, 'Descriptif' => 4, 'Texte' => 5, 'Importance' => 6, 'ObjetsEnLien' => 7, 'Systeme' => 8, 'Actif' => 9, 'Options' => 10, 'CreatedAt' => 11, 'UpdatedAt' => 12, ),
        self::TYPE_CAMELNAME     => array('idMotgroupe' => 0, 'idParent' => 1, 'nom' => 2, 'nomcourt' => 3, 'descriptif' => 4, 'texte' => 5, 'importance' => 6, 'objetsEnLien' => 7, 'systeme' => 8, 'actif' => 9, 'options' => 10, 'createdAt' => 11, 'updatedAt' => 12, ),
        self::TYPE_COLNAME       => array(MotgroupeTableMap::COL_ID_MOTGROUPE => 0, MotgroupeTableMap::COL_ID_PARENT => 1, MotgroupeTableMap::COL_NOM => 2, MotgroupeTableMap::COL_NOMCOURT => 3, MotgroupeTableMap::COL_DESCRIPTIF => 4, MotgroupeTableMap::COL_TEXTE => 5, MotgroupeTableMap::COL_IMPORTANCE => 6, MotgroupeTableMap::COL_OBJETS_EN_LIEN => 7, MotgroupeTableMap::COL_SYSTEME => 8, MotgroupeTableMap::COL_ACTIF => 9, MotgroupeTableMap::COL_OPTIONS => 10, MotgroupeTableMap::COL_CREATED_AT => 11, MotgroupeTableMap::COL_UPDATED_AT => 12, ),
        self::TYPE_FIELDNAME     => array('id_motgroupe' => 0, 'id_parent' => 1, 'nom' => 2, 'nomcourt' => 3, 'descriptif' => 4, 'texte' => 5, 'importance' => 6, 'objets_en_lien' => 7, 'systeme' => 8, 'actif' => 9, 'options' => 10, 'created_at' => 11, 'updated_at' => 12, ),
        self::TYPE_NUM           => array(0, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, )
    );

    /**
     * Initialize the table attributes and columns
     * Relations are not initialized by this method since they are lazy loaded
     *
     * @return void
     * @throws PropelException
     */
    public function initialize()
    {
        // attributes
        $this->setName('asso_motgroupes');
        $this->setPhpName('Motgroupe');
        $this->setIdentifierQuoting(true);
        $this->setClassName('\\Motgroupe');
        $this->setPackage('');
        $this->setUseIdGenerator(true);
        // columns
        $this->addPrimaryKey('id_motgroupe', 'IdMotgroupe', 'BIGINT', true, 21, null);
        $this->addColumn('id_parent', 'IdParent', 'BIGINT', true, 21, 0);
        $this->addColumn('nom', 'Nom', 'VARCHAR', true, 80, null);
        $this->addColumn('nomcourt', 'Nomcourt', 'VARCHAR', false, 12, '');
        $this->addColumn('descriptif', 'Descriptif', 'VARCHAR', false, 255, null);
        $this->addColumn('texte', 'Texte', 'CLOB', false, null, null);
        $this->addColumn('importance', 'Importance', 'INTEGER', true, 1, 1);
        $this->addColumn('objets_en_lien', 'ObjetsEnLien', 'CLOB', true, null, null);
        $this->addColumn('systeme', 'Systeme', 'BOOLEAN', false, 1, false);
        $this->addColumn('actif', 'Actif', 'INTEGER', true, 1, 1);
        $this->addColumn('options', 'Options', 'CLOB', false, null, null);
        $this->addColumn('created_at', 'CreatedAt', 'TIMESTAMP', false, null, null);
        $this->addColumn('updated_at', 'UpdatedAt', 'TIMESTAMP', false, null, null);
    } // initialize()

    /**
     * Build the RelationMap objects for this table relationships
     */
    public function buildRelations()
    {
        $this->addRelation('Mot', '\\Mot', RelationMap::ONE_TO_MANY, array (
  0 =>
  array (
    0 => ':id_motgroupe',
    1 => ':id_motgroupe',
  ),
), 'CASCADE', null, 'Mots', false);
    } // buildRelations()

    /**
     *
     * Gets the list of behaviors registered for this table
     *
     * @return array Associative array (name => parameters) of behaviors
     */
    public function getBehaviors()
    {
        return array(
            'timestampable' => array('create_column' => 'created_at', 'update_column' => 'updated_at', 'disable_created_at' => 'false', 'disable_updated_at' => 'false', ),
            'archivable' => array('archive_table' => '', 'archive_phpname' => '', 'archive_class' => '', 'log_archived_at' => 'true', 'archived_at_column' => 'archived_at', 'archive_on_insert' => 'false', 'archive_on_update' => 'false', 'archive_on_delete' => 'true', ),
        );
    } // getBehaviors()
    /**
     * Method to invalidate the instance pool of all tables related to asso_motgroupes     * by a foreign key with ON DELETE CASCADE
     */
    public static function clearRelatedInstancePool()
    {
        // Invalidate objects in related instance pools,
        // since one or more of them may be deleted by ON DELETE CASCADE/SETNULL rule.
        MotTableMap::clearInstancePool();
    }

    /**
     * Retrieves a string version of the primary key from the DB resultset row that can be used to uniquely identify a row in this table.
     *
     * For tables with a single-column primary key, that simple pkey value will be returned.  For tables with
     * a multi-column primary key, a serialize()d version of the primary key will be returned.
     *
     * @param array  $row       resultset row.
     * @param int    $offset    The 0-based offset for reading from the resultset row.
     * @param string $indexType One of the class type constants TableMap::TYPE_PHPNAME, TableMap::TYPE_CAMELNAME
     *                           TableMap::TYPE_COLNAME, TableMap::TYPE_FIELDNAME, TableMap::TYPE_NUM
     *
     * @return string The primary key hash of the row
     */
    public static function getPrimaryKeyHashFromRow($row, $offset = 0, $indexType = TableMap::TYPE_NUM)
    {
        // If the PK cannot be derived from the row, return NULL.
        if ($row[TableMap::TYPE_NUM == $indexType ? 0 + $offset : static::translateFieldName('IdMotgroupe', TableMap::TYPE_PHPNAME, $indexType)] === null) {
            return null;
        }

        return null === $row[TableMap::TYPE_NUM == $indexType ? 0 + $offset : static::translateFieldName('IdMotgroupe', TableMap::TYPE_PHPNAME, $indexType)] || is_scalar($row[TableMap::TYPE_NUM == $indexType ? 0 + $offset : static::translateFieldName('IdMotgroupe', TableMap::TYPE_PHPNAME, $indexType)]) || is_callable([$row[TableMap::TYPE_NUM == $indexType ? 0 + $offset : static::translateFieldName('IdMotgroupe', TableMap::TYPE_PHPNAME, $indexType)], '__toString']) ? (string) $row[TableMap::TYPE_NUM == $indexType ? 0 + $offset : static::translateFieldName('IdMotgroupe', TableMap::TYPE_PHPNAME, $indexType)] : $row[TableMap::TYPE_NUM == $indexType ? 0 + $offset : static::translateFieldName('IdMotgroupe', TableMap::TYPE_PHPNAME, $indexType)];
    }

    /**
     * Retrieves the primary key from the DB resultset row
     * For tables with a single-column primary key, that simple pkey value will be returned.  For tables with
     * a multi-column primary key, an array of the primary key columns will be returned.
     *
     * @param array  $row       resultset row.
     * @param int    $offset    The 0-based offset for reading from the resultset row.
     * @param string $indexType One of the class type constants TableMap::TYPE_PHPNAME, TableMap::TYPE_CAMELNAME
     *                           TableMap::TYPE_COLNAME, TableMap::TYPE_FIELDNAME, TableMap::TYPE_NUM
     *
     * @return mixed The primary key of the row
     */
    public static function getPrimaryKeyFromRow($row, $offset = 0, $indexType = TableMap::TYPE_NUM)
    {
        return (string) $row[
            $indexType == TableMap::TYPE_NUM
                ? 0 + $offset
                : self::translateFieldName('IdMotgroupe', TableMap::TYPE_PHPNAME, $indexType)
        ];
    }

    /**
     * The class that the tableMap will make instances of.
     *
     * If $withPrefix is true, the returned path
     * uses a dot-path notation which is translated into a path
     * relative to a location on the PHP include_path.
     * (e.g. path.to.MyClass -> 'path/to/MyClass.php')
     *
     * @param boolean $withPrefix Whether or not to return the path with the class name
     * @return string path.to.ClassName
     */
    public static function getOMClass($withPrefix = true)
    {
        return $withPrefix ? MotgroupeTableMap::CLASS_DEFAULT : MotgroupeTableMap::OM_CLASS;
    }

    /**
     * Populates an object of the default type or an object that inherit from the default.
     *
     * @param array  $row       row returned by DataFetcher->fetch().
     * @param int    $offset    The 0-based offset for reading from the resultset row.
     * @param string $indexType The index type of $row. Mostly DataFetcher->getIndexType().
                                 One of the class type constants TableMap::TYPE_PHPNAME, TableMap::TYPE_CAMELNAME
     *                           TableMap::TYPE_COLNAME, TableMap::TYPE_FIELDNAME, TableMap::TYPE_NUM.
     *
     * @throws PropelException Any exceptions caught during processing will be
     *                         rethrown wrapped into a PropelException.
     * @return array           (Motgroupe object, last column rank)
     */
    public static function populateObject($row, $offset = 0, $indexType = TableMap::TYPE_NUM)
    {
        $key = MotgroupeTableMap::getPrimaryKeyHashFromRow($row, $offset, $indexType);
        if (null !== ($obj = MotgroupeTableMap::getInstanceFromPool($key))) {
            // We no longer rehydrate the object, since this can cause data loss.
            // See http://www.propelorm.org/ticket/509
            // $obj->hydrate($row, $offset, true); // rehydrate
            $col = $offset + MotgroupeTableMap::NUM_HYDRATE_COLUMNS;
        } else {
            $cls = MotgroupeTableMap::OM_CLASS;
            /** @var Motgroupe $obj */
            $obj = new $cls();
            $col = $obj->hydrate($row, $offset, false, $indexType);
            MotgroupeTableMap::addInstanceToPool($obj, $key);
        }

        return array($obj, $col);
    }

    /**
     * The returned array will contain objects of the default type or
     * objects that inherit from the default.
     *
     * @param DataFetcherInterface $dataFetcher
     * @return array
     * @throws PropelException Any exceptions caught during processing will be
     *                         rethrown wrapped into a PropelException.
     */
    public static function populateObjects(DataFetcherInterface $dataFetcher)
    {
        $results = array();

        // set the class once to avoid overhead in the loop
        $cls = static::getOMClass(false);
        // populate the object(s)
        while ($row = $dataFetcher->fetch()) {
            $key = MotgroupeTableMap::getPrimaryKeyHashFromRow($row, 0, $dataFetcher->getIndexType());
            if (null !== ($obj = MotgroupeTableMap::getInstanceFromPool($key))) {
                // We no longer rehydrate the object, since this can cause data loss.
                // See http://www.propelorm.org/ticket/509
                // $obj->hydrate($row, 0, true); // rehydrate
                $results[] = $obj;
            } else {
                /** @var Motgroupe $obj */
                $obj = new $cls();
                $obj->hydrate($row);
                $results[] = $obj;
                MotgroupeTableMap::addInstanceToPool($obj, $key);
            } // if key exists
        }

        return $results;
    }
    /**
     * Add all the columns needed to create a new object.
     *
     * Note: any columns that were marked with lazyLoad="true" in the
     * XML schema will not be added to the select list and only loaded
     * on demand.
     *
     * @param Criteria $criteria object containing the columns to add.
     * @param string   $alias    optional table alias
     * @throws PropelException Any exceptions caught during processing will be
     *                         rethrown wrapped into a PropelException.
     */
    public static function addSelectColumns(Criteria $criteria, $alias = null)
    {
        if (null === $alias) {
            $criteria->addSelectColumn(MotgroupeTableMap::COL_ID_MOTGROUPE);
            $criteria->addSelectColumn(MotgroupeTableMap::COL_ID_PARENT);
            $criteria->addSelectColumn(MotgroupeTableMap::COL_NOM);
            $criteria->addSelectColumn(MotgroupeTableMap::COL_NOMCOURT);
            $criteria->addSelectColumn(MotgroupeTableMap::COL_DESCRIPTIF);
            $criteria->addSelectColumn(MotgroupeTableMap::COL_TEXTE);
            $criteria->addSelectColumn(MotgroupeTableMap::COL_IMPORTANCE);
            $criteria->addSelectColumn(MotgroupeTableMap::COL_OBJETS_EN_LIEN);
            $criteria->addSelectColumn(MotgroupeTableMap::COL_SYSTEME);
            $criteria->addSelectColumn(MotgroupeTableMap::COL_ACTIF);
            $criteria->addSelectColumn(MotgroupeTableMap::COL_OPTIONS);
            $criteria->addSelectColumn(MotgroupeTableMap::COL_CREATED_AT);
            $criteria->addSelectColumn(MotgroupeTableMap::COL_UPDATED_AT);
        } else {
            $criteria->addSelectColumn($alias . '.id_motgroupe');
            $criteria->addSelectColumn($alias . '.id_parent');
            $criteria->addSelectColumn($alias . '.nom');
            $criteria->addSelectColumn($alias . '.nomcourt');
            $criteria->addSelectColumn($alias . '.descriptif');
            $criteria->addSelectColumn($alias . '.texte');
            $criteria->addSelectColumn($alias . '.importance');
            $criteria->addSelectColumn($alias . '.objets_en_lien');
            $criteria->addSelectColumn($alias . '.systeme');
            $criteria->addSelectColumn($alias . '.actif');
            $criteria->addSelectColumn($alias . '.options');
            $criteria->addSelectColumn($alias . '.created_at');
            $criteria->addSelectColumn($alias . '.updated_at');
        }
    }

    /**
     * Returns the TableMap related to this object.
     * This method is not needed for general use but a specific application could have a need.
     * @return TableMap
     * @throws PropelException Any exceptions caught during processing will be
     *                         rethrown wrapped into a PropelException.
     */
    public static function getTableMap()
    {
        return Propel::getServiceContainer()->getDatabaseMap(MotgroupeTableMap::DATABASE_NAME)->getTable(MotgroupeTableMap::TABLE_NAME);
    }

    /**
     * Add a TableMap instance to the database for this tableMap class.
     */
    public static function buildTableMap()
    {
        $dbMap = Propel::getServiceContainer()->getDatabaseMap(MotgroupeTableMap::DATABASE_NAME);
        if (!$dbMap->hasTable(MotgroupeTableMap::TABLE_NAME)) {
            $dbMap->addTableObject(new MotgroupeTableMap());
        }
    }

    /**
     * Performs a DELETE on the database, given a Motgroupe or Criteria object OR a primary key value.
     *
     * @param mixed               $values Criteria or Motgroupe object or primary key or array of primary keys
     *              which is used to create the DELETE statement
     * @param  ConnectionInterface $con the connection to use
     * @return int             The number of affected rows (if supported by underlying database driver).  This includes CASCADE-related rows
     *                         if supported by native driver or if emulated using Propel.
     * @throws PropelException Any exceptions caught during processing will be
     *                         rethrown wrapped into a PropelException.
     */
     public static function doDelete($values, ConnectionInterface $con = null)
     {
        if (null === $con) {
            $con = Propel::getServiceContainer()->getWriteConnection(MotgroupeTableMap::DATABASE_NAME);
        }

        if ($values instanceof Criteria) {
            // rename for clarity
            $criteria = $values;
        } elseif ($values instanceof \Motgroupe) { // it's a model object
            // create criteria based on pk values
            $criteria = $values->buildPkeyCriteria();
        } else { // it's a primary key, or an array of pks
            $criteria = new Criteria(MotgroupeTableMap::DATABASE_NAME);
            $criteria->add(MotgroupeTableMap::COL_ID_MOTGROUPE, (array) $values, Criteria::IN);
        }

        $query = MotgroupeQuery::create()->mergeWith($criteria);

        if ($values instanceof Criteria) {
            MotgroupeTableMap::clearInstancePool();
        } elseif (!is_object($values)) { // it's a primary key, or an array of pks
            foreach ((array) $values as $singleval) {
                MotgroupeTableMap::removeInstanceFromPool($singleval);
            }
        }

        return $query->delete($con);
    }

    /**
     * Deletes all rows from the asso_motgroupes table.
     *
     * @param ConnectionInterface $con the connection to use
     * @return int The number of affected rows (if supported by underlying database driver).
     */
    public static function doDeleteAll(ConnectionInterface $con = null)
    {
        return MotgroupeQuery::create()->doDeleteAll($con);
    }

    /**
     * Performs an INSERT on the database, given a Motgroupe or Criteria object.
     *
     * @param mixed               $criteria Criteria or Motgroupe object containing data that is used to create the INSERT statement.
     * @param ConnectionInterface $con the ConnectionInterface connection to use
     * @return mixed           The new primary key.
     * @throws PropelException Any exceptions caught during processing will be
     *                         rethrown wrapped into a PropelException.
     */
    public static function doInsert($criteria, ConnectionInterface $con = null)
    {
        if (null === $con) {
            $con = Propel::getServiceContainer()->getWriteConnection(MotgroupeTableMap::DATABASE_NAME);
        }

        if ($criteria instanceof Criteria) {
            $criteria = clone $criteria; // rename for clarity
        } else {
            $criteria = $criteria->buildCriteria(); // build Criteria from Motgroupe object
        }

        if ($criteria->containsKey(MotgroupeTableMap::COL_ID_MOTGROUPE) && $criteria->keyContainsValue(MotgroupeTableMap::COL_ID_MOTGROUPE) ) {
            throw new PropelException('Cannot insert a value for auto-increment primary key ('.MotgroupeTableMap::COL_ID_MOTGROUPE.')');
        }


        // Set the correct dbName
        $query = MotgroupeQuery::create()->mergeWith($criteria);

        // use transaction because $criteria could contain info
        // for more than one table (I guess, conceivably)
        return $con->transaction(function () use ($con, $query) {
            return $query->doInsert($con);
        });
    }

} // MotgroupeTableMap
// This is the static code needed to register the TableMap for this table with the main Propel class.
//
MotgroupeTableMap::buildTableMap();
