<?php

namespace Map;

use \Ecriture;
use \EcritureQuery;
use Propel\Runtime\Propel;
use Propel\Runtime\ActiveQuery\Criteria;
use Propel\Runtime\ActiveQuery\InstancePoolTrait;
use Propel\Runtime\Connection\ConnectionInterface;
use Propel\Runtime\DataFetcher\DataFetcherInterface;
use Propel\Runtime\Exception\PropelException;
use Propel\Runtime\Map\RelationMap;
use Propel\Runtime\Map\TableMap;
use Propel\Runtime\Map\TableMapTrait;


/**
 * This class defines the structure of the 'assc_ecritures' table.
 *
 *
 *
 * This map class is used by Propel to do runtime db structure discovery.
 * For example, the createSelectSql() method checks the type of a given column used in an
 * ORDER BY clause to know whether it needs to apply SQL to make the ORDER BY case-insensitive
 * (i.e. if it's a text column type).
 *
 */
class EcritureTableMap extends TableMap
{
    use InstancePoolTrait;
    use TableMapTrait;

    /**
     * The (dot-path) name of this class
     */
    const CLASS_NAME = '.Map.EcritureTableMap';

    /**
     * The default database name for this class
     */
    const DATABASE_NAME = 'simplasso';

    /**
     * The table name for this class
     */
    const TABLE_NAME = 'assc_ecritures';

    /**
     * The related Propel class for this table
     */
    const OM_CLASS = '\\Ecriture';

    /**
     * A class that can be returned by this tableMap
     */
    const CLASS_DEFAULT = 'Ecriture';

    /**
     * The total number of columns
     */
    const NUM_COLUMNS = 20;

    /**
     * The number of lazy-loaded columns
     */
    const NUM_LAZY_LOAD_COLUMNS = 0;

    /**
     * The number of columns to hydrate (NUM_COLUMNS - NUM_LAZY_LOAD_COLUMNS)
     */
    const NUM_HYDRATE_COLUMNS = 20;

    /**
     * the column name for the id_ecriture field
     */
    const COL_ID_ECRITURE = 'assc_ecritures.id_ecriture';

    /**
     * the column name for the classement field
     */
    const COL_CLASSEMENT = 'assc_ecritures.classement';

    /**
     * the column name for the credit field
     */
    const COL_CREDIT = 'assc_ecritures.credit';

    /**
     * the column name for the debit field
     */
    const COL_DEBIT = 'assc_ecritures.debit';

    /**
     * the column name for the nom field
     */
    const COL_NOM = 'assc_ecritures.nom';

    /**
     * the column name for the id_compte field
     */
    const COL_ID_COMPTE = 'assc_ecritures.id_compte';

    /**
     * the column name for the id_journal field
     */
    const COL_ID_JOURNAL = 'assc_ecritures.id_journal';

    /**
     * the column name for the id_activite field
     */
    const COL_ID_ACTIVITE = 'assc_ecritures.id_activite';

    /**
     * the column name for the id_entite field
     */
    const COL_ID_ENTITE = 'assc_ecritures.id_entite';

    /**
     * the column name for the lettrage field
     */
    const COL_LETTRAGE = 'assc_ecritures.lettrage';

    /**
     * the column name for the date_lettrage field
     */
    const COL_DATE_LETTRAGE = 'assc_ecritures.date_lettrage';

    /**
     * the column name for the date_ecriture field
     */
    const COL_DATE_ECRITURE = 'assc_ecritures.date_ecriture';

    /**
     * the column name for the etat field
     */
    const COL_ETAT = 'assc_ecritures.etat';

    /**
     * the column name for the observation field
     */
    const COL_OBSERVATION = 'assc_ecritures.observation';

    /**
     * the column name for the id_piece field
     */
    const COL_ID_PIECE = 'assc_ecritures.id_piece';

    /**
     * the column name for the ecran_saisie field
     */
    const COL_ECRAN_SAISIE = 'assc_ecritures.ecran_saisie';

    /**
     * the column name for the contre_partie field
     */
    const COL_CONTRE_PARTIE = 'assc_ecritures.contre_partie';

    /**
     * the column name for the quantite field
     */
    const COL_QUANTITE = 'assc_ecritures.quantite';

    /**
     * the column name for the created_at field
     */
    const COL_CREATED_AT = 'assc_ecritures.created_at';

    /**
     * the column name for the updated_at field
     */
    const COL_UPDATED_AT = 'assc_ecritures.updated_at';

    /**
     * The default string format for model objects of the related table
     */
    const DEFAULT_STRING_FORMAT = 'YAML';

    /**
     * holds an array of fieldnames
     *
     * first dimension keys are the type constants
     * e.g. self::$fieldNames[self::TYPE_PHPNAME][0] = 'Id'
     */
    protected static $fieldNames = array (
        self::TYPE_PHPNAME       => array('IdEcriture', 'Classement', 'Credit', 'Debit', 'Nom', 'IdCompte', 'IdJournal', 'IdActivite', 'IdEntite', 'Lettrage', 'DateLettrage', 'DateEcriture', 'Etat', 'Observation', 'IdPiece', 'EcranSaisie', 'ContrePartie', 'Quantite', 'CreatedAt', 'UpdatedAt', ),
        self::TYPE_CAMELNAME     => array('idEcriture', 'classement', 'credit', 'debit', 'nom', 'idCompte', 'idJournal', 'idActivite', 'idEntite', 'lettrage', 'dateLettrage', 'dateEcriture', 'etat', 'observation', 'idPiece', 'ecranSaisie', 'contrePartie', 'quantite', 'createdAt', 'updatedAt', ),
        self::TYPE_COLNAME       => array(EcritureTableMap::COL_ID_ECRITURE, EcritureTableMap::COL_CLASSEMENT, EcritureTableMap::COL_CREDIT, EcritureTableMap::COL_DEBIT, EcritureTableMap::COL_NOM, EcritureTableMap::COL_ID_COMPTE, EcritureTableMap::COL_ID_JOURNAL, EcritureTableMap::COL_ID_ACTIVITE, EcritureTableMap::COL_ID_ENTITE, EcritureTableMap::COL_LETTRAGE, EcritureTableMap::COL_DATE_LETTRAGE, EcritureTableMap::COL_DATE_ECRITURE, EcritureTableMap::COL_ETAT, EcritureTableMap::COL_OBSERVATION, EcritureTableMap::COL_ID_PIECE, EcritureTableMap::COL_ECRAN_SAISIE, EcritureTableMap::COL_CONTRE_PARTIE, EcritureTableMap::COL_QUANTITE, EcritureTableMap::COL_CREATED_AT, EcritureTableMap::COL_UPDATED_AT, ),
        self::TYPE_FIELDNAME     => array('id_ecriture', 'classement', 'credit', 'debit', 'nom', 'id_compte', 'id_journal', 'id_activite', 'id_entite', 'lettrage', 'date_lettrage', 'date_ecriture', 'etat', 'observation', 'id_piece', 'ecran_saisie', 'contre_partie', 'quantite', 'created_at', 'updated_at', ),
        self::TYPE_NUM           => array(0, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15, 16, 17, 18, 19, )
    );

    /**
     * holds an array of keys for quick access to the fieldnames array
     *
     * first dimension keys are the type constants
     * e.g. self::$fieldKeys[self::TYPE_PHPNAME]['Id'] = 0
     */
    protected static $fieldKeys = array (
        self::TYPE_PHPNAME       => array('IdEcriture' => 0, 'Classement' => 1, 'Credit' => 2, 'Debit' => 3, 'Nom' => 4, 'IdCompte' => 5, 'IdJournal' => 6, 'IdActivite' => 7, 'IdEntite' => 8, 'Lettrage' => 9, 'DateLettrage' => 10, 'DateEcriture' => 11, 'Etat' => 12, 'Observation' => 13, 'IdPiece' => 14, 'EcranSaisie' => 15, 'ContrePartie' => 16, 'Quantite' => 17, 'CreatedAt' => 18, 'UpdatedAt' => 19, ),
        self::TYPE_CAMELNAME     => array('idEcriture' => 0, 'classement' => 1, 'credit' => 2, 'debit' => 3, 'nom' => 4, 'idCompte' => 5, 'idJournal' => 6, 'idActivite' => 7, 'idEntite' => 8, 'lettrage' => 9, 'dateLettrage' => 10, 'dateEcriture' => 11, 'etat' => 12, 'observation' => 13, 'idPiece' => 14, 'ecranSaisie' => 15, 'contrePartie' => 16, 'quantite' => 17, 'createdAt' => 18, 'updatedAt' => 19, ),
        self::TYPE_COLNAME       => array(EcritureTableMap::COL_ID_ECRITURE => 0, EcritureTableMap::COL_CLASSEMENT => 1, EcritureTableMap::COL_CREDIT => 2, EcritureTableMap::COL_DEBIT => 3, EcritureTableMap::COL_NOM => 4, EcritureTableMap::COL_ID_COMPTE => 5, EcritureTableMap::COL_ID_JOURNAL => 6, EcritureTableMap::COL_ID_ACTIVITE => 7, EcritureTableMap::COL_ID_ENTITE => 8, EcritureTableMap::COL_LETTRAGE => 9, EcritureTableMap::COL_DATE_LETTRAGE => 10, EcritureTableMap::COL_DATE_ECRITURE => 11, EcritureTableMap::COL_ETAT => 12, EcritureTableMap::COL_OBSERVATION => 13, EcritureTableMap::COL_ID_PIECE => 14, EcritureTableMap::COL_ECRAN_SAISIE => 15, EcritureTableMap::COL_CONTRE_PARTIE => 16, EcritureTableMap::COL_QUANTITE => 17, EcritureTableMap::COL_CREATED_AT => 18, EcritureTableMap::COL_UPDATED_AT => 19, ),
        self::TYPE_FIELDNAME     => array('id_ecriture' => 0, 'classement' => 1, 'credit' => 2, 'debit' => 3, 'nom' => 4, 'id_compte' => 5, 'id_journal' => 6, 'id_activite' => 7, 'id_entite' => 8, 'lettrage' => 9, 'date_lettrage' => 10, 'date_ecriture' => 11, 'etat' => 12, 'observation' => 13, 'id_piece' => 14, 'ecran_saisie' => 15, 'contre_partie' => 16, 'quantite' => 17, 'created_at' => 18, 'updated_at' => 19, ),
        self::TYPE_NUM           => array(0, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15, 16, 17, 18, 19, )
    );

    /**
     * Initialize the table attributes and columns
     * Relations are not initialized by this method since they are lazy loaded
     *
     * @return void
     * @throws PropelException
     */
    public function initialize()
    {
        // attributes
        $this->setName('assc_ecritures');
        $this->setPhpName('Ecriture');
        $this->setIdentifierQuoting(true);
        $this->setClassName('\\Ecriture');
        $this->setPackage('');
        $this->setUseIdGenerator(true);
        // columns
        $this->addPrimaryKey('id_ecriture', 'IdEcriture', 'BIGINT', true, 21, null);
        $this->addColumn('classement', 'Classement', 'VARCHAR', true, 10, null);
        $this->addColumn('credit', 'Credit', 'DOUBLE', true, 12, 0);
        $this->addColumn('debit', 'Debit', 'DOUBLE', true, 12, 0);
        $this->addColumn('nom', 'Nom', 'VARCHAR', true, 50, null);
        $this->addForeignKey('id_compte', 'IdCompte', 'BIGINT', 'assc_comptes', 'id_compte', true, 21, 1);
        $this->addForeignKey('id_journal', 'IdJournal', 'INTEGER', 'assc_journals', 'id_journal', true, null, 1);
        $this->addForeignKey('id_activite', 'IdActivite', 'BIGINT', 'assc_activites', 'id_activite', false, 21, 1);
        $this->addForeignKey('id_entite', 'IdEntite', 'BIGINT', 'asso_entites', 'id_entite', true, 21, 1);
        $this->addColumn('lettrage', 'Lettrage', 'VARCHAR', false, 10, null);
        $this->addColumn('date_lettrage', 'DateLettrage', 'TIMESTAMP', false, null, null);
        $this->addColumn('date_ecriture', 'DateEcriture', 'TIMESTAMP', true, null, null);
        $this->addColumn('etat', 'Etat', 'INTEGER', false, 2, 2);
        $this->addColumn('observation', 'Observation', 'LONGVARCHAR', false, null, null);
        $this->addColumn('id_piece', 'IdPiece', 'BIGINT', true, 21, 1);
        $this->addColumn('ecran_saisie', 'EcranSaisie', 'VARCHAR', true, 2, 'st');
        $this->addColumn('contre_partie', 'ContrePartie', 'BOOLEAN', true, 1, false);
        $this->addColumn('quantite', 'Quantite', 'DOUBLE', true, null, 0);
        $this->addColumn('created_at', 'CreatedAt', 'TIMESTAMP', false, null, null);
        $this->addColumn('updated_at', 'UpdatedAt', 'TIMESTAMP', false, null, null);
    } // initialize()

    /**
     * Build the RelationMap objects for this table relationships
     */
    public function buildRelations()
    {
        $this->addRelation('Compte', '\\Compte', RelationMap::MANY_TO_ONE, array (
  0 =>
  array (
    0 => ':id_compte',
    1 => ':id_compte',
  ),
), 'CASCADE', null, null, false);
        $this->addRelation('Journal', '\\Journal', RelationMap::MANY_TO_ONE, array (
  0 =>
  array (
    0 => ':id_journal',
    1 => ':id_journal',
  ),
), 'CASCADE', null, null, false);
        $this->addRelation('Activite', '\\Activite', RelationMap::MANY_TO_ONE, array (
  0 =>
  array (
    0 => ':id_activite',
    1 => ':id_activite',
  ),
), 'CASCADE', null, null, false);
        $this->addRelation('Entite', '\\Entite', RelationMap::MANY_TO_ONE, array (
  0 =>
  array (
    0 => ':id_entite',
    1 => ':id_entite',
  ),
), 'CASCADE', null, null, false);
    } // buildRelations()

    /**
     *
     * Gets the list of behaviors registered for this table
     *
     * @return array Associative array (name => parameters) of behaviors
     */
    public function getBehaviors()
    {
        return array(
            'timestampable' => array('create_column' => 'created_at', 'update_column' => 'updated_at', 'disable_created_at' => 'false', 'disable_updated_at' => 'false', ),
            'archivable' => array('archive_table' => '', 'archive_phpname' => '', 'archive_class' => '', 'log_archived_at' => 'true', 'archived_at_column' => 'archived_at', 'archive_on_insert' => 'false', 'archive_on_update' => 'false', 'archive_on_delete' => 'true', ),
        );
    } // getBehaviors()

    /**
     * Retrieves a string version of the primary key from the DB resultset row that can be used to uniquely identify a row in this table.
     *
     * For tables with a single-column primary key, that simple pkey value will be returned.  For tables with
     * a multi-column primary key, a serialize()d version of the primary key will be returned.
     *
     * @param array  $row       resultset row.
     * @param int    $offset    The 0-based offset for reading from the resultset row.
     * @param string $indexType One of the class type constants TableMap::TYPE_PHPNAME, TableMap::TYPE_CAMELNAME
     *                           TableMap::TYPE_COLNAME, TableMap::TYPE_FIELDNAME, TableMap::TYPE_NUM
     *
     * @return string The primary key hash of the row
     */
    public static function getPrimaryKeyHashFromRow($row, $offset = 0, $indexType = TableMap::TYPE_NUM)
    {
        // If the PK cannot be derived from the row, return NULL.
        if ($row[TableMap::TYPE_NUM == $indexType ? 0 + $offset : static::translateFieldName('IdEcriture', TableMap::TYPE_PHPNAME, $indexType)] === null) {
            return null;
        }

        return null === $row[TableMap::TYPE_NUM == $indexType ? 0 + $offset : static::translateFieldName('IdEcriture', TableMap::TYPE_PHPNAME, $indexType)] || is_scalar($row[TableMap::TYPE_NUM == $indexType ? 0 + $offset : static::translateFieldName('IdEcriture', TableMap::TYPE_PHPNAME, $indexType)]) || is_callable([$row[TableMap::TYPE_NUM == $indexType ? 0 + $offset : static::translateFieldName('IdEcriture', TableMap::TYPE_PHPNAME, $indexType)], '__toString']) ? (string) $row[TableMap::TYPE_NUM == $indexType ? 0 + $offset : static::translateFieldName('IdEcriture', TableMap::TYPE_PHPNAME, $indexType)] : $row[TableMap::TYPE_NUM == $indexType ? 0 + $offset : static::translateFieldName('IdEcriture', TableMap::TYPE_PHPNAME, $indexType)];
    }

    /**
     * Retrieves the primary key from the DB resultset row
     * For tables with a single-column primary key, that simple pkey value will be returned.  For tables with
     * a multi-column primary key, an array of the primary key columns will be returned.
     *
     * @param array  $row       resultset row.
     * @param int    $offset    The 0-based offset for reading from the resultset row.
     * @param string $indexType One of the class type constants TableMap::TYPE_PHPNAME, TableMap::TYPE_CAMELNAME
     *                           TableMap::TYPE_COLNAME, TableMap::TYPE_FIELDNAME, TableMap::TYPE_NUM
     *
     * @return mixed The primary key of the row
     */
    public static function getPrimaryKeyFromRow($row, $offset = 0, $indexType = TableMap::TYPE_NUM)
    {
        return (string) $row[
            $indexType == TableMap::TYPE_NUM
                ? 0 + $offset
                : self::translateFieldName('IdEcriture', TableMap::TYPE_PHPNAME, $indexType)
        ];
    }

    /**
     * The class that the tableMap will make instances of.
     *
     * If $withPrefix is true, the returned path
     * uses a dot-path notation which is translated into a path
     * relative to a location on the PHP include_path.
     * (e.g. path.to.MyClass -> 'path/to/MyClass.php')
     *
     * @param boolean $withPrefix Whether or not to return the path with the class name
     * @return string path.to.ClassName
     */
    public static function getOMClass($withPrefix = true)
    {
        return $withPrefix ? EcritureTableMap::CLASS_DEFAULT : EcritureTableMap::OM_CLASS;
    }

    /**
     * Populates an object of the default type or an object that inherit from the default.
     *
     * @param array  $row       row returned by DataFetcher->fetch().
     * @param int    $offset    The 0-based offset for reading from the resultset row.
     * @param string $indexType The index type of $row. Mostly DataFetcher->getIndexType().
                                 One of the class type constants TableMap::TYPE_PHPNAME, TableMap::TYPE_CAMELNAME
     *                           TableMap::TYPE_COLNAME, TableMap::TYPE_FIELDNAME, TableMap::TYPE_NUM.
     *
     * @throws PropelException Any exceptions caught during processing will be
     *                         rethrown wrapped into a PropelException.
     * @return array           (Ecriture object, last column rank)
     */
    public static function populateObject($row, $offset = 0, $indexType = TableMap::TYPE_NUM)
    {
        $key = EcritureTableMap::getPrimaryKeyHashFromRow($row, $offset, $indexType);
        if (null !== ($obj = EcritureTableMap::getInstanceFromPool($key))) {
            // We no longer rehydrate the object, since this can cause data loss.
            // See http://www.propelorm.org/ticket/509
            // $obj->hydrate($row, $offset, true); // rehydrate
            $col = $offset + EcritureTableMap::NUM_HYDRATE_COLUMNS;
        } else {
            $cls = EcritureTableMap::OM_CLASS;
            /** @var Ecriture $obj */
            $obj = new $cls();
            $col = $obj->hydrate($row, $offset, false, $indexType);
            EcritureTableMap::addInstanceToPool($obj, $key);
        }

        return array($obj, $col);
    }

    /**
     * The returned array will contain objects of the default type or
     * objects that inherit from the default.
     *
     * @param DataFetcherInterface $dataFetcher
     * @return array
     * @throws PropelException Any exceptions caught during processing will be
     *                         rethrown wrapped into a PropelException.
     */
    public static function populateObjects(DataFetcherInterface $dataFetcher)
    {
        $results = array();

        // set the class once to avoid overhead in the loop
        $cls = static::getOMClass(false);
        // populate the object(s)
        while ($row = $dataFetcher->fetch()) {
            $key = EcritureTableMap::getPrimaryKeyHashFromRow($row, 0, $dataFetcher->getIndexType());
            if (null !== ($obj = EcritureTableMap::getInstanceFromPool($key))) {
                // We no longer rehydrate the object, since this can cause data loss.
                // See http://www.propelorm.org/ticket/509
                // $obj->hydrate($row, 0, true); // rehydrate
                $results[] = $obj;
            } else {
                /** @var Ecriture $obj */
                $obj = new $cls();
                $obj->hydrate($row);
                $results[] = $obj;
                EcritureTableMap::addInstanceToPool($obj, $key);
            } // if key exists
        }

        return $results;
    }
    /**
     * Add all the columns needed to create a new object.
     *
     * Note: any columns that were marked with lazyLoad="true" in the
     * XML schema will not be added to the select list and only loaded
     * on demand.
     *
     * @param Criteria $criteria object containing the columns to add.
     * @param string   $alias    optional table alias
     * @throws PropelException Any exceptions caught during processing will be
     *                         rethrown wrapped into a PropelException.
     */
    public static function addSelectColumns(Criteria $criteria, $alias = null)
    {
        if (null === $alias) {
            $criteria->addSelectColumn(EcritureTableMap::COL_ID_ECRITURE);
            $criteria->addSelectColumn(EcritureTableMap::COL_CLASSEMENT);
            $criteria->addSelectColumn(EcritureTableMap::COL_CREDIT);
            $criteria->addSelectColumn(EcritureTableMap::COL_DEBIT);
            $criteria->addSelectColumn(EcritureTableMap::COL_NOM);
            $criteria->addSelectColumn(EcritureTableMap::COL_ID_COMPTE);
            $criteria->addSelectColumn(EcritureTableMap::COL_ID_JOURNAL);
            $criteria->addSelectColumn(EcritureTableMap::COL_ID_ACTIVITE);
            $criteria->addSelectColumn(EcritureTableMap::COL_ID_ENTITE);
            $criteria->addSelectColumn(EcritureTableMap::COL_LETTRAGE);
            $criteria->addSelectColumn(EcritureTableMap::COL_DATE_LETTRAGE);
            $criteria->addSelectColumn(EcritureTableMap::COL_DATE_ECRITURE);
            $criteria->addSelectColumn(EcritureTableMap::COL_ETAT);
            $criteria->addSelectColumn(EcritureTableMap::COL_OBSERVATION);
            $criteria->addSelectColumn(EcritureTableMap::COL_ID_PIECE);
            $criteria->addSelectColumn(EcritureTableMap::COL_ECRAN_SAISIE);
            $criteria->addSelectColumn(EcritureTableMap::COL_CONTRE_PARTIE);
            $criteria->addSelectColumn(EcritureTableMap::COL_QUANTITE);
            $criteria->addSelectColumn(EcritureTableMap::COL_CREATED_AT);
            $criteria->addSelectColumn(EcritureTableMap::COL_UPDATED_AT);
        } else {
            $criteria->addSelectColumn($alias . '.id_ecriture');
            $criteria->addSelectColumn($alias . '.classement');
            $criteria->addSelectColumn($alias . '.credit');
            $criteria->addSelectColumn($alias . '.debit');
            $criteria->addSelectColumn($alias . '.nom');
            $criteria->addSelectColumn($alias . '.id_compte');
            $criteria->addSelectColumn($alias . '.id_journal');
            $criteria->addSelectColumn($alias . '.id_activite');
            $criteria->addSelectColumn($alias . '.id_entite');
            $criteria->addSelectColumn($alias . '.lettrage');
            $criteria->addSelectColumn($alias . '.date_lettrage');
            $criteria->addSelectColumn($alias . '.date_ecriture');
            $criteria->addSelectColumn($alias . '.etat');
            $criteria->addSelectColumn($alias . '.observation');
            $criteria->addSelectColumn($alias . '.id_piece');
            $criteria->addSelectColumn($alias . '.ecran_saisie');
            $criteria->addSelectColumn($alias . '.contre_partie');
            $criteria->addSelectColumn($alias . '.quantite');
            $criteria->addSelectColumn($alias . '.created_at');
            $criteria->addSelectColumn($alias . '.updated_at');
        }
    }

    /**
     * Returns the TableMap related to this object.
     * This method is not needed for general use but a specific application could have a need.
     * @return TableMap
     * @throws PropelException Any exceptions caught during processing will be
     *                         rethrown wrapped into a PropelException.
     */
    public static function getTableMap()
    {
        return Propel::getServiceContainer()->getDatabaseMap(EcritureTableMap::DATABASE_NAME)->getTable(EcritureTableMap::TABLE_NAME);
    }

    /**
     * Add a TableMap instance to the database for this tableMap class.
     */
    public static function buildTableMap()
    {
        $dbMap = Propel::getServiceContainer()->getDatabaseMap(EcritureTableMap::DATABASE_NAME);
        if (!$dbMap->hasTable(EcritureTableMap::TABLE_NAME)) {
            $dbMap->addTableObject(new EcritureTableMap());
        }
    }

    /**
     * Performs a DELETE on the database, given a Ecriture or Criteria object OR a primary key value.
     *
     * @param mixed               $values Criteria or Ecriture object or primary key or array of primary keys
     *              which is used to create the DELETE statement
     * @param  ConnectionInterface $con the connection to use
     * @return int             The number of affected rows (if supported by underlying database driver).  This includes CASCADE-related rows
     *                         if supported by native driver or if emulated using Propel.
     * @throws PropelException Any exceptions caught during processing will be
     *                         rethrown wrapped into a PropelException.
     */
     public static function doDelete($values, ConnectionInterface $con = null)
     {
        if (null === $con) {
            $con = Propel::getServiceContainer()->getWriteConnection(EcritureTableMap::DATABASE_NAME);
        }

        if ($values instanceof Criteria) {
            // rename for clarity
            $criteria = $values;
        } elseif ($values instanceof \Ecriture) { // it's a model object
            // create criteria based on pk values
            $criteria = $values->buildPkeyCriteria();
        } else { // it's a primary key, or an array of pks
            $criteria = new Criteria(EcritureTableMap::DATABASE_NAME);
            $criteria->add(EcritureTableMap::COL_ID_ECRITURE, (array) $values, Criteria::IN);
        }

        $query = EcritureQuery::create()->mergeWith($criteria);

        if ($values instanceof Criteria) {
            EcritureTableMap::clearInstancePool();
        } elseif (!is_object($values)) { // it's a primary key, or an array of pks
            foreach ((array) $values as $singleval) {
                EcritureTableMap::removeInstanceFromPool($singleval);
            }
        }

        return $query->delete($con);
    }

    /**
     * Deletes all rows from the assc_ecritures table.
     *
     * @param ConnectionInterface $con the connection to use
     * @return int The number of affected rows (if supported by underlying database driver).
     */
    public static function doDeleteAll(ConnectionInterface $con = null)
    {
        return EcritureQuery::create()->doDeleteAll($con);
    }

    /**
     * Performs an INSERT on the database, given a Ecriture or Criteria object.
     *
     * @param mixed               $criteria Criteria or Ecriture object containing data that is used to create the INSERT statement.
     * @param ConnectionInterface $con the ConnectionInterface connection to use
     * @return mixed           The new primary key.
     * @throws PropelException Any exceptions caught during processing will be
     *                         rethrown wrapped into a PropelException.
     */
    public static function doInsert($criteria, ConnectionInterface $con = null)
    {
        if (null === $con) {
            $con = Propel::getServiceContainer()->getWriteConnection(EcritureTableMap::DATABASE_NAME);
        }

        if ($criteria instanceof Criteria) {
            $criteria = clone $criteria; // rename for clarity
        } else {
            $criteria = $criteria->buildCriteria(); // build Criteria from Ecriture object
        }

        if ($criteria->containsKey(EcritureTableMap::COL_ID_ECRITURE) && $criteria->keyContainsValue(EcritureTableMap::COL_ID_ECRITURE) ) {
            throw new PropelException('Cannot insert a value for auto-increment primary key ('.EcritureTableMap::COL_ID_ECRITURE.')');
        }


        // Set the correct dbName
        $query = EcritureQuery::create()->mergeWith($criteria);

        // use transaction because $criteria could contain info
        // for more than one table (I guess, conceivably)
        return $con->transaction(function () use ($con, $query) {
            return $query->doInsert($con);
        });
    }

} // EcritureTableMap
// This is the static code needed to register the TableMap for this table with the main Propel class.
//
EcritureTableMap::buildTableMap();
