<?php

namespace Map;

use \AssoMotsArchive;
use \AssoMotsArchiveQuery;
use Propel\Runtime\Propel;
use Propel\Runtime\ActiveQuery\Criteria;
use Propel\Runtime\ActiveQuery\InstancePoolTrait;
use Propel\Runtime\Connection\ConnectionInterface;
use Propel\Runtime\DataFetcher\DataFetcherInterface;
use Propel\Runtime\Exception\PropelException;
use Propel\Runtime\Map\RelationMap;
use Propel\Runtime\Map\TableMap;
use Propel\Runtime\Map\TableMapTrait;


/**
 * This class defines the structure of the 'asso_mots_archive' table.
 *
 *
 *
 * This map class is used by Propel to do runtime db structure discovery.
 * For example, the createSelectSql() method checks the type of a given column used in an
 * ORDER BY clause to know whether it needs to apply SQL to make the ORDER BY case-insensitive
 * (i.e. if it's a text column type).
 *
 */
class AssoMotsArchiveTableMap extends TableMap
{
    use InstancePoolTrait;
    use TableMapTrait;

    /**
     * The (dot-path) name of this class
     */
    const CLASS_NAME = '.Map.AssoMotsArchiveTableMap';

    /**
     * The default database name for this class
     */
    const DATABASE_NAME = 'simplasso';

    /**
     * The table name for this class
     */
    const TABLE_NAME = 'asso_mots_archive';

    /**
     * The related Propel class for this table
     */
    const OM_CLASS = '\\AssoMotsArchive';

    /**
     * A class that can be returned by this tableMap
     */
    const CLASS_DEFAULT = 'AssoMotsArchive';

    /**
     * The total number of columns
     */
    const NUM_COLUMNS = 11;

    /**
     * The number of lazy-loaded columns
     */
    const NUM_LAZY_LOAD_COLUMNS = 0;

    /**
     * The number of columns to hydrate (NUM_COLUMNS - NUM_LAZY_LOAD_COLUMNS)
     */
    const NUM_HYDRATE_COLUMNS = 11;

    /**
     * the column name for the id_mot field
     */
    const COL_ID_MOT = 'asso_mots_archive.id_mot';

    /**
     * the column name for the nom field
     */
    const COL_NOM = 'asso_mots_archive.nom';

    /**
     * the column name for the nomcourt field
     */
    const COL_NOMCOURT = 'asso_mots_archive.nomcourt';

    /**
     * the column name for the descriptif field
     */
    const COL_DESCRIPTIF = 'asso_mots_archive.descriptif';

    /**
     * the column name for the texte field
     */
    const COL_TEXTE = 'asso_mots_archive.texte';

    /**
     * the column name for the importance field
     */
    const COL_IMPORTANCE = 'asso_mots_archive.importance';

    /**
     * the column name for the actif field
     */
    const COL_ACTIF = 'asso_mots_archive.actif';

    /**
     * the column name for the id_motgroupe field
     */
    const COL_ID_MOTGROUPE = 'asso_mots_archive.id_motgroupe';

    /**
     * the column name for the created_at field
     */
    const COL_CREATED_AT = 'asso_mots_archive.created_at';

    /**
     * the column name for the updated_at field
     */
    const COL_UPDATED_AT = 'asso_mots_archive.updated_at';

    /**
     * the column name for the archived_at field
     */
    const COL_ARCHIVED_AT = 'asso_mots_archive.archived_at';

    /**
     * The default string format for model objects of the related table
     */
    const DEFAULT_STRING_FORMAT = 'YAML';

    /**
     * holds an array of fieldnames
     *
     * first dimension keys are the type constants
     * e.g. self::$fieldNames[self::TYPE_PHPNAME][0] = 'Id'
     */
    protected static $fieldNames = array (
        self::TYPE_PHPNAME       => array('IdMot', 'Nom', 'Nomcourt', 'Descriptif', 'Texte', 'Importance', 'Actif', 'IdMotgroupe', 'CreatedAt', 'UpdatedAt', 'ArchivedAt', ),
        self::TYPE_CAMELNAME     => array('idMot', 'nom', 'nomcourt', 'descriptif', 'texte', 'importance', 'actif', 'idMotgroupe', 'createdAt', 'updatedAt', 'archivedAt', ),
        self::TYPE_COLNAME       => array(AssoMotsArchiveTableMap::COL_ID_MOT, AssoMotsArchiveTableMap::COL_NOM, AssoMotsArchiveTableMap::COL_NOMCOURT, AssoMotsArchiveTableMap::COL_DESCRIPTIF, AssoMotsArchiveTableMap::COL_TEXTE, AssoMotsArchiveTableMap::COL_IMPORTANCE, AssoMotsArchiveTableMap::COL_ACTIF, AssoMotsArchiveTableMap::COL_ID_MOTGROUPE, AssoMotsArchiveTableMap::COL_CREATED_AT, AssoMotsArchiveTableMap::COL_UPDATED_AT, AssoMotsArchiveTableMap::COL_ARCHIVED_AT, ),
        self::TYPE_FIELDNAME     => array('id_mot', 'nom', 'nomcourt', 'descriptif', 'texte', 'importance', 'actif', 'id_motgroupe', 'created_at', 'updated_at', 'archived_at', ),
        self::TYPE_NUM           => array(0, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, )
    );

    /**
     * holds an array of keys for quick access to the fieldnames array
     *
     * first dimension keys are the type constants
     * e.g. self::$fieldKeys[self::TYPE_PHPNAME]['Id'] = 0
     */
    protected static $fieldKeys = array (
        self::TYPE_PHPNAME       => array('IdMot' => 0, 'Nom' => 1, 'Nomcourt' => 2, 'Descriptif' => 3, 'Texte' => 4, 'Importance' => 5, 'Actif' => 6, 'IdMotgroupe' => 7, 'CreatedAt' => 8, 'UpdatedAt' => 9, 'ArchivedAt' => 10, ),
        self::TYPE_CAMELNAME     => array('idMot' => 0, 'nom' => 1, 'nomcourt' => 2, 'descriptif' => 3, 'texte' => 4, 'importance' => 5, 'actif' => 6, 'idMotgroupe' => 7, 'createdAt' => 8, 'updatedAt' => 9, 'archivedAt' => 10, ),
        self::TYPE_COLNAME       => array(AssoMotsArchiveTableMap::COL_ID_MOT => 0, AssoMotsArchiveTableMap::COL_NOM => 1, AssoMotsArchiveTableMap::COL_NOMCOURT => 2, AssoMotsArchiveTableMap::COL_DESCRIPTIF => 3, AssoMotsArchiveTableMap::COL_TEXTE => 4, AssoMotsArchiveTableMap::COL_IMPORTANCE => 5, AssoMotsArchiveTableMap::COL_ACTIF => 6, AssoMotsArchiveTableMap::COL_ID_MOTGROUPE => 7, AssoMotsArchiveTableMap::COL_CREATED_AT => 8, AssoMotsArchiveTableMap::COL_UPDATED_AT => 9, AssoMotsArchiveTableMap::COL_ARCHIVED_AT => 10, ),
        self::TYPE_FIELDNAME     => array('id_mot' => 0, 'nom' => 1, 'nomcourt' => 2, 'descriptif' => 3, 'texte' => 4, 'importance' => 5, 'actif' => 6, 'id_motgroupe' => 7, 'created_at' => 8, 'updated_at' => 9, 'archived_at' => 10, ),
        self::TYPE_NUM           => array(0, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, )
    );

    /**
     * Initialize the table attributes and columns
     * Relations are not initialized by this method since they are lazy loaded
     *
     * @return void
     * @throws PropelException
     */
    public function initialize()
    {
        // attributes
        $this->setName('asso_mots_archive');
        $this->setPhpName('AssoMotsArchive');
        $this->setIdentifierQuoting(true);
        $this->setClassName('\\AssoMotsArchive');
        $this->setPackage('');
        $this->setUseIdGenerator(false);
        // columns
        $this->addPrimaryKey('id_mot', 'IdMot', 'BIGINT', true, 21, null);
        $this->addColumn('nom', 'Nom', 'VARCHAR', true, 40, null);
        $this->addColumn('nomcourt', 'Nomcourt', 'VARCHAR', false, 10, null);
        $this->addColumn('descriptif', 'Descriptif', 'VARCHAR', false, 255, null);
        $this->addColumn('texte', 'Texte', 'CLOB', false, null, null);
        $this->addColumn('importance', 'Importance', 'INTEGER', true, 1, 1);
        $this->addColumn('actif', 'Actif', 'INTEGER', true, 1, 1);
        $this->addColumn('id_motgroupe', 'IdMotgroupe', 'BIGINT', true, 21, 0);
        $this->addColumn('created_at', 'CreatedAt', 'TIMESTAMP', false, null, null);
        $this->addColumn('updated_at', 'UpdatedAt', 'TIMESTAMP', false, null, null);
        $this->addColumn('archived_at', 'ArchivedAt', 'TIMESTAMP', false, null, null);
    } // initialize()

    /**
     * Build the RelationMap objects for this table relationships
     */
    public function buildRelations()
    {
    } // buildRelations()

    /**
     * Retrieves a string version of the primary key from the DB resultset row that can be used to uniquely identify a row in this table.
     *
     * For tables with a single-column primary key, that simple pkey value will be returned.  For tables with
     * a multi-column primary key, a serialize()d version of the primary key will be returned.
     *
     * @param array  $row       resultset row.
     * @param int    $offset    The 0-based offset for reading from the resultset row.
     * @param string $indexType One of the class type constants TableMap::TYPE_PHPNAME, TableMap::TYPE_CAMELNAME
     *                           TableMap::TYPE_COLNAME, TableMap::TYPE_FIELDNAME, TableMap::TYPE_NUM
     *
     * @return string The primary key hash of the row
     */
    public static function getPrimaryKeyHashFromRow($row, $offset = 0, $indexType = TableMap::TYPE_NUM)
    {
        // If the PK cannot be derived from the row, return NULL.
        if ($row[TableMap::TYPE_NUM == $indexType ? 0 + $offset : static::translateFieldName('IdMot', TableMap::TYPE_PHPNAME, $indexType)] === null) {
            return null;
        }

        return null === $row[TableMap::TYPE_NUM == $indexType ? 0 + $offset : static::translateFieldName('IdMot', TableMap::TYPE_PHPNAME, $indexType)] || is_scalar($row[TableMap::TYPE_NUM == $indexType ? 0 + $offset : static::translateFieldName('IdMot', TableMap::TYPE_PHPNAME, $indexType)]) || is_callable([$row[TableMap::TYPE_NUM == $indexType ? 0 + $offset : static::translateFieldName('IdMot', TableMap::TYPE_PHPNAME, $indexType)], '__toString']) ? (string) $row[TableMap::TYPE_NUM == $indexType ? 0 + $offset : static::translateFieldName('IdMot', TableMap::TYPE_PHPNAME, $indexType)] : $row[TableMap::TYPE_NUM == $indexType ? 0 + $offset : static::translateFieldName('IdMot', TableMap::TYPE_PHPNAME, $indexType)];
    }

    /**
     * Retrieves the primary key from the DB resultset row
     * For tables with a single-column primary key, that simple pkey value will be returned.  For tables with
     * a multi-column primary key, an array of the primary key columns will be returned.
     *
     * @param array  $row       resultset row.
     * @param int    $offset    The 0-based offset for reading from the resultset row.
     * @param string $indexType One of the class type constants TableMap::TYPE_PHPNAME, TableMap::TYPE_CAMELNAME
     *                           TableMap::TYPE_COLNAME, TableMap::TYPE_FIELDNAME, TableMap::TYPE_NUM
     *
     * @return mixed The primary key of the row
     */
    public static function getPrimaryKeyFromRow($row, $offset = 0, $indexType = TableMap::TYPE_NUM)
    {
        return (string) $row[
            $indexType == TableMap::TYPE_NUM
                ? 0 + $offset
                : self::translateFieldName('IdMot', TableMap::TYPE_PHPNAME, $indexType)
        ];
    }

    /**
     * The class that the tableMap will make instances of.
     *
     * If $withPrefix is true, the returned path
     * uses a dot-path notation which is translated into a path
     * relative to a location on the PHP include_path.
     * (e.g. path.to.MyClass -> 'path/to/MyClass.php')
     *
     * @param boolean $withPrefix Whether or not to return the path with the class name
     * @return string path.to.ClassName
     */
    public static function getOMClass($withPrefix = true)
    {
        return $withPrefix ? AssoMotsArchiveTableMap::CLASS_DEFAULT : AssoMotsArchiveTableMap::OM_CLASS;
    }

    /**
     * Populates an object of the default type or an object that inherit from the default.
     *
     * @param array  $row       row returned by DataFetcher->fetch().
     * @param int    $offset    The 0-based offset for reading from the resultset row.
     * @param string $indexType The index type of $row. Mostly DataFetcher->getIndexType().
                                 One of the class type constants TableMap::TYPE_PHPNAME, TableMap::TYPE_CAMELNAME
     *                           TableMap::TYPE_COLNAME, TableMap::TYPE_FIELDNAME, TableMap::TYPE_NUM.
     *
     * @throws PropelException Any exceptions caught during processing will be
     *                         rethrown wrapped into a PropelException.
     * @return array           (AssoMotsArchive object, last column rank)
     */
    public static function populateObject($row, $offset = 0, $indexType = TableMap::TYPE_NUM)
    {
        $key = AssoMotsArchiveTableMap::getPrimaryKeyHashFromRow($row, $offset, $indexType);
        if (null !== ($obj = AssoMotsArchiveTableMap::getInstanceFromPool($key))) {
            // We no longer rehydrate the object, since this can cause data loss.
            // See http://www.propelorm.org/ticket/509
            // $obj->hydrate($row, $offset, true); // rehydrate
            $col = $offset + AssoMotsArchiveTableMap::NUM_HYDRATE_COLUMNS;
        } else {
            $cls = AssoMotsArchiveTableMap::OM_CLASS;
            /** @var AssoMotsArchive $obj */
            $obj = new $cls();
            $col = $obj->hydrate($row, $offset, false, $indexType);
            AssoMotsArchiveTableMap::addInstanceToPool($obj, $key);
        }

        return array($obj, $col);
    }

    /**
     * The returned array will contain objects of the default type or
     * objects that inherit from the default.
     *
     * @param DataFetcherInterface $dataFetcher
     * @return array
     * @throws PropelException Any exceptions caught during processing will be
     *                         rethrown wrapped into a PropelException.
     */
    public static function populateObjects(DataFetcherInterface $dataFetcher)
    {
        $results = array();

        // set the class once to avoid overhead in the loop
        $cls = static::getOMClass(false);
        // populate the object(s)
        while ($row = $dataFetcher->fetch()) {
            $key = AssoMotsArchiveTableMap::getPrimaryKeyHashFromRow($row, 0, $dataFetcher->getIndexType());
            if (null !== ($obj = AssoMotsArchiveTableMap::getInstanceFromPool($key))) {
                // We no longer rehydrate the object, since this can cause data loss.
                // See http://www.propelorm.org/ticket/509
                // $obj->hydrate($row, 0, true); // rehydrate
                $results[] = $obj;
            } else {
                /** @var AssoMotsArchive $obj */
                $obj = new $cls();
                $obj->hydrate($row);
                $results[] = $obj;
                AssoMotsArchiveTableMap::addInstanceToPool($obj, $key);
            } // if key exists
        }

        return $results;
    }
    /**
     * Add all the columns needed to create a new object.
     *
     * Note: any columns that were marked with lazyLoad="true" in the
     * XML schema will not be added to the select list and only loaded
     * on demand.
     *
     * @param Criteria $criteria object containing the columns to add.
     * @param string   $alias    optional table alias
     * @throws PropelException Any exceptions caught during processing will be
     *                         rethrown wrapped into a PropelException.
     */
    public static function addSelectColumns(Criteria $criteria, $alias = null)
    {
        if (null === $alias) {
            $criteria->addSelectColumn(AssoMotsArchiveTableMap::COL_ID_MOT);
            $criteria->addSelectColumn(AssoMotsArchiveTableMap::COL_NOM);
            $criteria->addSelectColumn(AssoMotsArchiveTableMap::COL_NOMCOURT);
            $criteria->addSelectColumn(AssoMotsArchiveTableMap::COL_DESCRIPTIF);
            $criteria->addSelectColumn(AssoMotsArchiveTableMap::COL_TEXTE);
            $criteria->addSelectColumn(AssoMotsArchiveTableMap::COL_IMPORTANCE);
            $criteria->addSelectColumn(AssoMotsArchiveTableMap::COL_ACTIF);
            $criteria->addSelectColumn(AssoMotsArchiveTableMap::COL_ID_MOTGROUPE);
            $criteria->addSelectColumn(AssoMotsArchiveTableMap::COL_CREATED_AT);
            $criteria->addSelectColumn(AssoMotsArchiveTableMap::COL_UPDATED_AT);
            $criteria->addSelectColumn(AssoMotsArchiveTableMap::COL_ARCHIVED_AT);
        } else {
            $criteria->addSelectColumn($alias . '.id_mot');
            $criteria->addSelectColumn($alias . '.nom');
            $criteria->addSelectColumn($alias . '.nomcourt');
            $criteria->addSelectColumn($alias . '.descriptif');
            $criteria->addSelectColumn($alias . '.texte');
            $criteria->addSelectColumn($alias . '.importance');
            $criteria->addSelectColumn($alias . '.actif');
            $criteria->addSelectColumn($alias . '.id_motgroupe');
            $criteria->addSelectColumn($alias . '.created_at');
            $criteria->addSelectColumn($alias . '.updated_at');
            $criteria->addSelectColumn($alias . '.archived_at');
        }
    }

    /**
     * Returns the TableMap related to this object.
     * This method is not needed for general use but a specific application could have a need.
     * @return TableMap
     * @throws PropelException Any exceptions caught during processing will be
     *                         rethrown wrapped into a PropelException.
     */
    public static function getTableMap()
    {
        return Propel::getServiceContainer()->getDatabaseMap(AssoMotsArchiveTableMap::DATABASE_NAME)->getTable(AssoMotsArchiveTableMap::TABLE_NAME);
    }

    /**
     * Add a TableMap instance to the database for this tableMap class.
     */
    public static function buildTableMap()
    {
        $dbMap = Propel::getServiceContainer()->getDatabaseMap(AssoMotsArchiveTableMap::DATABASE_NAME);
        if (!$dbMap->hasTable(AssoMotsArchiveTableMap::TABLE_NAME)) {
            $dbMap->addTableObject(new AssoMotsArchiveTableMap());
        }
    }

    /**
     * Performs a DELETE on the database, given a AssoMotsArchive or Criteria object OR a primary key value.
     *
     * @param mixed               $values Criteria or AssoMotsArchive object or primary key or array of primary keys
     *              which is used to create the DELETE statement
     * @param  ConnectionInterface $con the connection to use
     * @return int             The number of affected rows (if supported by underlying database driver).  This includes CASCADE-related rows
     *                         if supported by native driver or if emulated using Propel.
     * @throws PropelException Any exceptions caught during processing will be
     *                         rethrown wrapped into a PropelException.
     */
     public static function doDelete($values, ConnectionInterface $con = null)
     {
        if (null === $con) {
            $con = Propel::getServiceContainer()->getWriteConnection(AssoMotsArchiveTableMap::DATABASE_NAME);
        }

        if ($values instanceof Criteria) {
            // rename for clarity
            $criteria = $values;
        } elseif ($values instanceof \AssoMotsArchive) { // it's a model object
            // create criteria based on pk values
            $criteria = $values->buildPkeyCriteria();
        } else { // it's a primary key, or an array of pks
            $criteria = new Criteria(AssoMotsArchiveTableMap::DATABASE_NAME);
            $criteria->add(AssoMotsArchiveTableMap::COL_ID_MOT, (array) $values, Criteria::IN);
        }

        $query = AssoMotsArchiveQuery::create()->mergeWith($criteria);

        if ($values instanceof Criteria) {
            AssoMotsArchiveTableMap::clearInstancePool();
        } elseif (!is_object($values)) { // it's a primary key, or an array of pks
            foreach ((array) $values as $singleval) {
                AssoMotsArchiveTableMap::removeInstanceFromPool($singleval);
            }
        }

        return $query->delete($con);
    }

    /**
     * Deletes all rows from the asso_mots_archive table.
     *
     * @param ConnectionInterface $con the connection to use
     * @return int The number of affected rows (if supported by underlying database driver).
     */
    public static function doDeleteAll(ConnectionInterface $con = null)
    {
        return AssoMotsArchiveQuery::create()->doDeleteAll($con);
    }

    /**
     * Performs an INSERT on the database, given a AssoMotsArchive or Criteria object.
     *
     * @param mixed               $criteria Criteria or AssoMotsArchive object containing data that is used to create the INSERT statement.
     * @param ConnectionInterface $con the ConnectionInterface connection to use
     * @return mixed           The new primary key.
     * @throws PropelException Any exceptions caught during processing will be
     *                         rethrown wrapped into a PropelException.
     */
    public static function doInsert($criteria, ConnectionInterface $con = null)
    {
        if (null === $con) {
            $con = Propel::getServiceContainer()->getWriteConnection(AssoMotsArchiveTableMap::DATABASE_NAME);
        }

        if ($criteria instanceof Criteria) {
            $criteria = clone $criteria; // rename for clarity
        } else {
            $criteria = $criteria->buildCriteria(); // build Criteria from AssoMotsArchive object
        }


        // Set the correct dbName
        $query = AssoMotsArchiveQuery::create()->mergeWith($criteria);

        // use transaction because $criteria could contain info
        // for more than one table (I guess, conceivably)
        return $con->transaction(function () use ($con, $query) {
            return $query->doInsert($con);
        });
    }

} // AssoMotsArchiveTableMap
// This is the static code needed to register the TableMap for this table with the main Propel class.
//
AssoMotsArchiveTableMap::buildTableMap();
