<?php

namespace Map;

use \AssoImportlignesArchive;
use \AssoImportlignesArchiveQuery;
use Propel\Runtime\Propel;
use Propel\Runtime\ActiveQuery\Criteria;
use Propel\Runtime\ActiveQuery\InstancePoolTrait;
use Propel\Runtime\Connection\ConnectionInterface;
use Propel\Runtime\DataFetcher\DataFetcherInterface;
use Propel\Runtime\Exception\PropelException;
use Propel\Runtime\Map\RelationMap;
use Propel\Runtime\Map\TableMap;
use Propel\Runtime\Map\TableMapTrait;


/**
 * This class defines the structure of the 'asso_importlignes_archive' table.
 *
 *
 *
 * This map class is used by Propel to do runtime db structure discovery.
 * For example, the createSelectSql() method checks the type of a given column used in an
 * ORDER BY clause to know whether it needs to apply SQL to make the ORDER BY case-insensitive
 * (i.e. if it's a text column type).
 *
 */
class AssoImportlignesArchiveTableMap extends TableMap
{
    use InstancePoolTrait;
    use TableMapTrait;

    /**
     * The (dot-path) name of this class
     */
    const CLASS_NAME = '.Map.AssoImportlignesArchiveTableMap';

    /**
     * The default database name for this class
     */
    const DATABASE_NAME = 'simplasso';

    /**
     * The table name for this class
     */
    const TABLE_NAME = 'asso_importlignes_archive';

    /**
     * The related Propel class for this table
     */
    const OM_CLASS = '\\AssoImportlignesArchive';

    /**
     * A class that can be returned by this tableMap
     */
    const CLASS_DEFAULT = 'AssoImportlignesArchive';

    /**
     * The total number of columns
     */
    const NUM_COLUMNS = 10;

    /**
     * The number of lazy-loaded columns
     */
    const NUM_LAZY_LOAD_COLUMNS = 0;

    /**
     * The number of columns to hydrate (NUM_COLUMNS - NUM_LAZY_LOAD_COLUMNS)
     */
    const NUM_HYDRATE_COLUMNS = 10;

    /**
     * the column name for the id_importligne field
     */
    const COL_ID_IMPORTLIGNE = 'asso_importlignes_archive.id_importligne';

    /**
     * the column name for the id_import field
     */
    const COL_ID_IMPORT = 'asso_importlignes_archive.id_import';

    /**
     * the column name for the ligne field
     */
    const COL_LIGNE = 'asso_importlignes_archive.ligne';

    /**
     * the column name for the statut field
     */
    const COL_STATUT = 'asso_importlignes_archive.statut';

    /**
     * the column name for the variables field
     */
    const COL_VARIABLES = 'asso_importlignes_archive.variables';

    /**
     * the column name for the propositions field
     */
    const COL_PROPOSITIONS = 'asso_importlignes_archive.propositions';

    /**
     * the column name for the observation field
     */
    const COL_OBSERVATION = 'asso_importlignes_archive.observation';

    /**
     * the column name for the created_at field
     */
    const COL_CREATED_AT = 'asso_importlignes_archive.created_at';

    /**
     * the column name for the updated_at field
     */
    const COL_UPDATED_AT = 'asso_importlignes_archive.updated_at';

    /**
     * the column name for the archived_at field
     */
    const COL_ARCHIVED_AT = 'asso_importlignes_archive.archived_at';

    /**
     * The default string format for model objects of the related table
     */
    const DEFAULT_STRING_FORMAT = 'YAML';

    /**
     * holds an array of fieldnames
     *
     * first dimension keys are the type constants
     * e.g. self::$fieldNames[self::TYPE_PHPNAME][0] = 'Id'
     */
    protected static $fieldNames = array (
        self::TYPE_PHPNAME       => array('IdImportligne', 'IdImport', 'Ligne', 'Statut', 'Variables', 'Propositions', 'Observation', 'CreatedAt', 'UpdatedAt', 'ArchivedAt', ),
        self::TYPE_CAMELNAME     => array('idImportligne', 'idImport', 'ligne', 'statut', 'variables', 'propositions', 'observation', 'createdAt', 'updatedAt', 'archivedAt', ),
        self::TYPE_COLNAME       => array(AssoImportlignesArchiveTableMap::COL_ID_IMPORTLIGNE, AssoImportlignesArchiveTableMap::COL_ID_IMPORT, AssoImportlignesArchiveTableMap::COL_LIGNE, AssoImportlignesArchiveTableMap::COL_STATUT, AssoImportlignesArchiveTableMap::COL_VARIABLES, AssoImportlignesArchiveTableMap::COL_PROPOSITIONS, AssoImportlignesArchiveTableMap::COL_OBSERVATION, AssoImportlignesArchiveTableMap::COL_CREATED_AT, AssoImportlignesArchiveTableMap::COL_UPDATED_AT, AssoImportlignesArchiveTableMap::COL_ARCHIVED_AT, ),
        self::TYPE_FIELDNAME     => array('id_importligne', 'id_import', 'ligne', 'statut', 'variables', 'propositions', 'observation', 'created_at', 'updated_at', 'archived_at', ),
        self::TYPE_NUM           => array(0, 1, 2, 3, 4, 5, 6, 7, 8, 9, )
    );

    /**
     * holds an array of keys for quick access to the fieldnames array
     *
     * first dimension keys are the type constants
     * e.g. self::$fieldKeys[self::TYPE_PHPNAME]['Id'] = 0
     */
    protected static $fieldKeys = array (
        self::TYPE_PHPNAME       => array('IdImportligne' => 0, 'IdImport' => 1, 'Ligne' => 2, 'Statut' => 3, 'Variables' => 4, 'Propositions' => 5, 'Observation' => 6, 'CreatedAt' => 7, 'UpdatedAt' => 8, 'ArchivedAt' => 9, ),
        self::TYPE_CAMELNAME     => array('idImportligne' => 0, 'idImport' => 1, 'ligne' => 2, 'statut' => 3, 'variables' => 4, 'propositions' => 5, 'observation' => 6, 'createdAt' => 7, 'updatedAt' => 8, 'archivedAt' => 9, ),
        self::TYPE_COLNAME       => array(AssoImportlignesArchiveTableMap::COL_ID_IMPORTLIGNE => 0, AssoImportlignesArchiveTableMap::COL_ID_IMPORT => 1, AssoImportlignesArchiveTableMap::COL_LIGNE => 2, AssoImportlignesArchiveTableMap::COL_STATUT => 3, AssoImportlignesArchiveTableMap::COL_VARIABLES => 4, AssoImportlignesArchiveTableMap::COL_PROPOSITIONS => 5, AssoImportlignesArchiveTableMap::COL_OBSERVATION => 6, AssoImportlignesArchiveTableMap::COL_CREATED_AT => 7, AssoImportlignesArchiveTableMap::COL_UPDATED_AT => 8, AssoImportlignesArchiveTableMap::COL_ARCHIVED_AT => 9, ),
        self::TYPE_FIELDNAME     => array('id_importligne' => 0, 'id_import' => 1, 'ligne' => 2, 'statut' => 3, 'variables' => 4, 'propositions' => 5, 'observation' => 6, 'created_at' => 7, 'updated_at' => 8, 'archived_at' => 9, ),
        self::TYPE_NUM           => array(0, 1, 2, 3, 4, 5, 6, 7, 8, 9, )
    );

    /**
     * Initialize the table attributes and columns
     * Relations are not initialized by this method since they are lazy loaded
     *
     * @return void
     * @throws PropelException
     */
    public function initialize()
    {
        // attributes
        $this->setName('asso_importlignes_archive');
        $this->setPhpName('AssoImportlignesArchive');
        $this->setIdentifierQuoting(true);
        $this->setClassName('\\AssoImportlignesArchive');
        $this->setPackage('');
        $this->setUseIdGenerator(false);
        // columns
        $this->addPrimaryKey('id_importligne', 'IdImportligne', 'BIGINT', true, 21, null);
        $this->addColumn('id_import', 'IdImport', 'BIGINT', true, 21, null);
        $this->addColumn('ligne', 'Ligne', 'BIGINT', true, 21, null);
        $this->addColumn('statut', 'Statut', 'VARCHAR', true, 1, 'D');
        $this->addColumn('variables', 'Variables', 'CLOB', false, null, null);
        $this->addColumn('propositions', 'Propositions', 'CLOB', false, null, null);
        $this->addColumn('observation', 'Observation', 'CLOB', false, null, null);
        $this->addColumn('created_at', 'CreatedAt', 'TIMESTAMP', false, null, null);
        $this->addColumn('updated_at', 'UpdatedAt', 'TIMESTAMP', false, null, null);
        $this->addColumn('archived_at', 'ArchivedAt', 'TIMESTAMP', false, null, null);
    } // initialize()

    /**
     * Build the RelationMap objects for this table relationships
     */
    public function buildRelations()
    {
    } // buildRelations()

    /**
     * Retrieves a string version of the primary key from the DB resultset row that can be used to uniquely identify a row in this table.
     *
     * For tables with a single-column primary key, that simple pkey value will be returned.  For tables with
     * a multi-column primary key, a serialize()d version of the primary key will be returned.
     *
     * @param array  $row       resultset row.
     * @param int    $offset    The 0-based offset for reading from the resultset row.
     * @param string $indexType One of the class type constants TableMap::TYPE_PHPNAME, TableMap::TYPE_CAMELNAME
     *                           TableMap::TYPE_COLNAME, TableMap::TYPE_FIELDNAME, TableMap::TYPE_NUM
     *
     * @return string The primary key hash of the row
     */
    public static function getPrimaryKeyHashFromRow($row, $offset = 0, $indexType = TableMap::TYPE_NUM)
    {
        // If the PK cannot be derived from the row, return NULL.
        if ($row[TableMap::TYPE_NUM == $indexType ? 0 + $offset : static::translateFieldName('IdImportligne', TableMap::TYPE_PHPNAME, $indexType)] === null) {
            return null;
        }

        return null === $row[TableMap::TYPE_NUM == $indexType ? 0 + $offset : static::translateFieldName('IdImportligne', TableMap::TYPE_PHPNAME, $indexType)] || is_scalar($row[TableMap::TYPE_NUM == $indexType ? 0 + $offset : static::translateFieldName('IdImportligne', TableMap::TYPE_PHPNAME, $indexType)]) || is_callable([$row[TableMap::TYPE_NUM == $indexType ? 0 + $offset : static::translateFieldName('IdImportligne', TableMap::TYPE_PHPNAME, $indexType)], '__toString']) ? (string) $row[TableMap::TYPE_NUM == $indexType ? 0 + $offset : static::translateFieldName('IdImportligne', TableMap::TYPE_PHPNAME, $indexType)] : $row[TableMap::TYPE_NUM == $indexType ? 0 + $offset : static::translateFieldName('IdImportligne', TableMap::TYPE_PHPNAME, $indexType)];
    }

    /**
     * Retrieves the primary key from the DB resultset row
     * For tables with a single-column primary key, that simple pkey value will be returned.  For tables with
     * a multi-column primary key, an array of the primary key columns will be returned.
     *
     * @param array  $row       resultset row.
     * @param int    $offset    The 0-based offset for reading from the resultset row.
     * @param string $indexType One of the class type constants TableMap::TYPE_PHPNAME, TableMap::TYPE_CAMELNAME
     *                           TableMap::TYPE_COLNAME, TableMap::TYPE_FIELDNAME, TableMap::TYPE_NUM
     *
     * @return mixed The primary key of the row
     */
    public static function getPrimaryKeyFromRow($row, $offset = 0, $indexType = TableMap::TYPE_NUM)
    {
        return (string) $row[
            $indexType == TableMap::TYPE_NUM
                ? 0 + $offset
                : self::translateFieldName('IdImportligne', TableMap::TYPE_PHPNAME, $indexType)
        ];
    }

    /**
     * The class that the tableMap will make instances of.
     *
     * If $withPrefix is true, the returned path
     * uses a dot-path notation which is translated into a path
     * relative to a location on the PHP include_path.
     * (e.g. path.to.MyClass -> 'path/to/MyClass.php')
     *
     * @param boolean $withPrefix Whether or not to return the path with the class name
     * @return string path.to.ClassName
     */
    public static function getOMClass($withPrefix = true)
    {
        return $withPrefix ? AssoImportlignesArchiveTableMap::CLASS_DEFAULT : AssoImportlignesArchiveTableMap::OM_CLASS;
    }

    /**
     * Populates an object of the default type or an object that inherit from the default.
     *
     * @param array  $row       row returned by DataFetcher->fetch().
     * @param int    $offset    The 0-based offset for reading from the resultset row.
     * @param string $indexType The index type of $row. Mostly DataFetcher->getIndexType().
                                 One of the class type constants TableMap::TYPE_PHPNAME, TableMap::TYPE_CAMELNAME
     *                           TableMap::TYPE_COLNAME, TableMap::TYPE_FIELDNAME, TableMap::TYPE_NUM.
     *
     * @throws PropelException Any exceptions caught during processing will be
     *                         rethrown wrapped into a PropelException.
     * @return array           (AssoImportlignesArchive object, last column rank)
     */
    public static function populateObject($row, $offset = 0, $indexType = TableMap::TYPE_NUM)
    {
        $key = AssoImportlignesArchiveTableMap::getPrimaryKeyHashFromRow($row, $offset, $indexType);
        if (null !== ($obj = AssoImportlignesArchiveTableMap::getInstanceFromPool($key))) {
            // We no longer rehydrate the object, since this can cause data loss.
            // See http://www.propelorm.org/ticket/509
            // $obj->hydrate($row, $offset, true); // rehydrate
            $col = $offset + AssoImportlignesArchiveTableMap::NUM_HYDRATE_COLUMNS;
        } else {
            $cls = AssoImportlignesArchiveTableMap::OM_CLASS;
            /** @var AssoImportlignesArchive $obj */
            $obj = new $cls();
            $col = $obj->hydrate($row, $offset, false, $indexType);
            AssoImportlignesArchiveTableMap::addInstanceToPool($obj, $key);
        }

        return array($obj, $col);
    }

    /**
     * The returned array will contain objects of the default type or
     * objects that inherit from the default.
     *
     * @param DataFetcherInterface $dataFetcher
     * @return array
     * @throws PropelException Any exceptions caught during processing will be
     *                         rethrown wrapped into a PropelException.
     */
    public static function populateObjects(DataFetcherInterface $dataFetcher)
    {
        $results = array();

        // set the class once to avoid overhead in the loop
        $cls = static::getOMClass(false);
        // populate the object(s)
        while ($row = $dataFetcher->fetch()) {
            $key = AssoImportlignesArchiveTableMap::getPrimaryKeyHashFromRow($row, 0, $dataFetcher->getIndexType());
            if (null !== ($obj = AssoImportlignesArchiveTableMap::getInstanceFromPool($key))) {
                // We no longer rehydrate the object, since this can cause data loss.
                // See http://www.propelorm.org/ticket/509
                // $obj->hydrate($row, 0, true); // rehydrate
                $results[] = $obj;
            } else {
                /** @var AssoImportlignesArchive $obj */
                $obj = new $cls();
                $obj->hydrate($row);
                $results[] = $obj;
                AssoImportlignesArchiveTableMap::addInstanceToPool($obj, $key);
            } // if key exists
        }

        return $results;
    }
    /**
     * Add all the columns needed to create a new object.
     *
     * Note: any columns that were marked with lazyLoad="true" in the
     * XML schema will not be added to the select list and only loaded
     * on demand.
     *
     * @param Criteria $criteria object containing the columns to add.
     * @param string   $alias    optional table alias
     * @throws PropelException Any exceptions caught during processing will be
     *                         rethrown wrapped into a PropelException.
     */
    public static function addSelectColumns(Criteria $criteria, $alias = null)
    {
        if (null === $alias) {
            $criteria->addSelectColumn(AssoImportlignesArchiveTableMap::COL_ID_IMPORTLIGNE);
            $criteria->addSelectColumn(AssoImportlignesArchiveTableMap::COL_ID_IMPORT);
            $criteria->addSelectColumn(AssoImportlignesArchiveTableMap::COL_LIGNE);
            $criteria->addSelectColumn(AssoImportlignesArchiveTableMap::COL_STATUT);
            $criteria->addSelectColumn(AssoImportlignesArchiveTableMap::COL_VARIABLES);
            $criteria->addSelectColumn(AssoImportlignesArchiveTableMap::COL_PROPOSITIONS);
            $criteria->addSelectColumn(AssoImportlignesArchiveTableMap::COL_OBSERVATION);
            $criteria->addSelectColumn(AssoImportlignesArchiveTableMap::COL_CREATED_AT);
            $criteria->addSelectColumn(AssoImportlignesArchiveTableMap::COL_UPDATED_AT);
            $criteria->addSelectColumn(AssoImportlignesArchiveTableMap::COL_ARCHIVED_AT);
        } else {
            $criteria->addSelectColumn($alias . '.id_importligne');
            $criteria->addSelectColumn($alias . '.id_import');
            $criteria->addSelectColumn($alias . '.ligne');
            $criteria->addSelectColumn($alias . '.statut');
            $criteria->addSelectColumn($alias . '.variables');
            $criteria->addSelectColumn($alias . '.propositions');
            $criteria->addSelectColumn($alias . '.observation');
            $criteria->addSelectColumn($alias . '.created_at');
            $criteria->addSelectColumn($alias . '.updated_at');
            $criteria->addSelectColumn($alias . '.archived_at');
        }
    }

    /**
     * Returns the TableMap related to this object.
     * This method is not needed for general use but a specific application could have a need.
     * @return TableMap
     * @throws PropelException Any exceptions caught during processing will be
     *                         rethrown wrapped into a PropelException.
     */
    public static function getTableMap()
    {
        return Propel::getServiceContainer()->getDatabaseMap(AssoImportlignesArchiveTableMap::DATABASE_NAME)->getTable(AssoImportlignesArchiveTableMap::TABLE_NAME);
    }

    /**
     * Add a TableMap instance to the database for this tableMap class.
     */
    public static function buildTableMap()
    {
        $dbMap = Propel::getServiceContainer()->getDatabaseMap(AssoImportlignesArchiveTableMap::DATABASE_NAME);
        if (!$dbMap->hasTable(AssoImportlignesArchiveTableMap::TABLE_NAME)) {
            $dbMap->addTableObject(new AssoImportlignesArchiveTableMap());
        }
    }

    /**
     * Performs a DELETE on the database, given a AssoImportlignesArchive or Criteria object OR a primary key value.
     *
     * @param mixed               $values Criteria or AssoImportlignesArchive object or primary key or array of primary keys
     *              which is used to create the DELETE statement
     * @param  ConnectionInterface $con the connection to use
     * @return int             The number of affected rows (if supported by underlying database driver).  This includes CASCADE-related rows
     *                         if supported by native driver or if emulated using Propel.
     * @throws PropelException Any exceptions caught during processing will be
     *                         rethrown wrapped into a PropelException.
     */
     public static function doDelete($values, ConnectionInterface $con = null)
     {
        if (null === $con) {
            $con = Propel::getServiceContainer()->getWriteConnection(AssoImportlignesArchiveTableMap::DATABASE_NAME);
        }

        if ($values instanceof Criteria) {
            // rename for clarity
            $criteria = $values;
        } elseif ($values instanceof \AssoImportlignesArchive) { // it's a model object
            // create criteria based on pk values
            $criteria = $values->buildPkeyCriteria();
        } else { // it's a primary key, or an array of pks
            $criteria = new Criteria(AssoImportlignesArchiveTableMap::DATABASE_NAME);
            $criteria->add(AssoImportlignesArchiveTableMap::COL_ID_IMPORTLIGNE, (array) $values, Criteria::IN);
        }

        $query = AssoImportlignesArchiveQuery::create()->mergeWith($criteria);

        if ($values instanceof Criteria) {
            AssoImportlignesArchiveTableMap::clearInstancePool();
        } elseif (!is_object($values)) { // it's a primary key, or an array of pks
            foreach ((array) $values as $singleval) {
                AssoImportlignesArchiveTableMap::removeInstanceFromPool($singleval);
            }
        }

        return $query->delete($con);
    }

    /**
     * Deletes all rows from the asso_importlignes_archive table.
     *
     * @param ConnectionInterface $con the connection to use
     * @return int The number of affected rows (if supported by underlying database driver).
     */
    public static function doDeleteAll(ConnectionInterface $con = null)
    {
        return AssoImportlignesArchiveQuery::create()->doDeleteAll($con);
    }

    /**
     * Performs an INSERT on the database, given a AssoImportlignesArchive or Criteria object.
     *
     * @param mixed               $criteria Criteria or AssoImportlignesArchive object containing data that is used to create the INSERT statement.
     * @param ConnectionInterface $con the ConnectionInterface connection to use
     * @return mixed           The new primary key.
     * @throws PropelException Any exceptions caught during processing will be
     *                         rethrown wrapped into a PropelException.
     */
    public static function doInsert($criteria, ConnectionInterface $con = null)
    {
        if (null === $con) {
            $con = Propel::getServiceContainer()->getWriteConnection(AssoImportlignesArchiveTableMap::DATABASE_NAME);
        }

        if ($criteria instanceof Criteria) {
            $criteria = clone $criteria; // rename for clarity
        } else {
            $criteria = $criteria->buildCriteria(); // build Criteria from AssoImportlignesArchive object
        }


        // Set the correct dbName
        $query = AssoImportlignesArchiveQuery::create()->mergeWith($criteria);

        // use transaction because $criteria could contain info
        // for more than one table (I guess, conceivably)
        return $con->transaction(function () use ($con, $query) {
            return $query->doInsert($con);
        });
    }

} // AssoImportlignesArchiveTableMap
// This is the static code needed to register the TableMap for this table with the main Propel class.
//
AssoImportlignesArchiveTableMap::buildTableMap();
