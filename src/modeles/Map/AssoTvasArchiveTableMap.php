<?php

namespace Map;

use \AssoTvasArchive;
use \AssoTvasArchiveQuery;
use Propel\Runtime\Propel;
use Propel\Runtime\ActiveQuery\Criteria;
use Propel\Runtime\ActiveQuery\InstancePoolTrait;
use Propel\Runtime\Connection\ConnectionInterface;
use Propel\Runtime\DataFetcher\DataFetcherInterface;
use Propel\Runtime\Exception\PropelException;
use Propel\Runtime\Map\RelationMap;
use Propel\Runtime\Map\TableMap;
use Propel\Runtime\Map\TableMapTrait;


/**
 * This class defines the structure of the 'asso_tvas_archive' table.
 *
 *
 *
 * This map class is used by Propel to do runtime db structure discovery.
 * For example, the createSelectSql() method checks the type of a given column used in an
 * ORDER BY clause to know whether it needs to apply SQL to make the ORDER BY case-insensitive
 * (i.e. if it's a text column type).
 *
 */
class AssoTvasArchiveTableMap extends TableMap
{
    use InstancePoolTrait;
    use TableMapTrait;

    /**
     * The (dot-path) name of this class
     */
    const CLASS_NAME = '.Map.AssoTvasArchiveTableMap';

    /**
     * The default database name for this class
     */
    const DATABASE_NAME = 'simplasso';

    /**
     * The table name for this class
     */
    const TABLE_NAME = 'asso_tvas_archive';

    /**
     * The related Propel class for this table
     */
    const OM_CLASS = '\\AssoTvasArchive';

    /**
     * A class that can be returned by this tableMap
     */
    const CLASS_DEFAULT = 'AssoTvasArchive';

    /**
     * The total number of columns
     */
    const NUM_COLUMNS = 12;

    /**
     * The number of lazy-loaded columns
     */
    const NUM_LAZY_LOAD_COLUMNS = 0;

    /**
     * The number of columns to hydrate (NUM_COLUMNS - NUM_LAZY_LOAD_COLUMNS)
     */
    const NUM_HYDRATE_COLUMNS = 12;

    /**
     * the column name for the id_tva field
     */
    const COL_ID_TVA = 'asso_tvas_archive.id_tva';

    /**
     * the column name for the nom field
     */
    const COL_NOM = 'asso_tvas_archive.nom';

    /**
     * the column name for the nomcourt field
     */
    const COL_NOMCOURT = 'asso_tvas_archive.nomcourt';

    /**
     * the column name for the id_compte field
     */
    const COL_ID_COMPTE = 'asso_tvas_archive.id_compte';

    /**
     * the column name for the actif field
     */
    const COL_ACTIF = 'asso_tvas_archive.actif';

    /**
     * the column name for the signe field
     */
    const COL_SIGNE = 'asso_tvas_archive.signe';

    /**
     * the column name for the encaissement field
     */
    const COL_ENCAISSEMENT = 'asso_tvas_archive.encaissement';

    /**
     * the column name for the id_entite field
     */
    const COL_ID_ENTITE = 'asso_tvas_archive.id_entite';

    /**
     * the column name for the observation field
     */
    const COL_OBSERVATION = 'asso_tvas_archive.observation';

    /**
     * the column name for the created_at field
     */
    const COL_CREATED_AT = 'asso_tvas_archive.created_at';

    /**
     * the column name for the updated_at field
     */
    const COL_UPDATED_AT = 'asso_tvas_archive.updated_at';

    /**
     * the column name for the archived_at field
     */
    const COL_ARCHIVED_AT = 'asso_tvas_archive.archived_at';

    /**
     * The default string format for model objects of the related table
     */
    const DEFAULT_STRING_FORMAT = 'YAML';

    /**
     * holds an array of fieldnames
     *
     * first dimension keys are the type constants
     * e.g. self::$fieldNames[self::TYPE_PHPNAME][0] = 'Id'
     */
    protected static $fieldNames = array (
        self::TYPE_PHPNAME       => array('IdTva', 'Nom', 'Nomcourt', 'IdCompte', 'Actif', 'Signe', 'Encaissement', 'IdEntite', 'Observation', 'CreatedAt', 'UpdatedAt', 'ArchivedAt', ),
        self::TYPE_CAMELNAME     => array('idTva', 'nom', 'nomcourt', 'idCompte', 'actif', 'signe', 'encaissement', 'idEntite', 'observation', 'createdAt', 'updatedAt', 'archivedAt', ),
        self::TYPE_COLNAME       => array(AssoTvasArchiveTableMap::COL_ID_TVA, AssoTvasArchiveTableMap::COL_NOM, AssoTvasArchiveTableMap::COL_NOMCOURT, AssoTvasArchiveTableMap::COL_ID_COMPTE, AssoTvasArchiveTableMap::COL_ACTIF, AssoTvasArchiveTableMap::COL_SIGNE, AssoTvasArchiveTableMap::COL_ENCAISSEMENT, AssoTvasArchiveTableMap::COL_ID_ENTITE, AssoTvasArchiveTableMap::COL_OBSERVATION, AssoTvasArchiveTableMap::COL_CREATED_AT, AssoTvasArchiveTableMap::COL_UPDATED_AT, AssoTvasArchiveTableMap::COL_ARCHIVED_AT, ),
        self::TYPE_FIELDNAME     => array('id_tva', 'nom', 'nomcourt', 'id_compte', 'actif', 'signe', 'encaissement', 'id_entite', 'observation', 'created_at', 'updated_at', 'archived_at', ),
        self::TYPE_NUM           => array(0, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, )
    );

    /**
     * holds an array of keys for quick access to the fieldnames array
     *
     * first dimension keys are the type constants
     * e.g. self::$fieldKeys[self::TYPE_PHPNAME]['Id'] = 0
     */
    protected static $fieldKeys = array (
        self::TYPE_PHPNAME       => array('IdTva' => 0, 'Nom' => 1, 'Nomcourt' => 2, 'IdCompte' => 3, 'Actif' => 4, 'Signe' => 5, 'Encaissement' => 6, 'IdEntite' => 7, 'Observation' => 8, 'CreatedAt' => 9, 'UpdatedAt' => 10, 'ArchivedAt' => 11, ),
        self::TYPE_CAMELNAME     => array('idTva' => 0, 'nom' => 1, 'nomcourt' => 2, 'idCompte' => 3, 'actif' => 4, 'signe' => 5, 'encaissement' => 6, 'idEntite' => 7, 'observation' => 8, 'createdAt' => 9, 'updatedAt' => 10, 'archivedAt' => 11, ),
        self::TYPE_COLNAME       => array(AssoTvasArchiveTableMap::COL_ID_TVA => 0, AssoTvasArchiveTableMap::COL_NOM => 1, AssoTvasArchiveTableMap::COL_NOMCOURT => 2, AssoTvasArchiveTableMap::COL_ID_COMPTE => 3, AssoTvasArchiveTableMap::COL_ACTIF => 4, AssoTvasArchiveTableMap::COL_SIGNE => 5, AssoTvasArchiveTableMap::COL_ENCAISSEMENT => 6, AssoTvasArchiveTableMap::COL_ID_ENTITE => 7, AssoTvasArchiveTableMap::COL_OBSERVATION => 8, AssoTvasArchiveTableMap::COL_CREATED_AT => 9, AssoTvasArchiveTableMap::COL_UPDATED_AT => 10, AssoTvasArchiveTableMap::COL_ARCHIVED_AT => 11, ),
        self::TYPE_FIELDNAME     => array('id_tva' => 0, 'nom' => 1, 'nomcourt' => 2, 'id_compte' => 3, 'actif' => 4, 'signe' => 5, 'encaissement' => 6, 'id_entite' => 7, 'observation' => 8, 'created_at' => 9, 'updated_at' => 10, 'archived_at' => 11, ),
        self::TYPE_NUM           => array(0, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, )
    );

    /**
     * Initialize the table attributes and columns
     * Relations are not initialized by this method since they are lazy loaded
     *
     * @return void
     * @throws PropelException
     */
    public function initialize()
    {
        // attributes
        $this->setName('asso_tvas_archive');
        $this->setPhpName('AssoTvasArchive');
        $this->setIdentifierQuoting(true);
        $this->setClassName('\\AssoTvasArchive');
        $this->setPackage('');
        $this->setUseIdGenerator(false);
        // columns
        $this->addPrimaryKey('id_tva', 'IdTva', 'BIGINT', true, 21, null);
        $this->addColumn('nom', 'Nom', 'VARCHAR', true, 25, 'Tva % ');
        $this->addColumn('nomcourt', 'Nomcourt', 'VARCHAR', true, 6, '20.00');
        $this->addColumn('id_compte', 'IdCompte', 'BIGINT', true, 21, 1);
        $this->addColumn('actif', 'Actif', 'INTEGER', true, 1, 1);
        $this->addColumn('signe', 'Signe', 'VARCHAR', true, 1, '+');
        $this->addColumn('encaissement', 'Encaissement', 'INTEGER', true, 1, 0);
        $this->addColumn('id_entite', 'IdEntite', 'BIGINT', true, 21, 1);
        $this->addColumn('observation', 'Observation', 'LONGVARCHAR', false, null, null);
        $this->addColumn('created_at', 'CreatedAt', 'TIMESTAMP', false, null, null);
        $this->addColumn('updated_at', 'UpdatedAt', 'TIMESTAMP', false, null, null);
        $this->addColumn('archived_at', 'ArchivedAt', 'TIMESTAMP', false, null, null);
    } // initialize()

    /**
     * Build the RelationMap objects for this table relationships
     */
    public function buildRelations()
    {
    } // buildRelations()

    /**
     * Retrieves a string version of the primary key from the DB resultset row that can be used to uniquely identify a row in this table.
     *
     * For tables with a single-column primary key, that simple pkey value will be returned.  For tables with
     * a multi-column primary key, a serialize()d version of the primary key will be returned.
     *
     * @param array  $row       resultset row.
     * @param int    $offset    The 0-based offset for reading from the resultset row.
     * @param string $indexType One of the class type constants TableMap::TYPE_PHPNAME, TableMap::TYPE_CAMELNAME
     *                           TableMap::TYPE_COLNAME, TableMap::TYPE_FIELDNAME, TableMap::TYPE_NUM
     *
     * @return string The primary key hash of the row
     */
    public static function getPrimaryKeyHashFromRow($row, $offset = 0, $indexType = TableMap::TYPE_NUM)
    {
        // If the PK cannot be derived from the row, return NULL.
        if ($row[TableMap::TYPE_NUM == $indexType ? 0 + $offset : static::translateFieldName('IdTva', TableMap::TYPE_PHPNAME, $indexType)] === null) {
            return null;
        }

        return null === $row[TableMap::TYPE_NUM == $indexType ? 0 + $offset : static::translateFieldName('IdTva', TableMap::TYPE_PHPNAME, $indexType)] || is_scalar($row[TableMap::TYPE_NUM == $indexType ? 0 + $offset : static::translateFieldName('IdTva', TableMap::TYPE_PHPNAME, $indexType)]) || is_callable([$row[TableMap::TYPE_NUM == $indexType ? 0 + $offset : static::translateFieldName('IdTva', TableMap::TYPE_PHPNAME, $indexType)], '__toString']) ? (string) $row[TableMap::TYPE_NUM == $indexType ? 0 + $offset : static::translateFieldName('IdTva', TableMap::TYPE_PHPNAME, $indexType)] : $row[TableMap::TYPE_NUM == $indexType ? 0 + $offset : static::translateFieldName('IdTva', TableMap::TYPE_PHPNAME, $indexType)];
    }

    /**
     * Retrieves the primary key from the DB resultset row
     * For tables with a single-column primary key, that simple pkey value will be returned.  For tables with
     * a multi-column primary key, an array of the primary key columns will be returned.
     *
     * @param array  $row       resultset row.
     * @param int    $offset    The 0-based offset for reading from the resultset row.
     * @param string $indexType One of the class type constants TableMap::TYPE_PHPNAME, TableMap::TYPE_CAMELNAME
     *                           TableMap::TYPE_COLNAME, TableMap::TYPE_FIELDNAME, TableMap::TYPE_NUM
     *
     * @return mixed The primary key of the row
     */
    public static function getPrimaryKeyFromRow($row, $offset = 0, $indexType = TableMap::TYPE_NUM)
    {
        return (string) $row[
            $indexType == TableMap::TYPE_NUM
                ? 0 + $offset
                : self::translateFieldName('IdTva', TableMap::TYPE_PHPNAME, $indexType)
        ];
    }

    /**
     * The class that the tableMap will make instances of.
     *
     * If $withPrefix is true, the returned path
     * uses a dot-path notation which is translated into a path
     * relative to a location on the PHP include_path.
     * (e.g. path.to.MyClass -> 'path/to/MyClass.php')
     *
     * @param boolean $withPrefix Whether or not to return the path with the class name
     * @return string path.to.ClassName
     */
    public static function getOMClass($withPrefix = true)
    {
        return $withPrefix ? AssoTvasArchiveTableMap::CLASS_DEFAULT : AssoTvasArchiveTableMap::OM_CLASS;
    }

    /**
     * Populates an object of the default type or an object that inherit from the default.
     *
     * @param array  $row       row returned by DataFetcher->fetch().
     * @param int    $offset    The 0-based offset for reading from the resultset row.
     * @param string $indexType The index type of $row. Mostly DataFetcher->getIndexType().
                                 One of the class type constants TableMap::TYPE_PHPNAME, TableMap::TYPE_CAMELNAME
     *                           TableMap::TYPE_COLNAME, TableMap::TYPE_FIELDNAME, TableMap::TYPE_NUM.
     *
     * @throws PropelException Any exceptions caught during processing will be
     *                         rethrown wrapped into a PropelException.
     * @return array           (AssoTvasArchive object, last column rank)
     */
    public static function populateObject($row, $offset = 0, $indexType = TableMap::TYPE_NUM)
    {
        $key = AssoTvasArchiveTableMap::getPrimaryKeyHashFromRow($row, $offset, $indexType);
        if (null !== ($obj = AssoTvasArchiveTableMap::getInstanceFromPool($key))) {
            // We no longer rehydrate the object, since this can cause data loss.
            // See http://www.propelorm.org/ticket/509
            // $obj->hydrate($row, $offset, true); // rehydrate
            $col = $offset + AssoTvasArchiveTableMap::NUM_HYDRATE_COLUMNS;
        } else {
            $cls = AssoTvasArchiveTableMap::OM_CLASS;
            /** @var AssoTvasArchive $obj */
            $obj = new $cls();
            $col = $obj->hydrate($row, $offset, false, $indexType);
            AssoTvasArchiveTableMap::addInstanceToPool($obj, $key);
        }

        return array($obj, $col);
    }

    /**
     * The returned array will contain objects of the default type or
     * objects that inherit from the default.
     *
     * @param DataFetcherInterface $dataFetcher
     * @return array
     * @throws PropelException Any exceptions caught during processing will be
     *                         rethrown wrapped into a PropelException.
     */
    public static function populateObjects(DataFetcherInterface $dataFetcher)
    {
        $results = array();

        // set the class once to avoid overhead in the loop
        $cls = static::getOMClass(false);
        // populate the object(s)
        while ($row = $dataFetcher->fetch()) {
            $key = AssoTvasArchiveTableMap::getPrimaryKeyHashFromRow($row, 0, $dataFetcher->getIndexType());
            if (null !== ($obj = AssoTvasArchiveTableMap::getInstanceFromPool($key))) {
                // We no longer rehydrate the object, since this can cause data loss.
                // See http://www.propelorm.org/ticket/509
                // $obj->hydrate($row, 0, true); // rehydrate
                $results[] = $obj;
            } else {
                /** @var AssoTvasArchive $obj */
                $obj = new $cls();
                $obj->hydrate($row);
                $results[] = $obj;
                AssoTvasArchiveTableMap::addInstanceToPool($obj, $key);
            } // if key exists
        }

        return $results;
    }
    /**
     * Add all the columns needed to create a new object.
     *
     * Note: any columns that were marked with lazyLoad="true" in the
     * XML schema will not be added to the select list and only loaded
     * on demand.
     *
     * @param Criteria $criteria object containing the columns to add.
     * @param string   $alias    optional table alias
     * @throws PropelException Any exceptions caught during processing will be
     *                         rethrown wrapped into a PropelException.
     */
    public static function addSelectColumns(Criteria $criteria, $alias = null)
    {
        if (null === $alias) {
            $criteria->addSelectColumn(AssoTvasArchiveTableMap::COL_ID_TVA);
            $criteria->addSelectColumn(AssoTvasArchiveTableMap::COL_NOM);
            $criteria->addSelectColumn(AssoTvasArchiveTableMap::COL_NOMCOURT);
            $criteria->addSelectColumn(AssoTvasArchiveTableMap::COL_ID_COMPTE);
            $criteria->addSelectColumn(AssoTvasArchiveTableMap::COL_ACTIF);
            $criteria->addSelectColumn(AssoTvasArchiveTableMap::COL_SIGNE);
            $criteria->addSelectColumn(AssoTvasArchiveTableMap::COL_ENCAISSEMENT);
            $criteria->addSelectColumn(AssoTvasArchiveTableMap::COL_ID_ENTITE);
            $criteria->addSelectColumn(AssoTvasArchiveTableMap::COL_OBSERVATION);
            $criteria->addSelectColumn(AssoTvasArchiveTableMap::COL_CREATED_AT);
            $criteria->addSelectColumn(AssoTvasArchiveTableMap::COL_UPDATED_AT);
            $criteria->addSelectColumn(AssoTvasArchiveTableMap::COL_ARCHIVED_AT);
        } else {
            $criteria->addSelectColumn($alias . '.id_tva');
            $criteria->addSelectColumn($alias . '.nom');
            $criteria->addSelectColumn($alias . '.nomcourt');
            $criteria->addSelectColumn($alias . '.id_compte');
            $criteria->addSelectColumn($alias . '.actif');
            $criteria->addSelectColumn($alias . '.signe');
            $criteria->addSelectColumn($alias . '.encaissement');
            $criteria->addSelectColumn($alias . '.id_entite');
            $criteria->addSelectColumn($alias . '.observation');
            $criteria->addSelectColumn($alias . '.created_at');
            $criteria->addSelectColumn($alias . '.updated_at');
            $criteria->addSelectColumn($alias . '.archived_at');
        }
    }

    /**
     * Returns the TableMap related to this object.
     * This method is not needed for general use but a specific application could have a need.
     * @return TableMap
     * @throws PropelException Any exceptions caught during processing will be
     *                         rethrown wrapped into a PropelException.
     */
    public static function getTableMap()
    {
        return Propel::getServiceContainer()->getDatabaseMap(AssoTvasArchiveTableMap::DATABASE_NAME)->getTable(AssoTvasArchiveTableMap::TABLE_NAME);
    }

    /**
     * Add a TableMap instance to the database for this tableMap class.
     */
    public static function buildTableMap()
    {
        $dbMap = Propel::getServiceContainer()->getDatabaseMap(AssoTvasArchiveTableMap::DATABASE_NAME);
        if (!$dbMap->hasTable(AssoTvasArchiveTableMap::TABLE_NAME)) {
            $dbMap->addTableObject(new AssoTvasArchiveTableMap());
        }
    }

    /**
     * Performs a DELETE on the database, given a AssoTvasArchive or Criteria object OR a primary key value.
     *
     * @param mixed               $values Criteria or AssoTvasArchive object or primary key or array of primary keys
     *              which is used to create the DELETE statement
     * @param  ConnectionInterface $con the connection to use
     * @return int             The number of affected rows (if supported by underlying database driver).  This includes CASCADE-related rows
     *                         if supported by native driver or if emulated using Propel.
     * @throws PropelException Any exceptions caught during processing will be
     *                         rethrown wrapped into a PropelException.
     */
     public static function doDelete($values, ConnectionInterface $con = null)
     {
        if (null === $con) {
            $con = Propel::getServiceContainer()->getWriteConnection(AssoTvasArchiveTableMap::DATABASE_NAME);
        }

        if ($values instanceof Criteria) {
            // rename for clarity
            $criteria = $values;
        } elseif ($values instanceof \AssoTvasArchive) { // it's a model object
            // create criteria based on pk values
            $criteria = $values->buildPkeyCriteria();
        } else { // it's a primary key, or an array of pks
            $criteria = new Criteria(AssoTvasArchiveTableMap::DATABASE_NAME);
            $criteria->add(AssoTvasArchiveTableMap::COL_ID_TVA, (array) $values, Criteria::IN);
        }

        $query = AssoTvasArchiveQuery::create()->mergeWith($criteria);

        if ($values instanceof Criteria) {
            AssoTvasArchiveTableMap::clearInstancePool();
        } elseif (!is_object($values)) { // it's a primary key, or an array of pks
            foreach ((array) $values as $singleval) {
                AssoTvasArchiveTableMap::removeInstanceFromPool($singleval);
            }
        }

        return $query->delete($con);
    }

    /**
     * Deletes all rows from the asso_tvas_archive table.
     *
     * @param ConnectionInterface $con the connection to use
     * @return int The number of affected rows (if supported by underlying database driver).
     */
    public static function doDeleteAll(ConnectionInterface $con = null)
    {
        return AssoTvasArchiveQuery::create()->doDeleteAll($con);
    }

    /**
     * Performs an INSERT on the database, given a AssoTvasArchive or Criteria object.
     *
     * @param mixed               $criteria Criteria or AssoTvasArchive object containing data that is used to create the INSERT statement.
     * @param ConnectionInterface $con the ConnectionInterface connection to use
     * @return mixed           The new primary key.
     * @throws PropelException Any exceptions caught during processing will be
     *                         rethrown wrapped into a PropelException.
     */
    public static function doInsert($criteria, ConnectionInterface $con = null)
    {
        if (null === $con) {
            $con = Propel::getServiceContainer()->getWriteConnection(AssoTvasArchiveTableMap::DATABASE_NAME);
        }

        if ($criteria instanceof Criteria) {
            $criteria = clone $criteria; // rename for clarity
        } else {
            $criteria = $criteria->buildCriteria(); // build Criteria from AssoTvasArchive object
        }


        // Set the correct dbName
        $query = AssoTvasArchiveQuery::create()->mergeWith($criteria);

        // use transaction because $criteria could contain info
        // for more than one table (I guess, conceivably)
        return $con->transaction(function () use ($con, $query) {
            return $query->doInsert($con);
        });
    }

} // AssoTvasArchiveTableMap
// This is the static code needed to register the TableMap for this table with the main Propel class.
//
AssoTvasArchiveTableMap::buildTableMap();
