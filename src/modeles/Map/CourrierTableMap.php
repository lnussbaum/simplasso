<?php

namespace Map;

use \Courrier;
use \CourrierQuery;
use Propel\Runtime\Propel;
use Propel\Runtime\ActiveQuery\Criteria;
use Propel\Runtime\ActiveQuery\InstancePoolTrait;
use Propel\Runtime\Connection\ConnectionInterface;
use Propel\Runtime\DataFetcher\DataFetcherInterface;
use Propel\Runtime\Exception\PropelException;
use Propel\Runtime\Map\RelationMap;
use Propel\Runtime\Map\TableMap;
use Propel\Runtime\Map\TableMapTrait;


/**
 * This class defines the structure of the 'com_courriers' table.
 *
 *
 *
 * This map class is used by Propel to do runtime db structure discovery.
 * For example, the createSelectSql() method checks the type of a given column used in an
 * ORDER BY clause to know whether it needs to apply SQL to make the ORDER BY case-insensitive
 * (i.e. if it's a text column type).
 *
 */
class CourrierTableMap extends TableMap
{
    use InstancePoolTrait;
    use TableMapTrait;

    /**
     * The (dot-path) name of this class
     */
    const CLASS_NAME = '.Map.CourrierTableMap';

    /**
     * The default database name for this class
     */
    const DATABASE_NAME = 'simplasso';

    /**
     * The table name for this class
     */
    const TABLE_NAME = 'com_courriers';

    /**
     * The related Propel class for this table
     */
    const OM_CLASS = '\\Courrier';

    /**
     * A class that can be returned by this tableMap
     */
    const CLASS_DEFAULT = 'Courrier';

    /**
     * The total number of columns
     */
    const NUM_COLUMNS = 13;

    /**
     * The number of lazy-loaded columns
     */
    const NUM_LAZY_LOAD_COLUMNS = 0;

    /**
     * The number of columns to hydrate (NUM_COLUMNS - NUM_LAZY_LOAD_COLUMNS)
     */
    const NUM_HYDRATE_COLUMNS = 13;

    /**
     * the column name for the id_courrier field
     */
    const COL_ID_COURRIER = 'com_courriers.id_courrier';

    /**
     * the column name for the type field
     */
    const COL_TYPE = 'com_courriers.type';

    /**
     * the column name for the canal field
     */
    const COL_CANAL = 'com_courriers.canal';

    /**
     * the column name for the id_entite field
     */
    const COL_ID_ENTITE = 'com_courriers.id_entite';

    /**
     * the column name for the nom field
     */
    const COL_NOM = 'com_courriers.nom';

    /**
     * the column name for the nomcourt field
     */
    const COL_NOMCOURT = 'com_courriers.nomcourt';

    /**
     * the column name for the id_composition field
     */
    const COL_ID_COMPOSITION = 'com_courriers.id_composition';

    /**
     * the column name for the variables field
     */
    const COL_VARIABLES = 'com_courriers.variables';

    /**
     * the column name for the observation field
     */
    const COL_OBSERVATION = 'com_courriers.observation';

    /**
     * the column name for the date_envoi field
     */
    const COL_DATE_ENVOI = 'com_courriers.date_envoi';

    /**
     * the column name for the statut field
     */
    const COL_STATUT = 'com_courriers.statut';

    /**
     * the column name for the created_at field
     */
    const COL_CREATED_AT = 'com_courriers.created_at';

    /**
     * the column name for the updated_at field
     */
    const COL_UPDATED_AT = 'com_courriers.updated_at';

    /**
     * The default string format for model objects of the related table
     */
    const DEFAULT_STRING_FORMAT = 'YAML';

    /**
     * holds an array of fieldnames
     *
     * first dimension keys are the type constants
     * e.g. self::$fieldNames[self::TYPE_PHPNAME][0] = 'Id'
     */
    protected static $fieldNames = array (
        self::TYPE_PHPNAME       => array('IdCourrier', 'Type', 'Canal', 'IdEntite', 'Nom', 'Nomcourt', 'IdComposition', 'Variables', 'Observation', 'DateEnvoi', 'Statut', 'CreatedAt', 'UpdatedAt', ),
        self::TYPE_CAMELNAME     => array('idCourrier', 'type', 'canal', 'idEntite', 'nom', 'nomcourt', 'idComposition', 'variables', 'observation', 'dateEnvoi', 'statut', 'createdAt', 'updatedAt', ),
        self::TYPE_COLNAME       => array(CourrierTableMap::COL_ID_COURRIER, CourrierTableMap::COL_TYPE, CourrierTableMap::COL_CANAL, CourrierTableMap::COL_ID_ENTITE, CourrierTableMap::COL_NOM, CourrierTableMap::COL_NOMCOURT, CourrierTableMap::COL_ID_COMPOSITION, CourrierTableMap::COL_VARIABLES, CourrierTableMap::COL_OBSERVATION, CourrierTableMap::COL_DATE_ENVOI, CourrierTableMap::COL_STATUT, CourrierTableMap::COL_CREATED_AT, CourrierTableMap::COL_UPDATED_AT, ),
        self::TYPE_FIELDNAME     => array('id_courrier', 'type', 'canal', 'id_entite', 'nom', 'nomcourt', 'id_composition', 'variables', 'observation', 'date_envoi', 'statut', 'created_at', 'updated_at', ),
        self::TYPE_NUM           => array(0, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, )
    );

    /**
     * holds an array of keys for quick access to the fieldnames array
     *
     * first dimension keys are the type constants
     * e.g. self::$fieldKeys[self::TYPE_PHPNAME]['Id'] = 0
     */
    protected static $fieldKeys = array (
        self::TYPE_PHPNAME       => array('IdCourrier' => 0, 'Type' => 1, 'Canal' => 2, 'IdEntite' => 3, 'Nom' => 4, 'Nomcourt' => 5, 'IdComposition' => 6, 'Variables' => 7, 'Observation' => 8, 'DateEnvoi' => 9, 'Statut' => 10, 'CreatedAt' => 11, 'UpdatedAt' => 12, ),
        self::TYPE_CAMELNAME     => array('idCourrier' => 0, 'type' => 1, 'canal' => 2, 'idEntite' => 3, 'nom' => 4, 'nomcourt' => 5, 'idComposition' => 6, 'variables' => 7, 'observation' => 8, 'dateEnvoi' => 9, 'statut' => 10, 'createdAt' => 11, 'updatedAt' => 12, ),
        self::TYPE_COLNAME       => array(CourrierTableMap::COL_ID_COURRIER => 0, CourrierTableMap::COL_TYPE => 1, CourrierTableMap::COL_CANAL => 2, CourrierTableMap::COL_ID_ENTITE => 3, CourrierTableMap::COL_NOM => 4, CourrierTableMap::COL_NOMCOURT => 5, CourrierTableMap::COL_ID_COMPOSITION => 6, CourrierTableMap::COL_VARIABLES => 7, CourrierTableMap::COL_OBSERVATION => 8, CourrierTableMap::COL_DATE_ENVOI => 9, CourrierTableMap::COL_STATUT => 10, CourrierTableMap::COL_CREATED_AT => 11, CourrierTableMap::COL_UPDATED_AT => 12, ),
        self::TYPE_FIELDNAME     => array('id_courrier' => 0, 'type' => 1, 'canal' => 2, 'id_entite' => 3, 'nom' => 4, 'nomcourt' => 5, 'id_composition' => 6, 'variables' => 7, 'observation' => 8, 'date_envoi' => 9, 'statut' => 10, 'created_at' => 11, 'updated_at' => 12, ),
        self::TYPE_NUM           => array(0, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, )
    );

    /**
     * Initialize the table attributes and columns
     * Relations are not initialized by this method since they are lazy loaded
     *
     * @return void
     * @throws PropelException
     */
    public function initialize()
    {
        // attributes
        $this->setName('com_courriers');
        $this->setPhpName('Courrier');
        $this->setIdentifierQuoting(true);
        $this->setClassName('\\Courrier');
        $this->setPackage('');
        $this->setUseIdGenerator(true);
        // columns
        $this->addPrimaryKey('id_courrier', 'IdCourrier', 'BIGINT', true, 21, null);
        $this->addColumn('type', 'Type', 'INTEGER', true, 2, null);
        $this->addColumn('canal', 'Canal', 'VARCHAR', false, 3, null);
        $this->addForeignKey('id_entite', 'IdEntite', 'BIGINT', 'asso_entites', 'id_entite', true, null, null);
        $this->addColumn('nom', 'Nom', 'LONGVARCHAR', true, null, null);
        $this->addColumn('nomcourt', 'Nomcourt', 'VARCHAR', false, 6, null);
        $this->addForeignKey('id_composition', 'IdComposition', 'INTEGER', 'com_compositions', 'id_composition', false, null, null);
        $this->addColumn('variables', 'Variables', 'LONGVARCHAR', false, null, null);
        $this->addColumn('observation', 'Observation', 'LONGVARCHAR', false, null, null);
        $this->addColumn('date_envoi', 'DateEnvoi', 'TIMESTAMP', false, null, null);
        $this->addColumn('statut', 'Statut', 'VARCHAR', true, 10, 'redaction');
        $this->addColumn('created_at', 'CreatedAt', 'TIMESTAMP', false, null, null);
        $this->addColumn('updated_at', 'UpdatedAt', 'TIMESTAMP', false, null, null);
    } // initialize()

    /**
     * Build the RelationMap objects for this table relationships
     */
    public function buildRelations()
    {
        $this->addRelation('Entite', '\\Entite', RelationMap::MANY_TO_ONE, array (
  0 =>
  array (
    0 => ':id_entite',
    1 => ':id_entite',
  ),
), 'CASCADE', null, null, false);
        $this->addRelation('Composition', '\\Composition', RelationMap::MANY_TO_ONE, array (
  0 =>
  array (
    0 => ':id_composition',
    1 => ':id_composition',
  ),
), 'CASCADE', null, null, false);
        $this->addRelation('CourrierLien', '\\CourrierLien', RelationMap::ONE_TO_MANY, array (
  0 =>
  array (
    0 => ':id_courrier',
    1 => ':id_courrier',
  ),
), 'CASCADE', null, 'CourrierLiens', false);
    } // buildRelations()

    /**
     *
     * Gets the list of behaviors registered for this table
     *
     * @return array Associative array (name => parameters) of behaviors
     */
    public function getBehaviors()
    {
        return array(
            'timestampable' => array('create_column' => 'created_at', 'update_column' => 'updated_at', 'disable_created_at' => 'false', 'disable_updated_at' => 'false', ),
        );
    } // getBehaviors()
    /**
     * Method to invalidate the instance pool of all tables related to com_courriers     * by a foreign key with ON DELETE CASCADE
     */
    public static function clearRelatedInstancePool()
    {
        // Invalidate objects in related instance pools,
        // since one or more of them may be deleted by ON DELETE CASCADE/SETNULL rule.
        CourrierLienTableMap::clearInstancePool();
    }

    /**
     * Retrieves a string version of the primary key from the DB resultset row that can be used to uniquely identify a row in this table.
     *
     * For tables with a single-column primary key, that simple pkey value will be returned.  For tables with
     * a multi-column primary key, a serialize()d version of the primary key will be returned.
     *
     * @param array  $row       resultset row.
     * @param int    $offset    The 0-based offset for reading from the resultset row.
     * @param string $indexType One of the class type constants TableMap::TYPE_PHPNAME, TableMap::TYPE_CAMELNAME
     *                           TableMap::TYPE_COLNAME, TableMap::TYPE_FIELDNAME, TableMap::TYPE_NUM
     *
     * @return string The primary key hash of the row
     */
    public static function getPrimaryKeyHashFromRow($row, $offset = 0, $indexType = TableMap::TYPE_NUM)
    {
        // If the PK cannot be derived from the row, return NULL.
        if ($row[TableMap::TYPE_NUM == $indexType ? 0 + $offset : static::translateFieldName('IdCourrier', TableMap::TYPE_PHPNAME, $indexType)] === null) {
            return null;
        }

        return null === $row[TableMap::TYPE_NUM == $indexType ? 0 + $offset : static::translateFieldName('IdCourrier', TableMap::TYPE_PHPNAME, $indexType)] || is_scalar($row[TableMap::TYPE_NUM == $indexType ? 0 + $offset : static::translateFieldName('IdCourrier', TableMap::TYPE_PHPNAME, $indexType)]) || is_callable([$row[TableMap::TYPE_NUM == $indexType ? 0 + $offset : static::translateFieldName('IdCourrier', TableMap::TYPE_PHPNAME, $indexType)], '__toString']) ? (string) $row[TableMap::TYPE_NUM == $indexType ? 0 + $offset : static::translateFieldName('IdCourrier', TableMap::TYPE_PHPNAME, $indexType)] : $row[TableMap::TYPE_NUM == $indexType ? 0 + $offset : static::translateFieldName('IdCourrier', TableMap::TYPE_PHPNAME, $indexType)];
    }

    /**
     * Retrieves the primary key from the DB resultset row
     * For tables with a single-column primary key, that simple pkey value will be returned.  For tables with
     * a multi-column primary key, an array of the primary key columns will be returned.
     *
     * @param array  $row       resultset row.
     * @param int    $offset    The 0-based offset for reading from the resultset row.
     * @param string $indexType One of the class type constants TableMap::TYPE_PHPNAME, TableMap::TYPE_CAMELNAME
     *                           TableMap::TYPE_COLNAME, TableMap::TYPE_FIELDNAME, TableMap::TYPE_NUM
     *
     * @return mixed The primary key of the row
     */
    public static function getPrimaryKeyFromRow($row, $offset = 0, $indexType = TableMap::TYPE_NUM)
    {
        return (string) $row[
            $indexType == TableMap::TYPE_NUM
                ? 0 + $offset
                : self::translateFieldName('IdCourrier', TableMap::TYPE_PHPNAME, $indexType)
        ];
    }

    /**
     * The class that the tableMap will make instances of.
     *
     * If $withPrefix is true, the returned path
     * uses a dot-path notation which is translated into a path
     * relative to a location on the PHP include_path.
     * (e.g. path.to.MyClass -> 'path/to/MyClass.php')
     *
     * @param boolean $withPrefix Whether or not to return the path with the class name
     * @return string path.to.ClassName
     */
    public static function getOMClass($withPrefix = true)
    {
        return $withPrefix ? CourrierTableMap::CLASS_DEFAULT : CourrierTableMap::OM_CLASS;
    }

    /**
     * Populates an object of the default type or an object that inherit from the default.
     *
     * @param array  $row       row returned by DataFetcher->fetch().
     * @param int    $offset    The 0-based offset for reading from the resultset row.
     * @param string $indexType The index type of $row. Mostly DataFetcher->getIndexType().
                                 One of the class type constants TableMap::TYPE_PHPNAME, TableMap::TYPE_CAMELNAME
     *                           TableMap::TYPE_COLNAME, TableMap::TYPE_FIELDNAME, TableMap::TYPE_NUM.
     *
     * @throws PropelException Any exceptions caught during processing will be
     *                         rethrown wrapped into a PropelException.
     * @return array           (Courrier object, last column rank)
     */
    public static function populateObject($row, $offset = 0, $indexType = TableMap::TYPE_NUM)
    {
        $key = CourrierTableMap::getPrimaryKeyHashFromRow($row, $offset, $indexType);
        if (null !== ($obj = CourrierTableMap::getInstanceFromPool($key))) {
            // We no longer rehydrate the object, since this can cause data loss.
            // See http://www.propelorm.org/ticket/509
            // $obj->hydrate($row, $offset, true); // rehydrate
            $col = $offset + CourrierTableMap::NUM_HYDRATE_COLUMNS;
        } else {
            $cls = CourrierTableMap::OM_CLASS;
            /** @var Courrier $obj */
            $obj = new $cls();
            $col = $obj->hydrate($row, $offset, false, $indexType);
            CourrierTableMap::addInstanceToPool($obj, $key);
        }

        return array($obj, $col);
    }

    /**
     * The returned array will contain objects of the default type or
     * objects that inherit from the default.
     *
     * @param DataFetcherInterface $dataFetcher
     * @return array
     * @throws PropelException Any exceptions caught during processing will be
     *                         rethrown wrapped into a PropelException.
     */
    public static function populateObjects(DataFetcherInterface $dataFetcher)
    {
        $results = array();

        // set the class once to avoid overhead in the loop
        $cls = static::getOMClass(false);
        // populate the object(s)
        while ($row = $dataFetcher->fetch()) {
            $key = CourrierTableMap::getPrimaryKeyHashFromRow($row, 0, $dataFetcher->getIndexType());
            if (null !== ($obj = CourrierTableMap::getInstanceFromPool($key))) {
                // We no longer rehydrate the object, since this can cause data loss.
                // See http://www.propelorm.org/ticket/509
                // $obj->hydrate($row, 0, true); // rehydrate
                $results[] = $obj;
            } else {
                /** @var Courrier $obj */
                $obj = new $cls();
                $obj->hydrate($row);
                $results[] = $obj;
                CourrierTableMap::addInstanceToPool($obj, $key);
            } // if key exists
        }

        return $results;
    }
    /**
     * Add all the columns needed to create a new object.
     *
     * Note: any columns that were marked with lazyLoad="true" in the
     * XML schema will not be added to the select list and only loaded
     * on demand.
     *
     * @param Criteria $criteria object containing the columns to add.
     * @param string   $alias    optional table alias
     * @throws PropelException Any exceptions caught during processing will be
     *                         rethrown wrapped into a PropelException.
     */
    public static function addSelectColumns(Criteria $criteria, $alias = null)
    {
        if (null === $alias) {
            $criteria->addSelectColumn(CourrierTableMap::COL_ID_COURRIER);
            $criteria->addSelectColumn(CourrierTableMap::COL_TYPE);
            $criteria->addSelectColumn(CourrierTableMap::COL_CANAL);
            $criteria->addSelectColumn(CourrierTableMap::COL_ID_ENTITE);
            $criteria->addSelectColumn(CourrierTableMap::COL_NOM);
            $criteria->addSelectColumn(CourrierTableMap::COL_NOMCOURT);
            $criteria->addSelectColumn(CourrierTableMap::COL_ID_COMPOSITION);
            $criteria->addSelectColumn(CourrierTableMap::COL_VARIABLES);
            $criteria->addSelectColumn(CourrierTableMap::COL_OBSERVATION);
            $criteria->addSelectColumn(CourrierTableMap::COL_DATE_ENVOI);
            $criteria->addSelectColumn(CourrierTableMap::COL_STATUT);
            $criteria->addSelectColumn(CourrierTableMap::COL_CREATED_AT);
            $criteria->addSelectColumn(CourrierTableMap::COL_UPDATED_AT);
        } else {
            $criteria->addSelectColumn($alias . '.id_courrier');
            $criteria->addSelectColumn($alias . '.type');
            $criteria->addSelectColumn($alias . '.canal');
            $criteria->addSelectColumn($alias . '.id_entite');
            $criteria->addSelectColumn($alias . '.nom');
            $criteria->addSelectColumn($alias . '.nomcourt');
            $criteria->addSelectColumn($alias . '.id_composition');
            $criteria->addSelectColumn($alias . '.variables');
            $criteria->addSelectColumn($alias . '.observation');
            $criteria->addSelectColumn($alias . '.date_envoi');
            $criteria->addSelectColumn($alias . '.statut');
            $criteria->addSelectColumn($alias . '.created_at');
            $criteria->addSelectColumn($alias . '.updated_at');
        }
    }

    /**
     * Returns the TableMap related to this object.
     * This method is not needed for general use but a specific application could have a need.
     * @return TableMap
     * @throws PropelException Any exceptions caught during processing will be
     *                         rethrown wrapped into a PropelException.
     */
    public static function getTableMap()
    {
        return Propel::getServiceContainer()->getDatabaseMap(CourrierTableMap::DATABASE_NAME)->getTable(CourrierTableMap::TABLE_NAME);
    }

    /**
     * Add a TableMap instance to the database for this tableMap class.
     */
    public static function buildTableMap()
    {
        $dbMap = Propel::getServiceContainer()->getDatabaseMap(CourrierTableMap::DATABASE_NAME);
        if (!$dbMap->hasTable(CourrierTableMap::TABLE_NAME)) {
            $dbMap->addTableObject(new CourrierTableMap());
        }
    }

    /**
     * Performs a DELETE on the database, given a Courrier or Criteria object OR a primary key value.
     *
     * @param mixed               $values Criteria or Courrier object or primary key or array of primary keys
     *              which is used to create the DELETE statement
     * @param  ConnectionInterface $con the connection to use
     * @return int             The number of affected rows (if supported by underlying database driver).  This includes CASCADE-related rows
     *                         if supported by native driver or if emulated using Propel.
     * @throws PropelException Any exceptions caught during processing will be
     *                         rethrown wrapped into a PropelException.
     */
     public static function doDelete($values, ConnectionInterface $con = null)
     {
        if (null === $con) {
            $con = Propel::getServiceContainer()->getWriteConnection(CourrierTableMap::DATABASE_NAME);
        }

        if ($values instanceof Criteria) {
            // rename for clarity
            $criteria = $values;
        } elseif ($values instanceof \Courrier) { // it's a model object
            // create criteria based on pk values
            $criteria = $values->buildPkeyCriteria();
        } else { // it's a primary key, or an array of pks
            $criteria = new Criteria(CourrierTableMap::DATABASE_NAME);
            $criteria->add(CourrierTableMap::COL_ID_COURRIER, (array) $values, Criteria::IN);
        }

        $query = CourrierQuery::create()->mergeWith($criteria);

        if ($values instanceof Criteria) {
            CourrierTableMap::clearInstancePool();
        } elseif (!is_object($values)) { // it's a primary key, or an array of pks
            foreach ((array) $values as $singleval) {
                CourrierTableMap::removeInstanceFromPool($singleval);
            }
        }

        return $query->delete($con);
    }

    /**
     * Deletes all rows from the com_courriers table.
     *
     * @param ConnectionInterface $con the connection to use
     * @return int The number of affected rows (if supported by underlying database driver).
     */
    public static function doDeleteAll(ConnectionInterface $con = null)
    {
        return CourrierQuery::create()->doDeleteAll($con);
    }

    /**
     * Performs an INSERT on the database, given a Courrier or Criteria object.
     *
     * @param mixed               $criteria Criteria or Courrier object containing data that is used to create the INSERT statement.
     * @param ConnectionInterface $con the ConnectionInterface connection to use
     * @return mixed           The new primary key.
     * @throws PropelException Any exceptions caught during processing will be
     *                         rethrown wrapped into a PropelException.
     */
    public static function doInsert($criteria, ConnectionInterface $con = null)
    {
        if (null === $con) {
            $con = Propel::getServiceContainer()->getWriteConnection(CourrierTableMap::DATABASE_NAME);
        }

        if ($criteria instanceof Criteria) {
            $criteria = clone $criteria; // rename for clarity
        } else {
            $criteria = $criteria->buildCriteria(); // build Criteria from Courrier object
        }

        if ($criteria->containsKey(CourrierTableMap::COL_ID_COURRIER) && $criteria->keyContainsValue(CourrierTableMap::COL_ID_COURRIER) ) {
            throw new PropelException('Cannot insert a value for auto-increment primary key ('.CourrierTableMap::COL_ID_COURRIER.')');
        }


        // Set the correct dbName
        $query = CourrierQuery::create()->mergeWith($criteria);

        // use transaction because $criteria could contain info
        // for more than one table (I guess, conceivably)
        return $con->transaction(function () use ($con, $query) {
            return $query->doInsert($con);
        });
    }

} // CourrierTableMap
// This is the static code needed to register the TableMap for this table with the main Propel class.
//
CourrierTableMap::buildTableMap();
