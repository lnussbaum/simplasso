<?php

namespace Map;

use \AssoTresorsArchive;
use \AssoTresorsArchiveQuery;
use Propel\Runtime\Propel;
use Propel\Runtime\ActiveQuery\Criteria;
use Propel\Runtime\ActiveQuery\InstancePoolTrait;
use Propel\Runtime\Connection\ConnectionInterface;
use Propel\Runtime\DataFetcher\DataFetcherInterface;
use Propel\Runtime\Exception\PropelException;
use Propel\Runtime\Map\RelationMap;
use Propel\Runtime\Map\TableMap;
use Propel\Runtime\Map\TableMapTrait;


/**
 * This class defines the structure of the 'asso_tresors_archive' table.
 *
 *
 *
 * This map class is used by Propel to do runtime db structure discovery.
 * For example, the createSelectSql() method checks the type of a given column used in an
 * ORDER BY clause to know whether it needs to apply SQL to make the ORDER BY case-insensitive
 * (i.e. if it's a text column type).
 *
 */
class AssoTresorsArchiveTableMap extends TableMap
{
    use InstancePoolTrait;
    use TableMapTrait;

    /**
     * The (dot-path) name of this class
     */
    const CLASS_NAME = '.Map.AssoTresorsArchiveTableMap';

    /**
     * The default database name for this class
     */
    const DATABASE_NAME = 'simplasso';

    /**
     * The table name for this class
     */
    const TABLE_NAME = 'asso_tresors_archive';

    /**
     * The related Propel class for this table
     */
    const OM_CLASS = '\\AssoTresorsArchive';

    /**
     * A class that can be returned by this tableMap
     */
    const CLASS_DEFAULT = 'AssoTresorsArchive';

    /**
     * The total number of columns
     */
    const NUM_COLUMNS = 13;

    /**
     * The number of lazy-loaded columns
     */
    const NUM_LAZY_LOAD_COLUMNS = 0;

    /**
     * The number of columns to hydrate (NUM_COLUMNS - NUM_LAZY_LOAD_COLUMNS)
     */
    const NUM_HYDRATE_COLUMNS = 13;

    /**
     * the column name for the id_tresor field
     */
    const COL_ID_TRESOR = 'asso_tresors_archive.id_tresor';

    /**
     * the column name for the id_entite field
     */
    const COL_ID_ENTITE = 'asso_tresors_archive.id_entite';

    /**
     * the column name for the nom field
     */
    const COL_NOM = 'asso_tresors_archive.nom';

    /**
     * the column name for the nomcourt field
     */
    const COL_NOMCOURT = 'asso_tresors_archive.nomcourt';

    /**
     * the column name for the iban field
     */
    const COL_IBAN = 'asso_tresors_archive.iban';

    /**
     * the column name for the bic field
     */
    const COL_BIC = 'asso_tresors_archive.bic';

    /**
     * the column name for the id_compterb field
     */
    const COL_ID_COMPTERB = 'asso_tresors_archive.id_compterb';

    /**
     * the column name for the id_compte field
     */
    const COL_ID_COMPTE = 'asso_tresors_archive.id_compte';

    /**
     * the column name for the remise field
     */
    const COL_REMISE = 'asso_tresors_archive.remise';

    /**
     * the column name for the observation field
     */
    const COL_OBSERVATION = 'asso_tresors_archive.observation';

    /**
     * the column name for the created_at field
     */
    const COL_CREATED_AT = 'asso_tresors_archive.created_at';

    /**
     * the column name for the updated_at field
     */
    const COL_UPDATED_AT = 'asso_tresors_archive.updated_at';

    /**
     * the column name for the archived_at field
     */
    const COL_ARCHIVED_AT = 'asso_tresors_archive.archived_at';

    /**
     * The default string format for model objects of the related table
     */
    const DEFAULT_STRING_FORMAT = 'YAML';

    /**
     * holds an array of fieldnames
     *
     * first dimension keys are the type constants
     * e.g. self::$fieldNames[self::TYPE_PHPNAME][0] = 'Id'
     */
    protected static $fieldNames = array (
        self::TYPE_PHPNAME       => array('IdTresor', 'IdEntite', 'Nom', 'Nomcourt', 'Iban', 'Bic', 'IdCompterb', 'IdCompte', 'Remise', 'Observation', 'CreatedAt', 'UpdatedAt', 'ArchivedAt', ),
        self::TYPE_CAMELNAME     => array('idTresor', 'idEntite', 'nom', 'nomcourt', 'iban', 'bic', 'idCompterb', 'idCompte', 'remise', 'observation', 'createdAt', 'updatedAt', 'archivedAt', ),
        self::TYPE_COLNAME       => array(AssoTresorsArchiveTableMap::COL_ID_TRESOR, AssoTresorsArchiveTableMap::COL_ID_ENTITE, AssoTresorsArchiveTableMap::COL_NOM, AssoTresorsArchiveTableMap::COL_NOMCOURT, AssoTresorsArchiveTableMap::COL_IBAN, AssoTresorsArchiveTableMap::COL_BIC, AssoTresorsArchiveTableMap::COL_ID_COMPTERB, AssoTresorsArchiveTableMap::COL_ID_COMPTE, AssoTresorsArchiveTableMap::COL_REMISE, AssoTresorsArchiveTableMap::COL_OBSERVATION, AssoTresorsArchiveTableMap::COL_CREATED_AT, AssoTresorsArchiveTableMap::COL_UPDATED_AT, AssoTresorsArchiveTableMap::COL_ARCHIVED_AT, ),
        self::TYPE_FIELDNAME     => array('id_tresor', 'id_entite', 'nom', 'nomcourt', 'iban', 'bic', 'id_compterb', 'id_compte', 'remise', 'observation', 'created_at', 'updated_at', 'archived_at', ),
        self::TYPE_NUM           => array(0, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, )
    );

    /**
     * holds an array of keys for quick access to the fieldnames array
     *
     * first dimension keys are the type constants
     * e.g. self::$fieldKeys[self::TYPE_PHPNAME]['Id'] = 0
     */
    protected static $fieldKeys = array (
        self::TYPE_PHPNAME       => array('IdTresor' => 0, 'IdEntite' => 1, 'Nom' => 2, 'Nomcourt' => 3, 'Iban' => 4, 'Bic' => 5, 'IdCompterb' => 6, 'IdCompte' => 7, 'Remise' => 8, 'Observation' => 9, 'CreatedAt' => 10, 'UpdatedAt' => 11, 'ArchivedAt' => 12, ),
        self::TYPE_CAMELNAME     => array('idTresor' => 0, 'idEntite' => 1, 'nom' => 2, 'nomcourt' => 3, 'iban' => 4, 'bic' => 5, 'idCompterb' => 6, 'idCompte' => 7, 'remise' => 8, 'observation' => 9, 'createdAt' => 10, 'updatedAt' => 11, 'archivedAt' => 12, ),
        self::TYPE_COLNAME       => array(AssoTresorsArchiveTableMap::COL_ID_TRESOR => 0, AssoTresorsArchiveTableMap::COL_ID_ENTITE => 1, AssoTresorsArchiveTableMap::COL_NOM => 2, AssoTresorsArchiveTableMap::COL_NOMCOURT => 3, AssoTresorsArchiveTableMap::COL_IBAN => 4, AssoTresorsArchiveTableMap::COL_BIC => 5, AssoTresorsArchiveTableMap::COL_ID_COMPTERB => 6, AssoTresorsArchiveTableMap::COL_ID_COMPTE => 7, AssoTresorsArchiveTableMap::COL_REMISE => 8, AssoTresorsArchiveTableMap::COL_OBSERVATION => 9, AssoTresorsArchiveTableMap::COL_CREATED_AT => 10, AssoTresorsArchiveTableMap::COL_UPDATED_AT => 11, AssoTresorsArchiveTableMap::COL_ARCHIVED_AT => 12, ),
        self::TYPE_FIELDNAME     => array('id_tresor' => 0, 'id_entite' => 1, 'nom' => 2, 'nomcourt' => 3, 'iban' => 4, 'bic' => 5, 'id_compterb' => 6, 'id_compte' => 7, 'remise' => 8, 'observation' => 9, 'created_at' => 10, 'updated_at' => 11, 'archived_at' => 12, ),
        self::TYPE_NUM           => array(0, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, )
    );

    /**
     * Initialize the table attributes and columns
     * Relations are not initialized by this method since they are lazy loaded
     *
     * @return void
     * @throws PropelException
     */
    public function initialize()
    {
        // attributes
        $this->setName('asso_tresors_archive');
        $this->setPhpName('AssoTresorsArchive');
        $this->setIdentifierQuoting(true);
        $this->setClassName('\\AssoTresorsArchive');
        $this->setPackage('');
        $this->setUseIdGenerator(false);
        // columns
        $this->addPrimaryKey('id_tresor', 'IdTresor', 'BIGINT', true, 21, null);
        $this->addColumn('id_entite', 'IdEntite', 'BIGINT', true, 21, 1);
        $this->addColumn('nom', 'Nom', 'VARCHAR', true, 25, 'Moyen paiement');
        $this->addColumn('nomcourt', 'Nomcourt', 'VARCHAR', true, 8, 'MP');
        $this->addColumn('iban', 'Iban', 'VARCHAR', true, 27, 'Iban');
        $this->addColumn('bic', 'Bic', 'VARCHAR', true, 12, 'code BIC');
        $this->addColumn('id_compterb', 'IdCompterb', 'BIGINT', false, 21, null);
        $this->addColumn('id_compte', 'IdCompte', 'BIGINT', false, 21, null);
        $this->addColumn('remise', 'Remise', 'BIGINT', true, 12, 0);
        $this->addColumn('observation', 'Observation', 'LONGVARCHAR', false, null, null);
        $this->addColumn('created_at', 'CreatedAt', 'TIMESTAMP', false, null, null);
        $this->addColumn('updated_at', 'UpdatedAt', 'TIMESTAMP', false, null, null);
        $this->addColumn('archived_at', 'ArchivedAt', 'TIMESTAMP', false, null, null);
    } // initialize()

    /**
     * Build the RelationMap objects for this table relationships
     */
    public function buildRelations()
    {
    } // buildRelations()

    /**
     * Retrieves a string version of the primary key from the DB resultset row that can be used to uniquely identify a row in this table.
     *
     * For tables with a single-column primary key, that simple pkey value will be returned.  For tables with
     * a multi-column primary key, a serialize()d version of the primary key will be returned.
     *
     * @param array  $row       resultset row.
     * @param int    $offset    The 0-based offset for reading from the resultset row.
     * @param string $indexType One of the class type constants TableMap::TYPE_PHPNAME, TableMap::TYPE_CAMELNAME
     *                           TableMap::TYPE_COLNAME, TableMap::TYPE_FIELDNAME, TableMap::TYPE_NUM
     *
     * @return string The primary key hash of the row
     */
    public static function getPrimaryKeyHashFromRow($row, $offset = 0, $indexType = TableMap::TYPE_NUM)
    {
        // If the PK cannot be derived from the row, return NULL.
        if ($row[TableMap::TYPE_NUM == $indexType ? 0 + $offset : static::translateFieldName('IdTresor', TableMap::TYPE_PHPNAME, $indexType)] === null) {
            return null;
        }

        return null === $row[TableMap::TYPE_NUM == $indexType ? 0 + $offset : static::translateFieldName('IdTresor', TableMap::TYPE_PHPNAME, $indexType)] || is_scalar($row[TableMap::TYPE_NUM == $indexType ? 0 + $offset : static::translateFieldName('IdTresor', TableMap::TYPE_PHPNAME, $indexType)]) || is_callable([$row[TableMap::TYPE_NUM == $indexType ? 0 + $offset : static::translateFieldName('IdTresor', TableMap::TYPE_PHPNAME, $indexType)], '__toString']) ? (string) $row[TableMap::TYPE_NUM == $indexType ? 0 + $offset : static::translateFieldName('IdTresor', TableMap::TYPE_PHPNAME, $indexType)] : $row[TableMap::TYPE_NUM == $indexType ? 0 + $offset : static::translateFieldName('IdTresor', TableMap::TYPE_PHPNAME, $indexType)];
    }

    /**
     * Retrieves the primary key from the DB resultset row
     * For tables with a single-column primary key, that simple pkey value will be returned.  For tables with
     * a multi-column primary key, an array of the primary key columns will be returned.
     *
     * @param array  $row       resultset row.
     * @param int    $offset    The 0-based offset for reading from the resultset row.
     * @param string $indexType One of the class type constants TableMap::TYPE_PHPNAME, TableMap::TYPE_CAMELNAME
     *                           TableMap::TYPE_COLNAME, TableMap::TYPE_FIELDNAME, TableMap::TYPE_NUM
     *
     * @return mixed The primary key of the row
     */
    public static function getPrimaryKeyFromRow($row, $offset = 0, $indexType = TableMap::TYPE_NUM)
    {
        return (string) $row[
            $indexType == TableMap::TYPE_NUM
                ? 0 + $offset
                : self::translateFieldName('IdTresor', TableMap::TYPE_PHPNAME, $indexType)
        ];
    }

    /**
     * The class that the tableMap will make instances of.
     *
     * If $withPrefix is true, the returned path
     * uses a dot-path notation which is translated into a path
     * relative to a location on the PHP include_path.
     * (e.g. path.to.MyClass -> 'path/to/MyClass.php')
     *
     * @param boolean $withPrefix Whether or not to return the path with the class name
     * @return string path.to.ClassName
     */
    public static function getOMClass($withPrefix = true)
    {
        return $withPrefix ? AssoTresorsArchiveTableMap::CLASS_DEFAULT : AssoTresorsArchiveTableMap::OM_CLASS;
    }

    /**
     * Populates an object of the default type or an object that inherit from the default.
     *
     * @param array  $row       row returned by DataFetcher->fetch().
     * @param int    $offset    The 0-based offset for reading from the resultset row.
     * @param string $indexType The index type of $row. Mostly DataFetcher->getIndexType().
                                 One of the class type constants TableMap::TYPE_PHPNAME, TableMap::TYPE_CAMELNAME
     *                           TableMap::TYPE_COLNAME, TableMap::TYPE_FIELDNAME, TableMap::TYPE_NUM.
     *
     * @throws PropelException Any exceptions caught during processing will be
     *                         rethrown wrapped into a PropelException.
     * @return array           (AssoTresorsArchive object, last column rank)
     */
    public static function populateObject($row, $offset = 0, $indexType = TableMap::TYPE_NUM)
    {
        $key = AssoTresorsArchiveTableMap::getPrimaryKeyHashFromRow($row, $offset, $indexType);
        if (null !== ($obj = AssoTresorsArchiveTableMap::getInstanceFromPool($key))) {
            // We no longer rehydrate the object, since this can cause data loss.
            // See http://www.propelorm.org/ticket/509
            // $obj->hydrate($row, $offset, true); // rehydrate
            $col = $offset + AssoTresorsArchiveTableMap::NUM_HYDRATE_COLUMNS;
        } else {
            $cls = AssoTresorsArchiveTableMap::OM_CLASS;
            /** @var AssoTresorsArchive $obj */
            $obj = new $cls();
            $col = $obj->hydrate($row, $offset, false, $indexType);
            AssoTresorsArchiveTableMap::addInstanceToPool($obj, $key);
        }

        return array($obj, $col);
    }

    /**
     * The returned array will contain objects of the default type or
     * objects that inherit from the default.
     *
     * @param DataFetcherInterface $dataFetcher
     * @return array
     * @throws PropelException Any exceptions caught during processing will be
     *                         rethrown wrapped into a PropelException.
     */
    public static function populateObjects(DataFetcherInterface $dataFetcher)
    {
        $results = array();

        // set the class once to avoid overhead in the loop
        $cls = static::getOMClass(false);
        // populate the object(s)
        while ($row = $dataFetcher->fetch()) {
            $key = AssoTresorsArchiveTableMap::getPrimaryKeyHashFromRow($row, 0, $dataFetcher->getIndexType());
            if (null !== ($obj = AssoTresorsArchiveTableMap::getInstanceFromPool($key))) {
                // We no longer rehydrate the object, since this can cause data loss.
                // See http://www.propelorm.org/ticket/509
                // $obj->hydrate($row, 0, true); // rehydrate
                $results[] = $obj;
            } else {
                /** @var AssoTresorsArchive $obj */
                $obj = new $cls();
                $obj->hydrate($row);
                $results[] = $obj;
                AssoTresorsArchiveTableMap::addInstanceToPool($obj, $key);
            } // if key exists
        }

        return $results;
    }
    /**
     * Add all the columns needed to create a new object.
     *
     * Note: any columns that were marked with lazyLoad="true" in the
     * XML schema will not be added to the select list and only loaded
     * on demand.
     *
     * @param Criteria $criteria object containing the columns to add.
     * @param string   $alias    optional table alias
     * @throws PropelException Any exceptions caught during processing will be
     *                         rethrown wrapped into a PropelException.
     */
    public static function addSelectColumns(Criteria $criteria, $alias = null)
    {
        if (null === $alias) {
            $criteria->addSelectColumn(AssoTresorsArchiveTableMap::COL_ID_TRESOR);
            $criteria->addSelectColumn(AssoTresorsArchiveTableMap::COL_ID_ENTITE);
            $criteria->addSelectColumn(AssoTresorsArchiveTableMap::COL_NOM);
            $criteria->addSelectColumn(AssoTresorsArchiveTableMap::COL_NOMCOURT);
            $criteria->addSelectColumn(AssoTresorsArchiveTableMap::COL_IBAN);
            $criteria->addSelectColumn(AssoTresorsArchiveTableMap::COL_BIC);
            $criteria->addSelectColumn(AssoTresorsArchiveTableMap::COL_ID_COMPTERB);
            $criteria->addSelectColumn(AssoTresorsArchiveTableMap::COL_ID_COMPTE);
            $criteria->addSelectColumn(AssoTresorsArchiveTableMap::COL_REMISE);
            $criteria->addSelectColumn(AssoTresorsArchiveTableMap::COL_OBSERVATION);
            $criteria->addSelectColumn(AssoTresorsArchiveTableMap::COL_CREATED_AT);
            $criteria->addSelectColumn(AssoTresorsArchiveTableMap::COL_UPDATED_AT);
            $criteria->addSelectColumn(AssoTresorsArchiveTableMap::COL_ARCHIVED_AT);
        } else {
            $criteria->addSelectColumn($alias . '.id_tresor');
            $criteria->addSelectColumn($alias . '.id_entite');
            $criteria->addSelectColumn($alias . '.nom');
            $criteria->addSelectColumn($alias . '.nomcourt');
            $criteria->addSelectColumn($alias . '.iban');
            $criteria->addSelectColumn($alias . '.bic');
            $criteria->addSelectColumn($alias . '.id_compterb');
            $criteria->addSelectColumn($alias . '.id_compte');
            $criteria->addSelectColumn($alias . '.remise');
            $criteria->addSelectColumn($alias . '.observation');
            $criteria->addSelectColumn($alias . '.created_at');
            $criteria->addSelectColumn($alias . '.updated_at');
            $criteria->addSelectColumn($alias . '.archived_at');
        }
    }

    /**
     * Returns the TableMap related to this object.
     * This method is not needed for general use but a specific application could have a need.
     * @return TableMap
     * @throws PropelException Any exceptions caught during processing will be
     *                         rethrown wrapped into a PropelException.
     */
    public static function getTableMap()
    {
        return Propel::getServiceContainer()->getDatabaseMap(AssoTresorsArchiveTableMap::DATABASE_NAME)->getTable(AssoTresorsArchiveTableMap::TABLE_NAME);
    }

    /**
     * Add a TableMap instance to the database for this tableMap class.
     */
    public static function buildTableMap()
    {
        $dbMap = Propel::getServiceContainer()->getDatabaseMap(AssoTresorsArchiveTableMap::DATABASE_NAME);
        if (!$dbMap->hasTable(AssoTresorsArchiveTableMap::TABLE_NAME)) {
            $dbMap->addTableObject(new AssoTresorsArchiveTableMap());
        }
    }

    /**
     * Performs a DELETE on the database, given a AssoTresorsArchive or Criteria object OR a primary key value.
     *
     * @param mixed               $values Criteria or AssoTresorsArchive object or primary key or array of primary keys
     *              which is used to create the DELETE statement
     * @param  ConnectionInterface $con the connection to use
     * @return int             The number of affected rows (if supported by underlying database driver).  This includes CASCADE-related rows
     *                         if supported by native driver or if emulated using Propel.
     * @throws PropelException Any exceptions caught during processing will be
     *                         rethrown wrapped into a PropelException.
     */
     public static function doDelete($values, ConnectionInterface $con = null)
     {
        if (null === $con) {
            $con = Propel::getServiceContainer()->getWriteConnection(AssoTresorsArchiveTableMap::DATABASE_NAME);
        }

        if ($values instanceof Criteria) {
            // rename for clarity
            $criteria = $values;
        } elseif ($values instanceof \AssoTresorsArchive) { // it's a model object
            // create criteria based on pk values
            $criteria = $values->buildPkeyCriteria();
        } else { // it's a primary key, or an array of pks
            $criteria = new Criteria(AssoTresorsArchiveTableMap::DATABASE_NAME);
            $criteria->add(AssoTresorsArchiveTableMap::COL_ID_TRESOR, (array) $values, Criteria::IN);
        }

        $query = AssoTresorsArchiveQuery::create()->mergeWith($criteria);

        if ($values instanceof Criteria) {
            AssoTresorsArchiveTableMap::clearInstancePool();
        } elseif (!is_object($values)) { // it's a primary key, or an array of pks
            foreach ((array) $values as $singleval) {
                AssoTresorsArchiveTableMap::removeInstanceFromPool($singleval);
            }
        }

        return $query->delete($con);
    }

    /**
     * Deletes all rows from the asso_tresors_archive table.
     *
     * @param ConnectionInterface $con the connection to use
     * @return int The number of affected rows (if supported by underlying database driver).
     */
    public static function doDeleteAll(ConnectionInterface $con = null)
    {
        return AssoTresorsArchiveQuery::create()->doDeleteAll($con);
    }

    /**
     * Performs an INSERT on the database, given a AssoTresorsArchive or Criteria object.
     *
     * @param mixed               $criteria Criteria or AssoTresorsArchive object containing data that is used to create the INSERT statement.
     * @param ConnectionInterface $con the ConnectionInterface connection to use
     * @return mixed           The new primary key.
     * @throws PropelException Any exceptions caught during processing will be
     *                         rethrown wrapped into a PropelException.
     */
    public static function doInsert($criteria, ConnectionInterface $con = null)
    {
        if (null === $con) {
            $con = Propel::getServiceContainer()->getWriteConnection(AssoTresorsArchiveTableMap::DATABASE_NAME);
        }

        if ($criteria instanceof Criteria) {
            $criteria = clone $criteria; // rename for clarity
        } else {
            $criteria = $criteria->buildCriteria(); // build Criteria from AssoTresorsArchive object
        }


        // Set the correct dbName
        $query = AssoTresorsArchiveQuery::create()->mergeWith($criteria);

        // use transaction because $criteria could contain info
        // for more than one table (I guess, conceivably)
        return $con->transaction(function () use ($con, $query) {
            return $query->doInsert($con);
        });
    }

} // AssoTresorsArchiveTableMap
// This is the static code needed to register the TableMap for this table with the main Propel class.
//
AssoTresorsArchiveTableMap::buildTableMap();
