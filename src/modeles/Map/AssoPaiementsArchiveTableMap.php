<?php

namespace Map;

use \AssoPaiementsArchive;
use \AssoPaiementsArchiveQuery;
use Propel\Runtime\Propel;
use Propel\Runtime\ActiveQuery\Criteria;
use Propel\Runtime\ActiveQuery\InstancePoolTrait;
use Propel\Runtime\Connection\ConnectionInterface;
use Propel\Runtime\DataFetcher\DataFetcherInterface;
use Propel\Runtime\Exception\PropelException;
use Propel\Runtime\Map\RelationMap;
use Propel\Runtime\Map\TableMap;
use Propel\Runtime\Map\TableMapTrait;


/**
 * This class defines the structure of the 'asso_paiements_archive' table.
 *
 *
 *
 * This map class is used by Propel to do runtime db structure discovery.
 * For example, the createSelectSql() method checks the type of a given column used in an
 * ORDER BY clause to know whether it needs to apply SQL to make the ORDER BY case-insensitive
 * (i.e. if it's a text column type).
 *
 */
class AssoPaiementsArchiveTableMap extends TableMap
{
    use InstancePoolTrait;
    use TableMapTrait;

    /**
     * The (dot-path) name of this class
     */
    const CLASS_NAME = '.Map.AssoPaiementsArchiveTableMap';

    /**
     * The default database name for this class
     */
    const DATABASE_NAME = 'simplasso';

    /**
     * The table name for this class
     */
    const TABLE_NAME = 'asso_paiements_archive';

    /**
     * The related Propel class for this table
     */
    const OM_CLASS = '\\AssoPaiementsArchive';

    /**
     * A class that can be returned by this tableMap
     */
    const CLASS_DEFAULT = 'AssoPaiementsArchive';

    /**
     * The total number of columns
     */
    const NUM_COLUMNS = 15;

    /**
     * The number of lazy-loaded columns
     */
    const NUM_LAZY_LOAD_COLUMNS = 0;

    /**
     * The number of columns to hydrate (NUM_COLUMNS - NUM_LAZY_LOAD_COLUMNS)
     */
    const NUM_HYDRATE_COLUMNS = 15;

    /**
     * the column name for the id_paiement field
     */
    const COL_ID_PAIEMENT = 'asso_paiements_archive.id_paiement';

    /**
     * the column name for the objet field
     */
    const COL_OBJET = 'asso_paiements_archive.objet';

    /**
     * the column name for the id_objet field
     */
    const COL_ID_OBJET = 'asso_paiements_archive.id_objet';

    /**
     * the column name for the id_entite field
     */
    const COL_ID_ENTITE = 'asso_paiements_archive.id_entite';

    /**
     * the column name for the id_tresor field
     */
    const COL_ID_TRESOR = 'asso_paiements_archive.id_tresor';

    /**
     * the column name for the montant field
     */
    const COL_MONTANT = 'asso_paiements_archive.montant';

    /**
     * the column name for the numero field
     */
    const COL_NUMERO = 'asso_paiements_archive.numero';

    /**
     * the column name for the date_cheque field
     */
    const COL_DATE_CHEQUE = 'asso_paiements_archive.date_cheque';

    /**
     * the column name for the date_enregistrement field
     */
    const COL_DATE_ENREGISTREMENT = 'asso_paiements_archive.date_enregistrement';

    /**
     * the column name for the remise field
     */
    const COL_REMISE = 'asso_paiements_archive.remise';

    /**
     * the column name for the comptabilise field
     */
    const COL_COMPTABILISE = 'asso_paiements_archive.comptabilise';

    /**
     * the column name for the observation field
     */
    const COL_OBSERVATION = 'asso_paiements_archive.observation';

    /**
     * the column name for the created_at field
     */
    const COL_CREATED_AT = 'asso_paiements_archive.created_at';

    /**
     * the column name for the updated_at field
     */
    const COL_UPDATED_AT = 'asso_paiements_archive.updated_at';

    /**
     * the column name for the archived_at field
     */
    const COL_ARCHIVED_AT = 'asso_paiements_archive.archived_at';

    /**
     * The default string format for model objects of the related table
     */
    const DEFAULT_STRING_FORMAT = 'YAML';

    /**
     * holds an array of fieldnames
     *
     * first dimension keys are the type constants
     * e.g. self::$fieldNames[self::TYPE_PHPNAME][0] = 'Id'
     */
    protected static $fieldNames = array (
        self::TYPE_PHPNAME       => array('IdPaiement', 'Objet', 'IdObjet', 'IdEntite', 'IdTresor', 'Montant', 'Numero', 'DateCheque', 'DateEnregistrement', 'Remise', 'Comptabilise', 'Observation', 'CreatedAt', 'UpdatedAt', 'ArchivedAt', ),
        self::TYPE_CAMELNAME     => array('idPaiement', 'objet', 'idObjet', 'idEntite', 'idTresor', 'montant', 'numero', 'dateCheque', 'dateEnregistrement', 'remise', 'comptabilise', 'observation', 'createdAt', 'updatedAt', 'archivedAt', ),
        self::TYPE_COLNAME       => array(AssoPaiementsArchiveTableMap::COL_ID_PAIEMENT, AssoPaiementsArchiveTableMap::COL_OBJET, AssoPaiementsArchiveTableMap::COL_ID_OBJET, AssoPaiementsArchiveTableMap::COL_ID_ENTITE, AssoPaiementsArchiveTableMap::COL_ID_TRESOR, AssoPaiementsArchiveTableMap::COL_MONTANT, AssoPaiementsArchiveTableMap::COL_NUMERO, AssoPaiementsArchiveTableMap::COL_DATE_CHEQUE, AssoPaiementsArchiveTableMap::COL_DATE_ENREGISTREMENT, AssoPaiementsArchiveTableMap::COL_REMISE, AssoPaiementsArchiveTableMap::COL_COMPTABILISE, AssoPaiementsArchiveTableMap::COL_OBSERVATION, AssoPaiementsArchiveTableMap::COL_CREATED_AT, AssoPaiementsArchiveTableMap::COL_UPDATED_AT, AssoPaiementsArchiveTableMap::COL_ARCHIVED_AT, ),
        self::TYPE_FIELDNAME     => array('id_paiement', 'objet', 'id_objet', 'id_entite', 'id_tresor', 'montant', 'numero', 'date_cheque', 'date_enregistrement', 'remise', 'comptabilise', 'observation', 'created_at', 'updated_at', 'archived_at', ),
        self::TYPE_NUM           => array(0, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, )
    );

    /**
     * holds an array of keys for quick access to the fieldnames array
     *
     * first dimension keys are the type constants
     * e.g. self::$fieldKeys[self::TYPE_PHPNAME]['Id'] = 0
     */
    protected static $fieldKeys = array (
        self::TYPE_PHPNAME       => array('IdPaiement' => 0, 'Objet' => 1, 'IdObjet' => 2, 'IdEntite' => 3, 'IdTresor' => 4, 'Montant' => 5, 'Numero' => 6, 'DateCheque' => 7, 'DateEnregistrement' => 8, 'Remise' => 9, 'Comptabilise' => 10, 'Observation' => 11, 'CreatedAt' => 12, 'UpdatedAt' => 13, 'ArchivedAt' => 14, ),
        self::TYPE_CAMELNAME     => array('idPaiement' => 0, 'objet' => 1, 'idObjet' => 2, 'idEntite' => 3, 'idTresor' => 4, 'montant' => 5, 'numero' => 6, 'dateCheque' => 7, 'dateEnregistrement' => 8, 'remise' => 9, 'comptabilise' => 10, 'observation' => 11, 'createdAt' => 12, 'updatedAt' => 13, 'archivedAt' => 14, ),
        self::TYPE_COLNAME       => array(AssoPaiementsArchiveTableMap::COL_ID_PAIEMENT => 0, AssoPaiementsArchiveTableMap::COL_OBJET => 1, AssoPaiementsArchiveTableMap::COL_ID_OBJET => 2, AssoPaiementsArchiveTableMap::COL_ID_ENTITE => 3, AssoPaiementsArchiveTableMap::COL_ID_TRESOR => 4, AssoPaiementsArchiveTableMap::COL_MONTANT => 5, AssoPaiementsArchiveTableMap::COL_NUMERO => 6, AssoPaiementsArchiveTableMap::COL_DATE_CHEQUE => 7, AssoPaiementsArchiveTableMap::COL_DATE_ENREGISTREMENT => 8, AssoPaiementsArchiveTableMap::COL_REMISE => 9, AssoPaiementsArchiveTableMap::COL_COMPTABILISE => 10, AssoPaiementsArchiveTableMap::COL_OBSERVATION => 11, AssoPaiementsArchiveTableMap::COL_CREATED_AT => 12, AssoPaiementsArchiveTableMap::COL_UPDATED_AT => 13, AssoPaiementsArchiveTableMap::COL_ARCHIVED_AT => 14, ),
        self::TYPE_FIELDNAME     => array('id_paiement' => 0, 'objet' => 1, 'id_objet' => 2, 'id_entite' => 3, 'id_tresor' => 4, 'montant' => 5, 'numero' => 6, 'date_cheque' => 7, 'date_enregistrement' => 8, 'remise' => 9, 'comptabilise' => 10, 'observation' => 11, 'created_at' => 12, 'updated_at' => 13, 'archived_at' => 14, ),
        self::TYPE_NUM           => array(0, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, )
    );

    /**
     * Initialize the table attributes and columns
     * Relations are not initialized by this method since they are lazy loaded
     *
     * @return void
     * @throws PropelException
     */
    public function initialize()
    {
        // attributes
        $this->setName('asso_paiements_archive');
        $this->setPhpName('AssoPaiementsArchive');
        $this->setIdentifierQuoting(true);
        $this->setClassName('\\AssoPaiementsArchive');
        $this->setPackage('');
        $this->setUseIdGenerator(false);
        // columns
        $this->addPrimaryKey('id_paiement', 'IdPaiement', 'BIGINT', true, null, null);
        $this->addColumn('objet', 'Objet', 'VARCHAR', true, 25, 'membre');
        $this->addColumn('id_objet', 'IdObjet', 'BIGINT', true, null, 0);
        $this->addColumn('id_entite', 'IdEntite', 'BIGINT', true, 21, null);
        $this->addColumn('id_tresor', 'IdTresor', 'BIGINT', true, 21, null);
        $this->addColumn('montant', 'Montant', 'DOUBLE', false, 12, 0);
        $this->addColumn('numero', 'Numero', 'VARCHAR', false, 25, null);
        $this->addColumn('date_cheque', 'DateCheque', 'DATE', false, null, null);
        $this->addColumn('date_enregistrement', 'DateEnregistrement', 'DATE', false, null, null);
        $this->addColumn('remise', 'Remise', 'BIGINT', false, 12, 0);
        $this->addColumn('comptabilise', 'Comptabilise', 'BOOLEAN', false, 1, false);
        $this->addColumn('observation', 'Observation', 'LONGVARCHAR', false, null, null);
        $this->addColumn('created_at', 'CreatedAt', 'TIMESTAMP', false, null, null);
        $this->addColumn('updated_at', 'UpdatedAt', 'TIMESTAMP', false, null, null);
        $this->addColumn('archived_at', 'ArchivedAt', 'TIMESTAMP', false, null, null);
    } // initialize()

    /**
     * Build the RelationMap objects for this table relationships
     */
    public function buildRelations()
    {
    } // buildRelations()

    /**
     * Retrieves a string version of the primary key from the DB resultset row that can be used to uniquely identify a row in this table.
     *
     * For tables with a single-column primary key, that simple pkey value will be returned.  For tables with
     * a multi-column primary key, a serialize()d version of the primary key will be returned.
     *
     * @param array  $row       resultset row.
     * @param int    $offset    The 0-based offset for reading from the resultset row.
     * @param string $indexType One of the class type constants TableMap::TYPE_PHPNAME, TableMap::TYPE_CAMELNAME
     *                           TableMap::TYPE_COLNAME, TableMap::TYPE_FIELDNAME, TableMap::TYPE_NUM
     *
     * @return string The primary key hash of the row
     */
    public static function getPrimaryKeyHashFromRow($row, $offset = 0, $indexType = TableMap::TYPE_NUM)
    {
        // If the PK cannot be derived from the row, return NULL.
        if ($row[TableMap::TYPE_NUM == $indexType ? 0 + $offset : static::translateFieldName('IdPaiement', TableMap::TYPE_PHPNAME, $indexType)] === null) {
            return null;
        }

        return null === $row[TableMap::TYPE_NUM == $indexType ? 0 + $offset : static::translateFieldName('IdPaiement', TableMap::TYPE_PHPNAME, $indexType)] || is_scalar($row[TableMap::TYPE_NUM == $indexType ? 0 + $offset : static::translateFieldName('IdPaiement', TableMap::TYPE_PHPNAME, $indexType)]) || is_callable([$row[TableMap::TYPE_NUM == $indexType ? 0 + $offset : static::translateFieldName('IdPaiement', TableMap::TYPE_PHPNAME, $indexType)], '__toString']) ? (string) $row[TableMap::TYPE_NUM == $indexType ? 0 + $offset : static::translateFieldName('IdPaiement', TableMap::TYPE_PHPNAME, $indexType)] : $row[TableMap::TYPE_NUM == $indexType ? 0 + $offset : static::translateFieldName('IdPaiement', TableMap::TYPE_PHPNAME, $indexType)];
    }

    /**
     * Retrieves the primary key from the DB resultset row
     * For tables with a single-column primary key, that simple pkey value will be returned.  For tables with
     * a multi-column primary key, an array of the primary key columns will be returned.
     *
     * @param array  $row       resultset row.
     * @param int    $offset    The 0-based offset for reading from the resultset row.
     * @param string $indexType One of the class type constants TableMap::TYPE_PHPNAME, TableMap::TYPE_CAMELNAME
     *                           TableMap::TYPE_COLNAME, TableMap::TYPE_FIELDNAME, TableMap::TYPE_NUM
     *
     * @return mixed The primary key of the row
     */
    public static function getPrimaryKeyFromRow($row, $offset = 0, $indexType = TableMap::TYPE_NUM)
    {
        return (string) $row[
            $indexType == TableMap::TYPE_NUM
                ? 0 + $offset
                : self::translateFieldName('IdPaiement', TableMap::TYPE_PHPNAME, $indexType)
        ];
    }

    /**
     * The class that the tableMap will make instances of.
     *
     * If $withPrefix is true, the returned path
     * uses a dot-path notation which is translated into a path
     * relative to a location on the PHP include_path.
     * (e.g. path.to.MyClass -> 'path/to/MyClass.php')
     *
     * @param boolean $withPrefix Whether or not to return the path with the class name
     * @return string path.to.ClassName
     */
    public static function getOMClass($withPrefix = true)
    {
        return $withPrefix ? AssoPaiementsArchiveTableMap::CLASS_DEFAULT : AssoPaiementsArchiveTableMap::OM_CLASS;
    }

    /**
     * Populates an object of the default type or an object that inherit from the default.
     *
     * @param array  $row       row returned by DataFetcher->fetch().
     * @param int    $offset    The 0-based offset for reading from the resultset row.
     * @param string $indexType The index type of $row. Mostly DataFetcher->getIndexType().
                                 One of the class type constants TableMap::TYPE_PHPNAME, TableMap::TYPE_CAMELNAME
     *                           TableMap::TYPE_COLNAME, TableMap::TYPE_FIELDNAME, TableMap::TYPE_NUM.
     *
     * @throws PropelException Any exceptions caught during processing will be
     *                         rethrown wrapped into a PropelException.
     * @return array           (AssoPaiementsArchive object, last column rank)
     */
    public static function populateObject($row, $offset = 0, $indexType = TableMap::TYPE_NUM)
    {
        $key = AssoPaiementsArchiveTableMap::getPrimaryKeyHashFromRow($row, $offset, $indexType);
        if (null !== ($obj = AssoPaiementsArchiveTableMap::getInstanceFromPool($key))) {
            // We no longer rehydrate the object, since this can cause data loss.
            // See http://www.propelorm.org/ticket/509
            // $obj->hydrate($row, $offset, true); // rehydrate
            $col = $offset + AssoPaiementsArchiveTableMap::NUM_HYDRATE_COLUMNS;
        } else {
            $cls = AssoPaiementsArchiveTableMap::OM_CLASS;
            /** @var AssoPaiementsArchive $obj */
            $obj = new $cls();
            $col = $obj->hydrate($row, $offset, false, $indexType);
            AssoPaiementsArchiveTableMap::addInstanceToPool($obj, $key);
        }

        return array($obj, $col);
    }

    /**
     * The returned array will contain objects of the default type or
     * objects that inherit from the default.
     *
     * @param DataFetcherInterface $dataFetcher
     * @return array
     * @throws PropelException Any exceptions caught during processing will be
     *                         rethrown wrapped into a PropelException.
     */
    public static function populateObjects(DataFetcherInterface $dataFetcher)
    {
        $results = array();

        // set the class once to avoid overhead in the loop
        $cls = static::getOMClass(false);
        // populate the object(s)
        while ($row = $dataFetcher->fetch()) {
            $key = AssoPaiementsArchiveTableMap::getPrimaryKeyHashFromRow($row, 0, $dataFetcher->getIndexType());
            if (null !== ($obj = AssoPaiementsArchiveTableMap::getInstanceFromPool($key))) {
                // We no longer rehydrate the object, since this can cause data loss.
                // See http://www.propelorm.org/ticket/509
                // $obj->hydrate($row, 0, true); // rehydrate
                $results[] = $obj;
            } else {
                /** @var AssoPaiementsArchive $obj */
                $obj = new $cls();
                $obj->hydrate($row);
                $results[] = $obj;
                AssoPaiementsArchiveTableMap::addInstanceToPool($obj, $key);
            } // if key exists
        }

        return $results;
    }
    /**
     * Add all the columns needed to create a new object.
     *
     * Note: any columns that were marked with lazyLoad="true" in the
     * XML schema will not be added to the select list and only loaded
     * on demand.
     *
     * @param Criteria $criteria object containing the columns to add.
     * @param string   $alias    optional table alias
     * @throws PropelException Any exceptions caught during processing will be
     *                         rethrown wrapped into a PropelException.
     */
    public static function addSelectColumns(Criteria $criteria, $alias = null)
    {
        if (null === $alias) {
            $criteria->addSelectColumn(AssoPaiementsArchiveTableMap::COL_ID_PAIEMENT);
            $criteria->addSelectColumn(AssoPaiementsArchiveTableMap::COL_OBJET);
            $criteria->addSelectColumn(AssoPaiementsArchiveTableMap::COL_ID_OBJET);
            $criteria->addSelectColumn(AssoPaiementsArchiveTableMap::COL_ID_ENTITE);
            $criteria->addSelectColumn(AssoPaiementsArchiveTableMap::COL_ID_TRESOR);
            $criteria->addSelectColumn(AssoPaiementsArchiveTableMap::COL_MONTANT);
            $criteria->addSelectColumn(AssoPaiementsArchiveTableMap::COL_NUMERO);
            $criteria->addSelectColumn(AssoPaiementsArchiveTableMap::COL_DATE_CHEQUE);
            $criteria->addSelectColumn(AssoPaiementsArchiveTableMap::COL_DATE_ENREGISTREMENT);
            $criteria->addSelectColumn(AssoPaiementsArchiveTableMap::COL_REMISE);
            $criteria->addSelectColumn(AssoPaiementsArchiveTableMap::COL_COMPTABILISE);
            $criteria->addSelectColumn(AssoPaiementsArchiveTableMap::COL_OBSERVATION);
            $criteria->addSelectColumn(AssoPaiementsArchiveTableMap::COL_CREATED_AT);
            $criteria->addSelectColumn(AssoPaiementsArchiveTableMap::COL_UPDATED_AT);
            $criteria->addSelectColumn(AssoPaiementsArchiveTableMap::COL_ARCHIVED_AT);
        } else {
            $criteria->addSelectColumn($alias . '.id_paiement');
            $criteria->addSelectColumn($alias . '.objet');
            $criteria->addSelectColumn($alias . '.id_objet');
            $criteria->addSelectColumn($alias . '.id_entite');
            $criteria->addSelectColumn($alias . '.id_tresor');
            $criteria->addSelectColumn($alias . '.montant');
            $criteria->addSelectColumn($alias . '.numero');
            $criteria->addSelectColumn($alias . '.date_cheque');
            $criteria->addSelectColumn($alias . '.date_enregistrement');
            $criteria->addSelectColumn($alias . '.remise');
            $criteria->addSelectColumn($alias . '.comptabilise');
            $criteria->addSelectColumn($alias . '.observation');
            $criteria->addSelectColumn($alias . '.created_at');
            $criteria->addSelectColumn($alias . '.updated_at');
            $criteria->addSelectColumn($alias . '.archived_at');
        }
    }

    /**
     * Returns the TableMap related to this object.
     * This method is not needed for general use but a specific application could have a need.
     * @return TableMap
     * @throws PropelException Any exceptions caught during processing will be
     *                         rethrown wrapped into a PropelException.
     */
    public static function getTableMap()
    {
        return Propel::getServiceContainer()->getDatabaseMap(AssoPaiementsArchiveTableMap::DATABASE_NAME)->getTable(AssoPaiementsArchiveTableMap::TABLE_NAME);
    }

    /**
     * Add a TableMap instance to the database for this tableMap class.
     */
    public static function buildTableMap()
    {
        $dbMap = Propel::getServiceContainer()->getDatabaseMap(AssoPaiementsArchiveTableMap::DATABASE_NAME);
        if (!$dbMap->hasTable(AssoPaiementsArchiveTableMap::TABLE_NAME)) {
            $dbMap->addTableObject(new AssoPaiementsArchiveTableMap());
        }
    }

    /**
     * Performs a DELETE on the database, given a AssoPaiementsArchive or Criteria object OR a primary key value.
     *
     * @param mixed               $values Criteria or AssoPaiementsArchive object or primary key or array of primary keys
     *              which is used to create the DELETE statement
     * @param  ConnectionInterface $con the connection to use
     * @return int             The number of affected rows (if supported by underlying database driver).  This includes CASCADE-related rows
     *                         if supported by native driver or if emulated using Propel.
     * @throws PropelException Any exceptions caught during processing will be
     *                         rethrown wrapped into a PropelException.
     */
     public static function doDelete($values, ConnectionInterface $con = null)
     {
        if (null === $con) {
            $con = Propel::getServiceContainer()->getWriteConnection(AssoPaiementsArchiveTableMap::DATABASE_NAME);
        }

        if ($values instanceof Criteria) {
            // rename for clarity
            $criteria = $values;
        } elseif ($values instanceof \AssoPaiementsArchive) { // it's a model object
            // create criteria based on pk values
            $criteria = $values->buildPkeyCriteria();
        } else { // it's a primary key, or an array of pks
            $criteria = new Criteria(AssoPaiementsArchiveTableMap::DATABASE_NAME);
            $criteria->add(AssoPaiementsArchiveTableMap::COL_ID_PAIEMENT, (array) $values, Criteria::IN);
        }

        $query = AssoPaiementsArchiveQuery::create()->mergeWith($criteria);

        if ($values instanceof Criteria) {
            AssoPaiementsArchiveTableMap::clearInstancePool();
        } elseif (!is_object($values)) { // it's a primary key, or an array of pks
            foreach ((array) $values as $singleval) {
                AssoPaiementsArchiveTableMap::removeInstanceFromPool($singleval);
            }
        }

        return $query->delete($con);
    }

    /**
     * Deletes all rows from the asso_paiements_archive table.
     *
     * @param ConnectionInterface $con the connection to use
     * @return int The number of affected rows (if supported by underlying database driver).
     */
    public static function doDeleteAll(ConnectionInterface $con = null)
    {
        return AssoPaiementsArchiveQuery::create()->doDeleteAll($con);
    }

    /**
     * Performs an INSERT on the database, given a AssoPaiementsArchive or Criteria object.
     *
     * @param mixed               $criteria Criteria or AssoPaiementsArchive object containing data that is used to create the INSERT statement.
     * @param ConnectionInterface $con the ConnectionInterface connection to use
     * @return mixed           The new primary key.
     * @throws PropelException Any exceptions caught during processing will be
     *                         rethrown wrapped into a PropelException.
     */
    public static function doInsert($criteria, ConnectionInterface $con = null)
    {
        if (null === $con) {
            $con = Propel::getServiceContainer()->getWriteConnection(AssoPaiementsArchiveTableMap::DATABASE_NAME);
        }

        if ($criteria instanceof Criteria) {
            $criteria = clone $criteria; // rename for clarity
        } else {
            $criteria = $criteria->buildCriteria(); // build Criteria from AssoPaiementsArchive object
        }


        // Set the correct dbName
        $query = AssoPaiementsArchiveQuery::create()->mergeWith($criteria);

        // use transaction because $criteria could contain info
        // for more than one table (I guess, conceivably)
        return $con->transaction(function () use ($con, $query) {
            return $query->doInsert($con);
        });
    }

} // AssoPaiementsArchiveTableMap
// This is the static code needed to register the TableMap for this table with the main Propel class.
//
AssoPaiementsArchiveTableMap::buildTableMap();
