<?php

namespace Map;

use \AssoMotgroupesArchive;
use \AssoMotgroupesArchiveQuery;
use Propel\Runtime\Propel;
use Propel\Runtime\ActiveQuery\Criteria;
use Propel\Runtime\ActiveQuery\InstancePoolTrait;
use Propel\Runtime\Connection\ConnectionInterface;
use Propel\Runtime\DataFetcher\DataFetcherInterface;
use Propel\Runtime\Exception\PropelException;
use Propel\Runtime\Map\RelationMap;
use Propel\Runtime\Map\TableMap;
use Propel\Runtime\Map\TableMapTrait;


/**
 * This class defines the structure of the 'asso_motgroupes_archive' table.
 *
 *
 *
 * This map class is used by Propel to do runtime db structure discovery.
 * For example, the createSelectSql() method checks the type of a given column used in an
 * ORDER BY clause to know whether it needs to apply SQL to make the ORDER BY case-insensitive
 * (i.e. if it's a text column type).
 *
 */
class AssoMotgroupesArchiveTableMap extends TableMap
{
    use InstancePoolTrait;
    use TableMapTrait;

    /**
     * The (dot-path) name of this class
     */
    const CLASS_NAME = '.Map.AssoMotgroupesArchiveTableMap';

    /**
     * The default database name for this class
     */
    const DATABASE_NAME = 'simplasso';

    /**
     * The table name for this class
     */
    const TABLE_NAME = 'asso_motgroupes_archive';

    /**
     * The related Propel class for this table
     */
    const OM_CLASS = '\\AssoMotgroupesArchive';

    /**
     * A class that can be returned by this tableMap
     */
    const CLASS_DEFAULT = 'AssoMotgroupesArchive';

    /**
     * The total number of columns
     */
    const NUM_COLUMNS = 14;

    /**
     * The number of lazy-loaded columns
     */
    const NUM_LAZY_LOAD_COLUMNS = 0;

    /**
     * The number of columns to hydrate (NUM_COLUMNS - NUM_LAZY_LOAD_COLUMNS)
     */
    const NUM_HYDRATE_COLUMNS = 14;

    /**
     * the column name for the id_motgroupe field
     */
    const COL_ID_MOTGROUPE = 'asso_motgroupes_archive.id_motgroupe';

    /**
     * the column name for the id_parent field
     */
    const COL_ID_PARENT = 'asso_motgroupes_archive.id_parent';

    /**
     * the column name for the nom field
     */
    const COL_NOM = 'asso_motgroupes_archive.nom';

    /**
     * the column name for the nomcourt field
     */
    const COL_NOMCOURT = 'asso_motgroupes_archive.nomcourt';

    /**
     * the column name for the descriptif field
     */
    const COL_DESCRIPTIF = 'asso_motgroupes_archive.descriptif';

    /**
     * the column name for the texte field
     */
    const COL_TEXTE = 'asso_motgroupes_archive.texte';

    /**
     * the column name for the importance field
     */
    const COL_IMPORTANCE = 'asso_motgroupes_archive.importance';

    /**
     * the column name for the objets_en_lien field
     */
    const COL_OBJETS_EN_LIEN = 'asso_motgroupes_archive.objets_en_lien';

    /**
     * the column name for the systeme field
     */
    const COL_SYSTEME = 'asso_motgroupes_archive.systeme';

    /**
     * the column name for the actif field
     */
    const COL_ACTIF = 'asso_motgroupes_archive.actif';

    /**
     * the column name for the options field
     */
    const COL_OPTIONS = 'asso_motgroupes_archive.options';

    /**
     * the column name for the created_at field
     */
    const COL_CREATED_AT = 'asso_motgroupes_archive.created_at';

    /**
     * the column name for the updated_at field
     */
    const COL_UPDATED_AT = 'asso_motgroupes_archive.updated_at';

    /**
     * the column name for the archived_at field
     */
    const COL_ARCHIVED_AT = 'asso_motgroupes_archive.archived_at';

    /**
     * The default string format for model objects of the related table
     */
    const DEFAULT_STRING_FORMAT = 'YAML';

    /**
     * holds an array of fieldnames
     *
     * first dimension keys are the type constants
     * e.g. self::$fieldNames[self::TYPE_PHPNAME][0] = 'Id'
     */
    protected static $fieldNames = array (
        self::TYPE_PHPNAME       => array('IdMotgroupe', 'IdParent', 'Nom', 'Nomcourt', 'Descriptif', 'Texte', 'Importance', 'ObjetsEnLien', 'Systeme', 'Actif', 'Options', 'CreatedAt', 'UpdatedAt', 'ArchivedAt', ),
        self::TYPE_CAMELNAME     => array('idMotgroupe', 'idParent', 'nom', 'nomcourt', 'descriptif', 'texte', 'importance', 'objetsEnLien', 'systeme', 'actif', 'options', 'createdAt', 'updatedAt', 'archivedAt', ),
        self::TYPE_COLNAME       => array(AssoMotgroupesArchiveTableMap::COL_ID_MOTGROUPE, AssoMotgroupesArchiveTableMap::COL_ID_PARENT, AssoMotgroupesArchiveTableMap::COL_NOM, AssoMotgroupesArchiveTableMap::COL_NOMCOURT, AssoMotgroupesArchiveTableMap::COL_DESCRIPTIF, AssoMotgroupesArchiveTableMap::COL_TEXTE, AssoMotgroupesArchiveTableMap::COL_IMPORTANCE, AssoMotgroupesArchiveTableMap::COL_OBJETS_EN_LIEN, AssoMotgroupesArchiveTableMap::COL_SYSTEME, AssoMotgroupesArchiveTableMap::COL_ACTIF, AssoMotgroupesArchiveTableMap::COL_OPTIONS, AssoMotgroupesArchiveTableMap::COL_CREATED_AT, AssoMotgroupesArchiveTableMap::COL_UPDATED_AT, AssoMotgroupesArchiveTableMap::COL_ARCHIVED_AT, ),
        self::TYPE_FIELDNAME     => array('id_motgroupe', 'id_parent', 'nom', 'nomcourt', 'descriptif', 'texte', 'importance', 'objets_en_lien', 'systeme', 'actif', 'options', 'created_at', 'updated_at', 'archived_at', ),
        self::TYPE_NUM           => array(0, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, )
    );

    /**
     * holds an array of keys for quick access to the fieldnames array
     *
     * first dimension keys are the type constants
     * e.g. self::$fieldKeys[self::TYPE_PHPNAME]['Id'] = 0
     */
    protected static $fieldKeys = array (
        self::TYPE_PHPNAME       => array('IdMotgroupe' => 0, 'IdParent' => 1, 'Nom' => 2, 'Nomcourt' => 3, 'Descriptif' => 4, 'Texte' => 5, 'Importance' => 6, 'ObjetsEnLien' => 7, 'Systeme' => 8, 'Actif' => 9, 'Options' => 10, 'CreatedAt' => 11, 'UpdatedAt' => 12, 'ArchivedAt' => 13, ),
        self::TYPE_CAMELNAME     => array('idMotgroupe' => 0, 'idParent' => 1, 'nom' => 2, 'nomcourt' => 3, 'descriptif' => 4, 'texte' => 5, 'importance' => 6, 'objetsEnLien' => 7, 'systeme' => 8, 'actif' => 9, 'options' => 10, 'createdAt' => 11, 'updatedAt' => 12, 'archivedAt' => 13, ),
        self::TYPE_COLNAME       => array(AssoMotgroupesArchiveTableMap::COL_ID_MOTGROUPE => 0, AssoMotgroupesArchiveTableMap::COL_ID_PARENT => 1, AssoMotgroupesArchiveTableMap::COL_NOM => 2, AssoMotgroupesArchiveTableMap::COL_NOMCOURT => 3, AssoMotgroupesArchiveTableMap::COL_DESCRIPTIF => 4, AssoMotgroupesArchiveTableMap::COL_TEXTE => 5, AssoMotgroupesArchiveTableMap::COL_IMPORTANCE => 6, AssoMotgroupesArchiveTableMap::COL_OBJETS_EN_LIEN => 7, AssoMotgroupesArchiveTableMap::COL_SYSTEME => 8, AssoMotgroupesArchiveTableMap::COL_ACTIF => 9, AssoMotgroupesArchiveTableMap::COL_OPTIONS => 10, AssoMotgroupesArchiveTableMap::COL_CREATED_AT => 11, AssoMotgroupesArchiveTableMap::COL_UPDATED_AT => 12, AssoMotgroupesArchiveTableMap::COL_ARCHIVED_AT => 13, ),
        self::TYPE_FIELDNAME     => array('id_motgroupe' => 0, 'id_parent' => 1, 'nom' => 2, 'nomcourt' => 3, 'descriptif' => 4, 'texte' => 5, 'importance' => 6, 'objets_en_lien' => 7, 'systeme' => 8, 'actif' => 9, 'options' => 10, 'created_at' => 11, 'updated_at' => 12, 'archived_at' => 13, ),
        self::TYPE_NUM           => array(0, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, )
    );

    /**
     * Initialize the table attributes and columns
     * Relations are not initialized by this method since they are lazy loaded
     *
     * @return void
     * @throws PropelException
     */
    public function initialize()
    {
        // attributes
        $this->setName('asso_motgroupes_archive');
        $this->setPhpName('AssoMotgroupesArchive');
        $this->setIdentifierQuoting(true);
        $this->setClassName('\\AssoMotgroupesArchive');
        $this->setPackage('');
        $this->setUseIdGenerator(false);
        // columns
        $this->addPrimaryKey('id_motgroupe', 'IdMotgroupe', 'BIGINT', true, 21, null);
        $this->addColumn('id_parent', 'IdParent', 'BIGINT', true, 21, 0);
        $this->addColumn('nom', 'Nom', 'VARCHAR', true, 80, null);
        $this->addColumn('nomcourt', 'Nomcourt', 'VARCHAR', false, 12, '');
        $this->addColumn('descriptif', 'Descriptif', 'VARCHAR', false, 255, null);
        $this->addColumn('texte', 'Texte', 'CLOB', false, null, null);
        $this->addColumn('importance', 'Importance', 'INTEGER', true, 1, 1);
        $this->addColumn('objets_en_lien', 'ObjetsEnLien', 'CLOB', true, null, null);
        $this->addColumn('systeme', 'Systeme', 'BOOLEAN', false, 1, false);
        $this->addColumn('actif', 'Actif', 'INTEGER', true, 1, 1);
        $this->addColumn('options', 'Options', 'CLOB', false, null, null);
        $this->addColumn('created_at', 'CreatedAt', 'TIMESTAMP', false, null, null);
        $this->addColumn('updated_at', 'UpdatedAt', 'TIMESTAMP', false, null, null);
        $this->addColumn('archived_at', 'ArchivedAt', 'TIMESTAMP', false, null, null);
    } // initialize()

    /**
     * Build the RelationMap objects for this table relationships
     */
    public function buildRelations()
    {
    } // buildRelations()

    /**
     * Retrieves a string version of the primary key from the DB resultset row that can be used to uniquely identify a row in this table.
     *
     * For tables with a single-column primary key, that simple pkey value will be returned.  For tables with
     * a multi-column primary key, a serialize()d version of the primary key will be returned.
     *
     * @param array  $row       resultset row.
     * @param int    $offset    The 0-based offset for reading from the resultset row.
     * @param string $indexType One of the class type constants TableMap::TYPE_PHPNAME, TableMap::TYPE_CAMELNAME
     *                           TableMap::TYPE_COLNAME, TableMap::TYPE_FIELDNAME, TableMap::TYPE_NUM
     *
     * @return string The primary key hash of the row
     */
    public static function getPrimaryKeyHashFromRow($row, $offset = 0, $indexType = TableMap::TYPE_NUM)
    {
        // If the PK cannot be derived from the row, return NULL.
        if ($row[TableMap::TYPE_NUM == $indexType ? 0 + $offset : static::translateFieldName('IdMotgroupe', TableMap::TYPE_PHPNAME, $indexType)] === null) {
            return null;
        }

        return null === $row[TableMap::TYPE_NUM == $indexType ? 0 + $offset : static::translateFieldName('IdMotgroupe', TableMap::TYPE_PHPNAME, $indexType)] || is_scalar($row[TableMap::TYPE_NUM == $indexType ? 0 + $offset : static::translateFieldName('IdMotgroupe', TableMap::TYPE_PHPNAME, $indexType)]) || is_callable([$row[TableMap::TYPE_NUM == $indexType ? 0 + $offset : static::translateFieldName('IdMotgroupe', TableMap::TYPE_PHPNAME, $indexType)], '__toString']) ? (string) $row[TableMap::TYPE_NUM == $indexType ? 0 + $offset : static::translateFieldName('IdMotgroupe', TableMap::TYPE_PHPNAME, $indexType)] : $row[TableMap::TYPE_NUM == $indexType ? 0 + $offset : static::translateFieldName('IdMotgroupe', TableMap::TYPE_PHPNAME, $indexType)];
    }

    /**
     * Retrieves the primary key from the DB resultset row
     * For tables with a single-column primary key, that simple pkey value will be returned.  For tables with
     * a multi-column primary key, an array of the primary key columns will be returned.
     *
     * @param array  $row       resultset row.
     * @param int    $offset    The 0-based offset for reading from the resultset row.
     * @param string $indexType One of the class type constants TableMap::TYPE_PHPNAME, TableMap::TYPE_CAMELNAME
     *                           TableMap::TYPE_COLNAME, TableMap::TYPE_FIELDNAME, TableMap::TYPE_NUM
     *
     * @return mixed The primary key of the row
     */
    public static function getPrimaryKeyFromRow($row, $offset = 0, $indexType = TableMap::TYPE_NUM)
    {
        return (string) $row[
            $indexType == TableMap::TYPE_NUM
                ? 0 + $offset
                : self::translateFieldName('IdMotgroupe', TableMap::TYPE_PHPNAME, $indexType)
        ];
    }

    /**
     * The class that the tableMap will make instances of.
     *
     * If $withPrefix is true, the returned path
     * uses a dot-path notation which is translated into a path
     * relative to a location on the PHP include_path.
     * (e.g. path.to.MyClass -> 'path/to/MyClass.php')
     *
     * @param boolean $withPrefix Whether or not to return the path with the class name
     * @return string path.to.ClassName
     */
    public static function getOMClass($withPrefix = true)
    {
        return $withPrefix ? AssoMotgroupesArchiveTableMap::CLASS_DEFAULT : AssoMotgroupesArchiveTableMap::OM_CLASS;
    }

    /**
     * Populates an object of the default type or an object that inherit from the default.
     *
     * @param array  $row       row returned by DataFetcher->fetch().
     * @param int    $offset    The 0-based offset for reading from the resultset row.
     * @param string $indexType The index type of $row. Mostly DataFetcher->getIndexType().
                                 One of the class type constants TableMap::TYPE_PHPNAME, TableMap::TYPE_CAMELNAME
     *                           TableMap::TYPE_COLNAME, TableMap::TYPE_FIELDNAME, TableMap::TYPE_NUM.
     *
     * @throws PropelException Any exceptions caught during processing will be
     *                         rethrown wrapped into a PropelException.
     * @return array           (AssoMotgroupesArchive object, last column rank)
     */
    public static function populateObject($row, $offset = 0, $indexType = TableMap::TYPE_NUM)
    {
        $key = AssoMotgroupesArchiveTableMap::getPrimaryKeyHashFromRow($row, $offset, $indexType);
        if (null !== ($obj = AssoMotgroupesArchiveTableMap::getInstanceFromPool($key))) {
            // We no longer rehydrate the object, since this can cause data loss.
            // See http://www.propelorm.org/ticket/509
            // $obj->hydrate($row, $offset, true); // rehydrate
            $col = $offset + AssoMotgroupesArchiveTableMap::NUM_HYDRATE_COLUMNS;
        } else {
            $cls = AssoMotgroupesArchiveTableMap::OM_CLASS;
            /** @var AssoMotgroupesArchive $obj */
            $obj = new $cls();
            $col = $obj->hydrate($row, $offset, false, $indexType);
            AssoMotgroupesArchiveTableMap::addInstanceToPool($obj, $key);
        }

        return array($obj, $col);
    }

    /**
     * The returned array will contain objects of the default type or
     * objects that inherit from the default.
     *
     * @param DataFetcherInterface $dataFetcher
     * @return array
     * @throws PropelException Any exceptions caught during processing will be
     *                         rethrown wrapped into a PropelException.
     */
    public static function populateObjects(DataFetcherInterface $dataFetcher)
    {
        $results = array();

        // set the class once to avoid overhead in the loop
        $cls = static::getOMClass(false);
        // populate the object(s)
        while ($row = $dataFetcher->fetch()) {
            $key = AssoMotgroupesArchiveTableMap::getPrimaryKeyHashFromRow($row, 0, $dataFetcher->getIndexType());
            if (null !== ($obj = AssoMotgroupesArchiveTableMap::getInstanceFromPool($key))) {
                // We no longer rehydrate the object, since this can cause data loss.
                // See http://www.propelorm.org/ticket/509
                // $obj->hydrate($row, 0, true); // rehydrate
                $results[] = $obj;
            } else {
                /** @var AssoMotgroupesArchive $obj */
                $obj = new $cls();
                $obj->hydrate($row);
                $results[] = $obj;
                AssoMotgroupesArchiveTableMap::addInstanceToPool($obj, $key);
            } // if key exists
        }

        return $results;
    }
    /**
     * Add all the columns needed to create a new object.
     *
     * Note: any columns that were marked with lazyLoad="true" in the
     * XML schema will not be added to the select list and only loaded
     * on demand.
     *
     * @param Criteria $criteria object containing the columns to add.
     * @param string   $alias    optional table alias
     * @throws PropelException Any exceptions caught during processing will be
     *                         rethrown wrapped into a PropelException.
     */
    public static function addSelectColumns(Criteria $criteria, $alias = null)
    {
        if (null === $alias) {
            $criteria->addSelectColumn(AssoMotgroupesArchiveTableMap::COL_ID_MOTGROUPE);
            $criteria->addSelectColumn(AssoMotgroupesArchiveTableMap::COL_ID_PARENT);
            $criteria->addSelectColumn(AssoMotgroupesArchiveTableMap::COL_NOM);
            $criteria->addSelectColumn(AssoMotgroupesArchiveTableMap::COL_NOMCOURT);
            $criteria->addSelectColumn(AssoMotgroupesArchiveTableMap::COL_DESCRIPTIF);
            $criteria->addSelectColumn(AssoMotgroupesArchiveTableMap::COL_TEXTE);
            $criteria->addSelectColumn(AssoMotgroupesArchiveTableMap::COL_IMPORTANCE);
            $criteria->addSelectColumn(AssoMotgroupesArchiveTableMap::COL_OBJETS_EN_LIEN);
            $criteria->addSelectColumn(AssoMotgroupesArchiveTableMap::COL_SYSTEME);
            $criteria->addSelectColumn(AssoMotgroupesArchiveTableMap::COL_ACTIF);
            $criteria->addSelectColumn(AssoMotgroupesArchiveTableMap::COL_OPTIONS);
            $criteria->addSelectColumn(AssoMotgroupesArchiveTableMap::COL_CREATED_AT);
            $criteria->addSelectColumn(AssoMotgroupesArchiveTableMap::COL_UPDATED_AT);
            $criteria->addSelectColumn(AssoMotgroupesArchiveTableMap::COL_ARCHIVED_AT);
        } else {
            $criteria->addSelectColumn($alias . '.id_motgroupe');
            $criteria->addSelectColumn($alias . '.id_parent');
            $criteria->addSelectColumn($alias . '.nom');
            $criteria->addSelectColumn($alias . '.nomcourt');
            $criteria->addSelectColumn($alias . '.descriptif');
            $criteria->addSelectColumn($alias . '.texte');
            $criteria->addSelectColumn($alias . '.importance');
            $criteria->addSelectColumn($alias . '.objets_en_lien');
            $criteria->addSelectColumn($alias . '.systeme');
            $criteria->addSelectColumn($alias . '.actif');
            $criteria->addSelectColumn($alias . '.options');
            $criteria->addSelectColumn($alias . '.created_at');
            $criteria->addSelectColumn($alias . '.updated_at');
            $criteria->addSelectColumn($alias . '.archived_at');
        }
    }

    /**
     * Returns the TableMap related to this object.
     * This method is not needed for general use but a specific application could have a need.
     * @return TableMap
     * @throws PropelException Any exceptions caught during processing will be
     *                         rethrown wrapped into a PropelException.
     */
    public static function getTableMap()
    {
        return Propel::getServiceContainer()->getDatabaseMap(AssoMotgroupesArchiveTableMap::DATABASE_NAME)->getTable(AssoMotgroupesArchiveTableMap::TABLE_NAME);
    }

    /**
     * Add a TableMap instance to the database for this tableMap class.
     */
    public static function buildTableMap()
    {
        $dbMap = Propel::getServiceContainer()->getDatabaseMap(AssoMotgroupesArchiveTableMap::DATABASE_NAME);
        if (!$dbMap->hasTable(AssoMotgroupesArchiveTableMap::TABLE_NAME)) {
            $dbMap->addTableObject(new AssoMotgroupesArchiveTableMap());
        }
    }

    /**
     * Performs a DELETE on the database, given a AssoMotgroupesArchive or Criteria object OR a primary key value.
     *
     * @param mixed               $values Criteria or AssoMotgroupesArchive object or primary key or array of primary keys
     *              which is used to create the DELETE statement
     * @param  ConnectionInterface $con the connection to use
     * @return int             The number of affected rows (if supported by underlying database driver).  This includes CASCADE-related rows
     *                         if supported by native driver or if emulated using Propel.
     * @throws PropelException Any exceptions caught during processing will be
     *                         rethrown wrapped into a PropelException.
     */
     public static function doDelete($values, ConnectionInterface $con = null)
     {
        if (null === $con) {
            $con = Propel::getServiceContainer()->getWriteConnection(AssoMotgroupesArchiveTableMap::DATABASE_NAME);
        }

        if ($values instanceof Criteria) {
            // rename for clarity
            $criteria = $values;
        } elseif ($values instanceof \AssoMotgroupesArchive) { // it's a model object
            // create criteria based on pk values
            $criteria = $values->buildPkeyCriteria();
        } else { // it's a primary key, or an array of pks
            $criteria = new Criteria(AssoMotgroupesArchiveTableMap::DATABASE_NAME);
            $criteria->add(AssoMotgroupesArchiveTableMap::COL_ID_MOTGROUPE, (array) $values, Criteria::IN);
        }

        $query = AssoMotgroupesArchiveQuery::create()->mergeWith($criteria);

        if ($values instanceof Criteria) {
            AssoMotgroupesArchiveTableMap::clearInstancePool();
        } elseif (!is_object($values)) { // it's a primary key, or an array of pks
            foreach ((array) $values as $singleval) {
                AssoMotgroupesArchiveTableMap::removeInstanceFromPool($singleval);
            }
        }

        return $query->delete($con);
    }

    /**
     * Deletes all rows from the asso_motgroupes_archive table.
     *
     * @param ConnectionInterface $con the connection to use
     * @return int The number of affected rows (if supported by underlying database driver).
     */
    public static function doDeleteAll(ConnectionInterface $con = null)
    {
        return AssoMotgroupesArchiveQuery::create()->doDeleteAll($con);
    }

    /**
     * Performs an INSERT on the database, given a AssoMotgroupesArchive or Criteria object.
     *
     * @param mixed               $criteria Criteria or AssoMotgroupesArchive object containing data that is used to create the INSERT statement.
     * @param ConnectionInterface $con the ConnectionInterface connection to use
     * @return mixed           The new primary key.
     * @throws PropelException Any exceptions caught during processing will be
     *                         rethrown wrapped into a PropelException.
     */
    public static function doInsert($criteria, ConnectionInterface $con = null)
    {
        if (null === $con) {
            $con = Propel::getServiceContainer()->getWriteConnection(AssoMotgroupesArchiveTableMap::DATABASE_NAME);
        }

        if ($criteria instanceof Criteria) {
            $criteria = clone $criteria; // rename for clarity
        } else {
            $criteria = $criteria->buildCriteria(); // build Criteria from AssoMotgroupesArchive object
        }


        // Set the correct dbName
        $query = AssoMotgroupesArchiveQuery::create()->mergeWith($criteria);

        // use transaction because $criteria could contain info
        // for more than one table (I guess, conceivably)
        return $con->transaction(function () use ($con, $query) {
            return $query->doInsert($con);
        });
    }

} // AssoMotgroupesArchiveTableMap
// This is the static code needed to register the TableMap for this table with the main Propel class.
//
AssoMotgroupesArchiveTableMap::buildTableMap();
