<?php

namespace Map;

use \Membre;
use \MembreQuery;
use Propel\Runtime\Propel;
use Propel\Runtime\ActiveQuery\Criteria;
use Propel\Runtime\ActiveQuery\InstancePoolTrait;
use Propel\Runtime\Connection\ConnectionInterface;
use Propel\Runtime\DataFetcher\DataFetcherInterface;
use Propel\Runtime\Exception\PropelException;
use Propel\Runtime\Map\RelationMap;
use Propel\Runtime\Map\TableMap;
use Propel\Runtime\Map\TableMapTrait;


/**
 * This class defines the structure of the 'asso_membres' table.
 *
 *
 *
 * This map class is used by Propel to do runtime db structure discovery.
 * For example, the createSelectSql() method checks the type of a given column used in an
 * ORDER BY clause to know whether it needs to apply SQL to make the ORDER BY case-insensitive
 * (i.e. if it's a text column type).
 *
 */
class MembreTableMap extends TableMap
{
    use InstancePoolTrait;
    use TableMapTrait;

    /**
     * The (dot-path) name of this class
     */
    const CLASS_NAME = '.Map.MembreTableMap';

    /**
     * The default database name for this class
     */
    const DATABASE_NAME = 'simplasso';

    /**
     * The table name for this class
     */
    const TABLE_NAME = 'asso_membres';

    /**
     * The related Propel class for this table
     */
    const OM_CLASS = '\\Membre';

    /**
     * A class that can be returned by this tableMap
     */
    const CLASS_DEFAULT = 'Membre';

    /**
     * The total number of columns
     */
    const NUM_COLUMNS = 10;

    /**
     * The number of lazy-loaded columns
     */
    const NUM_LAZY_LOAD_COLUMNS = 0;

    /**
     * The number of columns to hydrate (NUM_COLUMNS - NUM_LAZY_LOAD_COLUMNS)
     */
    const NUM_HYDRATE_COLUMNS = 10;

    /**
     * the column name for the id_membre field
     */
    const COL_ID_MEMBRE = 'asso_membres.id_membre';

    /**
     * the column name for the id_individu_titulaire field
     */
    const COL_ID_INDIVIDU_TITULAIRE = 'asso_membres.id_individu_titulaire';

    /**
     * the column name for the identifiant_interne field
     */
    const COL_IDENTIFIANT_INTERNE = 'asso_membres.identifiant_interne';

    /**
     * the column name for the civilite field
     */
    const COL_CIVILITE = 'asso_membres.civilite';

    /**
     * the column name for the nom field
     */
    const COL_NOM = 'asso_membres.nom';

    /**
     * the column name for the nomcourt field
     */
    const COL_NOMCOURT = 'asso_membres.nomcourt';

    /**
     * the column name for the date_sortie field
     */
    const COL_DATE_SORTIE = 'asso_membres.date_sortie';

    /**
     * the column name for the observation field
     */
    const COL_OBSERVATION = 'asso_membres.observation';

    /**
     * the column name for the created_at field
     */
    const COL_CREATED_AT = 'asso_membres.created_at';

    /**
     * the column name for the updated_at field
     */
    const COL_UPDATED_AT = 'asso_membres.updated_at';

    /**
     * The default string format for model objects of the related table
     */
    const DEFAULT_STRING_FORMAT = 'YAML';

    /**
     * holds an array of fieldnames
     *
     * first dimension keys are the type constants
     * e.g. self::$fieldNames[self::TYPE_PHPNAME][0] = 'Id'
     */
    protected static $fieldNames = array (
        self::TYPE_PHPNAME       => array('IdMembre', 'IdIndividuTitulaire', 'IdentifiantInterne', 'Civilite', 'Nom', 'Nomcourt', 'DateSortie', 'Observation', 'CreatedAt', 'UpdatedAt', ),
        self::TYPE_CAMELNAME     => array('idMembre', 'idIndividuTitulaire', 'identifiantInterne', 'civilite', 'nom', 'nomcourt', 'dateSortie', 'observation', 'createdAt', 'updatedAt', ),
        self::TYPE_COLNAME       => array(MembreTableMap::COL_ID_MEMBRE, MembreTableMap::COL_ID_INDIVIDU_TITULAIRE, MembreTableMap::COL_IDENTIFIANT_INTERNE, MembreTableMap::COL_CIVILITE, MembreTableMap::COL_NOM, MembreTableMap::COL_NOMCOURT, MembreTableMap::COL_DATE_SORTIE, MembreTableMap::COL_OBSERVATION, MembreTableMap::COL_CREATED_AT, MembreTableMap::COL_UPDATED_AT, ),
        self::TYPE_FIELDNAME     => array('id_membre', 'id_individu_titulaire', 'identifiant_interne', 'civilite', 'nom', 'nomcourt', 'date_sortie', 'observation', 'created_at', 'updated_at', ),
        self::TYPE_NUM           => array(0, 1, 2, 3, 4, 5, 6, 7, 8, 9, )
    );

    /**
     * holds an array of keys for quick access to the fieldnames array
     *
     * first dimension keys are the type constants
     * e.g. self::$fieldKeys[self::TYPE_PHPNAME]['Id'] = 0
     */
    protected static $fieldKeys = array (
        self::TYPE_PHPNAME       => array('IdMembre' => 0, 'IdIndividuTitulaire' => 1, 'IdentifiantInterne' => 2, 'Civilite' => 3, 'Nom' => 4, 'Nomcourt' => 5, 'DateSortie' => 6, 'Observation' => 7, 'CreatedAt' => 8, 'UpdatedAt' => 9, ),
        self::TYPE_CAMELNAME     => array('idMembre' => 0, 'idIndividuTitulaire' => 1, 'identifiantInterne' => 2, 'civilite' => 3, 'nom' => 4, 'nomcourt' => 5, 'dateSortie' => 6, 'observation' => 7, 'createdAt' => 8, 'updatedAt' => 9, ),
        self::TYPE_COLNAME       => array(MembreTableMap::COL_ID_MEMBRE => 0, MembreTableMap::COL_ID_INDIVIDU_TITULAIRE => 1, MembreTableMap::COL_IDENTIFIANT_INTERNE => 2, MembreTableMap::COL_CIVILITE => 3, MembreTableMap::COL_NOM => 4, MembreTableMap::COL_NOMCOURT => 5, MembreTableMap::COL_DATE_SORTIE => 6, MembreTableMap::COL_OBSERVATION => 7, MembreTableMap::COL_CREATED_AT => 8, MembreTableMap::COL_UPDATED_AT => 9, ),
        self::TYPE_FIELDNAME     => array('id_membre' => 0, 'id_individu_titulaire' => 1, 'identifiant_interne' => 2, 'civilite' => 3, 'nom' => 4, 'nomcourt' => 5, 'date_sortie' => 6, 'observation' => 7, 'created_at' => 8, 'updated_at' => 9, ),
        self::TYPE_NUM           => array(0, 1, 2, 3, 4, 5, 6, 7, 8, 9, )
    );

    /**
     * Initialize the table attributes and columns
     * Relations are not initialized by this method since they are lazy loaded
     *
     * @return void
     * @throws PropelException
     */
    public function initialize()
    {
        // attributes
        $this->setName('asso_membres');
        $this->setPhpName('Membre');
        $this->setIdentifierQuoting(true);
        $this->setClassName('\\Membre');
        $this->setPackage('');
        $this->setUseIdGenerator(true);
        // columns
        $this->addPrimaryKey('id_membre', 'IdMembre', 'BIGINT', true, 21, null);
        $this->addForeignKey('id_individu_titulaire', 'IdIndividuTitulaire', 'BIGINT', 'asso_individus', 'id_individu', true, 21, null);
        $this->addColumn('identifiant_interne', 'IdentifiantInterne', 'VARCHAR', false, 40, null);
        $this->addColumn('civilite', 'Civilite', 'VARCHAR', false, 40, null);
        $this->addColumn('nom', 'Nom', 'VARCHAR', true, 250, null);
        $this->addColumn('nomcourt', 'Nomcourt', 'VARCHAR', false, 6, null);
        $this->addColumn('date_sortie', 'DateSortie', 'DATE', false, null, null);
        $this->addColumn('observation', 'Observation', 'LONGVARCHAR', false, null, null);
        $this->addColumn('created_at', 'CreatedAt', 'TIMESTAMP', false, null, null);
        $this->addColumn('updated_at', 'UpdatedAt', 'TIMESTAMP', false, null, null);
    } // initialize()

    /**
     * Build the RelationMap objects for this table relationships
     */
    public function buildRelations()
    {
        $this->addRelation('IndividuTitulaire', '\\Individu', RelationMap::MANY_TO_ONE, array (
  0 =>
  array (
    0 => ':id_individu_titulaire',
    1 => ':id_individu',
  ),
), 'CASCADE', null, null, false);
        $this->addRelation('MembreIndividu', '\\MembreIndividu', RelationMap::ONE_TO_MANY, array (
  0 =>
  array (
    0 => ':id_membre',
    1 => ':id_membre',
  ),
), 'CASCADE', null, 'MembreIndividus', false);
        $this->addRelation('Servicerendu', '\\Servicerendu', RelationMap::ONE_TO_MANY, array (
  0 =>
  array (
    0 => ':id_membre',
    1 => ':id_membre',
  ),
), 'CASCADE', null, 'Servicerendus', false);
        $this->addRelation('CourrierLien', '\\CourrierLien', RelationMap::ONE_TO_MANY, array (
  0 =>
  array (
    0 => ':id_membre',
    1 => ':id_membre',
  ),
), 'CASCADE', null, 'CourrierLiens', false);
        $this->addRelation('Individu', '\\Individu', RelationMap::MANY_TO_MANY, array(), 'CASCADE', null, 'Individus');
    } // buildRelations()

    /**
     *
     * Gets the list of behaviors registered for this table
     *
     * @return array Associative array (name => parameters) of behaviors
     */
    public function getBehaviors()
    {
        return array(
            'timestampable' => array('create_column' => 'created_at', 'update_column' => 'updated_at', 'disable_created_at' => 'false', 'disable_updated_at' => 'false', ),
            'archivable' => array('archive_table' => '', 'archive_phpname' => '', 'archive_class' => '', 'log_archived_at' => 'true', 'archived_at_column' => 'archived_at', 'archive_on_insert' => 'false', 'archive_on_update' => 'false', 'archive_on_delete' => 'true', ),
        );
    } // getBehaviors()
    /**
     * Method to invalidate the instance pool of all tables related to asso_membres     * by a foreign key with ON DELETE CASCADE
     */
    public static function clearRelatedInstancePool()
    {
        // Invalidate objects in related instance pools,
        // since one or more of them may be deleted by ON DELETE CASCADE/SETNULL rule.
        MembreIndividuTableMap::clearInstancePool();
        ServicerenduTableMap::clearInstancePool();
        CourrierLienTableMap::clearInstancePool();
    }

    /**
     * Retrieves a string version of the primary key from the DB resultset row that can be used to uniquely identify a row in this table.
     *
     * For tables with a single-column primary key, that simple pkey value will be returned.  For tables with
     * a multi-column primary key, a serialize()d version of the primary key will be returned.
     *
     * @param array  $row       resultset row.
     * @param int    $offset    The 0-based offset for reading from the resultset row.
     * @param string $indexType One of the class type constants TableMap::TYPE_PHPNAME, TableMap::TYPE_CAMELNAME
     *                           TableMap::TYPE_COLNAME, TableMap::TYPE_FIELDNAME, TableMap::TYPE_NUM
     *
     * @return string The primary key hash of the row
     */
    public static function getPrimaryKeyHashFromRow($row, $offset = 0, $indexType = TableMap::TYPE_NUM)
    {
        // If the PK cannot be derived from the row, return NULL.
        if ($row[TableMap::TYPE_NUM == $indexType ? 0 + $offset : static::translateFieldName('IdMembre', TableMap::TYPE_PHPNAME, $indexType)] === null) {
            return null;
        }

        return null === $row[TableMap::TYPE_NUM == $indexType ? 0 + $offset : static::translateFieldName('IdMembre', TableMap::TYPE_PHPNAME, $indexType)] || is_scalar($row[TableMap::TYPE_NUM == $indexType ? 0 + $offset : static::translateFieldName('IdMembre', TableMap::TYPE_PHPNAME, $indexType)]) || is_callable([$row[TableMap::TYPE_NUM == $indexType ? 0 + $offset : static::translateFieldName('IdMembre', TableMap::TYPE_PHPNAME, $indexType)], '__toString']) ? (string) $row[TableMap::TYPE_NUM == $indexType ? 0 + $offset : static::translateFieldName('IdMembre', TableMap::TYPE_PHPNAME, $indexType)] : $row[TableMap::TYPE_NUM == $indexType ? 0 + $offset : static::translateFieldName('IdMembre', TableMap::TYPE_PHPNAME, $indexType)];
    }

    /**
     * Retrieves the primary key from the DB resultset row
     * For tables with a single-column primary key, that simple pkey value will be returned.  For tables with
     * a multi-column primary key, an array of the primary key columns will be returned.
     *
     * @param array  $row       resultset row.
     * @param int    $offset    The 0-based offset for reading from the resultset row.
     * @param string $indexType One of the class type constants TableMap::TYPE_PHPNAME, TableMap::TYPE_CAMELNAME
     *                           TableMap::TYPE_COLNAME, TableMap::TYPE_FIELDNAME, TableMap::TYPE_NUM
     *
     * @return mixed The primary key of the row
     */
    public static function getPrimaryKeyFromRow($row, $offset = 0, $indexType = TableMap::TYPE_NUM)
    {
        return (string) $row[
            $indexType == TableMap::TYPE_NUM
                ? 0 + $offset
                : self::translateFieldName('IdMembre', TableMap::TYPE_PHPNAME, $indexType)
        ];
    }

    /**
     * The class that the tableMap will make instances of.
     *
     * If $withPrefix is true, the returned path
     * uses a dot-path notation which is translated into a path
     * relative to a location on the PHP include_path.
     * (e.g. path.to.MyClass -> 'path/to/MyClass.php')
     *
     * @param boolean $withPrefix Whether or not to return the path with the class name
     * @return string path.to.ClassName
     */
    public static function getOMClass($withPrefix = true)
    {
        return $withPrefix ? MembreTableMap::CLASS_DEFAULT : MembreTableMap::OM_CLASS;
    }

    /**
     * Populates an object of the default type or an object that inherit from the default.
     *
     * @param array  $row       row returned by DataFetcher->fetch().
     * @param int    $offset    The 0-based offset for reading from the resultset row.
     * @param string $indexType The index type of $row. Mostly DataFetcher->getIndexType().
                                 One of the class type constants TableMap::TYPE_PHPNAME, TableMap::TYPE_CAMELNAME
     *                           TableMap::TYPE_COLNAME, TableMap::TYPE_FIELDNAME, TableMap::TYPE_NUM.
     *
     * @throws PropelException Any exceptions caught during processing will be
     *                         rethrown wrapped into a PropelException.
     * @return array           (Membre object, last column rank)
     */
    public static function populateObject($row, $offset = 0, $indexType = TableMap::TYPE_NUM)
    {
        $key = MembreTableMap::getPrimaryKeyHashFromRow($row, $offset, $indexType);
        if (null !== ($obj = MembreTableMap::getInstanceFromPool($key))) {
            // We no longer rehydrate the object, since this can cause data loss.
            // See http://www.propelorm.org/ticket/509
            // $obj->hydrate($row, $offset, true); // rehydrate
            $col = $offset + MembreTableMap::NUM_HYDRATE_COLUMNS;
        } else {
            $cls = MembreTableMap::OM_CLASS;
            /** @var Membre $obj */
            $obj = new $cls();
            $col = $obj->hydrate($row, $offset, false, $indexType);
            MembreTableMap::addInstanceToPool($obj, $key);
        }

        return array($obj, $col);
    }

    /**
     * The returned array will contain objects of the default type or
     * objects that inherit from the default.
     *
     * @param DataFetcherInterface $dataFetcher
     * @return array
     * @throws PropelException Any exceptions caught during processing will be
     *                         rethrown wrapped into a PropelException.
     */
    public static function populateObjects(DataFetcherInterface $dataFetcher)
    {
        $results = array();

        // set the class once to avoid overhead in the loop
        $cls = static::getOMClass(false);
        // populate the object(s)
        while ($row = $dataFetcher->fetch()) {
            $key = MembreTableMap::getPrimaryKeyHashFromRow($row, 0, $dataFetcher->getIndexType());
            if (null !== ($obj = MembreTableMap::getInstanceFromPool($key))) {
                // We no longer rehydrate the object, since this can cause data loss.
                // See http://www.propelorm.org/ticket/509
                // $obj->hydrate($row, 0, true); // rehydrate
                $results[] = $obj;
            } else {
                /** @var Membre $obj */
                $obj = new $cls();
                $obj->hydrate($row);
                $results[] = $obj;
                MembreTableMap::addInstanceToPool($obj, $key);
            } // if key exists
        }

        return $results;
    }
    /**
     * Add all the columns needed to create a new object.
     *
     * Note: any columns that were marked with lazyLoad="true" in the
     * XML schema will not be added to the select list and only loaded
     * on demand.
     *
     * @param Criteria $criteria object containing the columns to add.
     * @param string   $alias    optional table alias
     * @throws PropelException Any exceptions caught during processing will be
     *                         rethrown wrapped into a PropelException.
     */
    public static function addSelectColumns(Criteria $criteria, $alias = null)
    {
        if (null === $alias) {
            $criteria->addSelectColumn(MembreTableMap::COL_ID_MEMBRE);
            $criteria->addSelectColumn(MembreTableMap::COL_ID_INDIVIDU_TITULAIRE);
            $criteria->addSelectColumn(MembreTableMap::COL_IDENTIFIANT_INTERNE);
            $criteria->addSelectColumn(MembreTableMap::COL_CIVILITE);
            $criteria->addSelectColumn(MembreTableMap::COL_NOM);
            $criteria->addSelectColumn(MembreTableMap::COL_NOMCOURT);
            $criteria->addSelectColumn(MembreTableMap::COL_DATE_SORTIE);
            $criteria->addSelectColumn(MembreTableMap::COL_OBSERVATION);
            $criteria->addSelectColumn(MembreTableMap::COL_CREATED_AT);
            $criteria->addSelectColumn(MembreTableMap::COL_UPDATED_AT);
        } else {
            $criteria->addSelectColumn($alias . '.id_membre');
            $criteria->addSelectColumn($alias . '.id_individu_titulaire');
            $criteria->addSelectColumn($alias . '.identifiant_interne');
            $criteria->addSelectColumn($alias . '.civilite');
            $criteria->addSelectColumn($alias . '.nom');
            $criteria->addSelectColumn($alias . '.nomcourt');
            $criteria->addSelectColumn($alias . '.date_sortie');
            $criteria->addSelectColumn($alias . '.observation');
            $criteria->addSelectColumn($alias . '.created_at');
            $criteria->addSelectColumn($alias . '.updated_at');
        }
    }

    /**
     * Returns the TableMap related to this object.
     * This method is not needed for general use but a specific application could have a need.
     * @return TableMap
     * @throws PropelException Any exceptions caught during processing will be
     *                         rethrown wrapped into a PropelException.
     */
    public static function getTableMap()
    {
        return Propel::getServiceContainer()->getDatabaseMap(MembreTableMap::DATABASE_NAME)->getTable(MembreTableMap::TABLE_NAME);
    }

    /**
     * Add a TableMap instance to the database for this tableMap class.
     */
    public static function buildTableMap()
    {
        $dbMap = Propel::getServiceContainer()->getDatabaseMap(MembreTableMap::DATABASE_NAME);
        if (!$dbMap->hasTable(MembreTableMap::TABLE_NAME)) {
            $dbMap->addTableObject(new MembreTableMap());
        }
    }

    /**
     * Performs a DELETE on the database, given a Membre or Criteria object OR a primary key value.
     *
     * @param mixed               $values Criteria or Membre object or primary key or array of primary keys
     *              which is used to create the DELETE statement
     * @param  ConnectionInterface $con the connection to use
     * @return int             The number of affected rows (if supported by underlying database driver).  This includes CASCADE-related rows
     *                         if supported by native driver or if emulated using Propel.
     * @throws PropelException Any exceptions caught during processing will be
     *                         rethrown wrapped into a PropelException.
     */
     public static function doDelete($values, ConnectionInterface $con = null)
     {
        if (null === $con) {
            $con = Propel::getServiceContainer()->getWriteConnection(MembreTableMap::DATABASE_NAME);
        }

        if ($values instanceof Criteria) {
            // rename for clarity
            $criteria = $values;
        } elseif ($values instanceof \Membre) { // it's a model object
            // create criteria based on pk values
            $criteria = $values->buildPkeyCriteria();
        } else { // it's a primary key, or an array of pks
            $criteria = new Criteria(MembreTableMap::DATABASE_NAME);
            $criteria->add(MembreTableMap::COL_ID_MEMBRE, (array) $values, Criteria::IN);
        }

        $query = MembreQuery::create()->mergeWith($criteria);

        if ($values instanceof Criteria) {
            MembreTableMap::clearInstancePool();
        } elseif (!is_object($values)) { // it's a primary key, or an array of pks
            foreach ((array) $values as $singleval) {
                MembreTableMap::removeInstanceFromPool($singleval);
            }
        }

        return $query->delete($con);
    }

    /**
     * Deletes all rows from the asso_membres table.
     *
     * @param ConnectionInterface $con the connection to use
     * @return int The number of affected rows (if supported by underlying database driver).
     */
    public static function doDeleteAll(ConnectionInterface $con = null)
    {
        return MembreQuery::create()->doDeleteAll($con);
    }

    /**
     * Performs an INSERT on the database, given a Membre or Criteria object.
     *
     * @param mixed               $criteria Criteria or Membre object containing data that is used to create the INSERT statement.
     * @param ConnectionInterface $con the ConnectionInterface connection to use
     * @return mixed           The new primary key.
     * @throws PropelException Any exceptions caught during processing will be
     *                         rethrown wrapped into a PropelException.
     */
    public static function doInsert($criteria, ConnectionInterface $con = null)
    {
        if (null === $con) {
            $con = Propel::getServiceContainer()->getWriteConnection(MembreTableMap::DATABASE_NAME);
        }

        if ($criteria instanceof Criteria) {
            $criteria = clone $criteria; // rename for clarity
        } else {
            $criteria = $criteria->buildCriteria(); // build Criteria from Membre object
        }

        if ($criteria->containsKey(MembreTableMap::COL_ID_MEMBRE) && $criteria->keyContainsValue(MembreTableMap::COL_ID_MEMBRE) ) {
            throw new PropelException('Cannot insert a value for auto-increment primary key ('.MembreTableMap::COL_ID_MEMBRE.')');
        }


        // Set the correct dbName
        $query = MembreQuery::create()->mergeWith($criteria);

        // use transaction because $criteria could contain info
        // for more than one table (I guess, conceivably)
        return $con->transaction(function () use ($con, $query) {
            return $query->doInsert($con);
        });
    }

} // MembreTableMap
// This is the static code needed to register the TableMap for this table with the main Propel class.
//
MembreTableMap::buildTableMap();
