<?php

namespace Map;

use \Import;
use \ImportQuery;
use Propel\Runtime\Propel;
use Propel\Runtime\ActiveQuery\Criteria;
use Propel\Runtime\ActiveQuery\InstancePoolTrait;
use Propel\Runtime\Connection\ConnectionInterface;
use Propel\Runtime\DataFetcher\DataFetcherInterface;
use Propel\Runtime\Exception\PropelException;
use Propel\Runtime\Map\RelationMap;
use Propel\Runtime\Map\TableMap;
use Propel\Runtime\Map\TableMapTrait;


/**
 * This class defines the structure of the 'asso_imports' table.
 *
 *
 *
 * This map class is used by Propel to do runtime db structure discovery.
 * For example, the createSelectSql() method checks the type of a given column used in an
 * ORDER BY clause to know whether it needs to apply SQL to make the ORDER BY case-insensitive
 * (i.e. if it's a text column type).
 *
 */
class ImportTableMap extends TableMap
{
    use InstancePoolTrait;
    use TableMapTrait;

    /**
     * The (dot-path) name of this class
     */
    const CLASS_NAME = '.Map.ImportTableMap';

    /**
     * The default database name for this class
     */
    const DATABASE_NAME = 'simplasso';

    /**
     * The table name for this class
     */
    const TABLE_NAME = 'asso_imports';

    /**
     * The related Propel class for this table
     */
    const OM_CLASS = '\\Import';

    /**
     * A class that can be returned by this tableMap
     */
    const CLASS_DEFAULT = 'Import';

    /**
     * The total number of columns
     */
    const NUM_COLUMNS = 14;

    /**
     * The number of lazy-loaded columns
     */
    const NUM_LAZY_LOAD_COLUMNS = 3;

    /**
     * The number of columns to hydrate (NUM_COLUMNS - NUM_LAZY_LOAD_COLUMNS)
     */
    const NUM_HYDRATE_COLUMNS = 11;

    /**
     * the column name for the id_import field
     */
    const COL_ID_IMPORT = 'asso_imports.id_import';

    /**
     * the column name for the modele field
     */
    const COL_MODELE = 'asso_imports.modele';

    /**
     * the column name for the avancement field
     */
    const COL_AVANCEMENT = 'asso_imports.avancement';

    /**
     * the column name for the nom field
     */
    const COL_NOM = 'asso_imports.nom';

    /**
     * the column name for the nomprovisoire field
     */
    const COL_NOMPROVISOIRE = 'asso_imports.nomprovisoire';

    /**
     * the column name for the taille field
     */
    const COL_TAILLE = 'asso_imports.taille';

    /**
     * the column name for the controlemd5 field
     */
    const COL_CONTROLEMD5 = 'asso_imports.controlemd5';

    /**
     * the column name for the originals field
     */
    const COL_ORIGINALS = 'asso_imports.originals';

    /**
     * the column name for the informations field
     */
    const COL_INFORMATIONS = 'asso_imports.informations';

    /**
     * the column name for the modifications field
     */
    const COL_MODIFICATIONS = 'asso_imports.modifications';

    /**
     * the column name for the nb_ligne field
     */
    const COL_NB_LIGNE = 'asso_imports.nb_ligne';

    /**
     * the column name for the ligne_en_cours field
     */
    const COL_LIGNE_EN_COURS = 'asso_imports.ligne_en_cours';

    /**
     * the column name for the created_at field
     */
    const COL_CREATED_AT = 'asso_imports.created_at';

    /**
     * the column name for the updated_at field
     */
    const COL_UPDATED_AT = 'asso_imports.updated_at';

    /**
     * The default string format for model objects of the related table
     */
    const DEFAULT_STRING_FORMAT = 'YAML';

    /**
     * holds an array of fieldnames
     *
     * first dimension keys are the type constants
     * e.g. self::$fieldNames[self::TYPE_PHPNAME][0] = 'Id'
     */
    protected static $fieldNames = array (
        self::TYPE_PHPNAME       => array('IdImport', 'Modele', 'Avancement', 'Nom', 'Nomprovisoire', 'Taille', 'Controlemd5', 'Originals', 'Informations', 'Modifications', 'NbLigne', 'LigneEnCours', 'CreatedAt', 'UpdatedAt', ),
        self::TYPE_CAMELNAME     => array('idImport', 'modele', 'avancement', 'nom', 'nomprovisoire', 'taille', 'controlemd5', 'originals', 'informations', 'modifications', 'nbLigne', 'ligneEnCours', 'createdAt', 'updatedAt', ),
        self::TYPE_COLNAME       => array(ImportTableMap::COL_ID_IMPORT, ImportTableMap::COL_MODELE, ImportTableMap::COL_AVANCEMENT, ImportTableMap::COL_NOM, ImportTableMap::COL_NOMPROVISOIRE, ImportTableMap::COL_TAILLE, ImportTableMap::COL_CONTROLEMD5, ImportTableMap::COL_ORIGINALS, ImportTableMap::COL_INFORMATIONS, ImportTableMap::COL_MODIFICATIONS, ImportTableMap::COL_NB_LIGNE, ImportTableMap::COL_LIGNE_EN_COURS, ImportTableMap::COL_CREATED_AT, ImportTableMap::COL_UPDATED_AT, ),
        self::TYPE_FIELDNAME     => array('id_import', 'modele', 'avancement', 'nom', 'nomprovisoire', 'taille', 'controlemd5', 'originals', 'informations', 'modifications', 'nb_ligne', 'ligne_en_cours', 'created_at', 'updated_at', ),
        self::TYPE_NUM           => array(0, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, )
    );

    /**
     * holds an array of keys for quick access to the fieldnames array
     *
     * first dimension keys are the type constants
     * e.g. self::$fieldKeys[self::TYPE_PHPNAME]['Id'] = 0
     */
    protected static $fieldKeys = array (
        self::TYPE_PHPNAME       => array('IdImport' => 0, 'Modele' => 1, 'Avancement' => 2, 'Nom' => 3, 'Nomprovisoire' => 4, 'Taille' => 5, 'Controlemd5' => 6, 'Originals' => 7, 'Informations' => 8, 'Modifications' => 9, 'NbLigne' => 10, 'LigneEnCours' => 11, 'CreatedAt' => 12, 'UpdatedAt' => 13, ),
        self::TYPE_CAMELNAME     => array('idImport' => 0, 'modele' => 1, 'avancement' => 2, 'nom' => 3, 'nomprovisoire' => 4, 'taille' => 5, 'controlemd5' => 6, 'originals' => 7, 'informations' => 8, 'modifications' => 9, 'nbLigne' => 10, 'ligneEnCours' => 11, 'createdAt' => 12, 'updatedAt' => 13, ),
        self::TYPE_COLNAME       => array(ImportTableMap::COL_ID_IMPORT => 0, ImportTableMap::COL_MODELE => 1, ImportTableMap::COL_AVANCEMENT => 2, ImportTableMap::COL_NOM => 3, ImportTableMap::COL_NOMPROVISOIRE => 4, ImportTableMap::COL_TAILLE => 5, ImportTableMap::COL_CONTROLEMD5 => 6, ImportTableMap::COL_ORIGINALS => 7, ImportTableMap::COL_INFORMATIONS => 8, ImportTableMap::COL_MODIFICATIONS => 9, ImportTableMap::COL_NB_LIGNE => 10, ImportTableMap::COL_LIGNE_EN_COURS => 11, ImportTableMap::COL_CREATED_AT => 12, ImportTableMap::COL_UPDATED_AT => 13, ),
        self::TYPE_FIELDNAME     => array('id_import' => 0, 'modele' => 1, 'avancement' => 2, 'nom' => 3, 'nomprovisoire' => 4, 'taille' => 5, 'controlemd5' => 6, 'originals' => 7, 'informations' => 8, 'modifications' => 9, 'nb_ligne' => 10, 'ligne_en_cours' => 11, 'created_at' => 12, 'updated_at' => 13, ),
        self::TYPE_NUM           => array(0, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, )
    );

    /**
     * Initialize the table attributes and columns
     * Relations are not initialized by this method since they are lazy loaded
     *
     * @return void
     * @throws PropelException
     */
    public function initialize()
    {
        // attributes
        $this->setName('asso_imports');
        $this->setPhpName('Import');
        $this->setIdentifierQuoting(true);
        $this->setClassName('\\Import');
        $this->setPackage('');
        $this->setUseIdGenerator(true);
        // columns
        $this->addPrimaryKey('id_import', 'IdImport', 'BIGINT', true, 21, null);
        $this->addColumn('modele', 'Modele', 'VARCHAR', true, 50, null);
        $this->addColumn('avancement', 'Avancement', 'TINYINT', true, null, 0);
        $this->addColumn('nom', 'Nom', 'VARCHAR', true, 100, null);
        $this->addColumn('nomprovisoire', 'Nomprovisoire', 'VARCHAR', true, 100, null);
        $this->addColumn('taille', 'Taille', 'BIGINT', true, 21, null);
        $this->addColumn('controlemd5', 'Controlemd5', 'VARCHAR', true, 32, null);
        $this->addColumn('originals', 'Originals', 'CLOB', false, null, null);
        $this->addColumn('informations', 'Informations', 'CLOB', false, null, null);
        $this->addColumn('modifications', 'Modifications', 'CLOB', false, null, null);
        $this->addColumn('nb_ligne', 'NbLigne', 'BIGINT', false, null, null);
        $this->addColumn('ligne_en_cours', 'LigneEnCours', 'BIGINT', false, null, 0);
        $this->addColumn('created_at', 'CreatedAt', 'TIMESTAMP', false, null, null);
        $this->addColumn('updated_at', 'UpdatedAt', 'TIMESTAMP', false, null, null);
    } // initialize()

    /**
     * Build the RelationMap objects for this table relationships
     */
    public function buildRelations()
    {
        $this->addRelation('Importligne', '\\Importligne', RelationMap::ONE_TO_MANY, array (
  0 =>
  array (
    0 => ':id_import',
    1 => ':id_import',
  ),
), 'CASCADE', null, 'Importlignes', false);
    } // buildRelations()

    /**
     *
     * Gets the list of behaviors registered for this table
     *
     * @return array Associative array (name => parameters) of behaviors
     */
    public function getBehaviors()
    {
        return array(
            'timestampable' => array('create_column' => 'created_at', 'update_column' => 'updated_at', 'disable_created_at' => 'false', 'disable_updated_at' => 'false', ),
            'archivable' => array('archive_table' => '', 'archive_phpname' => '', 'archive_class' => '', 'log_archived_at' => 'true', 'archived_at_column' => 'archived_at', 'archive_on_insert' => 'false', 'archive_on_update' => 'false', 'archive_on_delete' => 'true', ),
        );
    } // getBehaviors()
    /**
     * Method to invalidate the instance pool of all tables related to asso_imports     * by a foreign key with ON DELETE CASCADE
     */
    public static function clearRelatedInstancePool()
    {
        // Invalidate objects in related instance pools,
        // since one or more of them may be deleted by ON DELETE CASCADE/SETNULL rule.
        ImportligneTableMap::clearInstancePool();
    }

    /**
     * Retrieves a string version of the primary key from the DB resultset row that can be used to uniquely identify a row in this table.
     *
     * For tables with a single-column primary key, that simple pkey value will be returned.  For tables with
     * a multi-column primary key, a serialize()d version of the primary key will be returned.
     *
     * @param array  $row       resultset row.
     * @param int    $offset    The 0-based offset for reading from the resultset row.
     * @param string $indexType One of the class type constants TableMap::TYPE_PHPNAME, TableMap::TYPE_CAMELNAME
     *                           TableMap::TYPE_COLNAME, TableMap::TYPE_FIELDNAME, TableMap::TYPE_NUM
     *
     * @return string The primary key hash of the row
     */
    public static function getPrimaryKeyHashFromRow($row, $offset = 0, $indexType = TableMap::TYPE_NUM)
    {
        // If the PK cannot be derived from the row, return NULL.
        if ($row[TableMap::TYPE_NUM == $indexType ? 0 + $offset : static::translateFieldName('IdImport', TableMap::TYPE_PHPNAME, $indexType)] === null) {
            return null;
        }

        return null === $row[TableMap::TYPE_NUM == $indexType ? 0 + $offset : static::translateFieldName('IdImport', TableMap::TYPE_PHPNAME, $indexType)] || is_scalar($row[TableMap::TYPE_NUM == $indexType ? 0 + $offset : static::translateFieldName('IdImport', TableMap::TYPE_PHPNAME, $indexType)]) || is_callable([$row[TableMap::TYPE_NUM == $indexType ? 0 + $offset : static::translateFieldName('IdImport', TableMap::TYPE_PHPNAME, $indexType)], '__toString']) ? (string) $row[TableMap::TYPE_NUM == $indexType ? 0 + $offset : static::translateFieldName('IdImport', TableMap::TYPE_PHPNAME, $indexType)] : $row[TableMap::TYPE_NUM == $indexType ? 0 + $offset : static::translateFieldName('IdImport', TableMap::TYPE_PHPNAME, $indexType)];
    }

    /**
     * Retrieves the primary key from the DB resultset row
     * For tables with a single-column primary key, that simple pkey value will be returned.  For tables with
     * a multi-column primary key, an array of the primary key columns will be returned.
     *
     * @param array  $row       resultset row.
     * @param int    $offset    The 0-based offset for reading from the resultset row.
     * @param string $indexType One of the class type constants TableMap::TYPE_PHPNAME, TableMap::TYPE_CAMELNAME
     *                           TableMap::TYPE_COLNAME, TableMap::TYPE_FIELDNAME, TableMap::TYPE_NUM
     *
     * @return mixed The primary key of the row
     */
    public static function getPrimaryKeyFromRow($row, $offset = 0, $indexType = TableMap::TYPE_NUM)
    {
        return (string) $row[
            $indexType == TableMap::TYPE_NUM
                ? 0 + $offset
                : self::translateFieldName('IdImport', TableMap::TYPE_PHPNAME, $indexType)
        ];
    }

    /**
     * The class that the tableMap will make instances of.
     *
     * If $withPrefix is true, the returned path
     * uses a dot-path notation which is translated into a path
     * relative to a location on the PHP include_path.
     * (e.g. path.to.MyClass -> 'path/to/MyClass.php')
     *
     * @param boolean $withPrefix Whether or not to return the path with the class name
     * @return string path.to.ClassName
     */
    public static function getOMClass($withPrefix = true)
    {
        return $withPrefix ? ImportTableMap::CLASS_DEFAULT : ImportTableMap::OM_CLASS;
    }

    /**
     * Populates an object of the default type or an object that inherit from the default.
     *
     * @param array  $row       row returned by DataFetcher->fetch().
     * @param int    $offset    The 0-based offset for reading from the resultset row.
     * @param string $indexType The index type of $row. Mostly DataFetcher->getIndexType().
                                 One of the class type constants TableMap::TYPE_PHPNAME, TableMap::TYPE_CAMELNAME
     *                           TableMap::TYPE_COLNAME, TableMap::TYPE_FIELDNAME, TableMap::TYPE_NUM.
     *
     * @throws PropelException Any exceptions caught during processing will be
     *                         rethrown wrapped into a PropelException.
     * @return array           (Import object, last column rank)
     */
    public static function populateObject($row, $offset = 0, $indexType = TableMap::TYPE_NUM)
    {
        $key = ImportTableMap::getPrimaryKeyHashFromRow($row, $offset, $indexType);
        if (null !== ($obj = ImportTableMap::getInstanceFromPool($key))) {
            // We no longer rehydrate the object, since this can cause data loss.
            // See http://www.propelorm.org/ticket/509
            // $obj->hydrate($row, $offset, true); // rehydrate
            $col = $offset + ImportTableMap::NUM_HYDRATE_COLUMNS;
        } else {
            $cls = ImportTableMap::OM_CLASS;
            /** @var Import $obj */
            $obj = new $cls();
            $col = $obj->hydrate($row, $offset, false, $indexType);
            ImportTableMap::addInstanceToPool($obj, $key);
        }

        return array($obj, $col);
    }

    /**
     * The returned array will contain objects of the default type or
     * objects that inherit from the default.
     *
     * @param DataFetcherInterface $dataFetcher
     * @return array
     * @throws PropelException Any exceptions caught during processing will be
     *                         rethrown wrapped into a PropelException.
     */
    public static function populateObjects(DataFetcherInterface $dataFetcher)
    {
        $results = array();

        // set the class once to avoid overhead in the loop
        $cls = static::getOMClass(false);
        // populate the object(s)
        while ($row = $dataFetcher->fetch()) {
            $key = ImportTableMap::getPrimaryKeyHashFromRow($row, 0, $dataFetcher->getIndexType());
            if (null !== ($obj = ImportTableMap::getInstanceFromPool($key))) {
                // We no longer rehydrate the object, since this can cause data loss.
                // See http://www.propelorm.org/ticket/509
                // $obj->hydrate($row, 0, true); // rehydrate
                $results[] = $obj;
            } else {
                /** @var Import $obj */
                $obj = new $cls();
                $obj->hydrate($row);
                $results[] = $obj;
                ImportTableMap::addInstanceToPool($obj, $key);
            } // if key exists
        }

        return $results;
    }
    /**
     * Add all the columns needed to create a new object.
     *
     * Note: any columns that were marked with lazyLoad="true" in the
     * XML schema will not be added to the select list and only loaded
     * on demand.
     *
     * @param Criteria $criteria object containing the columns to add.
     * @param string   $alias    optional table alias
     * @throws PropelException Any exceptions caught during processing will be
     *                         rethrown wrapped into a PropelException.
     */
    public static function addSelectColumns(Criteria $criteria, $alias = null)
    {
        if (null === $alias) {
            $criteria->addSelectColumn(ImportTableMap::COL_ID_IMPORT);
            $criteria->addSelectColumn(ImportTableMap::COL_MODELE);
            $criteria->addSelectColumn(ImportTableMap::COL_AVANCEMENT);
            $criteria->addSelectColumn(ImportTableMap::COL_NOM);
            $criteria->addSelectColumn(ImportTableMap::COL_NOMPROVISOIRE);
            $criteria->addSelectColumn(ImportTableMap::COL_TAILLE);
            $criteria->addSelectColumn(ImportTableMap::COL_CONTROLEMD5);
            $criteria->addSelectColumn(ImportTableMap::COL_NB_LIGNE);
            $criteria->addSelectColumn(ImportTableMap::COL_LIGNE_EN_COURS);
            $criteria->addSelectColumn(ImportTableMap::COL_CREATED_AT);
            $criteria->addSelectColumn(ImportTableMap::COL_UPDATED_AT);
        } else {
            $criteria->addSelectColumn($alias . '.id_import');
            $criteria->addSelectColumn($alias . '.modele');
            $criteria->addSelectColumn($alias . '.avancement');
            $criteria->addSelectColumn($alias . '.nom');
            $criteria->addSelectColumn($alias . '.nomprovisoire');
            $criteria->addSelectColumn($alias . '.taille');
            $criteria->addSelectColumn($alias . '.controlemd5');
            $criteria->addSelectColumn($alias . '.nb_ligne');
            $criteria->addSelectColumn($alias . '.ligne_en_cours');
            $criteria->addSelectColumn($alias . '.created_at');
            $criteria->addSelectColumn($alias . '.updated_at');
        }
    }

    /**
     * Returns the TableMap related to this object.
     * This method is not needed for general use but a specific application could have a need.
     * @return TableMap
     * @throws PropelException Any exceptions caught during processing will be
     *                         rethrown wrapped into a PropelException.
     */
    public static function getTableMap()
    {
        return Propel::getServiceContainer()->getDatabaseMap(ImportTableMap::DATABASE_NAME)->getTable(ImportTableMap::TABLE_NAME);
    }

    /**
     * Add a TableMap instance to the database for this tableMap class.
     */
    public static function buildTableMap()
    {
        $dbMap = Propel::getServiceContainer()->getDatabaseMap(ImportTableMap::DATABASE_NAME);
        if (!$dbMap->hasTable(ImportTableMap::TABLE_NAME)) {
            $dbMap->addTableObject(new ImportTableMap());
        }
    }

    /**
     * Performs a DELETE on the database, given a Import or Criteria object OR a primary key value.
     *
     * @param mixed               $values Criteria or Import object or primary key or array of primary keys
     *              which is used to create the DELETE statement
     * @param  ConnectionInterface $con the connection to use
     * @return int             The number of affected rows (if supported by underlying database driver).  This includes CASCADE-related rows
     *                         if supported by native driver or if emulated using Propel.
     * @throws PropelException Any exceptions caught during processing will be
     *                         rethrown wrapped into a PropelException.
     */
     public static function doDelete($values, ConnectionInterface $con = null)
     {
        if (null === $con) {
            $con = Propel::getServiceContainer()->getWriteConnection(ImportTableMap::DATABASE_NAME);
        }

        if ($values instanceof Criteria) {
            // rename for clarity
            $criteria = $values;
        } elseif ($values instanceof \Import) { // it's a model object
            // create criteria based on pk values
            $criteria = $values->buildPkeyCriteria();
        } else { // it's a primary key, or an array of pks
            $criteria = new Criteria(ImportTableMap::DATABASE_NAME);
            $criteria->add(ImportTableMap::COL_ID_IMPORT, (array) $values, Criteria::IN);
        }

        $query = ImportQuery::create()->mergeWith($criteria);

        if ($values instanceof Criteria) {
            ImportTableMap::clearInstancePool();
        } elseif (!is_object($values)) { // it's a primary key, or an array of pks
            foreach ((array) $values as $singleval) {
                ImportTableMap::removeInstanceFromPool($singleval);
            }
        }

        return $query->delete($con);
    }

    /**
     * Deletes all rows from the asso_imports table.
     *
     * @param ConnectionInterface $con the connection to use
     * @return int The number of affected rows (if supported by underlying database driver).
     */
    public static function doDeleteAll(ConnectionInterface $con = null)
    {
        return ImportQuery::create()->doDeleteAll($con);
    }

    /**
     * Performs an INSERT on the database, given a Import or Criteria object.
     *
     * @param mixed               $criteria Criteria or Import object containing data that is used to create the INSERT statement.
     * @param ConnectionInterface $con the ConnectionInterface connection to use
     * @return mixed           The new primary key.
     * @throws PropelException Any exceptions caught during processing will be
     *                         rethrown wrapped into a PropelException.
     */
    public static function doInsert($criteria, ConnectionInterface $con = null)
    {
        if (null === $con) {
            $con = Propel::getServiceContainer()->getWriteConnection(ImportTableMap::DATABASE_NAME);
        }

        if ($criteria instanceof Criteria) {
            $criteria = clone $criteria; // rename for clarity
        } else {
            $criteria = $criteria->buildCriteria(); // build Criteria from Import object
        }

        if ($criteria->containsKey(ImportTableMap::COL_ID_IMPORT) && $criteria->keyContainsValue(ImportTableMap::COL_ID_IMPORT) ) {
            throw new PropelException('Cannot insert a value for auto-increment primary key ('.ImportTableMap::COL_ID_IMPORT.')');
        }


        // Set the correct dbName
        $query = ImportQuery::create()->mergeWith($criteria);

        // use transaction because $criteria could contain info
        // for more than one table (I guess, conceivably)
        return $con->transaction(function () use ($con, $query) {
            return $query->doInsert($con);
        });
    }

} // ImportTableMap
// This is the static code needed to register the TableMap for this table with the main Propel class.
//
ImportTableMap::buildTableMap();
