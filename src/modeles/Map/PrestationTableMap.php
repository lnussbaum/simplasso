<?php

namespace Map;

use \Prestation;
use \PrestationQuery;
use Propel\Runtime\Propel;
use Propel\Runtime\ActiveQuery\Criteria;
use Propel\Runtime\ActiveQuery\InstancePoolTrait;
use Propel\Runtime\Connection\ConnectionInterface;
use Propel\Runtime\DataFetcher\DataFetcherInterface;
use Propel\Runtime\Exception\PropelException;
use Propel\Runtime\Map\RelationMap;
use Propel\Runtime\Map\TableMap;
use Propel\Runtime\Map\TableMapTrait;


/**
 * This class defines the structure of the 'asso_prestations' table.
 *
 *
 *
 * This map class is used by Propel to do runtime db structure discovery.
 * For example, the createSelectSql() method checks the type of a given column used in an
 * ORDER BY clause to know whether it needs to apply SQL to make the ORDER BY case-insensitive
 * (i.e. if it's a text column type).
 *
 */
class PrestationTableMap extends TableMap
{
    use InstancePoolTrait;
    use TableMapTrait;

    /**
     * The (dot-path) name of this class
     */
    const CLASS_NAME = '.Map.PrestationTableMap';

    /**
     * The default database name for this class
     */
    const DATABASE_NAME = 'simplasso';

    /**
     * The table name for this class
     */
    const TABLE_NAME = 'asso_prestations';

    /**
     * The related Propel class for this table
     */
    const OM_CLASS = '\\Prestation';

    /**
     * A class that can be returned by this tableMap
     */
    const CLASS_DEFAULT = 'Prestation';

    /**
     * The total number of columns
     */
    const NUM_COLUMNS = 22;

    /**
     * The number of lazy-loaded columns
     */
    const NUM_LAZY_LOAD_COLUMNS = 0;

    /**
     * The number of columns to hydrate (NUM_COLUMNS - NUM_LAZY_LOAD_COLUMNS)
     */
    const NUM_HYDRATE_COLUMNS = 22;

    /**
     * the column name for the id_prestation field
     */
    const COL_ID_PRESTATION = 'asso_prestations.id_prestation';

    /**
     * the column name for the nom_groupe field
     */
    const COL_NOM_GROUPE = 'asso_prestations.nom_groupe';

    /**
     * the column name for the nom field
     */
    const COL_NOM = 'asso_prestations.nom';

    /**
     * the column name for the nomcourt field
     */
    const COL_NOMCOURT = 'asso_prestations.nomcourt';

    /**
     * the column name for the descriptif field
     */
    const COL_DESCRIPTIF = 'asso_prestations.descriptif';

    /**
     * the column name for the id_entite field
     */
    const COL_ID_ENTITE = 'asso_prestations.id_entite';

    /**
     * the column name for the id_tva field
     */
    const COL_ID_TVA = 'asso_prestations.id_tva';

    /**
     * the column name for the retard_jours field
     */
    const COL_RETARD_JOURS = 'asso_prestations.retard_jours';

    /**
     * the column name for the nombre_numero field
     */
    const COL_NOMBRE_NUMERO = 'asso_prestations.nombre_numero';

    /**
     * the column name for the prochain_numero field
     */
    const COL_PROCHAIN_NUMERO = 'asso_prestations.prochain_numero';

    /**
     * the column name for the prixlibre field
     */
    const COL_PRIXLIBRE = 'asso_prestations.prixlibre';

    /**
     * the column name for the quantite field
     */
    const COL_QUANTITE = 'asso_prestations.quantite';

    /**
     * the column name for the id_unite field
     */
    const COL_ID_UNITE = 'asso_prestations.id_unite';

    /**
     * the column name for the id_compte field
     */
    const COL_ID_COMPTE = 'asso_prestations.id_compte';

    /**
     * the column name for the prestation_type field
     */
    const COL_PRESTATION_TYPE = 'asso_prestations.prestation_type';

    /**
     * the column name for the active field
     */
    const COL_ACTIVE = 'asso_prestations.active';

    /**
     * the column name for the nb_voix field
     */
    const COL_NB_VOIX = 'asso_prestations.nb_voix';

    /**
     * the column name for the periodique field
     */
    const COL_PERIODIQUE = 'asso_prestations.periodique';

    /**
     * the column name for the signe field
     */
    const COL_SIGNE = 'asso_prestations.signe';

    /**
     * the column name for the objet_beneficiaire field
     */
    const COL_OBJET_BENEFICIAIRE = 'asso_prestations.objet_beneficiaire';

    /**
     * the column name for the created_at field
     */
    const COL_CREATED_AT = 'asso_prestations.created_at';

    /**
     * the column name for the updated_at field
     */
    const COL_UPDATED_AT = 'asso_prestations.updated_at';

    /**
     * The default string format for model objects of the related table
     */
    const DEFAULT_STRING_FORMAT = 'YAML';

    /** A key representing a particular subclass */
    const CLASSKEY_1 = '1';

    /** A key representing a particular subclass */
    const CLASSKEY_COTISATION = '\\Cotisation';

    /** A class that can be returned by this tableMap. */
    const CLASSNAME_1 = '\\Cotisation';

    /** A key representing a particular subclass */
    const CLASSKEY_2 = '2';

    /** A key representing a particular subclass */
    const CLASSKEY_ABONNEMENT = '\\Abonnement';

    /** A class that can be returned by this tableMap. */
    const CLASSNAME_2 = '\\Abonnement';

    /** A key representing a particular subclass */
    const CLASSKEY_3 = '3';

    /** A key representing a particular subclass */
    const CLASSKEY_VENTE = '\\Vente';

    /** A class that can be returned by this tableMap. */
    const CLASSNAME_3 = '\\Vente';

    /** A key representing a particular subclass */
    const CLASSKEY_4 = '4';

    /** A key representing a particular subclass */
    const CLASSKEY_DON = '\\Don';

    /** A class that can be returned by this tableMap. */
    const CLASSNAME_4 = '\\Don';

    /** A key representing a particular subclass */
    const CLASSKEY_5 = '5';

    /** A key representing a particular subclass */
    const CLASSKEY_PERTE = '\\Perte';

    /** A class that can be returned by this tableMap. */
    const CLASSNAME_5 = '\\Perte';

    /** A key representing a particular subclass */
    const CLASSKEY_6 = '6';

    /** A key representing a particular subclass */
    const CLASSKEY_ADHESION = '\\Adhesion';

    /** A class that can be returned by this tableMap. */
    const CLASSNAME_6 = '\\Adhesion';

    /**
     * holds an array of fieldnames
     *
     * first dimension keys are the type constants
     * e.g. self::$fieldNames[self::TYPE_PHPNAME][0] = 'Id'
     */
    protected static $fieldNames = array (
        self::TYPE_PHPNAME       => array('IdPrestation', 'NomGroupe', 'Nom', 'Nomcourt', 'Descriptif', 'IdEntite', 'IdTva', 'RetardJours', 'NombreNumero', 'ProchainNumero', 'Prixlibre', 'Quantite', 'IdUnite', 'IdCompte', 'PrestationType', 'Active', 'NbVoix', 'Periodique', 'Signe', 'ObjetBeneficiaire', 'CreatedAt', 'UpdatedAt', ),
        self::TYPE_CAMELNAME     => array('idPrestation', 'nomGroupe', 'nom', 'nomcourt', 'descriptif', 'idEntite', 'idTva', 'retardJours', 'nombreNumero', 'prochainNumero', 'prixlibre', 'quantite', 'idUnite', 'idCompte', 'prestationType', 'active', 'nbVoix', 'periodique', 'signe', 'objetBeneficiaire', 'createdAt', 'updatedAt', ),
        self::TYPE_COLNAME       => array(PrestationTableMap::COL_ID_PRESTATION, PrestationTableMap::COL_NOM_GROUPE, PrestationTableMap::COL_NOM, PrestationTableMap::COL_NOMCOURT, PrestationTableMap::COL_DESCRIPTIF, PrestationTableMap::COL_ID_ENTITE, PrestationTableMap::COL_ID_TVA, PrestationTableMap::COL_RETARD_JOURS, PrestationTableMap::COL_NOMBRE_NUMERO, PrestationTableMap::COL_PROCHAIN_NUMERO, PrestationTableMap::COL_PRIXLIBRE, PrestationTableMap::COL_QUANTITE, PrestationTableMap::COL_ID_UNITE, PrestationTableMap::COL_ID_COMPTE, PrestationTableMap::COL_PRESTATION_TYPE, PrestationTableMap::COL_ACTIVE, PrestationTableMap::COL_NB_VOIX, PrestationTableMap::COL_PERIODIQUE, PrestationTableMap::COL_SIGNE, PrestationTableMap::COL_OBJET_BENEFICIAIRE, PrestationTableMap::COL_CREATED_AT, PrestationTableMap::COL_UPDATED_AT, ),
        self::TYPE_FIELDNAME     => array('id_prestation', 'nom_groupe', 'nom', 'nomcourt', 'descriptif', 'id_entite', 'id_tva', 'retard_jours', 'nombre_numero', 'prochain_numero', 'prixlibre', 'quantite', 'id_unite', 'id_compte', 'prestation_type', 'active', 'nb_voix', 'periodique', 'signe', 'objet_beneficiaire', 'created_at', 'updated_at', ),
        self::TYPE_NUM           => array(0, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15, 16, 17, 18, 19, 20, 21, )
    );

    /**
     * holds an array of keys for quick access to the fieldnames array
     *
     * first dimension keys are the type constants
     * e.g. self::$fieldKeys[self::TYPE_PHPNAME]['Id'] = 0
     */
    protected static $fieldKeys = array (
        self::TYPE_PHPNAME       => array('IdPrestation' => 0, 'NomGroupe' => 1, 'Nom' => 2, 'Nomcourt' => 3, 'Descriptif' => 4, 'IdEntite' => 5, 'IdTva' => 6, 'RetardJours' => 7, 'NombreNumero' => 8, 'ProchainNumero' => 9, 'Prixlibre' => 10, 'Quantite' => 11, 'IdUnite' => 12, 'IdCompte' => 13, 'PrestationType' => 14, 'Active' => 15, 'NbVoix' => 16, 'Periodique' => 17, 'Signe' => 18, 'ObjetBeneficiaire' => 19, 'CreatedAt' => 20, 'UpdatedAt' => 21, ),
        self::TYPE_CAMELNAME     => array('idPrestation' => 0, 'nomGroupe' => 1, 'nom' => 2, 'nomcourt' => 3, 'descriptif' => 4, 'idEntite' => 5, 'idTva' => 6, 'retardJours' => 7, 'nombreNumero' => 8, 'prochainNumero' => 9, 'prixlibre' => 10, 'quantite' => 11, 'idUnite' => 12, 'idCompte' => 13, 'prestationType' => 14, 'active' => 15, 'nbVoix' => 16, 'periodique' => 17, 'signe' => 18, 'objetBeneficiaire' => 19, 'createdAt' => 20, 'updatedAt' => 21, ),
        self::TYPE_COLNAME       => array(PrestationTableMap::COL_ID_PRESTATION => 0, PrestationTableMap::COL_NOM_GROUPE => 1, PrestationTableMap::COL_NOM => 2, PrestationTableMap::COL_NOMCOURT => 3, PrestationTableMap::COL_DESCRIPTIF => 4, PrestationTableMap::COL_ID_ENTITE => 5, PrestationTableMap::COL_ID_TVA => 6, PrestationTableMap::COL_RETARD_JOURS => 7, PrestationTableMap::COL_NOMBRE_NUMERO => 8, PrestationTableMap::COL_PROCHAIN_NUMERO => 9, PrestationTableMap::COL_PRIXLIBRE => 10, PrestationTableMap::COL_QUANTITE => 11, PrestationTableMap::COL_ID_UNITE => 12, PrestationTableMap::COL_ID_COMPTE => 13, PrestationTableMap::COL_PRESTATION_TYPE => 14, PrestationTableMap::COL_ACTIVE => 15, PrestationTableMap::COL_NB_VOIX => 16, PrestationTableMap::COL_PERIODIQUE => 17, PrestationTableMap::COL_SIGNE => 18, PrestationTableMap::COL_OBJET_BENEFICIAIRE => 19, PrestationTableMap::COL_CREATED_AT => 20, PrestationTableMap::COL_UPDATED_AT => 21, ),
        self::TYPE_FIELDNAME     => array('id_prestation' => 0, 'nom_groupe' => 1, 'nom' => 2, 'nomcourt' => 3, 'descriptif' => 4, 'id_entite' => 5, 'id_tva' => 6, 'retard_jours' => 7, 'nombre_numero' => 8, 'prochain_numero' => 9, 'prixlibre' => 10, 'quantite' => 11, 'id_unite' => 12, 'id_compte' => 13, 'prestation_type' => 14, 'active' => 15, 'nb_voix' => 16, 'periodique' => 17, 'signe' => 18, 'objet_beneficiaire' => 19, 'created_at' => 20, 'updated_at' => 21, ),
        self::TYPE_NUM           => array(0, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15, 16, 17, 18, 19, 20, 21, )
    );

    /**
     * Initialize the table attributes and columns
     * Relations are not initialized by this method since they are lazy loaded
     *
     * @return void
     * @throws PropelException
     */
    public function initialize()
    {
        // attributes
        $this->setName('asso_prestations');
        $this->setPhpName('Prestation');
        $this->setIdentifierQuoting(true);
        $this->setClassName('\\Prestation');
        $this->setPackage('');
        $this->setUseIdGenerator(true);
        $this->setSingleTableInheritance(true);
        // columns
        $this->addPrimaryKey('id_prestation', 'IdPrestation', 'BIGINT', true, 21, null);
        $this->addColumn('nom_groupe', 'NomGroupe', 'VARCHAR', false, 40, '');
        $this->addColumn('nom', 'Nom', 'VARCHAR', true, 40, null);
        $this->addColumn('nomcourt', 'Nomcourt', 'VARCHAR', false, 6, null);
        $this->addColumn('descriptif', 'Descriptif', 'VARCHAR', false, 255, null);
        $this->addForeignKey('id_entite', 'IdEntite', 'BIGINT', 'asso_entites', 'id_entite', true, 21, null);
        $this->addColumn('id_tva', 'IdTva', 'BIGINT', true, 21, null);
        $this->addColumn('retard_jours', 'RetardJours', 'INTEGER', false, 3, null);
        $this->addColumn('nombre_numero', 'NombreNumero', 'INTEGER', false, 3, null);
        $this->addColumn('prochain_numero', 'ProchainNumero', 'INTEGER', false, 8, null);
        $this->addColumn('prixlibre', 'Prixlibre', 'BOOLEAN', true, 1, false);
        $this->addColumn('quantite', 'Quantite', 'BOOLEAN', true, 1, false);
        $this->addColumn('id_unite', 'IdUnite', 'BIGINT', true, 21, 0);
        $this->addForeignKey('id_compte', 'IdCompte', 'BIGINT', 'assc_comptes', 'id_compte', false, 21, null);
        $this->addColumn('prestation_type', 'PrestationType', 'INTEGER', true, 2, null);
        $this->addColumn('active', 'Active', 'BOOLEAN', true, 1, true);
        $this->addColumn('nb_voix', 'NbVoix', 'SMALLINT', false, null, 1);
        $this->addColumn('periodique', 'Periodique', 'VARCHAR', false, 50, null);
        $this->addColumn('signe', 'Signe', 'VARCHAR', true, 1, '+');
        $this->addColumn('objet_beneficiaire', 'ObjetBeneficiaire', 'VARCHAR', true, 50, 'membre');
        $this->addColumn('created_at', 'CreatedAt', 'TIMESTAMP', false, null, null);
        $this->addColumn('updated_at', 'UpdatedAt', 'TIMESTAMP', false, null, null);
    } // initialize()

    /**
     * Build the RelationMap objects for this table relationships
     */
    public function buildRelations()
    {
        $this->addRelation('Entite', '\\Entite', RelationMap::MANY_TO_ONE, array (
  0 =>
  array (
    0 => ':id_entite',
    1 => ':id_entite',
  ),
), 'CASCADE', null, null, false);
        $this->addRelation('Compte', '\\Compte', RelationMap::MANY_TO_ONE, array (
  0 =>
  array (
    0 => ':id_compte',
    1 => ':id_compte',
  ),
), 'CASCADE', null, null, false);
        $this->addRelation('Prix', '\\Prix', RelationMap::ONE_TO_MANY, array (
  0 =>
  array (
    0 => ':id_prestation',
    1 => ':id_prestation',
  ),
), 'CASCADE', null, 'Prixes', false);
        $this->addRelation('PrestationslotPrestation', '\\PrestationslotPrestation', RelationMap::ONE_TO_MANY, array (
  0 =>
  array (
    0 => ':id_prestation',
    1 => ':id_prestation',
  ),
), 'CASCADE', null, 'PrestationslotPrestations', false);
        $this->addRelation('Servicerendu', '\\Servicerendu', RelationMap::ONE_TO_MANY, array (
  0 =>
  array (
    0 => ':id_prestation',
    1 => ':id_prestation',
  ),
), 'CASCADE', null, 'Servicerendus', false);
        $this->addRelation('Prestationslot', '\\Prestationslot', RelationMap::MANY_TO_MANY, array(), 'CASCADE', null, 'Prestationslots');
    } // buildRelations()

    /**
     *
     * Gets the list of behaviors registered for this table
     *
     * @return array Associative array (name => parameters) of behaviors
     */
    public function getBehaviors()
    {
        return array(
            'timestampable' => array('create_column' => 'created_at', 'update_column' => 'updated_at', 'disable_created_at' => 'false', 'disable_updated_at' => 'false', ),
            'archivable' => array('archive_table' => '', 'archive_phpname' => '', 'archive_class' => '', 'log_archived_at' => 'true', 'archived_at_column' => 'archived_at', 'archive_on_insert' => 'false', 'archive_on_update' => 'false', 'archive_on_delete' => 'true', ),
        );
    } // getBehaviors()
    /**
     * Method to invalidate the instance pool of all tables related to asso_prestations     * by a foreign key with ON DELETE CASCADE
     */
    public static function clearRelatedInstancePool()
    {
        // Invalidate objects in related instance pools,
        // since one or more of them may be deleted by ON DELETE CASCADE/SETNULL rule.
        PrixTableMap::clearInstancePool();
        PrestationslotPrestationTableMap::clearInstancePool();
        ServicerenduTableMap::clearInstancePool();
    }

    /**
     * Retrieves a string version of the primary key from the DB resultset row that can be used to uniquely identify a row in this table.
     *
     * For tables with a single-column primary key, that simple pkey value will be returned.  For tables with
     * a multi-column primary key, a serialize()d version of the primary key will be returned.
     *
     * @param array  $row       resultset row.
     * @param int    $offset    The 0-based offset for reading from the resultset row.
     * @param string $indexType One of the class type constants TableMap::TYPE_PHPNAME, TableMap::TYPE_CAMELNAME
     *                           TableMap::TYPE_COLNAME, TableMap::TYPE_FIELDNAME, TableMap::TYPE_NUM
     *
     * @return string The primary key hash of the row
     */
    public static function getPrimaryKeyHashFromRow($row, $offset = 0, $indexType = TableMap::TYPE_NUM)
    {
        // If the PK cannot be derived from the row, return NULL.
        if ($row[TableMap::TYPE_NUM == $indexType ? 0 + $offset : static::translateFieldName('IdPrestation', TableMap::TYPE_PHPNAME, $indexType)] === null) {
            return null;
        }

        return null === $row[TableMap::TYPE_NUM == $indexType ? 0 + $offset : static::translateFieldName('IdPrestation', TableMap::TYPE_PHPNAME, $indexType)] || is_scalar($row[TableMap::TYPE_NUM == $indexType ? 0 + $offset : static::translateFieldName('IdPrestation', TableMap::TYPE_PHPNAME, $indexType)]) || is_callable([$row[TableMap::TYPE_NUM == $indexType ? 0 + $offset : static::translateFieldName('IdPrestation', TableMap::TYPE_PHPNAME, $indexType)], '__toString']) ? (string) $row[TableMap::TYPE_NUM == $indexType ? 0 + $offset : static::translateFieldName('IdPrestation', TableMap::TYPE_PHPNAME, $indexType)] : $row[TableMap::TYPE_NUM == $indexType ? 0 + $offset : static::translateFieldName('IdPrestation', TableMap::TYPE_PHPNAME, $indexType)];
    }

    /**
     * Retrieves the primary key from the DB resultset row
     * For tables with a single-column primary key, that simple pkey value will be returned.  For tables with
     * a multi-column primary key, an array of the primary key columns will be returned.
     *
     * @param array  $row       resultset row.
     * @param int    $offset    The 0-based offset for reading from the resultset row.
     * @param string $indexType One of the class type constants TableMap::TYPE_PHPNAME, TableMap::TYPE_CAMELNAME
     *                           TableMap::TYPE_COLNAME, TableMap::TYPE_FIELDNAME, TableMap::TYPE_NUM
     *
     * @return mixed The primary key of the row
     */
    public static function getPrimaryKeyFromRow($row, $offset = 0, $indexType = TableMap::TYPE_NUM)
    {
        return (string) $row[
            $indexType == TableMap::TYPE_NUM
                ? 0 + $offset
                : self::translateFieldName('IdPrestation', TableMap::TYPE_PHPNAME, $indexType)
        ];
    }

    /**
     * The returned Class will contain objects of the default type or
     * objects that inherit from the default.
     *
     * @param array   $row ConnectionInterface result row.
     * @param int     $colnum Column to examine for OM class information (first is 0).
     * @param boolean $withPrefix Whether or not to return the path with the class name
     * @throws PropelException Any exceptions caught during processing will be
     *                         rethrown wrapped into a PropelException.
     *
     * @return string The OM class
     */
    public static function getOMClass($row, $colnum, $withPrefix = true)
    {
        try {

            $omClass = null;
            $classKey = $row[$colnum + 14];

            switch ($classKey) {

                case PrestationTableMap::CLASSKEY_1:
                    $omClass = PrestationTableMap::CLASSNAME_1;
                    break;

                case PrestationTableMap::CLASSKEY_2:
                    $omClass = PrestationTableMap::CLASSNAME_2;
                    break;

                case PrestationTableMap::CLASSKEY_3:
                    $omClass = PrestationTableMap::CLASSNAME_3;
                    break;

                case PrestationTableMap::CLASSKEY_4:
                    $omClass = PrestationTableMap::CLASSNAME_4;
                    break;

                case PrestationTableMap::CLASSKEY_5:
                    $omClass = PrestationTableMap::CLASSNAME_5;
                    break;

                case PrestationTableMap::CLASSKEY_6:
                    $omClass = PrestationTableMap::CLASSNAME_6;
                    break;

                default:
                    $omClass = PrestationTableMap::CLASS_DEFAULT;

            } // switch
            if (!$withPrefix) {
                $omClass = preg_replace('#\.#', '\\', $omClass);
            }

        } catch (\Exception $e) {
            throw new PropelException('Unable to get OM class.', $e);
        }

        return $omClass;
    }

    /**
     * Populates an object of the default type or an object that inherit from the default.
     *
     * @param array  $row       row returned by DataFetcher->fetch().
     * @param int    $offset    The 0-based offset for reading from the resultset row.
     * @param string $indexType The index type of $row. Mostly DataFetcher->getIndexType().
                                 One of the class type constants TableMap::TYPE_PHPNAME, TableMap::TYPE_CAMELNAME
     *                           TableMap::TYPE_COLNAME, TableMap::TYPE_FIELDNAME, TableMap::TYPE_NUM.
     *
     * @throws PropelException Any exceptions caught during processing will be
     *                         rethrown wrapped into a PropelException.
     * @return array           (Prestation object, last column rank)
     */
    public static function populateObject($row, $offset = 0, $indexType = TableMap::TYPE_NUM)
    {
        $key = PrestationTableMap::getPrimaryKeyHashFromRow($row, $offset, $indexType);
        if (null !== ($obj = PrestationTableMap::getInstanceFromPool($key))) {
            // We no longer rehydrate the object, since this can cause data loss.
            // See http://www.propelorm.org/ticket/509
            // $obj->hydrate($row, $offset, true); // rehydrate
            $col = $offset + PrestationTableMap::NUM_HYDRATE_COLUMNS;
        } else {
            $cls = static::getOMClass($row, $offset, false);
            /** @var Prestation $obj */
            $obj = new $cls();
            $col = $obj->hydrate($row, $offset, false, $indexType);
            PrestationTableMap::addInstanceToPool($obj, $key);
        }

        return array($obj, $col);
    }

    /**
     * The returned array will contain objects of the default type or
     * objects that inherit from the default.
     *
     * @param DataFetcherInterface $dataFetcher
     * @return array
     * @throws PropelException Any exceptions caught during processing will be
     *                         rethrown wrapped into a PropelException.
     */
    public static function populateObjects(DataFetcherInterface $dataFetcher)
    {
        $results = array();

        // populate the object(s)
        while ($row = $dataFetcher->fetch()) {
            $key = PrestationTableMap::getPrimaryKeyHashFromRow($row, 0, $dataFetcher->getIndexType());
            if (null !== ($obj = PrestationTableMap::getInstanceFromPool($key))) {
                // We no longer rehydrate the object, since this can cause data loss.
                // See http://www.propelorm.org/ticket/509
                // $obj->hydrate($row, 0, true); // rehydrate
                $results[] = $obj;
            } else {
                // class must be set each time from the record row
                $cls = static::getOMClass($row, 0);
                $cls = preg_replace('#\.#', '\\', $cls);
                /** @var Prestation $obj */
                $obj = new $cls();
                $obj->hydrate($row);
                $results[] = $obj;
                PrestationTableMap::addInstanceToPool($obj, $key);
            } // if key exists
        }

        return $results;
    }
    /**
     * Add all the columns needed to create a new object.
     *
     * Note: any columns that were marked with lazyLoad="true" in the
     * XML schema will not be added to the select list and only loaded
     * on demand.
     *
     * @param Criteria $criteria object containing the columns to add.
     * @param string   $alias    optional table alias
     * @throws PropelException Any exceptions caught during processing will be
     *                         rethrown wrapped into a PropelException.
     */
    public static function addSelectColumns(Criteria $criteria, $alias = null)
    {
        if (null === $alias) {
            $criteria->addSelectColumn(PrestationTableMap::COL_ID_PRESTATION);
            $criteria->addSelectColumn(PrestationTableMap::COL_NOM_GROUPE);
            $criteria->addSelectColumn(PrestationTableMap::COL_NOM);
            $criteria->addSelectColumn(PrestationTableMap::COL_NOMCOURT);
            $criteria->addSelectColumn(PrestationTableMap::COL_DESCRIPTIF);
            $criteria->addSelectColumn(PrestationTableMap::COL_ID_ENTITE);
            $criteria->addSelectColumn(PrestationTableMap::COL_ID_TVA);
            $criteria->addSelectColumn(PrestationTableMap::COL_RETARD_JOURS);
            $criteria->addSelectColumn(PrestationTableMap::COL_NOMBRE_NUMERO);
            $criteria->addSelectColumn(PrestationTableMap::COL_PROCHAIN_NUMERO);
            $criteria->addSelectColumn(PrestationTableMap::COL_PRIXLIBRE);
            $criteria->addSelectColumn(PrestationTableMap::COL_QUANTITE);
            $criteria->addSelectColumn(PrestationTableMap::COL_ID_UNITE);
            $criteria->addSelectColumn(PrestationTableMap::COL_ID_COMPTE);
            $criteria->addSelectColumn(PrestationTableMap::COL_PRESTATION_TYPE);
            $criteria->addSelectColumn(PrestationTableMap::COL_ACTIVE);
            $criteria->addSelectColumn(PrestationTableMap::COL_NB_VOIX);
            $criteria->addSelectColumn(PrestationTableMap::COL_PERIODIQUE);
            $criteria->addSelectColumn(PrestationTableMap::COL_SIGNE);
            $criteria->addSelectColumn(PrestationTableMap::COL_OBJET_BENEFICIAIRE);
            $criteria->addSelectColumn(PrestationTableMap::COL_CREATED_AT);
            $criteria->addSelectColumn(PrestationTableMap::COL_UPDATED_AT);
        } else {
            $criteria->addSelectColumn($alias . '.id_prestation');
            $criteria->addSelectColumn($alias . '.nom_groupe');
            $criteria->addSelectColumn($alias . '.nom');
            $criteria->addSelectColumn($alias . '.nomcourt');
            $criteria->addSelectColumn($alias . '.descriptif');
            $criteria->addSelectColumn($alias . '.id_entite');
            $criteria->addSelectColumn($alias . '.id_tva');
            $criteria->addSelectColumn($alias . '.retard_jours');
            $criteria->addSelectColumn($alias . '.nombre_numero');
            $criteria->addSelectColumn($alias . '.prochain_numero');
            $criteria->addSelectColumn($alias . '.prixlibre');
            $criteria->addSelectColumn($alias . '.quantite');
            $criteria->addSelectColumn($alias . '.id_unite');
            $criteria->addSelectColumn($alias . '.id_compte');
            $criteria->addSelectColumn($alias . '.prestation_type');
            $criteria->addSelectColumn($alias . '.active');
            $criteria->addSelectColumn($alias . '.nb_voix');
            $criteria->addSelectColumn($alias . '.periodique');
            $criteria->addSelectColumn($alias . '.signe');
            $criteria->addSelectColumn($alias . '.objet_beneficiaire');
            $criteria->addSelectColumn($alias . '.created_at');
            $criteria->addSelectColumn($alias . '.updated_at');
        }
    }

    /**
     * Returns the TableMap related to this object.
     * This method is not needed for general use but a specific application could have a need.
     * @return TableMap
     * @throws PropelException Any exceptions caught during processing will be
     *                         rethrown wrapped into a PropelException.
     */
    public static function getTableMap()
    {
        return Propel::getServiceContainer()->getDatabaseMap(PrestationTableMap::DATABASE_NAME)->getTable(PrestationTableMap::TABLE_NAME);
    }

    /**
     * Add a TableMap instance to the database for this tableMap class.
     */
    public static function buildTableMap()
    {
        $dbMap = Propel::getServiceContainer()->getDatabaseMap(PrestationTableMap::DATABASE_NAME);
        if (!$dbMap->hasTable(PrestationTableMap::TABLE_NAME)) {
            $dbMap->addTableObject(new PrestationTableMap());
        }
    }

    /**
     * Performs a DELETE on the database, given a Prestation or Criteria object OR a primary key value.
     *
     * @param mixed               $values Criteria or Prestation object or primary key or array of primary keys
     *              which is used to create the DELETE statement
     * @param  ConnectionInterface $con the connection to use
     * @return int             The number of affected rows (if supported by underlying database driver).  This includes CASCADE-related rows
     *                         if supported by native driver or if emulated using Propel.
     * @throws PropelException Any exceptions caught during processing will be
     *                         rethrown wrapped into a PropelException.
     */
     public static function doDelete($values, ConnectionInterface $con = null)
     {
        if (null === $con) {
            $con = Propel::getServiceContainer()->getWriteConnection(PrestationTableMap::DATABASE_NAME);
        }

        if ($values instanceof Criteria) {
            // rename for clarity
            $criteria = $values;
        } elseif ($values instanceof \Prestation) { // it's a model object
            // create criteria based on pk values
            $criteria = $values->buildPkeyCriteria();
        } else { // it's a primary key, or an array of pks
            $criteria = new Criteria(PrestationTableMap::DATABASE_NAME);
            $criteria->add(PrestationTableMap::COL_ID_PRESTATION, (array) $values, Criteria::IN);
        }

        $query = PrestationQuery::create()->mergeWith($criteria);

        if ($values instanceof Criteria) {
            PrestationTableMap::clearInstancePool();
        } elseif (!is_object($values)) { // it's a primary key, or an array of pks
            foreach ((array) $values as $singleval) {
                PrestationTableMap::removeInstanceFromPool($singleval);
            }
        }

        return $query->delete($con);
    }

    /**
     * Deletes all rows from the asso_prestations table.
     *
     * @param ConnectionInterface $con the connection to use
     * @return int The number of affected rows (if supported by underlying database driver).
     */
    public static function doDeleteAll(ConnectionInterface $con = null)
    {
        return PrestationQuery::create()->doDeleteAll($con);
    }

    /**
     * Performs an INSERT on the database, given a Prestation or Criteria object.
     *
     * @param mixed               $criteria Criteria or Prestation object containing data that is used to create the INSERT statement.
     * @param ConnectionInterface $con the ConnectionInterface connection to use
     * @return mixed           The new primary key.
     * @throws PropelException Any exceptions caught during processing will be
     *                         rethrown wrapped into a PropelException.
     */
    public static function doInsert($criteria, ConnectionInterface $con = null)
    {
        if (null === $con) {
            $con = Propel::getServiceContainer()->getWriteConnection(PrestationTableMap::DATABASE_NAME);
        }

        if ($criteria instanceof Criteria) {
            $criteria = clone $criteria; // rename for clarity
        } else {
            $criteria = $criteria->buildCriteria(); // build Criteria from Prestation object
        }

        if ($criteria->containsKey(PrestationTableMap::COL_ID_PRESTATION) && $criteria->keyContainsValue(PrestationTableMap::COL_ID_PRESTATION) ) {
            throw new PropelException('Cannot insert a value for auto-increment primary key ('.PrestationTableMap::COL_ID_PRESTATION.')');
        }


        // Set the correct dbName
        $query = PrestationQuery::create()->mergeWith($criteria);

        // use transaction because $criteria could contain info
        // for more than one table (I guess, conceivably)
        return $con->transaction(function () use ($con, $query) {
            return $query->doInsert($con);
        });
    }

} // PrestationTableMap
// This is the static code needed to register the TableMap for this table with the main Propel class.
//
PrestationTableMap::buildTableMap();
