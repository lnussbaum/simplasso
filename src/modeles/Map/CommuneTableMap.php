<?php

namespace Map;

use \Commune;
use \CommuneQuery;
use Propel\Runtime\Propel;
use Propel\Runtime\ActiveQuery\Criteria;
use Propel\Runtime\ActiveQuery\InstancePoolTrait;
use Propel\Runtime\Connection\ConnectionInterface;
use Propel\Runtime\DataFetcher\DataFetcherInterface;
use Propel\Runtime\Exception\PropelException;
use Propel\Runtime\Map\RelationMap;
use Propel\Runtime\Map\TableMap;
use Propel\Runtime\Map\TableMapTrait;


/**
 * This class defines the structure of the 'geo_communes' table.
 *
 *
 *
 * This map class is used by Propel to do runtime db structure discovery.
 * For example, the createSelectSql() method checks the type of a given column used in an
 * ORDER BY clause to know whether it needs to apply SQL to make the ORDER BY case-insensitive
 * (i.e. if it's a text column type).
 *
 */
class CommuneTableMap extends TableMap
{
    use InstancePoolTrait;
    use TableMapTrait;

    /**
     * The (dot-path) name of this class
     */
    const CLASS_NAME = '.Map.CommuneTableMap';

    /**
     * The default database name for this class
     */
    const DATABASE_NAME = 'simplasso';

    /**
     * The table name for this class
     */
    const TABLE_NAME = 'geo_communes';

    /**
     * The related Propel class for this table
     */
    const OM_CLASS = '\\Commune';

    /**
     * A class that can be returned by this tableMap
     */
    const CLASS_DEFAULT = 'Commune';

    /**
     * The total number of columns
     */
    const NUM_COLUMNS = 21;

    /**
     * The number of lazy-loaded columns
     */
    const NUM_LAZY_LOAD_COLUMNS = 0;

    /**
     * The number of columns to hydrate (NUM_COLUMNS - NUM_LAZY_LOAD_COLUMNS)
     */
    const NUM_HYDRATE_COLUMNS = 21;

    /**
     * the column name for the id_commune field
     */
    const COL_ID_COMMUNE = 'geo_communes.id_commune';

    /**
     * the column name for the pays field
     */
    const COL_PAYS = 'geo_communes.pays';

    /**
     * the column name for the region field
     */
    const COL_REGION = 'geo_communes.region';

    /**
     * the column name for the departement field
     */
    const COL_DEPARTEMENT = 'geo_communes.departement';

    /**
     * the column name for the code field
     */
    const COL_CODE = 'geo_communes.code';

    /**
     * the column name for the arrondissement field
     */
    const COL_ARRONDISSEMENT = 'geo_communes.arrondissement';

    /**
     * the column name for the canton field
     */
    const COL_CANTON = 'geo_communes.canton';

    /**
     * the column name for the type_charniere field
     */
    const COL_TYPE_CHARNIERE = 'geo_communes.type_charniere';

    /**
     * the column name for the article field
     */
    const COL_ARTICLE = 'geo_communes.article';

    /**
     * the column name for the nom field
     */
    const COL_NOM = 'geo_communes.nom';

    /**
     * the column name for the lon field
     */
    const COL_LON = 'geo_communes.lon';

    /**
     * the column name for the lat field
     */
    const COL_LAT = 'geo_communes.lat';

    /**
     * the column name for the zoom field
     */
    const COL_ZOOM = 'geo_communes.zoom';

    /**
     * the column name for the elevation field
     */
    const COL_ELEVATION = 'geo_communes.elevation';

    /**
     * the column name for the elevation_moyenne field
     */
    const COL_ELEVATION_MOYENNE = 'geo_communes.elevation_moyenne';

    /**
     * the column name for the population field
     */
    const COL_POPULATION = 'geo_communes.population';

    /**
     * the column name for the autre_nom field
     */
    const COL_AUTRE_NOM = 'geo_communes.autre_nom';

    /**
     * the column name for the url field
     */
    const COL_URL = 'geo_communes.url';

    /**
     * the column name for the zonage field
     */
    const COL_ZONAGE = 'geo_communes.zonage';

    /**
     * the column name for the created_at field
     */
    const COL_CREATED_AT = 'geo_communes.created_at';

    /**
     * the column name for the updated_at field
     */
    const COL_UPDATED_AT = 'geo_communes.updated_at';

    /**
     * The default string format for model objects of the related table
     */
    const DEFAULT_STRING_FORMAT = 'YAML';

    /**
     * holds an array of fieldnames
     *
     * first dimension keys are the type constants
     * e.g. self::$fieldNames[self::TYPE_PHPNAME][0] = 'Id'
     */
    protected static $fieldNames = array (
        self::TYPE_PHPNAME       => array('IdCommune', 'Pays', 'Region', 'Departement', 'Code', 'Arrondissement', 'Canton', 'TypeCharniere', 'Article', 'Nom', 'Lon', 'Lat', 'Zoom', 'Elevation', 'ElevationMoyenne', 'Population', 'AutreNom', 'Url', 'Zonage', 'CreatedAt', 'UpdatedAt', ),
        self::TYPE_CAMELNAME     => array('idCommune', 'pays', 'region', 'departement', 'code', 'arrondissement', 'canton', 'typeCharniere', 'article', 'nom', 'lon', 'lat', 'zoom', 'elevation', 'elevationMoyenne', 'population', 'autreNom', 'url', 'zonage', 'createdAt', 'updatedAt', ),
        self::TYPE_COLNAME       => array(CommuneTableMap::COL_ID_COMMUNE, CommuneTableMap::COL_PAYS, CommuneTableMap::COL_REGION, CommuneTableMap::COL_DEPARTEMENT, CommuneTableMap::COL_CODE, CommuneTableMap::COL_ARRONDISSEMENT, CommuneTableMap::COL_CANTON, CommuneTableMap::COL_TYPE_CHARNIERE, CommuneTableMap::COL_ARTICLE, CommuneTableMap::COL_NOM, CommuneTableMap::COL_LON, CommuneTableMap::COL_LAT, CommuneTableMap::COL_ZOOM, CommuneTableMap::COL_ELEVATION, CommuneTableMap::COL_ELEVATION_MOYENNE, CommuneTableMap::COL_POPULATION, CommuneTableMap::COL_AUTRE_NOM, CommuneTableMap::COL_URL, CommuneTableMap::COL_ZONAGE, CommuneTableMap::COL_CREATED_AT, CommuneTableMap::COL_UPDATED_AT, ),
        self::TYPE_FIELDNAME     => array('id_commune', 'pays', 'region', 'departement', 'code', 'arrondissement', 'canton', 'type_charniere', 'article', 'nom', 'lon', 'lat', 'zoom', 'elevation', 'elevation_moyenne', 'population', 'autre_nom', 'url', 'zonage', 'created_at', 'updated_at', ),
        self::TYPE_NUM           => array(0, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15, 16, 17, 18, 19, 20, )
    );

    /**
     * holds an array of keys for quick access to the fieldnames array
     *
     * first dimension keys are the type constants
     * e.g. self::$fieldKeys[self::TYPE_PHPNAME]['Id'] = 0
     */
    protected static $fieldKeys = array (
        self::TYPE_PHPNAME       => array('IdCommune' => 0, 'Pays' => 1, 'Region' => 2, 'Departement' => 3, 'Code' => 4, 'Arrondissement' => 5, 'Canton' => 6, 'TypeCharniere' => 7, 'Article' => 8, 'Nom' => 9, 'Lon' => 10, 'Lat' => 11, 'Zoom' => 12, 'Elevation' => 13, 'ElevationMoyenne' => 14, 'Population' => 15, 'AutreNom' => 16, 'Url' => 17, 'Zonage' => 18, 'CreatedAt' => 19, 'UpdatedAt' => 20, ),
        self::TYPE_CAMELNAME     => array('idCommune' => 0, 'pays' => 1, 'region' => 2, 'departement' => 3, 'code' => 4, 'arrondissement' => 5, 'canton' => 6, 'typeCharniere' => 7, 'article' => 8, 'nom' => 9, 'lon' => 10, 'lat' => 11, 'zoom' => 12, 'elevation' => 13, 'elevationMoyenne' => 14, 'population' => 15, 'autreNom' => 16, 'url' => 17, 'zonage' => 18, 'createdAt' => 19, 'updatedAt' => 20, ),
        self::TYPE_COLNAME       => array(CommuneTableMap::COL_ID_COMMUNE => 0, CommuneTableMap::COL_PAYS => 1, CommuneTableMap::COL_REGION => 2, CommuneTableMap::COL_DEPARTEMENT => 3, CommuneTableMap::COL_CODE => 4, CommuneTableMap::COL_ARRONDISSEMENT => 5, CommuneTableMap::COL_CANTON => 6, CommuneTableMap::COL_TYPE_CHARNIERE => 7, CommuneTableMap::COL_ARTICLE => 8, CommuneTableMap::COL_NOM => 9, CommuneTableMap::COL_LON => 10, CommuneTableMap::COL_LAT => 11, CommuneTableMap::COL_ZOOM => 12, CommuneTableMap::COL_ELEVATION => 13, CommuneTableMap::COL_ELEVATION_MOYENNE => 14, CommuneTableMap::COL_POPULATION => 15, CommuneTableMap::COL_AUTRE_NOM => 16, CommuneTableMap::COL_URL => 17, CommuneTableMap::COL_ZONAGE => 18, CommuneTableMap::COL_CREATED_AT => 19, CommuneTableMap::COL_UPDATED_AT => 20, ),
        self::TYPE_FIELDNAME     => array('id_commune' => 0, 'pays' => 1, 'region' => 2, 'departement' => 3, 'code' => 4, 'arrondissement' => 5, 'canton' => 6, 'type_charniere' => 7, 'article' => 8, 'nom' => 9, 'lon' => 10, 'lat' => 11, 'zoom' => 12, 'elevation' => 13, 'elevation_moyenne' => 14, 'population' => 15, 'autre_nom' => 16, 'url' => 17, 'zonage' => 18, 'created_at' => 19, 'updated_at' => 20, ),
        self::TYPE_NUM           => array(0, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15, 16, 17, 18, 19, 20, )
    );

    /**
     * Initialize the table attributes and columns
     * Relations are not initialized by this method since they are lazy loaded
     *
     * @return void
     * @throws PropelException
     */
    public function initialize()
    {
        // attributes
        $this->setName('geo_communes');
        $this->setPhpName('Commune');
        $this->setIdentifierQuoting(true);
        $this->setClassName('\\Commune');
        $this->setPackage('');
        $this->setUseIdGenerator(true);
        // columns
        $this->addPrimaryKey('id_commune', 'IdCommune', 'INTEGER', true, 10, null);
        $this->addColumn('pays', 'Pays', 'VARCHAR', true, 2, null);
        $this->addColumn('region', 'Region', 'TINYINT', true, 2, null);
        $this->addColumn('departement', 'Departement', 'VARCHAR', true, 3, null);
        $this->addColumn('code', 'Code', 'SMALLINT', true, 3, null);
        $this->addColumn('arrondissement', 'Arrondissement', 'BOOLEAN', true, 1, null);
        $this->addColumn('canton', 'Canton', 'TINYINT', true, 2, null);
        $this->addColumn('type_charniere', 'TypeCharniere', 'BOOLEAN', true, 1, null);
        $this->addColumn('article', 'Article', 'VARCHAR', true, 5, null);
        $this->addColumn('nom', 'Nom', 'LONGVARCHAR', true, null, null);
        $this->addColumn('lon', 'Lon', 'DECIMAL', false, 10, null);
        $this->addColumn('lat', 'Lat', 'DECIMAL', false, 10, null);
        $this->addColumn('zoom', 'Zoom', 'TINYINT', false, null, null);
        $this->addColumn('elevation', 'Elevation', 'INTEGER', false, null, null);
        $this->addColumn('elevation_moyenne', 'ElevationMoyenne', 'INTEGER', false, null, null);
        $this->addColumn('population', 'Population', 'BIGINT', false, null, null);
        $this->addColumn('autre_nom', 'AutreNom', 'LONGVARCHAR', false, null, null);
        $this->addColumn('url', 'Url', 'VARCHAR', false, 255, null);
        $this->addColumn('zonage', 'Zonage', 'LONGVARCHAR', false, null, null);
        $this->addColumn('created_at', 'CreatedAt', 'TIMESTAMP', false, null, null);
        $this->addColumn('updated_at', 'UpdatedAt', 'TIMESTAMP', false, null, null);
    } // initialize()

    /**
     * Build the RelationMap objects for this table relationships
     */
    public function buildRelations()
    {
    } // buildRelations()

    /**
     *
     * Gets the list of behaviors registered for this table
     *
     * @return array Associative array (name => parameters) of behaviors
     */
    public function getBehaviors()
    {
        return array(
            'timestampable' => array('create_column' => 'created_at', 'update_column' => 'updated_at', 'disable_created_at' => 'false', 'disable_updated_at' => 'false', ),
        );
    } // getBehaviors()

    /**
     * Retrieves a string version of the primary key from the DB resultset row that can be used to uniquely identify a row in this table.
     *
     * For tables with a single-column primary key, that simple pkey value will be returned.  For tables with
     * a multi-column primary key, a serialize()d version of the primary key will be returned.
     *
     * @param array  $row       resultset row.
     * @param int    $offset    The 0-based offset for reading from the resultset row.
     * @param string $indexType One of the class type constants TableMap::TYPE_PHPNAME, TableMap::TYPE_CAMELNAME
     *                           TableMap::TYPE_COLNAME, TableMap::TYPE_FIELDNAME, TableMap::TYPE_NUM
     *
     * @return string The primary key hash of the row
     */
    public static function getPrimaryKeyHashFromRow($row, $offset = 0, $indexType = TableMap::TYPE_NUM)
    {
        // If the PK cannot be derived from the row, return NULL.
        if ($row[TableMap::TYPE_NUM == $indexType ? 0 + $offset : static::translateFieldName('IdCommune', TableMap::TYPE_PHPNAME, $indexType)] === null) {
            return null;
        }

        return null === $row[TableMap::TYPE_NUM == $indexType ? 0 + $offset : static::translateFieldName('IdCommune', TableMap::TYPE_PHPNAME, $indexType)] || is_scalar($row[TableMap::TYPE_NUM == $indexType ? 0 + $offset : static::translateFieldName('IdCommune', TableMap::TYPE_PHPNAME, $indexType)]) || is_callable([$row[TableMap::TYPE_NUM == $indexType ? 0 + $offset : static::translateFieldName('IdCommune', TableMap::TYPE_PHPNAME, $indexType)], '__toString']) ? (string) $row[TableMap::TYPE_NUM == $indexType ? 0 + $offset : static::translateFieldName('IdCommune', TableMap::TYPE_PHPNAME, $indexType)] : $row[TableMap::TYPE_NUM == $indexType ? 0 + $offset : static::translateFieldName('IdCommune', TableMap::TYPE_PHPNAME, $indexType)];
    }

    /**
     * Retrieves the primary key from the DB resultset row
     * For tables with a single-column primary key, that simple pkey value will be returned.  For tables with
     * a multi-column primary key, an array of the primary key columns will be returned.
     *
     * @param array  $row       resultset row.
     * @param int    $offset    The 0-based offset for reading from the resultset row.
     * @param string $indexType One of the class type constants TableMap::TYPE_PHPNAME, TableMap::TYPE_CAMELNAME
     *                           TableMap::TYPE_COLNAME, TableMap::TYPE_FIELDNAME, TableMap::TYPE_NUM
     *
     * @return mixed The primary key of the row
     */
    public static function getPrimaryKeyFromRow($row, $offset = 0, $indexType = TableMap::TYPE_NUM)
    {
        return (int) $row[
            $indexType == TableMap::TYPE_NUM
                ? 0 + $offset
                : self::translateFieldName('IdCommune', TableMap::TYPE_PHPNAME, $indexType)
        ];
    }

    /**
     * The class that the tableMap will make instances of.
     *
     * If $withPrefix is true, the returned path
     * uses a dot-path notation which is translated into a path
     * relative to a location on the PHP include_path.
     * (e.g. path.to.MyClass -> 'path/to/MyClass.php')
     *
     * @param boolean $withPrefix Whether or not to return the path with the class name
     * @return string path.to.ClassName
     */
    public static function getOMClass($withPrefix = true)
    {
        return $withPrefix ? CommuneTableMap::CLASS_DEFAULT : CommuneTableMap::OM_CLASS;
    }

    /**
     * Populates an object of the default type or an object that inherit from the default.
     *
     * @param array  $row       row returned by DataFetcher->fetch().
     * @param int    $offset    The 0-based offset for reading from the resultset row.
     * @param string $indexType The index type of $row. Mostly DataFetcher->getIndexType().
                                 One of the class type constants TableMap::TYPE_PHPNAME, TableMap::TYPE_CAMELNAME
     *                           TableMap::TYPE_COLNAME, TableMap::TYPE_FIELDNAME, TableMap::TYPE_NUM.
     *
     * @throws PropelException Any exceptions caught during processing will be
     *                         rethrown wrapped into a PropelException.
     * @return array           (Commune object, last column rank)
     */
    public static function populateObject($row, $offset = 0, $indexType = TableMap::TYPE_NUM)
    {
        $key = CommuneTableMap::getPrimaryKeyHashFromRow($row, $offset, $indexType);
        if (null !== ($obj = CommuneTableMap::getInstanceFromPool($key))) {
            // We no longer rehydrate the object, since this can cause data loss.
            // See http://www.propelorm.org/ticket/509
            // $obj->hydrate($row, $offset, true); // rehydrate
            $col = $offset + CommuneTableMap::NUM_HYDRATE_COLUMNS;
        } else {
            $cls = CommuneTableMap::OM_CLASS;
            /** @var Commune $obj */
            $obj = new $cls();
            $col = $obj->hydrate($row, $offset, false, $indexType);
            CommuneTableMap::addInstanceToPool($obj, $key);
        }

        return array($obj, $col);
    }

    /**
     * The returned array will contain objects of the default type or
     * objects that inherit from the default.
     *
     * @param DataFetcherInterface $dataFetcher
     * @return array
     * @throws PropelException Any exceptions caught during processing will be
     *                         rethrown wrapped into a PropelException.
     */
    public static function populateObjects(DataFetcherInterface $dataFetcher)
    {
        $results = array();

        // set the class once to avoid overhead in the loop
        $cls = static::getOMClass(false);
        // populate the object(s)
        while ($row = $dataFetcher->fetch()) {
            $key = CommuneTableMap::getPrimaryKeyHashFromRow($row, 0, $dataFetcher->getIndexType());
            if (null !== ($obj = CommuneTableMap::getInstanceFromPool($key))) {
                // We no longer rehydrate the object, since this can cause data loss.
                // See http://www.propelorm.org/ticket/509
                // $obj->hydrate($row, 0, true); // rehydrate
                $results[] = $obj;
            } else {
                /** @var Commune $obj */
                $obj = new $cls();
                $obj->hydrate($row);
                $results[] = $obj;
                CommuneTableMap::addInstanceToPool($obj, $key);
            } // if key exists
        }

        return $results;
    }
    /**
     * Add all the columns needed to create a new object.
     *
     * Note: any columns that were marked with lazyLoad="true" in the
     * XML schema will not be added to the select list and only loaded
     * on demand.
     *
     * @param Criteria $criteria object containing the columns to add.
     * @param string   $alias    optional table alias
     * @throws PropelException Any exceptions caught during processing will be
     *                         rethrown wrapped into a PropelException.
     */
    public static function addSelectColumns(Criteria $criteria, $alias = null)
    {
        if (null === $alias) {
            $criteria->addSelectColumn(CommuneTableMap::COL_ID_COMMUNE);
            $criteria->addSelectColumn(CommuneTableMap::COL_PAYS);
            $criteria->addSelectColumn(CommuneTableMap::COL_REGION);
            $criteria->addSelectColumn(CommuneTableMap::COL_DEPARTEMENT);
            $criteria->addSelectColumn(CommuneTableMap::COL_CODE);
            $criteria->addSelectColumn(CommuneTableMap::COL_ARRONDISSEMENT);
            $criteria->addSelectColumn(CommuneTableMap::COL_CANTON);
            $criteria->addSelectColumn(CommuneTableMap::COL_TYPE_CHARNIERE);
            $criteria->addSelectColumn(CommuneTableMap::COL_ARTICLE);
            $criteria->addSelectColumn(CommuneTableMap::COL_NOM);
            $criteria->addSelectColumn(CommuneTableMap::COL_LON);
            $criteria->addSelectColumn(CommuneTableMap::COL_LAT);
            $criteria->addSelectColumn(CommuneTableMap::COL_ZOOM);
            $criteria->addSelectColumn(CommuneTableMap::COL_ELEVATION);
            $criteria->addSelectColumn(CommuneTableMap::COL_ELEVATION_MOYENNE);
            $criteria->addSelectColumn(CommuneTableMap::COL_POPULATION);
            $criteria->addSelectColumn(CommuneTableMap::COL_AUTRE_NOM);
            $criteria->addSelectColumn(CommuneTableMap::COL_URL);
            $criteria->addSelectColumn(CommuneTableMap::COL_ZONAGE);
            $criteria->addSelectColumn(CommuneTableMap::COL_CREATED_AT);
            $criteria->addSelectColumn(CommuneTableMap::COL_UPDATED_AT);
        } else {
            $criteria->addSelectColumn($alias . '.id_commune');
            $criteria->addSelectColumn($alias . '.pays');
            $criteria->addSelectColumn($alias . '.region');
            $criteria->addSelectColumn($alias . '.departement');
            $criteria->addSelectColumn($alias . '.code');
            $criteria->addSelectColumn($alias . '.arrondissement');
            $criteria->addSelectColumn($alias . '.canton');
            $criteria->addSelectColumn($alias . '.type_charniere');
            $criteria->addSelectColumn($alias . '.article');
            $criteria->addSelectColumn($alias . '.nom');
            $criteria->addSelectColumn($alias . '.lon');
            $criteria->addSelectColumn($alias . '.lat');
            $criteria->addSelectColumn($alias . '.zoom');
            $criteria->addSelectColumn($alias . '.elevation');
            $criteria->addSelectColumn($alias . '.elevation_moyenne');
            $criteria->addSelectColumn($alias . '.population');
            $criteria->addSelectColumn($alias . '.autre_nom');
            $criteria->addSelectColumn($alias . '.url');
            $criteria->addSelectColumn($alias . '.zonage');
            $criteria->addSelectColumn($alias . '.created_at');
            $criteria->addSelectColumn($alias . '.updated_at');
        }
    }

    /**
     * Returns the TableMap related to this object.
     * This method is not needed for general use but a specific application could have a need.
     * @return TableMap
     * @throws PropelException Any exceptions caught during processing will be
     *                         rethrown wrapped into a PropelException.
     */
    public static function getTableMap()
    {
        return Propel::getServiceContainer()->getDatabaseMap(CommuneTableMap::DATABASE_NAME)->getTable(CommuneTableMap::TABLE_NAME);
    }

    /**
     * Add a TableMap instance to the database for this tableMap class.
     */
    public static function buildTableMap()
    {
        $dbMap = Propel::getServiceContainer()->getDatabaseMap(CommuneTableMap::DATABASE_NAME);
        if (!$dbMap->hasTable(CommuneTableMap::TABLE_NAME)) {
            $dbMap->addTableObject(new CommuneTableMap());
        }
    }

    /**
     * Performs a DELETE on the database, given a Commune or Criteria object OR a primary key value.
     *
     * @param mixed               $values Criteria or Commune object or primary key or array of primary keys
     *              which is used to create the DELETE statement
     * @param  ConnectionInterface $con the connection to use
     * @return int             The number of affected rows (if supported by underlying database driver).  This includes CASCADE-related rows
     *                         if supported by native driver or if emulated using Propel.
     * @throws PropelException Any exceptions caught during processing will be
     *                         rethrown wrapped into a PropelException.
     */
     public static function doDelete($values, ConnectionInterface $con = null)
     {
        if (null === $con) {
            $con = Propel::getServiceContainer()->getWriteConnection(CommuneTableMap::DATABASE_NAME);
        }

        if ($values instanceof Criteria) {
            // rename for clarity
            $criteria = $values;
        } elseif ($values instanceof \Commune) { // it's a model object
            // create criteria based on pk values
            $criteria = $values->buildPkeyCriteria();
        } else { // it's a primary key, or an array of pks
            $criteria = new Criteria(CommuneTableMap::DATABASE_NAME);
            $criteria->add(CommuneTableMap::COL_ID_COMMUNE, (array) $values, Criteria::IN);
        }

        $query = CommuneQuery::create()->mergeWith($criteria);

        if ($values instanceof Criteria) {
            CommuneTableMap::clearInstancePool();
        } elseif (!is_object($values)) { // it's a primary key, or an array of pks
            foreach ((array) $values as $singleval) {
                CommuneTableMap::removeInstanceFromPool($singleval);
            }
        }

        return $query->delete($con);
    }

    /**
     * Deletes all rows from the geo_communes table.
     *
     * @param ConnectionInterface $con the connection to use
     * @return int The number of affected rows (if supported by underlying database driver).
     */
    public static function doDeleteAll(ConnectionInterface $con = null)
    {
        return CommuneQuery::create()->doDeleteAll($con);
    }

    /**
     * Performs an INSERT on the database, given a Commune or Criteria object.
     *
     * @param mixed               $criteria Criteria or Commune object containing data that is used to create the INSERT statement.
     * @param ConnectionInterface $con the ConnectionInterface connection to use
     * @return mixed           The new primary key.
     * @throws PropelException Any exceptions caught during processing will be
     *                         rethrown wrapped into a PropelException.
     */
    public static function doInsert($criteria, ConnectionInterface $con = null)
    {
        if (null === $con) {
            $con = Propel::getServiceContainer()->getWriteConnection(CommuneTableMap::DATABASE_NAME);
        }

        if ($criteria instanceof Criteria) {
            $criteria = clone $criteria; // rename for clarity
        } else {
            $criteria = $criteria->buildCriteria(); // build Criteria from Commune object
        }

        if ($criteria->containsKey(CommuneTableMap::COL_ID_COMMUNE) && $criteria->keyContainsValue(CommuneTableMap::COL_ID_COMMUNE) ) {
            throw new PropelException('Cannot insert a value for auto-increment primary key ('.CommuneTableMap::COL_ID_COMMUNE.')');
        }


        // Set the correct dbName
        $query = CommuneQuery::create()->mergeWith($criteria);

        // use transaction because $criteria could contain info
        // for more than one table (I guess, conceivably)
        return $con->transaction(function () use ($con, $query) {
            return $query->doInsert($con);
        });
    }

} // CommuneTableMap
// This is the static code needed to register the TableMap for this table with the main Propel class.
//
CommuneTableMap::buildTableMap();
