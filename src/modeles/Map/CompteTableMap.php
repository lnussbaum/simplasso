<?php

namespace Map;

use \Compte;
use \CompteQuery;
use Propel\Runtime\Propel;
use Propel\Runtime\ActiveQuery\Criteria;
use Propel\Runtime\ActiveQuery\InstancePoolTrait;
use Propel\Runtime\Connection\ConnectionInterface;
use Propel\Runtime\DataFetcher\DataFetcherInterface;
use Propel\Runtime\Exception\PropelException;
use Propel\Runtime\Map\RelationMap;
use Propel\Runtime\Map\TableMap;
use Propel\Runtime\Map\TableMapTrait;


/**
 * This class defines the structure of the 'assc_comptes' table.
 *
 *
 *
 * This map class is used by Propel to do runtime db structure discovery.
 * For example, the createSelectSql() method checks the type of a given column used in an
 * ORDER BY clause to know whether it needs to apply SQL to make the ORDER BY case-insensitive
 * (i.e. if it's a text column type).
 *
 */
class CompteTableMap extends TableMap
{
    use InstancePoolTrait;
    use TableMapTrait;

    /**
     * The (dot-path) name of this class
     */
    const CLASS_NAME = '.Map.CompteTableMap';

    /**
     * The default database name for this class
     */
    const DATABASE_NAME = 'simplasso';

    /**
     * The table name for this class
     */
    const TABLE_NAME = 'assc_comptes';

    /**
     * The related Propel class for this table
     */
    const OM_CLASS = '\\Compte';

    /**
     * A class that can be returned by this tableMap
     */
    const CLASS_DEFAULT = 'Compte';

    /**
     * The total number of columns
     */
    const NUM_COLUMNS = 12;

    /**
     * The number of lazy-loaded columns
     */
    const NUM_LAZY_LOAD_COLUMNS = 0;

    /**
     * The number of columns to hydrate (NUM_COLUMNS - NUM_LAZY_LOAD_COLUMNS)
     */
    const NUM_HYDRATE_COLUMNS = 12;

    /**
     * the column name for the id_compte field
     */
    const COL_ID_COMPTE = 'assc_comptes.id_compte';

    /**
     * the column name for the ncompte field
     */
    const COL_NCOMPTE = 'assc_comptes.ncompte';

    /**
     * the column name for the nom field
     */
    const COL_NOM = 'assc_comptes.nom';

    /**
     * the column name for the nomcourt field
     */
    const COL_NOMCOURT = 'assc_comptes.nomcourt';

    /**
     * the column name for the id_poste field
     */
    const COL_ID_POSTE = 'assc_comptes.id_poste';

    /**
     * the column name for the type_op field
     */
    const COL_TYPE_OP = 'assc_comptes.type_op';

    /**
     * the column name for the solde_anterieur field
     */
    const COL_SOLDE_ANTERIEUR = 'assc_comptes.solde_anterieur';

    /**
     * the column name for the date_anterieure field
     */
    const COL_DATE_ANTERIEURE = 'assc_comptes.date_anterieure';

    /**
     * the column name for the id_entite field
     */
    const COL_ID_ENTITE = 'assc_comptes.id_entite';

    /**
     * the column name for the observation field
     */
    const COL_OBSERVATION = 'assc_comptes.observation';

    /**
     * the column name for the created_at field
     */
    const COL_CREATED_AT = 'assc_comptes.created_at';

    /**
     * the column name for the updated_at field
     */
    const COL_UPDATED_AT = 'assc_comptes.updated_at';

    /**
     * The default string format for model objects of the related table
     */
    const DEFAULT_STRING_FORMAT = 'YAML';

    /**
     * holds an array of fieldnames
     *
     * first dimension keys are the type constants
     * e.g. self::$fieldNames[self::TYPE_PHPNAME][0] = 'Id'
     */
    protected static $fieldNames = array (
        self::TYPE_PHPNAME       => array('IdCompte', 'Ncompte', 'Nom', 'Nomcourt', 'IdPoste', 'TypeOp', 'SoldeAnterieur', 'DateAnterieure', 'IdEntite', 'Observation', 'CreatedAt', 'UpdatedAt', ),
        self::TYPE_CAMELNAME     => array('idCompte', 'ncompte', 'nom', 'nomcourt', 'idPoste', 'typeOp', 'soldeAnterieur', 'dateAnterieure', 'idEntite', 'observation', 'createdAt', 'updatedAt', ),
        self::TYPE_COLNAME       => array(CompteTableMap::COL_ID_COMPTE, CompteTableMap::COL_NCOMPTE, CompteTableMap::COL_NOM, CompteTableMap::COL_NOMCOURT, CompteTableMap::COL_ID_POSTE, CompteTableMap::COL_TYPE_OP, CompteTableMap::COL_SOLDE_ANTERIEUR, CompteTableMap::COL_DATE_ANTERIEURE, CompteTableMap::COL_ID_ENTITE, CompteTableMap::COL_OBSERVATION, CompteTableMap::COL_CREATED_AT, CompteTableMap::COL_UPDATED_AT, ),
        self::TYPE_FIELDNAME     => array('id_compte', 'ncompte', 'nom', 'nomcourt', 'id_poste', 'type_op', 'solde_anterieur', 'date_anterieure', 'id_entite', 'observation', 'created_at', 'updated_at', ),
        self::TYPE_NUM           => array(0, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, )
    );

    /**
     * holds an array of keys for quick access to the fieldnames array
     *
     * first dimension keys are the type constants
     * e.g. self::$fieldKeys[self::TYPE_PHPNAME]['Id'] = 0
     */
    protected static $fieldKeys = array (
        self::TYPE_PHPNAME       => array('IdCompte' => 0, 'Ncompte' => 1, 'Nom' => 2, 'Nomcourt' => 3, 'IdPoste' => 4, 'TypeOp' => 5, 'SoldeAnterieur' => 6, 'DateAnterieure' => 7, 'IdEntite' => 8, 'Observation' => 9, 'CreatedAt' => 10, 'UpdatedAt' => 11, ),
        self::TYPE_CAMELNAME     => array('idCompte' => 0, 'ncompte' => 1, 'nom' => 2, 'nomcourt' => 3, 'idPoste' => 4, 'typeOp' => 5, 'soldeAnterieur' => 6, 'dateAnterieure' => 7, 'idEntite' => 8, 'observation' => 9, 'createdAt' => 10, 'updatedAt' => 11, ),
        self::TYPE_COLNAME       => array(CompteTableMap::COL_ID_COMPTE => 0, CompteTableMap::COL_NCOMPTE => 1, CompteTableMap::COL_NOM => 2, CompteTableMap::COL_NOMCOURT => 3, CompteTableMap::COL_ID_POSTE => 4, CompteTableMap::COL_TYPE_OP => 5, CompteTableMap::COL_SOLDE_ANTERIEUR => 6, CompteTableMap::COL_DATE_ANTERIEURE => 7, CompteTableMap::COL_ID_ENTITE => 8, CompteTableMap::COL_OBSERVATION => 9, CompteTableMap::COL_CREATED_AT => 10, CompteTableMap::COL_UPDATED_AT => 11, ),
        self::TYPE_FIELDNAME     => array('id_compte' => 0, 'ncompte' => 1, 'nom' => 2, 'nomcourt' => 3, 'id_poste' => 4, 'type_op' => 5, 'solde_anterieur' => 6, 'date_anterieure' => 7, 'id_entite' => 8, 'observation' => 9, 'created_at' => 10, 'updated_at' => 11, ),
        self::TYPE_NUM           => array(0, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, )
    );

    /**
     * Initialize the table attributes and columns
     * Relations are not initialized by this method since they are lazy loaded
     *
     * @return void
     * @throws PropelException
     */
    public function initialize()
    {
        // attributes
        $this->setName('assc_comptes');
        $this->setPhpName('Compte');
        $this->setIdentifierQuoting(true);
        $this->setClassName('\\Compte');
        $this->setPackage('');
        $this->setUseIdGenerator(true);
        // columns
        $this->addPrimaryKey('id_compte', 'IdCompte', 'BIGINT', true, 21, null);
        $this->addColumn('ncompte', 'Ncompte', 'VARCHAR', true, 12, '512NEF');
        $this->addColumn('nom', 'Nom', 'VARCHAR', true, 80, 'Compte courant NEF');
        $this->addColumn('nomcourt', 'Nomcourt', 'VARCHAR', true, 20, 'CC NEF');
        $this->addForeignKey('id_poste', 'IdPoste', 'BIGINT', 'assc_postes', 'id_poste', true, 21, 1);
        $this->addColumn('type_op', 'TypeOp', 'INTEGER', true, 2, 3);
        $this->addColumn('solde_anterieur', 'SoldeAnterieur', 'DOUBLE', false, null, null);
        $this->addColumn('date_anterieure', 'DateAnterieure', 'DATE', false, null, null);
        $this->addForeignKey('id_entite', 'IdEntite', 'BIGINT', 'asso_entites', 'id_entite', true, 21, 1);
        $this->addColumn('observation', 'Observation', 'LONGVARCHAR', false, null, null);
        $this->addColumn('created_at', 'CreatedAt', 'TIMESTAMP', false, null, null);
        $this->addColumn('updated_at', 'UpdatedAt', 'TIMESTAMP', false, null, null);
    } // initialize()

    /**
     * Build the RelationMap objects for this table relationships
     */
    public function buildRelations()
    {
        $this->addRelation('Entite', '\\Entite', RelationMap::MANY_TO_ONE, array (
  0 =>
  array (
    0 => ':id_entite',
    1 => ':id_entite',
  ),
), 'CASCADE', null, null, false);
        $this->addRelation('Poste', '\\Poste', RelationMap::MANY_TO_ONE, array (
  0 =>
  array (
    0 => ':id_poste',
    1 => ':id_poste',
  ),
), 'CASCADE', null, null, false);
        $this->addRelation('Ecriture', '\\Ecriture', RelationMap::ONE_TO_MANY, array (
  0 =>
  array (
    0 => ':id_compte',
    1 => ':id_compte',
  ),
), 'CASCADE', null, 'Ecritures', false);
        $this->addRelation('Journal', '\\Journal', RelationMap::ONE_TO_MANY, array (
  0 =>
  array (
    0 => ':id_compte',
    1 => ':id_compte',
  ),
), 'CASCADE', null, 'Journals', false);
        $this->addRelation('Prestation', '\\Prestation', RelationMap::ONE_TO_MANY, array (
  0 =>
  array (
    0 => ':id_compte',
    1 => ':id_compte',
  ),
), 'CASCADE', null, 'Prestations', false);
        $this->addRelation('Tresor', '\\Tresor', RelationMap::ONE_TO_MANY, array (
  0 =>
  array (
    0 => ':id_compte',
    1 => ':id_compte',
  ),
), 'CASCADE', null, 'Tresors', false);
        $this->addRelation('Tva', '\\Tva', RelationMap::ONE_TO_MANY, array (
  0 =>
  array (
    0 => ':id_compte',
    1 => ':id_compte',
  ),
), 'CASCADE', null, 'Tvas', false);
    } // buildRelations()

    /**
     *
     * Gets the list of behaviors registered for this table
     *
     * @return array Associative array (name => parameters) of behaviors
     */
    public function getBehaviors()
    {
        return array(
            'timestampable' => array('create_column' => 'created_at', 'update_column' => 'updated_at', 'disable_created_at' => 'false', 'disable_updated_at' => 'false', ),
            'archivable' => array('archive_table' => '', 'archive_phpname' => '', 'archive_class' => '', 'log_archived_at' => 'true', 'archived_at_column' => 'archived_at', 'archive_on_insert' => 'false', 'archive_on_update' => 'false', 'archive_on_delete' => 'true', ),
        );
    } // getBehaviors()
    /**
     * Method to invalidate the instance pool of all tables related to assc_comptes     * by a foreign key with ON DELETE CASCADE
     */
    public static function clearRelatedInstancePool()
    {
        // Invalidate objects in related instance pools,
        // since one or more of them may be deleted by ON DELETE CASCADE/SETNULL rule.
        EcritureTableMap::clearInstancePool();
        JournalTableMap::clearInstancePool();
        PrestationTableMap::clearInstancePool();
        TresorTableMap::clearInstancePool();
        TvaTableMap::clearInstancePool();
    }

    /**
     * Retrieves a string version of the primary key from the DB resultset row that can be used to uniquely identify a row in this table.
     *
     * For tables with a single-column primary key, that simple pkey value will be returned.  For tables with
     * a multi-column primary key, a serialize()d version of the primary key will be returned.
     *
     * @param array  $row       resultset row.
     * @param int    $offset    The 0-based offset for reading from the resultset row.
     * @param string $indexType One of the class type constants TableMap::TYPE_PHPNAME, TableMap::TYPE_CAMELNAME
     *                           TableMap::TYPE_COLNAME, TableMap::TYPE_FIELDNAME, TableMap::TYPE_NUM
     *
     * @return string The primary key hash of the row
     */
    public static function getPrimaryKeyHashFromRow($row, $offset = 0, $indexType = TableMap::TYPE_NUM)
    {
        // If the PK cannot be derived from the row, return NULL.
        if ($row[TableMap::TYPE_NUM == $indexType ? 0 + $offset : static::translateFieldName('IdCompte', TableMap::TYPE_PHPNAME, $indexType)] === null) {
            return null;
        }

        return null === $row[TableMap::TYPE_NUM == $indexType ? 0 + $offset : static::translateFieldName('IdCompte', TableMap::TYPE_PHPNAME, $indexType)] || is_scalar($row[TableMap::TYPE_NUM == $indexType ? 0 + $offset : static::translateFieldName('IdCompte', TableMap::TYPE_PHPNAME, $indexType)]) || is_callable([$row[TableMap::TYPE_NUM == $indexType ? 0 + $offset : static::translateFieldName('IdCompte', TableMap::TYPE_PHPNAME, $indexType)], '__toString']) ? (string) $row[TableMap::TYPE_NUM == $indexType ? 0 + $offset : static::translateFieldName('IdCompte', TableMap::TYPE_PHPNAME, $indexType)] : $row[TableMap::TYPE_NUM == $indexType ? 0 + $offset : static::translateFieldName('IdCompte', TableMap::TYPE_PHPNAME, $indexType)];
    }

    /**
     * Retrieves the primary key from the DB resultset row
     * For tables with a single-column primary key, that simple pkey value will be returned.  For tables with
     * a multi-column primary key, an array of the primary key columns will be returned.
     *
     * @param array  $row       resultset row.
     * @param int    $offset    The 0-based offset for reading from the resultset row.
     * @param string $indexType One of the class type constants TableMap::TYPE_PHPNAME, TableMap::TYPE_CAMELNAME
     *                           TableMap::TYPE_COLNAME, TableMap::TYPE_FIELDNAME, TableMap::TYPE_NUM
     *
     * @return mixed The primary key of the row
     */
    public static function getPrimaryKeyFromRow($row, $offset = 0, $indexType = TableMap::TYPE_NUM)
    {
        return (string) $row[
            $indexType == TableMap::TYPE_NUM
                ? 0 + $offset
                : self::translateFieldName('IdCompte', TableMap::TYPE_PHPNAME, $indexType)
        ];
    }

    /**
     * The class that the tableMap will make instances of.
     *
     * If $withPrefix is true, the returned path
     * uses a dot-path notation which is translated into a path
     * relative to a location on the PHP include_path.
     * (e.g. path.to.MyClass -> 'path/to/MyClass.php')
     *
     * @param boolean $withPrefix Whether or not to return the path with the class name
     * @return string path.to.ClassName
     */
    public static function getOMClass($withPrefix = true)
    {
        return $withPrefix ? CompteTableMap::CLASS_DEFAULT : CompteTableMap::OM_CLASS;
    }

    /**
     * Populates an object of the default type or an object that inherit from the default.
     *
     * @param array  $row       row returned by DataFetcher->fetch().
     * @param int    $offset    The 0-based offset for reading from the resultset row.
     * @param string $indexType The index type of $row. Mostly DataFetcher->getIndexType().
                                 One of the class type constants TableMap::TYPE_PHPNAME, TableMap::TYPE_CAMELNAME
     *                           TableMap::TYPE_COLNAME, TableMap::TYPE_FIELDNAME, TableMap::TYPE_NUM.
     *
     * @throws PropelException Any exceptions caught during processing will be
     *                         rethrown wrapped into a PropelException.
     * @return array           (Compte object, last column rank)
     */
    public static function populateObject($row, $offset = 0, $indexType = TableMap::TYPE_NUM)
    {
        $key = CompteTableMap::getPrimaryKeyHashFromRow($row, $offset, $indexType);
        if (null !== ($obj = CompteTableMap::getInstanceFromPool($key))) {
            // We no longer rehydrate the object, since this can cause data loss.
            // See http://www.propelorm.org/ticket/509
            // $obj->hydrate($row, $offset, true); // rehydrate
            $col = $offset + CompteTableMap::NUM_HYDRATE_COLUMNS;
        } else {
            $cls = CompteTableMap::OM_CLASS;
            /** @var Compte $obj */
            $obj = new $cls();
            $col = $obj->hydrate($row, $offset, false, $indexType);
            CompteTableMap::addInstanceToPool($obj, $key);
        }

        return array($obj, $col);
    }

    /**
     * The returned array will contain objects of the default type or
     * objects that inherit from the default.
     *
     * @param DataFetcherInterface $dataFetcher
     * @return array
     * @throws PropelException Any exceptions caught during processing will be
     *                         rethrown wrapped into a PropelException.
     */
    public static function populateObjects(DataFetcherInterface $dataFetcher)
    {
        $results = array();

        // set the class once to avoid overhead in the loop
        $cls = static::getOMClass(false);
        // populate the object(s)
        while ($row = $dataFetcher->fetch()) {
            $key = CompteTableMap::getPrimaryKeyHashFromRow($row, 0, $dataFetcher->getIndexType());
            if (null !== ($obj = CompteTableMap::getInstanceFromPool($key))) {
                // We no longer rehydrate the object, since this can cause data loss.
                // See http://www.propelorm.org/ticket/509
                // $obj->hydrate($row, 0, true); // rehydrate
                $results[] = $obj;
            } else {
                /** @var Compte $obj */
                $obj = new $cls();
                $obj->hydrate($row);
                $results[] = $obj;
                CompteTableMap::addInstanceToPool($obj, $key);
            } // if key exists
        }

        return $results;
    }
    /**
     * Add all the columns needed to create a new object.
     *
     * Note: any columns that were marked with lazyLoad="true" in the
     * XML schema will not be added to the select list and only loaded
     * on demand.
     *
     * @param Criteria $criteria object containing the columns to add.
     * @param string   $alias    optional table alias
     * @throws PropelException Any exceptions caught during processing will be
     *                         rethrown wrapped into a PropelException.
     */
    public static function addSelectColumns(Criteria $criteria, $alias = null)
    {
        if (null === $alias) {
            $criteria->addSelectColumn(CompteTableMap::COL_ID_COMPTE);
            $criteria->addSelectColumn(CompteTableMap::COL_NCOMPTE);
            $criteria->addSelectColumn(CompteTableMap::COL_NOM);
            $criteria->addSelectColumn(CompteTableMap::COL_NOMCOURT);
            $criteria->addSelectColumn(CompteTableMap::COL_ID_POSTE);
            $criteria->addSelectColumn(CompteTableMap::COL_TYPE_OP);
            $criteria->addSelectColumn(CompteTableMap::COL_SOLDE_ANTERIEUR);
            $criteria->addSelectColumn(CompteTableMap::COL_DATE_ANTERIEURE);
            $criteria->addSelectColumn(CompteTableMap::COL_ID_ENTITE);
            $criteria->addSelectColumn(CompteTableMap::COL_OBSERVATION);
            $criteria->addSelectColumn(CompteTableMap::COL_CREATED_AT);
            $criteria->addSelectColumn(CompteTableMap::COL_UPDATED_AT);
        } else {
            $criteria->addSelectColumn($alias . '.id_compte');
            $criteria->addSelectColumn($alias . '.ncompte');
            $criteria->addSelectColumn($alias . '.nom');
            $criteria->addSelectColumn($alias . '.nomcourt');
            $criteria->addSelectColumn($alias . '.id_poste');
            $criteria->addSelectColumn($alias . '.type_op');
            $criteria->addSelectColumn($alias . '.solde_anterieur');
            $criteria->addSelectColumn($alias . '.date_anterieure');
            $criteria->addSelectColumn($alias . '.id_entite');
            $criteria->addSelectColumn($alias . '.observation');
            $criteria->addSelectColumn($alias . '.created_at');
            $criteria->addSelectColumn($alias . '.updated_at');
        }
    }

    /**
     * Returns the TableMap related to this object.
     * This method is not needed for general use but a specific application could have a need.
     * @return TableMap
     * @throws PropelException Any exceptions caught during processing will be
     *                         rethrown wrapped into a PropelException.
     */
    public static function getTableMap()
    {
        return Propel::getServiceContainer()->getDatabaseMap(CompteTableMap::DATABASE_NAME)->getTable(CompteTableMap::TABLE_NAME);
    }

    /**
     * Add a TableMap instance to the database for this tableMap class.
     */
    public static function buildTableMap()
    {
        $dbMap = Propel::getServiceContainer()->getDatabaseMap(CompteTableMap::DATABASE_NAME);
        if (!$dbMap->hasTable(CompteTableMap::TABLE_NAME)) {
            $dbMap->addTableObject(new CompteTableMap());
        }
    }

    /**
     * Performs a DELETE on the database, given a Compte or Criteria object OR a primary key value.
     *
     * @param mixed               $values Criteria or Compte object or primary key or array of primary keys
     *              which is used to create the DELETE statement
     * @param  ConnectionInterface $con the connection to use
     * @return int             The number of affected rows (if supported by underlying database driver).  This includes CASCADE-related rows
     *                         if supported by native driver or if emulated using Propel.
     * @throws PropelException Any exceptions caught during processing will be
     *                         rethrown wrapped into a PropelException.
     */
     public static function doDelete($values, ConnectionInterface $con = null)
     {
        if (null === $con) {
            $con = Propel::getServiceContainer()->getWriteConnection(CompteTableMap::DATABASE_NAME);
        }

        if ($values instanceof Criteria) {
            // rename for clarity
            $criteria = $values;
        } elseif ($values instanceof \Compte) { // it's a model object
            // create criteria based on pk values
            $criteria = $values->buildPkeyCriteria();
        } else { // it's a primary key, or an array of pks
            $criteria = new Criteria(CompteTableMap::DATABASE_NAME);
            $criteria->add(CompteTableMap::COL_ID_COMPTE, (array) $values, Criteria::IN);
        }

        $query = CompteQuery::create()->mergeWith($criteria);

        if ($values instanceof Criteria) {
            CompteTableMap::clearInstancePool();
        } elseif (!is_object($values)) { // it's a primary key, or an array of pks
            foreach ((array) $values as $singleval) {
                CompteTableMap::removeInstanceFromPool($singleval);
            }
        }

        return $query->delete($con);
    }

    /**
     * Deletes all rows from the assc_comptes table.
     *
     * @param ConnectionInterface $con the connection to use
     * @return int The number of affected rows (if supported by underlying database driver).
     */
    public static function doDeleteAll(ConnectionInterface $con = null)
    {
        return CompteQuery::create()->doDeleteAll($con);
    }

    /**
     * Performs an INSERT on the database, given a Compte or Criteria object.
     *
     * @param mixed               $criteria Criteria or Compte object containing data that is used to create the INSERT statement.
     * @param ConnectionInterface $con the ConnectionInterface connection to use
     * @return mixed           The new primary key.
     * @throws PropelException Any exceptions caught during processing will be
     *                         rethrown wrapped into a PropelException.
     */
    public static function doInsert($criteria, ConnectionInterface $con = null)
    {
        if (null === $con) {
            $con = Propel::getServiceContainer()->getWriteConnection(CompteTableMap::DATABASE_NAME);
        }

        if ($criteria instanceof Criteria) {
            $criteria = clone $criteria; // rename for clarity
        } else {
            $criteria = $criteria->buildCriteria(); // build Criteria from Compte object
        }

        if ($criteria->containsKey(CompteTableMap::COL_ID_COMPTE) && $criteria->keyContainsValue(CompteTableMap::COL_ID_COMPTE) ) {
            throw new PropelException('Cannot insert a value for auto-increment primary key ('.CompteTableMap::COL_ID_COMPTE.')');
        }


        // Set the correct dbName
        $query = CompteQuery::create()->mergeWith($criteria);

        // use transaction because $criteria could contain info
        // for more than one table (I guess, conceivably)
        return $con->transaction(function () use ($con, $query) {
            return $query->doInsert($con);
        });
    }

} // CompteTableMap
// This is the static code needed to register the TableMap for this table with the main Propel class.
//
CompteTableMap::buildTableMap();
