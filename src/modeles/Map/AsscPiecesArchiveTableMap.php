<?php

namespace Map;

use \AsscPiecesArchive;
use \AsscPiecesArchiveQuery;
use Propel\Runtime\Propel;
use Propel\Runtime\ActiveQuery\Criteria;
use Propel\Runtime\ActiveQuery\InstancePoolTrait;
use Propel\Runtime\Connection\ConnectionInterface;
use Propel\Runtime\DataFetcher\DataFetcherInterface;
use Propel\Runtime\Exception\PropelException;
use Propel\Runtime\Map\RelationMap;
use Propel\Runtime\Map\TableMap;
use Propel\Runtime\Map\TableMapTrait;


/**
 * This class defines the structure of the 'assc_pieces_archive' table.
 *
 *
 *
 * This map class is used by Propel to do runtime db structure discovery.
 * For example, the createSelectSql() method checks the type of a given column used in an
 * ORDER BY clause to know whether it needs to apply SQL to make the ORDER BY case-insensitive
 * (i.e. if it's a text column type).
 *
 */
class AsscPiecesArchiveTableMap extends TableMap
{
    use InstancePoolTrait;
    use TableMapTrait;

    /**
     * The (dot-path) name of this class
     */
    const CLASS_NAME = '.Map.AsscPiecesArchiveTableMap';

    /**
     * The default database name for this class
     */
    const DATABASE_NAME = 'simplasso';

    /**
     * The table name for this class
     */
    const TABLE_NAME = 'assc_pieces_archive';

    /**
     * The related Propel class for this table
     */
    const OM_CLASS = '\\AsscPiecesArchive';

    /**
     * A class that can be returned by this tableMap
     */
    const CLASS_DEFAULT = 'AsscPiecesArchive';

    /**
     * The total number of columns
     */
    const NUM_COLUMNS = 14;

    /**
     * The number of lazy-loaded columns
     */
    const NUM_LAZY_LOAD_COLUMNS = 0;

    /**
     * The number of columns to hydrate (NUM_COLUMNS - NUM_LAZY_LOAD_COLUMNS)
     */
    const NUM_HYDRATE_COLUMNS = 14;

    /**
     * the column name for the id_piece field
     */
    const COL_ID_PIECE = 'assc_pieces_archive.id_piece';

    /**
     * the column name for the id_ecriture field
     */
    const COL_ID_ECRITURE = 'assc_pieces_archive.id_ecriture';

    /**
     * the column name for the classement field
     */
    const COL_CLASSEMENT = 'assc_pieces_archive.classement';

    /**
     * the column name for the nom field
     */
    const COL_NOM = 'assc_pieces_archive.nom';

    /**
     * the column name for the id_journal field
     */
    const COL_ID_JOURNAL = 'assc_pieces_archive.id_journal';

    /**
     * the column name for the id_entite field
     */
    const COL_ID_ENTITE = 'assc_pieces_archive.id_entite';

    /**
     * the column name for the date_piece field
     */
    const COL_DATE_PIECE = 'assc_pieces_archive.date_piece';

    /**
     * the column name for the observation field
     */
    const COL_OBSERVATION = 'assc_pieces_archive.observation';

    /**
     * the column name for the ecran_saisie field
     */
    const COL_ECRAN_SAISIE = 'assc_pieces_archive.ecran_saisie';

    /**
     * the column name for the etat field
     */
    const COL_ETAT = 'assc_pieces_archive.etat';

    /**
     * the column name for the nb_ligne field
     */
    const COL_NB_LIGNE = 'assc_pieces_archive.nb_ligne';

    /**
     * the column name for the created_at field
     */
    const COL_CREATED_AT = 'assc_pieces_archive.created_at';

    /**
     * the column name for the updated_at field
     */
    const COL_UPDATED_AT = 'assc_pieces_archive.updated_at';

    /**
     * the column name for the archived_at field
     */
    const COL_ARCHIVED_AT = 'assc_pieces_archive.archived_at';

    /**
     * The default string format for model objects of the related table
     */
    const DEFAULT_STRING_FORMAT = 'YAML';

    /**
     * holds an array of fieldnames
     *
     * first dimension keys are the type constants
     * e.g. self::$fieldNames[self::TYPE_PHPNAME][0] = 'Id'
     */
    protected static $fieldNames = array (
        self::TYPE_PHPNAME       => array('IdPiece', 'IdEcriture', 'Classement', 'Nom', 'IdJournal', 'IdEntite', 'DatePiece', 'Observation', 'EcranSaisie', 'Etat', 'NbLigne', 'CreatedAt', 'UpdatedAt', 'ArchivedAt', ),
        self::TYPE_CAMELNAME     => array('idPiece', 'idEcriture', 'classement', 'nom', 'idJournal', 'idEntite', 'datePiece', 'observation', 'ecranSaisie', 'etat', 'nbLigne', 'createdAt', 'updatedAt', 'archivedAt', ),
        self::TYPE_COLNAME       => array(AsscPiecesArchiveTableMap::COL_ID_PIECE, AsscPiecesArchiveTableMap::COL_ID_ECRITURE, AsscPiecesArchiveTableMap::COL_CLASSEMENT, AsscPiecesArchiveTableMap::COL_NOM, AsscPiecesArchiveTableMap::COL_ID_JOURNAL, AsscPiecesArchiveTableMap::COL_ID_ENTITE, AsscPiecesArchiveTableMap::COL_DATE_PIECE, AsscPiecesArchiveTableMap::COL_OBSERVATION, AsscPiecesArchiveTableMap::COL_ECRAN_SAISIE, AsscPiecesArchiveTableMap::COL_ETAT, AsscPiecesArchiveTableMap::COL_NB_LIGNE, AsscPiecesArchiveTableMap::COL_CREATED_AT, AsscPiecesArchiveTableMap::COL_UPDATED_AT, AsscPiecesArchiveTableMap::COL_ARCHIVED_AT, ),
        self::TYPE_FIELDNAME     => array('id_piece', 'id_ecriture', 'classement', 'nom', 'id_journal', 'id_entite', 'date_piece', 'observation', 'ecran_saisie', 'etat', 'nb_ligne', 'created_at', 'updated_at', 'archived_at', ),
        self::TYPE_NUM           => array(0, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, )
    );

    /**
     * holds an array of keys for quick access to the fieldnames array
     *
     * first dimension keys are the type constants
     * e.g. self::$fieldKeys[self::TYPE_PHPNAME]['Id'] = 0
     */
    protected static $fieldKeys = array (
        self::TYPE_PHPNAME       => array('IdPiece' => 0, 'IdEcriture' => 1, 'Classement' => 2, 'Nom' => 3, 'IdJournal' => 4, 'IdEntite' => 5, 'DatePiece' => 6, 'Observation' => 7, 'EcranSaisie' => 8, 'Etat' => 9, 'NbLigne' => 10, 'CreatedAt' => 11, 'UpdatedAt' => 12, 'ArchivedAt' => 13, ),
        self::TYPE_CAMELNAME     => array('idPiece' => 0, 'idEcriture' => 1, 'classement' => 2, 'nom' => 3, 'idJournal' => 4, 'idEntite' => 5, 'datePiece' => 6, 'observation' => 7, 'ecranSaisie' => 8, 'etat' => 9, 'nbLigne' => 10, 'createdAt' => 11, 'updatedAt' => 12, 'archivedAt' => 13, ),
        self::TYPE_COLNAME       => array(AsscPiecesArchiveTableMap::COL_ID_PIECE => 0, AsscPiecesArchiveTableMap::COL_ID_ECRITURE => 1, AsscPiecesArchiveTableMap::COL_CLASSEMENT => 2, AsscPiecesArchiveTableMap::COL_NOM => 3, AsscPiecesArchiveTableMap::COL_ID_JOURNAL => 4, AsscPiecesArchiveTableMap::COL_ID_ENTITE => 5, AsscPiecesArchiveTableMap::COL_DATE_PIECE => 6, AsscPiecesArchiveTableMap::COL_OBSERVATION => 7, AsscPiecesArchiveTableMap::COL_ECRAN_SAISIE => 8, AsscPiecesArchiveTableMap::COL_ETAT => 9, AsscPiecesArchiveTableMap::COL_NB_LIGNE => 10, AsscPiecesArchiveTableMap::COL_CREATED_AT => 11, AsscPiecesArchiveTableMap::COL_UPDATED_AT => 12, AsscPiecesArchiveTableMap::COL_ARCHIVED_AT => 13, ),
        self::TYPE_FIELDNAME     => array('id_piece' => 0, 'id_ecriture' => 1, 'classement' => 2, 'nom' => 3, 'id_journal' => 4, 'id_entite' => 5, 'date_piece' => 6, 'observation' => 7, 'ecran_saisie' => 8, 'etat' => 9, 'nb_ligne' => 10, 'created_at' => 11, 'updated_at' => 12, 'archived_at' => 13, ),
        self::TYPE_NUM           => array(0, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, )
    );

    /**
     * Initialize the table attributes and columns
     * Relations are not initialized by this method since they are lazy loaded
     *
     * @return void
     * @throws PropelException
     */
    public function initialize()
    {
        // attributes
        $this->setName('assc_pieces_archive');
        $this->setPhpName('AsscPiecesArchive');
        $this->setIdentifierQuoting(true);
        $this->setClassName('\\AsscPiecesArchive');
        $this->setPackage('');
        $this->setUseIdGenerator(false);
        // columns
        $this->addPrimaryKey('id_piece', 'IdPiece', 'BIGINT', true, 21, null);
        $this->addColumn('id_ecriture', 'IdEcriture', 'BIGINT', false, 21, null);
        $this->addColumn('classement', 'Classement', 'VARCHAR', true, 10, null);
        $this->addColumn('nom', 'Nom', 'VARCHAR', true, 50, null);
        $this->addColumn('id_journal', 'IdJournal', 'INTEGER', true, null, 1);
        $this->addColumn('id_entite', 'IdEntite', 'BIGINT', true, 21, 1);
        $this->addColumn('date_piece', 'DatePiece', 'TIMESTAMP', true, null, null);
        $this->addColumn('observation', 'Observation', 'LONGVARCHAR', false, null, null);
        $this->addColumn('ecran_saisie', 'EcranSaisie', 'VARCHAR', true, 2, 'st');
        $this->addColumn('etat', 'Etat', 'INTEGER', false, 2, 2);
        $this->addColumn('nb_ligne', 'NbLigne', 'INTEGER', true, 21, 2);
        $this->addColumn('created_at', 'CreatedAt', 'TIMESTAMP', false, null, null);
        $this->addColumn('updated_at', 'UpdatedAt', 'TIMESTAMP', false, null, null);
        $this->addColumn('archived_at', 'ArchivedAt', 'TIMESTAMP', false, null, null);
    } // initialize()

    /**
     * Build the RelationMap objects for this table relationships
     */
    public function buildRelations()
    {
    } // buildRelations()

    /**
     * Retrieves a string version of the primary key from the DB resultset row that can be used to uniquely identify a row in this table.
     *
     * For tables with a single-column primary key, that simple pkey value will be returned.  For tables with
     * a multi-column primary key, a serialize()d version of the primary key will be returned.
     *
     * @param array  $row       resultset row.
     * @param int    $offset    The 0-based offset for reading from the resultset row.
     * @param string $indexType One of the class type constants TableMap::TYPE_PHPNAME, TableMap::TYPE_CAMELNAME
     *                           TableMap::TYPE_COLNAME, TableMap::TYPE_FIELDNAME, TableMap::TYPE_NUM
     *
     * @return string The primary key hash of the row
     */
    public static function getPrimaryKeyHashFromRow($row, $offset = 0, $indexType = TableMap::TYPE_NUM)
    {
        // If the PK cannot be derived from the row, return NULL.
        if ($row[TableMap::TYPE_NUM == $indexType ? 0 + $offset : static::translateFieldName('IdPiece', TableMap::TYPE_PHPNAME, $indexType)] === null) {
            return null;
        }

        return null === $row[TableMap::TYPE_NUM == $indexType ? 0 + $offset : static::translateFieldName('IdPiece', TableMap::TYPE_PHPNAME, $indexType)] || is_scalar($row[TableMap::TYPE_NUM == $indexType ? 0 + $offset : static::translateFieldName('IdPiece', TableMap::TYPE_PHPNAME, $indexType)]) || is_callable([$row[TableMap::TYPE_NUM == $indexType ? 0 + $offset : static::translateFieldName('IdPiece', TableMap::TYPE_PHPNAME, $indexType)], '__toString']) ? (string) $row[TableMap::TYPE_NUM == $indexType ? 0 + $offset : static::translateFieldName('IdPiece', TableMap::TYPE_PHPNAME, $indexType)] : $row[TableMap::TYPE_NUM == $indexType ? 0 + $offset : static::translateFieldName('IdPiece', TableMap::TYPE_PHPNAME, $indexType)];
    }

    /**
     * Retrieves the primary key from the DB resultset row
     * For tables with a single-column primary key, that simple pkey value will be returned.  For tables with
     * a multi-column primary key, an array of the primary key columns will be returned.
     *
     * @param array  $row       resultset row.
     * @param int    $offset    The 0-based offset for reading from the resultset row.
     * @param string $indexType One of the class type constants TableMap::TYPE_PHPNAME, TableMap::TYPE_CAMELNAME
     *                           TableMap::TYPE_COLNAME, TableMap::TYPE_FIELDNAME, TableMap::TYPE_NUM
     *
     * @return mixed The primary key of the row
     */
    public static function getPrimaryKeyFromRow($row, $offset = 0, $indexType = TableMap::TYPE_NUM)
    {
        return (string) $row[
            $indexType == TableMap::TYPE_NUM
                ? 0 + $offset
                : self::translateFieldName('IdPiece', TableMap::TYPE_PHPNAME, $indexType)
        ];
    }

    /**
     * The class that the tableMap will make instances of.
     *
     * If $withPrefix is true, the returned path
     * uses a dot-path notation which is translated into a path
     * relative to a location on the PHP include_path.
     * (e.g. path.to.MyClass -> 'path/to/MyClass.php')
     *
     * @param boolean $withPrefix Whether or not to return the path with the class name
     * @return string path.to.ClassName
     */
    public static function getOMClass($withPrefix = true)
    {
        return $withPrefix ? AsscPiecesArchiveTableMap::CLASS_DEFAULT : AsscPiecesArchiveTableMap::OM_CLASS;
    }

    /**
     * Populates an object of the default type or an object that inherit from the default.
     *
     * @param array  $row       row returned by DataFetcher->fetch().
     * @param int    $offset    The 0-based offset for reading from the resultset row.
     * @param string $indexType The index type of $row. Mostly DataFetcher->getIndexType().
                                 One of the class type constants TableMap::TYPE_PHPNAME, TableMap::TYPE_CAMELNAME
     *                           TableMap::TYPE_COLNAME, TableMap::TYPE_FIELDNAME, TableMap::TYPE_NUM.
     *
     * @throws PropelException Any exceptions caught during processing will be
     *                         rethrown wrapped into a PropelException.
     * @return array           (AsscPiecesArchive object, last column rank)
     */
    public static function populateObject($row, $offset = 0, $indexType = TableMap::TYPE_NUM)
    {
        $key = AsscPiecesArchiveTableMap::getPrimaryKeyHashFromRow($row, $offset, $indexType);
        if (null !== ($obj = AsscPiecesArchiveTableMap::getInstanceFromPool($key))) {
            // We no longer rehydrate the object, since this can cause data loss.
            // See http://www.propelorm.org/ticket/509
            // $obj->hydrate($row, $offset, true); // rehydrate
            $col = $offset + AsscPiecesArchiveTableMap::NUM_HYDRATE_COLUMNS;
        } else {
            $cls = AsscPiecesArchiveTableMap::OM_CLASS;
            /** @var AsscPiecesArchive $obj */
            $obj = new $cls();
            $col = $obj->hydrate($row, $offset, false, $indexType);
            AsscPiecesArchiveTableMap::addInstanceToPool($obj, $key);
        }

        return array($obj, $col);
    }

    /**
     * The returned array will contain objects of the default type or
     * objects that inherit from the default.
     *
     * @param DataFetcherInterface $dataFetcher
     * @return array
     * @throws PropelException Any exceptions caught during processing will be
     *                         rethrown wrapped into a PropelException.
     */
    public static function populateObjects(DataFetcherInterface $dataFetcher)
    {
        $results = array();

        // set the class once to avoid overhead in the loop
        $cls = static::getOMClass(false);
        // populate the object(s)
        while ($row = $dataFetcher->fetch()) {
            $key = AsscPiecesArchiveTableMap::getPrimaryKeyHashFromRow($row, 0, $dataFetcher->getIndexType());
            if (null !== ($obj = AsscPiecesArchiveTableMap::getInstanceFromPool($key))) {
                // We no longer rehydrate the object, since this can cause data loss.
                // See http://www.propelorm.org/ticket/509
                // $obj->hydrate($row, 0, true); // rehydrate
                $results[] = $obj;
            } else {
                /** @var AsscPiecesArchive $obj */
                $obj = new $cls();
                $obj->hydrate($row);
                $results[] = $obj;
                AsscPiecesArchiveTableMap::addInstanceToPool($obj, $key);
            } // if key exists
        }

        return $results;
    }
    /**
     * Add all the columns needed to create a new object.
     *
     * Note: any columns that were marked with lazyLoad="true" in the
     * XML schema will not be added to the select list and only loaded
     * on demand.
     *
     * @param Criteria $criteria object containing the columns to add.
     * @param string   $alias    optional table alias
     * @throws PropelException Any exceptions caught during processing will be
     *                         rethrown wrapped into a PropelException.
     */
    public static function addSelectColumns(Criteria $criteria, $alias = null)
    {
        if (null === $alias) {
            $criteria->addSelectColumn(AsscPiecesArchiveTableMap::COL_ID_PIECE);
            $criteria->addSelectColumn(AsscPiecesArchiveTableMap::COL_ID_ECRITURE);
            $criteria->addSelectColumn(AsscPiecesArchiveTableMap::COL_CLASSEMENT);
            $criteria->addSelectColumn(AsscPiecesArchiveTableMap::COL_NOM);
            $criteria->addSelectColumn(AsscPiecesArchiveTableMap::COL_ID_JOURNAL);
            $criteria->addSelectColumn(AsscPiecesArchiveTableMap::COL_ID_ENTITE);
            $criteria->addSelectColumn(AsscPiecesArchiveTableMap::COL_DATE_PIECE);
            $criteria->addSelectColumn(AsscPiecesArchiveTableMap::COL_OBSERVATION);
            $criteria->addSelectColumn(AsscPiecesArchiveTableMap::COL_ECRAN_SAISIE);
            $criteria->addSelectColumn(AsscPiecesArchiveTableMap::COL_ETAT);
            $criteria->addSelectColumn(AsscPiecesArchiveTableMap::COL_NB_LIGNE);
            $criteria->addSelectColumn(AsscPiecesArchiveTableMap::COL_CREATED_AT);
            $criteria->addSelectColumn(AsscPiecesArchiveTableMap::COL_UPDATED_AT);
            $criteria->addSelectColumn(AsscPiecesArchiveTableMap::COL_ARCHIVED_AT);
        } else {
            $criteria->addSelectColumn($alias . '.id_piece');
            $criteria->addSelectColumn($alias . '.id_ecriture');
            $criteria->addSelectColumn($alias . '.classement');
            $criteria->addSelectColumn($alias . '.nom');
            $criteria->addSelectColumn($alias . '.id_journal');
            $criteria->addSelectColumn($alias . '.id_entite');
            $criteria->addSelectColumn($alias . '.date_piece');
            $criteria->addSelectColumn($alias . '.observation');
            $criteria->addSelectColumn($alias . '.ecran_saisie');
            $criteria->addSelectColumn($alias . '.etat');
            $criteria->addSelectColumn($alias . '.nb_ligne');
            $criteria->addSelectColumn($alias . '.created_at');
            $criteria->addSelectColumn($alias . '.updated_at');
            $criteria->addSelectColumn($alias . '.archived_at');
        }
    }

    /**
     * Returns the TableMap related to this object.
     * This method is not needed for general use but a specific application could have a need.
     * @return TableMap
     * @throws PropelException Any exceptions caught during processing will be
     *                         rethrown wrapped into a PropelException.
     */
    public static function getTableMap()
    {
        return Propel::getServiceContainer()->getDatabaseMap(AsscPiecesArchiveTableMap::DATABASE_NAME)->getTable(AsscPiecesArchiveTableMap::TABLE_NAME);
    }

    /**
     * Add a TableMap instance to the database for this tableMap class.
     */
    public static function buildTableMap()
    {
        $dbMap = Propel::getServiceContainer()->getDatabaseMap(AsscPiecesArchiveTableMap::DATABASE_NAME);
        if (!$dbMap->hasTable(AsscPiecesArchiveTableMap::TABLE_NAME)) {
            $dbMap->addTableObject(new AsscPiecesArchiveTableMap());
        }
    }

    /**
     * Performs a DELETE on the database, given a AsscPiecesArchive or Criteria object OR a primary key value.
     *
     * @param mixed               $values Criteria or AsscPiecesArchive object or primary key or array of primary keys
     *              which is used to create the DELETE statement
     * @param  ConnectionInterface $con the connection to use
     * @return int             The number of affected rows (if supported by underlying database driver).  This includes CASCADE-related rows
     *                         if supported by native driver or if emulated using Propel.
     * @throws PropelException Any exceptions caught during processing will be
     *                         rethrown wrapped into a PropelException.
     */
     public static function doDelete($values, ConnectionInterface $con = null)
     {
        if (null === $con) {
            $con = Propel::getServiceContainer()->getWriteConnection(AsscPiecesArchiveTableMap::DATABASE_NAME);
        }

        if ($values instanceof Criteria) {
            // rename for clarity
            $criteria = $values;
        } elseif ($values instanceof \AsscPiecesArchive) { // it's a model object
            // create criteria based on pk values
            $criteria = $values->buildPkeyCriteria();
        } else { // it's a primary key, or an array of pks
            $criteria = new Criteria(AsscPiecesArchiveTableMap::DATABASE_NAME);
            $criteria->add(AsscPiecesArchiveTableMap::COL_ID_PIECE, (array) $values, Criteria::IN);
        }

        $query = AsscPiecesArchiveQuery::create()->mergeWith($criteria);

        if ($values instanceof Criteria) {
            AsscPiecesArchiveTableMap::clearInstancePool();
        } elseif (!is_object($values)) { // it's a primary key, or an array of pks
            foreach ((array) $values as $singleval) {
                AsscPiecesArchiveTableMap::removeInstanceFromPool($singleval);
            }
        }

        return $query->delete($con);
    }

    /**
     * Deletes all rows from the assc_pieces_archive table.
     *
     * @param ConnectionInterface $con the connection to use
     * @return int The number of affected rows (if supported by underlying database driver).
     */
    public static function doDeleteAll(ConnectionInterface $con = null)
    {
        return AsscPiecesArchiveQuery::create()->doDeleteAll($con);
    }

    /**
     * Performs an INSERT on the database, given a AsscPiecesArchive or Criteria object.
     *
     * @param mixed               $criteria Criteria or AsscPiecesArchive object containing data that is used to create the INSERT statement.
     * @param ConnectionInterface $con the ConnectionInterface connection to use
     * @return mixed           The new primary key.
     * @throws PropelException Any exceptions caught during processing will be
     *                         rethrown wrapped into a PropelException.
     */
    public static function doInsert($criteria, ConnectionInterface $con = null)
    {
        if (null === $con) {
            $con = Propel::getServiceContainer()->getWriteConnection(AsscPiecesArchiveTableMap::DATABASE_NAME);
        }

        if ($criteria instanceof Criteria) {
            $criteria = clone $criteria; // rename for clarity
        } else {
            $criteria = $criteria->buildCriteria(); // build Criteria from AsscPiecesArchive object
        }


        // Set the correct dbName
        $query = AsscPiecesArchiveQuery::create()->mergeWith($criteria);

        // use transaction because $criteria could contain info
        // for more than one table (I guess, conceivably)
        return $con->transaction(function () use ($con, $query) {
            return $query->doInsert($con);
        });
    }

} // AsscPiecesArchiveTableMap
// This is the static code needed to register the TableMap for this table with the main Propel class.
//
AsscPiecesArchiveTableMap::buildTableMap();
