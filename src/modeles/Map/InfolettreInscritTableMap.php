<?php

namespace Map;

use \InfolettreInscrit;
use \InfolettreInscritQuery;
use Propel\Runtime\Propel;
use Propel\Runtime\ActiveQuery\Criteria;
use Propel\Runtime\ActiveQuery\InstancePoolTrait;
use Propel\Runtime\Connection\ConnectionInterface;
use Propel\Runtime\DataFetcher\DataFetcherInterface;
use Propel\Runtime\Exception\PropelException;
use Propel\Runtime\Map\RelationMap;
use Propel\Runtime\Map\TableMap;
use Propel\Runtime\Map\TableMapTrait;


/**
 * This class defines the structure of the 'com_infolettres_inscrits' table.
 *
 *
 *
 * This map class is used by Propel to do runtime db structure discovery.
 * For example, the createSelectSql() method checks the type of a given column used in an
 * ORDER BY clause to know whether it needs to apply SQL to make the ORDER BY case-insensitive
 * (i.e. if it's a text column type).
 *
 */
class InfolettreInscritTableMap extends TableMap
{
    use InstancePoolTrait;
    use TableMapTrait;

    /**
     * The (dot-path) name of this class
     */
    const CLASS_NAME = '.Map.InfolettreInscritTableMap';

    /**
     * The default database name for this class
     */
    const DATABASE_NAME = 'simplasso';

    /**
     * The table name for this class
     */
    const TABLE_NAME = 'com_infolettres_inscrits';

    /**
     * The related Propel class for this table
     */
    const OM_CLASS = '\\InfolettreInscrit';

    /**
     * A class that can be returned by this tableMap
     */
    const CLASS_DEFAULT = 'InfolettreInscrit';

    /**
     * The total number of columns
     */
    const NUM_COLUMNS = 5;

    /**
     * The number of lazy-loaded columns
     */
    const NUM_LAZY_LOAD_COLUMNS = 0;

    /**
     * The number of columns to hydrate (NUM_COLUMNS - NUM_LAZY_LOAD_COLUMNS)
     */
    const NUM_HYDRATE_COLUMNS = 5;

    /**
     * the column name for the id_infolettre_inscrit field
     */
    const COL_ID_INFOLETTRE_INSCRIT = 'com_infolettres_inscrits.id_infolettre_inscrit';

    /**
     * the column name for the email field
     */
    const COL_EMAIL = 'com_infolettres_inscrits.email';

    /**
     * the column name for the id_infolettre field
     */
    const COL_ID_INFOLETTRE = 'com_infolettres_inscrits.id_infolettre';

    /**
     * the column name for the date_synchro field
     */
    const COL_DATE_SYNCHRO = 'com_infolettres_inscrits.date_synchro';

    /**
     * the column name for the synchro field
     */
    const COL_SYNCHRO = 'com_infolettres_inscrits.synchro';

    /**
     * The default string format for model objects of the related table
     */
    const DEFAULT_STRING_FORMAT = 'YAML';

    /**
     * holds an array of fieldnames
     *
     * first dimension keys are the type constants
     * e.g. self::$fieldNames[self::TYPE_PHPNAME][0] = 'Id'
     */
    protected static $fieldNames = array (
        self::TYPE_PHPNAME       => array('IdInfolettreInscrit', 'Email', 'IdInfolettre', 'DateSynchro', 'Synchro', ),
        self::TYPE_CAMELNAME     => array('idInfolettreInscrit', 'email', 'idInfolettre', 'dateSynchro', 'synchro', ),
        self::TYPE_COLNAME       => array(InfolettreInscritTableMap::COL_ID_INFOLETTRE_INSCRIT, InfolettreInscritTableMap::COL_EMAIL, InfolettreInscritTableMap::COL_ID_INFOLETTRE, InfolettreInscritTableMap::COL_DATE_SYNCHRO, InfolettreInscritTableMap::COL_SYNCHRO, ),
        self::TYPE_FIELDNAME     => array('id_infolettre_inscrit', 'email', 'id_infolettre', 'date_synchro', 'synchro', ),
        self::TYPE_NUM           => array(0, 1, 2, 3, 4, )
    );

    /**
     * holds an array of keys for quick access to the fieldnames array
     *
     * first dimension keys are the type constants
     * e.g. self::$fieldKeys[self::TYPE_PHPNAME]['Id'] = 0
     */
    protected static $fieldKeys = array (
        self::TYPE_PHPNAME       => array('IdInfolettreInscrit' => 0, 'Email' => 1, 'IdInfolettre' => 2, 'DateSynchro' => 3, 'Synchro' => 4, ),
        self::TYPE_CAMELNAME     => array('idInfolettreInscrit' => 0, 'email' => 1, 'idInfolettre' => 2, 'dateSynchro' => 3, 'synchro' => 4, ),
        self::TYPE_COLNAME       => array(InfolettreInscritTableMap::COL_ID_INFOLETTRE_INSCRIT => 0, InfolettreInscritTableMap::COL_EMAIL => 1, InfolettreInscritTableMap::COL_ID_INFOLETTRE => 2, InfolettreInscritTableMap::COL_DATE_SYNCHRO => 3, InfolettreInscritTableMap::COL_SYNCHRO => 4, ),
        self::TYPE_FIELDNAME     => array('id_infolettre_inscrit' => 0, 'email' => 1, 'id_infolettre' => 2, 'date_synchro' => 3, 'synchro' => 4, ),
        self::TYPE_NUM           => array(0, 1, 2, 3, 4, )
    );

    /**
     * Initialize the table attributes and columns
     * Relations are not initialized by this method since they are lazy loaded
     *
     * @return void
     * @throws PropelException
     */
    public function initialize()
    {
        // attributes
        $this->setName('com_infolettres_inscrits');
        $this->setPhpName('InfolettreInscrit');
        $this->setIdentifierQuoting(true);
        $this->setClassName('\\InfolettreInscrit');
        $this->setPackage('');
        $this->setUseIdGenerator(true);
        // columns
        $this->addPrimaryKey('id_infolettre_inscrit', 'IdInfolettreInscrit', 'BIGINT', true, 21, null);
        $this->addColumn('email', 'Email', 'VARCHAR', true, 250, null);
        $this->addForeignKey('id_infolettre', 'IdInfolettre', 'BIGINT', 'com_infolettres', 'id_infolettre', true, 21, null);
        $this->addColumn('date_synchro', 'DateSynchro', 'TIMESTAMP', false, null, null);
        $this->addColumn('synchro', 'Synchro', 'INTEGER', false, null, 1);
    } // initialize()

    /**
     * Build the RelationMap objects for this table relationships
     */
    public function buildRelations()
    {
        $this->addRelation('Infolettre', '\\Infolettre', RelationMap::MANY_TO_ONE, array (
  0 =>
  array (
    0 => ':id_infolettre',
    1 => ':id_infolettre',
  ),
), 'CASCADE', null, null, false);
    } // buildRelations()

    /**
     * Retrieves a string version of the primary key from the DB resultset row that can be used to uniquely identify a row in this table.
     *
     * For tables with a single-column primary key, that simple pkey value will be returned.  For tables with
     * a multi-column primary key, a serialize()d version of the primary key will be returned.
     *
     * @param array  $row       resultset row.
     * @param int    $offset    The 0-based offset for reading from the resultset row.
     * @param string $indexType One of the class type constants TableMap::TYPE_PHPNAME, TableMap::TYPE_CAMELNAME
     *                           TableMap::TYPE_COLNAME, TableMap::TYPE_FIELDNAME, TableMap::TYPE_NUM
     *
     * @return string The primary key hash of the row
     */
    public static function getPrimaryKeyHashFromRow($row, $offset = 0, $indexType = TableMap::TYPE_NUM)
    {
        // If the PK cannot be derived from the row, return NULL.
        if ($row[TableMap::TYPE_NUM == $indexType ? 0 + $offset : static::translateFieldName('IdInfolettreInscrit', TableMap::TYPE_PHPNAME, $indexType)] === null) {
            return null;
        }

        return null === $row[TableMap::TYPE_NUM == $indexType ? 0 + $offset : static::translateFieldName('IdInfolettreInscrit', TableMap::TYPE_PHPNAME, $indexType)] || is_scalar($row[TableMap::TYPE_NUM == $indexType ? 0 + $offset : static::translateFieldName('IdInfolettreInscrit', TableMap::TYPE_PHPNAME, $indexType)]) || is_callable([$row[TableMap::TYPE_NUM == $indexType ? 0 + $offset : static::translateFieldName('IdInfolettreInscrit', TableMap::TYPE_PHPNAME, $indexType)], '__toString']) ? (string) $row[TableMap::TYPE_NUM == $indexType ? 0 + $offset : static::translateFieldName('IdInfolettreInscrit', TableMap::TYPE_PHPNAME, $indexType)] : $row[TableMap::TYPE_NUM == $indexType ? 0 + $offset : static::translateFieldName('IdInfolettreInscrit', TableMap::TYPE_PHPNAME, $indexType)];
    }

    /**
     * Retrieves the primary key from the DB resultset row
     * For tables with a single-column primary key, that simple pkey value will be returned.  For tables with
     * a multi-column primary key, an array of the primary key columns will be returned.
     *
     * @param array  $row       resultset row.
     * @param int    $offset    The 0-based offset for reading from the resultset row.
     * @param string $indexType One of the class type constants TableMap::TYPE_PHPNAME, TableMap::TYPE_CAMELNAME
     *                           TableMap::TYPE_COLNAME, TableMap::TYPE_FIELDNAME, TableMap::TYPE_NUM
     *
     * @return mixed The primary key of the row
     */
    public static function getPrimaryKeyFromRow($row, $offset = 0, $indexType = TableMap::TYPE_NUM)
    {
        return (string) $row[
            $indexType == TableMap::TYPE_NUM
                ? 0 + $offset
                : self::translateFieldName('IdInfolettreInscrit', TableMap::TYPE_PHPNAME, $indexType)
        ];
    }

    /**
     * The class that the tableMap will make instances of.
     *
     * If $withPrefix is true, the returned path
     * uses a dot-path notation which is translated into a path
     * relative to a location on the PHP include_path.
     * (e.g. path.to.MyClass -> 'path/to/MyClass.php')
     *
     * @param boolean $withPrefix Whether or not to return the path with the class name
     * @return string path.to.ClassName
     */
    public static function getOMClass($withPrefix = true)
    {
        return $withPrefix ? InfolettreInscritTableMap::CLASS_DEFAULT : InfolettreInscritTableMap::OM_CLASS;
    }

    /**
     * Populates an object of the default type or an object that inherit from the default.
     *
     * @param array  $row       row returned by DataFetcher->fetch().
     * @param int    $offset    The 0-based offset for reading from the resultset row.
     * @param string $indexType The index type of $row. Mostly DataFetcher->getIndexType().
                                 One of the class type constants TableMap::TYPE_PHPNAME, TableMap::TYPE_CAMELNAME
     *                           TableMap::TYPE_COLNAME, TableMap::TYPE_FIELDNAME, TableMap::TYPE_NUM.
     *
     * @throws PropelException Any exceptions caught during processing will be
     *                         rethrown wrapped into a PropelException.
     * @return array           (InfolettreInscrit object, last column rank)
     */
    public static function populateObject($row, $offset = 0, $indexType = TableMap::TYPE_NUM)
    {
        $key = InfolettreInscritTableMap::getPrimaryKeyHashFromRow($row, $offset, $indexType);
        if (null !== ($obj = InfolettreInscritTableMap::getInstanceFromPool($key))) {
            // We no longer rehydrate the object, since this can cause data loss.
            // See http://www.propelorm.org/ticket/509
            // $obj->hydrate($row, $offset, true); // rehydrate
            $col = $offset + InfolettreInscritTableMap::NUM_HYDRATE_COLUMNS;
        } else {
            $cls = InfolettreInscritTableMap::OM_CLASS;
            /** @var InfolettreInscrit $obj */
            $obj = new $cls();
            $col = $obj->hydrate($row, $offset, false, $indexType);
            InfolettreInscritTableMap::addInstanceToPool($obj, $key);
        }

        return array($obj, $col);
    }

    /**
     * The returned array will contain objects of the default type or
     * objects that inherit from the default.
     *
     * @param DataFetcherInterface $dataFetcher
     * @return array
     * @throws PropelException Any exceptions caught during processing will be
     *                         rethrown wrapped into a PropelException.
     */
    public static function populateObjects(DataFetcherInterface $dataFetcher)
    {
        $results = array();

        // set the class once to avoid overhead in the loop
        $cls = static::getOMClass(false);
        // populate the object(s)
        while ($row = $dataFetcher->fetch()) {
            $key = InfolettreInscritTableMap::getPrimaryKeyHashFromRow($row, 0, $dataFetcher->getIndexType());
            if (null !== ($obj = InfolettreInscritTableMap::getInstanceFromPool($key))) {
                // We no longer rehydrate the object, since this can cause data loss.
                // See http://www.propelorm.org/ticket/509
                // $obj->hydrate($row, 0, true); // rehydrate
                $results[] = $obj;
            } else {
                /** @var InfolettreInscrit $obj */
                $obj = new $cls();
                $obj->hydrate($row);
                $results[] = $obj;
                InfolettreInscritTableMap::addInstanceToPool($obj, $key);
            } // if key exists
        }

        return $results;
    }
    /**
     * Add all the columns needed to create a new object.
     *
     * Note: any columns that were marked with lazyLoad="true" in the
     * XML schema will not be added to the select list and only loaded
     * on demand.
     *
     * @param Criteria $criteria object containing the columns to add.
     * @param string   $alias    optional table alias
     * @throws PropelException Any exceptions caught during processing will be
     *                         rethrown wrapped into a PropelException.
     */
    public static function addSelectColumns(Criteria $criteria, $alias = null)
    {
        if (null === $alias) {
            $criteria->addSelectColumn(InfolettreInscritTableMap::COL_ID_INFOLETTRE_INSCRIT);
            $criteria->addSelectColumn(InfolettreInscritTableMap::COL_EMAIL);
            $criteria->addSelectColumn(InfolettreInscritTableMap::COL_ID_INFOLETTRE);
            $criteria->addSelectColumn(InfolettreInscritTableMap::COL_DATE_SYNCHRO);
            $criteria->addSelectColumn(InfolettreInscritTableMap::COL_SYNCHRO);
        } else {
            $criteria->addSelectColumn($alias . '.id_infolettre_inscrit');
            $criteria->addSelectColumn($alias . '.email');
            $criteria->addSelectColumn($alias . '.id_infolettre');
            $criteria->addSelectColumn($alias . '.date_synchro');
            $criteria->addSelectColumn($alias . '.synchro');
        }
    }

    /**
     * Returns the TableMap related to this object.
     * This method is not needed for general use but a specific application could have a need.
     * @return TableMap
     * @throws PropelException Any exceptions caught during processing will be
     *                         rethrown wrapped into a PropelException.
     */
    public static function getTableMap()
    {
        return Propel::getServiceContainer()->getDatabaseMap(InfolettreInscritTableMap::DATABASE_NAME)->getTable(InfolettreInscritTableMap::TABLE_NAME);
    }

    /**
     * Add a TableMap instance to the database for this tableMap class.
     */
    public static function buildTableMap()
    {
        $dbMap = Propel::getServiceContainer()->getDatabaseMap(InfolettreInscritTableMap::DATABASE_NAME);
        if (!$dbMap->hasTable(InfolettreInscritTableMap::TABLE_NAME)) {
            $dbMap->addTableObject(new InfolettreInscritTableMap());
        }
    }

    /**
     * Performs a DELETE on the database, given a InfolettreInscrit or Criteria object OR a primary key value.
     *
     * @param mixed               $values Criteria or InfolettreInscrit object or primary key or array of primary keys
     *              which is used to create the DELETE statement
     * @param  ConnectionInterface $con the connection to use
     * @return int             The number of affected rows (if supported by underlying database driver).  This includes CASCADE-related rows
     *                         if supported by native driver or if emulated using Propel.
     * @throws PropelException Any exceptions caught during processing will be
     *                         rethrown wrapped into a PropelException.
     */
     public static function doDelete($values, ConnectionInterface $con = null)
     {
        if (null === $con) {
            $con = Propel::getServiceContainer()->getWriteConnection(InfolettreInscritTableMap::DATABASE_NAME);
        }

        if ($values instanceof Criteria) {
            // rename for clarity
            $criteria = $values;
        } elseif ($values instanceof \InfolettreInscrit) { // it's a model object
            // create criteria based on pk values
            $criteria = $values->buildPkeyCriteria();
        } else { // it's a primary key, or an array of pks
            $criteria = new Criteria(InfolettreInscritTableMap::DATABASE_NAME);
            $criteria->add(InfolettreInscritTableMap::COL_ID_INFOLETTRE_INSCRIT, (array) $values, Criteria::IN);
        }

        $query = InfolettreInscritQuery::create()->mergeWith($criteria);

        if ($values instanceof Criteria) {
            InfolettreInscritTableMap::clearInstancePool();
        } elseif (!is_object($values)) { // it's a primary key, or an array of pks
            foreach ((array) $values as $singleval) {
                InfolettreInscritTableMap::removeInstanceFromPool($singleval);
            }
        }

        return $query->delete($con);
    }

    /**
     * Deletes all rows from the com_infolettres_inscrits table.
     *
     * @param ConnectionInterface $con the connection to use
     * @return int The number of affected rows (if supported by underlying database driver).
     */
    public static function doDeleteAll(ConnectionInterface $con = null)
    {
        return InfolettreInscritQuery::create()->doDeleteAll($con);
    }

    /**
     * Performs an INSERT on the database, given a InfolettreInscrit or Criteria object.
     *
     * @param mixed               $criteria Criteria or InfolettreInscrit object containing data that is used to create the INSERT statement.
     * @param ConnectionInterface $con the ConnectionInterface connection to use
     * @return mixed           The new primary key.
     * @throws PropelException Any exceptions caught during processing will be
     *                         rethrown wrapped into a PropelException.
     */
    public static function doInsert($criteria, ConnectionInterface $con = null)
    {
        if (null === $con) {
            $con = Propel::getServiceContainer()->getWriteConnection(InfolettreInscritTableMap::DATABASE_NAME);
        }

        if ($criteria instanceof Criteria) {
            $criteria = clone $criteria; // rename for clarity
        } else {
            $criteria = $criteria->buildCriteria(); // build Criteria from InfolettreInscrit object
        }

        if ($criteria->containsKey(InfolettreInscritTableMap::COL_ID_INFOLETTRE_INSCRIT) && $criteria->keyContainsValue(InfolettreInscritTableMap::COL_ID_INFOLETTRE_INSCRIT) ) {
            throw new PropelException('Cannot insert a value for auto-increment primary key ('.InfolettreInscritTableMap::COL_ID_INFOLETTRE_INSCRIT.')');
        }


        // Set the correct dbName
        $query = InfolettreInscritQuery::create()->mergeWith($criteria);

        // use transaction because $criteria could contain info
        // for more than one table (I guess, conceivably)
        return $con->transaction(function () use ($con, $query) {
            return $query->doInsert($con);
        });
    }

} // InfolettreInscritTableMap
// This is the static code needed to register the TableMap for this table with the main Propel class.
//
InfolettreInscritTableMap::buildTableMap();
