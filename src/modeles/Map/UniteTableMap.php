<?php

namespace Map;

use \Unite;
use \UniteQuery;
use Propel\Runtime\Propel;
use Propel\Runtime\ActiveQuery\Criteria;
use Propel\Runtime\ActiveQuery\InstancePoolTrait;
use Propel\Runtime\Connection\ConnectionInterface;
use Propel\Runtime\DataFetcher\DataFetcherInterface;
use Propel\Runtime\Exception\PropelException;
use Propel\Runtime\Map\RelationMap;
use Propel\Runtime\Map\TableMap;
use Propel\Runtime\Map\TableMapTrait;


/**
 * This class defines the structure of the 'asso_unites' table.
 *
 *
 *
 * This map class is used by Propel to do runtime db structure discovery.
 * For example, the createSelectSql() method checks the type of a given column used in an
 * ORDER BY clause to know whether it needs to apply SQL to make the ORDER BY case-insensitive
 * (i.e. if it's a text column type).
 *
 */
class UniteTableMap extends TableMap
{
    use InstancePoolTrait;
    use TableMapTrait;

    /**
     * The (dot-path) name of this class
     */
    const CLASS_NAME = '.Map.UniteTableMap';

    /**
     * The default database name for this class
     */
    const DATABASE_NAME = 'simplasso';

    /**
     * The table name for this class
     */
    const TABLE_NAME = 'asso_unites';

    /**
     * The related Propel class for this table
     */
    const OM_CLASS = '\\Unite';

    /**
     * A class that can be returned by this tableMap
     */
    const CLASS_DEFAULT = 'Unite';

    /**
     * The total number of columns
     */
    const NUM_COLUMNS = 8;

    /**
     * The number of lazy-loaded columns
     */
    const NUM_LAZY_LOAD_COLUMNS = 0;

    /**
     * The number of columns to hydrate (NUM_COLUMNS - NUM_LAZY_LOAD_COLUMNS)
     */
    const NUM_HYDRATE_COLUMNS = 8;

    /**
     * the column name for the id_unite field
     */
    const COL_ID_UNITE = 'asso_unites.id_unite';

    /**
     * the column name for the nom field
     */
    const COL_NOM = 'asso_unites.nom';

    /**
     * the column name for the nomcourt field
     */
    const COL_NOMCOURT = 'asso_unites.nomcourt';

    /**
     * the column name for the actif field
     */
    const COL_ACTIF = 'asso_unites.actif';

    /**
     * the column name for the nombre field
     */
    const COL_NOMBRE = 'asso_unites.nombre';

    /**
     * the column name for the observation field
     */
    const COL_OBSERVATION = 'asso_unites.observation';

    /**
     * the column name for the created_at field
     */
    const COL_CREATED_AT = 'asso_unites.created_at';

    /**
     * the column name for the updated_at field
     */
    const COL_UPDATED_AT = 'asso_unites.updated_at';

    /**
     * The default string format for model objects of the related table
     */
    const DEFAULT_STRING_FORMAT = 'YAML';

    /**
     * holds an array of fieldnames
     *
     * first dimension keys are the type constants
     * e.g. self::$fieldNames[self::TYPE_PHPNAME][0] = 'Id'
     */
    protected static $fieldNames = array (
        self::TYPE_PHPNAME       => array('IdUnite', 'Nom', 'Nomcourt', 'Actif', 'Nombre', 'Observation', 'CreatedAt', 'UpdatedAt', ),
        self::TYPE_CAMELNAME     => array('idUnite', 'nom', 'nomcourt', 'actif', 'nombre', 'observation', 'createdAt', 'updatedAt', ),
        self::TYPE_COLNAME       => array(UniteTableMap::COL_ID_UNITE, UniteTableMap::COL_NOM, UniteTableMap::COL_NOMCOURT, UniteTableMap::COL_ACTIF, UniteTableMap::COL_NOMBRE, UniteTableMap::COL_OBSERVATION, UniteTableMap::COL_CREATED_AT, UniteTableMap::COL_UPDATED_AT, ),
        self::TYPE_FIELDNAME     => array('id_unite', 'nom', 'nomcourt', 'actif', 'nombre', 'observation', 'created_at', 'updated_at', ),
        self::TYPE_NUM           => array(0, 1, 2, 3, 4, 5, 6, 7, )
    );

    /**
     * holds an array of keys for quick access to the fieldnames array
     *
     * first dimension keys are the type constants
     * e.g. self::$fieldKeys[self::TYPE_PHPNAME]['Id'] = 0
     */
    protected static $fieldKeys = array (
        self::TYPE_PHPNAME       => array('IdUnite' => 0, 'Nom' => 1, 'Nomcourt' => 2, 'Actif' => 3, 'Nombre' => 4, 'Observation' => 5, 'CreatedAt' => 6, 'UpdatedAt' => 7, ),
        self::TYPE_CAMELNAME     => array('idUnite' => 0, 'nom' => 1, 'nomcourt' => 2, 'actif' => 3, 'nombre' => 4, 'observation' => 5, 'createdAt' => 6, 'updatedAt' => 7, ),
        self::TYPE_COLNAME       => array(UniteTableMap::COL_ID_UNITE => 0, UniteTableMap::COL_NOM => 1, UniteTableMap::COL_NOMCOURT => 2, UniteTableMap::COL_ACTIF => 3, UniteTableMap::COL_NOMBRE => 4, UniteTableMap::COL_OBSERVATION => 5, UniteTableMap::COL_CREATED_AT => 6, UniteTableMap::COL_UPDATED_AT => 7, ),
        self::TYPE_FIELDNAME     => array('id_unite' => 0, 'nom' => 1, 'nomcourt' => 2, 'actif' => 3, 'nombre' => 4, 'observation' => 5, 'created_at' => 6, 'updated_at' => 7, ),
        self::TYPE_NUM           => array(0, 1, 2, 3, 4, 5, 6, 7, )
    );

    /**
     * Initialize the table attributes and columns
     * Relations are not initialized by this method since they are lazy loaded
     *
     * @return void
     * @throws PropelException
     */
    public function initialize()
    {
        // attributes
        $this->setName('asso_unites');
        $this->setPhpName('Unite');
        $this->setIdentifierQuoting(true);
        $this->setClassName('\\Unite');
        $this->setPackage('');
        $this->setUseIdGenerator(true);
        // columns
        $this->addPrimaryKey('id_unite', 'IdUnite', 'BIGINT', true, 21, null);
        $this->addColumn('nom', 'Nom', 'VARCHAR', true, 25, 'Unité');
        $this->addColumn('nomcourt', 'Nomcourt', 'VARCHAR', true, 6, 'U');
        $this->addColumn('actif', 'Actif', 'INTEGER', true, 1, 1);
        $this->addColumn('nombre', 'Nombre', 'INTEGER', true, 1, 0);
        $this->addColumn('observation', 'Observation', 'LONGVARCHAR', false, null, null);
        $this->addColumn('created_at', 'CreatedAt', 'TIMESTAMP', false, null, null);
        $this->addColumn('updated_at', 'UpdatedAt', 'TIMESTAMP', false, null, null);
    } // initialize()

    /**
     * Build the RelationMap objects for this table relationships
     */
    public function buildRelations()
    {
    } // buildRelations()

    /**
     *
     * Gets the list of behaviors registered for this table
     *
     * @return array Associative array (name => parameters) of behaviors
     */
    public function getBehaviors()
    {
        return array(
            'timestampable' => array('create_column' => 'created_at', 'update_column' => 'updated_at', 'disable_created_at' => 'false', 'disable_updated_at' => 'false', ),
            'archivable' => array('archive_table' => '', 'archive_phpname' => '', 'archive_class' => '', 'log_archived_at' => 'true', 'archived_at_column' => 'archived_at', 'archive_on_insert' => 'false', 'archive_on_update' => 'false', 'archive_on_delete' => 'true', ),
        );
    } // getBehaviors()

    /**
     * Retrieves a string version of the primary key from the DB resultset row that can be used to uniquely identify a row in this table.
     *
     * For tables with a single-column primary key, that simple pkey value will be returned.  For tables with
     * a multi-column primary key, a serialize()d version of the primary key will be returned.
     *
     * @param array  $row       resultset row.
     * @param int    $offset    The 0-based offset for reading from the resultset row.
     * @param string $indexType One of the class type constants TableMap::TYPE_PHPNAME, TableMap::TYPE_CAMELNAME
     *                           TableMap::TYPE_COLNAME, TableMap::TYPE_FIELDNAME, TableMap::TYPE_NUM
     *
     * @return string The primary key hash of the row
     */
    public static function getPrimaryKeyHashFromRow($row, $offset = 0, $indexType = TableMap::TYPE_NUM)
    {
        // If the PK cannot be derived from the row, return NULL.
        if ($row[TableMap::TYPE_NUM == $indexType ? 0 + $offset : static::translateFieldName('IdUnite', TableMap::TYPE_PHPNAME, $indexType)] === null) {
            return null;
        }

        return null === $row[TableMap::TYPE_NUM == $indexType ? 0 + $offset : static::translateFieldName('IdUnite', TableMap::TYPE_PHPNAME, $indexType)] || is_scalar($row[TableMap::TYPE_NUM == $indexType ? 0 + $offset : static::translateFieldName('IdUnite', TableMap::TYPE_PHPNAME, $indexType)]) || is_callable([$row[TableMap::TYPE_NUM == $indexType ? 0 + $offset : static::translateFieldName('IdUnite', TableMap::TYPE_PHPNAME, $indexType)], '__toString']) ? (string) $row[TableMap::TYPE_NUM == $indexType ? 0 + $offset : static::translateFieldName('IdUnite', TableMap::TYPE_PHPNAME, $indexType)] : $row[TableMap::TYPE_NUM == $indexType ? 0 + $offset : static::translateFieldName('IdUnite', TableMap::TYPE_PHPNAME, $indexType)];
    }

    /**
     * Retrieves the primary key from the DB resultset row
     * For tables with a single-column primary key, that simple pkey value will be returned.  For tables with
     * a multi-column primary key, an array of the primary key columns will be returned.
     *
     * @param array  $row       resultset row.
     * @param int    $offset    The 0-based offset for reading from the resultset row.
     * @param string $indexType One of the class type constants TableMap::TYPE_PHPNAME, TableMap::TYPE_CAMELNAME
     *                           TableMap::TYPE_COLNAME, TableMap::TYPE_FIELDNAME, TableMap::TYPE_NUM
     *
     * @return mixed The primary key of the row
     */
    public static function getPrimaryKeyFromRow($row, $offset = 0, $indexType = TableMap::TYPE_NUM)
    {
        return (string) $row[
            $indexType == TableMap::TYPE_NUM
                ? 0 + $offset
                : self::translateFieldName('IdUnite', TableMap::TYPE_PHPNAME, $indexType)
        ];
    }

    /**
     * The class that the tableMap will make instances of.
     *
     * If $withPrefix is true, the returned path
     * uses a dot-path notation which is translated into a path
     * relative to a location on the PHP include_path.
     * (e.g. path.to.MyClass -> 'path/to/MyClass.php')
     *
     * @param boolean $withPrefix Whether or not to return the path with the class name
     * @return string path.to.ClassName
     */
    public static function getOMClass($withPrefix = true)
    {
        return $withPrefix ? UniteTableMap::CLASS_DEFAULT : UniteTableMap::OM_CLASS;
    }

    /**
     * Populates an object of the default type or an object that inherit from the default.
     *
     * @param array  $row       row returned by DataFetcher->fetch().
     * @param int    $offset    The 0-based offset for reading from the resultset row.
     * @param string $indexType The index type of $row. Mostly DataFetcher->getIndexType().
                                 One of the class type constants TableMap::TYPE_PHPNAME, TableMap::TYPE_CAMELNAME
     *                           TableMap::TYPE_COLNAME, TableMap::TYPE_FIELDNAME, TableMap::TYPE_NUM.
     *
     * @throws PropelException Any exceptions caught during processing will be
     *                         rethrown wrapped into a PropelException.
     * @return array           (Unite object, last column rank)
     */
    public static function populateObject($row, $offset = 0, $indexType = TableMap::TYPE_NUM)
    {
        $key = UniteTableMap::getPrimaryKeyHashFromRow($row, $offset, $indexType);
        if (null !== ($obj = UniteTableMap::getInstanceFromPool($key))) {
            // We no longer rehydrate the object, since this can cause data loss.
            // See http://www.propelorm.org/ticket/509
            // $obj->hydrate($row, $offset, true); // rehydrate
            $col = $offset + UniteTableMap::NUM_HYDRATE_COLUMNS;
        } else {
            $cls = UniteTableMap::OM_CLASS;
            /** @var Unite $obj */
            $obj = new $cls();
            $col = $obj->hydrate($row, $offset, false, $indexType);
            UniteTableMap::addInstanceToPool($obj, $key);
        }

        return array($obj, $col);
    }

    /**
     * The returned array will contain objects of the default type or
     * objects that inherit from the default.
     *
     * @param DataFetcherInterface $dataFetcher
     * @return array
     * @throws PropelException Any exceptions caught during processing will be
     *                         rethrown wrapped into a PropelException.
     */
    public static function populateObjects(DataFetcherInterface $dataFetcher)
    {
        $results = array();

        // set the class once to avoid overhead in the loop
        $cls = static::getOMClass(false);
        // populate the object(s)
        while ($row = $dataFetcher->fetch()) {
            $key = UniteTableMap::getPrimaryKeyHashFromRow($row, 0, $dataFetcher->getIndexType());
            if (null !== ($obj = UniteTableMap::getInstanceFromPool($key))) {
                // We no longer rehydrate the object, since this can cause data loss.
                // See http://www.propelorm.org/ticket/509
                // $obj->hydrate($row, 0, true); // rehydrate
                $results[] = $obj;
            } else {
                /** @var Unite $obj */
                $obj = new $cls();
                $obj->hydrate($row);
                $results[] = $obj;
                UniteTableMap::addInstanceToPool($obj, $key);
            } // if key exists
        }

        return $results;
    }
    /**
     * Add all the columns needed to create a new object.
     *
     * Note: any columns that were marked with lazyLoad="true" in the
     * XML schema will not be added to the select list and only loaded
     * on demand.
     *
     * @param Criteria $criteria object containing the columns to add.
     * @param string   $alias    optional table alias
     * @throws PropelException Any exceptions caught during processing will be
     *                         rethrown wrapped into a PropelException.
     */
    public static function addSelectColumns(Criteria $criteria, $alias = null)
    {
        if (null === $alias) {
            $criteria->addSelectColumn(UniteTableMap::COL_ID_UNITE);
            $criteria->addSelectColumn(UniteTableMap::COL_NOM);
            $criteria->addSelectColumn(UniteTableMap::COL_NOMCOURT);
            $criteria->addSelectColumn(UniteTableMap::COL_ACTIF);
            $criteria->addSelectColumn(UniteTableMap::COL_NOMBRE);
            $criteria->addSelectColumn(UniteTableMap::COL_OBSERVATION);
            $criteria->addSelectColumn(UniteTableMap::COL_CREATED_AT);
            $criteria->addSelectColumn(UniteTableMap::COL_UPDATED_AT);
        } else {
            $criteria->addSelectColumn($alias . '.id_unite');
            $criteria->addSelectColumn($alias . '.nom');
            $criteria->addSelectColumn($alias . '.nomcourt');
            $criteria->addSelectColumn($alias . '.actif');
            $criteria->addSelectColumn($alias . '.nombre');
            $criteria->addSelectColumn($alias . '.observation');
            $criteria->addSelectColumn($alias . '.created_at');
            $criteria->addSelectColumn($alias . '.updated_at');
        }
    }

    /**
     * Returns the TableMap related to this object.
     * This method is not needed for general use but a specific application could have a need.
     * @return TableMap
     * @throws PropelException Any exceptions caught during processing will be
     *                         rethrown wrapped into a PropelException.
     */
    public static function getTableMap()
    {
        return Propel::getServiceContainer()->getDatabaseMap(UniteTableMap::DATABASE_NAME)->getTable(UniteTableMap::TABLE_NAME);
    }

    /**
     * Add a TableMap instance to the database for this tableMap class.
     */
    public static function buildTableMap()
    {
        $dbMap = Propel::getServiceContainer()->getDatabaseMap(UniteTableMap::DATABASE_NAME);
        if (!$dbMap->hasTable(UniteTableMap::TABLE_NAME)) {
            $dbMap->addTableObject(new UniteTableMap());
        }
    }

    /**
     * Performs a DELETE on the database, given a Unite or Criteria object OR a primary key value.
     *
     * @param mixed               $values Criteria or Unite object or primary key or array of primary keys
     *              which is used to create the DELETE statement
     * @param  ConnectionInterface $con the connection to use
     * @return int             The number of affected rows (if supported by underlying database driver).  This includes CASCADE-related rows
     *                         if supported by native driver or if emulated using Propel.
     * @throws PropelException Any exceptions caught during processing will be
     *                         rethrown wrapped into a PropelException.
     */
     public static function doDelete($values, ConnectionInterface $con = null)
     {
        if (null === $con) {
            $con = Propel::getServiceContainer()->getWriteConnection(UniteTableMap::DATABASE_NAME);
        }

        if ($values instanceof Criteria) {
            // rename for clarity
            $criteria = $values;
        } elseif ($values instanceof \Unite) { // it's a model object
            // create criteria based on pk values
            $criteria = $values->buildPkeyCriteria();
        } else { // it's a primary key, or an array of pks
            $criteria = new Criteria(UniteTableMap::DATABASE_NAME);
            $criteria->add(UniteTableMap::COL_ID_UNITE, (array) $values, Criteria::IN);
        }

        $query = UniteQuery::create()->mergeWith($criteria);

        if ($values instanceof Criteria) {
            UniteTableMap::clearInstancePool();
        } elseif (!is_object($values)) { // it's a primary key, or an array of pks
            foreach ((array) $values as $singleval) {
                UniteTableMap::removeInstanceFromPool($singleval);
            }
        }

        return $query->delete($con);
    }

    /**
     * Deletes all rows from the asso_unites table.
     *
     * @param ConnectionInterface $con the connection to use
     * @return int The number of affected rows (if supported by underlying database driver).
     */
    public static function doDeleteAll(ConnectionInterface $con = null)
    {
        return UniteQuery::create()->doDeleteAll($con);
    }

    /**
     * Performs an INSERT on the database, given a Unite or Criteria object.
     *
     * @param mixed               $criteria Criteria or Unite object containing data that is used to create the INSERT statement.
     * @param ConnectionInterface $con the ConnectionInterface connection to use
     * @return mixed           The new primary key.
     * @throws PropelException Any exceptions caught during processing will be
     *                         rethrown wrapped into a PropelException.
     */
    public static function doInsert($criteria, ConnectionInterface $con = null)
    {
        if (null === $con) {
            $con = Propel::getServiceContainer()->getWriteConnection(UniteTableMap::DATABASE_NAME);
        }

        if ($criteria instanceof Criteria) {
            $criteria = clone $criteria; // rename for clarity
        } else {
            $criteria = $criteria->buildCriteria(); // build Criteria from Unite object
        }

        if ($criteria->containsKey(UniteTableMap::COL_ID_UNITE) && $criteria->keyContainsValue(UniteTableMap::COL_ID_UNITE) ) {
            throw new PropelException('Cannot insert a value for auto-increment primary key ('.UniteTableMap::COL_ID_UNITE.')');
        }


        // Set the correct dbName
        $query = UniteQuery::create()->mergeWith($criteria);

        // use transaction because $criteria could contain info
        // for more than one table (I guess, conceivably)
        return $con->transaction(function () use ($con, $query) {
            return $query->doInsert($con);
        });
    }

} // UniteTableMap
// This is the static code needed to register the TableMap for this table with the main Propel class.
//
UniteTableMap::buildTableMap();
