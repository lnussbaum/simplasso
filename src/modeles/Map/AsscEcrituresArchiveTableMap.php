<?php

namespace Map;

use \AsscEcrituresArchive;
use \AsscEcrituresArchiveQuery;
use Propel\Runtime\Propel;
use Propel\Runtime\ActiveQuery\Criteria;
use Propel\Runtime\ActiveQuery\InstancePoolTrait;
use Propel\Runtime\Connection\ConnectionInterface;
use Propel\Runtime\DataFetcher\DataFetcherInterface;
use Propel\Runtime\Exception\PropelException;
use Propel\Runtime\Map\RelationMap;
use Propel\Runtime\Map\TableMap;
use Propel\Runtime\Map\TableMapTrait;


/**
 * This class defines the structure of the 'assc_ecritures_archive' table.
 *
 *
 *
 * This map class is used by Propel to do runtime db structure discovery.
 * For example, the createSelectSql() method checks the type of a given column used in an
 * ORDER BY clause to know whether it needs to apply SQL to make the ORDER BY case-insensitive
 * (i.e. if it's a text column type).
 *
 */
class AsscEcrituresArchiveTableMap extends TableMap
{
    use InstancePoolTrait;
    use TableMapTrait;

    /**
     * The (dot-path) name of this class
     */
    const CLASS_NAME = '.Map.AsscEcrituresArchiveTableMap';

    /**
     * The default database name for this class
     */
    const DATABASE_NAME = 'simplasso';

    /**
     * The table name for this class
     */
    const TABLE_NAME = 'assc_ecritures_archive';

    /**
     * The related Propel class for this table
     */
    const OM_CLASS = '\\AsscEcrituresArchive';

    /**
     * A class that can be returned by this tableMap
     */
    const CLASS_DEFAULT = 'AsscEcrituresArchive';

    /**
     * The total number of columns
     */
    const NUM_COLUMNS = 21;

    /**
     * The number of lazy-loaded columns
     */
    const NUM_LAZY_LOAD_COLUMNS = 0;

    /**
     * The number of columns to hydrate (NUM_COLUMNS - NUM_LAZY_LOAD_COLUMNS)
     */
    const NUM_HYDRATE_COLUMNS = 21;

    /**
     * the column name for the id_ecriture field
     */
    const COL_ID_ECRITURE = 'assc_ecritures_archive.id_ecriture';

    /**
     * the column name for the classement field
     */
    const COL_CLASSEMENT = 'assc_ecritures_archive.classement';

    /**
     * the column name for the credit field
     */
    const COL_CREDIT = 'assc_ecritures_archive.credit';

    /**
     * the column name for the debit field
     */
    const COL_DEBIT = 'assc_ecritures_archive.debit';

    /**
     * the column name for the nom field
     */
    const COL_NOM = 'assc_ecritures_archive.nom';

    /**
     * the column name for the id_compte field
     */
    const COL_ID_COMPTE = 'assc_ecritures_archive.id_compte';

    /**
     * the column name for the id_journal field
     */
    const COL_ID_JOURNAL = 'assc_ecritures_archive.id_journal';

    /**
     * the column name for the id_activite field
     */
    const COL_ID_ACTIVITE = 'assc_ecritures_archive.id_activite';

    /**
     * the column name for the id_entite field
     */
    const COL_ID_ENTITE = 'assc_ecritures_archive.id_entite';

    /**
     * the column name for the lettrage field
     */
    const COL_LETTRAGE = 'assc_ecritures_archive.lettrage';

    /**
     * the column name for the date_lettrage field
     */
    const COL_DATE_LETTRAGE = 'assc_ecritures_archive.date_lettrage';

    /**
     * the column name for the date_ecriture field
     */
    const COL_DATE_ECRITURE = 'assc_ecritures_archive.date_ecriture';

    /**
     * the column name for the etat field
     */
    const COL_ETAT = 'assc_ecritures_archive.etat';

    /**
     * the column name for the observation field
     */
    const COL_OBSERVATION = 'assc_ecritures_archive.observation';

    /**
     * the column name for the id_piece field
     */
    const COL_ID_PIECE = 'assc_ecritures_archive.id_piece';

    /**
     * the column name for the ecran_saisie field
     */
    const COL_ECRAN_SAISIE = 'assc_ecritures_archive.ecran_saisie';

    /**
     * the column name for the contre_partie field
     */
    const COL_CONTRE_PARTIE = 'assc_ecritures_archive.contre_partie';

    /**
     * the column name for the quantite field
     */
    const COL_QUANTITE = 'assc_ecritures_archive.quantite';

    /**
     * the column name for the created_at field
     */
    const COL_CREATED_AT = 'assc_ecritures_archive.created_at';

    /**
     * the column name for the updated_at field
     */
    const COL_UPDATED_AT = 'assc_ecritures_archive.updated_at';

    /**
     * the column name for the archived_at field
     */
    const COL_ARCHIVED_AT = 'assc_ecritures_archive.archived_at';

    /**
     * The default string format for model objects of the related table
     */
    const DEFAULT_STRING_FORMAT = 'YAML';

    /**
     * holds an array of fieldnames
     *
     * first dimension keys are the type constants
     * e.g. self::$fieldNames[self::TYPE_PHPNAME][0] = 'Id'
     */
    protected static $fieldNames = array (
        self::TYPE_PHPNAME       => array('IdEcriture', 'Classement', 'Credit', 'Debit', 'Nom', 'IdCompte', 'IdJournal', 'IdActivite', 'IdEntite', 'Lettrage', 'DateLettrage', 'DateEcriture', 'Etat', 'Observation', 'IdPiece', 'EcranSaisie', 'ContrePartie', 'Quantite', 'CreatedAt', 'UpdatedAt', 'ArchivedAt', ),
        self::TYPE_CAMELNAME     => array('idEcriture', 'classement', 'credit', 'debit', 'nom', 'idCompte', 'idJournal', 'idActivite', 'idEntite', 'lettrage', 'dateLettrage', 'dateEcriture', 'etat', 'observation', 'idPiece', 'ecranSaisie', 'contrePartie', 'quantite', 'createdAt', 'updatedAt', 'archivedAt', ),
        self::TYPE_COLNAME       => array(AsscEcrituresArchiveTableMap::COL_ID_ECRITURE, AsscEcrituresArchiveTableMap::COL_CLASSEMENT, AsscEcrituresArchiveTableMap::COL_CREDIT, AsscEcrituresArchiveTableMap::COL_DEBIT, AsscEcrituresArchiveTableMap::COL_NOM, AsscEcrituresArchiveTableMap::COL_ID_COMPTE, AsscEcrituresArchiveTableMap::COL_ID_JOURNAL, AsscEcrituresArchiveTableMap::COL_ID_ACTIVITE, AsscEcrituresArchiveTableMap::COL_ID_ENTITE, AsscEcrituresArchiveTableMap::COL_LETTRAGE, AsscEcrituresArchiveTableMap::COL_DATE_LETTRAGE, AsscEcrituresArchiveTableMap::COL_DATE_ECRITURE, AsscEcrituresArchiveTableMap::COL_ETAT, AsscEcrituresArchiveTableMap::COL_OBSERVATION, AsscEcrituresArchiveTableMap::COL_ID_PIECE, AsscEcrituresArchiveTableMap::COL_ECRAN_SAISIE, AsscEcrituresArchiveTableMap::COL_CONTRE_PARTIE, AsscEcrituresArchiveTableMap::COL_QUANTITE, AsscEcrituresArchiveTableMap::COL_CREATED_AT, AsscEcrituresArchiveTableMap::COL_UPDATED_AT, AsscEcrituresArchiveTableMap::COL_ARCHIVED_AT, ),
        self::TYPE_FIELDNAME     => array('id_ecriture', 'classement', 'credit', 'debit', 'nom', 'id_compte', 'id_journal', 'id_activite', 'id_entite', 'lettrage', 'date_lettrage', 'date_ecriture', 'etat', 'observation', 'id_piece', 'ecran_saisie', 'contre_partie', 'quantite', 'created_at', 'updated_at', 'archived_at', ),
        self::TYPE_NUM           => array(0, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15, 16, 17, 18, 19, 20, )
    );

    /**
     * holds an array of keys for quick access to the fieldnames array
     *
     * first dimension keys are the type constants
     * e.g. self::$fieldKeys[self::TYPE_PHPNAME]['Id'] = 0
     */
    protected static $fieldKeys = array (
        self::TYPE_PHPNAME       => array('IdEcriture' => 0, 'Classement' => 1, 'Credit' => 2, 'Debit' => 3, 'Nom' => 4, 'IdCompte' => 5, 'IdJournal' => 6, 'IdActivite' => 7, 'IdEntite' => 8, 'Lettrage' => 9, 'DateLettrage' => 10, 'DateEcriture' => 11, 'Etat' => 12, 'Observation' => 13, 'IdPiece' => 14, 'EcranSaisie' => 15, 'ContrePartie' => 16, 'Quantite' => 17, 'CreatedAt' => 18, 'UpdatedAt' => 19, 'ArchivedAt' => 20, ),
        self::TYPE_CAMELNAME     => array('idEcriture' => 0, 'classement' => 1, 'credit' => 2, 'debit' => 3, 'nom' => 4, 'idCompte' => 5, 'idJournal' => 6, 'idActivite' => 7, 'idEntite' => 8, 'lettrage' => 9, 'dateLettrage' => 10, 'dateEcriture' => 11, 'etat' => 12, 'observation' => 13, 'idPiece' => 14, 'ecranSaisie' => 15, 'contrePartie' => 16, 'quantite' => 17, 'createdAt' => 18, 'updatedAt' => 19, 'archivedAt' => 20, ),
        self::TYPE_COLNAME       => array(AsscEcrituresArchiveTableMap::COL_ID_ECRITURE => 0, AsscEcrituresArchiveTableMap::COL_CLASSEMENT => 1, AsscEcrituresArchiveTableMap::COL_CREDIT => 2, AsscEcrituresArchiveTableMap::COL_DEBIT => 3, AsscEcrituresArchiveTableMap::COL_NOM => 4, AsscEcrituresArchiveTableMap::COL_ID_COMPTE => 5, AsscEcrituresArchiveTableMap::COL_ID_JOURNAL => 6, AsscEcrituresArchiveTableMap::COL_ID_ACTIVITE => 7, AsscEcrituresArchiveTableMap::COL_ID_ENTITE => 8, AsscEcrituresArchiveTableMap::COL_LETTRAGE => 9, AsscEcrituresArchiveTableMap::COL_DATE_LETTRAGE => 10, AsscEcrituresArchiveTableMap::COL_DATE_ECRITURE => 11, AsscEcrituresArchiveTableMap::COL_ETAT => 12, AsscEcrituresArchiveTableMap::COL_OBSERVATION => 13, AsscEcrituresArchiveTableMap::COL_ID_PIECE => 14, AsscEcrituresArchiveTableMap::COL_ECRAN_SAISIE => 15, AsscEcrituresArchiveTableMap::COL_CONTRE_PARTIE => 16, AsscEcrituresArchiveTableMap::COL_QUANTITE => 17, AsscEcrituresArchiveTableMap::COL_CREATED_AT => 18, AsscEcrituresArchiveTableMap::COL_UPDATED_AT => 19, AsscEcrituresArchiveTableMap::COL_ARCHIVED_AT => 20, ),
        self::TYPE_FIELDNAME     => array('id_ecriture' => 0, 'classement' => 1, 'credit' => 2, 'debit' => 3, 'nom' => 4, 'id_compte' => 5, 'id_journal' => 6, 'id_activite' => 7, 'id_entite' => 8, 'lettrage' => 9, 'date_lettrage' => 10, 'date_ecriture' => 11, 'etat' => 12, 'observation' => 13, 'id_piece' => 14, 'ecran_saisie' => 15, 'contre_partie' => 16, 'quantite' => 17, 'created_at' => 18, 'updated_at' => 19, 'archived_at' => 20, ),
        self::TYPE_NUM           => array(0, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15, 16, 17, 18, 19, 20, )
    );

    /**
     * Initialize the table attributes and columns
     * Relations are not initialized by this method since they are lazy loaded
     *
     * @return void
     * @throws PropelException
     */
    public function initialize()
    {
        // attributes
        $this->setName('assc_ecritures_archive');
        $this->setPhpName('AsscEcrituresArchive');
        $this->setIdentifierQuoting(true);
        $this->setClassName('\\AsscEcrituresArchive');
        $this->setPackage('');
        $this->setUseIdGenerator(false);
        // columns
        $this->addPrimaryKey('id_ecriture', 'IdEcriture', 'BIGINT', true, 21, null);
        $this->addColumn('classement', 'Classement', 'VARCHAR', true, 10, null);
        $this->addColumn('credit', 'Credit', 'DOUBLE', true, 12, 0);
        $this->addColumn('debit', 'Debit', 'DOUBLE', true, 12, 0);
        $this->addColumn('nom', 'Nom', 'VARCHAR', true, 50, null);
        $this->addColumn('id_compte', 'IdCompte', 'BIGINT', true, 21, 1);
        $this->addColumn('id_journal', 'IdJournal', 'INTEGER', true, null, 1);
        $this->addColumn('id_activite', 'IdActivite', 'BIGINT', false, 21, 1);
        $this->addColumn('id_entite', 'IdEntite', 'BIGINT', true, 21, 1);
        $this->addColumn('lettrage', 'Lettrage', 'VARCHAR', false, 10, null);
        $this->addColumn('date_lettrage', 'DateLettrage', 'TIMESTAMP', false, null, null);
        $this->addColumn('date_ecriture', 'DateEcriture', 'TIMESTAMP', true, null, null);
        $this->addColumn('etat', 'Etat', 'INTEGER', false, 2, 2);
        $this->addColumn('observation', 'Observation', 'LONGVARCHAR', false, null, null);
        $this->addColumn('id_piece', 'IdPiece', 'BIGINT', true, 21, 1);
        $this->addColumn('ecran_saisie', 'EcranSaisie', 'VARCHAR', true, 2, 'st');
        $this->addColumn('contre_partie', 'ContrePartie', 'BOOLEAN', true, 1, false);
        $this->addColumn('quantite', 'Quantite', 'DOUBLE', true, null, 0);
        $this->addColumn('created_at', 'CreatedAt', 'TIMESTAMP', false, null, null);
        $this->addColumn('updated_at', 'UpdatedAt', 'TIMESTAMP', false, null, null);
        $this->addColumn('archived_at', 'ArchivedAt', 'TIMESTAMP', false, null, null);
    } // initialize()

    /**
     * Build the RelationMap objects for this table relationships
     */
    public function buildRelations()
    {
    } // buildRelations()

    /**
     * Retrieves a string version of the primary key from the DB resultset row that can be used to uniquely identify a row in this table.
     *
     * For tables with a single-column primary key, that simple pkey value will be returned.  For tables with
     * a multi-column primary key, a serialize()d version of the primary key will be returned.
     *
     * @param array  $row       resultset row.
     * @param int    $offset    The 0-based offset for reading from the resultset row.
     * @param string $indexType One of the class type constants TableMap::TYPE_PHPNAME, TableMap::TYPE_CAMELNAME
     *                           TableMap::TYPE_COLNAME, TableMap::TYPE_FIELDNAME, TableMap::TYPE_NUM
     *
     * @return string The primary key hash of the row
     */
    public static function getPrimaryKeyHashFromRow($row, $offset = 0, $indexType = TableMap::TYPE_NUM)
    {
        // If the PK cannot be derived from the row, return NULL.
        if ($row[TableMap::TYPE_NUM == $indexType ? 0 + $offset : static::translateFieldName('IdEcriture', TableMap::TYPE_PHPNAME, $indexType)] === null) {
            return null;
        }

        return null === $row[TableMap::TYPE_NUM == $indexType ? 0 + $offset : static::translateFieldName('IdEcriture', TableMap::TYPE_PHPNAME, $indexType)] || is_scalar($row[TableMap::TYPE_NUM == $indexType ? 0 + $offset : static::translateFieldName('IdEcriture', TableMap::TYPE_PHPNAME, $indexType)]) || is_callable([$row[TableMap::TYPE_NUM == $indexType ? 0 + $offset : static::translateFieldName('IdEcriture', TableMap::TYPE_PHPNAME, $indexType)], '__toString']) ? (string) $row[TableMap::TYPE_NUM == $indexType ? 0 + $offset : static::translateFieldName('IdEcriture', TableMap::TYPE_PHPNAME, $indexType)] : $row[TableMap::TYPE_NUM == $indexType ? 0 + $offset : static::translateFieldName('IdEcriture', TableMap::TYPE_PHPNAME, $indexType)];
    }

    /**
     * Retrieves the primary key from the DB resultset row
     * For tables with a single-column primary key, that simple pkey value will be returned.  For tables with
     * a multi-column primary key, an array of the primary key columns will be returned.
     *
     * @param array  $row       resultset row.
     * @param int    $offset    The 0-based offset for reading from the resultset row.
     * @param string $indexType One of the class type constants TableMap::TYPE_PHPNAME, TableMap::TYPE_CAMELNAME
     *                           TableMap::TYPE_COLNAME, TableMap::TYPE_FIELDNAME, TableMap::TYPE_NUM
     *
     * @return mixed The primary key of the row
     */
    public static function getPrimaryKeyFromRow($row, $offset = 0, $indexType = TableMap::TYPE_NUM)
    {
        return (string) $row[
            $indexType == TableMap::TYPE_NUM
                ? 0 + $offset
                : self::translateFieldName('IdEcriture', TableMap::TYPE_PHPNAME, $indexType)
        ];
    }

    /**
     * The class that the tableMap will make instances of.
     *
     * If $withPrefix is true, the returned path
     * uses a dot-path notation which is translated into a path
     * relative to a location on the PHP include_path.
     * (e.g. path.to.MyClass -> 'path/to/MyClass.php')
     *
     * @param boolean $withPrefix Whether or not to return the path with the class name
     * @return string path.to.ClassName
     */
    public static function getOMClass($withPrefix = true)
    {
        return $withPrefix ? AsscEcrituresArchiveTableMap::CLASS_DEFAULT : AsscEcrituresArchiveTableMap::OM_CLASS;
    }

    /**
     * Populates an object of the default type or an object that inherit from the default.
     *
     * @param array  $row       row returned by DataFetcher->fetch().
     * @param int    $offset    The 0-based offset for reading from the resultset row.
     * @param string $indexType The index type of $row. Mostly DataFetcher->getIndexType().
                                 One of the class type constants TableMap::TYPE_PHPNAME, TableMap::TYPE_CAMELNAME
     *                           TableMap::TYPE_COLNAME, TableMap::TYPE_FIELDNAME, TableMap::TYPE_NUM.
     *
     * @throws PropelException Any exceptions caught during processing will be
     *                         rethrown wrapped into a PropelException.
     * @return array           (AsscEcrituresArchive object, last column rank)
     */
    public static function populateObject($row, $offset = 0, $indexType = TableMap::TYPE_NUM)
    {
        $key = AsscEcrituresArchiveTableMap::getPrimaryKeyHashFromRow($row, $offset, $indexType);
        if (null !== ($obj = AsscEcrituresArchiveTableMap::getInstanceFromPool($key))) {
            // We no longer rehydrate the object, since this can cause data loss.
            // See http://www.propelorm.org/ticket/509
            // $obj->hydrate($row, $offset, true); // rehydrate
            $col = $offset + AsscEcrituresArchiveTableMap::NUM_HYDRATE_COLUMNS;
        } else {
            $cls = AsscEcrituresArchiveTableMap::OM_CLASS;
            /** @var AsscEcrituresArchive $obj */
            $obj = new $cls();
            $col = $obj->hydrate($row, $offset, false, $indexType);
            AsscEcrituresArchiveTableMap::addInstanceToPool($obj, $key);
        }

        return array($obj, $col);
    }

    /**
     * The returned array will contain objects of the default type or
     * objects that inherit from the default.
     *
     * @param DataFetcherInterface $dataFetcher
     * @return array
     * @throws PropelException Any exceptions caught during processing will be
     *                         rethrown wrapped into a PropelException.
     */
    public static function populateObjects(DataFetcherInterface $dataFetcher)
    {
        $results = array();

        // set the class once to avoid overhead in the loop
        $cls = static::getOMClass(false);
        // populate the object(s)
        while ($row = $dataFetcher->fetch()) {
            $key = AsscEcrituresArchiveTableMap::getPrimaryKeyHashFromRow($row, 0, $dataFetcher->getIndexType());
            if (null !== ($obj = AsscEcrituresArchiveTableMap::getInstanceFromPool($key))) {
                // We no longer rehydrate the object, since this can cause data loss.
                // See http://www.propelorm.org/ticket/509
                // $obj->hydrate($row, 0, true); // rehydrate
                $results[] = $obj;
            } else {
                /** @var AsscEcrituresArchive $obj */
                $obj = new $cls();
                $obj->hydrate($row);
                $results[] = $obj;
                AsscEcrituresArchiveTableMap::addInstanceToPool($obj, $key);
            } // if key exists
        }

        return $results;
    }
    /**
     * Add all the columns needed to create a new object.
     *
     * Note: any columns that were marked with lazyLoad="true" in the
     * XML schema will not be added to the select list and only loaded
     * on demand.
     *
     * @param Criteria $criteria object containing the columns to add.
     * @param string   $alias    optional table alias
     * @throws PropelException Any exceptions caught during processing will be
     *                         rethrown wrapped into a PropelException.
     */
    public static function addSelectColumns(Criteria $criteria, $alias = null)
    {
        if (null === $alias) {
            $criteria->addSelectColumn(AsscEcrituresArchiveTableMap::COL_ID_ECRITURE);
            $criteria->addSelectColumn(AsscEcrituresArchiveTableMap::COL_CLASSEMENT);
            $criteria->addSelectColumn(AsscEcrituresArchiveTableMap::COL_CREDIT);
            $criteria->addSelectColumn(AsscEcrituresArchiveTableMap::COL_DEBIT);
            $criteria->addSelectColumn(AsscEcrituresArchiveTableMap::COL_NOM);
            $criteria->addSelectColumn(AsscEcrituresArchiveTableMap::COL_ID_COMPTE);
            $criteria->addSelectColumn(AsscEcrituresArchiveTableMap::COL_ID_JOURNAL);
            $criteria->addSelectColumn(AsscEcrituresArchiveTableMap::COL_ID_ACTIVITE);
            $criteria->addSelectColumn(AsscEcrituresArchiveTableMap::COL_ID_ENTITE);
            $criteria->addSelectColumn(AsscEcrituresArchiveTableMap::COL_LETTRAGE);
            $criteria->addSelectColumn(AsscEcrituresArchiveTableMap::COL_DATE_LETTRAGE);
            $criteria->addSelectColumn(AsscEcrituresArchiveTableMap::COL_DATE_ECRITURE);
            $criteria->addSelectColumn(AsscEcrituresArchiveTableMap::COL_ETAT);
            $criteria->addSelectColumn(AsscEcrituresArchiveTableMap::COL_OBSERVATION);
            $criteria->addSelectColumn(AsscEcrituresArchiveTableMap::COL_ID_PIECE);
            $criteria->addSelectColumn(AsscEcrituresArchiveTableMap::COL_ECRAN_SAISIE);
            $criteria->addSelectColumn(AsscEcrituresArchiveTableMap::COL_CONTRE_PARTIE);
            $criteria->addSelectColumn(AsscEcrituresArchiveTableMap::COL_QUANTITE);
            $criteria->addSelectColumn(AsscEcrituresArchiveTableMap::COL_CREATED_AT);
            $criteria->addSelectColumn(AsscEcrituresArchiveTableMap::COL_UPDATED_AT);
            $criteria->addSelectColumn(AsscEcrituresArchiveTableMap::COL_ARCHIVED_AT);
        } else {
            $criteria->addSelectColumn($alias . '.id_ecriture');
            $criteria->addSelectColumn($alias . '.classement');
            $criteria->addSelectColumn($alias . '.credit');
            $criteria->addSelectColumn($alias . '.debit');
            $criteria->addSelectColumn($alias . '.nom');
            $criteria->addSelectColumn($alias . '.id_compte');
            $criteria->addSelectColumn($alias . '.id_journal');
            $criteria->addSelectColumn($alias . '.id_activite');
            $criteria->addSelectColumn($alias . '.id_entite');
            $criteria->addSelectColumn($alias . '.lettrage');
            $criteria->addSelectColumn($alias . '.date_lettrage');
            $criteria->addSelectColumn($alias . '.date_ecriture');
            $criteria->addSelectColumn($alias . '.etat');
            $criteria->addSelectColumn($alias . '.observation');
            $criteria->addSelectColumn($alias . '.id_piece');
            $criteria->addSelectColumn($alias . '.ecran_saisie');
            $criteria->addSelectColumn($alias . '.contre_partie');
            $criteria->addSelectColumn($alias . '.quantite');
            $criteria->addSelectColumn($alias . '.created_at');
            $criteria->addSelectColumn($alias . '.updated_at');
            $criteria->addSelectColumn($alias . '.archived_at');
        }
    }

    /**
     * Returns the TableMap related to this object.
     * This method is not needed for general use but a specific application could have a need.
     * @return TableMap
     * @throws PropelException Any exceptions caught during processing will be
     *                         rethrown wrapped into a PropelException.
     */
    public static function getTableMap()
    {
        return Propel::getServiceContainer()->getDatabaseMap(AsscEcrituresArchiveTableMap::DATABASE_NAME)->getTable(AsscEcrituresArchiveTableMap::TABLE_NAME);
    }

    /**
     * Add a TableMap instance to the database for this tableMap class.
     */
    public static function buildTableMap()
    {
        $dbMap = Propel::getServiceContainer()->getDatabaseMap(AsscEcrituresArchiveTableMap::DATABASE_NAME);
        if (!$dbMap->hasTable(AsscEcrituresArchiveTableMap::TABLE_NAME)) {
            $dbMap->addTableObject(new AsscEcrituresArchiveTableMap());
        }
    }

    /**
     * Performs a DELETE on the database, given a AsscEcrituresArchive or Criteria object OR a primary key value.
     *
     * @param mixed               $values Criteria or AsscEcrituresArchive object or primary key or array of primary keys
     *              which is used to create the DELETE statement
     * @param  ConnectionInterface $con the connection to use
     * @return int             The number of affected rows (if supported by underlying database driver).  This includes CASCADE-related rows
     *                         if supported by native driver or if emulated using Propel.
     * @throws PropelException Any exceptions caught during processing will be
     *                         rethrown wrapped into a PropelException.
     */
     public static function doDelete($values, ConnectionInterface $con = null)
     {
        if (null === $con) {
            $con = Propel::getServiceContainer()->getWriteConnection(AsscEcrituresArchiveTableMap::DATABASE_NAME);
        }

        if ($values instanceof Criteria) {
            // rename for clarity
            $criteria = $values;
        } elseif ($values instanceof \AsscEcrituresArchive) { // it's a model object
            // create criteria based on pk values
            $criteria = $values->buildPkeyCriteria();
        } else { // it's a primary key, or an array of pks
            $criteria = new Criteria(AsscEcrituresArchiveTableMap::DATABASE_NAME);
            $criteria->add(AsscEcrituresArchiveTableMap::COL_ID_ECRITURE, (array) $values, Criteria::IN);
        }

        $query = AsscEcrituresArchiveQuery::create()->mergeWith($criteria);

        if ($values instanceof Criteria) {
            AsscEcrituresArchiveTableMap::clearInstancePool();
        } elseif (!is_object($values)) { // it's a primary key, or an array of pks
            foreach ((array) $values as $singleval) {
                AsscEcrituresArchiveTableMap::removeInstanceFromPool($singleval);
            }
        }

        return $query->delete($con);
    }

    /**
     * Deletes all rows from the assc_ecritures_archive table.
     *
     * @param ConnectionInterface $con the connection to use
     * @return int The number of affected rows (if supported by underlying database driver).
     */
    public static function doDeleteAll(ConnectionInterface $con = null)
    {
        return AsscEcrituresArchiveQuery::create()->doDeleteAll($con);
    }

    /**
     * Performs an INSERT on the database, given a AsscEcrituresArchive or Criteria object.
     *
     * @param mixed               $criteria Criteria or AsscEcrituresArchive object containing data that is used to create the INSERT statement.
     * @param ConnectionInterface $con the ConnectionInterface connection to use
     * @return mixed           The new primary key.
     * @throws PropelException Any exceptions caught during processing will be
     *                         rethrown wrapped into a PropelException.
     */
    public static function doInsert($criteria, ConnectionInterface $con = null)
    {
        if (null === $con) {
            $con = Propel::getServiceContainer()->getWriteConnection(AsscEcrituresArchiveTableMap::DATABASE_NAME);
        }

        if ($criteria instanceof Criteria) {
            $criteria = clone $criteria; // rename for clarity
        } else {
            $criteria = $criteria->buildCriteria(); // build Criteria from AsscEcrituresArchive object
        }


        // Set the correct dbName
        $query = AsscEcrituresArchiveQuery::create()->mergeWith($criteria);

        // use transaction because $criteria could contain info
        // for more than one table (I guess, conceivably)
        return $con->transaction(function () use ($con, $query) {
            return $query->doInsert($con);
        });
    }

} // AsscEcrituresArchiveTableMap
// This is the static code needed to register the TableMap for this table with the main Propel class.
//
AsscEcrituresArchiveTableMap::buildTableMap();
