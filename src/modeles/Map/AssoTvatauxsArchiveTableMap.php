<?php

namespace Map;

use \AssoTvatauxsArchive;
use \AssoTvatauxsArchiveQuery;
use Propel\Runtime\Propel;
use Propel\Runtime\ActiveQuery\Criteria;
use Propel\Runtime\ActiveQuery\InstancePoolTrait;
use Propel\Runtime\Connection\ConnectionInterface;
use Propel\Runtime\DataFetcher\DataFetcherInterface;
use Propel\Runtime\Exception\PropelException;
use Propel\Runtime\Map\RelationMap;
use Propel\Runtime\Map\TableMap;
use Propel\Runtime\Map\TableMapTrait;


/**
 * This class defines the structure of the 'asso_tvatauxs_archive' table.
 *
 *
 *
 * This map class is used by Propel to do runtime db structure discovery.
 * For example, the createSelectSql() method checks the type of a given column used in an
 * ORDER BY clause to know whether it needs to apply SQL to make the ORDER BY case-insensitive
 * (i.e. if it's a text column type).
 *
 */
class AssoTvatauxsArchiveTableMap extends TableMap
{
    use InstancePoolTrait;
    use TableMapTrait;

    /**
     * The (dot-path) name of this class
     */
    const CLASS_NAME = '.Map.AssoTvatauxsArchiveTableMap';

    /**
     * The default database name for this class
     */
    const DATABASE_NAME = 'simplasso';

    /**
     * The table name for this class
     */
    const TABLE_NAME = 'asso_tvatauxs_archive';

    /**
     * The related Propel class for this table
     */
    const OM_CLASS = '\\AssoTvatauxsArchive';

    /**
     * A class that can be returned by this tableMap
     */
    const CLASS_DEFAULT = 'AssoTvatauxsArchive';

    /**
     * The total number of columns
     */
    const NUM_COLUMNS = 9;

    /**
     * The number of lazy-loaded columns
     */
    const NUM_LAZY_LOAD_COLUMNS = 0;

    /**
     * The number of columns to hydrate (NUM_COLUMNS - NUM_LAZY_LOAD_COLUMNS)
     */
    const NUM_HYDRATE_COLUMNS = 9;

    /**
     * the column name for the id_tvataux field
     */
    const COL_ID_TVATAUX = 'asso_tvatauxs_archive.id_tvataux';

    /**
     * the column name for the id_tva field
     */
    const COL_ID_TVA = 'asso_tvatauxs_archive.id_tva';

    /**
     * the column name for the taux field
     */
    const COL_TAUX = 'asso_tvatauxs_archive.taux';

    /**
     * the column name for the date_debut field
     */
    const COL_DATE_DEBUT = 'asso_tvatauxs_archive.date_debut';

    /**
     * the column name for the date_fin field
     */
    const COL_DATE_FIN = 'asso_tvatauxs_archive.date_fin';

    /**
     * the column name for the observation field
     */
    const COL_OBSERVATION = 'asso_tvatauxs_archive.observation';

    /**
     * the column name for the created_at field
     */
    const COL_CREATED_AT = 'asso_tvatauxs_archive.created_at';

    /**
     * the column name for the updated_at field
     */
    const COL_UPDATED_AT = 'asso_tvatauxs_archive.updated_at';

    /**
     * the column name for the archived_at field
     */
    const COL_ARCHIVED_AT = 'asso_tvatauxs_archive.archived_at';

    /**
     * The default string format for model objects of the related table
     */
    const DEFAULT_STRING_FORMAT = 'YAML';

    /**
     * holds an array of fieldnames
     *
     * first dimension keys are the type constants
     * e.g. self::$fieldNames[self::TYPE_PHPNAME][0] = 'Id'
     */
    protected static $fieldNames = array (
        self::TYPE_PHPNAME       => array('IdTvataux', 'IdTva', 'Taux', 'DateDebut', 'DateFin', 'Observation', 'CreatedAt', 'UpdatedAt', 'ArchivedAt', ),
        self::TYPE_CAMELNAME     => array('idTvataux', 'idTva', 'taux', 'dateDebut', 'dateFin', 'observation', 'createdAt', 'updatedAt', 'archivedAt', ),
        self::TYPE_COLNAME       => array(AssoTvatauxsArchiveTableMap::COL_ID_TVATAUX, AssoTvatauxsArchiveTableMap::COL_ID_TVA, AssoTvatauxsArchiveTableMap::COL_TAUX, AssoTvatauxsArchiveTableMap::COL_DATE_DEBUT, AssoTvatauxsArchiveTableMap::COL_DATE_FIN, AssoTvatauxsArchiveTableMap::COL_OBSERVATION, AssoTvatauxsArchiveTableMap::COL_CREATED_AT, AssoTvatauxsArchiveTableMap::COL_UPDATED_AT, AssoTvatauxsArchiveTableMap::COL_ARCHIVED_AT, ),
        self::TYPE_FIELDNAME     => array('id_tvataux', 'id_tva', 'taux', 'date_debut', 'date_fin', 'observation', 'created_at', 'updated_at', 'archived_at', ),
        self::TYPE_NUM           => array(0, 1, 2, 3, 4, 5, 6, 7, 8, )
    );

    /**
     * holds an array of keys for quick access to the fieldnames array
     *
     * first dimension keys are the type constants
     * e.g. self::$fieldKeys[self::TYPE_PHPNAME]['Id'] = 0
     */
    protected static $fieldKeys = array (
        self::TYPE_PHPNAME       => array('IdTvataux' => 0, 'IdTva' => 1, 'Taux' => 2, 'DateDebut' => 3, 'DateFin' => 4, 'Observation' => 5, 'CreatedAt' => 6, 'UpdatedAt' => 7, 'ArchivedAt' => 8, ),
        self::TYPE_CAMELNAME     => array('idTvataux' => 0, 'idTva' => 1, 'taux' => 2, 'dateDebut' => 3, 'dateFin' => 4, 'observation' => 5, 'createdAt' => 6, 'updatedAt' => 7, 'archivedAt' => 8, ),
        self::TYPE_COLNAME       => array(AssoTvatauxsArchiveTableMap::COL_ID_TVATAUX => 0, AssoTvatauxsArchiveTableMap::COL_ID_TVA => 1, AssoTvatauxsArchiveTableMap::COL_TAUX => 2, AssoTvatauxsArchiveTableMap::COL_DATE_DEBUT => 3, AssoTvatauxsArchiveTableMap::COL_DATE_FIN => 4, AssoTvatauxsArchiveTableMap::COL_OBSERVATION => 5, AssoTvatauxsArchiveTableMap::COL_CREATED_AT => 6, AssoTvatauxsArchiveTableMap::COL_UPDATED_AT => 7, AssoTvatauxsArchiveTableMap::COL_ARCHIVED_AT => 8, ),
        self::TYPE_FIELDNAME     => array('id_tvataux' => 0, 'id_tva' => 1, 'taux' => 2, 'date_debut' => 3, 'date_fin' => 4, 'observation' => 5, 'created_at' => 6, 'updated_at' => 7, 'archived_at' => 8, ),
        self::TYPE_NUM           => array(0, 1, 2, 3, 4, 5, 6, 7, 8, )
    );

    /**
     * Initialize the table attributes and columns
     * Relations are not initialized by this method since they are lazy loaded
     *
     * @return void
     * @throws PropelException
     */
    public function initialize()
    {
        // attributes
        $this->setName('asso_tvatauxs_archive');
        $this->setPhpName('AssoTvatauxsArchive');
        $this->setIdentifierQuoting(true);
        $this->setClassName('\\AssoTvatauxsArchive');
        $this->setPackage('');
        $this->setUseIdGenerator(false);
        // columns
        $this->addPrimaryKey('id_tvataux', 'IdTvataux', 'BIGINT', true, 21, null);
        $this->addColumn('id_tva', 'IdTva', 'BIGINT', true, 21, null);
        $this->addColumn('taux', 'Taux', 'DOUBLE', true, 10, 0);
        $this->addColumn('date_debut', 'DateDebut', 'DATE', true, null, '1980-01-01');
        $this->addColumn('date_fin', 'DateFin', 'DATE', true, null, '3000-12-31');
        $this->addColumn('observation', 'Observation', 'VARCHAR', false, 100, null);
        $this->addColumn('created_at', 'CreatedAt', 'TIMESTAMP', false, null, null);
        $this->addColumn('updated_at', 'UpdatedAt', 'TIMESTAMP', false, null, null);
        $this->addColumn('archived_at', 'ArchivedAt', 'TIMESTAMP', false, null, null);
    } // initialize()

    /**
     * Build the RelationMap objects for this table relationships
     */
    public function buildRelations()
    {
    } // buildRelations()

    /**
     * Retrieves a string version of the primary key from the DB resultset row that can be used to uniquely identify a row in this table.
     *
     * For tables with a single-column primary key, that simple pkey value will be returned.  For tables with
     * a multi-column primary key, a serialize()d version of the primary key will be returned.
     *
     * @param array  $row       resultset row.
     * @param int    $offset    The 0-based offset for reading from the resultset row.
     * @param string $indexType One of the class type constants TableMap::TYPE_PHPNAME, TableMap::TYPE_CAMELNAME
     *                           TableMap::TYPE_COLNAME, TableMap::TYPE_FIELDNAME, TableMap::TYPE_NUM
     *
     * @return string The primary key hash of the row
     */
    public static function getPrimaryKeyHashFromRow($row, $offset = 0, $indexType = TableMap::TYPE_NUM)
    {
        // If the PK cannot be derived from the row, return NULL.
        if ($row[TableMap::TYPE_NUM == $indexType ? 0 + $offset : static::translateFieldName('IdTvataux', TableMap::TYPE_PHPNAME, $indexType)] === null) {
            return null;
        }

        return null === $row[TableMap::TYPE_NUM == $indexType ? 0 + $offset : static::translateFieldName('IdTvataux', TableMap::TYPE_PHPNAME, $indexType)] || is_scalar($row[TableMap::TYPE_NUM == $indexType ? 0 + $offset : static::translateFieldName('IdTvataux', TableMap::TYPE_PHPNAME, $indexType)]) || is_callable([$row[TableMap::TYPE_NUM == $indexType ? 0 + $offset : static::translateFieldName('IdTvataux', TableMap::TYPE_PHPNAME, $indexType)], '__toString']) ? (string) $row[TableMap::TYPE_NUM == $indexType ? 0 + $offset : static::translateFieldName('IdTvataux', TableMap::TYPE_PHPNAME, $indexType)] : $row[TableMap::TYPE_NUM == $indexType ? 0 + $offset : static::translateFieldName('IdTvataux', TableMap::TYPE_PHPNAME, $indexType)];
    }

    /**
     * Retrieves the primary key from the DB resultset row
     * For tables with a single-column primary key, that simple pkey value will be returned.  For tables with
     * a multi-column primary key, an array of the primary key columns will be returned.
     *
     * @param array  $row       resultset row.
     * @param int    $offset    The 0-based offset for reading from the resultset row.
     * @param string $indexType One of the class type constants TableMap::TYPE_PHPNAME, TableMap::TYPE_CAMELNAME
     *                           TableMap::TYPE_COLNAME, TableMap::TYPE_FIELDNAME, TableMap::TYPE_NUM
     *
     * @return mixed The primary key of the row
     */
    public static function getPrimaryKeyFromRow($row, $offset = 0, $indexType = TableMap::TYPE_NUM)
    {
        return (string) $row[
            $indexType == TableMap::TYPE_NUM
                ? 0 + $offset
                : self::translateFieldName('IdTvataux', TableMap::TYPE_PHPNAME, $indexType)
        ];
    }

    /**
     * The class that the tableMap will make instances of.
     *
     * If $withPrefix is true, the returned path
     * uses a dot-path notation which is translated into a path
     * relative to a location on the PHP include_path.
     * (e.g. path.to.MyClass -> 'path/to/MyClass.php')
     *
     * @param boolean $withPrefix Whether or not to return the path with the class name
     * @return string path.to.ClassName
     */
    public static function getOMClass($withPrefix = true)
    {
        return $withPrefix ? AssoTvatauxsArchiveTableMap::CLASS_DEFAULT : AssoTvatauxsArchiveTableMap::OM_CLASS;
    }

    /**
     * Populates an object of the default type or an object that inherit from the default.
     *
     * @param array  $row       row returned by DataFetcher->fetch().
     * @param int    $offset    The 0-based offset for reading from the resultset row.
     * @param string $indexType The index type of $row. Mostly DataFetcher->getIndexType().
                                 One of the class type constants TableMap::TYPE_PHPNAME, TableMap::TYPE_CAMELNAME
     *                           TableMap::TYPE_COLNAME, TableMap::TYPE_FIELDNAME, TableMap::TYPE_NUM.
     *
     * @throws PropelException Any exceptions caught during processing will be
     *                         rethrown wrapped into a PropelException.
     * @return array           (AssoTvatauxsArchive object, last column rank)
     */
    public static function populateObject($row, $offset = 0, $indexType = TableMap::TYPE_NUM)
    {
        $key = AssoTvatauxsArchiveTableMap::getPrimaryKeyHashFromRow($row, $offset, $indexType);
        if (null !== ($obj = AssoTvatauxsArchiveTableMap::getInstanceFromPool($key))) {
            // We no longer rehydrate the object, since this can cause data loss.
            // See http://www.propelorm.org/ticket/509
            // $obj->hydrate($row, $offset, true); // rehydrate
            $col = $offset + AssoTvatauxsArchiveTableMap::NUM_HYDRATE_COLUMNS;
        } else {
            $cls = AssoTvatauxsArchiveTableMap::OM_CLASS;
            /** @var AssoTvatauxsArchive $obj */
            $obj = new $cls();
            $col = $obj->hydrate($row, $offset, false, $indexType);
            AssoTvatauxsArchiveTableMap::addInstanceToPool($obj, $key);
        }

        return array($obj, $col);
    }

    /**
     * The returned array will contain objects of the default type or
     * objects that inherit from the default.
     *
     * @param DataFetcherInterface $dataFetcher
     * @return array
     * @throws PropelException Any exceptions caught during processing will be
     *                         rethrown wrapped into a PropelException.
     */
    public static function populateObjects(DataFetcherInterface $dataFetcher)
    {
        $results = array();

        // set the class once to avoid overhead in the loop
        $cls = static::getOMClass(false);
        // populate the object(s)
        while ($row = $dataFetcher->fetch()) {
            $key = AssoTvatauxsArchiveTableMap::getPrimaryKeyHashFromRow($row, 0, $dataFetcher->getIndexType());
            if (null !== ($obj = AssoTvatauxsArchiveTableMap::getInstanceFromPool($key))) {
                // We no longer rehydrate the object, since this can cause data loss.
                // See http://www.propelorm.org/ticket/509
                // $obj->hydrate($row, 0, true); // rehydrate
                $results[] = $obj;
            } else {
                /** @var AssoTvatauxsArchive $obj */
                $obj = new $cls();
                $obj->hydrate($row);
                $results[] = $obj;
                AssoTvatauxsArchiveTableMap::addInstanceToPool($obj, $key);
            } // if key exists
        }

        return $results;
    }
    /**
     * Add all the columns needed to create a new object.
     *
     * Note: any columns that were marked with lazyLoad="true" in the
     * XML schema will not be added to the select list and only loaded
     * on demand.
     *
     * @param Criteria $criteria object containing the columns to add.
     * @param string   $alias    optional table alias
     * @throws PropelException Any exceptions caught during processing will be
     *                         rethrown wrapped into a PropelException.
     */
    public static function addSelectColumns(Criteria $criteria, $alias = null)
    {
        if (null === $alias) {
            $criteria->addSelectColumn(AssoTvatauxsArchiveTableMap::COL_ID_TVATAUX);
            $criteria->addSelectColumn(AssoTvatauxsArchiveTableMap::COL_ID_TVA);
            $criteria->addSelectColumn(AssoTvatauxsArchiveTableMap::COL_TAUX);
            $criteria->addSelectColumn(AssoTvatauxsArchiveTableMap::COL_DATE_DEBUT);
            $criteria->addSelectColumn(AssoTvatauxsArchiveTableMap::COL_DATE_FIN);
            $criteria->addSelectColumn(AssoTvatauxsArchiveTableMap::COL_OBSERVATION);
            $criteria->addSelectColumn(AssoTvatauxsArchiveTableMap::COL_CREATED_AT);
            $criteria->addSelectColumn(AssoTvatauxsArchiveTableMap::COL_UPDATED_AT);
            $criteria->addSelectColumn(AssoTvatauxsArchiveTableMap::COL_ARCHIVED_AT);
        } else {
            $criteria->addSelectColumn($alias . '.id_tvataux');
            $criteria->addSelectColumn($alias . '.id_tva');
            $criteria->addSelectColumn($alias . '.taux');
            $criteria->addSelectColumn($alias . '.date_debut');
            $criteria->addSelectColumn($alias . '.date_fin');
            $criteria->addSelectColumn($alias . '.observation');
            $criteria->addSelectColumn($alias . '.created_at');
            $criteria->addSelectColumn($alias . '.updated_at');
            $criteria->addSelectColumn($alias . '.archived_at');
        }
    }

    /**
     * Returns the TableMap related to this object.
     * This method is not needed for general use but a specific application could have a need.
     * @return TableMap
     * @throws PropelException Any exceptions caught during processing will be
     *                         rethrown wrapped into a PropelException.
     */
    public static function getTableMap()
    {
        return Propel::getServiceContainer()->getDatabaseMap(AssoTvatauxsArchiveTableMap::DATABASE_NAME)->getTable(AssoTvatauxsArchiveTableMap::TABLE_NAME);
    }

    /**
     * Add a TableMap instance to the database for this tableMap class.
     */
    public static function buildTableMap()
    {
        $dbMap = Propel::getServiceContainer()->getDatabaseMap(AssoTvatauxsArchiveTableMap::DATABASE_NAME);
        if (!$dbMap->hasTable(AssoTvatauxsArchiveTableMap::TABLE_NAME)) {
            $dbMap->addTableObject(new AssoTvatauxsArchiveTableMap());
        }
    }

    /**
     * Performs a DELETE on the database, given a AssoTvatauxsArchive or Criteria object OR a primary key value.
     *
     * @param mixed               $values Criteria or AssoTvatauxsArchive object or primary key or array of primary keys
     *              which is used to create the DELETE statement
     * @param  ConnectionInterface $con the connection to use
     * @return int             The number of affected rows (if supported by underlying database driver).  This includes CASCADE-related rows
     *                         if supported by native driver or if emulated using Propel.
     * @throws PropelException Any exceptions caught during processing will be
     *                         rethrown wrapped into a PropelException.
     */
     public static function doDelete($values, ConnectionInterface $con = null)
     {
        if (null === $con) {
            $con = Propel::getServiceContainer()->getWriteConnection(AssoTvatauxsArchiveTableMap::DATABASE_NAME);
        }

        if ($values instanceof Criteria) {
            // rename for clarity
            $criteria = $values;
        } elseif ($values instanceof \AssoTvatauxsArchive) { // it's a model object
            // create criteria based on pk values
            $criteria = $values->buildPkeyCriteria();
        } else { // it's a primary key, or an array of pks
            $criteria = new Criteria(AssoTvatauxsArchiveTableMap::DATABASE_NAME);
            $criteria->add(AssoTvatauxsArchiveTableMap::COL_ID_TVATAUX, (array) $values, Criteria::IN);
        }

        $query = AssoTvatauxsArchiveQuery::create()->mergeWith($criteria);

        if ($values instanceof Criteria) {
            AssoTvatauxsArchiveTableMap::clearInstancePool();
        } elseif (!is_object($values)) { // it's a primary key, or an array of pks
            foreach ((array) $values as $singleval) {
                AssoTvatauxsArchiveTableMap::removeInstanceFromPool($singleval);
            }
        }

        return $query->delete($con);
    }

    /**
     * Deletes all rows from the asso_tvatauxs_archive table.
     *
     * @param ConnectionInterface $con the connection to use
     * @return int The number of affected rows (if supported by underlying database driver).
     */
    public static function doDeleteAll(ConnectionInterface $con = null)
    {
        return AssoTvatauxsArchiveQuery::create()->doDeleteAll($con);
    }

    /**
     * Performs an INSERT on the database, given a AssoTvatauxsArchive or Criteria object.
     *
     * @param mixed               $criteria Criteria or AssoTvatauxsArchive object containing data that is used to create the INSERT statement.
     * @param ConnectionInterface $con the ConnectionInterface connection to use
     * @return mixed           The new primary key.
     * @throws PropelException Any exceptions caught during processing will be
     *                         rethrown wrapped into a PropelException.
     */
    public static function doInsert($criteria, ConnectionInterface $con = null)
    {
        if (null === $con) {
            $con = Propel::getServiceContainer()->getWriteConnection(AssoTvatauxsArchiveTableMap::DATABASE_NAME);
        }

        if ($criteria instanceof Criteria) {
            $criteria = clone $criteria; // rename for clarity
        } else {
            $criteria = $criteria->buildCriteria(); // build Criteria from AssoTvatauxsArchive object
        }


        // Set the correct dbName
        $query = AssoTvatauxsArchiveQuery::create()->mergeWith($criteria);

        // use transaction because $criteria could contain info
        // for more than one table (I guess, conceivably)
        return $con->transaction(function () use ($con, $query) {
            return $query->doInsert($con);
        });
    }

} // AssoTvatauxsArchiveTableMap
// This is the static code needed to register the TableMap for this table with the main Propel class.
//
AssoTvatauxsArchiveTableMap::buildTableMap();
