<?php

namespace Map;

use \AssoImportsArchive;
use \AssoImportsArchiveQuery;
use Propel\Runtime\Propel;
use Propel\Runtime\ActiveQuery\Criteria;
use Propel\Runtime\ActiveQuery\InstancePoolTrait;
use Propel\Runtime\Connection\ConnectionInterface;
use Propel\Runtime\DataFetcher\DataFetcherInterface;
use Propel\Runtime\Exception\PropelException;
use Propel\Runtime\Map\RelationMap;
use Propel\Runtime\Map\TableMap;
use Propel\Runtime\Map\TableMapTrait;


/**
 * This class defines the structure of the 'asso_imports_archive' table.
 *
 *
 *
 * This map class is used by Propel to do runtime db structure discovery.
 * For example, the createSelectSql() method checks the type of a given column used in an
 * ORDER BY clause to know whether it needs to apply SQL to make the ORDER BY case-insensitive
 * (i.e. if it's a text column type).
 *
 */
class AssoImportsArchiveTableMap extends TableMap
{
    use InstancePoolTrait;
    use TableMapTrait;

    /**
     * The (dot-path) name of this class
     */
    const CLASS_NAME = '.Map.AssoImportsArchiveTableMap';

    /**
     * The default database name for this class
     */
    const DATABASE_NAME = 'simplasso';

    /**
     * The table name for this class
     */
    const TABLE_NAME = 'asso_imports_archive';

    /**
     * The related Propel class for this table
     */
    const OM_CLASS = '\\AssoImportsArchive';

    /**
     * A class that can be returned by this tableMap
     */
    const CLASS_DEFAULT = 'AssoImportsArchive';

    /**
     * The total number of columns
     */
    const NUM_COLUMNS = 15;

    /**
     * The number of lazy-loaded columns
     */
    const NUM_LAZY_LOAD_COLUMNS = 3;

    /**
     * The number of columns to hydrate (NUM_COLUMNS - NUM_LAZY_LOAD_COLUMNS)
     */
    const NUM_HYDRATE_COLUMNS = 12;

    /**
     * the column name for the id_import field
     */
    const COL_ID_IMPORT = 'asso_imports_archive.id_import';

    /**
     * the column name for the modele field
     */
    const COL_MODELE = 'asso_imports_archive.modele';

    /**
     * the column name for the avancement field
     */
    const COL_AVANCEMENT = 'asso_imports_archive.avancement';

    /**
     * the column name for the nom field
     */
    const COL_NOM = 'asso_imports_archive.nom';

    /**
     * the column name for the nomprovisoire field
     */
    const COL_NOMPROVISOIRE = 'asso_imports_archive.nomprovisoire';

    /**
     * the column name for the taille field
     */
    const COL_TAILLE = 'asso_imports_archive.taille';

    /**
     * the column name for the controlemd5 field
     */
    const COL_CONTROLEMD5 = 'asso_imports_archive.controlemd5';

    /**
     * the column name for the originals field
     */
    const COL_ORIGINALS = 'asso_imports_archive.originals';

    /**
     * the column name for the informations field
     */
    const COL_INFORMATIONS = 'asso_imports_archive.informations';

    /**
     * the column name for the modifications field
     */
    const COL_MODIFICATIONS = 'asso_imports_archive.modifications';

    /**
     * the column name for the nb_ligne field
     */
    const COL_NB_LIGNE = 'asso_imports_archive.nb_ligne';

    /**
     * the column name for the ligne_en_cours field
     */
    const COL_LIGNE_EN_COURS = 'asso_imports_archive.ligne_en_cours';

    /**
     * the column name for the created_at field
     */
    const COL_CREATED_AT = 'asso_imports_archive.created_at';

    /**
     * the column name for the updated_at field
     */
    const COL_UPDATED_AT = 'asso_imports_archive.updated_at';

    /**
     * the column name for the archived_at field
     */
    const COL_ARCHIVED_AT = 'asso_imports_archive.archived_at';

    /**
     * The default string format for model objects of the related table
     */
    const DEFAULT_STRING_FORMAT = 'YAML';

    /**
     * holds an array of fieldnames
     *
     * first dimension keys are the type constants
     * e.g. self::$fieldNames[self::TYPE_PHPNAME][0] = 'Id'
     */
    protected static $fieldNames = array (
        self::TYPE_PHPNAME       => array('IdImport', 'Modele', 'Avancement', 'Nom', 'Nomprovisoire', 'Taille', 'Controlemd5', 'Originals', 'Informations', 'Modifications', 'NbLigne', 'LigneEnCours', 'CreatedAt', 'UpdatedAt', 'ArchivedAt', ),
        self::TYPE_CAMELNAME     => array('idImport', 'modele', 'avancement', 'nom', 'nomprovisoire', 'taille', 'controlemd5', 'originals', 'informations', 'modifications', 'nbLigne', 'ligneEnCours', 'createdAt', 'updatedAt', 'archivedAt', ),
        self::TYPE_COLNAME       => array(AssoImportsArchiveTableMap::COL_ID_IMPORT, AssoImportsArchiveTableMap::COL_MODELE, AssoImportsArchiveTableMap::COL_AVANCEMENT, AssoImportsArchiveTableMap::COL_NOM, AssoImportsArchiveTableMap::COL_NOMPROVISOIRE, AssoImportsArchiveTableMap::COL_TAILLE, AssoImportsArchiveTableMap::COL_CONTROLEMD5, AssoImportsArchiveTableMap::COL_ORIGINALS, AssoImportsArchiveTableMap::COL_INFORMATIONS, AssoImportsArchiveTableMap::COL_MODIFICATIONS, AssoImportsArchiveTableMap::COL_NB_LIGNE, AssoImportsArchiveTableMap::COL_LIGNE_EN_COURS, AssoImportsArchiveTableMap::COL_CREATED_AT, AssoImportsArchiveTableMap::COL_UPDATED_AT, AssoImportsArchiveTableMap::COL_ARCHIVED_AT, ),
        self::TYPE_FIELDNAME     => array('id_import', 'modele', 'avancement', 'nom', 'nomprovisoire', 'taille', 'controlemd5', 'originals', 'informations', 'modifications', 'nb_ligne', 'ligne_en_cours', 'created_at', 'updated_at', 'archived_at', ),
        self::TYPE_NUM           => array(0, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, )
    );

    /**
     * holds an array of keys for quick access to the fieldnames array
     *
     * first dimension keys are the type constants
     * e.g. self::$fieldKeys[self::TYPE_PHPNAME]['Id'] = 0
     */
    protected static $fieldKeys = array (
        self::TYPE_PHPNAME       => array('IdImport' => 0, 'Modele' => 1, 'Avancement' => 2, 'Nom' => 3, 'Nomprovisoire' => 4, 'Taille' => 5, 'Controlemd5' => 6, 'Originals' => 7, 'Informations' => 8, 'Modifications' => 9, 'NbLigne' => 10, 'LigneEnCours' => 11, 'CreatedAt' => 12, 'UpdatedAt' => 13, 'ArchivedAt' => 14, ),
        self::TYPE_CAMELNAME     => array('idImport' => 0, 'modele' => 1, 'avancement' => 2, 'nom' => 3, 'nomprovisoire' => 4, 'taille' => 5, 'controlemd5' => 6, 'originals' => 7, 'informations' => 8, 'modifications' => 9, 'nbLigne' => 10, 'ligneEnCours' => 11, 'createdAt' => 12, 'updatedAt' => 13, 'archivedAt' => 14, ),
        self::TYPE_COLNAME       => array(AssoImportsArchiveTableMap::COL_ID_IMPORT => 0, AssoImportsArchiveTableMap::COL_MODELE => 1, AssoImportsArchiveTableMap::COL_AVANCEMENT => 2, AssoImportsArchiveTableMap::COL_NOM => 3, AssoImportsArchiveTableMap::COL_NOMPROVISOIRE => 4, AssoImportsArchiveTableMap::COL_TAILLE => 5, AssoImportsArchiveTableMap::COL_CONTROLEMD5 => 6, AssoImportsArchiveTableMap::COL_ORIGINALS => 7, AssoImportsArchiveTableMap::COL_INFORMATIONS => 8, AssoImportsArchiveTableMap::COL_MODIFICATIONS => 9, AssoImportsArchiveTableMap::COL_NB_LIGNE => 10, AssoImportsArchiveTableMap::COL_LIGNE_EN_COURS => 11, AssoImportsArchiveTableMap::COL_CREATED_AT => 12, AssoImportsArchiveTableMap::COL_UPDATED_AT => 13, AssoImportsArchiveTableMap::COL_ARCHIVED_AT => 14, ),
        self::TYPE_FIELDNAME     => array('id_import' => 0, 'modele' => 1, 'avancement' => 2, 'nom' => 3, 'nomprovisoire' => 4, 'taille' => 5, 'controlemd5' => 6, 'originals' => 7, 'informations' => 8, 'modifications' => 9, 'nb_ligne' => 10, 'ligne_en_cours' => 11, 'created_at' => 12, 'updated_at' => 13, 'archived_at' => 14, ),
        self::TYPE_NUM           => array(0, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, )
    );

    /**
     * Initialize the table attributes and columns
     * Relations are not initialized by this method since they are lazy loaded
     *
     * @return void
     * @throws PropelException
     */
    public function initialize()
    {
        // attributes
        $this->setName('asso_imports_archive');
        $this->setPhpName('AssoImportsArchive');
        $this->setIdentifierQuoting(true);
        $this->setClassName('\\AssoImportsArchive');
        $this->setPackage('');
        $this->setUseIdGenerator(false);
        // columns
        $this->addPrimaryKey('id_import', 'IdImport', 'BIGINT', true, 21, null);
        $this->addColumn('modele', 'Modele', 'VARCHAR', true, 50, null);
        $this->addColumn('avancement', 'Avancement', 'TINYINT', true, null, 0);
        $this->addColumn('nom', 'Nom', 'VARCHAR', true, 100, null);
        $this->addColumn('nomprovisoire', 'Nomprovisoire', 'VARCHAR', true, 100, null);
        $this->addColumn('taille', 'Taille', 'BIGINT', true, 21, null);
        $this->addColumn('controlemd5', 'Controlemd5', 'VARCHAR', true, 32, null);
        $this->addColumn('originals', 'Originals', 'CLOB', false, null, null);
        $this->addColumn('informations', 'Informations', 'CLOB', false, null, null);
        $this->addColumn('modifications', 'Modifications', 'CLOB', false, null, null);
        $this->addColumn('nb_ligne', 'NbLigne', 'BIGINT', false, null, null);
        $this->addColumn('ligne_en_cours', 'LigneEnCours', 'BIGINT', false, null, 0);
        $this->addColumn('created_at', 'CreatedAt', 'TIMESTAMP', false, null, null);
        $this->addColumn('updated_at', 'UpdatedAt', 'TIMESTAMP', false, null, null);
        $this->addColumn('archived_at', 'ArchivedAt', 'TIMESTAMP', false, null, null);
    } // initialize()

    /**
     * Build the RelationMap objects for this table relationships
     */
    public function buildRelations()
    {
    } // buildRelations()

    /**
     * Retrieves a string version of the primary key from the DB resultset row that can be used to uniquely identify a row in this table.
     *
     * For tables with a single-column primary key, that simple pkey value will be returned.  For tables with
     * a multi-column primary key, a serialize()d version of the primary key will be returned.
     *
     * @param array  $row       resultset row.
     * @param int    $offset    The 0-based offset for reading from the resultset row.
     * @param string $indexType One of the class type constants TableMap::TYPE_PHPNAME, TableMap::TYPE_CAMELNAME
     *                           TableMap::TYPE_COLNAME, TableMap::TYPE_FIELDNAME, TableMap::TYPE_NUM
     *
     * @return string The primary key hash of the row
     */
    public static function getPrimaryKeyHashFromRow($row, $offset = 0, $indexType = TableMap::TYPE_NUM)
    {
        // If the PK cannot be derived from the row, return NULL.
        if ($row[TableMap::TYPE_NUM == $indexType ? 0 + $offset : static::translateFieldName('IdImport', TableMap::TYPE_PHPNAME, $indexType)] === null) {
            return null;
        }

        return null === $row[TableMap::TYPE_NUM == $indexType ? 0 + $offset : static::translateFieldName('IdImport', TableMap::TYPE_PHPNAME, $indexType)] || is_scalar($row[TableMap::TYPE_NUM == $indexType ? 0 + $offset : static::translateFieldName('IdImport', TableMap::TYPE_PHPNAME, $indexType)]) || is_callable([$row[TableMap::TYPE_NUM == $indexType ? 0 + $offset : static::translateFieldName('IdImport', TableMap::TYPE_PHPNAME, $indexType)], '__toString']) ? (string) $row[TableMap::TYPE_NUM == $indexType ? 0 + $offset : static::translateFieldName('IdImport', TableMap::TYPE_PHPNAME, $indexType)] : $row[TableMap::TYPE_NUM == $indexType ? 0 + $offset : static::translateFieldName('IdImport', TableMap::TYPE_PHPNAME, $indexType)];
    }

    /**
     * Retrieves the primary key from the DB resultset row
     * For tables with a single-column primary key, that simple pkey value will be returned.  For tables with
     * a multi-column primary key, an array of the primary key columns will be returned.
     *
     * @param array  $row       resultset row.
     * @param int    $offset    The 0-based offset for reading from the resultset row.
     * @param string $indexType One of the class type constants TableMap::TYPE_PHPNAME, TableMap::TYPE_CAMELNAME
     *                           TableMap::TYPE_COLNAME, TableMap::TYPE_FIELDNAME, TableMap::TYPE_NUM
     *
     * @return mixed The primary key of the row
     */
    public static function getPrimaryKeyFromRow($row, $offset = 0, $indexType = TableMap::TYPE_NUM)
    {
        return (string) $row[
            $indexType == TableMap::TYPE_NUM
                ? 0 + $offset
                : self::translateFieldName('IdImport', TableMap::TYPE_PHPNAME, $indexType)
        ];
    }

    /**
     * The class that the tableMap will make instances of.
     *
     * If $withPrefix is true, the returned path
     * uses a dot-path notation which is translated into a path
     * relative to a location on the PHP include_path.
     * (e.g. path.to.MyClass -> 'path/to/MyClass.php')
     *
     * @param boolean $withPrefix Whether or not to return the path with the class name
     * @return string path.to.ClassName
     */
    public static function getOMClass($withPrefix = true)
    {
        return $withPrefix ? AssoImportsArchiveTableMap::CLASS_DEFAULT : AssoImportsArchiveTableMap::OM_CLASS;
    }

    /**
     * Populates an object of the default type or an object that inherit from the default.
     *
     * @param array  $row       row returned by DataFetcher->fetch().
     * @param int    $offset    The 0-based offset for reading from the resultset row.
     * @param string $indexType The index type of $row. Mostly DataFetcher->getIndexType().
                                 One of the class type constants TableMap::TYPE_PHPNAME, TableMap::TYPE_CAMELNAME
     *                           TableMap::TYPE_COLNAME, TableMap::TYPE_FIELDNAME, TableMap::TYPE_NUM.
     *
     * @throws PropelException Any exceptions caught during processing will be
     *                         rethrown wrapped into a PropelException.
     * @return array           (AssoImportsArchive object, last column rank)
     */
    public static function populateObject($row, $offset = 0, $indexType = TableMap::TYPE_NUM)
    {
        $key = AssoImportsArchiveTableMap::getPrimaryKeyHashFromRow($row, $offset, $indexType);
        if (null !== ($obj = AssoImportsArchiveTableMap::getInstanceFromPool($key))) {
            // We no longer rehydrate the object, since this can cause data loss.
            // See http://www.propelorm.org/ticket/509
            // $obj->hydrate($row, $offset, true); // rehydrate
            $col = $offset + AssoImportsArchiveTableMap::NUM_HYDRATE_COLUMNS;
        } else {
            $cls = AssoImportsArchiveTableMap::OM_CLASS;
            /** @var AssoImportsArchive $obj */
            $obj = new $cls();
            $col = $obj->hydrate($row, $offset, false, $indexType);
            AssoImportsArchiveTableMap::addInstanceToPool($obj, $key);
        }

        return array($obj, $col);
    }

    /**
     * The returned array will contain objects of the default type or
     * objects that inherit from the default.
     *
     * @param DataFetcherInterface $dataFetcher
     * @return array
     * @throws PropelException Any exceptions caught during processing will be
     *                         rethrown wrapped into a PropelException.
     */
    public static function populateObjects(DataFetcherInterface $dataFetcher)
    {
        $results = array();

        // set the class once to avoid overhead in the loop
        $cls = static::getOMClass(false);
        // populate the object(s)
        while ($row = $dataFetcher->fetch()) {
            $key = AssoImportsArchiveTableMap::getPrimaryKeyHashFromRow($row, 0, $dataFetcher->getIndexType());
            if (null !== ($obj = AssoImportsArchiveTableMap::getInstanceFromPool($key))) {
                // We no longer rehydrate the object, since this can cause data loss.
                // See http://www.propelorm.org/ticket/509
                // $obj->hydrate($row, 0, true); // rehydrate
                $results[] = $obj;
            } else {
                /** @var AssoImportsArchive $obj */
                $obj = new $cls();
                $obj->hydrate($row);
                $results[] = $obj;
                AssoImportsArchiveTableMap::addInstanceToPool($obj, $key);
            } // if key exists
        }

        return $results;
    }
    /**
     * Add all the columns needed to create a new object.
     *
     * Note: any columns that were marked with lazyLoad="true" in the
     * XML schema will not be added to the select list and only loaded
     * on demand.
     *
     * @param Criteria $criteria object containing the columns to add.
     * @param string   $alias    optional table alias
     * @throws PropelException Any exceptions caught during processing will be
     *                         rethrown wrapped into a PropelException.
     */
    public static function addSelectColumns(Criteria $criteria, $alias = null)
    {
        if (null === $alias) {
            $criteria->addSelectColumn(AssoImportsArchiveTableMap::COL_ID_IMPORT);
            $criteria->addSelectColumn(AssoImportsArchiveTableMap::COL_MODELE);
            $criteria->addSelectColumn(AssoImportsArchiveTableMap::COL_AVANCEMENT);
            $criteria->addSelectColumn(AssoImportsArchiveTableMap::COL_NOM);
            $criteria->addSelectColumn(AssoImportsArchiveTableMap::COL_NOMPROVISOIRE);
            $criteria->addSelectColumn(AssoImportsArchiveTableMap::COL_TAILLE);
            $criteria->addSelectColumn(AssoImportsArchiveTableMap::COL_CONTROLEMD5);
            $criteria->addSelectColumn(AssoImportsArchiveTableMap::COL_NB_LIGNE);
            $criteria->addSelectColumn(AssoImportsArchiveTableMap::COL_LIGNE_EN_COURS);
            $criteria->addSelectColumn(AssoImportsArchiveTableMap::COL_CREATED_AT);
            $criteria->addSelectColumn(AssoImportsArchiveTableMap::COL_UPDATED_AT);
            $criteria->addSelectColumn(AssoImportsArchiveTableMap::COL_ARCHIVED_AT);
        } else {
            $criteria->addSelectColumn($alias . '.id_import');
            $criteria->addSelectColumn($alias . '.modele');
            $criteria->addSelectColumn($alias . '.avancement');
            $criteria->addSelectColumn($alias . '.nom');
            $criteria->addSelectColumn($alias . '.nomprovisoire');
            $criteria->addSelectColumn($alias . '.taille');
            $criteria->addSelectColumn($alias . '.controlemd5');
            $criteria->addSelectColumn($alias . '.nb_ligne');
            $criteria->addSelectColumn($alias . '.ligne_en_cours');
            $criteria->addSelectColumn($alias . '.created_at');
            $criteria->addSelectColumn($alias . '.updated_at');
            $criteria->addSelectColumn($alias . '.archived_at');
        }
    }

    /**
     * Returns the TableMap related to this object.
     * This method is not needed for general use but a specific application could have a need.
     * @return TableMap
     * @throws PropelException Any exceptions caught during processing will be
     *                         rethrown wrapped into a PropelException.
     */
    public static function getTableMap()
    {
        return Propel::getServiceContainer()->getDatabaseMap(AssoImportsArchiveTableMap::DATABASE_NAME)->getTable(AssoImportsArchiveTableMap::TABLE_NAME);
    }

    /**
     * Add a TableMap instance to the database for this tableMap class.
     */
    public static function buildTableMap()
    {
        $dbMap = Propel::getServiceContainer()->getDatabaseMap(AssoImportsArchiveTableMap::DATABASE_NAME);
        if (!$dbMap->hasTable(AssoImportsArchiveTableMap::TABLE_NAME)) {
            $dbMap->addTableObject(new AssoImportsArchiveTableMap());
        }
    }

    /**
     * Performs a DELETE on the database, given a AssoImportsArchive or Criteria object OR a primary key value.
     *
     * @param mixed               $values Criteria or AssoImportsArchive object or primary key or array of primary keys
     *              which is used to create the DELETE statement
     * @param  ConnectionInterface $con the connection to use
     * @return int             The number of affected rows (if supported by underlying database driver).  This includes CASCADE-related rows
     *                         if supported by native driver or if emulated using Propel.
     * @throws PropelException Any exceptions caught during processing will be
     *                         rethrown wrapped into a PropelException.
     */
     public static function doDelete($values, ConnectionInterface $con = null)
     {
        if (null === $con) {
            $con = Propel::getServiceContainer()->getWriteConnection(AssoImportsArchiveTableMap::DATABASE_NAME);
        }

        if ($values instanceof Criteria) {
            // rename for clarity
            $criteria = $values;
        } elseif ($values instanceof \AssoImportsArchive) { // it's a model object
            // create criteria based on pk values
            $criteria = $values->buildPkeyCriteria();
        } else { // it's a primary key, or an array of pks
            $criteria = new Criteria(AssoImportsArchiveTableMap::DATABASE_NAME);
            $criteria->add(AssoImportsArchiveTableMap::COL_ID_IMPORT, (array) $values, Criteria::IN);
        }

        $query = AssoImportsArchiveQuery::create()->mergeWith($criteria);

        if ($values instanceof Criteria) {
            AssoImportsArchiveTableMap::clearInstancePool();
        } elseif (!is_object($values)) { // it's a primary key, or an array of pks
            foreach ((array) $values as $singleval) {
                AssoImportsArchiveTableMap::removeInstanceFromPool($singleval);
            }
        }

        return $query->delete($con);
    }

    /**
     * Deletes all rows from the asso_imports_archive table.
     *
     * @param ConnectionInterface $con the connection to use
     * @return int The number of affected rows (if supported by underlying database driver).
     */
    public static function doDeleteAll(ConnectionInterface $con = null)
    {
        return AssoImportsArchiveQuery::create()->doDeleteAll($con);
    }

    /**
     * Performs an INSERT on the database, given a AssoImportsArchive or Criteria object.
     *
     * @param mixed               $criteria Criteria or AssoImportsArchive object containing data that is used to create the INSERT statement.
     * @param ConnectionInterface $con the ConnectionInterface connection to use
     * @return mixed           The new primary key.
     * @throws PropelException Any exceptions caught during processing will be
     *                         rethrown wrapped into a PropelException.
     */
    public static function doInsert($criteria, ConnectionInterface $con = null)
    {
        if (null === $con) {
            $con = Propel::getServiceContainer()->getWriteConnection(AssoImportsArchiveTableMap::DATABASE_NAME);
        }

        if ($criteria instanceof Criteria) {
            $criteria = clone $criteria; // rename for clarity
        } else {
            $criteria = $criteria->buildCriteria(); // build Criteria from AssoImportsArchive object
        }


        // Set the correct dbName
        $query = AssoImportsArchiveQuery::create()->mergeWith($criteria);

        // use transaction because $criteria could contain info
        // for more than one table (I guess, conceivably)
        return $con->transaction(function () use ($con, $query) {
            return $query->doInsert($con);
        });
    }

} // AssoImportsArchiveTableMap
// This is the static code needed to register the TableMap for this table with the main Propel class.
//
AssoImportsArchiveTableMap::buildTableMap();
