<?php

namespace Map;

use \Position;
use \PositionQuery;
use Propel\Runtime\Propel;
use Propel\Runtime\ActiveQuery\Criteria;
use Propel\Runtime\ActiveQuery\InstancePoolTrait;
use Propel\Runtime\Connection\ConnectionInterface;
use Propel\Runtime\DataFetcher\DataFetcherInterface;
use Propel\Runtime\Exception\PropelException;
use Propel\Runtime\Map\RelationMap;
use Propel\Runtime\Map\TableMap;
use Propel\Runtime\Map\TableMapTrait;


/**
 * This class defines the structure of the 'geo_positions' table.
 *
 *
 *
 * This map class is used by Propel to do runtime db structure discovery.
 * For example, the createSelectSql() method checks the type of a given column used in an
 * ORDER BY clause to know whether it needs to apply SQL to make the ORDER BY case-insensitive
 * (i.e. if it's a text column type).
 *
 */
class PositionTableMap extends TableMap
{
    use InstancePoolTrait;
    use TableMapTrait;

    /**
     * The (dot-path) name of this class
     */
    const CLASS_NAME = '.Map.PositionTableMap';

    /**
     * The default database name for this class
     */
    const DATABASE_NAME = 'simplasso';

    /**
     * The table name for this class
     */
    const TABLE_NAME = 'geo_positions';

    /**
     * The related Propel class for this table
     */
    const OM_CLASS = '\\Position';

    /**
     * A class that can be returned by this tableMap
     */
    const CLASS_DEFAULT = 'Position';

    /**
     * The total number of columns
     */
    const NUM_COLUMNS = 8;

    /**
     * The number of lazy-loaded columns
     */
    const NUM_LAZY_LOAD_COLUMNS = 0;

    /**
     * The number of columns to hydrate (NUM_COLUMNS - NUM_LAZY_LOAD_COLUMNS)
     */
    const NUM_HYDRATE_COLUMNS = 8;

    /**
     * the column name for the id_position field
     */
    const COL_ID_POSITION = 'geo_positions.id_position';

    /**
     * the column name for the titre field
     */
    const COL_TITRE = 'geo_positions.titre';

    /**
     * the column name for the descriptif field
     */
    const COL_DESCRIPTIF = 'geo_positions.descriptif';

    /**
     * the column name for the lat field
     */
    const COL_LAT = 'geo_positions.lat';

    /**
     * the column name for the lon field
     */
    const COL_LON = 'geo_positions.lon';

    /**
     * the column name for the zoom field
     */
    const COL_ZOOM = 'geo_positions.zoom';

    /**
     * the column name for the created_at field
     */
    const COL_CREATED_AT = 'geo_positions.created_at';

    /**
     * the column name for the updated_at field
     */
    const COL_UPDATED_AT = 'geo_positions.updated_at';

    /**
     * The default string format for model objects of the related table
     */
    const DEFAULT_STRING_FORMAT = 'YAML';

    /**
     * holds an array of fieldnames
     *
     * first dimension keys are the type constants
     * e.g. self::$fieldNames[self::TYPE_PHPNAME][0] = 'Id'
     */
    protected static $fieldNames = array (
        self::TYPE_PHPNAME       => array('IdPosition', 'Titre', 'Descriptif', 'Lat', 'Lon', 'Zoom', 'CreatedAt', 'UpdatedAt', ),
        self::TYPE_CAMELNAME     => array('idPosition', 'titre', 'descriptif', 'lat', 'lon', 'zoom', 'createdAt', 'updatedAt', ),
        self::TYPE_COLNAME       => array(PositionTableMap::COL_ID_POSITION, PositionTableMap::COL_TITRE, PositionTableMap::COL_DESCRIPTIF, PositionTableMap::COL_LAT, PositionTableMap::COL_LON, PositionTableMap::COL_ZOOM, PositionTableMap::COL_CREATED_AT, PositionTableMap::COL_UPDATED_AT, ),
        self::TYPE_FIELDNAME     => array('id_position', 'titre', 'descriptif', 'lat', 'lon', 'zoom', 'created_at', 'updated_at', ),
        self::TYPE_NUM           => array(0, 1, 2, 3, 4, 5, 6, 7, )
    );

    /**
     * holds an array of keys for quick access to the fieldnames array
     *
     * first dimension keys are the type constants
     * e.g. self::$fieldKeys[self::TYPE_PHPNAME]['Id'] = 0
     */
    protected static $fieldKeys = array (
        self::TYPE_PHPNAME       => array('IdPosition' => 0, 'Titre' => 1, 'Descriptif' => 2, 'Lat' => 3, 'Lon' => 4, 'Zoom' => 5, 'CreatedAt' => 6, 'UpdatedAt' => 7, ),
        self::TYPE_CAMELNAME     => array('idPosition' => 0, 'titre' => 1, 'descriptif' => 2, 'lat' => 3, 'lon' => 4, 'zoom' => 5, 'createdAt' => 6, 'updatedAt' => 7, ),
        self::TYPE_COLNAME       => array(PositionTableMap::COL_ID_POSITION => 0, PositionTableMap::COL_TITRE => 1, PositionTableMap::COL_DESCRIPTIF => 2, PositionTableMap::COL_LAT => 3, PositionTableMap::COL_LON => 4, PositionTableMap::COL_ZOOM => 5, PositionTableMap::COL_CREATED_AT => 6, PositionTableMap::COL_UPDATED_AT => 7, ),
        self::TYPE_FIELDNAME     => array('id_position' => 0, 'titre' => 1, 'descriptif' => 2, 'lat' => 3, 'lon' => 4, 'zoom' => 5, 'created_at' => 6, 'updated_at' => 7, ),
        self::TYPE_NUM           => array(0, 1, 2, 3, 4, 5, 6, 7, )
    );

    /**
     * Initialize the table attributes and columns
     * Relations are not initialized by this method since they are lazy loaded
     *
     * @return void
     * @throws PropelException
     */
    public function initialize()
    {
        // attributes
        $this->setName('geo_positions');
        $this->setPhpName('Position');
        $this->setIdentifierQuoting(true);
        $this->setClassName('\\Position');
        $this->setPackage('');
        $this->setUseIdGenerator(true);
        // columns
        $this->addPrimaryKey('id_position', 'IdPosition', 'BIGINT', true, 21, null);
        $this->addColumn('titre', 'Titre', 'LONGVARCHAR', false, null, null);
        $this->addColumn('descriptif', 'Descriptif', 'LONGVARCHAR', false, null, null);
        $this->addColumn('lat', 'Lat', 'DOUBLE', true, null, null);
        $this->addColumn('lon', 'Lon', 'DOUBLE', true, null, null);
        $this->addColumn('zoom', 'Zoom', 'TINYINT', false, null, null);
        $this->addColumn('created_at', 'CreatedAt', 'TIMESTAMP', false, null, null);
        $this->addColumn('updated_at', 'UpdatedAt', 'TIMESTAMP', false, null, null);
    } // initialize()

    /**
     * Build the RelationMap objects for this table relationships
     */
    public function buildRelations()
    {
        $this->addRelation('PositionLien', '\\PositionLien', RelationMap::ONE_TO_MANY, array (
  0 =>
  array (
    0 => ':id_position',
    1 => ':id_position',
  ),
), 'CASCADE', null, 'PositionLiens', false);
    } // buildRelations()

    /**
     *
     * Gets the list of behaviors registered for this table
     *
     * @return array Associative array (name => parameters) of behaviors
     */
    public function getBehaviors()
    {
        return array(
            'timestampable' => array('create_column' => 'created_at', 'update_column' => 'updated_at', 'disable_created_at' => 'false', 'disable_updated_at' => 'false', ),
        );
    } // getBehaviors()
    /**
     * Method to invalidate the instance pool of all tables related to geo_positions     * by a foreign key with ON DELETE CASCADE
     */
    public static function clearRelatedInstancePool()
    {
        // Invalidate objects in related instance pools,
        // since one or more of them may be deleted by ON DELETE CASCADE/SETNULL rule.
        PositionLienTableMap::clearInstancePool();
    }

    /**
     * Retrieves a string version of the primary key from the DB resultset row that can be used to uniquely identify a row in this table.
     *
     * For tables with a single-column primary key, that simple pkey value will be returned.  For tables with
     * a multi-column primary key, a serialize()d version of the primary key will be returned.
     *
     * @param array  $row       resultset row.
     * @param int    $offset    The 0-based offset for reading from the resultset row.
     * @param string $indexType One of the class type constants TableMap::TYPE_PHPNAME, TableMap::TYPE_CAMELNAME
     *                           TableMap::TYPE_COLNAME, TableMap::TYPE_FIELDNAME, TableMap::TYPE_NUM
     *
     * @return string The primary key hash of the row
     */
    public static function getPrimaryKeyHashFromRow($row, $offset = 0, $indexType = TableMap::TYPE_NUM)
    {
        // If the PK cannot be derived from the row, return NULL.
        if ($row[TableMap::TYPE_NUM == $indexType ? 0 + $offset : static::translateFieldName('IdPosition', TableMap::TYPE_PHPNAME, $indexType)] === null) {
            return null;
        }

        return null === $row[TableMap::TYPE_NUM == $indexType ? 0 + $offset : static::translateFieldName('IdPosition', TableMap::TYPE_PHPNAME, $indexType)] || is_scalar($row[TableMap::TYPE_NUM == $indexType ? 0 + $offset : static::translateFieldName('IdPosition', TableMap::TYPE_PHPNAME, $indexType)]) || is_callable([$row[TableMap::TYPE_NUM == $indexType ? 0 + $offset : static::translateFieldName('IdPosition', TableMap::TYPE_PHPNAME, $indexType)], '__toString']) ? (string) $row[TableMap::TYPE_NUM == $indexType ? 0 + $offset : static::translateFieldName('IdPosition', TableMap::TYPE_PHPNAME, $indexType)] : $row[TableMap::TYPE_NUM == $indexType ? 0 + $offset : static::translateFieldName('IdPosition', TableMap::TYPE_PHPNAME, $indexType)];
    }

    /**
     * Retrieves the primary key from the DB resultset row
     * For tables with a single-column primary key, that simple pkey value will be returned.  For tables with
     * a multi-column primary key, an array of the primary key columns will be returned.
     *
     * @param array  $row       resultset row.
     * @param int    $offset    The 0-based offset for reading from the resultset row.
     * @param string $indexType One of the class type constants TableMap::TYPE_PHPNAME, TableMap::TYPE_CAMELNAME
     *                           TableMap::TYPE_COLNAME, TableMap::TYPE_FIELDNAME, TableMap::TYPE_NUM
     *
     * @return mixed The primary key of the row
     */
    public static function getPrimaryKeyFromRow($row, $offset = 0, $indexType = TableMap::TYPE_NUM)
    {
        return (string) $row[
            $indexType == TableMap::TYPE_NUM
                ? 0 + $offset
                : self::translateFieldName('IdPosition', TableMap::TYPE_PHPNAME, $indexType)
        ];
    }

    /**
     * The class that the tableMap will make instances of.
     *
     * If $withPrefix is true, the returned path
     * uses a dot-path notation which is translated into a path
     * relative to a location on the PHP include_path.
     * (e.g. path.to.MyClass -> 'path/to/MyClass.php')
     *
     * @param boolean $withPrefix Whether or not to return the path with the class name
     * @return string path.to.ClassName
     */
    public static function getOMClass($withPrefix = true)
    {
        return $withPrefix ? PositionTableMap::CLASS_DEFAULT : PositionTableMap::OM_CLASS;
    }

    /**
     * Populates an object of the default type or an object that inherit from the default.
     *
     * @param array  $row       row returned by DataFetcher->fetch().
     * @param int    $offset    The 0-based offset for reading from the resultset row.
     * @param string $indexType The index type of $row. Mostly DataFetcher->getIndexType().
                                 One of the class type constants TableMap::TYPE_PHPNAME, TableMap::TYPE_CAMELNAME
     *                           TableMap::TYPE_COLNAME, TableMap::TYPE_FIELDNAME, TableMap::TYPE_NUM.
     *
     * @throws PropelException Any exceptions caught during processing will be
     *                         rethrown wrapped into a PropelException.
     * @return array           (Position object, last column rank)
     */
    public static function populateObject($row, $offset = 0, $indexType = TableMap::TYPE_NUM)
    {
        $key = PositionTableMap::getPrimaryKeyHashFromRow($row, $offset, $indexType);
        if (null !== ($obj = PositionTableMap::getInstanceFromPool($key))) {
            // We no longer rehydrate the object, since this can cause data loss.
            // See http://www.propelorm.org/ticket/509
            // $obj->hydrate($row, $offset, true); // rehydrate
            $col = $offset + PositionTableMap::NUM_HYDRATE_COLUMNS;
        } else {
            $cls = PositionTableMap::OM_CLASS;
            /** @var Position $obj */
            $obj = new $cls();
            $col = $obj->hydrate($row, $offset, false, $indexType);
            PositionTableMap::addInstanceToPool($obj, $key);
        }

        return array($obj, $col);
    }

    /**
     * The returned array will contain objects of the default type or
     * objects that inherit from the default.
     *
     * @param DataFetcherInterface $dataFetcher
     * @return array
     * @throws PropelException Any exceptions caught during processing will be
     *                         rethrown wrapped into a PropelException.
     */
    public static function populateObjects(DataFetcherInterface $dataFetcher)
    {
        $results = array();

        // set the class once to avoid overhead in the loop
        $cls = static::getOMClass(false);
        // populate the object(s)
        while ($row = $dataFetcher->fetch()) {
            $key = PositionTableMap::getPrimaryKeyHashFromRow($row, 0, $dataFetcher->getIndexType());
            if (null !== ($obj = PositionTableMap::getInstanceFromPool($key))) {
                // We no longer rehydrate the object, since this can cause data loss.
                // See http://www.propelorm.org/ticket/509
                // $obj->hydrate($row, 0, true); // rehydrate
                $results[] = $obj;
            } else {
                /** @var Position $obj */
                $obj = new $cls();
                $obj->hydrate($row);
                $results[] = $obj;
                PositionTableMap::addInstanceToPool($obj, $key);
            } // if key exists
        }

        return $results;
    }
    /**
     * Add all the columns needed to create a new object.
     *
     * Note: any columns that were marked with lazyLoad="true" in the
     * XML schema will not be added to the select list and only loaded
     * on demand.
     *
     * @param Criteria $criteria object containing the columns to add.
     * @param string   $alias    optional table alias
     * @throws PropelException Any exceptions caught during processing will be
     *                         rethrown wrapped into a PropelException.
     */
    public static function addSelectColumns(Criteria $criteria, $alias = null)
    {
        if (null === $alias) {
            $criteria->addSelectColumn(PositionTableMap::COL_ID_POSITION);
            $criteria->addSelectColumn(PositionTableMap::COL_TITRE);
            $criteria->addSelectColumn(PositionTableMap::COL_DESCRIPTIF);
            $criteria->addSelectColumn(PositionTableMap::COL_LAT);
            $criteria->addSelectColumn(PositionTableMap::COL_LON);
            $criteria->addSelectColumn(PositionTableMap::COL_ZOOM);
            $criteria->addSelectColumn(PositionTableMap::COL_CREATED_AT);
            $criteria->addSelectColumn(PositionTableMap::COL_UPDATED_AT);
        } else {
            $criteria->addSelectColumn($alias . '.id_position');
            $criteria->addSelectColumn($alias . '.titre');
            $criteria->addSelectColumn($alias . '.descriptif');
            $criteria->addSelectColumn($alias . '.lat');
            $criteria->addSelectColumn($alias . '.lon');
            $criteria->addSelectColumn($alias . '.zoom');
            $criteria->addSelectColumn($alias . '.created_at');
            $criteria->addSelectColumn($alias . '.updated_at');
        }
    }

    /**
     * Returns the TableMap related to this object.
     * This method is not needed for general use but a specific application could have a need.
     * @return TableMap
     * @throws PropelException Any exceptions caught during processing will be
     *                         rethrown wrapped into a PropelException.
     */
    public static function getTableMap()
    {
        return Propel::getServiceContainer()->getDatabaseMap(PositionTableMap::DATABASE_NAME)->getTable(PositionTableMap::TABLE_NAME);
    }

    /**
     * Add a TableMap instance to the database for this tableMap class.
     */
    public static function buildTableMap()
    {
        $dbMap = Propel::getServiceContainer()->getDatabaseMap(PositionTableMap::DATABASE_NAME);
        if (!$dbMap->hasTable(PositionTableMap::TABLE_NAME)) {
            $dbMap->addTableObject(new PositionTableMap());
        }
    }

    /**
     * Performs a DELETE on the database, given a Position or Criteria object OR a primary key value.
     *
     * @param mixed               $values Criteria or Position object or primary key or array of primary keys
     *              which is used to create the DELETE statement
     * @param  ConnectionInterface $con the connection to use
     * @return int             The number of affected rows (if supported by underlying database driver).  This includes CASCADE-related rows
     *                         if supported by native driver or if emulated using Propel.
     * @throws PropelException Any exceptions caught during processing will be
     *                         rethrown wrapped into a PropelException.
     */
     public static function doDelete($values, ConnectionInterface $con = null)
     {
        if (null === $con) {
            $con = Propel::getServiceContainer()->getWriteConnection(PositionTableMap::DATABASE_NAME);
        }

        if ($values instanceof Criteria) {
            // rename for clarity
            $criteria = $values;
        } elseif ($values instanceof \Position) { // it's a model object
            // create criteria based on pk values
            $criteria = $values->buildPkeyCriteria();
        } else { // it's a primary key, or an array of pks
            $criteria = new Criteria(PositionTableMap::DATABASE_NAME);
            $criteria->add(PositionTableMap::COL_ID_POSITION, (array) $values, Criteria::IN);
        }

        $query = PositionQuery::create()->mergeWith($criteria);

        if ($values instanceof Criteria) {
            PositionTableMap::clearInstancePool();
        } elseif (!is_object($values)) { // it's a primary key, or an array of pks
            foreach ((array) $values as $singleval) {
                PositionTableMap::removeInstanceFromPool($singleval);
            }
        }

        return $query->delete($con);
    }

    /**
     * Deletes all rows from the geo_positions table.
     *
     * @param ConnectionInterface $con the connection to use
     * @return int The number of affected rows (if supported by underlying database driver).
     */
    public static function doDeleteAll(ConnectionInterface $con = null)
    {
        return PositionQuery::create()->doDeleteAll($con);
    }

    /**
     * Performs an INSERT on the database, given a Position or Criteria object.
     *
     * @param mixed               $criteria Criteria or Position object containing data that is used to create the INSERT statement.
     * @param ConnectionInterface $con the ConnectionInterface connection to use
     * @return mixed           The new primary key.
     * @throws PropelException Any exceptions caught during processing will be
     *                         rethrown wrapped into a PropelException.
     */
    public static function doInsert($criteria, ConnectionInterface $con = null)
    {
        if (null === $con) {
            $con = Propel::getServiceContainer()->getWriteConnection(PositionTableMap::DATABASE_NAME);
        }

        if ($criteria instanceof Criteria) {
            $criteria = clone $criteria; // rename for clarity
        } else {
            $criteria = $criteria->buildCriteria(); // build Criteria from Position object
        }

        if ($criteria->containsKey(PositionTableMap::COL_ID_POSITION) && $criteria->keyContainsValue(PositionTableMap::COL_ID_POSITION) ) {
            throw new PropelException('Cannot insert a value for auto-increment primary key ('.PositionTableMap::COL_ID_POSITION.')');
        }


        // Set the correct dbName
        $query = PositionQuery::create()->mergeWith($criteria);

        // use transaction because $criteria could contain info
        // for more than one table (I guess, conceivably)
        return $con->transaction(function () use ($con, $query) {
            return $query->doInsert($con);
        });
    }

} // PositionTableMap
// This is the static code needed to register the TableMap for this table with the main Propel class.
//
PositionTableMap::buildTableMap();
