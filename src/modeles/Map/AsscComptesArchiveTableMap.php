<?php

namespace Map;

use \AsscComptesArchive;
use \AsscComptesArchiveQuery;
use Propel\Runtime\Propel;
use Propel\Runtime\ActiveQuery\Criteria;
use Propel\Runtime\ActiveQuery\InstancePoolTrait;
use Propel\Runtime\Connection\ConnectionInterface;
use Propel\Runtime\DataFetcher\DataFetcherInterface;
use Propel\Runtime\Exception\PropelException;
use Propel\Runtime\Map\RelationMap;
use Propel\Runtime\Map\TableMap;
use Propel\Runtime\Map\TableMapTrait;


/**
 * This class defines the structure of the 'assc_comptes_archive' table.
 *
 *
 *
 * This map class is used by Propel to do runtime db structure discovery.
 * For example, the createSelectSql() method checks the type of a given column used in an
 * ORDER BY clause to know whether it needs to apply SQL to make the ORDER BY case-insensitive
 * (i.e. if it's a text column type).
 *
 */
class AsscComptesArchiveTableMap extends TableMap
{
    use InstancePoolTrait;
    use TableMapTrait;

    /**
     * The (dot-path) name of this class
     */
    const CLASS_NAME = '.Map.AsscComptesArchiveTableMap';

    /**
     * The default database name for this class
     */
    const DATABASE_NAME = 'simplasso';

    /**
     * The table name for this class
     */
    const TABLE_NAME = 'assc_comptes_archive';

    /**
     * The related Propel class for this table
     */
    const OM_CLASS = '\\AsscComptesArchive';

    /**
     * A class that can be returned by this tableMap
     */
    const CLASS_DEFAULT = 'AsscComptesArchive';

    /**
     * The total number of columns
     */
    const NUM_COLUMNS = 13;

    /**
     * The number of lazy-loaded columns
     */
    const NUM_LAZY_LOAD_COLUMNS = 0;

    /**
     * The number of columns to hydrate (NUM_COLUMNS - NUM_LAZY_LOAD_COLUMNS)
     */
    const NUM_HYDRATE_COLUMNS = 13;

    /**
     * the column name for the id_compte field
     */
    const COL_ID_COMPTE = 'assc_comptes_archive.id_compte';

    /**
     * the column name for the ncompte field
     */
    const COL_NCOMPTE = 'assc_comptes_archive.ncompte';

    /**
     * the column name for the nom field
     */
    const COL_NOM = 'assc_comptes_archive.nom';

    /**
     * the column name for the nomcourt field
     */
    const COL_NOMCOURT = 'assc_comptes_archive.nomcourt';

    /**
     * the column name for the id_poste field
     */
    const COL_ID_POSTE = 'assc_comptes_archive.id_poste';

    /**
     * the column name for the type_op field
     */
    const COL_TYPE_OP = 'assc_comptes_archive.type_op';

    /**
     * the column name for the solde_anterieur field
     */
    const COL_SOLDE_ANTERIEUR = 'assc_comptes_archive.solde_anterieur';

    /**
     * the column name for the date_anterieure field
     */
    const COL_DATE_ANTERIEURE = 'assc_comptes_archive.date_anterieure';

    /**
     * the column name for the id_entite field
     */
    const COL_ID_ENTITE = 'assc_comptes_archive.id_entite';

    /**
     * the column name for the observation field
     */
    const COL_OBSERVATION = 'assc_comptes_archive.observation';

    /**
     * the column name for the created_at field
     */
    const COL_CREATED_AT = 'assc_comptes_archive.created_at';

    /**
     * the column name for the updated_at field
     */
    const COL_UPDATED_AT = 'assc_comptes_archive.updated_at';

    /**
     * the column name for the archived_at field
     */
    const COL_ARCHIVED_AT = 'assc_comptes_archive.archived_at';

    /**
     * The default string format for model objects of the related table
     */
    const DEFAULT_STRING_FORMAT = 'YAML';

    /**
     * holds an array of fieldnames
     *
     * first dimension keys are the type constants
     * e.g. self::$fieldNames[self::TYPE_PHPNAME][0] = 'Id'
     */
    protected static $fieldNames = array (
        self::TYPE_PHPNAME       => array('IdCompte', 'Ncompte', 'Nom', 'Nomcourt', 'IdPoste', 'TypeOp', 'SoldeAnterieur', 'DateAnterieure', 'IdEntite', 'Observation', 'CreatedAt', 'UpdatedAt', 'ArchivedAt', ),
        self::TYPE_CAMELNAME     => array('idCompte', 'ncompte', 'nom', 'nomcourt', 'idPoste', 'typeOp', 'soldeAnterieur', 'dateAnterieure', 'idEntite', 'observation', 'createdAt', 'updatedAt', 'archivedAt', ),
        self::TYPE_COLNAME       => array(AsscComptesArchiveTableMap::COL_ID_COMPTE, AsscComptesArchiveTableMap::COL_NCOMPTE, AsscComptesArchiveTableMap::COL_NOM, AsscComptesArchiveTableMap::COL_NOMCOURT, AsscComptesArchiveTableMap::COL_ID_POSTE, AsscComptesArchiveTableMap::COL_TYPE_OP, AsscComptesArchiveTableMap::COL_SOLDE_ANTERIEUR, AsscComptesArchiveTableMap::COL_DATE_ANTERIEURE, AsscComptesArchiveTableMap::COL_ID_ENTITE, AsscComptesArchiveTableMap::COL_OBSERVATION, AsscComptesArchiveTableMap::COL_CREATED_AT, AsscComptesArchiveTableMap::COL_UPDATED_AT, AsscComptesArchiveTableMap::COL_ARCHIVED_AT, ),
        self::TYPE_FIELDNAME     => array('id_compte', 'ncompte', 'nom', 'nomcourt', 'id_poste', 'type_op', 'solde_anterieur', 'date_anterieure', 'id_entite', 'observation', 'created_at', 'updated_at', 'archived_at', ),
        self::TYPE_NUM           => array(0, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, )
    );

    /**
     * holds an array of keys for quick access to the fieldnames array
     *
     * first dimension keys are the type constants
     * e.g. self::$fieldKeys[self::TYPE_PHPNAME]['Id'] = 0
     */
    protected static $fieldKeys = array (
        self::TYPE_PHPNAME       => array('IdCompte' => 0, 'Ncompte' => 1, 'Nom' => 2, 'Nomcourt' => 3, 'IdPoste' => 4, 'TypeOp' => 5, 'SoldeAnterieur' => 6, 'DateAnterieure' => 7, 'IdEntite' => 8, 'Observation' => 9, 'CreatedAt' => 10, 'UpdatedAt' => 11, 'ArchivedAt' => 12, ),
        self::TYPE_CAMELNAME     => array('idCompte' => 0, 'ncompte' => 1, 'nom' => 2, 'nomcourt' => 3, 'idPoste' => 4, 'typeOp' => 5, 'soldeAnterieur' => 6, 'dateAnterieure' => 7, 'idEntite' => 8, 'observation' => 9, 'createdAt' => 10, 'updatedAt' => 11, 'archivedAt' => 12, ),
        self::TYPE_COLNAME       => array(AsscComptesArchiveTableMap::COL_ID_COMPTE => 0, AsscComptesArchiveTableMap::COL_NCOMPTE => 1, AsscComptesArchiveTableMap::COL_NOM => 2, AsscComptesArchiveTableMap::COL_NOMCOURT => 3, AsscComptesArchiveTableMap::COL_ID_POSTE => 4, AsscComptesArchiveTableMap::COL_TYPE_OP => 5, AsscComptesArchiveTableMap::COL_SOLDE_ANTERIEUR => 6, AsscComptesArchiveTableMap::COL_DATE_ANTERIEURE => 7, AsscComptesArchiveTableMap::COL_ID_ENTITE => 8, AsscComptesArchiveTableMap::COL_OBSERVATION => 9, AsscComptesArchiveTableMap::COL_CREATED_AT => 10, AsscComptesArchiveTableMap::COL_UPDATED_AT => 11, AsscComptesArchiveTableMap::COL_ARCHIVED_AT => 12, ),
        self::TYPE_FIELDNAME     => array('id_compte' => 0, 'ncompte' => 1, 'nom' => 2, 'nomcourt' => 3, 'id_poste' => 4, 'type_op' => 5, 'solde_anterieur' => 6, 'date_anterieure' => 7, 'id_entite' => 8, 'observation' => 9, 'created_at' => 10, 'updated_at' => 11, 'archived_at' => 12, ),
        self::TYPE_NUM           => array(0, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, )
    );

    /**
     * Initialize the table attributes and columns
     * Relations are not initialized by this method since they are lazy loaded
     *
     * @return void
     * @throws PropelException
     */
    public function initialize()
    {
        // attributes
        $this->setName('assc_comptes_archive');
        $this->setPhpName('AsscComptesArchive');
        $this->setIdentifierQuoting(true);
        $this->setClassName('\\AsscComptesArchive');
        $this->setPackage('');
        $this->setUseIdGenerator(false);
        // columns
        $this->addPrimaryKey('id_compte', 'IdCompte', 'BIGINT', true, 21, null);
        $this->addColumn('ncompte', 'Ncompte', 'VARCHAR', true, 12, '512NEF');
        $this->addColumn('nom', 'Nom', 'VARCHAR', true, 80, 'Compte courant NEF');
        $this->addColumn('nomcourt', 'Nomcourt', 'VARCHAR', true, 20, 'CC NEF');
        $this->addColumn('id_poste', 'IdPoste', 'BIGINT', true, 21, 1);
        $this->addColumn('type_op', 'TypeOp', 'INTEGER', true, 2, 3);
        $this->addColumn('solde_anterieur', 'SoldeAnterieur', 'DOUBLE', false, null, null);
        $this->addColumn('date_anterieure', 'DateAnterieure', 'DATE', false, null, null);
        $this->addColumn('id_entite', 'IdEntite', 'BIGINT', true, 21, 1);
        $this->addColumn('observation', 'Observation', 'LONGVARCHAR', false, null, null);
        $this->addColumn('created_at', 'CreatedAt', 'TIMESTAMP', false, null, null);
        $this->addColumn('updated_at', 'UpdatedAt', 'TIMESTAMP', false, null, null);
        $this->addColumn('archived_at', 'ArchivedAt', 'TIMESTAMP', false, null, null);
    } // initialize()

    /**
     * Build the RelationMap objects for this table relationships
     */
    public function buildRelations()
    {
    } // buildRelations()

    /**
     * Retrieves a string version of the primary key from the DB resultset row that can be used to uniquely identify a row in this table.
     *
     * For tables with a single-column primary key, that simple pkey value will be returned.  For tables with
     * a multi-column primary key, a serialize()d version of the primary key will be returned.
     *
     * @param array  $row       resultset row.
     * @param int    $offset    The 0-based offset for reading from the resultset row.
     * @param string $indexType One of the class type constants TableMap::TYPE_PHPNAME, TableMap::TYPE_CAMELNAME
     *                           TableMap::TYPE_COLNAME, TableMap::TYPE_FIELDNAME, TableMap::TYPE_NUM
     *
     * @return string The primary key hash of the row
     */
    public static function getPrimaryKeyHashFromRow($row, $offset = 0, $indexType = TableMap::TYPE_NUM)
    {
        // If the PK cannot be derived from the row, return NULL.
        if ($row[TableMap::TYPE_NUM == $indexType ? 0 + $offset : static::translateFieldName('IdCompte', TableMap::TYPE_PHPNAME, $indexType)] === null) {
            return null;
        }

        return null === $row[TableMap::TYPE_NUM == $indexType ? 0 + $offset : static::translateFieldName('IdCompte', TableMap::TYPE_PHPNAME, $indexType)] || is_scalar($row[TableMap::TYPE_NUM == $indexType ? 0 + $offset : static::translateFieldName('IdCompte', TableMap::TYPE_PHPNAME, $indexType)]) || is_callable([$row[TableMap::TYPE_NUM == $indexType ? 0 + $offset : static::translateFieldName('IdCompte', TableMap::TYPE_PHPNAME, $indexType)], '__toString']) ? (string) $row[TableMap::TYPE_NUM == $indexType ? 0 + $offset : static::translateFieldName('IdCompte', TableMap::TYPE_PHPNAME, $indexType)] : $row[TableMap::TYPE_NUM == $indexType ? 0 + $offset : static::translateFieldName('IdCompte', TableMap::TYPE_PHPNAME, $indexType)];
    }

    /**
     * Retrieves the primary key from the DB resultset row
     * For tables with a single-column primary key, that simple pkey value will be returned.  For tables with
     * a multi-column primary key, an array of the primary key columns will be returned.
     *
     * @param array  $row       resultset row.
     * @param int    $offset    The 0-based offset for reading from the resultset row.
     * @param string $indexType One of the class type constants TableMap::TYPE_PHPNAME, TableMap::TYPE_CAMELNAME
     *                           TableMap::TYPE_COLNAME, TableMap::TYPE_FIELDNAME, TableMap::TYPE_NUM
     *
     * @return mixed The primary key of the row
     */
    public static function getPrimaryKeyFromRow($row, $offset = 0, $indexType = TableMap::TYPE_NUM)
    {
        return (string) $row[
            $indexType == TableMap::TYPE_NUM
                ? 0 + $offset
                : self::translateFieldName('IdCompte', TableMap::TYPE_PHPNAME, $indexType)
        ];
    }

    /**
     * The class that the tableMap will make instances of.
     *
     * If $withPrefix is true, the returned path
     * uses a dot-path notation which is translated into a path
     * relative to a location on the PHP include_path.
     * (e.g. path.to.MyClass -> 'path/to/MyClass.php')
     *
     * @param boolean $withPrefix Whether or not to return the path with the class name
     * @return string path.to.ClassName
     */
    public static function getOMClass($withPrefix = true)
    {
        return $withPrefix ? AsscComptesArchiveTableMap::CLASS_DEFAULT : AsscComptesArchiveTableMap::OM_CLASS;
    }

    /**
     * Populates an object of the default type or an object that inherit from the default.
     *
     * @param array  $row       row returned by DataFetcher->fetch().
     * @param int    $offset    The 0-based offset for reading from the resultset row.
     * @param string $indexType The index type of $row. Mostly DataFetcher->getIndexType().
                                 One of the class type constants TableMap::TYPE_PHPNAME, TableMap::TYPE_CAMELNAME
     *                           TableMap::TYPE_COLNAME, TableMap::TYPE_FIELDNAME, TableMap::TYPE_NUM.
     *
     * @throws PropelException Any exceptions caught during processing will be
     *                         rethrown wrapped into a PropelException.
     * @return array           (AsscComptesArchive object, last column rank)
     */
    public static function populateObject($row, $offset = 0, $indexType = TableMap::TYPE_NUM)
    {
        $key = AsscComptesArchiveTableMap::getPrimaryKeyHashFromRow($row, $offset, $indexType);
        if (null !== ($obj = AsscComptesArchiveTableMap::getInstanceFromPool($key))) {
            // We no longer rehydrate the object, since this can cause data loss.
            // See http://www.propelorm.org/ticket/509
            // $obj->hydrate($row, $offset, true); // rehydrate
            $col = $offset + AsscComptesArchiveTableMap::NUM_HYDRATE_COLUMNS;
        } else {
            $cls = AsscComptesArchiveTableMap::OM_CLASS;
            /** @var AsscComptesArchive $obj */
            $obj = new $cls();
            $col = $obj->hydrate($row, $offset, false, $indexType);
            AsscComptesArchiveTableMap::addInstanceToPool($obj, $key);
        }

        return array($obj, $col);
    }

    /**
     * The returned array will contain objects of the default type or
     * objects that inherit from the default.
     *
     * @param DataFetcherInterface $dataFetcher
     * @return array
     * @throws PropelException Any exceptions caught during processing will be
     *                         rethrown wrapped into a PropelException.
     */
    public static function populateObjects(DataFetcherInterface $dataFetcher)
    {
        $results = array();

        // set the class once to avoid overhead in the loop
        $cls = static::getOMClass(false);
        // populate the object(s)
        while ($row = $dataFetcher->fetch()) {
            $key = AsscComptesArchiveTableMap::getPrimaryKeyHashFromRow($row, 0, $dataFetcher->getIndexType());
            if (null !== ($obj = AsscComptesArchiveTableMap::getInstanceFromPool($key))) {
                // We no longer rehydrate the object, since this can cause data loss.
                // See http://www.propelorm.org/ticket/509
                // $obj->hydrate($row, 0, true); // rehydrate
                $results[] = $obj;
            } else {
                /** @var AsscComptesArchive $obj */
                $obj = new $cls();
                $obj->hydrate($row);
                $results[] = $obj;
                AsscComptesArchiveTableMap::addInstanceToPool($obj, $key);
            } // if key exists
        }

        return $results;
    }
    /**
     * Add all the columns needed to create a new object.
     *
     * Note: any columns that were marked with lazyLoad="true" in the
     * XML schema will not be added to the select list and only loaded
     * on demand.
     *
     * @param Criteria $criteria object containing the columns to add.
     * @param string   $alias    optional table alias
     * @throws PropelException Any exceptions caught during processing will be
     *                         rethrown wrapped into a PropelException.
     */
    public static function addSelectColumns(Criteria $criteria, $alias = null)
    {
        if (null === $alias) {
            $criteria->addSelectColumn(AsscComptesArchiveTableMap::COL_ID_COMPTE);
            $criteria->addSelectColumn(AsscComptesArchiveTableMap::COL_NCOMPTE);
            $criteria->addSelectColumn(AsscComptesArchiveTableMap::COL_NOM);
            $criteria->addSelectColumn(AsscComptesArchiveTableMap::COL_NOMCOURT);
            $criteria->addSelectColumn(AsscComptesArchiveTableMap::COL_ID_POSTE);
            $criteria->addSelectColumn(AsscComptesArchiveTableMap::COL_TYPE_OP);
            $criteria->addSelectColumn(AsscComptesArchiveTableMap::COL_SOLDE_ANTERIEUR);
            $criteria->addSelectColumn(AsscComptesArchiveTableMap::COL_DATE_ANTERIEURE);
            $criteria->addSelectColumn(AsscComptesArchiveTableMap::COL_ID_ENTITE);
            $criteria->addSelectColumn(AsscComptesArchiveTableMap::COL_OBSERVATION);
            $criteria->addSelectColumn(AsscComptesArchiveTableMap::COL_CREATED_AT);
            $criteria->addSelectColumn(AsscComptesArchiveTableMap::COL_UPDATED_AT);
            $criteria->addSelectColumn(AsscComptesArchiveTableMap::COL_ARCHIVED_AT);
        } else {
            $criteria->addSelectColumn($alias . '.id_compte');
            $criteria->addSelectColumn($alias . '.ncompte');
            $criteria->addSelectColumn($alias . '.nom');
            $criteria->addSelectColumn($alias . '.nomcourt');
            $criteria->addSelectColumn($alias . '.id_poste');
            $criteria->addSelectColumn($alias . '.type_op');
            $criteria->addSelectColumn($alias . '.solde_anterieur');
            $criteria->addSelectColumn($alias . '.date_anterieure');
            $criteria->addSelectColumn($alias . '.id_entite');
            $criteria->addSelectColumn($alias . '.observation');
            $criteria->addSelectColumn($alias . '.created_at');
            $criteria->addSelectColumn($alias . '.updated_at');
            $criteria->addSelectColumn($alias . '.archived_at');
        }
    }

    /**
     * Returns the TableMap related to this object.
     * This method is not needed for general use but a specific application could have a need.
     * @return TableMap
     * @throws PropelException Any exceptions caught during processing will be
     *                         rethrown wrapped into a PropelException.
     */
    public static function getTableMap()
    {
        return Propel::getServiceContainer()->getDatabaseMap(AsscComptesArchiveTableMap::DATABASE_NAME)->getTable(AsscComptesArchiveTableMap::TABLE_NAME);
    }

    /**
     * Add a TableMap instance to the database for this tableMap class.
     */
    public static function buildTableMap()
    {
        $dbMap = Propel::getServiceContainer()->getDatabaseMap(AsscComptesArchiveTableMap::DATABASE_NAME);
        if (!$dbMap->hasTable(AsscComptesArchiveTableMap::TABLE_NAME)) {
            $dbMap->addTableObject(new AsscComptesArchiveTableMap());
        }
    }

    /**
     * Performs a DELETE on the database, given a AsscComptesArchive or Criteria object OR a primary key value.
     *
     * @param mixed               $values Criteria or AsscComptesArchive object or primary key or array of primary keys
     *              which is used to create the DELETE statement
     * @param  ConnectionInterface $con the connection to use
     * @return int             The number of affected rows (if supported by underlying database driver).  This includes CASCADE-related rows
     *                         if supported by native driver or if emulated using Propel.
     * @throws PropelException Any exceptions caught during processing will be
     *                         rethrown wrapped into a PropelException.
     */
     public static function doDelete($values, ConnectionInterface $con = null)
     {
        if (null === $con) {
            $con = Propel::getServiceContainer()->getWriteConnection(AsscComptesArchiveTableMap::DATABASE_NAME);
        }

        if ($values instanceof Criteria) {
            // rename for clarity
            $criteria = $values;
        } elseif ($values instanceof \AsscComptesArchive) { // it's a model object
            // create criteria based on pk values
            $criteria = $values->buildPkeyCriteria();
        } else { // it's a primary key, or an array of pks
            $criteria = new Criteria(AsscComptesArchiveTableMap::DATABASE_NAME);
            $criteria->add(AsscComptesArchiveTableMap::COL_ID_COMPTE, (array) $values, Criteria::IN);
        }

        $query = AsscComptesArchiveQuery::create()->mergeWith($criteria);

        if ($values instanceof Criteria) {
            AsscComptesArchiveTableMap::clearInstancePool();
        } elseif (!is_object($values)) { // it's a primary key, or an array of pks
            foreach ((array) $values as $singleval) {
                AsscComptesArchiveTableMap::removeInstanceFromPool($singleval);
            }
        }

        return $query->delete($con);
    }

    /**
     * Deletes all rows from the assc_comptes_archive table.
     *
     * @param ConnectionInterface $con the connection to use
     * @return int The number of affected rows (if supported by underlying database driver).
     */
    public static function doDeleteAll(ConnectionInterface $con = null)
    {
        return AsscComptesArchiveQuery::create()->doDeleteAll($con);
    }

    /**
     * Performs an INSERT on the database, given a AsscComptesArchive or Criteria object.
     *
     * @param mixed               $criteria Criteria or AsscComptesArchive object containing data that is used to create the INSERT statement.
     * @param ConnectionInterface $con the ConnectionInterface connection to use
     * @return mixed           The new primary key.
     * @throws PropelException Any exceptions caught during processing will be
     *                         rethrown wrapped into a PropelException.
     */
    public static function doInsert($criteria, ConnectionInterface $con = null)
    {
        if (null === $con) {
            $con = Propel::getServiceContainer()->getWriteConnection(AsscComptesArchiveTableMap::DATABASE_NAME);
        }

        if ($criteria instanceof Criteria) {
            $criteria = clone $criteria; // rename for clarity
        } else {
            $criteria = $criteria->buildCriteria(); // build Criteria from AsscComptesArchive object
        }


        // Set the correct dbName
        $query = AsscComptesArchiveQuery::create()->mergeWith($criteria);

        // use transaction because $criteria could contain info
        // for more than one table (I guess, conceivably)
        return $con->transaction(function () use ($con, $query) {
            return $query->doInsert($con);
        });
    }

} // AsscComptesArchiveTableMap
// This is the static code needed to register the TableMap for this table with the main Propel class.
//
AsscComptesArchiveTableMap::buildTableMap();
