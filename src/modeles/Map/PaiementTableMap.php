<?php

namespace Map;

use \Paiement;
use \PaiementQuery;
use Propel\Runtime\Propel;
use Propel\Runtime\ActiveQuery\Criteria;
use Propel\Runtime\ActiveQuery\InstancePoolTrait;
use Propel\Runtime\Connection\ConnectionInterface;
use Propel\Runtime\DataFetcher\DataFetcherInterface;
use Propel\Runtime\Exception\PropelException;
use Propel\Runtime\Map\RelationMap;
use Propel\Runtime\Map\TableMap;
use Propel\Runtime\Map\TableMapTrait;


/**
 * This class defines the structure of the 'asso_paiements' table.
 *
 *
 *
 * This map class is used by Propel to do runtime db structure discovery.
 * For example, the createSelectSql() method checks the type of a given column used in an
 * ORDER BY clause to know whether it needs to apply SQL to make the ORDER BY case-insensitive
 * (i.e. if it's a text column type).
 *
 */
class PaiementTableMap extends TableMap
{
    use InstancePoolTrait;
    use TableMapTrait;

    /**
     * The (dot-path) name of this class
     */
    const CLASS_NAME = '.Map.PaiementTableMap';

    /**
     * The default database name for this class
     */
    const DATABASE_NAME = 'simplasso';

    /**
     * The table name for this class
     */
    const TABLE_NAME = 'asso_paiements';

    /**
     * The related Propel class for this table
     */
    const OM_CLASS = '\\Paiement';

    /**
     * A class that can be returned by this tableMap
     */
    const CLASS_DEFAULT = 'Paiement';

    /**
     * The total number of columns
     */
    const NUM_COLUMNS = 14;

    /**
     * The number of lazy-loaded columns
     */
    const NUM_LAZY_LOAD_COLUMNS = 0;

    /**
     * The number of columns to hydrate (NUM_COLUMNS - NUM_LAZY_LOAD_COLUMNS)
     */
    const NUM_HYDRATE_COLUMNS = 14;

    /**
     * the column name for the id_paiement field
     */
    const COL_ID_PAIEMENT = 'asso_paiements.id_paiement';

    /**
     * the column name for the objet field
     */
    const COL_OBJET = 'asso_paiements.objet';

    /**
     * the column name for the id_objet field
     */
    const COL_ID_OBJET = 'asso_paiements.id_objet';

    /**
     * the column name for the id_entite field
     */
    const COL_ID_ENTITE = 'asso_paiements.id_entite';

    /**
     * the column name for the id_tresor field
     */
    const COL_ID_TRESOR = 'asso_paiements.id_tresor';

    /**
     * the column name for the montant field
     */
    const COL_MONTANT = 'asso_paiements.montant';

    /**
     * the column name for the numero field
     */
    const COL_NUMERO = 'asso_paiements.numero';

    /**
     * the column name for the date_cheque field
     */
    const COL_DATE_CHEQUE = 'asso_paiements.date_cheque';

    /**
     * the column name for the date_enregistrement field
     */
    const COL_DATE_ENREGISTREMENT = 'asso_paiements.date_enregistrement';

    /**
     * the column name for the remise field
     */
    const COL_REMISE = 'asso_paiements.remise';

    /**
     * the column name for the comptabilise field
     */
    const COL_COMPTABILISE = 'asso_paiements.comptabilise';

    /**
     * the column name for the observation field
     */
    const COL_OBSERVATION = 'asso_paiements.observation';

    /**
     * the column name for the created_at field
     */
    const COL_CREATED_AT = 'asso_paiements.created_at';

    /**
     * the column name for the updated_at field
     */
    const COL_UPDATED_AT = 'asso_paiements.updated_at';

    /**
     * The default string format for model objects of the related table
     */
    const DEFAULT_STRING_FORMAT = 'YAML';

    /**
     * holds an array of fieldnames
     *
     * first dimension keys are the type constants
     * e.g. self::$fieldNames[self::TYPE_PHPNAME][0] = 'Id'
     */
    protected static $fieldNames = array (
        self::TYPE_PHPNAME       => array('IdPaiement', 'Objet', 'IdObjet', 'IdEntite', 'IdTresor', 'Montant', 'Numero', 'DateCheque', 'DateEnregistrement', 'Remise', 'Comptabilise', 'Observation', 'CreatedAt', 'UpdatedAt', ),
        self::TYPE_CAMELNAME     => array('idPaiement', 'objet', 'idObjet', 'idEntite', 'idTresor', 'montant', 'numero', 'dateCheque', 'dateEnregistrement', 'remise', 'comptabilise', 'observation', 'createdAt', 'updatedAt', ),
        self::TYPE_COLNAME       => array(PaiementTableMap::COL_ID_PAIEMENT, PaiementTableMap::COL_OBJET, PaiementTableMap::COL_ID_OBJET, PaiementTableMap::COL_ID_ENTITE, PaiementTableMap::COL_ID_TRESOR, PaiementTableMap::COL_MONTANT, PaiementTableMap::COL_NUMERO, PaiementTableMap::COL_DATE_CHEQUE, PaiementTableMap::COL_DATE_ENREGISTREMENT, PaiementTableMap::COL_REMISE, PaiementTableMap::COL_COMPTABILISE, PaiementTableMap::COL_OBSERVATION, PaiementTableMap::COL_CREATED_AT, PaiementTableMap::COL_UPDATED_AT, ),
        self::TYPE_FIELDNAME     => array('id_paiement', 'objet', 'id_objet', 'id_entite', 'id_tresor', 'montant', 'numero', 'date_cheque', 'date_enregistrement', 'remise', 'comptabilise', 'observation', 'created_at', 'updated_at', ),
        self::TYPE_NUM           => array(0, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, )
    );

    /**
     * holds an array of keys for quick access to the fieldnames array
     *
     * first dimension keys are the type constants
     * e.g. self::$fieldKeys[self::TYPE_PHPNAME]['Id'] = 0
     */
    protected static $fieldKeys = array (
        self::TYPE_PHPNAME       => array('IdPaiement' => 0, 'Objet' => 1, 'IdObjet' => 2, 'IdEntite' => 3, 'IdTresor' => 4, 'Montant' => 5, 'Numero' => 6, 'DateCheque' => 7, 'DateEnregistrement' => 8, 'Remise' => 9, 'Comptabilise' => 10, 'Observation' => 11, 'CreatedAt' => 12, 'UpdatedAt' => 13, ),
        self::TYPE_CAMELNAME     => array('idPaiement' => 0, 'objet' => 1, 'idObjet' => 2, 'idEntite' => 3, 'idTresor' => 4, 'montant' => 5, 'numero' => 6, 'dateCheque' => 7, 'dateEnregistrement' => 8, 'remise' => 9, 'comptabilise' => 10, 'observation' => 11, 'createdAt' => 12, 'updatedAt' => 13, ),
        self::TYPE_COLNAME       => array(PaiementTableMap::COL_ID_PAIEMENT => 0, PaiementTableMap::COL_OBJET => 1, PaiementTableMap::COL_ID_OBJET => 2, PaiementTableMap::COL_ID_ENTITE => 3, PaiementTableMap::COL_ID_TRESOR => 4, PaiementTableMap::COL_MONTANT => 5, PaiementTableMap::COL_NUMERO => 6, PaiementTableMap::COL_DATE_CHEQUE => 7, PaiementTableMap::COL_DATE_ENREGISTREMENT => 8, PaiementTableMap::COL_REMISE => 9, PaiementTableMap::COL_COMPTABILISE => 10, PaiementTableMap::COL_OBSERVATION => 11, PaiementTableMap::COL_CREATED_AT => 12, PaiementTableMap::COL_UPDATED_AT => 13, ),
        self::TYPE_FIELDNAME     => array('id_paiement' => 0, 'objet' => 1, 'id_objet' => 2, 'id_entite' => 3, 'id_tresor' => 4, 'montant' => 5, 'numero' => 6, 'date_cheque' => 7, 'date_enregistrement' => 8, 'remise' => 9, 'comptabilise' => 10, 'observation' => 11, 'created_at' => 12, 'updated_at' => 13, ),
        self::TYPE_NUM           => array(0, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, )
    );

    /**
     * Initialize the table attributes and columns
     * Relations are not initialized by this method since they are lazy loaded
     *
     * @return void
     * @throws PropelException
     */
    public function initialize()
    {
        // attributes
        $this->setName('asso_paiements');
        $this->setPhpName('Paiement');
        $this->setIdentifierQuoting(true);
        $this->setClassName('\\Paiement');
        $this->setPackage('');
        $this->setUseIdGenerator(true);
        // columns
        $this->addPrimaryKey('id_paiement', 'IdPaiement', 'BIGINT', true, null, null);
        $this->addColumn('objet', 'Objet', 'VARCHAR', true, 25, 'membre');
        $this->addColumn('id_objet', 'IdObjet', 'BIGINT', true, null, 0);
        $this->addForeignKey('id_entite', 'IdEntite', 'BIGINT', 'asso_entites', 'id_entite', true, 21, null);
        $this->addForeignKey('id_tresor', 'IdTresor', 'BIGINT', 'asso_tresors', 'id_tresor', true, 21, null);
        $this->addColumn('montant', 'Montant', 'DOUBLE', false, 12, 0);
        $this->addColumn('numero', 'Numero', 'VARCHAR', false, 25, null);
        $this->addColumn('date_cheque', 'DateCheque', 'DATE', false, null, null);
        $this->addColumn('date_enregistrement', 'DateEnregistrement', 'DATE', false, null, null);
        $this->addColumn('remise', 'Remise', 'BIGINT', false, 12, 0);
        $this->addColumn('comptabilise', 'Comptabilise', 'BOOLEAN', false, 1, false);
        $this->addColumn('observation', 'Observation', 'LONGVARCHAR', false, null, null);
        $this->addColumn('created_at', 'CreatedAt', 'TIMESTAMP', false, null, null);
        $this->addColumn('updated_at', 'UpdatedAt', 'TIMESTAMP', false, null, null);
    } // initialize()

    /**
     * Build the RelationMap objects for this table relationships
     */
    public function buildRelations()
    {
        $this->addRelation('Entite', '\\Entite', RelationMap::MANY_TO_ONE, array (
  0 =>
  array (
    0 => ':id_entite',
    1 => ':id_entite',
  ),
), 'CASCADE', null, null, false);
        $this->addRelation('Tresor', '\\Tresor', RelationMap::MANY_TO_ONE, array (
  0 =>
  array (
    0 => ':id_tresor',
    1 => ':id_tresor',
  ),
), 'CASCADE', null, null, false);
        $this->addRelation('Servicepaiement', '\\Servicepaiement', RelationMap::ONE_TO_MANY, array (
  0 =>
  array (
    0 => ':id_paiement',
    1 => ':id_paiement',
  ),
), 'CASCADE', null, 'Servicepaiements', false);
    } // buildRelations()

    /**
     *
     * Gets the list of behaviors registered for this table
     *
     * @return array Associative array (name => parameters) of behaviors
     */
    public function getBehaviors()
    {
        return array(
            'timestampable' => array('create_column' => 'created_at', 'update_column' => 'updated_at', 'disable_created_at' => 'false', 'disable_updated_at' => 'false', ),
            'archivable' => array('archive_table' => '', 'archive_phpname' => '', 'archive_class' => '', 'log_archived_at' => 'true', 'archived_at_column' => 'archived_at', 'archive_on_insert' => 'false', 'archive_on_update' => 'false', 'archive_on_delete' => 'true', ),
        );
    } // getBehaviors()
    /**
     * Method to invalidate the instance pool of all tables related to asso_paiements     * by a foreign key with ON DELETE CASCADE
     */
    public static function clearRelatedInstancePool()
    {
        // Invalidate objects in related instance pools,
        // since one or more of them may be deleted by ON DELETE CASCADE/SETNULL rule.
        ServicepaiementTableMap::clearInstancePool();
    }

    /**
     * Retrieves a string version of the primary key from the DB resultset row that can be used to uniquely identify a row in this table.
     *
     * For tables with a single-column primary key, that simple pkey value will be returned.  For tables with
     * a multi-column primary key, a serialize()d version of the primary key will be returned.
     *
     * @param array  $row       resultset row.
     * @param int    $offset    The 0-based offset for reading from the resultset row.
     * @param string $indexType One of the class type constants TableMap::TYPE_PHPNAME, TableMap::TYPE_CAMELNAME
     *                           TableMap::TYPE_COLNAME, TableMap::TYPE_FIELDNAME, TableMap::TYPE_NUM
     *
     * @return string The primary key hash of the row
     */
    public static function getPrimaryKeyHashFromRow($row, $offset = 0, $indexType = TableMap::TYPE_NUM)
    {
        // If the PK cannot be derived from the row, return NULL.
        if ($row[TableMap::TYPE_NUM == $indexType ? 0 + $offset : static::translateFieldName('IdPaiement', TableMap::TYPE_PHPNAME, $indexType)] === null) {
            return null;
        }

        return null === $row[TableMap::TYPE_NUM == $indexType ? 0 + $offset : static::translateFieldName('IdPaiement', TableMap::TYPE_PHPNAME, $indexType)] || is_scalar($row[TableMap::TYPE_NUM == $indexType ? 0 + $offset : static::translateFieldName('IdPaiement', TableMap::TYPE_PHPNAME, $indexType)]) || is_callable([$row[TableMap::TYPE_NUM == $indexType ? 0 + $offset : static::translateFieldName('IdPaiement', TableMap::TYPE_PHPNAME, $indexType)], '__toString']) ? (string) $row[TableMap::TYPE_NUM == $indexType ? 0 + $offset : static::translateFieldName('IdPaiement', TableMap::TYPE_PHPNAME, $indexType)] : $row[TableMap::TYPE_NUM == $indexType ? 0 + $offset : static::translateFieldName('IdPaiement', TableMap::TYPE_PHPNAME, $indexType)];
    }

    /**
     * Retrieves the primary key from the DB resultset row
     * For tables with a single-column primary key, that simple pkey value will be returned.  For tables with
     * a multi-column primary key, an array of the primary key columns will be returned.
     *
     * @param array  $row       resultset row.
     * @param int    $offset    The 0-based offset for reading from the resultset row.
     * @param string $indexType One of the class type constants TableMap::TYPE_PHPNAME, TableMap::TYPE_CAMELNAME
     *                           TableMap::TYPE_COLNAME, TableMap::TYPE_FIELDNAME, TableMap::TYPE_NUM
     *
     * @return mixed The primary key of the row
     */
    public static function getPrimaryKeyFromRow($row, $offset = 0, $indexType = TableMap::TYPE_NUM)
    {
        return (string) $row[
            $indexType == TableMap::TYPE_NUM
                ? 0 + $offset
                : self::translateFieldName('IdPaiement', TableMap::TYPE_PHPNAME, $indexType)
        ];
    }

    /**
     * The class that the tableMap will make instances of.
     *
     * If $withPrefix is true, the returned path
     * uses a dot-path notation which is translated into a path
     * relative to a location on the PHP include_path.
     * (e.g. path.to.MyClass -> 'path/to/MyClass.php')
     *
     * @param boolean $withPrefix Whether or not to return the path with the class name
     * @return string path.to.ClassName
     */
    public static function getOMClass($withPrefix = true)
    {
        return $withPrefix ? PaiementTableMap::CLASS_DEFAULT : PaiementTableMap::OM_CLASS;
    }

    /**
     * Populates an object of the default type or an object that inherit from the default.
     *
     * @param array  $row       row returned by DataFetcher->fetch().
     * @param int    $offset    The 0-based offset for reading from the resultset row.
     * @param string $indexType The index type of $row. Mostly DataFetcher->getIndexType().
                                 One of the class type constants TableMap::TYPE_PHPNAME, TableMap::TYPE_CAMELNAME
     *                           TableMap::TYPE_COLNAME, TableMap::TYPE_FIELDNAME, TableMap::TYPE_NUM.
     *
     * @throws PropelException Any exceptions caught during processing will be
     *                         rethrown wrapped into a PropelException.
     * @return array           (Paiement object, last column rank)
     */
    public static function populateObject($row, $offset = 0, $indexType = TableMap::TYPE_NUM)
    {
        $key = PaiementTableMap::getPrimaryKeyHashFromRow($row, $offset, $indexType);
        if (null !== ($obj = PaiementTableMap::getInstanceFromPool($key))) {
            // We no longer rehydrate the object, since this can cause data loss.
            // See http://www.propelorm.org/ticket/509
            // $obj->hydrate($row, $offset, true); // rehydrate
            $col = $offset + PaiementTableMap::NUM_HYDRATE_COLUMNS;
        } else {
            $cls = PaiementTableMap::OM_CLASS;
            /** @var Paiement $obj */
            $obj = new $cls();
            $col = $obj->hydrate($row, $offset, false, $indexType);
            PaiementTableMap::addInstanceToPool($obj, $key);
        }

        return array($obj, $col);
    }

    /**
     * The returned array will contain objects of the default type or
     * objects that inherit from the default.
     *
     * @param DataFetcherInterface $dataFetcher
     * @return array
     * @throws PropelException Any exceptions caught during processing will be
     *                         rethrown wrapped into a PropelException.
     */
    public static function populateObjects(DataFetcherInterface $dataFetcher)
    {
        $results = array();

        // set the class once to avoid overhead in the loop
        $cls = static::getOMClass(false);
        // populate the object(s)
        while ($row = $dataFetcher->fetch()) {
            $key = PaiementTableMap::getPrimaryKeyHashFromRow($row, 0, $dataFetcher->getIndexType());
            if (null !== ($obj = PaiementTableMap::getInstanceFromPool($key))) {
                // We no longer rehydrate the object, since this can cause data loss.
                // See http://www.propelorm.org/ticket/509
                // $obj->hydrate($row, 0, true); // rehydrate
                $results[] = $obj;
            } else {
                /** @var Paiement $obj */
                $obj = new $cls();
                $obj->hydrate($row);
                $results[] = $obj;
                PaiementTableMap::addInstanceToPool($obj, $key);
            } // if key exists
        }

        return $results;
    }
    /**
     * Add all the columns needed to create a new object.
     *
     * Note: any columns that were marked with lazyLoad="true" in the
     * XML schema will not be added to the select list and only loaded
     * on demand.
     *
     * @param Criteria $criteria object containing the columns to add.
     * @param string   $alias    optional table alias
     * @throws PropelException Any exceptions caught during processing will be
     *                         rethrown wrapped into a PropelException.
     */
    public static function addSelectColumns(Criteria $criteria, $alias = null)
    {
        if (null === $alias) {
            $criteria->addSelectColumn(PaiementTableMap::COL_ID_PAIEMENT);
            $criteria->addSelectColumn(PaiementTableMap::COL_OBJET);
            $criteria->addSelectColumn(PaiementTableMap::COL_ID_OBJET);
            $criteria->addSelectColumn(PaiementTableMap::COL_ID_ENTITE);
            $criteria->addSelectColumn(PaiementTableMap::COL_ID_TRESOR);
            $criteria->addSelectColumn(PaiementTableMap::COL_MONTANT);
            $criteria->addSelectColumn(PaiementTableMap::COL_NUMERO);
            $criteria->addSelectColumn(PaiementTableMap::COL_DATE_CHEQUE);
            $criteria->addSelectColumn(PaiementTableMap::COL_DATE_ENREGISTREMENT);
            $criteria->addSelectColumn(PaiementTableMap::COL_REMISE);
            $criteria->addSelectColumn(PaiementTableMap::COL_COMPTABILISE);
            $criteria->addSelectColumn(PaiementTableMap::COL_OBSERVATION);
            $criteria->addSelectColumn(PaiementTableMap::COL_CREATED_AT);
            $criteria->addSelectColumn(PaiementTableMap::COL_UPDATED_AT);
        } else {
            $criteria->addSelectColumn($alias . '.id_paiement');
            $criteria->addSelectColumn($alias . '.objet');
            $criteria->addSelectColumn($alias . '.id_objet');
            $criteria->addSelectColumn($alias . '.id_entite');
            $criteria->addSelectColumn($alias . '.id_tresor');
            $criteria->addSelectColumn($alias . '.montant');
            $criteria->addSelectColumn($alias . '.numero');
            $criteria->addSelectColumn($alias . '.date_cheque');
            $criteria->addSelectColumn($alias . '.date_enregistrement');
            $criteria->addSelectColumn($alias . '.remise');
            $criteria->addSelectColumn($alias . '.comptabilise');
            $criteria->addSelectColumn($alias . '.observation');
            $criteria->addSelectColumn($alias . '.created_at');
            $criteria->addSelectColumn($alias . '.updated_at');
        }
    }

    /**
     * Returns the TableMap related to this object.
     * This method is not needed for general use but a specific application could have a need.
     * @return TableMap
     * @throws PropelException Any exceptions caught during processing will be
     *                         rethrown wrapped into a PropelException.
     */
    public static function getTableMap()
    {
        return Propel::getServiceContainer()->getDatabaseMap(PaiementTableMap::DATABASE_NAME)->getTable(PaiementTableMap::TABLE_NAME);
    }

    /**
     * Add a TableMap instance to the database for this tableMap class.
     */
    public static function buildTableMap()
    {
        $dbMap = Propel::getServiceContainer()->getDatabaseMap(PaiementTableMap::DATABASE_NAME);
        if (!$dbMap->hasTable(PaiementTableMap::TABLE_NAME)) {
            $dbMap->addTableObject(new PaiementTableMap());
        }
    }

    /**
     * Performs a DELETE on the database, given a Paiement or Criteria object OR a primary key value.
     *
     * @param mixed               $values Criteria or Paiement object or primary key or array of primary keys
     *              which is used to create the DELETE statement
     * @param  ConnectionInterface $con the connection to use
     * @return int             The number of affected rows (if supported by underlying database driver).  This includes CASCADE-related rows
     *                         if supported by native driver or if emulated using Propel.
     * @throws PropelException Any exceptions caught during processing will be
     *                         rethrown wrapped into a PropelException.
     */
     public static function doDelete($values, ConnectionInterface $con = null)
     {
        if (null === $con) {
            $con = Propel::getServiceContainer()->getWriteConnection(PaiementTableMap::DATABASE_NAME);
        }

        if ($values instanceof Criteria) {
            // rename for clarity
            $criteria = $values;
        } elseif ($values instanceof \Paiement) { // it's a model object
            // create criteria based on pk values
            $criteria = $values->buildPkeyCriteria();
        } else { // it's a primary key, or an array of pks
            $criteria = new Criteria(PaiementTableMap::DATABASE_NAME);
            $criteria->add(PaiementTableMap::COL_ID_PAIEMENT, (array) $values, Criteria::IN);
        }

        $query = PaiementQuery::create()->mergeWith($criteria);

        if ($values instanceof Criteria) {
            PaiementTableMap::clearInstancePool();
        } elseif (!is_object($values)) { // it's a primary key, or an array of pks
            foreach ((array) $values as $singleval) {
                PaiementTableMap::removeInstanceFromPool($singleval);
            }
        }

        return $query->delete($con);
    }

    /**
     * Deletes all rows from the asso_paiements table.
     *
     * @param ConnectionInterface $con the connection to use
     * @return int The number of affected rows (if supported by underlying database driver).
     */
    public static function doDeleteAll(ConnectionInterface $con = null)
    {
        return PaiementQuery::create()->doDeleteAll($con);
    }

    /**
     * Performs an INSERT on the database, given a Paiement or Criteria object.
     *
     * @param mixed               $criteria Criteria or Paiement object containing data that is used to create the INSERT statement.
     * @param ConnectionInterface $con the ConnectionInterface connection to use
     * @return mixed           The new primary key.
     * @throws PropelException Any exceptions caught during processing will be
     *                         rethrown wrapped into a PropelException.
     */
    public static function doInsert($criteria, ConnectionInterface $con = null)
    {
        if (null === $con) {
            $con = Propel::getServiceContainer()->getWriteConnection(PaiementTableMap::DATABASE_NAME);
        }

        if ($criteria instanceof Criteria) {
            $criteria = clone $criteria; // rename for clarity
        } else {
            $criteria = $criteria->buildCriteria(); // build Criteria from Paiement object
        }

        if ($criteria->containsKey(PaiementTableMap::COL_ID_PAIEMENT) && $criteria->keyContainsValue(PaiementTableMap::COL_ID_PAIEMENT) ) {
            throw new PropelException('Cannot insert a value for auto-increment primary key ('.PaiementTableMap::COL_ID_PAIEMENT.')');
        }


        // Set the correct dbName
        $query = PaiementQuery::create()->mergeWith($criteria);

        // use transaction because $criteria could contain info
        // for more than one table (I guess, conceivably)
        return $con->transaction(function () use ($con, $query) {
            return $query->doInsert($con);
        });
    }

} // PaiementTableMap
// This is the static code needed to register the TableMap for this table with the main Propel class.
//
PaiementTableMap::buildTableMap();
