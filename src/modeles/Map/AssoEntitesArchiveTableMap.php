<?php

namespace Map;

use \AssoEntitesArchive;
use \AssoEntitesArchiveQuery;
use Propel\Runtime\Propel;
use Propel\Runtime\ActiveQuery\Criteria;
use Propel\Runtime\ActiveQuery\InstancePoolTrait;
use Propel\Runtime\Connection\ConnectionInterface;
use Propel\Runtime\DataFetcher\DataFetcherInterface;
use Propel\Runtime\Exception\PropelException;
use Propel\Runtime\Map\RelationMap;
use Propel\Runtime\Map\TableMap;
use Propel\Runtime\Map\TableMapTrait;


/**
 * This class defines the structure of the 'asso_entites_archive' table.
 *
 *
 *
 * This map class is used by Propel to do runtime db structure discovery.
 * For example, the createSelectSql() method checks the type of a given column used in an
 * ORDER BY clause to know whether it needs to apply SQL to make the ORDER BY case-insensitive
 * (i.e. if it's a text column type).
 *
 */
class AssoEntitesArchiveTableMap extends TableMap
{
    use InstancePoolTrait;
    use TableMapTrait;

    /**
     * The (dot-path) name of this class
     */
    const CLASS_NAME = '.Map.AssoEntitesArchiveTableMap';

    /**
     * The default database name for this class
     */
    const DATABASE_NAME = 'simplasso';

    /**
     * The table name for this class
     */
    const TABLE_NAME = 'asso_entites_archive';

    /**
     * The related Propel class for this table
     */
    const OM_CLASS = '\\AssoEntitesArchive';

    /**
     * A class that can be returned by this tableMap
     */
    const CLASS_DEFAULT = 'AssoEntitesArchive';

    /**
     * The total number of columns
     */
    const NUM_COLUMNS = 15;

    /**
     * The number of lazy-loaded columns
     */
    const NUM_LAZY_LOAD_COLUMNS = 0;

    /**
     * The number of columns to hydrate (NUM_COLUMNS - NUM_LAZY_LOAD_COLUMNS)
     */
    const NUM_HYDRATE_COLUMNS = 15;

    /**
     * the column name for the id_entite field
     */
    const COL_ID_ENTITE = 'asso_entites_archive.id_entite';

    /**
     * the column name for the nom field
     */
    const COL_NOM = 'asso_entites_archive.nom';

    /**
     * the column name for the nomcourt field
     */
    const COL_NOMCOURT = 'asso_entites_archive.nomcourt';

    /**
     * the column name for the adresse field
     */
    const COL_ADRESSE = 'asso_entites_archive.adresse';

    /**
     * the column name for the codepostal field
     */
    const COL_CODEPOSTAL = 'asso_entites_archive.codepostal';

    /**
     * the column name for the ville field
     */
    const COL_VILLE = 'asso_entites_archive.ville';

    /**
     * the column name for the pays field
     */
    const COL_PAYS = 'asso_entites_archive.pays';

    /**
     * the column name for the telephone field
     */
    const COL_TELEPHONE = 'asso_entites_archive.telephone';

    /**
     * the column name for the fax field
     */
    const COL_FAX = 'asso_entites_archive.fax';

    /**
     * the column name for the url field
     */
    const COL_URL = 'asso_entites_archive.url';

    /**
     * the column name for the assujettitva field
     */
    const COL_ASSUJETTITVA = 'asso_entites_archive.assujettitva';

    /**
     * the column name for the email field
     */
    const COL_EMAIL = 'asso_entites_archive.email';

    /**
     * the column name for the created_at field
     */
    const COL_CREATED_AT = 'asso_entites_archive.created_at';

    /**
     * the column name for the updated_at field
     */
    const COL_UPDATED_AT = 'asso_entites_archive.updated_at';

    /**
     * the column name for the archived_at field
     */
    const COL_ARCHIVED_AT = 'asso_entites_archive.archived_at';

    /**
     * The default string format for model objects of the related table
     */
    const DEFAULT_STRING_FORMAT = 'YAML';

    /**
     * holds an array of fieldnames
     *
     * first dimension keys are the type constants
     * e.g. self::$fieldNames[self::TYPE_PHPNAME][0] = 'Id'
     */
    protected static $fieldNames = array (
        self::TYPE_PHPNAME       => array('IdEntite', 'Nom', 'Nomcourt', 'Adresse', 'Codepostal', 'Ville', 'Pays', 'Telephone', 'Fax', 'Url', 'Assujettitva', 'Email', 'CreatedAt', 'UpdatedAt', 'ArchivedAt', ),
        self::TYPE_CAMELNAME     => array('idEntite', 'nom', 'nomcourt', 'adresse', 'codepostal', 'ville', 'pays', 'telephone', 'fax', 'url', 'assujettitva', 'email', 'createdAt', 'updatedAt', 'archivedAt', ),
        self::TYPE_COLNAME       => array(AssoEntitesArchiveTableMap::COL_ID_ENTITE, AssoEntitesArchiveTableMap::COL_NOM, AssoEntitesArchiveTableMap::COL_NOMCOURT, AssoEntitesArchiveTableMap::COL_ADRESSE, AssoEntitesArchiveTableMap::COL_CODEPOSTAL, AssoEntitesArchiveTableMap::COL_VILLE, AssoEntitesArchiveTableMap::COL_PAYS, AssoEntitesArchiveTableMap::COL_TELEPHONE, AssoEntitesArchiveTableMap::COL_FAX, AssoEntitesArchiveTableMap::COL_URL, AssoEntitesArchiveTableMap::COL_ASSUJETTITVA, AssoEntitesArchiveTableMap::COL_EMAIL, AssoEntitesArchiveTableMap::COL_CREATED_AT, AssoEntitesArchiveTableMap::COL_UPDATED_AT, AssoEntitesArchiveTableMap::COL_ARCHIVED_AT, ),
        self::TYPE_FIELDNAME     => array('id_entite', 'nom', 'nomcourt', 'adresse', 'codepostal', 'ville', 'pays', 'telephone', 'fax', 'url', 'assujettitva', 'email', 'created_at', 'updated_at', 'archived_at', ),
        self::TYPE_NUM           => array(0, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, )
    );

    /**
     * holds an array of keys for quick access to the fieldnames array
     *
     * first dimension keys are the type constants
     * e.g. self::$fieldKeys[self::TYPE_PHPNAME]['Id'] = 0
     */
    protected static $fieldKeys = array (
        self::TYPE_PHPNAME       => array('IdEntite' => 0, 'Nom' => 1, 'Nomcourt' => 2, 'Adresse' => 3, 'Codepostal' => 4, 'Ville' => 5, 'Pays' => 6, 'Telephone' => 7, 'Fax' => 8, 'Url' => 9, 'Assujettitva' => 10, 'Email' => 11, 'CreatedAt' => 12, 'UpdatedAt' => 13, 'ArchivedAt' => 14, ),
        self::TYPE_CAMELNAME     => array('idEntite' => 0, 'nom' => 1, 'nomcourt' => 2, 'adresse' => 3, 'codepostal' => 4, 'ville' => 5, 'pays' => 6, 'telephone' => 7, 'fax' => 8, 'url' => 9, 'assujettitva' => 10, 'email' => 11, 'createdAt' => 12, 'updatedAt' => 13, 'archivedAt' => 14, ),
        self::TYPE_COLNAME       => array(AssoEntitesArchiveTableMap::COL_ID_ENTITE => 0, AssoEntitesArchiveTableMap::COL_NOM => 1, AssoEntitesArchiveTableMap::COL_NOMCOURT => 2, AssoEntitesArchiveTableMap::COL_ADRESSE => 3, AssoEntitesArchiveTableMap::COL_CODEPOSTAL => 4, AssoEntitesArchiveTableMap::COL_VILLE => 5, AssoEntitesArchiveTableMap::COL_PAYS => 6, AssoEntitesArchiveTableMap::COL_TELEPHONE => 7, AssoEntitesArchiveTableMap::COL_FAX => 8, AssoEntitesArchiveTableMap::COL_URL => 9, AssoEntitesArchiveTableMap::COL_ASSUJETTITVA => 10, AssoEntitesArchiveTableMap::COL_EMAIL => 11, AssoEntitesArchiveTableMap::COL_CREATED_AT => 12, AssoEntitesArchiveTableMap::COL_UPDATED_AT => 13, AssoEntitesArchiveTableMap::COL_ARCHIVED_AT => 14, ),
        self::TYPE_FIELDNAME     => array('id_entite' => 0, 'nom' => 1, 'nomcourt' => 2, 'adresse' => 3, 'codepostal' => 4, 'ville' => 5, 'pays' => 6, 'telephone' => 7, 'fax' => 8, 'url' => 9, 'assujettitva' => 10, 'email' => 11, 'created_at' => 12, 'updated_at' => 13, 'archived_at' => 14, ),
        self::TYPE_NUM           => array(0, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, )
    );

    /**
     * Initialize the table attributes and columns
     * Relations are not initialized by this method since they are lazy loaded
     *
     * @return void
     * @throws PropelException
     */
    public function initialize()
    {
        // attributes
        $this->setName('asso_entites_archive');
        $this->setPhpName('AssoEntitesArchive');
        $this->setIdentifierQuoting(true);
        $this->setClassName('\\AssoEntitesArchive');
        $this->setPackage('');
        $this->setUseIdGenerator(false);
        // columns
        $this->addPrimaryKey('id_entite', 'IdEntite', 'BIGINT', true, 21, null);
        $this->addColumn('nom', 'Nom', 'VARCHAR', true, 30, null);
        $this->addColumn('nomcourt', 'Nomcourt', 'VARCHAR', true, 6, 'ncourt');
        $this->addColumn('adresse', 'Adresse', 'LONGVARCHAR', false, null, null);
        $this->addColumn('codepostal', 'Codepostal', 'LONGVARCHAR', false, null, null);
        $this->addColumn('ville', 'Ville', 'LONGVARCHAR', false, null, null);
        $this->addColumn('pays', 'Pays', 'VARCHAR', true, 2, 'FR');
        $this->addColumn('telephone', 'Telephone', 'VARCHAR', false, 50, null);
        $this->addColumn('fax', 'Fax', 'VARCHAR', false, 50, null);
        $this->addColumn('url', 'Url', 'LONGVARCHAR', false, null, null);
        $this->addColumn('assujettitva', 'Assujettitva', 'INTEGER', true, 1, 0);
        $this->addColumn('email', 'Email', 'VARCHAR', false, 255, null);
        $this->addColumn('created_at', 'CreatedAt', 'TIMESTAMP', false, null, null);
        $this->addColumn('updated_at', 'UpdatedAt', 'TIMESTAMP', false, null, null);
        $this->addColumn('archived_at', 'ArchivedAt', 'TIMESTAMP', false, null, null);
    } // initialize()

    /**
     * Build the RelationMap objects for this table relationships
     */
    public function buildRelations()
    {
    } // buildRelations()

    /**
     * Retrieves a string version of the primary key from the DB resultset row that can be used to uniquely identify a row in this table.
     *
     * For tables with a single-column primary key, that simple pkey value will be returned.  For tables with
     * a multi-column primary key, a serialize()d version of the primary key will be returned.
     *
     * @param array  $row       resultset row.
     * @param int    $offset    The 0-based offset for reading from the resultset row.
     * @param string $indexType One of the class type constants TableMap::TYPE_PHPNAME, TableMap::TYPE_CAMELNAME
     *                           TableMap::TYPE_COLNAME, TableMap::TYPE_FIELDNAME, TableMap::TYPE_NUM
     *
     * @return string The primary key hash of the row
     */
    public static function getPrimaryKeyHashFromRow($row, $offset = 0, $indexType = TableMap::TYPE_NUM)
    {
        // If the PK cannot be derived from the row, return NULL.
        if ($row[TableMap::TYPE_NUM == $indexType ? 0 + $offset : static::translateFieldName('IdEntite', TableMap::TYPE_PHPNAME, $indexType)] === null) {
            return null;
        }

        return null === $row[TableMap::TYPE_NUM == $indexType ? 0 + $offset : static::translateFieldName('IdEntite', TableMap::TYPE_PHPNAME, $indexType)] || is_scalar($row[TableMap::TYPE_NUM == $indexType ? 0 + $offset : static::translateFieldName('IdEntite', TableMap::TYPE_PHPNAME, $indexType)]) || is_callable([$row[TableMap::TYPE_NUM == $indexType ? 0 + $offset : static::translateFieldName('IdEntite', TableMap::TYPE_PHPNAME, $indexType)], '__toString']) ? (string) $row[TableMap::TYPE_NUM == $indexType ? 0 + $offset : static::translateFieldName('IdEntite', TableMap::TYPE_PHPNAME, $indexType)] : $row[TableMap::TYPE_NUM == $indexType ? 0 + $offset : static::translateFieldName('IdEntite', TableMap::TYPE_PHPNAME, $indexType)];
    }

    /**
     * Retrieves the primary key from the DB resultset row
     * For tables with a single-column primary key, that simple pkey value will be returned.  For tables with
     * a multi-column primary key, an array of the primary key columns will be returned.
     *
     * @param array  $row       resultset row.
     * @param int    $offset    The 0-based offset for reading from the resultset row.
     * @param string $indexType One of the class type constants TableMap::TYPE_PHPNAME, TableMap::TYPE_CAMELNAME
     *                           TableMap::TYPE_COLNAME, TableMap::TYPE_FIELDNAME, TableMap::TYPE_NUM
     *
     * @return mixed The primary key of the row
     */
    public static function getPrimaryKeyFromRow($row, $offset = 0, $indexType = TableMap::TYPE_NUM)
    {
        return (string) $row[
            $indexType == TableMap::TYPE_NUM
                ? 0 + $offset
                : self::translateFieldName('IdEntite', TableMap::TYPE_PHPNAME, $indexType)
        ];
    }

    /**
     * The class that the tableMap will make instances of.
     *
     * If $withPrefix is true, the returned path
     * uses a dot-path notation which is translated into a path
     * relative to a location on the PHP include_path.
     * (e.g. path.to.MyClass -> 'path/to/MyClass.php')
     *
     * @param boolean $withPrefix Whether or not to return the path with the class name
     * @return string path.to.ClassName
     */
    public static function getOMClass($withPrefix = true)
    {
        return $withPrefix ? AssoEntitesArchiveTableMap::CLASS_DEFAULT : AssoEntitesArchiveTableMap::OM_CLASS;
    }

    /**
     * Populates an object of the default type or an object that inherit from the default.
     *
     * @param array  $row       row returned by DataFetcher->fetch().
     * @param int    $offset    The 0-based offset for reading from the resultset row.
     * @param string $indexType The index type of $row. Mostly DataFetcher->getIndexType().
                                 One of the class type constants TableMap::TYPE_PHPNAME, TableMap::TYPE_CAMELNAME
     *                           TableMap::TYPE_COLNAME, TableMap::TYPE_FIELDNAME, TableMap::TYPE_NUM.
     *
     * @throws PropelException Any exceptions caught during processing will be
     *                         rethrown wrapped into a PropelException.
     * @return array           (AssoEntitesArchive object, last column rank)
     */
    public static function populateObject($row, $offset = 0, $indexType = TableMap::TYPE_NUM)
    {
        $key = AssoEntitesArchiveTableMap::getPrimaryKeyHashFromRow($row, $offset, $indexType);
        if (null !== ($obj = AssoEntitesArchiveTableMap::getInstanceFromPool($key))) {
            // We no longer rehydrate the object, since this can cause data loss.
            // See http://www.propelorm.org/ticket/509
            // $obj->hydrate($row, $offset, true); // rehydrate
            $col = $offset + AssoEntitesArchiveTableMap::NUM_HYDRATE_COLUMNS;
        } else {
            $cls = AssoEntitesArchiveTableMap::OM_CLASS;
            /** @var AssoEntitesArchive $obj */
            $obj = new $cls();
            $col = $obj->hydrate($row, $offset, false, $indexType);
            AssoEntitesArchiveTableMap::addInstanceToPool($obj, $key);
        }

        return array($obj, $col);
    }

    /**
     * The returned array will contain objects of the default type or
     * objects that inherit from the default.
     *
     * @param DataFetcherInterface $dataFetcher
     * @return array
     * @throws PropelException Any exceptions caught during processing will be
     *                         rethrown wrapped into a PropelException.
     */
    public static function populateObjects(DataFetcherInterface $dataFetcher)
    {
        $results = array();

        // set the class once to avoid overhead in the loop
        $cls = static::getOMClass(false);
        // populate the object(s)
        while ($row = $dataFetcher->fetch()) {
            $key = AssoEntitesArchiveTableMap::getPrimaryKeyHashFromRow($row, 0, $dataFetcher->getIndexType());
            if (null !== ($obj = AssoEntitesArchiveTableMap::getInstanceFromPool($key))) {
                // We no longer rehydrate the object, since this can cause data loss.
                // See http://www.propelorm.org/ticket/509
                // $obj->hydrate($row, 0, true); // rehydrate
                $results[] = $obj;
            } else {
                /** @var AssoEntitesArchive $obj */
                $obj = new $cls();
                $obj->hydrate($row);
                $results[] = $obj;
                AssoEntitesArchiveTableMap::addInstanceToPool($obj, $key);
            } // if key exists
        }

        return $results;
    }
    /**
     * Add all the columns needed to create a new object.
     *
     * Note: any columns that were marked with lazyLoad="true" in the
     * XML schema will not be added to the select list and only loaded
     * on demand.
     *
     * @param Criteria $criteria object containing the columns to add.
     * @param string   $alias    optional table alias
     * @throws PropelException Any exceptions caught during processing will be
     *                         rethrown wrapped into a PropelException.
     */
    public static function addSelectColumns(Criteria $criteria, $alias = null)
    {
        if (null === $alias) {
            $criteria->addSelectColumn(AssoEntitesArchiveTableMap::COL_ID_ENTITE);
            $criteria->addSelectColumn(AssoEntitesArchiveTableMap::COL_NOM);
            $criteria->addSelectColumn(AssoEntitesArchiveTableMap::COL_NOMCOURT);
            $criteria->addSelectColumn(AssoEntitesArchiveTableMap::COL_ADRESSE);
            $criteria->addSelectColumn(AssoEntitesArchiveTableMap::COL_CODEPOSTAL);
            $criteria->addSelectColumn(AssoEntitesArchiveTableMap::COL_VILLE);
            $criteria->addSelectColumn(AssoEntitesArchiveTableMap::COL_PAYS);
            $criteria->addSelectColumn(AssoEntitesArchiveTableMap::COL_TELEPHONE);
            $criteria->addSelectColumn(AssoEntitesArchiveTableMap::COL_FAX);
            $criteria->addSelectColumn(AssoEntitesArchiveTableMap::COL_URL);
            $criteria->addSelectColumn(AssoEntitesArchiveTableMap::COL_ASSUJETTITVA);
            $criteria->addSelectColumn(AssoEntitesArchiveTableMap::COL_EMAIL);
            $criteria->addSelectColumn(AssoEntitesArchiveTableMap::COL_CREATED_AT);
            $criteria->addSelectColumn(AssoEntitesArchiveTableMap::COL_UPDATED_AT);
            $criteria->addSelectColumn(AssoEntitesArchiveTableMap::COL_ARCHIVED_AT);
        } else {
            $criteria->addSelectColumn($alias . '.id_entite');
            $criteria->addSelectColumn($alias . '.nom');
            $criteria->addSelectColumn($alias . '.nomcourt');
            $criteria->addSelectColumn($alias . '.adresse');
            $criteria->addSelectColumn($alias . '.codepostal');
            $criteria->addSelectColumn($alias . '.ville');
            $criteria->addSelectColumn($alias . '.pays');
            $criteria->addSelectColumn($alias . '.telephone');
            $criteria->addSelectColumn($alias . '.fax');
            $criteria->addSelectColumn($alias . '.url');
            $criteria->addSelectColumn($alias . '.assujettitva');
            $criteria->addSelectColumn($alias . '.email');
            $criteria->addSelectColumn($alias . '.created_at');
            $criteria->addSelectColumn($alias . '.updated_at');
            $criteria->addSelectColumn($alias . '.archived_at');
        }
    }

    /**
     * Returns the TableMap related to this object.
     * This method is not needed for general use but a specific application could have a need.
     * @return TableMap
     * @throws PropelException Any exceptions caught during processing will be
     *                         rethrown wrapped into a PropelException.
     */
    public static function getTableMap()
    {
        return Propel::getServiceContainer()->getDatabaseMap(AssoEntitesArchiveTableMap::DATABASE_NAME)->getTable(AssoEntitesArchiveTableMap::TABLE_NAME);
    }

    /**
     * Add a TableMap instance to the database for this tableMap class.
     */
    public static function buildTableMap()
    {
        $dbMap = Propel::getServiceContainer()->getDatabaseMap(AssoEntitesArchiveTableMap::DATABASE_NAME);
        if (!$dbMap->hasTable(AssoEntitesArchiveTableMap::TABLE_NAME)) {
            $dbMap->addTableObject(new AssoEntitesArchiveTableMap());
        }
    }

    /**
     * Performs a DELETE on the database, given a AssoEntitesArchive or Criteria object OR a primary key value.
     *
     * @param mixed               $values Criteria or AssoEntitesArchive object or primary key or array of primary keys
     *              which is used to create the DELETE statement
     * @param  ConnectionInterface $con the connection to use
     * @return int             The number of affected rows (if supported by underlying database driver).  This includes CASCADE-related rows
     *                         if supported by native driver or if emulated using Propel.
     * @throws PropelException Any exceptions caught during processing will be
     *                         rethrown wrapped into a PropelException.
     */
     public static function doDelete($values, ConnectionInterface $con = null)
     {
        if (null === $con) {
            $con = Propel::getServiceContainer()->getWriteConnection(AssoEntitesArchiveTableMap::DATABASE_NAME);
        }

        if ($values instanceof Criteria) {
            // rename for clarity
            $criteria = $values;
        } elseif ($values instanceof \AssoEntitesArchive) { // it's a model object
            // create criteria based on pk values
            $criteria = $values->buildPkeyCriteria();
        } else { // it's a primary key, or an array of pks
            $criteria = new Criteria(AssoEntitesArchiveTableMap::DATABASE_NAME);
            $criteria->add(AssoEntitesArchiveTableMap::COL_ID_ENTITE, (array) $values, Criteria::IN);
        }

        $query = AssoEntitesArchiveQuery::create()->mergeWith($criteria);

        if ($values instanceof Criteria) {
            AssoEntitesArchiveTableMap::clearInstancePool();
        } elseif (!is_object($values)) { // it's a primary key, or an array of pks
            foreach ((array) $values as $singleval) {
                AssoEntitesArchiveTableMap::removeInstanceFromPool($singleval);
            }
        }

        return $query->delete($con);
    }

    /**
     * Deletes all rows from the asso_entites_archive table.
     *
     * @param ConnectionInterface $con the connection to use
     * @return int The number of affected rows (if supported by underlying database driver).
     */
    public static function doDeleteAll(ConnectionInterface $con = null)
    {
        return AssoEntitesArchiveQuery::create()->doDeleteAll($con);
    }

    /**
     * Performs an INSERT on the database, given a AssoEntitesArchive or Criteria object.
     *
     * @param mixed               $criteria Criteria or AssoEntitesArchive object containing data that is used to create the INSERT statement.
     * @param ConnectionInterface $con the ConnectionInterface connection to use
     * @return mixed           The new primary key.
     * @throws PropelException Any exceptions caught during processing will be
     *                         rethrown wrapped into a PropelException.
     */
    public static function doInsert($criteria, ConnectionInterface $con = null)
    {
        if (null === $con) {
            $con = Propel::getServiceContainer()->getWriteConnection(AssoEntitesArchiveTableMap::DATABASE_NAME);
        }

        if ($criteria instanceof Criteria) {
            $criteria = clone $criteria; // rename for clarity
        } else {
            $criteria = $criteria->buildCriteria(); // build Criteria from AssoEntitesArchive object
        }


        // Set the correct dbName
        $query = AssoEntitesArchiveQuery::create()->mergeWith($criteria);

        // use transaction because $criteria could contain info
        // for more than one table (I guess, conceivably)
        return $con->transaction(function () use ($con, $query) {
            return $query->doInsert($con);
        });
    }

} // AssoEntitesArchiveTableMap
// This is the static code needed to register the TableMap for this table with the main Propel class.
//
AssoEntitesArchiveTableMap::buildTableMap();
