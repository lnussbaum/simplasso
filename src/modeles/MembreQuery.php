<?php

use Base\MembreQuery as BaseMembreQuery;
use Propel\Runtime\ActiveQuery\Criteria as Criteria;

/**
 * Skeleton subclass for performing query and update operations on the 'asso_membres' table.
 *
 *
 *
 * You should add additional methods to this class to meet the
 * application requirements.  This class will only be generated as
 * long as it does not already exist in the output directory.
 *
 */
class MembreQuery extends BaseMembreQuery
{

    public static function getTabLiaison($code = null)
    {
        $tab = ['individu' => ['objet' => 'individu', 'local' => 'id_individu_titulaire', 'foreign' => 'id_individu']];
        return getValeur($tab, $code);
    }

    public static function getWhere($params = array())
    {


        $pr = descr('membre.nom_sql');
        $pr_i = descr('individu.nom_sql');
        $tables = array($pr => descr('membre.table_sql'));
        $where = "1=1";
        $where .= objet_selection_restriction_mot('membre');
        // La recherche
        if ($search = request_ou_options('search', $params)) {

            $recherche = trim($search['value']);


            if (!empty($recherche)) {

                $colle = 'AND';
                if (intval($recherche) > 0) {
                    $where = 'id_membre = \'' . $recherche . '\'';
                    $colle = 'OR';
                }

                $recherche_m = '+' . str_replace(' ', ' +', str_replace(array('@', '.'), ' ', $recherche)) . "*";
                $where .= ' ' . $colle . ' ( ' . $pr . '.nom like \'%' . $recherche . '%\'';
                $where .= ' OR ' . $pr . '.id_membre IN (select al.id_membre from asso_individus as `' . $pr_i . '`, asso_membres_individus as `al` where ' . $pr_i . '.id_individu = al.id_individu'
                    . ' AND ( email like \'%' . $recherche . '%\'  OR ( MATCH (nom_famille,prenom) AGAINST (\'' . $recherche_m . '\' IN BOOLEAN MODE)))))';
            }
        }


        // Sorti ou non de l'association

        $sorti = request_ou_options('sorti', $params);

        switch ($sorti) {
            case 'oui':
                $where .= ' AND YEAR(' . $pr . '.date_sortie) IS NOT NULL';
                break;
            case 'non':
                $where .= ' AND ' . $pr . '.date_sortie IS NULL';
                break;
            default :
                $where .= '';
                break;
        };


        // L'appartenance à une association (entité)

        if ($tab_entites = request_ou_options('asso_entites', $params)) {

            foreach ($tab_entites as $entite) {
                $where .= ' AND (select count(asr.id_servicerendu) from asso_servicerendus asr`
                where ' . $pr . '.id_membre = asr.id_membre  and asr.id_entite=' . intval($entite) . ' ) > 0';
            }

        }

        // A jour de cotisation ???

        if ($statut_cotis = request_ou_options('cotisation', $params)) {
            $tab_prestations = array_keys(getPrestationEntite('cotisation'));

            //Ajout au where de quelques clause
            $where_tmp = ' AND (select count(sr.id_servicerendu) from asso_servicerendus as `sr` where ' . $pr . '.id_membre = sr.id_membre  and id_prestation IN (' . implode(',',
                    $tab_prestations) . ')';

            switch ($statut_cotis) {
                case 'jamais' :
                    $where .= $where_tmp . ') = 0 ';
                    break;
                case 'echu' :
                    $where .= $where_tmp . ' AND  sr.date_fin < NOW()) > 0 ' . $where_tmp . ' AND sr.date_fin > NOW()) = 0 ';
                    break;
                case 'ok' :
                    $where .= $where_tmp . ' AND  sr.date_fin > NOW() AND sr.date_debut < NOW()) = 1';
                    break;
                case 'futur' :
                    $where .= $where_tmp . ' AND  sr.date_debut > NOW()) > 0 ' . $where_tmp . ' AND sr.date_fin < NOW()) = 0';
                    break;
            }

        }


        // A jour de cotisation sur une periode donnée

        if ($tab_periode_cotisation = request_ou_options('periode_cotisation', $params)) {

            $tab_prestations = array_keys(getPrestationEntite('cotisation'));
            foreach ($tab_periode_cotisation as $periode_cotisation) {
                $date_debut = tab('prestation_type_calendrier.cotisation.' . $periode_cotisation . '.date');
                $date_debut = $date_debut->format('Y-m-d');
                //Ajout au where de quelques clause
                $where_tmp = ' AND (select count(sr.id_servicerendu) from asso_servicerendus as `sr` where ' . $pr . '.id_membre = sr.id_membre  and id_prestation IN (' . implode(',',
                        $tab_prestations) . ')';
                $where .= $where_tmp . ' AND  sr.date_fin > \'' . $date_debut . '\' AND sr.date_debut <= \'' . $date_debut . '\') = 1';
            }

        }


        // Abonnenment
        $tab_abonenement = getPrestationDeType('abonnement');
        foreach ($tab_abonenement as $id_prestation => $abonnement) {
            if ($tab_statut_abonnement = request_ou_options('abonnement' . $id_prestation, $params)) {
                //Ajout au where de quelques clause
                $tab_statut_abonnement = is_array($tab_statut_abonnement) ? $tab_statut_abonnement : [$tab_statut_abonnement];
                $tab_where_tmp = [];
                $where_tmp = ' ( select count(sr.id_servicerendu) from asso_servicerendus as `sr` where ' . $pr . '.id_membre = sr.id_membre  and id_prestation = ' . $id_prestation;
                foreach ($tab_statut_abonnement as $statut_abonnement) {
                    $where_tmp0 = '';
                    switch ($statut_abonnement) {
                        case 'jamais' :
                            $where_tmp0 .= $where_tmp . ') = 0 ';
                            break;
                        case 'echu' :
                            $where_tmp0 .= $where_tmp . ' AND  sr.date_fin < NOW()) > 0 AND' . $where_tmp . ' AND sr.date_fin > NOW()) = 0 ';
                            break;
                        case 'ok' :
                            $where_tmp0 .= $where_tmp . ' AND  sr.date_fin > NOW() AND sr.date_debut < NOW()) = 1';
                            break;
                        case 'futur' :
                            $where_tmp0 .= $where_tmp . ' AND  sr.date_debut > NOW()) > 0 AND' . $where_tmp . ' AND sr.date_fin < NOW()) = 0';
                            break;
                    }
                    $tab_where_tmp[] = $where_tmp0;
                }

                if (!empty($tab_where_tmp)) {
                    $where .= ' AND ((' . implode(') OR (', $tab_where_tmp) . '))';
                }
            }

        }


        $motgroupe_individu = ['mots'=>'OR'] + liste_motgroupe('individu');

        foreach ($motgroupe_individu as $mg => $mg_operateur_par_defaut) {
            if ($mots = request_ou_options('ind_' . $mg, $params)) {
                if (!is_array($mots)) {
                    $mots = [$mots];
                }
                $op = $mg_operateur_par_defaut == "AND" ? '= ' . count($mots) : '> 0';
                $where .= ' AND ' . $pr . '.id_membre IN (select al.id_membre from asso_individus as `' . $pr_i . '`, asso_membres_individus as `al` where ' . $pr_i . '.id_individu = al.id_individu'
                    . ' AND (select count(ml' . $mg . '.id_mot) from asso_mots_liens ml' . $mg . ' where ml' . $mg . '.id_mot IN (' . implode(',',
                        $mots) . ') AND ml' . $mg . '.id_objet=' . $pr_i . '.id_individu and ml' . $mg . '.objet=\'individu\' )' . $op . ')';
            }
        }


        $motgroupe = ['mots'=>'OR'] + liste_motgroupe('membre');

        foreach ($motgroupe as $mg => $mg_operateur_par_defaut) {
            if ($mots = request_ou_options($mg, $params)) {
                if (!is_array($mots)) {
                    $mots = [$mots];
                }
                $op = $mg_operateur_par_defaut == "AND" ? '= ' . count($mots) : '> 0';
                $where .= ' AND (select count(ml' . $mg . '.id_mot) from asso_mots_liens ml' . $mg . ' where ml' . $mg . '.id_mot IN (' . implode(',',
                        $mots) . ') AND ml' . $mg . '.id_objet=' . $pr . '.id_membre and ml' . $mg . '.objet=\'membre\' )' . $op;
            }
        }

// Filtres géographiques
// Régions, Départements, Arrondissements,communes
        $where_geo = '';
        if ($geo_region = request_ou_options('region', $params)) {
            $where_geo .= " AND region IN (" . implode(',', $geo_region) . ')';
        }
        if ($geo_dep = request_ou_options('departement', $params)) {
            $where_geo .= " AND departement IN (" . implode(',', $geo_dep) . ')';
        }
        if ($geo_arr = request_ou_options('arrondissement', $params)) {
            $where_geo .= " AND arrondissement IN (" . implode(',', $geo_arr) . ')';
        }
        if ($geo_com = request_ou_options('commune', $params)) {
            $where_geo .= " AND id_commune IN (" . implode(',', $geo_com) . ')';
        }
        $select_geo = 'select id_commune from geo_communes where 1=1 ' . $where_geo;

        if (!empty($where_geo)) {
            $tables[$pr_i] = 'asso_individus';
            $tables['ccl'] = 'geo_communes_liens';
            $where .= ' AND ' . $pr . '.id_individu_titulaire=' . $pr_i . '.id_individu AND ccl.objet=\'individu\' AND ccl.id_objet=' . $pr_i . '.id_individu AND ccl.id_commune IN (' . $select_geo . ')';

        }


        //Filtre Zone
        if ($geo_zone = request_ou_options('zone', $params)) {
            $tables[$pr_i] = 'asso_individus';
            $tables['zl'] = 'geo_zones_liens';
            $where .= ' AND ' . $pr . '.id_individu_titulaire=' . $pr_i . '.id_individu AND zl.objet=\'individu\' AND zl.id_objet=' . $pr_i . '.id_individu AND zl.id_zone IN (' . implode(',',
                    $geo_zone) . ')';

        }


        $filtres_coords = ['email', 'adresse', 'telephone', 'telephone_pro', 'mobile'];


        foreach ($filtres_coords as $fc) {

            if ($filtre_c_valeur = request_ou_options($fc, $params)) {

                if ($filtre_c_valeur == 'oui') {
                    $tables[$pr_i] = 'asso_individus';
                    $where .= ' AND ' . $pr . '.id_individu_titulaire=' . $pr_i . '.id_individu AND ' . $pr_i . '.' . $fc . '!=\'\' ';
                } elseif ($filtre_c_valeur == 'non') {
                    $tables[$pr_i] = 'asso_individus';
                    $where .= ' AND ' . $pr . '.id_individu_titulaire=' . $pr_i . '.id_individu AND (' . $pr_i . '.' . $fc . '=\'\' OR  ' . $pr_i . '.' . $fc . ' IS NULL)';
                } elseif ($filtre_c_valeur == 'npai') {
                    $code = 'npai_' . substr($fc, 0, 5);
                    if ($fc == 'telephone_pro') {
                        $code = 'npai_tele2';
                    }
                    $id_mot = table_filtrer_valeur_premiere(tab('mot'), 'nomcourt', $code)['id_mot'];
                    $where .= ' and EXISTS (select * from asso_mots_liens ml' . $fc . ' where ml' . $fc . '.id_mot =' . $id_mot . ' and ml' . $fc . '.id_objet=' . $pr . '.id_individu_titulaire and ml' . $fc . '.objet=\'individu\')';
                }
            }
        }

        // Filtre carte


        if ($carte = request_ou_options('carte', $params)) {
            $mg = 'carte';
            $where .= ' AND (select count(ml' . $mg . '.id_mot) from asso_mots_liens ml' . $mg . ' where ml' . $mg . '.id_mot IN (' .
                $carte . ') AND ml' . $mg . '.id_objet=' . $pr . '.id_membre and ml' . $mg . '.objet=\'membre\' )=1';
        }


        if ($tab_log = request_ou_options('log_impcar', $params)) {
            $tab_membre_log = LogLienQuery::create()->filterByObjet('membre')->findByIdLog($tab_log)->toKeyValue('idObjet',
                'idObjet');
            if (!empty($tab_membre_log)) {
                $where .= ' AND ' . $pr . '.id_membre IN (' . implode(',', $tab_membre_log) . ')';
            }
        }


        // Filtre adhérent ardent

        if ($ardent = request_ou_options('ardent', $params)) {
            $id_entite = 1;
            $tab_prestations = array_keys(getPrestationDeType('cotisation'));
            $nb_annee = request_ou_options('ardent_nb_annee');
            if ($nb_annee < 1) {
                $nb_annee = 4;
            }
            //Ajout au where de quelques clause
            if ($ardent == 'non') {
                $where_tmp = '(select count(asr.id_servicerendu) from asso_servicerendus as `asr` where ' . $pr . '.id_membre = asr.id_membre  and id_prestation IN (' . implode(',',
                        $tab_prestations) . ')';
                $where .= ' AND NOT (' . $where_tmp . ' AND (YEAR(date_fin) > (YEAR(NOW())-' . $nb_annee . ')) > 0 ))';

            } else {
                $where_tmp = ' AND (select count(asr.id_servicerendu) from asso_servicerendus as `asr` where ' . $pr . '.id_membre = asr.id_membre  and id_prestation IN (' . implode(',',
                        $tab_prestations) . ')';
                $where .= $where_tmp . ' AND (YEAR(date_fin) > (YEAR(NOW())-' . ($nb_annee) . ')) > 0)';
            }

        }

        foreach ($tables as $k => $t) {
            $from[$k] = $t . ' ' . $k;
        }

        $where = selection_ajouter_limitation_id('membre', $where, $params);

        $filtre_date_cotisation = request_ou_options('date_cotisation', $params);
        if ($filtre_date_cotisation) {
            $table['individu'] = 'asso_individus';
            $table['cotisation'] = 'asso_cotisations';
            $where .= 'AND individu.id_individu = m.id_membre AND cotisation.id_membre=m.id_membre AND cotisation.date_cotisation>=' . sql_quote(substr($filtre_date_cotisation,
                        6, 4) . '-' . substr($filtre_date_cotisation, 3, 2) . '-' . substr($filtre_date_cotisation, 0,
                        2));
        }
        if (request_ou_options('inverse_selection', $params)) {
            $where = 'NOT (' . $where . ')';
        }
        return [$from, $where];


    }


    public static function getAll($tab_id_membre, $order = array(), $offset = 0, $limit = 0, $options = array())
    {
        $q = MembreQuery::create();

        if (isset($options['not_in']) && $options['not_in']) {
            $q = $q->filterBy('IdMembre', $tab_id_membre, Criteria::NOT_IN);
        } else {
            $q = $q->filterBy('IdMembre', $tab_id_membre, Criteria::IN);
        }

        $q = $q
            ->useIndividuTitulaireQuery()
            ->endUse()
            ->leftJoinWithServicerendu();
        if (!empty($order) && is_array($order)) {
            foreach ($order as $k => $o) {
                $table = '';
                if (!(strpos($k, '.') > 0)) {
                    $table = 'asso_membres.';
                }
                if (strtoupper($o) == 'ASC') {
                    $q = $q->addAscendingOrderByColumn($table . $k);
                } else {
                    $q = $q->addDescendingOrderByColumn($table . $k);
                }
            }
        }
        if ($offset > 0) {
            $q = $q->offset($offset);
        }
        if ($limit > 0) {
            $q = $q->limit($limit);
        }

        return $q->find();


    }


    static function getStat()
    {
//        Global $app;
        $stat = array();
        list($sql, $stat['nb']) = getSelectionObjetNb('membre', array('sorti' => 'non'));
        if ($stat['nb'] > 0) {
            list($sql, $stat['nb_ok']) = getSelectionObjetNb('membre', array('sorti' => 'non', 'cotisation' => 'ok'));

        } else {
            $stat['nb_ok'] = 0;
        }
        //pour depannage partiel yoga
//        $stat['nb']=$app['db']->fetchColumn("SELECT COUNT(*) as nb_total FROM asso_membres membre WHERE (select count(ml.id_mot) from asso_mots_liens ml where ml.id_mot =".suc('operateur.id')." AND ml.id_objet = membre.id_membre AND ml.objet='membre')>0 AND membre.date_sortie IS NULL");
//        $stat['nb_ok']=$app['db']->fetchColumn("SELECT COUNT(*) as nb_total FROM asso_membres membre WHERE (select count(ml.id_mot) from asso_mots_liens ml where ml.id_mot = ".suc('operateur.id')." AND ml.id_objet = membre.id_membre AND ml.objet='membre')>0 AND membre.date_sortie IS NULL AND (select count(sr.id_servicerendu) from asso_servicerendus as `sr` where membre.id_membre = sr.id_membre and id_prestation IN (13,1,4,7,10,22,16) AND sr.date_fin > NOW() AND sr.date_debut < NOW()) = 1 ") ;

        return $stat;

    }


    static function genererNomCourt($nom, $prenom)
    {
        $nom = str_replace(array('\''), '', $nom);
        $nom = str_replace(array(' '), '', $nom);
        $prenom = str_replace(array(' '), '', $prenom);
        $longeur_max = max(5 - strlen($prenom), 3);
        $nom = strtolower(substr(substr($nom, 0, $longeur_max) . $prenom, 0, 5));
        return $nom;
    }


    /**
     * liste_filtre_statut_cotis
     * @param $where
     * @param int $id_entite
     * @return array
     */
    static function liste_filtre_statut_cotis($where, $id_entite = 1)
    {

        global $app;
        $tab_prestations = array_keys(getPrestationEntite('cotisation'));

        $tab_statut_cotis = array();
        $sel_col = 'SELECT count(am.id_membre) FROM asso_membres am WHERE am.id_membre IN (' . $where . ')';

        $where_tmp = ' AND (select count(ass.id_servicerendu) from asso_servicerendus ass where am.id_membre = ass.id_membre and id_prestation IN (' . implode(',',
                $tab_prestations) . ')';

        //	case 'jamais' :
        $where_tmp2 = $where_tmp . ')=0';
        $nb = $app['db']->fetchColumn($sel_col . $where_tmp2);
        $tab_statut_cotis['jamais'] = $nb;
        //	case 'echu' :
        $where_tmp2 = $where_tmp . ' AND  date_fin < NOW()) > 0 ' . $where_tmp . ' AND date_fin > NOW()) = 0 ';
        $nb = $app['db']->fetchColumn($sel_col . $where_tmp2);
        $tab_statut_cotis['echu'] = $nb;
        //	case 'ok' :
        $where_tmp2 = $where_tmp . ' AND  date_fin > NOW() AND date_debut < NOW()) = 1';
        $nb = $app['db']->fetchColumn($sel_col . $where_tmp2);
        $tab_statut_cotis['ok'] = $nb;
        //	case 'futur' :
        $where_tmp2 = $where_tmp . ' AND  date_debut > NOW()) > 0 ' . $where_tmp . ' AND date_fin < NOW()) = 0';
        $nb = $app['db']->fetchColumn($sel_col . $where_tmp2);
        $tab_statut_cotis['futur'] = $nb;

        return $tab_statut_cotis;
    }


}
