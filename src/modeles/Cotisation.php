<?php

use \Prestation;
use Map\PrestationTableMap;


/**
 * Skeleton subclass for representing a row from one of the subclasses of the 'asso_prestations' table.
 *
 *
 *
 * You should add additional methods to this class to meet the
 * application requirements.  This class will only be generated as
 * long as it does not already exist in the output directory.
 *
 */
class Cotisation extends Prestation
{

    /**
     * Constructs a new Cotisation class, setting the prestation_type column to PrestationTableMap::CLASSKEY_1.
     */
    public function __construct()
    {
        parent::__construct();
        $this->setPrestationType(PrestationTableMap::CLASSKEY_1);
    }

    static function getCount(){

        $tab_cotisations = CotisationQuery::create()->find()->toKeyValue('IdPrestation','IdPrestation');
        $query = ServicerenduQuery::create()->filterByIdPrestation($tab_cotisations);
        return  $query->count();
    }

    public function getQuiCree(){
        return Log::QuiCree('prestation',$this->id_prestation);
    }

    public function getQuiModifie(){
        return Log::QuiMetAJour('prestation',$this->id_prestation);
    }


} // Cotisation
