<?php

use Base\PrixQuery as BasePrixQuery;

/**
 * Skeleton subclass for performing query and update operations on the 'asso_prixs' table.
 *
 *
 *
 * You should add additional methods to this class to meet the
 * application requirements.  This class will only be generated as
 * long as it does not already exist in the output directory.
 *
 */
class PrixQuery extends BasePrixQuery
{

    public static $champs_recherche = ['observation'];

    public static function getWhere($params = array()) {
        list($from, $where) = getWhereObjet('prix', $params);
        $pr = descr('prix.nom_sql');
        if ($id_zonegroupe = request_ou_options('id_prestation', $params)){
            $where .= ' AND '.$pr.'.id_prestation ='.$id_zonegroupe;
        };
        return [$from, $where];
    }


    public static function getAll($tab_id, $order = array(), $offset = 0, $limit = 0) {
        return getAllObjet('prix', $tab_id, $order, $offset, $limit);
    }



}
