<?php

use Base\Tresor as BaseTresor;

/**
 * Skeleton subclass for representing a row from the 'asso_tresors' table.
 *
 *
 *
 * You should add additional methods to this class to meet the
 * application requirements.  This class will only be generated as
 * long as it does not already exist in the output directory.
 *
 */
class Tresor extends BaseTresor
{
    public function getQuiCree(){
        return Log::getQuiCree('tresor',$this->id_tresor);
    }

    public function getQuiModifie(){
        return Log::getQuiModifie('tresor',$this->id_tresor);
    }

}
