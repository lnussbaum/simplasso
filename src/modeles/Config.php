<?php

use Base\Config as BaseConfig;

/**
 * Skeleton subclass for representing a row from the 'asso_configs' table.
 *
 *
 *
 * You should add additional methods to this class to meet the
 * application requirements.  This class will only be generated as
 * long as it does not already exist in the output directory.
 *
 */
class Config extends BaseConfig
{
    function getValeur()
    {
        return special_json_decode($this->variables);
    }

    function setValeur($valeur){
        return $this->setVariables(json_encode($valeur));
    }

}
