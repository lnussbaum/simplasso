<?php

use Base\CourrierLien as BaseCourrierLien;

/**
 * Skeleton subclass for representing a row from the 'asso_courriers_liens' table.
 *
 *
 *
 * You should add additional methods to this class to meet the
 * application requirements.  This class will only be generated as
 * long as it does not already exist in the output directory.
 *
 */
class CourrierLien extends BaseCourrierLien
{
    public static function getAll($tab_id,$id_courrier='',$canal='',$envoyer='',$order=array(),$offset=0,$limit=0)
    {

        $q = CourrierLienQuery::create();
        if( !empty($tab_id) )
            $q = $q->filterByPrimaryKeys($tab_id);
        if( $id_courrier!='' )
            $q =  $q->filterByIdCourrier($id_courrier);
        if( $canal!='' )
            $q =  $q->filterByCanal($canal);
        if( $envoyer===true )
            $q =  $q->filterByDateEnvoi(null,Criteria::ISNOTNULL);
        elseif( $envoyer===false )
            $q =  $q->filterByDateEnvoi(null,Criteria::ISNULL);
        $q = $q->useIndividuQuery()
            ->endUse()
            ->leftJoin('Individu');

        if (!empty($order) && is_array($order)) {
            foreach($order as $k=>$o){
                $table='';
                if (!(strpos($k,'.')>0))
                    $table = 'asso_courriers_liens.';
                if (strtoupper($o) =='ASC')
                    $q = $q->addAscendingOrderByColumn($table.$k);
                else
                    $q = $q->addDescendingOrderByColumn($table.$k);
            }

        }
        if ($offset > 0)
            $q=$q->offset($offset);
        if ($limit > 0)
            $q=$q->limit($limit);

        return $q->find();


    }


}
