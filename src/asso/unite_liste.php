<?php
function unite_liste()
{
    global $app;
    $args_twig=[
        'tab_col'=>unite_colonnes()
        ];
    return $app['twig']->render(fichier_twig(), $args_twig);

}


function unite_colonnes(){

    $tab_colonne= array();
	$tab_colonne['id_unite'] = ['title'=> 'id',];
	$tab_colonne['nom'] = [];
	$tab_colonne['nomcourt'] = [];
	$tab_colonne['actif'] = [];
	$tab_colonne['nombre'] = [];
	$tab_colonne['observation'] = [];
	$tab_colonne['created_at'] = ['type' => 'date-eu'];

    $tab_colonne['action']=["orderable"=> false];
    $tab_colonne = datatable_complete_liste_colonne($tab_colonne);

    return $tab_colonne;

}





function action_unite_liste_dataliste()
{
    return objet_liste_dataliste();

}


