<?php

use mikehaertl\wkhtmlto\Pdf;

function paiement_liste()
{
    global $app;
// effacement de hello asso crée manuellemnt avant reprise en fin de fichier
    $args_twig = [
        'tab_col' => paiement_colonnes(),
        'tab_filtres' => paiement_filtres(),
    ];

    return $app['twig']->render(fichier_twig(), $args_twig);
}

function paiement_colonnes()
{
    $tab_colonne = array();
    $tab_colonne['id_paiement'] = ['title' => 'id'];

    $tab_colonne['date_enregistrement'] = ["type" => "date-eu"];
    if (sac('entite_multi')) {
        $tab_colonne['id_entite'] = [];
    }
    $tab_colonne['beneficiaire'] = ['traitement' => 'genererLienBeneficiaire'];
    $tab_colonne['id_tresor'] = ['traitement' => ['transformeTresor']];
    $tab_colonne['numero'] = [];
    $tab_colonne['date_cheque'] = ["type" => "date-eu"];
    $tab_colonne['montant'] = [];
    $tab_colonne['remise'] = [];
    $tab_colonne['comptabilise'] = ['traitement' => ['transformeOuiNon']];
    $tab_colonne['observation'] = [];

    $tab_colonne['action'] = ["orderable" => false];
    $tab_colonne = datatable_complete_liste_colonne($tab_colonne);

    return $tab_colonne;
}

function paiement_filtres()
{

    global $app;
    $request = $app['request'];
    $rechercher = $request->get('search');
    $tab_filtres['premier']['search'] = filtre_ajouter_recherche($rechercher['value']);
    $statut = $request->get('statut');
    $tab_statut = [
        trans('depose') => 'depose',
        trans('en_attente') => 'en_attente',
        trans('valide') => 'valide'
    ];
    $tab_filtres['premier']['statut'] = $filtre_prestation = filtre_prepare_donnee('statut', $tab_statut, $statut);

    return $tab_filtres;
}

function paiement_dataliste_complement($tab, $tab_data, $only_values = true)
{

    foreach ($tab as $k => $sr) {
        $m_objet = $sr->getObjet();
        if ($m_objet == 'individu') {
            $individu = IndividuQuery::create()->findPk($sr->getIdIndividu());
            $tab_data[$k]['beneficiaire'] = 'membre:' . $sr->getIdObjet() . ':' . $individu->getNom();
        } else {
            $membre = MembreQuery::create()->findPk($sr->getIdObjet());
            $tab_data[$k]['beneficiaire'] = 'membre:' . $sr->getIdObjet() . ':' . $membre->getNom();
        }
        if ($only_values) {
            $tab_data[$k] = array_values($tab_data[$k]);
        }
    }
    return $tab_data;
}

function action_paiement_liste_dataliste()
{

    return objet_liste_dataliste('paiement');
}

function action_paiement_liste_remise0()
{
    return remise_en_banque('paiement.export');
}

function action_paiement_liste_remise1()
{
    return remise_en_banque('paiement.edition_comptabilise');
}

function remise_en_banque($option)
{
    global $app;
    $message = '';
    include_once($app['basepath'] . '/src/inc/fichier_creation.php');
    $n_page_total = 0;
    $nb_page_total = 0;
    $prefs = pref($option);
    $prefixe = 'asso_paiements';
    // reedition
    if (isset($prefs['numero_remise']) and $prefs['numero_remise']) {
        $where = $prefixe . '.remise = ' . $prefs['numero_remise'];
        $numero = $prefs['numero_remise'];
    } else {
        $where = $prefixe . '.remise =0 ';
        $numero = 0;
        if (isset($prefs['amj_fin']) and $prefs['amj_fin']) {//Date de fin de prise en compte
            $amj_fin = $prefs['amj_fin']->format('Y-m-d');
        }
    }
    //todo non testé en multi societe
//    if (isset($prefs['entite_tous_un']) and !$prefs['entite_tous_un']) {
//        $entite=suc('entite');
//    }else{
//        $entite= array(pref('en_cours.id_entite') => pref('en_cours.id_entite'));
//    }
    $where .= ' and id_tresor = ' . $prefs['tresor'];
    if (!isset($amj_fin)) {
        $amj_fin = date('Y-m-d');
    }
    $where_amj_fin = ' and ' . $prefixe . '.date_enregistrement <= \'' . $amj_fin . "'";
    $where .= $where_amj_fin;
    $sortie = ($prefs['sortie']) ? $prefs['sortie'] : 'tableur';
    $nb_exemplaire = (isset($prefs['nb_exemplaire'])) ? $prefs['nb_exemplaire'] : 1;//+1 si validattion
    $nb_ligne_page = (isset($prefs['$nb_ligne_page'])) ? $prefs['$nb_ligne_page'] : 35;
    $select = 'SELECT distinct ' . $prefixe . '.id_entite ,' . $prefixe . '.id_tresor,count(*)as nb_ligne_section,sum(' . $prefixe . '.montant) as montant_section,MONTH(' . $prefixe . '.date_enregistrement) as mois,YEAR(' . $prefixe . '.date_enregistrement) as annee';
    $from = ' FROM asso_paiements ';
    $groupby = ' GROUP BY ' . $prefixe . '.id_entite, ' . $prefixe . '.id_tresor ,year(' . $prefixe . '.date_enregistrement),MONTH(' . $prefixe . '.date_enregistrement)';
    $sql = $select . $from . ' WHERE ' . $where . $groupby;
    $nb_section = 0;
    $statement = $app['db']->executeQuery($sql);
    $data = array();
    $total = array('montant' => 0, 'nb_ligne' => 0);
    while ($val = $statement->fetch()) {
        $nb_section++;
        $data[$nb_section] = $val;
        $data[$nb_section]['nb_page_section'] = ceil($val['nb_ligne_section'] / $nb_ligne_page);
        $nb_page_total += ceil($val['nb_ligne_section'] / $nb_ligne_page);
        $total['montant'] += $val['montant_section'];
        $total['nb_ligne'] += $val['nb_ligne_section'];
        $pluriel = ($val['nb_ligne_section'] >= 1) ? 's' : '';
        $data_entite = tab('entite.' . $val['id_entite']);
        $data_tresor = tab('tresor.' . $val['id_tresor']);
//        arbre($data_tresor);
        if (!$data_tresor) {
            $app['session']->getFlashBag()->add('success', 'pas de moyens de paiement id:' . $val['id_tresor']);
        }
        if (conf('module.pre_compta') and $data_tresor) {
            $data_journal = JournalQuery::create()->filterByIdCompte($data_tresor['id_compterb'])->findOne();
            $data_activite = ActiviteQuery::create()->filterByIdentite($val['id_entite'])->filterByNomcourt('BI')->findOne();
            if ($data_activite) {
                $data[$nb_section]['activite'] = $data_activite->getIdActivite();
            } else {
                $data[$nb_section]['activite'] = activite_creation(array(
                    'id_entite' => $val['id_entite'],
                    'nom' => 'Bilan',
                    'observation' => 'remise en banque',
                    'nomcourt' => 'BI'
                ));
            }
        } else {
            $data[$nb_section]['activite'] = '';
        }
        if ($numero) {//re edition
            $remisetresor[$val['id_entite']][$val['id_tresor']] = $numero;
        } else {
            if (!isset($remisetresor[$val['id_entite']][$val['id_tresor']])) {
                $remisetresor[$val['id_entite']][$val['id_tresor']] = $data_tresor['remise'];
            }
            $remisetresor[$val['id_entite']][$val['id_tresor']]++;
        }
        $data[$nb_section]['where'] = $where . ' and ' . $prefixe . '.id_entite=' . $val['id_entite'] . ' and ' . $prefixe . '.id_tresor='
            . $val['id_tresor'] . ' and year(' . $prefixe . '.date_enregistrement)='
            . $val['annee'] . ' and month(' . $prefixe . '.date_enregistrement)=' . $val['mois'];
        $data[$nb_section]['remisetresor'] = $remisetresor[$val['id_entite']][$val['id_tresor']];
        $data[$nb_section]['comptetresor'] = $data_tresor['id_compterb'];
        $data[$nb_section]['comptetiers'] = $data_tresor['id_compte'];
        if (conf('module.pre_compta') and $data_tresor) {
            $data[$nb_section]['journal'] = $data_journal->getIdJournal();
        } else {
            $data[$nb_section]['journal'] = '';
        }

        $data[$nb_section]['entete'][] = 'Entité : ' . $val['id_entite'] . ' :' . $data_entite['nom'];
        $data[$nb_section]['entete'][] = $app->trans('tresor') . ' :' . $val['id_tresor'] . ' :' . $data_tresor['nom'] . ' Iban :' . $data_tresor['iban'] . ' Bic :' . $data_tresor['bic'];
        $data[$nb_section]['entete'][] = ' année :' . $val['annee'] . ' mois :' . $val['mois'] . ' Remise n° :' . $remisetresor[$val['id_entite']][$val['id_tresor']];
        $data[$nb_section]['entete'][] = ' Nombre de paiement' . $pluriel . ' :' . $val['nb_ligne_section'] . ' pour un montant de :' . $val['montant_section'] . ' euros';
    }
    $titre_colonnes = array('enregiste_le', 'nom', 'montant', 'observation');
    $select = 'SELECT  date_format(' . $prefixe . '.date_enregistrement,"%d-%b-%y") as enregiste_le, me.nom as nom, ' . $prefixe . '.numero as numero, ' . $prefixe . '.montant as montant, ' . $prefixe . '.observation as observation';
    $from = ' FROM asso_paiements ' . $prefixe . ', asso_membres me ';
    //todo a voir si individu
    $where = ' and me.id_membre = ' . $prefixe . '.id_objet and ' . $prefixe . '.objet=\'membre\' ';
    $val = array();
    $orderby = ' ORDER BY ' . $prefixe . '.date_enregistrement';
    $export = '';
    $tab_values = array();
    $html = '';
    $css = '';
    $debut_page = 'Remise en Banque : ';
    if ($prefs['valider']) {
        $debut_page .= $app->trans('Edition validée.');
    }
    if ($prefs['comptabiliser'] and $numero) {
        $debut_page .= $app->trans(' Avec comptabilisation.');
    }
    $debut_page .= ' Fin de prise en compte : ' . $amj_fin;
    foreach ($data as $section => $val) {
        $sql = $select . $from . ' WHERE ' . $val['where'] . $where . $orderby;
        $n_ligne_section = 0;
        $n_page = 0;
        $liste_section = $app['db']->executeQuery($sql);
        while ($val_section = $liste_section->fetch()) {
            if ($n_ligne_section == 0) {
                debut_page($html, $export, $debut_page, $val['entete']);
                table_ligne($tab_values, $export, $titre_colonnes);
            }
            $n_ligne_section++;
            table_ligne($tab_values, $export, $val_section);
            if ($n_ligne_section == $nb_ligne_page and $n_ligne_section < $val['nb_ligne_section']) {
                table_fin($html, $export, $tab_values);
                fin_page($html, $export, $n_page, $val['nb_page_section'], $n_page_total, $nb_page_total);
                debut_page($html, $export, $debut_page, $val['entete']);
            }

        }
        table_fin($html, $export, $tab_values);
        fin_page($html, $export, $n_page, $val['nb_page_section'], $n_page_total, $nb_page_total);
    }
//    $html = $app['twig']->render( 'document_style.html.twig',['css'=>$css] ) . implode('<div class="nouvelle_page"></div>',$html);
    if ($total['nb_ligne'] == 0) {
        $message .= 'Aucun ' .
            $app->trans('paiement trouvé suivant vos preferences');
        echo($message);
    } else {
        if ((isset($prefs['numero_remise']) and $prefs['numero_remise'] and $prefs['comptabiliser']) or
            (isset($prefs['valider']) and $prefs['valider'])
        ) {
            comptabiliser($prefs['comptabiliser'], $data, $total, $prefixe);
        }
        if (strpos($sortie, 'tableur')) {
            header("Content-type: application/vnd.ms-excel");
            header("Content-disposition: attachment; filename=\"remise_en_banque_du_" . date('Y-m-d h:m:s') . ".csv");
            echo($export);
        }
        if (strpos($sortie, 'html')) {
            //            $largeur_colonne = array(40, 75, 45, 95);
            //            $titre_cols = array('Date remise','Nom-prenom',  'Montant', 'Observations');
            //            $pdf = new PDF('L','mm','A4');   // On crée le pdf
            //            $pdf->SetMargins(5, 5 );// Titres des colonnes
            //            $pdf->SetFont('Arial','',8);
            //            $pdf->AliasNbPages();
            //            $pdf->SetAutoPageBreak(true ,10);
            //            $pdf->titre = utf8_decode($texte2);// 3 lignes pour entéte
            //            $pdf->nom = utf8_decode($entete_tresor[trnom]);
            //            $pdf->objet = utf8_decode($remise);
            //            $pdf->AddPage();
            //            $pdf->BasicTable($titre_cols,$datas,$largeur_colonne,0.69);//tableau
            //            $nom_fic=_DIR_TMP.'remise_banque_'.$prefs2.'.pdf';
            //            $doc = $pdf->Output($nom_fic, 'F');
            //            $chargement="Charger le document à imprimer ".$nom_fic;
        }
        if (strpos($sortie, 'pdf')) {
            include_once($app['basepath'] . '/src/inc/fonctions_document.php');
            $loader = new Twig_Loader_Filesystem($app['resources.path'] . '/documents');
            $twig_page = new Twig_Environment($loader, ['autoescape' => false]);
            $css = $twig_page->render('document_style.css.twig', ['css' => $css]);
            $css .= $twig_page->render('document_style_table.css.twig', ['css' => $css]);
            return generer_pdf([$html], $css, 'ecran');


            $pdf = new Pdf($options);
//             $html='';
//             foreach($pages as $k=>$page){
//                 $html.=$app['twig']->render( 'document_style.html.twig',['css'=>$css] ).'<div class="page '.(($k>0)?'nouvelle_page':'').'">'. $page.'</div>';
//             }
            $html = '<!DOCTYPE html><html><meta http-equiv="Content-Type" content="text/html; charset=UTF-8"><body>' . $html . '</body></html>';
            echo($html);
            exit;
            $pdf = $pdf->WriteHTML($html);
            $id_log = Log::EnrOps('EXPREB', '', ["test reb"]);
            $nom_fichier_pdf = $app['tmp.path'] . '/remise_en_banque_' . date('Y-d-m-h-m') . '.pdf';
            $pdf->saveAs($nom_fichier_pdf);
            ged_enregister($nom_fichier_pdf, ['log' => $id_log]);
//            $args_twig=[
//                'sujet'=>'Rapport',
//                'texte'=>'Voir pièce jointe'
//            ];
//            $options=['pieces_jointes'=>['rapport.pdf'=>$nom_fichier_pdf]];
//            courriel_twig($app['email_from'],'basic',$args_twig,$options);
            unlink($nom_fichier_pdf);
            $pdf->send();
            exit();
        }
    }
    exit();
}

function debut_page(&$html, &$export, $debut_page, $entete)
{
    if (!is_array($debut_page)) {
        $debut_page = array($debut_page);
    }
    if (!is_array($entete)) {
        $entete = array($entete);
    }
    $html .= '<div class="hautdepage">';
    foreach ($debut_page as $texte) {
        $export .= $texte . PHP_EOL;
        $html .= $texte . '<br />';
    }
    $export .= PHP_EOL;
    $html .= '<br />';
    foreach ($entete as $texte) {
        $export .= $texte . PHP_EOL;
        $html .= $texte . '<br />';
    }
    $export .= PHP_EOL;
    $html .= '<br /></div>';

}

function fin_page(&$html, &$export, &$n_page, $nb_page_section, &$n_page_total, $nb_page_total)
{
    $n_page++;
    $n_page_total++;
    $temp = 'Page : ' . $n_page . '/' . $nb_page_section;
    if ($nb_page_section < $nb_page_total) {
        $temp .= ' -- section : ' . $n_page_total . '/' . $nb_page_total;
    }
    $temp2 = ' Edité le ' . date('Y-m-d h:m:s');
    $export .= $temp . $temp2 . PHP_EOL . PHP_EOL;
    $html .= '<div class="basdepage" ><div style="text-align: left;">' . $temp . '</div>
    <div style="text-align: right;">' . $temp2 . '</div><br /><br /></div>
    <div style="page-break-after:always;"></div>';

}

function table_ligne(&$table, &$export, $ligne)
{
    $export .= str_replace(array("\n", "\r", "\""), '', implode("\t", $ligne)) . PHP_EOL;
    $table[] = $ligne;

}

function table_fin(&$html, &$export, &$table)
{
    $export .= ' Edité le ' . date('Y-m-d h:m:s') . PHP_EOL . PHP_EOL;
    $html .= array2htmltable($table) . '<br />';
    $table = [];
}

//function formulaires_simplasso_remisebanque_traiter_dist()
//{
//    $message = '';
//    $prefs2 = '';
//    $imprime = '';
//    if (_request('retour')) {
//        return array('editable' => false, 'message_ok' => '', 'redirect' => generer_url_ecrire('asso_membres'));
//    }
//    if (_request('param1')) {
//        $prefs1 = _request('param1');
//    }
//    include_spip('base/abstract_sql');
//    include_spip('inc/acces');
//    include_spip('fpdf');
//    define('FPDF_FONTPATH', 'font/');
//
//    class PDF extends FPDF
//    {
//        function BasicTable($tit_col, $data, $largeur_colonne, $taille_caractere)
//        {                // Tableau simple
//            foreach ($data as $row) {
//                $i = 0;// Données
//                foreach ($row as $col) {
//                    $text = utf8_decode($col);//	$k= GetStringWidth( $text );//longueur d'une chaine
//                    $text = substr($text, 0, $largeur_colonne[$i] * $taille_caractere);//
//                    $this->Cell($largeur_colonne[$i], 10, $text, 1);
//                    $i++;
//                }
//                $this->Ln();
//            }
//            $this->Cell(array_sum($largeur_colonne), 0, '', 'T');
//        }// Trait de terminaison
//
//        function Header()
//        {                                                                        //entete
//            $largeur_colonne = array(40, 75, 45, 95);
//            $titre_cols = array('Date remise', 'Nom-prenom', 'Montant', 'Observations');
// //				$this->Image('../plugins/ccfd/images/logo.png',0,0,30);// Logo
//            $this->SetFont('Arial', 'B', 12);// Police Arial gras 15
//            $this->Cell(30);// Décalage à droite
//            $this->Cell(200, 10, $this->titre, 1, 1, 'L');// Titre
//            $this->Ln(5);// Saut de ligne
//            $this->SetFont('Arial', '', 8);
//            $this->Cell(0, 5, $this->nom, 0, 0);// nom
//            $this->Ln(5);// Saut de ligne
//            $this->Cell(0, 5, $this->objet, 0, 0);// nom
//            $this->Ln(5);// Saut de ligne
//            for ($i = 0; $i < count($titre_cols); $i++) {
//                $this->Cell($largeur_colonne[$i], 7, utf8_decode($titre_cols[$i]), 1, 0, 'c');
//            }
//            $this->Ln();
//        }
//
//        function Footer()
//        {
//            $this->SetY(-12);// Positionnement à 1,5 cm du bas					// Pied de page
//            $this->SetFont('Arial', 'I', 8);// Police Arial italique 8
//            $this->Cell(0, 7, utf8_decode('Imprimé le ') . date('d-m-Y H:i:s') . ' Page ' . $this->PageNo() . '/{nb}',
//                0, 0, 'C');
//        }// Numéro de page
//    }
//
//    $largeur_colonne = array(40, 75, 45, 95);
//    $titre_cols = array('Date remise', 'Nom-prenom', 'Montant', 'Observations');
//
//les lignes cheek de l'écran de saisie'
//    $val4='';
//    $i=0;
//    for($i = 0; $i < count($tab_id_paiement); $i++){
//        $val4 .= sql_quote($tab_id_paiement[$i]).',';
//    }
//    $val4 = substr($val4, 0, -1);//enlever la dernière virgule
//    $nb_cheques=$i.' chéque' ;
//    if ($i>1) $nb_cheques.='s';
//echo '------------'.$val4.'<BR>';
//
//}

function simplasso_liste_filtre_paiement($where)
{

    global $app;
    $objet = 'paiement';
    $Objet = ucfirst($objet);

    $tab_id = PaiementQuery::create()->filterByIdEntite(suc('entite'))->find()->toKeyValue('id' . $Objet,
        'id' . $Objet);
    $where_tmp = ' and id_' . $objet . ' IN (' . implode(',', $tab_id) . ')';


    $tab_filtre1 = array();
    $sel_col = ' SELECT count(ob.id_' . $objet . ') FROM asso_' . $objet . 's ob WHERE ob.id_' . $objet . ' IN (' . $where . ')';


    //	case 'valide' :
    $where_tmp2 = $where_tmp . ' AND  ob.comptabilise >0 ';
    $nb = $app['db']->fetchColumn($sel_col . $where_tmp2);
    $tab_filtre1['valide'] = $nb;
    //	case 'depose' :
    $where_tmp2 = $where_tmp . ' AND  ob.remise > 0';
    $nb = $app['db']->fetchColumn($sel_col . $where_tmp2);
    $tab_filtre1['depose'] = $nb;
    //	case 'en attente' :
    $where_tmp2 = $where_tmp . ' AND  ob.remise < 1';
    $nb = $app['db']->fetchColumn($sel_col . $where_tmp2);
    $tab_filtre1['en_attente'] = $nb;

    return $tab_filtre1;
}


function comptabiliser($comptabilise = false, $data, $total, $prefixe)
{
    global $app;
    include_once($app['basepath'] . '/src/inc/fichier_creation.php');
    $maintenant = new DateTime();
    if (conf('module.pre_compta')) {
        $objet_piece = PieceQuery::create()->orderByIdPiece('desc')->findOne();
        $piece = ($objet_piece) ? intval($objet_piece->getIdPiece()) + 1 : 1;
    }

    $ligne = 0;
    foreach ($data as $section => $val) {
        $data_tresor = TresorQuery::create()->filterByPrimaryKey($val['id_tresor'])->findOne();
        $data_tresor->setRemise($val['remisetresor']);
        $data_tresor->save();
        $data = ['Remise' => $val['remisetresor']];
        if ($comptabilise) {
            $data['Comptabilise'] = 1;
        }

        PaiementQuery::create()->where($val['where'])->update($data);


        if (conf('module.pre_compta')) {

            $date = $val['annee'] . "-" . substr('00' . $val['mois'], -2);
            $datefin = substr($val['where'], -11, 10);
            $periode = ($date == substr($datefin, 0, 7)) ? "au " . $datefin : "mois " . $date;
            $ecriture = new Ecriture();
            $data_ecriture = [
                'classement' => 'remise',
                'credit' => -$val['montant_section'],
                'nom' => $val['nb_ligne_section'] . " encaissements remise " . $val['remisetresor'] . " " . $periode,
                'id_compte' => $val['comptetiers'],
                'id_journal' => $val['journal'],
                'id_activite' => $val['activite'],
                'id_entite' => $val['id_entite'],
                'date_ecriture' => $maintenant,
                'observation' => 'par ' . suc('operateur.nom'),
                'etat' => 2,
                'id_piece' => $piece,
                'ecran_saisie' => 'st',
                'created_at' => $maintenant,
                'updated_at' => $maintenant
            ];
            $ecriture->fromArray($data_ecriture);
            $ecriture->save();
            $ecriture = new Ecriture();
            unset($data_ecriture['credit']);
            $data_ecriture['debit'] = $val['montant_section'];
            $data_ecriture['id_compte'] = $val['comptetresor'];
            $ecriture->fromArray($data_ecriture);
            $ecriture->save();
            $ligne = $ligne + 2;
        }

    }
    if (conf('module.pre_compta')) {
        $piece = new Piece();
        $piece->fromArray([
            'classement' => 'remise',
            'nom' => $total['nb_ligne'] . " encaissements  " . $periode,
            'id_journal' => $val['journal'],
            'id_entite' => $val['id_entite'],
            'date_piece' => $maintenant,
            'observation' => 'par ' . suc('operateur.nom'),
            'id_piece' => $piece,
            'ecran_saisie' => 'st',
            'etat' => 2,
            'nb_ligne' => $ligne,
            'created_at' => $maintenant,
            'updated_at' => $maintenant,
        ]);
        $piece->save();
    }
    return true;
}

