<?php

use Propel\Runtime\Map\TableMap;
use Symfony\Component\Form\Extension\Core\Type\DateType;
use Symfony\Component\Form\Extension\Core\Type\FormType;
use Symfony\Component\Form\Extension\Core\Type\MoneyType;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\Form\Extension\Core\Type\TextareaType;
use Symfony\Component\Validator\Constraints as Assert;


function prix_form()
{

    global $app;
    $args_rep = [];
    $request = $app['request'];
    $id = sac('id');
    $id_prestation = $request->get('id_prestation');
    $prix_existant = null;
    if ($id) {
        $objet_data = charger_objet();
        $modification = true;
        $data = $objet_data->toArray(TableMap::TYPE_FIELDNAME);

    } else {
        $modification = false;
        $data['id_prestation'] = $id_prestation;
        $prix_existant = PrixQuery::create()->filterByIdPrestation($id_prestation)->count() > 0;
        $data['date_debut'] = new Datetime();
    }

    $builder = $app['form.factory']->createNamedBuilder('prix', FormType::class, $data);
    $builder->setRequired(false);
    if ($modification) {
        $builder->setAction($app->path(sac('objet') . '_form',
            array('id_prestation' => $id_prestation, 'id_' . sac('objet') => sac('id'))));
    } else {
        $builder->setAction($app->path(sac('objet') . '_form', array('id_prestation' => $id_prestation)));
    }
    if ($prix_existant) {
        $builder
            ->add('date_debut', DateType::class, array(
                'label' => 'Date début du prix',
                'constraints' => new Assert\NotBlank(),
                'widget' => 'single_text',
                'format' => 'dd/MM/yyyy',
                'attr' => array('class' => 'datepickerb')
            ));
    }
    $form = $builder
        ->add('montant', MoneyType::class,
            array('constraints' => new Assert\NotBlank(), 'label' => 'Montant', 'attr' => array()))
        ->add('observation', TextareaType::class, array('label' => 'Observation', 'attr' => array('placeholder' => '')))
        ->add('submit', SubmitType::class,
            array('label' => $app->trans('Enregistrer'), 'attr' => array('class' => 'btn-primary')))
        ->getForm();

    $form->handleRequest($request);
    if ($form->isSubmitted()) {
        $data = $form->getData();
        if ($form->isValid()) {
            if (!$modification) {
                $objet_data = new Prix();
                if (!$prix_existant) {
                    $data['date_debut'] = $objet_data->getDateDebut();//valeur initiale dans la description des fichiers
                }
            }


            // rechercher si la date existe
            $tab_prixs = PrixQuery::create()->filterByIdPrestation(sac('id_prestation'))->findByDateDebut($data['date_debut']);
            $msg = '';

            foreach ($tab_prixs as $p) {
                $msg = 'Le montant de l\'enregistrement existant a été modifié';
                $p->setMontant($data['montant']);
                $p->setObservation($data['observation']);
                $p->save();

            }
            $objet_data->fromArray($data);
            $objet_data->save();
            $objet_data->prix_coherence_date();
            $args_rep['url_redirect'] = $app->path('prestation',['id_prestation' => $objet_data->getIdPrestation()]);
            chargement_table();
        }
    }
    return reponse_formulaire($form, $args_rep);
}


function action_prix_form_supprimer()
{
    return action_supprimer_une_instance();
}