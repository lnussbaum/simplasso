<?php

use Symfony\Component\Validator\Constraints as Assert;
use Symfony\Component\Form\FormError as FormError; use Symfony\Component\Form\Extension\Core\Type\FormType; use Symfony\Component\Form\Extension\Core\Type\TextType;

include_once('paiement_form.php');


function cotisation_masse_form()
{
    global $app;
    $request = $app['request'];
    $id_servicerendu = $request->get('id_servicerendu');
    $id_membre = $request->get('id_membre');
    $servicerendu = null;
    $modification = false;
    $data = array();
    $data_reglement = array();
    $tab_prochaines_cotisations = getProchainesCotisations();
    //todo reprendre modele sur servicerendu_form

    $data['id_entite'] = 1;//$preferences['cotisation']['entite'];
    $data['id_prestation'] = 7;//$preferences['cotisation']['prestation'];
    $data['date_debut'] = $tab_prochaines_cotisations[$data['id_entite']][$data['id_prestation']]['date_debut'];
    $data['date_fin'] = $tab_prochaines_cotisations[$data['id_entite']][$data['id_prestation']]['date_fin'];
    $data['montant'] = selectionne_montant($tab_prochaines_cotisations[$data['id_entite']][$data['id_prestation']]['prix'], $data['date_debut']);
    $data_reglement['montant'] = $data['montant'];
    $data_reglement['date_cheque'] = new Datetime();
    $data_reglement['date_enregistrement'] = new Datetime();

    $builder = $app['form.factory']->createBuilder('cotisation_en_masse','form', $data);
    $builder->setRequired(false);
    $choices_entites = EntiteQuery::create()->filterByIdEntite(array_keys($tab_prochaines_cotisations))->find()->toKeyValue('IdEntite', 'nom');
    $choices_prestations = CotisationsQuery::create()->find()->toKeyValue('idPrestation', 'nom');
    if (count($choices_entites) == 1)
        $builder = $builder->add('id_entite',HiddenType::class, array());
    else
        $builder = $builder->add('id_entite',ChoiceType::class, array('label' => 'Entite', 'expanded' => true, 'choices' => $choices_entites, 'attr' => array('inline' => true)));

    $builder = $builder
        ->add('id_membre',TextType::class, array('label' => 'Identifiant des membres','attr' => array('help_text' => 'séparés par des virgules')))
        ->add('id_prestation',ChoiceType::class, array('label' => 'Cotisation',
            'choices' => $choices_prestations,
            'expanded' => true,
            'attr' => array('inline' => true)))
        ->add('montant',MoneyType::class, array('label' => 'Montant', 'attr' => array('placeholder' => '')))
        ->add('date_debut',DateType::class, array('label' => 'Date de validité', 'widget' => 'single_text', 'format' => 'dd/MM/yyyy', 'attr' => array('class' => 'datepickerb')))
        ->add('date_fin',DateType::class, array('label' => 'au', 'widget' => 'single_text', 'format' => 'dd/MM/yyyy', 'attr' => array('class' => 'datepickerb')))
        ->add('observation',TextareaType::class, array('label' => 'Observations'))
        ->add('reglement_question',CheckboxType::class, array('label' => 'Differer le paiement','required' => false,'attr' => array('align_with_widget' => true)));

        $subform = $app['form.factory']->createNamedBuilder('reglement',FormType::class, $data_reglement);
        $subform = getFormPaiement($subform);
        $builder->add($subform, '', array('label' => ''));

    $form = $builder->add('submit',SubmitType::class, array('label' => 'Enregistrer la cotisation', 'attr' => array('class' => 'btn-primary')))
        ->getForm();


    $form->handleRequest($request);

    if ($form->isSubmitted()) {
        $data = $form->getData();

        if ($form->isValid()) {


            $membres = explode(',',$data['id_membres']);
            foreach($membres as $id_membre){

                $servicerendu = new Servicerendu();
                $servicerendu->fromArray($data);
                $servicerendu->setIdMembre($id_membre);
                $servicerendu->save();

               if (!$data['reglement_question']) {
                    $paiement = new Paiement();
                    $paiement->fromArray($data);
                    $paiement->fromArray($data['reglement']);
                    $paiement->save();

                    $service_paiement = new Servicepaiement();
                    $service_paiement->setIdPaiement($paiement->getIdPaiement());
                    $service_paiement->setIdServicerendu($servicerendu->getIdServicerendu());
                    $service_paiement->setMontant($data['reglement']['montant']);
                    $service_paiement->save();
                }
            }

            $url_redirect = $app->path('membre_liste');
            if ($modification)
                $app['session']->getFlashBag()->add('success', $app->trans('cotisations_modifier_ok'));
            else
                $app['session']->getFlashBag()->add('success', $app->trans('cotisations_ajouter_ok'));
            return $app->redirect($url_redirect);
        }
    }
    foreach ($tab_prochaines_cotisations as &$pc_entite) {
        foreach ($pc_entite as &$pc_prestation) {
            $pc_prestation['date_debut'] = $pc_prestation['date_debut']->format('d/m/Y');
            $pc_prestation['date_fin'] = $pc_prestation['date_fin']->format('d/m/Y');

        }
    }
//  $tab_prochaines_cotisations['nouveau']=!$modification;
    $tab_param = array('form' => $form->createView(),
        'id_servicerendu' => $id_servicerendu,
        'id_membre' => $id_membre,
        'tab_prochaines_cotisations' => $tab_prochaines_cotisations);
    if ($modification)
        $tab_param['servicerendu'] = $servicerendu;
    return $app['twig']->render('cotisation_masse_form.html.twig', $tab_param);


}
function selectionne_montant($tab_prix, $date)
{


    $date = $date->getTimestamp();
    $result = $tab_prix[0]['montant'];
    foreach ($tab_prix as $prix) {
        if ($date > $prix['date_fin']) {
            break;
        }
        $result = $prix['montant'];
    }
    return $result;


}
