<?php
function individu(){
    global $app;
    $args_twig=[];
    return $app['twig']->render(fichier_twig(),$args_twig);

}


function action_individu_npai(){
    global $app;
    $request=$app['request'];
    $id_individu=$request->get('id_individu');
    $type=$request->get('type');
    $mot = table_filtrer_valeur_premiere(tab('mot'),'nomcourt','npai_'.$type);
    $ajout = Mot::MotLien($mot['id_mot'],$id_individu,'individu','',true);
    if($ajout){
        Log::EnrOp('NPA', 'individu', null, $id_individu);
    }
    return $app->json(['ok'=>true,'ajout'=>$ajout]);
}