<?php


use Symfony\Component\Validator\Constraints as Assert;
use Symfony\Component\Form\Extension\Core\Type\FormType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\Extension\Core\Type\TextareaType;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\Extension\Core\Type\IntegerType;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Propel\Runtime\Map\TableMap;


function tresor_form()
{
    global $app;
    $args_rep = [];
    $request = $app['request'];
    $data = array();
    $modification = false;
    $id=sac('id');
    if ($id) {
        $objet_data = charger_objet();
       $modification = true;
        $data = $objet_data->toArray(TableMap::TYPE_FIELDNAME);
     } else {
        $data['remise'] = 1;
    }

    $builder = $app['form.factory']->createNamedBuilder('tresor',FormType::class, $data);
    $builder->setRequired(false);
    formSetAction($builder,$modification);

    $form = $builder
        ->add('nomcourt',TextType::class, array(
            'label' => $app->trans('nomcourt'),
            'attr' => array('class' => 'span2', 'placeholder' => 'nom')
        ))
        ->add('nom',TextType::class, array(
            'constraints' => new Assert\NotBlank(),
            'attr' => array('class' => 'span2', 'placeholder' => 'nom')
        ))
        ->add('iban',TextType::class, array(
            'constraints' => new Assert\NotBlank(),
            'attr' => array('class' => 'span2', 'placeholder' => 'IBAN')))
        ->add('bic',TextType::class,array(
            'constraints' => new Assert\NotBlank(),
            'attr' => array('class' => 'span2', 'placeholder' => 'BIC')))
        ->add('id_compterb',ChoiceType::class, array(
            'label' => 'Compte',
            'choices' => array_flip(table_simplifier(tab('compte'))),
            'attr' => array()))
        ->add('id_compte',ChoiceType::class, array(
            'label' => 'Compte',
            'choices' => array_flip(table_simplifier(tab('compte'))),
            'attr' => array()))
        ->add('remise', IntegerType::class,
            array('label' => $app->trans('remise_prochaine', array('objet' => 'compte')), 'attr' => array()))
        ->add('observation',TextareaType::class, array('label' => 'Observation'))
        ->add('id_entite',ChoiceType::class, array(
            'label' => 'entite',
            'choices' => getEntitesDeLUtilisateur(),
            'attr' => array()
        ))
        ->add('submit',SubmitType::class,
            array('label' => $app->trans('Enregistrer'), 'attr' => array('class' => 'btn-primary')))
        ->getForm();

    $form->handleRequest($request);

    if ($form->isSubmitted()) {
        $data = $form->getData();

        if ($form->isValid()) {
            if ($modification) {
                $objet_data->fromArray($data);
                $objet_data->save();
                $id=$objet_data->getPrimaryKey();

            }else {
                require_once($app['basepath'] . '/src/inc/fichier_creation.php');
                $id=tresor_creation($data);
            }
            $args_rep['id']=$id;
            chargement_table();

        }
    }
    return reponse_formulaire($form, $args_rep);
}


function action_tresor_form_supprimer(){
    return action_supprimer_une_instance();
}