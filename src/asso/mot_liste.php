<?php
function mot_liste()
{
    global $app;
    $args_twig=[
        'tab_col'=>mot_colonnes()
        ];
    return $app['twig']->render(fichier_twig(), $args_twig);

}


function mot_colonnes(){

    $tab_colonne= array();
	$tab_colonne['id_mot'] = ['title'=> 'id'];
	$tab_colonne['nom'] = [];
	$tab_colonne['nomcourt'] = [];
	$tab_colonne['descriptif'] = [];
	$tab_colonne['texte'] = [];
	$tab_colonne['importance'] = [];
	$tab_colonne['id_motgroupe'] = [];
	$tab_colonne['actif'] = [];
	$tab_colonne['created_at'] = ['type' => 'date-eu'];

    $tab_colonne['action']=["orderable"=> false];
    $tab_colonne = datatable_complete_liste_colonne($tab_colonne);

    return $tab_colonne;

}





function action_mot_liste_dataliste()
{

    return objet_liste_dataliste();

}


