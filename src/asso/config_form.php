<?php

use Symfony\Component\Validator\Constraints as Assert;
use Symfony\Component\Form\FormError as FormError;
use Symfony\Component\Form\Extension\Core\Type\FormType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\Extension\Core\Type\TextareaType;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Propel\Runtime\Map\TableMap;
use Symfony\Component\Form\Extension\Core\Type\HiddenType;
use Symfony\Component\Form\Extension\Core\Type\DateType;
use Symfony\Component\Form\Extension\Core\Type\MoneyType;

/**
 *Permet de modifier les valeurs de configurations
 * reste a faire fonctionner la modal pour les données et mette des expications sur les boutons
 * la structure origine est dan /inc/configuration
 * le chargement est dans config.liste
 * @return mixed
 */
function config_form()
{
    global $app;
    $args_rep = [];
    $request = $app['request'];
    $nom = $request->get('param');
    $appel_config = true;
    $modification = true;


    if (!sac('id') and !isset($nom)) {

        $modification = false;
        $appel_config = false;

    }
    $objet_data = charger_objet();
    $data = $objet_data->toArray(TableMap::TYPE_FIELDNAME);
    $data['valeur'] = $objet_data->getValeur();
    $ajout_vc = null;
    $clef = explode('.', $nom);
    $maitre = true;
    if (!empty($data['id_entite']) or !empty($data['id_individu'])) {
        $maitre = false;
    }
//    echo '<BR> Developpement et modif des noms de variables : ' . $maitre.'-<BR>'.$data['id_entite'].$data['id_individu'];
    if ($modification) {
        list($temp, $data['ajout_vc']) =saisie_config($data['valeur'], $nom, $clef, $maitre);
        $data = array_merge($data, $temp);
        $popup_valeur = 'configuration';
        if (isset($data['saisie'])) {
            list($form_valide1, $form_etape1) = formulaire_modif($clef, $data['saisie'],
                $data['id_entite'], $data['id_individu'], $appel_config);
            if ($form_valide1) {
                return $app->redirect(sac('retour_form'));
            }
            $tab_btn_popup['popup_saisie'] = array(
                'ico' => 'fa fa-eye',
                'class' => 'btn-fa-pencil',
                'label' => 'Modifier ' . $nom,
                'contenu' => $form_etape1
            );
        }
    }
    $builder = $app['form.factory']->createNamedBuilder('config',FormType::class, $data);
    formSetAction($builder,$modification);
    $builder->setRequired(false);
     if ($modification) {
        if (isset($data['vnom'])) {
            foreach ($data['vnom'] as $key => $temp) {
                $builder->add('vnom_' . $key,TextType::class, array('label' => $temp));
            }
        }
        if (isset($data['ajout'])) {
            $builder
                ->add('ajout_vc',ChoiceType::class, array(
                    'label' => $app->trans('config_choisir_sv'),
                    'expanded' => false,
                    'choices' => $data['ajout']
                ))
                //todo transformer ce bouton validaton popup  oui = création d'une sous valeur non = positionnement ( bouton du dessous) lors de la sélection s'il n'y a pas de choix et demander une confirmation de l'ajout
                ->add('bt_ajoute',SubmitType::class, array(
                    'label' => $app->trans('config_ajouter_sv'),
                    'attr' => array('class' => 'btn-primary')
                ))
                //todo transformer ce bouton en execution de clic lors de la sélection s'il n'y a pas de choix et demander une confirmation de l'ajout
                ->add('bt_clef',SubmitType::class,
                    array('label' => $app->trans('config_choisir_affichage'), 'attr' => array('class' => 'btn-info')));
        }
    } else {

    $builder
            ->add('id_individu',TextType::class, array('label' => 'individu' ))
            ->add('id_entite',ChoiceType::class, array('label' => 'entite', 'choices' => array_unshift( table_simplifier(tab('entite'))), "Pour toutes"))
            ->add('creer',TextType::class, array('label' => 'Valeur'));
    }
    $form = $builder
        ->add('nom',TextType::class, array('label' => 'Nom'))
        ->add('nomcourt',TextType::class, array('label' => 'Nom court'))
        ->add('niveau_prefs',ChoiceType::class, array(
            'label' => 'preference',
            'expanded' => true,
            'choices' => lire_config('niveau_prefs'),
            'attr' => array('inline' => true)
        ))
        ->add('observation',TextareaType::class, array('label' => 'observation', 'attr' => array('placeholder' => '')))
        ->add('save',SubmitType::class,
            array('label' => $app->trans('Enregistrer'), 'attr' => array('class' => 'btn-primary')))
        ->getForm();

    $form->handleRequest($request);
    if ($form->isSubmitted()) {
        $data = $form->getData();
        if ($modification) {
            if ($form->get('bt_clef')->isClicked()) {
                $clef = $data['idv' . $data['ajout_vc']]['clef'];
                foreach ($clef as $key => $v) {
                    if ($key == 0) {
                        $temp_clef = '?nom=' . $v;
                    } else {
                        $temp_clef .= '.' . $v;
                    }
                }
                $app['session']->getFlashBag()->add('info', 'Changement d\'affichage ');
                return $app->redirect(sac('objet') . '_form' . $temp_clef);
            }
            if ($form->get('bt_ajoute')->isClicked()) {
                if ($data['ajout_vc']) {
                    $clef = $data['idv' . $data['ajout_vc']]['clef'];
                } else {
                    $clef = array('nouveau' => 'valeur nouvelle');
                }
                $texte = ajoute_clef($data['valeur'], $clef);
                $data['variables'] = json_encode($data['valeur']);
                $objet_data->setValeur($data['valeur']);
                $objet_data->fromArray($data);
                $objet_data->save();
                $app['session']->getFlashBag()->add('info', $texte);
                $form->isValid(false);
                return $app->redirect(sac('objet') . '_form?id_' . sac('objet') . '=' . sac('id'));
            }
        } else {
            $temp = ConfigQuery::create()->filterByNom($data['nom'])
                ->filterByIdEntite($data['id_entite'])
                ->findOne();
            if ($temp) {
                $form->addError(new FormError('La configuration existe passer par la modification'));
                return $app->redirect(sac('objet') . '_form?id_' . sac('objet') . '=' . sac('id'));
            }
        }
        if ($form->isValid()) {
            if ($form->get('save')->isClicked()) {
                $valeur = $data['valeur'];
                if ($modification) {
                    $temp = array();
                    if (!array_key_exists('0', $clef)) {
                        $clef = array($data['nom']);
                    }
                    $ok = false;
                    if (isset($data['vnom'])) {

                        foreach ($data['vnom'] as $key => $ancien) {
                            $data['vnom_' . $key] = strtolower(str_replace(' ', '_', $data['vnom_' . $key]));
                            if ($ancien != $data['vnom_' . $key]) {
                                $temp[$ancien] = array($data['vnom_' . $key], $data['idv' . $key]['clef']);
                                if (count($data['idv' . $key]['clef']) >= 2) {
                                    $ok = true;
                                }
                            }
                            if ($key== $data['nom']) {
                                $data['nom'] = substr($data['vnom_' . $key], 0, 14);
                            }
                        }
                        if (count($temp) and $ok) {
                            modifie_clef($valeur, $temp);
                            $data['variables'] = json_encode($valeur);
                        }
                    }
                } else {
                    //Creation
                    $data['nom'] = substr(strtolower(str_replace(' ', '_', $data['nom'])), 0, 14);
                    if ($data['creer'] == '') {
                        $data['creer'] = $data['nom'];
                    }
                    $data['creer'] = substr(strtolower(str_replace(' ', '_', $data['creer'])), 0, 14);
                    if ($data['nomcourt'] = 'n') {
                        $data['nomcourt'] = $data['nom'];
                    }
                    $data['variables'] = json_encode(array($data['creer'] => $data['creer']));
                }

                $objet_data->fromArray($data);
                $objet_data->save();
               // $app['session']->getFlashBag()->add('success', $app->trans(sac('objet_action') . '_ok'));
            }
        }

    }
    $args_rep['objet_data'] =    $objet_data;
    if (isset($data['parent'])) {
        $args_rep['parent'] = $data['parent'];
    }
    if ($modification and isset($data['saisie'])) {
        $args_rep['tab_btn_popup'] = $tab_btn_popup;
    }

    return reponse_formulaire($form,$args_rep);
}

/**
 * @param $valeur
 * @param $clef
 * @param array $saisie contient 2 ou 4 champs de saisie  ou les valeurs déja traitée
 * @return string
 */
function ajoute_clef(&$valeur, $clef, $saisie = array())
{
    $i = count(array_keys($clef));
    if (isset($saisie['nouveau'])) {
        $data_temp = array(strtolower(str_replace(' ', '_', $saisie['nouveau'])) => $saisie['valeur']);
        if (isset($saisie['ancien'])) {
            $data_temp[strtolower(str_replace(' ', '_', $saisie['ancien']))] = $saisie['vancien'];
        }
    } else {
        $data_temp = $saisie;
    }
    switch ($i) {
        case 1:
            $texte = ajoute_clef_valeurs($j, $data_temp, $valeur);
            $valeur = $data_temp;
            break;
        case 2:
            $texte = ajoute_clef_valeurs($j, $data_temp, $valeur[$clef[1]]);
            $valeur[$clef[1]] = $data_temp;
            break;
        case 3:
            $texte = ajoute_clef_valeurs($j, $data_temp, $valeur[$clef[1]][$clef[2]]);
            $valeur[$clef[1]][$clef[2]] = $data_temp;//ok
            break;
        case 4:
            $texte = ajoute_clef_valeurs($j, $data_temp, $valeur[$clef[1]][$clef[2]][$clef[3]]);
            $valeur[$clef[1]][$clef[2]][$clef[3]] = $data_temp;
            break;
        case 5:
            $texte = ajoute_clef_valeurs($j, $data_temp, $valeur[$clef[1]][$clef[2]][$clef[3]][$clef[4]]);
            $valeur[$clef[1]][$clef[2]][$clef[3]][$clef[4]] = $data_temp;
            break;
        default:
            echo '<br>fin ajoute clef niveau non traité ';
            exit;
    }
    return $texte;
}


/**
 * @param $j
 * @param $saisie
 * @param $valeur
 * @return string
 */
function ajoute_clef_valeurs(&$j, &$saisie, $valeur)
{
    $j = 1;
    $nom = '';
    if (is_array($valeur)) {
        $j = count(array_keys($valeur), COUNT_RECURSIVE) + 1;
    } else {
        $nom = $valeur;
    }
    if ($j >= 2) {
        $saisie = array('nom_' . $j => 'Valeur ' . $j);
        $saisie = array_merge($valeur, $saisie);
        $texte = 'Ajout d\'une valeur a ' . $nom;
    } else {
        $texte = 'Ajout de 2 sous valeurs a ' . $nom;
        $nom = strtolower(str_replace(' ', '_', $nom));
        $j1 = $j + 1;
        $saisie = array($nom . $j => $nom . ' valeur ' . $j, $nom . ($j1) => $nom . ' valeur ' . ($j1));
    }
    return $texte;
}

function saisie_config($valeur, $nom, $clef, $maitre)
{
    $temp = array();
    $ajout_vc = null;
    $nbniveau = count(array_keys($clef));
    $temp['clef']['debut'] = array(0 => $nom);
    $nbcle = 0;
    $valeur1 = $valeur;
    $i = 0;
    while ($i < $nbniveau):
        $j = 0;
        while ($j <= $i):
            $tab_cle[$i][$j] = $clef[$j];
            $j++;
        endwhile;
        $tab_cle1[$i] = $clef[$i];
        $i++;
    endwhile;
    $nb = 1;
    foreach ($clef as $key => $clef2) {
//        echo $key.'--'.$nbcle.'-*'.$maitre.'*-';
        $nbcle++;
        $temp['ajout']=array_keys($valeur1);
        if (!is_array($valeur1)) {
//            arbre($valeur1);
//            ae('Ligne 381 nombre de clef dans valeur :' . count(array_keys($valeur)));
            exit;
            break;
        }
        if ($nbcle >= $nbniveau) {

            foreach ($valeur1 as $keyv => $temp2) {
                $nbvaleur = count($temp2);
                $temp['idv' . $nb]['clef'] = $tab_cle[($nbcle - 1)];
                $temp['idv' . $nb]['clef'][] = $keyv;
                $temp['idv' . $nb]['nom'] = $keyv;
                if ($maitre) {
                    $temp['vnom'][$nb] = $keyv;
                    $temp['vnom_' . $nb] = $keyv;
                    if (is_array($temp2)) {
                        $temp['ajout'][$nb] = $keyv . ' : ' . $nbvaleur . ' valeur(s)';
                    }
                }
                $nb++;
                if (!is_array($temp2)) {
                    if ($nbcle == $nbniveau or $nbniveau == 1) {
                        $temp['saisie'][$nb - 1] = $temp2;
                        $temp['idv' . ($nb - 1)]['valeur'] = $temp2;
                    }
                }
            }
        } else {
            $temp['ajout'][$nb] = $clef[$nbcle];
            $temp['idv' . $nb]['clef'] = $tab_cle[$nbcle];
            $nb++;
        }
        if ($nbcle <= ($nbniveau - 1)) {
            $valeur1 = $valeur1[$tab_cle1[$nbcle]];
        }
    }
    $temp['ajout'][$nb] = $clef[0];
    $temp['idv' . $nb]['clef'] = $tab_cle[0];
    ksort($temp);
    return array($temp, $ajout_vc);
}

function modifie_clef(&$valeur, $saisie)
{
    foreach ($saisie as $key => $temp) {
        $ancien = $key;
        $nouveau = $temp[0];
        $clef = $temp[1];
        $i = count(array_keys($clef));
        switch ($i) {
            case 1:// la racine Imprimante ne devrait pas passer traiter dans save
                echo '<br>ligne 462';
                exit;
                break;
            case 2: //premier niveau Etiquette
                if ($nouveau) {
                    $valeur[$nouveau] = $valeur[$ancien];
                }
                unset($valeur[$ancien]);
                break;
            case 3://Depart pour  imprimante etiquette
                if ($nouveau) {
                    $valeur[$clef[1]][$nouveau] = $valeur[$clef[1]][$ancien];
                }
                unset($valeur[$clef[1]][$ancien]);
                break;
            case 4:
                if ($nouveau) {
                    $valeur[$clef[1]][$clef[2]][$nouveau] = $valeur[$clef[1]][$clef[2]][$ancien];
                }
                unset($valeur[$clef[1]][$clef[2]][$ancien]);
                break;
            case 5:
                if ($nouveau) {
                    $valeur[$clef[1]][$clef[2]][$clef[3]][$nouveau] = $valeur[$clef[1]][$clef[2]][$clef[3]][$ancien];
                }
                unset($valeur[$clef[1]][$clef[2]][$clef[3]][$ancien]);
                break;
            case 6:
                if ($nouveau) {
                    $valeur[$clef[1]][$clef[2]][$clef[3]][$clef[4]][$nouveau] = $valeur[$clef[1]][$clef[2]][$clef[3]][$clef[4]][$ancien];
                }
                unset($valeur[$clef[1]][$clef[2]][$clef[3]][$clef[4]][$ancien]);
                break;
            case 6:
                echo '<br>*****  ligne 405 modifie clef ';//$i :' . $i . ' isset de clef ' . isset($clef) . '<br>';
                exit;
                break;
        }
    }
    return;
}


function action_config_form_supprimer(){
    return action_supprimer_une_instance();
}
