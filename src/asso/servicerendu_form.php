<?php

use Propel\Runtime\Map\TableMap;
use Symfony\Component\Form\Extension\Core\Type\CheckboxType;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\Extension\Core\Type\DateType;
use Symfony\Component\Form\Extension\Core\Type\FormType;
use Symfony\Component\Form\Extension\Core\Type\HiddenType;
use Symfony\Component\Form\Extension\Core\Type\IntegerType;
use Symfony\Component\Form\Extension\Core\Type\MoneyType;
use Symfony\Component\Form\Extension\Core\Type\NumberType;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\Form\Extension\Core\Type\TextareaType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\FormError as FormError;

include_once('paiement_form.php');


function servicerendu_form()
{
    global $app;
    $args_rep = [];
    $request = $app['request'];
    $tab_modepaiements = array();
    $tab_soldes = array();
    $lot = $request->get('lot');
    $objet = sac('objet');
    $nb_max_prestation = 1;
    $id = sac('id');
    $data = array();
    $objet_beneficiaire = '';
    $id_objet_beneficiaire = null;
    $ok=false;
    $args = [];
    $modification = false;
    if ($id > 0) {

        $modification = true;
        $objet_data = charger_objet(sac('objet'),  $id);
        $data_prestation['pr0'] = $objet_data->toArray(TableMap::TYPE_FIELDNAME);
        $data_prestation['pr0']['date_enregistrement'] = $objet_data->getDateEnregistrement();
        $data_prestation['pr0']['date_debut'] = $objet_data->getDateDebut();
        $data_prestation['pr0']['date_fin'] = $objet_data->getDateFin();
        $data['id_entite'] = $objet_data->getIdEntite();
        if ($objet_data->getIdMembre()) {
            $objet_beneficiaire = 'membre';
            $data['id_membre'] = $objet_data->getIdMembre();
            $id_objet_beneficiaire = $data['id_membre'];
        } else {
            $objet_beneficiaire = 'individu';
            $tab_individu = $objet_data->getIndividus();
            $data['id_individu'] = $tab_individu[0]->getIdIndividus();
            $id_objet_beneficiaire = $data['id_individu'];
        }
        $args_rep['objet_data'] = $objet_data;
        $args['id_servicerendu'] = $id;
        $args['prestation_type'] = sac('alias_valeur');


    } else {

        if ($lot) {
            $tab_prestationslot = tab('prestationslot');
            foreach ($tab_prestationslot as $k => $pl) {
                $nb_max_prestation = max($nb_max_prestation, count($pl['prestations']));
            }
        }
        $data_prestation = array();
        for ($i = 0; $i < $nb_max_prestation; $i++) {
            $data_prestation['pr' . $i] = [
                'id_prestation' => pref($objet . '.id_prestation'),
                'montant' => 0,
                'premier' => 0,
                'dernier' => 0,
                'quantite' => 1,
                'taux' => 0.20,
                'date_enregistrement' => new Datetime(),
                'prestation_type' => $request->get('prestation_type')
            ];

        }
        $data['id_entite'] = pref('en_cours.id_entite');

        $data['reglement']['id_tresor'] = pref('paiement.tresor');
        $data['reglement']['date_cheque'] = new Datetime();
        $data['reglement']['date_enregistrement'] = new Datetime();
        $data['reglement']['montant'] = 0;
        $data['reglement']['id_paiement'] = null;

        if ($request->get('objet_beneficiaire') !== null) {
            $objet_beneficiaire = $request->get('objet_beneficiaire');
            $data['id_' . $objet_beneficiaire] = $request->get('id_objet_beneficiaire');
        } else {
            if ($request->get('id_membre') !== null) {
                $objet_beneficiaire = 'membre';

            } elseif ($request->get('id_individu') !== null) {
                $objet_beneficiaire = 'individu';
            }
            $id_objet_beneficiaire = $request->get('id_' . $objet_beneficiaire);
            $data['id_' . $objet_beneficiaire] = $id_objet_beneficiaire;
        }
        if (isset($id_objet_beneficiaire)) {
            $args['id_' . $objet_beneficiaire] = $id_objet_beneficiaire;
        } elseif (isset($objet_beneficiaire)) {

            $args['objet_beneficiaire'] = $objet_beneficiaire;
        }
        if ($lot) {
            $args['lot'] = true;
        }
    }
    $data['objet_beneficiaire'] = $objet_beneficiaire;
    $data['id_objet_beneficiaire'] = $id_objet_beneficiaire;

    list($tab_prochains_servicerendu, $tab_choix_prestation, $tab_defaut) = getSrPrestation($objet_beneficiaire,
        $data['id_objet_beneficiaire']);
    if (!$tab_prochains_servicerendu) {
        echo $app->trans('Pas de ') . $app->trans($objet) . $app->trans(' pour cette saisie');
        exit;
    }


    $form_name = $modification ? 'form_modif_' . sac('objet') . '_' . $id : 'form_ajout_' . sac('objet');
    $builder = $app['form.factory']->createNamedBuilder($form_name, FormType::class, $data);
    $builder->setRequired(false);
    formSetAction($builder, $modification, $args);
    if ($modification) {
        $builder = $builder->add('id_entite', HiddenType::class, array());
    } else {
        if (suc('entite_multi')) {
            $builder = $builder->add('id_entite', ChoiceType::class, array(
                'label' => 'Entite',
                'expanded' => true,
                'label_attr' => array('class' => 'radio-inline'),
//                   'choices' => getEntitesDeLUtilisateur(),
                'choices' => sr_entites(array_keys($tab_prochains_servicerendu), getEntitesDeLUtilisateur()),
                'attr' => array('inline' => true)
            ));
        } else {
            $builder = $builder->add('id_entite', HiddenType::class, array());
        }
    }


    $builder->add('objet_beneficiaire', ChoiceType::class, array(
        'label' => 'beneficiaire',
        'required' => true,
        'expanded' => true,
        'label_attr' => array('class' => 'radio-inline'),
        'choices' => array('individu' => 'individu', 'membre' => 'membre'),
        'attr' => array('inline' => true, 'class_group' => 'choix_objet_beneficiaires')
    ));

    $builder->add('id_individu', TextType::class,
        [
            'label' => $app->trans('individu'),
            'required' => false,
            'attr' => array('class' => 'autocomplete_individu typeahead')
        ]);

    $builder->add('id_membre', TextType::class,
        [
            'label' => $app->trans('membre'),
            'required' => false,
            'attr' => array('class' => 'autocomplete_membre typeahead')
        ]);

    if ($lot) {
        $builder = $builder->add('id_prestationslot', ChoiceType::class, array(
            'label' => 'prestationslot',
            'choices' => array_flip(table_simplifier($tab_prestationslot)),
            'expanded' => true,
            'label_attr' => array('class' => 'radio-inline')
        ));
    }
    if (isset($tab_choix_prestation)) {
        foreach ($tab_choix_prestation as &$cp) {
            $cp = array_flip($cp);
        }
        if (isset($data_prestation)) {

            foreach ($data_prestation as $k => $d) {

                $subform = $app['form.factory']->createNamedBuilder($k, FormType::class, $d);
                $subform = servicerendu_formulaire($subform, $tab_choix_prestation, $d['id_prestation']);
                $builder->add($subform, '', array('label' => ''));
            }
        }

    }

    if (!$modification) {
        $builder = $builder
            ->add('reglement_question', CheckboxType::class, array(
                'label' => 'Differer le paiement',
                'required' => false,
                'attr' => array('align_with_widget' => true)
            ));

        $subform = $app['form.factory']->createNamedBuilder('reglement', FormType::class);
        $args_paiement = [];
        if ($id_objet_beneficiaire > 0) {
            $args_paiement = ['objet' => $objet_beneficiaire, 'id_objet' => $id_objet_beneficiaire];
        }
        list($subform, $tab_modepaiements, $tab_soldes) = getFormPaiement($subform, $args_paiement);
        $builder->add($subform, '', array('label' => ''));
    }
    $form = $builder
        ->add('submit', SubmitType::class,
            array('label' => sac('objet') . '_enregistrer', 'attr' => array('class' => 'btn-primary')))
        ->getForm();
    $form->handleRequest($request);
    if ($form->isSubmitted()) {

        $data = $form->getData();
        if (isset($data['id_membre'])) {
            $data['id_objet_beneficiaire'] = intval($data['id_membre']);
        } else {
            $data['id_objet_beneficiaire'] = intval($data['id_individu']);
        }

        if ($form->isValid()) {


            $nb_prestation = 1;
            if (!$modification && $lot) {
                $nb_prestation = count($tab_prestationslot[$data['id_prestationslot']]['prestations']);
            }
            $montant = 0;
            $tab_servicerendu = [];
            $tab_prestation_type=[];
            $montant_du = 0;
            for ($i = 0; $i < $nb_prestation; $i++) {

                if (isset($data['pr' . $i]['quantite']) && $data['pr' . $i]['quantite'] > 0) {

                    if (!$modification) {
                        $objet_data = new Servicerendu();
                    }
                    $objet_data->fromArray($data['pr' . $i]);
                    $objet_data->setIdEntite(intval($data['id_entite']));

                    if (isset($data['id_membre'])) {
                        $objet_data->setIdMembre(intval($data['id_membre']));
                    } else {
                        $objet_data->setIdMembre(null);
                    }
                    if (!$modification) {
                        $objet_data->setRegle(1);
                    }
                    $prestation_type = tab('prestation.' . $data['pr' . $i]['id_prestation'] . '.prestation_type');
                    $objet_data->save();

                    $montant_du = $montant_du + ($data['pr' . $i]['montant'] * floatval($data['pr' . $i]['quantite']));


                    $id_servicerendu = $objet_data->getIdServicerendu();
                    if ($data['objet_beneficiaire'] == 'individu') {
                        $objet_data_ind = new Servicerenduindividu();
                        $objet_data_ind->setIdServicerendu($id_servicerendu);
                        $objet_data_ind->setIdIndividu(intval($data['id_individu']));
                        $objet_data_ind->save();
                    }
                    $tab_servicerendu[$id_servicerendu] = $objet_data;
                    Log::EnrOp($modification, $objet, null, $id_servicerendu, 'SR' . $prestation_type);
                    // todo particulier Ify va etre remplacer par une taches
                    if (conf('restriction_mode.nom')=='mot_operateur') {
                        switch ($data['pr' . $i]['id_prestation']) {
                            case 1 :
                                $id_mot = 3;//14
                                break;
                            case 4 :
                                $id_mot = 22;//36
                                break;
                            case 7 :
                                $id_mot = 22;//45
                                break;
                            case 10 :
                                $id_mot = 23;//52
                                break;
                            case 13 :
                                $id_mot = 24;//100
                                break;
                            case 16 :
                                $id_mot = 23;//82
                                break;
                            case 22 :
                                $id_mot = 12;//8
                                break;
                            default :
                                $id_mot = 0;

                        };
                    };
                    if ($id_mot >0) {
//                        echo $id_mot.'--'.$data['id_membre'].'--'. 'membre'.'---'.date('d-m-Y');
//                        exit;
                        Mot::MotLien($id_mot, $data['id_membre'], 'membre',date('d-m-Y'));
                    }
//                 todo fin de particularité

                    if ($objet_beneficiaire == 'membre') {
                        $objet_data->ajouterIndividuDuMembre();
                    }
                    $quantite = 1;
                    if (isset($data['pr' . $i]['quantite'])) {
                        $quantite = floatval($data['pr' . $i]['quantite']);
                    }
                    $tab_servicerendu_montant[$id_servicerendu] = floatval($data['pr' . $i]['montant']) * $quantite;
                    $tab_prestation_type[] = tab('prestation_type.'.$prestation_type.'.nom');
                }
            }//fin enregistrement service rendu



            if (!$modification) {
                 if ( conf('carte_adherent.actif')){
                    if ((conf('carte_adherent.automatique_adhesion') && in_array( 'adhesion',$tab_prestation_type))
                    || (conf('carte_adherent.automatique_cotisation') && in_array( 'cotisation',$tab_prestation_type))
                    ) {
                        $id_mot = mot('carte_adh1');
                        $objet_carte = conf('carte_adherent.objet');
                        if (isset($data['id_membre']) && $data['id_membre'] > 0) {
                            if ($objet_carte == 'individu') {
                                $tab_individu = MembreQuery::create()->findPk($data['id_membre'])->getIndividusEnCours();
                                foreach ($tab_individu as $individu) {
                                    Mot::MotLien($id_mot, $individu->getPrimaryKey(), $objet_carte);
                                }
                            } else {
                                Mot::MotLien($id_mot, $data['id_membre'], $objet_carte);
                            }
                        } elseif ($data['id_individu']) {
                            if ($objet_carte == 'individu') {
                                Mot::MotLien($id_mot, $data['id_individu'], $objet_carte);
                            }
                        }
                    }
                }//fin de carte


                if ((!$data['reglement_question']) and ($data['reglement']['montant'] > 0 or isset($data['reglement']['solde']))) {


                    $tab_servicerendu_paye = array();
                    if ($data['reglement']['montant'] > 0 and !isset($data['reglement']['solde'])){
                        $paiement = new Paiement();
                        $data['id_objet'] = $data['id_objet_beneficiaire'];
                        $data['objet'] = $data['objet_beneficiaire'];
                        $paiement->fromArray($data);
                        $paiement->fromArray($data['reglement']);

                        $paiement->save();
                        $id_paiement = $paiement->getIdPaiement();
                        Log::EnrOp($modification, 'paiement', $paiement, $id_paiement);

                        $credit = floatval($data['reglement']['montant']);


                        foreach ($tab_servicerendu_montant as $id_servicerendu => &$montant) {
                            $montant_paiement = min($credit, $montant);
                            servicepaiement_creation($id_paiement, $id_servicerendu, $montant_paiement);
                            $montant_du -= $montant_paiement;
                            $credit -= $montant_paiement;
                            if ($montant_paiement == $montant) {
                                $tab_servicerendu_paye[$id_servicerendu] = $montant;
                                $tab_servicerendu[$id_servicerendu]->setRegle(2)->save();
                            } else {
                                $montant = $montant - $montant_paiement;
                                $tab_servicerendu[$id_servicerendu]->setRegle(3)->save();
                            }

                            if ($montant_paiement <= 0 && $credit == 0) {
                                break;
                            }
                        }
                    }
                    $tab_servicerendu_a_paye = array_diff_key($tab_servicerendu_montant, $tab_servicerendu_paye);

                    // S'il reste une partie de la ou des prestations à payer, on pioche éventuellement
                    // dans le solde de l'adherent

                    if ($montant_du >= 0.01) {

                        if (isset($data['reglement']['solde'])) {

                            foreach ($data['reglement']['solde'] as $id_paiement) {
                                // todo ajouter le filtre des cases cochées
                                $credit = $tab_soldes[$data['id_entite']][$id_paiement];

                                $tab_servicerendu_paye = array();
                                foreach ($tab_servicerendu_a_paye as $id_servicerendu => $montant) {

                                    $montant_paiement = min($credit, $montant);
                                    servicepaiement_creation($id_paiement, $id_servicerendu, $montant_paiement);
                                    $montant_du -= $montant_paiement;
                                    $credit -= $montant_paiement;
                                    if ($montant_paiement == $montant) {
                                        $tab_servicerendu_paye[$id_servicerendu] = $montant;
                                        $tab_servicerendu[$id_servicerendu]->setRegle(2)->save();
                                    } else {
                                        $montant = $montant - $montant_paiement;
                                        $tab_servicerendu[$id_servicerendu]->setRegle(3)->save();
                                    }
                                    if ($montant_paiement <= 0 && $credit == 0) {
                                        break;
                                    }
                                }
                                $tab_servicerendu_a_paye = array_diff_key($tab_servicerendu_montant,
                                    $tab_servicerendu_paye);


                                if ($montant_du <= 0) {
                                    break;
                                }

                            }
                        }
                    }
                }
            }
            if ($data['objet_beneficiaire'] == '') {
                $data['objet_beneficiaire'] = 'membre';
            }

            $args_rep['url_redirect'] = $app->path($data['objet_beneficiaire'],
                ['id_' . $data['objet_beneficiaire'] => $data['id_objet_beneficiaire']]);
           $ok=true ;
        }
    }

    if (!$ok) {
        if (isset($tab_prochains_servicerendu)) {
            foreach ($tab_prochains_servicerendu as &$pc_entite) {
                foreach ($pc_entite as &$pc_prestation) {
                    $pc_prestation['date_fin'] = $pc_prestation['date_fin']->format('d/m/Y');
                    $pc_prestation['date_debut'] = $pc_prestation['date_debut']->format('d/m/Y');
                }
            }
            $args_rep['tab_prochains_servicerendu'] = $tab_prochains_servicerendu;
        }
        $args_rep['tab_soldes'] = $tab_soldes;
        $args_rep['id_objet_beneficiaire'] = $id_objet_beneficiaire;
        $args_rep['modification'] = $modification;
        $args_rep['objet_beneficiaire'] = $objet_beneficiaire;
        $args_rep['form_name'] = $form_name;
        $args_rep['nb_prestation'] = $nb_max_prestation;
        $args_rep['lot'] = ($lot) ? true : false;
        if ($lot) {
            $temp = array();
            foreach ($tab_prestationslot as $k => $prestationlot) {
                $temp[$prestationlot['id_entite']][$k] = $prestationlot['prestations'];
            }
            $args_rep['tab_prestationslot'] = $temp;
        }
        $args_rep['tab_modepaiements'] = $tab_modepaiements;
    }
    return reponse_formulaire($form, $args_rep);
}


function servicerendu_formulaire($builder, $tab_choix_prestation, $defaut)
{

    global $app;
    $objet = sac('objet');
    $unite = '1';
    // l'unite doit servir au format de saisie de la quantite
    //         ->add('id_unite',ChoiceType::class, array('label' => $app->trans('unite'), 'choices' => uniteNom(), 'attr' => array()))

    $builder = $builder->add('id_prestation', ChoiceType::class, array(
        'label' => $app->trans($objet),
        'data' => $defaut,
        'choices' => $tab_choix_prestation,
        'expanded' => true,
        'label_attr' => array('class' => 'radio-inline')
    ))
        ->add('quantite', NumberType::class, array(
            'label' => $app->trans('quantite') . ' en ' . tab('unite.' . $unite . '.nom'),
            'attr' => array('placeholder' => '')
        ))
        ->add('total', MoneyType::class, array(
            'label' => $app->trans('total'),
            'attr' => array('placeholder' => '')
        ))
        ->add('montant', MoneyType::class, array('label' => 'Prix unitaire', 'attr' => array('placeholder' => '')))
        ->add('taux', MoneyType::class, array('label' => 'taux', 'attr' => array('placeholder' => '')))
        ->add('date_enregistrement', DateType::class, array(
            'label' => 'date_enregistrement',
            'widget' => 'single_text',
            'format' => 'dd/MM/yyyy',
            'attr' => array('class' => 'datepickerb')
        ))
        ->add('date_debut', DateType::class, array(
            'label' => 'date_debut',
            'widget' => 'single_text',
            'format' => 'dd/MM/yyyy',
            'attr' => array('class' => 'datepickerb')
        ))
        ->add('date_fin', DateType::class, array(
            'label' => 'au',
            'widget' => 'single_text',
            'format' => 'dd/MM/yyyy',
            'attr' => array('class' => 'datepickerb'),
        ))
        ->add('premier', IntegerType::class, array('label' => 'premier'))
        ->add('dernier', IntegerType::class, array('label' => 'dernier'))
        ->add('observation', TextareaType::class, array('label' => 'Observations'));

    return $builder;
}


function action_servicerendu_form_supprimer()
{
    ServicepaiementQuery::create()->filterByIdServicerendu(sac('id'))->delete();
    return action_supprimer_une_instance();
}

function sr_entites($ent_prestation, $ent_user)
{
    $test3 = array_flip($ent_prestation);
    foreach ($ent_user as $k => $id_e) {
        if (array_key_exists($id_e, $test3)) {
            $getentite_user_prestation[$k] = $id_e;
        }
    }
    return ($getentite_user_prestation);
}
