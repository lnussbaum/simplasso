<?php


use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\Extension\Core\Type\DateType;
use Symfony\Component\Form\Extension\Core\Type\FormType;
use Symfony\Component\Form\Extension\Core\Type\HiddenType;
use Symfony\Component\Form\Extension\Core\Type\MoneyType;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\Form\Extension\Core\Type\TextareaType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\FormError as FormError;
use Symfony\Component\Validator\Constraints as Assert;
use Propel\Runtime\Map\TableMap;

function restriction_form()
{
    global $app;
    $args_rep = [];
    $request = $app['request'];
    $selections_pref = pref('selection');
    //remplacer l'indice par le nom
    foreach ($selections_pref as $objet=>$valeur){
        foreach ($valeur as $key=> $val){
            $selections_choix[$objet][$val['nom']] = $key;
        }
    }
     if (sac('id')) {
        $modification = true;
        $objet_data = charger_objet();
        $data = $objet_data->toArray(TableMap::TYPE_FIELDNAME);
        $ancien=json_decode($data['variables'],true);
     } else {
         $modification = false;
         $objet_data = array();
         $data = array();
         $test=array();
    }

    unset($data['variables']);
    $builder = $app['form.factory']->createNamedBuilder('restriction',FormType::class,$data);

    $builder->add('nom', TextType::CLASS,array('label' => 'Nom', 'attr' => ['class'=>'']));
    $builder->add('nomcourt', TextType::CLASS,array('label' => 'Nomcourt', 'attr' => ['class'=>'']));
    $builder->add('actif', ChoiceType::class, array('expanded' => true,
        'label_attr' => array('class' => 'radio-inline'),'choices' => array('oui' => true, 'non' => false) ));

        $builder->add('selection', ChoiceType::CLASS,array(
            'label' => 'Eleves du ou des Profs',
            'expanded' => false,
            'multiple' => true,
            'choices' => tab('mot_arbre.individu.Prof')));
    $builder->setRequired(false);
    formSetAction($builder,$modification);
    $form = $builder->add('submit',SubmitType::class,
                array('label' => $app->trans('Enregistrer'), 'attr' => array('class' => 'btn-primary')))
            ->getForm();
    $form->handleRequest($request);
    if ($form->isSubmitted()) {
        $data = $form->getData();
        if ($form->isValid()) {
            $temp = array();
            if  (isset($data['selection'])) {
                    $temp['membre'] = array("search"=>array("regex"=>false,"value"=>""),"mots"=>$data['selection'],"sorti"=>"non");
                    $temp['individu'] = array("search"=>array("regex"=>false,"value"=>""),"mots"=>$data['selection'],"sorti"=>"non");
             }
            if (!$modification)
                $objet_data = new Restriction();

            $data['variables']=json_encode($temp);
            $objet_data->fromArray($data);
            $objet_data->save();
            $args_rep['id'] = $objet_data->getPrimaryKey();
            chargement_table();
        }
    }
    return reponse_formulaire($form,$args_rep);
}
function action_restriction_form_supprimer(){
    return action_supprimer_une_instance();
}