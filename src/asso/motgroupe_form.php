<?php


use Propel\Runtime\Map\TableMap;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\Validator\Constraints as Assert;


function motgroupe_form()
{
    global $app;
    $args_rep = [];
    $request = $app['request'];
    $modification = false;
    $data = [];

    //Modification du mot groupe existant
    if (sac('id')) {
        $objet_data = charger_objet();
        $modification = true;
        $data = $objet_data->toArray(TableMap::TYPE_FIELDNAME);
        $option = $objet_data->getOption();
        if (strpos($data['objets_en_lien'], ';') >= 0) {
            $tab_ol = explode(';', $data['objets_en_lien']);
        } else {
            $tab_ol=[$data['objets_en_lien']];
        }
        foreach($tab_ol as $ol){

            $data['nom_option1'] = $option[$ol]['nom'];
            $data['optgroup_option1'] = $option[$ol]['optgroup'];
            $data['classement_option1'] = $option[$ol]['classement'];
            $data['indice_option1'] = $option[$ol]['indice'];
            if (isset($option[$ol]['operateur']))
                $data['operateur'] = $option[$ol]['operateur'];

        }

    }


    $builder = $app['form.factory']->createNamedBuilder('motgroupe', MotgroupesForm::class, $data);
    $builder->setRequired(false);
    formSetAction($builder, $modification);
    $form = $builder->add('submit', SubmitType::class,
        array('label' => $app->trans('Enregistrer'), 'attr' => array('class' => 'btn-primary')))
        ->getForm();


    $form->handleRequest($request);

    if ($form->isSubmitted()) {
        $data = $form->getData();

        if ($form->isValid()) {
            if (!$modification) {
                $objet_data = new Motgroupe();
            }
            $tab_ol = strpos(';', $data['objets_en_lien']) >= 0 ? explode(';',
                $data['objets_en_lien']) : array($data['objets_en_lien']);
            $option = array();
            foreach ($tab_ol as $ol) {
                $option[$ol] = array(
                    'nom' => $data['nom_option1'],
                    'optgroup' => $data['optgroup_option1'],
                    'classement' => $data['classement_option1'],
                    'indice' => $data['indice_option1'],
                    'operateur' => $data['operateur']
                );
            }
            $data['options'] = json_encode($option);
            $objet_data->fromArray($data);
            $objet_data->save();
            chargement_table();
        }
    }

    return reponse_formulaire($form, $args_rep);

}


function action_motgroupe_form_supprimer()
{
    return action_supprimer_une_instance();
}