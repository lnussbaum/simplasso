<?php

use Propel\Runtime\Map\TableMap;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\Extension\Core\Type\FormType;
use Symfony\Component\Form\Extension\Core\Type\NumberType;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\Form\Extension\Core\Type\TextareaType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Validator\Constraints as Assert;


function prestationslot_form()
{
    global $app;
    $args_rep = [];
    $request = $app['request'];
    if (sac('id')) {
        $objet_data = charger_objet();
        $modification = true;
        $data = $objet_data->toArray(TableMap::TYPE_FIELDNAME);
    } else {
        $data = array();
        $objet_data = array();
        $modification = false;
    }
    $builder = $app['form.factory']->createNamedBuilder('prestationslot', FormType::class, $data);
    $builder->setRequired(false);
    formSetAction($builder, $modification);
    $form = $builder
        ->add('nom', TextType::class, array(
            'constraints' => new Assert\NotBlank(),
            'attr' => array('placeholder' => 'nom'),
        ))
        ->add('nomcourt', TextType::class, array(
            'label' => $app->trans('nomcourt'),
            'constraints' => new Assert\NotBlank(),
            'attr' => array('placeholder' => 'nom court')
        ))
        ->add('observation', TextareaType::class, array('label' => 'observation'))
        ->add('submit', SubmitType::class,
            array('label' => $app->trans('Enregistrer'), 'attr' => array('class' => 'btn-primary')))
        ->getForm();


    $form->handleRequest($request);
    if ($form->isSubmitted()) {
        $data = $form->getData();
        if ($form->isValid()) {
            if (!$modification) {
                $objet_data = new Prestationslot();
            }
            $objet_data->fromArray($data);
            $objet_data->save();
            $args_rep['id']=$objet_data->getPrimaryKey();
            chargement_table();
        }
    }

    return reponse_formulaire($form, $args_rep);
}


function action_prestationslot_form_ajouter_prestation()
{
    global $app;
    $args_rep = [];
    $request = $app['request'];
    $modification = false;
    $id_prestation = $request->get('id_prestation');
    if (!empty($id_prestation)) {
        $pr_du_lot = PrestationslotPrestationQuery::create()->filterByIdPrestationslot(sac('id'))->findOneByIdPrestation($id_prestation);
        $data = $pr_du_lot->toArray();
        $modification = true;
    }

        $builder = $app['form.factory']->createNamedBuilder('prestationslot', FormType::class);
        $builder->setRequired(false);
        formSetAction($builder, false, array('action' => 'ajouter_prestation', 'id_prestationslot' => sac('id')));
        $choices_prestation = array_flip(table_simplifier(tab('prestation')));
        $form = $builder
            ->add('id_prestation', ChoiceType::class, array('label' => 'prestation', 'choices' => $choices_prestation))
            ->add('quantite', NumberType::class)
            ->add('submit', SubmitType::class,
                array('label' => 'Ajouter', 'attr' => array('class' => 'btn-primary')))
            ->getForm();


        $form->handleRequest($request);
        if ($form->isSubmitted()) {
            if ($form->isValid()) {
                $data = $form->getData();
                if (!$modification) {
                    $pr_du_lot = new PrestationslotPrestation();
                }
                $pr_du_lot->fromArray($data);
                $pr_du_lot->setQuantite($data['quantite']);
                $pr_du_lot->setIdPrestationslot(sac('id'));
                $pr_du_lot->save();
                $args_rep['id'] = sac('id');
                $args_rep['modification'] = true;
                chargement_table();
            }
        }
        return reponse_formulaire($form, $args_rep);


}


function action_prestationslot_form_supprimer_prestation()
{
    global $app;
    $url_redirect = '';
    $builder = $app['form.factory']->createNamedBuilder('prestationslot', FormType::class);
    $builder->setRequired(false);
    $request = $app['request'];
    $id_prestation = $request->get('id_prestation');
    if (!empty($id_prestation)) {
        PrestationslotPrestationQuery::create()->filterByIdPrestationslot(sac('id'))->filterByIdPrestation($id_prestation)->delete();
    }
    if ($url_redirect == '') {
        $url_redirect = $app->path(sac('objet'), ['id_' . sac('objet') => sac('id')]);

    }
    chargement_table();
    return $app->redirect($url_redirect);

}