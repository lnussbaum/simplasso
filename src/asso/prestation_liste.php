<?php
function prestation_liste()
{
    global $app;
    $args_twig=[
        'tab_cols'=>prestation_colonnes()
        ];
    return $app['twig']->render(fichier_twig(), $args_twig);

}


function prestation_colonnes(){

    $tab_colonne= array();
	$tab_colonne['id_prestation'] = ['title'=> 'id'];
	//$tab_colonne['nom_groupe'] = [];
	$tab_colonne['nom'] = [];
	//$tab_colonne['nomcourt'] = [];
	$tab_colonne['descriptif'] = [];
	if (suc('entite_multi')) {
		$tab_colonne['id_entite'] = [];
	}
	//$tab_colonne['id_tva'] = [];
	//$tab_colonne['retard_jours'] = [];
	//$tab_colonne['prixlibre'] = [];
	//$tab_colonne['quantite'] = [];
	//$tab_colonne['id_unite'] = [];
	//$tab_colonne['id_compte'] = [];
	$tab_colonne['created_at'] = ["type" => "date-eu"];
	//$tab_colonne['nb_numero'] = [];
	//$tab_colonne['prochain_numero'] = [];
	//$tab_colonne['periodique'] = [];
	//$tab_colonne['signe'] = [];
	$tab_colonne['objet_beneficiaire'] = [];
	$tab_colonne['prestation_type'] = ['title'=>'type',"traitement" => 'transformePrestationType'];
	$tab_colonne['active'] = [];
	//$tab_colonne['nb_voix'] = [];

    $tab_colonne['action']=["orderable"=> false];
    $tab_colonne = datatable_complete_liste_colonne($tab_colonne);

    return $tab_colonne;

}





function action_prestation_liste_dataliste()
{

	return objet_liste_dataliste();

}


