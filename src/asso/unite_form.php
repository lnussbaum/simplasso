<?php

use Propel\Runtime\Map\TableMap;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\Extension\Core\Type\FormType;
use Symfony\Component\Form\Extension\Core\Type\IntegerType;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Validator\Constraints as Assert;

function unite_form()
{
    global $app;
    $args_rep = [];
    $request = $app['request'];
    $modification = false;
    $dupliquer = $request->get('dupliquer');
    $data = [];
    if (sac('id')) {
        $objet_data = charger_objet();
        $modification = true;
        $data = $objet_data->toArray(TableMap::TYPE_FIELDNAME);
    }

    $builder = $app['form.factory']->createNamedBuilder('tva', FormType::class, $data);
    $builder->setRequired(false);
    formSetAction($builder, $modification);
    $form = $builder
        ->add('nom', TextType::class, array(
            'constraints' => new Assert\NotBlank(),
            'attr' => array('class' => 'span2', 'placeholder' => 'Nom de la prestation'),
            'extra_fields_message' => 'Nom de la prestation'
        ))
        ->add('nomcourt', TextType::class, array(
            'constraints' => new Assert\NotBlank(),
            'attr' => array('class' => 'span2', 'placeholder' => 'Nom de la prestation'),
            'extra_fields_message' => 'Ncourt'
        ))
        ->add('prestation_type', ChoiceType::class, array(
            'label' => 'Type de prestation',
            'choices' => array_flip(getPrestationTypeActive()),
            'expanded' => false,
            'attr' => array('inline' => true, 'placeholder' => 'nom')
        ))
        ->add('duree', IntegerType::class,
            array('label' => $app->trans('duree'), 'attr' => array('placeholder' => '12')))
        ->add('id_compte', ChoiceType::class, array(
            'label' => $app->trans('compte'),
            'choices' => array_flip(table_simplifier(tab('compte'))),
            'attr' => array()
        ))
        ->add('submit', SubmitType::class,
            array('label' => $app->trans('Enregistrer'), 'attr' => array('class' => 'btn-primary')))
        ->getForm();


    $form->handleRequest($request);
    if ($form->isSubmitted()) {
        $data = $form->getData();

        if ($form->isValid()) {
            if (!$modification) {
                $objet_data = new Unite();
            } elseif ($dupliquer == "dupliquer") {
                $objet_data = $objet_data->copy(true);
            }
            $objet_data->fromArray($data);
            $objet_data->save();
            $args_rep['id'] = $objet_data->getPrimaryKey();
            chargement_table();
        }
    }
    return reponse_formulaire($form, $args_rep);
}


function action_unite_form_supprimer()
{
    return action_supprimer_une_instance();
}