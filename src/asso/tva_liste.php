<?php
function tva_liste()
{
    global $app;
    $args_twig=[
        'tab_col'=>tva_colonnes()

        ];
    return $app['twig']->render(fichier_twig(), $args_twig);

}


function tva_colonnes(){

    $tab_colonne= array();
	$tab_colonne['id_tva'] = ['title'=> 'id',];
	$tab_colonne['nom'] = [];
	$tab_colonne['nomcourt'] = [];
	$tab_colonne['id_compte'] = [];
	$tab_colonne['actif'] = [];
	$tab_colonne['signe'] = [];
	$tab_colonne['encaissement'] = [];
	$tab_colonne['id_entite'] = [];
	$tab_colonne['observation'] = [];
	$tab_colonne['created_at'] = ['type' => 'date-eu'];

    $tab_colonne['action']=["orderable"=> false];
    $tab_colonne = datatable_complete_liste_colonne($tab_colonne);

    return $tab_colonne;

}





function action_tva_liste_dataliste()
{

    return objet_liste_dataliste();

}


