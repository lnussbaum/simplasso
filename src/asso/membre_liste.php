<?php


use Symfony\Component\Validator\Constraints as Assert;


function membre_liste()
{
    global $app;
    $args_twig = [
        'tab_col' => membre_colonnes(),
        'tab_filtres' => membre_filtres()
    ];
    return $app['twig']->render(fichier_twig(), $args_twig);
}


function membre_colonnes()
{

    $tab_colonne = array();
    $tab_colonne['id_membre'] = ['title' => 'id'];
    $tab_colonne['created_at'] = ['title' => 'cree_le',"type" => "date-eu"];
    $tab_colonne['nom'] = [];
    $tab_colonne['telephones'] = ["orderable" => false,'title'=>'Mobile/téléphone'];
    $tab_colonne['ville'] = ['title' => 'ville',"orderable" => false];
    $tab_colonne['adresse'] = ['title' => 'adresse',"orderable" => false];
    $tab_colonne['email'] = ['title' => 'email',"orderable" => false];
    $tab_colonne['action'] = ["orderable" => false];
    $tab_colonne = datatable_complete_liste_colonne($tab_colonne);
    return $tab_colonne;
}



function membre_filtres()
{

    global $app;
    $request = $app['request'];
    $rechercher = $request->get('search');
    $tab_filtres['premier']['search'] = filtre_ajouter_recherche($rechercher['value']);
    $cotis = $request->get('cotisation');
    $tab_filtres['premier']['cotisation'] = filtre_ajouter_cotisation($cotis);
    $tab_filtres['second']['abonnement'] = filtre_ajouter_abonnement();
    if (empty($tab_filtres['second']['abonnement']))
        unset($tab_filtres['second']['abonnement']) ;
    $selection_defaut = pref('membre.selection_defaut');
    $tab_filtres['premier'] = array_merge($tab_filtres['premier'], filtre_ajouter_mots('membre'));

    $tab_filtres['second']['mots'] =  filtre_ajouter_mots('individu','ind_');
    $tab_filtre_second=[
        'coordonnees'=>['email', 'adresse','mobile','telephone','telephone_pro'],
        'adhesion'=>['sorti'],
        'cotisation'=>['ardent']
    ];
    foreach ($tab_filtre_second as $cat =>$tab_fs) {
        foreach ($tab_fs as $nom_filtre) {
            $valeur_filtre = $request->get($nom_filtre);
            if ($valeur_filtre === null && isset($selection_defaut[$nom_filtre])) {
                $valeur_filtre = $selection_defaut[$nom_filtre];
            }
            $tab_filtres['second'][$cat][$nom_filtre] = filtre_prepare_donnee($nom_filtre, filtre_modele($nom_filtre), $valeur_filtre);
        }
    }
    $tab_filtres['second']['cotisation'] += filtre_ajouter_cotisation_second();
    $tab_filtres['second']['geographique'] = filtre_ajouter_geo();
    $tab_filtres['second']['divers']['limitation_id'] = filtre_ajouter_limitation_id();
    $tab_filtres['second']['divers']['inverse_selection'] = filtre_ajouter_inverse();
    return $tab_filtres;

}






function action_membre_liste_dataliste()
{
    session_enregistrer_selection_courante();
    return objet_liste_dataliste('membre');
}



function membre_dataliste_complement($tab, $tab_data)
{
    foreach ($tab as $k => $membre) {
        $individu = $membre->getIndividuTitulaire();
        if ($individu) {
            $tab_data[$k]['telephones'] = $individu->getTelephones();
            $tab_data[$k]['ville'] = $individu->getVille();
            $tab_data[$k]['adresse'] = $individu->getAdresse();
            $tab_data[$k]['email'] = $individu->getEmail();
        }
    }
    return $tab_data;
}





function action_membre_liste_rechercher()
{
    global $app;
    $sous_requete = getSelectionObjet();
    $tab_data = array();
    $tab_data2 = array();
    $tab_id_membres = $app['db']->fetchAll($sous_requete . ' LIMIT 0,10');
    foreach ($tab_id_membres as &$result) {
        $result = $result['id_membre'];
    }
    $tab_membres = MembreQuery::getAll($tab_id_membres);
    foreach ($tab_membres as $membre) {
        $tab_data[] = array(
            'id' => $membre->getIdMembre(),
            'nom' => $membre->getNom()
        );
    }
    foreach ($tab_membres as $membre) {
        $tab_data2[] = $membre->getNom();
    }
    return $app->json($tab_data);
}


function action_membre_liste_etiquettes()
{
    global $app;
    include($app['basepath'] . '/src/systeme/etiquette.php');
    return imprimer_etiquette('membre');
}

function action_membre_liste_carte_adherent()
{
    global $app;
    include($app['basepath'] . '/src/systeme/etiquette.php');
    return imprimer_carte_adherent('membre');
}


function action_membre_liste_export_membre()
{
    global $app;
    include($app['basepath'] . '/src/inc/exports.php');
    $erreurs = action_export('membre');
    return $erreurs;
}



function action_membre_liste_export_vcard()
{
    global $app;
    include($app['basepath'] . '/src/inc/exports.php');
    $erreurs = action_export_vcard('membre');
    return $erreurs;
}
