<?php

use Propel\Runtime\Map\TableMap;
use Symfony\Component\Form\Extension\Core\Type\CheckboxType;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\Extension\Core\Type\DateType;
use Symfony\Component\Form\Extension\Core\Type\FormType;
use Symfony\Component\Form\Extension\Core\Type\HiddenType;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\FormError as FormError;
use Symfony\Component\Form\FormEvent as FormEvent;
use Symfony\Component\Form\FormEvents as FormEvents;
use Symfony\Component\Validator\Constraints as Assert;

include_once($app['basepath'] . '/src/asso/paiement_form.php');
include_once($app['basepath'] . '/src/inc/fonctions_ged.php');
include_once($app['basepath'] . '/src/inc/fonctions_geo.php');
include_once($app['basepath'] . '/src/inc/fonctions_a_classer.php');


function membre_form()
{

    global $app;
    $args_rep = [];
    $request = $app['request'];
    $id_membre = $request->get('id_membre');;
    $data = [];
    $modification = intval($id_membre) > 0;
    if (!$modification) {
        return membre_liaison_individu_form();
    }

    if ($id_membre) {
        $membre = MembreQuery::create()->findPk($id_membre);
        if (isset($membre)) {
            $data = $membre->toArray();
        }
    }

    $builder = $app['form.factory']->createNamedBuilder('form_individu', MembreForm::class, $data);
    $builder
        ->add('submit', SubmitType::class,
            ['label' => $app->trans('Enregistrer'), 'attr' => ['class' => 'btn-primary']]);
    $builder->setRequired(false);
    formSetAction($builder, $modification, ['id_membre' => $id_membre]);
    $form = $builder->getForm();


    $form->handleRequest($request);
    if ($form->isSubmitted()) {
        $data = $form->getData();

        if ($form->isValid()) {

            $membre->fromArray($data);
            $membre->save();
            Log::EnrOp($modification, 'membre', $membre, $data['id_membre']);
        }
    }

    $args_rep['form_schema'] = 'membre';
    $args_rep['js_init'] = 'composant_form_membre';
    return reponse_formulaire($form, $args_rep);

}


function membre_liaison_individu_form()
{
    global $app;
    $args_rep = [];
    $request = $app['request'];
    $id_membre = $request->get('id_membre');
    $modification = intval($id_membre) > 0;
    $data = ['mode_individu' => pref('membre.mode_individu')];
    $nouveau_membre = true;
    if ($id_membre > 0) {
        $membre = charger_objet('membre', $id_membre);
        if (isset($membre)) {
            $data['membre'] = $membre->toArray(TableMap::TYPE_FIELDNAME);
        }
        $nouveau_membre = false;

    } else {
        $data['individu']['pays'] = pref('individu.pays');
    }

    if (isset($data['nomcourt']) and $data['nomcourt'] == '') {
        $data['nomcourt'] = MembreQuery::genererNomCourt($data['nom_famille'], $data['prenom']);
    }

    $builder = $app['form.factory']->createNamedBuilder('form_membre', MembreForm::class, $data);
    $builder->setRequired(false);
    formSetAction($builder, $modification, ['id_membre' => $id_membre]);
    $builder->add('individu', IndividuForm::class);
    $builder->add('recherche_individu', TextType::class, array(
        'label' => $app->trans('individu_choisir'),
        'required' => false,
        'attr' => array(
            'class' => 'autocomplete_individu typeahead',
            'placeholder' => $app->trans('individu_choisirholder')
        )
    ))->addEventListener(FormEvents::PRE_SUBMIT, function (FormEvent $event) {
        $form = $event->getForm();
        $form->add('nom_famille', TextType::class,
            array('attr' => array('class' => '', 'placeholder' => 'nom de famille')));
    });

    $builder->add('id_individu', HiddenType::class)
        ->add('mode_individu', HiddenType::class)
        ->add('submit', SubmitType::class,
            ['label' => $app->trans('Enregistrer'), 'attr' => ['class' => 'btn-primary']]);

    if ($nouveau_membre) {
        switch (pref('membre.enchainement')) {
            case 'sr_cotisation':
                $builder = $builder->add('submit_cotisation', SubmitType::class, [
                    'label' => $app->trans('Enregistrer + cotisation'),
                    'attr' => ['class' => 'btn-primary']
                ]);
                break;
            case 'sr_adhesion':
                $builder = $builder->add('submit_adhesion', SubmitType::class, [
                    'label' => $app->trans('Enregistrer + adhesion'),
                    'attr' => ['class' => 'btn-primary']
                ]);
                break;
            case 'servicerendu':
                $builder = $builder->add('submit_servicerendu', SubmitType::class, [
                        'label' => $app->trans('Enregistrer + servicerendu'),
                        'attr' => array('class' => 'btn-primary')
                    ]
                );
                break;
        }
    }
    $form = $builder->getForm();


    $form->handleRequest($request);
    if ($form->isSubmitted()) {


        include_once($app['basepath'] . '/src/asso/individu_form.php');

        $data = $form->getData();
        $nouvel_individu = $data['mode_individu'] == 'nouvel';

        $form = verification_individu_form($form);
        if ($nouvel_individu && $data['id_individu'] > 0) {
            if (IndividuQuery::create()->filterByIdIndividu($data['id_individu'])->count() > 0) {
                $form->get('id_individu')->addError(new FormError($app->trans('erreur_saisie_individu_inconnu')));
            }
        }


        if ($form->isValid()) {


            $data_individu = $data['individu'];
            $ok_individu = false;
            if ($nouvel_individu) {

                // Création de l'individu

                $individu = new Individu();
                $individu->fromArray($data_individu);
                $individu->setNom($data_individu['nom_famille'] . ' ' . $data_individu['prenom']);
                $ok_individu = $individu->save();
                $id_individu = $individu->getIdIndividu();
                if (!$ok_individu) {
                    $mes = ($modification) ? 'individu_modifier_ko' : 'individu_ajouter_ko';
                    $app['session']->getFlashBag()->add('error', $app->trans($mes));
                } else {
                    individu_traitement_form($individu, $data_individu, false, 'individu');
                    if (conf('identifiant.idem_individu_membre')) {
                        $app['session']->getFlashBag()->add('info',
                            'faire la fonction idem_individu_membre ligne 175 de de membre_form_php pour individu');
                        //                        $new_individu = autoincrement();
                        //                        $new_membre = autoincrement(false, 'individu', 'membre');
                        //                        if ($new_individu) { $app['db']->executeQuery("ALTER TABLE asso_individus AUTO_INCREMENT =" . $new_individu);}
                    }
                    Log::EnrOp($modification, 'individu', $individu);
                }
            } else {
                $individu = IndividuQuery::create()->findPk($data['id_individu']);
                $data_individu = $individu->toArray();
                $id_individu = $individu->getIdIndividu();
                $ok_individu = true;
            }


            if ($nouveau_membre) {

                // Creation du membre
                $membre = new Membre();
                if (!isset($data['nom']) or (isset($data['nom']) or empty($data['nom']))) {
                    $data['nom'] = $data_individu['nom_famille'] . ' ' . $data_individu['prenom'];
                }
                if (empty($data['nom'])) {
                    $data['nom'] = 'A saisir';
                }
                $membre->setIdIndividuTitulaire($id_individu);
                $membre->fromArray($data);

                $ok_membre = $membre->save();
                if (conf('identifiant.idem_individu_membre')) {
                    $app['session']->getFlashBag()->add('info',
                        'faire la fonction idem_individu_membre ligne 297 de de membre_form_php pour individu');
                }
                Log::EnrOp($modification, 'membre', $membre);
            } else {
                $ok_membre = true;
            }

            if (!$ok_membre) {
                $mes = ($modification) ? 'membre_modifier_ko' : 'membre_ajouter_ko';
                $app['session']->getFlashBag()->add('error', $app->trans($mes));
            } else {
                $id_membre = $membre->getIdMembre();
                if (conf('module.restriction') and conf('restriction_mode.nom')=='mot_operateur') {
                    Mot::MotLien(suc('operateur.id'), $id_membre, 'membre');
                }
            }


            if ($ok_individu and $ok_membre) {
                $args = array('id_membre' => $id_membre);
                membre_individu_creation(array(
                    'id_individu' => $id_individu,
                    'id_membre' => $id_membre
                ));


                if ($nouveau_membre) {

                    switch (pref('membre.enchainement')) {

                        case 'sr_cotisation':

                            if ($form->get('submit_cotisation')->isClicked()) {
                                $args_rep['declencheur'] = 'cotisation';
                            }
                            break;
                        case 'sr_adhesion':
                            if ($form->get('submit_adhesion')->isClicked()) {
                                $args_rep['declencheur'] = 'cotisation';
                            }
                            break;
                        case 'servicerendu':
                            if ($form->get('submit_servicerendu')->isClicked()) {
                                $args_rep['declencheur'] = 'servicerendu';
                            }
                            break;
                    }
                }
                membre_individu_creation(['id_individu' => $id_individu, 'id_membre' => $id_membre]);
            }
        }
        $args_rep['id'] = $id_membre;
    }
    $args_rep['saisie_nom'] = true;
    $args_rep['nouveau_membre'] = $nouveau_membre;
    $args_rep['modification'] = $modification;
    return reponse_formulaire($form, $args_rep, 'asso/membre_liaison_individu_form.html.twig');
}


function action_membre_form_carte_adherent()
{

    global $app;
    include_once($app['basepath'] . '/src/inc/fonctions_carte.php');
    $request = $app['request'];
    $id_membre = $request->get('id_membre');
    return carte_changer_etat('membre', $id_membre);

}


function action_membre_form_titularise()
{
    global $app;
    $request = $app['request'];
    $id_individu = $request->get('id_individu');
    $id = $request->get('id_membre');
    if ($id and $id_individu) {
        $objet_data = charger_objet();
        $objet_data->setIdIndividuTitulaire($id_individu);
        $objet_data->save();
        Log::EnrOp('TIT', 'membre',$objet_data, $id);
        Log::EnrOp('TIT', 'individu', null,$id_individu);
        return $app->redirect($app->path('membre', array('id_membre' => $id)));
    }
}


function action_membre_form_detacher_individu()
{
    global $app;
    $request = $app['request'];
    $id_individu = $request->get('id_individu');
    $id = $request->get('id_membre');
    if ($id and $id_individu) {
       $individu = MembreIndividuQuery::create()->filterByIdIndividu($id_individu)->filterByIdMembre(sac('id'))->delete();
        Log::EnrOp('DET', 'membre',null, $id);
        Log::EnrOp('DET', 'individu', $individu,$id_individu);
        return $app->redirect($app->path('membre', array('id_membre' => sac('id'))));
    }
}


function action_membre_form_generer_nomcourt()
{
    global $app;
    $request = $app['request'];
    $nom = $request->get('nom');
    $prenom = $request->get('prenom');
    return MembreQuery::genererNomCourt($nom, $prenom);
}


function action_membre_form_sortir()
{
    global $app;
    $ok = false;

    $request = $app['request'];
    $id_membre = $request->get('id_membre');
    if ($id_membre !== null) {
        $objet = charger_objet();
    }

    $data = [
        'date_sortie' => new Datetime(),
        'desabonner_infolettre' => true,
    ];

    $builder = $app['form.factory']->createNamedBuilder('membre_sortir', FormType::class, $data,
        array('action' => $app->path('membre_form', array('action' => 'sortir', 'id_membre' => $id_membre))));
    $builder->setRequired(false);

    $builder = $builder
        ->add('date_sortie', DateType::class, array(
            'label' => 'Date de sortie',
            'widget' => 'single_text',
            'format' => 'dd/MM/yyyy',
            'attr' => array('class' => 'datepickerb')
        ))
        ->add('desabonner_infolettre', CheckboxType::class, array(
            'label' => 'Desabonner des mailling list'
        ));

    if ($id_membre) {


        list($derniere_adhesion, $montant_remboursable) = $objet->getAdhesionRemboursable();
        if ($objet->solde() > 0) {
            $choices_don = ['nerienfaire' => 'Ne rien faire', 'remboursement' => 'Remboursement', 'don' => 'Don'];
            $builder = $builder
                ->add('don', ChoiceType::class, array(
                    'label' => $app->trans('solde'),
                    'choices' => array_flip($choices_don),
                    'expanded' => true,
                    'attr' => array('inline' => false)
                ));
        } elseif ($objet->solde() < 0) {
            $choices_perte = ['nerienfaire' => 'Ne rien faire', 'perte' => 'Perte'];
            $builder = $builder
                ->add('perte', ChoiceType::class, array(
                    'label' => $app->trans('solde'),
                    'choices' => array_flip($choices_perte),
                    'expanded' => true,
                    'attr' => array('inline' => false)
                ));

        }


        list($derniere_adhesion, $montant_remboursable) = $objet->getAdhesionRemboursable();
        if ($montant_remboursable > 0) {
            $data_reglement = array(
                'id_membre' => sac('id'),
                'id_tresor' => pref('paiement.tresor'),
                'montant' => $montant_remboursable,
                'date_cheque' => new \DateTime(),
                'date_enregistrement' => new \DateTime()
            );
            $choices_don = ['' => 'Ne rien faire', 'remboursement' => 'Remboursement', 'don' => 'Don'];
            $builder = $builder
                ->add('adhesion_remboursable', ChoiceType::class, array(
                    'label' => $app->trans('adhesion remboursable'),
                    'choices' => array_flip($choices_don),
                    'expanded' => true,
                    'attr' => array('inline' => false)
                ))
                ->add('reglement_question', CheckboxType::class, array(
                    'label' => 'Differer le paiement',
                    'required' => false,
                    'attr' => array('align_with_widget' => true)
                ));


            $subform = $app['form.factory']->createNamedBuilder('reglement', FormType::class, $data_reglement);
            list($subform, $tab_modepaiements, $tab_soldes) = getFormPaiement($subform, $data_reglement);
            $builder->add($subform, '', array('label' => ''));
        }
    } else {
        $choices_don = ['' => 'Ne rien faire', 'don' => 'Don'];
        $builder = $builder
            ->add('adhesion_remboursable', ChoiceType::class, array(
                'label' => $app->trans('adhesion remboursable'),
                'choices' => array_flip($choices_don),
                'expanded' => true,
                'attr' => array('inline' => false)
            ));

    }
    $form = $builder->add('submit', SubmitType::class,
        array('label' => $app->trans('Enregistrer'), 'attr' => array('class' => 'btn-primary')))
        ->getForm();

    $form->handleRequest($request);
    if ($form->isSubmitted()) {
        $data = $form->getData();
        if ($form->isValid()) {
            if ($id_membre) {
                $tab_id = [$id_membre];
            } else {
                $args = $app['session']->get('selection_courante_membre');
                list($tab_id, $nb) = objet_liste_dataliste_preselection('membre', $args, false, false);

            }
            MembreQuery::create()->filterByPrimaryKeys($tab_id)->update(['DateSortie' => $data['date_sortie']]);
            $tab_membre = MembreQuery::create()->findPks($tab_id);

            if ($data['desabonner_infolettre']) {
                foreach ($tab_membre as $membre) {
                    $tab_individu = $membre->getIndividus();
                    foreach ($tab_individu as $individu) {
                        $email = $individu->getEmail();
                        if (!empty($email)) {
                            include_once($app['basepath'] . '/src/inc/fonctions_infolettre.php');
                            changer_inscription($email, []);
                        }
                    }
                }
            }

            foreach ($tab_id as $id) {

                if (!$id_membre) {
                    $objet = charger_objet(sac('objet'), $id);
                    list($derniere_adhesion, $montant_remboursable) = $objet->getAdhesionRemboursable();
                }


                if (!empty($data['adhesion_remboursable'])) {

                    $date_enr = new DateTime();
                    if ($derniere_adhesion) {
                        $id_entite = $derniere_adhesion->getIdEntite();
                        $adhesion_cloture = $derniere_adhesion->copy();
                        $adhesion_cloture->setOrigine($derniere_adhesion->getIdServicerendu());
                        $adhesion_cloture->setDateEnregistrement($date_enr);
                        $adhesion_cloture->setCreatedAt($date_enr);
                        $adhesion_cloture->setDateDebut($data['date_sortie']);
                        $adhesion_cloture->setDateFin($data['date_sortie']);
                        $adhesion_cloture->setMontant('-' . $derniere_adhesion->getMontant());
                        $adhesion_cloture->setComptabilise(0);
                        $adhesion_cloture->setRegle(0);
                        $adhesion_cloture->setObservation('');
                        $adhesion_cloture->save();
                    }

                    if ($data['adhesion_remboursable'] == 'don') {

                        $don = new Servicerendu();
                        if ($id_membre) {
                            $montant_adhesion = $data['reglement']['montant'];
                        } else {
                            $montant_adhesion = $derniere_adhesion->getMontant();
                        }

                        $id_prestation_don = array_keys(tab_prestation('don'))[0];
                        $don->fromArray(array(
                            'id_prestation' => $id_prestation_don,
                            'prestation_type' => tab('prestation.' . $id_prestation_don . '.prestation_type'),
                            'id_entite' => $id_entite,
                            'date_enregistrement' => $date_enr->format('Y-m-d'),
                            'montant' => $montant_adhesion,
                            'observation' => 'Don suite à la fin d\'adhésion',
                            'comptabilise' => false,
                            'id_membre' => $id
                        ));

                        $don->setDateEnregistrement(new DateTime());
                        $don->setDateDebut($data['date_sortie']);
                        $don->setDateFin($data['date_sortie']);
                        $don->save();


                    } elseif ($data['adhesion_remboursable'] == 'remboursement') {
                        if ($data['reglement_question'] != 1 and $data['reglement']['montant'] != 0) {
                            $paiement = new Paiement();
                            $paiement->setIdObjet($id);
                            $paiement->setObjet('membre');
                            $paiement->setIdEntite($id_entite);
                            $paiement->fromArray($data['reglement']);
                            $paiement->setMontant('-' . $paiement->getMontant());
                            $paiement->save();

                            servicepaiement_creation($paiement->getIdPaiement(), $adhesion_cloture->getPrimaryKey(),
                                min(-$data['reglement']['montant'], -$derniere_adhesion->getMontant()));
                        }
                    }
                }
                Log::EnrOp('SOR', 'membre', null,$id);

            }
            if ($id_membre) {
                $url_redirect = $app->path('membre', array('id_membre' => $id_membre));
            } else {
                $url_redirect = $app->path('membre_liste');
            }

            $ok = true;
        }
    }


    $args_twig = array(
        'form' => $form->createView(),
        'js' => 'membre_sortir'
    );

    if ($ok) {
        $message = $app->trans(sac('objet_action') . '_ok');
    } else {
        $args_twig['ajax'] = '';
        if ($app['request']->isXmlHttpRequest()) {
            $args_twig['ajax'] = '_zero';
        }
        $message = $app['twig']->render('inclure/form.html.twig', $args_twig);
    }
    if ($app['request']->isXmlHttpRequest() && !empty($request->get('json'))) {
        return $app->json(array('message' => $message, 'ok' => $ok, 'redirect' => $url_redirect));
    } else {
        if ($ok) {
            $app['session']->getFlashBag()->add('success', $message);
            return $app->redirect($url_redirect);
        } else {
            return $message;
        }
    }


}

function action_membre_form_reprendre()
{
    global $app;
    $objet_data = charger_objet();
    $id = $objet_data->getIdMembre();
    $objet_data->setDateSortie(null);
    $objet_data->save();
    Log::EnrOp('REI', 'membre',$objet_data, $id);
    return $app->redirect($app->path('membre', array('id_membre' => $id)));
}


function action_membre_form_recopier_adresse()
{
    global $app;

    $url_redirect = $app['request']->get('redirect');
    $membre = charger_objet();
    $individu_titulaire = $membre->getIndividuTitulaire();
    $tab_data = [
        'adresse' => $individu_titulaire->getAdresse(),
        'codepostal' => $individu_titulaire->getCodepostal(),
        'ville' => $individu_titulaire->getVille()
    ];
    $position = $individu_titulaire->getPosition();

    $tab_individu = $membre->getIndividus();
    foreach ($tab_individu as $individu) {
        if ($individu->getPrimaryKey() != $individu_titulaire->getPrimaryKey()) {
            if ($position){
                creer_modifier_position_lien($position->getLon(), $position->getLat(), 'individu', $individu->getIdIndividu());
            }
            $individu->fromArray($tab_data);
            $individu->save();
        }
    }
    return $app->redirect($url_redirect);
}


function action_membre_form_supprimer($retour = '')
{
    global $app;
    $objet1 = 'Membreindividu';
    $id = sac('id');
    if ($retour = '') {
        $retour = 'membre_liste';
    }
    $url_redirect = construire_redirection($retour);
    if ($id > 0) {
        $class = 'MembreindividuQuery';
        if (!class_exists($class)) {

            $app['session']->getFlashBag()->add('info', 'La classe MembreindividuQuery n\'existe pas');
            return $app->redirect($url_redirect);

        }

        $objet_data = $class::create()->filterByIdMembre($id)->find();
        if ($objet_data) {
            $objet_data->delete();
            $app['session']->getFlashBag()->add('info',
                'La suppression de ' . $app->trans($objet1) . ' : ' . nommer_objet($objet1,
                    $objet_data) . ' est effective');
        }

    }


    return action_supprimer_une_instance();
}

function action_membre_form_ajoute_titularise()
{
    global $app;
    $request = $app['request'];
    $url_redirect = $app['request']->get('redirect');
    $id_individu = $request->get('titularise');
    $individu = charger_objet( 'individu', $id_individu);
    $membre = new Membre();
    $membre->setIdIndividuTitulaire($id_individu);
    $membre->setNomcourt(MembreQuery::genererNomCourt($individu->getNomFamille(), $individu->getPrenom()));
    $membre->setNom($individu->getNom());
    $ok = $membre->save();
    if ($ok) {
        $id = $membre->getIdMembre();
        Log::EnrOp('CRE', 'membre', null,$id);
        Log::EnrOp('TIT', 'individu', null,$id_individu);
        if (conf('identifiant.idem_individu_membre')) {
            $app['session']->getFlashBag()->add('info',
                'faire la fonction idem_individu_membre ligne 414 de individu_form et ligne 271 de membre_form_php');
        }
        membre_individu_creation(array('id_membre' => $id, 'id_individu' => $id_individu));
    } else {
        $app['session']->getFlashBag()->add('error', 'Le membre n\'est pas ajouté');
    }
    return $app->redirect($url_redirect);

}


function action_membre_form_fusion()
{
    global $app;
    $args_rep = [];
    $request = $app['request'];
    $data['id_membre_origine'] = $request->get('id_membre');
    $args_rep['titre'] = $request->get('titre');
    $data['titre'] = $args_rep['titre'];
    $data['id_membre'] = null;
    $builder = $app['form.factory']->createNamedBuilder('form_membre', FormType::class, $data);
    $builder->setRequired(false);
    $form = $builder
        ->add('recherche_membre', TextType::class, array(
            'label' => $app->trans('membre_choisir'),
            'required' => false,
            'attr' => array('class' => 'autocomplete_membre typeahead',
                'placeholder' => $app->trans('membre_choisirholder'))))
        ->addEventListener(FormEvents::PRE_SUBMIT, function (FormEvent $event) {
            $form = $event->getForm();
            $form->add('nom', TextType::class,
                array('attr' => array('class' => '', 'placeholder' => 'nom du membre')));
        })
        ->add('recherche_individu', TextType::class, array(
            'label' => $app->trans('individu_choisir'),
            'required' => false,
            'attr' => array('class' => 'autocomplete_individu typeahead',
                'placeholder' => $app->trans('individu_choisirholder'))))
        ->addEventListener(FormEvents::PRE_SUBMIT, function (FormEvent $event) {
            $form = $event->getForm();
            $form->add('nom_individu', TextType::class,
                array('attr' => array('class' => '', 'placeholder' => 'nom individu')));
        })
        ->add('id_individu', TextType::class)
        ->add('id_membre', TextType::class)
        ->add('submit', SubmitType::class,
            array('label' => 'Fusionner', 'attr' => array('class' => 'btn-primary')))
        ->getForm();
    $form->handleRequest($request);
    if ($form->isSubmitted()) {
        $data = $form->getData();
        if ($data['id_membre']==$data['id_membre_origine']) {
            $form->get('recherche_membre')->addError(new FormError($app->trans('erreur_saisie_id_identique')));
        }
        if ($data['id_membre']<1) {
            $form->get('recherche_membre')->addError(new FormError($app->trans('erreur_saisie_pas_de_choix')));
        }
        if ($form->isValid()) {
            $membre = MembreQuery::create()->findPk($data['id_membre']);
            if ($membre) {
                $membre_origine = MembreQuery::create()->findPk($data['id_membre_origine']);
                if ($membre_origine) {
                    //copie de membre dans onservation
                    $valeur_transferee =' fusion membre : '.$membre->getNom().' '.
                        $membre->getIdentifiantInterne().' numero '.   $data['id_membre'];
                    $membre_origine->setObservation($membre->getObservation().$valeur_transferee);
                    //copie de individu dans observation
                    $id_individu=$membre->getIdIndividuTitulaire();
                    $individu = IndividuQuery::create()->findPk($id_individu);
                    if ($individu) {
                        $id_individu_origine=$membre_origine->getIdIndividuTitulaire();
                        $individu_origine = IndividuQuery::create()->findPk($id_individu_origine);
                        if ($membre_origine) {
                            $valeur_transferee =' fusion membre : '.$individu->getNom().' '.
                                $individu->getEmail().' numero '.   $data['id_membre'];
                            $individu_origine->setObservation($individu->getObservation().$valeur_transferee);
                            $individu_origine->save();
                            $where='id_objet = '.$id_individu." and objet = 'individu' ";
                            $set='id_objet = '. $id_individu_origine;
                            $transfert[]=array('fichier'=>'asso_paiements',"set"=>$set,"where"=>$where);
                            $liens = MotLienQuery::create()->filterByIdObjet($id_individu)->findByObjet('individu');
                            foreach ($liens as $lien){
                                $mot=$lien->getIdMot();
                                Mot::MotLien($lien->getIdMot(), $id_individu, 'individu','',true);
                                Mot::MotLien($mot, $id_individu_origine, 'individu','fusion '.$id_individu);
                            }
                            $where= 'id_individu = '.$id_individu ;
                            $set='id_individu = '. $id_individu_origine;
                            $transfert[]=array('fichier'=>'asso_servicerendus_individus',"set"=>$set,"where"=>$where);

// il existe une fonction dans branche membre form
                            $membresindividus = MembreIndividuQuery::create()->filterByIdMembre($data['id_membre'])->findOneByIdIndividu($id_individu);
                            if ($membresindividus)  $membresindividus->delete();
                            $membresindividus = MembreIndividuQuery::create()->filterByIdMembre($data['id_membre_origine'])->findOneByIdIndividu($id_individu_origine);
                            if (!$membresindividus) {
                                $membresindividus = new MembreIndividu();
                                $membresindividus->setIdIndividu($id_individu_origine);
                                $membresindividus->setIdMembre($data['id_membre_origine']);
                                $membresindividus->save();
                            }
                            $delete_individu=true;

                        }else{
                            $app['session']->getFlashBag()->add('info', 'Pas trouvé l\'individu du membre origine numéro :'.$membre_origine->getIdIndividuTitulaire());
                        }
                    }else{
                        $app['session']->getFlashBag()->add('info', 'Pas trouvé l\'individu du membre fusionné numéro :'.$membre->getIdIndividuTitulaire());
                    }
// todo reste a faire notification, mots_liens non individu et membre voir avec motgroupe, logs ?
                    //todo les autres individus à faire et faire attention aux individus ayant plusieurs membre
                    //transfert paiement, servicerendu mots liens
                    $where='id_objet = '.$data['id_membre']. " and objet = 'membre' ";
                    $set='id_objet = '. $data['id_membre_origine'];
                    $transfert[]=array('fichier'=>'asso_paiements',"set"=>$set,"where"=>$where);

                    $liens = MotLienQuery::create()->filterByIdObjet($data['id_membre'])->findByObjet('membre');
                    foreach ($liens as $lien){
                        $mot=$lien->getIdMot();
                        Mot::MotLien($lien->getIdMot(), $id_individu, 'membre','',true);
                        Mot::MotLien($mot, $data['id_membre_origine'], 'membre','fusion '.$data['id_membre']);
                    }

                    $where= 'id_membre = '.$data['id_membre'] ;
                    $set='id_membre = '. $data['id_membre_origine'];
                    $transfert[]=array('fichier'=>'asso_servicerendus',"set"=>$set,"where"=>$where);
                    foreach($transfert as $v){
                        $sql='update '.$v['fichier'].' set '.$v['set'].' where '.$v['where'];
                        $app['db']->executequery($sql);
                    }
// todo gerer les archives a faire
                    $membre_origine->save();
                    Log::EnrOp('FUS', 'membre',  $data['id_membre']);

                    $membre->delete();
                    if ($delete_individu) $individu->delete();

                }else{
                    $app['session']->getFlashBag()->add('info', 'Pas trouvé le membre origine numéro :'.$data['id_membre_origine']);
                }
            }else{
                $app['session']->getFlashBag()->add('info', 'Pas trouvé leu membre fusionné numéro :'.$data['id_membre']);
            }
        }
    }
    return reponse_formulaire($form, $args_rep, 'asso/membre_fusion.html.twig');
}

function action_membre_form_fusionderniermaster()
{
    global $app;
    $args_rep = [];
    $request = $app['request'];
    $data['id_membre_vers'] = $request->get('id_membre');
    $args_rep['titre'] = $request->get('titre');
    $data['titre'] = $args_rep['titre'];
    $data['id_membre'] = 0;
    $builder = $app['form.factory']->createNamedBuilder('form_membre', FormType::class, $data);
    $builder->setRequired(false);
    $form = $builder
        ->add('recherche_membre', TextType::class, array(
            'label' => $app->trans('membre_choisir'),
            'required' => false,
            'attr' => array(
                'class' => 'autocomplete_membre typeahead',
                'placeholder' => $app->trans('membre_choisirholder')
            )
        ))
        ->addEventListener(FormEvents::PRE_SUBMIT, function (FormEvent $event) {
            $form = $event->getForm();
            $form->add('nom_membre', TextType::class,
                array('attr' => array('class' => '', 'placeholder' => 'nom du membre')));
        })
        ->add('recherche_individu', TextType::class, array(
            'label' => $app->trans('individu_choisir'),
            'required' => false,
            'attr' => array(
                'class' => 'autocomplete_individu typeahead',
                'placeholder' => $app->trans('individu_choisirholder')
            )
        ))
        ->addEventListener(FormEvents::PRE_SUBMIT, function (FormEvent $event) {
            $form = $event->getForm();
            $form->add('nom_individu', TextType::class,
                array('attr' => array('class' => '', 'placeholder' => 'nom individu')));
        })
        ->add('id_individu', TextType::class)
        ->add('id_membre', TextType::class)
        ->add('submit', SubmitType::class,
            array('label' => 'Fusionner', 'attr' => array('class' => 'btn-primary')))
        ->getForm();
    $form->handleRequest($request);
    if ($form->isSubmitted()) {
        $data = $form->getData();
        if ($data['id_membre'] == $data['id_membre_origine']) {
            $form->get('recherche_membre')->addError(new FormError($app->trans('erreur_saisie_id_identique')));
        }
        if ($data['id_membre'] < 1) {
            $form->get('recherche_membre')->addError(new FormError($app->trans('erreur_saisie_pas_de_choix')));
        }
        if ($form->isValid()) {
            $id_membre_depuis = $data['id_membre'];
            $id_membre_vers = $data['id_membre_origine'];
            $membre_depuis = MembreQuery::create()->findPk($id_membre_depuis);
            if ($membre_depuis) {
                $membre_vers = MembreQuery::create()->findPk($id_membre_vers);
                if ($membre_vers) {
                    $valeur_transferee = ' fusion membre : ' . $membre_depuis->getNom() . ' ' .
                        $membre_depuis->getIdentifiantInterne() . ' numero ' . $id_membre_depuis;
                    $membre_vers->setObservation($membre_depuis->getObservation() . $valeur_transferee);
                    //copie de individu dans observation
                    $id_individu_depuis = $membre_depuis->getIdIndividuTitulaire();
                    $individu_depuis = IndividuQuery::create()->findPk($id_individu_depuis);
                    if ($individu_depuis) {
                        $id_individu_vers = $membre_vers->getIdIndividuTitulaire();
                        $individu_vers = IndividuQuery::create()->findPk($id_individu_vers);
                        if ($membre_vers) {
                            $valeur_transferee = ' fusion membre : ' . $individu_depuis->getNom() . ' ' .
                                $individu_depuis->getEmail() . ' numero ' . $id_membre_depuis;
                            $individu_vers->setObservation($individu_depuis->getObservation() . $valeur_transferee);
                            $individu_vers->save();
                            $depuis = 'id_objet = ' . $id_individu_depuis . " and objet = 'individu' ";
                            $vers = 'id_objet = ' . $id_individu_vers;
                            $transfert[] = array('fichier' => 'asso_paiements', "vers" => $vers, "de" => $depuis);

                            $liens_transfere[] = array(
                                'objet' => 'individu',
                                'depuis' => $id_individu_depuis,
                                'vers' => $id_individu_vers,
                                'msg' => 'fusion'
                            );

                            $depuis = 'id_individu = ' . $id_individu_depuis;
                            $vers = 'id_individu = ' . $id_individu_vers;
                            $transfert[] = array(
                                'fichier' => 'asso_servicerendus_individus',
                                "vers" => $vers,
                                "de" => $depuis
                            );
                            membre_individu_creation(array(
                                'id_individu' => $id_individu_depuis,
                                'id_membre' => $id_membre_depuis
                            ), false);
                            membre_individu_creation(array(
                                'id_individu' => $id_individu_vers,
                                'id_membre' => $id_membre_vers
                            ));
                            $delete_individu = true;

                        } else {
                            $app['session']->getFlashBag()->add('info',
                                'Pas trouvé l\'individu du membre origine numéro :' . $membre_vers->getIdIndividuTitulaire());
                        }
                    } else {
                        $app['session']->getFlashBag()->add('info',
                            'Pas trouvé l\'individu du membre fusionné numéro :' . $membre_depuis->getIdIndividuTitulaire());
                    }
// todo reste a faire notification, mots_liens non individu et membre voir avec motgroupe, logs ?
                    //todo les autres individus à faire et faire attention aux individus ayant plusieurs membre
                    //transfert paiement, servicerendu mots liens
                    $depuis = 'id_objet = ' . $id_membre_depuis . " and objet = 'membre' ";
                    $vers = 'id_objet = ' . $id_membre_vers;
                    $transfert[] = array('fichier' => 'asso_paiements', "de" => $depuis, "vers" => $vers);

                    $liens_transfere[] = array(
                        'objet' => 'membre',
                        'depuis' => $id_membre_depuis,
                        'vers' => $id_membre_vers
                    );
                    $depuis = 'id_membre = ' . $id_membre_depuis;
                    $vers = 'id_membre = ' . $id_membre_vers;

                    Mot::MotLiensTransfere($liens_transfere);
                    $transfert[] = array('fichier' => 'asso_servicerendus', "vers" => $vers, "de" => $depuis);
                    foreach ($transfert as $v) {
                        $sql = 'update ' . $v['fichier'] . ' set ' . $v['vers'] . ' where ' . $v['depuis'];
                        echo '<br>' . $sql;
//                        $app['db']->executequery($sql);
                    }
// todo gerer les archives a faire
                    $membre_vers->save();
                    Log::EnrOp('FUS', 'membre', $membre_vers,$id_membre_depuis);

                    $membre_depuis->delete();
                    if ($delete_individu) {
                        $individu_depuis->delete();
                    }

                } else {
                    $app['session']->getFlashBag()->add('info',
                        'Pas trouvé le membre origine numéro :' . $id_membre_vers);
                }
            } else {
                $app['session']->getFlashBag()->add('info',
                    'Pas trouvé leu membre fusionné numéro :' . $id_membre_depuis);
            }
        }
    }
    return reponse_formulaire($form, $args_rep, 'asso/membre_fusion.html.twig');
}


