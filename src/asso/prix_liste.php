<?php
function prix_liste()
{
    global $app;
    $args_twig=[
        'tab_col'=>prix_colonnes()
        ];
    return $app['twig']->render(fichier_twig(), $args_twig);

}


function prix_colonnes(){

    $tab_colonne= array();
	$tab_colonne['id_prix'] = ['title'=> 'id'];
	$tab_colonne['id_prestation'] = [];
	$tab_colonne['montant'] = [];
	$tab_colonne['date_debut'] = ['type'=> 'date-eu'];
	$tab_colonne['date_fin'] = ['type'=> 'date-eu'];


    $tab_colonne['action']=["orderable"=> false];
    $tab_colonne = datatable_complete_liste_colonne($tab_colonne);

    return $tab_colonne;

}





function action_prix_liste_dataliste()
{

    return objet_liste_dataliste();

}


