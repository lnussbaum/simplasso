<?php

use Propel\Runtime\Map\TableMap;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\Extension\Core\Type\EmailType;
use Symfony\Component\Form\Extension\Core\Type\FileType;
use Symfony\Component\Form\Extension\Core\Type\FormType;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\Form\Extension\Core\Type\TextareaType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Validator\Constraints as Assert;

include_once($app['basepath'] . '/src/inc/fonctions_ged.php');

function entite_form()
{
    global $app;
    $args_rep = [];
    $request = $app['request'];
    if (sac('id')) {
        $objet_data = charger_objet();
        $modification = true;
        $data = $objet_data->toArray(TableMap::TYPE_FIELDNAME);
    } else {
        $data = array();
        $objet_data = array();
        $modification = false;
        $data['pays'] = pref('individu.pays');
    }
    $builder = $app['form.factory']->createNamedBuilder('entite', FormType::class, $data);
    $builder->setRequired(false);
    formSetAction($builder, $modification);
    $choices_pays = array_flip(tab('pays'));

    $form = $builder
        ->add('nom', TextType::class, array(
            'constraints' => new Assert\NotBlank(),
            'attr' => array('class' => 'span2', 'placeholder' => 'nom'),
            'extra_fields_message' => 'foobar'
        ))
        ->add('nomcourt', TextType::class, array(
            'label' => $app->trans('nomcourt'),
            'constraints' => new Assert\NotBlank(),
            'attr' => array('class' => 'span2', 'placeholder' => 'ncours'),
            'extra_fields_message' => 'foobar'
        ))
        ->add('image', FileType::class, array('label' => 'Logo', 'attr' => array('class' => 'jq-ufs secondaire')))
        ->add('adresse', TextareaType::class, array('label' => 'Adresse', 'attr' => array('placeholder' => '')))
        ->add('codepostal', TextType::class, array(
            'label' => 'Code postal',
            'attr' => array('placeholder' => '', 'class' => 'autocomplete_cp', 'data-champ_commun' => 'ville')
        ))
        ->add('ville', TextType::class,
            array('label' => 'Ville', 'attr' => array('placeholder' => '', 'class' => 'autocomplete_ville')))
        ->add('pays', ChoiceType::class,
            array('label' => 'Pays', 'choices' => $choices_pays, 'attr' => array('class' => 'secondaire')))
        ->add('telephone', TextType::class,
            array('label' => 'Téléphone fixe', 'attr' => array('class' => 'telephone', 'placeholder' => '')))
        ->add('fax', TextType::class, array('attr' => array('class' => 'telephone secondaire', 'placeholder' => '')))
        ->add('email', EmailType::class, array(
            'constraints' => array(/*new Assert\NotBlank(),*/
                new Assert\Email()
            ),
            'attr' => array('placeholder' => 'adresse@email.fr'),
            'label' => 'Email',
        ))
        ->add('submit', SubmitType::class,
            array('label' => $app->trans('Enregistrer'), 'attr' => array('class' => 'btn-primary')))
        ->getForm();


    $form->handleRequest($request);
    if ($form->isSubmitted()) {
        $data = $form->getData();
        if ($form->isValid()) {
            if ($modification) {
                $objet_data->fromArray($data);
                $objet_data->save();
                $id = $objet_data->getIdEntite();
            } else {
                require_once($app['basepath'] . '/src/inc/fichier_creation.php');
                $id = entite_creation($data);
            }
            traitement_form_geo($objet_data, $data, $modification, 'entite');
            traitement_form_ged('entite_image',$id, 'entite','logo');
            $args_rep['id'] = $id;
            chargement_table();
        }
    }
    return reponse_formulaire($form, $args_rep);
}


function action_entite_form_supprimer()
{
    return action_supprimer_une_instance();
}