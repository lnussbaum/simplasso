<?php


function motgroupes()
{
    global $app;
    $args_twig=[
        'choix'=> table_simplifier(table_filtrer_valeur(tab('motgroupe'),'id_parent',0)),
        'tab_mots_orphelins'=> table_simplifier(table_filtrer_valeur(tab('mot'),'id_motgroupe',0))
    ];
    return $app['twig']->render('asso/motgroupes.html.twig', $args_twig);

}

