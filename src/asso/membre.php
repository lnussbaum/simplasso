<?php
function membre()
{
    global $app;
    $args_twig['objet_data'] = charger_objet();
    $args_twig['declencheur'] = preparation_declencheur(['cotisation','lot','adhesion','don']);
    return $app['twig']->render(fichier_twig(), $args_twig);

}
