<?php

use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\Extension\Core\Type\FormType;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\FormError as FormError;
use Symfony\Component\Validator\Constraints as Assert;


function autorisation_form()
{//todo reste retour de suppression et affichage de création a l'ouverture
    global $app;
    $request = $app['request'];
    $id_individu = $request->get('id_individu');
    $faire = $request->get('xx');
    $id_operateur = suc('operateur.id');
    $args_rep=[];
    $modification = false;
    $data = array();
    $choices_niveau[0] = 'Aucun';
    $choices_profil = getProfil();
    $choices_niveau = array_merge($choices_niveau, getNiveau());
    $choices_restriction = ['Aucune' => null];
    $choices_restriction = array_merge($choices_restriction,
        array_flip(table_simplifier(charger_table('restriction'))));
    if ($id_individu) {
        $objet_data = IndividuQuery::create()->findPk($id_individu);
        $nom = $objet_data->getNom();
        $tab_ind = array($objet_data->getIdIndividu() => $nom);
    } else {
        $nom = '';
    }
    foreach ($app['user']->getRoles() as $role) {
        $pos = strpos($role, '-');
        $id_entite = substr($role, $pos + 1);
        $tab_entite[$id_entite] = $id_entite;
        $profil = $role[0];
        $tab_profil[$role[0]] = $role[0];
        if ($pos > 1) {
            $niveau = substr($role, 1, $pos - 1);
            $tab_niveau[$niveau] = $niveau;
            if ($id_operateur == 2) {
                $niveau = 6;
            }// todo pour permettre la modif et voir l'ensemble des droits
            $tab_saisie[$profil . $id_entite] = array(
                'niveau_operateur' => $niveau,
                'id_restricrion' => null,
                'niveau_utilisateur' => 0,
                'id_autorisation' => 0,
                'id_entite' => $id_entite,
                'profil' => $profil,
                'key' => $profil . $id_entite
            );
        }
    }
    if (intval($id_individu)) {
        $modification = true;
        $tab_base = AutorisationQuery::create()
            ->filterByIdIndividu($id_individu)
            ->filterbyprofil($tab_profil)
            ->filterbyidEntite($tab_entite)
            ->find();
        foreach ($tab_base as $tab) {
            $key = $tab->getProfil() . $tab->getIdEntite();
            if (!isset($tab_saisie[$key]) or $tab->getNiveau() > $tab_saisie[$key]['niveau_operateur']) {
                unset($tab_saisie[$tab->getProfil() . $tab->getIdEntite()]);
            } else {
                $tab_saisie[$key]['niveau_utilisateur'] = $tab->getNiveau();
                $tab_saisie[$key]['id_autorisation'] = $tab->getIdAutorisation();
                $tab_saisie[$key]['id_restriction'] = $tab->getIdRestriction();
            }
        }
    }
    $data['id_individu'] = $id_individu;
    $builder = $app['form.factory']->createNamedBuilder('autorisation', FormType::class, $data);
    $builder->setRequired(false);
    formSetAction($builder, $modification, ['id_individu' => $id_individu]);

    if ($modification) {
        $builder
            ->add('id_individu', TextType::class, array(
                'data' => $nom,
                'label' => $app->trans('individu'),
                'attr' => array(),
                'disabled' => true
            ));
    } else {
        $builder
            ->add('id_individu', TextType::class, array(
                'label' => $app->trans('numero individu'),
                'constraints' => new Assert\NotBlank(),
                'attr' => array('class' => 'autocomplete_individu typeahead')
            ));
        if (conf('module.restriction') and conf('restriction_mode.nom') == 'mot_operateur') {
            $builder
                ->add('specifique', ChoiceType::class, array(
                    'label' => $app->trans(conf('restriction_mode.nom')),
                    'expanded' => true,
                    'data' => true,
                    'label_attr' => array('class' => 'radio-inline'),
                    'choices' => array('oui' => true, 'non' => false),
                    'attr' => array('inline' => true)
                ));
        }
    }
    foreach ($tab_saisie as $tab) {
        if (isset($tab)) {
            $choix = array();
            for ($j = 0; $j <= $tab['niveau_operateur']; $j++) {
                $choix[$choices_niveau[$j]] = $j;
            }
//            $data['ligne' . $tab['key']] = $tab['niveau_utilisateur'];
            if (!isset($tab['id_restriction'])) {
                $tab['id_restriction'] = 1;
            }
            $builder->add('ligne' . $tab['key'], ChoiceType::class, array(
                'label' => tab('entite.' . $tab['id_entite'] . '.nomcourt') . ' : ' . $choices_profil[$tab['profil']],
                'data' => $tab['niveau_utilisateur'],
                'expanded' => true,
                'label_attr' => array('class' => 'radio-inline'),
                'choices' => $choix
            ));
            if (conf('module.restriction') and conf('restriction_mode.nom') == 'restriction') {
                $builder->add('ligneR' . $tab['key'], ChoiceType::class, array(
                    'label' => 'Restriction : ',
                    'data' => $tab['id_restriction'],
                    'choices' => $choices_restriction,
                    'attr' => array()
                ));
            }
        }
    }
    $lab = 'Enregistrer les autorisations';
    if ($faire != 'voir') {
        $builder->add('submit', SubmitType::class,
            array('label' => $lab, 'attr' => array('class' => 'btn-primary')));
    }
    $form = $builder->getForm();
    $form->handleRequest($request);
    if ($form->isSubmitted()) {
        $data = $form->getData();
        $objet_data_individu = IndividuQuery::create()->findpk($data['id_individu']);
        if ($form->isValid() and $objet_data_individu) {
            if (conf('module.restriction') and conf('restriction_mode.nom') == 'mot_operateur') {
                if ('spécipique') {
                    $prenom = strtoupper($objet_data_individu->getPrenom());
                    include_once($app['basepath'] . '/src/inc/fichier_creation.php');
                    $data_compte = array(
                        'ncompte' => '511' . substr($prenom, 0, 4) . $data['id_individu'],
                        'nom' => 'Encaissement ' . $prenom,
                        'nomcourt' => substr($prenom, 0, 6),
                        'id_poste' => 1,
                        'id_entite' => pref('en_cours.id_entite'),
                        'type_op' => 3,
                    );
                    $id_compterb = compte_creation($data_compte);
                    $data_compte = array(
                        'ncompte' => '411' . substr($prenom, 0, 4) . $data['id_individu'],
                        'nom' => 'Tiers ' . $prenom,
                        'nomcourt' => substr($prenom, 0, 6),
                        'id_poste' => 1,
                        'id_entite' => pref('en_cours.id_entite'),
                        'type_op' => 3,
                    );
                    $id_compte = compte_creation($data_compte);
                    $id_journal = journal_creation(array(
                        'id_compte' => $id_compterb,
                        'nom' => substr('Encaissement ' . $prenom, 0, 25),
                        'nomcourt' => substr($prenom, 0, 6),
                        'actif' => 1,
                        'id_entite' => pref('en_cours.id_entite'),
                        'mouvement' => 3,
                    ));
                    $id_tresor = tresor_creation(array(
                        'id_entite' => pref('en_cours.id_entite'),
                        'nom' => 'Encaissement ' . $prenom,
                        'nomcourt' => substr($prenom, 0, 6),
                        'iban' => 'N° iban  ' . $prenom,
                        'bic' => substr('Bic  ' . $prenom, 0, 12),
                        'id_compterb' => intval($id_compterb),
                        'id_compte' => intval($id_compte),
                    ));
                    //prefence opérateur en création
                    $nom_racine = 'paiement';
                    $pref_paiement = pref('paiement');
                    $pref_paiement['entite'] = pref('en_cours.id_entite');
                    $pref_paiement['tresor'] = $id_tresor;
                    $pref_paiement['export']['entite'] = pref('en_cours.id_entite');
                    $pref_paiement['export']['tresor'] = $id_tresor;
                    enregistre_preference('paiement', $pref_paiement, $data['id_individu'], null, true);
                }
            }
            foreach ($tab_saisie as $tab) {
                $objet_data = null;
                if (!isset($tab['id_restriction'])) {
                    $tab['id_restriction'] = 1;
                }
                if (!isset($data['ligneR' . $tab['key']])) {
                    $data['ligneR' . $tab['key']] = 1;
                }
                if ($tab['niveau_utilisateur'] != $data['ligne' . $tab['key']]
                    or ($tab['id_restriction'] != $data['ligneR' . $tab['key']])
                ) { //changement
                    if ($tab['id_autorisation'] >= 1) {
                        $objet_data = AutorisationQuery::create()->findpk($tab['id_autorisation']);
                    }//charger
                    if ($data['ligne' . $tab['key']] >= 1) {
                        if ($tab['id_autorisation'] <= 0) {//creer
                            if (!isset($objet_data)) {//si en creation d'un existant le 21/9/2017
                                $objet_data = AutorisationQuery::create()
                                    ->filterByIdIndividu($data['id_individu'])
                                    ->filterByProfil($tab['profil'])
                                    ->filterByIdEntite($tab['id_entite'])
                                    ->findone();
                                if (!isset($objet_data)) {
                                    $objet_data = new Autorisation();
                                    $objet_data->setIdEntite($tab['id_entite']);
                                    $objet_data->setIdIndividu($data['id_individu']);
                                    $objet_data->setProfil($tab['profil']);
                                }
                            }
                        }
                        $objet_data->setNiveau($data['ligne' . $tab['key']]);
                        if (conf('module.restriction') and conf('restriction_mode.nom') == 'restriction') {
                            if (!empty($data['ligneR' . $tab['key']])) {
                                $objet_data->setIdRestriction($data['ligneR' . $tab['key']]);
                                $temp = RestrictionQuery::create()
                                    ->filterByPrimaryKey($data['ligneR' . $tab['key']])
                                    ->findOne()->getVariables();
                                $objet_data->setVariables($temp);
                            }
                        } else {
                            $objet_data->setIdRestriction(1);
                            $objet_data->setVariables(null);
                        }
                        $objet_data->save();//enregistre
                        $tab['id_autorisation'] = $objet_data->getIdAutorisation();
                        Log::EnrOp($modification, 'autorisation', $objet_data, $tab['id_autorisation']);

                    } elseif ($tab['id_autorisation'] >= 1) {
                        $objet_data->delete();//supprimer
                        Log::EnrOp('SUP', 'autorisation', $objet_data, $tab['id_autorisation']);
                    }

                }

            }
            if ($objet_data)
                $args_rep['id'] = $objet_data->getPrimaryKey();
        }
    }

    return reponse_formulaire($form, $args_rep);

}

function action_autorisation_form_modifier()
{
    return autorisation_form();
}

function action_autorisation_form_supprimer()
{
    return action_supprimer_une_instance();
}