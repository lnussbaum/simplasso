<?php

use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\Extension\Core\Type\DateType;
use Symfony\Component\Form\Extension\Core\Type\FormType;
use Symfony\Component\Form\Extension\Core\Type\HiddenType;
use Symfony\Component\Form\Extension\Core\Type\MoneyType;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\Form\Extension\Core\Type\TextareaType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\FormError as FormError;
use Symfony\Component\Validator\Constraints as Assert;

function paiement_form()
{
    global $app;
    $request = $app['request'];
    $args_rep = [];
    $id = sac('id');
    $objet_data = null;
    $args = [];
    if (!$id) {
        $id = $request->get('id_paiement');
        setContexte('id', $id);
    }
    $data = ['objet' => 'membre', 'id_objet' => null];
    if ($request->get('id_membre') !== null) {
        $data['objet'] = 'membre';
        $data['id_objet'] = $request->get('id_membre');
        $args['id_membre'] = $data['id_objet'];
    } elseif ($request->get('id_individu') !== null) {
        $data['objet'] = 'individu';
        $data['id_objet'] = $request->get('id_individu');
        $args['id_individu'] = $data['id_objet'];
    }
    if ($id > 0) {
        $modification = true;
        $objet_data = charger_objet();
        $data = $objet_data->toArray();
        $data['date_cheque'] = $objet_data->getDateCheque();
        $data['date_enregistrement'] = $objet_data->getDateEnregistrement();
        $data['objet'] = $objet_data->getObjet();
        $data['id_objet'] = $objet_data->getIdObjet();
    } else {
        $modification = false;
        $objet_data = null;
        $data['id_tresor'] = pref('paiement.tresor');
        $data['montant'] = 0;
        $data['id_paiement'] = null;
        $data['date_cheque'] = new Datetime();
        $data['date_enregistrement'] = new Datetime();
        $data['montant'] = 0;
        $data['id_servicerendu'] = $request->get('id_servicerendu');
        setContexte('entite', suc('entite'));
        if ($data['id_servicerendu']) {
            $servicerendu = charger_objet( 'servicerendu',  $data['id_servicerendu']);
            $data['id_entite'] = $servicerendu->getIdEntite();
            $modification = true;
            $data['id_prestation'] = $servicerendu->getIdPrestation();
            if ($id_membre = $servicerendu->getIdMembre()){
                $data['objet'] = 'membre';
                $data['id_objet'] = $id_membre;
            }
            else {
                $data['objet'] = 'individu';
                //$data['id_objet'] = $servicerendu->getIndividus();
            }
        }

    }
    $data['montant_origine'] = $data['montant'];

    $builder = $app['form.factory']->createNamedBuilder('form_paiement', FormType::class, $data);
    formSetAction($builder, $modification, $args);
    $builder->setRequired(false);
//    if ($data['objet'] == '') {
    $builder->add('objet', ChoiceType::class, array(
        'label' => $app->trans('beneficiare'),
        'required' => true,
        'expanded' => true,
        'label_attr' => array('class' => 'radio-inline'),
        'choices' => array('individu' => 'individu', 'membre' => 'membre'),
        'attr' => array('inline' => true)
    ));
// }
    if ($data['id_objet'] > 0) {
//        $builder->add('id_objet',HiddenType::class);
        $builder->add('id_objet', TextType::class);
        $temp = charger_objet($data['objet'],  $data['id_objet']);
        $args_twig['soustitre'] = 'Pour ' . $temp->getNom() . ' :';
    } else {
        $builder->add('id_objet', TextType::class, array(
            'label' => $app->trans($data['objet']),
            'attr' => array('class' => 'autocomplete_' . $data['objet'] . ' typeahead'),
            'disabled' => $modification
        ));
        $args_twig['soustitre'] = '  ';
    }

    //todo la valeur par defaut pourra etre modifié par les preferences non réalisé
    if (!suc('entite_multi')) {
        $builder = $builder->add('id_entite', HiddenType::class, array('data' => suc('entite')[0]));
    } else {
        $builder = $builder->add('id_entite', ChoiceType::class, array('data' => suc('entite')[0],
            'label' => $app->trans('entite'),
            'expanded' => true,
            'label_attr' => array('class' => 'radio-inline'),
            'choices' => getEntitesDeLUtilisateur(),
            'attr' => array('inline' => true)
        ));
    }
    list($subform, $tab_modepaiements, $tab_soldes) = getFormPaiement($builder, $data, false);
    $args_twig['soustitre'] .= sous_titre($tab_soldes);
    $form = $builder
        ->add('submit', SubmitType::class,
            array('label' => 'Enregistrer le paiement', 'attr' => array('class' => 'btn-primary')))
        ->getForm();
    $form->handleRequest($request);
    if ($form->isSubmitted()) {
        $erreur = null;
        $data = $form->getData();
        if ($data['montant'] == 0 ) {
            $erreur = 'Saisir un montant';
        }
        if ($form->isValid() and empty($erreur)) {
            // avec un paiement on ne peut utiliser 2 entites
            if (!$modification and $data['montant'] != 0) {
                $objet_data = new Paiement();
                //$service_paiement = new Servicepaiement();//si une prestation est choisie
            }
            $objet_data->fromArray($data);
            $objet_data->setIdObjet(intval($data['id_objet']));
            $objet_data->setObjet($data['objet']);
            $objet_data->save();
            $id_paiement = $objet_data->getIdPaiement();
            $id_entite = $objet_data->getIdEntite();
            Log::EnrOp($modification, 'paiement', $objet_data, $id_paiement);


            // Utilisation du paiement selon les choix dans les dettes

            $montant = floatval($data['montant']);



            if ($montant >= 0.01) {
                if (isset($data['utilise'])) {

                    foreach ($data['utilise'] as $id_servicerendu) {
                        if (isset($tab_soldes[$id_entite][$id_servicerendu])) {
                            $dispo = $tab_soldes[$id_entite][$id_servicerendu]['utilise']['montant'];
                            if ($montant >= $dispo) {
                                $montant -= $dispo;
                                $regle = 2;
                            } else {
                                $dispo = $montant;
                                $montant = -1;
                                $regle = 3;
                            }
                            $paie[$id_servicerendu] = array(
                                'mt' => $dispo,
                                'regle' => $regle,
                                'id_servicepaiement' => $tab_soldes[$id_entite][$id_servicerendu]['utilise']['id_servicepaiement']
                            );
                            $tab_soldes[$id_entite][$id_servicerendu]['utilise'] = 0;
                            if ($montant <= 0) {
                                break;
                            }
                        }
                    }
                }

                if (isset($data['solde'])) {
                    foreach ($data['solde'] as $id_servicerendu) {
                        $solde_dispo = $tab_soldes[$id_entite][$id_servicerendu]['solde'];
                        if ($montant >= $solde_dispo) {
                            $montant -= $solde_dispo;
                            $regle = 2;
                        } else {
                            $besoin = $solde_dispo;
                            $solde_dispo = $montant;
                            $montant -= $besoin;
                            $regle = 3;
                        }
                        if (isset($paie[$id_servicerendu]) && $paie[$id_servicerendu]['id_servicepaiement']) {
                            servicepaiement_modifie($paie[$id_servicerendu]['id_servicepaiement'], ($solde_dispo + $paie[$id_servicerendu]['mt']));
                            unset($paie[$id_servicerendu]);
                        } else {
                            servicepaiement_creation($id_paiement, $id_servicerendu, $solde_dispo);
                        }
                        servicerendu_regle($id_servicerendu, $regle);
                        if ($montant <= 0) {
                            break;
                        }
                    }
                }
            }

            // Suppression des lignes de service paiement

            if (isset($tab_soldes)) {
                foreach ($tab_soldes as $id_entite => &$soldes) {
                    foreach ($soldes as $id_servicerendu=> &$sold) {
                        if (isset($sold['utilise']['montant']) && $sold['utilise']['montant'] != 0) {
                            ServicepaiementQuery::create()->findPk($sold['utilise']['id_servicepaiement'])->delete();
                        }
                    }
                }
            }
            if (isset($paie)) {
                foreach ($paie as $key => $temp) {
                    if ($paie[$key]['id_servicepaiement'])
                        servicepaiement_modifie($paie[$key]['id_servicepaiement'], $paie[$key]['mt']);
                }
            }
            $message = '';
            if ($montant >= 0.01) {
                $message = 'Ajout d\'une somme disponible de ' . $montant . ' €uros';
            }
//            ecrire_config('pref.paiement',
//                array('entite' => $data['id_entite'], 'tresor' => $data['id_tresor']));
            $app['session']->getFlashBag()->add('success', $app->trans(sac('objet_action') . '_ok') . $message);
            $args_twig['message'] = $message;
//            arbre($data['objet']);
//            arbre(descr($data['objet'].'.cle'));
//            arbre($data['id_objet']);
//            arbre($data['id_objet']);
//            exit;
//
            $url_redirect = $app->path($data['objet'], array(descr($data['objet'] . '.cle') => $data['id_objet']));
//            $url_redirect = $app->path('membre', array('id_membre' => $data['id_objet']));
            $ok = true;
            // todo reste un enregistrement dans servicepaiement a zero quand on diminue le montant de cheque utilse pour 2 service rendu
            // todo voir la ligne 130 $sup avait peut être été faite pour cela
        } else {
            $form->addError(new FormError($erreur . $app->trans('probleme_contole_traitement_formulaire')));
        }
    }
    $args_rep['paiement'] = $data;
    $args_rep['modification'] = $modification;
    $args_rep['tab_combinaisons'] = getEntitesDeLUtilisateur();
    $args_rep['tab_soldes'] = $tab_soldes;
    $args_rep['tab_modepaiements'] = $tab_modepaiements;
    $args_rep['objet_data'] = $objet_data;
    return reponse_formulaire($form, $args_rep);


}


function servicepaiement_creation($id_paiement, $id_servicerendu, $montant)
{
    $objet_data = new Servicepaiement();
    $objet_data->setIdPaiement($id_paiement);
    $objet_data->setIdServicerendu($id_servicerendu);
    $objet_data->setMontant($montant + 0);
    $objet_data->save();
    return $objet_data->getIdServicepaiement();

}

function servicepaiement_modifie($id_servicepaiement, $montant)
{
    $objet_data = ServicepaiementQuery::create()->findPk($id_servicepaiement);
    $objet_data->setMontant($montant);
    $objet_data->save();
    return $objet_data->getIdServicepaiement();

}

function servicerendu_regle($id_servicerendu, $regle)
{
    $temp = ServicerenduQuery::create()->findPk($id_servicerendu);
    $temp->setRegle($regle);
    $temp->save();
    return true;

}

function servicerendu_creation($data)
{
    $objet_data = new Servicerendu();
    $objet_data->fromArray($data);
    $objet_data->setIdPrestation($data['id_prestation']);
    $objet_data->setIdEntite(intval($data['id_entite']));
    $objet_data->save();
    return $objet_data->getIdServicerendu();

}

/**  détermine le statut de paiement du service rendu
 * @param $du_mt si negatif il y a encore du dispo
 * @param $servicerendu_mt si le montant du service rendu est null il est considéré comme réglé
 * @return int le statut de paiement du service rendu
 * '1' => 'Non',
 * '2' => 'Oui',
 * '3' => 'Partiel+',
 * '4' => '+Don',
 * '5' => '-Perte',
 * '6' => '+Autre ou disponible',
 * '7' => 'A revoir'  voir variables  getListeRegle()
 */


function sous_titre($tab_soldes)
{
    global $app;
    $nb_entites = count($tab_soldes);
    $nb_prestations = round((count($tab_soldes, 1) - $nb_entites) / 2);
    if ($nb_prestations) {
        if ($nb_entites >= 2) {
            $temp = $app->trans('entites');
        } else {
            $temp = $app->trans('entite');
        }
        if ($nb_prestations >= 2) {
            $temp1 = $app->trans('prestations');
        } else {
            $temp1 = $app->trans('prestation');
        }
        $soustitre = '* ' . $nb_prestations . ' ' . $temp1 . ' ' . $app->trans('en attente de paiement pour ') . $nb_entites . ' ' . $temp;
    } else {
        $soustitre = '-';
    }
    return $soustitre;

}

function getFormPaiement($builder, $data = array(), $origine = true)
{
    global $app;
    $tab_modepaiements = suc('tresor');
    $tab_regle = getListeRegle();
    $lib_utilise = 'Utilise';

    $choices_utilise = array();
    $choices_soldes = array();
    $tab_soldes = array();



    if (isset($data['id_objet']) && $data['id_objet'] > 0) {

        if ($origine) {
            $lib = 'disponible';
            $tab_paiements = PaiementQuery::listePaiementsDUnMembre($data['id_objet'], $data['objet']);
            if (!empty($tab_paiements)) {
                foreach ($tab_paiements as  $paiement) {
                    $tsolde = $paiement['montant'] - $paiement['montantutilise'];

                    if ($tsolde >= 0.001) {
                        $lib_solde = ' réglement du ' . $paiement['tdc'] . ' enregistré le ' . $paiement['tde'] . ' pour ' . $paiement['nomen'] . ' reste : ' . $tsolde . ' Euros par le '.trans('mode_de_paiement').' : '.$paiement['nomtr'];
                        $choices_soldes[$lib_solde] = $paiement['id_paiement'];
                        $tab_soldes[$paiement['id_entite']][$paiement['id_paiement']] = $tsolde;
                    }
                }
            }
        } else {

            $lib = 'du';
            $tab_sr = ServicerenduQuery::listeServiceRenduEtSommePaiementDUnMembre($data['id_objet']);

            if (!empty($tab_sr)) {
                foreach ($tab_sr as $sr) {
                    $tsolde = $sr['sr_total'] - $sr['montantpaye'];
                    if ($tsolde >= 0.001) {
                        $lib_periode='';
                        if ($sr['prestationtype'] != 4)
                            $lib_periode=' du ' . $sr['tdd'] . ' au ' . $sr['tdf'];
                        $lib_solde = $sr['id_servicerendu'].' - '.$sr['nompr'].' '.$lib_periode. ' pour ' . $sr['nomen'] . ' du : ' . $tsolde . ' Euros';
                        $choices_soldes[$lib_solde] = $sr['id_servicerendu'];
                        $tab_soldes[$sr['id_entite']][$sr['id_servicerendu']]['solde'] = $tsolde;
                    }
                }

            }
            $tab_temp = array();
            if (isset($data['id_servicerendu']))
                $tab_temp = PaiementQuery::listeDesPaiementsPourUnServiceRendu($data['id_servicerendu']);
            elseif (isset($data['id_paiement']))
                $tab_temp = ServicerenduQuery::listeDesServiceRenduPourUnPaiement($data['id_paiement']);
            if (!empty($tab_temp)) {
                $lib_regle = ' €uros ' . $app->trans('regle') . ' :';
                foreach ($tab_temp as $key => $temp) {
                    $tmp = $temp['montantpaye'] * 1;
                    $lib_uti = $temp['nompr'] . ' du ' . $temp['tdd'] . ' au ' . $temp['tdf'] . ' pour ' . $temp['nomen'] . ' paiement de : ' . $temp['montantpaye'] . ' sur ' . $temp['montant'] . $lib_regle . $tab_regle[$temp['regle']];
                    $choices_utilise[$lib_uti] = $temp['id_servicerendu'];
                    $tab_soldes[$temp['id_entite']][$temp['id_servicerendu']]['utilise'] = array(
                        'montant' => $tmp,
                        'id_servicepaiement' => $temp['id_servicepaiement']
                    );
                }
            }


        }
    }
    //todo deplacement du montant apres le choix entite et du lieu de remise caisse ou banque

    foreach ($tab_modepaiements as $id_entite => $modepaiements)
        $tab_modepaiements_choice[$id_entite] = array_flip($modepaiements);

    $builder->add('id_tresor', ChoiceType::class, array(
        'label' => 'Mode de paiement',
        'expanded' => true,
        'choices' => $tab_modepaiements_choice,
        'required' => true,
        'label_attr' => array('class' => 'radio-inline'),
        'constraints' => new Assert\NotBlank()
    ))
        ->add('montant', MoneyType::class, array('label' => 'Montant', 'attr' => array('placeholder' => '')))
        ->add('date_cheque', DateType::class, array(
            'label' => 'Date du cheque',
            'format' => 'dd/MM/yyyy',
            'widget' => 'single_text',
            'attr' => array('class' => 'datepickerb')
        ))
        ->add('date_enregistrement', DateType::class, array(
            'label' => 'Date d\'enregistrement',
            'format' => 'dd/MM/yyyy',
            'widget' => 'single_text',
            'attr' => array('class' => 'datepickerb')
        ))
        ->add('numero', TextType::class, array( ))
        ->add('observation', TextareaType::class, array());
    if ($choices_utilise) {
        $builder->add('utilise', ChoiceType::class, array(
            'label' => $lib_utilise,
            'expanded' => true,
            'choices' => $choices_utilise,
            'multiple' => true,
            'attr' => array('class' => 'multiselectshow'),
            'required' => false,
            'label_attr' => array('class' => 'radio-inline')
        ));
    }
    if ($choices_soldes) {
        $builder->add('solde', ChoiceType::class, array(
            'label' => $lib,
            'expanded' => true,
            'choices' => $choices_soldes,
            'multiple' => true,
            'attr' => array('class' => 'multiselectshow'),
            'label_attr' => array('class' => 'radio-inline')
        ));
    }
    return array($builder, $tab_modepaiements, $tab_soldes);
}


function action_paiement_form_supprimer()
{
    return action_supprimer_une_instance();
}