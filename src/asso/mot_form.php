<?php

use Symfony\Component\Form\FormError as FormError;
use Symfony\Component\Form\Extension\Core\Type\FormType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\Extension\Core\Type\TextareaType;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\Validator\Constraints as Assert;


function mot_form()
{
    global $app;
    $args_rep = [];
    $request = $app['request'];
    $objet=sac('objet');
    $modification = false;
    $data=array();

    if (sac('id')) {
        $objet_data = charger_objet();
        $modification = true;
        $data = $objet_data->toArray();

    }
    else{
        if($id_motgroupe=$request->get('id_motgroupe'))
            $data['id_motgroupe'] = $id_motgroupe;
    }

    $builder = $app['form.factory']->createNamedBuilder('mot_form',FormType::class,$data);
    formSetAction($builder,$modification);
    $builder->setRequired(false);
    $form = $builder
        ->add('id_motgroupe',ChoiceType::class, array('label' =>  $app->trans('motgroupe'),
            'constraints' => new Assert\NotBlank(),
            'choices' => array_flip(table_simplifier(tab('motgroupe'))), 'attr' => array()))
        ->add('nom',TextType::class, array(
            'attr' => array('class' => 'span2', ),
            'extra_fields_message' => 'mot_sans_espace'
        ))
        ->add('nomcourt',TextType::class, array(
            'constraints' => new Assert\NotBlank(),
            'attr' => array('class' => 'span2','maxlength' => 6 ),
        ))
        ->add('descriptif',TextType::class, array(
            'label' => $app->trans('descriptif'),
            'attr' => array('required' => false,'class' => 'span2',
                'placeholder' => "Pour Aider à l'histoire du ".$app->trans($objet))
        ))
        ->add('texte',TextareaType::class, array(
            'label' => $app->trans('texte'),
            'attr' => array('required' => false,'class' => 'span2',
                'placeholder' => "Pour Aider à l'histoire du ".$app->trans($objet))))
        ->add('submit',SubmitType::class,
            array('label' => $app->trans('Enregistrer'), 'attr' => array('class' => 'btn-primary')))
        ->getForm();
    $form->handleRequest($request);

    if ($form->isSubmitted()) {

        if ($form->isValid()) {
            $data = $form->getData();

            if (!$modification) {
                $objet_data = new Mot();
            }
            $objet_data->fromArray($data);
            $objet_data->save();
            $args_rep['id']=$objet_data->getPrimaryKey();
            chargement_table();
     }
    }

    return reponse_formulaire($form, $args_rep);
}







function action_mot_form_associer(){

    global $app;
    $args_rep = [];
    $request=$app['request'];
    $objet=$request->get('objet');
    $id_objet=$request->get('id_objet');
    $tab_mots_select= MotLienQuery::create()->filterByObjet($objet)->filterByIdObjet($id_objet)->find()->toKeyValue('IdMot','IdMot');
    $tab_mots = tab('mot_arbre.'.$objet);
    $tab_mot_groupe_exclu = table_simplifier(table_filtrer_valeur(tab('motgroupe'),'systeme',true),'nom');
    $tab_mot_groupe_exclu += table_simplifier(table_filtrer_valeur(tab('motgroupe'),'actif',false),'nom');

    $tab_mots =array_diff_key($tab_mots,array_flip($tab_mot_groupe_exclu));

    $data = array('mots' => $tab_mots_select);
    $builder = $app['form.factory']->createNamedBuilder('objet_mot',FormType::class, $data);
    formSetAction($builder,true,array('action'=>'associer','objet'=>$objet,'id_objet'=>$id_objet));
    $builder->setRequired(false);
    $builder->add('mots',ChoiceType::class, array('choices' => $tab_mots, 'multiple' => true));
    $form = $builder
        ->add('Tagguer',SubmitType::class, array('label' => 'Tagguer'))
        ->getForm();

    $form->handleRequest($request);
    if ($form->isSubmitted()) {
        if ($form->isValid()) {

            $data = $form->getData();
            Mot::associerMotsObjet($data['mots'],$objet,$id_objet);

        } else {
            $form->addError(new FormError($app->trans('erreur_saisie_formulaire')));
        }
    }

    $args_rep['js_init'] = 'mot_objet';
    return reponse_formulaire($form,$args_rep);
}



function action_mot_form_fusionner(){

    global $app;
    $args_rep = [];
    $request=$app['request'];
    $objet_data = charger_objet();
    $data=['nouveau_nom' => $objet_data->getNom()];
    $builder = $app['form.factory']->createNamedBuilder('objet_mot',FormType::class,$data);
    formSetAction($builder,true,['action'=>'fusionner']);
    $builder->setRequired(false);

    $form = $builder

        ->add('id_mot_fusion',ChoiceType::class, [
            'constraints' => new Assert\NotBlank(),
            'choices' =>array_flip( [''=>'']+table_simplifier(tab('mot'))),
            'multiple'=>true,
            'attr' => ['class' => 'select2']])
        ->add('nouveau_nom',TextType::class)
        ->add('submit',SubmitType::class,
            array('label' => $app->trans('Fusionner'), 'attr' => array('class' => 'btn-primary')))
        ->getForm();
    $form->handleRequest($request);

    if ($form->isSubmitted()) {

        if ($form->isValid()) {
            $data = $form->getData();

            if (!empty($data['nouveau_nom'])){
                $objet_data->setNom($data['nouveau_nom']);
                $objet_data->save();
            }
            foreach($data['id_mot_fusion'] as $id_mot_disparu){
                MotLienQuery::fusion(sac('id'),$id_mot_disparu);
            }
            chargement_table();
        }
    }

    return reponse_formulaire($form,$args_rep);
}







function action_mot_form_supprimer(){
    $result = action_supprimer_une_instance();
    chargement_table();
    return $result;
}