<?php

use Symfony\Component\Form\FormError as FormError;
use Symfony\Component\Form\Extension\Core\Type\FormType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\Extension\Core\Type\IntegerType;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\Extension\Core\Type\MoneyType;
use Symfony\Component\Form\Extension\Core\Type\HiddenType;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\Validator\Constraints as Assert;
use Propel\Runtime\Map\TableMap;

function prestation_form()
{
    global $app;
    $args_rep = [];
    $request = $app['request'];
    $modification = false;
    $dupliquer=$request->get('dupliquer');
    if (sac('id')) {
        $objet_data = charger_objet();
        $modification = true;
        $data = $objet_data->toArray(TableMap::TYPE_FIELDNAME);
    } else {
        $data['id_entite']=pref('en_cours.id_entite');
        $data['retard_jours'] = 0;
        $data['active'] = 1;
        $data['prixlibre'] = 1;
        $data['nb_voix'] = 1;
        $data['periodique'] = '';
        $data['beneficiaire'] = 'membre';
    }
    if (substr_count($data['periodique'], ',') <= 4) $data['periodique'] = '0,M,12,1,1,attente';
    list($data['periodiquea'],
        $data['periodiqueb'],
        $data['duree'],
        $data['mois_debut'],
        $data['jour_debut'],
        $data['fin'],
        ) = explode(',', $data['periodique']);
    $choices_periodea = array_flip(getListePeriodiqueA());
    $choices_periodeb = array_flip(getListePeriodiqueB());
    $choices_compte = array_flip(table_simplifier(tab('compte')));
    $choices_beneficiaire = array('membre' => $app->trans('membre'),
        'individu' => $app->trans('individu'),
        'membre;individu' => $app->trans('membre;individu')        );

    $builder = $app['form.factory']->createNamedBuilder('prestation',FormType::class, $data);
    $builder->setRequired(false);
    if ($modification)
        $builder->setAction($app->path('prestation_form',array('id_prestation'=>sac('id'))));
    else
        $builder->setAction($app->path('prestation_form'));
    $builder
        ->add('nom',TextType::class, array('constraints' => new Assert\NotBlank(), 'attr' => array('class' => 'span2', 'placeholder' => 'Nom de la prestation'), 'extra_fields_message' => 'Nom de la prestation'))
        ->add('nomcourt',TextType::class, array('constraints' => new Assert\NotBlank(), 'attr' => array('class' => 'span2', 'placeholder' => 'Nom de la prestation'), 'extra_fields_message' => 'Ncourt'))
        ->add('nom_groupe',TextType::class, array('attr' => array('class' => 'span2', 'placeholder' => 'Nom du groupe Exemple Groupe Abonnement mensuel'), 'extra_fields_message' => 'Nom du groupe Exemple Groupe Abonnement mensuel'))
        ->add('descriptif',TextType::class, array('attr' => array('class' => 'span2', 'placeholder' => 'Pour Aider à l\'histoire de la prestation'), 'extra_fields_message' => 'nom'))
        ->add('prestation_type',ChoiceType::class, array('label' => 'Type de prestation', 'choices' => array_flip(getPrestationTypeActive()), 'expanded' => false, 'attr' => array('inline' => true, 'placeholder' => 'nom')))
        ->add('objet_beneficiaire',ChoiceType::class, array('label' => 'beneficiaire', 'choices' => array_flip($choices_beneficiaire), 'expanded' => false, 'attr' => array('inline' => false)))
//todo voir pour gerer en java script les champs liés a prestation_type ici tout est proposé
        ->add('nombre_numero',IntegerType::class, array('label' => $app->trans('nombre_numero'), 'attr' => array('placeholder' => '12')))
        ->add('prochain_numero',IntegerType::class, array('label' => $app->trans('prochain_numero'), 'attr' => array('placeholder' => '1')))
        ->add('periodiquea',ChoiceType::class, array('label' => $app->trans('periodicite'), 'choices' => $choices_periodea, 'expanded' => true, 'attr' => array('inline' => false)))
        ->add('periodiqueb',ChoiceType::class, array(
            'label' => $app->trans('duree_unite'),
            'choices' => $choices_periodeb,
            'expanded' => true,
            'label_attr' => array('class' => 'radio-inline')))
        ->add('duree',IntegerType::class, array('label' => $app->trans('duree'), 'attr' => array('placeholder' => '12')))
        ->add('mois_debut', IntegerType::class, array('label' => $app->trans('mois Début de Période'), 'attr' => array('placeholder' => '1')))
        ->add('jour_debut', IntegerType::class, array('label' => $app->trans('Jour Début de Période'), 'attr' => array('placeholder' => '1')))
        ->add('retard_jours',IntegerType::class, array('label' => 'Retard en jours', 'attr' => array('class' => 'span2', 'placeholder' => 'nom'), 'extra_fields_message' => 'foobar'))
        ->add('active',ChoiceType::class, array('label' => 'active', 'choices' => array( 'Oui'=>True,'Non'=>false), 'expanded' => true, 'label_attr' => array('class' => 'radio-inline'), 'extra_fields_message' => 'foobar'))
        ->add('prixlibre',ChoiceType::class, array('label' => 'Prix libre', 'choices' => array( 'Non'=>false,'Oui'=>true), 'expanded' => true, 'label_attr' => array('class' => 'radio-inline'), 'extra_fields_message' => 'foobar'))
        ->add('id_compte',ChoiceType::class, array('label' => $app->trans('compte'), 'choices' => $choices_compte, 'attr' => array()))
        ->add('id_tva',ChoiceType::class, array('label' => $app->trans('tva'), 'choices' => tvaNom(), 'attr' => array()))
        ->add('quantite',ChoiceType::class, array('label' => $app->trans('quantite'), 'choices' => array( 'Non montant = prix unitaire'=>false,'Oui avec le nombre de décimales défint dans unité'=>true), 'expanded' => true, 'label_attr' => array('class' => 'radio-inline'),'extra_fields_message' => 'foobar'))
        ->add('id_unite',ChoiceType::class, array('label' => $app->trans('unite'), 'choices' => uniteNom(), 'attr' => array()))
        ->add('nb_voix',IntegerType::class, array( 'attr' => array()))
        ->add('id_entite',HiddenType::class, array('label' => 'A rendre invisible', 'attr' => array('class' => 'span2'), 'extra_fields_message' => 'foobar')) ;
//todo ajouter filtre de entite en cours pour les comptes
    if(!$modification) {
        $builder->add('montant',MoneyType::class, array('constraints' => new Assert\NotBlank(), 'label' => 'Montant', 'attr' => array()));
    }
    $form =$builder
        ->add('submit',SubmitType::class, array('label' => $app->trans('Enregistrer'), 'attr' => array('class' => 'btn-primary')))
        ->getForm();



// todo le prix libre a non ne s'affiche pas le fonctionnement est correct voir pour une case a cocher

    $form->handleRequest($request);

    if ($form->isSubmitted()) {
        $data = $form->getData();

        if ($form->isValid()) {
            if (!$modification) {
                $objet_data = new Prestation();
            }
            if ($dupliquer=="dupliquer")
                $objet_data->copy(true);//todo true copie les dépendances donc prix et service rendu
            // id_prestation est dans data ?? voir pour  unset
            $data['periodique'] = $data['periodiquea'] . ',' .//glissant
                $data['periodiqueb'] . ',' .//unite de la durée
                $data['duree'] . ',' .
                $data['mois_debut'] . ',' .
                $data['jour_debut'] . ',' .
                $data['fin'];
            $objet_data->fromArray($data);
            $objet_data->save();
            $args_rep['id']=$objet_data->getIdPrestation();

            if (!$modification){
                $p = new Prix();
                $p->setMontant($data['montant']);
                $p->setObservation('En création de la prestation');
                $p->setIdPrestation($objet_data->getIdPrestation());
                $p->save();
            }
            chargement_table();

        }
    }
    return reponse_formulaire($form,$args_rep);
}


function action_prestation_form_supprimer(){
    return action_supprimer_une_instance();
}