<?php

use Symfony\Component\Form\FormError as FormError; use Symfony\Component\Form\Extension\Core\Type\FormType; use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Validator\Constraints as Assert;

function autorisation_liste()
{//todo reste retour de suppression et affichage de création a l'ouverture
    global $app;
    $tab_roles=$app['user']->getRoles();
    $args_twig=[
        'roles_profils' => rolesProfils($tab_roles),
        'roles_niveaux' => rolesNiveaux($tab_roles)];
    return $app['twig']->render(fichier_twig(), $args_twig);


}




function autorisation_colonnes(){

    $tab_colonne= array();
    $tab_colonne['id_autorisation'] = ['title'=> 'id'];
    $tab_colonne['individu.nom'] = ['title'=> 'individu'];
    $tab_colonne['profil'] = [];
    $tab_colonne['niveau'] = [];
    if (suc('entite_multi')) {
        $tab_colonne['id_entite'] = ["orderable" => false];
    }
    $tab_colonne['created_at'] = ['title' => 'cree_le',"type" => "date-eu"];

    $tab_colonne['action']=["orderable"=> false];
    $tab_colonne = datatable_complete_liste_colonne($tab_colonne);

    return $tab_colonne;

}