<?php
function servicerendu_liste()
{
    global $app;
    $args_twig=[
        'tab_col' => servicerendu_colonnes(),
        'tab_filtres' => servicerendu_filtres(),
    ];

    return $app['twig']->render(fichier_twig(), $args_twig);

}


function servicerendu_colonnes()
{

    $tab_colonne = array(
        'id_servicerendu' => array('title' => 'id'),
        'created_at' => array("type" => "date-eu"),
        'date_enregistrement' => array("type" => "date-eu")
    );
    if (suc('entite_multi')) {
        $tab_colonne['id_entite'] = ['title'=>'entite',"orderable" => false,"traitement" => 'transformeIdEntite'];
    }

    $tab_colonne['id_prestation'] = ['title'=>'prestation',"traitement" => 'transformeIdPrestation'];
    if (sac('objet') == 'servicerendu') {
        $tab_colonne['prestation.prestation_type'] = ['title'=>'type',"orderable" => false,"traitement" => 'transformePrestationType'];
    }

    $tab_colonne['beneficiaire'] = ['traitement'=>'genererLienBeneficiaire'];
    $tab_colonne['date_debut'] = ["type" => "date-eu"];
    $tab_colonne['date_fin'] = ["type" => "date-eu"];

    if (tab('prestation_type.' . sac('objet') . '.quantite')) {
        $tab_colonne['quantite'] = [];
        $tab_colonne['total'] = [];
    }
    $tab_colonne['montant'] = [];
    if (tab('prestation_type.' . sac('objet') . '.tva')) {
        $tab_colonne['tva'] = [];
    }
    $tab_colonne['paiement'] = [];
    $tab_colonne['action'] = ["orderable" => false];

    $tab_colonne = datatable_complete_liste_colonne($tab_colonne);

    return $tab_colonne;

}

function servicerendu_filtres(){

    global $app;
    $request = $app['request'];
    $rechercher = $request->get('search');
    $tab_filtres['premier']['search'] = filtre_ajouter_recherche($rechercher['value']);
    $id_prestation = $request->get('id_prestation') ? $request->get('id_prestation') : array();
    if (sac('objet')=='servicerendu')
        $tab_prestation=array_flip(table_simplifier(tab('prestation')));
    else
        $tab_prestation=array_flip(table_simplifier(getPrestationDeType(descr(sac('objet').'.objet_sans_sr'))));
    if (count($tab_prestation)>1){
        $filtre_prestation = filtre_prepare_donnee('id_prestation', $tab_prestation, $id_prestation);
        $tab_filtres['premier']['id_prestation'] = $filtre_prestation;
    }
    return $tab_filtres;
}


function action_servicerendu_liste_dataliste()
{

    $args=[];
    if (sac('objet') != 'servicerendu') {
        $args = array('prestation_type' => descr(sac('objet') . '.alias_valeur'));
    }
    list($tab_id,$nb_total) = objet_liste_dataliste_preselection('servicerendu',$args);
    $tab = objet_liste_dataliste_selection('servicerendu',$tab_id,tri_dataliste());
    $tab_data = objet_liste_dataliste_preparation('servicerendu',$tab,false);
    $tab_data = objet_liste_dataliste_complement('servicerendu', $tab,$tab_data);

    return objet_liste_dataliste_envoi($tab_data,$nb_total);

}


function servicerendu_dataliste_complement($tab, $tab_data)
{
    $tab_id_adhesion=[];
    $tab_prestation = table_simplifier(tab('prestation'),'prestation_type');
    foreach ($tab as $k => $sr) {
        if ($tab_prestation[$sr->getIdPrestation()]==6){
            $tab_id_adhesion[] = $sr->getPrimaryKey();
        }
    }
    if(!empty($tab_id_adhesion)){
        $tab_sr_amend = ServicerenduQuery::create()->filterByOrigine($tab_id_adhesion)->find();
        foreach ($tab_sr_amend as $sra) {
            $tab_sra[$sra->getOrigine()] = $sra;
        }

    }
    foreach ($tab as $k => $sr) {

        $membre = $sr->getMembre();
        if (!$membre) {
            $individus = $sr->getIndividus();
            foreach($individus as $individu) {
                $tab_data[$k]['beneficiaire'] = 'membre:'.$individu->getIdIndividu().':'.$individu->getNom();
            }
            if(empty($individus)){
                $tab_data[$k]['beneficiaire'] = 'Individu introuvable';
            }
        } else {
            $tab_data[$k]['beneficiaire'] = 'membre:'.$membre->getIdMembre().':'.$membre->getNom();
        }
        if ($tab_data[$k]['montant']) {
            $tab_data[$k]['montant'] = $sr->getMontant() * $sr->getQuantite();
        }

        if ($tab_prestation[$sr->getIdPrestation()]==6) {


            if (isset($tab_sra[$sr->getPrimaryKey()]))
            {
                $tab_data[$k]['date_fin'] = $tab_sra[$sr->getPrimaryKey()]->getDateFin()->format('d/m/Y');

            }elseif ($sr->getDateFin()->format('Y-m-d') == '3000-01-01') {
                $tab_data[$k]['date_fin'] = '---';
            }

        }


        $tab_data[$k]['paiement'] = $sr->getSommeServicepaiement();




        $tab_data[$k] = array_values($tab_data[$k]);
    }

    return $tab_data;
}





function servicerendu_export($type = null, $date_debut = null, $date_fin = null, $avec_paiement = true)
{

    $args = ['date_debut' => $date_debut, 'date_fin' => $date_fin];
    $tab_objets = ServicerenduQuery::getAllByTypePrestation($type, null, "", 0, 0, $args);
    $tab_colonnes = servicerendu_colonnes();

    $pos = 3;
    if (suc('entite_multi')) $pos++;;
    if (sac('objet') == 'servicerendu') $pos++;
    $tab_colonnes = table_inserer($tab_colonnes,$pos,['beneficiaire_id'=>'']);


    unset($tab_colonnes['action']);
    $tab_data = datatable_prepare_data($tab_objets, $tab_colonnes, false);

    $tab_entete = array_keys($tab_colonnes);


    $tab_prestation_type = table_simplifier(tab('prestation_type'));
    $tab_prestation = table_simplifier(tab('prestation'));
    $tab_tresor = table_simplifier(tab('tresor'));
    $nb_max_paiement = 0;
    foreach ($tab_objets as $k => $sr) {

        if (isset($tab_data[$k]['Prestations.prestation_type'])) {
            $tab_data[$k]['Prestations.prestation_type'] = $tab_prestation_type[$tab_data[$k]['Prestation.prestation_type']];
        }
        if ($tab_data[$k]['id_prestation']) {
            $tab_data[$k]['id_prestation'] = $tab_prestation[$tab_data[$k]['id_prestation']];
        }
        /*claude
                     $id_objet = $sr->getIdObjet();
                     // todo voir pour nom propel
                     if ($id_objet) {
                          if ($sr->getObjet=='individu') {
                             $tab_data[$k]['beneficiaire'] = IndividuQuery::create()->findPk($id_objet);
                         } else
                             $tab_data[$k]['beneficiaire']=$sr->getMembre()->getNom();
                     } else
                         $tab_data[$k]['beneficiaire']='Contact non trouvé '.$id_objet;

         claude*/
        $membre = $sr->getMembre();
        if (!$membre) {// todo service rendu a des individus avec cas particulier id_membre=id_individu
            $idindividu = $sr->getIdMembre();
            $individu = IndividuQuery::create()->findPk($idindividu);
            if ($individu) {
                $tab_data[$k]['beneficiaire_id'] = $individu->getPrimaryKey();
                $tab_data[$k]['beneficiaire'] = $individu->getNom();
            } else {
                $tab_data[$k]['beneficiaire'] = 'Contact non trouvé ' . $idindividu;
            }
        } else {
            $tab_data[$k]['beneficiaire_id'] = $sr->getMembre()->getPrimaryKey();
            $tab_data[$k]['beneficiaire'] = $sr->getMembre()->getNom();
        }
        if ($tab_data[$k]['montant']) {
            $tab_data[$k]['montant'] = $sr->getMontant() * $sr->getQuantite();
        }
        $tab_data[$k]['paiement'] = $sr->getSommeServicepaiement();
        //$sr=new Servicerendus();
        if ($avec_paiement) {
            $tab_paiement = $sr->getServicePaiementsJoinPaiement();
            foreach ($tab_paiement as $indice => $spaiement) {
                $paiement = $spaiement->getPaiement();
                if ($paiement) {
                    $nb_max_paiement = max($nb_max_paiement, count($paiement));
                    $tab_data[$k]['mode paiement ' . $indice] = $tab_tresor[$paiement->getIdTresor()];
                    $tab_data[$k]['montant ' . $indice] = $paiement->getMontant();
                    $date_enregistrement = $paiement->getDateEnregistrement();
                    $tab_data[$k]['date_enregistrement ' . $indice] = is_a($date_enregistrement,
                        'DateTime') ? $date_enregistrement->format('d/m/Y') : '';
                    $date_cheque = $paiement->getDateCheque();
                    $tab_data[$k]['date_cheque ' . $indice] = is_a($date_cheque,
                        'DateTime') ? $date_cheque->format('d/m/Y') : '';;

                }
            }
        }

        $tab_data[$k] = array_values($tab_data[$k]);
    }
    for($i=1;$i<=$nb_max_paiement;$i++){
        $tab_entete[]='mode paiement ' . $i;
        $tab_entete[]='montant paiement ' . $i;
        $tab_entete[]='date_enregistrement ' . $i;
        $tab_entete[]='date_cheque ' . $i;
    }


    return [$tab_entete]+$tab_data;


}

function action_servicerendu_liste_export_sr_objet($objet)
{
    global $app;
    include($app['basepath'] . '/src/inc/exports.php');
    $prefs = pref('sr_' . $objet . '.export');
    $tab = servicerendu_export('sr_' . $objet, $prefs['date_debut'], $prefs['date_fin'], $prefs['avec_paiement']);
    $date = new DateTime();
    export_tableur($objet . '_' . $date->format('Y-m-d'), $tab);
}


function action_servicerendu_liste_export_sr_cotisation()
{
    action_servicerendu_liste_export_sr_objet('cotisation');
}

function action_servicerendu_liste_export_sr_adhesion()
{
    action_servicerendu_liste_export_sr_objet('adhesion');
}

function action_servicerendu_liste_export_sr_abonnement()
{
    action_servicerendu_liste_export_sr_objet('abonnement');
}

function action_servicerendu_liste_export_sr_vente()
{
    action_servicerendu_liste_export_sr_objet('vente');
}

function action_servicerendu_liste_export_sr_perte()
{
    action_servicerendu_liste_export_sr_objet('perte');
}

function action_servicerendu_liste_export_sr_don()
{
    action_servicerendu_liste_export_sr_objet('don');
}

function action_servicerendu_liste_export_servicerendu()
{
    global $app;
    include($app['basepath'] . '/src/inc/exports.php');
    $prefs = pref('servicerendu.export');
    $tab = servicerendu_export('', $prefs['date_debut'], $prefs['date_fin'], $prefs['avec_paiement']);
    $date = new DateTime();
    export_tableur('servicerendu_' . $date->format('Y-m-d'), $tab);
}