<?php
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\Extension\Core\Type\FileType;
use Symfony\Component\Form\Extension\Core\Type\FormType;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\Form\FormError as FormError;
use Symfony\Component\Validator\Constraints as Assert;

function import_formation()
{
    return;

}

function import_form()
{
    global $app;
    $args_twig=[];
    $request = $app['request'];
    $data = array();
    $modification = false;
    $id = sac('id');
    $data['operation'] = $request->get('maitre');
    $etape = array(
        '0abandon' => $app->trans('abandonne'),
        '1chargeentete' => '1chargeentete',// $app->trans('chargement'),
        '2chargeligne' => '2chargeligne',// $app->trans('chargement'),
        '3proposition' => '3controle',//=> $app->trans('propositions'),
//        '4m_m_i' => $app->trans('modification_membre_individus'),
//        '4m_option_1' => $app->trans('modification_cotisations'),
//        '4m_option_2' => $app->trans('modification_adhesions'),
//        '4m_mot1' => $app->trans('modification_motgroupe1'),
//        '4m_mot2' => $app->trans('modification_motgroupe2'),
//        '4m_mot3' => $app->trans('modification_motgroupe3'),
//        '5a_m_i' => $app->trans('ajout_membre_individus'),
//        '5a_option_1' => $app->trans('ajout_cotisations'),
//        '5a_option_2' => $app->trans('ajout_adhesions'),
        '6valide' => '6valide',//=> $app->trans('valide'),
        '7compte_rendu' => '7compte_rendu'//$app->trans('compte_rendu')
    );
    if ($id) {
        $objet_data = charger_objet();
        $modification = true;
        $data['import_mode'] = 'etape';
        $data = $objet_data->toArray();
        if ($data['avancement'] == '6controle') {
            unset($etape['1chargeentete']);
            unset($etape['2chargeligne']);
            unset($etape['3proposition']);
        } elseif ($data['avancement'] == '3proposition') {
            unset($etape['1chargeentete']);
            unset($etape['2chargeligne']);
        } elseif ($data['avancement'] == '2chargeligne') {
            unset($etape['1chargeentete']);
        }
    } else {
        $data['import_mode'] = 'auto';
        $data['import_etapes'] = array(
            '1chargeentete',
            '2chargeligne',
            '3controle',
            '3proposition',
            '6valide',
            '7compte_rendu'
        );
    }
    $builder = $app['form.factory']->createNamedBuilder('import', FormType::class, $data);
    formSetAction($builder);
    $builder->setRequired(false);
    $bt = 'Lancer (1 à 2 secondes par lignes)';
    if (!$modification) {
         //todo voir gestion en js auto effacer import etapes
        $builder
            ->add('fichier', FileType::class,
                array(
                    'data' => '',
                    'label' => $app->trans('import_fichier'),
                    'constraints' => new Assert\NotBlank()
                ))
            ->add('import_mode', ChoiceType::class, array(
                'label' => $app->trans('import_mode'),
                'expanded' => true,
                'label_attr' => array('class' => 'radio-inline'),
                'choices' => array($app->trans('automatique') => 'auto', $app->trans('etape') => 'etape'),
                'extra_fields_message' => 'foobar',
                'attr' => array('inline' => true)
            ))
            ->add('import_efface', ChoiceType::class, array(
                    'label' => $app->trans('import_efface'),
                    'expanded' => true,
                    'label_attr' => array('class' => 'radio-inline'),
                    'choices' => array('oui' =>1,  'non' => 0),
                    'extra_fields_message' => 'foobar',
                    'attr' => array('inline' => true)

                ));
    }
//    if(!$data['operation']){
$import_choix=array(
    $app->trans('individu') => 'individu',
    $app->trans('membre') => 'membre',
    $app->trans('piece') => 'piece',
    $app->trans('compte') => 'compte');
//   arbre(lire_config('pref.en_cours.id_entite'));
//    echo 'ligne 110';
//
//    if (autoriser('A5',1))
//        echo "<br>A5 1";
//    if (autoriser('A5',3))
//        echo "<br>A5 3";
//    if (autoriser('A',1))
//        echo "<br>A 1";
//    if (autoriser('A',3))
//        echo "<br>A 3";
//    if (autoriser('A'))
//        echo "<br>A";
//    if (autoriser('A5'))
//        echo "<br>A5 ";
//    if (autoriser('A'))
//        echo "<br>A ";
//    if (autoriser('A5'))
//        echo "<br> blanc ";
    if (autoriser('A6',lire_preference('en_cours.id_entite')))
        $import_choix[$app->trans('reprise')]='reprise';
    $builder
        ->add('operation', ChoiceType::class, array(
            'label' => $app->trans('import_objet'),
            'expanded' => true,
            'label_attr' => array('class' => 'radio-inline'),
            'choices' => $import_choix,
            'extra_fields_message' => 'foobar',
            'attr' => array('inline' => true)
        ));

//    }
    $form = $builder
        ->add('import_etapes', ChoiceType::class, array(
            'label' => $app->trans('import_etapes'),
            'multiple' => true,
            'expanded' => true,
            'choices' => $etape,
            'extra_fields_message' => 'foobar',
            'attr' => array('inline' => true)

        ))
        ->add('submit', SubmitType::class,
            array('label' => $app->trans($bt), 'attr' => array('class' => 'btn-primary')))
        ->getForm();

    $form->handleRequest($request);

    if ($form->isSubmitted()) {
        $data = $form->getData();

        if ($form->isValid()) {
            if (!$modification) {
                $file = $data['fichier'];
                $nom = $file->getClientOriginalName();
                $mime = $file->getmimeType();
                $size = $file->getsize();
                $error = $file->geterror();
                $pathname = $file->getpathName();
                $nomprovisoire = $file->getfileName();
                $tempextension = $file->guessExtension();
                $controlemd5 = $file->guessExtension();
                if (!$tempextension) {
                    $Extension = 'bin';
                }//                     Extension ne peut pas être deviné
//            arbre('<br>controlemd5 ??? :'.$controlemd5 .'<br> $nomprovisoire :'.$nomprovisoire.'<br> extension :'.$tempextension);
//            ae( '<br> mime :'.$mime.'<br> taille :'.$size.'<br>pathname : '.$pathname );
                $file->move($app['upload.path'], $nom);
                //todo ajouter MD5
                $objet_data = new Import();
                $objet_data->setNom($nom);
                $objet_data->setNomprovisoire($nomprovisoire);
                $objet_data->setTaille($size);
                $objet_data->setControlemd5("xx");
                $objet_data->setOperation($data['operation']);
                $objet_data->setAvancement('0debut');// todo limite de 2Mo pour CSV le 5/1/2017 Adav
                if ($data['import_mode'] == 'auto') {
                    $data['import_etapes'] = array(
                        '1chargeentete',
                        '2chargeligne',
                        '3controle',
                        '6valide',
                        '7compte_rendu'
                    );
                }
                $temp = json_encode(array(
                    'debut' => '0debut',
                    'nb' => array('valide' => 0),
                    'message' => 'début',
                    'efface'=>$data['import_efface'],
                    'url' => '<a href=" ">Pas de compte rendu a telecharger</a>'
                ));
                $objet_data->setInfo($temp);
                $objet_data->setNbLigne(0);
            }
            $temp = $objet_data->getModification();
            unset($temp['etapes']);
            if ($data['import_etapes']) {
                foreach ($data['import_etapes'] as $k => $v) {
                    $temp['etapes'][$v] = $v;
                }
                $objet_data->setModifications(json_encode($temp));

            }
            $objet_data->save();
            $id = $objet_data->getIdImport();
            $args_rep['id'] = $id;
                        // Met au meme niveau les autoincrement
            autoincrement_idem('membre', 'asso', 'individu', 'asso');
            $url_redirect = $app->path('import_form',
                ['action' => 'attente', 'id_import' => $id]);
        }
    }
    return reponse_formulaire($form, $args_rep);
}

function action_import_form_attente()
{

    global $app;
    $request = $app['request'];
    $id = $request->get('id_import');
    $url = $app->path('import_form', ['action' => 'moteur', 'id_import' => $id]);
    $url_redirect = $app->path('import_form', ['action' => 'resultat', 'id_import' => $id]);
    $args_twig = array(
        'titre' => 'Importation',
        'url' => $url,
        'url_redirect' => $url_redirect,
        'progression_pourcent' => 0,
        'message' => "n°" . $id
    );



// todo permet l'entrée avec les message d'erreurs mais sans l'affichage de la progression;
 //todo Voir le return ligne 660
//     for ($i = 1; $i <= 100; $i++) {
//        action_import_form_moteur();
//      echo $i.'*';
//    }
//    echo 'Fin de l import Ligne 217';
//    exit;
    return $app['twig']->render('attente.html.twig', $args_twig);
}

function action_import_form_moteur()
{
    global $app;
    $request = $app['request'];
    $id = $request->get('id_import');

    $message_attente = '';
    $fin='';
    $pourcentage = 100;
    if ($id) {
        $objet_data = ImportQuery::create()->findOneByIdImport($id);
        $fichier = $objet_data->getNom();
        $extension = substr($fichier, -4);// premiere lecture
        $operation = $objet_data->getOperation();
        $modif = $objet_data->getModification();
        $message_attente .= ' id ' . $id . ' ';
        if (isset($modif['etapes'])) {
            asort($modif['etapes']);
            $proposition = $objet_data->getProposition();
            $etape = array_shift($modif['etapes']);
            $originals = null;
            $avance = $etape;
            $action = $etape;
            $efface = $etape;
            $message_attente .= ' operation : ' . $operation . ' - ' . $etape . ' : ';

            switch ($etape) {
                case '0abandon':
                    if ($objet_data->getNbLigne()) {
                        $message_attente .= 'Abandon impossible ' . $objet_data->getNbLigne() . ' lignes ont été validées  Import :' . $id;
                    }
                    break;
//                case '1copie':
//                    if ($objet_data->getAvancement() == '0debut') {
//                        $message = ' debut a : ' . date('Y-m-d H:i:s');
//                        $pourcentage = 6;
//                        $proposition['message'] = $message;
////                        $originals = "on ne l'enregistre on lit le fichier plus passer en binaire"; //transfert_fichier($fichier);
//                    }
//                    break;
                case '1chargeentete':
                    if ($objet_data->getAvancement() == '0debut') {

//                        if($proposition['efface'] and $operation='reprise' ) {
//                            $app['db']->execute_query("DELETE FROM asso_logs");////TRUNCATE asso_imports ;
//                        }
                        $message = ' debut a : ' . date('Y-m-d H:i:s');
                        import_lecture_entete($fichier, $proposition, $operation, $id);
                        $pourcentage = 10;
                        $message_attente .= ' lignes lues : ' . $proposition['nb']['lignes_lues'];
                    }
                    break;

                case '2chargeligne' :
                    $pas = 100;
                    $debut = ($proposition['nb']['lignes_lues'] + 1);
                    $fin = ($proposition['nb']['lignes_lues'] + $pas);
                    switch ($extension) {
                        case  '.csv' :
                            lecture_fichier_csv_donnees($fichier,
                                $extension,
                                $id,
                                $debut,
                                $fin,
                                $proposition['colonnes'],
                                $operation,
                                $proposition['nb']['curseur'],
                                $proposition['nb']['colonnes']);
                            $pourcentage = floor(20 + (20 / ($objet_data->getTaille()) * $proposition['nb']['curseur']));
                             break;
                        default :
                            $proposition['nb']['lignes_lues'] = import_lignes($fichier,
                                $extension,
                                $id,
                                $debut,
                                $fin,
                                $proposition['colonnes'],
                                $operation,
                                $proposition['nb']['curseur'],
                                $proposition['nb']['colonnes']
                            );
                            $pourcentage = floor(20 + (20 / ($objet_data->getTaille()) * $proposition['nb']['curseur']));
                    }

                    $proposition['nb']['lignes_lues'] = $fin;
                    if ($proposition['nb']['lignes_lues'] == ($debut + $pas-1)) {
                        $efface = "";
                    } else {
                        $proposition['nb']['lignes_lues']--;
                    }
                    break;
                case '3controle':
                case '3proposition':
                    if ($objet_data->getAvancement() < '2chargeligne') {
                        $message_attente .= 'l\'étape de chargement n\'est pas réalisée pour import :' . $id;
                        $avance = '';
                    } else {
                    $pas = round(max(5, min(50, ($proposition['nb']['lignes_lues'] / 10))));//entre 5 et 10
                    $debut = $proposition['nb']['lignes_controlees'] + 1;
                    $fin = min($debut + $pas, $proposition['nb']['lignes_lues']);
                    for ($i = $debut; $i <= $fin; $i++) {
                        $ligne_data = ImportligneQuery::create()->filterByIdImport($id)->filterByLigne($i)->findone();
                        if (!$ligne_data) {
                            echo 'erreur de lecture import ligne ';
                            exit;
                        }
                        $tab_l = importligne_liredata(
                            $ligne_data->getVariables(),
                            $proposition['colonnes'],
                            $proposition['nb']['colonnes']
                        );
                        switch ($operation) {
                            case 'individu':
                                import_controle_ligne_individu($i,
                                    $tab_l,
                                    $proposition['entete'],
                                    $proposition['nb'],
                                    $proposition['message'],
                                    $proposition['m_nom'],
                                    $proposition['m_individu'],
                                    $proposition['a_individu']);
                                break;
                            case 'membre':
                                import_controle_ligne_membre($i,
                                    $tab_l,
                                    $proposition['entete'],
                                    $proposition['nb'],
                                    $proposition['message'],
                                    $proposition['m_nom'],
                                    $proposition['m_individu'],
                                    $proposition['a_individu'],
                                    $proposition['a_membre'],
                                    $proposition['m_cot'],
                                    $proposition['m_cotp']);
                                break;
                            case 'reprise':
                                if (trim($tab_l['nom_famille'] . $tab_l['prenom']) == '') {
                                    if (trim($tab_l['organismefonction']) >= '!') {
                                        $tab_l['prenom'] = 'Organisme';
                                    }
                                    $tab_l['nom_famille'] = substr(trim($tab_l['organismefonction']), 0, 50);
                                }
                                import_controle_reprise($i,
                                    $tab_l,
                                    $proposition['entete'],
                                    $proposition['nb'],
                                    $proposition['message']);
                                break;
                        }
                        $ligne_data->setVariables(json_encode($tab_l));
                        $ligne_data->setObservation($etape);
                        $ligne_data->save();
//                        }
                        $message_attente = $action . " debut : " . $debut . " fin : " . $fin . " nombre lues : " . $proposition['nb']['lignes_lues'];
                        $proposition['nb']['lignes_controlees'] = $fin;
                        // todo $individus_plusieurs et $membres_plusieurs en commentaire dans controle ligne
                        $pourcentage = floor(40 + (20 / $proposition['nb']['lignes_lues'] * $fin));
                        if ($proposition['nb']['lignes_lues'] <= $fin) {
                            if ($etape == '3proposition') {
                                $avance = '4modification';
                                $pourcentage = 100;
                                $message_attente .= " etape suivante :  Validation des modifications et créations à réaliser<br/>";
                            }
                        } else {
                            $efface = '';
                        }
                    }
                    }
                    break;
                case '6valide':
                    $pas = round(min(20, ($proposition['nb']['lignes_lues'])));
                    $debut = $proposition['nb']['lignes_enregistrees'] + 1;
                    $fin = min($debut + $pas, $proposition['nb']['lignes_lues']);

                    for ($i = $debut; $i <= $fin; $i++) {
                        $ligne_data = ImportligneQuery::create()->filterByIdImport($id)->filterByLigne($i)->findone();
                        $tab_l = json_decode($ligne_data->getVariables(),true);

                        switch ($operation) {
                            case 'individu':
                                if ($tab_l['OK_ind'] == "bon") {
                                    individu_creation($tab_l);
                                    $proposition['nb']['ind']++;
                                }
                                if ($tab_l['id_mot1']) {
                                    $tab_l['compte_rendu_individu'].= MotLienQuery::mot_liens($tab_l['id_mot1'], $tab_l['id_individu'],
                                        'individu');
                                }
                                if ($tab_l['id_mot2']) {
                                    $tab_l['compte_rendu_individu'].=MotLienQuery::mot_liens($tab_l['id_mot2'], $tab_l['id_individu'],
                                        'individu');
                                }
                                if ($tab_l['id_mot3']) {
                                    $tab_l['compte_rendu_individu'].=MotLienQuery::mot_liens($tab_l['id_mot3'], $tab_l['id_individu'],
                                        'individu');
                                }
                                break;
                            case 'membre':
                                if ($tab_l['OK_ind'] == "bon") {
                                    individu_creation($tab_l);
                                    $proposition['nb']['ind']++;
                                }
                                if ($tab_l['OK_mem'] == "bon") {
                                    membre_creation($tab_l);
                                    $proposition['nb']['mem']++;
                                }
                                if ($tab_l['OK_cot'] == "bon") {
                                    servicerendu($tab_l, 'cot', 1);
                                    $proposition['nb']['cot']++;
                                }
                                if ($tab_l['OK_cotp'] == "bon") {
                                    paiement($tab_l, 'cot');
                                    $proposition['nb']['cotp']++;
                                }
                                if ($tab_l['cotid_mot']) {
                                    $tab_l['compte_rendu_membre'].=MotLienQuery::mot_liens($tab_l['cotid_mot'], $tab_l['id_membre'],
                                        'membre');
                                }
                                break;
                            case 'reprise':
                                if ($tab_l['OK_ind'] == "bon") {
                                    $tab_l['id_individu']= individu_creation_import($tab_l, false);
                                    $proposition['nb']['ind']++;
                                }
                                if ($tab_l['OK_mem'] == "bon") {

                                    $tab_l['id_membre'] = $tab_l['id_individu'];
                                    mots_creation_reprise('observation_m2',$proposition['entete'], 11,'cree_et_enleve','membre',$tab_l);

                                    $tab_l['id_membre'] = membre_creation_import($tab_l,false);
                                    $proposition['nb']['mem']++;
                                }
                                if  (substr($tab_l['date2000'],6,4)== '1900')//1900 au lieu de 2000 dans cette colonne
                                    $tab_l['date2000'] = substr($tab_l['date2000'], 0, 6) . '2000';

                                $options=array('prestation_type'=>1,'id_prestation'=>1,'paiement'=>true);
                                sr_cotisation(1992,2017,$tab_l,'cot',$options,$proposition['nb']['cot'],$proposition['nb']['cotp']);
                                $options=array('prestation_type'=>4,'id_prestation'=>4,'paiement'=>true);
                                sr_boucle(1997,2017,$tab_l,"don",$options,$proposition['nb']['cot'],$proposition['nb']['cotp']);
                                $options=array('prestation_type'=>2,'id_prestation'=>2,'paiement'=>true);
                                sr_boucle(1997,2017,$tab_l,"abo",$options,$proposition['nb']['abo'],$proposition['nb']['abop']);
                                $date_debut = test_date_valeur($tab_l['datebenevol']);
                                if ($date_debut) {
                                    $options=array('prestation_type'=>3,'id_prestation'=>3,'paiement'=>false);
                                    $options['date_debut'] = $date_debut;
                                    $options['prefixe'] = 'ven';
                                    if (trim($tab_l['benevol']) > '!') {
                                        $options['observation'] = 'benevole : ' . trim($tab_l['benevol']);
                                    }
                                    sr_unique($tab_l,$options,$proposition['nb']['ven'],$proposition['nb']['venp']);
                                }
                                mots_creation_reprise('pvu',$proposition['entete'], 4,'','membre',$tab_l);
                                mots_creation_reprise('benevol',$proposition['entete'], 2,'decompose_9','membre',$tab_l);
                                mots_creation_reprise('decouverteadav',$proposition['entete'], 5,'','membre',$tab_l);
                                mots_creation_reprise('orgtype',$proposition['entete'], 6,'lg_5_6','membre',$tab_l);
                                mots_creation_reprise('roledansadav',$proposition['decompose_virgule'], 3,'','membre',$tab_l);
                                if ($tab_l['signatureappel']=='s')   MotLienQuery::mot_liens(126,$tab_l['id_membre'],'membre');
                                if ($tab_l['signatureappel']=='1')   MotLienQuery::mot_liens(127,$tab_l['id_membre'],'membre');
                                if ($tab_l['signatureappel']=='1s')   MotLienQuery::mot_liens(128,$tab_l['id_membre'],'membre');
                                $pos= strpos($tab_l['quartier'],'/ MEL');
                                if ($pos){
                                    $tab_l['quartier']=trim(substr($tab_l['quartier'],0,$pos));
                                    MotLienQuery::mot_liens(129,$tab_l['id_membre'],'membre');
                                }
                                mots_creation_reprise('quartier',$proposition['entete'], 8,'','membre',$tab_l);
                                if ($tab_l['invitationauca']=='oui')   MotLienQuery::mot_liens(46,$tab_l['id_membre'],'membre');
                                if ($tab_l['residence_appart']=='Oui')   MotLienQuery::mot_liens(131,$tab_l['id_membre'],'membre');
                                if ($tab_l['pvl']=='Oui')   MotLienQuery::mot_liens(45,$tab_l['id_membre'],'membre');

                                mots_creation_reprise('mot1',$proposition['entete'], 7,'decompose_9_10','membre',$tab_l);
                                mots_creation_reprise('mot3',$proposition['entete'], 7,'','membre',$tab_l);
                                mots_creation_reprise('statut',$proposition['entete'], 9,'decompose_virgule','membre',$tab_l);
                                mots_creation_reprise('nomhote',$proposition['entete'], 12,'','membre',$tab_l);
                                mots_creation_reprise('mot2',$proposition['entete'], 13,'','membre',$tab_l);
                                if (isset($data['datemail'])and trim($data['datemail'])>'!') {
                                    MotLienQuery::mot_liens(493,$tab_l['id_membre'],'membre',trim($data['datemail']));
                                }


                                break;

                        }
                        $ligne_data->setVariables(json_encode($tab_l));
                        $ligne_data->setObservation($etape);
                        $ligne_data->save();
                    }

                    $pourcentage = (floor(60 + (35 / ($proposition['nb']['lignes_lues']) * $fin)));
                    $proposition['nb']['lignes_enregistrees'] = $fin;
                    if (($proposition['nb']['lignes_lues']) <= $fin) {
                            $message_attente .= compte_rendu($proposition, $id, $fichier, $etape, "6valide");
                    } else {
                        $efface = '';
                        $message_attente .= $action . " debut : " . $debut . " fin : " . $fin;
                    }
                    break;
                case '7compte_rendu':
                    export_csv($proposition,$id);
                    $proposition['nb']['maitre'] = $proposition['nb']['ind'];
                    $pourcentage = 100;
                    break;
                default :// todo a faire 4modification 5ajout
//                // en attente phase 2 les différentes validations des lignes sélectionnées
                    if ($objet_data->getAvancement() == '4modification') {
                        $proposition = $objet_data->getProposition();
                    }
                    $message_attente .= $etape . ' n\'est pas connu <br/>';
                    break;
            }
            import_modifie($id, $avance, $originals, $proposition, $action.' : '.$fin, $efface);
        } else {
            $message_attente .= 'Pas d\'etape<br/>';
        }
    } else {
        $message_attente .= 'Id non renseignée<br/>';

    }
//    return $message_attente;
    return json_encode([
        'message' => 'Etape en cours : ' . $message_attente,
        'progression_pourcent' => $pourcentage
    ]);

}

function action_import_form_resultat()
{
    global $app;
    $request = $app['request'];
    $id = $request->get('id_import');
    $j = 0;
    $ok = false;
    $args_twig = array();
    if ($id) {
        $objet_data = ImportQuery::create()->findPk($id);
        $fichier = $objet_data->getNom();
        $proposition = $objet_data->getProposition();
        $etape = $objet_data->getAvancement();
        if (isset($proposition['nom_output'])) {
            $outputFileName = $app['output.path'] . '/' . $proposition['nom_output'];
            if (!file_exists($outputFileName)) {
                $ok = true;
            }
        } elseif ($etape > '6') {
            $outputFileName = $app['output.path'] . '/' . $id . '*.*';
            if (!file_exists($outputFileName)) {
                $ok = true;
            }
        }
        if (!$ok) {// si necessite un retour dans le fichier d'origine
            include_once($app['basepath'] . '/src/inc/imports.php');
            compte_rendu($proposition, $id,$fichier, $etape, "6valide");
//  todo doit il y avoir un export
            export_csv($proposition,$id);
            $url_redirect = $app->path('import_form', ['action' => 'resultat', 'id_import' => $id]);
        }
        $args_twig = array('message' => $proposition['message']);
    }
    return $app['twig']->render('import_resultat.html.twig', $args_twig);
}


function action_import_form_supprimer(){
    return action_supprimer_une_instance();
}