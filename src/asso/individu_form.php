<?php

use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\Extension\Core\Type\DateType;
use Symfony\Component\Form\Extension\Core\Type\FormType;
use Symfony\Component\Form\Extension\Core\Type\PasswordType;
use Symfony\Component\Form\Extension\Core\Type\RepeatedType;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\Validator\Constraints as Assert;
use Symfony\Component\Form\FormError as FormError;
use Symfony\Component\Form\FormEvent as FormEvent;
use Symfony\Component\Form\FormEvents as FormEvents;
use \Propel\Runtime\ActiveQuery\Criteria;


include_once($app['basepath'] . '/src/inc/fonctions_ged.php');
include_once($app['basepath'] . '/src/inc/fonctions_geo.php');
include_once($app['basepath'] . '/src/inc/fonctions_a_classer.php');


function individu_form(){

    global $app;
    $args_rep = [];
    $request = $app['request'];

    if($liaison_membre=$request->get('id_membre')){
        include_once($app['basepath'] . '/src/asso/membre_form.php');
        return membre_liaison_individu_form();
    }
    $modification = false ;
    $id_individu = $request->get('id_individu');
    if ($id_individu) {
        $individu = IndividuQuery::create()->findPk($id_individu);
        if (isset($individu)) {
            $data['individu'] = $individu->toArray();
            $data['individu']['naissance'] = $individu->getNaissance();
            $modification=true;
        }
    } else {
        $data['individu']['pays'] = pref('individu.pays');
    }

    $builder = $app['form.factory']->createNamedBuilder('form_membre', FormType::class, $data);
    $builder
        ->add('individu', IndividuForm::class)
        ->add('submit', SubmitType::class, ['label' => $app->trans('Enregistrer'), 'attr' => ['class' => 'btn-primary']]);
    $builder->setRequired(false);
    formSetAction($builder, $modification,['id_individu'=>$id_individu]);
    $form = $builder->getForm();
    $form->handleRequest($request);
    if ($form->isSubmitted()) {
        $data = $form->getData();
    if (!$modification) {
        $form = verification_individu_form($form);
        }
    if ($form->isValid()) {
        $data_individu = $data['individu'];
        if (!$modification ) {
            $individu = new Individu();
        }
        $individu->fromArray($data_individu);
        $individu->save();
        individu_traitement_form($individu, $data_individu, false);
        Log::EnrOp($modification, 'individu', $individu, $individu->getIdIndividu());
        }
    }
    $args_rep['modification'] = $modification;
    $args_rep['js_init'] = 'composant_form_membre';
    $args_rep['form_schema'] = 'individu';
    return reponse_formulaire($form, $args_rep);

}

function individu_traitement_form($individu, $data_individu, $modification){

    $ok = traitement_form_geo($individu, $data_individu, $modification, 'individu');
    $ok = $ok && traitement_form_ged('form_membre_individu_image',$individu->getPrimaryKey(), 'individu','logo');
    return $ok;

}



function verification_individu_form($form, $id_individu=0){

    global $app;
    $data =$form->getData();

            // Vérification doublon de login
            if ($data['individu']['login'] != '') {
                if (IndividuQuery::create()->filterByLogin($data['individu']['login'])->count() > 0) {
                    $form->get('individu')->get('login')->addError(new FormError($app->trans('erreur_saisie_login_existant')));
                }
            }

            if ($data['individu']['email'] != '') {
                $query = IndividuQuery::create()->filterByEmail($data['individu']['email']);
                if ($id_individu > 0)
                    $query = $query->filterByIdIndividu($id_individu,Criteria::NOT_EQUAL);
                if ($query->count() > 0) {
                    if ($form->get('individu'))
                        $form->get('individu')->get('email')->addError(new FormError($app->trans('erreur_saisie_email_existant')));
                }
            }
            if ( $id_individu ) {

                if ($data['individu']['nom_famille'] == '' and $data['individu']['email'] == '') {
                    $form->get('individu')->get('nom_famille')->addError(new FormError($app->trans('erreur_saisie_nom_email')));
                    $form->get('individu')->get('email')->addError(new FormError($app->trans('erreur_saisie_nom_email')));
                }
                if ($data['individu']['prenom'] == '') {
                    $form->get('individu')->get('prenom')->addError(new FormError($app->trans('erreur_saisie_prenom')));
                }

            }
    return $form;

}


function action_individu_form_retour_courrier() {

    global $app;
    $args_rep = [];
    $request = $app['request'];
    $id = sac('id');
    $data = array(
        'date' => new Datetime(),
        'type' => $request->get('type')
    );

    $retour_type = mots('npai');

    $builder = $app['form.factory']->createNamedBuilder('retour_courrier', FormType::class, $data);
    $builder->setRequired(false);
    formSetAction($builder, true, array('action' => 'retour_courrier', 'id_individu' => $id));
    $builder = $builder
        ->add('date', DateType::class, array(
            'label' => $app->trans('retour_date'),
            'widget' => 'single_text',
            'format' => 'dd/MM/yyyy',
            'attr' => array('class' => 'datepickerb')
        ))
        ->add('type', ChoiceType::class, array(
            'label' => $app->trans('retour_type'),
            'expanded' => true,
            'label_attr' => array('class' => 'radio-inline'),
            'choices' => $retour_type,
            'attr' => array('inline' => true)

        ));

    $form = $builder->add('submit', SubmitType::class,
        array('label' => $app->trans('Enregistrer'), 'attr' => array('class' => 'btn-primary')))
        ->getForm();

    $form->handleRequest($request);
    if ($form->isSubmitted()) {
        $data = $form->getData();
        if ($form->isValid()) {
            $id_mot = $data['type'];
            $ajout=mot::MotLien($id_mot, $id, 'individu', 'le ' . $data['date']->format('d/m/Y'));
            if ($ajout){
                $args_rep['id']= $id;
            }
        }
    }
    return reponse_formulaire($form, $args_rep, 'inclure/form.html.twig');
}






function action_individu_form_carte_adherent() {

    global $app;
    include_once($app['basepath'] . '/src/inc/fonctions_carte.php');
    $request = $app['request'];
    $id_individu = $request->get('id_individu');
    return carte_changer_etat('individu',$id_individu);

}

function action_individu_form_changer_mdp() {
    global $app;
    $args_rep=[];
    $request = $app['request'];
    $id = $request->get('id_individu');

    if (!empty($id)) {
        $individu = IndividuQuery::create()->findPk($id);
        $builder = $app['form.factory']->createNamedBuilder('individu_form_mdp', MotDePasseForm::class);
        $builder->setRequired(false);
        formSetAction($builder, true, array('action' => 'changer_mdp', 'id_individu' => $id));

            $form = $builder->add('submit', SubmitType::class,
                array('label' => $app->trans('Changer le mot de passe'), 'attr' => array('class' => 'btn-primary')))
            ->getForm();

        $form->handleRequest($request);


        if ($form->isSubmitted()) {
            $data = $form->getData();

            if ($form->isValid()) {
                if (!empty($data['password'])) {
                    $individu->setPass($data['password']);
                }
                $individu->save();

            }
        }

        return reponse_formulaire($form, $args_rep, 'inclure/form.html.twig');

    }

    return 'PB';
}



function action_individu_form_est_decede(){

    global $app;
    $url_redirect = $app['request']->get('redirect');
    $individu = charger_objet();
    $id_mot_decede = table_filtrer_valeur_premiere(tab('mot'),'nomcourt','ind_dcd')['id_mot'];
    if ($individu->estDecede()){
        MotLienQuery::supprimer_mot_liens($id_mot_decede,sac('id'),'individu');
    } else {
        MotLienQuery::mot_liens($id_mot_decede,sac('id'),'individu');
    }
    return $app->redirect($url_redirect);

}

function action_individu_form_generer_login() {
    global $app;
    $request = $app['request'];
    $nom = $request->get('nom');
    $prenom = $request->get('prenom');
    return IndividuQuery::donnerLogin($nom, $prenom);
}



function action_individu_form_verifier_login() {
    global $app;
    $request = $app['request'];
    $login = $request->get('login');
    $id_individu = $request->get('id_individu');
    return IndividuQuery::loginExiste($login, $id_individu);

}


function action_individu_form_supprimer() {
    return action_supprimer_une_instance();
}