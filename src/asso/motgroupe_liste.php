<?php


function motgroupe_liste()
{
    global $app;
    $args_twig=[
        'tab_col'=>motgroupe_colonnes()
        ];
    return $app['twig']->render(fichier_twig(), $args_twig);

}


function action_motgroupe_arbre()
{
    global $app;
    $args_twig=[];
    return $app['twig']->render('asso/motgroupe_arbre.html.twig', $args_twig);

}



function motgroupe_colonnes(){

    $tab_colonne= array();
	$tab_colonne['id_motgroupe'] = ['title'=> 'id'];
	$tab_colonne['nom'] = [];
	$tab_colonne['nomcourt'] = [];
	$tab_colonne['descriptif'] = [];
	$tab_colonne['texte'] = [];
	$tab_colonne['importance'] = [];
	$tab_colonne['objets_en_lien'] = [];
	$tab_colonne['systeme'] = [];
	$tab_colonne['options'] = [];
	$tab_colonne['id_parent'] = [];
	$tab_colonne['actif'] = [];
	$tab_colonne['created_at'] = ['type' => 'date-eu'];

    $tab_colonne['action']=["orderable"=> false];
    $tab_colonne = datatable_complete_liste_colonne($tab_colonne);

    return $tab_colonne;

}



function action_motgroupe_liste_dataliste()
{
    return objet_liste_dataliste();
}


