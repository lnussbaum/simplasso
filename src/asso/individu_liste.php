<?php


function individu_liste()
{
    global $app;
    $args_twig = [
        'tab_col' => individu_colonnes(),
        'tab_filtres' => individu_filtres()
    ];
    return $app['twig']->render(fichier_twig(), $args_twig);
}


function individu_colonnes()
{

    $tab_colonne = array();
    $tab_colonne['id_individu'] = ['title' => 'id'];
    $tab_colonne['nom'] = [];
    $tab_colonne['nom_famille'] = [];
    $tab_colonne['prenom'] = [];
    $tab_colonne['telephones'] = ["orderable" => false, 'title' => 'Mobile/téléphone'];
    $tab_colonne['adresse'] = [];
    $tab_colonne['ville'] = [];
    $tab_colonne['email'] = [];

    $tab_colonne['action'] = ["orderable" => false];
    $tab_colonne = datatable_complete_liste_colonne($tab_colonne);

    return $tab_colonne;

}


function individu_filtres()
{

    global $app;
    $request = $app['request'];
    $rechercher = $request->get('search');
    $tab_filtres['premier']['search'] = filtre_ajouter_recherche($rechercher['value']);

    $tab_filtres['premier'] = array_merge($tab_filtres['premier'], filtre_ajouter_mots('individu'));
    $est_adherent = $request->get('est_adherent') ? $request->get('est_adherent') : array();
    $tab_filtres['premier']['est_adherent'] = filtre_ajouter_est_adherent($est_adherent);
    $tab_filtre_second = [
        'coordonnees' => ['email', 'adresse', 'mobile', 'telephone', 'telephone_pro'],
        'divers' => ['est_decede']
    ];
    foreach ($tab_filtre_second as $cat => $tab_fs) {
        foreach ($tab_fs as $nom_filtre) {
            $valeur_filtre = $request->get($nom_filtre);
            if ($valeur_filtre === null && isset($selection_defaut[$nom_filtre])) {
                $valeur_filtre = $selection_defaut[$nom_filtre];
            }
            $tab_filtres['second'][$cat][$nom_filtre] = filtre_prepare_donnee($nom_filtre, filtre_modele($nom_filtre),
                $valeur_filtre);
        }
    }
    $tab_filtres['second']['geographique'] = filtre_ajouter_geo();
    $tab_filtres['second']['divers']['limitation_id'] = filtre_ajouter_limitation_id();
    $tab_filtres['second']['divers']['inverse_selection'] = filtre_ajouter_inverse();

    return $tab_filtres;
}


function action_individu_liste_dataliste()
{
    return objet_liste_dataliste();
}


function individu_dataliste_complement($tab, $tab_data)
{
    foreach ($tab as $k => $individu) {

        $tab_data[$k]['telephones'] = $individu->getTelephones();
    }
    return $tab_data;
}


function action_individu_liste_etiquettes()
{
    global $app;
    include($app['basepath'] . '/src/systeme/etiquette.php');
    return imprimer_etiquette('individu');
}


function action_individu_liste_export_vcard()
{
    global $app;
    include($app['basepath'] . '/src/inc/exports.php');
    $erreurs = action_export_vcard('individu');
    return $erreurs;
}


function action_individu_liste_rechercher()
{
    global $app;
    $sous_requete = getSelectionObjet();
    $tab_data = array();
    $tab_data2 = array();
    $tab_tri = tri_dataliste();
    $tab_id_individus = $app['db']->fetchAll($sous_requete . ' LIMIT 0,10');
    foreach ($tab_id_individus as &$result) {
        $result = $result['id_individu'];
    }
    $tab_individus = IndividuQuery::getAll($tab_id_individus, $tab_tri);
    foreach ($tab_individus as $individu) {
        $tab_data[] = array(
            'id' => $individu->getIdIndividu(),
            'nom' => $individu->getNom(),
            'nom_famille' => $individu->getNomFamille(),
            'prenom' => $individu->getPrenom(),
            'adresse' => $individu->getAdresse(),
            'code_postal' => $individu->getCodePostal(),
            'ville' => $individu->getVille(),
            'email' => $individu->getEmail()
        );
    }
    foreach ($tab_individus as $individu) {

        $tab_data2[] = $individu->getNom();

    }

    return $app->json($tab_data);
}


function action_individu_liste_export_individu()
{
    global $app;
    include($app['basepath'] . '/src/inc/exports.php');
    $message = action_export('individu');
    $app['session']->getFlashBag()->add('warning', $message);
    return $app->redirect($app->path('individu_liste'));
}

