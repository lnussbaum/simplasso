<?php

use GuzzleHttp\Client as GuzzleHttpClient;
use JsonRPC\Client;
use Ovh\Api as Ovhapi;
use Propel\Silex\PropelServiceProvider;
use Silex\Provider\FormServiceProvider;
use Silex\Provider\HttpCacheServiceProvider;
use Silex\Provider\MonologServiceProvider;
use Silex\Provider\SecurityServiceProvider;
use Silex\Provider\SessionServiceProvider;
use Silex\Provider\TranslationServiceProvider;
use Silex\Provider\TwigServiceProvider;
use Silex\Provider\ValidatorServiceProvider;
use Symfony\Component\Security\Core\Encoder\PasswordEncoder;
use Symfony\Component\Translation\Loader\YamlFileLoader;
use Symfony\Component\Validator\Constraints as Assert;
use WhoopsSilex\WhoopsServiceProvider;

if ($app['debug']) {
    $app->register(new WhoopsServiceProvider);

}
$app->register(new HttpCacheServiceProvider());
$app->register(new SessionServiceProvider(), [
    'session.storage.options' => [
        'name' => $app['session.variable.name']
    ]
]);
$app->register(new ValidatorServiceProvider());
$app->register(new FormServiceProvider());
$app->register(new Silex\Provider\DoctrineServiceProvider(), array('dbs.options' => $app['db.options']));
$app->register(new Moust\Silex\Provider\CacheServiceProvider(), array(
    'cache.options' => $app['cache.driver']
));

$app['security.jwt'] = [
    'key' => "sssssss",
    'lifetime'=>86400,//$app['securite.cookie.lifetime_jwt'],
    'options' => [
        'username_claim' => 'name', // default name, option specifying claim containing username
        'header_name' => 'X-Access-Token', // default null, option for usage normal oauth2 header
        'token_prefix' => 'Bearer',
    ]
];

$app->register(new SecurityServiceProvider());
$app->register(new Silex\Provider\SecurityJWTServiceProvider());
$app->register(new Silex\Provider\RememberMeServiceProvider());
$app['users'] = function () use ($app) {
    return new UserProvider($app['db']);
};$app['users_api'] = function () use ($app) {
    return new UserProviderAPI($app['db']);
};


$app['security.firewalls'] = array(

    'login' => array(
        'pattern' => '/(apilogin|login$|oauth)',
        'anonymous' => true
    ),
    'api' => array(
        'pattern' => '^/(apiservice|api)/$',
        'logout' => array('logout_path' => '/logout'),
        'jwt' => array(
            'use_forward' => true,
            'require_previous_session' => false,
            'stateless' => true,
        ),
        'users' => $app['users_api'],
    ),
    'default' => array(
        'pattern' => '^.*$',
        'form' => array(
            'login_path' => '/login',
            'username_parameter' => 'login[username]',
            'password_parameter' => 'login[password]'
        ),
        'logout' => true,
        'anonymous' => false,
        'remember_me' => array(
            'key' => $app['securite.cookie.secret_key'],
            'lifetime'=>$app['securite.cookie.lifetime'],
        ),
        'users' => $app['users']
    ),
);
$app['security.default_encoder'] = function () use ($app) {
    return new PasswordEncoder();
};

ini_set('date.timezone', $app['date.timezone']);
date_default_timezone_set($app['date.timezone']);
$app->register(new Silex\Provider\LocaleServiceProvider());
$app->register(new TranslationServiceProvider(), array(
    'locale_fallbacks' => array('fr'),
));
$app->extend('translator', function ($translator, $app) {
    $translator->addLoader('yaml', new YamlFileLoader());
    foreach ($app['translator.messages'] as $locale => $files) {
        foreach ($files as $file) {
            $translator->addResource('yaml', $file, $locale);
        }
    }
    return $translator;
});


$app->register(new MonologServiceProvider(), array(
    'monolog.logfile' => $app['log.path'] . '/app.log',
    'monolog.name' => 'app',
    'monolog.level' => $app['log.level']// = Logger::WARNING
));

$app->register(new TwigServiceProvider(), array(
    'twig.options' => array(
        'cache' => isset($app['twig.options.cache']) ? $app['twig.options.cache'] : false,
        'strict_variables' => true
    ),
    'twig.path' => [$app['resources.path'] . '/views']
));

$app->register(new PropelServiceProvider(), array(
    'propel.config_file' => $app['config.propel'],
    'propel.model_path' => $app['basepath'] . '/src/simplasso/'
));

$app->register(new Silex\Provider\SwiftmailerServiceProvider(), array(
    'swiftmailer.options' => $app['swiftmailer.options'],
    'swiftmailer.use_spool' => false

));

$app->register(new Silex\Provider\AssetServiceProvider(), array(
    'assets.version' => 'v1',
    'assets.version_format' => '%s?version=%s',
   /* 'assets.named_packages' => array(
        'css' => array('version' => 'css2', 'base_path' => '/whatever-makes-sense'),
        'images' => array('base_urls' => array('https://img.example.com')),
    ),*/
));

if ($app['apispip']) {
    $client = new Client($app['apispip_url']);
    $client->getHttpClient()
        ->withUsername($app['apispip_login'])
        ->withPassword($app['apispip_mot_de_passe'])
        ->withoutSslVerification();
    $app['API_spip'] = $client;
}

if ($app['apiovh']) {
    $client = new GuzzleHttpClient();
    $app['API_ovh'] = new Ovhapi(
        $app['apiovh_applicationkey'],
        $app['apiovh_applicationsecret'],
        $app['apiovh_endpoint'],
        $app['apiovh_consumer_key']);
}


$app['request'] = function () use ($app) {
    return $app['request_stack']->getCurrentRequest();
};

require_once($app['basepath'] . '/src/inc/fonctions_twig_basic.php');



return $app;
