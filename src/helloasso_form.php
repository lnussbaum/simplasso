<?php


use Symfony\Component\Form\Extension\Core\Type\FileType;
use Symfony\Component\Form\Extension\Core\Type\FormType;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\Form\FormError as FormError;
use Symfony\Component\Validator\Constraints as Assert;

include_once($app['basepath'] . '/src/inc/fonctions_ged.php');

function helloasso_form(){
    global $app;
    $args_rep=[];
    $request = $app['request'];
    $data = array();
    $modification = false;

    $builder = $app['form.factory']->createNamedBuilder('helloasso', FormType::class, $data);
    formSetAction($builder,$modification);
    $builder->setRequired(false);


        //todo voir gestion en js auto effacer import etapes
        $form = $builder
            ->add('fichier', FileType::class,
                [
                    'data' => '',
                    'label' => $app->trans('import_fichier'),
                    'constraints' => new Assert\NotBlank()
                ])
        ->add('submit', SubmitType::class,
            array('label' => $app->trans('Lancer'), 'attr' => array('class' => 'btn-primary')))
        ->getForm();

    $form->handleRequest($request);

    if ($form->isSubmitted()) {
        $data = $form->getData();

        if ($form->isValid()) {// todo limite de 2Mo pour CSV le 5/1/2017 Adav
            if (!$modification) {


                $file = $data['fichier'];
                $nom = $file->getClientOriginalName();
                $mime = $file->getmimeType();
                $size = $file->getsize();
                $error = $file->geterror();
                $pathname = $file->getpathName();
                $nomprovisoire = $file->getfileName();
                $tempextension = $file->guessExtension();
                $controlemd5 = $file->guessExtension();
                if (!$tempextension) {
                    $extension = 'bin';
                }


                $objet_data = new Import();
                $objet_data->setNom($nom);
                $objet_data->setNomprovisoire($nomprovisoire);
                $objet_data->setTaille($size);
                $objet_data->setControlemd5("xx");
                $objet_data->setOperation('helloasso');
                $objet_data->setAvancement(0);
                //todo ajouter MD5
                $objet_data->setNbLigne(0);
                $objet_data->save();




                ged_televerser($file,['import'=>$objet_data->getPrimaryKey()] );


                tache_ajouter('import',"Taches d'imporation des fichiers hello asso",['operation'=>'helloasso']);
                if (TacheQuery::create()->filterByStatut(array(0, 1))->filterByFonction('import')->count() == 0) {
                    $tache = new Tache();
                    $date_exec = new DateTime();
                    $tache->fromArray([
                        'descriptif' => "Taches d'imporation des fichiers hello asso",
                        'fonction' => 'import',
                        'args' => ['operation'=>'helloasso'],
                        'statut' => '0',
                        'priorité' => '0',
                        'date_execution' => $date_exec

                    ]);

                    $tache->save();
                }


            }
        }
    }
    return reponse_formulaire($form);
}

